﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.Custom;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderForReportDataEditView {
        private QueryWindowBase queryWindow;
        private bool isIfDirectProvision = true;
        private bool isCanCheck = true;
        private bool dlHasWarehouse = true;
        private decimal lnotApprovePrice;
        private ObservableCollection<PreOrder> preOrders;
        private decimal lorderUseablePosition;
        private ObservableCollection<KeyValuePair> kvProvinceNames;
        private ObservableCollection<KeyValuePair> kvCityNames;
        private ObservableCollection<KeyValuePair> kvShippingMethods;
        private readonly List<TiledRegion> TiledRegions = new List<TiledRegion>();

        //本地缓存，记录修改前的配件销售订单类型Id
        private int oldPartsSalesOrderTypeId;

        public ObservableCollection<KeyValuePair> KvProvinceNames {
            get {
                return this.kvProvinceNames ?? (this.kvProvinceNames = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvCityNames {
            get {
                return this.kvCityNames ?? (this.kvCityNames = new ObservableCollection<KeyValuePair>());
            }
        }
        private QueryWindowBase QueryWindow {
            get {
                if(this.queryWindow == null) {
                    this.queryWindow = DI.GetQueryWindow("SecondClassStationPlan");
                    this.queryWindow.Loaded += this.QueryWindow_Loaded;
                    this.queryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
                }
                return this.queryWindow;
            }
        }

        private void QueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var qw = sender as QueryWindowBase;
            if(qw == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            qw.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Status", (int)DcsSecondClassStationPlanStatus.审核通过
            });
            qw.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "PartsSalesCategoryId", partsSalesOrder.SalesCategoryId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Status", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsSalesCategoryId", false
            });
        }

        private RadWindow radWindow;

        private RadWindow RadWindow {
            get {
                return this.radWindow ?? (this.radWindow = new RadWindow());
            }
        }

        private void QueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var secondClassStationPlan = queryWindow.SelectedEntities.Cast<SecondClassStationPlan>().FirstOrDefault();
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(secondClassStationPlan == null || partsSalesOrder == null)
                return;

            this.DomainContext.Load(this.DomainContext.GetSecondClassStationPlanDetailsQuery().Where(r => r.SecondClassStationPlanId == secondClassStationPlan.Id && r.ProcessMode == (int)DcsProcessMode.转销售订单), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null) {
                    foreach(var entity in loadOp.Entities) {
                        if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.SparePartId == entity.SparePartId)) {
                            var detail = partsSalesOrder.PartsSalesOrderDetails.First();
                            detail.OrderedQuantity += 1;
                            detail.OrderSum = detail.OrderedQuantity * detail.OrderPrice;
                            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                        } else {
                            var partsSalesOrderDetail = new PartsSalesOrderDetail {
                                SparePartCode = entity.SparePartCode,
                                SparePartId = entity.SparePartId,
                                SparePartName = entity.SparePartName,
                                IfCanNewPart = true,
                                OrderedQuantity = (int)entity.Quantity
                            };
                            //获取订货价格
                            this.DomainContext.Load(this.DomainContext.根据销售类型查询配件销售价3Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, partsSalesOrder.IfDirectProvision, partsSalesOrder.SalesUnitOwnerCompanyId, partsSalesOrder.WarehouseId ?? 0, null, null, null, null), LoadBehavior.RefreshCurrent, loadOp3 => {
                                if(loadOp3.HasError) {
                                    if(!loadOp3.IsErrorHandled)
                                        loadOp3.MarkErrorAsHandled();
                                    return;
                                }
                                var partsOrderPrice = loadOp3.Entities.FirstOrDefault(r => r.SparePartId == entity.SparePartId);
                                if(partsOrderPrice != null) {
                                    if(partsOrderPrice.PartsTreatyPrice != null)
                                        partsSalesOrderDetail.OrderPrice = partsOrderPrice.PartsTreatyPrice.Value;
                                    else
                                        partsSalesOrderDetail.OrderPrice = partsOrderPrice.Price;
                                    partsSalesOrderDetail.MeasureUnit = partsOrderPrice.MeasureUnit;
                                    partsSalesOrderDetail.CustOrderPriceGradeCoefficient = partsOrderPrice.Coefficient;
                                    partsSalesOrderDetail.OriginalPrice = partsOrderPrice.RetailGuidePrice;
                                    partsSalesOrderDetail.DiscountedPrice = partsSalesOrderDetail.OriginalPrice - partsSalesOrderDetail.OrderPrice;
                                    partsSalesOrderDetail.PriceTypeName = partsOrderPrice.PriceTypeName;
                                    partsSalesOrderDetail.ABCStrategy = partsOrderPrice.ABCStrategy;
                                    partsSalesOrderDetail.OrderSum = partsSalesOrderDetail.OrderedQuantity * partsSalesOrderDetail.OrderPrice;
                                    partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                                }
                            }, null);
                            partsSalesOrder.PartsSalesOrderDetails.Add(partsSalesOrderDetail);
                        }
                    }
                }
            }, null);
        }

        private readonly ObservableCollection<KeyValuePair> kvReceivingWarehouse = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<SalesUnitAffiWarehouse> kvSalesUnitAffiWarehouses;
        private Company company;

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return kvPartsSalesCategories;
            }
        }

        public ObservableCollection<PreOrder> PreOrders {
            get {
                return this.preOrders ?? (this.preOrders = new ObservableCollection<PreOrder>());
            }
        }

        private ObservableCollection<CompanyAddress> kvCompanyAddresses;

        public ObservableCollection<SalesUnitAffiWarehouse> KvSalesUnitAffiWarehouses {
            get {
                return this.kvSalesUnitAffiWarehouses ?? (this.kvSalesUnitAffiWarehouses = new ObservableCollection<SalesUnitAffiWarehouse>());
            }
        }

        private DataGridViewBase partsSalesOrderDetailForEditDataGridView;
        private KeyValueManager keyValueManager;
        private string strFileName;
        private decimal lRebateAmount;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private readonly int? companyType = null;

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsSalesOrder = this.DataContext as PartsSalesOrder;
                        if(partsSalesOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPartsSalesOrderDetailAsync(e.HandlerData.CustomData["Path"].ToString(), partsSalesOrder.IfDirectProvision, partsSalesOrder.SubmitCompanyId, partsSalesOrder.SalesCategoryId);
                        this.excelServiceClient.ImportPartsSalesOrderDetailCompleted -= this.ExcelServiceClient_ImportPartsSalesOrderDetailCompleted;
                        this.excelServiceClient.ImportPartsSalesOrderDetailCompleted += this.ExcelServiceClient_ImportPartsSalesOrderDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void ExcelServiceClient_ImportPartsSalesOrderDetailCompleted(object sender, ImportPartsSalesOrderDetailCompletedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var detailList = partsSalesOrder.PartsSalesOrderDetails.ToList();
            foreach(var detail in detailList) {
                partsSalesOrder.PartsSalesOrderDetails.Remove(detail);
            }

            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    var partsSalesOrderDetail = new PartsSalesOrderDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        EstimatedFulfillTime = data.EstimatedFulfillTime,
                        OrderedQuantity = data.OrderedQuantity,
                        IfCanNewPart = data.IfCanNewPart,
                        Remark = data.Remark
                    };
                    partsSalesOrder.PartsSalesOrderDetails.Add(partsSalesOrderDetail);
                }
                var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                this.DomainContext.Load(this.DomainContext.根据销售类型查询配件销售价2Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, partsSalesOrder.IfDirectProvision, sparePartIds, partsSalesOrder.SalesUnitOwnerCompanyId, partsSalesOrder.WarehouseId ?? 0), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        return;
                    }
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        var entity = loadOp.Entities.FirstOrDefault();
                        if(entity != null) {
                            foreach(var partsSalesOrderDetails in partsSalesOrder.PartsSalesOrderDetails) {
                                var partsOrderPrice = loadOp.Entities.FirstOrDefault(r => r.SparePartId == partsSalesOrderDetails.SparePartId);
                                if(partsOrderPrice != null) {
                                    if(partsOrderPrice.PartsTreatyPrice != null)
                                        partsSalesOrderDetails.OrderPrice = partsOrderPrice.PartsTreatyPrice.Value;
                                    else
                                        partsSalesOrderDetails.OrderPrice = partsOrderPrice.Price;
                                    partsSalesOrderDetails.MeasureUnit = partsOrderPrice.MeasureUnit;
                                    partsSalesOrderDetails.CustOrderPriceGradeCoefficient = partsOrderPrice.Coefficient;
                                    partsSalesOrderDetails.OriginalPrice = partsOrderPrice.RetailGuidePrice;
                                    partsSalesOrderDetails.DiscountedPrice = partsSalesOrderDetails.OriginalPrice - partsSalesOrderDetails.OrderPrice;
                                    partsSalesOrderDetails.OrderSum = partsSalesOrderDetails.OrderedQuantity * partsSalesOrderDetails.OrderPrice;
                                    partsSalesOrderDetails.PriceTypeName = partsOrderPrice.PriceTypeName;
                                    partsSalesOrderDetails.ABCStrategy = partsOrderPrice.ABCStrategy;
                                    partsSalesOrderDetails.SalesPrice = partsOrderPrice.SalesPrice;
                                    //partsSalesOrderDetails.MInSaleingAmount = partsOrderPrice.MInPackingAmount;
                                }
                            }
                        }
                        partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                    }
                }, null);
                if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单") {
                    this.DomainContext.Load(this.DomainContext.GetSparePartsByIdsQuery(sparePartIds).Where(o => o.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.Entities == null || !loadOp1.Entities.Any()) {
                            UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoSparepart);
                            return;
                        }
                        if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
                            var spareParts = loadOp1.Entities.ToArray();
                            foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                var sparePart = spareParts.FirstOrDefault(o => o.Id == detail.SparePartId);
                                if(sparePart != null) {
                                    detail.MInSaleingAmount = sparePart.MInPackingAmount.Value;
                                }
                            }
                        }
                    }, null);
                } else {
                    this.DomainContext.Load(this.DomainContext.获取当前企业的配件营销包装属性Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.BranchId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.Entities == null || !loadOp2.Entities.Any()) {
                            UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoPartsBranchPackingPop);
                            return;
                        }
                        if(loadOp2.Entities != null && loadOp2.Entities.Any()) {
                            var partsBranchPackingProps = loadOp2.Entities.ToArray();

                            foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                var partsBranchPackingProp = partsBranchPackingProps.FirstOrDefault(o => o.SparePartId == detail.SparePartId);
                                if(partsBranchPackingProp != null) {
                                    detail.MInSaleingAmount = partsBranchPackingProp.PackingCoefficient.Value;
                                }
                            }
                        }
                    }, null);
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private readonly string[] kvNames = {
            "PartsSalesOrder_SecondLevelOrderType", "PartsShipping_Method"
        };

        public PartsSalesOrderForReportDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsSalesOrderForReportDataEditView_DataContextChanged;
            this.Loaded += PartsSalesOrderForReportDataEditView_Loaded;
        }
        private void Initialize() {
            this.DomainContext.Load(this.DomainContext.GetTiledRegionsQuery().OrderBy(ex => ex.ProvinceName), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var repairObject in loadOp.Entities)
                    this.TiledRegions.Add(new TiledRegion {
                        Id = repairObject.Id,
                        ProvinceName = repairObject.ProvinceName,
                        CityName = repairObject.CityName,

                    });
                var partsSalesOrder = this.DataContext as PartsSalesOrder;
                this.KvProvinceNames.Clear();
                if(partsSalesOrder != null && partsSalesOrder.CustomerType == (int)DcsCompanyType.代理库) {
                    this.DomainContext.Load(this.DomainContext.getCenterAuthorizedProvincesForAgencyQuery(partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var centerAithorized in loadOp1.Entities) {
                            foreach(var item in TiledRegions) {
                                if(item.Id == centerAithorized.ProvinceId) {
                                    KvProvinceNames.Add(new KeyValuePair {
                                        Key = item.Id,
                                        Value = item.ProvinceName
                                    });
                                }
                            }
                        }
                    }, null);
                } else {
                    foreach(var item in TiledRegions) {
                        if(partsSalesOrder != null) {
                            if(item.Id == partsSalesOrder.ProvinceID) {
                                KvProvinceNames.Add(new KeyValuePair {
                                    Key = item.Id,
                                    Value = item.ProvinceName
                                });
                            }
                        }
                        //else {
                        //    if (item.CityName == null && item.CountyName == null) {
                        //        KvProvinceNames.Add(new KeyValuePair {
                        //            Key = item.Id,
                        //            Value = item.ProvinceName
                        //        });
                        //    }
                        //}
                    }
                }

            }, null);
            this.DomainContext.Load(this.DomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "PartsShipping_Method" && r.Key != (int)DcsPartsShippingMethod.DHL_出口 && r.Key != (int)DcsPartsShippingMethod.海运_出口 && r.Key != (int)DcsPartsShippingMethod.空运_出口 && r.Key != (int)DcsPartsShippingMethod.铁路_出口), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesOrderTypes.Clear();
                foreach(var partsSalesOrderType in loadOp.Entities) {
                    this.KvShippingMethods.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Key,
                        Value = partsSalesOrderType.Value,
                    });
                }
            }, null);
        }

        private void PartsSalesOrderForReportDataEditView_Loaded(object sender, RoutedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(this.KvCompanyAddresses.Count == 1) {
                if(partsSalesOrder != null) {
                    if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress)) {
                        partsSalesOrder.ReceivingAddress = this.KvCompanyAddresses.First().DetailAddress;
                        partsSalesOrder.ContactPerson = this.KvCompanyAddresses.First().ContactPerson;
                        partsSalesOrder.ContactPhone = this.KvCompanyAddresses.First().ContactPhone;
                        partsSalesOrder.CompanyAddressId = this.KvCompanyAddresses.First().Id;
                    }
                }
            }
            if(partsSalesOrder != null && this.KvBranches.Any() && this.EditState == DataEditState.New) {
                partsSalesOrder.BranchId = this.KvBranches.First().Key;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void PartsSalesOrderForReportDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            this.DetachUnexpectedRepairContracts();
            partsSalesOrder.PropertyChanged -= this.PartsSalesOrder_PropertyChanged;
            partsSalesOrder.PropertyChanged += this.PartsSalesOrder_PropertyChanged;
            partsSalesOrder.PartsSalesOrderDetails.EntityRemoved -= this.PartsSalesOrderDetails_EntityRemoved;
            partsSalesOrder.PartsSalesOrderDetails.EntityRemoved += this.PartsSalesOrderDetails_EntityRemoved;
            if(this.DomainContext == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPersonSubDealersQuery().Where(r => r.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                if(loadOp.Entities == null || !loadOp.Entities.Any())
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.DomainContext.Load(this.DomainContext.GetSubDealersQuery().Where(r => r.Id == entity.SubDealerId), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.HasError)
                            if(!loadOp2.IsErrorHandled) {
                                loadOp2.MarkErrorAsHandled();
                                return;
                            }
                        if(loadOp2.Entities == null || !loadOp2.Entities.Any())
                            return;
                        var subDealer = loadOp2.Entities.FirstOrDefault();
                        if(subDealer != null) {
                            partsSalesOrder.FirstClassStationCode = subDealer.Code;
                            partsSalesOrder.FirstClassStationId = subDealer.Id;
                            partsSalesOrder.FirstClassStationName = subDealer.Name;
                        }
                    }, null);
                }
            }, null);

            this.KvPartsSalesOrderTypes.Clear();
            kvPartsSalesCategories.Clear();
        }

        // 移除意外产生的PartsSalesOrder
        private void DetachUnexpectedRepairContracts() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(this.DomainContext != null) {
                foreach(var item in this.DomainContext.PartsSalesOrders.Where(t => t.EntityState == EntityState.New || t.EntityState == EntityState.Modified).ToArray()) {
                    if(item != partsSalesOrder)
                        this.DomainContext.PartsSalesOrders.Detach(item);
                }
            }
        }

        private void PartsSalesOrderDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsSalesOrderDetail> e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
            //获取配件编号 在预订单集合中删除 所选预订单

            if(this.PreOrders.All(r => r.PartCode != e.Entity.SparePartCode))
                return;
            var removePreorder = this.PreOrders.Where(r => r.PartCode == e.Entity.SparePartCode).ToArray();
            foreach(var preOrder in removePreorder)
                this.PreOrders.Remove(preOrder);
        }

        private void PartsSalesOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(this.DomainContext == null) {
                this.DomainContext = new DcsDomainContext();
            }
            switch(e.PropertyName) {
                case "WarehouseId":
                    if(!isCanCheck) {
                        isCanCheck = true;
                        return;
                    }
                    if(!partsSalesOrder.WarehouseId.HasValue || partsSalesOrder.WarehouseId == default(int))
                        return;
                    if(partsSalesOrder.PartsSalesOrderDetails.Any()) {
                        var detailList = partsSalesOrder.PartsSalesOrderDetails.ToList();
                        foreach(var partsSalesOrderDetail in detailList) {
                            partsSalesOrder.PartsSalesOrderDetails.Remove(partsSalesOrderDetail);
                        }
                    }
                    this.KvPartsSalesOrderTypes.Clear();
                    var salesUnitKv = this.KvSalesUnitAffiWarehouses.FirstOrDefault(kv => kv.WarehouseId == partsSalesOrder.WarehouseId);
                    if(salesUnitKv == null)
                        return;
                    var salesUnitInsert = salesUnitKv;
                    if(salesUnitInsert.SalesUnit == null)
                        return;
                    partsSalesOrder.SalesUnitId = salesUnitInsert.SalesUnitId;
                    partsSalesOrder.SalesUnitCode = salesUnitInsert.SalesUnit.Code;
                    partsSalesOrder.SalesUnitName = salesUnitInsert.SalesUnit.Name;
                    partsSalesOrder.SalesUnitOwnerCompanyId = salesUnitInsert.SalesUnit.OwnerCompanyId;
                    var parameteras = new Dictionary<string, object>();
                    parameteras.Add("submitCompanyId", partsSalesOrder.SubmitCompanyId);
                    parameteras.Add("salesUnitId", partsSalesOrder.SalesUnitId);
                    parameteras.Add("partsSalesCategoryId", partsSalesOrder.SalesCategoryId);

                    this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == salesUnitInsert.SalesUnit.OwnerCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOp.Entities != null && loadOp.Entities.Any()) {
                            partsSalesOrder.SalesUnitOwnerCompanyId = loadOp.Entities.First().Id;
                            partsSalesOrder.SalesUnitOwnerCompanyCode = loadOp.Entities.First().Code;
                            partsSalesOrder.SalesUnitOwnerCompanyName = loadOp.Entities.First().Name;
                            partsSalesOrder.SalesUnitOwnerCompanyType = loadOp.Entities.First().Type;
                        }
                    }, null);

                    //this.DomainContext.Load(this.DomainContext.GetPartsSalesOrdersQuery().Where(r=>r.SubmitCompanyId == partsSalesOrder.SubmitCompanyId&& r.SalesUnitId == partsSalesOrder.SalesUnitId && r.SalesCategoryId == partsSalesOrder.SalesCategoryId && r.Status == (int)DcsPartsSalesOrderStatus.新增))

                    //if(partsSalesOrder.SalesUnitOwnerCompanyId != default(int)) {
                    //    this.DomainContext.Load(DomainContext.GetCompaniesQuery().Where(v => v.Id == partsSalesOrder.SalesUnitOwnerCompanyId && v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                    //        if(loadOp.HasError)
                    //            return;
                    //        if(loadOp.Entities.Any()) {
                    //            partsSalesOrder.SalesUnitOwnerCompanyCode = loadOp.Entities.First().Code;
                    //            partsSalesOrder.SalesUnitOwnerCompanyName = loadOp.Entities.First().Name;
                    //        } else
                    //            UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_SalesUnitOwnerCompanyIsNull);
                    //    }, null);
                    //}

                    partsSalesOrder.SalesUnit = salesUnitInsert.SalesUnit;
                    if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库) {
                        this.DomainContext.Load(this.DomainContext.GetSalesUnitsQuery().Where(v => v.PartsSalesCategoryId == partsSalesOrder.SalesUnit.PartsSalesCategoryId && v.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }
                            if(loadOp.Entities.Any())
                                foreach(var salesUnit in loadOp.Entities)
                                    this.DomainContext.Load(DomainContext.GetSalesUnitAffiWarehousesQuery().Where(v => v.SalesUnitId == salesUnit.Id), LoadBehavior.RefreshCurrent, loadOp2 => {
                                        if(loadOp2.HasError) {
                                            loadOp2.MarkErrorAsHandled();
                                            return;
                                        }
                                        if(!loadOp2.Entities.Any()) {
                                            UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ReceivingWarehouse);
                                            this.dlHasWarehouse = false;
                                        }
                                    }, null);
                            else {
                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ReceivingWarehouse);
                                this.dlHasWarehouse = false;
                            }
                        }, null);
                        this.DomainContext.Load(this.DomainContext.GetAgenciesQuery().Where(agency => agency.Id == partsSalesOrder.SalesUnitId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }

                            if(this.companyType == (int)DcsCompanyType.代理库 || this.companyType == null)
                                return;
                            var agency = loadOp.Entities.FirstOrDefault();
                            if(agency == null) {
                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_AgencyDealerRelationNotExists);
                                return;
                            }
                            this.DomainContext.Load(this.DomainContext.GetAgencyDealerRelationsQuery().Where(r => r.BranchId == partsSalesOrder.BranchId && r.DealerId == BaseApp.Current.CurrentUserData.EnterpriseId && r.AgencyId == partsSalesOrder.SalesUnitOwnerCompanyId), loadOpRelation => {
                                if(loadOpRelation.HasError) {
                                    loadOpRelation.MarkErrorAsHandled();
                                    return;
                                }
                                if(loadOpRelation.Entities.FirstOrDefault() == null)
                                    UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_AgencyDealerRelationNotExists);
                            }, null);
                        }, null);
                    }
                    this.DomainContext.InvokeOperation("查询未审核订单金额", typeof(decimal), parameteras, true, invokeOp => {
                        if(invokeOp.HasError)
                            return;
                        this.notApprovePrice.Text = "￥" + invokeOp.Value.ToString();
                        this.lnotApprovePrice = decimal.Parse(invokeOp.Value.ToString());

                        this.DomainContext.Load(this.DomainContext.查询客户可用资金Query(null, partsSalesOrder.SalesUnit.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }
                            var customerAccount = loadOp.Entities.FirstOrDefault();
                            if(customerAccount == null) {
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_CustomerAccountIsNull);
                                partsSalesOrder.CustomerAccount = null;
                                return;
                            }
                            partsSalesOrder.CustomerAccountId = customerAccount.Id;
                            partsSalesOrder.CustomerAccount = customerAccount;
                            this.DomainContext.Load(this.DomainContext.GetPartsRebateAccountsQuery().Where(r => r.AccountGroupId == partsSalesOrder.SalesUnit.AccountGroupId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.BranchId == salesUnitInsert.SalesUnit.OwnerCompanyId), LoadBehavior.RefreshCurrent, loadRebateAccount => {
                                if(loadRebateAccount.HasError) {
                                    loadRebateAccount.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadRebateAccount);
                                    return;
                                }
                                if(loadRebateAccount.Entities.Any()) {
                                    this.txtRebateAmount.Text = loadRebateAccount.Entities.First().AccountBalanceAmount.ToString("c2");
                                    this.lRebateAmount = loadRebateAccount.Entities.First().AccountBalanceAmount;
                                    this.txtUseablePosition.Text = (partsSalesOrder.CustomerAccount.UseablePosition).ToString("c2");
                                } else {
                                    this.txtRebateAmount.Text = "￥" + "0.00";
                                    this.txtUseablePosition.Text = partsSalesOrder.CustomerAccount.UseablePosition.ToString("c2");
                                }
                                this.txtCredenceAmount.Text = (partsSalesOrder.CustomerAccount.CredenceAmount + (partsSalesOrder.CustomerAccount.SIHCredit ?? 0)).ToString("c2");
                                this.orderUseablePosition.Text = (partsSalesOrder.CustomerAccount.AccountBalance + lRebateAmount + partsSalesOrder.CustomerAccount.CustomerCredenceAmount + (partsSalesOrder.CustomerAccount.TempCreditTotalFee ?? 0) + (partsSalesOrder.CustomerAccount.SIHCredit ?? 0) - partsSalesOrder.CustomerAccount.LockBalance - lnotApprovePrice).ToString("C");
                                this.lorderUseablePosition = partsSalesOrder.CustomerAccount.AccountBalance + lRebateAmount + partsSalesOrder.CustomerAccount.CustomerCredenceAmount + (partsSalesOrder.CustomerAccount.TempCreditTotalFee ?? 0) + (partsSalesOrder.CustomerAccount.SIHCredit ?? 0) - partsSalesOrder.CustomerAccount.LockBalance - lnotApprovePrice;
                            }, null);
                        }, null);
                    }, null);
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypeWithCustomerOrderPriceGradesQuery(salesUnitInsert.SalesUnit.PartsSalesCategoryId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        foreach(var partsSalesOrderType in loadOp.Entities) {
                            this.KvPartsSalesOrderTypes.Add(new KeyValuePair {
                                Key = partsSalesOrderType.Id,
                                Value = partsSalesOrderType.Name
                            });
                        }
                    }, null);
                    break;
                case "BranchId":
                    if(partsSalesOrder.BranchId == 0)
                        return;

                    partsSalesOrder.SalesUnitId = 0;
                    partsSalesOrder.SalesUnitCode = string.Empty;
                    partsSalesOrder.SalesUnitName = string.Empty;
                    partsSalesOrder.SalesUnit = null;
                    partsSalesOrder.SalesCategoryId = 0;
                    partsSalesOrder.SalesCategoryName = string.Empty;
                    partsSalesOrder.ReceivingWarehouseId = 0;
                    partsSalesOrder.ReceivingWarehouseName = string.Empty;
                    partsSalesOrder.InvoiceReceiveSaleCateId = 0;
                    partsSalesOrder.InvoiceReceiveSaleCateName = string.Empty;
                    partsSalesOrder.PartsSalesOrderTypeId = 0;
                    partsSalesOrder.PartsSalesOrderTypeName = string.Empty;
                    partsSalesOrder.CustomerAccount = null;
                    partsSalesOrder.SecondLevelOrderType = default(int);
                    partsSalesOrder.RequestedDeliveryTime = null;
                    partsSalesOrder.RequestedShippingTime = null;
                    partsSalesOrder.ShippingMethod = default(int);
                    partsSalesOrder.Remark = string.Empty;
                    partsSalesOrder.IfDirectProvision = false;
                    if(partsSalesOrder.PartsSalesOrderDetails.Any()) {
                        var detailList = partsSalesOrder.PartsSalesOrderDetails.ToList();
                        foreach(var partsSalesOrderDetail in detailList)
                            partsSalesOrder.PartsSalesOrderDetails.Remove(partsSalesOrderDetail);
                    }
                    //查询品牌
                    this.kvPartsSalesCategories.Clear();
                    if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库 || partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站兼代理库)
                        this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == partsSalesOrder.BranchId), LoadBehavior.RefreshCurrent, loadOption => {
                            if(loadOption.HasError)
                                return;
                            this.KvPartsSalesCategories.Clear();
                            foreach(var partsSalesCategory in loadOption.Entities) {
                                this.KvPartsSalesCategories.Add(new KeyValuePair {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name
                                });
                            }
                            if(!this.KvPartsSalesCategories.Any())
                                return;
                            partsSalesOrder.SalesCategoryId = this.KvPartsSalesCategories.First().Key;
                            partsSalesOrder.SalesCategoryName = this.KvPartsSalesCategories.First().Value;
                        }, null);
                    else
                        this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesByDealerServiceInfoQuery(partsSalesOrder.BranchId, BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError)
                                return;
                            this.KvPartsSalesCategories.Clear();
                            foreach(var partsSalesCategory in loadOp.Entities) {
                                this.KvPartsSalesCategories.Add(new KeyValuePair {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name
                                });
                            }
                            if(!this.KvPartsSalesCategories.Any())
                                return;
                            partsSalesOrder.SalesCategoryId = this.KvPartsSalesCategories.First().Key;
                            partsSalesOrder.SalesCategoryName = this.KvPartsSalesCategories.First().Value;
                        }, null);


                    var branchId = partsSalesOrder.BranchId;
                    if(this.KvBranches.Any(r => r.Key == branchId)) {
                        var branch = this.KvBranches.First(r => r.Key == branchId).UserObject as Branch;
                        if(branch != null) {
                            partsSalesOrder.BranchCode = branch.Code;
                            partsSalesOrder.BranchName = branch.Name;
                        }
                    }
                    break;
                case "SalesCategoryId":
                    this.DomainContext.Load(this.DomainContext.查询销售组织仓库2Query(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesOrder.BranchId, null, partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadop => {
                        if(loadop.HasError) {
                            loadop.MarkErrorAsHandled();
                            return;
                        }
                        this.KvSalesUnitAffiWarehouses.Clear();
                        if(loadop.Entities != null) {
                            foreach(var salesUnitAffiWarehouse in loadop.Entities) {
                                if(salesUnitAffiWarehouse.Warehouse.Type != (int)DcsWarehouseType.虚拟库 && salesUnitAffiWarehouse.Warehouse.Name.IndexOf("出口") < 0)
                                    this.KvSalesUnitAffiWarehouses.Add(salesUnitAffiWarehouse);
                            }
                            this.DomainContext.Load(this.DomainContext.GetAgenciesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Id == partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOption1 => {
                                if(loadOption1.HasError)
                                    return;
                                if(loadOption1.Entities.Count() != 0) {
                                    partsSalesOrder.WarehouseId = loadOption1.Entities.FirstOrDefault().WarehouseId;
                                    partsSalesOrder.WarehouseName = loadOption1.Entities.FirstOrDefault().WarehouseName;
                                }
                                if(!partsSalesOrder.WarehouseId.HasValue || (partsSalesOrder.WarehouseId.HasValue && this.KvSalesUnitAffiWarehouses.Any(t => t.Id == partsSalesOrder.WarehouseId))) {
                                    partsSalesOrder.WarehouseId = this.KvSalesUnitAffiWarehouses.First().WarehouseId;
                                }
                            }, null);
                             
                        }
                    }, null);
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypeWithCustomerOrderPriceGradesQuery(partsSalesOrder.SalesCategoryId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        this.KvPartsSalesOrderTypes.Clear();
                        foreach(var partsSalesOrderType in loadOp.Entities) {
                            this.KvPartsSalesOrderTypes.Add(new KeyValuePair {
                                Key = partsSalesOrderType.Id,
                                Value = partsSalesOrderType.Name,
                            });
                        }
                    }, null);
                    if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                        this.blockReceivingWarehouse.Visibility = Visibility.Visible;
                        this.comboBoxReceivingWarehouse.Visibility = Visibility.Visible;
                        this.DomainContext.Load(this.DomainContext.GetWarehousesForSalesOrderReportQuery(partsSalesOrder.SalesCategoryId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadWarehouse => {
                            if(loadWarehouse.HasError) {
                                loadWarehouse.MarkErrorAsHandled();
                                return;
                            }
                            this.kvReceivingWarehouse.Clear();
                            if(loadWarehouse.Entities != null && loadWarehouse.Entities.Any()) {
                                foreach(var warehouse in loadWarehouse.Entities) {
                                    if(warehouse.Type == (int)DcsWarehouseType.虚拟库 || (warehouse.IsQualityWarehouse ?? false))
                                        continue;
                                    this.kvReceivingWarehouse.Add(new KeyValuePair {
                                        Key = warehouse.Id,
                                        Value = warehouse.Name,
                                        UserObject = warehouse
                                    });
                                }
                                var defaultWarehouse = kvReceivingWarehouse.FirstOrDefault();
                                if(defaultWarehouse != null) {
                                    partsSalesOrder.ReceivingWarehouseId = defaultWarehouse.Key;
                                    this.comboBoxReceivingWarehouse.Text = defaultWarehouse.Value;
                                }
                            }
                        }, null);
                    }
                    break;
                case "PartsSalesOrderTypeId":
                    if(this.oldPartsSalesOrderTypeId != partsSalesOrder.PartsSalesOrderTypeId) {
                        if(partsSalesOrder.PartsSalesOrderTypeId == 0)
                            return;

                        this.DomainContext.Load(this.DomainContext.GetCustomerCompanyAndPartsSalesOrderTypeQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesOrderTypeId == partsSalesOrder.PartsSalesOrderTypeId && r.CustomerCompanyId == partsSalesOrder.SubmitCompanyId && r.PartsSalesCategoryId == partsSalesOrder.PartsSalesCategory.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.Entities == null || !loadOp.Entities.Any()) {
                                partsSalesOrder.PartsSalesOrderTypeId = 0;
                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_PartsSalesOrderTypeNameisNull);
                                return;
                            }
                            if(partsSalesOrder.PartsSalesOrderTypeName != "正常订单") {
                                ShippingMethod.IsEnabled = true;
                                this.TextBoxContactPerson.IsReadOnly = false;
                                this.TextBoxContactPhone.IsReadOnly = false;
                            } else {
                                partsSalesOrder.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                                ShippingMethod.IsEnabled = false;
                                this.TextBoxContactPerson.IsReadOnly = true;
                                this.TextBoxContactPhone.IsReadOnly = true;
                            }
                            if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单" || partsSalesOrder.PartsSalesOrderTypeName == "油品订单")
                                partsSalesOrder.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
                            if(partsSalesOrder.PartsSalesOrderTypeName == "油品订单")
                                partsSalesOrder.IfDirectProvision = true;
                            else
                                partsSalesOrder.IfDirectProvision = false;

                            if(!partsSalesOrder.PartsSalesOrderDetails.Any()) {
                                this.oldPartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
                                this.loadCompanyAddress();
                                return;
                            }

                            if(partsSalesOrder.PartsSalesOrderTypeName == "油品订单") {
                                foreach(var detail in partsSalesOrder.PartsSalesOrderDetails.ToArray()) {
                                    partsSalesOrder.PartsSalesOrderDetails.Remove(detail);
                                }
                            }
                            if(partsSalesOrder.PartsSalesOrderTypeName != "油品订单") {
                                DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_RebuildPartsSalesPrice, () => {
                                    //订单类型变更，重新计算订货价格
                                    this.DomainContext.Load(this.DomainContext.根据销售类型查询配件销售价3Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, partsSalesOrder.IfDirectProvision, partsSalesOrder.SalesUnitOwnerCompanyId, partsSalesOrder.WarehouseId ?? 0, null, null, null, null), LoadBehavior.RefreshCurrent, loadOp1 => {
                                        if(loadOp1.HasError)
                                            return;
                                        foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                            var virtualPartsSalesPrice = loadOp1.Entities.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                                            if(virtualPartsSalesPrice != null) {
                                                if(virtualPartsSalesPrice.PartsTreatyPrice != null)
                                                    detail.OrderPrice = virtualPartsSalesPrice.PartsTreatyPrice.Value;
                                                else
                                                    detail.OrderPrice = virtualPartsSalesPrice.Price;
                                                detail.MeasureUnit = virtualPartsSalesPrice.MeasureUnit;
                                                detail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;
                                                detail.OriginalPrice = virtualPartsSalesPrice.RetailGuidePrice;
                                                detail.DiscountedPrice = detail.OriginalPrice - detail.OrderPrice;
                                                detail.OrderSum = detail.OrderedQuantity * detail.OrderPrice;
                                                detail.PriceTypeName = virtualPartsSalesPrice.PriceTypeName;
                                                detail.ABCStrategy = virtualPartsSalesPrice.ABCStrategy;
                                            }
                                        }
                                        partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                                        this.loadCompanyAddress();
                                    }, null);
                                    this.oldPartsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
                                    if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单") {
                                        var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(o => o.SparePartId).Distinct().ToArray();
                                        this.DomainContext.Load(this.DomainContext.GetSparePartsByIdsQuery(sparePartIds).Where(o => o.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                                            if(loadOp1.Entities == null || !loadOp1.Entities.Any()) {
                                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoSparepart);
                                                return;
                                            }
                                            if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
                                                var spareParts = loadOp1.Entities.ToArray();
                                                foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                                    var sparePart = spareParts.FirstOrDefault(o => o.Id == detail.SparePartId);
                                                    if(sparePart != null) {
                                                        detail.MInSaleingAmount = sparePart.MInPackingAmount.Value;
                                                    }
                                                }
                                            }
                                        }, null);
                                    } else {
                                        var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(o => o.SparePartId).Distinct().ToArray();
                                        this.DomainContext.Load(this.DomainContext.获取当前企业的配件营销包装属性Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.BranchId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp2 => {
                                            if(loadOp2.Entities == null || !loadOp2.Entities.Any()) {
                                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoPartsBranchPackingPop);
                                                return;
                                            }
                                            if(loadOp2.Entities != null && loadOp2.Entities.Any()) {
                                                var partsBranchPackingProps = loadOp2.Entities.ToArray();

                                                foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                                    var partsBranchPackingProp = partsBranchPackingProps.FirstOrDefault(o => o.SparePartId == detail.SparePartId);
                                                    if(partsBranchPackingProp != null) {
                                                        detail.MInSaleingAmount = partsBranchPackingProp.PackingCoefficient.Value;
                                                    }
                                                }
                                            }
                                        }, null);
                                    }
                                }, () => {
                                    partsSalesOrder.PartsSalesOrderTypeId = this.oldPartsSalesOrderTypeId;
                                });
                            }
                        }, null);
                    }
                    break;
                case "IfDirectProvision":
                    //是否直供改变。 提示是否清空清单
                    if(partsSalesOrder.PartsSalesOrderDetails.Any() && isIfDirectProvision && partsSalesOrder.PartsSalesOrderTypeName != "油品订单")
                        DcsUtils.Confirm(PartsSalesUIStrings.DataEdit_tips_PartsSalesOrder_IfDirectProvision, () => {
                            //清空清单
                            foreach(var partsSalesOrderDetails in partsSalesOrder.PartsSalesOrderDetails) {
                                partsSalesOrderDetails.ValidationErrors.Clear();
                            }
                            var detailList = partsSalesOrder.PartsSalesOrderDetails.ToList();
                            foreach(var partsSalesOrderDetails in detailList) {
                                partsSalesOrder.PartsSalesOrderDetails.Remove(partsSalesOrderDetails);
                            }
                        }, () => {
                            //不改变
                            isIfDirectProvision = false;
                            partsSalesOrder.IfDirectProvision = !partsSalesOrder.IfDirectProvision;
                        });
                    if(partsSalesOrder.ValidationErrors.Any(entity => entity.MemberNames.Contains("RequestedDeliveryTime")))
                        partsSalesOrder.ValidationErrors.Remove(partsSalesOrder.ValidationErrors.FirstOrDefault(entity => entity.MemberNames.Contains("RequestedDeliveryTime")));
                    else if(partsSalesOrder.IfDirectProvision && !partsSalesOrder.RequestedDeliveryTime.HasValue)
                        partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_IfDirectProvisionThenRequestedDeliveryTimeIsNotNull, new[] {
                            "RequestedDeliveryTime"
                        }));
                    isIfDirectProvision = true;
                    break;
                case "CompanyAddressId":
                    if(partsSalesOrder.CompanyAddressId != null && partsSalesOrder.CustomerType != (int)DcsCompanyType.代理库) {
                        this.DomainContext.Load(this.DomainContext.GetCompanyAddressesQuery().Where(r => r.Id == partsSalesOrder.CompanyAddressId), LoadBehavior.RefreshCurrent, loadOption => {
                            if(loadOption.HasError) {
                                loadOption.MarkErrorAsHandled();
                                return;
                            }
                            var entity = loadOption.Entities.FirstOrDefault();
                            if(entity == null) return;
                            KvProvinceNames.Clear();
                            foreach(var item in TiledRegions) {
                                if(entity.ProvinceName == item.ProvinceName && item.CityName == null && item.CountyName == null) {
                                    KvProvinceNames.Add(new KeyValuePair {
                                        Key = item.Id,
                                        Value = item.ProvinceName
                                    });
                                    this.provinceNameRadCombox.SelectedValue = item.Id;
                                    this.provinceNameRadCombox.Text = item.ProvinceName;
                                }
                            }

                        }, null);
                    }
                    break;
                case "BranchCode":
                    if(this.ExistsMuiltiChangedPartsSalesOrders())
                        DcsUtils.ShowDomainServiceOperationWindow(new Exception(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ExistsMuiltiChangedPartsSalesOrders));
                    break;
            }
        }

        private bool ExistsMuiltiChangedPartsSalesOrders() {
            return this.DomainContext.PartsSalesOrders.Count(r => r.EntityState == EntityState.New || r.EntityState == EntityState.Modified) > 1;
        }

        //加载企业地址
        private void loadCompanyAddress() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(this.company.Type == (int)DcsCompanyType.代理库 && (partsSalesOrder.PartsSalesOrderTypeName == "紧急订单" || partsSalesOrder.PartsSalesOrderTypeName == "特急订单" || partsSalesOrder.PartsSalesOrderTypeName == "事故订单")) {
                this.InitCompanyAddress(true); //查询登陆企业和下属企业的收货地址。 
            } else {
                this.InitCompanyAddress(false);//只查询登陆企业的收货地址。
            }
        }

        /// <summary>
        /// 根据条件查询本企业和下属企业地址
        /// </summary>
        /// <param name="isBranch">是为包含下属企业</param>
        private void InitCompanyAddress(bool isBranch) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(isBranch) {
                //查询企业收获地址
                this.DomainContext.Load(this.DomainContext.GetCompanyAddressWithAgencyDealerRelationsQuery(partsSalesOrder.SalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError) {
                        loadOption.MarkErrorAsHandled();
                        return;
                    }
                    this.KvCompanyAddresses.Clear();
                    foreach(var entity in loadOption.Entities) {
                        this.KvCompanyAddresses.Add(entity);
                    }
                    //收货地址和联系人，默认选中中心库本身的那条
                    if(loadOption.Entities != null && loadOption.Entities.Count() >= 1) {
                        if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress)) {
                            var agencyAddress = loadOption.Entities.FirstOrDefault(r => r.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId);
                            if(agencyAddress != null) {
                                partsSalesOrder.ReceivingAddress = agencyAddress.DetailAddress;
                                partsSalesOrder.ContactPerson = agencyAddress.ContactPerson;
                                partsSalesOrder.ContactPhone = agencyAddress.ContactPhone;
                                partsSalesOrder.CompanyAddressId = agencyAddress.Id;
                            }
                        }
                    }
                }, null);
            } else {
                //查询企业收货地址
                this.DomainContext.Load(this.DomainContext.GetCompanyAddressesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError) {
                        loadOption.MarkErrorAsHandled();
                        return;
                    }
                    this.KvCompanyAddresses.Clear();
                    foreach(var entity in loadOption.Entities) {
                        this.KvCompanyAddresses.Add(entity);
                    }
                    if(loadOption.Entities != null && loadOption.Entities.Count() >= 1) {
                        if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress)) {
                            partsSalesOrder.ReceivingAddress = loadOption.Entities.First().DetailAddress;
                            partsSalesOrder.ContactPerson = loadOption.Entities.First().ContactPerson;
                            partsSalesOrder.ContactPhone = loadOption.Entities.First().ContactPhone;
                            partsSalesOrder.CompanyAddressId = loadOption.Entities.First().Id;

                        }
                    }
                }, null);
            }
            if(partsSalesOrder.PartsSalesOrderTypeName != "正常订单") {
                if(this.cbAddress != null)
                    this.cbAddress.IsEditable = true;
            } else {
                if(this.cbAddress != null)
                    this.cbAddress.IsEditable = false;
            }
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();
            //查询营销分公司
            this.DomainContext.Load(this.DomainContext.根据企业查询营销分公司Query(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranches.Clear();
                foreach(var branch in loadOp.Entities) {
                    this.KvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                        UserObject = branch
                    });
                }
                var partsSalesOrder = this.DataContext as PartsSalesOrder;
                if(partsSalesOrder != null && this.KvBranches.Any() && this.EditState == DataEditState.New) {
                    partsSalesOrder.BranchId = this.KvBranches.First().Key;
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError) {
                    loadOption.MarkErrorAsHandled();
                    return;
                }
                if(loadOption.Entities != null && loadOption.Entities.Any()) {
                    company = loadOption.Entities.First();
                    if(company.Type != (int)DcsCompanyType.代理库 && company.Type != (int)DcsCompanyType.服务站兼代理库) {
                        this.blockReceivingWarehouse.Visibility = Visibility.Collapsed;
                        this.comboBoxReceivingWarehouse.Visibility = Visibility.Collapsed;
                    }
                }
            }, null);
            this.InitCompanyAddress(false);//加载企业收货地址
            //this.RegisterButton(new ButtonItem {
            //    Command = new DelegateCommand(this.CheckPartsStock),
            //    Icon = Utils.MakeServerUri("Client/Dcs/Images/Operations/CheckStock.png"),
            //    Title = "校验库存"
            //}, true);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = this.ImportFileCommand
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesOrder), "PartsSalesOrderDetails"), null, () => this.PartsSalesOrderDetailForEditDataGridView);
            this.Detail.Children.Add(detailDataEditView);
            this.provinceNameRadCombox.SelectionChanged += provinceNameRadCombox_SelectionChanged;
        }
        void provinceNameRadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            this.KvCityNames.Clear();
            foreach(var item in TiledRegions.Where(ex => ex.ProvinceName == provinceNameRadCombox.Text && !string.IsNullOrEmpty(ex.CityName) && string.IsNullOrEmpty(ex.CountyName)).OrderBy(ex => ex.CityName)) {
                var values = KvCityNames.Select(ex => ex.Value);
                if(values.Contains(item.CityName))
                    continue;
                KvCityNames.Add(new KeyValuePair {
                    Key = item.Id,
                    Value = item.CityName
                });
            }
        }
        private RadWindow radSparePartDirectoryWindow;
        private RadWindow RadSparePartDirectoryWindow {
            get {
                return this.radSparePartDirectoryWindow ?? (this.radSparePartDirectoryWindow = new RadWindow {
                    Content = this.SparePartDirectoryQueryWindow,
                    Header = "选择电子配件目录",
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true,
                    Width = 400
                });
            }
        }

        private SparePartDirectoryQueryWindow sparePartDirectoryQueryWindow;
        private SparePartDirectoryQueryWindow SparePartDirectoryQueryWindow {
            get {
                if(this.sparePartDirectoryQueryWindow == null) {
                    this.sparePartDirectoryQueryWindow = new SparePartDirectoryQueryWindow();
                    this.sparePartDirectoryQueryWindow.Loaded += this.SparePartDirectoryQueryWindow_Loaded;
                }
                return this.sparePartDirectoryQueryWindow;
            }
        }

        private void SparePartDirectoryQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindows = sender as SparePartDirectoryQueryWindow;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(queryWindows == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            queryWindows.PMSBrandEdit.Text = partsSalesOrder.SalesCategoryName ?? "";
            compositeFilterItem.Filters.Add(new FilterItem("PMSBrandId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.SalesCategoryId));
            queryWindows.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void proxyStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesQuery().Where(s => s.Id == partsSalesOrder.WarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                partsSalesOrder.WarehouseName = entity.Name;
                var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
                if(queryWindow == null || partsSalesOrder == null)
                    return;
                var compositeFilterItem = new CompositeFilterItem();
                queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                     "Common", "WarehouseName", partsSalesOrder.WarehouseName
                });
                if(partsSalesOrder.SalesUnit != null) {
                    compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.SalesUnit.PartsSalesCategoryId));
                }
                compositeFilterItem.Filters.Add(new FilterItem("PartsSalesOrderTypeId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.PartsSalesOrderTypeId));
                compositeFilterItem.Filters.Add(new FilterItem("CustomerId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.SubmitCompanyId));
                compositeFilterItem.Filters.Add(new FilterItem("WarehouseId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.WarehouseId));
                queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
            }, null);
        }

        private RadWindow radPreOrderWindow;
        private RadWindow RadPreOrderWindow {
            get {
                return this.radPreOrderWindow ?? (this.radPreOrderWindow = new RadWindow());
            }
        }

        private void PreOrderSelect() {
            this.RadPreOrderWindow.Content = this.PreOrderQueryWindow;
            this.RadPreOrderWindow.Header = PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_PreOrder;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(partsSalesOrder.SalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesCategoryIdIsNull);
                return;
            }
            if(!partsSalesOrder.WarehouseId.HasValue || partsSalesOrder.WarehouseId.Value == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_WarehouseIsNull2);
                return;
            }
            if(partsSalesOrder.PartsSalesOrderTypeId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIdIsNull2);
                return;
            }
            this.RadPreOrderWindow.ShowDialog();
        }

        private PreOrderForReportQueryWindow preOrderQueryWindow;
        private PreOrderForReportQueryWindow PreOrderQueryWindow {
            get {
                if(preOrderQueryWindow == null) {
                    preOrderQueryWindow = new PreOrderForReportQueryWindow();
                    preOrderQueryWindow.Loaded += this.PreOrderQueryWindow_Loaded;
                    preOrderQueryWindow.SelectionDecided += preOrderQueryWindow_SelectionDecided;
                }
                return this.preOrderQueryWindow;
            }
        }


        private void PreOrderQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var qw = sender as PreOrderForReportQueryWindow;
            if(qw == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            qw.BrandCode.Text = partsSalesOrder.PartsSalesCategory.Code;
        }

        private void preOrderQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindows = sender as PreOrderForReportQueryWindow;
            if(queryWindows == null || queryWindows.SelectedEntities == null)
                return;
            var preOrder = queryWindows.SelectedEntities.Cast<PreOrder>();
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(preOrder == null || partsSalesOrder == null)
                return;
            foreach(var order in preOrder)
                this.PreOrders.Add(order);
            var spCodes = preOrder.Select(r => r.PartCode).ToArray();
            this.DomainContext.Load(this.DomainContext.GetSparePartsByCodeQuery(spCodes), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null) {
                    foreach(var entity in loadOp.Entities) {
                        if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.SparePartId == entity.Id)) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SameParts);
                            return;
                        } else {
                            var pre = preOrder.First(r => r.PartCode == entity.Code);
                            var partsSalesOrderDetail = new PartsSalesOrderDetail {
                                SparePartCode = entity.Code,
                                SparePartId = entity.Id,
                                SparePartName = entity.Name,
                                IfCanNewPart = true,
                                OrderedQuantity = preOrder.Where(r => r.PartCode == pre.Code).Sum(r => r.Qty) == null ? 1 : (int)preOrder.Where(r => r.PartCode == entity.Code).Sum(r => r.Qty),
                            };
                            //获取订货价格
                            this.DomainContext.Load(this.DomainContext.根据销售类型查询配件销售价3Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, partsSalesOrder.IfDirectProvision, partsSalesOrder.SalesUnitOwnerCompanyId, partsSalesOrder.WarehouseId ?? 0, null, null, null, null), LoadBehavior.RefreshCurrent, loadOp3 => {
                                if(loadOp3.HasError) {
                                    if(!loadOp3.IsErrorHandled)
                                        loadOp3.MarkErrorAsHandled();
                                    return;
                                }
                                var partsOrderPrice = loadOp3.Entities.FirstOrDefault(r => r.SparePartId == entity.Id);
                                if(partsOrderPrice != null) {
                                    if(partsOrderPrice.PartsTreatyPrice != null)
                                        partsSalesOrderDetail.OrderPrice = partsOrderPrice.PartsTreatyPrice.Value;
                                    else
                                        partsSalesOrderDetail.OrderPrice = partsOrderPrice.Price;
                                    partsSalesOrderDetail.MeasureUnit = partsOrderPrice.MeasureUnit;
                                    partsSalesOrderDetail.CustOrderPriceGradeCoefficient = partsOrderPrice.Coefficient;
                                    partsSalesOrderDetail.OriginalPrice = partsOrderPrice.RetailGuidePrice;
                                    partsSalesOrderDetail.DiscountedPrice = partsSalesOrderDetail.OriginalPrice - partsSalesOrderDetail.OrderPrice;
                                    partsSalesOrderDetail.OrderSum = partsSalesOrderDetail.OrderedQuantity * partsSalesOrderDetail.OrderPrice;
                                    partsSalesOrderDetail.PriceTypeName = partsOrderPrice.PriceTypeName;
                                    partsSalesOrderDetail.ABCStrategy = partsOrderPrice.ABCStrategy;
                                    partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                                }
                            }, null);
                            partsSalesOrder.PartsSalesOrderDetails.Add(partsSalesOrderDetail);
                        }
                    }
                }
            }, null);
        }

        private ICommand exportFileCommand;

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private ICommand importFileCommand;

        public ICommand ImportFileCommand {
            get {
                return this.importFileCommand ?? (this.importFileCommand = new DelegateCommand(this.ShowFileDialog));
            }
        }

        private void ShowFileDialog() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(partsSalesOrder.BranchId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_BranchIdIsNull);
                return;
            }
            if(partsSalesOrder.SalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesCategoryIdIsNull);
                return;
            }
            if(!partsSalesOrder.WarehouseId.HasValue || partsSalesOrder.WarehouseId.Value == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_WarehouseIsNull2);
                return;
            }
            if(partsSalesOrder.PartsSalesOrderTypeId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIdIsNull2);
                return;
            }
            this.Uploader.ShowFileDialog();
        }

        private const string EXPORT_DATA_FILE_NAME = "导出配件销售订单清单模板.xlsx";

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },
                    new ImportTemplateColumn {
                        Name=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderQuntity,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = Name=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_MeetingTime,
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark,
                    },
                    new ImportTemplateColumn {
                        Name = Name=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderCode,
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void SecondClassStationPlanBtn() {
            this.RadWindow.Content = this.QueryWindow;
            this.RadWindow.Header = "二级站计划需求选择";
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(partsSalesOrder.BranchId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_BranchIdIsNull);
                return;
            }
            if(partsSalesOrder.SalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesCategoryIdIsNull);
                return;
            }
            if(!partsSalesOrder.WarehouseId.HasValue || partsSalesOrder.WarehouseId.Value == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_WarehouseIsNull2);
                return;
            }
            if(partsSalesOrder.PartsSalesOrderTypeId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIdIsNull2);
                return;
            }
            this.RadWindow.ShowDialog();
        }

        private void CheckPartsSalesOrder(PartsSalesOrder partsSalesOrder) {
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.ValidationErrors.Clear();
            if(partsSalesOrder.BranchId == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_BranchIdIsNull, new[] {
                    "BranchId"
                }));
            if(!partsSalesOrder.WarehouseId.HasValue || partsSalesOrder.WarehouseId.Value == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_WarehouseIdIdIsNull, new[] {
                    "WarehouseId"
                }));
            if(partsSalesOrder.SalesUnitId == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesCategoryIdIsNull, new[] {
                    "SalesCategoryId"
                }));
            if(partsSalesOrder.SalesUnitId == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesUnitIdIsNull, new[] {
                    "SalesUnitId"
                }));
            if(partsSalesOrder.PartsSalesOrderTypeId == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIdIsNull, new[] {
                    "PartsSalesOrderTypeId"
                }));
            if(partsSalesOrder.ReceivingCompanyId == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingCompanyIdIsNull, new[] {
                    "ReceivingCompanyId"
                }));
            if(partsSalesOrder.ShippingMethod == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ShippingMethodIsNull, new[] {
                    "ShippingMethod"
                }));
            if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingAddressIsNull, new[] {
                    "ReceivingAddress"
                }));
            foreach(var item in partsSalesOrder.PartsSalesOrderDetails) {
                item.ValidationErrors.Clear();
                if(item.OrderedQuantity <= default(int))
                    item.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderDetail_OrderedQuantityIsLessThanZeroError, new[] {
                        "OrderedQuantity"
                    }));
                if(item.OrderPrice == 0)
                    item.ValidationErrors.Add(new ValidationResult(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_InCurrentPartsSalesOrderTypeNameOrderSumIsZeroError, partsSalesOrder.PartsSalesOrderTypeName, item.SparePartCode), new[] {
                        "OrderPrice"
                    }));
                if(!item.HasValidationErrors)
                    ((IEditableObject)item).EndEdit();
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderWithPartsSalesOrderDetailsAndCustomerAccountQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                if(entity.PartsSalesOrderTypeName == "正常订单") {
                    this.ShippingMethod.IsEnabled = false;
                }
                this.DomainContext.Load(this.DomainContext.GetCompanyAddressesQuery().Where(r => r.Id == entity.CompanyAddressId), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError) {
                        loadOption.MarkErrorAsHandled();
                        return;
                    }
                    var entity1 = loadOption.Entities.FirstOrDefault();
                    if(entity1 == null) return;
                    foreach(var item in TiledRegions) {
                        if(entity1.ProvinceName == item.ProvinceName && item.CityName == null && item.CountyName == null) {
                            this.provinceNameRadCombox.SelectedValue = item.Id;
                            this.provinceNameRadCombox.Text = item.ProvinceName;
                        }
                    }
                }, null);
                entity.Status = (int)DcsPartsSalesOrderStatus.新增;
                this.DomainContext.Load(this.DomainContext.查询销售组织仓库Query(BaseApp.Current.CurrentUserData.EnterpriseId, entity.BranchId, null, entity.SalesCategoryId, null), LoadBehavior.RefreshCurrent, loadop => {
                    if(loadop.HasError) {
                        loadop.MarkErrorAsHandled();
                        return;
                    }
                    var warhouseId = entity.WarehouseId.Value;
                    this.KvSalesUnitAffiWarehouses.Clear();
                    if(loadop.Entities != null)
                        foreach(var salesUnitAffiWarehouse in loadop.Entities) {
                            this.KvSalesUnitAffiWarehouses.Add(salesUnitAffiWarehouse);
                        }
                    isCanCheck = false;
                    entity.WarehouseId = warhouseId;
                    isCanCheck = true;
                }, null);
                this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == entity.SalesUnitOwnerCompanyId), LoadBehavior.RefreshCurrent, loadOpCom => {
                    if(loadOpCom.HasError) {
                        loadOpCom.MarkErrorAsHandled();
                        return;
                    }
                    if(loadOpCom.Entities != null && loadOpCom.Entities.Any()) {
                        entity.SalesUnitOwnerCompanyType = loadOpCom.Entities.First().Type;
                    }
                }, null);
                // 查询订货仓库  -
                if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                    this.blockReceivingWarehouse.Visibility = Visibility.Visible;
                    this.comboBoxReceivingWarehouse.Visibility = Visibility.Visible;
                    this.DomainContext.Load(this.DomainContext.GetWarehousesBySalesUnitQuery(entity.SalesCategoryId), LoadBehavior.RefreshCurrent, loadWarehouse => {
                        if(loadWarehouse.HasError) {
                            loadWarehouse.MarkErrorAsHandled();
                            return;
                        }
                        this.kvReceivingWarehouse.Clear();
                        if(loadWarehouse.Entities != null && loadWarehouse.Entities.Any()) {
                            foreach(var warehouse in loadWarehouse.Entities) {
                                this.kvReceivingWarehouse.Add(new KeyValuePair {
                                    Key = warehouse.Id,
                                    Value = warehouse.Name,
                                    UserObject = warehouse
                                });
                                this.comboBoxReceivingWarehouse.SelectedItem = loadWarehouse.Entities.FirstOrDefault();
                            }
                        }
                    }, null);
                }
                var parameteras = new Dictionary<string, object>();
                parameteras.Add("submitCompanyId", entity.SubmitCompanyId);
                parameteras.Add("salesUnitId", entity.SalesUnitId);
                parameteras.Add("partsSalesCategoryId", entity.SalesCategoryId);
                this.DomainContext.InvokeOperation("查询未审核订单金额", typeof(decimal), parameteras, true, invokeOp => {
                    if(invokeOp.HasError)
                        return;
                    this.notApprovePrice.Text = "￥" + invokeOp.Value.ToString();
                    this.lnotApprovePrice = decimal.Parse(invokeOp.Value.ToString());

                    this.DomainContext.Load(this.DomainContext.查询客户可用资金Query(null, entity.SalesUnit.AccountGroupId, entity.InvoiceReceiveCompanyId), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        var customerAccount = loadOp2.Entities.FirstOrDefault();

                        if(customerAccount == null) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_CustomerAccountIsNull);
                            entity.CustomerAccount = null;
                            return;
                        }
                        entity.CustomerAccountId = customerAccount.Id;
                        entity.CustomerAccount = customerAccount;
                        this.DomainContext.Load(this.DomainContext.GetPartsRebateAccountsQuery().Where(r => r.AccountGroupId == entity.SalesUnit.AccountGroupId && r.CustomerCompanyId == entity.SubmitCompanyId && r.BranchId == entity.SalesUnit.OwnerCompanyId), LoadBehavior.RefreshCurrent, loadRebateAccount => {
                            if(loadRebateAccount.HasError) {
                                loadRebateAccount.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadRebateAccount);
                                return;
                            }
                            if(loadRebateAccount.Entities.Any()) {
                                this.txtRebateAmount.Text = loadRebateAccount.Entities.First().AccountBalanceAmount.ToString("c2");
                                this.lRebateAmount = loadRebateAccount.Entities.First().AccountBalanceAmount;
                                this.txtUseablePosition.Text = (entity.CustomerAccount.UseablePosition).ToString("c2");
                            } else {
                                this.txtRebateAmount.Text = "￥" + "0.00";
                                this.txtUseablePosition.Text = entity.CustomerAccount.UseablePosition.ToString("c2");
                            }
                            this.txtCredenceAmount.Text = (entity.CustomerAccount.CredenceAmount + (entity.CustomerAccount.SIHCredit ?? 0)).ToString("c2");
                            this.orderUseablePosition.Text = (entity.CustomerAccount.AccountBalance + lRebateAmount + entity.CustomerAccount.CustomerCredenceAmount + (entity.CustomerAccount.TempCreditTotalFee ?? 0) + (entity.CustomerAccount.SIHCredit ?? 0) - entity.CustomerAccount.LockBalance - lnotApprovePrice).ToString("c2");
                            this.lorderUseablePosition = entity.CustomerAccount.AccountBalance + lRebateAmount + entity.CustomerAccount.CustomerCredenceAmount + (entity.CustomerAccount.TempCreditTotalFee ?? 0) + (entity.CustomerAccount.SIHCredit ?? 0) - entity.CustomerAccount.LockBalance - lnotApprovePrice;

                        }, null);

                        if(this.company.Type == (int)DcsCompanyType.代理库 && (entity.PartsSalesOrderTypeName == "紧急订单" || entity.PartsSalesOrderTypeName == "特急订单" || entity.PartsSalesOrderTypeName == "事故订单")) {
                            this.DomainContext.Load(this.DomainContext.GetCompanyAddressWithAgencyDealerRelationsQuery(entity.SalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption1 => {
                                if(loadOption1.HasError) {
                                    loadOption1.MarkErrorAsHandled();
                                    return;
                                }
                                this.KvCompanyAddresses.Clear();
                                foreach(var address in loadOption1.Entities) {
                                    this.KvCompanyAddresses.Add(address);
                                }
                                this.DomainContext.Load(this.DomainContext.GetTiledRegionsQuery().Where(ex => ex.ProvinceName == entity.Province && !string.IsNullOrEmpty(ex.CityName) && string.IsNullOrEmpty(ex.CountyName)), LoadBehavior.RefreshCurrent, loadOp1 => {
                                    if(loadOp1.HasError)
                                        return;
                                    var entitycityids = loadOp1.Entities;
                                    kvCityNames.Clear();
                                    foreach(var entitycityid in entitycityids) {
                                        kvCityNames.Add(new KeyValuePair {
                                            Key = entitycityid.Id,
                                            Value = entitycityid.CityName
                                        });
                                        if(entitycityid.CityName.Equals(entity.City)) {
                                            entity.CityId = entitycityid.Id;
                                            cityNameRadCombox.SelectedValue = entitycityid.Id;
                                        }
                                    }
                                    this.SetObjectToEdit(entity);
                                }, null);
                            }, null);
                        } else {
                            this.DomainContext.Load(this.DomainContext.GetCompanyAddressesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadAdd => {
                                if(loadAdd.HasError) {
                                    loadAdd.MarkErrorAsHandled();
                                    return;
                                }
                                this.KvCompanyAddresses.Clear();
                                foreach(var address in loadAdd.Entities) {
                                    this.KvCompanyAddresses.Add(address);
                                }
                                this.DomainContext.Load(this.DomainContext.GetTiledRegionsQuery().Where(ex => ex.ProvinceName == entity.Province && !string.IsNullOrEmpty(ex.CityName) && string.IsNullOrEmpty(ex.CountyName)), LoadBehavior.RefreshCurrent, loadOp1 => {
                                    if(loadOp1.HasError)
                                        return;
                                    var entitycityids = loadOp1.Entities;
                                    kvCityNames.Clear();
                                    foreach(var entitycityid in entitycityids) {
                                        kvCityNames.Add(new KeyValuePair {
                                            Key = entitycityid.Id,
                                            Value = entitycityid.CityName
                                        });
                                        if(entitycityid.CityName.Equals(entity.City)) {
                                            entity.CityId = entitycityid.Id;
                                            cityNameRadCombox.SelectedValue = entitycityid.Id;
                                        }
                                    }
                                    this.SetObjectToEdit(entity);
                                }, null);
                            }, null);
                        }
                    }, null);
                }, null);
            }, null);
        }

        private DataGridViewBase PartsSalesOrderDetailForEditDataGridView {
            get {
                if(this.partsSalesOrderDetailForEditDataGridView == null) {
                    this.partsSalesOrderDetailForEditDataGridView = DI.GetDataGridView("PartsSalesOrderDetailForEdit");
                    this.partsSalesOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesOrderDetailForEditDataGridView;
            }
        }

        protected override void Reset() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(this.DomainContext.PartsSalesOrders.Contains(partsSalesOrder))
                this.DomainContext.PartsSalesOrders.Detach(partsSalesOrder);
            this.DataContext = null;
            this.KvPartsSalesOrderTypes.Clear();
            this.KvPartsSalesCategories.Clear();
            this.KvSalesUnitAffiWarehouses.Clear();
            this.notApprovePrice.Text = "￥0.00";
            this.orderUseablePosition.Text = "￥0.00";
            this.txtRebateAmount.Text = "￥0.00";
            this.txtUseablePosition.Text = "￥0.00";
            this.isCanCheck = true;
            this.dlHasWarehouse = true;
            this.lnotApprovePrice = 0;
            this.oldPartsSalesOrderTypeId = 0;
            this.lorderUseablePosition = 0;
            this.PreOrders.Clear();
        }

        protected override void OnEditSubmitting() {
            this.DetachUnexpectedRepairContracts();
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null || !this.PartsSalesOrderDetailForEditDataGridView.CommitEdit())
                return;
            partsSalesOrder.ValidationErrors.Clear();

            foreach(var item in partsSalesOrder.PartsSalesOrderDetails) {
                item.ValidationErrors.Clear();
            }
            if(partsSalesOrder.CustomerAccount == null) {
                var validationMessage = PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_AccountBalanceIsNull;
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PendingAmountIsNull);
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ShippedProductValueIsNull);
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_CustomerCredenceAmountIsNull);
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_UseablePositionIsNull);
                UIHelper.ShowAlertMessage(validationMessage);
                return;
            }
            if(partsSalesOrder.SalesUnitOwnerCompanyId == partsSalesOrder.SubmitCompanyId) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SalesUnitOwner);
                return;
            }
            if(partsSalesOrder.WarehouseId == default(int) || partsSalesOrder.WarehouseId == null) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_WarehouseIdIdIsNull);
                return;
            }
            if(!partsSalesOrder.RequestedDeliveryTime.HasValue) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcess_RequestedDeliveryTimeIsNull);
                return;
            }
            if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库)
                if(!partsSalesOrder.ReceivingWarehouseId.HasValue) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingWarehouseNameIsNull);
                    return;
                }
            if(partsSalesOrder.RequestedDeliveryTime.Value < new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_RequestedDeliveryTime);
                return;
            }
            if(this.comboBoxReceivingWarehouse.Visibility == Visibility.Visible && partsSalesOrder.ReceivingWarehouseId == 0) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingWarehouseNameIsNull);
                return;
            }
            if(!partsSalesOrder.PartsSalesOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderDetailsIsEmpty);
                return;
            }
            foreach(var items in partsSalesOrder.PartsSalesOrderDetails.GroupBy(ex => ex.SparePartCode).Where(obj => obj.Count() > 1)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SamePartsNew, items.FirstOrDefault().SparePartCode));
                return;
            }
            if(partsSalesOrder.TotalAmount>1000000) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_PartssalesOrder_FeeOver);
                return;
            }
            if(string.IsNullOrWhiteSpace(this.provinceNameRadCombox.Text)) {
                partsSalesOrder.Province = company.ProvinceName;
                partsSalesOrder.City = company.CityName;
            } else {
                partsSalesOrder.Province = this.provinceNameRadCombox.Text;
                partsSalesOrder.City = this.cityNameRadCombox.Text;
            }
            this.CheckPartsSalesOrder(partsSalesOrder);

            if(partsSalesOrder.HasValidationErrors || partsSalesOrder.PartsSalesOrderDetails.Any(e => e.HasValidationErrors))
                return;
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
            ((IEditableObject)partsSalesOrder).EndEdit();
            //校验销售订单清单数是否大于分公司策略的最大开票行数
            this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsSalesOrder.BranchId), LoadBehavior.RefreshCurrent, loadOpS => {
                if(loadOpS.HasError) {
                    if(loadOpS.IsErrorHandled)
                        loadOpS.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpS);
                    return;
                }
                var branchstrategy = loadOpS.Entities.SingleOrDefault();
                if(branchstrategy != null && partsSalesOrder.PartsSalesOrderDetails.Count > branchstrategy.MaxInvoiceRow) {
                    UIHelper.ShowAlertMessage(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_MaxInvoiceRow, branchstrategy.MaxInvoiceRow));
                    return;
                }
                if(partsSalesOrder.EntityState == EntityState.New) {
                    if(!this.dlHasWarehouse) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ReceivingWarehouse);
                        return;
                    }
                    partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.新增;
                    this.CheckNumberMultiple(partsSalesOrder);
                } else {
                    this.CheckNumberMultiple(partsSalesOrder);
                }
            }, null);
        }

        private void CheckNumberMultiple(PartsSalesOrder partsSalesOrder) {
            var sparepartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
            this.DomainContext.Load(DomainContext.GetSparePartsByIdsQuery(sparepartIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                }
                this.DomainContext.Load(this.DomainContext.GetAgencyAffiBranchesQuery().Where(r => r.BranchId == partsSalesOrder.BranchId), LoadBehavior.RefreshCurrent, aloadOp => {
                    if(aloadOp.HasError) {
                        if(!aloadOp.IsErrorHandled)
                            aloadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(aloadOp);
                        return;
                    }
                    var entity = aloadOp.Entities.Select(e => e.BranchId).ToArray();
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        var spareParts = loadOp.Entities.ToArray();
                        if(partsSalesOrder.PartsSalesOrderTypeName == "油品订单") {
                            var sparePart = spareParts.FirstOrDefault(r => r.GoldenTaxClassifyCode != "1");
                            if(sparePart != null) {
                                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_GoldenTaxClassifyCode, sparePart.Code));
                                return;
                            }
                        }
                        var isNotCheckMInPackingAmount = (partsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.代理库 || partsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.服务站兼代理库)
                            && entity.Any();
                        if(spareParts.Any(r => r.MInPackingAmount == null || r.MInPackingAmount == 0) && !isNotCheckMInPackingAmount) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_MInPackingAmount);
                            return;
                        }
                        if(!isNotCheckMInPackingAmount && partsSalesOrder.PartsSalesOrderTypeName == "正常订单" || partsSalesOrder.PartsSalesOrderTypeName == "油品订单") {
                            foreach(var partsSalesOrderDetail in partsSalesOrder.PartsSalesOrderDetails) {
                                var partsValidation = spareParts.FirstOrDefault(r => r.Id == partsSalesOrderDetail.SparePartId && (partsSalesOrderDetail.OrderedQuantity % r.MInPackingAmount.Value) != 0);
                                if(spareParts.Any(r => r.Id == partsSalesOrderDetail.SparePartId && (partsSalesOrderDetail.OrderedQuantity % r.MInPackingAmount.Value) != 0) && partsValidation != null) {
                                    UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_OrderedQuantity, partsValidation.Code, partsValidation.MInPackingAmount));
                                    partsSalesOrderDetail.MInSaleingAmount = partsValidation.MInPackingAmount;
                                    partsSalesOrderDetail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SaleingAmount, new[] { "OrderedQuantity" }));
                                    return;
                                }
                            }
                        }
                        if(partsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.分公司) {
                            this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsSalesOrder.SalesUnitOwnerCompanyId), LoadBehavior.RefreshCurrent, loadOpS => {
                                if(loadOpS.HasError) {
                                    if(loadOpS.IsErrorHandled)
                                        loadOpS.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpS);
                                    return;
                                }
                                var branchstrategy = loadOpS.Entities.SingleOrDefault();
                                if(branchstrategy != null && branchstrategy.PurDistributionStrategy.HasValue && branchstrategy.PurDistributionStrategy == (int)DcsPurDistStrategy.纳入统购分销) {
                                    var parameteras = new Dictionary<string, object>();
                                    parameteras.Add("ownerCompanyId", partsSalesOrder.SalesUnitOwnerCompanyId);
                                    parameteras.Add("list", partsSalesOrder.PartsSalesOrderDetails.ToList());
                                    this.CheckStock(parameteras);
                                } else {
                                    this.CheckAllowCustomerCredit(partsSalesOrder);
                                }
                            }, null);
                        } else {
                            this.CheckAllowCustomerCredit(partsSalesOrder);
                        }
                    }
                }, null);
            }, null);
        }

        private void CheckAllowCustomerCredit(PartsSalesOrder partsSalesOrder) {
            var parameteras = new Dictionary<string, object>();
            parameteras.Add("ownerCompanyId", partsSalesOrder.SalesUnitOwnerCompanyId);
            parameteras.Add("list", partsSalesOrder.PartsSalesOrderDetails.ToList());
            if(partsSalesOrder.EntityState == EntityState.New) {
                this.DomainContext.Load(this.DomainContext.查询客户可用资金含未审批订单Query(partsSalesOrder.CustomerAccountId, partsSalesOrder.CustomerAccount.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId, null, partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                    }
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        var customerAccount = loadOp.Entities.First();
                        if(this.lorderUseablePosition < partsSalesOrder.TotalAmount) {
                            if(customerAccount.AllowCustomerCredit.HasValue) {
                                if(customerAccount.AllowCustomerCredit.Value) {
                                    DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_TotalAmountMoreUseablePosition, () => this.CheckStock(parameteras));
                                } else
                                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_TotalAmount);
                            } else
                                DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_TotalAmountMoreUseablePosition, () => this.CheckStock(parameteras));
                        } else
                            this.CheckStock(parameteras);
                    } else
                        this.CheckStock(parameteras);
                }, null);
            } else {
                this.DomainContext.Load(this.DomainContext.查询客户可用资金含未审批订单Query(partsSalesOrder.CustomerAccountId, partsSalesOrder.CustomerAccount.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId, partsSalesOrder.Id, partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                    }
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        var customerAccount = loadOp.Entities.First();
                        if(lorderUseablePosition < partsSalesOrder.TotalAmount) {
                            if(customerAccount.AllowCustomerCredit.HasValue) {
                                if(customerAccount.AllowCustomerCredit.Value) {
                                    DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_TotalAmountMoreUseablePosition, () => this.CheckStock(parameteras));
                                } else
                                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_TotalAmountMoreUseablePosition);
                            } else
                                DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_TotalAmountMoreUseablePosition, () => this.CheckStock(parameteras));
                        } else
                            this.CheckStock(parameteras);
                    } else
                        this.CheckStock(parameteras);
                }, null);
            }
        }

        private void CheckStock(Dictionary<string, object> parameteras) {
            //todo:FTDCS_ISS20150608040取消校验
            //this.DomainContext.InvokeOperation("校验销售订单库存满足", typeof(bool), parameteras, true, invokeOp => {
            //    if(invokeOp.HasError)
            //        return;
            //    if(!bool.Parse(invokeOp.Value.ToString()))
            //        DcsUtils.Confirm("配件库存不足,是否继续保存", this.Submitting);
            //    else
            //        Submitting();
            //}, null);
            Submitting();
        }

        private void Submitting() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            //获取预订单Id集合
            var preOrderIds = this.PreOrders.Select(r => r.Id).ToArray();
            if(partsSalesOrder.Can销售订单提报)
                partsSalesOrder.销售订单提报(preOrderIds);
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesOrderReport;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<CompanyAddress> KvCompanyAddresses {
            get {
                return this.kvCompanyAddresses ?? (this.kvCompanyAddresses = new ObservableCollection<CompanyAddress>());
            }
        }

        public ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public ObservableCollection<KeyValuePair> KvReceivingWarehouse {
            get {
                return this.kvReceivingWarehouse;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes {
            get {
                return this.kvPartsSalesOrderTypes;
            }
        }

        public object KvSecondLevelOrderTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }



        //private void DcsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
        //    var comboBox = sender as RadComboBox;
        //    if(comboBox == null)
        //        return;
        //    var partsSalesOrder = this.DataContext as PartsSalesOrder;
        //    if(partsSalesOrder == null)
        //        return;
        //    if(partsSalesOrder.PartsSalesOrderTypeId == 0)
        //        return;
        //    //不是正常订单，省 不用填充
        //    if(partsSalesOrder.PartsSalesOrderTypeName != "正常订单") {
        //        this.TextBoxContactPerson.IsReadOnly = false;
        //        this.TextBoxContactPhone.IsReadOnly = false;
        //        this.KvProvinceNames.Clear();
        //        foreach(var item in this.TiledRegions) {
        //            if(item.CityName == null && item.CountyName == null) {
        //                KvProvinceNames.Add(new KeyValuePair {
        //                    Key = item.Id,
        //                    Value = item.ProvinceName
        //                });
        //            }
        //        }
        //    } else {
        //        this.TextBoxContactPerson.IsReadOnly = true;
        //        this.TextBoxContactPhone.IsReadOnly = true;
        //        this.KvProvinceNames.Clear();
        //        foreach(var item in this.TiledRegions) {
        //            if(item.Id == partsSalesOrder.ProvinceID) {
        //                KvProvinceNames.Add(new KeyValuePair {
        //                    Key = item.Id,
        //                    Value = item.ProvinceName
        //                });
        //            }
        //        }
        //    }
        //    this.DomainContext.Load(this.DomainContext.GetCustomerCompanyAndPartsSalesOrderTypeQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.PartsSalesOrderTypeId == partsSalesOrder.PartsSalesOrderTypeId && r.CustomerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
        //        if(loadOp.Entities == null || !loadOp.Entities.Any()) {
        //            partsSalesOrder.PartsSalesOrderTypeId = 0;
        //            UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_PartsSalesOrderTypeNameisNull);
        //            return;
        //        }
        //        if(e.RemovedItems != null && e.RemovedItems.Count == 1 && e.AddedItems != null && e.AddedItems.Count == 1) {
        //            if(partsSalesOrder.PartsSalesOrderDetails != null && partsSalesOrder.PartsSalesOrderDetails.Any()) {
        //                DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_ReAggregateTheAmount, () => {
        //                    if(partsSalesOrder.PartsSalesOrderTypeId == 0 || !partsSalesOrder.PartsSalesOrderDetails.Any())
        //                        return;
        //                    partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
        //                    if(partsSalesOrder.PartsSalesOrderTypeName == "油品订单") {
        //                        foreach(var detail in partsSalesOrder.PartsSalesOrderDetails.ToArray()) {
        //                            partsSalesOrder.PartsSalesOrderDetails.Remove(detail);
        //                        }
        //                    } else {
        //                        this.DomainContext.Load(this.DomainContext.根据销售类型查询配件销售价Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, partsSalesOrder.IfDirectProvision), LoadBehavior.RefreshCurrent, loadOption => {
        //                            if(loadOption.HasError)
        //                                return;
        //                            foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
        //                                var virtualPartsSalesPrice = loadOption.Entities.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
        //                                if(virtualPartsSalesPrice != null) {
        //                                    if(virtualPartsSalesPrice.PartsTreatyPrice != null)
        //                                        detail.OrderPrice = virtualPartsSalesPrice.PartsTreatyPrice.Value;
        //                                    else
        //                                        detail.OrderPrice = virtualPartsSalesPrice.Price;
        //                                    detail.MeasureUnit = virtualPartsSalesPrice.MeasureUnit;
        //                                    detail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;
        //                                    detail.OriginalPrice = virtualPartsSalesPrice.RetailGuidePrice;
        //                                    detail.DiscountedPrice = detail.OriginalPrice - detail.OrderPrice;
        //                                    detail.OrderSum = detail.OrderedQuantity * detail.OrderPrice;
        //                                    detail.PriceTypeName = virtualPartsSalesPrice.PriceTypeName;
        //                                    detail.ABCStrategy = virtualPartsSalesPrice.ABCStrategy;
        //                                }
        //                            }
        //                            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
        //                            if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单")
        //                                partsSalesOrder.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
        //                            this.loadCompanyAddress();
        //                        }, null);
        //                    }
        //                    if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单") {
        //                        var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(o => o.SparePartId).Distinct().ToArray();
        //                        this.DomainContext.Load(this.DomainContext.GetSparePartsByIdsQuery(sparePartIds).Where(o => o.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
        //                            if(loadOp1.Entities == null || !loadOp1.Entities.Any()) {
        //                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoSparepart);
        //                                return;
        //                            }
        //                            if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
        //                                var spareParts = loadOp1.Entities.ToArray();
        //                                foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
        //                                    var sparePart = spareParts.FirstOrDefault(o => o.Id == detail.SparePartId);
        //                                    if(sparePart != null) {
        //                                        detail.MInSaleingAmount = sparePart.MInPackingAmount.Value;
        //                                    }
        //                                }
        //                            }
        //                        }, null);
        //                    } else {
        //                        var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(o => o.SparePartId).Distinct().ToArray();
        //                        this.DomainContext.Load(this.DomainContext.获取当前企业的配件营销包装属性Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.BranchId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp2 => {
        //                            if(loadOp2.Entities == null || !loadOp2.Entities.Any()) {
        //                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_NoPartsBranchPackingPop);
        //                                return;
        //                            }
        //                            if(loadOp2.Entities != null && loadOp2.Entities.Any()) {
        //                                var partsBranchPackingProps = loadOp2.Entities.ToArray();

        //                                foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
        //                                    var partsBranchPackingProp = partsBranchPackingProps.FirstOrDefault(o => o.SparePartId == detail.SparePartId);
        //                                    if(partsBranchPackingProp != null) {
        //                                        detail.MInSaleingAmount = partsBranchPackingProp.PackingCoefficient.Value;
        //                                    }
        //                                }
        //                            }
        //                        }, null);
        //                    }
        //                }, () => {
        //                    comboBox.SelectionChanged -= this.DcsComboBox_SelectionChanged;
        //                    comboBox.SelectedItem = e.RemovedItems[0];
        //                    comboBox.SelectionChanged += this.DcsComboBox_SelectionChanged;
        //                });
        //            }
        //        }
        //        if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单") {
        //            partsSalesOrder.ShippingMethod = (int)DcsPartsShippingMethod.公路物流;
        //            this.ShippingMethod.IsEnabled = false;
        //        } else {
        //            this.ShippingMethod.IsEnabled = true;
        //        }
        //        this.loadCompanyAddress();
        //    }, null);
        //}

        private void DcsComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var companyAddress = comboBox.SelectedItem as CompanyAddress;
            if(companyAddress == null) {
                partsSalesOrder.ContactPerson = string.Empty;
                partsSalesOrder.ContactPhone = string.Empty;
                return;
            }
            partsSalesOrder.CompanyAddressId = companyAddress.Id;
            partsSalesOrder.ContactPerson = companyAddress.ContactPerson;
            partsSalesOrder.ContactPhone = companyAddress.ContactPhone;
            if(partsSalesOrder.PartsSalesOrderTypeName == "正常订单") {
                this.TextBoxContactPerson.IsReadOnly = true;
                this.TextBoxContactPhone.IsReadOnly = true;
            } else {
                this.TextBoxContactPerson.IsReadOnly = false;
                this.TextBoxContactPhone.IsReadOnly = false;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        //提报单位为代理库或服务站兼代理库时必须填写收货仓库，并且限制选择收货仓库后按Backspace键不清空收货仓库
        private int comboxIndex;
        private void ComboBoxReceivingWarehouse_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var keyValuePair = comboBox.SelectedItem as KeyValuePair;
            if(keyValuePair == null)
                return;
            var warehouse = keyValuePair.UserObject as Warehouse;
            if(warehouse != null)
                partsSalesOrder.ReceivingWarehouseName = warehouse.Name;
            comboxIndex = comboBox.SelectedIndex;
        }

        private void ComboBoxReceivingWarehouse_OnKeyUp(object sender, KeyEventArgs e) {
            if(e.PlatformKeyCode == 8) {
                comboBoxReceivingWarehouse.SelectedIndex = comboxIndex;
            }
        }

        //校验库存
        private string warehouseCode, warehouseName;
        private void CheckPartsStock() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(!partsSalesOrder.WarehouseId.HasValue || partsSalesOrder.WarehouseId.Value == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_WarehouseIsNull2);
                return;
            }
            if(!partsSalesOrder.PartsSalesOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderDetailsIsEmpty);
                return;
            }
            this.CompanyPartsStocks.Clear();
            var partIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
            this.DomainContext.Load(this.DomainContext.GetWarehousesQuery().Where(r => r.Id == partsSalesOrder.WarehouseId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOpWarehouse => {
                if(loadOpWarehouse.HasError) {
                    loadOpWarehouse.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpWarehouse);
                    return;
                }
                //给没有库存数据的清单订货仓库赋值
                var warehouse = loadOpWarehouse.Entities.First();
                warehouseCode = warehouse.Code;
                warehouseName = warehouse.Name;
                this.DomainContext.Load(this.DomainContext.销售订单查询仓库库存Query(partsSalesOrder.SalesUnitOwnerCompanyId, (int)partsSalesOrder.WarehouseId, partIds), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    if(loadOp.Entities.Any()) {
                        foreach(var item in loadOp.Entities.Distinct()) {
                            var partsSalesOrderDetail = partsSalesOrder.PartsSalesOrderDetails.FirstOrDefault(r => r.SparePartId == item.SparePartId);
                            if(partsSalesOrderDetail != null) {
                                item.OrderQuantity = partsSalesOrderDetail.OrderedQuantity;
                                item.UsableStock = item.UsableQuantity < item.OrderQuantity ? item.UsableQuantity : item.OrderQuantity;
                                item.UnUsableStock = item.OrderQuantity > item.UsableQuantity ? item.OrderQuantity - item.UsableQuantity : 0;
                            }
                            CompanyPartsStocks.Add(item);
                        }
                    }
                    var idsCount = loadOp.Entities.Distinct().Count();
                    if(idsCount != partsSalesOrder.PartsSalesOrderDetails.Count()) {
                        var loadOpPartIds = loadOp.Entities.Distinct().Select(r => r.SparePartId);
                        foreach(var detail in partsSalesOrder.PartsSalesOrderDetails.Where(r => !loadOpPartIds.Contains(r.SparePartId))) {
                            var companyPartsStock = new CompanyPartsStock {
                                WarehouseCode = warehouseCode,
                                WarehouseName = warehouseName,
                                SparePartCode = detail.SparePartCode,
                                SparePartName = detail.SparePartName,
                                OrderQuantity = detail.OrderedQuantity,
                                UsableStock = 0,
                                UnUsableStock = detail.OrderedQuantity
                            };
                            CompanyPartsStocks.Add(companyPartsStock);
                        }
                    }
                    this.CheckPartsStockWindow.Show();
                }, null);
            }, null);
        }

        private RadWindow checkPartsStockWindow;
        private RadWindow CheckPartsStockWindow {
            get {
                return this.checkPartsStockWindow ?? (this.checkPartsStockWindow = new RadWindow {
                    Content = this.CompanyPartsStockForCheckDataGridView,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true,
                    Header = PartsSalesUIStrings.Action_Title_CheckStock
                });
            }
        }

        private DataGridViewBase companyPartsStockForCheckDataGridView;
        private DataGridViewBase CompanyPartsStockForCheckDataGridView {
            get {
                if(this.companyPartsStockForCheckDataGridView == null) {
                    this.companyPartsStockForCheckDataGridView = DI.GetDataGridView("CompanyPartsStockForCheck");
                    this.companyPartsStockForCheckDataGridView.DomainContext = this.DomainContext;
                    this.companyPartsStockForCheckDataGridView.DataContext = this;
                }
                return this.companyPartsStockForCheckDataGridView;
            }
        }

        private ObservableCollection<CompanyPartsStock> companyPartsStocks = new ObservableCollection<CompanyPartsStock>();
        public ObservableCollection<CompanyPartsStock> CompanyPartsStocks {
            get {
                return companyPartsStocks ?? (this.companyPartsStocks = new ObservableCollection<CompanyPartsStock>());
            }
        }
        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.kvShippingMethods ?? (this.kvShippingMethods = new ObservableCollection<KeyValuePair>());

            }
        }

        private void DcsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(partsSalesOrder.PartsSalesOrderTypeName == "油品订单" && company.Type == (int)DcsCompanyType.代理库) {
                this.checkBoxIfDirectProvision.IsEnabled = false;
            } else {
                this.checkBoxIfDirectProvision.IsEnabled = true;
            }
        }

    }

}