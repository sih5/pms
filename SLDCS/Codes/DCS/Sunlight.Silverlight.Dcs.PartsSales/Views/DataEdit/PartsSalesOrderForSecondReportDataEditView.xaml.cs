﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderForSecondReportDataEditView : INotifyPropertyChanged {
        private int? CompanyType = null;
        public bool isCanCheck2 = true;
        private const string DEFALUT_BALANCE = "￥0.00";
        private readonly string[] kvNames = {
            "PartsShipping_Method"
        };
        private decimal lnotApprovePrice = 0;
        private decimal lorderUseablePosition = 0;
        private DataGridViewBase partsSalesOrderDetailForEditDataGridView;

        private DataGridViewBase PartsSalesOrderDetailForEditDataGridView {
            get {
                if(this.partsSalesOrderDetailForEditDataGridView == null) {
                    this.partsSalesOrderDetailForEditDataGridView = DI.GetDataGridView("PartsSalesOrderDetailForEdit");
                    this.partsSalesOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesOrderDetailForEditDataGridView;
            }
        }

        private KeyValueManager keyValueManager;

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();

        public ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes {
            get {
                return this.kvPartsSalesOrderTypes;
            }
        }

        private ObservableCollection<CompanyAddress> kvCompanyAddresses;

        public ObservableCollection<CompanyAddress> KvCompanyAddresses {
            get {
                return this.kvCompanyAddresses ?? (this.kvCompanyAddresses = new ObservableCollection<CompanyAddress>());
            }
        }

        private QueryWindowBase queryWindow;

        private QueryWindowBase QueryWIndow {
            get {
                if(this.queryWindow == null) {
                    this.queryWindow = DI.GetQueryWindow("SecondClassStationPlan");
                    this.queryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
                }
                return this.queryWindow;
            }
        }

        private void QueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var secondClassStationPlan = queryWindow.SelectedEntities.Cast<SecondClassStationPlan>().FirstOrDefault();
            var partsSalesOrder = queryWindow.DataContext as PartsSalesOrder;
            if(secondClassStationPlan == null || partsSalesOrder == null)
                return;

            this.DomainContext.Load(this.DomainContext.GetSecondClassStationPlanDetailsQuery().Where(r => r.SecondClassStationPlanId == secondClassStationPlan.Id && r.ProcessMode == (int)DcsProcessMode.转销售订单), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null) {
                    foreach(var entity in loadOp.Entities) {
                        if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.SparePartId == entity.SparePartId)) {
                            var detail = partsSalesOrder.PartsSalesOrderDetails.First();
                            detail.OrderedQuantity += 1;
                        } else {
                            var partsSalesOrderDetail = new PartsSalesOrderDetail {
                                SparePartCode = entity.SparePartCode,
                                SparePartId = entity.SparePartId,
                                SparePartName = entity.SparePartName,
                                OrderedQuantity = 1,
                            };
                            partsSalesOrder.PartsSalesOrderDetails.Add(partsSalesOrderDetail);
                            this.DomainContext.Load(this.DomainContext.GetPartsSalesPricesQuery().Where(r => r.Id == entity.SparePartId), LoadBehavior.RefreshCurrent, loadOp2 => {
                                if(loadOp2.HasError) {
                                    if(!loadOp2.IsErrorHandled)
                                        loadOp2.MarkErrorAsHandled();
                                    return;
                                }
                                var partsSalesPrice = loadOp2.Entities.FirstOrDefault();
                                if(partsSalesPrice != null)
                                    partsSalesOrderDetail.OriginalPrice = partsSalesPrice.SalesPrice;
                            }, null);
                        }
                    }
                }
            }, null);
        }

        private RadWindow radWindow;

        private RadWindow RadWindow {
            get {

                return this.radWindow ?? (this.radWindow = new RadWindow());
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private string strFileName;

        private RadUpload uploader;

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsSalesOrder = this.DataContext as PartsSalesOrder;
                        if(partsSalesOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPartsSalesOrderDetailAsync(e.HandlerData.CustomData["Path"].ToString(), partsSalesOrder.IfDirectProvision, partsSalesOrder.SubmitCompanyId, partsSalesOrder.SalesCategoryId);
                        this.excelServiceClient.ImportPartsSalesOrderDetailCompleted -= this.ExcelServiceClient_ImportPartsSalesOrderDetailCompleted;
                        this.excelServiceClient.ImportPartsSalesOrderDetailCompleted += this.ExcelServiceClient_ImportPartsSalesOrderDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void ExcelServiceClient_ImportPartsSalesOrderDetailCompleted(object sender, ImportPartsSalesOrderDetailCompletedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var detailList = partsSalesOrder.PartsSalesOrderDetails.ToList();
            foreach(var detail in detailList) {
                partsSalesOrder.PartsSalesOrderDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    partsSalesOrder.PartsSalesOrderDetails.Add(new PartsSalesOrderDetail {
                        SparePartId = (int)data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        EstimatedFulfillTime = data.EstimatedFulfillTime,
                        OrderedQuantity = (int)data.OrderedQuantity,
                        Remark = data.Remark,
                        IfCanNewPart = true
                    });
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        public object KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        private ObservableCollection<SalesUnitAffiWarehouse> kvSalesUnitAffiWarehouses;

        public ObservableCollection<SalesUnitAffiWarehouse> KvSalesUnitAffiWarehouses {
            get {
                return this.kvSalesUnitAffiWarehouses ?? (this.kvSalesUnitAffiWarehouses = new ObservableCollection<SalesUnitAffiWarehouse>());
            }
            set {
                this.kvSalesUnitAffiWarehouses = value;
                this.OnPropertyChanged("KvSalesUnitAffiWarehouses");
            }
        }

        private readonly ObservableCollection<KeyValuePair> kvReceivingWarehouse = new ObservableCollection<KeyValuePair>();

        public ObservableCollection<KeyValuePair> KvReceivingWarehouse {
            get {
                return this.kvReceivingWarehouse;
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesOrderReport;
            }
        }

        private void CreateUI() {
            //查询企业收获地址
            this.DomainContext.Load(this.DomainContext.GetCompanyAddressesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError) {
                    loadOption.MarkErrorAsHandled();
                    return;
                }

                this.KvCompanyAddresses.Clear();
                foreach(var entity in loadOption.Entities) {
                    this.KvCompanyAddresses.Add(entity);
                }
                if(loadOption.Entities != null && loadOption.Entities.Count() == 1) {
                    var partsSalesOrder = this.DataContext as PartsSalesOrder;
                    if(partsSalesOrder != null) {
                        if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress)) {
                            partsSalesOrder.ReceivingAddress = loadOption.Entities.First().DetailAddress;
                            partsSalesOrder.ContactPerson = loadOption.Entities.First().ContactPerson;
                            partsSalesOrder.ContactPhone = loadOption.Entities.First().ContactPhone;
                            partsSalesOrder.CompanyAddressId = loadOption.Entities.First().Id;
                        }
                    }
                }

            }, null);

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(() => this.Uploader.ShowFileDialog())
            });
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesOrder), "PartsSalesOrderDetails"), null, () => this.PartsSalesOrderDetailForEditDataGridView);
            this.Detail.Children.Add(detailDataEditView);
        }

        private void CheckPartsSalesOrder(PartsSalesOrder partsSalesOrder) {
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.ValidationErrors.Clear();
            if(partsSalesOrder.BranchId == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_BranchIdIsNull, new[] {
                    "BranchId"
                }));
            if(!partsSalesOrder.WarehouseId.HasValue || partsSalesOrder.WarehouseId.Value == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_WarehouseIdIdIsNull, new[] {
                    "WarehouseId"
                }));
            if(partsSalesOrder.SalesUnitId == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesCategoryIdIsNull, new[] {
                    "SalesCategoryId"
                }));
            if(partsSalesOrder.SalesUnitId == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesUnitIdIsNull, new[] {
                    "SalesUnitId"
                }));
            if(partsSalesOrder.PartsSalesOrderTypeId == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIdIsNull, new[] {
                    "PartsSalesOrderTypeId"
                }));
            if(partsSalesOrder.ReceivingCompanyId == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingCompanyIdIsNull, new[] {
                    "ReceivingCompanyId"
                }));
            if(partsSalesOrder.ShippingMethod == 0)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ShippingMethodIsNull, new[] {
                    "ShippingMethod"
                }));
            if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingAddressIsNull, new[] {
                    "ReceivingAddress"
                }));
            foreach(var item in partsSalesOrder.PartsSalesOrderDetails) {
                item.ValidationErrors.Clear();
                if(item.OrderedQuantity <= default(int))
                    item.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderDetail_OrderedQuantityIsLessThanZeroError, new[] {
                        "OrderedQuantity"
                    }));
                if(item.OrderPrice == 0)
                    item.ValidationErrors.Add(new ValidationResult(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_InCurrentPartsSalesOrderTypeNameOrderSumIsZeroError, partsSalesOrder.PartsSalesOrderTypeName, item.SparePartCode), new[] {
                        "OrderPrice"
                    }));
                if(!item.HasValidationErrors)
                    ((IEditableObject)partsSalesOrder).EndEdit();
            }
        }

        public PartsSalesOrderForSecondReportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.DataContextChanged -= PartsSalesOrderForSecondReportDataEditView_DataContextChanged;
            this.DataContextChanged += PartsSalesOrderForSecondReportDataEditView_DataContextChanged;
            this.Loaded += PartsSalesOrderForSecondReportDataEditView_Loaded;
        }

        void PartsSalesOrderForSecondReportDataEditView_Loaded(object sender, RoutedEventArgs e) {
            if(this.KvCompanyAddresses.Count == 1) {
                var partsSalesOrder = this.DataContext as PartsSalesOrder;
                if(partsSalesOrder != null) {
                    if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress)) {
                        partsSalesOrder.ReceivingAddress = this.KvCompanyAddresses.First().DetailAddress;
                        partsSalesOrder.ContactPerson = this.KvCompanyAddresses.First().ContactPerson;
                        partsSalesOrder.ContactPhone = this.KvCompanyAddresses.First().ContactPhone;
                        partsSalesOrder.CompanyAddressId = kvCompanyAddresses.First().Id;
                    }
                }
            }
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.KvPartsSalesOrderTypes.Clear();
            this.KvSalesUnitAffiWarehouses.Clear();
            this.KvReceivingWarehouse.Clear();
            this.notApprovePrice.Text = DEFALUT_BALANCE;
            this.lnotApprovePrice = 0;
            this.lorderUseablePosition = 0;
            this.AccountBalanceTextBox.Text = DEFALUT_BALANCE;
            this.BalanceTextBox.Text = DEFALUT_BALANCE;
            this.CustomerCredenceAmountTextBox.Text = DEFALUT_BALANCE;
            this.LockBalanceTextBox.Text = DEFALUT_BALANCE;
            this.UseablePositionTextBox.Text = DEFALUT_BALANCE;
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.KvPartsSalesOrderTypes.Clear();
            this.KvSalesUnitAffiWarehouses.Clear();
            this.KvReceivingWarehouse.Clear();
            this.notApprovePrice.Text = DEFALUT_BALANCE;
            this.lnotApprovePrice = 0;
            this.lorderUseablePosition = 0;
            this.AccountBalanceTextBox.Text = DEFALUT_BALANCE;
            this.BalanceTextBox.Text = DEFALUT_BALANCE;
            this.CustomerCredenceAmountTextBox.Text = DEFALUT_BALANCE;
            this.LockBalanceTextBox.Text = DEFALUT_BALANCE;
            this.UseablePositionTextBox.Text = DEFALUT_BALANCE;
        }

        protected override void OnEditSubmitting() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null || !this.PartsSalesOrderDetailForEditDataGridView.CommitEdit())
                return;
            partsSalesOrder.ValidationErrors.Clear();

            foreach(var item in partsSalesOrder.PartsSalesOrderDetails) {
                item.ValidationErrors.Clear();
            }

            this.CheckPartsSalesOrder(partsSalesOrder);
            if(!partsSalesOrder.ReceivingWarehouseId.HasValue) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingWarehouseNameIsNull);
                return;
            }
            if(!partsSalesOrder.InvoiceReceiveSaleCateId.HasValue)
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_InvoiceReceiveSaleCateIdIsNull, new[] {
                    "InvoiceReceiveSaleCateId"
                }));
            if(partsSalesOrder.CustomerAccount == null) {
                var validationMessage = PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_AccountBalanceIsNull;
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PendingAmountIsNull);
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ShippedProductValueIsNull);
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_CustomerCredenceAmountIsNull);
                validationMessage = string.Concat(validationMessage, Environment.NewLine, PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_UseablePositionIsNull);
                UIHelper.ShowAlertMessage(validationMessage);
                return;
            }

            if(!partsSalesOrder.RequestedDeliveryTime.HasValue) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcess_RequestedDeliveryTimeIsNull);
                return;
            }

            if(partsSalesOrder.RequestedDeliveryTime.Value < new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_RequestedDeliveryTime);
                return;
            }

            if(!partsSalesOrder.PartsSalesOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderDetailsIsEmpty);
                return;
            }

            if(partsSalesOrder.HasValidationErrors || partsSalesOrder.PartsSalesOrderDetails.Any(e => e.HasValidationErrors))
                return;
            if(partsSalesOrder.ReceivingWarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ReceivingWarehouseId);
                return;
            }
            var parameteras = new Dictionary<string, object>();
            parameteras.Add("ownerCompanyId", partsSalesOrder.SalesUnitOwnerCompanyId);
            parameteras.Add("list", partsSalesOrder.PartsSalesOrderDetails.ToList());
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);

            ((IEditableObject)partsSalesOrder).EndEdit();
            if(partsSalesOrder.EntityState == EntityState.New) {
                partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.新增;
                this.CheckAllowCustomerCredit(partsSalesOrder, parameteras);
            } else {
                this.CheckAllowCustomerCredit(partsSalesOrder, parameteras);
            }
        }

        private void CheckAllowCustomerCredit(PartsSalesOrder partsSalesOrder, Dictionary<string, object> parameteras) {
            if(partsSalesOrder.EntityState == EntityState.New) {
                this.DomainContext.Load(this.DomainContext.查询客户可用资金含未审批订单Query(partsSalesOrder.CustomerAccountId, partsSalesOrder.CustomerAccount.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId, null, partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                    }
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        var customerAccount = loadOp.Entities.First();
                        if(this.lorderUseablePosition < partsSalesOrder.TotalAmount) {
                            if(customerAccount.AllowCustomerCredit.HasValue) {
                                if(customerAccount.AllowCustomerCredit.Value) {
                                    DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_TotalAmountMoreUseablePosition, () => this.CheckStock(parameteras));
                                } else
                                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_TotalAmount);
                            } else
                                DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_TotalAmountMoreUseablePosition, () => this.CheckStock(parameteras));
                        } else
                            this.CheckStock(parameteras);
                    } else
                        this.CheckStock(parameteras);
                }, null);
            } else {
                this.DomainContext.Load(this.DomainContext.查询客户可用资金含未审批订单Query(partsSalesOrder.CustomerAccountId, partsSalesOrder.CustomerAccount.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId, partsSalesOrder.Id, partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                    }
                    if(loadOp.Entities != null && loadOp.Entities.Any()) {
                        var customerAccount = loadOp.Entities.First();
                        if(lorderUseablePosition < partsSalesOrder.TotalAmount) {
                            if(customerAccount.AllowCustomerCredit.HasValue) {
                                if(customerAccount.AllowCustomerCredit.Value) {
                                    DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_TotalAmountMoreUseablePosition, () => this.CheckStock(parameteras));
                                } else
                                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_TotalAmountMoreUseablePosition);
                            } else
                                DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_TotalAmountMoreUseablePosition, () => this.CheckStock(parameteras));
                        } else
                            this.CheckStock(parameteras);
                    } else
                        this.CheckStock(parameteras);
                }, null);
            }
        }


        private void CheckStock(Dictionary<string, object> parameteras) {
            this.DomainContext.InvokeOperation("校验销售订单库存满足", typeof(bool), parameteras, true, invokeOp => {
                if(invokeOp.HasError)
                    return;
                if(!bool.Parse(invokeOp.Value.ToString()))
                    DcsUtils.Confirm("配件库存不足,是否继续保存", () => base.OnEditSubmitting());
                else
                    base.OnEditSubmitting();
            }, null);
        }

        private void PartsSalesOrderDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsSalesOrderDetail> e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
        }

        private void PartsSalesOrderForSecondReportDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.PropertyChanged -= this.PartsSalesOrder_PropertyChanged;
            partsSalesOrder.PropertyChanged += this.PartsSalesOrder_PropertyChanged;
            partsSalesOrder.PartsSalesOrderDetails.EntityRemoved -= this.PartsSalesOrderDetails_EntityRemoved;
            partsSalesOrder.PartsSalesOrderDetails.EntityRemoved += this.PartsSalesOrderDetails_EntityRemoved;
            if(this.DomainContext == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPersonSubDealersQuery().Where(r => r.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                if(loadOp.Entities == null || !loadOp.Entities.Any())
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.DomainContext.Load(this.DomainContext.GetSubDealersQuery().Where(r => r.Id == entity.SubDealerId), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp2.HasError)
                            if(!loadOp2.IsErrorHandled) {
                                loadOp2.MarkErrorAsHandled();
                                return;
                            }
                        if(loadOp2.Entities == null || !loadOp2.Entities.Any())
                            return;
                        var subDealer = loadOp2.Entities.FirstOrDefault();
                        if(subDealer != null) {
                            partsSalesOrder.FirstClassStationCode = subDealer.Code;
                            partsSalesOrder.FirstClassStationId = subDealer.Id;
                            partsSalesOrder.FirstClassStationName = subDealer.Name;
                        }
                    }, null);
                }
            }, null);

            this.KvPartsSalesOrderTypes.Clear();
        }

        private void PartsSalesOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(this.DomainContext == null) {
                this.DomainContext = new DcsDomainContext();
            }
            switch(e.PropertyName) {
                case "SalesCategoryId":
                    if(!isCanCheck2) {
                        isCanCheck2 = true;
                        return;
                    }
                    partsSalesOrder.SalesUnitId = 0;
                    partsSalesOrder.SalesUnitCode = string.Empty;
                    partsSalesOrder.SalesUnitName = string.Empty;
                    this.DomainContext.Load(this.DomainContext.查询销售组织仓库Query(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesOrder.BranchId, null, partsSalesOrder.SalesCategoryId, null), LoadBehavior.RefreshCurrent, loadop => {
                        if(loadop.HasError) {
                            loadop.MarkErrorAsHandled();
                            return;
                        }
                        isCanCheck2 = false;
                        this.KvSalesUnitAffiWarehouses.Clear();
                        if(loadop.Entities != null)
                            foreach(var salesUnitAffiWarehouse in loadop.Entities) {
                                if(partsSalesOrder.CustomerType == (int)DcsCompanyType.代理库 || partsSalesOrder.CustomerType == (int)DcsCompanyType.服务站兼代理库) {
                                    if(salesUnitAffiWarehouse.Warehouse.StorageCompanyType == (int)DcsCompanyType.分公司 && salesUnitAffiWarehouse.Warehouse.Type != (int)DcsWarehouseType.虚拟库 && salesUnitAffiWarehouse.Warehouse.StorageCompanyId == partsSalesOrder.BranchId && salesUnitAffiWarehouse.Warehouse.StorageCompanyId != BaseApp.Current.CurrentUserData.EnterpriseId)
                                        this.KvSalesUnitAffiWarehouses.Add(salesUnitAffiWarehouse);
                                } else
                                    if(salesUnitAffiWarehouse.Warehouse.Type != (int)DcsWarehouseType.虚拟库 && salesUnitAffiWarehouse.Warehouse.StorageCompanyId == partsSalesOrder.BranchId && salesUnitAffiWarehouse.Warehouse.StorageCompanyId != BaseApp.Current.CurrentUserData.EnterpriseId)
                                        this.KvSalesUnitAffiWarehouses.Add(salesUnitAffiWarehouse);
                            }
                        isCanCheck2 = true;
                    }, null);

                    this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypesQuery().Where(r => r.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        isCanCheck2 = false;
                        var partsSalesOrderTypeId = partsSalesOrder.PartsSalesOrderTypeId;
                        this.KvPartsSalesOrderTypes.Clear();
                        foreach(var partsSalesOrderType in loadOp.Entities) {
                            this.KvPartsSalesOrderTypes.Add(new KeyValuePair {
                                Key = partsSalesOrderType.Id,
                                Value = partsSalesOrderType.Name,
                            });
                        }
                        isCanCheck2 = false;
                        partsSalesOrder.PartsSalesOrderTypeId = partsSalesOrderTypeId;
                        isCanCheck2 = true;
                        if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库) {
                            this.DomainContext.Load(this.DomainContext.GetWarehousesByPartsSalesOrderQuery(partsSalesOrder.SubmitCompanyId, partsSalesOrder.BranchId, partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp3 => {
                                if(loadOp3.HasError) {
                                    loadOp3.MarkErrorAsHandled();
                                    return;
                                }
                                if(loadOp3.Entities != null && loadOp3.Entities.Count() == 1) {
                                    partsSalesOrder.ReceivingWarehouseId = loadOp3.Entities.First().Id;
                                    partsSalesOrder.ReceivingWarehouseName = loadOp3.Entities.First().Name;
                                } else {
                                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_ReceivingWarehouseId);
                                    partsSalesOrder.ReceivingWarehouseId = default(int);
                                    partsSalesOrder.ReceivingWarehouseName = string.Empty;
                                }
                            }, null);
                        }
                    }, null);
                    this.DomainContext.Load(this.DomainContext.GetWarehousesBySalesUnitQuery(partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadWarehouse => {
                        if(loadWarehouse.HasError) {
                            loadWarehouse.MarkErrorAsHandled();
                            return;
                        }
                        this.kvReceivingWarehouse.Clear();
                        if(loadWarehouse.Entities != null && loadWarehouse.Entities.Any()) {
                            foreach(var warehouse in loadWarehouse.Entities) {
                                this.kvReceivingWarehouse.Add(new KeyValuePair {
                                    Key = warehouse.Id,
                                    Value = warehouse.Name,
                                    UserObject = warehouse
                                });
                            }
                        }
                    }, null);
                    break;
                case "WarehouseId":
                    if(!isCanCheck2) {
                        isCanCheck2 = true;
                        return;
                    }

                    var salesUnitKv = this.KvSalesUnitAffiWarehouses.FirstOrDefault(kv => kv.WarehouseId == partsSalesOrder.WarehouseId);
                    if(salesUnitKv == null)
                        return;
                    var salesUnitInsert = salesUnitKv;
                    if(salesUnitInsert.SalesUnit == null)
                        return;
                    partsSalesOrder.SalesUnitId = salesUnitInsert.SalesUnitId;
                    partsSalesOrder.SalesUnitCode = salesUnitInsert.SalesUnit.Code;
                    partsSalesOrder.SalesUnitName = salesUnitInsert.SalesUnit.Name;
                    partsSalesOrder.SalesUnitOwnerCompanyId = salesUnitInsert.SalesUnit.OwnerCompanyId;
                    var parameteras = new Dictionary<string, object>();
                    parameteras.Add("submitCompanyId", partsSalesOrder.SubmitCompanyId);
                    parameteras.Add("salesUnitId", partsSalesOrder.SalesUnitId);
                    parameteras.Add("partsSalesCategoryId", partsSalesOrder.SalesCategoryId);

                    this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == salesUnitInsert.SalesUnit.OwnerCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOp.Entities != null && loadOp.Entities.Any()) {
                            partsSalesOrder.SalesUnitOwnerCompanyId = loadOp.Entities.First().Id;
                            partsSalesOrder.SalesUnitOwnerCompanyCode = loadOp.Entities.First().Code;
                            partsSalesOrder.SalesUnitOwnerCompanyName = loadOp.Entities.First().Name;
                        }
                    }, null);

                    partsSalesOrder.SalesUnit = salesUnitInsert.SalesUnit;
                    if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库) {
                        this.DomainContext.Load(this.DomainContext.GetSalesUnitsQuery().Where(v => v.PartsSalesCategoryId == partsSalesOrder.SalesUnit.PartsSalesCategoryId && v.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }
                            if(loadOp.Entities.Any())
                                foreach(var salesUnit in loadOp.Entities)
                                    this.DomainContext.Load(DomainContext.GetSalesUnitAffiWarehousesQuery().Where(v => v.SalesUnitId == salesUnit.Id), LoadBehavior.RefreshCurrent, loadOp2 => {
                                        if(loadOp2.HasError) {
                                            loadOp2.MarkErrorAsHandled();
                                            return;
                                        }
                                        if(!loadOp2.Entities.Any())
                                            UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Notification_SalesUnitAffiWarehouse_WarehouseIsNull);
                                    }, null);
                            else
                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Notification_SalesUnitAffiWarehouse_WarehouseIsNull);
                        }, null);
                        this.DomainContext.Load(this.DomainContext.GetAgenciesQuery().Where(agency => agency.Id == partsSalesOrder.SalesUnitId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }

                            if(CompanyType == (int)DcsCompanyType.代理库 || CompanyType == null)
                                return;
                            var agency = loadOp.Entities.FirstOrDefault();
                            if(agency == null) {
                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_AgencyDealerRelationNotExists);
                                return;
                            }
                            this.DomainContext.Load(this.DomainContext.GetAgencyDealerRelationsQuery().Where(r => r.BranchId == partsSalesOrder.BranchId && r.DealerId == BaseApp.Current.CurrentUserData.EnterpriseId && r.AgencyId == partsSalesOrder.SalesUnitOwnerCompanyId), loadOpRelation => {
                                if(loadOpRelation.HasError) {
                                    loadOpRelation.MarkErrorAsHandled();
                                    return;
                                }
                                if(loadOpRelation.Entities.FirstOrDefault() == null)
                                    UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_AgencyDealerRelationNotExists);
                            }, null);
                        }, null);
                    }

                    this.DomainContext.InvokeOperation("查询未审核订单金额", typeof(decimal), parameteras, true, invokeOp => {
                        if(invokeOp.HasError)
                            return;
                        this.notApprovePrice.Text = "￥" + invokeOp.Value.ToString();
                        this.lnotApprovePrice = decimal.Parse(invokeOp.Value.ToString());
                        this.DomainContext.Load(this.DomainContext.查询客户可用资金Query(null, partsSalesOrder.SalesUnit.AccountGroupId, partsSalesOrder.InvoiceReceiveCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }
                            var customerAccount = loadOp.Entities.FirstOrDefault();

                            if(customerAccount == null) {
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrder_CustomerAccountIsNull);
                                partsSalesOrder.CustomerAccount = null;
                                this.AccountBalanceTextBox.Text = DEFALUT_BALANCE;
                                this.BalanceTextBox.Text = DEFALUT_BALANCE;
                                this.CustomerCredenceAmountTextBox.Text = DEFALUT_BALANCE;
                                this.LockBalanceTextBox.Text = DEFALUT_BALANCE;
                                this.UseablePositionTextBox.Text = DEFALUT_BALANCE;
                                return;
                            }
                            partsSalesOrder.CustomerAccountId = customerAccount.Id;
                            partsSalesOrder.CustomerAccount = customerAccount;
                            this.AccountBalanceTextBox.Text = customerAccount.AccountBalance.ToString("C");
                            this.BalanceTextBox.Text = customerAccount.Balance.ToString("C");
                            this.CustomerCredenceAmountTextBox.Text = (customerAccount.CustomerCredenceAmount + customerAccount.TempCreditTotalFee ?? 0).ToString("C");
                            this.LockBalanceTextBox.Text = customerAccount.LockBalance.ToString("C");
                            this.UseablePositionTextBox.Text = customerAccount.UseablePosition.ToString("C");

                            //this.AccountBalance.Text = "￥" + customerAccount.AccountBalance;
                            //this.PendingAmount.Text = "￥" + customerAccount.PendingAmount;
                            //this.CustomerCredenceAmount.Text = "￥" + customerAccount.CustomerCredenceAmount;
                            //this.UseablePosition.Text = "￥" + customerAccount.UseablePosition;
                            this.orderUseablePosition.Text = (partsSalesOrder.CustomerAccount.AccountBalance + partsSalesOrder.CustomerAccount.CustomerCredenceAmount - partsSalesOrder.CustomerAccount.LockBalance - lnotApprovePrice).ToString("C");
                            this.lorderUseablePosition = partsSalesOrder.CustomerAccount.AccountBalance + partsSalesOrder.CustomerAccount.CustomerCredenceAmount - partsSalesOrder.CustomerAccount.LockBalance - lnotApprovePrice;
                        }, null);
                    }, null);
                    break;
            }
        }

        private void DcsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(partsSalesOrder.PartsSalesOrderTypeId == 0)
                return;
            this.DomainContext.Load(this.DomainContext.GetCustomerCompanyAndPartsSalesOrderTypeQuery().Where(r => r.PartsSalesOrderTypeId == partsSalesOrder.PartsSalesOrderTypeId && r.CustomerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.Entities == null || !loadOp.Entities.Any()) {
                    partsSalesOrder.PartsSalesOrderTypeId = 0;
                    UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_PartsSalesOrderTypeNameisNull);
                    return;
                }
                if(e.RemovedItems != null && e.RemovedItems.Count == 1 && e.AddedItems != null && e.AddedItems.Count == 1)
                    if(partsSalesOrder.PartsSalesOrderDetails != null && partsSalesOrder.PartsSalesOrderDetails.Any())
                        DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesOrder_ReAggregateTheAmount, () => {
                            if(partsSalesOrder.PartsSalesOrderTypeId == 0 || !partsSalesOrder.PartsSalesOrderDetails.Any())
                                return;
                            var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                            this.DomainContext.Load(this.DomainContext.根据销售类型查询配件销售价Query(partsSalesOrder.SalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, partsSalesOrder.IfDirectProvision), LoadBehavior.RefreshCurrent, loadOption => {
                                if(loadOp.HasError)
                                    return;
                                foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                                    var virtualPartsSalesPrice = loadOption.Entities.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                                    if(virtualPartsSalesPrice != null) {
                                        if(virtualPartsSalesPrice.PartsTreatyPrice != null)
                                            detail.OrderPrice = virtualPartsSalesPrice.PartsTreatyPrice.Value;
                                        else
                                            detail.OrderPrice = virtualPartsSalesPrice.Price;
                                        detail.MeasureUnit = virtualPartsSalesPrice.MeasureUnit;
                                        detail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;
                                        detail.OriginalPrice = virtualPartsSalesPrice.RetailGuidePrice;
                                        detail.DiscountedPrice = detail.OriginalPrice - detail.OrderPrice;
                                        detail.OrderSum = detail.OrderedQuantity * detail.OrderPrice;
                                        detail.PriceTypeName = virtualPartsSalesPrice.PriceTypeName;
                                        detail.ABCStrategy = virtualPartsSalesPrice.ABCStrategy;
                                    }
                                }
                                partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                            }, null);

                        }, () => {
                            comboBox.SelectionChanged -= this.DcsComboBox_SelectionChanged;
                            comboBox.SelectedItem = e.RemovedItems[0];
                            comboBox.SelectionChanged += this.DcsComboBox_SelectionChanged;
                        });
            }, null);
        }

        private void DcsComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var companyAddress = comboBox.SelectedItem as CompanyAddress;
            if(companyAddress == null) {
                partsSalesOrder.ContactPerson = string.Empty;
                partsSalesOrder.ContactPhone = string.Empty;
                return;
            }
            partsSalesOrder.ContactPerson = companyAddress.ContactPerson;
            partsSalesOrder.ContactPhone = companyAddress.ContactPhone;
            partsSalesOrder.CompanyAddressId = companyAddress.Id;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        //提报单位为代理库或服务站兼代理库时必须填写收货仓库，并且限制选择收货仓库后按Backspace键不清空收货仓库
        private int comboxIndex;
        private void ComboBoxReceivingWarehouse_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var keyValuePair = comboBox.SelectedItem as KeyValuePair;
            if(keyValuePair == null)
                return;
            var warehouse = keyValuePair.UserObject as Warehouse;
            if(warehouse != null)
                partsSalesOrder.ReceivingWarehouseName = warehouse.Name;
            comboxIndex = comboBox.SelectedIndex;
        }
        private void ComboBoxReceivingWarehouse_OnKeyUp(object sender, KeyEventArgs e) {
            if(e.PlatformKeyCode == 8) {
                comboBoxReceivingWarehouse.SelectedIndex = comboxIndex;
            }
        }
    }
}