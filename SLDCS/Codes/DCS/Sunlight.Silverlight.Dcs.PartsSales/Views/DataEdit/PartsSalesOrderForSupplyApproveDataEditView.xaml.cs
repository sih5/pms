﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderForSupplyApproveDataEditView {
        private FrameworkElement partsSalesOrderForApproveDetailPanel;
        private DataGridViewBase partsSalesOrderProcessDetailForSupplyApproveDataGridView;
        private int branchId;
        private KeyValueManager keyValueManager;
        private PartsSalesOrder currentPartsSalesOrder;

        private readonly string[] kvNames = {
            "PartsShipping_Method"
        };

        public PartsSalesOrderForSupplyApproveDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private FrameworkElement PartsSalesOrderForApproveDetailPanel {
            get {
                return this.partsSalesOrderForApproveDetailPanel ?? (this.partsSalesOrderForApproveDetailPanel = DI.GetDataEditPanel("PartsSalesOrderForSupplyApprove"));
            }
        }

        private DataGridViewBase PartsSalesOrderProcessDetailForSupplyApproveDataGridView {
            get {
                if(this.partsSalesOrderProcessDetailForSupplyApproveDataGridView == null) {
                    this.partsSalesOrderProcessDetailForSupplyApproveDataGridView = DI.GetDataGridView("PartsSalesOrderProcessDetailForSupplyApprove");
                    this.partsSalesOrderProcessDetailForSupplyApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesOrderProcessDetailForSupplyApproveDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();
            this.Root.Children.Add(PartsSalesOrderForApproveDetailPanel);
            this.innerFramework.Children.Add(this.CreateHorizontalLine(2));
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Content = this.PartsSalesOrderProcessDetailForSupplyApproveDataGridView,
                Header = Utils.GetEntityLocalizedName(typeof(PartsSalesOrderProcess), "PartsSalesOrderProcessDetails")
            });
            tabControl.SetValue(Grid.RowProperty, 0);
            tabControl.SetValue(Grid.ColumnProperty, 3);
            tabControl.SetValue(Grid.RowSpanProperty, 2);
            this.innerFramework.Children.Add(tabControl);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void PartsSalesOrderProcessDetail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            switch(e.PropertyName) {
                case "SupplierCompanyId":
                    var partsSalesOrderProcessDetail = sender as PartsSalesOrderProcessDetail;
                    if(partsSalesOrderProcessDetail == null)
                        return;
                    //界面无法清除校验出错信息，手工清除
                    if(partsSalesOrderProcessDetail.HasValidationErrors && partsSalesOrderProcessDetail.ValidationErrors.Any(entity => entity.MemberNames.Contains("SupplierCompanyId"))) {
                        foreach(var error in partsSalesOrderProcessDetail.ValidationErrors.Where(entity => entity.MemberNames.Contains("SupplierCompanyId")).ToArray()) {
                            partsSalesOrderProcessDetail.ValidationErrors.Remove(error);
                        }
                    }
                    this.NotifyCommandsCanExecuteChanged();
                    break;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderWithPartsSalesOrderDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.currentPartsSalesOrder = entity;
                this.PartsSalesOrderForApproveDetailPanel.DataContext = null;
                this.PartsSalesOrderForApproveDetailPanel.DataContext = entity;
                this.branchId = entity.BranchId;
                var salesOrderProcess = new PartsSalesOrderProcess();
                salesOrderProcess.Code = GlobalVar.ASSIGNED_BY_SERVER;
                salesOrderProcess.OriginalSalesOrderId = entity.Id;
                salesOrderProcess.BillStatusBeforeProcess = entity.Status;
                salesOrderProcess.ShippingMethod = entity.ShippingMethod;
                salesOrderProcess.RequestedShippingTime = entity.RequestedShippingTime;
                salesOrderProcess.RequestedDeliveryTime = entity.RequestedDeliveryTime;
                salesOrderProcess.PartsSalesCategoryId = entity.SalesCategoryId;
                salesOrderProcess.IsTransSap = true;
                var partsIds = entity.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();

                foreach(var detail in entity.PartsSalesOrderDetails) {
                    var processDetail = new PartsSalesOrderProcessDetail {
                        PartsSalesOrderProcess = salesOrderProcess,
                        SparePartId = detail.SparePartId,
                        SparePartCode = detail.SparePartCode,
                        SparePartName = detail.SparePartName,
                        MeasureUnit = detail.MeasureUnit,
                        OrderedQuantity = detail.OrderedQuantity,
                        OrderProcessMethod = (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发,
                        //当件销售订单.状态=提交时，未满足数量等于订单数量
                        UnfulfilledQuantity = entity.Status == (int)DcsPartsSalesOrderStatus.提交 ? detail.OrderedQuantity : (detail.OrderedQuantity - detail.ApproveQuantity.GetValueOrDefault()),
                        CurrentFulfilledQuantity = entity.Status == (int)DcsPartsSalesOrderStatus.提交 ? detail.OrderedQuantity : (detail.OrderedQuantity - detail.ApproveQuantity.GetValueOrDefault()),
                        OrderPrice = detail.OrderPrice,
                        WarehouseCode = "xxx",
                        WarehouseName = "xxx"
                    };
                    processDetail.PropertyChanged -= PartsSalesOrderProcessDetail_PropertyChanged;
                    processDetail.PropertyChanged += PartsSalesOrderProcessDetail_PropertyChanged;
                    salesOrderProcess.PartsSalesOrderProcessDetails.Add(processDetail);

                }
                this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == entity.SalesUnitOwnerCompanyId), LoadBehavior.RefreshCurrent, loadOpCom => {
                    if(loadOpCom.HasError) {
                        loadOpCom.MarkErrorAsHandled();
                        return;
                    }
                    if(loadOpCom.Entities != null && loadOpCom.Entities.Any()) {
                        this.currentPartsSalesOrder.SalesUnitOwnerCompanyType = loadOpCom.Entities.First().Type;
                        entity.SalesUnitOwnerCompanyType = loadOpCom.Entities.First().Type;
                    }
                }, null);
                this.SetObjectToEdit(salesOrderProcess);
                this.DomainContext.PartsSalesOrderProcesses.Add(salesOrderProcess);
                this.DomainContext.Load(this.DomainContext.GetPartsSupplierRelationsByPartsIdsQuery(partsIds), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError) {
                        loadOption.MarkErrorAsHandled();
                        return;
                    }
                    foreach(var partsSalesOrderProcessDetail in salesOrderProcess.PartsSalesOrderProcessDetails) {
                        if(loadOption.Entities != null && loadOption.Entities.FirstOrDefault(r => r.PartId == partsSalesOrderProcessDetail.SparePartId && (r.IsPrimary.HasValue && r.IsPrimary.Value)) != null) {
                            partsSalesOrderProcessDetail.SupplierCompanyId = default(int);
                            partsSalesOrderProcessDetail.SupplierCompanyId = loadOption.Entities.FirstOrDefault(r => r.PartId == partsSalesOrderProcessDetail.SparePartId && (r.IsPrimary.HasValue && r.IsPrimary.Value)).PartsSupplier.Id;
                            partsSalesOrderProcessDetail.SupplierCompanyCode = loadOption.Entities.FirstOrDefault(r => r.PartId == partsSalesOrderProcessDetail.SparePartId && (r.IsPrimary.HasValue && r.IsPrimary.Value)).PartsSupplier.Code;
                            partsSalesOrderProcessDetail.SupplierCompanyName = loadOption.Entities.FirstOrDefault(r => r.PartId == partsSalesOrderProcessDetail.SparePartId && (r.IsPrimary.HasValue && r.IsPrimary.Value)).PartsSupplier.Name;
                        }
                    }
                }, null);
            }, null);
        }


        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEditView_Title_DirectApprove_PartsSalesOrder;
            }
        }

        protected override void OnEditSubmitting() {
            var partsSalesOrderProcess = this.DataContext as PartsSalesOrderProcess;
            if(partsSalesOrderProcess == null || !this.PartsSalesOrderProcessDetailForSupplyApproveDataGridView.CommitEdit())
                return;
            partsSalesOrderProcess.ValidationErrors.Clear();
            if(partsSalesOrderProcess.ShippingMethod == default(int))
                partsSalesOrderProcess.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcess_ShippingMethodIsNull, new[] {
                    "ShippingMethod"
                }));
            if(!partsSalesOrderProcess.RequestedDeliveryTime.HasValue) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcess_RequestedDeliveryTimeIsNull);
                return;
            }
            if(partsSalesOrderProcess.PartsSalesOrderProcessDetails.Any(r => r.CurrentFulfilledQuantity > r.OrderedQuantity)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_CurrentFulfilledQuantityGreateThanOrderQuantityError);
                return;
            }
            if(partsSalesOrderProcess.PartsSalesOrderProcessDetails.Any(r => r.CurrentFulfilledQuantity > r.UnfulfilledQuantity)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_CurrentFulfilledQuantityGreateThanUnfulfilledQuantityError);
                return;
            }
            if(partsSalesOrderProcess.PartsSalesOrderProcessDetails.Any(r => r.SupplierCompanyId.GetValueOrDefault() == default(int))) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_SupplierIsRequired);
                return;
            }
            if(partsSalesOrderProcess.HasValidationErrors || partsSalesOrderProcess.PartsSalesOrderProcessDetails.Any(e => e.HasValidationErrors))
                return;
            if(partsSalesOrderProcess.PartsSalesOrderProcessDetails.All(e => e.CurrentFulfilledQuantity == 0)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcess_ApprovalResultIsNull);
                return;
            }
            if(partsSalesOrderProcess.PartsSalesOrderProcessDetails.All(r => r.OrderProcessMethod == (int)DcsPartsSalesOrderProcessDetailProcessMethod.未处理)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_OrderProcessMethod);
                return;
            }
            if (!partsSalesOrderProcess.PartsSalesOrderProcessDetails.Any(e => e.CurrentFulfilledQuantity != 0)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_CurrentFulfilledQuantity);
                return;
            }
            foreach (var item in partsSalesOrderProcess.PartsSalesOrderProcessDetails) {
                if (item.CurrentFulfilledQuantity <= 0)
                    partsSalesOrderProcess.PartsSalesOrderProcessDetails.Remove(item);
            }
            //计算订单处理清单的本次本次满足金额
            if(partsSalesOrderProcess.PartsSalesOrderProcessDetails.Any(e => e.CurrentFulfilledQuantity != 0)) {
                var partsSalesOrderProcessdetail = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Where(r => r.CurrentFulfilledQuantity != 0);
                var partsSalesOrderProcessDetails = partsSalesOrderProcessdetail as PartsSalesOrderProcessDetail[] ?? partsSalesOrderProcessdetail.ToArray();
                partsSalesOrderProcess.CurrentFulfilledAmount = partsSalesOrderProcessDetails.Sum(r => r.CurrentFulfilledQuantity * r.OrderPrice);
                //校验销售订单处理单的本次满足金额是否大于分公司策略的最大开票金额
                this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == branchId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    var branchstrategy = loadOp.Entities.SingleOrDefault();
                    if(branchstrategy != null && partsSalesOrderProcess.CurrentFulfilledAmount > branchstrategy.MaxInvoiceAmount) {
                        UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_CurrentFulfilledAmount);
                        return;
                    }
                    //校验订单处理清单本次满足数量必须为配件信息.最小包装数量整数倍
                    var checkpackageids = partsSalesOrderProcessDetails.Select(v => v.SparePartId).ToArray();
                    this.DomainContext.Load(this.DomainContext.GetSparePartByIdsQuery(checkpackageids), LoadBehavior.RefreshCurrent, loadSparePart => {
                        if(loadSparePart.HasError)
                            return;
                        var spareParts = loadSparePart.Entities.ToArray();
                        if(spareParts.Any(r => r.MInPackingAmount == null || r.MInPackingAmount == 0)) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_MInPackingAmountNew);
                            return;
                        }
                        var isNotCheckMInPackingAmount = this.currentPartsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.代理库 || this.currentPartsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.服务站兼代理库;
                        if(!isNotCheckMInPackingAmount && this.currentPartsSalesOrder.PartsSalesOrderTypeName != PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_Urgent && this.currentPartsSalesOrder.PartsSalesOrderTypeName != PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_ExtraUrgent && this.currentPartsSalesOrder.PartsSalesOrderTypeName != "三包垫件" && this.currentPartsSalesOrder.PartsSalesOrderTypeName != "国内赠送" && this.currentPartsSalesOrder.PartsSalesOrderTypeName != "集团订单" && this.currentPartsSalesOrder.PartsSalesOrderTypeName != "6K1" && this.currentPartsSalesOrder.PartsSalesOrderTypeName != "6K2") {
                            foreach(var detail in partsSalesOrderProcessDetails) {
                                if(spareParts.Any(r => r.Id == detail.SparePartId && (detail.CurrentFulfilledQuantity % r.MInPackingAmount.Value) != 0)) {
                                    var partsValidation = spareParts.First(r => r.Id == detail.SparePartId && (detail.CurrentFulfilledQuantity % r.MInPackingAmount.Value) != 0);
                                    UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_OrderedQuantity, partsValidation.Code, partsValidation.MInPackingAmount));
                                    return;
                                }
                            }
                        }
                        ((IEditableObject)partsSalesOrderProcess).EndEdit();
                        try {
                            if(partsSalesOrderProcess.Can配件销售直供审批服务)
                                partsSalesOrderProcess.配件销售直供审批服务(partsSalesOrderProcess.IsTransSap);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                        base.OnEditSubmitting();
                    }, null);
                }, null);
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public IEnumerable<KeyValuePair> KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}