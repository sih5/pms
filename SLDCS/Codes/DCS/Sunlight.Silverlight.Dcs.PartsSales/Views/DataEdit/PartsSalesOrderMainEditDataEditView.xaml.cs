﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderMainEditDataEditView {
        private ObservableCollection<CompanyAddress> kvCompanyAddresses;
        private ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes;
        private ObservableCollection<KeyValuePair> kvSalesUnits;
        private ObservableCollection<KeyValuePair> kvShippingMethods;
        private readonly List<TiledRegion> TiledRegions = new List<TiledRegion>();
        private readonly ObservableCollection<KeyValuePair> kvReceiverSalesCateNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvReceivingWarehouseNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvReceivingWarehouse = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvProvinceNames;
        private ObservableCollection<KeyValuePair> kvCityNames;
        private KeyValueManager keyValueManager;
        private Company companys;
        //private int? receivingCompanyType = null; //收货单位企业类型
        /// <summary>
        /// 当前登录人企业类型 用于过滤品牌
        /// </summary>
        private int currentCompanyType;

        private readonly string[] kvNames = {
            "PartsSalesOrder_SecondLevelOrderType", "PartsShipping_Method"
        };
        public PartsSalesOrderMainEditDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            //this.DataContextChanged += PartsSalesOrderDataEditView_DataContextChanged;
            this.Initializer.Register(this.Initialize);
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public ObservableCollection<CompanyAddress> KvCompanyAddresses {
            get {
                return this.kvCompanyAddresses ?? (this.kvCompanyAddresses = new ObservableCollection<CompanyAddress>());
            }
        }

        public ObservableCollection<KeyValuePair> KvProvinceNames {
            get {
                return this.kvProvinceNames ?? (this.kvProvinceNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvCityNames {
            get {
                return this.kvCityNames ?? (this.kvCityNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes {
            get {
                return this.kvPartsSalesOrderTypes ?? (this.kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvShippingMethods {
            get {
                return this.kvShippingMethods ?? (this.kvShippingMethods = new ObservableCollection<KeyValuePair>());

            }
        }

        public ObservableCollection<KeyValuePair> KvSalesUnits {
            get {
                return this.kvSalesUnits ?? (this.kvSalesUnits = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvReceivingWarehouse {
            get {
                return this.kvReceivingWarehouse;
            }
        }


        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories;
            }
        }

        private ObservableCollection<SalesUnitAffiWarehouse> kvSalesUnitAffiWarehouses;

        public ObservableCollection<SalesUnitAffiWarehouse> KvSalesUnitAffiWarehouses {
            get {
                return this.kvSalesUnitAffiWarehouses ?? (this.kvSalesUnitAffiWarehouses = new ObservableCollection<SalesUnitAffiWarehouse>());
            }
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "PartsShipping_Method" && r.Key != (int)DcsPartsShippingMethod.DHL_出口 && r.Key != (int)DcsPartsShippingMethod.海运_出口 && r.Key != (int)DcsPartsShippingMethod.空运_出口 && r.Key != (int)DcsPartsShippingMethod.铁路_出口), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvShippingMethods.Clear();
                foreach(var partsSalesOrderType in loadOp.Entities) {
                    this.KvShippingMethods.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Key,
                        Value = partsSalesOrderType.Value,
                    });
                }
            }, null);
        }

        private void InitPartsSalesCategorie() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOpCompanies => {
                if(loadOpCompanies.HasError) {
                    loadOpCompanies.MarkErrorAsHandled();
                    return;
                }
                if(loadOpCompanies.Entities != null && loadOpCompanies.Entities.Any()) {
                    currentCompanyType = loadOpCompanies.Entities.First().Type;
                    if(currentCompanyType == (int)DcsCompanyType.分公司) {
                        if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库 || partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                            this.kvPartsSalesCategories.Clear();
                            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                                if(loadOption.HasError)
                                    return;
                                this.KvPartsSalesCategories.Clear();
                                foreach(var partsSalesCategory in loadOption.Entities) {
                                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                                        Key = partsSalesCategory.Id,
                                        Value = partsSalesCategory.Name
                                    });
                                }
                                this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                            }, null);
                        }
                        if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.服务站 || partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.集团企业) {
                            this.kvPartsSalesCategories.Clear();
                            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesByDealerServiceInfoQuery(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                this.KvPartsSalesCategories.Clear();
                                foreach(var partsSalesCategory in loadOp.Entities) {
                                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                                        Key = partsSalesCategory.Id,
                                        Value = partsSalesCategory.Name
                                    });
                                }
                                this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                            }, null);
                        }
                    }
                    if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.分公司) {
                        this.kvPartsSalesCategories.Clear();
                        this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                            if(loadOption.HasError)
                                return;
                            this.KvPartsSalesCategories.Clear();
                            foreach(var partsSalesCategory in loadOption.Entities) {
                                this.KvPartsSalesCategories.Add(new KeyValuePair {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name
                                });
                            }
                            this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                        }, null);
                    }

                    if(currentCompanyType == (int)DcsCompanyType.代理库 || currentCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                        this.kvPartsSalesCategories.Clear();
                        this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesByAgencyDealerRelationQuery(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }
                            this.KvPartsSalesCategories.Clear();
                            foreach(var partsSalesCategory in loadOp.Entities) {
                                this.KvPartsSalesCategories.Add(new KeyValuePair {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name
                                });
                            }
                            this.txtSalesCategory.SelectedIndex = 0;//默认品牌
                        }, null);
                    }
                }
            }, null);

            this.DomainContext.Load(this.DomainContext.GetSalesUnitsQuery().Where(ex => ex.Status == (int)DcsMasterDataStatus.有效 && ex.OwnerCompanyId == partsSalesOrder.ReceivingCompanyId && ex.PartsSalesCategoryId == partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError) {
                    loadOp2.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
                    return;
                }
                var entity = loadOp2.Entities.FirstOrDefault();
                if(entity != null) {
                    this.DomainContext.Load(this.DomainContext.GetWarehousesBySalesUnitIdQuery(entity.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            loadOp1.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                            return;
                        }
                        this.KvReceivingWarehouse.Clear();
                        if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
                            foreach(var warehouses in loadOp1.Entities) {
                                this.KvReceivingWarehouse.Add(new KeyValuePair {
                                    Key = warehouses.Id,
                                    Value = warehouses.Name,
                                    UserObject = warehouses
                                });
                            }
                        }
                    }, null);
                }
            }, null);

            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Id == partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOptionM => {
                if(loadOptionM.HasError) {
                    loadOptionM.MarkErrorAsHandled();
                    return;
                }
                if(currentCompanyType == (int)DcsCompanyType.代理库 || currentCompanyType == (int)DcsCompanyType.服务站兼代理库) {
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Id == partsSalesOrder.SalesCategoryId), LoadBehavior.RefreshCurrent, loadOption => {
                        if(loadOption.HasError) {
                            loadOption.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOption.Entities != null && loadOption.Entities.Any()) {
                            this.LoadWarehouse(partsSalesOrder.SubmitCompanyId, loadOption.Entities.First().BranchId, partsSalesOrder.SalesCategoryId);
                        }
                    }, null);
                } else {
                    this.LoadWarehouse(partsSalesOrder.SubmitCompanyId, partsSalesOrder.BranchId, partsSalesOrder.SalesCategoryId);
                }
                this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypeWithCustomerOrderPriceGradesQuery(partsSalesOrder.SalesCategoryId, partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.KvPartsSalesOrderTypes.Clear();
                    foreach(var partsSalesOrderType in loadOp.Entities) {
                        this.KvPartsSalesOrderTypes.Add(new KeyValuePair {
                            Key = partsSalesOrderType.Id,
                            Value = partsSalesOrderType.Name,
                        });
                    }
                }, null);
            }, null);
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(ex => ex.Id == partsSalesOrder.SubmitCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.FirstOrDefault();
                if(company == null)
                    return;
                this.companys = company;
            }, null);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_EditMain;
            }
        }
        private void CreateUI() {
            this.KeyValueManager.LoadData();
            this.DomainContext.Load(this.DomainContext.GetSalesUnitsQuery().Where(r => r.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.KvSalesUnits.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvSalesUnits.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name,
                        UserObject = entity
                    });
                }
            }, null);
        }

        private void LoadWarehouse(int submitCompanyId, int branchId, int salesCategoryId) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            this.DomainContext.Load(this.DomainContext.查询销售组织仓库Query(submitCompanyId, branchId, null, salesCategoryId, currentCompanyType).Where(r => r.Warehouse.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadop => {
                if(loadop.HasError) {
                    loadop.MarkErrorAsHandled();
                    return;
                }
                if(loadop.Entities != null) {
                    SalesUnitAffiWarehouse warehouse = loadop.Entities.FirstOrDefault(o => o.WarehouseId == partsSalesOrder.WarehouseId && o.Warehouse.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && o.Warehouse.IsQualityWarehouse != true);
                    if(warehouse != null)
                        this.WarehouseName.Text = warehouse.Warehouse.Name;
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    this.InitPartsSalesCategorie();
                }
            }, null);
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.KvPartsSalesOrderTypes.Clear();
            this.KvCompanyAddresses.Clear();
            this.KvReceivingWarehouse.Clear();
            this.KvPartsSalesCategories.Clear();
        }

        protected override void OnEditSubmitting() {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            partsSalesOrder.ValidationErrors.Clear();
            foreach(var item in partsSalesOrder.PartsSalesOrderDetails) {
                item.ValidationErrors.Clear();
            }
            if(partsSalesOrder.WarehouseId == default(int) || partsSalesOrder.WarehouseId == null) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_WarehouseIsNull);
                return;
            }
            if(partsSalesOrder.PartsSalesOrderTypeId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIsNull, new[] {
                    "PartsSalesOrderTypeName"
                }));
            if(partsSalesOrder.ShippingMethod == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ShippingMethodIsNull, new[] {
                    "ShippingMethod"
                }));
            if(partsSalesOrder.SalesUnitId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesUnitIdIsNull, new[] {
                    "SalesUnitId"
                }));
            if(partsSalesOrder.SubmitCompanyId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SubmitCompanyIdIsNull, new[] {
                    "SubmitCompanyName"
                }));
            if(string.IsNullOrEmpty(partsSalesOrder.ReceivingAddress))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingAddressIsNull, new[] {
                    "ReceivingAddress"
                }));
            if(partsSalesOrder.PartsSalesOrderTypeId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIdIsNull, new[] {
                    "PartsSalesOrderTypeId"
                }));
            if(partsSalesOrder.ReceivingCompanyId == default(int))
                partsSalesOrder.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingCompanyIdIsNull, new[] {
                    "ReceivingCompanyName"
                }));
            if(companys.Type == (int)DcsCompanyType.代理库 || companys.Type == (int)DcsCompanyType.服务站兼代理库) {
                if(partsSalesOrder.ReceivingWarehouseId == default(int) || partsSalesOrder.ReceivingWarehouseId == null) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_ReceivingWarehouseNameIsNull);
                    return;
                }
            }
            if(partsSalesOrder.HasValidationErrors || partsSalesOrder.PartsSalesOrderDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsSalesOrder).EndEdit();
            base.OnEditSubmitting();
        }
    }
}
