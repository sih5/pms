﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderTypeNDataEditView : INotifyPropertyChanged {
        private DataGridViewBase partsSalesOrderTypeNForEditDataGridView;
        public event PropertyChangedEventHandler PropertyChanged;
        private int partsSalesCategoryId;
        private string partsSalesCategoryName;
        private ObservableCollection<PartsSalesOrderType> partsSalesOrderTypes;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategoryName;

        public ObservableCollection<KeyValuePair> KvPartsSalesCategoryName {
            get {
                if(this.kvPartsSalesCategoryName == null)
                    this.kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
                return this.kvPartsSalesCategoryName;
            }
        }

        public ObservableCollection<PartsSalesOrderType> PartsSalesOrderTypes {
            get {
                if(this.partsSalesOrderTypes == null) {
                    this.partsSalesOrderTypes = new ObservableCollection<PartsSalesOrderType>();
                } return partsSalesOrderTypes;
            }
        }


        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public int PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }

        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        private DataGridViewBase PartsSalesOrderTypeNForEditDataGridView {
            get {
                if(this.partsSalesOrderTypeNForEditDataGridView == null) {
                    this.partsSalesOrderTypeNForEditDataGridView = DI.GetDataGridView("PartsSalesOrderTypeNForEdit");
                    this.partsSalesOrderTypeNForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsSalesOrderTypeNForEditDataGridView.DataContext = this;
                }
                return this.partsSalesOrderTypeNForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.DataGridViewRoot.Visibility = Visibility.Visible;
            this.First.Visibility = Visibility.Visible;
            this.Second.Visibility = Visibility.Collapsed;
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DataEditView_Title_PartsSalesOrderTypeDetail, null, () => this.PartsSalesOrderTypeNForEditDataGridView);
            this.DataGridViewRoot.Children.Add(detailDataEditView);
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsSalesOrderTypeNForEditDataGridView.CommitEdit())
                return;
            var partsSalesOrderType = this.DataContext as PartsSalesOrderType;
            if(this.EditState == DataEditState.New) {
                if(partsSalesCategoryId == default(int)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesCategoryIdIsNull);
                    return;
                }
                if(partsSalesOrderTypes.Count() == 0) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderTypeIsNull);
                    return;
                }
                if(partsSalesOrderTypes.GroupBy(entity => new {
                    entity.Name,
                    entity.Code
                }).Any(array => array.Count() > 1)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesCategory_PartsSalesOrderTypesCodeAndNameUnique);
                    return;
                }
                foreach(var entity in this.partsSalesOrderTypes)
                    entity.ValidationErrors.Clear();
                foreach(var entity in this.partsSalesOrderTypes) {
                    partsSalesOrderType.PartsSalesCategoryId = partsSalesCategoryId;
                    partsSalesOrderType.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsSalesOrderType.Status = (int)DcsBaseDataStatus.有效;
                    partsSalesOrderType.Code = entity.Code;
                    partsSalesOrderType.Name = entity.Name;
                    partsSalesOrderType.Remark = entity.Remark;
                    ((IEditableObject)partsSalesOrderType).EndEdit();
                }
            }

            try {
                if(this.EditState == DataEditState.Edit) {
                    partsSalesOrderType.Status = (int)DcsBaseDataStatus.有效;
                    if(string.IsNullOrWhiteSpace(partsSalesOrderType.Code)) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderType_CodeIsNull);
                        return;
                    }
                    if(string.IsNullOrWhiteSpace(partsSalesOrderType.Name)) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderType_NameIsNull);
                        return;
                    }
                    ((IEditableObject)partsSalesOrderType).EndEdit();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesCategory;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }


        protected override void OnEditSubmitted() {
            if(this.PartsSalesOrderTypes != null)
                PartsSalesOrderTypes.Clear();
            this.DataGridViewRoot.Visibility = Visibility.Visible;
            this.First.Visibility = Visibility.Visible;
            this.Second.Visibility = Visibility.Collapsed;
            this.PartsSalesCategoryId = default(int);
            this.PartsSalesCategoryName = string.Empty;
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            if(this.PartsSalesOrderTypes != null)
                PartsSalesOrderTypes.Clear();
            this.DataGridViewRoot.Visibility = Visibility.Visible;
            this.First.Visibility = Visibility.Visible;
            this.Second.Visibility = Visibility.Collapsed;
            this.PartsSalesCategoryId = default(int);
            this.PartsSalesCategoryName = string.Empty;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypesQuery().Where(v => v.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                this.DataGridViewRoot.Visibility = Visibility.Collapsed;
                this.First.Visibility = Visibility.Collapsed;
                this.Second.Visibility = Visibility.Visible;
                this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.Id == entity.PartsSalesCategoryId), loadOp1 => {
                    if(loadOp1.HasError)
                        return;
                    var entity1 = loadOp1.Entities.FirstOrDefault();
                    PartsSalesCategoryName = entity1.Name;
                }, null);

                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public PartsSalesOrderTypeNDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded+=PartsSalesOrderTypeNDataEditView_Loaded;
        }

        private void PartsSalesOrderTypeNDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategoryName.Clear();
                foreach(var partsSalesCategory in loadOp.Entities) {
                    this.KvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                }
            }, null);
        }

    }
}
