﻿
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderTypeNNDataEditView : INotifyPropertyChanged {
        private DataGridViewBase partsSalesOrderTypeNNForEditDataGridView;
        public event PropertyChangedEventHandler PropertyChanged;
        private int partsSalesCategoryId;
        private int branchId;
        private string partsSalesCategoryName;
        private string branchName;
        private ObservableCollection<PartsSalesOrderType> partsSalesOrderTypes;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategoryName;
        private ObservableCollection<KeyValuePair> kvBranch;
       
        public ObservableCollection<KeyValuePair> KvPartsSalesCategoryName {
            get {
                if(this.kvPartsSalesCategoryName == null)
                    this.kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
                return this.kvPartsSalesCategoryName;
            }
        }

        public ObservableCollection<KeyValuePair> KvBranch {
            get {
                if(this.kvBranch == null)
                    this.kvBranch = new ObservableCollection<KeyValuePair>();
                return this.kvBranch;
            }
        }

        public ObservableCollection<PartsSalesOrderType> PartsSalesOrderTypes {
            get {
                if(this.partsSalesOrderTypes == null) {
                    this.partsSalesOrderTypes = new ObservableCollection<PartsSalesOrderType>();
                } return partsSalesOrderTypes;
            }
        }


        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public int PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }

        public int BranchId {
            get {
                return this.branchId;
            }
            set {
                this.branchId = value;
                this.OnPropertyChanged("BranchId");
            }
        }

        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        public string BranchName {
            get {
                return this.branchName;
            }
            set {
                this.branchName = value;
                this.OnPropertyChanged("BranchName");
            }
        }

        private DataGridViewBase PartsSalesOrderTypeNForEditDataGridView {
            get {
                if(this.partsSalesOrderTypeNNForEditDataGridView == null) {
                    this.partsSalesOrderTypeNNForEditDataGridView = DI.GetDataGridView("PartsSalesOrderTypeNForEdit");
                    this.partsSalesOrderTypeNNForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsSalesOrderTypeNNForEditDataGridView.DataContext = this;
                }
                return this.partsSalesOrderTypeNNForEditDataGridView;
            }
        }

        private void CreateUI() {
            //this.DataGridViewRoot.Visibility = Visibility.Visible;
            this.Root.Children.Add(this.CreateVerticalLine(1, 0, 0, 0));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DataEditView_Title_PartsSalesOrderTypeDetail, null, () => this.PartsSalesOrderTypeNForEditDataGridView);
            Grid.SetColumn(detailDataEditView, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsSalesOrderTypeNForEditDataGridView.CommitEdit())
                return;
            if(partsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesCategoryIdIsNull);
                return;
            }
            if(PartsSalesOrderTypes.Count() == 0) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderTypeIsNull);
                return;
            }
            if(PartsSalesOrderTypes.GroupBy(entity => new {
                entity.Name,
                entity.Code
            }).Any(array => array.Count() > 1)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesCategory_PartsSalesOrderTypesCodeAndNameUnique);
                return;
            }
            foreach(var entity in this.PartsSalesOrderTypes)
                entity.ValidationErrors.Clear();
            foreach(var partsSalesOrderType in this.PartsSalesOrderTypes) {
                if(null == partsSalesOrderType.SettleType) {
                    UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SettleType, partsSalesOrderType.Code));
                    return;
                }
                if(null == partsSalesOrderType.BusinessType) {
                    UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_BusinessType, partsSalesOrderType.Code));
                    return;
                }
                partsSalesOrderType.PartsSalesCategoryId = partsSalesCategoryId;
                partsSalesOrderType.BranchId = branchId;
                partsSalesOrderType.Status = (int)DcsBaseDataStatus.有效;
                //partsSalesOrderType.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                ((IEditableObject)partsSalesOrderType).EndEdit();
            }
            foreach(var item in this.PartsSalesOrderTypes) {
                if(this.DomainContext.PartsSalesOrderTypes.Contains(item))
                    this.DomainContext.PartsSalesOrderTypes.Remove(item);
            }
            foreach(var item in this.PartsSalesOrderTypes) {
                if(!this.DomainContext.PartsSalesOrderTypes.Contains(item))
                    this.DomainContext.PartsSalesOrderTypes.Add(item);
            }
            if(this.PartsSalesOrderTypes.Any(r => r.ValidationErrors.Any()))
                return;
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesCategory;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }


        protected override void OnEditSubmitted() {
            if(this.PartsSalesOrderTypes != null)
                PartsSalesOrderTypes.Clear();
            //this.DataGridViewRoot.Visibility = Visibility.Visible;
            this.PartsSalesCategoryId = default(int);
            this.BranchId = default(int);
            this.PartsSalesCategoryName = string.Empty;
            this.BranchName = string.Empty;
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            if(this.PartsSalesOrderTypes != null)
                PartsSalesOrderTypes.Clear();
            //this.DataGridViewRoot.Visibility = Visibility.Visible;
            this.PartsSalesCategoryId = default(int);
            this.BranchId = default(int);
            this.BranchName = string.Empty;
            this.PartsSalesCategoryName = string.Empty;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypesQuery().Where(v => v.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                //this.DataGridViewRoot.Visibility = Visibility.Collapsed;
                this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.Id == entity.PartsSalesCategoryId), loadOp1 => {
                    if(loadOp1.HasError)
                        return;
                    var entity1 = loadOp1.Entities.FirstOrDefault();
                    PartsSalesCategoryName = entity1.Name;
                }, null);

                if (entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public PartsSalesOrderTypeNNDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += PartsSalesOrderTypeNDataEditView_Loaded;
        }

        private void PartsSalesOrderTypeNDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranch.Clear();
                foreach(var branch in loadOp.Entities) {
                    this.KvBranch.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
                }
                this.txtBranch.SelectedIndex = 0;//默认分公司
            }, null);


        }

        private void DcsComboBox_SelectionChanged_1(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {

            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.BranchId == BranchId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategoryName.Clear();
                foreach(var partsSalesCategory in loadOp.Entities) {
                    this.KvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                }
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
        }

    }
}
