﻿
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesOrderTypeNNEditDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private int partsSalesCategoryId;
        private int branchId;
        private string partsSalesCategoryName;
        private string branchName;
        private ObservableCollection<PartsSalesOrderType> partsSalesOrderTypes;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategoryName;
        private ObservableCollection<KeyValuePair> kvBranch;

        private KeyValueManager keyValueManager;

        private readonly string[] kvNames = {
            "SalesOrderType_SettleType","SalesOrderType_BusinessType"
        };
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
     
      
        public object KvSettleTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        public object KvBusinessTypes {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
       
        public ObservableCollection<KeyValuePair> KvPartsSalesCategoryName {
            get {
                if(this.kvPartsSalesCategoryName == null)
                    this.kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
                return this.kvPartsSalesCategoryName;
            }
        }

        public ObservableCollection<KeyValuePair> KvBranch {
            get {
                if(this.kvBranch == null)
                    this.kvBranch = new ObservableCollection<KeyValuePair>();
                return this.kvBranch;
            }
        }

        private ObservableCollection<KeyValuePair> kvBranchName;

        public ObservableCollection<KeyValuePair> KvBranchName {
            get {
                if(this.kvBranchName == null)
                    this.kvBranchName = new ObservableCollection<KeyValuePair>();
                return this.kvBranchName;
            }
        }
        public ObservableCollection<PartsSalesOrderType> PartsSalesOrderTypes {
            get {
                if(this.partsSalesOrderTypes == null) {
                    this.partsSalesOrderTypes = new ObservableCollection<PartsSalesOrderType>();
                } return partsSalesOrderTypes;
            }
        }


        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public int PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }

        public int BranchId {
            get {
                return this.branchId;
            }
            set {
                this.branchId = value;
                this.OnPropertyChanged("BranchId");
            }
        }

        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        public string BranchName {
            get {
                return this.branchName;
            }
            set {
                this.branchName = value;
                this.OnPropertyChanged("BranchName");
            }
        }

        protected override void OnEditSubmitting() {
          
            var partsSalesOrderType = this.DataContext as PartsSalesOrderType;
           
            try {
                if(this.EditState == DataEditState.Edit) {
                    partsSalesOrderType.Status = (int)DcsBaseDataStatus.有效;
                    if(string.IsNullOrWhiteSpace(partsSalesOrderType.Code)) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderType_CodeIsNull);
                        return;
                    }
                    if(string.IsNullOrWhiteSpace(partsSalesOrderType.Name)) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderType_NameIsNull);
                        return;
                    }
                    if(null == partsSalesOrderType.SettleType) {
                        UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_SettleType, partsSalesOrderType.Code));
                        return;
                    }
                    if(null == partsSalesOrderType.BusinessType) {
                        UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_BusinessType, partsSalesOrderType.Code));
                        return;
                    }
                    ((IEditableObject)partsSalesOrderType).EndEdit();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesCategory;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }


        protected override void OnEditSubmitted() {
            if(this.PartsSalesOrderTypes != null)
                PartsSalesOrderTypes.Clear();
            this.PartsSalesCategoryId = default(int);
            this.BranchId = default(int);
            this.PartsSalesCategoryName = string.Empty;
            this.BranchName = string.Empty;
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            if(this.PartsSalesOrderTypes != null)
                PartsSalesOrderTypes.Clear();
            this.PartsSalesCategoryId = default(int);
            this.BranchId = default(int);
            this.PartsSalesCategoryName = string.Empty;
            this.BranchName = string.Empty;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypesQuery().Where(v => v.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.Id == entity.PartsSalesCategoryId), loadOp1 => {
                    if(loadOp1.HasError)
                        return;
                    var entity1 = loadOp1.Entities.FirstOrDefault();
                    PartsSalesCategoryName = entity1.Name;
                }, null);

                this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(ex => ex.Id == entity.BranchId), loadOp2 => {
                    if(loadOp2.HasError)
                        return;
                    var entity2 = loadOp2.Entities.FirstOrDefault();
                    BranchName = entity2.Name;
                }, null);

                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public PartsSalesOrderTypeNNEditDataEditView() {
            InitializeComponent();
            this.Loaded += PartsSalesOrderTypeNDataEditView_Loaded;
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
        }

        private void PartsSalesOrderTypeNDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranch.Clear();
                foreach(var branch in loadOp.Entities) {
                    this.KvBranch.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
                }
            }, null);

            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategoryName.Clear();
                foreach(var partsSalesCategory in loadOp.Entities) {
                    this.KvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                }
            }, null);
        }

    }
}
