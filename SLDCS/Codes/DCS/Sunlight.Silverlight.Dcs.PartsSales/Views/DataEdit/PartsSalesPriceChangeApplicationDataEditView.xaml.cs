﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesPriceChangeApplicationDataEditView {
        public PartsSalesPriceChangeApplicationDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsSalesPriceChangeApplicationDataEditView_DataContextChanged;
        }

        void PartsSalesPriceChangeApplicationDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            SetPropertyChanged();
            partsSalesPriceChange.PartsSalesPriceChangeDetails.EntityAdded -= PartsSalesPriceChangeDetails_EntityAdded;
            partsSalesPriceChange.PartsSalesPriceChangeDetails.EntityAdded += PartsSalesPriceChangeDetails_EntityAdded;
        }

        private void SetPropertyChanged() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            foreach(var partsSalesPriceChangeDetail in partsSalesPriceChange.PartsSalesPriceChangeDetails) {
                partsSalesPriceChangeDetail.PropertyChanged -= PartsSalesPriceChangeDetail_PropertyChanged;
                partsSalesPriceChangeDetail.PropertyChanged += PartsSalesPriceChangeDetail_PropertyChanged;
            }
        }

        private void PartsSalesPriceChangeDetails_EntityAdded(object sender, EntityCollectionChangedEventArgs<PartsSalesPriceChangeDetail> e) {
            SetPropertyChanged();
        }

        private void PartsSalesPriceChangeDetail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesPriceChangeDetails = sender as PartsSalesPriceChangeDetail;
            if(partsSalesPriceChangeDetails == null)
                return;
            switch(e.PropertyName) {
                case "SalesPrice":
                    partsSalesPriceChangeDetails.SalesPrice = decimal.Round(partsSalesPriceChangeDetails.SalesPrice, 2);
                    break;
                case "RetailGuidePrice":
                    partsSalesPriceChangeDetails.RetailGuidePrice = partsSalesPriceChangeDetails.RetailGuidePrice.HasValue ? decimal.Round((decimal)partsSalesPriceChangeDetails.RetailGuidePrice, 2) : partsSalesPriceChangeDetails.RetailGuidePrice;
                    break;
                case "DealerSalesPrice":
                    partsSalesPriceChangeDetails.DealerSalesPrice = partsSalesPriceChangeDetails.DealerSalesPrice.HasValue ? decimal.Round((decimal)partsSalesPriceChangeDetails.DealerSalesPrice, 2) : partsSalesPriceChangeDetails.DealerSalesPrice;
                    break;
            }
        }

        public DcsPopupsQueryWindowBase partsSalesPriceQueryWindow;
        private RadWindow radQueryWindow;
        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
                        if(partsSalesPriceChange == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPartsSalesPriceChangeDetailPartsSalesCategoryAsync(e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportPartsSalesPriceChangeDetailPartsSalesCategoryCompleted -= excelServiceClient_ImportPartsSalesPriceChangeDetailPartsSalesCategoryCompleted;
                        this.excelServiceClient.ImportPartsSalesPriceChangeDetailPartsSalesCategoryCompleted += excelServiceClient_ImportPartsSalesPriceChangeDetailPartsSalesCategoryCompleted;

                    };
                }
                return this.uploader;
            }
        }

        private void excelServiceClient_ImportPartsSalesPriceChangeDetailPartsSalesCategoryCompleted(object sender, ImportPartsSalesPriceChangeDetailPartsSalesCategoryCompletedEventArgs e) {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            foreach(var detail in partsSalesPriceChange.PartsSalesPriceChangeDetails) {
                partsSalesPriceChange.PartsSalesPriceChangeDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    partsSalesPriceChange.PartsSalesPriceChangeDetails.Add(new PartsSalesPriceChangeDetail {
                        PartsSalesCategoryId = data.PartsSalesCategoryId,
                        PartsSalesCategoryCode = data.PartsSalesCategoryCode,
                        PartsSalesCategoryName = data.PartsSalesCategoryName,
                        MaxExchangeSalePrice = data.MaxExchangeSalePrice,
                        PriceType = data.PriceType,
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        DealerSalesPrice = data.DealerSalesPrice.HasValue ? decimal.Round((decimal)data.DealerSalesPrice, 2) : data.DealerSalesPrice,
                        SalesPrice = decimal.Round(data.SalesPrice, 2),
                        RetailGuidePrice = data.RetailGuidePrice.HasValue ? decimal.Round((decimal)data.RetailGuidePrice, 2) : data.RetailGuidePrice,
                        Remark = data.Remark,
                    });
                }
            }
            SetIsUpsideDownAndPrice();
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }
        private void SetIsUpsideDownAndPrice() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            this.DomainContext.Load(DomainContext.GetPartsPurchasePricingByPartIdsQuery(partsSalesPriceChange.PartsSalesPriceChangeDetails.Select(r => r.SparePartId).ToArray()), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(loadOp.Entities == null)
                    return;
                foreach(var partsSalesPriceChangeDetail in partsSalesPriceChange.PartsSalesPriceChangeDetails) {
                    var purchasePrice = loadOp.Entities.Where(r => r.PartId == partsSalesPriceChangeDetail.SparePartId && r.PartsSalesCategoryId == partsSalesPriceChangeDetail.PartsSalesCategoryId).ToArray();
                    partsSalesPriceChangeDetail.PurchasePrice = 0;
                    if(purchasePrice.Any()) {
                        partsSalesPriceChangeDetail.PurchasePrice = purchasePrice.Min(r => r.PurchasePrice);
                    }
                    var maxPurchasePricing = loadOp.Entities.Where(r => r.PartId == partsSalesPriceChangeDetail.SparePartId && r.PartsSalesCategoryId == partsSalesPriceChangeDetail.PartsSalesCategoryId && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.PriceType == (int)DcsPurchasePriceType.正式价格).ToArray();
                    partsSalesPriceChangeDetail.MaxPurchasePricing = 0;
                    if(maxPurchasePricing.Any()) {
                        partsSalesPriceChangeDetail.MaxPurchasePricing = maxPurchasePricing.Max(r => r.PurchasePrice);
                    }
                    partsSalesPriceChangeDetail.IsUpsideDown = partsSalesPriceChangeDetail.SalesPrice >= partsSalesPriceChangeDetail.PurchasePrice ? 0 : 1;
                }
            }, null);
        }
        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }
        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesPriceChange;
            }
        }

        private DataGridViewBase partsSalesPriceChangeForEditDataGridView;

        private DataGridViewBase PartsSalesPriceChangeApplicationForEditDataGridView {
            get {
                if(this.partsSalesPriceChangeForEditDataGridView == null) {
                    this.partsSalesPriceChangeForEditDataGridView = DI.GetDataGridView("PartsSalesPriceChangeApplicationForEdit");
                    this.partsSalesPriceChangeForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesPriceChangeForEditDataGridView;
            }
        }

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.PartsSalesPriceQueryWindow,
                    Header = PartsSalesUIStrings.DataEdit_Header_PartsSalesOrderDetailList,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }

        public DcsPopupsQueryWindowBase PartsSalesPriceQueryWindow {
            get {
                if(this.partsSalesPriceQueryWindow == null) {
                    this.partsSalesPriceQueryWindow = DI.GetQueryWindow("PartsSalesPriceApplication") as DcsPopupsQueryWindowBase;
                    this.partsSalesPriceQueryWindow.Loaded += partsSalesPriceQueryWindow_Loaded;
                }
                return partsSalesPriceQueryWindow;
            }
        }

        private void partsSalesPriceQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;

            var partsSalesPriceChangeDetail = this.PartsSalesPriceChangeApplicationForEditDataGridView.SelectedEntities.Cast<PartsSalesPriceChangeDetail>().FirstOrDefault();
            if(partsSalesPriceChangeDetail == null)
                return;

            if(queryWindow == null || partsSalesPriceChangeDetail == null)
                return;

            var compositeFilterItem = new CompositeFilterItem {
                LogicalOperator = LogicalOperator.And,
                Filters = {
                    new FilterItem {
                        MemberType = typeof(string),
                        MemberName = "SparePartCode",
                        Operator = FilterOperator.IsEqualTo,
                        Value = partsSalesPriceChangeDetail.SparePartCode,
                    },
                    new FilterItem {
                        MemberType = typeof(string),
                        MemberName = "SparePartName",
                        Operator = FilterOperator.IsEqualTo,
                        Value = partsSalesPriceChangeDetail.SparePartName,
                    }
                }
            };
            this.PartsSalesPriceQueryWindow.ExchangeData(this, "SetQueryItemValue", new object[] {
                "Common", "SparePartCode", partsSalesPriceChangeDetail.SparePartCode
            });
            this.PartsSalesPriceQueryWindow.ExchangeData(this, "SetQueryItemValue", new object[] {
                "Common", "SparePartName", partsSalesPriceChangeDetail.SparePartName
            });
            this.PartsSalesPriceQueryWindow.ExchangeData(this, "SetQueryItemEnabled", new object[] {
                "Common", "SparePartCode", false
            });
            this.PartsSalesPriceQueryWindow.ExchangeData(this, "SetQueryItemEnabled", new object[] {
                "Common", "SparePartName", false
            });
            this.PartsSalesPriceQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);

        }

        protected override void OnEditSubmitting() {
            if(!this.PartsSalesPriceChangeApplicationForEditDataGridView.CommitEdit())
                return;
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            partsSalesPriceChange.ValidationErrors.Clear();
            foreach(var item in partsSalesPriceChange.PartsSalesPriceChangeDetails) {
                item.ValidationErrors.Clear();
            }
            foreach(var relation in partsSalesPriceChange.PartsSalesPriceChangeDetails.Where(e => e.SalesPrice <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPriceChangeDetail_SalesPrice, relation.SparePartCode));
                return;
            }
            foreach(var errorSpareParts in partsSalesPriceChange.PartsSalesPriceChangeDetails.GroupBy(d => new {
                d.SparePartCode,
                d.SparePartName,
                d.PartsSalesCategoryId
            }).Where(d => d.Count() > 1)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPriceChangeDetail_DoubleSparePart, errorSpareParts.Key.SparePartName));
                return;
            }
            if(partsSalesPriceChange.HasValidationErrors || partsSalesPriceChange.PartsSalesPriceChangeDetails.Any(e => e.HasValidationErrors))
                return;

            var partsSalesPriceChangeDetails = partsSalesPriceChange.PartsSalesPriceChangeDetails.Where(r => r.IsUpsideDown == 1);
            if(partsSalesPriceChangeDetails != null)
                foreach(var item in partsSalesPriceChangeDetails)
                    UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_PriceChange, item.SparePartName));
            if(partsSalesPriceChange.PartsSalesPriceChangeDetails.Any(r => r.RetailGuidePrice > 0)) {
                var Details = partsSalesPriceChange.PartsSalesPriceChangeDetails.Where(r => r.RetailGuidePrice <= r.SalesPrice);
                foreach(var item in Details)
                    UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_RetailGuidePrice, item.SparePartName));
            }
            if(partsSalesPriceChange.PartsSalesPriceChangeDetails.Any(r => r.IncreaseRateSale > 0)) {
                var changeDetails = partsSalesPriceChange.PartsSalesPriceChangeDetails.Where(r => r.PurchasePrice > 0);
                var rates = changeDetails.Where(r => ((r.SalesPrice / r.PurchasePrice) < r.IncreaseRateSale));
                if(rates != null)
                    foreach(var rate in rates)
                        UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_IncreaseRateSale, rate.SparePartCode));
            }

            partsSalesPriceChange.PartsSalesCategoryId = 1;
            partsSalesPriceChange.PartsSalesCategoryCode = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Stop;
            partsSalesPriceChange.PartsSalesCategoryName = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Stop;
            ((IEditableObject)partsSalesPriceChange).EndEdit();
            if(partsSalesPriceChange.PartsSalesPriceChangeDetails.Any(r => r.IsUpsideDown == (int)DcsIsOrNot.是)) {
                DcsUtils.Confirm(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_IIsUpsideDown, this.save);
            } else {
                save();
            }
        }

        private void save() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            try {
                this.DomainContext.生成配件销售价变更申请(partsSalesPriceChange, partsSalesPriceChange.PartsSalesPriceChangeDetails.ToList(), loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    this.DomainContext.RejectChanges();
                    this.NotifyEditSubmitted();
                    this.OnCustomEditSubmitted();
                }, null);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }



        public new event EventHandler EditSubmitted;

        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }

        private ICommand exportFileCommand;

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private const string EXPORT_DATA_FILE_NAME = "导出配件销售价变更申请清单模板.xlsx";

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name= PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesPriceChange_PriceType,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name= PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_SalesPrice,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name= PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_RetailGuidePrice,
                    },
                    new ImportTemplateColumn {
                        Name= PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_DealerSalesPrice,
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark,
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }


        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("PartsSalesPriceChangeApplication"));

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DetailPanel_Title_SparePartDetails, null, () => this.PartsSalesPriceChangeApplicationForEditDataGridView);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.Uploader.ShowFileDialog())
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            this.RegisterButton(new ButtonItem {
                Command = new Sunlight.Silverlight.Core.Command.DelegateCommand(this.PartsPurchasePricing),
                Title =PartsSalesUIStrings.DataEdit_Title_PartsPurchasePricing,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/HistoryQuery.png", UriKind.Relative),
            }, true);

            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        private void PartsPurchasePricing() {
            if(this.partsSalesPriceChangeForEditDataGridView.SelectedEntities == null) {
                UIHelper.ShowNotification( PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_PurchasePrice);
                return;
            }
            this.RadQueryWindow.ShowDialog();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesPriceChangeWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

    }
}