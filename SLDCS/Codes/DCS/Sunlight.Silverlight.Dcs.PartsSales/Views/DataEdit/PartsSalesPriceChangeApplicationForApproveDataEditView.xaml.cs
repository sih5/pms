﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesPriceChangeApplicationForApproveDataEditView {
        public PartsSalesPriceChangeApplicationForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase partsSalesPriceChangeDetailForApproveDataGridView;

        private DataGridViewBase PartsSalesPriceChangeApplicationDetailForApproveDataGridView {
            get {
                if(this.partsSalesPriceChangeDetailForApproveDataGridView == null) {
                    this.partsSalesPriceChangeDetailForApproveDataGridView = DI.GetDataGridView("PartsSalesPriceChangeApplicationDetail");
                    this.partsSalesPriceChangeDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesPriceChangeDetailForApproveDataGridView;
            }
        }


        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDetailPanel("PartsSalesPriceChangeApplicationForApprove"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(PartsSalesUIStrings.DetailPanel_Title_SparePartDetails, null, () => this.PartsSalesPriceChangeApplicationDetailForApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void Reset() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange != null && this.DomainContext.PartsSalesPriceChanges.Contains(partsSalesPriceChange))
                this.DomainContext.PartsSalesPriceChanges.Detach(partsSalesPriceChange);
        }

        protected override void OnEditSubmitting() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            ((IEditableObject)partsSalesPriceChange).EndEdit();
            if(partsSalesPriceChange.PartsSalesPriceChangeDetails.Any(r => r.IsUpsideDown == (int)DcsIsOrNot.是)) {
                DcsUtils.Confirm(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_IIsUpsideDown, this.save);
            } else {
                save();
            }
        }

        private void save() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            try {
                if(partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.新建) {
                    if(partsSalesPriceChange.Can初审配件销售价格变更申请)
                        partsSalesPriceChange.初审配件销售价格变更申请();
                } else if(partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.初审通过) {
                    if(partsSalesPriceChange.Can终审配件销售价格变更申请)
                        partsSalesPriceChange.终审配件销售价格变更申请();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title {
            get {
                var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
                if(partsSalesPriceChange != null && partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.初审通过) {
                    return PartsSalesUIStrings.DataEditView_Title_PartsSalesPriceChangeForFinalApprove;
                } else {
                    return PartsSalesUIStrings.DataEditView_Title_PartsSalesPriceChangeForInitialApprove;
                }
            }
        }


        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesPriceChangeWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
    }
}
