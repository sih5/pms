﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesPriceChangeApplicationForFinalApproveDataEditView {
        public PartsSalesPriceChangeApplicationForFinalApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase partsSalesPriceChangeDetailForApproveDataGridView;

        private DataGridViewBase PartsSalesPriceChangeApplicationDetailForApproveDataGridView {
            get {
                if(this.partsSalesPriceChangeDetailForApproveDataGridView == null) {
                    this.partsSalesPriceChangeDetailForApproveDataGridView = DI.GetDataGridView("PartsSalesPriceChangeApplicationDetail");
                    this.partsSalesPriceChangeDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesPriceChangeDetailForApproveDataGridView;
            }
        }


        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDetailPanel("PartsSalesPriceChangeApplicationForApprove"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(PartsSalesUIStrings.DetailPanel_Title_SparePartDetails, null, () => this.PartsSalesPriceChangeApplicationDetailForApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.RegisterButton(new ButtonItem {
                Command = new Sunlight.Silverlight.Core.Command.DelegateCommand(this.RejecrCurrentData),
                Title = PartsSalesUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            }, true);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        private RadWindow rejectRadWindow;

        private RadWindow RejectRadWindow {
            get {
                if(this.rejectRadWindow == null) {
                    this.rejectRadWindow = new RadWindow();
                    this.rejectRadWindow.CanClose = false;
                    this.rejectRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.rejectRadWindow.Content = this.RejectDataEditView;
                    this.rejectRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.rejectRadWindow.Height = 200;
                    this.rejectRadWindow.Width = 400;
                    this.rejectRadWindow.Header = "";
                }
                return this.rejectRadWindow;
            }
        }

        private DataEditViewBase rejectDataEditView;

        private DataEditViewBase RejectDataEditView {
            get {
                if(this.rejectDataEditView == null) {
                    this.rejectDataEditView = DI.GetDataEditView("PartsSalesPriceChangeApplicationForReject");
                    this.rejectDataEditView.EditCancelled += RejectDataEditView_EditCancelled;
                    this.rejectDataEditView.EditSubmitted += RejectDataEditView_EditSubmitted;
                }
                return this.rejectDataEditView;
            }
        }

        private void RejectDataEditView_EditCancelled(object sender, EventArgs e) {
            this.RejectRadWindow.Close();
        }

        private void RejectDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.RejectRadWindow.Close();
            base.OnEditSubmitting();
        }


        //private void DataEditView_EditCancelled(object sender, EventArgs e) {
        //    this.SwitchViewTo(DATA_GRID_VIEW);
        //}
        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void Reset() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange != null && this.DomainContext.PartsSalesPriceChanges.Contains(partsSalesPriceChange))
                this.DomainContext.PartsSalesPriceChanges.Detach(partsSalesPriceChange);
        }

        protected override void OnEditSubmitting() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            ((IEditableObject)partsSalesPriceChange).EndEdit();
            if(partsSalesPriceChange.PartsSalesPriceChangeDetails.Any(r => r.IsUpsideDown == (int)DcsIsOrNot.是)) {
                DcsUtils.Confirm(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_IIsUpsideDown, this.save);
            } else {
                save();
            }
        }

        private void save() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            try {
                if(partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.初审通过 || partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.新建) {
                    if(partsSalesPriceChange.Can终审配件销售价格变更申请)
                        partsSalesPriceChange.终审配件销售价格变更申请();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        //驳回
        private void RejecrCurrentData() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            this.RejectDataEditView.SetObjectToEditById(partsSalesPriceChange.GetIdentity());
            this.RejectRadWindow.ShowDialog();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title {
            get {
                var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
                if(partsSalesPriceChange != null && partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.初审通过) {
                    return PartsSalesUIStrings.DataEditView_Title_PartsSalesPriceChangeForFinalApprove;
                } else {
                    return PartsSalesUIStrings.DataEditView_Title_PartsSalesPriceChangeForInitialApprove;
                }
            }
        }


        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesPriceChangeWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
    }
}
