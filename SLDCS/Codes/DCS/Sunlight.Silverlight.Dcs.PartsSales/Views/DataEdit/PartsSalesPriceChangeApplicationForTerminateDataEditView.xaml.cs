﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesPriceChangeApplicationForTerminateDataEditView {
        public PartsSalesPriceChangeApplicationForTerminateDataEditView() {
            InitializeComponent();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesPriceChangeByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        protected override void OnEditSubmitting() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            partsSalesPriceChange.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(partsSalesPriceChange.AbandonComment))
                partsSalesPriceChange.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_PartsSalesPriceChange_AnandonReason, new[] {
                    "RejectOpinion"
                }));
            if(partsSalesPriceChange.HasValidationErrors)
                return;
            try {
                if(partsSalesPriceChange.Can作废配件销售价格变更申请)
                    partsSalesPriceChange.作废配件销售价格变更申请();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();

        }


        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_PartsSalesPriceChangeApplicationForAnandon;
            }
        }

    }
}
