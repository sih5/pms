﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesPriceChangeDetailDataEditView {
        public PartsSalesPriceChangeDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private PartsSalePriceChangeFileUploadDataEditPanel fileUploadDataEditPanels;
        public PartsSalePriceChangeFileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsSalePriceChangeFileUploadDataEditPanel)DI.GetDataEditPanel("PartsSalePriceChangeFileUpload"));
            }
        }

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
                        if(partsSalesPriceChange == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPartsSalesPriceChangeDetailAsync(e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportPartsSalesPriceChangeDetailCompleted -= ExcelServiceClient_ImportPartsSalesPriceChangeDetailCompleted;
                        this.excelServiceClient.ImportPartsSalesPriceChangeDetailCompleted += ExcelServiceClient_ImportPartsSalesPriceChangeDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void ExcelServiceClient_ImportPartsSalesPriceChangeDetailCompleted(object sender, ImportPartsSalesPriceChangeDetailCompletedEventArgs e) {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            foreach(var detail in partsSalesPriceChange.PartsSalesPriceChangeDetails) {
                partsSalesPriceChange.PartsSalesPriceChangeDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    partsSalesPriceChange.PartsSalesPriceChangeDetails.Add(new PartsSalesPriceChangeDetail {
                        PriceType = data.PriceType,
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        CenterPrice = data.CenterPrice.HasValue ? decimal.Round((decimal)data.CenterPrice, 2) : data.CenterPrice,
                        //DealerSalesPrice = data.DealerSalesPrice.HasValue ? decimal.Round((decimal)data.DealerSalesPrice, 2) : data.DealerSalesPrice,
                        SalesPrice = decimal.Round(data.SalesPrice, 2),
                        RetailGuidePrice = data.RetailGuidePrice.HasValue ? decimal.Round((decimal)data.RetailGuidePrice, 2) : data.RetailGuidePrice,
                        Remark = data.Remark,
                        PriceTypeName = data.PriceTypeCode
                    });
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }
        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesPriceChange;
            }
        }
        private DataGridViewBase partsSalesPriceChangeForEditDataGridView;

        private DataGridViewBase PartsSalesPriceChangeForEditDataGridView {
            get {
                if(this.partsSalesPriceChangeForEditDataGridView == null) {
                    this.partsSalesPriceChangeForEditDataGridView = DI.GetDataGridView("PartsSalesPriceChangeForEdit");
                    this.partsSalesPriceChangeForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesPriceChangeForEditDataGridView;
            }
        }


        private ICommand exportFileCommand;
        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private const string EXPORT_DATA_FILE_NAME = "导出配件销售价变更申请清单模板.xlsx";

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                           new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                                            },
                                            new ImportTemplateColumn {
                                                Name= PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Centerprice,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Dealerprice,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark,
                                            },
                                            new ImportTemplateColumn {
                                                Name= PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesPriceChange_PriceType,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }
        private void CreateUI() {
            PartsSalesPriceChangeForDetailDataEditPanel dataEditPanel = (PartsSalesPriceChangeForDetailDataEditPanel)DI.GetDataEditPanel("PartsSalesPriceChangeForDetail");
            dataEditPanel.IsEnabled = false;
            this.LeftRoot.Children.Add(dataEditPanel);

            FileUploadDataEditPanels.FilePath = null;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 1);
            FileUploadDataEditPanels.Margin = new Thickness(75,0, 0, 0);
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.LeftRoot.Children.Add(FileUploadDataEditPanels);

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.IsEnabled = false;
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(PartsSalesUIStrings.DetailPanel_Title_SparePartDetails, null, () => this.PartsSalesPriceChangeForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesPriceChangeWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null) { 
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

    }
}
