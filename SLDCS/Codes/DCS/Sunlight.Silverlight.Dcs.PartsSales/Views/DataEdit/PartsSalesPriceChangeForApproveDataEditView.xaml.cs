﻿using System;
using System.Windows;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesPriceChangeForApproveDataEditView {
        public PartsSalesPriceChangeForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private PartsSalePriceChangeFileUploadDataEditPanel fileUploadDataEditPanels;
        public PartsSalePriceChangeFileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsSalePriceChangeFileUploadDataEditPanel)DI.GetDataEditPanel("PartsSalePriceChangeFileUpload"));
            }
        }
        private DataGridViewBase partsSalesPriceChangeDetailForApproveDataGridView;

        private DataGridViewBase PartsSalesPriceChangeDetailForApproveDataGridView {
            get {
                if(this.partsSalesPriceChangeDetailForApproveDataGridView == null) {
                    this.partsSalesPriceChangeDetailForApproveDataGridView = DI.GetDataGridView("PartsSalesPriceChangeDetailForApprove");
                    this.partsSalesPriceChangeDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesPriceChangeDetailForApproveDataGridView;
            }
        }


        private void CreateUI() {
            this.LeftRoot.Children.Add(DI.GetDetailPanel("PartsSalesPriceChangeForApprove"));

            FileUploadDataEditPanels.FilePath = null;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 1);
            FileUploadDataEditPanels.Margin = new Thickness(75,0, 0, 0);
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.LeftRoot.Children.Add(FileUploadDataEditPanels);

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(PartsSalesUIStrings.DetailPanel_Title_SparePartDetails, null, () => this.PartsSalesPriceChangeDetailForApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejecrCurrentData),
                Title = PartsSalesUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            },true);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void Reset() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange != null && this.DomainContext.PartsSalesPriceChanges.Contains(partsSalesPriceChange))
                this.DomainContext.PartsSalesPriceChanges.Detach(partsSalesPriceChange);
        }

        protected override void OnEditSubmitting() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            ((IEditableObject)partsSalesPriceChange).EndEdit();
            try {
                if(partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.提交) {
                    if(partsSalesPriceChange.Can初审配件销售价格变更申请)
                        partsSalesPriceChange.初审配件销售价格变更申请();
                } else if(partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.初审通过) {
                    if(partsSalesPriceChange.Can审核配件销售价格变更申请)
                        partsSalesPriceChange.审核配件销售价格变更申请();
                }else if(partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.审核通过) {
                    if(partsSalesPriceChange.Can终审配件销售价格变更申请)
                        partsSalesPriceChange.终审配件销售价格变更申请();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        //驳回
        private void RejecrCurrentData() {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null)
                return;
            ((IEditableObject)partsSalesPriceChange).EndEdit();
            try {
                if(partsSalesPriceChange.Can驳回配件销售价格变更申请)
                    partsSalesPriceChange.驳回配件销售价格变更申请();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title {
            get {
                var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
                if(partsSalesPriceChange != null && partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.初审通过) {
                    return "审核配件销售价变更申请";
                } else if(partsSalesPriceChange != null && partsSalesPriceChange.Status == (int)DcsPartsSalesPriceChangeStatus.审核通过){
                    return PartsSalesUIStrings.DataEditView_Title_PartsSalesPriceChangeForFinalApprove;
                } else {
                    return "初审配件销售价变更申请";
                }
            }
        }


        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesPriceChangeWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null) {
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
    }
}
