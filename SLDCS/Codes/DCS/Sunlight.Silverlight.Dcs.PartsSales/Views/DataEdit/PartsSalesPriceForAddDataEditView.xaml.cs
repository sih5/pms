﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesPriceForAddDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private ObservableCollection<PartsSalesCategory> partsSalesCategories;
        private ObservableCollection<PartsSalesPrice> collectionPartsSalesPrices;
        private DataGridViewBase partsSalesPriceForEditDataGridView;
        private int partsSalesCategoryId, priceType;
        private string partsSalesCategoryCode, partsSalesCategoryName, remark;
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly string[] kvNames = {
            "PartsSalesPrice_PriceType"
        };

        public PartsSalesPriceForAddDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PartsSalesPriceForAddDataEditView_DataContextChanged;
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private DataGridViewBase PartsSalesPriceForEditDataGridView {
            get {
                if(this.partsSalesPriceForEditDataGridView == null) {
                    this.partsSalesPriceForEditDataGridView = DI.GetDataGridView("PartsSalesPriceForEdit");
                    this.partsSalesPriceForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsSalesPriceForEditDataGridView.DataContext = this;
                }
                return this.partsSalesPriceForEditDataGridView;
            }
        }

        private void PartsSalesPriceForAddDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.ClearCache();
            this.PropertyChanged -= this.PartsSalesPriceForAddDataEditView_PropertyChanged;
            this.PropertyChanged += this.PartsSalesPriceForAddDataEditView_PropertyChanged;
        }

        private void PartsSalesPriceForAddDataEditView_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var dataEditView = sender as PartsSalesPriceForAddDataEditView;
            if(dataEditView == null)
                return;
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    var partsSalesCategory = this.PartsSalesCategories.SingleOrDefault(entity => entity.Id == dataEditView.PartsSalesCategoryId);
                    if(partsSalesCategory != null)
                        this.PartsSalesCategoryCode = partsSalesCategory.Code;
                    break;
            }
        }

        private void ClearCache() {
            this.PartsSalesCategoryId = default(int);
            this.PartsSalesCategoryName = string.Empty;
            this.PartsSalesCategoryCode = string.Empty;
            this.PriceType = default(int);
            this.Remark = string.Empty;
            this.CollectionPartsSalesPrices.Clear();
        }

        private void CreateUI() {
            this.PartsSalesCategories.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.PartsSalesCategories.Add(partsSalesCategory);
            }, null);
            this.root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.PartsSalesPrice_PartsPurchasePricingDetails, null, () => this.PartsSalesPriceForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.root.Children.Add(detailDataEditView);
            this.KeyValueManager.LoadData();
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsSalesPriceForEditDataGridView.CommitEdit())
                return;
            var validationErrors = new List<string>();
            if(this.PartsSalesCategoryId == default(int))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_PartsSalesCategoryNameIsNull);
            if(string.IsNullOrEmpty(this.PartsSalesCategoryName))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_PartsSalesCategoryNameIsNull);
            if(string.IsNullOrEmpty(this.partsSalesCategoryCode))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_PartsSalesCategoryCodeIsNull);
            if(this.PriceType < default(int))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_PriceTypeIsNull);
            if(!this.CollectionPartsSalesPrices.Any())
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_SparePartDetailsIsNull);
            if(this.CollectionPartsSalesPrices.GroupBy(v => v.SparePartId).Any(e => e.Count() > 1))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_SparePartIsRepeat);
            foreach(var partsSalesPrice in this.CollectionPartsSalesPrices) {
                partsSalesPrice.ValidationErrors.Clear();
                //if(partsSalesPrice.ValidationTime.CompareTo(partsSalesPrice.ExpireTime) > 0)
                //    partsSalesPrice.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_ExpireTimeEarlierThanValidationTime, new[] {
                //        "ValidationTime"
                //    }));
                if(partsSalesPrice.SalesPrice <= default(decimal))
                    partsSalesPrice.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_SalesPriceLessThanZero, new[] {
                        "SalesPrice"
                    }));
            }

            if(validationErrors.Any() || this.CollectionPartsSalesPrices.Any(e => e.HasValidationErrors)) {
                if(validationErrors.Any())
                    UIHelper.ShowNotification(string.Join(Environment.NewLine, validationErrors));
                return;
            }
            foreach(var partsSalesPrice in this.CollectionPartsSalesPrices) {
                partsSalesPrice.PartsSalesCategoryId = this.PartsSalesCategoryId;
                partsSalesPrice.PartsSalesCategoryCode = this.partsSalesCategoryCode;
                partsSalesPrice.PartsSalesCategoryName = this.PartsSalesCategoryName;
                ((IEditableObject)partsSalesPrice).EndEdit();
                if(!this.DomainContext.PartsSalesPrices.Contains(partsSalesPrice))
                    this.DomainContext.PartsSalesPrices.Add(partsSalesPrice);
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.ClearCache();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.ClearCache();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesPrice;
            }
        }

        public ObservableCollection<PartsSalesCategory> PartsSalesCategories {
            get {
                return this.partsSalesCategories ?? (this.partsSalesCategories = new ObservableCollection<PartsSalesCategory>());
            }
        }

        public ObservableCollection<PartsSalesPrice> CollectionPartsSalesPrices {
            get {
                return this.collectionPartsSalesPrices ?? (this.collectionPartsSalesPrices = new ObservableCollection<PartsSalesPrice>());
            }
        }

        public object KvPriceTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        public string PartsSalesCategoryCode {
            get {
                return this.partsSalesCategoryCode;
            }
            set {
                this.partsSalesCategoryCode = value;
                this.OnPropertyChanged("PartsSalesCategoryCode");
            }
        }

        public string Remark {
            get {
                return this.remark;
            }
            set {
                this.remark = value;
                this.OnPropertyChanged("Remark");
            }
        }

        public int PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }

        public int PriceType {
            get {
                return this.priceType;
            }
            set {
                this.priceType = value;
                this.OnPropertyChanged("PriceType");
            }
        }
    }
}
