﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesPriceForEditDataEditView {

        public PartsSalesPriceForEditDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsSalesPrice"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesPricesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsSalesPrice = this.DataContext as PartsSalesPrice;
            if(partsSalesPrice == null)
                return;
            partsSalesPrice.ValidationErrors.Clear();
            if(partsSalesPrice.PriceType < default(int))
                partsSalesPrice.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_PriceTypeIsNull, new[] {
                    "PriceType"
                }));
            //if(partsSalesPrice.ValidationTime.CompareTo(partsSalesPrice.ExpireTime) > 0)
            //    partsSalesPrice.ValidationErrors.Add(new ValidationResult(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_ExpireTimeEarlierThanValidationTime, partsSalesPrice.SparePartCode), new[] {
            //        "ValidationTime"
            //    }));
            if(partsSalesPrice.HasValidationErrors)
                return;
            ((IEditableObject)partsSalesPrice).EndEdit();
            try {
                if(EditState == DataEditState.Edit)
                    if(partsSalesPrice.Can修改配件销售价)
                        partsSalesPrice.修改配件销售价();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesPrice;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
