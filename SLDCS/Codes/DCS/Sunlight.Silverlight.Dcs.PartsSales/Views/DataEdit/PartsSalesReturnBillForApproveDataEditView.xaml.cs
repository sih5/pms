﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesReturnBillForApproveDataEditView {
        private DataGridViewBase partsSalesReturnBillDetailForApproveEdit;
        private RadWindow queryWindow;

        public PartsSalesReturnBillForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase PartsSalesReturnBillDetailForApproveEdit {
            get {
                if(this.partsSalesReturnBillDetailForApproveEdit == null) {
                    this.partsSalesReturnBillDetailForApproveEdit = DI.GetDataGridView("PartsSalesReturnBillDetailForApproveEdit");
                    this.partsSalesReturnBillDetailForApproveEdit.DomainContext = this.DomainContext;
                }
                return this.partsSalesReturnBillDetailForApproveEdit;
            }
        }

        private RadWindow PartsOutboundBillDetailQueryWindow {
            get {
                if(queryWindow == null) {
                    queryWindow = new RadWindow {
                        Content = MultiSelectQueryWindow,
                        Header = PartsSalesUIStrings.Button_Text_SearchOutboundOrderHistory
                    };
                }
                return this.queryWindow;
            }
        }

        private DcsMultiSelectQueryWindowBase multiSelectQueryWindow;

        private DcsMultiSelectQueryWindowBase MultiSelectQueryWindow {
            get {
                if(this.multiSelectQueryWindow == null) {
                    multiSelectQueryWindow = DI.GetQueryWindow("PartsOutboundBillDetailForPartsSalesOrder") as DcsMultiSelectQueryWindowBase;
                    if(multiSelectQueryWindow != null) {
                        multiSelectQueryWindow.QueryMode();
                        multiSelectQueryWindow.Loaded += PartsOutboundBillDetailQueryWindow_Loaded;
                    }
                }
                return multiSelectQueryWindow;
            }
        }
        private PartsSaleFileUploadDataEditPanel fileUploadDataEditPanels;
        protected PartsSaleFileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsSaleFileUploadDataEditPanel)DI.GetDataEditPanel("PartsSaleFileUpload"));
            }
        }
        private void PartsOutboundBillDetailQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindowBase = sender as DcsMultiDataQueryWindowBase;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(queryWindowBase == null || partsSalesReturnBill == null)
                return;
            var query = this.MultiSelectQueryWindow as PartsOutboundBillDetailForPartsSalesOrderQueryWindow;
            if(query != null) {
                query.SetQueryPanelParameters(partsSalesReturnBill.PartsSalesCategoryId);
            }
            queryWindowBase.ExchangeData(null, "SetAdditionalFilterItem", new object[] {
                new FilterItem("Id", typeof(int), FilterOperator.IsEqualTo, partsSalesReturnBill.PartsSalesOrderId)
            });
            queryWindowBase.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void CreateUI() {
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.Attachment.Children.Add(FileUploadDataEditPanels);
            this.Root.Children.Add(DI.GetDataEditPanel("PartsSalesReturnBillForApprove"));
            var queryWindowButton = new RadButton {
                Content = PartsSalesUIStrings.Button_Text_SearchOutboundOrderHistory,
                Command = new DelegateCommand(() => this.PartsOutboundBillDetailQueryWindow.ShowDialog())
            };
            queryWindowButton.HorizontalAlignment = HorizontalAlignment.Right;
            queryWindowButton.Height = 30;
            queryWindowButton.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(queryWindowButton);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title=PartsSalesUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            },true);

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesReturnBill), "PartsSalesReturnBillDetails"), null, () => this.PartsSalesReturnBillDetailForApproveEdit);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_UnStock,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = new DelegateCommand(this.ExportDetails)
            }, true);
            detailDataEditView.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        private void ExportDetails() {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            var parameteras = new Dictionary<string, object>();
            parameteras.Add("id", partsSalesReturnBill.Id);
            this.DomainContext.InvokeOperation(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_UnStockList, typeof(string), parameteras, false, invokeOp => {
                if(invokeOp.HasError) {
                    invokeOp.MarkErrorAsHandled();
                    UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                    return;
                }
                if(invokeOp.Value == null) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_Satisfy);
                }
                if(invokeOp.Value != null)
                    if(!string.IsNullOrEmpty(invokeOp.Value.ToString()))
                        HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(invokeOp.Value.ToString()));
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesReturnBillWithDetailsAndSalesUnitQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    if(entity.SalesUnit != null)
                        entity.PartsSalesCategoryId = entity.SalesUnit.PartsSalesCategoryId;
                    foreach(var item in entity.PartsSalesReturnBillDetails) {
                        item.ApproveQuantity = item.ReturnedQuantity;
                        entity.PartsSalesReturnBillDetails.Add(item);
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataGridView_Title_Approve_PartsSalesReturnBill;
            }
        }

        private void RejectCurrentData() {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if (partsSalesReturnBill == null)
                return;
            ((IEditableObject)partsSalesReturnBill).EndEdit();
            try {
                if (partsSalesReturnBill.Can审核不通过)
                    partsSalesReturnBill.审核不通过();
            } catch (ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsSalesReturnBillDetailForApproveEdit.CommitEdit())
                return;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            //if(partsSalesReturnBill.PartsSalesReturnBillDetails.Count > 200) {
            //    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_DetailCountMoreThan200);
            //    return;
            //}
            partsSalesReturnBill.ValidationErrors.Clear();
            if(!partsSalesReturnBill.WarehouseId.HasValue || partsSalesReturnBill.WarehouseId.GetValueOrDefault() == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_WarehouseIdIsNull);
                return;
            }
            if (null != partsSalesReturnBill.DiscountRate && (partsSalesReturnBill.DiscountRate > 1 || partsSalesReturnBill.DiscountRate<=0 ))
            {
                UIHelper.ShowNotification(String.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_DiscountRate));
                return;
            }
            if (partsSalesReturnBill.DiscountRate > 1 || partsSalesReturnBill.DiscountRate <= 0)
            {
                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_DiscountRate);
                return;
            }
            foreach(var detail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                detail.ValidationErrors.Clear();
                if(!detail.ApproveQuantity.HasValue)
                    detail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_ApproveQuantityIsNull, new[] {
                        "ApproveQuantity"
                    }));
                if(!detail.OriginalOrderPrice.HasValue)
                    detail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_OriginalOrderPriceIsNull, new[] {
                        "OriginalOrderPrice"
                    }));
                if(detail.ApproveQuantity > detail.ReturnedQuantity) {
                    UIHelper.ShowNotification(String.Format(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_ApproveQuantityReturnedQuantity, detail.SparePartCode));
                    return;
                }
                if(detail.ApproveQuantity < 0) {
                    UIHelper.ShowNotification(String.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_ApproveQuantity, detail.SparePartCode));
                    return;
                }
                if(!detail.HasValidationErrors)
                    ((IEditableObject)detail).EndEdit();


            }
            if(partsSalesReturnBill.HasValidationErrors || partsSalesReturnBill.PartsSalesReturnBillDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsSalesReturnBill).EndEdit();
            try {
                if(partsSalesReturnBill.Can审批配件销售退货单)
                    partsSalesReturnBill.审批配件销售退货单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
         protected override bool OnRequestCanSubmit()
        {
            return true;
        }
    }
}
