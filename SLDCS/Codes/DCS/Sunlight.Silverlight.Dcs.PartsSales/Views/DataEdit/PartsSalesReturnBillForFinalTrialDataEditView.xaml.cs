﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit
{
    public partial class PartsSalesReturnBillForFinalTrialDataEditView
    {
        private DataGridViewBase partsSalesReturnBillDetailForFirstTrialEdit;
        public PartsSalesReturnBillForFinalTrialDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private DataGridViewBase PartsSalesReturnBillDetailForFirstTrialEdit
        {
            get
            {
                if (this.partsSalesReturnBillDetailForFirstTrialEdit == null)
                {
                    this.partsSalesReturnBillDetailForFirstTrialEdit = DI.GetDataGridView("PartsSalesReturnBillDetailForUploadEdit");
                    this.partsSalesReturnBillDetailForFirstTrialEdit.DomainContext = this.DomainContext;
                }
                return this.partsSalesReturnBillDetailForFirstTrialEdit;
            }
        }

        private PartsSaleFileUploadDataEditPanel fileUploadDataEditPanels;
        protected PartsSaleFileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsSaleFileUploadDataEditPanel)DI.GetDataEditPanel("PartsSaleFileUpload"));
            }
        }

        private void CreateUI()
        {
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.Attachment.Children.Add(FileUploadDataEditPanels);
            this.Root.Children.Add(DI.GetDataEditPanel("PartsSalesReturnBillForTrial"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesReturnBill), "PartsSalesReturnBillDetails"), null, () => this.PartsSalesReturnBillDetailForFirstTrialEdit);
            detailDataEditView.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title=PartsSalesUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            },true);
        }

        private void RejectCurrentData() {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if (partsSalesReturnBill == null)
                return;
            ((IEditableObject)partsSalesReturnBill).EndEdit();
            try {
                if (partsSalesReturnBill.Can终审不通过)
                    partsSalesReturnBill.终审不通过();
            } catch (ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        private void LoadEntityToEdit(int id)
        {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesReturnBillWithDetailsAndSalesUnitQuery(id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null)
                {
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    if (entity.SalesUnit != null)
                        entity.PartsSalesCategoryId = entity.SalesUnit.PartsSalesCategoryId;
                    foreach (var item in entity.PartsSalesReturnBillDetails)
                    {
                        item.ApproveQuantity = item.ReturnedQuantity;
                        entity.PartsSalesReturnBillDetails.Add(item);
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        protected override string Title
        {
            get
            {
                return PartsSalesUIStrings.DataEdit_Title_PartsSalesReturnBillForFinalTrial;
            }
        }
        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting()
        {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if (partsSalesReturnBill == null)
                return;
            partsSalesReturnBill.ApprovalId = 1;
            if ((!partsSalesReturnBill.ApprovalId.Equals(1)) && (!partsSalesReturnBill.ApprovalId.Equals(0)))
            {
                partsSalesReturnBill.ApprovalId = 1;
            }
            if (partsSalesReturnBill.DiscountRate > 1 || partsSalesReturnBill.DiscountRate <= 0)
            {
                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_DiscountRate);
                return;
            }
            ((IEditableObject)partsSalesReturnBill).EndEdit();
            try
            {
                if (partsSalesReturnBill.ApprovalId.Equals(1))
                {
                    if (partsSalesReturnBill.Can销售订单退货终审)
                        partsSalesReturnBill.销售订单退货终审();
                }
                else
                {
                    if (partsSalesReturnBill.Can终审不通过)
                        partsSalesReturnBill.终审不通过();
                }

            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override bool OnRequestCanSubmit()
        {
            return true;
        }
    }
}