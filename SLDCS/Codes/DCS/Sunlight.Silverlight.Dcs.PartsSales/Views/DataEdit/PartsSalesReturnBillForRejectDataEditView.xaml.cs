﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesReturnBillForRejectDataEditView {
        public PartsSalesReturnBillForRejectDataEditView() {
            InitializeComponent();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesReturnBillsQueryByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            partsSalesReturnBill.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(partsSalesReturnBill.RejectComment))
                partsSalesReturnBill.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_RejectReason, new[] {
                    "RejectComment"
                }));

            if(partsSalesReturnBill.HasValidationErrors)
                return;

            try {
                if(partsSalesReturnBill.Can驳回配件销售退货单)
                    partsSalesReturnBill.驳回配件销售退货单();
                //todo:调用驳回方法
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_PartsSalesReturnBillForReject;
            }
        }

    }
}
