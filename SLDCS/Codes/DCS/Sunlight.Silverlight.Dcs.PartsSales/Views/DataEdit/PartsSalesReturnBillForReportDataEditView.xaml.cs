﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesReturnBillForReportDataEditView {
        private DataGridViewBase partsSalesReturnBillDetailForReportEditDataGridView;
        private RadUpload uploader;
        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected Action<string> UploadFileSuccessedProcessing;
        private Company companys;
        private readonly DcsDomainContext domainContext = new DcsDomainContext();

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    //this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
                        if(partsSalesReturnBill == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPartsSalesReturnBillDetailAsync(e.HandlerData.CustomData["Path"].ToString(), BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesReturnBill.ReturnWarehouseId, partsSalesReturnBill.ReturnType, partsSalesReturnBill.BranchId, partsSalesReturnBill.SalesUnitId, partsSalesReturnBill.PartsSalesCategoryId,companys.Type);
                        this.excelServiceClient.ImportPartsSalesReturnBillDetailCompleted -= this.ExcelServiceClient_ImportPartsSalesReturnBillDetailCompleted;
                        this.excelServiceClient.ImportPartsSalesReturnBillDetailCompleted += this.ExcelServiceClient_ImportPartsSalesReturnBillDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }
        private void ExcelServiceClient_ImportPartsSalesReturnBillDetailCompleted(object sender, ImportPartsSalesReturnBillDetailCompletedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            var detailList = partsSalesReturnBill.PartsSalesReturnBillDetails.ToList();
            foreach(var detail in detailList) {
                partsSalesReturnBill.PartsSalesReturnBillDetails.Remove(detail);
            }
            if (e.rightData != null && e.rightData.Length > 0) {
                foreach (var data in e.rightData) {
                    partsSalesReturnBill.PartsSalesReturnBillDetails.Add(new PartsSalesReturnBillDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        MeasureUnit = data.MeasureUnit,
                        ReturnedQuantity = data.ReturnedQuantity,
                        Remark = data.Remark,
                        OriginalOrderPrice = data.OriginalOrderPrice,
                        ReturnPrice = data.ReturnPrice,
                        PartsSalesOrderId = data.PartsSalesOrderId,
                        PartsSalesOrderCode = data.PartsSalesOrderCode,
                        OriginalPrice = data.OriginalPrice,
                        SIHLabelCode=data.SIHLabelCode,
                        TraceProperty = data.TraceProperty
                    });
                }
                partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(r => r.ReturnPrice * r.ReturnedQuantity);

                var partIds = partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.SparePartId).ToArray();
                var partsSalesOrderIds =  partsSalesReturnBill.PartsSalesReturnBillDetails.Select(r => r.PartsSalesOrderId).ToArray();
                if (partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货) {
                    domainContext.Load(domainContext.GetPartsSalesOrderByIdsQuery(partsSalesOrderIds), LoadBehavior.RefreshCurrent, loadOp => {
                        if (loadOp.HasError)
                            return;
                        var partsSalesOrders = loadOp.Entities;
                        domainContext.Load(domainContext.GetCenterSaleReturnStrategiesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.正常退货), LoadBehavior.RefreshCurrent, loadOp1 => {
                            if (loadOp1.HasError)
                                return;
                            var strategies = loadOp1.Entities;
                            if (strategies != null) {
                                foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                                    var partssalesorder = partsSalesOrders.Where(r => r.Id == item.PartsSalesOrderId).FirstOrDefault();
                                    if (partssalesorder != null) {
                                        var timeSlot = (DateTime.Now.Date - partssalesorder.SubmitTime.Value.Date).Days;
                                        var returnDays = strategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                                        if (returnDays != null && returnDays.DiscountRate != 0) {
                                            item.DiscountRate = returnDays.DiscountRate;
                                        }
                                        if (null != partsSalesReturnBill.DiscountRate) {
                                            item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                                        }
                                        if (item.DiscountRate != null)
                                            item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                                    }
                                }
                            }
                            partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(entity => entity.ReturnedQuantity * entity.ReturnPrice);
                        }, null);
                    }, null);

                } else if (partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货) {
                    int? warehouseId = 0;
                    if (this.companys.Type == (int)DcsCompanyType.代理库 || this.companys.Type == (int)DcsCompanyType.服务站兼代理库) {
                        warehouseId = partsSalesReturnBill.ReturnWarehouseId;
                    }
                    domainContext.Load(domainContext.GetCenterSaleReturnStrategiesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货), LoadBehavior.RefreshCurrent, loadOp => {
                        if (loadOp.HasError)
                            return;
                        var returnStrategies = loadOp.Entities;
                        domainContext.Load(domainContext.查询保底库存表信息2Query(companys.Id,warehouseId,partIds), LoadBehavior.RefreshCurrent, loadOp1 => {
                            if (loadOp1.HasError)
                                return;
                            var bottomStocks = loadOp1.Entities;
                            if (bottomStocks != null) {
                                foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                                    var bts = bottomStocks.Where(r => r.SparePartId == item.SparePartId).FirstOrDefault();
                                    if(bts != null && bts.Status == (int)DcsBaseDataStatus.作废) {
                                        var abandonTime = bts.EndTime;
                                        var timeSlot = (DateTime.Now.Date - abandonTime.Value.Date).Days;
                                        var returnDays = returnStrategies.Where(r => timeSlot > r.TimeFrom.Value && timeSlot <= r.TimeTo.Value).SingleOrDefault();
                                        if (returnDays != null && returnDays.DiscountRate != 0) {
                                            item.DiscountRate = returnDays.DiscountRate;
                                        }
                                        if (null != partsSalesReturnBill.DiscountRate) {
                                            item.ReturnPrice = Decimal.Parse((item.OriginalOrderPrice * (partsSalesReturnBill.DiscountRate)).ToString());
                                        }
                                        if (item.DiscountRate != null)
                                            item.ReturnPrice = Math.Round(item.ReturnPrice * Math.Round((decimal)item.DiscountRate, 2), 2);
                                    }
                                }
                            }
                            partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(entity => entity.ReturnedQuantity * entity.ReturnPrice);
                        }, null);
                    }, null);
                } else if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                    foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                        item.DiscountRate = 1;
                    }
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }


        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }


        public PartsSalesReturnBillForReportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsSalesReturnBillForReportDataEditView_DataContextChanged;
        }

        private void PartsSalesReturnBillForReportDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            foreach(var detail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                detail.PropertyChanged -= this.PartsSalesReturnBillDetail_PropertyChanged;
                detail.PropertyChanged += this.PartsSalesReturnBillDetail_PropertyChanged;
            }
            domainContext.Load(domainContext.GetCompaniesQuery().Where(ex => ex.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.FirstOrDefault();
                if(company == null)
                    return;
                this.companys = company;
            }, null);
            partsSalesReturnBill.PartsSalesReturnBillDetails.EntityAdded -= PartsSalesReturnBillDetails_EntityAdded;
            partsSalesReturnBill.PartsSalesReturnBillDetails.EntityAdded += PartsSalesReturnBillDetails_EntityAdded;
            partsSalesReturnBill.PartsSalesReturnBillDetails.EntityRemoved -= PartsSalesReturnBillDetails_EntityRemoved;
            partsSalesReturnBill.PartsSalesReturnBillDetails.EntityRemoved += PartsSalesReturnBillDetails_EntityRemoved;
        }

        private void PartsSalesReturnBillDetails_EntityAdded(object sender, EntityCollectionChangedEventArgs<PartsSalesReturnBillDetail> e) {
            e.Entity.PropertyChanged += this.PartsSalesReturnBillDetail_PropertyChanged;
        }

        private void PartsSalesReturnBillDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsSalesReturnBillDetail> e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(entity => entity.ReturnedQuantity * entity.ReturnPrice);
        }

        private void PartsSalesReturnBillDetail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            switch(e.PropertyName) {
                case "ReturnedQuantity":
                    partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(entity => entity.ReturnedQuantity * entity.ReturnPrice);
                    break;
            }
        }

        private DataGridViewBase PartsSalesReturnBillDetailForReportEditDataGridView {
            get {
                if(this.partsSalesReturnBillDetailForReportEditDataGridView == null) {
                    this.partsSalesReturnBillDetailForReportEditDataGridView = DI.GetDataGridView("PartsSalesReturnBillDetailForReportEdit");
                    this.partsSalesReturnBillDetailForReportEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesReturnBillDetailForReportEditDataGridView;
            }
        }

        private PartsSalesReturnBillForReportDataEditPanel dataEditPanel;
        private PartsSalesReturnBillForReportDataEditPanel DataEditPanel {
            get {
                return this.dataEditPanel ?? (this.dataEditPanel = (PartsSalesReturnBillForReportDataEditPanel)DI.GetDataEditPanel("PartsSalesReturnBillForReport"));
            }
        }

        private PartsSaleFileUploadDataEditPanel fileUploadDataEditPanels;
        public PartsSaleFileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsSaleFileUploadDataEditPanel)DI.GetDataEditPanel("PartsSaleFileUpload"));
            }
        }
        private void CreateUI() {
            this.Root.Children.Add(DataEditPanel);
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.showFileDialog)
            });
            dcsDetailGridView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            dcsDetailGridView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesReturnBill), "PartsSalesReturnBillDetails"), null, () => this.PartsSalesReturnBillDetailForReportEditDataGridView);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(dcsDetailGridView);
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 2);
            this.Attachment.Children.Add(FileUploadDataEditPanels);
        }
        private ICommand exportFileCommand;
        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        private const string EXPORT_DATA_FILE_NAME = "导出配件销售退货清单模板.xlsx";
        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ReturnedQuantity,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_SalesOrder,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark,
                    },
                    new ImportTemplateColumn {
                         Name="标签码",
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void showFileDialog() {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            if(partsSalesReturnBill.BranchId == default(int) || partsSalesReturnBill.SalesUnitId == default(int) || partsSalesReturnBill.ReturnType == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_Error);
                return;
            }
            if ((this.companys.Type == (int)DcsCompanyType.代理库 || this.companys.Type == (int)DcsCompanyType.服务站兼代理库) && partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturn_ReturnType.保底退货) {
                if (partsSalesReturnBill.ReturnWarehouseId == null || partsSalesReturnBill.ReturnWarehouseId == default(int)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditPanel_Validation_PartsSalesReturnBill_ReturnWarehouseIsNull);
                    return;
                }
            }
            this.Uploader.ShowFileDialog();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesReturnBillWithPartsSalesReturnBillDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.FileUploadDataEditPanels.FilePath = entity.Path;
                this.DomainContext.Load(this.DomainContext.GetWarehousesBySalesUnitId2Query(entity.SalesUnitId, BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
                        DataEditPanel.KvReturnWarehouse.Clear();
                        foreach(var warehouses in loadOp1.Entities) {
                            DataEditPanel.KvReturnWarehouse.Add(new KeyValuePair {
                                Key = warehouses.Id,
                                Value = warehouses.Name,
                                UserObject = warehouses
                            });
                        }
                    }
                    this.SetObjectToEdit(entity);
                }, null);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesReturnBillReport;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsSalesReturnBillDetailForReportEditDataGridView.CommitEdit())
                return;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            partsSalesReturnBill.ValidationErrors.Clear();
            partsSalesReturnBill.Path = FileUploadDataEditPanels.FilePath;
            foreach(var detail in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                detail.ValidationErrors.Clear();
            }
            if(companys.Type == (int)DcsCompanyType.代理库 || companys.Type == (int)DcsCompanyType.服务站兼代理库) {
                if(partsSalesReturnBill.ReturnWarehouseId == default(int) || partsSalesReturnBill.ReturnWarehouseId == null) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_ReturnWarehouseIsnull);
                    return;
                }
            } else if((companys.Type == (int)DcsCompanyType.服务站||companys.Type == (int)DcsCompanyType.集团企业) && partsSalesReturnBill.PartsSalesReturnBillDetails.Any(t => t.TraceProperty.HasValue && string.IsNullOrEmpty(t.SIHLabelCode))) {
                //UIHelper.ShowNotification("请填写标签码");
                //return;
            }
            if(partsSalesReturnBill.BranchId == default(int))
                partsSalesReturnBill.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_BranchIsNull, new[] {
                        "BranchName"
                    }));
            if(partsSalesReturnBill.SalesUnitId == default(int))
                partsSalesReturnBill.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_SalesUnitIsNull, new[] {
                        "SalesUnitName"
                    }));
            if(partsSalesReturnBill.ReturnType == default(int))
                partsSalesReturnBill.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_ReturnTypeIsNull, new[] {
                        "ReturnType"
                    }));
            if(string.IsNullOrEmpty(partsSalesReturnBill.ReturnReason))
                partsSalesReturnBill.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_ReturnReasonIsNull, new[] {
                        "ReturnReason"
                    }));
            if(partsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.特殊退货) {
                if(partsSalesReturnBill.PartsSalesOrderId == default(int))
                    partsSalesReturnBill.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_PartsSalesOrderIdIsNull, new[] {
                        "PartsSalesOrderId"
                    }));
            }
            if(partsSalesReturnBill.CustomerAccountId == default(int)) {
                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_CustomerAccountIsNull);
                return;
            }
            if(partsSalesReturnBill.DiscountRate > 1 || partsSalesReturnBill.DiscountRate <= 0) {
                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_DiscountRate);
                return;
            }
            if(!partsSalesReturnBill.PartsSalesReturnBillDetails.Any()) {
                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBill_PartsSalesReturnBillDetailsIsEmpty);
                return;
            }

            if(partsSalesReturnBill.PartsSalesReturnBillDetails.GroupBy(entity => new {
                entity.SparePartCode,
                entity.SparePartId,
                entity.PartsSalesOrderId
            }).Any(array => array.Count() > 1)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SameOrder);
                return;
            }
            if (partsSalesReturnBill.ReturnType != (int)DcsPartsSalesReturn_ReturnType.特殊退货) {
                foreach (var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                    if (item.DiscountRate == null || item.DiscountRate == 0) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_DiscountRateNew);
                        return;
                    }
                }
            }
            
            foreach(var item in partsSalesReturnBill.PartsSalesReturnBillDetails) {
                item.ValidationErrors.Clear();
                if(item.ReturnedQuantity <= 0)
                    item.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBillDetail_ReturnedQuantityLessThanZero, new[] {
                        "ReturnedQuantity"
                    }));
                if(item.SparePartId == default(int))
                    item.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBillDetail_SparePartIsNull, new[] {
                        "SparePartName"
                    }));
                if(!item.HasValidationErrors)
                    ((IEditableObject)item).EndEdit();
            }
            if(partsSalesReturnBill.HasValidationErrors || partsSalesReturnBill.PartsSalesReturnBillDetails.Any(detail => detail.HasValidationErrors))
                return;
            ((IEditableObject)partsSalesReturnBill).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(partsSalesReturnBill.Can生成配件销售退货单)
                        partsSalesReturnBill.生成配件销售退货单();
                } else {
                    if(partsSalesReturnBill.Can修改配件销售退货单)
                        partsSalesReturnBill.修改配件销售退货单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            var partsSalesReturnBills = this.DomainContext.PartsSalesReturnBills.ToArray();
            foreach(var partsSalesReturnBill in partsSalesReturnBills) {
                this.DomainContext.PartsSalesReturnBills.Detach(partsSalesReturnBill);
            }
            DataEditPanel.KvReturnWarehouse.Clear();
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            var partsSalesReturnBills = this.DomainContext.PartsSalesReturnBills.ToArray();
            foreach(var partsSalesReturnBill in partsSalesReturnBills) {
                this.DomainContext.PartsSalesReturnBills.Detach(partsSalesReturnBill);
            }
            DataEditPanel.KvReturnWarehouse.Clear();
            base.OnEditCancelled();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
