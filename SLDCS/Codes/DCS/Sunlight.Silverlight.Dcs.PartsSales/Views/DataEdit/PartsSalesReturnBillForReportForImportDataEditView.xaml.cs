﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesReturnBillForReportForImportDataEditView {
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出销售退货单清单模板.xlsx";
        private int companyType = 0;


        public PartsSalesReturnBillForReportForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(CreateUI);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_PartsSalesReturnBillForReportForImport;
            }
        }

        private void CreateUI() {

            this.ShowSaveButton();
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;

        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportPartsSalesReturnBillForReportAsync(fileName);
            this.ExcelServiceClient.ImportPartsSalesReturnBillForReportCompleted -= ExcelServiceClient_ImportPartsSalesReturnBillForReportCompleted;
            this.ExcelServiceClient.ImportPartsSalesReturnBillForReportCompleted += ExcelServiceClient_ImportPartsSalesReturnBillForReportCompleted;
        }

        private void ExcelServiceClient_ImportPartsSalesReturnBillForReportCompleted(object sender, ImportPartsSalesReturnBillForReportCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!HasImportingError) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImport, 10);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        //分公司编号，销售组织名称，退货类型，销售订单编号，开票要求，蓝字发票号，发运方式，退货原因，联系人，联系电话，备注，配件编号，配件名称，退货数量，清单备注
        //分公司编号，销售组织名称，退货类型，销售订单编号，退货原因，配件编号，退货数量 必须填写。
        private void InitializeCommand() {
            //服务站模板
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {

                this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError) {
                        if(!loadOption.IsErrorHandled)
                            loadOption.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOption);
                        return;
                    }
                    var entity = loadOption.Entities.FirstOrDefault();
                    if(entity != null) {
                        companyType = entity.Type;
                    }
                    if(companyType == (int)DcsCompanyType.服务站) {
                        var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_InvoiceReceiveCompanyCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_SalesUnitName2,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ReturnedType,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_Code,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_InvoiceRequirementNew
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_InvoiceBlue
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ShippingMethod
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnReason,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ContactPerson
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_ContactPhone
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ReturnedQuantity,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnPrice,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_DetailRemark
                    }
                };
                        this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                            }
                            if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                                this.ExportFile(loadOp.Value);
                        }, null);
                    } else if(companyType == (int)DcsCompanyType.服务站兼代理库 || companyType == (int)DcsCompanyType.代理库) {
                        var columnItemsValues = new List<ImportTemplateColumn> {
                   new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_InvoiceReceiveCompanyCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_SalesUnitName2,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ReturnedType,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_Code,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_InvoiceRequirementNew
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_InvoiceBlue
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ShippingMethod
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnReason,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ContactPerson
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_ContactPhone
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ReturnedQuantity,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnPrice,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_DetailRemark
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnWarehouseCode,
                        IsRequired = true
                    }
                };
                        this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                            }
                            if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                                this.ExportFile(loadOp.Value);
                        }, null);
                    }
                }, null);


            });


        }
        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;

            }
        }
    }
}