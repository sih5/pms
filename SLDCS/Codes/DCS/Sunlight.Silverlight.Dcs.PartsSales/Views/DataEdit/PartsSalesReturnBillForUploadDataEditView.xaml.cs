﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit
{
    public partial class PartsSalesReturnBillForUploadDataEditView 
    {
        private DataGridViewBase partsSalesReturnBillDetailForApproveEdit;
        public PartsSalesReturnBillForUploadDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private DataGridViewBase PartsSalesReturnBillDetailForApproveEdit
        {
            get
            {
                if (this.partsSalesReturnBillDetailForApproveEdit == null)
                {
                    this.partsSalesReturnBillDetailForApproveEdit = DI.GetDataGridView("PartsSalesReturnBillDetailForUploadEdit");
                    this.partsSalesReturnBillDetailForApproveEdit.DomainContext = this.DomainContext;
                }
                return this.partsSalesReturnBillDetailForApproveEdit;
            }
        }
       

        private void CreateUI()
        {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsSalesReturnBillForUpload"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesReturnBill), "PartsSalesReturnBillDetails"), null, () => this.PartsSalesReturnBillDetailForApproveEdit);
            detailDataEditView.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            this.Attachment.Children.Add(FileUploadDataEditPanels);
        }
        private void LoadEntityToEdit(int id)
        {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesReturnBillWithDetailsAndSalesUnitQuery(id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null)
                {
                    if (entity.SalesUnit != null)
                        entity.PartsSalesCategoryId = entity.SalesUnit.PartsSalesCategoryId;
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    foreach (var item in entity.PartsSalesReturnBillDetails)
                    {
                        item.ApproveQuantity = item.ReturnedQuantity;
                        entity.PartsSalesReturnBillDetails.Add(item);
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private PartsSaleFileUploadDataEditPanel fileUploadDataEditPanels;
        protected PartsSaleFileUploadDataEditPanel FileUploadDataEditPanels
        {
            get
            {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsSaleFileUploadDataEditPanel)DI.GetDataEditPanel("PartsSaleFileUpload"));
            }
        }
        protected override string Title
        {
            get
            {
                return PartsSalesUIStrings.DataEdit_Title_UploadFile;
            }
        }
        protected override void OnEditSubmitting()
        {
            if (!this.PartsSalesReturnBillDetailForApproveEdit.CommitEdit())
                return;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if (partsSalesReturnBill == null)
                return;
            partsSalesReturnBill.ValidationErrors.Clear();
            partsSalesReturnBill.Path = FileUploadDataEditPanels.FilePath;
            if (string.IsNullOrWhiteSpace(partsSalesReturnBill.Path))
            {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_UploadFile);
                return;
            }
            ((IEditableObject)partsSalesReturnBill).EndEdit();
            try
            {
                if (partsSalesReturnBill.Can上传销售退货单附件)
                    partsSalesReturnBill.上传销售退货单附件();
            }
            catch (Exception ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override bool OnRequestCanSubmit()
        {
            return true;
        }
    }
}
