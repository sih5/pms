﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using System.Collections.Generic;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesRtnSettlementDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvAccountGroups;
        public event PropertyChangedEventHandler PropertyChanged;
        private bool searchCanUse, sureCanUse;
        private ButtonItem saveBtn, returnBtn;
        private DateTime? cutoffTime = DateTime.Now.Date;
        private DataGridViewBase partsSalesRtnSettlementDetailForEditDataGridView;
        private DataGridViewBase partsSalesRtnSettlementRefForEditDataGridView;
        private ICommand searchCommand, sureCommand;
        private ObservableCollection<PartsSalesRtnSettlementRef> partsSalesRtnSettlementRefs = new ObservableCollection<PartsSalesRtnSettlementRef>();
        private ObservableCollection<PartsSalesRtnSettlementDetail> partsSalesRtnSettlementDetails = new ObservableCollection<PartsSalesRtnSettlementDetail>();
        private ObservableCollection<PartsSalesRtnSettlementRef> lPartsSalesRtnSettlementRefs = new ObservableCollection<PartsSalesRtnSettlementRef>();
        private DateTime? invoiceDate;
        private ObservableCollection<KeyValuePair> kvBusinessTypes;
        private ObservableCollection<KeyValuePair> kvSettleTypes;
        private List<PartsSalesOrderType> PartsSalesOrderTypes = new List<PartsSalesOrderType>();
        private int settleType, businessType;
        private readonly string[] kvNames = {
            "PartsPurchaseRtnSettleBill_InvoicePath","SalesOrderType_SettleType"
        };
        //public object KvSettleType
        //{
        //    get
        //    {
        //        return this.KeyValueManager[this.kvNames[1]];
        //    }
        //}
        public DateTime? InvoiceDate
        {
            get
            {
                return this.invoiceDate;
            }
            set
            {
                this.invoiceDate = value;
                this.OnPropertyChanged("InvoiceDate");
            }
        }
        public PartsSalesRtnSettlementDataEditView() {
            InitializeComponent();        
            this.Receipt.Visibility = Visibility.Collapsed;
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
            this.DataContextChanged += this.PartsSalesRtnSettlementDataEditView_DataContextChanged;
            this.PartsSalesRtnSettlementRefs.CollectionChanged += this.PartsSalesRtnSettlementRefs_CollectionChanged;

            this.KeyValueManager.LoadData();
          
          
        }
        public ObservableCollection<KeyValuePair> KvBusinessTypes
        {
            get
            {
                return this.kvBusinessTypes ?? (this.kvBusinessTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvSettleTypes
        {
            get
            {
                return this.kvSettleTypes ?? (this.kvSettleTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        void BusinessType_RadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesRtnSettlement;
            if(partsSalesSettlement == null)
                return;
            if((0 != businessType && null != partsSalesSettlement.BusinessType && businessType != partsSalesSettlement.BusinessType.Value) && partsSalesRtnSettlementRefs.Count()>0) {
                DcsUtils.Confirm(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_BusinessChange, () => {
                    partsSalesRtnSettlementRefs.Clear();
                    partsSalesRtnSettlementDetails.Clear();
                });
            }
            if(null != partsSalesSettlement.BusinessType) {
                businessType = partsSalesSettlement.BusinessType.Value;
            } else {
                businessType = 0;
            }
            if(0 == businessType || 0 == partsSalesSettlement.AccountGroupId) {
                this.SearchCanUse = false;
            } else {
                this.SearchCanUse = true;
            }
        }
        void SettleType_RadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesRtnSettlement;
            if(partsSalesSettlement == null)
                return;
            //清空业务类型
            KvBusinessTypes.Clear();
            this.PartsSalesOrderTypes.Clear();
            if (null!=partsSalesSettlement.SettleType)
            {
                settleType = partsSalesSettlement.SettleType.Value;
            }
            this.KvBusinessTypes.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypesQuery().Where(ex => ex.SettleType == partsSalesSettlement.SettleType), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var repairObject in loadOp.Entities)
                    this.PartsSalesOrderTypes.Add(new PartsSalesOrderType {
                        Id = repairObject.Id,
                        BusinessType = repairObject.BusinessType
                    });

                foreach(var item in PartsSalesOrderTypes) {

                    this.DomainContext.Load(this.DomainContext.GetKeyValueItemsQuery().Where(ex => ex.Name == "SalesOrderType_BusinessType" && ex.Key == item.BusinessType), LoadBehavior.RefreshCurrent, loadOps => {
                        if(null != loadOps) {
                            var values = KvBusinessTypes.Select(es => es.Key);
                            if(!values.Contains(item.BusinessType.Value))
                                KvBusinessTypes.Add(new KeyValuePair {
                                    Key = item.BusinessType.Value,
                                    Value = loadOps.Entities.First().Value
                                });
                            partsSalesSettlement.BusinessType = (int)DcsSalesOrderType_BusinessType.国内销售;
                        }
                    }, null);

                }
            }, null);
        }
        private void Initialize() {        
            this.KeyValueManager.LoadData(() => {
                if(BaseApp.Current.CurrentUserData.EnterpriseCode == "1101") {
                    KvInvoicePaths.Remove(this.keyValueManager[this.kvNames[0]].FirstOrDefault(r => r.Key == (int)DcsPartsPurchaseRtnSettleBillInvoicePath.反开销售发票));
                }
            });

            this.DomainContext.Load(this.DomainContext.GetKeyValueItemsQuery().Where(ex => ex.Name == "SalesOrderType_SettleType"), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var repairObject in loadOp.Entities)
                    this.KvSettleTypes.Add(new KeyValuePair
                    {
                        Key = repairObject.Key,
                        Value = repairObject.Value
                    });
                var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
                if (partsSalesRtnSettlement == null)
                    return;
                if (null == partsSalesRtnSettlement.SettleType)
                {
                    partsSalesRtnSettlement.SettleType = (int)DcsSalesOrderType_SettleType.销售结算;
                    partsSalesRtnSettlement.BusinessType = (int)DcsSalesOrderType_BusinessType.国内销售;
                }
            }, null);
        }

        private DataGridViewBase PartsSalesRtnSettlementRefForEditDataGridView {
            get {
                if(this.partsSalesRtnSettlementRefForEditDataGridView == null) {
                    this.partsSalesRtnSettlementRefForEditDataGridView = DI.GetDataGridView("PartsSalesRtnSettlementRefForEdit");
                    this.partsSalesRtnSettlementRefForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsSalesRtnSettlementRefForEditDataGridView.DataContext = this;
                }
                return this.partsSalesRtnSettlementRefForEditDataGridView;
            }
        }

        private DataGridViewBase PartsSalesRtnSettlementDetailForEditDataGridView {
            get {
                if(this.partsSalesRtnSettlementDetailForEditDataGridView == null) {
                    this.partsSalesRtnSettlementDetailForEditDataGridView = DI.GetDataGridView("PartsSalesRtnSettlementDetailForEdit");
                    this.partsSalesRtnSettlementDetailForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsSalesRtnSettlementDetailForEditDataGridView.DataContext = this;
                }
                return this.partsSalesRtnSettlementDetailForEditDataGridView;
            }
        }


        #region 界面事件

        //private void Spreadsheet() {
        //    var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
        //    if(partsSalesRtnSettlement == null)
        //        return;
            //if(partsSalesRtnSettlement.Discount == null && partsSalesRtnSettlement.DiscountRate == null)
            //    return;
            //var partsSalesRtnSettlementDetail = this.PartsSalesRtnSettlementDetails.Where(e => e.SparePartCode == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt);
            //if(partsSalesRtnSettlement.DiscountRate == 0 || partsSalesRtnSettlement.Discount == 0) {
            //    if(partsSalesRtnSettlementDetail == null || !partsSalesRtnSettlementDetail.Any())
            //        return;
            //    this.PartsSalesRtnSettlementDetails.Remove(partsSalesRtnSettlementDetail.Single());
            //} else {
            //    if(partsSalesRtnSettlementDetail.Any()) {
            //        var partsSalesRtnSettlementDeta = partsSalesRtnSettlementDetail.Single();
            //        partsSalesRtnSettlementDeta.SettlementPrice = partsSalesRtnSettlement.Discount != null ? (decimal)partsSalesRtnSettlement.Discount * -1 : (decimal)partsSalesRtnSettlement.DiscountRate * partsSalesRtnSettlement.TotalSettlementAmount * -1;
            //        partsSalesRtnSettlementDeta.SettlementAmount = partsSalesRtnSettlementDeta.SettlementPrice;
            //    } else {
            //        this.PartsSalesRtnSettlementDetails.Add(new PartsSalesRtnSettlementDetail {
            //            SerialNumber = (this.PartsSalesRtnSettlementDetails.Count + 1),
            //            SparePartCode = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt,
            //            SparePartName = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt,
            //            SettlementPrice = partsSalesRtnSettlement.Discount != null ? (decimal)partsSalesRtnSettlement.Discount * -1 : (decimal)partsSalesRtnSettlement.DiscountRate * partsSalesRtnSettlement.TotalSettlementAmount * -1,
            //            QuantityToSettle = 1,
            //            SettlementAmount = partsSalesRtnSettlement.Discount != null ? (decimal)partsSalesRtnSettlement.Discount * -1 : (decimal)partsSalesRtnSettlement.DiscountRate * partsSalesRtnSettlement.TotalSettlementAmount * -1
            //        });
            //    }
            //}
        //    partsSalesRtnSettlement.TotalSettlementAmount = this.PartsSalesRtnSettlementRefs.Sum(item => item.SettlementAmount);
        //   // partsSalesRtnSettlement.TotalSettlementAmount = partsSalesRtnSettlement.TotalSettlementAmount - (partsSalesRtnSettlement.Discount != null ? (decimal)partsSalesRtnSettlement.Discount : (decimal)partsSalesRtnSettlement.DiscountRate * partsSalesRtnSettlement.TotalSettlementAmount);
        //    partsSalesRtnSettlement.Tax = Math.Round((partsSalesRtnSettlement.TotalSettlementAmount / (decimal)(1 + partsSalesRtnSettlement.TaxRate)) * (decimal)partsSalesRtnSettlement.TaxRate, 2);
        //}

        private void PartsSalesRtnSettlementDetail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            var partsSalesRtnSettlementDetail = sender as PartsSalesRtnSettlementDetail;
            if(partsSalesRtnSettlement == null || partsSalesRtnSettlementDetail == null)
                return;
            switch(e.PropertyName) {
                case "SettlementPrice":
                    partsSalesRtnSettlementDetail.SettlementAmount = partsSalesRtnSettlementDetail.QuantityToSettle * partsSalesRtnSettlementDetail.SettlementPrice;
                    break;
                case "SettlementAmount":
                    //当清单的结算价格改变以后 改变主单对应的结算金额 和 税额
                    partsSalesRtnSettlement.TotalSettlementAmount = this.PartsSalesRtnSettlementDetails.Sum(item => item.SettlementAmount);
                    break;
            }
        }

        private void PartsSalesRtnSettlementRefs_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            if(partsSalesRtnSettlement == null)
                return;
            this.SureCanUse = this.PartsSalesRtnSettlementRefs.Any();
            partsSalesRtnSettlement.TotalSettlementAmount = PartsSalesRtnSettlementRefs.Sum(item => item.SettlementAmount);
        }

        //控制查询按钮启用
        private void PartsSalesRtnSettlement_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            if(partsSalesRtnSettlement == null)
                return;
            switch(e.PropertyName) {
                case "AccountGroupId":
                    partsSalesRtnSettlement.CustomerCompanyId = 0;
                    partsSalesRtnSettlement.CustomerCompanyCode = string.Empty;
                    partsSalesRtnSettlement.CustomerCompanyName = string.Empty;
                    partsSalesRtnSettlement.TotalSettlementAmount = 0;
                    partsSalesRtnSettlement.TaxRate = 0.13;
                    if(this.KeyValueManager[this.kvNames[0]].Count > 1)
                        partsSalesRtnSettlement.InvoicePath = (int)DcsPartsPurchaseRtnSettleBillInvoicePath.反开销售发票;
                    else
                        partsSalesRtnSettlement.InvoicePath = (int)DcsPartsPurchaseRtnSettleBillInvoicePath.开红字发票;
                    this.PartsSalesRtnSettlementDetails.Clear();
                    this.PartsSalesRtnSettlementRefs.Clear();
                    this.SearchCanUse = false;
                    this.CutoffTime = DateTime.Now.Date;
                    break;
                case "CustomerCompanyId":
                    if(partsSalesRtnSettlement.AccountGroupId != default(int) && partsSalesRtnSettlement.CustomerCompanyId != default(int)) {
                        // 根据 客户企业id  和账户组id 获取客户账户id
                        this.DomainContext.Load(this.DomainContext.GetCustomerAccountsQuery().Where(r => r.AccountGroupId == partsSalesRtnSettlement.AccountGroupId && r.CustomerCompanyId == partsSalesRtnSettlement.CustomerCompanyId && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            var entity = loadOp.Entities.SingleOrDefault();
                            if(entity == null) {
                                partsSalesRtnSettlement.CustomerAccountId = default(int);
                                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEditView_Validation_PartsSalesRtnSettlement_CustomerAccountIsNull);
                                return;
                            }
                            partsSalesRtnSettlement.CustomerAccountId = entity.Id;
                            partsSalesRtnSettlement.TotalSettlementAmount = 0;
                            this.PartsSalesRtnSettlementDetails.Clear();
                            this.PartsSalesRtnSettlementRefs.Clear();
                        }, null);
                    }
                    break;
                case "CutoffTime":
                    this.SearchCanUse = partsSalesRtnSettlement.AccountGroupId != 0 && !string.IsNullOrWhiteSpace(partsSalesRtnSettlement.CustomerCompanyCode) && this.CutoffTime.HasValue;
                    break;
                case "TaxRate":
                    if(partsSalesRtnSettlement.TaxRate > 1 || partsSalesRtnSettlement.TaxRate < 0) {
                        partsSalesRtnSettlement.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesRtnSettlement_TaxRateMustBetweenZeroAndOne, new[] {
                            "TaxRate"
                        }));
                        return;
                    }
                    partsSalesRtnSettlement.Tax = Math.Round((partsSalesRtnSettlement.TotalSettlementAmount / (decimal)(1 + partsSalesRtnSettlement.TaxRate)) * (decimal)partsSalesRtnSettlement.TaxRate, 2);
                    break;
                case "DiscountRate":
                    if(partsSalesRtnSettlement.DiscountRate < 0 || partsSalesRtnSettlement.DiscountRate > 1) {
                        partsSalesRtnSettlement.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesRtnSettlement_DiscountRateMustBetweenZeroAndOne, new[] {
                            "DiscountRate"
                        }));
                    }
                    break;
                case "Discount":
                    if(partsSalesRtnSettlement.Discount < 0) {
                        partsSalesRtnSettlement.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DiscountFee, new[] {
                            "Discount"
                        }));
                    }
                    break;
                case "TotalSettlementAmount":
                    if(partsSalesRtnSettlement.TaxRate <= 1 && partsSalesRtnSettlement.TaxRate >= 0)
                        partsSalesRtnSettlement.Tax = Math.Round((partsSalesRtnSettlement.TotalSettlementAmount / (decimal)(1 + partsSalesRtnSettlement.TaxRate)) * (decimal)partsSalesRtnSettlement.TaxRate, 2);
                    break;
            }
        }

        private void CustomerInformationForPartsSalesReturn_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var customerInformation = queryWindow.SelectedEntities.Cast<VirtualCustomerInformation>().FirstOrDefault();
            if(customerInformation == null)
                return;
            var partsPurchaseSettleBill = this.DataContext as PartsSalesRtnSettlement;
            if(partsPurchaseSettleBill == null)
                return;
            partsPurchaseSettleBill.CustomerCompanyId = customerInformation.CustomerCompanyId;
            partsPurchaseSettleBill.CustomerCompanyName = customerInformation.CustomerCompanyName;
            partsPurchaseSettleBill.CustomerCompanyCode = customerInformation.CustomerCompanyCode;
            if(partsPurchaseSettleBill.AccountGroupId != 0 && !string.IsNullOrWhiteSpace(partsPurchaseSettleBill.CustomerCompanyCode) && this.CutoffTime.HasValue && 0!=businessType) {
                
                this.SearchCanUse = true;
            } else {
                this.SearchCanUse = false;
            }
            //this.SearchCanUse = partsPurchaseSettleBill.AccountGroupId != 0 && !string.IsNullOrWhiteSpace(partsPurchaseSettleBill.CustomerCompanyCode) && this.CutoffTime.HasValue;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void CustomerInformationForPartsSalesReturnDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            var queryWindow = sender as QueryWindowBase;

            if(queryWindow == null || partsSalesRtnSettlement == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("AccountGroupId", typeof(int), FilterOperator.IsEqualTo, partsSalesRtnSettlement.AccountGroupId));
        }

        private void AccountGroupDcsComboBox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            var temp = sender as RadComboBox;
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            if(temp != null && partsSalesRtnSettlement != null) {
                var accountGroup = temp.SelectedItem as KeyValuePair;
                if(accountGroup != null)
                    partsSalesRtnSettlement.AccountGroupCode = ((AccountGroup)accountGroup.UserObject).Code;
            }

            this.DomainContext.Load(this.DomainContext.GetSalesUnitsQuery().Where(a => a.AccountGroupId == (int)this.DcsComboBoxAccountGroupId.SelectedValue && a.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                var nameEntity = loadOp.Entities.FirstOrDefault();
                if(nameEntity != null) {
                    var id = nameEntity.PartsSalesCategoryId;
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoryWithPartsSalesOrderTypesByIdQuery(id), op => {
                        if(op.HasError)
                            return;
                        var name = op.Entities.First();
                        if(name != null) {
                            this.DcsTextBoxPartsSalesCategoryName.Text = name.Name;
                            partsSalesRtnSettlement.PartsSalesCategoryId = name.Id;
                            partsSalesRtnSettlement.PartsSalesCategoryName = this.DcsTextBoxPartsSalesCategoryName.Text;
                        }
                    }, null);
                }
            }, null);
        }

        private void PartsSalesRtnSettlementDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if(!(this.DataContext is PartsSalesRtnSettlement))
                return;
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            this.SearchCanUse = partsSalesRtnSettlement.AccountGroupId != 0 && !string.IsNullOrWhiteSpace(partsSalesRtnSettlement.CustomerCompanyCode) && this.CutoffTime.HasValue;
            this.SureCanUse = this.PartsSalesRtnSettlementRefs.Any();
            this.Root.Visibility = Visibility.Visible;
            this.Receipt.Visibility = Visibility.Collapsed;
            partsSalesRtnSettlement.PropertyChanged -= this.PartsSalesRtnSettlement_PropertyChanged;
            partsSalesRtnSettlement.PropertyChanged += this.PartsSalesRtnSettlement_PropertyChanged;
        }

        #endregion

        private void OnPropertyChanged(string property) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(property));
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }


        private void CreateUI() {
            this.settleTypeRadCombox.SelectionChanged += this.SettleType_RadCombox_SelectionChanged;
            this.businessTypeRadCombox.SelectionChanged += this.BusinessType_RadCombox_SelectionChanged;
            this.HideSaveButton();
            var queryWindow = DI.GetQueryWindow("CustomerInformationForPartsSalesReturn");
            queryWindow.SelectionDecided += this.CustomerInformationForPartsSalesReturn_SelectionDecided;
            queryWindow.Loaded += this.CustomerInformationForPartsSalesReturnDropDownQueryWindow_Loaded;
            this.popupTextBoxPartsSupplierCode.PopupContent = queryWindow;
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesRtnSettlementRef), PartsSalesUIStrings.DataEditView_GroupTitle_PartsSalesRtnSettlementRef), null, this.PartsSalesRtnSettlementRefForEditDataGridView);
            detailEditView.SetValue(Grid.RowProperty, 1);
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            this.Root.Children.Add(detailEditView);

            var detailEditView2 = new DcsDetailDataEditView();
            detailEditView2.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesRtnSettlementDetail), PartsSalesUIStrings.DataEditView_GroupTitle_PartsSalesRtnSettlementDetail), null, this.PartsSalesRtnSettlementDetailForEditDataGridView);
            detailEditView2.SetValue(Grid.RowProperty, 1);
            detailEditView2.UnregisterButton(detailEditView2.InsertButton);
            detailEditView2.UnregisterButton(detailEditView2.DeleteButton);
            detailEditView2.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.Action_Title_ExportDetail,
                Icon = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportDetail.png"),
                Command = new DelegateCommand(this.ExportDetail)
            });
            detailEditView2.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.Action_Title_Merge,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new DelegateCommand(this.MergerDetail)
            });
            this.Receipt.Children.Add(detailEditView2);
            this.saveBtn = new ButtonItem {
                Command = new DelegateCommand(this.SaveCurrentData),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/save.png", UriKind.Relative),
                Title = PartsSalesUIStrings.Action_Title_Save
            };
            this.returnBtn = new ButtonItem {
                Command = new DelegateCommand(this.ReturnCurrentData),
                Title = PartsSalesUIStrings.Action_Title_Return,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/return.png", UriKind.Relative)
            };
            //var radButton = new RadButton {
            //    Content = PartsSalesUIStrings.Button_Text_Common_Spreadsheet,
            //    Command = new DelegateCommand(this.Spreadsheet)
            //};
            //radButton.SetValue(Grid.ColumnProperty, 2);
            //radButton.SetValue(Grid.RowProperty, 2);
            //this.ForAddButton.Children.Add(radButton);
            this.KvAccountGroups.Clear();
            this.DomainContext.Load(this.DomainContext.GetAccountGroupsQuery().Where(v => v.Status == (int)DcsMasterDataStatus.有效 && v.SalesCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var accountGroup in loadOp.Entities) {
                    this.KvAccountGroups.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name,
                        UserObject = accountGroup
                    });
                }
                this.DcsComboBoxAccountGroupId.SelectedIndex = 0;//默认品牌
            }, null);

        }

        private void ReturnCurrentData() {
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            if(partsSalesRtnSettlement == null)
                return;
            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            this.ShowCancelButton();
            this.Root.Visibility = Visibility.Visible;
            this.Receipt.Visibility = Visibility.Collapsed;
            this.PartsSalesRtnSettlementDetails.Clear();
            partsSalesRtnSettlement.TotalSettlementAmount = this.PartsSalesRtnSettlementRefs.Sum(item => item.SettlementAmount);
        }

        //保存
        private void SaveCurrentData() {
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            //保存之前试算一下
            if(partsSalesRtnSettlement == null)
                return;
            //var spreadsheet = this.txtDiscount.Value;
            //if(Convert.ToDecimal(spreadsheet) != 0)
            //    Spreadsheet();
            partsSalesRtnSettlement.TotalSettlementAmount = this.PartsSalesRtnSettlementRefs.Sum(item => item.SettlementAmount);
            partsSalesRtnSettlement.Tax = Math.Round((partsSalesRtnSettlement.TotalSettlementAmount / (decimal)(1 + partsSalesRtnSettlement.TaxRate)) * (decimal)partsSalesRtnSettlement.TaxRate, 2);
            if(!this.PartsSalesRtnSettlementDetailForEditDataGridView.CommitEdit())
                return;
            if(!this.PartsSalesRtnSettlementRefForEditDataGridView.CommitEdit())
                return;
            if (this.InvoiceDate.HasValue)
            {
                this.InvoiceDate = new DateTime(this.InvoiceDate.Value.Year, this.InvoiceDate.Value.Month, this.InvoiceDate.Value.Day, 00, 00, 00);
                partsSalesRtnSettlement.InvoiceDate = this.InvoiceDate.Value;
            } else if(!partsSalesRtnSettlement.InvoiceDate.HasValue)
            {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_InvoiceDate);
                return;
            }

            partsSalesRtnSettlement.ValidationErrors.Clear();
            if(partsSalesRtnSettlement.CustomerCompanyId == default(int))
                partsSalesRtnSettlement.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesRtnSettlement_CustomerCompanyIsNull, new[] {
                    "CustomerCompanyCode"
                }));
            if(partsSalesRtnSettlement.InvoicePath == default(int))
                partsSalesRtnSettlement.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesRtnSettlement_InvoicePathIsNull, new[] {
                    "InvoicePath"
                }));
            if((partsSalesRtnSettlement.TaxRate < 0 || partsSalesRtnSettlement.TaxRate > 1))
                partsSalesRtnSettlement.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesRtnSettlement_TaxRateMustBetweenZeroAndOne, new[] { "TaxRate" }));
            foreach(var item in PartsSalesRtnSettlementDetails) {
                item.ValidationErrors.Clear();
                if(!item.HasValidationErrors) {
                    ((IEditableObject)item).EndEdit();
                    if(!partsSalesRtnSettlement.PartsSalesRtnSettlementDetails.Contains(item))
                        partsSalesRtnSettlement.PartsSalesRtnSettlementDetails.Add(item);
                }
            }
            foreach(var item in PartsSalesRtnSettlementRefs) {
                item.ValidationErrors.Clear();
                if(!item.HasValidationErrors) {
                    ((IEditableObject)item).EndEdit();
                    if(!partsSalesRtnSettlement.PartsSalesRtnSettlementRefs.Contains(item))
                        partsSalesRtnSettlement.PartsSalesRtnSettlementRefs.Add(item);
                }
            }

            if(partsSalesRtnSettlement.CustomerAccountId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesRtnSettlement_CustomerAccountIsNull);
                return;
            }
            if(partsSalesRtnSettlement.HasValidationErrors || this.PartsSalesRtnSettlementRefs.Any(e => e.HasValidationErrors) || this.PartsSalesRtnSettlementDetails.Any(e => e.HasValidationErrors))
                return;
            //取消清单不允许超过200条的校验
            //if(PartsSalesRtnSettlementDetails.Count() > 200) {
            //    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_DetailCountMoreThan200);
            //    return;
            //}
            ((IEditableObject)partsSalesRtnSettlement).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(partsSalesRtnSettlement.Can生成配件销售退货结算单)
                        partsSalesRtnSettlement.生成配件销售退货结算单();
                } else {
                    if(partsSalesRtnSettlement.Can修改配件销售退货结算单)
                        partsSalesRtnSettlement.修改配件销售退货结算单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        //导出清单事件
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportDetail() {
            ShellViewModel.Current.IsBusy = true;
            var inboundIds = this.PartsSalesRtnSettlementRefs.Select(item => item.SourceId).ToArray();
            if(inboundIds.Length == 0)
                return;
            this.excelServiceClient.ExportPartsSalesRtnSettlementDetailByRefIdAsync(inboundIds);
            this.excelServiceClient.ExportPartsSalesRtnSettlementDetailByRefIdCompleted -= excelServiceClientOnExportPartsSalesRtnSettlementDetailByRefIdCompleted;
            this.excelServiceClient.ExportPartsSalesRtnSettlementDetailByRefIdCompleted += excelServiceClientOnExportPartsSalesRtnSettlementDetailByRefIdCompleted;
        }

        private void excelServiceClientOnExportPartsSalesRtnSettlementDetailByRefIdCompleted(object sender, ExportPartsSalesRtnSettlementDetailByRefIdCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        //清单合并事件
        private void MergerDetail() {
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            if(partsSalesRtnSettlement == null || !this.PartsSalesRtnSettlementDetails.Any())
                return;
            var mergeDetail = from detail in this.PartsSalesRtnSettlementDetails
                              group detail by new {
                                  detail.SparePartId,
                                  detail.SettlementPrice
                              }
                                  into g
                                  select new {
                                      g.Key.SparePartId,
                                      g.Key.SettlementPrice
                                  };
            var mergeCollection = new ObservableCollection<PartsSalesRtnSettlementDetail>();
            var serialNumber = 1;
            foreach(var detailItem in mergeDetail) {
                var item = detailItem;
                var details = this.PartsSalesRtnSettlementDetails.Where(e => e.SparePartId == item.SparePartId && e.SettlementPrice == item.SettlementPrice);
                var partsSalesRtnSettlementDetailArray = details as PartsSalesRtnSettlementDetail[] ?? details.ToArray();
                var partsSalesRtnSettlementDetail = new PartsSalesRtnSettlementDetail();
                mergeCollection.Add(partsSalesRtnSettlementDetail);
                partsSalesRtnSettlementDetail.PropertyChanged += this.PartsSalesRtnSettlementDetail_PropertyChanged;
                partsSalesRtnSettlementDetail.SerialNumber = serialNumber++;
                partsSalesRtnSettlementDetail.SparePartId = partsSalesRtnSettlementDetailArray.First().SparePartId;
                partsSalesRtnSettlementDetail.SparePartCode = partsSalesRtnSettlementDetailArray.First().SparePartCode;
                partsSalesRtnSettlementDetail.SparePartName = partsSalesRtnSettlementDetailArray.First().SparePartName;
                partsSalesRtnSettlementDetail.SettlementPrice = partsSalesRtnSettlementDetailArray.First().SettlementPrice;
                partsSalesRtnSettlementDetail.QuantityToSettle = partsSalesRtnSettlementDetailArray.Sum(e => e.QuantityToSettle);
                partsSalesRtnSettlementDetail.SettlementAmount = partsSalesRtnSettlementDetailArray.First().SettlementPrice * partsSalesRtnSettlementDetailArray.Sum(e => e.QuantityToSettle);
                partsSalesRtnSettlementDetail.Remark = partsSalesRtnSettlementDetailArray.First().Remark;
            }
            partsSalesRtnSettlement.TotalSettlementAmount = mergeCollection.Sum(e => e.SettlementAmount);
            this.PartsSalesRtnSettlementDetails = new ObservableCollection<PartsSalesRtnSettlementDetail>(mergeCollection.OrderBy(e => e.QuantityToSettle).ThenBy(e => e.SettlementAmount));
        }

        //确定事件
        private void Sure() {
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            if(partsSalesRtnSettlement == null)
                return;
            this.RegisterButton(saveBtn);
            this.RegisterButton(returnBtn);
            this.HideCancelButton();
            this.Root.Visibility = Visibility.Collapsed;
            this.Receipt.Visibility = Visibility.Visible;
            var inboundIds = this.PartsSalesRtnSettlementRefs.Select(item => item.SourceId).ToArray();
            if(inboundIds.Length == 0)
                return;
            this.PartsSalesRtnSettlementDetails.Clear();
            this.DomainContext.Load(this.DomainContext.查询待结算出入库明细清单Query(inboundIds, null, null), loadOp => {
                if(loadOp.HasError || loadOp.Entities == null)
                    return;
                var serialNumber = 1;
                foreach(var item in loadOp.Entities) {
                    var partsSalesRtnSettlementDetail = new PartsSalesRtnSettlementDetail();

                    partsSalesRtnSettlementDetail.PropertyChanged += this.PartsSalesRtnSettlementDetail_PropertyChanged;
                    partsSalesRtnSettlementDetail.SerialNumber = serialNumber++;
                    partsSalesRtnSettlementDetail.SparePartId = item.SparePartId;
                    partsSalesRtnSettlementDetail.SparePartName = item.SparePartName;
                    partsSalesRtnSettlementDetail.SparePartCode = item.SparePartCode;
                    partsSalesRtnSettlementDetail.SettlementPrice = item.SettlementPrice;
                    partsSalesRtnSettlementDetail.QuantityToSettle = item.Quantity;
                    partsSalesRtnSettlementDetail.SettlementAmount = item.SettlementAmount;
                    this.PartsSalesRtnSettlementDetails.Add(partsSalesRtnSettlementDetail);
                }
                partsSalesRtnSettlement.TotalSettlementAmount = this.PartsSalesRtnSettlementDetails.Sum(item => item.SettlementAmount);
            }, null);
        }

        //查询事件
        private void Search() {
            this.PartsSalesRtnSettlementRefs.Clear();
            foreach(var partsSalesRtnSettlementRef in this.lPartsSalesRtnSettlementRefs)
                this.PartsSalesRtnSettlementRefs.Add(partsSalesRtnSettlementRef);
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            if(partsSalesRtnSettlement == null)
                return;
            this.DomainContext.Load(this.DomainContext.查询待销售退货结算入库明细Query(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesRtnSettlement.CustomerCompanyId, null, partsSalesRtnSettlement.CustomerAccountId, partsSalesRtnSettlement.PartsSalesCategoryId, new[] { (int)DcsPartsInboundType.销售退货, (int)DcsPartsInboundType.质量件索赔, (int)DcsPartsInboundType.代发退货 }, this.CutoffTime, partsSalesRtnSettlement.SettleType, partsSalesRtnSettlement.BusinessType), loadOp => {
                if(loadOp.HasError || loadOp.Entities == null)
                    return;
                foreach(var item in loadOp.Entities.Distinct()) {
                    this.PartsSalesRtnSettlementRefs.Add(new PartsSalesRtnSettlementRef {
                        PartsSalesRtnSettlementId = partsSalesRtnSettlement.Id,
                        SourceId = item.BillId,
                        SourceCode = item.BillCode,
                        SettlementAmount = item.SettlementAmount,
                        SourceBillCreateTime = item.BillCreateTime,
                        WarehouseName = item.WarehouseName,
                        WarehouseId = item.WarehouseId,
                        ReturnReason = item.ReturnReason,
                        ReturnType = item.ReturnType,
                        PartsSalesReturnBillRemark = item.PartsSalesReturnBillRemark

                    });
                }
                partsSalesRtnSettlement.TotalSettlementAmount = this.PartsSalesRtnSettlementRefs.Sum(item => item.SettlementAmount);
                this.SureCanUse = this.PartsSalesRtnSettlementRefs.Any();
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.RadButtonSearch.Visibility = Visibility.Visible;
            this.RadButtondSure.Visibility = Visibility.Visible;
            this.DomainContext.Load(this.DomainContext.GetPartsSalesRtnSettlementsWithDetailsAndRefsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.PartsSalesRtnSettlementRefs.Clear();
                    this.PartsSalesRtnSettlementDetails.Clear();
                    foreach(var item in entity.PartsSalesRtnSettlementRefs) {
                        var partsSalesRtnSettlementRef = new PartsSalesRtnSettlementRef();
                        partsSalesRtnSettlementRef.PartsSalesRtnSettlementId = item.PartsSalesRtnSettlementId;
                        partsSalesRtnSettlementRef.SourceId = item.SourceId;
                        partsSalesRtnSettlementRef.SourceCode = item.SourceCode;
                        partsSalesRtnSettlementRef.SourceBillCreateTime = item.SourceBillCreateTime;
                        partsSalesRtnSettlementRef.WarehouseId = item.WarehouseId;
                        partsSalesRtnSettlementRef.WarehouseName = item.WarehouseName;
                        partsSalesRtnSettlementRef.SettlementAmount = item.SettlementAmount;
                        partsSalesRtnSettlementRef.ReturnReason = item.ReturnReason;
                        partsSalesRtnSettlementRef.PartsSalesReturnBillRemark = item.PartsSalesReturnBillRemark;
                        partsSalesRtnSettlementRef.ReturnType = item.ReturnType;
                        this.PartsSalesRtnSettlementRefs.Add(partsSalesRtnSettlementRef);
                        this.lPartsSalesRtnSettlementRefs.Add(partsSalesRtnSettlementRef);
                        entity.PartsSalesRtnSettlementRefs.Remove(item);
                    }
                    foreach(var item in entity.PartsSalesRtnSettlementDetails)
                        entity.PartsSalesRtnSettlementDetails.Remove(item);
                    this.SureCanUse = this.PartsSalesRtnSettlementRefs.Any();
                    this.SearchCanUse = true;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        protected override bool OnRequestCanSubmit()
        {
            this.DcsComboBoxAccountGroupId.SelectedIndex = 0;//默认品牌
            return true;
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            this.ShowCancelButton();
            this.Root.Visibility = Visibility.Visible;
            this.Receipt.Visibility = Visibility.Collapsed;
            this.PartsSalesRtnSettlementDetails.Clear();
            this.PartsSalesRtnSettlementRefs.Clear();
            this.lPartsSalesRtnSettlementRefs.Clear();
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            this.ShowCancelButton();
            this.Root.Visibility = Visibility.Visible;
            this.Receipt.Visibility = Visibility.Collapsed;
            this.PartsSalesRtnSettlementDetails.Clear();
            this.PartsSalesRtnSettlementRefs.Clear();
            this.lPartsSalesRtnSettlementRefs.Clear();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesRtnSettlement;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ICommand SearchCommand {
            get {
                return this.searchCommand ?? (this.searchCommand = new DelegateCommand(this.Search));
            }
        }

        public ICommand SureCommand {
            get {
                return this.sureCommand ?? (this.sureCommand = new DelegateCommand(this.Sure));
            }
        }

        //截止时间
        public DateTime? CutoffTime {
            get {
                return this.cutoffTime;
            }
            set {
                this.cutoffTime = value;
                this.OnPropertyChanged("CutoffTime");
            }
        }

        public bool SearchCanUse {
            get {
                return this.searchCanUse;
            }
            set {
                this.searchCanUse = value;
                this.OnPropertyChanged("SearchCanUse");
            }
        }

        public bool SureCanUse {
            get {
                return this.sureCanUse;
            }
            set {
                this.sureCanUse = value;
                this.OnPropertyChanged("SureCanUse");
            }
        }

        public ObservableCollection<KeyValuePair> KvInvoicePaths {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public ObservableCollection<PartsSalesRtnSettlementDetail> PartsSalesRtnSettlementDetails {
            get {
                return partsSalesRtnSettlementDetails ?? (this.partsSalesRtnSettlementDetails = new ObservableCollection<PartsSalesRtnSettlementDetail>());
            }
            set {
                partsSalesRtnSettlementDetails = value;
                this.OnPropertyChanged("PartsSalesRtnSettlementDetails");
            }
        }

        public ObservableCollection<PartsSalesRtnSettlementRef> PartsSalesRtnSettlementRefs {
            get {
                return partsSalesRtnSettlementRefs ?? (this.partsSalesRtnSettlementRefs = new ObservableCollection<PartsSalesRtnSettlementRef>());
            }
        }

        public ObservableCollection<KeyValuePair> KvAccountGroups {
            get {
                return this.kvAccountGroups ?? (this.kvAccountGroups = new ObservableCollection<KeyValuePair>());
            }
        }

        //private void TxtDiscountRate_OnValueChanged(object sender, RadRoutedEventArgs e) {
        //    if(this.txtDiscountRate.IsFocused) {
        //        var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
        //        if(partsSalesRtnSettlement == null)
        //            return;
        //        partsSalesRtnSettlement.Discount = null;
        //    }
        //}

        //private void TxtDiscount_OnValueChanged(object sender, RadRoutedEventArgs e) {
        //    if(this.txtDiscount.IsFocused) {
        //        var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
        //        if(partsSalesRtnSettlement == null)
        //            return;
        //        partsSalesRtnSettlement.DiscountRate = null;
        //    }
        //}

        protected override void Reset() {
            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            if(partsSalesRtnSettlement != null && this.DomainContext.PartsSalesRtnSettlements.Contains(partsSalesRtnSettlement))
                this.DomainContext.PartsSalesRtnSettlements.Detach(partsSalesRtnSettlement);
            base.Reset();
        }
    }
}