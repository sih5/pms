﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesRtnSettlementForInvoiceRegisterDataEditView {
        private ObservableCollection<InvoiceInformation> invoiceInformations;
        private DataGridViewBase invoiceInformationForSettleDataGridView;

        public PartsSalesRtnSettlementForInvoiceRegisterDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase InvoiceInformationForSettleDataGridView {
            get {
                if(this.invoiceInformationForSettleDataGridView == null) {
                    this.invoiceInformationForSettleDataGridView = DI.GetDataGridView("InvoiceInformationForPartsSalesRtnSettlement");
                    this.invoiceInformationForSettleDataGridView.DomainContext = this.DomainContext;
                    this.invoiceInformationForSettleDataGridView.DataContext = this;
                }
                return this.invoiceInformationForSettleDataGridView;
            }
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DataGridView_Title_InvoiceInformation, null, () => this.InvoiceInformationForSettleDataGridView);
            this.Root.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesRtnSettlementsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEditView_Title_InvoiceRegister_PartsSalesRtnSettlement;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.InvoiceInformationForSettleDataGridView.CommitEdit())
                return;

            var partsSalesRtnSettlement = this.DataContext as PartsSalesRtnSettlement;
            if(partsSalesRtnSettlement == null)
                return;
            var invoiceAmountTotal = this.InvoiceInformations.Sum(v => v.InvoiceAmount);

            if(InvoiceInformations.GroupBy(entity => entity.InvoiceNumber).Any(array => array.Count() > 1)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeRepeat);
                return;
            }
            foreach (var invoiceInformation in InvoiceInformations)
            {
                if (invoiceInformation.HasValidationErrors)
                {
                    invoiceInformation.ValidationErrors.Clear();
                }
            }
            foreach(var invoiceInformation in InvoiceInformations) {
                invoiceInformation.ValidationErrors.Clear();
                if(string.IsNullOrWhiteSpace(invoiceInformation.InvoiceCode))
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeIsNull, new[] {
                        "InvoiceCode"
                    }));
                if(string.IsNullOrWhiteSpace(invoiceInformation.InvoiceNumber))
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceNumberIsNull, new[] {
                        "InvoiceNumber"
                    }));
                if(!invoiceInformation.InvoiceAmount.HasValue) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceAmountIsNull, new[] {
                        "InvoiceAmount"
                    }));
                } else if(invoiceInformation.InvoiceAmount <= 0) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceAmountMustGreaterZero, new[] {
                        "InvoiceAmount"
                    }));
                }
                if(!invoiceInformation.TaxRate.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_TaxRateIsNull, new[] {
                        "TaxRate"
                    }));
                if(!invoiceInformation.InvoiceTax.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceTaxIsNull, new[] {
                        "InvoiceTax"
                    }));
                if(!invoiceInformation.InvoiceDate.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceDateIsNull, new[] {
                        "InvoiceDate"
                    }));
                if(!invoiceInformation.HasValidationErrors)
                    ((IEditableObject)invoiceInformation).EndEdit();

                this.DomainContext.InvoiceInformations.Clear();
                foreach (var invoice in InvoiceInformations) {
                    this.DomainContext.InvoiceInformations.Add(invoice);
                }
            }
            if(InvoiceInformations.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsSalesRtnSettlement).EndEdit();
            try {
                if(partsSalesRtnSettlement.Can配件销售退货结算单发票登记)
                    if(invoiceAmountTotal != null)
                        partsSalesRtnSettlement.配件销售退货结算单发票登记(invoiceAmountTotal.GetValueOrDefault());
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.InvoiceInformations.Clear();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.InvoiceInformations.Clear();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<InvoiceInformation> InvoiceInformations {
            get {
                return this.invoiceInformations ?? (this.invoiceInformations = new ObservableCollection<InvoiceInformation>());
            }
        }
    }
}
