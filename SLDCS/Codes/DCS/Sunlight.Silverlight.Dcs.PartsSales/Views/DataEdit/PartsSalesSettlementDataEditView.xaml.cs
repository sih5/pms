﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.MaskedInput;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesSettlementDataEditView : INotifyPropertyChanged {
        private PartsSalesSettlementDataTreeView partsSalesSettlementDataTreeView;
        private DataGridViewBase partsSalesSettlementRefForEditDataGridView;
        private DataGridViewBase partsSalesSettlementDetailForEditDataGridView;
        private ObservableCollection<PartsSalesSettlementDetail> PartsSalesSettlementDetails = new ObservableCollection<PartsSalesSettlementDetail>();
        private RadTreeViewItem lastNode;
        private bool searchCanUse, sureCanUse;
        private ButtonItem saveBtn, returnBtn;
       // private RadMaskedNumericInput txtSpreadsheet;
        private ObservableCollection<KeyValuePair> kvAccountGroups;
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private DateTime? cutOffTime = DateTime.Now;
        private DateTime? invoiceDate;
        private int accountGroupId;
        private int warehouseId;
        private ICommand searchCommand, sureCommand;
        private KeyValueManager keyValueManager;
        private decimal amortizedRebate;
        private decimal rebateBlance;
        private bool SalesSettleStrategy = false;
        private ObservableCollection<KeyValuePair> kvBusinessTypes;
        private ObservableCollection<KeyValuePair> kvIsOils;
        private List<PartsSalesOrderType> PartsSalesOrderTypes = new List<PartsSalesOrderType>();
        private int settleType, businessType;
        private readonly string[] kvNames = {
            "SalesOrderType_SettleType"
        };
        public object KvSettleType
        {
            get
            {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
       
        public ObservableCollection<KeyValuePair> KvBusinessTypes
        {
            get
            {
                return this.kvBusinessTypes ?? (this.kvBusinessTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvIsOils {
            get {
                return this.kvIsOils ?? (this.kvIsOils = new ObservableCollection<KeyValuePair>());
            }
        }
        void BusinessType_RadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if (partsSalesSettlement == null)
                return;
            if ((0 != businessType && null!=partsSalesSettlement.BusinessType&& businessType != partsSalesSettlement.BusinessType.Value)&& partsSalesSettlement.PartsSalesSettlementRefs.Any())
            {
                DcsUtils.Confirm(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_BusinessChange, () =>
                {
                    if (partsSalesSettlement.PartsSalesSettlementRefs.Any())
                        foreach (var item in partsSalesSettlement.PartsSalesSettlementRefs.ToArray())
                            partsSalesSettlement.PartsSalesSettlementRefs.Remove(item);
                });
            }
            if(null != partsSalesSettlement.BusinessType) {
                businessType = partsSalesSettlement.BusinessType.Value;
            } else {
                businessType = 0;
            }
            if(0 == businessType || 0 == partsSalesSettlement.AccountGroupId) {
                this.SearchCanUse = false;
            } else {
                this.SearchCanUse = true;
            }
        }
        void SettleType_RadCombox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null || null == partsSalesSettlement.SettleType)
                return;
            //清空业务类型
            KvBusinessTypes.Clear();          
            this.PartsSalesOrderTypes.Clear();           
            settleType = partsSalesSettlement.SettleType.Value;
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypesQuery().Where(ex => ex.SettleType == settleType), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var repairObject in loadOp.Entities)
                    this.PartsSalesOrderTypes.Add(new PartsSalesOrderType
                    {
                        Id = repairObject.Id,
                        BusinessType = repairObject.BusinessType
                    });

                foreach (var item in PartsSalesOrderTypes)
                {

                    this.DomainContext.Load(this.DomainContext.GetKeyValueItemsQuery().Where(ex => ex.Name == "SalesOrderType_BusinessType" && ex.Key == item.BusinessType), LoadBehavior.RefreshCurrent, loadOps =>
                    {
                        if (null != loadOps)
                        {
                            var values = KvBusinessTypes.Select(es => es.Key);
                            if (!values.Contains(item.BusinessType.Value))
                            KvBusinessTypes.Add(new KeyValuePair
                            {
                                Key = item.BusinessType.Value,
                                Value = loadOps.Entities.First().Value
                            });
                            partsSalesSettlement.BusinessType = (int)DcsSalesOrderType_BusinessType.国内销售;
                        }
                    }, null);     
                   
                }
            }, null);
 
        }
        private ObservableCollection<PartsSalesSettlementDetail> PartsPurchaseRtnSettleDetails {
            get {
                return this.PartsSalesSettlementDetails;
            }
        }

        private ObservableCollection<PartsSalesSettlementRef> partsSalesSettlementRefs;

        public ObservableCollection<PartsSalesSettlementRef> PartsSalesSettlementRefs {
            get {
                if(this.partsSalesSettlementRefs == null) {
                    this.partsSalesSettlementRefs = new ObservableCollection<PartsSalesSettlementRef>();
                }
                return partsSalesSettlementRefs;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public PartsSalesSettlementDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
            this.DataContextChanged += PartsSalesSettlementDataEditView_DataContextChanged;
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private DataGridViewBase PartsSalesSettlementRefForEditDataGridView {
            get {
                if(this.partsSalesSettlementRefForEditDataGridView == null) {
                    this.partsSalesSettlementRefForEditDataGridView = DI.GetDataGridView("PartsSalesSettlementRefForEdit");
                    this.partsSalesSettlementRefForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesSettlementRefForEditDataGridView;
            }
        }

        private DataGridViewBase PartsSalesSettlementDetailForEditDataGridView {
            get {
                if(this.partsSalesSettlementDetailForEditDataGridView == null) {
                    this.partsSalesSettlementDetailForEditDataGridView = DI.GetDataGridView("PartsSalesSettlementDetailForEdit");
                    this.partsSalesSettlementDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesSettlementDetailForEditDataGridView;
            }
        }

        private PartsSalesSettlementDataTreeView PartsSalesSettlementDataTreeView {
            get {
                if(this.partsSalesSettlementDataTreeView == null) {
                    this.partsSalesSettlementDataTreeView = new PartsSalesSettlementDataTreeView();
                    this.partsSalesSettlementDataTreeView.DomainContext = this.DomainContext;
                    //this.partsSalesSettlementDataTreeView.RefreshDataTree();
                    this.partsSalesSettlementDataTreeView.IsExpanderFirstNode = false;
                    this.partsSalesSettlementDataTreeView.OnTreeViewItemExpanded += this.PartsSalesSettlementDataTreeView_OnTreeViewItemExpanded;
                    this.partsSalesSettlementDataTreeView.OnTreeViewItemClick += this.PartsSalesSettlementDataTreeView_OnTreeViewItemClick;

                }
                return this.partsSalesSettlementDataTreeView;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesSettlementWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                if(entity.Id != default(int)) {
                    refDataEditView.UnregisterButton(this.BtnExportLiaison);
                }
                foreach(var item in entity.PartsSalesSettlementRefs) {
                    this.PartsSalesSettlementRefs.Add(item);
                }
                var serialNumber = 1;
                foreach(var detail in entity.PartsSalesSettlementRefs)
                    detail.SerialNumber = serialNumber++;
                foreach(var detail in entity.PartsSalesSettlementDetails.ToArray()) {
                    entity.PartsSalesSettlementDetails.Remove(detail);
                }
                this.SetObjectToEdit(entity);
                this.SearchCanUse = true;
                this.SureCanUse = entity.PartsSalesSettlementRefs.Any();
                this.gdTreeView.Visibility = Visibility.Collapsed;
                this.gdPartsSalesSettlementWithRef.Visibility = Visibility.Visible;
                this.gdPartsSalesSettlementWithDetail.Visibility = Visibility.Collapsed;
            }, null);
        }

        #region 界面事件

        #region PropertyChanged事件
        private void PartsSalesSettlementDetail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            var partsSalesSettlementDetail = sender as PartsSalesSettlementDetail;
            if(partsSalesSettlement == null || partsSalesSettlementDetail == null)
                return;
            switch(e.PropertyName) {
                case "SettlementAmount":
                    //当清单的结算价格改变以后 改变主单对应的结算金额 和 税额
                    partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(item => item.SettlementAmount);
                    break;
                case "SettlementPrice":
                    partsSalesSettlementDetail.SettlementAmount = partsSalesSettlementDetail.QuantityToSettle * partsSalesSettlementDetail.SettlementPrice;
                    break;
            }
        }

        private void partsSalesSettlement_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            switch(e.PropertyName) {
                case "TaxRate":
                    if(partsSalesSettlement.TaxRate > 1 || partsSalesSettlement.TaxRate < 0) {
                        partsSalesSettlement.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_TaxRateMustBetweenZeroAndOne, new[] {
                            "TaxRate"
                        }));
                        return;
                    }
                    partsSalesSettlement.Tax = Math.Round((partsSalesSettlement.TotalSettlementAmount / (decimal)(1 + partsSalesSettlement.TaxRate)) * (decimal)partsSalesSettlement.TaxRate, 2);
                    break;
                case "TotalSettlementAmount":
                    if(partsSalesSettlement.TaxRate <= 1 && partsSalesSettlement.TaxRate >= 0)
                        partsSalesSettlement.Tax = Math.Round((partsSalesSettlement.TotalSettlementAmount / (decimal)(1 + partsSalesSettlement.TaxRate)) * (decimal)partsSalesSettlement.TaxRate, 2);
                    if(partsSalesSettlement.PartsSalesSettlementRefs.Count > 0)
                        this.SureCanUse = true;
                    break;
            }
        }

        private void PartsSalesSettlementDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsSalesSettlementDetail> e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            partsSalesSettlement.PropertyChanged -= this.PartsSalesSettlementDetail_PropertyChanged;
            partsSalesSettlement.PropertyChanged += this.PartsSalesSettlementDetail_PropertyChanged;
        }

        private void PartsSalesSettlementRefs_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsSalesSettlementRef> e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementRefs.Sum(entity => entity.SettlementAmount);
            this.SureCanUse = partsSalesSettlement.PartsSalesSettlementRefs.Any();
        }
        #endregion

        #region 页面加载

        private void PartsSalesSettlementDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            partsSalesSettlement.PropertyChanged -= partsSalesSettlement_PropertyChanged;
            partsSalesSettlement.PropertyChanged += partsSalesSettlement_PropertyChanged;
            partsSalesSettlement.PartsSalesSettlementRefs.EntityRemoved -= PartsSalesSettlementRefs_EntityRemoved;
            partsSalesSettlement.PartsSalesSettlementRefs.EntityRemoved += PartsSalesSettlementRefs_EntityRemoved;
            partsSalesSettlement.PartsSalesSettlementDetails.EntityRemoved -= PartsSalesSettlementDetails_EntityRemoved;
            partsSalesSettlement.PartsSalesSettlementDetails.EntityRemoved += PartsSalesSettlementDetails_EntityRemoved;
            this.gdTreeView.Visibility = partsSalesSettlement.Id == default(int) ? Visibility.Visible : Visibility.Collapsed;
            this.gdPartsSalesSettlementWithRef.Visibility = Visibility.Visible;
            this.gdPartsSalesSettlementWithDetail.Visibility = Visibility.Collapsed;
        }

        private ButtonItem btnExportLiaison;
        private ButtonItem BtnExportLiaison {
            get {
                return this.btnExportLiaison ?? (this.btnExportLiaison = new ButtonItem {
                    Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ExportDetail,
                    Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/export.png", UriKind.Relative),
                    Command = new DelegateCommand(this.ExportLiaison)
                });
            }
        }

        private ButtonItem btnExportNoGoldenTaxClassify;
        private ButtonItem BtnExportNoGoldenTaxClassify {
            get {
                return this.btnExportNoGoldenTaxClassify ?? (this.btnExportNoGoldenTaxClassify = new ButtonItem {
                    Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_GoldenTaxClassify,
                    Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/export.png", UriKind.Relative),
                    Command = new DelegateCommand(this.ExportNoGoldenTaxClassify)
                });
            }
        }
        private readonly DcsDetailDataEditView refDataEditView = new DcsDetailDataEditView();
        private void CreateUI() {
            this.settleTypeRadCombox.SelectionChanged += this.SettleType_RadCombox_SelectionChanged;
            this.businessTypeRadCombox.SelectionChanged += this.BusinessType_RadCombox_SelectionChanged;
            // 树
            this.PartsSalesSettlementDataTreeView.SetValue(Grid.RowProperty, 4);
            this.PartsSalesSettlementDataTreeView.SetValue(Grid.ColumnProperty, 0);
            this.PartsSalesSettlementDataTreeView.SetValue(Grid.ColumnSpanProperty, 2);
            this.gdTreeView.Children.Add(this.PartsSalesSettlementDataTreeView);
            this.gdTreeView.Children.Add(this.CreateVerticalLine(2, 0, 0, 3));

            //  关联单
            //var refDataEditView = new DcsDetailDataEditView();
            refDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesSettlement), "PartsSalesSettlementRefs"), null, this.PartsSalesSettlementRefForEditDataGridView);
            refDataEditView.SetValue(Grid.RowProperty, 1);
            refDataEditView.SetValue(Grid.ColumnProperty, 0);
            refDataEditView.SetValue(Grid.ColumnSpanProperty, 3);
            refDataEditView.UnregisterButton(refDataEditView.InsertButton);
            this.gdPartsSalesSettlementWithRef.Children.Add(refDataEditView);
            refDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.Button_Text_Common_Returns,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new DelegateCommand(this.Returns)
            });
            refDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_Return,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new DelegateCommand(this.ReturnsComplete)
            });
            refDataEditView.RegisterButton(this.BtnExportLiaison);

            refDataEditView.RegisterButton(this.BtnExportNoGoldenTaxClassify);

            this.DCB_Warehouse.SelectionChanged += DCB_Warehouse_SelectionChanged;
            this.DCB_Warehouse.KeyDown -= DCB_Warehouse_KeyDown;
            this.DCB_Warehouse.KeyDown += DCB_Warehouse_KeyDown;
            // 清单
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesSettlement), "PartsSalesSettlementDetails"), null, this.PartsSalesSettlementDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnProperty, 0);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 2);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.Button_Text_Common_MergerLists,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new DelegateCommand(this.MergerDetail)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ExportDetailsNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = new DelegateCommand(this.ExportDetails)
            }, true);
            this.gdPartsSalesSettlementWithDetail.Children.Add(detailDataEditView);

            var testCalc = new Grid();
            testCalc.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            testCalc.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(150)
            });
            testCalc.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(35)
            });
            testCalc.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(20)
            });          
            testCalc.HorizontalAlignment = HorizontalAlignment.Center;
            testCalc.VerticalAlignment = VerticalAlignment.Top;
            testCalc.SetValue(Grid.RowProperty, 1);
            testCalc.SetValue(Grid.ColumnProperty, 0);
            testCalc.Margin = new Thickness(0, 5, 0, 0);
            this.gdPartsSalesSettlementWithDetail.Children.Add(testCalc);
            this.saveBtn = new ButtonItem {
                Command = new DelegateCommand(this.SaveCurrentData),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/confirm1.png", UriKind.Relative),
                Title = PartsSalesUIStrings.Action_Title_Save
            };
            this.returnBtn = new ButtonItem {
                Command = new DelegateCommand(this.ReturnCurrentData),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/return.png", UriKind.Relative),
                Title = PartsSalesUIStrings.Action_Title_Return,
            };

            this.DomainContext.Load(this.DomainContext.GetCompanyWithBranchIdsQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp1 => {
                if(loadOp1.HasError)
                    return;
                var company = loadOp1.Entities.SingleOrDefault();
                if(company == null)
                    return;             
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(entity => entity.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        this.DcsComboBoxPartsSalesCategoryName.ItemsSource = loadOp.Entities;
                        this.DcsComboBoxPartsSalesCategoryName.SelectedIndex = 0;//默认品牌
                    }, null);
            }, null);
            this.HideSaveButton();
            KvIsOils.Add(new KeyValuePair {
                Key = 0,
                Value = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_NotOil
            });
            KvIsOils.Add(new KeyValuePair {
                Key = 1,
                Value = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_Oil
            });
            this.Initialize();
        }

        private void DCB_Warehouse_KeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Space) {
                var dcsComboBox = sender as DcsComboBox;
                if(dcsComboBox == null)
                    return;
                var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
                if(partsSalesSettlement != null) {
                    dcsComboBox.SelectedValue = string.Empty;
                    partsSalesSettlement.WarehouseId = default(int);
                    this.WarehouseId = default(int);
                    txtWarehouseName.Text = string.Empty;
                }
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExportDetails() {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            var inboundTypes = partsSalesSettlement.PartsSalesSettlementRefs.Where(e => e.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单).Select(e => e.SourceId).ToArray();
            var outboundTypes = partsSalesSettlement.PartsSalesSettlementRefs.Where(e => e.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(e => e.SourceId).ToArray();
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsSalesSettlementDetailByRefIdAsync(inboundTypes, outboundTypes);
            this.excelServiceClient.ExportPartsSalesSettlementDetailByRefIdCompleted -= excelServiceClient_ExportPartsSalesSettlementDetailByRefIdCompleted;
            this.excelServiceClient.ExportPartsSalesSettlementDetailByRefIdCompleted += excelServiceClient_ExportPartsSalesSettlementDetailByRefIdCompleted;
        }

        private void excelServiceClient_ExportPartsSalesSettlementDetailByRefIdCompleted(object sender, ExportPartsSalesSettlementDetailByRefIdCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        //private void txtSpreadsheet_KeyDown(object sender, KeyEventArgs e) {
        //    if(e.Key == Key.Enter)
        //        this.Spreadsheet();
        //}

        private void DCB_Warehouse_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var warehouse = this.KvWarehouses.FirstOrDefault(v => v.Key == this.WarehouseId);
            if(warehouse == null)
                return;
            txtWarehouseName.Text = (warehouse.UserObject as Warehouse).Name;
        }

        //private void combbox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
        //    var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
        //    if(partsSalesSettlement == null)
        //        return;
        //    partsSalesSettlement.RebateMethod = (int?)this.combbox.SelectedValue;
        //    var partsSalesSettlementDetail = partsSalesSettlement.PartsSalesSettlementDetails.FirstOrDefault(ex => ex.SparePartCode == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt);
        //    //不返利时候  删除折扣商品数据 清除返利金额
        //    if(partsSalesSettlementDetail != null)
        //        partsSalesSettlement.PartsSalesSettlementDetails.Remove(partsSalesSettlementDetail);
        //    partsSalesSettlement.RebateAmount = null;
        //    txtSpreadsheet.Value = null;
        //    partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
        //}

        //退货未完成标记
        private void Returns() {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null || partsSalesSettlement.PartsSalesSettlementRefs == null || partsSalesSettlement.PartsSalesSettlementRefs.Count == 0)
                return;
            var sourceCode = partsSalesSettlement.PartsSalesSettlementRefs.Select(e => e.SourceCode).ToArray();
            this.DomainContext.Load(this.DomainContext.查询退货未完成销售出库单Query(sourceCode), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var partsOutboundBillids = (object[])loadOp.Entities.Select(e => e.Code.ToLower()).ToArray();
                if(partsOutboundBillids.Length > 0)
                    PartsSalesSettlementRefForEditDataGridView.ExchangeData(null, "Red", partsOutboundBillids);
            }, null);
        }

        //退货完成标记
        private void ReturnsComplete() {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            var dataGrigView = PartsSalesSettlementRefForEditDataGridView as PartsSalesSettlementRefForEditDataGridView;
            if(partsSalesSettlement == null || dataGrigView == null || partsSalesSettlement.PartsSalesSettlementRefs == null || partsSalesSettlement.PartsSalesSettlementRefs.Count == 0)
                return;
            var inBoundBillcodes = partsSalesSettlement.PartsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单).Select(e => e.SourceCode).ToArray();
            //var outBoundBillcodes = partsSalesSettlement.PartsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(e => e.SourceCode).ToArray();
            if(!inBoundBillcodes.Any())
                dataGrigView.ClearGridViewRowStyleSelector();
            this.DomainContext.Load(this.DomainContext.查询退货完成销售出入库单Query(null, inBoundBillcodes), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var BillCodes = (object[])loadOp.Entities.Select(e => e.BillCode.ToLower()).ToArray();
                if(BillCodes.Length > 0)
                    PartsSalesSettlementRefForEditDataGridView.ExchangeData(null, "ReturnComplete", BillCodes);
                else {
                    dataGrigView.ClearGridViewRowStyleSelector();
                }
            }, null);
        }

        private void Initialize() {
            this.KeyValueManager.LoadData();
            this.KvAccountGroups.Clear();
            this.KvWarehouses.Clear();
            //代码报错注释  缺少方法 20180309
            //this.DomainContext.Load(this.DomainContext.GetBranchstrategyWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError) {
            //        if(!loadOp.IsErrorHandled)
            //            loadOp.MarkErrorAsHandled();
            //        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
            //        return;
            //    }
            //    var item = loadOp.Entities.FirstOrDefault();
            //    if(item == null)
            //        return;
            //    SalesSettleStrategy = item.SalesSettleStrategy.HasValue ? item.SalesSettleStrategy.Value : false;
            //}, null);
            this.DomainContext.Load(this.DomainContext.GetAccountGroupsByOwnerCompanyIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var accountGroup in loadOp.Entities)
                    this.KvAccountGroups.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name,
                        UserObject = accountGroup
                    });
                this.DCB_refurbish.SelectedItem = 0;//默认品牌
            }, null);
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameWithBranchIdQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var warehouse in loadOp.Entities)
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
            }, null);
        }
        #endregion

        #region Button按钮

        //确定事件
        private void Selected() {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            //this.combbox.SelectionChanged -= combbox_SelectionChanged;
            //this.combbox.SelectedValue = partsSalesSettlement.RebateMethod;
            //this.combbox.SelectionChanged += combbox_SelectionChanged;
            var inboundTypes = partsSalesSettlement.PartsSalesSettlementRefs.Where(e => e.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件入库检验单).Select(e => e.SourceId).ToArray();
            var outboundTypes = partsSalesSettlement.PartsSalesSettlementRefs.Where(e => e.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(e => e.SourceId).ToArray();

            var warehouseName = partsSalesSettlement.PartsSalesSettlementRefs.Where(v => v.WarehouseName == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_WarehouseCar);
            if(warehouseName.Any()) {
                if(partsSalesSettlement.PartsSalesSettlementRefs.Count != warehouseName.Count()) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_FictitiousWarehouse);
                    return;
                }
            }

            if(partsSalesSettlement.PartsSalesCategoryName == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_WarehouseCarNew && warehouseName == null) {
                var count = partsSalesSettlement.PartsSalesSettlementRefs.Select(v => v.InvoiceCustomerName).Distinct();
                if(count.Count() > 1) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_InvoiceCustomerName);
                    return;
                }
            }

            if(partsSalesSettlement.PartsSalesSettlementDetails.Any()) {
                foreach(var item in partsSalesSettlement.PartsSalesSettlementDetails.ToArray())
                    partsSalesSettlement.PartsSalesSettlementDetails.Remove(item);
            }
            var currentUserData = BaseApp.Current.CurrentUserData;
            DomainContext.Load(DomainContext.GetBranchesQuery().Where(r => r.Id == currentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError)
                    if(!loadOp1.IsErrorHandled) {
                        loadOp1.MarkErrorAsHandled();
                        return;
                    }
                var branch = loadOp1.Entities.FirstOrDefault();

                DomainContext.Load(DomainContext.查询待结算出入库明细清单Query(inboundTypes, outboundTypes, null), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    if(branch != null) {
                        if(branch.Code == "1101" || branch.Code == "2450" || branch.Code == "2290") {
                            bool flag = loadOp.Entities.Any(r => string.IsNullOrEmpty(r.GoldenTaxClassifyCode));
                            if(flag) {
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_GoldenTaxClassifyCode);
                                return;
                            }
                        }
                    }
                    // 获取所有配件出入库信息
                    foreach(var outboundAndInboundDetail in loadOp.Entities) {
                        var partsSalesSettlementDetail = new PartsSalesSettlementDetail();
                        partsSalesSettlementDetail.SparePartId = outboundAndInboundDetail.SparePartId;
                        partsSalesSettlementDetail.SparePartCode = outboundAndInboundDetail.SparePartCode;
                        partsSalesSettlementDetail.SparePartName = outboundAndInboundDetail.SparePartName;
                        partsSalesSettlementDetail.SettlementPrice = outboundAndInboundDetail.SettlementPrice;
                        partsSalesSettlementDetail.PriceBeforeRebate = outboundAndInboundDetail.SettlementPrice;
                        partsSalesSettlementDetail.BillCode = outboundAndInboundDetail.BillCode;

                        if(outboundAndInboundDetail.BillType == (int)DcsPartsLogisticBatchBillDetailBillType.配件入库检验单) {
                            partsSalesSettlementDetail.QuantityToSettle = (-outboundAndInboundDetail.Quantity);
                            partsSalesSettlementDetail.SettlementAmount = (-outboundAndInboundDetail.SettlementAmount);
                        } else {
                            partsSalesSettlementDetail.QuantityToSettle = outboundAndInboundDetail.Quantity;
                            partsSalesSettlementDetail.SettlementAmount = outboundAndInboundDetail.SettlementAmount;
                        }
                        partsSalesSettlement.PartsSalesSettlementDetails.Add(partsSalesSettlementDetail);
                    }
                    var serialNumber = 1;
                    // 根据配件id、结算价 相同分组
                    var groupAmout = from amount in partsSalesSettlement.PartsSalesSettlementDetails
                                     group amount by new {
                                         amount.SparePartId,
                                         amount.PriceBeforeRebate,
                                         amount.BillCode
                                     }
                                         into groups
                                         select new {
                                             groups.Key.SparePartId,
                                             groups.Key.PriceBeforeRebate,
                                             groups.Key.BillCode
                                         };
                    // 合并配件id、结算价相同的配件
                    foreach(var group in groupAmout) {
                        var item = group;
                        var details = partsSalesSettlement.PartsSalesSettlementDetails.Where(ex => ex.PriceBeforeRebate == item.PriceBeforeRebate && ex.SparePartId == item.SparePartId && ex.BillCode==item.BillCode).ToArray();
                        var partsSalesSettlementDetail = new PartsSalesSettlementDetail();
                        partsSalesSettlementDetail.SerialNumber = serialNumber++;
                        partsSalesSettlementDetail.SparePartId = details.First().SparePartId;
                        partsSalesSettlementDetail.SparePartCode = details.First().SparePartCode;
                        partsSalesSettlementDetail.SparePartName = details.First().SparePartName;
                        partsSalesSettlementDetail.SettlementPrice = details.First().SettlementPrice;
                        partsSalesSettlementDetail.QuantityToSettle = details.Sum(ex => ex.QuantityToSettle);
                        partsSalesSettlementDetail.SettlementAmount = details.Sum(ex => ex.SettlementAmount);
                        partsSalesSettlementDetail.PriceBeforeRebate = details.First().PriceBeforeRebate;
                        partsSalesSettlementDetail.OriginalPrice = details.First().SettlementPrice;
                        partsSalesSettlementDetail.BillCode = details.First().BillCode;
                        partsSalesSettlement.PartsSalesSettlementDetails.Add(partsSalesSettlementDetail);
                    }
                    ////增加返利列
                    //if(partsSalesSettlement.RebateAmount != null && partsSalesSettlement.RebateAmount != 0) {
                    //    partsSalesSettlement.PartsSalesSettlementDetails.Add(new PartsSalesSettlementDetail {
                    //        SerialNumber = partsSalesSettlement.PartsSalesSettlementDetails.Any() ? (partsSalesSettlement.PartsSalesSettlementDetails.Max(x => x.SerialNumber) + 1) : 1,
                    //        SparePartCode = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt,
                    //        SparePartName = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt,
                    //        PriceBeforeRebate = 0,
                    //        DiscountedPrice = 0,
                    //        SettlementPrice = partsSalesSettlement.RebateAmount.Value * -1,
                    //        QuantityToSettle = 1,
                    //        SettlementAmount = partsSalesSettlement.RebateAmount.Value * -1
                    //    });
                    //}
                    // 删除合并后多余的配件信息
                    foreach(var item in partsSalesSettlement.PartsSalesSettlementDetails.Where(item => item.SerialNumber == 0).ToArray())
                        partsSalesSettlement.PartsSalesSettlementDetails.Remove(item);
                    partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
                    this.PartsPurchaseRtnSettleDetails.Clear();
                    foreach(var item in partsSalesSettlement.PartsSalesSettlementDetails) {
                        this.PartsPurchaseRtnSettleDetails.Add(item);
                    }
                    // 获取返利余额
                    DomainContext.Load(DomainContext.查询客户可用返利Query(partsSalesSettlement.AccountGroupId, partsSalesSettlement.CustomerCompanyId, partsSalesSettlement.Id == default(int) ? (int?)null : partsSalesSettlement.Id), LoadBehavior.RefreshCurrent, loadOperation => {
                        if(loadOperation.HasError) {
                            if(!loadOperation.IsErrorHandled)
                                loadOperation.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOperation);
                            return;
                        }
                        if(!loadOperation.Entities.Any())
                            return;
                        rebateBlance = loadOperation.Entities.First().UseablePosition;
                        if(partsSalesSettlement.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.返利池) {
                            partsSalesSettlement.RebateBalance = loadOperation.Entities.First().UseablePosition - (partsSalesSettlement.RebateAmount ?? 0);
                        } else {
                            partsSalesSettlement.RebateBalance = loadOperation.Entities.First().UseablePosition;
                        }
                        //根据分公司策略的销售结算返利比例增加返利商品
                        this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsSalesSettlement.SalesCompanyId), LoadBehavior.RefreshCurrent, loadBranchstrategy => {
                            if(loadBranchstrategy.HasError)
                                return;
                            var branchstrategy = loadBranchstrategy.Entities.SingleOrDefault();
                            if(branchstrategy != null && branchstrategy.ReturnFeeRate != null && partsSalesSettlement.RebateBalance != 0 && branchstrategy.ReturnFeeRate > 0 && branchstrategy.ReturnFeeRate <= 1) {
                                partsSalesSettlement.RebateMethod = (int)DcsPartsSalesSettlementRebateMethod.返利池;
                               // this.combbox.SelectedValue = partsSalesSettlement.RebateMethod;
                                var tempSpreadsheet = (partsSalesSettlement.TotalSettlementAmount * (decimal)branchstrategy.ReturnFeeRate > partsSalesSettlement.RebateBalance) ? partsSalesSettlement.RebateBalance : (partsSalesSettlement.TotalSettlementAmount * (decimal)branchstrategy.ReturnFeeRate);
                                //if(partsSalesSettlement.RebateBalance > 0) {
                                //    partsSalesSettlement.PartsSalesSettlementDetails.Add(new PartsSalesSettlementDetail {
                                //        SerialNumber = partsSalesSettlement.PartsSalesSettlementDetails.Any() ? (partsSalesSettlement.PartsSalesSettlementDetails.Max(x => x.SerialNumber) + 1) : 1,
                                //        SparePartCode = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt,
                                //        SparePartName = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt,
                                //        PriceBeforeRebate = 0,
                                //        DiscountedPrice = 0,
                                //        SettlementPrice = (partsSalesSettlement.TotalSettlementAmount * (decimal)branchstrategy.ReturnFeeRate > partsSalesSettlement.RebateBalance) ? -partsSalesSettlement.RebateBalance : (partsSalesSettlement.TotalSettlementAmount * (decimal)branchstrategy.ReturnFeeRate) * -1,
                                //        QuantityToSettle = 1,
                                //        SettlementAmount = (partsSalesSettlement.TotalSettlementAmount * (decimal)branchstrategy.ReturnFeeRate > partsSalesSettlement.RebateBalance) ? -partsSalesSettlement.RebateBalance : (partsSalesSettlement.TotalSettlementAmount * (decimal)branchstrategy.ReturnFeeRate) * -1,
                                //    });
                                //}
                                partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
                                if(Convert.ToDecimal(tempSpreadsheet) > rebateBlance) {
                                    tempSpreadsheet = partsSalesSettlement.RebateBalance;
                                    partsSalesSettlement.RebateAmount = tempSpreadsheet;
                                    partsSalesSettlement.RebateBalance = rebateBlance - tempSpreadsheet;
                                } else if(rebateBlance < 0) {
                                    partsSalesSettlement.RebateAmount = 0;
                                } else {
                                    partsSalesSettlement.RebateAmount = tempSpreadsheet;
                                    partsSalesSettlement.RebateBalance = rebateBlance - tempSpreadsheet;
                                }
                            }

                        }, null);
                    }, null);
                   // this.txtSpreadsheet.Value = null;
                    this.gdPartsSalesSettlementWithDetail.Visibility = Visibility.Visible;
                    this.gdPartsSalesSettlementWithRef.Visibility = Visibility.Collapsed;
                    this.gdTreeView.Visibility = Visibility.Collapsed;
                    this.RegisterButton(saveBtn);
                    this.RegisterButton(returnBtn);
                    this.HideCancelButton();

                    this.设置虚拟运费清单(partsSalesSettlement);

                }, null);
            }, null);
        }


        private void 设置虚拟运费清单(PartsSalesSettlement partsSalesSettlement) {
            var billCodes = partsSalesSettlement.PartsSalesSettlementRefs.Where(r => r.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单).Select(r => r.SourceCode).ToArray();
            //获取电商运费处理单 生成虚拟配件清单
            this.DomainContext.Load(this.DomainContext.GetEcommerceFreightDisposalByPartsOutboundBillCodeQuery(billCodes), LoadBehavior.RefreshCurrent, loadEnity => {
                if(loadEnity.HasError)
                    return;
                var partsSalesSettlementDetail = partsSalesSettlement.PartsSalesSettlementDetails.SingleOrDefault(r => r.SparePartCode == "XNPJYF");
                var ecommerceFreightDisposals = loadEnity.Entities.ToArray();
                if(ecommerceFreightDisposals.Any()) {
                    this.DomainContext.Load(this.DomainContext.GetSparePartsQuery().Where(r => r.Code == "XNPJYF" && r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOption => {
                        if(loadOption.HasError)
                            return;
                        var sparePart = loadOption.Entities.SingleOrDefault();
                        if(sparePart == null) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_XNPJYF);
                            return;
                        }
                        //生成虚拟配件清单
                        if(partsSalesSettlementDetail == null) {
                            partsSalesSettlement.PartsSalesSettlementDetails.Add(new PartsSalesSettlementDetail {
                                SerialNumber = partsSalesSettlement.PartsSalesSettlementDetails.Any() ? (partsSalesSettlement.PartsSalesSettlementDetails.Max(x => x.SerialNumber) + 1) : 1,
                                SparePartCode = sparePart.Code,
                                SparePartId = sparePart.Id,
                                SparePartName = sparePart.Name,
                                PriceBeforeRebate = 0,
                                DiscountedPrice = 0,
                                SettlementPrice = ecommerceFreightDisposals.Sum(r => r.FreightCharges ?? 0),
                                QuantityToSettle = 1,
                                SettlementAmount = ecommerceFreightDisposals.Sum(r => r.FreightCharges ?? 0),
                            });
                        } else {
                            partsSalesSettlementDetail.SettlementPrice = ecommerceFreightDisposals.Sum(r => r.FreightCharges ?? 0);
                            partsSalesSettlementDetail.SettlementAmount = ecommerceFreightDisposals.Sum(r => r.FreightCharges ?? 0);
                        }
                        //重新计算一次总金额
                        partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
                    }, null);
                } 
                //else {
                //    if(partsSalesSettlementDetail != null)
                //        partsSalesSettlement.PartsSalesSettlementDetails.Remove(partsSalesSettlementDetail);
                //    //重新计算一次总金额
                //    partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
                //}
            }, null);
        }

        //导出关联单
        private void ExportLiaison() {
            //if(this.PartsSalesSettlementRefForEditDataGridView.Entities == null){
            //    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_Settlement);
            //    return;
            //}
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            var enterpriseId = BaseApp.Current.CurrentUserData.EnterpriseId;
            var customerCompanyId = partsSalesSettlement.CustomerCompanyId;
            int? selectWarehouseId;
            var warehouse = this.KvWarehouses.FirstOrDefault(v => v.Key == this.WarehouseId);
            if(warehouse == null)
                selectWarehouseId = null;
            else
                selectWarehouseId = (warehouse.UserObject as Warehouse).Id;
            int[] inboundTypes = {
                (int)DcsPartsInboundType.销售退货
            };

            int[] outboundTypes = {
                (int)DcsPartsOutboundType.配件销售
            };
            if(this.CutOffTime.HasValue)
                this.CutOffTime = new DateTime(this.CutOffTime.Value.Year, this.CutOffTime.Value.Month, this.CutOffTime.Value.Day, 23, 59, 59);
            var customerAccountId = partsSalesSettlement.CustomerAccountId;
            var partsSalesCategoryId = partsSalesSettlement.PartsSalesCategoryId;
            if(this.PartsSalesSettlementRefs == null || this.partsSalesSettlementRefs.Count() == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_Settlement);
                return;
            }
            bool? isIol = false;
            if(partsSalesSettlement.IsOil.HasValue) {
                if(partsSalesSettlement.IsOil.Value == 1) {
                    isIol = true;
                }
            } else {
                isIol = null;
            }
            this.DomainContext.ExportSalesSettlementOutboundAndInboundBillDetail(enterpriseId, customerCompanyId, selectWarehouseId, customerAccountId, partsSalesCategoryId, inboundTypes, outboundTypes, CutOffTime, partsSalesSettlement.SettleType, partsSalesSettlement.BusinessType, isIol, loadOp =>
            {
                if(loadOp.HasError)
                    return;
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                }
                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                }
            }, null);
        }
        //导出关联单
        private void ExportNoGoldenTaxClassify() {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            var enterpriseId = BaseApp.Current.CurrentUserData.EnterpriseId;
            var customerCompanyId = partsSalesSettlement.CustomerCompanyId;
            int? selectWarehouseId;
            var warehouse = this.KvWarehouses.FirstOrDefault(v => v.Key == this.WarehouseId);
            if(warehouse == null)
                selectWarehouseId = null;
            else
                selectWarehouseId = (warehouse.UserObject as Warehouse).Id;
            int[] inboundTypes = {
                (int)DcsPartsInboundType.销售退货
            };

            int[] outboundTypes = {
                (int)DcsPartsOutboundType.配件销售
            };
            if(this.CutOffTime.HasValue)
                this.CutOffTime = new DateTime(this.CutOffTime.Value.Year, this.CutOffTime.Value.Month, this.CutOffTime.Value.Day, 23, 59, 59);
            var customerAccountId = partsSalesSettlement.CustomerAccountId;
            var partsSalesCategoryId = partsSalesSettlement.PartsSalesCategoryId;
            if(this.PartsSalesSettlementRefs == null || this.partsSalesSettlementRefs.Count() == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_Settlement);
                return;
            }
            this.DomainContext.ExportSalesSettlementOutboundAndInboundBillDetailWithGoldenTaxClassify(enterpriseId, customerCompanyId, selectWarehouseId, customerAccountId, partsSalesCategoryId, inboundTypes, outboundTypes, CutOffTime, partsSalesSettlement.SettleType, partsSalesSettlement.BusinessType, loadOp =>
            {
                if(loadOp.HasError)
                    return;
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                }
                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                }
            }, null);
        }

        //查询事件
        private void Search() {

            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            if(partsSalesSettlement.PartsSalesCategoryId == default(int)) {
                //UIHelper.ShowNotification("请先指定品牌后再查询");
                return;
            }
            var dataGrigView = PartsSalesSettlementRefForEditDataGridView as PartsSalesSettlementRefForEditDataGridView;
            if(dataGrigView != null)
                dataGrigView.ClearGridViewRowStyleSelector();
            int[] inboundTypes = {
                (int)DcsPartsInboundType.销售退货
            };

            int[] outboundTypes = {
                (int)DcsPartsOutboundType.配件销售
            };

            if(partsSalesSettlement.PartsSalesSettlementDetails.Any())
                foreach(var detail in partsSalesSettlement.PartsSalesSettlementDetails.ToArray())
                    partsSalesSettlement.PartsSalesSettlementDetails.Remove(detail);
            if(partsSalesSettlement.EntityState != EntityState.Modified) {
                if(partsSalesSettlement.PartsSalesSettlementRefs.Any())
                    foreach(var item in partsSalesSettlement.PartsSalesSettlementRefs.ToArray())
                        partsSalesSettlement.PartsSalesSettlementRefs.Remove(item);
            }
            int? selectWarehouseId;
            var warehouse = this.KvWarehouses.FirstOrDefault(v => v.Key == this.WarehouseId);
            if(warehouse == null)
                selectWarehouseId = null;
            else
                selectWarehouseId = (warehouse.UserObject as Warehouse).Id;           
            if(this.CutOffTime.HasValue)
                this.CutOffTime = new DateTime(this.CutOffTime.Value.Year, this.CutOffTime.Value.Month, this.CutOffTime.Value.Day, 23, 59, 59);
            bool? isIol = false;
            if(partsSalesSettlement.IsOil.HasValue) {
                if(partsSalesSettlement.IsOil.Value==1) {
                    isIol = true;
                }
            } else {
                isIol = null;
            }
            this.DomainContext.Load(this.DomainContext.查询待销售结算出入库明细NewQuery(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesSettlement.CustomerCompanyId, selectWarehouseId, partsSalesSettlement.CustomerAccountId, partsSalesSettlement.PartsSalesCategoryId, new[] { (int)DcsPartsInboundType.销售退货 }, outboundTypes, this.CutOffTime, partsSalesSettlement.SettleType, partsSalesSettlement.BusinessType, isIol), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(loadOp.Entities == null || !loadOp.Entities.Any()) {
                    this.SureCanUse = false;
                    return;
                }
                this.SureCanUse = true;
                var ids = loadOp.Entities.Where(r => r.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单).Select(e => e.OriginalRequirementBillId).ToArray();

                this.DomainContext.Load(this.DomainContext.GetPartsSalesReturnBillsQueryByIdsQuery(ids), LoadBehavior.RefreshCurrent, loadOpInternal => {
                    if(loadOpInternal.HasError) {
                        loadOpInternal.MarkErrorAsHandled();
                    }
                    var partsSalesReturnBills = loadOpInternal.Entities.ToList();

                    foreach(var outboundAndInboundBill in loadOp.Entities.Distinct()) {
                        var partsSalesSettlementRef = new PartsSalesSettlementRef();
                        partsSalesSettlementRef.PartsSalesSettlementId = partsSalesSettlement.Id;
                        partsSalesSettlementRef.SourceId = outboundAndInboundBill.BillId;
                        partsSalesSettlementRef.SourceBillCreateTime = outboundAndInboundBill.BillCreateTime;
                        partsSalesSettlementRef.SourceCode = outboundAndInboundBill.BillCode;
                        partsSalesSettlementRef.SourceType = outboundAndInboundBill.BillType;
                        partsSalesSettlementRef.SerialNumber = partsSalesSettlement.PartsSalesSettlementRefs.Count + 1;
                        partsSalesSettlementRef.WarehouseId = outboundAndInboundBill.WarehouseId;
                        partsSalesSettlementRef.WarehouseName = outboundAndInboundBill.WarehouseName;
                        partsSalesSettlementRef.PartsSalesOrderTypeName = outboundAndInboundBill.PartsSalesOrderTypeName;
                      //  partsSalesSettlementRef.InvoiceType = outboundAndInboundBill.InvoiceType;
                      //  partsSalesSettlementRef.ERPSourceOrderCode = outboundAndInboundBill.ERPOrderCode;
                        //if(outboundAndInboundBill.PartsSalesCategoryName == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_WarehouseCarNew) {
                        //    partsSalesSettlementRef.InvoiceCustomerName = outboundAndInboundBill.InvoiceCustomerName;
                        //    partsSalesSettlementRef.Taxpayeridentification = outboundAndInboundBill.Taxpayeridentification;
                        //} else {
                        //    partsSalesSettlementRef.InvoiceCustomerName = "";
                        //    partsSalesSettlementRef.Taxpayeridentification = "";
                        //}
                        //partsSalesSettlementRef.InvoiceCustomerName = outboundAndInboundBill.InvoiceCustomerName;
                        if(partsSalesSettlementRef.SourceType != (int)DcsPartsSalesSettlementRefSourceType.配件出库单)
                            partsSalesSettlementRef.SettlementAmount = (-outboundAndInboundBill.SettlementAmount);
                        else
                            partsSalesSettlementRef.SettlementAmount = outboundAndInboundBill.SettlementAmount;
                        //数据添加完成后再做汇总
                        partsSalesSettlement.PartsSalesSettlementRefs.Add(partsSalesSettlementRef);

                        if(outboundAndInboundBill.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单) {
                            var bill = outboundAndInboundBill;
                            var partsSalesReturnBill = partsSalesReturnBills.FirstOrDefault(r => r.Id == bill.OriginalRequirementBillId);
                            if(partsSalesReturnBill != null) {
                                partsSalesSettlementRef.ReturnType = partsSalesReturnBill.ReturnType;
                            }
                        }

                        this.PartsSalesSettlementRefs.Add(partsSalesSettlementRef);
                    }
                    partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementRefs.Sum(entity => entity.SettlementAmount);
                }, null);
                partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementRefs.Sum(entity => entity.SettlementAmount);
            }, null);
        }

        // 试算
        //private void Spreadsheet() {
        //    var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
        //    if(partsSalesSettlement == null)
        //        return;
        //    // 试算的值
        //    var spreadsheet = this.txtSpreadsheet.Value != null ? this.txtSpreadsheet.Value : 0;
        //    if(spreadsheet == null)
        //        return;
        //    if(partsSalesSettlement.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.返利池) {
        //        if(Convert.ToDecimal(spreadsheet) > rebateBlance) {
        //            this.txtSpreadsheet.Value = (double)partsSalesSettlement.RebateBalance;
        //            partsSalesSettlement.RebateAmount = Convert.ToDecimal(this.txtSpreadsheet.Value);
        //            partsSalesSettlement.RebateBalance = rebateBlance - Convert.ToDecimal(this.txtSpreadsheet.Value);
        //        } else {
        //            partsSalesSettlement.RebateAmount = Convert.ToDecimal(spreadsheet);
        //            partsSalesSettlement.RebateBalance = rebateBlance - Convert.ToDecimal(spreadsheet);
        //        }
        //    } else if(partsSalesSettlement.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.直接折扣) {
        //        partsSalesSettlement.RebateAmount = Convert.ToDecimal(spreadsheet);
        //    } else {
        //        return;
        //    }
        //    // 获取所有正数行
        //    //var details = partsSalesSettlement.PartsSalesSettlementDetails.Where(e => e.SettlementAmount > 0).ToArray();
        //    //if(details == null)
        //    //    return;
        //    ////试算前 先恢复以前数据
        //    //foreach(var item in partsSalesSettlement.PartsSalesSettlementDetails) {
        //    //    var detail = this.PartsSalesSettlementDetails.SingleOrDefault(ex => ex.SparePartCode == item.SparePartCode);
        //    //    if(detail != null) {
        //    //        item.DiscountedPrice = detail.DiscountedPrice;
        //    //        item.SettlementPrice = detail.SettlementPrice;
        //    //        item.PriceBeforeRebate = detail.PriceBeforeRebate;
        //    //        item.SettlementAmount = detail.SettlementAmount;
        //    //    }
        //    //}
        //    // 返利钱的总金额
        //    //var sumBefore = partsSalesSettlement.PartsSalesSettlementDetails.Where(e => e.SettlementAmount > 0).Sum(e => e.SettlementAmount);
        //    //foreach(var item in details) {
        //    //    item.DiscountedPrice = 0;
        //    //    item.SettlementAmount -= Convert.ToDecimal(spreadsheet) / details.Length;
        //    //    item.SettlementPrice = Math.Round(item.SettlementAmount / item.QuantityToSettle, 4);
        //    //    item.PriceBeforeRebate = item.SettlementPrice + item.DiscountedPrice;
        //    //    // 返利平均值大于原始价格的1/2
        //    //    if(item.PriceBeforeRebate < item.OriginalPrice / 2) {
        //    //        item.PriceBeforeRebate = item.OriginalPrice / 2;
        //    //        item.SettlementPrice = item.OriginalPrice / 2;
        //    //        item.SettlementAmount = item.SettlementPrice * item.QuantityToSettle;
        //    //    }
        //    //}
        //    // 返利后的总金额
        //    //var sumAfter = partsSalesSettlement.PartsSalesSettlementDetails.Where(e => e.SettlementAmount > 0).Sum(e => e.SettlementAmount);


        //    //当前返利
        //    //partsSalesSettlement.RebateAmount = sumBefore - sumAfter;
        //    //var partsSalesSettlementDetail = partsSalesSettlement.PartsSalesSettlementDetails.FirstOrDefault(ex => ex.SparePartCode == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt);
        //    ////当返利摊销为0时 删除折扣商品数据
        //    //if(partsSalesSettlement.RebateAmount == 0 || partsSalesSettlement.RebateAmount == null) {
        //    //    if(partsSalesSettlementDetail != null)
        //    //        partsSalesSettlement.PartsSalesSettlementDetails.Remove(partsSalesSettlementDetail);
        //    //    //计算总金额（减去返利金额）
        //    //    partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
        //    //    return;
        //    //}
        //    ////已经存在返利商品。 不需要继续新增。 只需要修改里面的金额即可
        //    //if(partsSalesSettlementDetail != null) {
        //    //    partsSalesSettlementDetail.SettlementPrice = (partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0) * -1;
        //    //    partsSalesSettlementDetail.SettlementAmount = (partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0) * -1;
        //    //    //计算总金额（减去返利金额）
        //    //    partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
        //    //    return;
        //    //}
        //    //if(sumBefore - sumAfter == 0) {
        //    //    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_RebateTheMinimun);
        //    //    return;
        //    //}

        //    //增加返利摊销清单
        //    //partsSalesSettlement.PartsSalesSettlementDetails.Add(new PartsSalesSettlementDetail {
        //    //    SerialNumber = partsSalesSettlement.PartsSalesSettlementDetails.Max(x => x.SerialNumber) + 1,
        //    //    SparePartCode = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt,
        //    //    SparePartName = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt,
        //    //    PriceBeforeRebate = 0,
        //    //    DiscountedPrice = 0,
        //    //    SettlementPrice = (partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0) * -1,
        //    //    QuantityToSettle = 1,
        //    //    SettlementAmount = (partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0) * -1
        //    //});

        //    //计算总金额（减去返利金额）
        //    partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
        //}

        //合并勾选行
        private void MergerDetail() {
            //todo:自定义四舍五入
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            if(this.PartsSalesSettlementDetailForEditDataGridView.SelectedEntities == null)
                return;
            var partsSalesSettlementDetails = this.PartsSalesSettlementDetailForEditDataGridView.SelectedEntities.Cast<PartsSalesSettlementDetail>().ToArray();
            if(partsSalesSettlementDetails == null)
                return;

            if((partsSalesSettlementDetails.Where(e => e.SettlementAmount > 0 && e.SettlementPrice > (e.OriginalPrice) / 2 && e.SparePartCode != PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt).Sum(e => e.SettlementAmount) / 2 + partsSalesSettlementDetails.Where(e => e.SettlementAmount < 0).Sum(e => e.SettlementAmount)) < 0) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_PositiveNumberLessThanAmortization);
                return;
            }
            // 负数和
            decimal negative = 0;
            foreach(var item in partsSalesSettlementDetails.Where(e => e.SettlementAmount < 0 && e.SparePartCode != PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt)) {
                negative += ClientVar.Round(item.SettlementAmount, 2);
                partsSalesSettlement.PartsSalesSettlementDetails.Remove(item);
            }
            // 正数不够余下的
            decimal remainder = 0;
            // 第一次
            bool first = true;
            while(remainder != 0 || first) {
                first = false;
                if(remainder != 0) {
                    negative = remainder;
                    remainder = 0;
                }
                var details = partsSalesSettlementDetails.Where(e => e.SettlementAmount > 0 && e.SettlementPrice > (e.OriginalPrice) / 2 && e.SparePartCode != PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt).ToArray();
                if(details == null)
                    return;
                foreach(var item in details) {
                    item.SettlementAmount += ClientVar.Round(negative / details.Length, 2);
                    item.SettlementPrice = ClientVar.Round(item.SettlementAmount / item.QuantityToSettle, 2);
                    if(item.PriceBeforeRebate - item.SettlementPrice <= ((item.OriginalPrice) / 2)) {
                        item.DiscountedPrice = ClientVar.Round(item.PriceBeforeRebate, 2) - item.SettlementPrice;
                    } else {
                        item.DiscountedPrice = ClientVar.Round(item.OriginalPrice / 2, 2);
                        item.SettlementPrice = item.DiscountedPrice;
                        item.SettlementAmount = item.SettlementPrice * item.QuantityToSettle;
                        remainder += ClientVar.Round((negative / details.Length), 2) + item.SettlementAmount;
                        if(remainder > (decimal)-0.000001)
                            remainder = 0;
                    }
                }
            }
            partsSalesSettlement.TotalSettlementAmount = 0;
            int serialNumber = 1;
            this.PartsPurchaseRtnSettleDetails.Clear();
            foreach(var item in partsSalesSettlement.PartsSalesSettlementDetails) {
                partsSalesSettlement.TotalSettlementAmount += item.SettlementAmount;
                this.PartsPurchaseRtnSettleDetails.Add(item);
                item.SerialNumber = serialNumber;
                serialNumber++;
            }
        }

        //// 试算
        //private void Spreadsheet() {
        //    var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
        //    if(partsSalesSettlement == null)
        //        return;
        //    decimal spreadsheetAmount;
        //    decimal.TryParse(txtSpreadsheet.Value.ToString(), out spreadsheetAmount);
        //    if(partsSalesSettlement.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.返利池) {
        //        if(spreadsheetAmount > partsSalesSettlement.RebateBalance)
        //            spreadsheetAmount = partsSalesSettlement.RebateBalance;
        //    }
        //    // 获取所有正数行
        //    var details = partsSalesSettlement.PartsSalesSettlementDetails.Where(e => e.SettlementAmount > 0).ToArray();
        //    if(details == null)
        //        return;

        //    // 返利钱的总金额
        //    var sumBefore = partsSalesSettlement.PartsSalesSettlementDetails.Where(e => e.SettlementAmount > 0).Sum(e => e.SettlementAmount);
        //    foreach(var item in details) {
        //        item.DiscountedPrice = 0;
        //        item.SettlementAmount -= Convert.ToDecimal(spreadsheetAmount) / details.Length;
        //        item.SettlementPrice = Math.Round(item.SettlementAmount / item.QuantityToSettle, 4);
        //        item.PriceBeforeRebate = item.SettlementPrice + item.DiscountedPrice;
        //        // 返利平均值大于原始价格的1/2
        //        if(item.OriginalPrice < item.OriginalPrice / 2) {
        //            item.PriceBeforeRebate = item.OriginalPrice / 2;
        //            item.SettlementPrice = item.OriginalPrice / 2;
        //            item.SettlementAmount = item.SettlementPrice * item.QuantityToSettle;
        //        }
        //    }
        //    // 返利后的总金额
        //    var sumAfter = partsSalesSettlement.PartsSalesSettlementDetails.Where(e => e.SettlementAmount > 0).Sum(e => e.SettlementAmount);
        //    if(sumBefore - sumAfter == 0) {
        //        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_RebateTheMinimun);
        //        return;
        //    }
        //    // 当前返利
        //    partsSalesSettlement.RebateAmount += sumBefore - sumAfter;
        //    //    var allRebate = spreadsheetAmount;
        //    //    Action<PartsSalesSettlementDetail> averageAmount = (partsSalesSettlementDetail) => {
        //    //        partsSalesSettlementDetail.SettlementPrice = Math.Round(partsSalesSettlementDetail.SettlementAmount / partsSalesSettlementDetail.QuantityToSettle, 4);
        //    //        partsSalesSettlementDetail.PriceBeforeRebate = partsSalesSettlementDetail.SettlementPrice + partsSalesSettlementDetail.DiscountedPrice;
        //    //    };
        //    //    foreach(var partsSalesSettlementDetail in details) {
        //    //        partsSalesSettlementDetail.DiscountedPrice = 0;
        //    //        var amount = partsSalesSettlementDetail.OriginalPrice * partsSalesSettlementDetail.QuantityToSettle;
        //    //        if(amount / 2 >= spreadsheetAmount) {
        //    //            partsSalesSettlementDetail.SettlementAmount = partsSalesSettlementDetail.OriginalPrice - spreadsheetAmount;
        //    //            spreadsheetAmount = 0;
        //    //            averageAmount.Invoke(partsSalesSettlementDetail);
        //    //            break;
        //    //        }
        //    //        if(amount / 2 == partsSalesSettlementDetail.SettlementAmount)
        //    //            continue;
        //    //        partsSalesSettlementDetail.SettlementAmount = amount;
        //    //        spreadsheetAmount -= amount;
        //    //        averageAmount.Invoke(partsSalesSettlementDetail);
        //    //    }
        //    //    partsSalesSettlement.RebateAmount = allRebate - spreadsheetAmount;
        //}


        //合并勾选行
        //private void MergerDetail() {
        //    var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
        //    if(partsSalesSettlement == null)
        //        return;
        //    if(this.PartsSalesSettlementDetailForEditDataGridView.SelectedEntities == null || !this.PartsSalesSettlementDetailForEditDataGridView.SelectedEntities.Any()) {
        //        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesSettlement_NeedSelectMergeItem);
        //        return;
        //    }
        //    var partsSalesSettlementDetails = this.PartsSalesSettlementDetailForEditDataGridView.SelectedEntities.Cast<PartsSalesSettlementDetail>().ToArray();
        //    if(partsSalesSettlementDetails == null)
        //        return;

        //    var amortizeSpace = partsSalesSettlementDetails.Where(e => e.SettlementAmount > 0).Sum(e => e.SettlementAmount) / 2;
        //    if(amortizeSpace < (0 - partsSalesSettlementDetails.Where(e => e.SettlementAmount < 0).Sum(e => e.SettlementAmount))) {
        //        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_PositiveNumberLessThanAmortization);
        //        return;
        //    }


        //    // 负数和
        //    // var negative = Math.Abs(partsSalesSettlementDetails.Where(e => e.SettlementAmount < 0).Sum(e => e.SettlementAmount));
        //    //Action<PartsSalesSettlementDetail> averageAmount = (partsSalesSettlementDetail) => {
        //    //    partsSalesSettlementDetail.SettlementPrice = Math.Round(partsSalesSettlementDetail.SettlementAmount / partsSalesSettlementDetail.QuantityToSettle, 4);
        //    //    partsSalesSettlementDetail.DiscountedPrice = partsSalesSettlementDetail.PriceBeforeRebate - partsSalesSettlementDetail.SettlementPrice;
        //    //};
        //    //foreach(var item in partsSalesSettlementDetails.Where(e => e.SettlementAmount < 0).ToArray()) {
        //    //    partsSalesSettlement.PartsSalesSettlementDetails.Remove(item);
        //    //}
        //    //foreach(var item in partsSalesSettlementDetails.Where(e => e.SettlementAmount >= 0)) {
        //    //    var amount = item.OriginalPrice * item.QuantityToSettle;
        //    //    if(amount / 2 >= negative) {
        //    //        item.SettlementAmount = amount - negative;
        //    //        averageAmount.Invoke(item);
        //    //        break;
        //    //    }
        //    //    if(amount / 2 == item.SettlementAmount)
        //    //        continue;
        //    //    item.SettlementAmount = amount;
        //    //    negative -= amount;
        //    //    averageAmount.Invoke(item);
        //    //}
        //    //partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(e => e.SettlementAmount);
        //    //var serialNumber = 1;
        //    //foreach(var item in partsSalesSettlement.PartsSalesSettlementDetails.ToArray()) {
        //    //    item.SerialNumber = serialNumber++;
        //    //}


        //    // 负数和
        //    decimal negative = 0;
        //    foreach(var item in partsSalesSettlementDetails.Where(e => e.SettlementAmount < 0)) {
        //        negative += item.SettlementAmount;
        //        partsSalesSettlement.PartsSalesSettlementDetails.Remove(item);
        //    }

        //    // 正数不够余下的
        //    decimal remainder = 0;
        //    do {
        //        if(remainder != 0) {
        //            negative = remainder;
        //            remainder = 0;
        //        }
        //        var details = partsSalesSettlementDetails.Where(e => e.SettlementAmount > 0 && e.SettlementPrice > (e.OriginalPrice) / 2).ToArray();
        //        if(details == null)
        //            return;
        //        foreach(var item in details) {
        //            item.SettlementAmount += negative / details.Length;
        //            item.SettlementPrice = item.SettlementAmount / item.QuantityToSettle;
        //            item.SettlementPrice = Math.Round(item.SettlementPrice, 4);
        //            if(item.PriceBeforeRebate - item.SettlementPrice <= ((item.OriginalPrice) / 2)) {
        //                item.DiscountedPrice = item.PriceBeforeRebate - item.SettlementPrice;

        //            } else {
        //                item.DiscountedPrice = item.OriginalPrice / 2;
        //                item.SettlementPrice = item.DiscountedPrice;
        //                item.SettlementAmount = item.SettlementPrice * item.QuantityToSettle;
        //                remainder += (negative / details.Length) + item.SettlementAmount;
        //                remainder = Math.Round(remainder, 4);
        //                if(remainder > (decimal)-0.000001)
        //                    remainder = 0;
        //            }
        //        }
        //    } while(remainder != 0);
        //}

        private void ReturnCurrentData() {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            this.ShowCancelButton();
            if(this.EditState == DataEditState.Edit)
                this.gdTreeView.Visibility = Visibility.Collapsed;
            else {
                this.gdTreeView.Visibility = Visibility.Visible;
                partsSalesSettlement.RebateBalance = default(decimal);
                partsSalesSettlement.RebateMethod = default(int);
            }
            this.gdPartsSalesSettlementWithRef.Visibility = Visibility.Visible;
            this.gdPartsSalesSettlementWithDetail.Visibility = Visibility.Collapsed;
            foreach(var settlementDetail in partsSalesSettlement.PartsSalesSettlementDetails.ToArray())
                partsSalesSettlement.PartsSalesSettlementDetails.Remove(settlementDetail);
            partsSalesSettlement.RebateAmount = 0;
            partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(e => e.SettlementAmount);
        }

        //保存
        private void SaveCurrentData() {
            //保存之前 试算一下
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            //var spreadsheet = this.txtSpreadsheet.Value != null ? this.txtSpreadsheet.Value : 0;
            //if(Convert.ToDecimal(spreadsheet) != 0)
            //    Spreadsheet();
            //计算总金额
            partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
            if(!this.PartsSalesSettlementDetailForEditDataGridView.CommitEdit())
                return;
            if(!this.PartsSalesSettlementRefForEditDataGridView.CommitEdit())
                return;
            partsSalesSettlement.ValidationErrors.Clear();
            //if(this.PartsSalesSettlementDetails.Count > 200) {
            //    UIHelper.ShowNotification("清单条目数量不允许超过200条");
            //    return;
            //}
            if(partsSalesSettlement.TotalSettlementAmount < 0)
                partsSalesSettlement.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_TotalSettlementAmountGreaterZero, new[] {
                    "TotalSettlementAmount"
                }));
            if(partsSalesSettlement.HasValidationErrors)
                return;
            partsSalesSettlement.WarehouseId = this.WarehouseId;
            ((IEditableObject)partsSalesSettlement).EndEdit();
            //校验销售结算单的总金额是否大于分公司策略的最大开票金额,清单数是否大于分公司策略的最大开票行数
            this.DomainContext.Load(this.DomainContext.GetBranchstrategiesQuery().Where(r => r.BranchId == partsSalesSettlement.SalesCompanyId), LoadBehavior.RefreshCurrent, loadOpS => {
                if(loadOpS.HasError) {
                    if(loadOpS.IsErrorHandled)
                        loadOpS.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpS);
                    return;
                }
                var branchstrategy = loadOpS.Entities.SingleOrDefault();
                //if(branchstrategy != null && partsSalesSettlement.TotalSettlementAmount > branchstrategy.MaxInvoiceAmount) {
                //    UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SettlementAllFee);
                //    return;
                //}
                if(branchstrategy != null && this.PartsSalesSettlementDetails.Count > branchstrategy.MaxInvoiceRow) {
                    DcsUtils.Confirm(string.Format(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_MaxInvoiceRow, branchstrategy.MaxInvoiceRow), () => this.DomainContext.Load(this.DomainContext.查询客户可用资金Query(partsSalesSettlement.CustomerAccountId, null, null), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        if(partsSalesSettlement.Id == default(int)) {
                            if(loadOp.Entities != null && loadOp.Entities.Any()) {
                                var customerAccount = loadOp.Entities.First();
                                if(partsSalesSettlement.TotalSettlementAmount > customerAccount.AccountBalance) {
                                    DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesSettlement_TotalAmountMoreThanAccountBalance, () => this.SubmitOperate(partsSalesSettlement), this.ReturnCurrentData);
                                } else {
                                    this.SubmitOperate(partsSalesSettlement);
                                }
                            }
                        } else {
                            this.SubmitOperate(partsSalesSettlement);
                        }
                    }, null));
                } else {
                    this.DomainContext.Load(this.DomainContext.查询客户可用资金Query(partsSalesSettlement.CustomerAccountId, null, null), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        if(partsSalesSettlement.Id == default(int)) {
                            if(loadOp.Entities != null && loadOp.Entities.Any()) {
                                var customerAccount = loadOp.Entities.First();
                                if(partsSalesSettlement.TotalSettlementAmount > customerAccount.AccountBalance) {
                                    DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Confirm_PartsSalesSettlement_TotalAmountMoreThanAccountBalance, () => this.SubmitOperate(partsSalesSettlement), this.ReturnCurrentData);
                                } else {
                                    this.SubmitOperate(partsSalesSettlement);
                                }
                            }
                        } else {
                            this.SubmitOperate(partsSalesSettlement);
                        }
                    }, null);
                }
            }, null);

        }
        //生成结算单事件
        //执行OnEditSubmitting()
        private void SubmitOperate(PartsSalesSettlement partsSalesSettlement) {
            try {
                if(this.InvoiceDate.HasValue)
                {
                    this.InvoiceDate = new DateTime(this.InvoiceDate.Value.Year, this.InvoiceDate.Value.Month, this.InvoiceDate.Value.Day, 00, 00, 00);
                    partsSalesSettlement.InvoiceDate = this.InvoiceDate.Value;
                }
                else if(!partsSalesSettlement.InvoiceDate.HasValue)
                {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_InvoiceDate);
                    return;
                }
                if(this.DomainContext.IsBusy)
                    return;
                if(partsSalesSettlement.Id == default(int)) {                               
                        
                    decimal line = 1000000;
                    if(partsSalesSettlement.TotalSettlementAmount <= line || partsSalesSettlement.PartsSalesSettlementRefs.Count()==1) {
                        if(partsSalesSettlement.Can生成配件销售结算单) {
                            partsSalesSettlement.生成配件销售结算单();
                            base.OnEditSubmitting();
                        }
                    } else {
                    try {
                        this.DomainContext.生成配件销售结算单2(partsSalesSettlement, partsSalesSettlement.PartsSalesSettlementRefs.ToList(), partsSalesSettlement.PartsSalesSettlementDetails.ToList(), invokeOp => {
                            if(invokeOp.HasError) {
                                if(!invokeOp.IsErrorHandled)
                                    invokeOp.MarkErrorAsHandled();
                                var error = invokeOp.ValidationErrors.FirstOrDefault();
                                if(error != null) {
                                    UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                                } else {
                                    DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                }
                                DomainContext.RejectChanges();
                                return;
                            }
                            this.重置界面();
                        }, null);
                    } catch(ValidationException ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                        return;
                    }
                }
                } else {
                    if(partsSalesSettlement.Can修改配件销售结算单)
                        partsSalesSettlement.修改配件销售结算单();
                        base.OnEditSubmitting();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
           
        }        
        ////修改结算单事件
        //private void ModifyOperate(PartsSalesSettlement partsSalesSettlement) {
        //    try {
        //        if(partsSalesSettlement.Can修改配件销售结算单)
        //            partsSalesSettlement.修改配件销售结算单();
        //    } catch(ValidationException ex) {
        //        UIHelper.ShowAlertMessage(ex.Message);
        //        return;
        //    }
        //    base.OnEditSubmitting();
        //}
        #endregion

        #region 树事件

        private void PartsSalesSettlementDataTreeView_OnTreeViewItemClick(object sender, RadRoutedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            lastNode = e.OriginalSource as RadTreeViewItem;
            if(this.lastNode == null)
                return;
            if(!(this.lastNode.Tag is VirtualCustomerInformation))
                return;
            var company = this.lastNode.Tag as VirtualCustomerInformation;
            var accountGroup = this.KvAccountGroups.First(v => v.Key == this.AccountGroupId).UserObject as AccountGroup;
            if(accountGroup == null)
                return;
            Action copyValue = () => {
                partsSalesSettlement.CustomerCompanyId = company.Id;
                partsSalesSettlement.CustomerCompanyCode = company.Code;
                partsSalesSettlement.CustomerCompanyName = company.Name;
                partsSalesSettlement.AccountGroupName = accountGroup.Name;
                partsSalesSettlement.AccountGroupId = accountGroup.Id;
                partsSalesSettlement.AccountGroupCode = accountGroup.Code;
                DomainContext.Load(this.DomainContext.GetSalesUnitsQuery().Where(r => r.AccountGroupId == accountGroup.Id && r.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    if(loadOp.Entities == null || !loadOp.Entities.Any()) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesSettlement_SettlementDealerIsEmpty);
                        return;
                    }
                    foreach(SalesUnit entity in loadOp.Entities) {
                        if(entity.AccountGroupId == accountGroup.Id && entity.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId)
                            partsSalesSettlement.PartsSalesCategoryId = entity.PartsSalesCategoryId;
                    }
                }, null);

                //IEnumerable<CustomerAccount> temp = null;
                //if(company.CustomerAccounts.Any())
                //    temp = company.CustomerAccounts.Where(c => c.AccountGroupId == accountGroup.Id);
                if(company.Customeraccountid != 0)
                    partsSalesSettlement.CustomerAccountId = company.Customeraccountid;
                else {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesSettlement_CustomerAccountIsNull);
                    return;
                }
                foreach(var item in partsSalesSettlement.PartsSalesSettlementRefs.ToArray())
                    partsSalesSettlement.PartsSalesSettlementRefs.Remove(item);
                if(0 != partsSalesSettlement.BusinessType && null != partsSalesSettlement.BusinessType) {
                    this.SearchCanUse = true;
                } else {
                    this.SearchCanUse = false;
                }
            };
            if((partsSalesSettlement.CustomerCompanyId != company.Id && partsSalesSettlement.CustomerCompanyId != default(int)) || (partsSalesSettlement.AccountGroupId != default(int) && partsSalesSettlement.AccountGroupId != this.AccountGroupId))
                DcsUtils.Confirm(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_SettlementingCustomerIsClearCustomer, copyValue.Invoke);
            else if(partsSalesSettlement.CustomerCompanyId == default(int) && partsSalesSettlement.AccountGroupId == default(int))
                copyValue.Invoke();
        }

        private void PartsSalesSettlementDataTreeView_OnTreeViewItemExpanded(object sender, RadRoutedEventArgs e) {
            //if(AccountGroupId == default(int)) {
            //    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_AccountGroupNameIsNull);
            //    return;
            //}
            //if(CutOffTime.HasValue == false) {
            //    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_CutOffTimeIsNull);
            //    return;
            //}
            var endTime = new DateTime(this.CutOffTime.Value.Year, this.CutOffTime.Value.Month, this.CutOffTime.Value.Day, 00, 00, 00).AddDays(1);

            var item = e.OriginalSource as RadTreeViewItem;
            if(item == null)
                return;

            var region = item.Tag as Region;
            if(region == null)
                return;

            this.DomainContext.Load(this.DomainContext.获取配件销售结算经销商Query(BaseApp.Current.CurrentUserData.EnterpriseId, this.AccountGroupId, endTime, region.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(loadOp.Entities == null || !loadOp.Entities.Any()) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesSettlement_SettlementDealerIsEmpty);
                    return;
                }
                item.Items.Clear();
                foreach(var nextNode in loadOp.Entities.Select(customerInformation => new RadTreeViewItem {
                    Header = customerInformation.Name,
                    Tag = customerInformation
                }))
                    item.Items.Add(nextNode);
            }, null);
        }

        #endregion

        #endregion

        //private DcsComboBox combbox {
        //    get;
        //    set;
        //}

        private  void 重置界面() {
         //   base.OnEditSubmitted();
            this.gdTreeView.Visibility = Visibility.Visible;
            this.gdPartsSalesSettlementWithRef.Visibility = Visibility.Visible;
            this.gdPartsSalesSettlementWithDetail.Visibility = Visibility.Collapsed;
            this.SureCanUse = false;
            this.SearchCanUse = false;
          //  this.combbox.SelectedValue = default(int);
            if(EditState == DataEditState.New) {
                this.Rest();
                var partsSalesSettlement = this.CreateObjectToEdit<PartsSalesSettlement>();
                partsSalesSettlement.Code = GlobalVar.ASSIGNED_BY_SERVER;
                partsSalesSettlement.TaxRate = 0.13;
                //partsSalesSettlement.RebateMethod = (int)DcsPartsSalesSettlementRebateMethod.返利池;
                partsSalesSettlement.Status = (int)DcsPartsSalesSettlementStatus.新建;
                partsSalesSettlement.SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算;
                partsSalesSettlement.SalesCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                partsSalesSettlement.SalesCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                partsSalesSettlement.SalesCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                partsSalesSettlement.SettleType = (int)DcsSalesOrderType_SettleType.销售结算;
                partsSalesSettlement.BusinessType = (int)DcsSalesOrderType_BusinessType.国内销售;
            }
            this.PartsSalesSettlementRefs.Clear();
            refDataEditView.RegisterButton(this.BtnExportLiaison);
            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            this.ShowCancelButton();
        }
        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.gdTreeView.Visibility = Visibility.Visible;
            this.gdPartsSalesSettlementWithRef.Visibility = Visibility.Visible;
            this.gdPartsSalesSettlementWithDetail.Visibility = Visibility.Collapsed;
            this.SureCanUse = false;
            this.SearchCanUse = false;
            //  this.combbox.SelectedValue = default(int);
            if(EditState == DataEditState.New) {
                this.Rest();
                var partsSalesSettlement = this.CreateObjectToEdit<PartsSalesSettlement>();
                partsSalesSettlement.Code = GlobalVar.ASSIGNED_BY_SERVER;
                partsSalesSettlement.TaxRate = 0.13;
                //partsSalesSettlement.RebateMethod = (int)DcsPartsSalesSettlementRebateMethod.返利池;
                partsSalesSettlement.Status = (int)DcsPartsSalesSettlementStatus.新建;
                partsSalesSettlement.SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算;
                partsSalesSettlement.SalesCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                partsSalesSettlement.SalesCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                partsSalesSettlement.SalesCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                partsSalesSettlement.SettleType = (int)DcsSalesOrderType_SettleType.销售结算;
                partsSalesSettlement.BusinessType = (int)DcsSalesOrderType_BusinessType.国内销售;
            }
            this.PartsSalesSettlementRefs.Clear();
            refDataEditView.RegisterButton(this.BtnExportLiaison);
            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            this.ShowCancelButton();
        }
        protected override bool OnRequestCanSubmit()
        {
            this.DCB_refurbish.SelectedIndex = 0;//默认品牌
            return true;
        }

        private void Rest() {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            if(this.DomainContext.PartsSalesSettlements.Contains(partsSalesSettlement)) {
                this.DomainContext.PartsSalesSettlements.Detach(partsSalesSettlement);
            }
            this.DomainContext.RejectChanges();
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            refDataEditView.RegisterButton(this.BtnExportLiaison);
            this.PartsSalesSettlementRefs.Clear();
            this.SureCanUse = false;
            this.SearchCanUse = false;
            this.gdTreeView.Visibility = Visibility.Visible;
            this.gdPartsSalesSettlementWithRef.Visibility = Visibility.Visible;
            this.gdPartsSalesSettlementWithDetail.Visibility = Visibility.Collapsed;
          //  this.combbox.SelectedValue = default(int);
            this.DCB_refurbish.SelectedValue = default(int);
            this.AccountGroupId = default(int);
            this.UnregisterButton(returnBtn);
            this.UnregisterButton(saveBtn);
            this.ShowCancelButton();
        }

        protected override void Reset() {
            this.DCB_Warehouse.SelectedValue = default(int);
            this.txtWarehouseName.Text = string.Empty;
            //this.PartsSalesSettlementDataTreeView.ClearTreeViewItem();
            base.Reset();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSalesSettlement;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ICommand SearchCommand {
            get {
                return this.searchCommand ?? (this.searchCommand = new DelegateCommand(this.Search));
            }
        }

        public ICommand SureCommand {
            get {
                return this.sureCommand ?? (this.sureCommand = new Core.Command.DelegateCommand(this.Selected));
            }
        }

        public bool SearchCanUse {
            get {
                return this.searchCanUse;
            }
            set {
                this.searchCanUse = value;
                this.OnPropertyChanged("SearchCanUse");
            }
        }

        public bool SureCanUse {
            get {
                return this.sureCanUse;
            }
            set {
                this.sureCanUse = value;
                this.OnPropertyChanged("SureCanUse");
            }
        }

        public int AccountGroupId {
            get {
                return this.accountGroupId;
            }
            set {
                this.accountGroupId = value;
                this.OnPropertyChanged("AccountGroupId");
            }
        }

        public int WarehouseId {
            get {
                return this.warehouseId;
            }
            set {
                this.warehouseId = value;
                this.OnPropertyChanged("WarehouseId");
            }
        }

        public DateTime? CutOffTime {
            get {
                return this.cutOffTime;
            }
            set {
                this.cutOffTime = value;
                this.OnPropertyChanged("CutOffTime");
            }
        }
        public DateTime? InvoiceDate
        {
            get
            {
                return this.invoiceDate;
            }
            set
            {
                this.invoiceDate = value;
                this.OnPropertyChanged("InvoiceDate");
            }
        }

        public decimal AmortizedRebate {
            get {
                return this.amortizedRebate;
            }
            set {
                this.amortizedRebate = value;
                this.OnPropertyChanged("AmortizedRebate");
            }
        }

        public ObservableCollection<KeyValuePair> KvAccountGroups {
            get {
                return this.kvAccountGroups ?? (this.kvAccountGroups = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> kvRebateMethods;

        public ObservableCollection<KeyValuePair> KvRebateMethods {
            get {
                return this.kvRebateMethods ?? (this.kvRebateMethods = new ObservableCollection<KeyValuePair>());
            }
        }

        private void RadButton_Click_1(object sender, RoutedEventArgs e) {

            this.reFreshTree();
        }

        private void reFreshTree() {
            if(AccountGroupId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_AccountGroupNameIsNull);
                return;
            }
            if(CutOffTime.HasValue == false) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesSettlement_CutOffTimeIsNull);
                return;
            }
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            if(SalesSettleStrategy) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_WareHouse);
                return;
            }
            if(settleType == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SetType);
                return;
            }
            if(businessType == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_businessType);
                return;
            }
            if(this.AccountGroupId == 0 || CutOffTime == null)
                return;
            var accountGroup = this.KvAccountGroups.First(v => v.Key == this.AccountGroupId).UserObject as AccountGroup;
            if(accountGroup == null)
                return;
            var item = this.KvWarehouses.FirstOrDefault(v => v.Key == this.WarehouseId);
            int? warehouseId;
            if(item == null)
                warehouseId = null;
            else
                warehouseId = (item.UserObject as Warehouse).Id;
            bool? isIol = false;
            if(partsSalesSettlement.IsOil.HasValue) {
                if(partsSalesSettlement.IsOil.Value == 1) {
                    isIol = true;
                }
            } else {
                isIol = null;
            }
            this.partsSalesSettlementDataTreeView.EntityQuery = this.PartsSalesSettlementDataTreeView.DomainContext.查询存在未结算单据的区域Query(BaseApp.Current.CurrentUserData.EnterpriseId, warehouseId, new DateTime(CutOffTime.Value.Year, CutOffTime.Value.Month, CutOffTime.Value.Day, 23, 59, 59), accountGroup.Id, settleType, businessType, isIol);
            this.partsSalesSettlementDataTreeView.RefreshDataTree();
        }
        private void isOilCombox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            if(partsSalesSettlement.PartsSalesSettlementRefs.Count() > 0) {
                DcsUtils.Confirm(PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SameSettle, () => {
                    this.reFreshTree();
                    foreach(var item in partsSalesSettlement.PartsSalesSettlementRefs.ToArray())
                        partsSalesSettlement.PartsSalesSettlementRefs.Remove(item);
                });
            } else {
                this.reFreshTree();
            }                                 
        }

    }
}
