﻿
using System;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.MaskedInput;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesSettlementForAppoveRebateDataEditView {
        private DataGridViewBase partsSalesSettlementDetailForEditDataGridView;
        private RadMaskedNumericInput txtSpreadsheet;
        private KeyValueManager keyValueManager;
        private decimal rebateBlance;

        private readonly string[] kvNames = {
            "PartsSalesSettlement_RebateMethod"
        };

        private DcsComboBox combbox {
            get;
            set;
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public PartsSalesSettlementForAppoveRebateDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
            this.keyValueManager.LoadData();
            this.DataContextChanged += PartsSalesSettlementForAppoveRebateDataEditView_DataContextChanged;
        }

        private void PartsSalesSettlementForAppoveRebateDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            partsSalesSettlement.PropertyChanged -= partsSalesSettlement_PropertyChanged;
            partsSalesSettlement.PropertyChanged += partsSalesSettlement_PropertyChanged;
        }

        private void partsSalesSettlement_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            switch(e.PropertyName) {
                case "TotalSettlementAmount":
                    if(partsSalesSettlement.TaxRate <= 1 && partsSalesSettlement.TaxRate >= 0)
                        partsSalesSettlement.Tax = Math.Round((partsSalesSettlement.TotalSettlementAmount / (decimal)(1 + partsSalesSettlement.TaxRate)) * (decimal)partsSalesSettlement.TaxRate, 2);
                    break;
            }
        }

        private DataGridViewBase PartsSalesSettlementDetailForEditDataGridView {
            get {
                if(this.partsSalesSettlementDetailForEditDataGridView == null) {
                    this.partsSalesSettlementDetailForEditDataGridView = DI.GetDataGridView("PartsSalesSettlementDetailForEdit");
                    this.partsSalesSettlementDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSalesSettlementDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            // 清单
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsSalesSettlement), "PartsSalesSettlementDetails"), null, this.PartsSalesSettlementDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnProperty, 0);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 2);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);

            this.gdPartsSalesSettlementWithDetail.Children.Add(detailDataEditView);

            var testCalc = new Grid();
            testCalc.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            testCalc.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(150)
            });
            testCalc.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(35)
            });
            testCalc.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(20)
            });
            var textBlock = new TextBlock {
                Text = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesSettlement_RebateAmount
            };
            combbox = new DcsComboBox {
                Height = 22,
                Width = 150,
                ItemsSource = this.keyValueManager[this.kvNames[0]],
                CanKeyboardNavigationSelectItems = true,
                ClearSelectionButtonVisibility = Visibility.Visible,
                ClearSelectionButtonContent = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_Clear,
                IsEditable = false,
                IsMouseWheelEnabled = true,
                DisplayMemberPath = "Value",
                SelectedValuePath = "Key"
            };
            combbox.SelectionChanged += combbox_SelectionChanged;
            combbox.SetValue(Grid.RowProperty, 1);
            combbox.SetValue(Grid.ColumnProperty, 1);
            this.gdPartsSalesSettlement.Children.Add(combbox);
            textBlock.SetValue(Grid.ColumnProperty, 0);
            testCalc.Children.Add(textBlock);
            txtSpreadsheet = new RadMaskedNumericInput();
            txtSpreadsheet.MinWidth = 130;
            txtSpreadsheet.Mask = "";
            txtSpreadsheet.FormatString = "n2";
            txtSpreadsheet.IsClearButtonVisible = false;
            txtSpreadsheet.SpinMode = SpinMode.Position;
            txtSpreadsheet.SelectionOnFocus = SelectionOnFocus.SelectAll;
            txtSpreadsheet.VerticalAlignment = VerticalAlignment.Center;
            txtSpreadsheet.HorizontalContentAlignment = HorizontalAlignment.Right;
            txtSpreadsheet.Margin = new Thickness(10, -2, 10, 0);
            txtSpreadsheet.SetValue(Grid.ColumnProperty, 1);
            txtSpreadsheet.EmptyContent = "";
            txtSpreadsheet.KeyDown += txtSpreadsheet_KeyDown;
            testCalc.Children.Add(txtSpreadsheet);

            var radButton = new RadButton {
                Content = PartsSalesUIStrings.Button_Text_Common_Spreadsheet,
                Command = new Core.Command.DelegateCommand(this.Spreadsheet)
            };
            radButton.SetValue(Grid.ColumnProperty, 2);
            testCalc.Children.Add(radButton);
            testCalc.HorizontalAlignment = HorizontalAlignment.Center;
            testCalc.VerticalAlignment = VerticalAlignment.Top;
            testCalc.SetValue(Grid.RowProperty, 1);
            testCalc.SetValue(Grid.ColumnProperty, 0);
            testCalc.Margin = new Thickness(0, 5, 0, 0);
            this.gdPartsSalesSettlementWithDetail.Children.Add(testCalc);

        }

        private void txtSpreadsheet_KeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Enter)
                this.Spreadsheet();
        }

        private void combbox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            partsSalesSettlement.RebateMethod = (int?)this.combbox.SelectedValue;
            var partsSalesSettlementDetail = partsSalesSettlement.PartsSalesSettlementDetails.FirstOrDefault(ex => ex.SparePartCode == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt);
            //不返利时候  删除折扣商品数据 清除返利金额
            if(partsSalesSettlementDetail != null)
                partsSalesSettlement.PartsSalesSettlementDetails.Remove(partsSalesSettlementDetail);
            partsSalesSettlement.RebateAmount = null;
            txtSpreadsheet.Value = null;
        }

        private void Spreadsheet() {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            // 试算的值
            var spreadsheet = this.txtSpreadsheet.Value != null ? this.txtSpreadsheet.Value : 0;
            if(spreadsheet == null)
                return;
            if(partsSalesSettlement.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.返利池) {
                if(Convert.ToDecimal(spreadsheet) > rebateBlance) {
                    this.txtSpreadsheet.Value = (double)partsSalesSettlement.RebateBalance;
                    partsSalesSettlement.RebateAmount = Convert.ToDecimal(this.txtSpreadsheet.Value);
                    partsSalesSettlement.RebateBalance = rebateBlance - Convert.ToDecimal(this.txtSpreadsheet.Value);
                } else {
                    partsSalesSettlement.RebateAmount = Convert.ToDecimal(spreadsheet);
                    partsSalesSettlement.RebateBalance = rebateBlance - Convert.ToDecimal(spreadsheet);
                }
            } else if(partsSalesSettlement.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.直接折扣) {
                partsSalesSettlement.RebateAmount = Convert.ToDecimal(spreadsheet);
            } else {
                return;
            }
            //当前返利
            //partsSalesSettlement.RebateAmount = sumBefore - sumAfter;
            var partsSalesSettlementDetail = partsSalesSettlement.PartsSalesSettlementDetails.FirstOrDefault(ex => ex.SparePartCode == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt);
            //当返利摊销为0时 删除折扣商品数据
            if(partsSalesSettlement.RebateAmount == 0 || partsSalesSettlement.RebateAmount == null) {
                if(partsSalesSettlementDetail != null)
                    partsSalesSettlement.PartsSalesSettlementDetails.Remove(partsSalesSettlementDetail);
                //计算总金额（减去返利金额）
                partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
                return;
            }
            //已经存在返利商品。 不需要继续新增。 只需要修改里面的金额即可
            if(partsSalesSettlementDetail != null) {
                partsSalesSettlementDetail.SettlementPrice = (partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0) * -1;
                partsSalesSettlementDetail.SettlementAmount = (partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0) * -1;
                //计算总金额（减去返利金额）
                partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
                return;
            }
            //增加返利摊销清单
            partsSalesSettlement.PartsSalesSettlementDetails.Add(new PartsSalesSettlementDetail {
                SerialNumber = partsSalesSettlement.PartsSalesSettlementDetails.Max(x => x.SerialNumber) + 1,
                SparePartCode = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt,
                SparePartName = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_DisCountSpareprt,
                PriceBeforeRebate = 0,
                DiscountedPrice = 0,
                SettlementPrice = (partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0) * -1,
                QuantityToSettle = 1,
                SettlementAmount = (partsSalesSettlement.RebateAmount.HasValue ? partsSalesSettlement.RebateAmount.Value : 0) * -1
            });

            //计算总金额（减去返利金额）
            partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementDetails.Sum(entity => entity.SettlementAmount);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesSettlementWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                // 获取返利余额
                DomainContext.Load(DomainContext.查询客户可用返利Query(entity.AccountGroupId, entity.CustomerCompanyId, entity.Id == default(int) ? (int?)null : entity.Id), LoadBehavior.RefreshCurrent, loadOperation => {
                    if(loadOperation.HasError) {
                        if(!loadOperation.IsErrorHandled)
                            loadOperation.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOperation);
                        return;
                    }
                    if(!loadOperation.Entities.Any())
                        return;
                    rebateBlance = loadOperation.Entities.First().UseablePosition;
                    if(entity.RebateMethod == (int)DcsPartsSalesSettlementRebateMethod.返利池) {
                        entity.RebateBalance = loadOperation.Entities.First().UseablePosition - (entity.RebateAmount ?? 0);
                    } else {
                        entity.RebateBalance = loadOperation.Entities.First().UseablePosition;
                    }
                }, null);

                this.combbox.SelectedValue = entity.RebateMethod;
                this.txtSpreadsheet.Value = null;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Validation_PartsSalesReturnBill_SettleApprove;
            }
        }

        protected override void OnEditSubmitting() {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            //保存之前 试算一下
            var spreadsheet = this.txtSpreadsheet.Value != null ? this.txtSpreadsheet.Value : 0;
            if(Convert.ToDecimal(spreadsheet) != 0)
                Spreadsheet();
            try {
                if(partsSalesSettlement.Can审批配件销售结算单)
                    partsSalesSettlement.审批配件销售结算单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override void Reset() {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            if(this.DomainContext.PartsSalesSettlements.Contains(partsSalesSettlement)) {
                this.DomainContext.PartsSalesSettlements.Detach(partsSalesSettlement);
            }
        }
    }
}
