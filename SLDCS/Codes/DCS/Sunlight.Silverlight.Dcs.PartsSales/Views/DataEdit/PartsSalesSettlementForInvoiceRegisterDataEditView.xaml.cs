﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSalesSettlementForInvoiceRegisterDataEditView {
        private ObservableCollection<InvoiceInformation> invoiceInformations;
        private DataGridViewBase invoiceInformationForSettleDataGridView;

        public PartsSalesSettlementForInvoiceRegisterDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public DataGridViewBase InvoiceInformationForSettleDataGridView {
            get {
                if(this.invoiceInformationForSettleDataGridView == null) {
                    this.invoiceInformationForSettleDataGridView = DI.GetDataGridView("InvoiceInformationForPartsSalesSettlement");
                    this.invoiceInformationForSettleDataGridView.DomainContext = this.DomainContext;
                    this.invoiceInformationForSettleDataGridView.DataContext = this;
                }
                return this.invoiceInformationForSettleDataGridView;
            }
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DataGridView_Title_InvoiceInformation, null, () => this.InvoiceInformationForSettleDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 8);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEditView_Title_InvoiceRegister_PartsSalesRtnSettlement;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesSettlementsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.InvoiceInformationForSettleDataGridView.CommitEdit())
                return;

            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            var id = 0;
            var invoiceAmountTotal = this.InvoiceInformations.Sum(v => v.InvoiceAmount);
            foreach (var invoiceInformation in InvoiceInformations)
            {
                if (invoiceInformation.HasValidationErrors)
                {
                    invoiceInformation.ValidationErrors.Clear();
                }
            }
            foreach(var invoiceInformation in InvoiceInformations) {
                if(string.IsNullOrEmpty(invoiceInformation.InvoiceCode))
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeIsNull, new[] {
                        "InvoiceCode"
                    }));
                if(string.IsNullOrEmpty(invoiceInformation.InvoiceNumber))
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceNumberIsNull, new[] {
                        "InvoiceNumber"
                    }));
                if(!invoiceInformation.InvoiceAmount.HasValue) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceAmountIsNull, new[] {
                        "InvoiceAmount"
                    }));
                } else if(invoiceInformation.InvoiceAmount < 0) {
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceAmountMustGreaterZero, new[] {
                        "InvoiceAmount"
                    }));
                }
                if(!invoiceInformation.TaxRate.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_TaxRateIsNull, new[] {
                        "TaxRate"
                    }));
                if(!invoiceInformation.InvoiceTax.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceTaxIsNull, new[] {
                        "InvoiceTax"
                    }));
                if(!invoiceInformation.InvoiceDate.HasValue)
                    invoiceInformation.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceDateIsNull, new[] {
                        "InvoiceDate"
                    }));
                invoiceInformation.Id = id--;
                ((IEditableObject)invoiceInformation).EndEdit();
            }

            if(InvoiceInformations.GroupBy(entity => entity.InvoiceNumber).Any(array => array.Count() > 1)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_InvoiceInformation_InvoiceCodeRepeat);
                return;
            }
            if(InvoiceInformations.Any(e => e.HasValidationErrors))
                return;

            this.DomainContext.InvoiceInformations.Clear();
            foreach (var invoiceInformation in InvoiceInformations)
            {
                this.DomainContext.InvoiceInformations.Add(invoiceInformation);
            }
            ((IEditableObject)partsSalesSettlement).EndEdit();
            try
            {
                if (partsSalesSettlement.Can登记发票)
                    if (invoiceAmountTotal != null)
                        partsSalesSettlement.登记发票(invoiceAmountTotal.Value);
            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            
            base.OnEditSubmitting();
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.InvoiceInformations.Clear();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.InvoiceInformations.Clear();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<InvoiceInformation> InvoiceInformations {
            get {
                if(this.invoiceInformations == null) {
                    this.invoiceInformations = new ObservableCollection<InvoiceInformation>();
                }
                return invoiceInformations;
            }
        }
    }
}
