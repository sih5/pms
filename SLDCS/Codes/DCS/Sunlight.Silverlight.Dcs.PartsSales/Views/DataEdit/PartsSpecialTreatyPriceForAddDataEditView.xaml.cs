﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSpecialTreatyPriceForAddDataEditView : INotifyPropertyChanged {
        private ObservableCollection<PartsSalesCategory> kvPartsSalesCategories;
        private ObservableCollection<PartsSpecialTreatyPrice> partsSpecialTreatyPrices;
        public event PropertyChangedEventHandler PropertyChanged;
        private DataGridViewBase partsSpecialTreatyPriceForEditDataGridView;
        private string partsSalesCategoryCode, partsSalesCategoryName, remark, customerCode, customerName;
        private int partsSalesCategoryId, CustomerId;

        public PartsSpecialTreatyPriceForAddDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsSpecialTreatyPriceForAddDataEditView_DataContextChanged;
        }

        private void PartsSpecialTreatyPriceForAddDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.Clear();
            this.PropertyChanged -= this.PartsSpecialTreatyPriceForAddDataEditView_PropertyChanged;
            this.PropertyChanged += this.PartsSpecialTreatyPriceForAddDataEditView_PropertyChanged;
        }

        private void PartsSpecialTreatyPriceForAddDataEditView_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var dataEditView = sender as PartsSpecialTreatyPriceForAddDataEditView;
            if(dataEditView == null)
                return;
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    var partsSalesCategory = this.KvPartsSalesCategories.SingleOrDefault(entity => entity.Id == dataEditView.PartsSalesCategoryId);
                    if(partsSalesCategory != null)
                        this.PartsSalesCategoryCode = partsSalesCategory.Code;
                    break;
            }
        }

        private DataGridViewBase PartsSpecialTreatyPriceForEditDataGridView {
            get {
                if(this.partsSpecialTreatyPriceForEditDataGridView == null) {
                    this.partsSpecialTreatyPriceForEditDataGridView = DI.GetDataGridView("PartsSpecialTreatyPriceForEdit");
                    this.partsSpecialTreatyPriceForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsSpecialTreatyPriceForEditDataGridView.DataContext = this;
                }
                return this.partsSpecialTreatyPriceForEditDataGridView;
            }
        }

        private void CreateUI() {

            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategories.Add(partsSalesCategory);
            }, null);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.PartsSalesPrice_PartsPurchasePricingDetails, null, () => this.PartsSpecialTreatyPriceForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            var queryWindowCustomer = DI.GetQueryWindow("CustomerInformationBySparepart");
            queryWindowCustomer.SelectionDecided += this.QueryWindowCompany_SelectionDecided;
            this.popupTextBoxDealer.PopupContent = queryWindowCustomer;
        }

        private void Clear() {
            this.CustomerId = default(int);
            this.CustomerCode = string.Empty;
            this.CustomerName = string.Empty;
            this.PartsSalesCategoryCode = string.Empty;
            this.PartsSalesCategoryName = string.Empty;
            this.PartsSalesCategoryId = default(int);
            this.Remark = string.Empty;
            this.PartsSpecialTreatyPrices.Clear();
        }

        private void QueryWindowCompany_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var CustomerInformation = queryWindow.SelectedEntities.Cast<CustomerInformation>().FirstOrDefault();
            if(CustomerInformation == null)
                return;
            this.CustomerId = CustomerInformation.CustomerCompany.Id;
            this.CustomerCode = CustomerInformation.CustomerCompany.Code;
            this.CustomerName = CustomerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void OnPropertyChanged(string filedName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(filedName));
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSpecialTreatyPrice;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.partsSpecialTreatyPriceForEditDataGridView.CommitEdit())
                return;
            var validationErrors = new List<string>();
            if(this.CustomerId == default(int))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_PartsSpecialTreatyPrice_DealerCodeIsNull);
            if(string.IsNullOrEmpty(this.PartsSalesCategoryCode))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_PartsSpecialTreatyPrice_PartsSalesCategoryCodeIsNull);
            if(string.IsNullOrEmpty(this.PartsSalesCategoryName) || this.PartsSalesCategoryId == default(int))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_PartsSpecialTreatyPrice_PartsSalesCategoryNameIsNull);
            if(!this.PartsSpecialTreatyPrices.Any())
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_SparePartDetailsIsNull);
            if(this.PartsSpecialTreatyPrices.GroupBy(v => v.SparePartId).Any(e => e.Count() > 1))
                validationErrors.Add(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_SparePartIsRepeat);

            foreach(var partsSpecialTreatyPrice in PartsSpecialTreatyPrices) {
                //if(partsSpecialTreatyPrice.ValidationTime.CompareTo(partsSpecialTreatyPrice.ExpireTime) > 0) {
                //    UIHelper.ShowNotification(String.Format(PartsSalesUIStrings.DataEditView_Validation_PartsSpecialTreatyPrice_ExpireTimeEarlierThanValidationTime));
                //    return;
                //}
                if(partsSpecialTreatyPrice.TreatyPrice <= default(decimal))
                    partsSpecialTreatyPrice.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSpecialTreatyPrice_TreatyPriceLowerZero, new[] {
                        "TreatyPrice"
                    }));
            }

            if(validationErrors.Any() || this.PartsSpecialTreatyPrices.Any(e => e.HasValidationErrors)) {
                if(validationErrors.Any())
                    UIHelper.ShowNotification(string.Join(Environment.NewLine, validationErrors));
                return;
            }
            foreach(var partsSpecialTreatyPrice in PartsSpecialTreatyPrices) {
                partsSpecialTreatyPrice.CompanyId = this.CustomerId;
                partsSpecialTreatyPrice.PartsSalesCategoryId = this.PartsSalesCategoryId;
                partsSpecialTreatyPrice.PartsSalesCategoryName = this.PartsSalesCategoryName;
                partsSpecialTreatyPrice.PartsSalesCategoryCode = this.PartsSalesCategoryCode;
                partsSpecialTreatyPrice.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                if(!this.DomainContext.PartsSpecialTreatyPrices.Contains(partsSpecialTreatyPrice))
                    this.DomainContext.PartsSpecialTreatyPrices.Add(partsSpecialTreatyPrice);
            }
            base.OnEditSubmitting();
            this.DomainContext.Load(this.DomainContext.GetPartsPurchasePricingByPartIdsQuery(PartsSpecialTreatyPrices.Select(r => r.SparePartId).ToArray()), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null)
                    return;
                foreach(var partsSpecialTreatyPrice in PartsSpecialTreatyPrices) {
                    var partsPurchasePricing = loadOp.Entities.FirstOrDefault(r => r.PartId == partsSpecialTreatyPrice.SparePartId);

                }

            }, null);
        }

        protected override void OnEditCancelled() {
            this.Clear();
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            if(this.PartsSalesCategoryName == "欧曼保外" && this.CustomerCode == "FT000333") { //FT000333服务站的特殊协议价审核生效后，
                //触发：生成营销库中该配件对欧曼虚拟供应商FT001078的新建状态的采购价申请单，其价格等于福戴业务维护的特殊协议价。
                //生成特殊协议采购价申请单
                var partsPurchasePricingChange = new PartsPurchasePricingChange();
                List<PartsPurchasePricingDetail> partsPurchasePricingDetails = new List<PartsPurchasePricingDetail>();

                partsPurchasePricingChange.Code = GlobalVar.ASSIGNED_BY_SERVER;
                partsPurchasePricingChange.BranchId = this.CustomerId;
                partsPurchasePricingChange.Status = (int)DcsPartsPurchasePricingChangeStatus.新增;
                partsPurchasePricingChange.PartsSalesCategoryId = this.PartsSalesCategoryId;
                partsPurchasePricingChange.PartsSalesCategoryName = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_WarehouseCarNew;

                var partPurchasePricingDetail = new PartsPurchasePricingDetail();
                foreach(var partSpecialTreatyPrice in PartsSpecialTreatyPrices) {
                    partPurchasePricingDetail = new PartsPurchasePricingDetail();
                    partPurchasePricingDetail.SupplierCode = "FT001078";
                    partPurchasePricingDetail.PartName = partSpecialTreatyPrice.SparePartName;
                    partPurchasePricingDetail.PartCode = partSpecialTreatyPrice.SparePartCode;
                    partPurchasePricingDetail.Price = partSpecialTreatyPrice.TreatyPrice;
                    partsPurchasePricingDetails.Add(partPurchasePricingDetail);
                }
                this.DomainContext.生成特殊协议采购价申请单(partsPurchasePricingChange, partsPurchasePricingDetails);
            }

            this.Clear();
            base.OnEditSubmitted();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public ObservableCollection<PartsSalesCategory> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<PartsSalesCategory>());
            }
        }


        public string CustomerCode {
            get {
                return this.customerCode;
            }
            set {
                this.customerCode = value;
                this.OnPropertyChanged("CustomerCode");
            }
        }

        public string CustomerName {
            get {
                return this.customerName;
            }
            set {
                this.customerName = value;
                this.OnPropertyChanged("CustomerName");
            }
        }
        public string PartsSalesCategoryCode {
            get {
                return this.partsSalesCategoryCode;
            }
            set {
                this.partsSalesCategoryCode = value;
                this.OnPropertyChanged("PartsSalesCategoryCode");
            }
        }

        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        public int PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }

        public string Remark {
            get {
                return this.remark;
            }
            set {
                this.remark = value;
                this.OnPropertyChanged("Remark");
            }
        }

        public ObservableCollection<PartsSpecialTreatyPrice> PartsSpecialTreatyPrices {
            get {
                return this.partsSpecialTreatyPrices ?? (this.partsSpecialTreatyPrices = new ObservableCollection<PartsSpecialTreatyPrice>());
            }
        }
    }
}