﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSpecialTreatyPriceForEditDataEditView {
        public PartsSpecialTreatyPriceForEditDataEditView() {
            this.InitializeComponent();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsSpecialTreatyPricesWithCompanyQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsSpecialTreatyPrice;
            }
        }

        protected override void OnEditSubmitting() {
            var partsSpecialTreatyPrice = this.DataContext as PartsSpecialTreatyPrice;
            if(partsSpecialTreatyPrice == null)
                return;
            if(partsSpecialTreatyPrice.HasValidationErrors)
                return;
            ((IEditableObject)partsSpecialTreatyPrice).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
