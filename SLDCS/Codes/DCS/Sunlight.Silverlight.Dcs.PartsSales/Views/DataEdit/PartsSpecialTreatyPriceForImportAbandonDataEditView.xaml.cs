﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsSpecialTreatyPriceForImportAbandonDataEditView {
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出作废配件特殊协议价模板.xlsx";
        private DataGridViewBase partsSpecialTreatyPriceForImportForEdit;

        protected DataGridViewBase PartsSpecialTreatyPriceForImportForEdit {
            get {
                if(this.partsSpecialTreatyPriceForImportForEdit == null) {
                    this.partsSpecialTreatyPriceForImportForEdit = DI.GetDataGridView("PartsSpecialTreatyPriceForImportForAbandon");
                    this.partsSpecialTreatyPriceForImportForEdit.DataContext = this.DataContext;
                }
                return this.partsSpecialTreatyPriceForImportForEdit;
            }
        }

        public PartsSpecialTreatyPriceForImportAbandonDataEditView() {
            InitializeComponent();
            this.Initializer.Register(CreateUI);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_TitlePartsSpecialTreatyPriceForImport;
            }
        }

        private void CreateUI() {
            this.ShowSaveButton();
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = PartsSalesUIStrings.DataEdit_TitlePartsSpecialTreatyPriceForImportList,
                Content = this.PartsSpecialTreatyPriceForImportForEdit
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportPartsSpecialTreatyPriceAbandonAsync(fileName);
            this.ExcelServiceClient.ImportPartsSpecialTreatyPriceAbandonCompleted -= ExcelServiceClient_ImportPartsSpecialTreatyPriceAbandonCompleted;
            this.ExcelServiceClient.ImportPartsSpecialTreatyPriceAbandonCompleted += ExcelServiceClient_ImportPartsSpecialTreatyPriceAbandonCompleted;
        }

        private void ExcelServiceClient_ImportPartsSpecialTreatyPriceAbandonCompleted(object sender, ImportPartsSpecialTreatyPriceAbandonCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!HasImportingError) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImport, 10);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.QueryPanel_QueeryItem_Title_Company_Code,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                         Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                        IsRequired = true
                    },
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
    }
}