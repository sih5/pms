﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PartsStockCoefficientDataEditView {
        private KeyValueManager keyValueManager;
        private DataGridViewBase priceDetailForEditDataGridView;
        private ObservableCollection<KeyValuePair> kvPartsStockCoefficientTypes = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvProductTypes = new ObservableCollection<KeyValuePair>();
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public PartsStockCoefficientDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.keyValueManager.LoadData(() => {
                this.KvPartsStockCoefficientTypes.Clear();
                foreach(var item in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvPartsStockCoefficientTypes.Add(new KeyValuePair {
                        Key = item.Key,
                        Value = item.Value
                    });
                }
                this.KvProductTypes.Clear();
                foreach(var item in this.KeyValueManager[this.kvNames[1]]) {
                    this.KvProductTypes.Add(new KeyValuePair {
                        Key = item.Key,
                        Value = item.Value
                    });
                }
            });
        }
        private void CreateUI() {
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register("价格区间", null, () => this.PartsStockCoefficientPriceForEditDataGridView);
            this.SubRoot.Children.Add(detailEditView);
        }
        private readonly string[] kvNames = {
            "PartsStockCoefficientType","ABCSetting_Type"
        };
        public ObservableCollection<KeyValuePair> KvPartsStockCoefficientTypes {
            get {
                return this.kvPartsStockCoefficientTypes ?? (this.kvPartsStockCoefficientTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        public ObservableCollection<KeyValuePair> KvProductTypes {
            get {
                return this.kvProductTypes ?? (this.kvProductTypes = new ObservableCollection<KeyValuePair>());
            }
        }
        private DataGridViewBase PartsStockCoefficientPriceForEditDataGridView {
            get {
                if(this.priceDetailForEditDataGridView == null) {
                    this.priceDetailForEditDataGridView = DI.GetDataGridView("PartsStockCoefficientPriceForEdit");
                    this.priceDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.priceDetailForEditDataGridView;
            }
        }
        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.KvPartsStockCoefficientTypes.Clear();
            this.KvProductTypes.Clear();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.KvPartsStockCoefficientTypes.Clear();
            this.KvProductTypes.Clear();
        }
        protected override void OnEditSubmitting() {
            var partsStockCoefficient = this.DataContext as PartsStockCoefficient;
            if(partsStockCoefficient == null || !this.PartsStockCoefficientPriceForEditDataGridView.CommitEdit())
                return;
            partsStockCoefficient.ValidationErrors.Clear();
            foreach(var item in partsStockCoefficient.PartsStockCoefficientPrices) {
                item.ValidationErrors.Clear();
            }
            if(partsStockCoefficient.Type == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_partsStockCoefficient_TypeIsNull);
                return;
            }
            if(partsStockCoefficient.PRODUCTTYPE == default(int) ) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_partsStockCoefficient_ProductTypeIsNull);
                return;
            }
            if(partsStockCoefficient.PartsStockCoefficientPrices.Any()) {
                if(partsStockCoefficient.PartsStockCoefficientPrices.Any(r => r.MinPrice == null)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_partsStockCoefficientDetails_MinpriceIsNull);
                    return;
                }
                if(partsStockCoefficient.PartsStockCoefficientPrices.Any(r => r.MinPrice == null)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_partsStockCoefficientDetails_MaxPriceIsNull);
                    return;
                }
                foreach(var item in partsStockCoefficient.PartsStockCoefficientPrices) {
                    if(partsStockCoefficient.PartsStockCoefficientPrices.Any(r => ((item.MinPrice > r.MinPrice && item.MinPrice < r.MaxPrice)||(item.MaxPrice > r.MinPrice && item.MaxPrice < r.MaxPrice))&&r.MinPrice!=item.MinPrice)||partsStockCoefficient.PartsStockCoefficientPrices.Where(r=>r.MinPrice==item.MinPrice|| r.MaxPrice==item.MaxPrice).Count()>1) {
                         UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_partsStockCoefficientDetails_priceReport);
                         return;
                    }
                }
            }
            foreach(var type in partsStockCoefficient.PartsStockCoefficientPrices) {
                type.ValidationErrors.Clear();
                ((IEditableObject)type).EndEdit();
            }
            if(partsStockCoefficient.HasValidationErrors || partsStockCoefficient.PartsStockCoefficientPrices.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsStockCoefficient).EndEdit();
            base.OnEditSubmitting();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsStockCoefficientForDetailsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }
        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.BusinessName_PartsStockCoefficient;
            }
        }

        public void OnCustomEditSubmitted() {
            this.KvPartsStockCoefficientTypes.Clear();
            this.KvProductTypes.Clear();
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }

    }
}
