﻿
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class PersonSalesCenterLinkDataEditView : INotifyPropertyChanged {
        private ObservableCollection<PersonSalesCenterLink> personSalesCenterLinks;
        private DataGridViewBase personnelForEditGridView;
        private int partsSalesCategoryId;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public PersonSalesCenterLinkDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += PersonSalesCenterLinkDataEditView_Loaded;
        }

        void PersonSalesCenterLinkDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.Title = PartsSalesUIStrings.DataEdit_Title_PersonSalesCenterLink;
            this.cbxPartsSalesCategory.IsEnabled = true;
        }
        public int PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }

        public DataGridViewBase PersonnelForEditGridView {
            get {
                if(this.personnelForEditGridView == null) {
                    this.personnelForEditGridView = DI.GetDataGridView("PersonnelForEdit");
                    this.personnelForEditGridView.DataContext = this;
                }
                return this.personnelForEditGridView;
            }
        }

        public virtual void CreateUI() {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.Status == (int)DcsBaseDataStatus.有效 && ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                this.cbxPartsSalesCategory.ItemsSource = loadOp.Entities;
                this.cbxPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(2));
            var detail = new DcsDetailDataEditView();
            detail.Register(PartsSalesUIStrings.DataEditView_Title_Personl, null, () => {
                var view = this.PersonnelForEditGridView;
                view.DomainContext = this.DomainContext;
                return view;
            });
            Grid.SetColumn(detail, 2);
            this.LayoutRoot.Children.Add(detail);
            this.cbxPartsSalesCategory.IsEnabled = true;
        }

        protected override bool OnRequestCanSubmit() {
            this.cbxPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        public ObservableCollection<PersonSalesCenterLink> PersonSalesCenterLinks {
            get {
                if(this.personSalesCenterLinks == null) {
                    this.personSalesCenterLinks = new ObservableCollection<PersonSalesCenterLink>();
                } return personSalesCenterLinks;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PersonnelForEditGridView.CommitEdit())
                return;
            if(cbxPartsSalesCategory.SelectedValue == null) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesCategoryIdIsNull);
                return;
            }
            foreach(var entity in this.PersonSalesCenterLinks)
                entity.ValidationErrors.Clear();
            foreach(var entity in this.PersonSalesCenterLinks) {
                entity.PartsSalesCategoryId = partsSalesCategoryId;
                entity.CompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                ((IEditableObject)entity).EndEdit();
            }
            foreach(var item in this.DomainContext.PersonSalesCenterLinks.Where(e => e.EntityState == EntityState.New).ToList()) {
                if(!PersonSalesCenterLinks.Contains(item))
                    this.DomainContext.PersonSalesCenterLinks.Remove(item);
            }
            if(this.PersonSalesCenterLinks.Any(r => r.ValidationErrors.Any()))
                return;
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            PartsSalesCategoryId = 0;
            this.PersonSalesCenterLinks.Clear();
        }

        private void LoadEntityToEdit(int id) {
            this.PersonSalesCenterLinks.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.Status == (int)DcsBaseDataStatus.有效 && ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                this.cbxPartsSalesCategory.ItemsSource = loadOp.Entities;
            }, null);
            this.DomainContext.Load(this.DomainContext.GetPersonSalesCenterLinksByPartsSalesCategoryIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.Title = PartsSalesUIStrings.DataEdit_Title_PersonSalesCenterLinkEdit;
                var personSalesCenterLinks = loadOp.Entities;
                var SequeueNumber = 1;
                foreach(var personSalesCenterLink in personSalesCenterLinks) {
                    personSalesCenterLink.Personnel.SequeueNumber = SequeueNumber;
                    PersonSalesCenterLinks.Add(personSalesCenterLink);
                    this.cbxPartsSalesCategory.SelectedItem = personSalesCenterLink.PartsSalesCategory;
                    SequeueNumber++;
                }
                this.cbxPartsSalesCategory.IsEnabled = false;
            }, null);
        }
    }
}
