﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class SalesUnitForMainTainDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategories, kvAccountGroups;
        private string ownerCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
        private DataGridViewBase salesUnitAffiWarehouseForEditDataGridView;
        private DataGridViewBase salesUnitAffiPersonnelForEditDataGridView;
        public event PropertyChangedEventHandler PropertyChanged;

        private DataGridViewBase SalesUnitAffiWarehouseForEditDataGridView {
            get {
                if(this.salesUnitAffiWarehouseForEditDataGridView == null) {
                    this.salesUnitAffiWarehouseForEditDataGridView = DI.GetDataGridView("SalesUnitAffiWarehouseForEdit");
                    this.salesUnitAffiWarehouseForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.salesUnitAffiWarehouseForEditDataGridView;
            }
        }

        private DataGridViewBase SalesUnitAffiPersonnelForEditDataGridView {
            get {
                if(this.salesUnitAffiPersonnelForEditDataGridView == null) {
                    this.salesUnitAffiPersonnelForEditDataGridView = DI.GetDataGridView("SalesUnitAffiPersonnelForEdit");
                    this.salesUnitAffiPersonnelForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.salesUnitAffiPersonnelForEditDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private readonly string[] kvNames = {
            "SalesUnit_SubmitCompanyId"
        };

        public object KvSubmitCompanyIds {
            get {
                return this.keyValueManager[this.kvNames[0]];
            }
        }

        public SalesUnitForMainTainDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.SalesUnitDataEditView_DataContextChanged;
        }

        private void SalesUnitDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var salesUnit = this.DataContext as SalesUnit;
            if(salesUnit == null)
                return;
            salesUnit.PropertyChanged -= this.SalesUnit_PropertyChanged;
            salesUnit.PropertyChanged += this.SalesUnit_PropertyChanged;
        }

        private void SalesUnit_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var salesUnit = this.DataContext as SalesUnit;
            if(salesUnit == null)
                return;
            if(e.PropertyName != "OwnerCompanyId")
                return;
            this.KvAccountGroups.Clear();
            salesUnit.AccountGroupId = default(int);
            if(salesUnit.OwnerCompanyId == default(int))
                return;
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadAccountGroup(salesUnit.OwnerCompanyId));
            else
                this.LoadAccountGroup(salesUnit.OwnerCompanyId);
            if(salesUnit.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId)
                this.OwnerCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
        }

        private void LoadAccountGroup(int ownerCompanyId) {
            this.DomainContext.Load(this.DomainContext.GetAccountGroupsQuery().Where(entity => entity.SalesCompanyId == ownerCompanyId && entity.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var accountGroup in loadOp.Entities) {
                    this.KvAccountGroups.Add(new KeyValuePair {
                        Key = accountGroup.Id,
                        Value = accountGroup.Name
                    });
                }
            }, null);
        }

        private void CreateUI() {
            this.HideSaveButton();
            this.KvPartsSalesCategories.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(entity => entity.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && entity.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var partsSaledCategories = loadOp.Entities;
                foreach(var partsSalesCategory in partsSaledCategories) {
                    KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                }
            }, null);

            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1, 0, 0, 2));
            var salesUnitAffiWarehouseDetailDataEditView = new DcsDetailDataEditView();
            salesUnitAffiWarehouseDetailDataEditView.Register(PartsSalesUIStrings.DataEditView_GroupTitle_WarehouseInfo, null, () => this.SalesUnitAffiWarehouseForEditDataGridView);
            salesUnitAffiWarehouseDetailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(salesUnitAffiWarehouseDetailDataEditView);

            var salesUnitAffiPersonnelDetailDataEditView = new DcsDetailDataEditView();
            salesUnitAffiPersonnelDetailDataEditView.Register(PartsSalesUIStrings.DataEditView_GroupTitle_PersonnelInfo, null, () => this.SalesUnitAffiPersonnelForEditDataGridView);
            salesUnitAffiPersonnelDetailDataEditView.SetValue(Grid.RowProperty, 1);
            salesUnitAffiPersonnelDetailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(salesUnitAffiPersonnelDetailDataEditView);
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void LoadEntityToEdit(int id) {
            this.KvAccountGroups.Clear();
            this.DomainContext.Load(this.DomainContext.GetSalesUnitWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                if(entity.Company != null)
                    this.OwnerCompanyName = entity.Company.Name;
                this.SetObjectToEdit(entity);
                this.LoadAccountGroup(entity.OwnerCompanyId);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SalesUnit;
            }
        }
        protected override void Reset() {
            var salesUnit = this.DataContext as SalesUnit;
            if(salesUnit == null)
                return;
            if(this.DomainContext.SalesUnits.Contains(salesUnit))
                this.DomainContext.SalesUnits.Detach(salesUnit);
            var salesUnitAffiWarehouses = this.DomainContext.SalesUnitAffiWarehouses.ToArray();
            foreach(var salesUnitAffiWarehouse in salesUnitAffiWarehouses)
                this.DomainContext.SalesUnitAffiWarehouses.Detach(salesUnitAffiWarehouse);
        }
        protected override void OnEditSubmitting() {
            var salesUnit = this.DataContext as SalesUnit;
            if(salesUnit == null)
                return;
            salesUnit.ValidationErrors.Clear();
            if(!salesUnit.SubmitCompanyId.HasValue) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_SubmitCompanyIdIsNull);
                return;
            }
            if(salesUnit.PartsSalesCategoryId == default(int))
                salesUnit.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_PartsSalesCategoryIdIsNull, new[] {
                    "PartsSalesCategoryId"
                }));
            if(salesUnit.OwnerCompanyId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_OwnerCompanyIsNull);
                return;
            }
            if(salesUnit.AccountGroupId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_AccountGroupIsNull);
                return;
            }
            if(salesUnit.BranchId != salesUnit.OwnerCompanyId)
                if(salesUnit.SalesUnitAffiWarehouses.Count() > 1) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_WarehouseOnlyOne);
                    return;
                }
            if(salesUnit.SalesUnitAffiWarehouses.Any(e => e.WarehouseId == default(int))) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_WarehouseIsNull);
                return;
            }
            if(salesUnit.SalesUnitAffiPersonnels.Any(e => e.PersonnelId == default(int))) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_PersonnelIsNull);
                return;
            }
            if(salesUnit.AccountGroup.SalesCompanyId != salesUnit.OwnerCompanyId) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_AccountGroupAndOwnerCompanyId);
                return;
            }

            var salesUnitAffiWarehouses = DomainContext.SalesUnitAffiWarehouses.Where(r => !salesUnit.SalesUnitAffiWarehouses.Contains(r)).ToArray();
            foreach(var salesUnitAffiWarehouse in salesUnitAffiWarehouses) {
                DomainContext.SalesUnitAffiWarehouses.Remove(salesUnitAffiWarehouse);
            }

            foreach(var affiWarehouse in salesUnit.SalesUnitAffiWarehouses) {
                if(affiWarehouse.Warehouse.StorageCompanyId != salesUnit.OwnerCompanyId) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_WarehouseAndOwnerCompanyId);
                    return;
                }
                affiWarehouse.ValidationErrors.Clear();
                if(!affiWarehouse.HasValidationErrors)
                    ((IEditableObject)affiWarehouse).EndEdit();
            }
            foreach(var affiPersonnel in salesUnit.SalesUnitAffiPersonnels) {
                if(affiPersonnel.Personnel.CorporationId != salesUnit.OwnerCompanyId) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_PersonnelAndOwnerCompanyId);
                    return;
                }
                affiPersonnel.ValidationErrors.Clear();
                if(!affiPersonnel.HasValidationErrors)
                    ((IEditableObject)affiPersonnel).EndEdit();
            }
            if(salesUnit.HasValidationErrors || salesUnit.SalesUnitAffiPersonnels.Any(e => e.HasValidationErrors) || salesUnit.SalesUnitAffiWarehouses.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)salesUnit).EndEdit();

            if(salesUnit.EntityState == EntityState.New) {
                try {
                    if(salesUnit.Can生成销售组织)
                        salesUnit.生成销售组织(salesUnit.SalesUnitAffiWarehouses.Select(r => r.WarehouseId).ToArray());
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            } else {
                try {
                    if(salesUnit.Can修改销售组织)
                        salesUnit.修改销售组织(salesUnit.SalesUnitAffiWarehouses.Select(r => r.WarehouseId).ToArray());
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public string BranchName {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseName;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvAccountGroups {
            get {
                return this.kvAccountGroups ?? (this.kvAccountGroups = new ObservableCollection<KeyValuePair>());
            }
        }
        public string OwnerCompanyName {
            get {
                return this.ownerCompanyName;
            }
            set {
                this.ownerCompanyName = value;
                this.OnPropertyChanged("OwnerCompanyName");
            }
        }
    }
}
