﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using System.Windows.Shapes;
using Sunlight.Silverlight;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class SettlementAutomaticTaskSetDataEditView  {

        //public event PropertyChangedEventHandler PropertyChanged;
        private KeyValueManager keyValueManager;

        private readonly string[] kvNames = { "SettlementAutomaticTaskStatus" };
        public ObservableCollection<KeyValuePair> ProductLineTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSettlementAutomaticTaskSetsQuery().Where(v => v.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.DcsComboBoxPartsSalesCategoryName.IsEnabled = false;
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var settlementAutomaticTaskSet = this.DataContext as SettlementAutomaticTaskSet;
            if(settlementAutomaticTaskSet == null)
                return;
            if(string.IsNullOrEmpty(settlementAutomaticTaskSet.BrandName)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_CustomerOrderPriceGrade_PartsSalesCategoryIsNull);
                return;
            }
            if(!settlementAutomaticTaskSet.AutomaticSettleTime.HasValue) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_SettlementAutomaticTaskSet_AutomaticSettleTime);
                return;
            }
            if(!settlementAutomaticTaskSet.SettleStartTime.HasValue) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_SettlementAutomaticTaskSet_SettleStartTime);
                return;
            }
            if(!settlementAutomaticTaskSet.SettleEndTime.HasValue) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_SettlementAutomaticTaskSet_SettleStartTime);
                return;
            }
            settlementAutomaticTaskSet.SettleEndTime = DateTime.Parse(DateTime.Parse(settlementAutomaticTaskSet.SettleEndTime.ToString()).ToString("yyyy-MM-dd 23:59:59"));
            if(settlementAutomaticTaskSet.SettleStartTime > settlementAutomaticTaskSet.SettleEndTime) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_SettlementAutomaticTaskSet_SettleEndTime);
                return;
            }
            if(!this.DomainContext.HasChanges)
                return;
            if(settlementAutomaticTaskSet.HasValidationErrors)
                return;
            ((IEditableObject)settlementAutomaticTaskSet).EndEdit();
            base.OnEditSubmitting();
        }

        protected override void Reset() {
            this.DcsComboBoxPartsSalesCategoryName.IsEnabled = true;
        }

        public SettlementAutomaticTaskSetDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Loaded += SettlementAutomaticTaskSetDataEditView_Loaded;
        }

        private void SettlementAutomaticTaskSetDataEditView_Loaded(object sender, RoutedEventArgs e) {
            var settlementAutomaticTaskSet = this.DataContext as SettlementAutomaticTaskSet;
            if(settlementAutomaticTaskSet == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && ex.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.DcsComboBoxPartsSalesCategoryName.ItemsSource = loadOp.Entities;
            }, null);
        }
    }
}
