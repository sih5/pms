﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class SpecialTreatyPriceChangeDataEditView {
        public SpecialTreatyPriceChangeDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += SpecialTreatyPriceChangeDataEditView_DataContextChanged;
        }



        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
                        if(specialTreatyPriceChange == null)
                            return;
                        if(!specialTreatyPriceChange.BrandId.HasValue) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_BrandIsNull);
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportSpecialPriceChangeListAsync(specialTreatyPriceChange.BrandId.Value, e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportSpecialPriceChangeListCompleted -= ExcelServiceClient_ImportSpecialPriceChangeListCompleted;
                        this.excelServiceClient.ImportSpecialPriceChangeListCompleted += ExcelServiceClient_ImportSpecialPriceChangeListCompleted;
                    };
                }
                return this.uploader;
            }
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void ExcelServiceClient_ImportSpecialPriceChangeListCompleted(object sender, ImportSpecialPriceChangeListCompletedEventArgs e) {
            var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            if(specialTreatyPriceChange == null)
                return;
            foreach(var detail in specialTreatyPriceChange.SpecialPriceChangeLists) {
                specialTreatyPriceChange.SpecialPriceChangeLists.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    var specialPriceChangeList = new SpecialPriceChangeList {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        SpecialTreatyPrice = decimal.Round(data.SpecialTreatyPrice, 2),
                        PurchasePrice = data.PurchasePrice,
                        Remark = data.Remark,
                    };
                    this.DomainContext.Load(this.DomainContext.GetPartsSalesPricesQuery().Where(r => r.PartsSalesCategoryId == specialTreatyPriceChange.BrandId && r.SparePartId == data.SparePartId), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            loadOp1.MarkErrorAsHandled();
                            return;
                        }
                        var salesPrice = loadOp1.Entities.FirstOrDefault();
                        if(salesPrice != null) {
                            specialPriceChangeList.RetailPrice = salesPrice.SalesPrice;
                            specialPriceChangeList.Ratio = specialPriceChangeList.SpecialTreatyPrice.HasValue && specialPriceChangeList.RetailPrice.HasValue ? (double?)specialPriceChangeList.SpecialTreatyPrice.Value / (double)specialPriceChangeList.RetailPrice.Value : null;
                        }
                    }, null);
                    this.DomainContext.Load(this.DomainContext.GetEnterprisePartsCostsQuery().Where(r => r.PartsSalesCategoryId == specialTreatyPriceChange.BrandId && r.SparePartId == data.SparePartId && r.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            loadOp1.MarkErrorAsHandled();
                            return;
                        }
                        var salesPrice = loadOp1.Entities.FirstOrDefault();
                        if(salesPrice != null) {
                            specialPriceChangeList.CostPrice = salesPrice.CostPrice;
                        }
                    }, null);
                    specialTreatyPriceChange.SpecialPriceChangeLists.Add(specialPriceChangeList);
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void SpecialTreatyPriceChangeDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            if(specialTreatyPriceChange == null)
                return;
            SetPropertyChanged();
            specialTreatyPriceChange.SpecialPriceChangeLists.EntityAdded += SpecialPriceChangeLists_EntityAdded;
        }

        private void SetPropertyChanged() {
            var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            if(specialTreatyPriceChange == null)
                return;
            foreach(var specialPriceChangeList in specialTreatyPriceChange.SpecialPriceChangeLists) {
                specialPriceChangeList.PropertyChanged -= SpecialPriceChangeList_PropertyChanged;
                specialPriceChangeList.PropertyChanged += SpecialPriceChangeList_PropertyChanged;
            }
        }

        void SpecialPriceChangeLists_EntityAdded(object sender, EntityCollectionChangedEventArgs<SpecialPriceChangeList> e) {
            SetPropertyChanged();
        }


        private void SpecialPriceChangeList_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var specialPriceChangeList = sender as SpecialPriceChangeList;
            if(specialPriceChangeList == null)
                return;
            switch(e.PropertyName) {
                case "SpecialTreatyPrice":
                    specialPriceChangeList.SpecialTreatyPrice = specialPriceChangeList.SpecialTreatyPrice.HasValue ? decimal.Round((decimal)specialPriceChangeList.SpecialTreatyPrice, 2) : specialPriceChangeList.SpecialTreatyPrice;
                    specialPriceChangeList.Ratio = specialPriceChangeList.SpecialTreatyPrice.HasValue && specialPriceChangeList.RetailPrice.HasValue ? (double?)specialPriceChangeList.SpecialTreatyPrice.Value / (double)specialPriceChangeList.RetailPrice.Value : null;
                    break;
            }
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }
        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.DataEdit_BusinessName_SpecialTreatyPriceChange;
            }
        }
        private DataGridViewBase specialTreatyPriceChangeForEditDataGridView;

        private DataGridViewBase SpecialTreatyPriceChangeForEditDataGridView {
            get {
                if(this.specialTreatyPriceChangeForEditDataGridView == null) {
                    this.specialTreatyPriceChangeForEditDataGridView = DI.GetDataGridView("SpecialTreatyPriceChangeForEdit");
                    this.specialTreatyPriceChangeForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.specialTreatyPriceChangeForEditDataGridView;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        protected override void OnEditSubmitting() {
            if(!this.SpecialTreatyPriceChangeForEditDataGridView.CommitEdit())
                return;
            var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            specialTreatyPriceChange.ValidationErrors.Clear();
            foreach(var item in specialTreatyPriceChange.SpecialPriceChangeLists)
                item.ValidationErrors.Clear();
            if(specialTreatyPriceChange == null)
                return;
            specialTreatyPriceChange.Path = FileUploadDataEditPanels.FilePath;
            if(!specialTreatyPriceChange.BrandId.HasValue)
                specialTreatyPriceChange.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_CustomerOrderPriceGrade_PartsSalesCategoryIsNull, new[] {
                    "BrandId"
                }));
            if(!specialTreatyPriceChange.CorporationId.HasValue)
                specialTreatyPriceChange.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_Corporation, new[] {
                    "CorporationCode"
                }));
            if(!specialTreatyPriceChange.ValidationTime.HasValue)
                specialTreatyPriceChange.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_ValidationTime, new[] {
                    "ValidationTime"
                }));
            if(!specialTreatyPriceChange.ExpireTime.HasValue)
                specialTreatyPriceChange.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_ExpireTime, new[] {
                    "ExpireTime"
                }));
            foreach(var relation in specialTreatyPriceChange.SpecialPriceChangeLists.Where(e => e.SpecialTreatyPrice <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_SpecialTreatyPrice, relation.SparePartCode));
                return;
            }
            foreach(var errorSpareParts in specialTreatyPriceChange.SpecialPriceChangeLists.GroupBy(d => new {
                d.SparePartCode,
                d.SparePartName
            }).Where(d => d.Count() > 1)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPriceChangeDetail_DoubleSparePart, errorSpareParts.Key.SparePartName));
                return;
            }
            foreach(var errorSpareParts in specialTreatyPriceChange.SpecialPriceChangeLists) {
                if((!errorSpareParts.CostPrice.HasValue || errorSpareParts.CostPrice == 0) && (!errorSpareParts.PurchasePrice.HasValue || errorSpareParts.PurchasePrice == 0))
                    errorSpareParts.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_PurchasePrice));
            }          
            if(specialTreatyPriceChange.HasValidationErrors || specialTreatyPriceChange.SpecialPriceChangeLists.Any(e => e.HasValidationErrors))
                return;
            string purchasePriceA = "";
            //foreach(var specialPriceChangeList in specialTreatyPriceChange.SpecialPriceChangeLists) {
            //    if(specialPriceChangeList.SpecialTreatyPrice < specialPriceChangeList.PurchasePrice)
            //        purchasePriceA += string.Format("配件编号:{0} 的特殊协议价小于采购价\n", specialPriceChangeList.SparePartCode);
            //}
            var tipsList = specialTreatyPriceChange.SpecialPriceChangeLists.ToArray();
            int tipsCount = 0;
            for(int i = 0;i < tipsList.Count(); i++ ) {
                if(tipsCount==10) {
                    continue;
                }
                if(tipsList[i].SpecialTreatyPrice < tipsList[i].PurchasePrice) {
                    tipsCount += 1;
                    purchasePriceA += string.Format(PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_PriceSmall, tipsList[i].SparePartCode);
                }
            }
            if(specialTreatyPriceChange.SpecialPriceChangeLists.Count==0) {
                UIHelper.ShowNotification("请添加变更申请清单");
                return;
            }
            if(!string.IsNullOrEmpty(purchasePriceA)) {
                DcsUtils.Confirm(purchasePriceA + PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_Continue, () => {
                    ((IEditableObject)specialTreatyPriceChange).EndEdit();
                    try {
                        if(this.EditState == DataEditState.Edit) {
                            if(specialTreatyPriceChange.Can修改配件特殊协议价变更申请)
                                specialTreatyPriceChange.修改配件特殊协议价变更申请();
                        } else {
                            if(specialTreatyPriceChange.Can新增配件特殊协议价变更申请)
                                specialTreatyPriceChange.新增配件特殊协议价变更申请();
                        }
                    } catch(ValidationException ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                        return;
                    }
                    base.OnEditSubmitting();
                });
            } else {
                ((IEditableObject)specialTreatyPriceChange).EndEdit();
                try {
                    if(this.EditState == DataEditState.Edit) {
                        if(specialTreatyPriceChange.Can修改配件特殊协议价变更申请)
                            specialTreatyPriceChange.修改配件特殊协议价变更申请();
                    } else {
                        if(specialTreatyPriceChange.Can新增配件特殊协议价变更申请)
                            specialTreatyPriceChange.新增配件特殊协议价变更申请();
                    }
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
                base.OnEditSubmitting();
            }
        }

        private ICommand exportFileCommand;
        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private const string EXPORT_DATA_FILE_NAME = "导出特殊配件协议价变更申请清单模板.xlsx";

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_AgreementPrice,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }
        private void CreateUI() {
            var editPanel = DI.GetDataEditPanel("SpecialTreatyPriceChange");
            editPanel.SetValue(Grid.RowProperty, 0);
            editPanel.SetValue(Grid.ColumnProperty, 0);
            this.LayoutRoot.Children.Add(editPanel);

            FileUploadDataEditPanels.Margin = new Thickness(54,5,0,0);
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 1);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 0);
            FileUploadDataEditPanels.FilePath = null;
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DetailPanel_Title_SparePartDetails, null, () => this.SpecialTreatyPriceChangeForEditDataGridView);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.Uploader.ShowFileDialog())
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            detailDataEditView.SetValue(Grid.RowSpanProperty, 2);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        private SpecialTreatyFileUploadDataEditPanel fileUploadDataEditPanels;
        public SpecialTreatyFileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (SpecialTreatyFileUploadDataEditPanel)DI.GetDataEditPanel("SpecialTreatyFileUpload"));
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSpecialTreatyPriceChangesWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null) {
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

    }
}
