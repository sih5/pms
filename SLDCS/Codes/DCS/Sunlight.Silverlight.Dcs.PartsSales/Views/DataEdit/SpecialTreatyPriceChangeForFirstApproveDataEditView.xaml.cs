﻿using System;
using System.Windows;
using Sunlight.Silverlight.Dcs.PartsSales.Panels.DataEdit;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core.Model;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand; 

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit
{
    public partial class SpecialTreatyPriceChangeForFirstApproveDataEditView
    {
        public SpecialTreatyPriceChangeForFirstApproveDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase specialPriceChangeListsForApproveDataGridView;

        private DataGridViewBase SpecialPriceChangeListsForApproveDataGridView
        {
            get
            {
                if (this.specialPriceChangeListsForApproveDataGridView == null)
                {
                    this.specialPriceChangeListsForApproveDataGridView = DI.GetDataGridView("SpecialPriceChangeList");
                    this.specialPriceChangeListsForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.specialPriceChangeListsForApproveDataGridView;
            }
        }


        private void CreateUI()
        {
            this.LayoutRoot.Children.Add(DI.GetDetailPanel("SpecialTreatyPriceChangeForFirstApprove"));

            FileUploadDataEditPanels.Margin = new Thickness(54,5,0,0);
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 1);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 0);
            FileUploadDataEditPanels.FilePath = null;
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(PartsSalesUIStrings.DetailPanel_Title_SparePartDetails, null, () => this.SpecialPriceChangeListsForApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            detailDataEditView.SetValue(Grid.RowSpanProperty, 2);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = PartsSalesUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            },true);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        private SpecialTreatyFileUploadDataEditPanel fileUploadDataEditPanels;
        public SpecialTreatyFileUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (SpecialTreatyFileUploadDataEditPanel)DI.GetDataEditPanel("SpecialTreatyFileUpload"));
            }
        }

        private void RejectCurrentData() {
            var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            if (specialTreatyPriceChange == null)
                return;
            if (string.IsNullOrEmpty(specialTreatyPriceChange.InitialApproverComment)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsPackingPropertyApp_ApprovalComment);
                return;
            }
            ((IEditableObject)specialTreatyPriceChange).EndEdit();
            try {
                if (specialTreatyPriceChange.Can驳回配件特殊协议价变更申请)
                    specialTreatyPriceChange.驳回配件特殊协议价变更申请();
            } catch (ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit()
        {
            return true;
        }

        protected override void Reset()
        {
            var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            if (specialTreatyPriceChange != null && this.DomainContext.SpecialTreatyPriceChanges.Contains(specialTreatyPriceChange))
                this.DomainContext.SpecialTreatyPriceChanges.Detach(specialTreatyPriceChange);
        }

        protected override void OnEditSubmitting()
        {
            var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            if (specialTreatyPriceChange == null)
                return;
            //去掉审核通过和驳回的按钮后，状态默认审核通过
            specialTreatyPriceChange.Status = 66;
            if ((!specialTreatyPriceChange.Status.Equals(66)) && (!specialTreatyPriceChange.Status.Equals(77)))
            {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_SpecialTreatyPriceChange_Status);
                return;
            }
            ((IEditableObject)specialTreatyPriceChange).EndEdit();
            try
            {
                if (specialTreatyPriceChange.Can初审配件特殊协议价变更申请)
                    specialTreatyPriceChange.初审配件特殊协议价变更申请();
            }
            catch (ValidationException ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title
        {
            get
            {
                return PartsSalesUIStrings.DataEdit_Title_SpecialTreatyPriceChange_FirApprove;
            }
        }


        private void LoadEntityToEdit(int id)
        {
            this.DomainContext.Load(this.DomainContext.GetSpecialTreatyPriceChangesWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null) { 
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
    }
}
