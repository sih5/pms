﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using System.Windows.Browser;
using System.Collections.ObjectModel;
using System.Linq;
using System;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class SpecialTreatyPriceChangeForImportDataEditView {

        private ICommand exportFileCommand;
        private ICommand importCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出配件配件特殊协议价模板.xlsx";

        private ObservableCollection<SpecialTreatyPriceChangeWithDetailExtend> specialTreatyPriceChangeWithDetailExtends;

        public ObservableCollection<SpecialTreatyPriceChangeWithDetailExtend> SpecialTreatyPriceChangeWithDetailExtends {
            get {
                if(this.specialTreatyPriceChangeWithDetailExtends == null) {
                    this.specialTreatyPriceChangeWithDetailExtends = new ObservableCollection<SpecialTreatyPriceChangeWithDetailExtend>();
                }
                return specialTreatyPriceChangeWithDetailExtends;
            }
        }
        private DataGridViewBase specialTreatyPriceChangeForImportForEdit;

        protected DataGridViewBase SpecialTreatyPriceChangeForImportForEdit {
            get {
                if(this.specialTreatyPriceChangeForImportForEdit == null) {
                    this.specialTreatyPriceChangeForImportForEdit = DI.GetDataGridView("SpecialTreatyPriceChangeForImportForEdit");
                    //this.specialTreatyPriceChangeForImportForEdit.DataContext = this.DataContext;
                    this.specialTreatyPriceChangeForImportForEdit.DomainContext = this.DomainContext;
                    this.specialTreatyPriceChangeForImportForEdit.DataContext = this;
                }
                return this.specialTreatyPriceChangeForImportForEdit;
            }
        }
        public SpecialTreatyPriceChangeForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(CreateUI);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_SpecialTreatyPriceChangeImport;
            }
        }

        private void CreateUI() {
            this.ShowSaveButton();
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = PartsSalesUIStrings.DataEdit_Title_SpecialTreatyPriceChangeImportList,
                Content = this.SpecialTreatyPriceChangeForImportForEdit
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitted() {
            if(this.SpecialTreatyPriceChangeWithDetailExtends.Any())
                this.SpecialTreatyPriceChangeWithDetailExtends.Clear();
        }

        protected override void OnEditCancelled() {
            if(this.SpecialTreatyPriceChangeWithDetailExtends.Any())
                this.SpecialTreatyPriceChangeWithDetailExtends.Clear();
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.QueryPanel_QueeryItem_Title_Company_Code,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_CorporationName,
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ValidationTime,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ExpireTime,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEdit_Title_SpecialTreatyPriceChange_SpecialPrice,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        public ICommand ImportCommand {
            get {
                if(importCommand == null)
                    this.InitializeCommand();
                this.importCommand = new DelegateCommand(() => this.Uploader.ShowFileDialog());
                return this.importCommand;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportSpecialTreatyPriceChangeAsync(e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportSpecialTreatyPriceChangeCompleted -= ExcelServiceClient_ImportSpecialTreatyPriceChangeCompleted;
                        this.excelServiceClient.ImportSpecialTreatyPriceChangeCompleted += ExcelServiceClient_ImportSpecialTreatyPriceChangeCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void ExcelServiceClient_ImportSpecialTreatyPriceChangeCompleted(object sender, ImportSpecialTreatyPriceChangeCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
            if(this.SpecialTreatyPriceChangeWithDetailExtends.Any())
                this.SpecialTreatyPriceChangeWithDetailExtends.Clear();
            int Num = 0;
            foreach(var detail in e.rightData) {
                detail.SequeueNumber = ++Num;
                SpecialTreatyPriceChangeWithDetailExtends.Add(detail);
            }
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        protected override void OnEditSubmitting() {
            if(!this.SpecialTreatyPriceChangeForImportForEdit.CommitEdit())
                return;
            var specialTreatyPriceChangeGroup = SpecialTreatyPriceChangeWithDetailExtends.GroupBy(i => new {
                i.BrandId,
                i.BrandNameStr,
                i.ValidationTime,
                i.ValidationTimeStr,
                i.ExpireTime,
                i.ExpireTimeStr,
                i.CorporationCodeStr,
                i.CorporationId,
                i.CorporationNameStr
            });
            foreach(var item in specialTreatyPriceChangeGroup) {
                SpecialTreatyPriceChange specialTreatyPriceChange = new SpecialTreatyPriceChange() {
                    BrandId = item.Key.BrandId,
                    BrandName = item.Key.BrandNameStr,
                    Code = GlobalVar.ASSIGNED_BY_SERVER,
                    CorporationCode = item.Key.CorporationCodeStr,
                    CorporationId = item.Key.CorporationId,
                    CorporationName = item.Key.CorporationNameStr,
                    ValidationTime=item.Key.ValidationTimeStr,
                    ExpireTime=item.Key.ExpireTimeStr,
                    Status = (int)DcsPartsPurchaseOrderStatus.新增,
                };
                foreach(SpecialTreatyPriceChangeWithDetailExtend specialTreatyPriceChangeWithDetailExtend in item) {
                    SpecialPriceChangeList specialPriceChangeList = new SpecialPriceChangeList() {
                        Remark = specialTreatyPriceChangeWithDetailExtend.RemarkStr,
                        SparePartCode = specialTreatyPriceChangeWithDetailExtend.SparePartCodeStr,
                        SparePartId= specialTreatyPriceChangeWithDetailExtend.SparePartId,
                        SparePartName=specialTreatyPriceChangeWithDetailExtend.SparePartNameStr,
                        SpecialTreatyPrice = specialTreatyPriceChangeWithDetailExtend.SpecialTreatyPrice
                    };
                    specialTreatyPriceChange.SpecialPriceChangeLists.Add(specialPriceChangeList);
                }
                if(!this.DomainContext.SpecialTreatyPriceChanges.Contains(specialTreatyPriceChange))
                    this.DomainContext.SpecialTreatyPriceChanges.Add(specialTreatyPriceChange);
                ((IEditableObject)specialTreatyPriceChange).EndEdit();
                try {
                    if(this.EditState == DataEditState.New) {
                        if(specialTreatyPriceChange.Can新增配件特殊协议价变更申请)
                            specialTreatyPriceChange.新增配件特殊协议价变更申请();
                    }
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
            }
            base.OnEditSubmitting();
        }
    }
}