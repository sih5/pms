﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class TowGradePartsNeedDataEditView : INotifyPropertyChanged {
        private DataGridViewBase towGradePartsNeedForEditDataGridView;
        private ObservableCollection<SecondClassStationPlan> secondClassStationPlans;
        private string remark;
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public TowGradePartsNeedDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += TowGradePartsNeedDataEditView_DataContextChanged;
        }

        private DataGridViewBase TowGradePartsNeedForEditDataGridView {
            get {
                if(this.towGradePartsNeedForEditDataGridView == null) {
                    this.towGradePartsNeedForEditDataGridView = DI.GetDataGridView("TowGradePartsNeedForEdit");
                    this.towGradePartsNeedForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.towGradePartsNeedForEditDataGridView;
            }
        }

        public ObservableCollection<SecondClassStationPlan> SecondClassStationPlans {
            get {
                return this.secondClassStationPlans ?? (this.secondClassStationPlans = new ObservableCollection<SecondClassStationPlan>());
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SecondClassStationPlan;
            }
        }

        public ObservableCollection<PartsSalesCategory> KvPartsSalesCategory {
            get {
                return this.KvPartsSalesCategory;
            }
        }

        private void CreateUI() {
            //this.DomainContext.Load(this.DomainContext.Get ByBreachIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.DcsComboBoxPartsSalesCategoryName.ItemsSource = loadOp.Entities;
            }, null);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DataGridView_Title_SecondClassStationPlanDetail, null, () => this.TowGradePartsNeedForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            this.DcsComboBoxPartsSalesCategoryName.SelectionChanged += DcsComboBoxPartsSalesCategoryName_SelectionChanged;
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void DcsComboBoxPartsSalesCategoryName_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var secondClassStationPlan = this.DataContext as SecondClassStationPlan;
            if(secondClassStationPlan == null)
                return;
            if(secondClassStationPlan.SecondClassStationPlanDetails.Count > 0) {
                //清空清单
                foreach(var detail in secondClassStationPlan.SecondClassStationPlanDetails)
                    detail.ValidationErrors.Clear();
                var detailList = secondClassStationPlan.SecondClassStationPlanDetails.ToList();
                foreach(var item in detailList)
                    secondClassStationPlan.SecondClassStationPlanDetails.Remove(item);
            }
        }

        protected override void OnEditCancelled() {
            if(SecondClassStationPlans != null)
                this.SecondClassStationPlans.Clear();
            Remark = string.Empty;
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            if(SecondClassStationPlans != null)
                this.SecondClassStationPlans.Clear();
            Remark = string.Empty;
            base.OnEditSubmitted();
        }

        protected override void OnEditSubmitting() {
            if(!this.TowGradePartsNeedForEditDataGridView.CommitEdit())
                return;
            var secondClassStationPlan = this.DataContext as SecondClassStationPlan;
            if(secondClassStationPlan == null)
                return;
            if(string.IsNullOrEmpty(secondClassStationPlan.SecondClassStationName))
                secondClassStationPlan.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_SecondClassStationPlan_SecondClassStationNameIsNull, new[] {
                    "SecondClassStationName"
                }));
            if(string.IsNullOrEmpty(secondClassStationPlan.FirstClassStationName))
                secondClassStationPlan.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_SecondClassStationPlan_FirstClassStationNameIsNull, new[] {
                    "FirstClassStationName"
                }));
            if(secondClassStationPlan.PartsSalesCategoryId == default(int))
                secondClassStationPlan.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesCategoryIdIsNull, new[] {
                    "PartsSalesCategoryId"
                }));
            if(secondClassStationPlan.SecondClassStationPlanDetails == null) {
                return;
            }

            if(!secondClassStationPlan.SecondClassStationPlanDetails.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SecondClassStationPlan_SecondClassStationPlanDetailsIsNull);
                return;
            }

            secondClassStationPlan.ValidationErrors.Clear();

            if(secondClassStationPlan.SecondClassStationPlanDetails.Any(r => !r.Quantity.HasValue || r.Quantity.Value <= 0)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Title_TowGradePartsNeed_Quantity);
                return;
            }

            foreach(var entity in secondClassStationPlan.SecondClassStationPlanDetails) {
                ((IEditableObject)entity).EndEdit();
            }

            try {
                if(this.EditState == DataEditState.Edit)
                    if(secondClassStationPlan.Can修改二级站需求计划)
                        secondClassStationPlan.修改二级站需求计划();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }

            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSecondClassStationPlanWithDetailQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    var sequeueNumber = 1;
                    foreach(var detail in entity.SecondClassStationPlanDetails)
                        detail.SequeueNumber = sequeueNumber++;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public string Remark {
            get {
                return this.remark;
            }
            set {
                this.remark = value;
                this.OnPropertyChanged("Remark");
            }
        }

        private void TowGradePartsNeedDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var secondClassStationPlan = this.DataContext as SecondClassStationPlan;
            if(secondClassStationPlan == null || secondClassStationPlan.Id == default(int))
                return;
            var domainContext = this.DomainContext as DcsDomainContext;
            domainContext.Load(domainContext.GetSecondClassStationPlanDetailsQuery().Where(s => s.SecondClassStationPlanId == secondClassStationPlan.Id), loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.ToList();
                int number = entity.Count,
                    maxSerialNumber = 1;
                for(int i = 0; i < number; i++) {
                    entity[i].SequeueNumber = maxSerialNumber;
                    maxSerialNumber++;
                }
            }, null);
        }
    }
}
