﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class TowGradePartsNeedManagerApproveDataEditView {
        private DataGridViewBase towGradePartsNeedManagerForEditDataGridView;
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return kvPartsSalesCategories;
            }
        }

        public TowGradePartsNeedManagerApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase TowGradePartsNeedManagerForEditDataGridView {
            get {
                if(this.towGradePartsNeedManagerForEditDataGridView == null) {
                    this.towGradePartsNeedManagerForEditDataGridView = DI.GetDataGridView("TowGradePartsNeedManagerForApproveEdit");
                    this.towGradePartsNeedManagerForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.towGradePartsNeedManagerForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.kvPartsSalesCategories.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError)
                    return;
                this.KvPartsSalesCategories.Clear();

                foreach(var partsSalesCategory in loadOption.Entities) {
                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                }
            }, null);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(PartsSalesUIStrings.DataGridView_Title_SecondClassStationPlanDetail, null, () => this.TowGradePartsNeedManagerForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var secondClassStationPlan = this.DataContext as SecondClassStationPlan;
            if(secondClassStationPlan == null)
                return;
            secondClassStationPlan.ValidationErrors.Clear();
            foreach(var item in secondClassStationPlan.SecondClassStationPlanDetails) {
                item.ValidationErrors.Clear();
                if(item.ProcessMode == null) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_TowGradePartsNeed_ProcessMode);
                    return;
                }
                if(!item.HasValidationErrors)
                    ((IEditableObject)item).EndEdit();
            }
            if(secondClassStationPlan.HasValidationErrors || secondClassStationPlan.SecondClassStationPlanDetails.Any(item => item.HasValidationErrors))
                return;
            try {
                if(secondClassStationPlan.Can审核二级站需求计划)
                    secondClassStationPlan.审核二级站需求计划();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            ((IEditableObject)secondClassStationPlan).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSecondClassStationPlansQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
                this.Title = PartsSalesUIStrings.DataEdit_Title_TowGradePartsNeed_Approve;
            }, null);
        }
    }
}
