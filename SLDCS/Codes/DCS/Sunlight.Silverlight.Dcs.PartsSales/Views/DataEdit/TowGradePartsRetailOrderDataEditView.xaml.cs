﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class TowGradePartsRetailOrderDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<KeyValuePair> kvBranches;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategoryNames;
        private DataGridViewBase towGradePartsRetailOrderForEditDataGridView;
        private bool isCustomer, customIsEnabled;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出二级站配件零售清单模板.xlsx";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private string strFileName;
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
                        if(dealerPartsRetailOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportDealerPartsRetailOrderAsync(dealerPartsRetailOrder.PartsSalesCategoryId, dealerPartsRetailOrder.DealerId, (int)dealerPartsRetailOrder.SubDealerId, e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportDealerPartsRetailOrderCompleted -= excelServiceClient_ImportDealerPartsRetailOrderCompleted;
                        this.excelServiceClient.ImportDealerPartsRetailOrderCompleted += excelServiceClient_ImportDealerPartsRetailOrderCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void excelServiceClient_ImportDealerPartsRetailOrderCompleted(object sender, ImportDealerPartsRetailOrderCompletedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            foreach(var detail in dealerPartsRetailOrder.DealerRetailOrderDetails) {
                dealerPartsRetailOrder.DealerRetailOrderDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                int serialNum = 1;
                foreach(var data in e.rightData) {
                    dealerPartsRetailOrder.DealerRetailOrderDetails.Add(new DealerRetailOrderDetail {
                        SerialNumber = serialNum++,
                        PartsId = data.PartsId,
                        PartsCode = data.PartsCode,
                        PartsName = data.PartsName,
                        Quantity = data.Quantity,
                        Price = data.Price,
                        Remark = data.Remark
                    });
                }
                dealerPartsRetailOrder.TotalAmount = dealerPartsRetailOrder.DealerRetailOrderDetails.Sum(r => r.Quantity * r.Price);
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }
        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }
        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage( PartsSalesUIStrings.DataEdit_Validation_Import_CloseFile);
                    e.Handled = true;
                }
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        private readonly string[] kvNames = {
            "DealerPartsRetailOrder_RetailOrderType"
        };
        private KeyValueManager keyValueManager;
        public object KvRetailOrderTypes {
            get {
                return this.keyValueManager[this.kvNames[0]];
            }
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public DataGridViewBase TowGradePartsRetailOrderForEditDataGridView {
            get {
                if(this.towGradePartsRetailOrderForEditDataGridView == null) {
                    this.towGradePartsRetailOrderForEditDataGridView = DI.GetDataGridView("TowGradePartsRetailOrderForEdit");
                    this.towGradePartsRetailOrderForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.towGradePartsRetailOrderForEditDataGridView;
            }
        }
        public ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches ?? (this.kvBranches = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategoryNames {
            get {
                return this.kvPartsSalesCategoryNames ?? (this.kvPartsSalesCategoryNames = new ObservableCollection<KeyValuePair>());
            }
        }


        public TowGradePartsRetailOrderDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
            isCustomer = true;
            this.CustomIsEnabled = true;
            this.ptbCustomer.IsEnabled = true;
        }
        private void ShowFileDialog() {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            if(dealerPartsRetailOrder.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_DealerPartsRetailOrder_PartsSalesCategoryNameIsNull);
                return;
            }
            this.Uploader.ShowFileDialog();
        }

        private void CreateUI() {
            InitializeComponent();
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1));
            var towGradeDataEditView = new DcsDetailDataEditView();
            towGradeDataEditView.Register(PartsSalesUIStrings.DataEditView_GroupTitle_PartsSalesOrderDetail, null, () => this.TowGradePartsRetailOrderForEditDataGridView);
            towGradeDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_ImportNew,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(() => ShowFileDialog())
            });
            towGradeDataEditView.RegisterButton(new ButtonItem {
                Title = PartsSalesUIStrings.DataEditPanel_Action_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            towGradeDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(towGradeDataEditView);
            var queryWindowRetailOrderCustomer = DI.GetQueryWindow("RetailOrderCustomer");
            if(queryWindowRetailOrderCustomer != null)
                queryWindowRetailOrderCustomer.SelectionDecided += this.QueryWindowRetailOrderCustomer_SelectionDecided;
            this.ptbCustomer.PopupContent = queryWindowRetailOrderCustomer;

            this.DomainContext.Load(DomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranches.Clear();
                foreach(var branch in loadOp.Entities)
                    this.KvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                        UserObject = branch
                    });
            }, null);

        }
        private void queryWindowSubDealer_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var subDealer = queryWindow.SelectedEntities.Cast<SubDealer>().SingleOrDefault();
            if(subDealer == null)
                return;
            var towGradePartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(towGradePartsRetailOrder == null)
                return;
            towGradePartsRetailOrder.SubDealerName = subDealer.Name;
            towGradePartsRetailOrder.CustomerCellPhone = subDealer.ManagerMobile;
            towGradePartsRetailOrder.CustomerPhone = subDealer.ManagerPhoneNumber;
            towGradePartsRetailOrder.CustomerUnit = string.Empty;
            towGradePartsRetailOrder.Customer = string.Empty;
            isCustomer = false;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        private void QueryWindowRetailOrderCustomer_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var retailOrderCustomer = queryWindow.SelectedEntities.Cast<RetailOrderCustomer>().SingleOrDefault();
            if(retailOrderCustomer == null)
                return;
            var towGradePartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(towGradePartsRetailOrder == null)
                return;
            towGradePartsRetailOrder.Customer = retailOrderCustomer.CustomerName;
            towGradePartsRetailOrder.CustomerCellPhone = retailOrderCustomer.CustomerCellPhone;
            towGradePartsRetailOrder.CustomerPhone = retailOrderCustomer.CustomerPhone;
            towGradePartsRetailOrder.CustomerUnit = retailOrderCustomer.CustomerUnit;
            towGradePartsRetailOrder.Address = retailOrderCustomer.Address;
            isCustomer = false;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void OnEditSubmitting() {
            var towGradePartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(towGradePartsRetailOrder == null || !this.TowGradePartsRetailOrderForEditDataGridView.CommitEdit())
                return;

            towGradePartsRetailOrder.ValidationErrors.Clear();
            foreach(var item in towGradePartsRetailOrder.DealerRetailOrderDetails) {
                item.ValidationErrors.Clear();
            }

            foreach(var relation in towGradePartsRetailOrder.DealerRetailOrderDetails.Where(e => e.Quantity <= 0)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Validation_PartsRetailGuidePrice_RetailGuidePriceLessZero, relation.PartsCode));
                return;
            }
            if(string.IsNullOrWhiteSpace(towGradePartsRetailOrder.BranchName)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_BranchIdIsNull);
                return;
            }

            if(string.IsNullOrWhiteSpace(towGradePartsRetailOrder.SalesCategoryName)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_CustomerOrderPriceGrade_PartsSalesCategoryIsNull);
                return;
            }
            if(!towGradePartsRetailOrder.DealerRetailOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_DealerPartsRetailOrder_DealerRetailOrderDetailIsNull);
                return;
            }

            ((IEditableObject)towGradePartsRetailOrder).EndEdit();
            if(towGradePartsRetailOrder.EntityState == EntityState.New) {
                if(towGradePartsRetailOrder.Can生成服务站配件零售订单)
                    towGradePartsRetailOrder.生成服务站配件零售订单();
            } else {
                if(towGradePartsRetailOrder.Can修改服务站配件零售订单)
                    towGradePartsRetailOrder.修改服务站配件零售订单();
            }

            if(isCustomer) {
                RetailOrderCustomer retailOrderCustomer = new RetailOrderCustomer();
                retailOrderCustomer.企业Id = BaseApp.Current.CurrentUserData.EnterpriseId;
                retailOrderCustomer.CustomerName = towGradePartsRetailOrder.Customer;
                retailOrderCustomer.CustomerPhone = towGradePartsRetailOrder.CustomerPhone;
                retailOrderCustomer.CustomerUnit = towGradePartsRetailOrder.CustomerUnit;
                retailOrderCustomer.CustomerCellPhone = towGradePartsRetailOrder.CustomerCellPhone;
                retailOrderCustomer.Address = towGradePartsRetailOrder.Address;
                retailOrderCustomer.Status = (int)DcsBaseDataStatus.有效;
                ((IEditableObject)retailOrderCustomer).EndEdit();
                DomainContext.RetailOrderCustomers.Add(retailOrderCustomer);
            }
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRetailOrderWithDetailsQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
        protected override string BusinessName {
            get {
                return PartsSalesUIStrings.BusinessName_TowGradePartsRetailOrder;

            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void DealerPartsRetailOrder_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var towGradePartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(towGradePartsRetailOrder == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.BranchId == towGradePartsRetailOrder.BranchId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategoryNames.Clear();
                foreach(var partsSalesCategory in loadOp.Entities) {
                    this.KvPartsSalesCategoryNames.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name,
                        UserObject = partsSalesCategory
                    });
                }
            }, null);
        }

        public bool CustomIsEnabled {
            get {
                return this.customIsEnabled;
            }
            set {
                if(this.customIsEnabled == value)
                    return;
                this.customIsEnabled = value;
                this.OnPropertyChanged("CustomIsEnabled");
            }
        }
        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_Quantity,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name=PartsSalesUIStrings.DataEdit_Text_DealerPartsRetailOrder_Price,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark,
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

    }
}
