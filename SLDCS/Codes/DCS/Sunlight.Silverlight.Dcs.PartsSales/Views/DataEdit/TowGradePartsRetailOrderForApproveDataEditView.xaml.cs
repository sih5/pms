﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class TowGradePartsRetailOrderForApproveDataEditView {
        private DataGridViewBase towGradePartsRetailOrderForEditDataGridView;

        public DataGridViewBase TowGradePartsRetailOrderForEditDataGridView {
            get {
                if(this.towGradePartsRetailOrderForEditDataGridView == null) {
                    this.towGradePartsRetailOrderForEditDataGridView = DI.GetDataGridView("DealerPartsRetailOrderForAppproveEdit");
                    this.towGradePartsRetailOrderForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.towGradePartsRetailOrderForEditDataGridView;
            }
        }
        public TowGradePartsRetailOrderForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private void CreateUI() {
            InitializeComponent();
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1));
            var towGradeDataEditView = new DcsDetailDataEditView();
            towGradeDataEditView.Register(PartsSalesUIStrings.DataEditView_GroupTitle_DealerPartsRetailOrder, null, () => this.TowGradePartsRetailOrderForEditDataGridView);
            towGradeDataEditView.UnregisterButton(towGradeDataEditView.InsertButton);
            towGradeDataEditView.UnregisterButton(towGradeDataEditView.DeleteButton);
            towGradeDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(towGradeDataEditView);
        }
        //导入
        private void ImportDataInternal() {

        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRetailOrderWithDetailsQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
        protected override void OnEditSubmitting() {
            var towGradePartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(towGradePartsRetailOrder == null)
                return;
            towGradePartsRetailOrder.ValidationErrors.Clear();
            ((IEditableObject)towGradePartsRetailOrder).EndEdit();
            if(towGradePartsRetailOrder.Can服务站审批二级站配件零售订单)
                towGradePartsRetailOrder.服务站审批二级站配件零售订单();

            base.OnEditSubmitting();
        }

        protected override void Reset() {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder != null && this.DomainContext.DealerPartsRetailOrders.Contains(dealerPartsRetailOrder))
                this.DomainContext.DealerPartsRetailOrders.Detach(dealerPartsRetailOrder);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.BusinessName_TowGradePartsRetailOrderForApprove;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }


    }
}
