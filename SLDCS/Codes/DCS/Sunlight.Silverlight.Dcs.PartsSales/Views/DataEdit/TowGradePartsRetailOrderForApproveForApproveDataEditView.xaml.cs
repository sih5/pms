﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class TowGradePartsRetailOrderForApproveForApproveDataEditView {
        private DataGridViewBase towGradePartsRetailOrderDetailForApproveDataGridView;

        public DataGridViewBase TowGradePartsRetailOrderDetailForApproveDataGridView {
            get {
                if(this.towGradePartsRetailOrderDetailForApproveDataGridView == null) {
                    this.towGradePartsRetailOrderDetailForApproveDataGridView = DI.GetDataGridView("TowGradePartsRetailOrderDetailForApproveForApprove");
                    this.towGradePartsRetailOrderDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.towGradePartsRetailOrderDetailForApproveDataGridView;
            }
        }

        public TowGradePartsRetailOrderForApproveForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return PartsSalesUIStrings.BusinessName_DealerPartsRetailOrderForApproveForApprove;
            }
        }

        private void DealerPartsRetailOrder_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            throw new NotImplementedException();
        }



        private void CreateUI() {
            InitializeComponent();
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsSalesUIStrings.DataEditView_GroupTitle_DealerPartsRetailOrder, null, () => this.TowGradePartsRetailOrderDetailForApproveDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsRetailOrderWithDetailsQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            dealerPartsRetailOrder.ValidationErrors.Clear();
            ((IEditableObject)dealerPartsRetailOrder).EndEdit();
            if(dealerPartsRetailOrder.Can审批二级站配件零售订单)
                dealerPartsRetailOrder.审批二级站配件零售订单();
            base.OnEditSubmitting();
        }
    }
}
