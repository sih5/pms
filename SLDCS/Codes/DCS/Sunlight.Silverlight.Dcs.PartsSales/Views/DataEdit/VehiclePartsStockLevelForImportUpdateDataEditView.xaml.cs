﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class VehiclePartsStockLevelForImportUpdateDataEditView  {
        private DataGridViewBase vehiclePartsStockLevelForImportForEdit;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出随车行配件库存高低限模板.xlsx";
        protected override string Title {
            get {
                return PartsSalesUIStrings.DataEdit_Title_VehiclePartsStockLevelForImport;
            }
        }
        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.vehiclePartsStockLevelForImportForEdit == null) {
                    this.vehiclePartsStockLevelForImportForEdit = DI.GetDataGridView("VehiclePartsStockLevelForImportForEdit");
                    this.vehiclePartsStockLevelForImportForEdit.DataContext = this.DataContext;
                }
                return this.vehiclePartsStockLevelForImportForEdit;
            }
        }
        public VehiclePartsStockLevelForImportUpdateDataEditView() {
            InitializeComponent();
            this.Initializer.Register(CreateUI);
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = PartsSalesUIStrings.DataEdit_Title_VehiclePartsStockLevelForImportList,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }
        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            //this.ExcelServiceClient.导入修改随车行配件仓库高低限Async(fileName);
            //this.ExcelServiceClient.导入修改随车行配件仓库高低限Completed -= ExcelServiceClient_导入修改随车行配件仓库高低限Completed;
            //this.ExcelServiceClient.导入修改随车行配件仓库高低限Completed += ExcelServiceClient_导入修改随车行配件仓库高低限Completed;
        }

        //private void ExcelServiceClient_导入修改随车行配件仓库高低限Completed(object sender, 导入修改随车行配件仓库高低限CompletedEventArgs e) {
        //    ShellViewModel.Current.IsBusy = false;
        //    this.ImportComplete();
        //    this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
        //    if(!HasImportingError) {
        //        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImport, 10);
        //    } else {
        //        if(!string.IsNullOrEmpty(e.errorMessage))
        //            UIHelper.ShowAlertMessage(e.errorMessage);
        //        if(!string.IsNullOrEmpty(e.errorDataFileName))
        //            this.ExportFile(e.errorDataFileName);
        //        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
        //    }
        //}
        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseName
                                            },
                                            new ImportTemplateColumn {
                                                Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                 Name=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_StockMaximum,
                                                //IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_StockMimimum,
                                                //IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_SafeStock,
                                                //IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
