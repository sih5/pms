﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit {
    public partial class WarehouseSequenceDataEditView {
        private readonly string[] kvNames = {
            "OrderAppDate"
        };
        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        private ObservableCollection<SalesUnit> SalesUnits = new ObservableCollection<SalesUnit>();
        private ObservableCollection<SalesUnitAffiWarehouse> SalesUnitAffiWarehouses = new ObservableCollection<SalesUnitAffiWarehouse>();
        private ObservableCollection<KeyValuePair> kvPartsSalesCategories;
        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<Warehouse> kvWarehouses;
        public ObservableCollection<Warehouse> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<Warehouse>());
            }
        }

        public object KvWeeks {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public WarehouseSequenceDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += WarehouseSequenceDataEditView_DataContextChanged;
        }

        private void WarehouseSequenceDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var warehouseSequence = this.DataContext as WarehouseSequence;
            if(warehouseSequence == null)
                return;
            warehouseSequence.PropertyChanged -= warehouseSequence_PropertyChanged;
            warehouseSequence.PropertyChanged += warehouseSequence_PropertyChanged;
        }

        private void warehouseSequence_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var warehouseSequence = this.DataContext as WarehouseSequence;
            if(warehouseSequence == null)
                return;
            switch(e.PropertyName) {
                case "DefaultWarehouseId":
                    var defaultWarehouse = this.KvWarehouses.Where(r => r.Id == warehouseSequence.DefaultWarehouseId).FirstOrDefault();
                    if(defaultWarehouse != null) {
                        warehouseSequence.DefaultWarehouseName = defaultWarehouse.Name;
                        if(defaultWarehouse.IsCentralizedPurchase == true) {
                            this.DefaultOutWarehouse.IsReadOnly = false;
                            this.DefaultOutWarehouse.IsEnabled = true;
                            var saleUnitIds = this.SalesUnits.Where(r => r.PartsSalesCategoryId == warehouseSequence.PartsSalesCategoryId && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId).Select(r => r.Id);
                            var salesUnitAffiWarehouseIds = this.SalesUnitAffiWarehouses.Where(r => saleUnitIds.Contains(r.SalesUnitId)).Select(r => r.WarehouseId).ToArray();
                            this.DefaultOutWarehouse.ItemsSource = this.KvWarehouses.Where(r => salesUnitAffiWarehouseIds.Contains(r.Id) && r.StorageCenter == defaultWarehouse.StorageCenter);
                        } else {
                            this.DefaultOutWarehouse.IsReadOnly = true;
                            this.DefaultOutWarehouse.IsEnabled = false;
                            this.DefaultOutWarehouse.ItemsSource = this.KvWarehouses;
                            warehouseSequence.DefaultOutWarehouseId = defaultWarehouse.Id;
                            warehouseSequence.DefaultOutWarehouseName = defaultWarehouse.Name;
                        }
                    }
                    break;
                case "FirstWarehouseId":
                    var firstWarehouse = this.KvWarehouses.Where(r => r.Id == warehouseSequence.FirstWarehouseId).FirstOrDefault();
                    if(firstWarehouse != null) {
                        warehouseSequence.FirstWarehouseName = firstWarehouse.Name;
                        if(firstWarehouse.IsCentralizedPurchase == true) {
                            this.FirstOutWarehouse.IsReadOnly = false;
                            this.FirstOutWarehouse.IsEnabled = true;
                            var saleUnitIds = this.SalesUnits.Where(r => r.PartsSalesCategoryId == warehouseSequence.PartsSalesCategoryId && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId).Select(r => r.Id);
                            var salesUnitAffiWarehouseIds = this.SalesUnitAffiWarehouses.Where(r => saleUnitIds.Contains(r.SalesUnitId)).Select(r => r.WarehouseId).ToArray();
                            this.FirstOutWarehouse.ItemsSource = this.KvWarehouses.Where(r => salesUnitAffiWarehouseIds.Contains(r.Id) && r.StorageCenter == firstWarehouse.StorageCenter);
                        } else {
                            this.FirstOutWarehouse.IsReadOnly = true;
                            this.FirstOutWarehouse.IsEnabled = false;
                            this.FirstOutWarehouse.ItemsSource = this.KvWarehouses;
                            warehouseSequence.FirstOutWarehouseId = firstWarehouse.Id;
                            warehouseSequence.FirstOutWarehouseName = firstWarehouse.Name;
                        }
                    }
                    break;
                case "SecoundWarehouseId":
                    var secoundWarehouse = this.KvWarehouses.Where(r => r.Id == warehouseSequence.SecoundWarehouseId).FirstOrDefault();
                    if(secoundWarehouse != null) {
                        warehouseSequence.SecoundWarehouseName = secoundWarehouse.Name;
                        if(secoundWarehouse.IsCentralizedPurchase == true) {
                            this.SecoundOutWarehouse.IsReadOnly = false;
                            this.SecoundOutWarehouse.IsEnabled = true;
                            var saleUnitIds = this.SalesUnits.Where(r => r.PartsSalesCategoryId == warehouseSequence.PartsSalesCategoryId && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId).Select(r => r.Id);
                            var salesUnitAffiWarehouseIds = this.SalesUnitAffiWarehouses.Where(r => saleUnitIds.Contains(r.SalesUnitId)).Select(r => r.WarehouseId).ToArray();
                            this.SecoundOutWarehouse.ItemsSource = this.KvWarehouses.Where(r => salesUnitAffiWarehouseIds.Contains(r.Id) && r.StorageCenter == secoundWarehouse.StorageCenter);
                        } else {
                            this.SecoundOutWarehouse.IsReadOnly = true;
                            this.SecoundOutWarehouse.IsEnabled = false;
                            this.SecoundOutWarehouse.ItemsSource = this.KvWarehouses;
                            warehouseSequence.SecoundOutWarehouseId = secoundWarehouse.Id;
                            warehouseSequence.SecoundOutWarehouseName = secoundWarehouse.Name;
                        }
                    }
                    break;
                case "ThirdWarehouseId":
                    var thridWarehouse = this.KvWarehouses.Where(r => r.Id == warehouseSequence.ThirdWarehouseId).FirstOrDefault();
                    if(thridWarehouse != null) {
                        warehouseSequence.ThirdWarehouseName = thridWarehouse.Name;
                        if(thridWarehouse.IsCentralizedPurchase == true) {
                            this.ThirdOutWarehouse.IsReadOnly = false;
                            this.ThirdOutWarehouse.IsEnabled = true;
                            var saleUnitIds = this.SalesUnits.Where(r => r.PartsSalesCategoryId == warehouseSequence.PartsSalesCategoryId && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId).Select(r => r.Id);
                            var salesUnitAffiWarehouseIds = this.SalesUnitAffiWarehouses.Where(r => saleUnitIds.Contains(r.SalesUnitId)).Select(r => r.WarehouseId).ToArray();
                            this.ThirdOutWarehouse.ItemsSource = this.KvWarehouses.Where(r => salesUnitAffiWarehouseIds.Contains(r.Id) && r.StorageCenter == thridWarehouse.StorageCenter);
                        } else {
                            this.ThirdOutWarehouse.IsReadOnly = true;
                            this.ThirdOutWarehouse.IsEnabled = false;
                            this.ThirdOutWarehouse.ItemsSource = this.KvWarehouses;
                            warehouseSequence.ThirdOutWarehouseId = thridWarehouse.Id;
                            warehouseSequence.ThirdOutWarehouseName = thridWarehouse.Name;
                        }
                    }
                    break;
            }
        }

        private void CreateUI() {
            var queryWindowCompany = DI.GetQueryWindow("CompanyWithType");
            queryWindowCompany.SelectionDecided += queryWindowCompany_SelectionDecided;
            this.ptbcustomerCompanyName.PopupContent = queryWindowCompany;

            this.KvPartsSalesCategories.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(entity => entity.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && entity.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var partsSaledCategories = loadOp.Entities;
                foreach(var partsSalesCategory in partsSaledCategories) {
                    KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                }
            }, null);

            this.DomainContext.Load(this.DomainContext.GetWarehousesWithBranchQuery().Where(entity => entity.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null)
                    return;
                KvWarehouses.Clear();
                var _entities = loadOp.Entities.OrderBy(i => i.Name);
                foreach(var warehouse in _entities) {
                    this.KvWarehouses.Add(warehouse);
                }
            }, null);

            this.DomainContext.Load(this.DomainContext.GetSalesUnitsQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null)
                    return;
                SalesUnits.Clear();
                foreach(var saleUnit in loadOp.Entities) {
                    this.SalesUnits.Add(saleUnit);
                }
            }, null);

            this.DomainContext.Load(this.DomainContext.GetSalesUnitAffiWarehousesQuery(), loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities == null)
                    return;
                this.SalesUnitAffiWarehouses.Clear();
                foreach(var saleUnit in loadOp.Entities) {
                    this.SalesUnitAffiWarehouses.Add(saleUnit);
                }
            }, null);
        }

        private void queryWindowCompany_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var selectedCompany = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if(selectedCompany == null)
                return;
            var warehouseSequence = this.DataContext as WarehouseSequence;
            if(warehouseSequence == null)
                return;
            warehouseSequence.CustomerCompanyId = selectedCompany.Id;
            warehouseSequence.CustomerCompanyCode = selectedCompany.Code;
            warehouseSequence.CustomerCompanyName = selectedCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetWarehouseSequencesQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                if(KvWarehouses.Any())
                    this.SetObjectToEdit(entity);
                else {
                    //TODO 临时解决 异步仓库未加载的问题
                    this.DomainContext.Load(this.DomainContext.GetWarehousesWithBranchQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        if(loadOp1.Entities == null)
                            return;
                        KvWarehouses.Clear();

                        var _entities = loadOp1.Entities.OrderBy(i => i.Name);
                        foreach(var warehouse in _entities) {
                            this.KvWarehouses.Add(warehouse);
                        }
                        this.SetObjectToEdit(entity);
                    }, null);
                }
            }, null);
        }

        protected override string BusinessName {
            get {
                return "出库仓库优先顺序管理";
            }
        }

        protected override void OnEditSubmitting() {
            var warehouseSequence = this.DataContext as WarehouseSequence;
            if(warehouseSequence == null)
                return;
            if(warehouseSequence.PartsSalesCategoryName == "集团配件") {
                UIHelper.ShowNotification("统购品牌无销售业务，无需维护发货仓库信息");
                return;
            }
            //判断首选仓库 第一仓库 第二仓库 第三仓库是否重复
            if(!warehouseSequence.DefaultWarehouseId.HasValue) {
                UIHelper.ShowNotification("首选仓库不能为空");
                return;
            }
            if(!warehouseSequence.DefaultOutWarehouseId.HasValue) {
                UIHelper.ShowNotification("首选出库仓库不能为空");
                return;
            }

            List<int> warehouseIds = new List<int>();
            warehouseIds.Add(warehouseSequence.DefaultWarehouseId.Value);
            if(warehouseSequence.FirstWarehouseId.HasValue && warehouseSequence.FirstWarehouseId > 0)
                warehouseIds.Add(warehouseSequence.FirstWarehouseId.Value);
            if(warehouseSequence.SecoundWarehouseId.HasValue && warehouseSequence.SecoundWarehouseId > 0)
                warehouseIds.Add(warehouseSequence.SecoundWarehouseId.Value);
            if(warehouseSequence.ThirdWarehouseId.HasValue && warehouseSequence.ThirdWarehouseId > 0)
                warehouseIds.Add(warehouseSequence.ThirdWarehouseId.Value);
            if(warehouseIds.Count > 1) {
                int count = warehouseIds.Distinct().Count();
                if(count < warehouseIds.Count) {
                    UIHelper.ShowNotification("仓库信息不能重复,请修改");
                    return;
                }
            }

            //查询集团配件 品牌ID
            int jtPartsSalesCategoryId = this.KvPartsSalesCategories.FirstOrDefault(r => r.Value == "集团配件").Key;
            this.DomainContext.Load(this.DomainContext.GetSalesUnitsQuery().
                   Where(r => r.Status == (int)DcsMasterDataStatus.有效
                     && (r.PartsSalesCategoryId == warehouseSequence.PartsSalesCategoryId
                       || r.PartsSalesCategoryId == jtPartsSalesCategoryId)), LoadBehavior.RefreshCurrent, loadOp => {
                           if(loadOp.HasError)
                               return;
                           var salesUnitIds = loadOp.Entities.Select(r => r.Id).ToArray();
                           if(salesUnitIds.Any()) {
                               this.DomainContext.Load(this.DomainContext.GetSalesUnitAffiWarehousesWithWarehouseAndSalesUnitBySalesUnitIdsQuery(salesUnitIds), LoadBehavior.RefreshCurrent, loadOp1 => {
                                   if(loadOp.HasError)
                                       return;
                                   var salesUnitAffiWarehouses = loadOp1.Entities.Select(r => r.WarehouseId).ToArray();
                                   if(salesUnitAffiWarehouses.Any()) {
                                       if(salesUnitAffiWarehouses.Intersect(warehouseIds).Count() < warehouseIds.Count()) {
                                           UIHelper.ShowNotification("所选仓库非本品牌仓库或统购库，不允许保存");
                                           return;
                                       } else
                                           base.OnEditSubmitting();
                                   } else {
                                       UIHelper.ShowNotification("所选仓库非本品牌仓库或统购库，不允许保存");
                                       return;
                                   }
                               }, null);
                           } else {
                               UIHelper.ShowNotification("所选仓库非本品牌仓库或统购库，不允许保存");
                               return;
                           }

                       }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
