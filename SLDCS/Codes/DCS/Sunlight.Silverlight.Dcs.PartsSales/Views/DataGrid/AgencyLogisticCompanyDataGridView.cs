﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class AgencyLogisticCompanyDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public AgencyLogisticCompanyDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem{
                        Name = "Code"
                    }, new ColumnItem{
                        Name = "Name"
                    }, new ColumnItem{
                        Name = "Remark"
                    }, new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    }, new ColumnItem{
                        Name = "ModifierName"
                    }, new ColumnItem{
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyLogisticCompany);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAgencyLogisticCompanies";
        }
    }
}
