﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class AgencyPartsOutboundBillDetailDataGridView : DcsDataGridViewBase {
        public AgencyPartsOutboundBillDetailDataGridView() {
            this.DataContextChanged += this.PartsOutboundBillDetailDataGridView_DataContextChanged;
        }

        private void PartsOutboundBillDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsOutboundBill = e.NewValue as AgencyPartsOutboundBillWithOtherInfo;
            if(partsOutboundBill == null || partsOutboundBill.PartsOutboundBillId == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsOutboundBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsOutboundBill.PartsOutboundBillId
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(APartsOutboundBillDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "SparePart.OverseasPartsFigure",
                        Title = PartsSalesUIStrings.QueryPanel_Title_VirtualSparePartQuery_OverseasPartsFigure
                    }, new ColumnItem {
                        Name = "SparePart.MeasureUnit",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_MeasureUnit
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_Title_APartsOutboundBillDetail_WarehouseAreaCode,
                        Name = "WarehouseAreaCode"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_OutboundQuantity,
                        Name = "OutboundAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementDetail_SettlementPrice,
                        Name = "SettlementPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_Remark,
                        Name = "Remark"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAgencyPartsOutboundBillDetailsWithSparePart";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["OutboundAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
        }
    }
}
