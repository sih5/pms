﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class AgencyPartsSalesOrderWithDetailsDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "PartsSalesOrder_SecondLevelOrderType", "PartsSalesOrder_Status", "PartsShipping_Method", "Company_Type"
        };
        public readonly ObservableCollection<KeyValuePair> KvWarehouses = new ObservableCollection<KeyValuePair>();
        public readonly ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();
        public AgencyPartsSalesOrderWithDetailsDataGridView() {
            this.LoadKeyValue();
            this.Initializer.Register(this.Initialize);
        }

        protected void LoadKeyValue() {
            this.KeyValueManager.Register(this.KvNames);
        }

        private void Initialize() {
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesOrderTypesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesOrderType in loadOp.Entities)
                    this.KvPartsSalesOrderTypes.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Id,
                        Value = partsSalesOrderType.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null) {
                    foreach(var warehouse in loadOp.Entities)
                        this.KvWarehouses.Add(new KeyValuePair {
                            Key = warehouse.Id,
                            Value = warehouse.Name
                        });
                }
            }, null);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    }, new ColumnItem {
                        Name = "Code"
                    },
                    new KeyValuesColumnItem {
                        Name = "WarehouseId",
                        KeyValueItems = this.KvWarehouses
                    },
                    new ColumnItem {
                        Name = "Province"
                    },
                    new ColumnItem {
                        Name = "City"
                    },
                    new ColumnItem {
                        Name = "SubmitCompanyCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Code
                    },
                    new ColumnItem {
                        Name = "SubmitCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_SubmitCompanyName
                    },
                    new KeyValuesColumnItem {
                        Name = "CustomerType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[3]]
                    },
                    new KeyValuesColumnItem {
                        Name = "PartsSalesOrderTypeId",
                        KeyValueItems = this.KvPartsSalesOrderTypes,
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_PartsSalesOrderType
                    },
                    new ColumnItem {
                        Name = "IfDirectProvision"
                    },                  
                    new ColumnItem {
                        Name = "IsDebt"
                    },
                    new ColumnItem {
                        Name = "Remark"
                    },
                    new ColumnItem {
                        Name = "TotalAmount"
                    },
                    new ColumnItem {
                        Name = "SubmitTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime
                    },
                    //new ColumnItem {
                    //    Name = "StopComment",
                    //    Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_StopReason
                    //},
                    new ColumnItem {
                        Name = "SalesUnitOwnerCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_SalesUnitOwnerCompanyName
                    },
                    new ColumnItem {
                        Name = "ReceivingAddress"
                    },
                    new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.KvNames[2]]
                    },
                    new ColumnItem {
                        Name = "RequestedDeliveryTime"
                    },
                    new ColumnItem {
                        Name = "StopComment"
                    },
                    new ColumnItem {
                        Name = "CreatorName"
                    },
                    new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime
                    },
                    new ColumnItem {
                        Name = "SubmitterName"
                    },
                    new ColumnItem {
                        Name = "ApproverName"
                    },
                    new ColumnItem {
                        Name = "ApproveTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime
                    },
                    new ColumnItem {
                        Name = "FirstApproveTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime,
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_FirstApproveTime
                    },
                    new ColumnItem {
                        Name = "AbandonerName"
                    },
                    new ColumnItem {
                        Name = "AbandonTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime
                    },
                    new ColumnItem {
                        Name = "IsTransSuccess"
                    },
                    new ColumnItem {
                        Name = "ERPSourceOrderCode",
                        Title = PartsSalesUIStrings.QueryPanel_Title_AgencyRetailerOrder_ERPSourceOrderCode
                    },
                    new ColumnItem {
                        Name = "IsBarter"
                    },
                    new ColumnItem {
                        Name = "IsAutoApprove"
                    },
                    new ColumnItem {
                        Name = "AutoApproveComment"
                    },
                    new ColumnItem {
                        Name = "AutoApproveTime"
                    },
                    new ColumnItem {
                        Name = "IfInnerDirectProvision",
                        Title = "是否内部转直供"
                    },new ColumnItem {
                        Name = "OverseasDemandSheetNo",
                        Title = "SAP订单编号"
                    },
                    new ColumnItem {
                        Name = "SalesCategoryName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach(var filter in compositeFilterItem.Filters.Where(item => item.MemberName != "CityName" && item.MemberName != "ProvinceName"))
                newCompositeFilterItem.Filters.Add(filter);
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            FilterItem filterItem;
            switch(parameterName) {
                case "personnelId":
                    return BaseApp.Current.CurrentUserData.UserId;
                case "cityName":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "CityName");
                    return filterItem == null ? null : filterItem.Value;
                case "provinceName":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "ProvinceName");
                    return filterItem == null ? null : filterItem.Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return "根据审批人员查询销售订单省市代理库";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                         "PartsSalesOrderDetail", "PartsSalesOrder","PartsSalesOrderProcess", "PartsOutboundBillDetailForPartsSalesAddAParam"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            //this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.RowLoaded += GridView_RowLoaded;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var partsSalesOrder = e.Row.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            GridViewRow gridViewRow = this.GridView.GetRowForItem(partsSalesOrder);
            if(gridViewRow != null)
                gridViewRow.Background = new SolidColorBrush(Colors.White);
        }

        //实现此方法用于校验清单库存是否满足
        public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            var selectedItem = this.GridView.SelectedItems;
            var dic = contents[0] as Dictionary<int, int>;
            if(dic == null || dic.Count == 0)
                return null;
            if(selectedItem == null)
                return null;
            foreach(var item in selectedItem) {
                var gridViewRow = this.GridView.GetRowForItem(item);
                if(gridViewRow == null)
                    return null;
                var partsSalesOrder = item as PartsSalesOrder;
                if(partsSalesOrder == null)
                    return null;
                if(dic[partsSalesOrder.Id] == 1) {
                    gridViewRow.Background = new SolidColorBrush(Colors.Blue);
                } else if(dic[partsSalesOrder.Id] == 2) {
                    gridViewRow.Background = new SolidColorBrush(Colors.Green);
                } else {
                    gridViewRow.Background = new SolidColorBrush(Colors.White);
                }
            }
            return null;
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrder);
            }
        }
    }
}
