﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class AgencyRetailerOrderDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "AgencyRetailerOrderStatus"
        };

        public AgencyRetailerOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                     new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "ERPSourceOrderCode",
                        Title=PartsSalesUIStrings.QueryPanel_Title_AgencyRetailerOrder_ERPSourceOrderCode
                    }, new ColumnItem{
                        Name = "Code"
                    }, new ColumnItem{
                        Name = "VehiclePartsHandleOrderCode"
                    }, new ColumnItem{
                        Name = "ShippingCompanyCode",
                    }, new ColumnItem{
                        Name = "ShippingCompanyName"
                    }, new ColumnItem{
                        Name = "WarehouseCode",
                    }, new ColumnItem{
                        Name = "WarehouseName",
                    },new ColumnItem{
                        Name = "CounterpartCompanyCode",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_ReceivingCompanyCode
                    }, new ColumnItem{
                        Name = "CounterpartCompanyName",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_ReceivingCompanyName
                    }, new ColumnItem{
                        Name = "VehiclePartsHandleOrder.ReceiverName",
                        Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ContactPerson
                    }, new ColumnItem{
                        Name = "VehiclePartsHandleOrder.ContactPhone",
                        Title = PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_ContactPhone
                    }, new ColumnItem{
                        Name = "VehiclePartsHandleOrder.ReceivingAddress",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_ReceivingAddress
                    }, new ColumnItem{
                        Name = "CreatorName",
                    }, new ColumnItem{
                        Name = "CreateTime",
                    }, new ColumnItem{
                        Name = "ConfirmorName",
                    }, new ColumnItem{
                        Name = "ConfirmorTime",
                    }, new ColumnItem{
                        Name = "CloserName",
                    }, new ColumnItem{
                        Name = "CloserTime",
                    }, new ColumnItem{
                        Name = "Remark",
                    }, new ColumnItem{
                        Name="BranchName"
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyRetailerOrder);
            }
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "AgencyRetailerOrderList"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetAgencyRetailerOrdersWithVehiclePartsHandleOrder";
        }
    }
}
