﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class AgencyRetailerOrderListDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SparePartCode"
                    }, new ColumnItem{
                        Name = "SparePartName"
                    }, new ColumnItem{
                        Name = "OrderedQuantity"
                    }, new ColumnItem{
                        Name = "ConfirmedAmount"
                    }, new ColumnItem{
                        Name = "UnitPrice"
                    }, new ColumnItem{
                        Name = "OrderSum"
                    }, new ColumnItem{
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyRetailerList);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["UnitPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OrderSum"]).DataFormatString = "c2";
        }

        public AgencyRetailerOrderListDataGridView() {
            this.DataContextChanged += AgencyRetailerOrderListDataGridView_DataContextChanged;
        }

        private void AgencyRetailerOrderListDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var agencyRetailerOrder = e.NewValue as AgencyRetailerOrder;
            if(agencyRetailerOrder == null)
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "AgencyRetailerOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = agencyRetailerOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override string OnRequestQueryName() {
            return "GetAgencyRetailerLists";
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
