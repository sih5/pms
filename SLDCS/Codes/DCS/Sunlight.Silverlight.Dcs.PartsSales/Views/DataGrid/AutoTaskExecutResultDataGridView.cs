﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class AutoTaskExecutResultDataGridView  : DcsDataGridViewBase{

        private readonly string[] kvNames = { 
             "ExecutionStatus"                               
        };

        public AutoTaskExecutResultDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new ColumnItem[] { 
                new ColumnItem{
                Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                Name = "BrandName"
                },
                new ColumnItem{
                Title = PartsSalesUIStrings.QueryPanel_QueeryItem_Title_Company_Code,
                Name = "CompanyCode"
                },
                new ColumnItem{
                Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_CorporationName,
                Name = "CompanyName"
                },
                new KeyValuesColumnItem{
                 Title = PartsSalesUIStrings.QueryPanel_Title_SettlementAutomaticTaskSet_ExecutionStatus,
                 Name = "ExecutionStatus",
                 KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                },
                new ColumnItem{
                 Title = "执行失败原因",
                 Name = "ExecutionLoseReason"
                },
                new ColumnItem{
                 Title = PartsSalesUIStrings.QueryPanel_Title_SettlementAutomaticTaskSet_ExecutionTime,
                 Name = "ExecutionTime"
                }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AutoTaskExecutResult);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAutoTaskExecutResults";
        }
    }
}
