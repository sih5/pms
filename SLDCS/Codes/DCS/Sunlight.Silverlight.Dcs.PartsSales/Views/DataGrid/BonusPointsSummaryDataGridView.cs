﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class BonusPointsSummaryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BonusPointsSummaryStatus"
        };

        public BonusPointsSummaryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                       Name = "Status",
                       Title=PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "BonusPointsSummaryCode",
                        Title="积分汇总单号"
                    }, new ColumnItem {
                        Name = "BrandName",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name
                    }, new ColumnItem {
                        Name = "SummaryAmount",
                        Title="汇总金额"
                    }, new ColumnItem {
                        Name = "SummaryBonusPoints",
                        Title="汇总积分"
                    }, new ColumnItem {
                       Name = "CreatorName",
                       Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName
                    }, new ColumnItem {
                       Name = "CreateTime",
                       Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime
                    }, new ColumnItem {
                       Name = "ApproverName",
                       Title=PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproverName
                    }, new ColumnItem {
                       Name = "ApproveTime",
                       Title=PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproveTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetBonusPointsSummaries";
        }

        protected override Type EntityType {
            get {
                return typeof(BonusPointsSummary);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "BonusPointsSummaryList"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["SummaryAmount"]).DataFormatString = "c2";
        }
    }
}
