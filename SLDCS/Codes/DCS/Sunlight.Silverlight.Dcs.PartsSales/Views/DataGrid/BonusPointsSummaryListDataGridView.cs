﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class BonusPointsSummaryListDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "BonusPointsOrderCode",
                       Title = "积分订单编号"
                    },new ColumnItem {
                       Name = "PlatForm_Code",
                       Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_PlatForm_Code
                    },new ColumnItem {
                       Name = "ServiceApplyCode",
                       Title = "平台退货单号"
                    },new ColumnItem {
                       Name = "BonusPoints",
                       Title = "积分数"
                    },new ColumnItem {
                       Name = "BonusPointsAmount",
                       Title = "积分金额"
                    }
                };
            }
        }

        public BonusPointsSummaryListDataGridView() {
            this.DataContextChanged += BonusPointsSummaryListDataGridView_DataContextChanged;
        }

        private void BonusPointsSummaryListDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var bonusPointsSummary = e.NewValue as BonusPointsSummary;
            if(bonusPointsSummary == null)
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "BonusPointsSummaryId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = bonusPointsSummary.Id;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(BonusPointsSummaryList);
            }
        }

        protected override void OnControlsCreated() {
            ((GridViewDataColumn)this.GridView.Columns["BonusPointsAmount"]).DataFormatString = "c2";
        }

        protected override string OnRequestQueryName() {
            return "GetBonusPointsSummaryLists";
        }
    }
}
