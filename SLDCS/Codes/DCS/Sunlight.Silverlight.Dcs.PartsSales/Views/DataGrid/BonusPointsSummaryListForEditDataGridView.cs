﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class BonusPointsSummaryListForEditDataGridView : DcsDataGridViewBase {
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private QueryWindowBase bonusPointsOrderQueryWindow;
        private RadWindow radWindow;
        private readonly string[] kvNames = {
            "BonusPointsType"
        };

        public BonusPointsSummaryListForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            if(bonusPointsSummary == null)
                return;
            this.SetSerialNumber();
        }

        private void SetSerialNumber() {
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            if(bonusPointsSummary == null)
                return;
            var serialNumber = 1;
            foreach(var bonusPointsSummaryList in bonusPointsSummary.BonusPointsSummaryLists) {
                bonusPointsSummaryList.SerialNumber = serialNumber;
                serialNumber++;
            }
        }

        private RadWindow SelectedWindow {
            get {
                if(radWindow == null) {
                    radWindow = new RadWindow();
                    radWindow.Header = "积分订单查询";
                    radWindow.Content = this.BonusPointsOrderQueryWindow;
                    radWindow.Width = 1000;
                    radWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;
                }
                return radWindow;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            if(bonusPointsSummary == null || !bonusPointsSummary.BrandId.HasValue) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_CustomerOrderPriceGrade_PartsSalesCategoryIsNull);
                return;
            }
            this.SelectedWindow.ShowDialog();
        }

        private QueryWindowBase BonusPointsOrderQueryWindow {
            get {
                if(this.bonusPointsOrderQueryWindow == null) {
                    this.bonusPointsOrderQueryWindow = DI.GetQueryWindow("BonusPointsOrder");
                    this.bonusPointsOrderQueryWindow.Loaded += this.BonusPointsOrderQueryWindow_Loaded;
                    this.bonusPointsOrderQueryWindow.SelectionDecided += this.BonusPointsOrderQueryWindow_SelectionDecided;
                }
                return this.bonusPointsOrderQueryWindow;
            }
        }

        private void BonusPointsOrderQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            var queryWindow = sender as QueryWindowBase;
            if(bonusPointsSummary == null || queryWindow == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            queryWindow.ExchangeData(null, "ExecuteQuery", null);
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "SettlementStatus", (int)DcsBonusPointsSettlementStatus.未汇总 });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "SettlementStatus", false });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItemNotQuery", compositeFilterItem);
        }

        private void BonusPointsOrderQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var bonusPointsSummary = this.DataContext as BonusPointsSummary;
            if(bonusPointsSummary == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var bonusPointsOrder = queryWindow.SelectedEntities.Cast<BonusPointsOrder>().FirstOrDefault();
            if(bonusPointsOrder == null)
                return;

            if(bonusPointsSummary.BonusPointsSummaryLists.Any(r => r.BonusPointsId == bonusPointsOrder.Id)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_PartsSalesSettlementRef_SourceCodeIsExist);
                return;
            }
            var bonusPointsSummaryList = new BonusPointsSummaryList();
            bonusPointsSummaryList.SerialNumber = bonusPointsSummary.BonusPointsSummaryLists.Any() ? bonusPointsSummary.BonusPointsSummaryLists.Max(entity => entity.SerialNumber) + 1 : 1;
            bonusPointsSummaryList.BonusPointsOrderCode = bonusPointsOrder.BonusPointsOrderCode;
            bonusPointsSummaryList.PlatForm_Code = bonusPointsOrder.PlatForm_Code;
            bonusPointsSummaryList.ServiceApplyCode = bonusPointsOrder.ServiceApplyCode;
            bonusPointsSummaryList.BonusPoints = bonusPointsOrder.BonusPoints;
            bonusPointsSummaryList.BonusPointsAmount = bonusPointsOrder.BonusPointsAmount;
            bonusPointsSummaryList.SouceType = bonusPointsOrder.Type;
            bonusPointsSummaryList.BonusPointsTime = bonusPointsOrder.CreateTime;
            bonusPointsSummaryList.BonusPointsId = bonusPointsOrder.Id;
            bonusPointsSummary.BonusPointsSummaryLists.Add(bonusPointsSummaryList);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "BonusPointsOrderCode",
                        Title="积分订单编号",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "SouceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = "源单据类型",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PlatForm_Code",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_PlatForm_Code,
                        IsReadOnly = true
                     }, new ColumnItem {
                        Name = "ServiceApplyCode",
                        Title="平台退货单号",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "BonusPointsAmount",
                        Title="积分金额",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "BonusPoints",
                        Title="积分数",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "BonusPointsTime",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(BonusPointsSummaryList);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserInsertRows = true;
            this.GridView.CanUserDeleteRows = true;
            this.DataPager.PageSize = 15;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            ((GridViewDataColumn)this.GridView.Columns["BonusPointsAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["BonusPointsTime"]).DataFormatString = "d";
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("BonusPointsSummaryLists");
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }
    }
}
