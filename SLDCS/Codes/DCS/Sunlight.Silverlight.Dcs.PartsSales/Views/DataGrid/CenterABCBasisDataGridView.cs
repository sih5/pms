﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CenterABCBasisDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
           "ABCStrategy_Category", "ABCSetting_Type"
        };
        public CenterABCBasisDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "WeekNum",
                        Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_WeekNum
                    },new ColumnItem{
                        Name = "CenterCode",
                        Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_CenterCode
                    },new ColumnItem{
                        Name = "CenterName",
                        Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_CenterName
                    }, new ColumnItem{
                        Name = "SparePartCode",
                        Title=PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPrice_SparePartCode
                    }, new ColumnItem{
                        Name = "SihCode",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_SihCode
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Name
                    }, new KeyValuesColumnItem{
                        Name = "OldType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_OldType
                    }, new ColumnItem{
                        Name = "SalesPrice",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPrice_Price
                    }, new ColumnItem{
                        Name = "SaleSubNumber",
                        Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_SaleSubNumber
                    }, new ColumnItem{
                        Name = "SaleSubAmount",
                        Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_SaleSubAmount
                    }, new ColumnItem{
                        Name = "SubAmountPercentage",
                        Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_SubAmountPercentage
                    }, new ColumnItem{
                        Name = "SaleSubFrequency",
                        Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_SaleSubFrequency
                    }, new ColumnItem{
                        Name = "SubFrequencyPercentage",
                        Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_SubFrequencyPercentage
                    }, new KeyValuesColumnItem{
                        Name = "NewType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_NewType
                    }
                };
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }     
        protected override Type EntityType {
            get {
                return typeof(CenterABCBasi);
            }
        }

        protected override string OnRequestQueryName() {
            return "QueryCenterABCBasis";
        }
        protected override void OnControlsCreated() {
            this.DataPager.PageSize = 100;          
        }
    }
}
