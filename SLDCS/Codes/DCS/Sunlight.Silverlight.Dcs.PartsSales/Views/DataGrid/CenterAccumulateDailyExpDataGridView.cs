﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CenterAccumulateDailyExpDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "ABCSetting_Type"
        };
        public CenterAccumulateDailyExpDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Year",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesWeeklyBase_Year
                    },new ColumnItem {
                        Name = "CreateTime",
                         Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesWeeklyBase_CreateTime
                    },new ColumnItem {
                        Name = "CenterName",
                         Title=PartsSalesUIStrings.QueryPanel_QueeryItem_Title_Company_Name
                    },new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                         Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_Type
                    },new ColumnItem {
                        Name = "OrderCycle",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_OrderCycle
                    },new ColumnItem {
                        Name = "ArrivalCycle",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ArriveCycle
                    },new ColumnItem {
                        Name = "LogisticsCycle",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_LogisticsCycle
                    },  new ColumnItem {
                        Name = "ReserveCoefficient",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ReserveCoefficient
                    },new ColumnItem {
                        Name = "StockUpperCoefficient",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_StockUpperCoefficient
                    },new ColumnItem {
                        Name = "StockUpperQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_StockUpperQty            
                    },new ColumnItem {
                        Name = "PackingQty",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_PackingCoefficient
                    },new ColumnItem {
                        Name = "UsableQty",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_UsableQuantity
                    } ,new ColumnItem {
                        Name = "OnLineQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_OnLineQty
                    },new ColumnItem{
                        Name="OweQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_OweQty
                    },new ColumnItem{
                        Name="SparePartCode",
                        Title=PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartCode
                    },new ColumnItem{
                        Name="SparePartName",
                        Title=PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartName
                    },new ColumnItem{
                        Name="CenterSubmit",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_CenterSubmit
                    },new ColumnItem{
                        Name="ExpectPlanQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ExpectPlanQty
                    } ,new ColumnItem{
                        Name="CumulativeFst",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_CumulativeFst
                    }                    
                };
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override Type EntityType {
            get {
                return typeof(CenterAccumulateDailyExp);
            }
        }

        protected override string OnRequestQueryName() {
            return "CenterAccumulateDailyExpByCenter";
        }
        protected override void OnControlsCreated() {
            this.DataPager.PageSize = 100;
        }
    }
}
