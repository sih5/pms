﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CheckPartsStockForAgenchDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SparePartCode
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SparePartName,
                    }, new ColumnItem{
                        Name = "AgencyCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_AgencyCode
                    }, new ColumnItem{
                        Name = "AgencyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_AgencyName
                    }, new ColumnItem{
                        Name = "OrderedQuantity",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_OrderedQuantity,
                         TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name = "Usablequantity",
                         Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_Usablequantity,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name = "Fulfilquantity",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_Fulfilquantity, 
                        TextAlignment = TextAlignment.Right
                    },
                     new ColumnItem{
                        Name = "NoFulfilquantity",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_NoFulfilquantity,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyStockQryFrmOrder);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAgencyStockQryFrmOrders";
        }
    }
}
