﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CompanyPartsStockForCheckDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            base.OnControlsCreated();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "WarehouseCode",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_WarehouseCode
                    }, new ColumnItem {
                        Name = "WarehouseName",
                          Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_WarehouseName
                    },new ColumnItem {
                        Name = "SparePartCode",
                         Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                       Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem {
                        Name = "OrderQuantity",
                        Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderQuntity
                    }, new ColumnItem {
                        Name = "UsableStock",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_Fulfilquantity
                    }, new ColumnItem {
                        Name = "UnUsableStock",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_NoFulfilquantity
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CompanyPartsStock);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("CompanyPartsStocks");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
