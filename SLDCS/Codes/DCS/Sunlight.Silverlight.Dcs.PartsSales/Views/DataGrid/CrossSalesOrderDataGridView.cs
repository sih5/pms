﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CrossSalesOrderDataGridView  : DcsDataGridViewBase
    {
        protected readonly string[] KvNames = {
            "CrossSalesOrderStatus", "CrossSalesOrderType", "PartsShipping_Method"
        };
        public CrossSalesOrderDataGridView()
        {
            this.LoadKeyValue();
            this.Initializer.Register(this.Initialize);
        }

        protected void LoadKeyValue()
        {
            this.KeyValueManager.Register(this.KvNames);
        }

        private void Initialize()
        {
            
        }

        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                        Title="状态"
                    },new ColumnItem {
                        Name = "Code",
                        Title="单据编号"
                    },
                    new ColumnItem {
                        Name = "IsUplodFile",
                        Title="是否有附件"
                    },
                    new ColumnItem {
                        Name = "IsAutoSales",
                        Title="是否生成销售订单"
                    },
                    new ColumnItem {
                        Name = "ApplyCompnayName",
                        Title="申请企业名称"
                    },
                    new ColumnItem {
                        Name = "ApplyCompnayCode",
                        Title = "申请企业编号"
                    },
                    new ColumnItem {
                        Name = "SubCompanyName",
                        Title = "隶属企业名称"
                    },
                    new ColumnItem {
                        Name = "SubCompanyCode",
                        Title="隶属企业编号"
                    },
                    new KeyValuesColumnItem {
                        Name = "Type",
                        Title="品种类型",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]],
                    },
                    new ColumnItem {
                        Name = "ReceivingNames",
                        Title ="收货人"
                    },
                    new ColumnItem {
                        Name = "ReceivingPhones",
                        Title="收货电话"
                    },                                    
                    new ColumnItem {
                        Name = "ReceivingAddresss",
                        Title="收货地址"
                    },
                    new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        Title="发运方式",
                        KeyValueItems = this.KeyValueManager[this.KvNames[2]],
                    },
                    new ColumnItem {
                        Name = "SalesCode",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime,
                        Title="销售订单编号"
                    },
                    new ColumnItem {
                        Name = "CreatorName",
                        Title="创建人"
                    },
                    new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime,
                        Title="创建时间"
                    },
                    new ColumnItem {
                        Name = "ModifierName",
                        Title="修改人"
                    },
                    new ColumnItem {
                        Name = "ModifyTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime,
                        Title="修改时间"
                    },
                    new ColumnItem {
                        Name = "SubmitterName",
                        Title="提交人"
                    },
                    new ColumnItem {
                        Name = "SubmitTime",
                        Title="提交时间"
                    },
                    new ColumnItem {
                        Name = "AbandonerName",
                        Title="作废人"
                    },
                    new ColumnItem {
                        Name = "AbandonTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime,
                        Title="作废时间"
                    },
                    new ColumnItem {
                        Name = "ApproverName",
                        Title="初审人"
                    },
                    new ColumnItem {
                        Name = "ApproveTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime,
                        Title="初审时间"
                    },
                    new ColumnItem {
                        Name="CheckerName",
                        Title = "审核人"
                    },                  
                    new ColumnItem {
                        Name = "CheckTime",
                        Title = "审核时间"
                    },
                    new ColumnItem {
                        Name = "CloserName",
                        Title="审批人"
                    },
                    new ColumnItem {
                        Name = "CloseTime",
                        Title="审批时间"
                    },
                    new ColumnItem {
                        Name = "SeniorAuditName",
                        Title="高级审核人"
                    },
                    new ColumnItem {
                        Name = "SeniorAuditTime",
                        Title="高级审核时间"
                    },
                    new ColumnItem {
                        Name = "ApproveMent",
                        Title="审核意见"
                    },
                    new ColumnItem {
                        Name = "Finisher",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime,
                        Title = "完善资料人员"
                    },                   
                    new ColumnItem {
                        Name = "FinisherTime",
                        Title="完善时间"
                    },
                     new ColumnItem {
                        Name = "Stoper",
                        Title = "终止人"
                    },
                    new ColumnItem {
                        Name = "StopTime",
                        Title="终止时间"
                    }
                };
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach(var filter in compositeFilterItem.Filters.Where(item => item.MemberName != "SparePartName" && item.MemberName != "SparePartCode"))
                newCompositeFilterItem.Filters.Add(filter);
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filter = this.FilterItem as CompositeFilterItem;
            if (filter == null)
                return null;
            FilterItem filterItem;
            switch (parameterName)
            {
                case "sparePartName":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SparePartName");
                    return filterItem == null ? null : filterItem.Value;               
                case "sparePartCode":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode");
                    return filterItem == null ? null : filterItem.Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName()
        {
            return "GetCrossSalesOrderBySparepart";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider
        {
            get
            {
                return new TabControlRowDetailsTemplateProvider
                {
                    DetailPanelNames = new[] {
                         "CrossSalesOrderDetail", "PartsSalesOrderCross","PartsOutboundBillCross"
                    }
                };
            }
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }      

        protected override Type EntityType
        {
            get
            {
                return typeof(CrossSalesOrder);
            }
        }
    }
}
