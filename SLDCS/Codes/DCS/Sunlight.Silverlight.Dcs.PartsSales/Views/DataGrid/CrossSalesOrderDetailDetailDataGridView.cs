﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CrossSalesOrderDetailDetailDataGridView : DcsDataGridViewBase {
        public CrossSalesOrderDetailDetailDataGridView() {
            this.DataContextChanged += PartsSalesOrderDetailDataGridView_DataContextChanged;
        }

        private void PartsSalesOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var crossSalesOrder = e.NewValue as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "CrossSalesOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = crossSalesOrder.Id
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsBaseDataStatus.有效
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem{
                        Name = "OrderedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title="订货数量"
                    }, new ColumnItem{
                        Name = "CenterPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = "中心库价"
                    }, new ColumnItem{
                        Name = "SalesPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = "经销价"
                    }, new ColumnItem{
                        Name = "RetailGuidePrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = "建议售价"
                    }, new ColumnItem {
                        Name = "PriceTypeName",
                        IsReadOnly = true,
                        Title="价格类型"
                    }, new ColumnItem {
                        Name = "ABCStrategy",
                        IsReadOnly = true,
                        Title="配件ABC分类"
                    }, new ColumnItem{
                        Name = "Vin",
                        TextAlignment = TextAlignment.Right,
                        Title="底盘号"
                    }, new ColumnItem{
                        Name = "Explain",
                        Title="跨区域说明"
                    }, new ColumnItem {
                        Name = "IsHand",
                        Title="是否手填"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CrossSalesOrderDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCrossSalesOrderDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["OrderedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CenterPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
