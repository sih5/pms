﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CrossSalesOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase partsSupplierDropDownQueryWindow;
        private int idNumber = -32768;
        private void SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var spareParts = queryWindow.SelectedEntities.Cast<VirtualSparePart>().FirstOrDefault();
            if(spareParts == null)
                return;
            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;
            if(crossSalesOrder.CrossSalesOrderDetails.Any(r => r.SparePartId == spareParts.Id)) {
                return;
            }
            var crossSalesOrderDetail = queryWindow.DataContext as CrossSalesOrderDetail;
            crossSalesOrderDetail.SparePartId = spareParts.Id;
            crossSalesOrderDetail.SparePartCode = spareParts.Code;
            crossSalesOrderDetail.SparePartName = spareParts.Name;
            crossSalesOrderDetail.ABCStrategy = Enum.GetName(typeof(DcsABCStrategyCategory), spareParts.PartABC == null ? 0 : spareParts.PartABC);
            crossSalesOrderDetail.SalesPrice = spareParts.SalesPrice;
            crossSalesOrderDetail.CenterPrice = spareParts.CenterPrice;
            crossSalesOrderDetail.RetailGuidePrice = spareParts.RetailGuidePrice;
            crossSalesOrderDetail.PriceTypeName = spareParts.PriceTypeName;
        }

        private QueryWindowBase PartsSupplierDropDownQueryWindow {
            get {
                if(this.partsSupplierDropDownQueryWindow == null) {
                    this.partsSupplierDropDownQueryWindow = DI.GetQueryWindow("SparePartForCrossSalesOrder");
                    this.partsSupplierDropDownQueryWindow.SelectionDecided += this.SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided;
                }
                return this.partsSupplierDropDownQueryWindow;
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                  new DropDownTextBoxColumnItem {
                        Name = "SparePartCode",
                        Title="配件编号",
                        DropDownContent = this.PartsSupplierDropDownQueryWindow,
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title="配件名称"
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title="订货数量"
                    }, new ColumnItem {
                        Name = "ABCStrategy",
                        Title ="配件ABC分类"
                    }, new ColumnItem {
                        Name = "CenterPrice",
                        Title="中心库价"
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        Title="经销价"
                    }, new ColumnItem {
                        Name = "RetailGuidePrice",
                        Title="建议售价"
                    }, new ColumnItem {
                        Name = "PriceTypeName",
                        Title="价格类型"
                    }, new ColumnItem {
                        Name = "Vin",
                        Title="底盘号"
                    }, new ColumnItem {
                        Name = "Explain",
                        Title="跨区域说明"
                    }, new ColumnItem {
                        Name = "IsHand",
                        Title="是否手填",
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CrossSalesOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DataContextChanged += PartsPurchaseOrderDetailForEditDataGridView_DataContextChanged;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.CellValidating += GridView_CellValidating;
            ((GridViewDataColumn)this.GridView.Columns["OrderedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CenterPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;
            var crossSalesOrderDetail = e.Cell.DataContext as CrossSalesOrderDetail;
            if(crossSalesOrderDetail == null)
                return;
            switch(e.Cell.Column.UniqueName) {
                case "SparePartName":
                case "SparePartCode":
                case "ABCStrategy":
                case "CenterPrice":
                case "SalesPrice":
                case "RetailGuidePrice":
                case "PriceTypeName":
                    if(crossSalesOrderDetail.SparePartId.HasValue) {
                        e.Cancel = true;
                    } else {
                        e.Cancel = false;
                    }
                    break;
            }
        }
        private void PartsPurchaseOrderDetailForEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            switch(e.Cell.DataColumn.UniqueName) {
                case "OrderedQuantity":
                    if(!(e.NewValue is int) || (int)e.NewValue <= 0) {
                        e.IsValid = false;
                        e.ErrorMessage = "订货数量不能小于0";
                    }

                    break;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;
            var crossSalesOrderDetail = new CrossSalesOrderDetail {
                Id = idNumber++,
                OrderedQuantity = 0,
                Status = (int)DcsBaseDataStatus.有效
            };
            e.NewObject = crossSalesOrderDetail;
            crossSalesOrder.CrossSalesOrderDetails.Add(crossSalesOrderDetail);
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("CrossSalesOrderDetails");
        }
    }
}