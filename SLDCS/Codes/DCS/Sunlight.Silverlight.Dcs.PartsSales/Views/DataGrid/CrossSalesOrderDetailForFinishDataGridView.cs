﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CrossSalesOrderDetailForFinishDataGridView : DcsDataGridViewBase {
        private QueryWindowBase partsSupplierDropDownQueryWindow;
        private QueryWindowBase PartsSupplierDropDownQueryWindow {
            get {
                if(this.partsSupplierDropDownQueryWindow == null) {
                    this.partsSupplierDropDownQueryWindow = DI.GetQueryWindow("SparePartForCrossSalesOrder");
                    this.partsSupplierDropDownQueryWindow.Loaded += SparePartForPartsPurchaseOrderDropDownQueryWindow_Loaded;
                    this.partsSupplierDropDownQueryWindow.SelectionDecided += this.SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided;
                }
                return this.partsSupplierDropDownQueryWindow;
            }
        }
        private void SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var spareParts = queryWindow.SelectedEntities.Cast<VirtualSparePart>().FirstOrDefault();
            if(spareParts == null)
                return;
            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;
            if(crossSalesOrder.CrossSalesOrderDetails.Any(r => r.SparePartId==spareParts.Id) ){
                return;
            }
            var crossSalesOrderDetail = queryWindow.DataContext as CrossSalesOrderDetail;
            crossSalesOrderDetail.SparePartId = spareParts.Id;
            crossSalesOrderDetail.SparePartCode = spareParts.Code;
            crossSalesOrderDetail.SparePartName = spareParts.Name;
            crossSalesOrderDetail.ABCStrategy = Enum.GetName(typeof(DcsABCStrategyCategory), spareParts.PartABC == null ? 0 : spareParts.PartABC);
            crossSalesOrderDetail.SalesPrice = spareParts.SalesPrice;
            crossSalesOrderDetail.CenterPrice = spareParts.CenterPrice;
            crossSalesOrderDetail.RetailGuidePrice = spareParts.RetailGuidePrice;
            crossSalesOrderDetail.PriceTypeName = spareParts.PriceTypeName;                      
        }

        private void SparePartForPartsPurchaseOrderDropDownQueryWindow_Loaded(object sender, RoutedEventArgs routedEventArgs) {
            var queryWindowSparePartForPartsPurchaseOrder = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindowSparePartForPartsPurchaseOrder == null)
                return;
            var partsPurchasePlan = this.DataContext as CrossSalesOrder;
            if(partsPurchasePlan == null)
                return;

        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                  new DropDownTextBoxColumnItem {
                        Name = "SparePartCode",
                        Title="配件编号",
                        IsEditable = false,
                        DropDownContent = this.PartsSupplierDropDownQueryWindow,
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title="配件名称",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title="订货数量",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "ABCStrategy",
                        Title ="配件ABC分类",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "CenterPrice",
                        Title="中心库价",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        Title="经销价",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "RetailGuidePrice",
                        Title="建议售价",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "PriceTypeName",
                        Title="价格类型",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Vin",
                        Title="底盘号",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Explain",
                        Title="跨区域说明",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "IsHand",
                        Title="是否手填"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CrossSalesOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DataContextChanged += PartsPurchaseOrderDetailForEditDataGridView_DataContextChanged;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.ValidatesOnDataErrors = GridViewValidationMode.InViewMode;
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;
            var crossSalesOrderDetail = e.Cell.DataContext as CrossSalesOrderDetail;
            if(crossSalesOrderDetail == null)
                return;
            switch(e.Cell.Column.UniqueName) {
                case "SparePartCode":

                    if(crossSalesOrderDetail.IsHand.HasValue && crossSalesOrderDetail.IsHand.Value) {
                        e.Cancel = false;
                    } else {
                        e.Cancel = true;
                    }
                    break;
            }
        }

        private void PartsPurchaseOrderDetailForEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var crossSalesOrder = this.DataContext as CrossSalesOrder;
            if(crossSalesOrder == null)
                return;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
        }


        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("CrossSalesOrderDetails");
        }
    }
}