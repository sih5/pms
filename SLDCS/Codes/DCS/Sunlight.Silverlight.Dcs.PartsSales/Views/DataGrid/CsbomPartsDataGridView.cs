﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid
{
    public class CsbomPartsDataGridView : DcsDataGridViewBase
    {
        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualCsbomParts);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    },new ColumnItem {
                        Name = "SPMReferenceCode",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_SihCode
                    },new ColumnItem {
                        Name = "ReferenceCode",
                        Title = "CSBOM红岩号"
                    },new ColumnItem {
                        Name = "Name",
                        Title = "CSBOM名称"
                    },new ColumnItem {
                        Name = "SPMName",
                        Title = "SPM名称"
                    },new ColumnItem {
                        Name = "EnglishName",
                        Title = "CSBOM英文名称"
                    },new ColumnItem {
                        Name = "SPMEnglishName",
                        Title = "SPM英文名称"
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title =  "传输时间"
                    },new ColumnItem{
                        Name = "IsUpdate",
                        Title =  "接口是否修改"
                    }
                };
            }
        }

        protected override string OnRequestQueryName()
        {
            return "查询CSBOM配件信息";
        }

     
        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }
        }
      
        protected override void OnControlsCreated()
        {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            base.OnControlsCreated();
        }

       protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if (compositeFilterItem == null)
            {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            }
            else
            {
                foreach (var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "SPMName" && filter.MemberName != "SPMEnglishName"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "name":
                        return filters.Filters.Single(item => item.MemberName == "Name").Value;
                    case "spmName":
                        return filters.Filters.Single(item => item.MemberName == "SPMName").Value;                 
                    case "englishName":
                        return filters.Filters.Single(item => item.MemberName == "EnglishName").Value;
                    case "spmEnglishName":
                        return filters.Filters.Single(item => item.MemberName == "SPMEnglishName").Value;                   
                    case "bCreateTime":
                        var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;                  
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
