﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;


namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CustomerDirectSpareListDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public CustomerDirectSpareListDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                    } , new ColumnItem{
                        Name = "CustomerCode" ,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Code
                    }, new ColumnItem{
                        Name = "CustomerName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Name
                    }, new ColumnItem{
                        Name = "SparePartCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },  new ColumnItem{
                        Name = "CreatorName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime
                    }, new ColumnItem{
                        Name = "ModifierName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifierName
                    }, new ColumnItem{
                        Name = "ModifyTime",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifyTime
                    }, new ColumnItem{
                        Name = "IsPrimary",
                        Title=PartsSalesUIStrings.DataGridView_Title_VirtualCustomerDirectSpareList_IsPrimary
                    }, new ColumnItem{
                        Name = "PartsSupplierCode",
                        Title=PartsSalesUIStrings.DataEdit_Text_CustomerSupplyInitialFee_SupplierCode
                    }, new ColumnItem{
                        Name = "PartsSupplierName",
                        Title=PartsSalesUIStrings.DataEdit_Text_CustomerSupplyInitialFee_SupplierName
                    },new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title=PartsSalesUIStrings.DataEditView_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualCustomerDirectSpareList);
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                var param = new[] { "IsPrimary"};
                foreach (var filterItem in compositeFilterItem.Filters.Where(filter => !param.Contains(filter.MemberName)))
                {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "isPrimarys":
                        return filters.Filters.Single(item => item.MemberName == "IsPrimary").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "GetVirtualCustomerDirectSpareList";
        }
    }
}
