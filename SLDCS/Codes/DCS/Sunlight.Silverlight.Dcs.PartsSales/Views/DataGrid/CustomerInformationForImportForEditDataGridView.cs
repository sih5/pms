﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CustomerInformationForImportForEditDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem {
                        Name = "SequeueNumber",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_SequeueNumber
                    },new ColumnItem{
                        Name="SalesCompany.Name",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsBranch_SalesCompanyName
                    },new ColumnItem{
                        Name="CustomerCompany.Name",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsBranch_CustomerCompanyName
                    },new ColumnItem{
                        Name="ContactPerson",
                        Title=Utils.GetEntityLocalizedName(typeof(CustomerInformation),"ContactPerson")
                    },new ColumnItem{
                        Name="ContactPhone",
                        Title=Utils.GetEntityLocalizedName(typeof(CustomerInformation),"ContactPhone")
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CustomerInformation);
            }
        }
    }
}