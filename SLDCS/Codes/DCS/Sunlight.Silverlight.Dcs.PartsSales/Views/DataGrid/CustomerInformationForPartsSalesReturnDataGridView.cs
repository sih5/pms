﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CustomerInformationForPartsSalesReturnDataGridView : DcsDataGridViewBase {


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CustomerCompanyCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_Code
                    }, new ColumnItem {
                        Name = "CustomerCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_CustomerInformation_Name
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualCustomerInformation);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }

        protected override string OnRequestQueryName() {
            return "获取配件销售退货经销商";
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var newCompositeFilterItem = new CompositeFilterItem();
                var compositeFilters = compositeFilterItem.Filters.Where
                    (item => (item.GetType() == typeof(FilterItem)
                        && item.MemberName != "AccountGroupId"
                        && item.MemberName != "CutOffDate"
                        && item.MemberName != "CustomerCompany.Code"
                        && item.MemberName != "CustomerCompany.Name")
                        || (item.GetType() == typeof(CompositeFilterItem)
                        && ((CompositeFilterItem)item).Filters.All(member => member.MemberName != "AccountGroupId" || member.MemberName != "CutOffDate" || item.MemberName != "CustomerCompany.Name")));
                if(compositeFilters.Any())
                    foreach(var item in compositeFilters)
                        newCompositeFilterItem.Filters.Add(item);
                return newCompositeFilterItem.ToFilterDescriptor();
            }
            return this.FilterItem.ToFilterDescriptor();
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            switch(parameterName) {
                case "accountGroupId":
                    return filter != null ? filter.Filters.SingleOrDefault(e => e.MemberName == "AccountGroupId").Value : default(int);
                case "name":
                    return filter != null ? filter.Filters.SingleOrDefault(e => e.MemberName == "CustomerCompany.Name").Value : null;
                case "code":
                    return filter.Filters.SingleOrDefault(e => e.MemberName == "CustomerCompany.Code") != null ? filter.Filters.Single(e => e.MemberName == "CustomerCompany.Code").Value : null;
                case "cutoffTime":
                    var endTimeFilters = filter.Filters.FirstOrDefault(item => item.MemberName != null && item.MemberName == "CutOffDate");
                    if(endTimeFilters != null) {
                        var dateTime = endTimeFilters.Value as DateTime?;
                        if(dateTime.HasValue)
                            return new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                    }
                    return null;
            }
            return null;
        }

    }
}
