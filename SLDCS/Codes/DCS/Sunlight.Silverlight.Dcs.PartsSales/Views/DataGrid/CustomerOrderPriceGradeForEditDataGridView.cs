﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using System.Windows.Markup;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CustomerOrderPriceGradeForEditDataGridView : DcsDataGridViewBase {
        private ObservableCollection<KeyValuePair> kvPartsSalesOrderTypes;

        public CustomerOrderPriceGradeForEditDataGridView() {
            this.Initializer.Register(this.InitialData);
        }

        private ObservableCollection<KeyValuePair> PartsSalesOrderTypes {
            get {
                return this.kvPartsSalesOrderTypes ?? (this.kvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        private void InitialData() {
            this.PartsSalesOrderTypes.Clear();
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.Load(domainContext.GetPartsSalesOrderTypesQuery().Where(entity => entity.Status == (int)DcsBaseDataStatus.有效 && entity.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var entity in loadOp.Entities) {
                    this.PartsSalesOrderTypes.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name,
                        UserObject = entity
                    });
                }
            }, null);
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var frameworkElement = this.DataContext as CustomerOrderPriceGradeForAddDataEditView;
            if(frameworkElement != null) {
                if(frameworkElement.PartsSalesCategoryId == default(int)) {
                    e.Cancel = true;
                }
            }
            e.NewObject = new CustomerOrderPriceGrade {
                Status = (int)DcsBaseDataStatus.有效,
                CustomerCompanyCode = "default",
                CustomerCompanyName = "default"
            };
        }

        private void GridView_CellEditEnded(object sender, Telerik.Windows.Controls.GridViewCellEditEndedEventArgs e) {
            if(e.Cell.Column.UniqueName.Equals("PartsSalesOrderTypeName")) {
                var customerOrderPriceGrade = e.Cell.ParentRow.DataContext as CustomerOrderPriceGrade;
                if(customerOrderPriceGrade == null)
                    return;
                var partsSalesOrderType = this.PartsSalesOrderTypes.SingleOrDefault(entity => entity.UserObject is PartsSalesOrderType && entity.Key == customerOrderPriceGrade.PartsSalesOrderTypeId);
                if(partsSalesOrderType == null)
                    return;
                customerOrderPriceGrade.PartsSalesOrderTypeCode = ((PartsSalesOrderType)partsSalesOrderType.UserObject).Code;
                customerOrderPriceGrade.PartsSalesOrderTypeName = ((PartsSalesOrderType)partsSalesOrderType.UserObject).Name;
            }
        }
        private void GridView_PreparingCellForEdit(object sender, GridViewPreparingCellForEditEventArgs e) {
            if(e.Column.UniqueName.Equals("PartsSalesOrderTypeName")) {
                var frameworkElement = this.DataContext as CustomerOrderPriceGradeForAddDataEditView;
                if(frameworkElement != null) {
                    if(frameworkElement.PartsSalesCategoryId != default(int)) {

                        this.PartsSalesOrderTypes.Clear();
                        var domainContext = this.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.Load(domainContext.GetPartsSalesOrderTypesQuery().Where(entity => entity.Status == (int)DcsBaseDataStatus.有效 && entity.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && entity.PartsSalesCategoryId == frameworkElement.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError)
                                return;
                            foreach(var entity in loadOp.Entities) {
                                this.PartsSalesOrderTypes.Add(new KeyValuePair {
                                    Key = entity.Id,
                                    Value = entity.Name,
                                    UserObject = entity
                                });
                            }
                            var detail = e.Row.DataContext as CustomerOrderPriceGrade;
                            if(detail == null)
                                return;
                            var comboBox = e.EditingElement as RadComboBox;
                            if(comboBox == null)
                                return;
                            comboBox.ItemsSource = this.PartsSalesOrderTypes;
                        }, null);
                    }
                }

            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            //this.GridView.CellEditEnded -= this.GridView_CellEditEnded;
            //this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.CellValidating += GridView_CellValidating;
            ((GridViewDataColumn)this.GridView.Columns["Coefficient"]).DataFormatString = "n2";
            this.GridView.Columns["PartsSalesOrderTypeName"].CellEditTemplate =
                                                     (DataTemplate)XamlReader.
                                                     Load("<DataTemplate xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' xmlns:telerik='http://schemas.telerik.com/2008/xaml/presentation'><telerik:RadComboBox IsEditable='False' DisplayMemberPath='Value' SelectedValuePath='Key' SelectedValue='{Binding PartsSalesOrderTypeId, Mode=TwoWay, NotifyOnValidationError=True, UpdateSourceTrigger=PropertyChanged}' /></DataTemplate>");
            this.GridView.PreparingCellForEdit += GridView_PreparingCellForEdit;
        }
        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            if(e.Cell.Column.UniqueName.Equals("PartsSalesOrderTypeName")) {
                var customerOrderPriceGrade = e.Cell.ParentRow.DataContext as CustomerOrderPriceGrade;
                if(customerOrderPriceGrade == null)
                    return;
                var partsSalesOrderType = this.PartsSalesOrderTypes.SingleOrDefault(entity => entity.UserObject is PartsSalesOrderType && entity.Key == customerOrderPriceGrade.PartsSalesOrderTypeId);
                if(partsSalesOrderType == null)
                    return;
                customerOrderPriceGrade.PartsSalesOrderTypeCode = ((PartsSalesOrderType)partsSalesOrderType.UserObject).Code;
                customerOrderPriceGrade.PartsSalesOrderTypeName = ((PartsSalesOrderType)partsSalesOrderType.UserObject).Name;
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "PartsSalesOrderTypeName",
                       // KeyValueItems = this.PartsSalesOrderTypes,
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_CustomerOrderPriceGradeHistory_PartsSalesOrderTypeName
                    },new ColumnItem {
                        Name = "PartsSalesOrderTypeCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_CustomerOrderPriceGradeHistory_PartsSalesOrderTypeCode,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Coefficient",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CustomerOrderPriceGrade);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("CustomerOrderPriceGrades");
        }
    }
}
