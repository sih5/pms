﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CustomerOrderPriceGradeForImportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem {
                        Name = "CustomerCompanyCode",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyCode
                    },new ColumnItem{
                        Name="CustomerCompanyName",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyName
                    },new ColumnItem{
                        Name="PartsSalesOrderTypeName",
                        Title= PartsSalesUIStrings.DataGridView_ColumnItem_Title_CustomerOrderPriceGrade_PartsSalesOrderTypeName
                    },new ColumnItem{
                        Name="Coefficient",
                         Title = PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_Coefficient
                    },new ColumnItem{
                        Name="PartsSalesCategoryName",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CustomerOrderPriceGradeExtend);
            }
        }
    }
}
