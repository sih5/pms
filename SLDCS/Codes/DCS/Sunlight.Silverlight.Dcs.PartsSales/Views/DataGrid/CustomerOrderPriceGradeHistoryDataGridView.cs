﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CustomerOrderPriceGradeHistoryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public CustomerOrderPriceGradeHistoryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.CustomerOrderPriceGradeHistoryDataGridView_DataContextChanged;
        }

        private void CustomerOrderPriceGradeHistoryDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var customerOrderPriceGrade = e.NewValue as CustomerOrderPriceGrade;
            if(customerOrderPriceGrade == null || customerOrderPriceGrade.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "CustomerCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = customerOrderPriceGrade.CustomerCompanyId
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesOrderTypeId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = customerOrderPriceGrade.PartsSalesOrderTypeId
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesCategoryId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = customerOrderPriceGrade.PartsSalesCategoryId
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },  new ColumnItem {
                        Name = "CustomerCompanyCode"
                    }, new ColumnItem {
                        Name = "CustomerCompanyName"
                    }, new ColumnItem {
                        Name = "PartsSalesOrderTypeCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_CustomerOrderPriceGradeHistory_PartsSalesOrderTypeCode
                    }, new ColumnItem {
                        Name = "PartsSalesOrderTypeName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_CustomerOrderPriceGradeHistory_PartsSalesOrderTypeName
                    }, new ColumnItem {
                        Name = "Coefficient"
                    }, new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CustomerOrderPriceGradeHistory);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCustomerOrderPriceGradeHistories";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }
    }
}
