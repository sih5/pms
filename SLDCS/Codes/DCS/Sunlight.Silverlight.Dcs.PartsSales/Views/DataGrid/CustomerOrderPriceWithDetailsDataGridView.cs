﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CustomerOrderPriceWithDetailsDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public CustomerOrderPriceWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "CustomerCompanyCode",
                        IsDefaultGroup = true
                    }, new ColumnItem {
                        Name = "CustomerCompanyName"
                    }, new ColumnItem {
                        Name = "PartsSalesOrderTypeCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_CustomerOrderPriceGrade_PartsSalesOrderTypeCode
                    }, new ColumnItem {
                        Name = "PartsSalesOrderTypeName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_CustomerOrderPriceGrade_PartsSalesOrderTypeName
                    }, new ColumnItem {
                        Name = "Coefficient"
                    }, new ColumnItem {
                        Name = "Remark"
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title= PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CustomerOrderPriceGrade);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCustomerCompanyAndPartsSalesPartsSalesCategory";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.Columns["CustomerCompanyCode"].ShowColumnWhenGrouped = false;
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "CustomerOrderPriceGradeHistory"
                    }
                };
            }
        }
    }
}
