﻿using System;
using System.Collections.Generic; 
using Sunlight.Silverlight.Core.Model; 
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class CustomerSupplyInitialFeeSetDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public CustomerSupplyInitialFeeSetDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem{
                        Name = "CustomerCode"
                    }, new ColumnItem{
                        Name = "CustomerName"
                    }, new ColumnItem{
                        Name = "SupplierCode"
                    }, new ColumnItem{
                        Name = "SupplierName"
                    }, new ColumnItem{
                        Name = "InitialFee"
                    }, new ColumnItem{
                        Name = "AppointShippingDays",
                        Title="约定发货时间"
                    },  new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    }, new ColumnItem{
                        Name = "ModifierName"
                    }, new ColumnItem{
                        Name = "ModifyTime"
                    },new ColumnItem{
                        Name = "PartsSalesCategoryName"
                    },
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CustomerSupplyInitialFeeSet);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCustomerSupplyInitialFeeSets";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["InitialFee"]).DataFormatString = "c2";
        }
    }
}
