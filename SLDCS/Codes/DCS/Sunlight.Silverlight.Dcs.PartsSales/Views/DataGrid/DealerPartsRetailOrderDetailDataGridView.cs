﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerPartsRetailOrderDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "TraceProperty" 
        };
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SerialNumber"
                    }, new ColumnItem{
                        Name = "PartsCode"
                    }, new ColumnItem{
                        Name = "PartsName"
                    }, new ColumnItem{
                        Name = "Quantity"
                    }, new ColumnItem{
                        Name = "Price"
                    }, new ColumnItem{
                        Name = "Remark"
                    }, new KeyValuesColumnItem { 
                        Name = "TraceProperty", 
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]], 
                        Title = "追溯属性" 
                    }, new ColumnItem { 
                        Name = "SIHLabelCode", 
                        Title = "标签码" 
                    } , new ColumnItem { 
                        Name = "SalesPrice", 
                        Title = "经销价" 
                    } , new ColumnItem { 
                        Name = "RetailGuidePrice", 
                        Title = "建议售价" 
                    } , new ColumnItem { 
                        Name = "GroupCode", 
                        Title = "价格属性" 
                    } 
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerRetailOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
        }

        protected override string OnRequestQueryName() {
            return "GetDealerRetailOrderDetails";
        }


        private void PartsOuterPurchaseChangeDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealerPartsRetailOrder = e.NewValue as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null || dealerPartsRetailOrder.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "DealerPartsRetailOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = dealerPartsRetailOrder.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        public DealerPartsRetailOrderDetailDataGridView() {
            this.DataContextChanged += PartsOuterPurchaseChangeDetailDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames); 
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
