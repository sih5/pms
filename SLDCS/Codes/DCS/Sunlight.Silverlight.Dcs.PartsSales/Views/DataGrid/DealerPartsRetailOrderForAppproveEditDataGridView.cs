﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerPartsRetailOrderForAppproveEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "TraceProperty" 
        };

        public DealerPartsRetailOrderForAppproveEditDataGridView() { 
            this.KeyValueManager.Register(this.kvNames); 
        } 
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new ColumnItem[] { 
                    new ColumnItem{
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "PartsCode",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "PartsName",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "Quantity",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "Price",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "Remark"
                    }, new KeyValuesColumnItem { 
                        Name = "TraceProperty", 
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]], 
                        Title = "追溯属性" 
                    }, new ColumnItem { 
                        Name = "SIHLabelCode", 
                        Title = "标签码" 
                    } , new ColumnItem { 
                        Name = "SalesPrice", 
                        Title = "经销价" 
                    } , new ColumnItem { 
                        Name = "RetailGuidePrice", 
                        Title = "建议售价" 
                    } , new ColumnItem { 
                        Name = "GroupCode", 
                        Title = "价格属性" 
                    } 
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerRetailOrderDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerRetailOrderDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
