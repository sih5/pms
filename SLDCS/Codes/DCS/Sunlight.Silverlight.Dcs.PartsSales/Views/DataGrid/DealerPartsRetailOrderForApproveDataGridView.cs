﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerPartsRetailOrderForApproveDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = { 
            "DealerPartsRetailOrder_RetailOrderType","WorkflowOfSimpleApproval_Status"
        };

        public DealerPartsRetailOrderForApproveDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsRetailOrder);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem{
                        Name="Code",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_Code
                    }, new ColumnItem{
                        Name = "DealerCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_DealerCode
                    }, new ColumnItem{
                        Name = "DealerName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_DealerName
                    }, new KeyValuesColumnItem{
                        Name = "RetailOrderType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "Customer",
                        Title= PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_Customer,
                    }, new ColumnItem{
                        Name = "CustomerPhone"
                    }, new ColumnItem{
                        Name = "SubDealerName",
                    }, new ColumnItem{
                        Name = "CustomerCellPhone",
                    }, new ColumnItem{
                        Name = "CustomerUnit",
                    }, new ColumnItem{
                        Name = "Address"
                    }, new ColumnItem{
                        Name = "TotalAmount",
                    }, new ColumnItem{
                        Name = "ApprovalComment",
                    }, new ColumnItem{
                        Name = "Remark",
                    }, new ColumnItem{
                        Name = "CreatorName",
                    }, new ColumnItem{
                        Name = "CreateTime",
                    }, new ColumnItem{
                        Name = "ModifierName",
                    }, new ColumnItem{
                        Name = "ModifyTime"
                    }, new ColumnItem{
                        Name = "ApproverName",
                    }, new ColumnItem{
                        Name = "ApproveTime",
                    }, new ColumnItem{
                        Name = "AbandonerName",
                    }, new ColumnItem{
                        Name = "AbandonTime",
                    }, new ColumnItem{
                        Name = "SalesCategoryName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_SalesCategoryName
                    },new ColumnItem{
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "DealerPartsRetailOrderDetailForApprove"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerPartsRetailOrders";
        }
    }
}
