﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerPartsRetailOrderForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "TraceProperty" 
        };
        public DealerPartsRetailOrderForEditDataGridView() { 
            this.KeyValueManager.Register(this.kvNames); 
        } 
        private RadWindow radQueryWindow;
        private DcsMultiPopupsQueryWindowBase dealerPartsRetailOrderDownQueryWindow;

        private DcsMultiPopupsQueryWindowBase DealerPartsRetailOrderDownQueryWindow {
            get {
                if(this.dealerPartsRetailOrderDownQueryWindow == null) {
                    this.dealerPartsRetailOrderDownQueryWindow = DI.GetQueryWindow("DealerPartsStock") as DcsMultiPopupsQueryWindowBase;
                    this.dealerPartsRetailOrderDownQueryWindow.SelectionDecided += this.DealerPartsRetailOrderDownQueryWindow_SelectionDecided;
                    this.dealerPartsRetailOrderDownQueryWindow.Loaded += dealerPartsRetailOrderDownQueryWindow_Loaded;
                }
                return this.dealerPartsRetailOrderDownQueryWindow;
            }
        }

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.DealerPartsRetailOrderDownQueryWindow,
                    Header = PartsSalesUIStrings.QueryPanel_Title_DealerPartsRetailOrderQueryWindow,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private void dealerPartsRetailOrderDownQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "DealerId", dealerPartsRetailOrder.DealerId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "DealerId", false
            });

            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SalesCategoryId", dealerPartsRetailOrder.PartsSalesCategoryId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "SalesCategoryId", false
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("SubDealerId", typeof(int), FilterOperator.IsEqualTo, -1));

        }

        private void DealerPartsRetailOrderDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            if(dealerPartsRetailOrder == null)
                return;
            var dealerPartsStocks = queryWindow.SelectedEntities.Cast<DealerPartsStockForBussiness>();
            if(dealerPartsStocks == null)
                return;
            if(dealerPartsRetailOrder.DealerRetailOrderDetails.Any(r => dealerPartsStocks.Any(ex => ex.SparePartCode == r.PartsCode))) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_VirtualPartsPurchaseOrderDetail_SparePartCode);
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;

            foreach(var item in dealerPartsStocks) {
                dealerPartsRetailOrder.DealerRetailOrderDetails.Add(new DealerRetailOrderDetail {
                    SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<DealerRetailOrderDetail>().Max(entity => entity.SerialNumber) + 1 : 1,
                    PartsId = item.SparePartId,
                    PartsCode = item.SparePartCode,
                    PartsName = item.SparePartName,
                    Quantity = default(int),
                    Price = item.RetailGuidePrice,
                    TraceProperty = item.TraceProperty,
                    RetailGuidePrice =item.RetailGuidePrice,
                    SalesPrice=item.SalesPrice,
                    GroupCode=item.GroupCode
                });
            }

        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem{
                        Name = "SerialNumber",
                        IsReadOnly=true
                    }, new DropDownTextBoxColumnItem{
                        Name = "PartsCode",
                        DropDownContent = this.DealerPartsRetailOrderDownQueryWindow,
                        IsReadOnly = false,
                    }, new ColumnItem{
                        Name = "PartsName",
                        IsReadOnly = true,
                    }, new ColumnItem{
                        Name = "Quantity",
                        IsReadOnly = false,
                    }, new ColumnItem{
                        Name = "Price",
                        IsReadOnly=false
                    }, new ColumnItem{
                        Name = "Remark",
                    }, new KeyValuesColumnItem { 
                        Name = "TraceProperty", 
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]], 
                        Title = "追溯属性" ,
                        IsReadOnly = true,
                    }, new ColumnItem { 
                        Name = "SIHLabelCode", 
                        Title = "标签码" 
                    } , new ColumnItem { 
                        Name = "SalesPrice", 
                        Title = "经销价" ,
                        IsReadOnly=true
                    } , new ColumnItem { 
                        Name = "RetailGuidePrice", 
                        Title = "建议售价" ,
                        IsReadOnly=true
                    } , new ColumnItem { 
                        Name = "GroupCode", 
                        Title = "价格属性" ,
                        IsReadOnly=true
                    } 
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerRetailOrderDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerRetailOrderDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.CellEditEnded -= GridView_CellEditEnded;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            var serialNumber = 1;
            foreach(var dealerRetailOrderDetail in dealerPartsRetailOrder.DealerRetailOrderDetails.Where(entity => !e.Items.Contains(entity))) {
                dealerRetailOrderDetail.SerialNumber = serialNumber;
                serialNumber++;
            }
            dealerPartsRetailOrder.TotalAmount = dealerPartsRetailOrder.DealerRetailOrderDetails.Sum(ex => ex.Quantity * ex.Price);
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            e.Cancel = true;
            if(dealerPartsRetailOrder.BranchId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_DealerPartsRetailOrder_DealerNameIsNull);
                return;
            }
            if(dealerPartsRetailOrder.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_DealerPartsRetailOrder_PartsSalesCategoryNameIsNull);
                return;
            }
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            RadQueryWindow.ShowDialog();
            //e.Cancel = true;
            //RadQueryWindow.ShowDialog();
            //var maxSerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<DealerRetailOrderDetail>().Max(entity => entity.SerialNumber) + 1 : 1;
            //e.NewObject = new DealerRetailOrderDetail {
            //    SerialNumber = maxSerialNumber
            //};
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            dealerPartsRetailOrder.TotalAmount = dealerPartsRetailOrder.DealerRetailOrderDetails.Sum(ex => ex.Quantity * ex.Price);
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
