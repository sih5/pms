﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerPartsRetailOrderForImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                        Name = "PartCode"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                        Name = "PartName"
                    }, new ColumnItem {
                        Name = "QuantityStr",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Qty
                    }, new ColumnItem{
                        Name="PriceStr",
                        Title=PartsSalesUIStrings.DataEdit_Text_DealerPartsRetailOrder_Price
                    },new ColumnItem{
                        Name = "Remark", 
                        Title = PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsRetailOrderExtend);
            }
        }
    }
}
