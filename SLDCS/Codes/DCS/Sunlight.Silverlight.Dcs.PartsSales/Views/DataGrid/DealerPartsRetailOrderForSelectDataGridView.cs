﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerPartsRetailOrderForSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "WorkflowOfSimpleApproval_Status","DealerPartsRetailOrder_RetailOrderType"
        };

        public DealerPartsRetailOrderForSelectDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                     new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem{
                        Name="Code",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_Code
                    }, new ColumnItem{
                        Name = "DealerCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_DealerCode
                    }, new ColumnItem{
                        Name = "DealerName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_DealerName
                    }, new KeyValuesColumnItem{
                         Name = "RetailOrderType",
                          KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem{
                        Name = "Customer",
                        Title= PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_Customer,
                    }, new ColumnItem{
                        Name = "CustomerPhone"
                    }, new ColumnItem{
                        Name = "SubDealerName",
                    }, new ColumnItem{
                        Name = "CustomerCellPhone",
                    }, new ColumnItem{
                        Name = "CustomerUnit",
                    }, new ColumnItem{
                        Name = "Address"
                    }, new ColumnItem{
                        Name = "TotalAmount",
                    }, new ColumnItem{
                        Name = "ApprovalComment",
                    }, new ColumnItem{
                        Name = "Remark",
                    }, new ColumnItem{
                        Name = "CreatorName",
                    }, new ColumnItem{
                        Name = "CreateTime",
                    }, new ColumnItem{
                        Name = "ModifierName",
                    }, new ColumnItem{
                        Name = "ModifyTime",
                    }, new ColumnItem{
                        Name = "ApproverName",
                    }, new ColumnItem{
                        Name = "ApproveTime",
                    }, new ColumnItem{
                        Name = "AbandonerName",
                    }, new ColumnItem{
                        Name = "AbandonTime",
                    },new ColumnItem{
                        Name = "BranchName"
                    }, new ColumnItem{
                        Name = "SalesCategoryName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_SalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsRetailOrder);
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach (var filter in compositeFilterItem.Filters.Where(item => item.MemberName != "SparePartId"))
                newCompositeFilterItem.Filters.Add(filter);
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filter = this.FilterItem as CompositeFilterItem;
            if (filter == null)
                return null;
            FilterItem filterItem;
            switch (parameterName)
            {
                case "partid":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SparePartId");
                    return filterItem == null ? null : filterItem.Value;
                case "dealerId":
                    return 0;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return "查询配件索赔服务站零售单";
        }
    }
}
