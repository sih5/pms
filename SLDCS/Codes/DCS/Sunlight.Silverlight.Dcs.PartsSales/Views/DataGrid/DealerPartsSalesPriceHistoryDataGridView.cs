﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerPartsSalesPriceHistoryDataGridView : DcsDataGridViewBase {
        public DealerPartsSalesPriceHistoryDataGridView() {
            this.DataContextChanged += this.DealerPartsSalesPriceHistoryDataGridView_DataContextChanged;
        }

        private void DealerPartsSalesPriceHistoryDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealerPartsSalesPrice = e.NewValue as DealerPartsSalesPrice;
            if(dealerPartsSalesPrice == null || dealerPartsSalesPrice.SparePartId == default(int))
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "SparePartId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = dealerPartsSalesPrice.SparePartId;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsSalesPriceHistory);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "DealerSalesPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerPartsSalesPriceHistories";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["DealerSalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }
    }
}
