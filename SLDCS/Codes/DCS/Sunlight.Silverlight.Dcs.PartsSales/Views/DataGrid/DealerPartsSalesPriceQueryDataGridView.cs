﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerPartsSalesPriceQueryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public DealerPartsSalesPriceQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                    },new ColumnItem {
                        Name = "PartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "PartName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsSalesPrice_SalesPrice
                    }, new ColumnItem {
                        Name = "RetailGuidePrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsSalesPrice_RetailGuidePrice
                    }
                };
            }
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null) {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName) {
                    case "sparePartCode":
                        var filterSparePartCode = filters.Filters.SingleOrDefault(item => item.MemberName == "PartCode");
                        return filterSparePartCode == null ? null : filterSparePartCode.Value;
                    case "sparePartName":
                        var filterSparePartName = filters.Filters.SingleOrDefault(item => item.MemberName == "PartName");
                        return filterSparePartName == null ? null : filterSparePartName.Value;
                    case "status":
                        var filterStatus = filters.Filters.SingleOrDefault(item => item.MemberName == "Status");
                        return filterStatus == null ? null : filterStatus.Value;
                    case "bCreateTime":
                        var createTime = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return "查询配件经销价和零售价";
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualDealerPartsSalesPrice);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
        }
    }
}
