﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerPartsSalesReturnBillWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsRetailReturnBill_Status"
        };

        public DealerPartsSalesReturnBillWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsSalesReturnBill);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceCode
                    }, new ColumnItem {
                        Name = "SourceCode",
                        Title=PartsSalesUIStrings.DataGridView_Title_DealerPartsSalesReturnBill_SourceCode
                    }, new ColumnItem {
                        Name = "Customer",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Name
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsSalesUIStrings.DetailPanel_Text_PartsSalesReturnBill_TotalAmount
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                    }, new ColumnItem {
                        Name = "Remark",
                        Title=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                    }, new ColumnItem {
                        Name = "ApproverName",
                        Title=PartsSalesUIStrings.DataGridView_Title_DealerPartsSalesReturnBill_ApproverName
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsSalesUIStrings.DataGridView_Title_DealerPartsSalesReturnBill_ApproveTime
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerPartsSalesReturnBills";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                         "DealerRetailReturnBillDetail"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }
    }
}
