﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerPartsStockDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "DealerCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_DealerCode
                    }, new ColumnItem{
                        Name = "DealerName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_DealerName
                    }, new ColumnItem{
                        Name = "SalesCategoryName", //PartsSalesCategory.Name
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_SalesCategoryName
                    }, new ColumnItem{
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SparePartCode
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SparePartName,
                    }, new ColumnItem{
                        Name = "Quantity",
                         Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_Quantity,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name = "RetailGuidePrice",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_RetailGuidePrice 
                    }, new ColumnItem{
                        Name = "SalesPrice",
                        Title = "经销价" 
                    }, new ColumnItem{
                        Name = "GroupCode",
                        Title = "价格属性"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsStockForBussiness);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询经销商配件零售价2";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "dealerId":
                    var dealerFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "DealerId");
                    return dealerFilterItem == null ? null : dealerFilterItem.Value;
                case "salesCategoryId":
                    var categoryFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SalesCategoryId");
                    return categoryFilterItem == null ? null : categoryFilterItem.Value;
                case "sparePartId":
                    return null;
                case "subDealerId":
                    var subFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SubDealerId");
                    return subFilterItem == null ? null : subFilterItem.Value;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "DealerId", "SalesCategoryId", "SubDealerId" };
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => !param.Contains(filter.MemberName))) {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }


    }
}
