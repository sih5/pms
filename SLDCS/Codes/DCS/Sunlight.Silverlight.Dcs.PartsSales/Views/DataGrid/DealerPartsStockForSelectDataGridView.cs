﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerPartsStockForSelectDataGridView : DcsDataGridViewBase {
        private readonly ObservableCollection<KeyValuePair> kVSalesCategories = new ObservableCollection<KeyValuePair>();
      //  private readonly string[] kvNames = { "Company_Type" };

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "DealerCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_DealerCode
                    }, new ColumnItem {
                        Name = "DealerName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_DealerName
                    },
                    new ColumnItem {
                        Name = "DealerType",
                        Title=PartsSalesUIStrings.QueryPanel_Title_VirtualDealerPartsStock_CompanyType
                    }
                    , new ColumnItem {
                        Name = "MarketingDepartmentName",
                        Title= PartsSalesUIStrings.DataGridView_Column_Text_MktName,
                    }
                    //, new ColumnItem {
                    //    Name = "SubDealerCode",
                    //    Title = "二级站编号"
                    //}, new ColumnItem {
                    //    Name = "SubDealerName",
                    //    Title = "二级站名称"
                    //}
                    , new KeyValuesColumnItem {
                        Name = "SalesCategoryId",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_SalesCategoryName,
                        KeyValueItems = this.kVSalesCategories
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem {
                        Name = "Quantity",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Qty,
                        TextAlignment = TextAlignment.Right
                    }
                    //,new KeyValuesColumnItem {
                    //    Name = "PriceType",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    //    Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualDealerPartsStock_PriceType
                    //}
                    ,new ColumnItem{
                        Name="BasicSalePrice",
                       Title=PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsSalesPrice_SalesPrice,
                        TextAlignment = TextAlignment.Right
                    }
                     ,new ColumnItem{
                        Name="BasicSalePriceAll",
                       Title=PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsStock_BasicSalePriceAll,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualDealerPartsStock);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            ((GridViewDataColumn)this.GridView.Columns["BasicSalePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["BasicSalePriceAll"]).DataFormatString = "c2";
        }

        protected override string OnRequestQueryName() {
            return "GetDealerPartsStocksWithSubDealer";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null) {
                switch (parameterName) {
                    case "sparePartCode":
                        var filterSparePartCode = filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode");
                        return filterSparePartCode == null ? null : filterSparePartCode.Value;
                    case "isDealerQuery":
                        var isAgencyQuery = filters.Filters.SingleOrDefault(item => item.MemberName == "IsDealerQuery");
                        return isAgencyQuery == null ? false : isAgencyQuery.Value;
                    case "isZero":
                        var isZero = filters.Filters.SingleOrDefault(item => item.MemberName == "IsZero");
                        return isZero == null ? false : isZero.Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                var param = new[] { "IsDealerQuery", "IsZero" };
                foreach(var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName)))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        public DealerPartsStockForSelectDataGridView() {
           // this.KeyValueManager.Register(kvNames);
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null && loadOp.Entities.Any())
                    foreach(var entity in loadOp.Entities) {
                        this.kVSalesCategories.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Name
                        });
                    }
            }, null);
        }
    }
}