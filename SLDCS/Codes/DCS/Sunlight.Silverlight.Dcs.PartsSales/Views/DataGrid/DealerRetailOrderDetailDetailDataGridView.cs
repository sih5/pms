﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerRetailOrderDetailDetailDataGridView : DcsDataGridViewBase {
        public DealerRetailOrderDetailDetailDataGridView() {
            this.DataContextChanged += this.PartsOutboundBillDetailForPartsRetailOrderDataGridView_DataContextChanged;
        }

        private void PartsOutboundBillDetailForPartsRetailOrderDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.ExecuteQueryDelayed();
        }

        /// <summary>
        /// 根据配件销售业务下， 配件零售退货 节点设计需求。选择 配件出库单 时，同时带回 配件零售退货 实体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView_DataLoaded(object sender, EventArgs e) {
           if(this.GridView.Items.Count > 0) {
               var partsRetailOrder = this.DataContext as DealerPartsRetailOrder;
                if(partsRetailOrder == null)
                    return;
                foreach(var entity in this.GridView.Items.Cast<DealerRetailOrderDetail>())
                    entity.DealerPartsRetailOrder = partsRetailOrder;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "DealerPartsRetailOrder.Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_Code
                    }, new ColumnItem {
                        Name = "PartsCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "PartsName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem {
                        Name = "Quantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Qty
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerRetailOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["Quantity"]).DataFormatString = "d";
            this.DataLoaded -= this.GridView_DataLoaded;
            this.DataLoaded += this.GridView_DataLoaded;
        }

        protected override string OnRequestQueryName() {
            return "服务站零售订单退货查询";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "dealerPartsRetailOrderId":
                    var partsRetailOrder = this.DataContext as DealerPartsRetailOrder;
                    return partsRetailOrder != null ? partsRetailOrder.Id : default(int);               
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

    }
}
