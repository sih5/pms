﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class DealerRetailOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {  
            "TraceProperty"  
        };
        public DealerRetailOrderDetailForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);  
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerRetailReturnBillDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsCode",
                        IsReadOnly = true,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "PartsName",
                        IsReadOnly = true,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem {
                        Name = "Quantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Qty
                    }, new ColumnItem {
                        Name = "Price",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsRetailGuidePrice_RetailGuidePrice
                    }, new KeyValuesColumnItem {  
                        Name = "TraceProperty",  
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]],  
                        Title = "追溯属性"  ,
                        IsReadOnly = true,
                    }, new ColumnItem {  
                        Name = "SIHLabelCode",  
                        Title = "标签码"  
                    } 
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["Quantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerRetailReturnBillDetails");
        }
    }
}

