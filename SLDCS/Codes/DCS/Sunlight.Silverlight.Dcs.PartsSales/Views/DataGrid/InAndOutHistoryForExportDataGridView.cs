﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class InAndOutHistoryForExportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceCode
                    },new ColumnItem {
                        Name = "OutInBandtype",
                        Title = PartsSalesUIStrings.DataGridView_Title_DealerPartsStockOutInRecord_OutInBandtype
                    },new ColumnItem {
                        Name = "PartsCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    },new ColumnItem {
                        Name = "PartsName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Qty
                    },new ColumnItem {
                        Name = "CreateTime",
                        Title = PartsSalesUIStrings.QueryPanel_Title_DealerPartsStockOutInRecord_CreateTime
                    }
                };
            }
        }


        protected override string OnRequestQueryName() {
            return "出入库记录查询";
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsStockOutInRecord);
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var dateFilter = filters.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                DateTime? dateTimeBegin = null;
                DateTime? dateTimeEnd = null;
                if(dateFilter != null) {
                    //定义existTimeFilter变量用于判断是否存在创建时间这个过滤条件
                    var existTimeFilter = dateFilter.Filters.FirstOrDefault(r => r.MemberName == "CreateTime");
                    if(existTimeFilter != null) {
                        dateTimeBegin = dateFilter.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        dateTimeEnd = dateFilter.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                }
                switch(parameterName) {
                    case "billCode":
                        FilterItem filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "code");
                        if(filterItem != null)
                            return filterItem.Value;
                        return null;
                    case "sparepartIds":
                        var sparepartIds = filters.Filters.LastOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        FilterItem partIdsfilterItem = sparepartIds.Filters.SingleOrDefault(item => item.MemberName == "SparePartIds");
                        if(partIdsfilterItem == null)
                            return null;
                        return partIdsfilterItem.Value;
                    case "dateTimeBegin":
                        return dateTimeBegin;
                    case "dateTimeEnd":
                        if(!dateTimeEnd.HasValue)
                            return null;
                        return dateTimeEnd.Value > DateTime.MaxValue.AddDays(-1) ? dateTimeEnd.Value : dateTimeEnd.Value.AddDays(1);
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
