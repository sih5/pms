﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class InvoiceInformationForPartsSalesSettlementDataGridView : DcsDataGridViewBase {

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {

            var invoiceInformation = e.Cell.DataContext as InvoiceInformation;
            if(invoiceInformation == null)
                return;
            invoiceInformation.ValidationErrors.Clear();
            switch(e.Cell.Column.UniqueName) {
                case "InvoiceAmount":
                    if(invoiceInformation.TaxRate != null && (1 + invoiceInformation.TaxRate) != 0)
                        invoiceInformation.InvoiceTax = (invoiceInformation.InvoiceAmount / (decimal)(1 + invoiceInformation.TaxRate)) * (decimal)invoiceInformation.TaxRate;
                    break;
            }
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var serialNumber = 1;
            foreach(var information in this.GridView.Items.Cast<InvoiceInformation>())
                information.SerialNumber = serialNumber++;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var dataEditView = this.DataContext as FrameworkElement;
            if(dataEditView == null)
                return;
            var partsSalesSettlement = dataEditView.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            e.NewObject = new InvoiceInformation {
                Code = GlobalVar.ASSIGNED_BY_SERVER,
                SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<InvoiceInformation>().Max(entity => entity.SerialNumber) + 1 : 1,
                TaxRate = partsSalesSettlement.TaxRate,
                InvoicePurpose = (int)DcsInvoiceInformationInvoicePurpose.配件销售,
                InvoiceCompanyId = partsSalesSettlement.SalesCompanyId,
                InvoiceCompanyCode = partsSalesSettlement.SalesCompanyCode,
                InvoiceCompanyName = partsSalesSettlement.SalesCompanyName,
                InvoiceReceiveCompanyId = partsSalesSettlement.CustomerCompanyId,
                InvoiceReceiveCompanyCode = partsSalesSettlement.CustomerCompanyCode,
                InvoiceReceiveCompanyName = partsSalesSettlement.CustomerCompanyName,
                Type = (int)DcsInvoiceInformationType.增值税发票,
                SourceId = partsSalesSettlement.Id,
                SourceType = (int)DcsInvoiceInformationSourceType.配件销售结算单,
                SourceCode = partsSalesSettlement.Code,
                Status = (int)DcsInvoiceInformationStatus.已开票,
                OwnerCompanyId = partsSalesSettlement.SalesCompanyId,
            };
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "InvoiceCode"
                    }, new ColumnItem {
                        Name = "InvoiceNumber"
                    }, new ColumnItem {
                        Name = "InvoiceAmount",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_InvoiceInformation_InvoiceAmount
                    }, new ColumnItem {
                        Name = "TaxRate",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric
                    }, new ColumnItem {
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        Name = "InvoiceTax"
                    }, new ColumnItem {
                        Name = "InvoiceDate",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(InvoiceInformation);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.CellEditEnded -= GridView_CellEditEnded;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.Deleting -= this.GridView_Deleting;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["InvoiceAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TaxRate"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["InvoiceTax"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["InvoiceDate"]).DataFormatString = "d";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("InvoiceInformations");
        }
    }
}
