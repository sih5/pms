﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class InvoiceInformationForPartsSalesSettlementQueryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "InvoiceInformation_SourceType", "InvoiceInformation_InvoicePurpose", "InvoiceInformation_Type", "InvoiceInformation_Status"
        };

        public InvoiceInformationForPartsSalesSettlementQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                  new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[kvNames[3]],
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "Status")
                    }, new ColumnItem {
                        Name = "InvoiceCode",
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceCode")
                    }, new ColumnItem {
                        Name = "InvoiceNumber",
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceNumber")
                    }, new ColumnItem {
                        Name = "InvoiceCompanyCode",
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceCompanyCode")
                    }, new ColumnItem {
                        Name = "InvoiceCompanyName",
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceCompanyName")
                    },new ColumnItem {
                        Name = "InvoiceReceiveCompanyCode",
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceReceiveCompanyCode")
                    }, new ColumnItem {
                        Name = "InvoiceReceiveCompanyName",
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceReceiveCompanyName")
                    }, new ColumnItem {
                        Name = "InvoiceAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceAmount")
                    }, new ColumnItem {
                        Name = "TaxRate",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "TaxRate")
                    }, new ColumnItem {
                        Name = "InvoiceTax",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceTax")
                    }, new ColumnItem {
                        Name = "InvoiceDate",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoiceDate")
                    }, new KeyValuesColumnItem {
                        Name = "SourceType",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "SourceType")
                    }, new ColumnItem {
                        Name = "SourceCode",
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "SourceCode")
                    }, new KeyValuesColumnItem {
                        Name = "InvoicePurpose",
                        KeyValueItems = this.KeyValueManager[kvNames[1]],
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "InvoicePurpose")
                    }, new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[kvNames[2]],
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "Type")
                    },  new ColumnItem {
                        Name = "Code",
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "Code")
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "CreatorName")
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title = Utils.GetEntityLocalizedName(typeof(InvoiceInformation), "CreateTime")
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "根据业务类型查询发票信息";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 500;  //配件销售结算发票查询每页显示500条
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["InvoiceAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["InvoiceTax"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TaxRate"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["InvoiceDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "InvoiceInformationForPartsSalesSettlement","PartsSalesSettleInvoiceDetail"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualInvoiceInformation);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }
    }
}
