﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class IvecoPriceChangeAppDetailDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(IvecoPriceChangeAppDetail);
            }
        }

        public IvecoPriceChangeAppDetailDataGridView() {
        }

        protected override string OnRequestQueryName() {
            return "GetIvecoPriceChangeAppDetails";// GetPartsSalesPriceChangeDetails
        }
        private int id;
        void PartsSalesPriceChangeDetailDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null || partsSalesPriceChange.Id == default(int))
                return;
            this.id = partsSalesPriceChange.Id;
            this.ExecuteQueryDelayed();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                         Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "IvecoPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_IvecoPriceChangeAppDetail_IvecoPrice
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "parentId":
                    return id;
                default:
                    return null;
            }
        }
    }
}
