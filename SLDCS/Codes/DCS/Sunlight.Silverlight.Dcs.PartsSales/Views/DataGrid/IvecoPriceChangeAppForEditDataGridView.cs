﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class IvecoPriceChangeAppForEditDataGridView : DcsDataGridViewBase {
        public IvecoPriceChangeAppForEditDataGridView() {
        }



        private QueryWindowBase sparePartPriceQueryWindow;
        private QueryWindowBase SparePartPriceQueryWindow {
            get {
                if(this.sparePartPriceQueryWindow == null) {
                    this.sparePartPriceQueryWindow = DI.GetQueryWindow("SparePartDropDown");
                    this.sparePartPriceQueryWindow.SelectionDecided += partsSalesPriceQueryPanel_SelectionDecided;
                }
                return this.sparePartPriceQueryWindow;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(IvecoPriceChangeAppDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("IvecoPriceChangeAppDetails");
        }

        void partsSalesPriceQueryPanel_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var ivecoPriceChangeAppDetail = queryWindow.DataContext as IvecoPriceChangeAppDetail;
            if(ivecoPriceChangeAppDetail == null)
                return;
            ivecoPriceChangeAppDetail.SparePartId = sparePart.Id;
            ivecoPriceChangeAppDetail.SparePartCode = sparePart.Code;
            ivecoPriceChangeAppDetail.SparePartName = sparePart.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();

            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            var domainContext = new DcsDomainContext();
            if(partsSalesPriceChange.PartsSalesCategoryId == default(int))
                return;
            domainContext.Load(domainContext.查询配件销售价变更申请清单附带采购价指导价Query(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesPriceChange.PartsSalesCategoryId, 0, null, partsSalesPriceChange.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(loadOp.Entities == null)
                    return;
                var virtualPartsSalesPriceChangeDetail = loadOp.Entities.FirstOrDefault();
                if(virtualPartsSalesPriceChangeDetail != null) {
                    //ivecoPriceChangeAppDetail.PurchasePrice = virtualPartsSalesPriceChangeDetail.PurchasePrice;
                    //ivecoPriceChangeAppDetail.PartsSalesPrice_SalePrice = virtualPartsSalesPriceChangeDetail.PartsSalesPrice_SalePrice;
                    //ivecoPriceChangeAppDetail.PartsRetailGuidePrice_RetailGuidePrice = virtualPartsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice;
                }
            }, null);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "SparePartCode",
                        DropDownContent = this.SparePartPriceQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsSalesPrice_SalesPrice,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "IvecoPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_IvecoPriceChangeAppDetail_IvecoPrice,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            //this.GridView.CellEditEnded -= GridView_CellEdited;
            //this.GridView.CellEditEnded += GridView_CellEdited;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            //e.NewObject = new PartsSalesPriceChangeDetail {
            //    PriceType = (int)DcsPartsSalesPricePriceType.基准销售价,
            //    ValidationTime = DateTime.Now.Date,
            //    ExpireTime = new DateTime(DateTime.Now.Year, 12, 31)
            //};
        }

        void GridView_CellEdited(object sender, GridViewCellEditEndedEventArgs e) {
            var partsSalesPriceChangeDetail = e.Cell.DataContext as PartsSalesPriceChangeDetail;
            var partsSalesPriceChang = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChangeDetail == null)
                return;
            if(e.Cell.DataColumn.UniqueName == "SalesPrice" && partsSalesPriceChangeDetail.SalesPrice > 0) {
                partsSalesPriceChangeDetail.IsUpsideDown = partsSalesPriceChangeDetail.SalesPrice >= partsSalesPriceChangeDetail.PurchasePrice ? 0 : 1;
            }
        }
    }
}
