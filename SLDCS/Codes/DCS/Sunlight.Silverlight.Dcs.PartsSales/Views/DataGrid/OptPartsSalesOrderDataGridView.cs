﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {

    public class OptPartsSalesOrderDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "PartsSalesOrder_SecondLevelOrderType", "PartsSalesOrder_Status"
        };

        public OptPartsSalesOrderDataGridView() {
            this.LoadKeyValue();
        }

        protected void LoadKeyValue() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "SalesUnitName"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_SalesUnitOwnerCompanyName,
                        Name = "SalesUnitOwnerCompanyName"
                    }, new ColumnItem {
                        Name = "SubmitCompanyName"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_PartsSalesOrderTypeName,
                        Name = "PartsSalesOrderTypeName"
                    }, new ColumnItem {
                        Name = "IfDirectProvision"
                    },  new ColumnItem {
                        Name = "ReceivingAddress"
                    }, new ColumnItem {
                        Name = "SalesActivityDiscountRate",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric
                    }, new ColumnItem {
                        Name = "SalesActivityDiscountAmount",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }, new ColumnItem {
                        Name = "SubmitterName"
                    }, new ColumnItem {
                        Name = "SubmitTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    },new ColumnItem{
                        Name="SalesCategoryName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrder);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["SalesActivityDiscountRate"]).DataFormatString = "p";
            ((GridViewDataColumn)this.GridView.Columns["SalesActivityDiscountAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesOrders";
        }

        //protected override object OnRequestQueryParameter(string queryName, string parameterName) {
        //    switch(parameterName) {
        //        case "SubmitCompanyId":
        //            return BaseApp.Current.CurrentUserData.UserId;
        //    }
        //    return base.OnRequestQueryParameter(queryName, parameterName);
        //}


        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSalesOrder", "PartsSalesOrderDetail", "PartsSalesOrderProcess", "PartsOutboundBillDetailForPartsSales"
                    }
                };
            }
        }
    }
}
