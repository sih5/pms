﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class OrderApproveWeekdayDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status","OrderAppDate"
        };
        public OrderApproveWeekdayDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "ProvinceName"
                    }, new KeyValuesColumnItem{
                        Name = "WeeksName",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName", 
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_SalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OrderApproveWeekday);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetOrderApproveWeekdays";
        }
    }
}
