﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class OverstockPartsRecommendBillSubmitDataGridView : DcsDataGridViewBase {
        public OverstockPartsRecommendBillSubmitDataGridView() {
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CenterName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_CenterName,
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name = "WarehouseCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseCode,
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name = "DealerCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DealerCode,
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name = "DealerName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DealerName,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_SparePartCode,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_SparePartName,
                        IsReadOnly=true
                    },  new ColumnItem {
                        Name = "OverstockPartsAmount",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_OverstockPartsAmount,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Price",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_Price,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "DiscountRate",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DiscountRate
                    },new ColumnItem {
                        Name = "CreateTime",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_CreateTime,
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockPartsRecommendBill);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VirtualOverstockPartsRecommendBills");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            this.GridView.CellValidating += GridView_CellValidating;
            //this.GridView.CellEditEnded += GridView_CellEditEnded;
        }

        private void GridView_CellValidating(object sender, Telerik.Windows.Controls.GridViewCellValidatingEventArgs e) {
            var overstockPartsRecommendBill = e.Cell.DataContext as OverstockPartsRecommendBill;
            if(overstockPartsRecommendBill == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "DiscountRate":
                    if(string.IsNullOrEmpty(e.NewValue.ToString())) {
                        e.IsValid = false;
                        e.ErrorMessage = string.Format("折让比例不能为空");
                    }
                    break;
            }
        }
    }
}
