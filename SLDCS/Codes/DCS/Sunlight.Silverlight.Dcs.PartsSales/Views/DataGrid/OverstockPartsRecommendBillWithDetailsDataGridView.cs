﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class OverstockPartsRecommendBillWithDetailsDataGridView : DcsDataGridViewBase {
        public OverstockPartsRecommendBillWithDetailsDataGridView() {
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem {
                        Name = "CenterName",
                        Title=PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_CenterCompanyName
                    }, new ColumnItem{
                        Name = "DealerCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DealerCode
                    }, new ColumnItem{
                        Name = "DealerName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_DealerName
                    }, new ColumnItem{
                        Name = "WarehouseCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseCode
                    }, new ColumnItem{
                        Name = "WarehouseName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseName
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem {
                        Name = "OverstockPartsAmount",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_OverstockPartsAmount
                    }, new ColumnItem {
                        Name = "Price",
                        Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockPartsRecommendBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetOverstockPartsRecommendBillAmounts";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
        }
    }
}
