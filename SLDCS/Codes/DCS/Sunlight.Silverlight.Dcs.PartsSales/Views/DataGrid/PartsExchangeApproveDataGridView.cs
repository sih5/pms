﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsExchangeApproveDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsReplacement_ReplacementType"
        };

        protected override Type EntityType {
            get {
                return typeof(WarehousePartsStock);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        public PartsExchangeApproveDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.Loaded += PartsExchangeApproveDataGridView_Loaded;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_ReplacementType,
                        Name = "ReplacementType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_Title_WarehousePartsStock_NewPartCode,
                        Name = "NewPartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_NewPartName,
                        Name = "NewPartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_CurrentFulfilledQuantity,
                        Name = "UsableQuantity"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_UsableQuantity,
                        Name = "Quantity",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PartsSalesPrice",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsSalesPrice
                    }
                };
            }
        }

        //protected override string OnRequestQueryName() {
        //    return "GetWarehousePartsStocks";
        //}

        //protected override System.Windows.Data.Binding OnRequestDataSourceBinding() {
        //    return new Binding("WarehousePartsStocks");
        //}

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            ((GridViewDataColumn)this.GridView.Columns["PartsSalesPrice"]).DataFormatString = "c2";
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            int priceType = 0;
            if(this.DataContext is PartsSalesOrderForTemplateApproveDataEditView) {
                var dataEditView = this.DataContext as PartsSalesOrderForTemplateApproveDataEditView;
                var partsSalesOrder = dataEditView.DataContext as PartsSalesOrder;
                priceType = partsSalesOrder != null ? partsSalesOrder.PriceType ?? 0 : 0;
            }
            if(this.DataContext is PartsSalesOrderForExportApproveDataEditView) {
                var dataEditView = this.DataContext as PartsSalesOrderForExportApproveDataEditView;
                var partsSalesOrder = dataEditView.DataContext as PartsSalesOrder;
                priceType = partsSalesOrder != null ? partsSalesOrder.PriceType ?? 0 : 0;
            }
            if(this.DataContext is PartsSalesOrderForSeniorApproveDataEditView) {
                var dataEditView = this.DataContext as PartsSalesOrderForSeniorApproveDataEditView;
                var partsSalesOrder = dataEditView.DataContext as PartsSalesOrder;
                priceType = partsSalesOrder != null ? partsSalesOrder.PriceType ?? 0 : 0;
            }
            if(e.Cell.Column.UniqueName == "PartsSalesPrice" && priceType != (int)DcsExportCustPriceType.其他) {
                e.Cancel = true;
            }
        }

        private void PartsExchangeApproveDataGridView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            ObservableCollection<WarehousePartsStock> warehousePartsStocks = null;
            //界面共用
            if(this.DataContext is PartsSalesOrderForTemplateApproveDataEditView) {
                var dataEditView = this.DataContext as PartsSalesOrderForTemplateApproveDataEditView;
                warehousePartsStocks = dataEditView.WarehousePartsStocks;
            }
            if(this.DataContext is PartsSalesOrderForExportApproveDataEditView) {
                var dataEditView = this.DataContext as PartsSalesOrderForExportApproveDataEditView;
                warehousePartsStocks = dataEditView.WarehousePartsStocks;
            }
            if(this.DataContext is PartsSalesOrderForSeniorApproveDataEditView) {
                var dataEditView = this.DataContext as PartsSalesOrderForSeniorApproveDataEditView;
                warehousePartsStocks = dataEditView.WarehousePartsStocks;
            }
            this.GridView.ItemsSource = warehousePartsStocks;
        }
    }
}
