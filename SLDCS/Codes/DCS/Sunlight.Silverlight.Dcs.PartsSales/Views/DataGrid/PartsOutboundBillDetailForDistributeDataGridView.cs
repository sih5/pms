﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsOutboundBillDetailForDistributeDataGridView : DcsDataGridViewBase {
        private void PartsOutboundBillDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            //var vehiclePartsHandleOrder = e.NewValue as VehiclePartsHandleOrder;
            //if(vehiclePartsHandleOrder == null)
            //    return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "VehiclePartsHandleOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            //this.FilterItem.Value = vehiclePartsHandleOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsOutboundBill.Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_Code
                    }, new ColumnItem {
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_Code,
                        Name = "PartsOutboundBill.PartsOutboundPlan.Code"
                    }, new ColumnItem {
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBillDetail_CreateTime,
                        Name = "PartsOutboundBill.CreateTime"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_OutboundQuantity,
                        Name = "OutboundAmount"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOutboundBillDetail);
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            if(this.FilterItem != null) {
                switch(parameterName) {
                    case "handId":
                        var handId = this.FilterItem.Value as int?;
                        return handId;
                    default:
                        return null;
                }
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();

            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "GetPartsOutboundBillDetailsByHandId";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        public PartsOutboundBillDetailForDistributeDataGridView() {
            this.DataContextChanged += PartsOutboundBillDetailDataGridView_DataContextChanged;
        }
    }
}
