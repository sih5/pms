﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsOutboundBillDetailForPartsRetailOrderDataGridView : DcsDataGridViewBase {
        public PartsOutboundBillDetailForPartsRetailOrderDataGridView() {
            this.DataContextChanged += this.PartsOutboundBillDetailForPartsRetailOrderDataGridView_DataContextChanged;
        }

        private void PartsOutboundBillDetailForPartsRetailOrderDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.ExecuteQueryDelayed();
        }

        /// <summary>
        /// 根据配件销售业务下， 配件零售退货 节点设计需求。选择 配件出库单 时，同时带回 配件零售退货 实体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView_DataLoaded(object sender, EventArgs e) {
            if(this.GridView.Items.Count > 0) {
                var partsRetailOrder = this.DataContext as PartsRetailOrder;
                if(partsRetailOrder == null)
                    return;
                foreach(var entity in this.GridView.Items.Cast<PartsOutboundBillDetail>())
                    entity.PartsRetailOrder = partsRetailOrder;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsOutboundBill.Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_Code
                    }, new ColumnItem {
                        Name = "PartsOutboundBill.CreateTime"
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "OutboundAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "BatchNumber"
                    },
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOutboundBillDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["OutboundAmount"]).DataFormatString = "d";
            this.DataLoaded -= this.GridView_DataLoaded;
            this.DataLoaded += this.GridView_DataLoaded;
        }

        protected override string OnRequestQueryName() {
            return "按原始需求单据查询出库明细";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "originalRequirementBillId":
                    var partsRetailOrder = this.DataContext as PartsRetailOrder;
                    return partsRetailOrder != null ? partsRetailOrder.Id : default(int);
                case "originalRequirementBillType":
                    return (int)DcsOriginalRequirementBillType.配件零售订单;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

    }
}
