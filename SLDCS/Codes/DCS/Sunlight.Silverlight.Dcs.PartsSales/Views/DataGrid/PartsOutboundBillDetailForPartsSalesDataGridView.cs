﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsOutboundBillDetailForPartsSalesDataGridView : DcsDataGridViewBase {
        public PartsOutboundBillDetailForPartsSalesDataGridView() {
            this.DataContextChanged += this.PartsOutboundBillDetailDataGridView_DataContextChanged;
        }

        private void PartsOutboundBillDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsOutboundBill.Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_Code
                    }, new ColumnItem {
                        Name = "PartsOutboundBill.CreateTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBillDetail_CreateTime,
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.DateTime
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "OutboundAmount",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "originalRequirementBillId":
                    var partsSalesOrder = this.DataContext as PartsSalesOrder;
                    return partsSalesOrder != null ? partsSalesOrder.Id : default(int);
                case "originalRequirementBillType":
                    return (int)DcsOriginalRequirementBillType.配件销售订单;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOutboundBillDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "按原始需求单据查询出库明细";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["PartsOutboundBill.CreateTime"]).DataFormatString = "d";
        }
    }
}
