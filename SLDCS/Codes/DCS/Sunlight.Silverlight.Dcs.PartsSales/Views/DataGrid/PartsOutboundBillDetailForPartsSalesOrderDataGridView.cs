﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsOutboundBillDetailForPartsSalesOrderDataGridView : DcsDataGridViewBase {

        public PartsOutboundBillDetailForPartsSalesOrderDataGridView() {
            this.DataContextChanged += PartsOutboundBillDetailForPartsSalesOrderDataGridView_DataContextChanged;
        }

        private void PartsOutboundBillDetailForPartsSalesOrderDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.ExecuteQuery();
        }

        /// <summary>
        /// 根据配件销售业务下， 配件订单提报 节点设计需求。选择 配件出库单 时，同时带回 配件销售订单编号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView_DataLoaded(object sender, EventArgs e) {
            if(this.GridView.Items.Count > 0) {
                var partsSalesOrder = this.DataContext as PartsSalesOrder;
                if(partsSalesOrder == null)
                    return;
                foreach(var entity in this.GridView.Items.Cast<PartsOutboundBillDetail>()) {
                    entity.OriginalRequirementBillCode = partsSalesOrder.Code;
                    entity.ContactPerson = partsSalesOrder.ContactPerson;
                    entity.ContactPhone = partsSalesOrder.ContactPhone;
                    entity.ERPSourceOrderCode = partsSalesOrder.ERPSourceOrderCode;
                }


            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsOutboundBill.Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_Code
                    }, new ColumnItem {
                        Name = "PartsOutboundBill.WarehouseName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseName
                    }, new ColumnItem {
                        Name = "PartsOutboundBill.CreateTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_CreateTime
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "OutboundAmount"
                    }, new ColumnItem {
                        Name = "BatchNumber"
                    }, 
                    new ColumnItem {
                        Name = "SparePart.MeasureUnit"
                    }, 
                    new ColumnItem {
                        Name = "SettlementPrice"
                    },
                    new ColumnItem {
                        Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice,
                        Name = "OriginalPrice"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "按原始需求单据查询出库明细不含调拨出库";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOutboundBillDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.DataPager.PageSize = 15;
            this.GridView.DataLoaded += this.GridView_DataLoaded;
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "originalRequirementBillId":
                    var partsSalesOrder = this.DataContext as PartsSalesOrder;
                    return partsSalesOrder != null ? partsSalesOrder.Id : default(int);
                case "originalRequirementBillType":
                    return (int)DcsOriginalRequirementBillType.配件销售订单;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
