﻿using System;
using System.Linq;
using Telerik.Windows.Data;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsOutboundBillDetailForQueryDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesOrder_Status"
        };

        public PartsOutboundBillDetailForQueryDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsSalesOrderCode",
                        Title =PartsSalesUIStrings.DetailPanel_Text_PartsRetailOrder_Code
                    },new ColumnItem {
                        Name = "SubmitTime",
                        Title = PartsSalesUIStrings.QueryPanel_Title_ServiceAgentsHistoryQuery_SubmitTime
                    },new ColumnItem {
                        Name = "PartsSalesOrderTypeName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_PartsSalesOrderTypeName
                    },new ColumnItem {
                        Name = "IfDirectProvision",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_IfDirectProvision,
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title =PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },new ColumnItem {
                        Name = "ReturnedQuantity",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPartsOutboundBillDetail_ReturnedQuantity
                    },new ColumnItem {
                        Name = "MeasureUnit",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_MeasureUnit
                    },new ColumnItem {
                        Name = "SettlementPrice",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_OrderPrice
                    },new ColumnItem {
                        Name = "OriginalPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice               
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "退货节点选取配件销售单";
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsOutboundBillDetail);
            }
        }
       
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "submitCompanyId":
                        var bOutBoundTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "SubmitCompanyId");
                        return bOutBoundTime == null ? null : bOutBoundTime.Filters.First(item => item.MemberName == "SubmitCompanyId").Value;
                    case "salesUnitId":
                        var eOutBoundTime = composites.FirstOrDefault(p => p.Filters.Last().MemberName == "SalesUnitId");
                        return eOutBoundTime == null ? null : eOutBoundTime.Filters.First(item => item.MemberName == "SalesUnitId").Value;
                    case "partsSalesOrderCode":
                        var partsSalesOrderCode = composites.FirstOrDefault(p => p.Filters.Last().MemberName == "PartsSalesOrderCode");
                        return partsSalesOrderCode == null ? null : partsSalesOrderCode.Filters.First(item => item.MemberName == "PartsSalesOrderCode").Value;
                    case "partsSalesOrderTypeId":
                        var partsSalesOrderTypeId = composites.FirstOrDefault(p => p.Filters.Last().MemberName == "PartsSalesOrderTypeId");
                        return partsSalesOrderTypeId == null ? null : partsSalesOrderTypeId.Filters.First(item => item.MemberName == "PartsSalesOrderTypeId").Value;
                    case "sparePartCode":
                        var sparePartCode = composites.FirstOrDefault(p => p.Filters.Last().MemberName == "SparePartCode");
                        return sparePartCode == null ? null : sparePartCode.Filters.First(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        var sparePartName = composites.FirstOrDefault(p => p.Filters.Last().MemberName == "SparePartName");
                        return sparePartName == null ? null : sparePartName.Filters.First(item => item.MemberName == "SparePartName").Value;
                    case "ifDirectProvision":
                        var ifDirectProvision = composites.FirstOrDefault(p => p.Filters.Last().MemberName == "IfDirectProvision");
                        return ifDirectProvision == null ? null : ifDirectProvision.Filters.First(item => item.MemberName == "IfDirectProvision").Value;
                    case "bCreateTime":
                        var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
        }
    }
}
