﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid
{
    public class PartsOutboundBillDetaillForWarehouseDataGridView  : DcsDataGridViewBase {
        public PartsOutboundBillDetaillForWarehouseDataGridView()
        {
            this.DataContextChanged += this.PartsOutboundBillDetailDataGridView_DataContextChanged;
        }

        private void PartsOutboundBillDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsOutboundBill = e.NewValue as PartsOutboundBillWithOtherInfo;
            if(partsOutboundBill == null || partsOutboundBill.PartsOutboundBillId == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsOutboundBillId",
                MemberType = typeof(int),
                Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsEqualTo,
                Value = partsOutboundBill.PartsOutboundBillId
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsOutboundBillDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_MeasureUnit
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode",
                        Title=PartsSalesUIStrings.DataGridView_Title_APartsOutboundBillDetail_WarehouseAreaCode
                    }, new ColumnItem {
                        Name = "OutboundAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_OutboundQuantity
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementDetail_SettlementPrice
                    }, new ColumnItem {
                        Name = "Remark",
                        Title=PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsOutboundBillDetailsWithSparePart2";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "partsOutboundBillId":
                        return filters.Filters.Single(item => item.MemberName == "PartsOutboundBillId").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["OutboundAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
        }
    }
}