﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid
{
    public class PartsPackingPropertyAppDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = {
           "PackingPropertyAppStatus", "PackingUnitType"
        };

        public PartsPackingPropertyAppDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(PartsPackingPropertyAppQuery);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                    },new ColumnItem {
                        Name = "Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceCode
                    } ,new ColumnItem {
                        Name = "SpareCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    },new ColumnItem {
                        Name = "SpareName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },new ColumnItem {
                        Name = "SihCode",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_SihCode
                    },new ColumnItem {
                        Name = "RetailGuidePrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice
                    },new KeyValuesColumnItem {
                        Name = "MainPackingType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsPackingPropertyAppQuery_MainPackingType
                    },new ColumnItem{
                        Name = "MInSalesAmount",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_MInSalesAmount
                    },new ColumnItem{
                        Name = "OldMInPackingAmount",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_OldMInPackingAmount
                    },new ColumnItem{
                        Name = "FirPackingCoefficient",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_FirPackingCoefficient
                    },new ColumnItem{
                        Name = "SecPackingCoefficient",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_SecPackingCoefficient
                    },new ColumnItem{
                        Name = "ThidPackingCoefficient",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ThidPackingCoefficient
                    },new ColumnItem{
                        Name = "CreatorName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title =  PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime
                    },new ColumnItem{
                        Name = "ModifierName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifierName
                    }, new ColumnItem{
                        Name = "ModifyTime",
                        Title =  PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifyTime
                    },new ColumnItem {
                        Name = "ApproverName",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproverName
                    },new ColumnItem {
                        Name = "ApproveTime",
                       Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproveTime
                    },new ColumnItem {
                        Name = "ApprovalComment",
                        Title = PartsSalesUIStrings.DataEditView_Text_DealerPartsRetailOrder_Comment
                    },
                };
            }
        }

        protected override string OnRequestQueryName()
        {
            return "GetPartsPackingPropertyAppLists";
        }

     
        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }
        }
      
        protected override void OnControlsCreated()
        {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            base.OnControlsCreated();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "spareCode":
                        return filters.Filters.Single(item => item.MemberName == "SpareCode").Value;
                    case "spareName":
                        return filters.Filters.Single(item => item.MemberName == "SpareName").Value;                 
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "mainPackingType":
                        return filters.Filters.Single(item => item.MemberName == "MainPackingType").Value;                   
                    case "bCreateTime":
                        var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;                  
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
