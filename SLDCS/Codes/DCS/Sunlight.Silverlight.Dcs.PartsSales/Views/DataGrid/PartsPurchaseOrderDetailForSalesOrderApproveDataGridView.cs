﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsPurchaseOrderDetailForSalesOrderApproveDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = { "PartsPurchaseOrder_Status" };

        public PartsPurchaseOrderDetailForSalesOrderApproveDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "PartsPurchaseOrderStatus",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPartsPurchaseOrderDetail_PartsPurchaseOrderStatus,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "PartsPurchaseOrderCode",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPartsPurchaseOrderDetail_PartsPurchaseOrderCode
                    },
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    },
                     new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },
                    new ColumnItem {
                        Name = "OrderAmount",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPartsPurchaseOrderDetail_OrderAmount
                    },
                    new ColumnItem {
                        Name = "ConfirmAmount",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPartsPurchaseOrderDetail_ConfirmAmount
                    },
                    new ColumnItem {
                        Name = "ShippingAmount",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ShippingQuantity
                    },
                    new ColumnItem {
                        Name = "UnInboundAmount",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPartsPurchaseOrderDetail_UnInboundAmount
                    },
                     
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsPurchaseOrderDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsPurchaseOrderDetails");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            base.OnControlsCreated();
        }
    }
}
