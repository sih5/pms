﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsRetailGuidePriceSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsRetailGuidePriceSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    //new KeyValuesColumnItem {
                    //    Name = "Status",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    //    Title=PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                    //}, 
                    new ColumnItem {
                        Name = "SparePartCode",
                         Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem {
                        Name = "RetailGuidePrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_RetailGuidePrice
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title=PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsSalesPrice_SalesPrice
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            //return "查询配件零售指导价";
            return "GetPartsRetailGuidePriceByBranchType";
        }

        protected override Type EntityType {
            get {
                return typeof(VirTualPartsRetailGuidePrice);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsRetailGuidePriceHistory"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                FilterItem filterItem;
                switch(parameterName) {
                    case "isSale":
                        filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "isSale");
                        if(filterItem != null)
                            return filterItem.Value;
                        else return null;
                    case "isOrderable":
                        filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "isOrderable");
                        if(filterItem != null)
                            return filterItem.Value;
                        else return null;
                    //case "PartsSalesCategoryId":
                    //    filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesCategoryId");
                    //    if(filterItem != null)
                    //        return filterItem.Value;
                    //    else return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach (var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "isSale" && filter.MemberName != "isOrderable" && filter.MemberName != "BranchId" && filter.MemberName != "CreateTime" && filter.MemberName != "PartsSalesCategoryId" && filter.MemberName != "Status"))
                {
                    newCompositeFilterItem.Filters.Add(item);

                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
