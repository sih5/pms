﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsRetailOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase virtualPartsStockForPartsRetailDropDownQueryWindow;

        private DcsMultiPopupsQueryWindowBase VirtualPartsStockForPartsRetailDropDownQueryWindow {
            get {
                if(this.virtualPartsStockForPartsRetailDropDownQueryWindow == null) {
                    this.virtualPartsStockForPartsRetailDropDownQueryWindow = DI.GetQueryWindow("PartsRetailDetail") as DcsMultiPopupsQueryWindowBase;
                    this.virtualPartsStockForPartsRetailDropDownQueryWindow.SelectionDecided += this.VirtualPartsStockForPartsRetailDropDownQueryWindow_SelectionDecided;
                    this.virtualPartsStockForPartsRetailDropDownQueryWindow.Loaded += this.VirtualPartsStockForPartsRetailDropDownQueryWindow_Loaded;
                }
                return this.virtualPartsStockForPartsRetailDropDownQueryWindow;
            }
        }

        private void VirtualPartsStockForPartsRetailDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;

            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null || queryWindow == null)
                return;

            var compositeFilterItem = new CompositeFilterItem();
            //获取 根据所选仓库Id 获取 仓库与销售组织关系。 在获取 配件销售类型Id  传入弹出框。获取唯一的价格属性
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "WarehouseId", partsRetailOrder.WarehouseId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "WarehouseId", false
            });
        }

        private void VirtualPartsStockForPartsRetailDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualPartsStocks = queryWindow.SelectedEntities.Cast<WarehousePartsStock>().ToArray();
            if(virtualPartsStocks == null)
                return;
            if(virtualPartsStocks.Any(r => r.UsableQuantity <= 0)) {
                UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataGridView_Notification_PartsRetailOrderDetail_SelectedSparePartUsableQuantityLessThenZero);
                return;
            }

            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            if(partsRetailOrder.PartsRetailOrderDetails.Any(r => virtualPartsStocks.Any(ex => ex.SparePartCode == r.SparePartCode))) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_VirtualPartsPurchaseOrderDetail_SparePartCode);
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            foreach(var virtualPartsStock in virtualPartsStocks) {
                domainContext.Load(domainContext.GetPartsRetailGuidePricesQuery().Where(entity => entity.SparePartId == virtualPartsStock.SparePartId && entity.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    if(!loadOp.Entities.Any()) {
                        UIHelper.ShowAlertMessage(PartsSalesUIStrings.DataGridView_Notification_PartsRetailOrderDetail_SelectedSparePartNotExistRetailGuidePrice);
                        return;
                    }
                    var entity = loadOp.Entities.FirstOrDefault();
                    if(entity == null)
                        return;

                    var partsRetailOrderDetail = new PartsRetailOrderDetail {
                        SparePartId = virtualPartsStock.SparePartId,
                        SparePartCode = virtualPartsStock.SparePartCode,
                        SparePartName = virtualPartsStock.SparePartName,
                        SalesPrice = entity.RetailGuidePrice,
                        Quantity = 1,
                        DiscountedPrice = entity.RetailGuidePrice,
                        DiscountedAmount = entity.RetailGuidePrice,
                        DelaerPrice = virtualPartsStock.PartsSalesPrice

                    };
                    partsRetailOrder.PartsRetailOrderDetails.Add(partsRetailOrderDetail);


                }, null);
            }
            //var parent = queryWindow.ParentOfType<RadWindow>();
            //if(parent != null)
            //    parent.Close();
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
            e.Cancel = true;
            if(partsRetailOrder.WarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsRetailOrder_WarehouseIsNull);
                return;
            }
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            RadQueryWindow.ShowDialog();

        }
        private RadWindow radQueryWindow;
        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.VirtualPartsStockForPartsRetailDropDownQueryWindow,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    Header = PartsSalesUIStrings.DataGridView_Header_VirtualPartsPurchaseOrderDetail_RetailOrder
                });
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Quantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "DiscountAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "DiscountedPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsRetailOrderDetail_DiscountPrice
                    }, new ColumnItem {
                        Name = "DiscountedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                          IsReadOnly = true
                    }, new ColumnItem {
                        Name = "DelaerPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true,
                        Title="服务站价"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsRetailOrderDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsRetailOrderDetails");
        }


        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;
        }
        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var partsRetailOrder = this.DataContext as PartsRetailOrder;
            if(partsRetailOrder == null)
                return;

        }
        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var partsRetailOrderDetail = e.Cell.DataContext as PartsRetailOrderDetail;
            if(partsRetailOrderDetail == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "DiscountedPrice":
                    if((decimal)e.NewValue > partsRetailOrderDetail.SalesPrice) {
                        e.IsValid = false;
                        e.ErrorMessage = "折扣后价格不能大于销售价格";
                    }
                    break;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.CellEditEnded -= GridView_CellEditEnded;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.CellValidating += GridView_CellValidating;
            ((GridViewDataColumn)this.GridView.Columns["Quantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DelaerPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountedPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountedAmount"]).DataFormatString = "c2";

        }


        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }
    }
}