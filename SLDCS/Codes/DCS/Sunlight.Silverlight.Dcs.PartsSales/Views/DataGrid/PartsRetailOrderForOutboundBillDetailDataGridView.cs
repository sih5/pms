﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsRetailOrderForOutboundBillDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PayOutBill_PaymentMethod", "PartsRetailOrder_Status"
        };

        public PartsRetailOrderForOutboundBillDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsRetailOrder_Code
                    }, new ColumnItem {
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Name = "SalesUnitName"
                    }, new ColumnItem {
                        Name = "CustomerName"
                    }, new ColumnItem {
                        Name = "CustomerCellPhone"
                    }, new ColumnItem {
                        Name = "CustomerAddress"
                    }, new ColumnItem {
                        Name = "OriginalAmount",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsRetailOrder_OriginalAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "DiscountedAmount",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsRetailOrder_DiscountedAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "PaymentMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem{
                       Name="CreateTime"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["OriginalAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountedAmount"]).DataFormatString = "c2";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsRetailOrder);
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            var newCompositeFilterItem = new CompositeFilterItem();
            CompositeFilterItem statusCompositeFilterItem;
            if(compositeFilterItem == null) {
                statusCompositeFilterItem = new CompositeFilterItem();
                statusCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "Status",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = (int)DcsPartsRetailOrderStatus.审批
                });
                statusCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "Status",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = (int)DcsPartsRetailOrderStatus.开票
                });
                statusCompositeFilterItem.LogicalOperator = LogicalOperator.Or;
                newCompositeFilterItem.Filters.Add(statusCompositeFilterItem);
                newCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "SalesUnitOwnerCompanyId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            } else {
                statusCompositeFilterItem = new CompositeFilterItem();
                statusCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "Status",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = (int)DcsPartsRetailOrderStatus.审批
                });
                statusCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "Status",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = (int)DcsPartsRetailOrderStatus.开票
                });
                statusCompositeFilterItem.LogicalOperator = LogicalOperator.Or;
                newCompositeFilterItem.Filters.Add(statusCompositeFilterItem);
                newCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "SalesUnitOwnerCompanyId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });

                foreach (var item in compositeFilterItem.Filters.Where(e => e.MemberName != "SparePartCode")) { 
                    newCompositeFilterItem.Filters.Add(item);
                }
                //newCompositeFilterItem.Filters.Add(compositeFilterItem);
            }

            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null) {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName) {
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return "GetPartsRetailOrderByPartsOutboundBillDetails";
        }
    }
}
