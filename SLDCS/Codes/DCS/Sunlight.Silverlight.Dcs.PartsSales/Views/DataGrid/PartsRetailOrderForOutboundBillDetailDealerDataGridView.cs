﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsRetailOrderForOutboundBillDetailDealerDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "DealerPartsRetailOrder_RetailOrderType", "PartsRetailReturnBill_Status"
        };

        public PartsRetailOrderForOutboundBillDetailDealerDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsRetailOrder_Code
                    }, new ColumnItem {
                        Name = "DealerCode",
                       Title= PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_DealerCode
                    }, new ColumnItem {
                        Name = "Customer",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Name
                    }, new ColumnItem {
                        Name = "CustomerCellPhone"
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        Title=PartsSalesUIStrings.DataEditView_Title_PartsSalesSettlement_TotalSettlementAmount
                    }, new KeyValuesColumnItem {
                        Name = "RetailOrderType",
                        Title=PartsSalesUIStrings.DataGridView_Title_DealerPartsRetailOrder_RetailOrderType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title=PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                    },new ColumnItem{
                       Name="CreateTime",
                       Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsRetailOrder);
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            var newCompositeFilterItem = new CompositeFilterItem();
            CompositeFilterItem statusCompositeFilterItem;
            if(compositeFilterItem == null) {
                statusCompositeFilterItem = new CompositeFilterItem();               
                statusCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "Status",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = (int)DcsWorkflowOfSimpleApprovalStatus.已审核
                });
                statusCompositeFilterItem.LogicalOperator = LogicalOperator.Or;
                newCompositeFilterItem.Filters.Add(statusCompositeFilterItem);
                newCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "DealerId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            } else {
                statusCompositeFilterItem = new CompositeFilterItem();
              
                statusCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "Status",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = (int)DcsWorkflowOfSimpleApprovalStatus.已审核
                });
                statusCompositeFilterItem.LogicalOperator = LogicalOperator.Or;
                newCompositeFilterItem.Filters.Add(statusCompositeFilterItem);
                newCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "DealerId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                foreach(var item in compositeFilterItem.Filters.Where(e => e.MemberName != "PartsCode")) {
                    newCompositeFilterItem.Filters.Add(item);
                }             
               // newCompositeFilterItem.Filters.Add(compositeFilterItem);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "partsCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsCode").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "服务站零售订单退货查询主单";
        }
    }
}