﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsRetailOrderForSelectDataGridView : DcsDataGridViewBase {
         private readonly string[] kvNames = {
            "PayOutBill_PaymentMethod", "PartsRetailOrder_Status"
        };

         public PartsRetailOrderForSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsRetailOrder_Code
                    }, new ColumnItem {
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Name = "SalesUnitName"
                    }, new ColumnItem {
                        Name = "CustomerName"
                    }, new ColumnItem {
                        Name = "CustomerCellPhone"
                    }, new ColumnItem {
                        Name = "CustomerAddress"
                    }, new ColumnItem {
                        Name = "OriginalAmount",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsRetailOrder_OriginalAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "DiscountedAmount",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsRetailOrder_DiscountedAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "PaymentMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "InvoiceOperatorName"
                    }, new ColumnItem {
                        Name = "InvoiceTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsRetailOrder);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["DiscountedAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalAmount"]).DataFormatString = "c2";
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {

                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "SparePartId" && filter.MemberName != "SalesUnitOwnerCompanyId" && filter.MemberName != "SalesCategoryId"))
                    newCompositeFilterItem.Filters.Add(item);
            }

            return newCompositeFilterItem.ToFilterDescriptor();

        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            FilterItem filterItem;
            switch(parameterName) {
                case "partid":
                    //filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SparePartId");
                    filterItem = filter.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).SingleOrDefault(item => item.MemberName == "SparePartId");
                    return filterItem == null ? null : filterItem.Value;
                case "salesUnitOwnerCompanyId":
                    //filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SalesUnitOwnerCompanyId");
                    filterItem = filter.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).SingleOrDefault(item => item.MemberName == "SalesUnitOwnerCompanyId");
                    return filterItem == null ? null : filterItem.Value;
                case "salesCategoryId":
                    filterItem = filter.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).SingleOrDefault(item => item.MemberName == "SalesCategoryId");
                    //filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SalesCategoryId");
                    return filterItem == null ? null : filterItem.Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }


        protected override string OnRequestQueryName() {
            return "查询配件索赔零售订单";
        }
    }
}
