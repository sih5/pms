﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsRetailReturnBillWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsRetailReturnBill_Status"
        };

        public PartsRetailReturnBillWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsRetailReturnBill);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "SourceCode"
                    }, new ColumnItem {
                        Name = "CustomerName"
                    }, new ColumnItem {
                        Name = "CustomerPhone"
                    }, new ColumnItem {
                        Name = "CustomerCellPhone"
                    }, new ColumnItem {
                        Name = "CustomerAddress"
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ReturnReason"
                    }, new ColumnItem {
                        Name = "WarehouseName"
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsRetailReturnBillWithDetails";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsRetailReturnBill", "PartsRetailReturnBillDetail"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }
    }
}
