﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalePriceIncreaseRateDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsSalePriceIncreaseRateDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new KeyValuesColumnItem {
                         Name = "status",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "GroupCode"
                    }, new ColumnItem {
                        Name = "GroupName"
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PartsSalePriceIncreaseRate);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalePriceIncreaseRates";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
