﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderCrossDataGridView: DcsDataGridViewBase {
        public PartsSalesOrderCrossDataGridView() {
            this.DataContextChanged += PartsSalesOrderDetailDataGridView_DataContextChanged;
        }

        private void PartsSalesOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var crossSalesOrder = e.NewValue as CrossSalesOrder;
            if(crossSalesOrder == null || string.IsNullOrEmpty(crossSalesOrder.SalesCode))
                return;

            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesOrderCode",
                MemberType = typeof(string),
                Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsEqualTo,
                Value = crossSalesOrder.SalesCode
            });
           
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "PartsSalesOrderCode",
                        TextAlignment = TextAlignment.Right,
                        Title = "销售单号"
                    },new ColumnItem{
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem{
                        Name = "OrderedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title="订货数量"
                    }, new ColumnItem{
                        Name = "ApproveQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title="审单数量"
                    }, new ColumnItem{
                        Name = "OrderPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = "订货价格"
                    }, new ColumnItem{
                        Name = "SalesPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = "服务站价"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderDetailCross);
            }
        }

        protected override string OnRequestQueryName() {
            return "跨区域销售销售订单看板查询";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            FilterItem filterItem;
            switch(parameterName) {
                case "partsSalesOrderCode":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesOrderCode");
                    return filterItem == null ? null : filterItem.Value;               
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
           
        }
    }
}