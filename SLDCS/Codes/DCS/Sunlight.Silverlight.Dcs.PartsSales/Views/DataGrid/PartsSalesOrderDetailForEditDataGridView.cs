﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase partsSalesPriceDropDownQueryWindow;
        private RadWindow radWindow;
        private DcsMultiPopupsQueryWindowBase partsSalesPriceQueryWindow;

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            switch(e.Cell.DataColumn.UniqueName) {
                case "OrderedQuantity":
                    if(!(e.NewValue is int) || (int)e.NewValue <= 0) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsSalesUIStrings.DataGridView_Validation_PartsSalesOrderDetail_OrderedQuantityIsLessThanZeroError;
                    } else {
                        var partsSalesOrder = this.DataContext as PartsSalesOrder;
                        var detail = e.Cell.DataContext as PartsSalesOrderDetail;
                        if(detail == null)
                            return;
                        if(partsSalesOrder.SalesUnitOwnerCompanyType == (int)DcsCompanyType.分公司 && (partsSalesOrder.PartsSalesOrderTypeName == PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_Normal || partsSalesOrder.PartsSalesOrderTypeName == PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_Oil) && detail.MInSaleingAmount != null && (int)e.NewValue % (detail.MInSaleingAmount.Value) != 0) {
                            e.IsValid = false;
                            e.ErrorMessage = PartsSalesUIStrings.DataGridView_Validation_VirtualPartsPurchaseOrderDetail_MInSaleingAmount;
                        }
                    }
                    break;
            }
        }

        protected DcsMultiPopupsQueryWindowBase PartsClaimPriceQueryWindow {
            get {
                if(partsSalesPriceQueryWindow == null) {
                    partsSalesPriceQueryWindow = DI.GetQueryWindow("PartsSalesPriceMulti") as DcsMultiPopupsQueryWindowBase;
                    if(partsSalesPriceQueryWindow != null) {
                        this.partsSalesPriceQueryWindow.Loaded += this.PartsSalesPriceQueryWindow_Loaded;
                        this.partsSalesPriceQueryWindow.SelectionDecided += this.PartsSalesPriceQueryWindow_SelectionDecided;
                    }
                }
                return this.partsSalesPriceQueryWindow;
            }
        }

        protected QueryWindowBase PartsSalesPriceDropDownQueryWindow {
            get {
                if(this.partsSalesPriceDropDownQueryWindow == null) {
                    this.partsSalesPriceDropDownQueryWindow = DI.GetQueryWindow("PartsSalesPriceDropDown");
                    this.partsSalesPriceDropDownQueryWindow.Loaded += this.PartsSalesPriceDropDownQueryWindow_Loaded;
                    this.partsSalesPriceDropDownQueryWindow.SelectionDecided += this.PartsSalesPriceDropDownQueryWindow_SelectionDecided;
                }
                return this.partsSalesPriceDropDownQueryWindow;
            }
        }

        protected virtual void PartsSalesPriceQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || partsSalesOrder == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            if(partsSalesOrder.SalesUnit != null) {
                compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.SalesUnit.PartsSalesCategoryId));
            }
            compositeFilterItem.Filters.Add(new FilterItem("OrderTypeId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.PartsSalesOrderTypeId));
            compositeFilterItem.Filters.Add(new FilterItem("CustomerId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.SubmitCompanyId));
            compositeFilterItem.Filters.Add(new FilterItem("IfDirectProvision", typeof(bool), FilterOperator.IsEqualTo, partsSalesOrder.IfDirectProvision));
            compositeFilterItem.Filters.Add(new FilterItem("SalesUnitOwnerCompanyId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.SalesUnitOwnerCompanyId));
            compositeFilterItem.Filters.Add(new FilterItem("WarehouseId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.WarehouseId));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        protected virtual void PartsSalesPriceQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualPartsSalesPrices = queryWindow.SelectedEntities.Cast<VirtualPartsSalesPrice>();
            if(virtualPartsSalesPrices == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            foreach(var virtualPartsSalesPrice in virtualPartsSalesPrices) {
                if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.SparePartId == virtualPartsSalesPrice.SparePartId)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_PartsSalesOrderDetail_SparePartCode);
                    return;
                }
                var partsSalesOrderDetail = new PartsSalesOrderDetail();
                partsSalesOrderDetail.ApproveQuantity = default(int);
                partsSalesOrderDetail.SparePartId = virtualPartsSalesPrice.SparePartId;
                partsSalesOrderDetail.SparePartCode = virtualPartsSalesPrice.SparePartCode;
                partsSalesOrderDetail.SparePartName = virtualPartsSalesPrice.SparePartName;
                partsSalesOrderDetail.OrderedQuantity = virtualPartsSalesPrice.MInPackingAmount ?? 1;
                partsSalesOrderDetail.OriginalPrice = virtualPartsSalesPrice.RetailGuidePrice;
                partsSalesOrderDetail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;
                partsSalesOrderDetail.OrderPrice = virtualPartsSalesPrice.Price;
                partsSalesOrderDetail.DiscountedPrice = partsSalesOrderDetail.OriginalPrice - partsSalesOrderDetail.OrderPrice;
                //partsSalesOrderDetail.OrderPrice = Math.Round((partsSalesOrderDetail.OriginalPrice * (decimal)partsSalesOrderDetail.CustOrderPriceGradeCoefficient), 2) - partsSalesOrderDetail.DiscountedPrice;
                partsSalesOrderDetail.OrderSum = partsSalesOrderDetail.OrderPrice * partsSalesOrderDetail.OrderedQuantity;
                partsSalesOrderDetail.IfCanNewPart = true;
                partsSalesOrderDetail.MeasureUnit = virtualPartsSalesPrice.MeasureUnit;
                partsSalesOrderDetail.PriceTypeName = virtualPartsSalesPrice.PriceTypeName;
                partsSalesOrderDetail.ABCStrategy = virtualPartsSalesPrice.ABCStrategy;
                partsSalesOrderDetail.MInSaleingAmount = virtualPartsSalesPrice.MInPackingAmount;
                partsSalesOrderDetail.SalesPrice = virtualPartsSalesPrice.PartsSalesPrice;
                partsSalesOrder.PartsSalesOrderDetails.Add(partsSalesOrderDetail);
                //if(partsSalesOrder.PartsSalesOrderTypeId == 0 || !partsSalesOrder.PartsSalesOrderDetails.Any())
                //    return;
                //var sparePartIds = partsSalesOrder.PartsSalesOrderDetails.Select(r => r.SparePartId).ToArray();
                //domainContext.Load(domainContext.根据配件清单获取销售价格Query(partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.SubmitCompanyId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp => {
                //    if(loadOp.HasError)
                //        return;
                //    foreach(var detail in partsSalesOrder.PartsSalesOrderDetails) {
                //        var salesPrice = loadOp.Entities.FirstOrDefault(r => r.SparePartId == detail.SparePartId);
                //        if(salesPrice != null) {
                //            detail.OriginalPrice = salesPrice.PartsSalesPrice;
                //            detail.CustOrderPriceGradeCoefficient = salesPrice.PriceType == (int)DcsPartsSalesPricePriceType.基准销售价 ? salesPrice.Coefficient : 1;
                //            detail.OrderPrice = Math.Round((detail.OriginalPrice * (decimal)detail.CustOrderPriceGradeCoefficient), 2) - detail.DiscountedPrice;
                //        } else
                //            detail.OrderPrice = 0;
                //        detail.OrderSum = detail.OrderedQuantity * detail.OrderPrice;
                //    }
                //    partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                //}, null);
            }
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected virtual void PartsSalesPriceDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || partsSalesOrder == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.SalesUnit.PartsSalesCategoryId));
            compositeFilterItem.Filters.Add(new FilterItem("CustomerId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.SubmitCompanyId));
            compositeFilterItem.Filters.Add(new FilterItem("IfDirectProvision", typeof(bool), FilterOperator.IsEqualTo, partsSalesOrder.IfDirectProvision));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        protected virtual void PartsSalesPriceDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualPartsSalesPrice = queryWindow.SelectedEntities.Cast<VirtualPartsSalesPrice>().FirstOrDefault();
            if(virtualPartsSalesPrice == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            var partsSalesOrderDetail = queryWindow.DataContext as PartsSalesOrderDetail;
            if(partsSalesOrder == null || partsSalesOrderDetail == null || partsSalesOrderDetail.SparePartId == virtualPartsSalesPrice.SparePartId)
                return;
            if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.SparePartId == virtualPartsSalesPrice.SparePartId)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderDetail_SparePartIdIsDouble);
                return;
            }
            partsSalesOrderDetail.SparePartId = virtualPartsSalesPrice.SparePartId;
            partsSalesOrderDetail.SparePartCode = virtualPartsSalesPrice.SparePartCode;
            partsSalesOrderDetail.SparePartName = virtualPartsSalesPrice.SparePartName;
            partsSalesOrderDetail.OriginalPrice = virtualPartsSalesPrice.PartsSalesPrice;
            partsSalesOrderDetail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;
            partsSalesOrderDetail.OrderPrice = virtualPartsSalesPrice.Price;
            partsSalesOrderDetail.OrderSum = partsSalesOrderDetail.OrderPrice * partsSalesOrderDetail.OrderedQuantity;
            partsSalesOrderDetail.SalesPrice = virtualPartsSalesPrice.PartsSalesPrice;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                        //DropDownContent = this.PartsSalesPriceDropDownQueryWindow,
                        //IsEditable = false,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MInSaleingAmount",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    },new ColumnItem {
                        Name = "IfCanNewPart"
                    }, new ColumnItem {
                        Name = "OriginalPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true,
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsSalesPrice_SalesPrice 
                    }, new ColumnItem {
                        Name = "DiscountedPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderSum",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "EstimatedFulfillTime",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.DateTime
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "PriceTypeName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ABCStrategy",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderDetail);
            }
        }

        protected virtual void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(!e.Cell.Column.UniqueName.Equals("OrderedQuantity"))
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var detail = e.Cell.DataContext as PartsSalesOrderDetail;
            if(detail == null)
                return;
            detail.OrderSum = detail.OrderedQuantity * detail.OrderPrice;
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.Deleted += this.GridView_Deleted;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
            ((GridViewDataColumn)this.GridView.Columns["OrderedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OrderPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountedPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OrderSum"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["EstimatedFulfillTime"]).DataFormatString = "d";
            this.DataPager.PageSize = 100;
        }

        protected virtual void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(partsSalesOrder.BranchId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_BranchIdIsNull);
                return;
            }
            if(partsSalesOrder.SalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SalesCategoryIdIsNull);
                return;
            }
            if(!partsSalesOrder.WarehouseId.HasValue || partsSalesOrder.WarehouseId.Value == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_WarehouseIsNull2);
                return;
            }
            if(partsSalesOrder.PartsSalesOrderTypeId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesOrderTypeIdIsNull2);
                return;
            }

            SelectedWindow.ShowDialog();
        }

        protected RadWindow SelectedWindow {
            get {
                if(radWindow == null) {
                    radWindow = new RadWindow();
                    radWindow.Header = PartsSalesUIStrings.QueryPanel_Title_PartsSalesPrice_PartsSelect;
                    radWindow.Content = this.PartsClaimPriceQueryWindow;
                    radWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;
                }
                return radWindow;
            }
        }

        protected virtual void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var partsSalesOrderDetail = e.Row.DataContext as PartsSalesOrderDetail;
            if(partsSalesOrderDetail == null)
                return;
            if(partsSalesOrderDetail.EntityState == EntityState.New || partsSalesOrderDetail.EntityState == EntityState.Detached) {
                if(e.Cell.Column.UniqueName.Equals("SparePartCode")) {
                    if(partsSalesOrder.SalesUnit == null) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_NameIsNull);
                        e.Cancel = true;
                    }
                    if(partsSalesOrder.SubmitCompanyId == 0) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SubmitCompanyIdIsNull);
                        e.Cancel = true;
                    }
                }
                return;
            }
            if(e.Cell.Column.UniqueName == "SparePartCode" || e.Cell.Column.UniqueName == "OrderPrice" || e.Cell.Column.UniqueName == "OrderSum")
                e.Cancel = true;

        }

        protected virtual void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesOrderDetails");
        }
    }
}
