﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderDetailForEditDetail : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "IfCanNewPart",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderSum",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "EstimatedFulfillTime",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.DateTime,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Deleting += GridView_Deleting;
            this.GridView.Deleted += GridView_Deleted;
            ((GridViewDataColumn)this.GridView.Columns["OrderedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OrderPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OrderSum"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["EstimatedFulfillTime"]).DataFormatString = "d";
            this.DataPager.PageSize = 100;
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var dataEditView = this.DataContext as PartsSalesOrderForEditDetailDataEditView;
            if(dataEditView == null)
                return;
            var partsSalesOrder = dataEditView.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var partsSalesOrderDetails = e.Items.Cast<PartsSalesOrderDetail>();
            if(partsSalesOrderDetails == null)
                return;
            if(partsSalesOrderDetails.Any(r => r.ApproveQuantity != 0)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_PartsSalesOrderDetail_ApproveQuantity);
                e.Cancel = true;
                return;
            }
            foreach(var deleteDetail in partsSalesOrderDetails)
                partsSalesOrder.PartsSalesOrderDetails.Remove(deleteDetail);

        }

        protected virtual void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var dataEditView = this.DataContext as PartsSalesOrderForEditDetailDataEditView;
            if(dataEditView == null)
                return;
            var partsSalesOrder = dataEditView.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesOrderDetails");
        }
    }
}
