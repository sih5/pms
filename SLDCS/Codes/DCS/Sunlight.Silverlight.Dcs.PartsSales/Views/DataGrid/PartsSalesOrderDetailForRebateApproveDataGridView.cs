﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderDetailForRebateApproveDataGridView : DcsDataGridViewBase {

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var partsSalesOrderDetail = e.Cell.DataContext as PartsSalesOrderDetail;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrderDetail == null || partsSalesOrder == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "OrderPrice":
                    partsSalesOrderDetail.DiscountedPrice = partsSalesOrderDetail.OriginalPrice * (decimal)partsSalesOrderDetail.CustOrderPriceGradeCoefficient - partsSalesOrderDetail.OrderPrice;
                    partsSalesOrderDetail.OrderSum = partsSalesOrderDetail.OrderPrice * partsSalesOrderDetail.OrderedQuantity;
                    partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
                    if(partsSalesOrderDetail.OriginalPrice == 0)
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrderDetail_OriginalPriceIsZero);
                    if(partsSalesOrderDetail.CustOrderPriceGradeCoefficient.CompareTo(0) == 0)
                        UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrderDetail_CustOrderPriceGradeCoefficientIsZero, partsSalesOrderDetail.SparePartCode));
                    if(partsSalesOrderDetail.OrderedQuantity == 0)
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Notification_PartsSalesOrderDetail_OrderedQuantityIsZero);
                    var amount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => (double)r.OriginalPrice * r.CustOrderPriceGradeCoefficient * r.OrderedQuantity);
                    if(amount.CompareTo(0) > 0)//检查是否是除零
                        partsSalesOrder.SalesActivityDiscountRate = (double)partsSalesOrder.TotalAmount / amount;
                    partsSalesOrder.SalesActivityDiscountAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.DiscountedPrice * r.OrderedQuantity);
                    break;
            }
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "OrderPrice":
                    if(!(e.NewValue is decimal) || (decimal)e.NewValue <= 0) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderDetail_OrderPriceMustGreateThanZero;
                    }
                    break;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[]{ 
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "DiscountedPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "OrderSum",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "EstimatedFulfillTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["OrderPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountedPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OrderSum"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OrderedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["EstimatedFulfillTime"]).DataFormatString = "d";
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesOrderDetails");
        }
    }
}
