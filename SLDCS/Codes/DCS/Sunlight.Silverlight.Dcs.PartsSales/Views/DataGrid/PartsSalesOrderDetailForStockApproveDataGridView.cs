﻿using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderDetailForStockApproveDataGridView : PartsSalesOrderDetailTemplateApproveDataGridView {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_UnfulfilledQuantity,
                        Name = "UnfulfilledQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_CurrentFulfilledQuantity,
                        Name = "CurrentFulfilledQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_UsableQuantity,
                        Name = "StockQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderPrice",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsSalesPrice,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_SupplierWarehouseName,
                        Name = "SupplierWarehouseName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_Title_DealerPartsRetailOrder_MinSaleQuantity,
                        Name = "MinSaleQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "IfCanNewPart",
                        IsReadOnly = true
                    }
                };
            }
        }
    }
}