﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    //将配件销售订单管理的业务逻辑从配件订单提报管理的组件中抽取，以保证便于维护
    public class PartsSalesOrderDetailForSupplyEditDataGridView : PartsSalesOrderDetailForEditDataGridView {

        protected override void PartsSalesPriceQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualPartsSalesPrices = queryWindow.SelectedEntities.Cast<VirtualPartsSalesPrice>();
            if(virtualPartsSalesPrices == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;

            foreach(var virtualPartsSalesPrice in virtualPartsSalesPrices) {
                if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.SparePartId == virtualPartsSalesPrice.SparePartId))
                    continue;
                var partsSalesOrderDetail = new PartsSalesOrderDetail();
                partsSalesOrderDetail.SparePartId = virtualPartsSalesPrice.SparePartId;
                partsSalesOrderDetail.SparePartCode = virtualPartsSalesPrice.SparePartCode;
                partsSalesOrderDetail.SparePartName = virtualPartsSalesPrice.SparePartName;
                partsSalesOrderDetail.OriginalPrice = virtualPartsSalesPrice.PartsSalesPrice;
                partsSalesOrderDetail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;
                partsSalesOrderDetail.OrderPrice = virtualPartsSalesPrice.Price;
                partsSalesOrderDetail.IfCanNewPart = true;
                partsSalesOrderDetail.OrderedQuantity = 1;
                partsSalesOrder.PartsSalesOrderDetails.Add(partsSalesOrderDetail);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        //单条清单进行维护
        protected override void PartsSalesPriceDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualPartsSalesPrice = queryWindow.SelectedEntities.Cast<VirtualPartsSalesPrice>().FirstOrDefault();
            if(virtualPartsSalesPrice == null)
                return;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            var partsSalesOrderDetail = queryWindow.DataContext as PartsSalesOrderDetail;
            if(partsSalesOrder == null || partsSalesOrderDetail == null || partsSalesOrderDetail.SparePartId == virtualPartsSalesPrice.SparePartId)
                return;
            if(partsSalesOrder.PartsSalesOrderDetails.Any(r => r.SparePartId == virtualPartsSalesPrice.SparePartId)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderDetail_SparePartIdIsDouble);
                return;
            }
            partsSalesOrderDetail.SparePartId = virtualPartsSalesPrice.SparePartId;
            partsSalesOrderDetail.SparePartCode = virtualPartsSalesPrice.SparePartCode;
            partsSalesOrderDetail.SparePartName = virtualPartsSalesPrice.SparePartName;
            partsSalesOrderDetail.MeasureUnit = virtualPartsSalesPrice.MeasureUnit;
            partsSalesOrderDetail.OriginalPrice = virtualPartsSalesPrice.PartsSalesPrice;
            partsSalesOrderDetail.CustOrderPriceGradeCoefficient = virtualPartsSalesPrice.Coefficient;
            partsSalesOrderDetail.OrderPrice = virtualPartsSalesPrice.Price;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            var detail = e.Cell.DataContext as PartsSalesOrderDetail;
            if(partsSalesOrder == null || detail == null)
                return;
            if(e.Cell.Column.UniqueName.Equals("OrderedQuantity")) {
                detail.OrderSum = detail.OrderedQuantity * detail.OrderPrice;
                partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
            }
        }

        protected override void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(!partsSalesOrder.WarehouseId.HasValue || partsSalesOrder.WarehouseId.Value == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_WarehouseIsNull2);
                return;
            }
            if(partsSalesOrder.SubmitCompanyId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SubmitCompanyIdIsNull);
                return;
            }
            SelectedWindow.ShowDialog();
        }

        protected override void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            var partsSalesOrderDetail = e.Row.DataContext as PartsSalesOrderDetail;
            if(partsSalesOrderDetail == null)
                return;
            if(partsSalesOrderDetail.EntityState == EntityState.New || partsSalesOrderDetail.EntityState == EntityState.Detached) {
                if(e.Cell.Column.UniqueName.Equals("SparePartCode")) {
                    if(partsSalesOrder.SalesUnit == null) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_NameIsNull);
                        e.Cancel = true;
                    }
                    if(partsSalesOrder.SubmitCompanyId == 0) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrder_SubmitCompanyIdIsNull);
                        e.Cancel = true;
                    }
                }
                return;
            }
            if(e.Cell.Column.UniqueName == "SparePartCode" || e.Cell.Column.UniqueName == "OrderedQuantity" || e.Cell.Column.UniqueName == "OrderPrice" || e.Cell.Column.UniqueName == "OrderSum")
                e.Cancel = true;
        }

        protected override void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var partsSalesOrder = this.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            partsSalesOrder.TotalAmount = partsSalesOrder.PartsSalesOrderDetails.Sum(r => r.OrderSum);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "IfCanNewPart"
                    }, new ColumnItem {
                        Name = "OrderPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderSum",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "EstimatedFulfillTime",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

    }
}
