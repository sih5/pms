﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderDetailTemplateApproveDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_UnfulfilledQuantity,
                        Name = "UnfulfilledQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_CurrentFulfilledQuantity,
                        Name = "CurrentFulfilledQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_UsableQuantity,
                        Name = "StockQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderPrice",
                         Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsSalesPrice,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_SupplierWarehouseName,
                        Name = "SupplierWarehouseName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "IfCanNewPart",
                        IsReadOnly = true
                    } 
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("TempPartsSalesOrderDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["OrderedQuantity"]).DataFormatString = "d";
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            PartsSalesOrder partsSalesOrder = null;
            //界面共用
            if(this.DataContext is PartsSalesOrderForTemplateApproveDataEditView) {
                var dataEdit = this.DataContext as PartsSalesOrderForTemplateApproveDataEditView;
                partsSalesOrder = dataEdit.DataContext as PartsSalesOrder;
            }
            if(this.DataContext is PartsSalesOrderForExportApproveDataEditView) {
                var dataEdit = this.DataContext as PartsSalesOrderForExportApproveDataEditView;
                partsSalesOrder = dataEdit.DataContext as PartsSalesOrder;
            }
            if(this.DataContext is PartsSalesOrderForSeniorApproveDataEditView) {
                var dataEdit = this.DataContext as PartsSalesOrderForSeniorApproveDataEditView;
                partsSalesOrder = dataEdit.DataContext as PartsSalesOrder;
            }
            if(partsSalesOrder == null)
                return;
            if(partsSalesOrder.ProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理 && partsSalesOrder.ProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.积压件平台) {
                e.Cancel = false;
                return;
            }
            if(e.Cell.Column.UniqueName.Equals("CurrentFulfilledQuantity")) {
                var partsSalesOrderDetail = e.Row.DataContext as PartsSalesOrderDetail;
                if(partsSalesOrderDetail == null) {
                    e.Cancel = true;
                    return;
                }
                if(partsSalesOrderDetail.StockQuantity <= 0)
                    e.Cancel = true;
            }
        }

        private GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is PartsSalesOrderDetail) {
                        var partsSalesOrderDetail = item as PartsSalesOrderDetail;
                        if(partsSalesOrderDetail.UnfulfilledQuantity > partsSalesOrderDetail.CurrentFulfilledQuantity) {
                            return this.Resources["BlueDataBackground"] as Style;
                        } else {
                            return this.Resources["WhiteDataBackground"] as Style;
                        }

                    }
                    return null;
                }
            };
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
