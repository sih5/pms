﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderExportDetailDataGridView : DcsDataGridViewBase {
        public PartsSalesOrderExportDetailDataGridView() {
            this.DataContextChanged += PartsSalesOrderDetailDataGridView_DataContextChanged;
        }

        private void PartsSalesOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesOrder = e.NewValue as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartsSalesOrderId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int)
                };
            this.FilterItem.Value = partsSalesOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SparePart.Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    }, new ColumnItem{
                        Name = "SparePart.Name",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem{
                        Name = "OrderedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem{
                        Name = "ApproveQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                         Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_ApproveQuantity
                    }, new ColumnItem{
                        Name = "OriginalPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice
                    }, new ColumnItem{
                        Name = "OrderPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_OrderPrice
                    }, new ColumnItem{
                        Name = "DiscountedPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = PartsSalesUIStrings.DataGridView_Title_DealerPartsRetailOrder_DiscountedPrice
                    }, new ColumnItem{
                        Name = "SalesPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = "经销价"
                    }, new ColumnItem{
                        Name = "OrderSum",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem{
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "PriceTypeName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ABCStrategy",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderNo",
                        Title=Name=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderCode,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesOrderDetailInfo";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["OrderedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ApproveQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OrderPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OrderSum"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
        }
    }
}
