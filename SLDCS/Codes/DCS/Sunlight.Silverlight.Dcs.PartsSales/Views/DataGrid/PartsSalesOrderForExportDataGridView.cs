﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderForExportDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
           "PartsSalesOrder_Status", "PartsShipping_Method", "ExportCustPriceType"
        };

        public PartsSalesOrderForExportDataGridView() {
            this.LoadKeyValue();
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesOrderTypesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesOrderType in loadOp.Entities)
                    this.KvPartsSalesOrderTypes.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Id,
                        Value = partsSalesOrderType.Name
                    });
            }, null);
        }

        public readonly ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();

        protected void LoadKeyValue() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceCode
                    },new ColumnItem {
                        Name = "ContractCode",
                        Title = PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesOrder_ContractCode
                    },new ColumnItem {
                        Name = "ReceivingCompanyCode",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrderQuery_ReceivingCompanyCode
                    },new ColumnItem {
                        Name = "ReceivingCompanyName",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesOrderQuery_ReceivingCompanyName
                    },new KeyValuesColumnItem {
                        Name = "PartsSalesOrderTypeId",
                        KeyValueItems = this.KvPartsSalesOrderTypes,
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_PartsSalesOrderType
                    },new KeyValuesColumnItem {
                        Name = "PriceType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[2]],
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesPriceChange_PriceType
                    },new ColumnItem {
                        Name = "InvoiceReceiveCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesOrder_InvoiceReceiveCompanyName
                    },new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    },new ColumnItem {
                        Name = "TotalAmount"
                    },new ColumnItem {
                        Name = "PackingMethod",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_PackingMethod
                    },new ColumnItem {
                        Name = "ReceivingAddress"
                    },new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }
                };
            }
        }


        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrder);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            //this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }

        protected override string OnRequestQueryName() {
            return "查询出口订单";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                         "PartsSalesOrderExportDetail", "PartsSalesOrderForReport","PartsSalesOrderProcess", "PartsOutboundBillDetailForPartsSalesAddAParam"
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            FilterItem filterItem;
            switch(parameterName) {
                case "sparePartCode":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode");
                    return filterItem == null ? null : filterItem.Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach(var filter in compositeFilterItem.Filters.Where(item => item.MemberName != "SparePartCode"))
                newCompositeFilterItem.Filters.Add(filter);
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}