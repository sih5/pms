﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderForPartsOutboundBillDetailDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
            "PartsSalesOrder_Status"
        };

        public PartsSalesOrderForPartsOutboundBillDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    },new ColumnItem {
                        Name = "SalesUnitName"
                    },new ColumnItem {
                        Name = "SubmitTime"
                    },new ColumnItem {
                        Name = "PartsSalesOrderTypeName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_PartsSalesOrderTypeName
                    },new ColumnItem {
                        Name = "IfDirectProvision",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_IfDirectProvision,
                    },new KeyValuesColumnItem {
                        Name  = "Status",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "InvoiceReceiveCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_InvoiceReceiveCompanyName,
                    },new ColumnItem {
                        Name = "ReceivingCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_ReceivingCompanyName,
                    },new ColumnItem {
                        Name = "TotalAmount",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrder_TotalAmount,               
                    },new ColumnItem {
                        Name = "ContactPhone",
                        Title = PartsSalesUIStrings.QueryPanel_QueeryItem_Title_PartsSalesOrder_ContactPhone,
                    },new ColumnItem {
                        Name = "ContactPerson",
                        Title = PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ContactPerson,
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesOrderHaveOutBound";
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrder);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
