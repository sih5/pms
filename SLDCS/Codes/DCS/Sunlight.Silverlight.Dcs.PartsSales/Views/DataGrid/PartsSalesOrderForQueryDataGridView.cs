﻿
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderForQueryDataGridView : PartsSalesOrderWithDetailsDataGridView {

        protected override string OnRequestQueryName() {
            return "GetPartsSalesOrders";
        }
    }
}
