﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderForReportWithDetailsDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "PartsSalesOrder_SecondLevelOrderType", "PartsSalesOrder_Status", "PartsShipping_Method", "Company_Type"
        };

        public readonly ObservableCollection<KeyValuePair> KvWarehouses = new ObservableCollection<KeyValuePair>();
        public PartsSalesOrderForReportWithDetailsDataGridView() {
            this.LoadKeyValue();
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesOrderTypesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesOrderType in loadOp.Entities)
                    this.KvPartsSalesOrderTypes.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Id,
                        Value = partsSalesOrderType.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null) {
                    foreach(var warehouse in loadOp.Entities)
                        this.KvWarehouses.Add(new KeyValuePair {
                            Key = warehouse.Id,
                            Value = warehouse.Name
                        });
                }
            }, null);
        }

        public readonly ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();

        protected void LoadKeyValue() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new KeyValuesColumnItem {
                        Name = "PartsSalesOrderTypeId",
                        KeyValueItems = this.KvPartsSalesOrderTypes,
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_PartsSalesOrderType
                    }, new ColumnItem {
                        Name = "IfDirectProvision"
                    },  new ColumnItem {
                        Name = "IsDebt"
                    }, new ColumnItem {
                        Name = "TotalAmount"
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.KvNames[2]]
                    }, new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }, new ColumnItem {
                        Name = "SubmitTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    },new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }, new ColumnItem {
                        Name = "StopComment",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_StopReason
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "RequestedDeliveryTime"
                    }, new ColumnItem {
                        Name = "ReceivingAddress"
                    }, new KeyValuesColumnItem {
                        Name = "WarehouseId",
                        KeyValueItems = this.KvWarehouses 
                    }, new ColumnItem {
                        Name = "SalesUnitOwnerCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_SalesUnitOwnerCompanyName
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "SubmitterName"
                    }, new KeyValuesColumnItem {
                        Name = "CustomerType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[3]]
                    }, new ColumnItem {
                        Name = "SubmitCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_SubmitCompanyName
                    }, new ColumnItem {
                        Name = "SubmitCompanyCode",
                        Title =PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Code
                    }, new ColumnItem {
                        Name = "SalesCategoryName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_Name
                    },
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrder);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesOrderWithWarehouse";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                         "PartsSalesOrderDetail", "PartsSalesOrderForReport","PartsSalesOrderProcess", "PartsOutboundBillDetailForPartsSalesAddAParam"
                    }
                };
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if (compositeFilterItem == null)
            {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            }
            else
            {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "SparePartCode" && filter.MemberName != "SparePartName"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                FilterItem filterItem;
                switch (parameterName)
                {
                    case "sparePartCode":
                        filterItem = filters.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).SingleOrDefault(item => item.MemberName == "SparePartCode");
                        return filterItem == null ? null : filterItem.Value;
                    case "sparePartName":
                        filterItem = filters.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).SingleOrDefault(item => item.MemberName == "SparePartName");
                        return filterItem == null ? null : filterItem.Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}