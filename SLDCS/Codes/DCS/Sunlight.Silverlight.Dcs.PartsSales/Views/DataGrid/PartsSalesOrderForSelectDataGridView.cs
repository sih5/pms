﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;


namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid
{
    public class PartsSalesOrderForSelectDataGridView : DcsDataGridViewBase
    {
        protected readonly string[] KvNames = {
            "PartsSalesOrder_SecondLevelOrderType", "PartsSalesOrder_Status", "PartsShipping_Method", "Company_Type"
        };

        public readonly ObservableCollection<KeyValuePair> KvWarehouses = new ObservableCollection<KeyValuePair>();
        public PartsSalesOrderForSelectDataGridView()
        {
            this.KeyValueManager.Register(this.KvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize()
        {
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if (dcsDomainContext == null)
                return;
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesOrderTypesQuery(), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var partsSalesOrderType in loadOp.Entities)
                    this.KvPartsSalesOrderTypes.Add(new KeyValuePair
                    {
                        Key = partsSalesOrderType.Id,
                        Value = partsSalesOrderType.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery(), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if (loadOp.Entities != null)
                {
                    foreach (var warehouse in loadOp.Entities)
                        this.KvWarehouses.Add(new KeyValuePair
                        {
                            Key = warehouse.Id,
                            Value = warehouse.Name
                        });
                }
            }, null);
        }

        public readonly ObservableCollection<KeyValuePair> KvPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();


        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    }, new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "SalesUnitOwnerCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_SalesUnitOwnerCompanyName
                    },new KeyValuesColumnItem {
                        Name = "WarehouseId",
                        KeyValueItems = this.KvWarehouses 
                    }, new ColumnItem {
                        Name = "SubmitCompanyCode",
                        Title =PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Code
                    }, new ColumnItem {
                        Name = "SubmitCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_SubmitCompanyName
                    }, new KeyValuesColumnItem {
                        Name = "CustomerType",
                        KeyValueItems = this.KeyValueManager[this.KvNames[3]]
                    }, new KeyValuesColumnItem {
                        Name = "PartsSalesOrderTypeId",
                        KeyValueItems = this.KvPartsSalesOrderTypes,
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_PartsSalesOrderType
                    }, new ColumnItem {
                        Name = "IfDirectProvision"
                    }, new ColumnItem {
                        Name = "IsDebt"
                    }, new ColumnItem {
                        Name = "TotalAmount"
                    }, new ColumnItem {
                        Name = "Remark"
                    },  new ColumnItem {
                        Name = "StopComment",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_StopReason
                    }, new ColumnItem {
                        Name = "ReceivingAddress"
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.KvNames[2]]
                    }, new ColumnItem {
                        Name = "RequestedDeliveryTime"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }, new ColumnItem {
                        Name = "SubmitterName"
                    }, new ColumnItem {
                        Name = "SubmitTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = Core.Model.MaskType.DateTime
                    },new ColumnItem {
                        Name = "IsTransSuccess"
                    }, new ColumnItem {
                        Name = "SalesCategoryName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(PartsSalesOrder);
            }
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach (var filter in compositeFilterItem.Filters.Where(item => item.MemberName != "SparePartId"))
                newCompositeFilterItem.Filters.Add(filter);
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filter = this.FilterItem as CompositeFilterItem;
            if (filter == null)
                return null;
            FilterItem filterItem;
            switch (parameterName)
            {
                case "sparePartId":
                    filterItem = filter.Filters.SingleOrDefault(item => item.MemberName == "SparePartId");
                    return filterItem == null ? null : filterItem.Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }


        protected override string OnRequestQueryName()
        {
            return "配件索赔查询销售订单";
        }

    }
}