﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderProcessDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesOrder_Status", "PartsShipping_Method"
        };

        public PartsSalesOrderProcessDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderProcess_Code,
                        Name = "Code"
                    },new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesOrderProcess_CurrentFulfilledAmount,
                        Name = "CurrentFulfilledAmount"
                    }, new KeyValuesColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderProcess_BillStatusBeforeProcess,
                        Name = "BillStatusBeforeProcess",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderProcess_BillStatusAfterProcess,
                        Name = "BillStatusAfterProcess",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "RequestedDeliveryTime",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.DateTime
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "ShippingInformationRemark"
                    }, new ColumnItem {
                        Name = "ApprovalComment"
                    },
                    new ColumnItem {
                        Name = "Remark"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesOrderProcess_Time,
                        Name = "Time"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderProcess);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesOrderProcesses";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSalesOrderProcessDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["RequestedDeliveryTime"]).DataFormatString = "d";
        }
    }
}
