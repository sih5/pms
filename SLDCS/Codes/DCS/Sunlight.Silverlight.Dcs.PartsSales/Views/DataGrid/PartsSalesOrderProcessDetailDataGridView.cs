﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderProcessDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesOrderProcessDetail_OrderProcessStatus", "PartsSalesOrderProcessDetail_ProcessMethod"
        };

        public PartsSalesOrderProcessDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsSalesOrderProcessDetailDataGridView_DataContextChanged;
        }

        private void PartsSalesOrderProcessDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesOrderProcess = e.NewValue as PartsSalesOrderProcess;
            if(partsSalesOrderProcess == null)
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartsSalesOrderProcessId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = partsSalesOrderProcess.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "OrderProcessStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem {
                        Name = "CurrentFulfilledQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new KeyValuesColumnItem {
                        Name = "OrderProcessMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderProcessDetail_NewPartCode,
                        Name = "NewPartCode"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderProcessDetail_NewPartName,
                        Name = "NewPartName"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderProcessDetail_SupplierCompanyName,
                        Name = "SupplierCompanyName"
                    }, new ColumnItem {
                        Name = "WarehouseName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderProcessDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesOrderProcessDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["OrderedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CurrentFulfilledQuantity"]).DataFormatString = "d";
        }
    }
}
