﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderProcessDetailForPartsSalesOrderDataGridView : DcsDataGridViewBase {
        private QueryWindowBase overstockPartsInformationDropDownQueryWindow;
        private QueryWindowBase companyStockDropDownQueryWindow;

        public readonly string[] kvNames = new[] {
            "PartsSalesOrderProcessDetail_ProcessMethod", "PartsSalesOrderProcessDetail_OrderProcessStatus"
        };

        public PartsSalesOrderProcessDetailForPartsSalesOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        public QueryWindowBase OverstockPartsInformationDropDownQueryWindow {
            get {
                if(this.overstockPartsInformationDropDownQueryWindow == null) {
                    this.overstockPartsInformationDropDownQueryWindow = DI.GetQueryWindow("OverstockPartsInformationDropDown");
                    this.overstockPartsInformationDropDownQueryWindow.SelectionDecided += this.OverstockPartsInformationDropDownQueryWindow_SelectionDecided;
                    this.overstockPartsInformationDropDownQueryWindow.Loaded += this.OverstockPartsInformationDropDownQueryWindow_Loaded;
                }
                return this.overstockPartsInformationDropDownQueryWindow;
            }
        }

        public QueryWindowBase CompanyStockDropDownQueryWindow {
            get {
                if(this.companyStockDropDownQueryWindow == null) {
                    this.companyStockDropDownQueryWindow = DI.GetQueryWindow("CompanyStockDropDown");
                    this.companyStockDropDownQueryWindow.SelectionDecided += this.CompanyStockDropDownQueryWindow_SelectionDecided;
                    this.companyStockDropDownQueryWindow.Loaded += this.CompanyStockDropDownQueryWindow_Loaded;
                }
                return this.companyStockDropDownQueryWindow;
            }
        }

        private void OverstockPartsInformationDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsSalesOrderForTemplateApproveDataEditView = this.DataContext as PartsSalesOrderForTemplateApproveDataEditView;
            if(partsSalesOrderForTemplateApproveDataEditView == null)
                return;
            var partsSalesOrder = partsSalesOrderForTemplateApproveDataEditView.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(queryWindow == null)
                return;
            var partsSalesOrderProcessDetail = queryWindow.DataContext as PartsSalesOrderProcessDetail;
            if(partsSalesOrderProcessDetail == null)
                return;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.Load(domainContext.GetCompaniesQuery().Where(r => r.Id == partsSalesOrder.InvoiceReceiveCompanyId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                var compositeFilterItem = new CompositeFilterItem();
                compositeFilterItem.LogicalOperator = LogicalOperator.And;
                compositeFilterItem.Filters.Add(new FilterItem("CompanyId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrder.BranchId));
                compositeFilterItem.Filters.Add(new FilterItem("SparePartId", typeof(int), FilterOperator.IsEqualTo, partsSalesOrderProcessDetail.SparePartId));
                compositeFilterItem.Filters.Add(new FilterItem("StorageCompanyType", typeof(int), FilterOperator.IsEqualTo, entity.Type));
                queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
                queryWindow.ExchangeData(null, "RefreshQueryResult", null);
            }, null);
        }

        private void OverstockPartsInformationDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsSalesOrderForTemplateApproveDataEditView = this.DataContext as PartsSalesOrderForTemplateApproveDataEditView;
            if(partsSalesOrderForTemplateApproveDataEditView == null)
                return;
            var partsSalesOrder = partsSalesOrderForTemplateApproveDataEditView.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            if(queryWindow == null)
                return;
            var partsSalesOrderProcessDetail = queryWindow.DataContext as PartsSalesOrderProcessDetail;
            if(partsSalesOrderProcessDetail == null)
                return;
            var selectItem = queryWindow.SelectedEntities.Cast<OverstockPartsStock>().FirstOrDefault();
            if(selectItem == null)
                return;
            partsSalesOrderProcessDetail.SupplierCompanyId = selectItem.StorageCompanyId;
            partsSalesOrderProcessDetail.SupplierCompanyCode = selectItem.StorageCompanyCode;
            partsSalesOrderProcessDetail.SupplierCompanyName = selectItem.StorageCompanyName;
            if(selectItem.UsableQuantity >= partsSalesOrderProcessDetail.UnfulfilledQuantity) {
                partsSalesOrderProcessDetail.CurrentFulfilledQuantity = partsSalesOrderProcessDetail.UnfulfilledQuantity;
                partsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.满足;
            } else if(selectItem.UsableQuantity > 0) {
                partsSalesOrderProcessDetail.CurrentFulfilledQuantity = selectItem.UsableQuantity;
                partsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.未满足;
            }


            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void CompanyStockDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var companyPartsStock = queryWindow.SelectedEntities.Cast<CompanyPartsStock>().FirstOrDefault();
            if(companyPartsStock == null)
                return;

            var partsSalesOrderForTemplateApproveDataEditView = this.DataContext as PartsSalesOrderForTemplateApproveDataEditView;
            if(partsSalesOrderForTemplateApproveDataEditView == null)
                return;
            var partsSalesOrder = partsSalesOrderForTemplateApproveDataEditView.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;

            var partsSalesOrderProcessDetail = queryWindow.DataContext as PartsSalesOrderProcessDetail;
            if(partsSalesOrderProcessDetail == null)
                return;
            partsSalesOrderProcessDetail.NewPartId = companyPartsStock.SparePartId;
            partsSalesOrderProcessDetail.NewPartCode = companyPartsStock.SparePartCode;
            partsSalesOrderProcessDetail.NewPartName = companyPartsStock.SparePartName;
            if(companyPartsStock.UsableQuantity >= partsSalesOrderProcessDetail.UnfulfilledQuantity) {
                partsSalesOrderProcessDetail.CurrentFulfilledQuantity = partsSalesOrderProcessDetail.UnfulfilledQuantity;
                partsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.满足;
            } else if(companyPartsStock.UsableQuantity > 0) {
                partsSalesOrderProcessDetail.CurrentFulfilledQuantity = companyPartsStock.UsableQuantity;
                partsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.未满足;
            }

            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null || partsSalesOrder.SalesUnit == null)
                return;
            domainContext.Load(domainContext.根据销售类型查询配件销售价Query(partsSalesOrder.SalesUnit.PartsSalesCategoryId, partsSalesOrder.PartsSalesOrderTypeId, partsSalesOrder.InvoiceReceiveCompanyId, partsSalesOrder.IfDirectProvision), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError)
                    return;
                var entity = loadOption.Entities.FirstOrDefault();
                if(entity != null) {
                    partsSalesOrderProcessDetail.OrderPrice = entity.PartsSalesPrice;
                    partsSalesOrderProcessDetail.OrderProcessMethod = (int)DcsPartsSalesOrderProcessDetailProcessMethod.本部处理;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void CompanyStockDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow != null)
                queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is PartsSalesOrderProcessDetail) {
                        var partsSalesOrderProcessDetail = item as PartsSalesOrderProcessDetail;
                        if(partsSalesOrderProcessDetail.UnfulfilledQuantity > partsSalesOrderProcessDetail.CurrentFulfilledQuantity)
                            return this.Resources["BlueDataBackground"] as Style;
                    }
                    return null;
                }
            };
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var partsSalesOrderProcessDetail = e.Cell.DataContext as PartsSalesOrderProcessDetail;
            if(partsSalesOrderProcessDetail == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "TransferPrice":
                    if(!(e.NewValue is Decimal) || (decimal)e.NewValue <= 0) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsSalesUIStrings.DataGridView_Validation_PartsSalesOrderProcessDetail_TransferPriceIsNotValid;
                    }
                    break;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var partsSalesOrderProcessDetail = e.Cell.DataContext as PartsSalesOrderProcessDetail;
            if(partsSalesOrderProcessDetail == null)
                return;
            var partsSalesOrderForTemplateApproveDataEditView = this.DataContext as PartsSalesOrderForTemplateApproveDataEditView;
            if(partsSalesOrderForTemplateApproveDataEditView == null)
                return;
            var partsSalesOrder = partsSalesOrderForTemplateApproveDataEditView.DataContext as PartsSalesOrder;
            if(partsSalesOrder == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "OrderProcessMethod":
                    if(e.NewData.Equals(e.OldData))
                        return;
                    partsSalesOrderProcessDetail.CurrentFulfilledQuantity = default(int);
                    partsSalesOrderProcessDetail.SupplierCompanyId = default(int);
                    partsSalesOrderProcessDetail.SupplierCompanyName = default(string);
                    partsSalesOrderProcessDetail.SupplierCompanyCode = default(string);
                    partsSalesOrderProcessDetail.WarehouseId = default(int);
                    partsSalesOrderProcessDetail.WarehouseCode = default(string);
                    partsSalesOrderProcessDetail.WarehouseName = default(string);
                    partsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.未满足;
                    break;
                case "WarehouseId":
                    var radComboBox = e.EditingElement as RadComboBox;
                    var domainContext = this.DomainContext as DcsDomainContext;
                    if(radComboBox == null || radComboBox.SelectedItem == null || domainContext == null)
                        return;

                    var keyValuePair = radComboBox.SelectedItem as KeyValuePair;
                    if(keyValuePair == null)
                        return;
                    var warehouse = keyValuePair.UserObject as Warehouse;
                    if(warehouse == null)
                        return;
                    partsSalesOrderProcessDetail.WarehouseId = warehouse.Id;
                    partsSalesOrderProcessDetail.WarehouseCode = warehouse.Code;
                    partsSalesOrderProcessDetail.WarehouseName = warehouse.Name;
                    // 操作步骤2  过程同步骤1一致
                    domainContext.Load(domainContext.查询企业库存Query(partsSalesOrder.SalesUnit.OwnerCompanyId, new[] {
                        partsSalesOrderProcessDetail.SparePartId
                    }), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        var companyPartsStock = loadOp.Entities.FirstOrDefault(r => r.SparePartId == partsSalesOrderProcessDetail.SparePartId);
                        if(companyPartsStock != null)
                            if(companyPartsStock.UsableQuantity >= partsSalesOrderProcessDetail.UnfulfilledQuantity) {
                                partsSalesOrderProcessDetail.CurrentFulfilledQuantity = partsSalesOrderProcessDetail.UnfulfilledQuantity;
                                partsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.满足;
                            } else if(companyPartsStock.UsableQuantity > 0) {
                                partsSalesOrderProcessDetail.CurrentFulfilledQuantity = companyPartsStock.UsableQuantity;
                                partsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.未满足;
                            }
                    }, null);
                    break;
                case "CurrentFulfilledQuantity":
                    if(partsSalesOrderProcessDetail.CurrentFulfilledQuantity > partsSalesOrderProcessDetail.OrderedQuantity) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderProcessDetail_CurrentFulfilledQuantityGreateThanUnfulfilledQuantityError);
                        partsSalesOrderProcessDetail.CurrentFulfilledQuantity = partsSalesOrderProcessDetail.OrderedQuantity;
                    }
                    if(partsSalesOrderProcessDetail.OrderedQuantity == partsSalesOrderProcessDetail.CurrentFulfilledQuantity)
                        partsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.满足;
                    else
                        partsSalesOrderProcessDetail.OrderProcessStatus = (int)DcsPartsSalesOrderProcessDetailOrderProcessStatus.未满足;
                    break;
            }
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsSalesOrderProcessDetail = e.Row.DataContext as PartsSalesOrderProcessDetail;

            if(partsSalesOrderProcessDetail == null || (partsSalesOrderProcessDetail.WarehouseId == default(int) && partsSalesOrderProcessDetail.OrderProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发 && partsSalesOrderProcessDetail.OrderProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.未处理)) {
                e.Cancel = true;
                UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_PartsSalesOrderProcess_WarehouseIsRequire);
                return;
            }
            switch(e.Cell.Column.UniqueName) {
                case "SupplierCompanyName":
                case "TransferPrice":
                    e.Cancel = partsSalesOrderProcessDetail.OrderProcessMethod != (int)DcsPartsSalesOrderProcessDetailProcessMethod.积压件平台;
                    break;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderProcessDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "OrderProcessStatus",
                        IsReadOnly = true,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "CurrentFulfilledQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "OrderProcessMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }, new DropDownTextBoxColumnItem {
                        Name = "SupplierCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderProcessDetail_SupplierCompanyName,
                        DropDownContent = this.OverstockPartsInformationDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        IsReadOnly = true,
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderProcessDetail_WarehouseName,
                    }, new DropDownTextBoxColumnItem {
                        Name = "NewPartCode",
                        //DropDownContent = this.CompanyStockDropDownQueryWindow,
                        IsEditable = true,
                        IsReadOnly = true,
                        Title = PartsSalesUIStrings.DataGridView_Title_WarehousePartsStock_NewPartCode
                    },new ColumnItem {
                        Name = "OrderPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }
                    //, new ColumnItem {
                    //    Name = "TransferPrice",
                    //    MaskType = MaskType.Numeric,
                    //    TextAlignment = TextAlignment.Right,
                    //    Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderProcessDetail_TransferPrice
                    //}
                    , 
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.CellValidating += this.GridView_CellValidating;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["OrderedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CurrentFulfilledQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OrderPrice"]).DataFormatString = "c2";
            //((GridViewDataColumn)this.GridView.Columns["TransferPrice"]).DataFormatString = "c2";
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesOrderProcessDetails");
        }

    }
}
