﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderProcessDetailForSupplyApproveDataGridView : DcsDataGridViewBase {
        private ObservableCollection<KeyValuePair> kvPartsSupplierRelations;
        private ObservableCollection<KeyValuePair> kvProcessMethods;

        private readonly string[] kvNames = new[] {
            "PartsSalesOrderProcessDetail_ProcessMethod"
        };

        public PartsSalesOrderProcessDetailForSupplyApproveDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                KvProcessMethods.Clear();
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]].Where(r => r.Key == (int)DcsPartsSalesOrderProcessDetailProcessMethod.供应商直发)) {
                    this.KvProcessMethods.Add(keyValuePair);
                }
            });
            this.DataContextChanged += PartsSalesOrderProcessDetailForSupplyApproveDataGridView_DataContextChanged;
        }

        private void PartsSalesOrderProcessDetailForSupplyApproveDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesOrderProcess = this.DataContext as PartsSalesOrderProcess;
            if(partsSalesOrderProcess == null)
                return;
            var sparePartIds = partsSalesOrderProcess.PartsSalesOrderProcessDetails.Select(v => v.SparePartId).ToArray();
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null || !sparePartIds.Any())
                return;
            domainContext.Load(domainContext.GetPartsSupplierRelationsWithPartsSupplierByIdsQuery(sparePartIds, partsSalesOrderProcess.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSupplierRelations.Clear();
                var partsSupplierRelationsList = loadOp.Entities.Distinct().ToList();
                foreach(var entity in partsSupplierRelationsList) {
                    if(this.KvPartsSupplierRelations.Any(r => r.Key == (entity.PartsSupplier == null ? default(int) : entity.PartsSupplier.Id)))
                        continue;
                    this.KvPartsSupplierRelations.Add(new KeyValuePair {
                        Key = entity.PartsSupplier == null ? default(int) : entity.PartsSupplier.Id,
                        Value = entity.PartsSupplier == null ? default(string) : entity.PartsSupplier.Name,
                        UserObject = entity.PartsSupplier
                    });
                }
            }, null);
        }

        private ObservableCollection<KeyValuePair> KvProcessMethods {
            get {
                return this.kvProcessMethods ?? (this.kvProcessMethods = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<KeyValuePair> KvPartsSupplierRelations {
            get {
                return this.kvPartsSupplierRelations ?? (this.kvPartsSupplierRelations = new ObservableCollection<KeyValuePair>());
            }
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var partsSalesOrderProcessDetail = e.Cell.DataContext as PartsSalesOrderProcessDetail;
            if(partsSalesOrderProcessDetail == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "CurrentFulfilledQuantity":
                    if(!(e.NewValue is int) || (int)e.NewValue < 0) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsSalesUIStrings.DataGridView_Validation_PartsSalesOrderProcessDetail_CurrentFulfilledQuantityIsLessZeroError;
                    } else if((int)e.NewValue > partsSalesOrderProcessDetail.UnfulfilledQuantity) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsSalesUIStrings.DataGridView_Validation_PartsSalesOrderProcessDetail_CurrentFulfilledQuantityGreateThanUnfulfilledQuantityError;
                    }
                    break;
                case "SupplierCompanyId":
                    if(!(e.NewValue is int) || (int)e.NewValue == 0) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsSalesUIStrings.DataGridView_Validation_PartsSalesOrderProcessDetail_SupplierCompanyIsRequire;
                    }
                    break;
            }
        }

        private void GridView_PreparingCellForEdit(object sender, GridViewPreparingCellForEditEventArgs e) {
            var partsSalesOrderProcessDetail = e.Row.DataContext as PartsSalesOrderProcessDetail;
            if(partsSalesOrderProcessDetail == null)
                return;

            switch(e.Column.UniqueName) {
                case "SupplierCompanyId":
                    var comboBox = e.EditingElement as RadComboBox;
                    if(comboBox == null)
                        return;
                    comboBox.ItemsSource = this.KvPartsSupplierRelations.Where(r => ((PartsSupplier)r.UserObject).PartsSupplierRelations.Any(v => v.PartId == partsSalesOrderProcessDetail.SparePartId)).ToList();
                    break;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var partsSalesOrderProcessDetail = e.Cell.DataContext as PartsSalesOrderProcessDetail;
            if(partsSalesOrderProcessDetail == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "SupplierCompanyId":
                    var radComboBox = e.EditingElement as RadComboBox;
                    if(radComboBox == null || radComboBox.SelectedItem == null)
                        return;

                    var keyValuePair = radComboBox.SelectedItem as KeyValuePair;
                    if(keyValuePair != null) {
                        partsSalesOrderProcessDetail.SupplierCompanyId = keyValuePair.Key;
                        partsSalesOrderProcessDetail.SupplierCompanyName = ((PartsSupplier)keyValuePair.UserObject).Name;
                        partsSalesOrderProcessDetail.SupplierCompanyCode = ((PartsSupplier)keyValuePair.UserObject).Code;
                    }
                    break;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderProcessDetail_SupplierCompany,
                        Name = "SupplierCompanyId",
                        KeyValueItems = this.KvPartsSupplierRelations
                    }, new KeyValuesColumnItem {
                        Name = "OrderProcessMethod",
                        KeyValueItems = this.KvProcessMethods
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UnfulfilledQuantity",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_UnfulfilledQuantity,
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem {
                        Name = "CurrentFulfilledQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem {
                        Name = "OrderPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderProcessDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesOrderProcessDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.MaxHeight = 450;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.PreparingCellForEdit += GridView_PreparingCellForEdit;
            ((GridViewDataColumn)this.GridView.Columns["OrderedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CurrentFulfilledQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OrderPrice"]).DataFormatString = "c2";
        }
    }
}
