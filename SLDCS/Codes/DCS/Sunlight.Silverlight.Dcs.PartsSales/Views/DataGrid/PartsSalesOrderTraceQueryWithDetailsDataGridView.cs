﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderTraceQueryWithDetailsDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = { "PartsShipping_Method", "Company_Type", "PartsSalesOrder_Status", "PartsShippingOrder_Status", "PartsOutboundPlan_Status" };

        public PartsSalesOrderTraceQueryWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override string OnRequestQueryName() {
            return "根据登陆人员查询销售订单";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] { "PartsSalesOrderTrace" }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehiclePartsSalesOrdercs);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new KeyValuesColumnItem{
                       Name="Status",
                       KeyValueItems = this.KeyValueManager[this.KvNames[2]],
                       Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_Status
                    },new ColumnItem{
                       Name="PartsSalesOrderCode"
                    },new ColumnItem{
                       Name="SalesCategoryName"
                    },new ColumnItem{
                       Name="WarehouseCode"
                    },new ColumnItem{
                       Name="WarehouseName"
                    },new ColumnItem{
                       Name="SubmitCompanyCode",
                       Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Code
                    },new ColumnItem{
                       Name="SubmitCompanyName",
                       Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Name
                    },new KeyValuesColumnItem{
                       Name="CustomerType",
                       KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    },new ColumnItem{
                       Name="PartsSalesOrderTypeName",
                       Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsSalesOrderTypeNameNew
                    },new ColumnItem{
                       Name="SubmitTime"
                    },new ColumnItem{
                       Name="ApproveTime"
                    },new ColumnItem {
                        Name = "PartsOutboundPlanCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_Code
                    },new KeyValuesColumnItem {
                        Name = "PartsOutboundPlanStatus",
                        Title = PartsSalesUIStrings.DataGridView_Title_VehiclePartsSalesOrdercs_PartsOutboundPlanStatus,
                        KeyValueItems = this.KeyValueManager[this.KvNames[4]],
                    }, new ColumnItem{
                       Name="PartsShippingOrderCode"
                    },new KeyValuesColumnItem{
                       Name="ShippingStatus",
                       KeyValueItems = this.KeyValueManager[this.KvNames[3]],
                       Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ShippingStatus
                    },new ColumnItem{
                       Name="ShippingDate"
                    },new KeyValuesColumnItem{
                       Name="ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    },new ColumnItem{
                       Name="PSOrderConsigneeName"
                    }
                    ,new ColumnItem{
                       Name="ERPSourceOrderCode",
                       Title=PartsSalesUIStrings.QueryPanel_Title_AgencyRetailerOrder_ERPSourceOrderCode
                    }
                    ,new ColumnItem{
                       Name="ConfirmedReceptionTime"
                    },new ColumnItem{
                       Name="PartsInboundPlanCode",
                       Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsInboundPlanCode
                    },new ColumnItem{
                       Name="PartsInboundCheckBillCode",
                       Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsInboundCheckBillCode
                    },new ColumnItem{
                       Name="SparePartCode"
                    },new ColumnItem{
                       Name="SparePartName"
                    },new ColumnItem{
                       Name="OrderedQuantity",
                       Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_OrderedQuantity
                    },new ColumnItem{
                       Name="ApproveQuantity",
                       Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ApproveQuantity
                    },new ColumnItem{
                       Name="OutboundQuantity",
                       Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_OutboundQuantity
                    },new ColumnItem{
                       Name="ShippingQuantity",
                       Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_ShippingQuantity
                    },new ColumnItem{
                       Name="ConfirmQuantity",
                       Title=PartsSalesUIStrings.DataGridView_Title_VirtualPartsPurchaseOrderDetail_ConfirmAmount
                    },new ColumnItem{
                       Name="InboundQuantity",
                       Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_InboundQuantity
                    },new ColumnItem{
                        Name="SalesUnitOwnerCompanyName",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_SalesUnitOwnerCompanyName
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var dateTimeFilterItem = filters.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                DateTime? createTimeBegin = null;
                DateTime? createTimeEnd = null;
                if(dateTimeFilterItem != null) {
                    createTimeBegin = dateTimeFilterItem.Filters.First(r => r.MemberName == "SubmitTime").Value as DateTime?;
                    createTimeEnd = dateTimeFilterItem.Filters.Last(r => r.MemberName == "SubmitTime").Value as DateTime?;
                }
                switch(parameterName) {
                    case "submitTimeBegin":
                        return createTimeBegin;
                    case "submitTimeEnd":
                        return createTimeEnd;
                }
                if(parameterName == "erpSourceOrderCode") {
                    var filter = filters.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "ERPSourceOrderCode");
                    return filter != null ? filter.Value : null;
                }
                if(parameterName == "salesUnitOwnerCompanyId") {
                    var filter = filters.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "SalesUnitOwnerCompanyId");
                    return filter != null ? filter.Value : null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);

        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "SubmitTime" && filter.MemberName != "ERPSourceOrderCode")) {
                    newCompositeFilterItem.Filters.Add(item);

                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
