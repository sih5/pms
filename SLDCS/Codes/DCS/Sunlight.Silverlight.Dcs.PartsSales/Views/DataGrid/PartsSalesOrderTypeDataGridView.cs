﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderTypeDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsSalesOrderTypeDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_BranchName,
                        Name = "PartsSalesCategory.BranchName"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_Name,
                        Name = "PartsSalesCategory.Name"
                    }
                };

            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            if(this.FilterItem is CompositeFilterItem) {
                var compositeFilterItem = this.FilterItem as CompositeFilterItem;
                foreach(var filter in compositeFilterItem.Filters.Where(e => e.MemberName != "PartsSalesCategory.Code" && e.MemberName != "PartsSalesCategory.Name"))
                    newCompositeFilterItem.Filters.Add(filter);
            } else
                if(this.FilterItem.MemberName != "PartsSalesCategory.Code" && this.FilterItem.MemberName != "PartsSalesCategory.Name")
                    newCompositeFilterItem.Filters.Add(this.FilterItem);
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "categoryCode":
                    var compositeFilterItem = this.FilterItem as CompositeFilterItem;
                    if(compositeFilterItem != null) {
                        var filterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategory.Code");
                        return filterItem == null ? null : filterItem.Value;
                    }
                    return this.FilterItem.MemberName == "PartsSalesCategory.Code" ? this.FilterItem.Value : null;
                case "categoryName":
                    var item = this.FilterItem as CompositeFilterItem;
                    if(item != null) {
                        var filterItem = item.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategory.Name");
                        return filterItem == null ? null : filterItem.Value;
                    }
                    return this.FilterItem.MemberName == "PartsSalesCategory.Name" ? this.FilterItem.Value : null;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderType);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesOrderTypeWithPartsSalesCategoriesByParameters";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["AbandonTime"]).DataFormatString = "d";
        }
    }
}
