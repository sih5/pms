﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderTypeForEditDataGridView : DcsDataGridViewBase {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderType);
            }
        }


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            //this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            //this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            //this.GridView.CellValidating -= this.GridView_CellValidating;
            //this.GridView.CellValidating += this.GridView_CellValidating;
        }

        //private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
        //    if(e.Cell.Column.UniqueName.Equals("Remark")) {
        //        var partsSalesOrderType = e.Cell.ParentRow.DataContext as PartsSalesOrderType;
        //        if(partsSalesOrderType == null)
        //            return;
        //        if(!string.IsNullOrWhiteSpace(partsSalesOrderType.Remark)) {
        //            e.ErrorMessage = PartsSalesUIStrings.PartsSalesOrderType_Remark;
        //        }
        //    }
        //}

        //private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
        //    e.NewObject = new PartsSalesOrderType {
        //        Status = (int)DcsBaseDataStatus.有效,
        //        BranchId = BaseApp.Current.CurrentUserData.EnterpriseId
        //    };
        //}

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesOrderTypes");
        }
    }
}
