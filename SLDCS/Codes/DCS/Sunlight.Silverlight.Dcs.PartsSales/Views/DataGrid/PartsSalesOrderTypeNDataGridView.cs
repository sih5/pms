﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderTypeNDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status","SalesOrderType_SettleType","SalesOrderType_BusinessType"
        };

        public PartsSalesOrderTypeNDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_BusinessCode,
                        Name = "BusinessCode"
                    }, new KeyValuesColumnItem {
                        Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_SettleType,
                        Name = "SettleType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new KeyValuesColumnItem {
                        Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_BusinessType,
                        Name = "BusinessType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Title="是否参与智能计划",
                        Name = "IsSmart",
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_BranchName,
                        Name="Branch.Name"
                        //Name = "PartsSalesCategory.BranchName"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_Name,
                        Name = "PartsSalesCategory.Name"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderType);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesOrderTypeWithPartsSalesCategories";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

    }
}
