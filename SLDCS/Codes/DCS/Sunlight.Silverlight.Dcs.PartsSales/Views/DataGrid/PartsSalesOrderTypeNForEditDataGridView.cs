﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesOrderTypeNForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "SalesOrderType_SettleType","SalesOrderType_BusinessType"
        };
        public PartsSalesOrderTypeNForEditDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }  
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesOrderType);
            }
        }


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "BusinessCode",
                        Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_BusinessCode
                    }, new KeyValuesColumnItem {
                        Name = "SettleType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_SettleType
                    }, new KeyValuesColumnItem {
                        Name = "BusinessType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_BusinessType
                    }, new ColumnItem {
                        Name = "IsSmart",
                        Title="是否参与智能计划"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            //this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;

        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            e.NewObject = new PartsSalesOrderType {
                Status = (int)DcsBaseDataStatus.有效,
                BranchId = BaseApp.Current.CurrentUserData.EnterpriseId
            };
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesOrderTypes");
        }

    }
}
