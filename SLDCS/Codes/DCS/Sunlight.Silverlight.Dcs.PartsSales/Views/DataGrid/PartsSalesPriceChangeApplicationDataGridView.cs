﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceChangeApplicationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesPriceChange_Status"
        };
        public PartsSalesPriceChangeApplicationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(PartsSalesPriceChange);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },  new ColumnItem {
                        Name = "Code"
                    },new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "InitialApproverName"
                    }, new ColumnItem {
                        Name = "InitialApproveTime"
                    }, new ColumnItem {
                        Name = "FinalApproverName",
                        Title=PartsSalesUIStrings.DataGridView_Title_FinalApproverName
                    }, new ColumnItem {
                        Name = "FinalApproveTime",
                        Title=PartsSalesUIStrings.DataGridView_Title_FinalApproveTime
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "AbandonComment",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_AbandonComment
                    }, new ColumnItem {
                        Name = "Rejecter"
                    }, new ColumnItem {
                        Name = "RejectTime"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                         Name = "PartsSalesCategoryCode"
                    }, new ColumnItem {
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPartsSalesPriceChangesForOrder";
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSalesPriceChange"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["RejectTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["FinalApproveTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["InitialApproveTime"]).DataFormatString = "d";
        }
    }
}
