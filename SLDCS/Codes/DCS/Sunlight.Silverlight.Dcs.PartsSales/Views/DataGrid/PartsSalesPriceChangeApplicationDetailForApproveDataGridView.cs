﻿
using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceChangeApplicationDetailForApproveDataGridView : DcsDataGridViewBase {
        public PartsSalesPriceChangeApplicationDetailForApproveDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsSalesPriceChangeDetailForApproveDataGridView_DataContextChanged;
        }
        private int id;
        private void PartsSalesPriceChangeDetailForApproveDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null || partsSalesPriceChange.Id == default(int))
                return;
            this.id = partsSalesPriceChange.Id;
            this.ExecuteQueryDelayed();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "parentId":
                    return id;
                default:
                    return null;
            }
        }

        private readonly string[] kvNames = {
            "PartsSalesPrice_PriceType","IsOrNot"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                         Name = "SparePartName"
                    }, new KeyValuesColumnItem {
                        Name = "PriceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "SalesPrice"
                    }, new ColumnItem{
                        Name="ReferencePrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_ReferencePrice
                    }, new ColumnItem{
                        Name="PurchasePrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_PurchasePrice
                    },new KeyValuesColumnItem {
                        Name = "IsUpsideDown",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        IsReadOnly=true,
                        TextAlignment=TextAlignment.Center
                    }, new ColumnItem{
                        Name="MaxPurchasePricing",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxPurchasePricing
                    }, new ColumnItem{
                        Name="MaxExchangeSalePrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxExchangeSalePrice
                    }, new ColumnItem{
                        Name="IncreaseRateSale",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_IncreaseRateSale
                    }, new ColumnItem{
                        Name="IncreaseRateRetail",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_IncreaseRateRetail
                    }, new ColumnItem {
                        Name = "RetailGuidePrice"
                    },new ColumnItem {
                        Name = "DealerSalesPrice",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_DealerSalesPrice
                    }, new ColumnItem {
                        Name="CategoryCode",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_CategoryCode,
                        IsReadOnly=true
                    },new ColumnItem{
                         Name="CategoryName",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_CategoryName,
                        IsReadOnly=true
                    },new ColumnItem{
                         Name="SalesPriceFluctuationRatio",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_SalesPriceFluctuationRatio
                    },new ColumnItem{
                          Name="RetailPriceFluctuationRatio",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_RetailPriceFluctuationRatio,
                        IsReadOnly=true
                    },new ColumnItem{
                         Name="MaxSalesPriceFloating",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxSalesPriceFloating,
                        IsReadOnly=true
                    },new ColumnItem{
                         Name="MinSalesPriceFloating",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MinSalesPriceFloating,
                        IsReadOnly=true
                    },new ColumnItem{
                         Name="MaxRetailOrderPriceFloating",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxRetailOrderPriceFloating,
                        IsReadOnly=true
                    },new ColumnItem{
                         Name="MinRetailOrderPriceFloating",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MinRetailOrderPriceFloating,
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件销售价变更申请清单附带加价率";
        }

        //protected override Binding OnRequestDataSourceBinding() {
        //    return new Binding("PartsSalesPriceChangeDetails");
        //}

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["DealerSalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ReferencePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["IncreaseRateSale"]).DataFormatString = "P";
            ((GridViewDataColumn)this.GridView.Columns["IncreaseRateRetail"]).DataFormatString = "P";
            ((GridViewDataColumn)this.GridView.Columns["SalesPriceFluctuationRatio"]).DataFormatString = "P";
            ((GridViewDataColumn)this.GridView.Columns["RetailPriceFluctuationRatio"]).DataFormatString = "P";
        }
        protected override Type EntityType {
            get {
                return typeof(PartsSalesPriceChangeDetail);
            }
        }
    }
}
