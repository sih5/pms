﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceChangeApplicationForUpateDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesPrice_PriceType","IsOrNot"
        };

        private ObservableCollection<KeyValuePair> kvPriceType;
        public ObservableCollection<KeyValuePair> KvPriceType {
            get {
                return kvPriceType ?? (kvPriceType = new ObservableCollection<KeyValuePair>());
            }
        }
        public PartsSalesPriceChangeApplicationForUpateDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                this.KvPriceType.Clear();
                foreach(var priceType in this.KeyValueManager[this.kvNames[0]].Where(r => r.Key == (int)DcsPartsSalesPricePriceType.基准销售价)) {
                    this.KvPriceType.Add(new KeyValuePair {
                        Key = priceType.Key,
                        Value = priceType.Value
                    });
                }
            });
        }



        private QueryWindowBase sparePartPriceQueryWindow;
        private QueryWindowBase SparePartPriceQueryWindow {
            get {
                if(this.sparePartPriceQueryWindow == null) {
                    this.sparePartPriceQueryWindow = DI.GetQueryWindow("SparePartDropDown");
                    this.sparePartPriceQueryWindow.SelectionDecided += partsSalesPriceQueryPanel_SelectionDecided;
                }
                return this.sparePartPriceQueryWindow;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesPriceChangeDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesPriceChangeDetails");
        }

        void partsSalesPriceQueryPanel_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var partsSalesPriceChangeDetail = queryWindow.DataContext as PartsSalesPriceChangeDetail;
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChangeDetail == null || partsSalesPriceChange == null)
                return;
            DateTime dateTime = DateTime.Now.Date;
            partsSalesPriceChangeDetail.SparePartId = sparePart.Id;
            partsSalesPriceChangeDetail.SparePartCode = sparePart.Code;
            partsSalesPriceChangeDetail.SparePartName = sparePart.Name;
            partsSalesPriceChangeDetail.PriceType = (int)DcsPartsSalesPricePriceType.基准销售价;
            partsSalesPriceChangeDetail.CategoryCode = sparePart.CategoryCode;
            partsSalesPriceChangeDetail.CategoryName = sparePart.CategoryName;
            partsSalesPriceChangeDetail.ValidationTime = dateTime;
            partsSalesPriceChangeDetail.ExpireTime = new DateTime(dateTime.AddYears(1).Year, dateTime.Month, dateTime.Day, 23, 59, 59);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();

            var partsSalesPriceChang = this.DataContext as PartsSalesPriceChange;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(partsSalesPriceChang.PartsSalesCategoryId == default(int))
                return;
            domainContext.Load(domainContext.查询配件销售价变更申请清单附带采购价指导价Query(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesPriceChange.PartsSalesCategoryId, partsSalesPriceChangeDetail.SparePartId, partsSalesPriceChangeDetail.CategoryCode, partsSalesPriceChange.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(loadOp.Entities == null)
                    return;
                var virtialPartsSalesPriceChangeDetail = loadOp.Entities.FirstOrDefault();
                if(virtialPartsSalesPriceChangeDetail != null) {
                    partsSalesPriceChangeDetail.PurchasePrice = virtialPartsSalesPriceChangeDetail.PurchasePrice;
                    partsSalesPriceChangeDetail.PartsSalesPrice_SalePrice = virtialPartsSalesPriceChangeDetail.PartsSalesPrice_SalePrice;
                    partsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice = virtialPartsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice;
                    partsSalesPriceChangeDetail.MaxSalesPriceFloating = virtialPartsSalesPriceChangeDetail.MaxSalesPriceFloating;
                    partsSalesPriceChangeDetail.MinSalesPriceFloating = virtialPartsSalesPriceChangeDetail.MinSalesPriceFloating;
                    partsSalesPriceChangeDetail.MaxRetailOrderPriceFloating = virtialPartsSalesPriceChangeDetail.MaxRetailOrderPriceFloating;
                    partsSalesPriceChangeDetail.MinRetailOrderPriceFloating = virtialPartsSalesPriceChangeDetail.MinRetailOrderPriceFloating;
                }
            }, null);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "SparePartCode",
                        DropDownContent = this.SparePartPriceQueryWindow,
                        IsEditable = false
                    },
                    new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    },
                    new KeyValuesColumnItem {
                        Name = "PriceType",
                        KeyValueItems = this.KvPriceType
                    },
                    new ColumnItem {
                        Name = "SalesPrice",
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "DealerSalesPrice"
                    },
                    new ColumnItem {
                        Name = "RetailGuidePrice",
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "MaxPurchasePricing",
                        TextAlignment = TextAlignment.Right,
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxPurchasePricing,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "MaxExchangeSalePrice",
                        TextAlignment = TextAlignment.Right,
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxExchangeSalePrice,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "Remark"
                    },
                    new KeyValuesColumnItem {
                        Name = "IsUpsideDown",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Center
                    },
                    new ColumnItem {
                        Name = "CategoryCode",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_CategoryCode,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "CategoryName",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_CategoryName,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "SalesPriceFluctuationRatio",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_SalesPriceFluctuationRatio,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "RetailPriceFluctuationRatio",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_RetailPriceFluctuationRatio,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "MaxSalesPriceFloating",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxSalesPriceFloating,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "MinSalesPriceFloating",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MinSalesPriceFloating,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "MaxRetailOrderPriceFloating",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MaxRetailOrderPriceFloating,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "MinRetailOrderPriceFloating",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_MinRetailOrderPriceFloating,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.CellEditEnded -= GridView_CellEdited;
            this.GridView.CellEditEnded += GridView_CellEdited;
            ((GridViewDataColumn)this.GridView.Columns["DealerSalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPriceFluctuationRatio"]).DataFormatString = "p";
            ((GridViewDataColumn)this.GridView.Columns["RetailPriceFluctuationRatio"]).DataFormatString = "p";

            //this.GridView.CellValidating += GridView_CellValidating;
        }


        void GridView_CellEdited(object sender, GridViewCellEditEndedEventArgs e) {
            var partsSalesPriceChangeDetail = e.Cell.DataContext as PartsSalesPriceChangeDetail;
            var partsSalesPriceChang = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChangeDetail == null || partsSalesPriceChang.PartsSalesCategoryId == default(int))
                return;
            switch(e.Cell.DataColumn.UniqueName) {
                case "SalesPrice":
                    if(partsSalesPriceChangeDetail.SalesPrice > 0) {
                        partsSalesPriceChangeDetail.IsUpsideDown = partsSalesPriceChangeDetail.SalesPrice >= partsSalesPriceChangeDetail.PurchasePrice ? 0 : 1;
                        if(partsSalesPriceChangeDetail.PartsSalesPrice_SalePrice > 0)
                            partsSalesPriceChangeDetail.SalesPriceFluctuationRatio = Convert.ToDouble(partsSalesPriceChangeDetail.SalesPrice / partsSalesPriceChangeDetail.PartsSalesPrice_SalePrice - 1);
                    }
                    break;
                case "RetailGuidePrice":
                    if(partsSalesPriceChangeDetail.RetailGuidePrice > 0 && partsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice > 0)
                        partsSalesPriceChangeDetail.RetailPriceFluctuationRatio = Convert.ToDouble(partsSalesPriceChangeDetail.RetailGuidePrice / partsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice - 1);
                    break;
            }

        }

    }
}
