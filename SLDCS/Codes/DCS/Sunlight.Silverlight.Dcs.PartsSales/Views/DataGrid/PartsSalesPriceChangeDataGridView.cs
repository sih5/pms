﻿
using System;
using System.Linq;
using Telerik.Windows.Data;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceChangeDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesPriceChange_Status"
        };
        public PartsSalesPriceChangeDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(PartsSalesPriceChange);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        IsSortable = false,
                        Name = "IsUplodFile",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_IsUplodFile
                    },new ColumnItem {
                        Name = "Code"
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "InitialApproverName"
                    }, new ColumnItem {
                        Name = "InitialApproveTime"
                    }, new ColumnItem {
                        Name = "ApproverName",
                        Title =PartsSalesUIStrings.DataGridView_ColumnItem_Title_Approver
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ApproveTime
                    }, new ColumnItem {
                        Name = "FinalApproverName",
                        Title= PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproverName
                    }, new ColumnItem {
                        Name = "FinalApproveTime",
                        Title= PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproveTime
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "AbandonComment",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_AbandonComment
                    }, new ColumnItem {
                        Name = "Rejecter"
                    }, new ColumnItem {
                        Name = "RejectTime"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                         Name = "PartsSalesCategoryCode"
                    }, new ColumnItem {
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPartsSalesPriceChangesForOrder";
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSalesPriceChange"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["RejectTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["FinalApproveTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["InitialApproveTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ApproveTime"]).DataFormatString = "d";
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if (compositeFilterItem == null)
            {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            }
            else
            {
                foreach (var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "SparePartCode"  && filter.MemberName != "SparePartName"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                switch (parameterName)
                {
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
