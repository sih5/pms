﻿
using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceChangeDetailDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(PartsSalesPriceChangeDetail);
            }
        }

        public PartsSalesPriceChangeDetailDataGridView() {
            this.DataContextChanged += PartsSalesPriceChangeDetailDataGridView_DataContextChanged;
        }

        protected override string OnRequestQueryName() {
            return "查询配件销售价变更申请清单附带加价率";// GetPartsSalesPriceChangeDetails
        }
        private int id;
        void PartsSalesPriceChangeDetailDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChange == null || partsSalesPriceChange.Id == default(int))
                return;
            this.id = partsSalesPriceChange.Id;
            this.ExecuteQueryDelayed();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["MaxPurchasePricing"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CenterPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CenterPriceRatio"]).DataFormatString = "P";
            ((GridViewDataColumn)this.GridView.Columns["SalesPriceFluctuationRatio"]).DataFormatString = "P";
            ((GridViewDataColumn)this.GridView.Columns["RetailPriceFluctuationRatio"]).DataFormatString = "P";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                         Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "PriceTypeName"
                    }, new ColumnItem {
                        Name = "MaxPurchasePricing",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_PurchasePrice,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "CenterPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Centerprice,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "CenterPriceRatio",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChangeDetail_CenterPriceRatio,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Dealerprice,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SalesPriceFluctuationRatio",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChangeDetail_CenterPriceRatio,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "RetailGuidePrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "RetailPriceFluctuationRatio",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChangeDetail_CenterPriceRatio,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "parentId":
                    return id;
                default:
                    return null;
            }
        }
    }
}
