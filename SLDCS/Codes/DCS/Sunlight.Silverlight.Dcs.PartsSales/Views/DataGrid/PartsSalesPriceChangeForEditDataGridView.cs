﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceChangeForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesPrice_PriceType","IsOrNot"
        };

        private ObservableCollection<KeyValuePair> kvPriceType;
        public ObservableCollection<KeyValuePair> KvPriceType {
            get {
                return kvPriceType ?? (kvPriceType = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<KeyValuePair> kvPriceIncreaseRates;
        public ObservableCollection<KeyValuePair> KvPriceIncreaseRates {
            get {
                return this.kvPriceIncreaseRates ?? (this.kvPriceIncreaseRates = new ObservableCollection<KeyValuePair>());
            }
        }

        public PartsSalesPriceChangeForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                this.KvPriceType.Clear();
                foreach(var priceType in this.KeyValueManager[this.kvNames[0]].Where(r => r.Key == (int)DcsPartsSalesPricePriceType.基准销售价)) {
                    this.KvPriceType.Add(new KeyValuePair {
                        Key = priceType.Key,
                        Value = priceType.Value
                    });
                }
            });
        }



        private QueryWindowBase sparePartPriceQueryWindow;
        private QueryWindowBase SparePartPriceQueryWindow {
            get {
                if(this.sparePartPriceQueryWindow == null) {
                    this.sparePartPriceQueryWindow = DI.GetQueryWindow("SparePartDropDown");
                    this.sparePartPriceQueryWindow.SelectionDecided += partsSalesPriceQueryPanel_SelectionDecided;
                }
                return this.sparePartPriceQueryWindow;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesPriceChangeDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesPriceChangeDetails");
        }

        void partsSalesPriceQueryPanel_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var partsSalesPriceChangeDetail = queryWindow.DataContext as PartsSalesPriceChangeDetail;
            if(partsSalesPriceChangeDetail == null)
                return;
            partsSalesPriceChangeDetail.SparePartId = sparePart.Id;
            partsSalesPriceChangeDetail.SparePartCode = sparePart.Code;
            partsSalesPriceChangeDetail.SparePartName = sparePart.Name;
            partsSalesPriceChangeDetail.PriceType = (int)DcsPartsSalesPricePriceType.基准销售价;
            partsSalesPriceChangeDetail.PriceTypeName = this.KvPriceIncreaseRates[0].Value;
            partsSalesPriceChangeDetail.CategoryCode = sparePart.CategoryCode;
            partsSalesPriceChangeDetail.CategoryName = sparePart.CategoryName;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();

            var partsSalesPriceChange = this.DataContext as PartsSalesPriceChange;
            var domainContext = new DcsDomainContext();
            if(partsSalesPriceChange.PartsSalesCategoryId == default(int))
                return;
            domainContext.Load(domainContext.查询配件销售价变更申请清单附带采购价指导价Query(BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesPriceChange.PartsSalesCategoryId, partsSalesPriceChangeDetail.SparePartId, partsSalesPriceChangeDetail.CategoryCode, partsSalesPriceChange.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(loadOp.Entities == null)
                    return;
                var virtualPartsSalesPriceChangeDetail = loadOp.Entities.FirstOrDefault();
                if(virtualPartsSalesPriceChangeDetail != null) {
                    //partsSalesPriceChangeDetail.PriceTypeName = virtualPartsSalesPriceChangeDetail.PriceTypeName;
                    partsSalesPriceChangeDetail.PurchasePrice = virtualPartsSalesPriceChangeDetail.PurchasePrice;
                    partsSalesPriceChangeDetail.PartsSalesPrice_SalePrice = virtualPartsSalesPriceChangeDetail.PartsSalesPrice_SalePrice;
                    partsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice = virtualPartsSalesPriceChangeDetail.PartsRetailGuidePrice_RetailGuidePrice;
                }
            }, null);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "SparePartCode",
                        DropDownContent = this.SparePartPriceQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    },new KeyValuesColumnItem{
                        Name = "PriceTypeName",
                        KeyValueItems = this.KvPriceIncreaseRates
                    }, new ColumnItem {
                        Name = "CenterPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Centerprice,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Dealerprice,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "RetailGuidePrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice,
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "IsUpsideDown",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        IsReadOnly=true,
                        TextAlignment=TextAlignment.Center
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CenterPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewComboBoxColumn)this.GridView.Columns["PriceTypeName"]).SelectedValueMemberPath = "Value";
            ((GridViewComboBoxColumn)this.GridView.Columns["PriceTypeName"]).DisplayMemberPath = "Value";
            this.DomainContext.Load(((DcsDomainContext)this.DomainContext).GetPartsSalePriceIncreaseRatesQuery().Where(r => r.status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                foreach(var item in loadOp.Entities) {
                    this.KvPriceIncreaseRates.Add(new KeyValuePair {
                        Value = item.GroupCode
                    });
                }
            }, null);
        }

        void GridView_CellEdited(object sender, GridViewCellEditEndedEventArgs e) {
            var partsSalesPriceChangeDetail = e.Cell.DataContext as PartsSalesPriceChangeDetail;
            var partsSalesPriceChang = this.DataContext as PartsSalesPriceChange;
            if(partsSalesPriceChangeDetail == null)
                return;
            if(e.Cell.DataColumn.UniqueName == "SalesPrice" && partsSalesPriceChangeDetail.SalesPrice > 0) {
                partsSalesPriceChangeDetail.IsUpsideDown = partsSalesPriceChangeDetail.SalesPrice >= partsSalesPriceChangeDetail.PurchasePrice ? 0 : 1;
            }
        }
    }
}
