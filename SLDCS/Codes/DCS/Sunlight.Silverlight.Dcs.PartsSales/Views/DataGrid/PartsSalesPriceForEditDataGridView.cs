﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceForEditDataGridView : DcsDataGridViewBase {
        private MultiDataQueryWindowBase sparePartsInfoAndPriceQueryWindow;
        private RadWindow radQueryWindow;

        private MultiDataQueryWindowBase SparePartsInfoAndPriceQueryWindow {
            get {
                if(this.sparePartsInfoAndPriceQueryWindow == null) {
                    this.sparePartsInfoAndPriceQueryWindow = DI.GetQueryWindow("SparePartsInfoAndPrice") as DcsMultiDataQueryWindowBase;
                    if(this.sparePartsInfoAndPriceQueryWindow != null) {
                        this.sparePartsInfoAndPriceQueryWindow.SelectionDecided += this.SparePartsInfoAndPriceQueryWindow_SelectionDecided;
                        this.sparePartsInfoAndPriceQueryWindow.Loaded += this.PartsRetailGuidePriceQueryWindowLoaded;
                    }
                }
                return this.sparePartsInfoAndPriceQueryWindow;
            }
        }

        private void PartsRetailGuidePriceQueryWindowLoaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsMultiDataQueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void SparePartsInfoAndPriceQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiDataQueryWindowBase;
            if(queryWindow == null)
                return;
            var selectedEntities = queryWindow.SelectedEntities;
            if(selectedEntities == null)
                return;
            var partsSalesPrices = this.GridView.GetValue(DataControl.ItemsSourceProperty) as ObservableCollection<PartsSalesPrice>;
            if(partsSalesPrices == null)
                return;

            var partsSalesPrice = new PartsSalesPrice {
                ValidationTime = DateTime.Now.Date,
                ExpireTime = DateTime.Now.Date,
                //为了避免报错，赋默认值。在DataEditView保存数据时，替换为页面上编辑的值
                PartsSalesCategoryCode = "Empty",
                PartsSalesCategoryName = "Empty",
                PriceType = (int)DcsPartsSalesPricePriceType.基准销售价,
                Status = (int)DcsBaseDataStatus.有效
            };
            var enumerable = selectedEntities as Entity[] ?? selectedEntities.ToArray();
            if(enumerable.First() is PartsRetailGuidePrice) {
                var selectedPartsRetailGuidePrice = enumerable.Cast<PartsRetailGuidePrice>().ToArray();
                if(selectedPartsRetailGuidePrice != null && selectedPartsRetailGuidePrice.Any()) {
                    var partsRetailGuidePrice = selectedPartsRetailGuidePrice.First();
                    partsSalesPrice.SparePartId = partsRetailGuidePrice.SparePartId;
                    partsSalesPrice.SparePartCode = partsRetailGuidePrice.SparePartCode;
                    partsSalesPrice.SparePartName = partsRetailGuidePrice.SparePartName;
                    if(partsRetailGuidePrice.Ratio == 1) {
                        partsSalesPrice.SalesPrice = partsRetailGuidePrice.RetailGuidePrice;
                    } else {
                        partsSalesPrice.SalesPrice = partsRetailGuidePrice.RetailGuidePrice * partsRetailGuidePrice.Ratio;
                    }
                }
            } else {
                var selectedPartsPurchasePricingDetail = enumerable.Cast<PartsPurchasePricingDetail>().ToArray();
                if(selectedPartsPurchasePricingDetail != null && selectedPartsPurchasePricingDetail.Any()) {
                    var partsPurchasePricingDetail = selectedPartsPurchasePricingDetail.First();
                    partsSalesPrice.SparePartId = partsPurchasePricingDetail.ParentId;
                    partsSalesPrice.SparePartCode = partsPurchasePricingDetail.PartCode;
                    partsSalesPrice.SparePartName = partsPurchasePricingDetail.PartName;
                    if(partsPurchasePricingDetail.Ratio == 1) {
                        partsSalesPrice.SalesPrice = partsPurchasePricingDetail.Price;
                    } else {
                        partsSalesPrice.SalesPrice = partsPurchasePricingDetail.Price * partsPurchasePricingDetail.Ratio;
                    }
                }
            }
            if(partsSalesPrices.All(entity => entity.SparePartId != partsSalesPrice.SparePartId))
                partsSalesPrices.Add(partsSalesPrice);
            else
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesPrice_SparePartIsRepeat);
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            this.RadQueryWindow.ShowDialog();
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(e.Cell.IsSelected) {
                if(e.Cell.Column.UniqueName == "ValidationTime" || e.Cell.Column.UniqueName == "ExpireTime") {
                    var partsSalesPrice = (PartsSalesPrice)e.Cell.DataContext;
                    foreach(var salesPrice in this.GridView.SelectedItems.Cast<PartsSalesPrice>()) {
                        if(e.Cell.Column.UniqueName == "ValidationTime")
                            salesPrice.ValidationTime = partsSalesPrice.ValidationTime;
                        if(e.Cell.Column.UniqueName == "ExpireTime")
                            salesPrice.ExpireTime = partsSalesPrice.ExpireTime;
                    }
                }
            }
        }

        private RadWindow RadQueryWindow {
            get {
                if(this.radQueryWindow == null) {
                    radQueryWindow = new RadWindow {
                        Content = this.SparePartsInfoAndPriceQueryWindow,
                        Header = PartsSalesUIStrings.QueryPanel_Title_SparePartsInfoAndPrice,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen
                    };
                }
                return this.radQueryWindow;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.CellEditEnded -= this.GridView_CellEditEnded;
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            ((GridViewDataColumn)this.GridView.Columns["ValidationTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ExpireTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("CollectionPartsSalesPrices");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem {
                        Name = "ValidationTime"
                    }, new ColumnItem {
                        Name = "ExpireTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesPrice);
            }
        }
    }
}
