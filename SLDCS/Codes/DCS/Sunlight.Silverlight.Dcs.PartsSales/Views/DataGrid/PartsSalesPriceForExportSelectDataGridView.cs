﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceForExportSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesPrice_PriceType"
        };

        public PartsSalesPriceForExportSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPrice_SparePartCode,
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPrice_SparePartName,
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPrice_ReferenceCode,
                        Name = "ReferenceCode"
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPrice_Price,
                        Name = "Price",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    },new ColumnItem {
                        Name = "Remark",
                        Title =PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPrice_Remark
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsSalesPrice);
            }
        }

        protected override string OnRequestQueryName() {
            return "根据销售类型查询配件销售价出口使用";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var cFilterItem = compositeFilterItem.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(cFilterItem == null)
                    return null;
                FilterItem filter;
                switch(parameterName) {
                    case "partsSalesCategoryId":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesCategoryId");
                        return filter == null ? 0 : filter.Value;
                    case "customerId":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "CustomerId");
                        return filter == null ? 0 : filter.Value;
                    case "ifDirectProvision":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "IfDirectProvision");
                        return filter == null ? 0 : filter.Value;
                    case "orderTypeId":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "OrderTypeId");
                        return filter == null ? 0 : filter.Value;
                    case "priceType":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "PriceType");
                        return filter == null ? 0 : filter.Value;
                }
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach(var filter in compositeFilterItem.Filters.
                Where(item => (item.GetType() != typeof(CompositeFilterItem) ||
                              !((CompositeFilterItem)item).Filters.Any(cItem => (new string[] { "PartsSalesCategoryId", "CustomerId", "IfDirectProvision", "PriceType" }).Contains(cItem.MemberName)))
                    ))
                newCompositeFilterItem.Filters.Add(filter);
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
        }
    }
}
