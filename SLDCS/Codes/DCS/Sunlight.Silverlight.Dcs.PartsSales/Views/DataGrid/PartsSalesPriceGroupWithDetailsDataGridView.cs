﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceGroupWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsSalesPriceGroupWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        private ObservableCollection<KeyValuePair> kvPriceIncreaseRates;
        public ObservableCollection<KeyValuePair> KvPriceIncreaseRates {
            get {
                return this.kvPriceIncreaseRates ?? (this.kvPriceIncreaseRates = new ObservableCollection<KeyValuePair>());
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PartsSalesPrice);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "RetailGuidePrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = "集团客户价"
                    }, new ColumnItem {
                        Name = "CenterPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Centerprice
                    }, new KeyValuesColumnItem {
                        Name = "PriceType",
                        KeyValueItems = this.KvPriceIncreaseRates
                    },  new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    },new ColumnItem {
                        Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSalesPriceHistory"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "主查询配件销售价";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["ApproveTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CenterPrice"]).DataFormatString = "c2";
            this.DomainContext.Load(((DcsDomainContext)this.DomainContext).GetPartsSalePriceIncreaseRatesQuery().Where(r => r.status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                foreach(var item in loadOp.Entities) {
                    this.KvPriceIncreaseRates.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.GroupCode
                    });
                }
            }, null);
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                FilterItem filterItem;
                switch(parameterName) {
                    case "isSale":
                        filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "isSale");
                        if(filterItem != null)
                            return filterItem.Value;
                        else
                            return null;
                    case "isOrderable":
                        filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "isOrderable");
                        if(filterItem != null)
                            return filterItem.Value;
                        else
                            return null;
                    case "PartsSalesCategoryId":
                        filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesCategoryId");
                        if(filterItem != null)
                            return filterItem.Value;
                        else
                            return null;
                    case "priceType":
                        filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "PriceType");
                        if(filterItem != null)
                            return filterItem.Value;
                        else
                            return null;
                    case "sparePartCodes": 
                        var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");  
                        if(codes == null || codes.Value == null)  
                            return null;  
                        return codes.Value.ToString();  
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "isSale" && filter.MemberName != "isOrderable" && filter.MemberName != "PriceType")) {
                    newCompositeFilterItem.Filters.Add(item);

                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
