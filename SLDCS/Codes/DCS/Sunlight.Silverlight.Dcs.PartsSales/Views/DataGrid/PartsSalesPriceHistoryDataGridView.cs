﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceHistoryDataGridView : DcsDataGridViewBase {
        //private readonly string[] kvNames = {
        //    "PartsSalesPrice_PriceType"
        //};

        public PartsSalesPriceHistoryDataGridView() {
            //this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PartsSalesPriceHistoryDataGridView_DataContextChanged;
        }

        private void PartsSalesPriceHistoryDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesPrice = e.NewValue as PartsSalesPrice;
            var getAllPartsSalesPrice = e.NewValue as GetAllPartsSalesPrice;

            var compositeFilter = new CompositeFilterItem();
            if(partsSalesPrice != null && partsSalesPrice.Id != default(int)) {
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "SparePartId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = partsSalesPrice.SparePartId
                });
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "PartsSalesCategoryId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = partsSalesPrice.PartsSalesCategoryId
                });
                this.FilterItem = compositeFilter;
                this.ExecuteQueryDelayed();
                return;
            }

            if(getAllPartsSalesPrice != null && !string.IsNullOrEmpty(getAllPartsSalesPrice.PartsSalesCategoryName)) {
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "SparePartCode",
                    MemberType = typeof(string),
                    Operator = FilterOperator.IsEqualTo,
                    Value = getAllPartsSalesPrice.SparePartCode
                });
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "PartsSalesCategoryName",
                    MemberType = typeof(string),
                    Operator = FilterOperator.IsEqualTo,
                    Value = getAllPartsSalesPrice.PartsSalesCategoryName
                });
                this.FilterItem = compositeFilter;
                this.ExecuteQueryDelayed();
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesPriceHistory);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "RetailGuidePrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice
                    }, new ColumnItem {
                        Name = "CenterPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Centerprice
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Dealerprice
                    }, new ColumnItem {
                        Name = "PriceTypeName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesPriceChange_PriceType
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Title=PartsSalesUIStrings.QueryPanel_QueryItem_Title_PartsSalesCategory_Name,
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesPriceHistories";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CenterPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";

        }
    }
}
