﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesPriceHistoryForSelectDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
            "PartsSalesPrice_PriceType"
        };

        public PartsSalesPriceHistoryForSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_Code,
                        Name = "PartsSalesPriceChange.Code"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_SparePartCode,
                        Name = "SparePartCode"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_SparePartName,
                        Name = "SparePartName"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice,
                        Name = "RetailGuidePrice"
                    },
                    new ColumnItem {
                        Name = "CenterPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Centerprice
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Dealerprice,
                        Name = "SalesPrice"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_PriceType,
                        Name = "PriceTypeName"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_CreatorName,
                        Name = "PartsSalesPriceChange.CreatorName"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_CreateTime,
                        Name = "PartsSalesPriceChange.CreateTime"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_ModifierName,
                        Name = "PartsSalesPriceChange.ModifierName"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_ModifyTime,
                        Name = "PartsSalesPriceChange.ModifyTime"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_InitialApproverName,
                        Name = "PartsSalesPriceChange.InitialApproverName"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_InitialApproveTime,
                        Name = "PartsSalesPriceChange.InitialApproveTime"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_FinalApproverName,
                        Name = "PartsSalesPriceChange.FinalApproverName"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_FinalApproveTime,
                        Name = "PartsSalesPriceChange.FinalApproveTime"
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_PartsSalesCategory,
                        Name = "PartsSalesPriceChange.PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesPriceChangeDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件销售价格变更申请";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CenterPrice"]).DataFormatString = "c2";
        }
    }
}
