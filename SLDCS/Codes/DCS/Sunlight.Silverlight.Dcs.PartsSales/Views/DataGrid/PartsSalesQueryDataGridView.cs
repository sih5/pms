﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesQueryDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseName
                    },
                    new ColumnItem {
                        Name = "ReceivingCompanyCode",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_ReceivingCompanyCode
                    },
                    new ColumnItem {
                        Name = "ReceivingCompanyName",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_ReceivingCompanyName
                    },
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    },
                    new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },
                    new ColumnItem {
                        Name = "SettlementPrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesQuery_SettlementPrice
                    },
                    new ColumnItem {
                        Name = "Qty",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Qty
                    },
                    new ColumnItem {
                        Name = "TotalPrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesQuery_TotalPrice
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesQuery);
            }
        }

        protected override string OnRequestQueryName() {
            return PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay;
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }
    }
}
