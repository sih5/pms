﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    /// <summary>
    /// 配件销售退货单清单
    /// </summary>
    public class PartsSalesReturnBillDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "TraceProperty"
        };
        public PartsSalesReturnBillDetailDataGridView() {
            this.DataContextChanged += this.PartsSalesReturnBillDetailDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SparePartCode"
                    }, new ColumnItem{
                        Name = "SparePartName"
                    }, new ColumnItem{
                        Name = "MeasureUnit"
                    }, new ColumnItem{
                        Name = "ReturnedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem{
                        Name = "ApproveQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem{
                        Name = "OriginalOrderPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem{
                        Name = "ReturnPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    },new ColumnItem{
                        Name = "DiscountRate",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_DiscountRate,
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }, new ColumnItem{
                        Name = "BatchNumber"
                    }, new ColumnItem{
                        Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice,
                        Name = "OriginalPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    },  new ColumnItem {
                        Name = "PartsSalesOrderCode",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_SalesOrder
                    },new ColumnItem {
                        Name = "PartsSalesOrder.ERPSourceOrderCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_PlatForm_Code
                    },new  ColumnItem{
                        Name = "Remark"
                    }, new KeyValuesColumnItem {
                        Name = "TraceProperty",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]],
                        Title = "追溯属性"
                    }, new ColumnItem {
                        Name = "SIHLabelCode",
                        Title = "标签码"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesReturnBillDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesReturnBillDetailForOrders";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["ReturnedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ApproveQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OriginalOrderPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ReturnPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
        }

        private void PartsSalesReturnBillDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesReturnBill = e.NewValue as PartsSalesReturnBill;
            if(partsSalesReturnBill == null || partsSalesReturnBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesReturnBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsSalesReturnBill.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
    }
}
