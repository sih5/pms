﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesReturnBillDetailForAddByDeputyEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase partsSalesPriceDropDownQueryWindow;
        private readonly string[] kvNames = {
            "TraceProperty"
        };

        public PartsSalesReturnBillDetailForAddByDeputyEditDataGridView() {
            this.DataContextChanged += PartsSalesReturnBillDetailForAddByDeputyEditDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
        }

        private void PartsSalesReturnBillDetailForAddByDeputyEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            //partsSalesReturnBill.PropertyChanged -= this.PartsSalesReturnBill_PropertyChanged;
            //partsSalesReturnBill.PropertyChanged += this.PartsSalesReturnBill_PropertyChanged;
        }

        private QueryWindowBase PartsSalesPriceDropDownQueryWindow {
            get {
                if(this.partsSalesPriceDropDownQueryWindow == null) {
                    this.partsSalesPriceDropDownQueryWindow = DI.GetQueryWindow("PartsSalesPriceDropDown");
                    this.partsSalesPriceDropDownQueryWindow.Loaded += this.PartsSalesPriceDropDownQueryWindow_Loaded;
                    this.partsSalesPriceDropDownQueryWindow.SelectionDecided += this.PartsSalesPriceDropDownQueryWindow_SelectionDecided;
                }
                return this.partsSalesPriceDropDownQueryWindow;
            }
        }

        private void PartsSalesPriceDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindowBase = sender as QueryWindowBase;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(queryWindowBase == null || partsSalesReturnBill == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesCategoryId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsSalesReturnBill.PartsSalesCategoryId
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "CustomerId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsSalesReturnBill.ReturnCompanyId
            });
            queryWindowBase.ExchangeData(null, "SetAdditionalFilterItem", new object[] { compositeFilterItem });
        }

        private void PartsSalesPriceDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(queryWindow == null || queryWindow.SelectedEntities == null || partsSalesReturnBill == null)
                return;
            var partsSalesPrice = queryWindow.SelectedEntities.Cast<VirtualPartsSalesPrice>().FirstOrDefault();
            if(partsSalesPrice == null)
                return;
            var partsSalesReturnBillDetail = queryWindow.DataContext as PartsSalesReturnBillDetail;
            if(partsSalesReturnBillDetail == null)
                return;

            if(partsSalesReturnBill.PartsSalesReturnBillDetails.Any(r => r.SparePartId == partsSalesPrice.SparePartId)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBillDetail_SparePartIdIsDouble);
                return;
            }

            partsSalesReturnBillDetail.SparePartId = partsSalesPrice.SparePartId;
            partsSalesReturnBillDetail.SparePartCode = partsSalesPrice.SparePartCode;
            partsSalesReturnBillDetail.SparePartName = partsSalesPrice.SparePartName;
            partsSalesReturnBillDetail.MeasureUnit = partsSalesPrice.MeasureUnit;
            partsSalesReturnBillDetail.OriginalOrderPrice = partsSalesPrice.PartsSalesPrice;
            partsSalesReturnBillDetail.ReturnPrice = partsSalesReturnBillDetail.OriginalOrderPrice.GetValueOrDefault();
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            if(partsSalesReturnBill.SalesUnitId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_PartsSalesReturnBill_PartsSalesCategoryIsNull);
                e.Cancel = true;
                return;
            }
            domainContext.Load(domainContext.GetSalesUnitsQuery().Where(ex => ex.Id == partsSalesReturnBill.SalesUnitId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null || entity.PartsSalesCategoryId == default(int)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_PartsSalesReturnBill_PartsSalesCategoryIsNull);
                    e.Cancel = true;
                    return;
                }
                partsSalesPriceDropDownQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, entity.PartsSalesCategoryId));
            }, null);
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsSalesReturnBillDetail = e.Row.DataContext as PartsSalesReturnBillDetail;
            if(partsSalesReturnBillDetail == null)
                return;
        }

        //private void PartsSalesReturnBill_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
        //    var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
        //    if(partsSalesReturnBill == null)
        //        return;
        //    switch(e.PropertyName) {
        //        case "ReturnType":
        //            if(partsSalesReturnBill.ReturnType == 0 || partsSalesReturnBill.ReturnType != (int)DcsPartsSalesReturn_ReturnType.特殊退货)
        //                ((GridViewDataColumn)this.GridView.Columns["ReturnPrice"]).IsReadOnly = true;
        //            else
        //                ((GridViewDataColumn)this.GridView.Columns["ReturnPrice"]).IsReadOnly = false;
        //            break;
        //    }
        //}

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "SparePartCode",
                        DropDownContent = this.PartsSalesPriceDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PartsSalesOrderCode",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_SalesOrder,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ReturnedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric
                    }, new ColumnItem {
                        Name = "OriginalOrderPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ReturnPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly=true
                    },
                    new ColumnItem { 
                        Name = "DiscountRate", 
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_DiscountRate, 
                        TextAlignment = TextAlignment.Right, 
                        MaskType = Core.Model.MaskType.Numeric, 
                        IsReadOnly = true 
                    }, new ColumnItem {
                        Name = "OriginalPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly = true,
                        Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice
                    },new ColumnItem {
                        Name = "Remark"
                    }, new KeyValuesColumnItem {
                        Name = "TraceProperty",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly=true,
                        Title="追溯属性"
                    },new ColumnItem {
                        Name = "SIHLabelCode",
                        Title="标签码"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesReturnBillDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesReturnBillDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.DataPager.PageSize = 15;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleted += GridView_Deleted;
            ((GridViewDataColumn)this.GridView.Columns["ReturnedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OriginalOrderPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ReturnPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ReturnPrice"]).DataFormatString = "c2";
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "ReturnedQuantity":
                    partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(r => r.ReturnedQuantity * r.ReturnPrice);
                    break;
            }
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            if(partsSalesReturnBill.PartsSalesReturnBillDetails.Any())
                partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(r => r.ReturnedQuantity * r.ReturnPrice);
            else
                partsSalesReturnBill.TotalAmount = default(decimal);
        }
    }
}
