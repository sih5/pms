﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesReturnBillDetailForReportEditDataGridView : DcsDataGridViewBase {

        private QueryWindowBase companyStockDropDownQueryWindow;
        private QueryWindowBase companyStockQueryWindow;
        private RadWindow radWindow;
        private readonly string[] kvNames = {
            "TraceProperty"
        };

        public PartsSalesReturnBillDetailForReportEditDataGridView() {
            this.DataContextChanged += PartsSalesReturnBillDetailForReportEditDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
        }

        private QueryWindowBase CompanyStockQueryWindow {
            get {
                if(companyStockQueryWindow == null) {
                    companyStockQueryWindow = DI.GetQueryWindow("CompanyStock");
                    if(companyStockQueryWindow != null) {
                        this.companyStockQueryWindow.Loaded += this.CompanyStockQueryWindow_Loaded;
                        this.companyStockQueryWindow.SelectionDecided += this.CompanyStockQueryWindow_SelectionDecided;
                    }
                }
                return this.companyStockQueryWindow;
            }
        }

        private QueryWindowBase warehousePartsStockQueryWindow;
        private QueryWindowBase WarehousePartsStockQueryWindow {
            get {
                if(warehousePartsStockQueryWindow == null) {
                    warehousePartsStockQueryWindow = DI.GetQueryWindow("WarehousePartsStock");
                    warehousePartsStockQueryWindow.Loaded += warehousePartsStockQueryWindow_Loaded;
                    warehousePartsStockQueryWindow.SelectionDecided += warehousePartsStockQueryWindow_SelectionDecided;
                }
                return this.warehousePartsStockQueryWindow;
            }
        }
        private QueryWindowBase warehousePartsStockQueryWindowDropDown;
        private QueryWindowBase WarehousePartsStockQueryWindowDropDown {
            get {
                if(warehousePartsStockQueryWindowDropDown == null) {
                    warehousePartsStockQueryWindowDropDown = DI.GetQueryWindow("WarehousePartsStockDropDown");
                    warehousePartsStockQueryWindowDropDown.Loaded += warehousePartsStockQueryWindow_Loaded;
                    warehousePartsStockQueryWindowDropDown.SelectionDecided += warehousePartsStockQueryWindow_SelectionDecided;
                }
                return this.warehousePartsStockQueryWindowDropDown;
            }
        }

        private void warehousePartsStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(queryWindow == null || !queryWindow.SelectedEntities.Any() || partsSalesReturnBill == null)
                return;
            var warehousePartsStock = queryWindow.SelectedEntities.Cast<WarehousePartsStock>().FirstOrDefault();
            if(warehousePartsStock == null)
                return;
            if(partsSalesReturnBill.PartsSalesReturnBillDetails.Any(r => r.SparePartId == warehousePartsStock.SparePartId)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderDetail_SparePartIdIsDouble);
                return;
            }
            var partsSalesReturnBillDetail = new PartsSalesReturnBillDetail();
            partsSalesReturnBill.PartsSalesReturnBillDetails.Add(partsSalesReturnBillDetail);
            partsSalesReturnBillDetail.PartsSalesReturnBillId = partsSalesReturnBill.Id;
            partsSalesReturnBillDetail.SparePartId = warehousePartsStock.SparePartId;
            partsSalesReturnBillDetail.SparePartCode = warehousePartsStock.SparePartCode;
            partsSalesReturnBillDetail.SparePartName = warehousePartsStock.SparePartName;
            partsSalesReturnBillDetail.MeasureUnit = warehousePartsStock.MeasureUnit;
            partsSalesReturnBillDetail.OriginalOrderPrice = warehousePartsStock.PartsSalesPrice;
            partsSalesReturnBillDetail.ReturnPrice = partsSalesReturnBillDetail.OriginalOrderPrice.GetValueOrDefault();
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void warehousePartsStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || partsSalesReturnBill == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "WarehouseId", partsSalesReturnBill.ReturnWarehouseId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "WarehouseId", false
            });
        }

        private RadWindow warehouseWindow;
        private RadWindow WarehouseWindow {
            get {
                if(warehouseWindow == null) {
                    warehouseWindow = new RadWindow();
                    warehouseWindow.Header = PartsSalesUIStrings.DataGridView_Header_WarehousePartsStockQueryWindow;
                    warehouseWindow.Content = this.WarehousePartsStockQueryWindow;
                    warehouseWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;
                }
                return warehouseWindow;
            }
        }
        private RadWindow SelectedWindow {
            get {
                if(radWindow == null) {
                    radWindow = new RadWindow();
                    radWindow.Header = PartsSalesUIStrings.QueryPanel_Title_CompanyPartsStock;
                    radWindow.Content = this.CompanyStockQueryWindow;
                    radWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;
                }
                return radWindow;
            }
        }

        private Company company;
        private DcsDropDownQueryWindowBase CompanyStockDropDownQueryWindow {
            get {
                if(this.companyStockDropDownQueryWindow == null) {
                    this.companyStockDropDownQueryWindow = DI.GetQueryWindow("CompanyStockDropDown");
                    this.companyStockDropDownQueryWindow.Loaded += this.CompanyStockDropDownQueryWindow_Loaded;
                    this.companyStockDropDownQueryWindow.SelectionDecided += this.CompanyStockDropDownQueryWindow_SelectionDecided;
                }
                return (DcsDropDownQueryWindowBase)this.companyStockDropDownQueryWindow;
            }
        }

        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        private void PartsSalesReturnBillDetailForReportEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            this.GridView.CanUserInsertRows = false;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            partsSalesReturnBill.PropertyChanged -= this.PartsSalesReturnBill_PropertyChanged;
            partsSalesReturnBill.PropertyChanged += this.PartsSalesReturnBill_PropertyChanged;
            domainContext.Load(domainContext.GetCompaniesQuery().Where(ex => ex.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.FirstOrDefault();
                if(company == null)
                    return;
                this.company = company;
            }, null);
        }

        private void CompanyStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || partsSalesReturnBill == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("CompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void CompanyStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(queryWindow == null || !queryWindow.SelectedEntities.Any() || partsSalesReturnBill == null)
                return;
            var companyPartsStock = queryWindow.SelectedEntities.Cast<CompanyPartsStock>().FirstOrDefault();
            if(companyPartsStock == null)
                return;
            if(partsSalesReturnBill.PartsSalesReturnBillDetails.Any(r => r.SparePartId == companyPartsStock.SparePartId)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesReturnBillDetail_SparePartIdIsDouble);
                return;
            }
            var partsSalesReturnBillDetail = new PartsSalesReturnBillDetail();
            partsSalesReturnBill.PartsSalesReturnBillDetails.Add(partsSalesReturnBillDetail);
            partsSalesReturnBillDetail.PartsSalesReturnBillId = partsSalesReturnBill.Id;
            partsSalesReturnBillDetail.SparePartId = companyPartsStock.SparePartId;
            partsSalesReturnBillDetail.SparePartCode = companyPartsStock.SparePartCode;
            partsSalesReturnBillDetail.SparePartName = companyPartsStock.SparePartName;
            partsSalesReturnBillDetail.MeasureUnit = companyPartsStock.MeasureUnit;
            partsSalesReturnBillDetail.OriginalOrderPrice = companyPartsStock.PartsSalesPrice;
            partsSalesReturnBillDetail.ReturnPrice = partsSalesReturnBillDetail.OriginalOrderPrice.GetValueOrDefault();
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void CompanyStockDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(queryWindow == null || partsSalesReturnBill == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("CompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void CompanyStockDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(queryWindow == null || !queryWindow.SelectedEntities.Any() || partsSalesReturnBill == null)
                return;
            var companyPartsStock = queryWindow.SelectedEntities.Cast<CompanyPartsStock>().FirstOrDefault();
            if(companyPartsStock == null)
                return;
            if(partsSalesReturnBill.PartsSalesReturnBillDetails.Any(r => r.SparePartId == companyPartsStock.SparePartId)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_PartsSalesOrderDetail_SparePartIdIsDouble);
                return;
            }
            var partsSalesReturnBillDetail = queryWindow.DataContext as PartsSalesReturnBillDetail;
            if(partsSalesReturnBillDetail == null)
                return;
            partsSalesReturnBillDetail.PartsSalesReturnBillId = partsSalesReturnBill.Id;
            partsSalesReturnBillDetail.SparePartId = companyPartsStock.SparePartId;
            partsSalesReturnBillDetail.SparePartCode = companyPartsStock.SparePartCode;
            partsSalesReturnBillDetail.SparePartName = companyPartsStock.SparePartName;
            partsSalesReturnBillDetail.MeasureUnit = companyPartsStock.MeasureUnit;
            partsSalesReturnBillDetail.OriginalOrderPrice = companyPartsStock.PartsSalesPrice;
            partsSalesReturnBillDetail.ReturnPrice = partsSalesReturnBillDetail.OriginalOrderPrice.GetValueOrDefault();
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                if(partsSalesReturnBill.ReturnWarehouseId == null) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_PartsSalesReturnBill_ReturnWarehouseId);
                    return;
                }
                this.WarehouseWindow.ShowDialog();
            } else {
                if(partsSalesReturnBill.BranchId == default(int) || partsSalesReturnBill.SalesUnitId == default(int)) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_PartsSalesReturnBill_PartsSalesCategoryIsNull);
                    return;
                }
                this.SelectedWindow.ShowDialog();
            }
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsSalesReturnBillDetail = e.Row.DataContext as PartsSalesReturnBillDetail;
            if(partsSalesReturnBillDetail == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "SparePartCode":
                    e.Cancel = partsSalesReturnBillDetail.PartsSalesReturnBill.ReturnType == (int)DcsPartsSalesReturnBillReturnType.特殊退货;
                    break;
            }
        }

        private void PartsSalesReturnBill_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsSalesReturnBill = this.DataContext as PartsSalesReturnBill;
            if(partsSalesReturnBill == null)
                return;
            switch(e.PropertyName) {
                //case "ReturnType":
                //    if(partsSalesReturnBill.ReturnType == 0 || partsSalesReturnBill.ReturnType != (int)DcsPartsSalesReturn_ReturnType.特殊退货)
                //        ((GridViewDataColumn)this.GridView.Columns["ReturnPrice"]).IsReadOnly = true;
                //    else
                //        ((GridViewDataColumn)this.GridView.Columns["ReturnPrice"]).IsReadOnly = false;
                //    break;
                case "TotalAmount":
                    partsSalesReturnBill.TotalAmount = partsSalesReturnBill.PartsSalesReturnBillDetails.Sum(entity => entity.ReturnedQuantity * entity.ReturnPrice);
                    break;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "SparePartCode",
                        //DropDownContent = this.CompanyStockDropDownQueryWindow,
                        IsEditable = false
                    },
                    new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "PartsSalesOrderCode",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_SalesOrder,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "ReturnedQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "Remark"
                    },
                    new ColumnItem {
                        Name = "ReturnPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "DiscountRate",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_DiscountRate,
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "OriginalPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice,
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "TraceProperty",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]],
                        Title = "追溯属性",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SIHLabelCode",
                        Title = "标签码"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesReturnBillDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesReturnBillDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.DataPager.PageSize = 15;
            this.GridView.BeginningEdit -= GridView_BeginningEdit;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            ((GridViewDataColumn)this.GridView.Columns["ReturnedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ReturnPrice"]).DataFormatString = "c2";
            this.GridView.CellValidating += GridView_CellValidating;
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            if(e.Cell.Column.UniqueName.Equals("SparePartCode")) {
                var dropDownTextBoxColumnItem = e.EditingElement as DropDownTextBoxColumnItem;
                if(dropDownTextBoxColumnItem == null)
                    return;
                if(company.Type == (int)DcsCompanyType.代理库 || company.Type == (int)DcsCompanyType.服务站兼代理库) {
                    dropDownTextBoxColumnItem.DropDownContent = this.WarehousePartsStockQueryWindowDropDown;
                } else {
                    dropDownTextBoxColumnItem.DropDownContent = this.CompanyStockDropDownQueryWindow;
                }
            }
        }
    }
}
