﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesReturnBillDetailForUploadEditDataGridView
    : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "TraceProperty"
        };
        public PartsSalesReturnBillDetailForUploadEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    },  new ColumnItem {
                        Name = "PartsSalesOrderCode",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_SalesOrder,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ReturnedQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ApproveQuantity",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                         IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OriginalOrderPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ReturnPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly = true
                    }, new ColumnItem{
                        Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice,
                        Name = "OriginalPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = Core.Model.MaskType.Numeric,
                        IsReadOnly = true
                    }, new KeyValuesColumnItem{
                        Title="追溯属性",
                        Name = "TraceProperty",
                         KeyValueItems=this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }, new ColumnItem{
                        Title="标签码",
                        Name = "SIHLabelCode",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesReturnBillDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["ReturnedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ApproveQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OriginalOrderPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ReturnPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OriginalPrice"]).DataFormatString = "c2";
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesReturnBillDetails");
        }
    }
}
