﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    /// <summary>
    /// 配件销售退货单
    /// </summary>
    public class PartsSalesReturnBillWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesReturn_ReturnType", "PartsSalesReturnBill_Status","PartsSalesReturnBill_InvoiceRequirement"
        };

        public PartsSalesReturnBillWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        IsSortable = false,
                        Name = "IsUplodFile",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_IsUplodFile
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "ReturnCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesReturnBill_ReturnCompanyName
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "DiscountRate",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_DiscountRate
                    }
                    //, new ColumnItem {
                    //    Name = "ApproveAmount",
                    //    MaskType = MaskType.Numeric,
                    //    TextAlignment = TextAlignment.Right,
                    //    Title = "审核总金额"
                    //}
                    , new ColumnItem {
                        Name = "SalesUnitName"
                    }, new KeyValuesColumnItem {
                        Name = "ReturnType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "ReturnReason"
                    }, new ColumnItem {
                        Name = "ContactPerson"
                    }, new ColumnItem {
                        Name = "ContactPhone"
                    }, new ColumnItem {
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Name = "serviceApplyCode",
                        Title = PartsSalesUIStrings.DataEdit__PartsSalesOrder_ServiceApplyCode
                    }, new ColumnItem {
                        Name = "IsBarter"
                    }, new ColumnItem {
                        Name = "RejectComment",
                        Title = PartsSalesUIStrings.DataEdit__PartsSalesOrder_RejectReason
                    },new ColumnItem {
                        Name = "StopComment",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_StopReason
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new KeyValuesColumnItem {
                        Name = "InvoiceRequirement",
                         KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Name = "BlueInvoiceNumber"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ApproverName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Approver
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ApproveTime
                    }, new ColumnItem {
                        Name = "RejecterName",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_RejecterName
                    }, new ColumnItem {
                        Name = "RejectTime",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_RejectTime
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "StopName"
                    }, new ColumnItem {
                        Name = "StopTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "InitialApproverName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_InitialApproverName
                    }, new ColumnItem {
                        Name = "InitialApproveTime",
                         MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_InitialApproveTime
                    }, new ColumnItem {
                        Name = "FinalApproverName",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproverName
                    }, new ColumnItem {
                        Name = "FinalApproveTime",
                         MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproveTime
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesReturnBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "根据审批人员查询销售退货单";
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var newCompositeFilterItem = new CompositeFilterItem();
                foreach(var filter in compositeFilterItem.Filters.Where(filter => filter.MemberName != "PartsSalesOrderCode"))
                    newCompositeFilterItem.Filters.Add(filter);
                return newCompositeFilterItem.ToFilterDescriptor();
            }
            return base.OnRequestFilterDescriptor(queryName);
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                if(parameterName == "personnelId") {
                    return BaseApp.Current.CurrentUserData.UserId;
                }
                if(parameterName == "partsSalesOrderCode") {
                    var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "PartsSalesOrderCode");
                    return filter != null ? filter.Value : null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSalesReturnBill", "PartsSalesReturnBillDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
            //((GridViewDataColumn)this.GridView.Columns["ApproveAmount"]).DataFormatString = "c2";
        }
    }
}
