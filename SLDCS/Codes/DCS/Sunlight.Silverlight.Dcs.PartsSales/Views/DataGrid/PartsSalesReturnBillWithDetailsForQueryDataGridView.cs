﻿
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesReturnBillWithDetailsForQueryDataGridView : PartsSalesReturnBillWithDetailsDataGridView {

        protected override string OnRequestQueryName() {
            return "GetPartsSalesReturnBills";
        }
    }
}
