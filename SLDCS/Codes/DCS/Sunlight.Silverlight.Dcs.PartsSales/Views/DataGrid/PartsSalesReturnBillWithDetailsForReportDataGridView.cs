﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesReturnBillWithDetailsForReportDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesReturn_ReturnType", "PartsSalesReturnBill_Status","PartsSalesReturnBill_InvoiceRequirement","PartsShipping_Method"
        };

        public PartsSalesReturnBillWithDetailsForReportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "SalesUnitName"
                    }, new ColumnItem {
                        Name = "DiscountRate",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_DiscountRate
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "ReturnType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "WarehouseName"
                    },new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ShippingMethod,
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    }, new ColumnItem {
                        Name = "ContactPerson"
                    }, new ColumnItem {
                        Name = "ContactPhone"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new KeyValuesColumnItem{
                        Name = "InvoiceRequirement",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Name = "ReturnReason"
                    }, new ColumnItem {
                        Name = "RejectComment",
                        Title = PartsSalesUIStrings.DataEdit__PartsSalesOrder_RejectReason
                    },new ColumnItem {
                        Name = "StopComment",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_StopReason
                    }, new ColumnItem {
                        Name = "BlueInvoiceNumber"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "RejecterName",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_RejecterName
                    }, new ColumnItem {
                        Name = "RejectTime",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_RejectTime
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "StopName"
                    }, new ColumnItem {
                        Name = "StopTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesReturnBill);
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var newCompositeFilterItem = new CompositeFilterItem();
                foreach(var filter in compositeFilterItem.Filters.Where(filter => filter.MemberName != "PartsSalesOrderCode" ))
                    newCompositeFilterItem.Filters.Add(filter);
                return newCompositeFilterItem.ToFilterDescriptor();
            }
            return base.OnRequestFilterDescriptor(queryName);

        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                if(parameterName == "partsSalesOrderCode") {
                    var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "PartsSalesOrderCode");
                    return filter != null ? filter.Value : null;
                }          
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "GetPartsSalesReturnBillsReport";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSalesReturnBill", "PartsSalesReturnBillDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
        }
    }
}
