﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesRtnSettlementDetailDataGridView : DcsDataGridViewBase {

        public PartsSalesRtnSettlementDetailDataGridView() {
            this.DataContextChanged += this.PartsSalesRtnSettlementDetailDataGridView_DataContextChanged;
        }

        private void PartsSalesRtnSettlementDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            //var partsSalesRtnSettlement = e.NewValue as PartsSalesRtnSettlement;
            var partsSalesRtnSettlement = e.NewValue as PartsSalesRtnSettlementEx;
            if(partsSalesRtnSettlement == null)
                return;
            if(this.FilterItem == null) {
                this.FilterItem = new FilterItem {
                    MemberName = "PartsSalesRtnSettlementId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            }
            this.FilterItem.Value = partsSalesRtnSettlement.Id;
            this.ExecuteQueryDelayed();
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementDetail_SettlementPrice,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "QuantityToSettle",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["QuantityToSettle"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SettlementAmount"]).DataFormatString = "c2";
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesRtnSettlementDetails";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesRtnSettlementDetail);
            }
        }
    }
}
