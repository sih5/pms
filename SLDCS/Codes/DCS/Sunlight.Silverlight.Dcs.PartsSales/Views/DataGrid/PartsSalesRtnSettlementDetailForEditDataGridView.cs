﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesRtnSettlementDetailForEditDataGridView : DcsDataGridViewBase {

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {

            var partsSalesRtnSettlementDetail = e.Cell.DataContext as PartsSalesRtnSettlementDetail;
            if(partsSalesRtnSettlementDetail == null)
                return;
            switch(e.Cell.Column.UniqueName) {
                case "SettlementPrice":
                    partsSalesRtnSettlementDetail.SettlementAmount = partsSalesRtnSettlementDetail.SettlementPrice * partsSalesRtnSettlementDetail.QuantityToSettle;
                    break;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementDetail_SettlementPrice,
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "QuantityToSettle",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = false
                    }
                };

            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesRtnSettlementDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CellEditEnded -= GridView_CellEditEnded;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["QuantityToSettle"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SettlementAmount"]).DataFormatString = "c2";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesRtnSettlementDetails");
        }
    }
}
