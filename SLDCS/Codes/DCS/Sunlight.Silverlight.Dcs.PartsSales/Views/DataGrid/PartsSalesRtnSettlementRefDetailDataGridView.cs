﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesRtnSettlementRefDetailDataGridView : DcsDataGridViewBase {
        public PartsSalesRtnSettlementRefDetailDataGridView() {
            this.DataContextChanged += this.PartsSalesRtnSettlementRefDetailGridView_DataContextChanged;
        }

        private void PartsSalesRtnSettlementRefDetailGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            //var partsSalesRtnSettlement = e.NewValue as PartsSalesRtnSettlement;
            var partsSalesRtnSettlement = e.NewValue as PartsSalesRtnSettlementEx;
            if(partsSalesRtnSettlement == null)
                return;
            if(this.FilterItem == null) {
                this.FilterItem = new FilterItem {
                    MemberName = "PartsSalesRtnSettlementId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            }
            this.FilterItem.Value = partsSalesRtnSettlement.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SourceCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceCode
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseName
                    }, new ColumnItem {
                        Name = "SourceBillCreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SourceBillCreateTime
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SettlementAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ReturnReason",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnReason
                    },new ColumnItem {
                        Name = "PartsSalesReturnBillRemark",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_PartsSalesReturnBillRemark
                    }
                };
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {

            switch(parameterName) {
                case "partsSalesRtnSettlementId":
                    return this.FilterItem.Value;
            }

            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "GetPartsSalesRtnSettlementRefDetail";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["SourceBillCreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SettlementAmount"]).DataFormatString = "c2";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VietualPartsSalesRtnSettlementRef);
            }
        }
    }
}
