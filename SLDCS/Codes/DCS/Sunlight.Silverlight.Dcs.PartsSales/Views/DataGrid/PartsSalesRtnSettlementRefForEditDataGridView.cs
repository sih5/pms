﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesRtnSettlementRefForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesReturn_ReturnType"
        };

        public PartsSalesRtnSettlementRefForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var dataEditView = this.DataContext as PartsSalesRtnSettlementDataEditView;
            if(dataEditView == null)
                return;
            var partsSalesRtnSettlement = dataEditView.DataContext as PartsSalesRtnSettlement;
            if(partsSalesRtnSettlement == null)
                return;
            partsSalesRtnSettlement.TotalSettlementAmount = dataEditView.PartsSalesRtnSettlementRefs.Sum(item => item.SettlementAmount);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SourceCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "WarehouseName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SourceBillCreateTime",
                        IsReadOnly = true,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new KeyValuesColumnItem {
                        Name ="ReturnType",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ReturnedType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "ReturnReason",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ReturnReason,
                        IsReadOnly = true,
                    },new ColumnItem {
                        Name = "PartsSalesReturnBillRemark",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_PartsSalesReturnBillRemark,
                        IsReadOnly = true,
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            ((GridViewDataColumn)this.GridView.Columns["SourceBillCreateTime"]).DataFormatString = "d";
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesRtnSettlementRef);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesRtnSettlementRefs");
        }
    }
}
