﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesRtnSettlementWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchaseRtnSettleBill_InvoicePath", "PartsPurchaseSettleBill_SettlementPath", "PartsSalesRtnSettlement_Status","SalesOrderType_SettleType","SalesOrderType_BusinessType"
        };

        public PartsSalesRtnSettlementWithDetailsDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                    },
                    new ColumnItem {
                        Name = "InvoiceDate",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_InvoiceDate,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "Code",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesRtnSettlement_Code
                    },
                    new ColumnItem {
                        Name = "SalesCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlement_SalesCompanyName
                    },
                    new ColumnItem {
                        Name = "AccountGroupName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlement_AccountGroupName
                    },
                    new ColumnItem {
                        Name = "CustomerCompanyCode",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyCode
                    },
                    new ColumnItem {
                        Name = "CustomerCompanyName",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyName
                    },
                    //new ColumnItem {
                    //    Name = "TotalSettlementAmount",
                    //    MaskType = MaskType.Numeric,
                    //    TextAlignment = TextAlignment.Right
                    //},
                    new AggregateColumnItem {
                        Name = "TotalSettlementAmount",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_TotalSettlementAmount,
                        AggregateType = AggregateType.Sum ,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem{
                     Name = "PlannedPriceTotalAmount",
                     Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_PlannedPriceTotalAmount,
                     TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem{
                     Name = "Discount",
                     Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_Discount,
                     MaskType = MaskType.Numeric,
                     TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem{
                     Name = "NoTaxAmount",
                     Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_NoTaxAmount,
                     MaskType = MaskType.Numeric,
                     TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "TaxRate",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_TaxRate,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "Tax",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_Tax,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "InvoiceAmountDifference",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_InvoiceAmountDifference,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },                   
                    new KeyValuesColumnItem {
                        Name = "InvoicePath",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_InvoicePath,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    },
                    new KeyValuesColumnItem {
                        Name = "SettlementPath",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_SettlementPath,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                    },
                    new ColumnItem {
                        Name = "OffsettedSettlementBillCode",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_OffsettedSettlementBillCode
                    },
                    new ColumnItem {
                        Name = "Remark",
                        Title = PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                    },
                    new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName
                    },
                    new ColumnItem {
                        Name = "CreateTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },
                    new KeyValuesColumnItem {
                        Name = "SettleType",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_SettleType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]],
                    },
                    new KeyValuesColumnItem {
                        Name = "BusinessType",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_BusinessType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[4]],
                    },
                    new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlement_PartsSalesCategoryName,
                        Name = "PartsSalesCategoryName",
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSalesRtnSettlement", "PartsSalesRtnSettlementDetail", "PartsSalesRtnSettlementRef"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["TotalSettlementAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PlannedPriceTotalAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TaxRate"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["Tax"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["InvoiceAmountDifference"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["Discount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["NoTaxAmount"]).DataFormatString = "c2";
        }

        protected override string OnRequestQueryName() {
            return "根据业务类型查询配件销售退货结算单";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "businessCode":
                        var businessCode = filters.Filters.SingleOrDefault(item => item.MemberName == "BusinessCode");
                        return businessCode == null ? null : businessCode.Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "BusinessCode"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override Type EntityType {
            get {
                //return typeof(PartsSalesRtnSettlement);
                return typeof(PartsSalesRtnSettlementEx);
            }
        }
    }
}
