﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesSettleInvoiceDetailDataGridView : DcsDataGridViewBase {

        public PartsSalesSettleInvoiceDetailDataGridView() {
            this.DataContextChanged += PartsSalesSettleInvoiceDetailDataGridView_DataContextChanged;
        }

        private int type;
        private int invoiceId;
        private void PartsSalesSettleInvoiceDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var invoiceInformation = this.DataContext as VirtualInvoiceInformation;
            if(invoiceInformation == null)
                return;
            invoiceId = invoiceInformation.Id;
            type = invoiceInformation.SourceType;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem  {
                        Name = "SerialNumber"
                    }, new ColumnItem {
                        Name = "SalesSettleCode",
                        Title =PartsSalesUIStrings.DetailPanel_Text_PartsSalesSettlement_Code
                    }, new ColumnItem {
                        Name = "Type",
                          Title = PartsSalesUIStrings.DetailPanel_Title_PartsSalesSettlement_Type
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "invoiceId":
                    return invoiceId;
                case "type":
                    return type == 1 ? "配件销售结算单" : "配件销售退货结算单";
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override Type EntityType {
            get {
                return typeof(SalesSettleInvoiceResource);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询销售结算发票源单据";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.DataLoaded += GridView_DataLoaded;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        private void GridView_DataLoaded(object sender, EventArgs e) {
            var serialNumber = 1;
            foreach(var detail in this.GridView.Items.Cast<SalesSettleInvoiceResource>())
                detail.SerialNumber = serialNumber++;
        }
    }
}
