﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesSettlementDetailDataGridView : DcsDataGridViewBase {

        public PartsSalesSettlementDetailDataGridView() {
            this.DataContextChanged += PartsSalesSettlementDetailDataGridView_DataContextChanged;
        }

        private void PartsSalesSettlementDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesSettlement = e.NewValue as PartsSalesSettlement;
            if(partsSalesSettlement == null || partsSalesSettlement.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesSettlementId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsSalesSettlement.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "SparePart.OverseasPartsFigure",
                        Title = PartsSalesUIStrings.QueryPanel_Title_VirtualSparePartQuery_OverseasPartsFigure
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "QuantityToSettle",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesSettlementDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesSettlementDetailWithSparePart";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["QuantityToSettle"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SettlementAmount"]).DataFormatString = "c2";
        }
    }
}
