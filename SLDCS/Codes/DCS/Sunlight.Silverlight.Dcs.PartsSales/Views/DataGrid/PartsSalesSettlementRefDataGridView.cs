﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesSettlementRefDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesSettlementRef_SourceType"
        };

        public PartsSalesSettlementRefDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsSalesSettlementRefDataGridView_DataContextChanged;
        }

        private void PartsSalesSettlementRefDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesSettlement = e.NewValue as PartsSalesSettlement;
            if(partsSalesSettlement == null || partsSalesSettlement.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesSettlementId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsSalesSettlement.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SourceCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceCode,
                    }, new KeyValuesColumnItem {
                        Name = "SourceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceType,
                    },new ColumnItem {
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Name = "SourceBillCreateTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesSettlementRef);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSalesSettlementRefs";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["SourceBillCreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SettlementAmount"]).DataFormatString = "c2";
        }
    }
}
