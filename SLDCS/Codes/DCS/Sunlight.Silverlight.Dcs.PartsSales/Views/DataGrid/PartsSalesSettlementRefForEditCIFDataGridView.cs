﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesSettlementRefForEditCIFDataGridView : DcsDataGridViewBase {
        private QueryWindowBase outboundAndInboundBillQueryWindow;
        private RadWindow radWindow;
        private readonly string[] kvNames = {
            "PartsSalesSettlementRef_SourceType","ERPInvoiceInformation_Type","PartsSalesReturn_ReturnType"
        };

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PartsSalesSettlementRefForEditCIFDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            this.SetSerialNumber();
        }
        public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            if(subject == "Red") {
                this.GridView.RowStyleSelector = this.GridViewStyleSelector(contents);
                this.GridView.Rebind();
            }
            if(subject == "ReturnComplete") {
                this.GridView.RowStyleSelector = this.GridViewStyleSelectorForReturnComplete(contents);
                this.GridView.Rebind();
            }
            return null;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null || partsSalesSettlement.CustomerCompanyId == default(int) || partsSalesSettlement.AccountGroupId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_PartsSalesSettlement_CustomerCompanyIdOrAccountGroupIdIsNull);
                return;
            }
            this.SelectedWindow.ShowDialog();
        }

        private RadWindow SelectedWindow {
            get {
                if(radWindow == null) {
                    radWindow = new RadWindow();
                    radWindow.Header = PartsSalesUIStrings.QueryPanel_Title_OutboundAndInboundBill;
                    radWindow.Content = this.OutboundAndInboundBillQueryWindow;
                    radWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                }
                return radWindow;
            }
        }

        private QueryWindowBase OutboundAndInboundBillQueryWindow {
            get {
                if(this.outboundAndInboundBillQueryWindow == null) {
                    this.outboundAndInboundBillQueryWindow = DI.GetQueryWindow("OutboundAndInboundBillCIF");
                    this.outboundAndInboundBillQueryWindow.Loaded += this.OutboundAndInboundBillQueryWindow_Loaded;
                    this.outboundAndInboundBillQueryWindow.SelectionDecided += this.OutboundAndInboundBillQueryWindow_SelectionDecided;
                }
                return this.outboundAndInboundBillQueryWindow;
            }
        }

        private void OutboundAndInboundBillQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var outboundAndInboundBill = queryWindow.SelectedEntities.Cast<OutboundAndInboundBill>().FirstOrDefault();
            if(outboundAndInboundBill == null)
                return;

            if(partsSalesSettlement.PartsSalesSettlementRefs.Any(r => r.SourceId == outboundAndInboundBill.BillId)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_PartsSalesSettlementRef_SourceCodeIsExist);
                return;
            }

            var partsSalesSettlementRef = new PartsSalesSettlementRef();
            partsSalesSettlement.PartsSalesSettlementRefs.Add(partsSalesSettlementRef);
            partsSalesSettlementRef.SerialNumber = partsSalesSettlement.PartsSalesSettlementRefs.Any() ? partsSalesSettlement.PartsSalesSettlementRefs.Max(entity => entity.SerialNumber) + 1 : 1;
            partsSalesSettlementRef.PartsSalesSettlementId = partsSalesSettlement.Id;
            partsSalesSettlementRef.SourceId = outboundAndInboundBill.BillId;
            partsSalesSettlementRef.SourceBillCreateTime = outboundAndInboundBill.BillCreateTime;
            partsSalesSettlementRef.SourceCode = outboundAndInboundBill.BillCode;
            partsSalesSettlementRef.SourceType = outboundAndInboundBill.BillType;
            partsSalesSettlementRef.InvoiceType = outboundAndInboundBill.InvoiceType;
            partsSalesSettlementRef.ERPSourceOrderCode = outboundAndInboundBill.ERPOrderCode;
            partsSalesSettlementRef.ReturnType = outboundAndInboundBill.ReturnType;
            partsSalesSettlementRef.PartsPurchaseSettleCode = outboundAndInboundBill.PartsPurchaseSettleCode;
            if (string.IsNullOrEmpty(partsSalesSettlementRef.PartsPurchaseSettleCode))
            {
                partsSalesSettlementRef.DealerIsSettlement = "否";
            }
            else
            {
                partsSalesSettlementRef.DealerIsSettlement = "是";
            }
            if (outboundAndInboundBill.PartsSalesCategoryName == PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_WarehouseCarNew) {
                partsSalesSettlementRef.InvoiceCustomerName = outboundAndInboundBill.InvoiceCustomerName;
                partsSalesSettlementRef.Taxpayeridentification = outboundAndInboundBill.Taxpayeridentification;
            } else {
                partsSalesSettlementRef.InvoiceCustomerName = "";
                partsSalesSettlementRef.Taxpayeridentification = "";
            }
            if(partsSalesSettlementRef.SourceType != (int)DcsPartsSalesSettlementRefSourceType.配件出库单)
                partsSalesSettlementRef.SettlementAmount = (-outboundAndInboundBill.SettlementAmount);
            else
                partsSalesSettlementRef.SettlementAmount = outboundAndInboundBill.SettlementAmount;
            partsSalesSettlement.TotalSettlementAmount = partsSalesSettlement.PartsSalesSettlementRefs.Sum(entity => entity.SettlementAmount);

            this.dcsDomainContext.Load(this.dcsDomainContext.GetWarehousesOrderByNameQuery().Where(r => r.Id == outboundAndInboundBill.WarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    partsSalesSettlementRef.WarehouseId = loadOp.Entities.First().Id;
                    partsSalesSettlementRef.WarehouseName = loadOp.Entities.First().Name;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void OutboundAndInboundBillQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            var queryWindow = sender as QueryWindowBase;
            if(partsSalesSettlement == null || queryWindow == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            //compositeFilterItem.Filters.Add(new FilterItem("CompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            compositeFilterItem.Filters.Add(new FilterItem("CustomerCompanyId", typeof(int), FilterOperator.IsEqualTo, partsSalesSettlement.CustomerCompanyId));
            compositeFilterItem.Filters.Add(new FilterItem("CustomerAccountId", typeof(int), FilterOperator.IsEqualTo, partsSalesSettlement.CustomerAccountId));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void SetSerialNumber() {
            var partsSalesSettlement = this.DataContext as PartsSalesSettlement;
            if(partsSalesSettlement == null)
                return;
            var serialNumber = 1;
            foreach(var partsSalesSettlementRef in partsSalesSettlement.PartsSalesSettlementRefs) {
                partsSalesSettlementRef.SerialNumber = serialNumber;
                serialNumber++;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SourceCode",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "SourceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "InvoiceCustomerName",
                        Title="发票抬头",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Taxpayeridentification",
                        Title="纳税人识别号",
                        IsReadOnly = true
                    },new KeyValuesColumnItem {
                        Name = "InvoiceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title="开票类型",
                        IsReadOnly = true
                     }, new ColumnItem {
                        Name = "ERPSourceOrderCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_PlatForm_Code,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "WarehouseName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SourceBillCreateTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime,
                        IsReadOnly = true,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        Name = "ReturnType",
                        IsReadOnly = true,
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesReturnBill_ReturnedType
                    }, new ColumnItem {
                        Name = "DealerIsSettlement",
                        IsReadOnly = true,
                        Title = "供应商是否已结算"
                    }, new ColumnItem {
                        Name = "PartsPurchaseSettleCode",
                        IsReadOnly = true,
                        Title = "采购结算单号"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesSettlementRef);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserInsertRows = true;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.Loaded -= GridView_Loaded;
            this.GridView.Loaded += GridView_Loaded;
            ((GridViewDataColumn)this.GridView.Columns["SettlementAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SourceBillCreateTime"]).DataFormatString = "d";
        }

        //查询的时候清除gridview的样式
        public void ClearGridViewRowStyleSelector() {
            this.GridView.RowStyleSelector = null;
        }

        private void GridView_Loaded(object sender, RoutedEventArgs e) {
            this.GridView.RowStyleSelector = null;
        }

        private GridViewStyleSelector GridViewStyleSelector(params object[] contents) {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is PartsSalesSettlementRef) {
                        var partsSalesSettlementRef = item as PartsSalesSettlementRef;
                        if(partsSalesSettlementRef.SourceType == (int)DcsPartsSalesSettlementRefSourceType.配件出库单 && contents.Contains(partsSalesSettlementRef.SourceCode.ToLower()))
                            return this.Resources["ErrorDataBackground"] as Style;
                    }
                    return null;
                }
            };
        }

        private GridViewStyleSelector GridViewStyleSelectorForReturnComplete(params object[] contents) {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is PartsSalesSettlementRef) {
                        var partsSalesSettlementRef = item as PartsSalesSettlementRef;
                        if(contents.Contains(partsSalesSettlementRef.SourceCode.ToLower()))
                            return this.Resources["ErrorDataBackground"] as Style;
                    }
                    return null;
                }
            };
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSalesSettlementRefs");
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }
    }
}
