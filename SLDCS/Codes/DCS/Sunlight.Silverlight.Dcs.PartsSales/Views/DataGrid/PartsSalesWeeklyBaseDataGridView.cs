﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSalesWeeklyBaseDataGridView : DcsDataGridViewBase {     
        public PartsSalesWeeklyBaseDataGridView() {
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                     new ColumnItem{
                        Name = "Year",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesWeeklyBase_Year
                    },new ColumnItem{
                        Name = "Week",
                        Title=PartsSalesUIStrings.DataGridView_Title_CenterABCBasis_WeekNum
                    },new ColumnItem{
                        Name = "WarehouseName",
                        Title=PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_WarehouseId
                    }, new ColumnItem{
                        Name = "PartsSalesOrderTypeName",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrder_PartsSalesOrderTypeName
                    }, new ColumnItem{
                        Name = "SparePartCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_OverstockPartsRecommendBill_SparePartCode
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Name
                    }, new ColumnItem{
                        Name = "Quantity",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_Quantity
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesWeeklyBase_CreateTime
                    }
                };
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }     
        protected override Type EntityType {
            get {
                return typeof(PartsSalesWeeklyBase);
            }
        }

        protected override string OnRequestQueryName() {
            return "QueryPartsSalesWeeklyBasesForCenter";
        }
        protected override void OnControlsCreated() {
            this.DataPager.PageSize = 100;          
        }
    }
}