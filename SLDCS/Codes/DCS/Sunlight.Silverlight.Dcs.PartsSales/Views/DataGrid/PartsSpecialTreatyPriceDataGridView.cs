﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSpecialTreatyPriceDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "BaseData_Status"
        };

        public PartsSpecialTreatyPriceDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSpecialTreatyPrice);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Company.Code",
                        IsDefaultGroup = true
                    }, new ColumnItem {
                        Name = "Company.Name"
                    }, new ColumnItem {
                        Name = "SparePart.Code"
                    }, new ColumnItem {
                        Name = "SparePart.Name"
                    },
                    new ColumnItem {
                        Name="ValidationTime",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ValidationTime
                    },
                    new ColumnItem {
                        Name="ExpireTime",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ExpireTime
                    },
                    new ColumnItem {
                        Name = "TreatyPrice",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    },
                     new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSpecialTreatyPriceHistory"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSpecialTreatyPricesWithCompany";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["TreatyPrice"]).DataFormatString = "c2";
        }
    }
}
