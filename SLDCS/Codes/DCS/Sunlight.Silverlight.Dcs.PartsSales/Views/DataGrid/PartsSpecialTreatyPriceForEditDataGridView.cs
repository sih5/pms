﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSpecialTreatyPriceForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase sparePartDropDownQueryWindow;
        private DcsMultiPopupsQueryWindowBase sparePartMulitQueryWindow;
        private RadWindow selectRadWindow;



        private QueryWindowBase SparePartDropDownQueryWindow {
            get {
                if(this.sparePartDropDownQueryWindow == null) {
                    this.sparePartDropDownQueryWindow = DI.GetQueryWindow("SparePartDropDown");
                    this.sparePartDropDownQueryWindow.SelectionDecided += this.SparePartDropDownQueryWindow_SelectionDecided;
                }
                return this.sparePartDropDownQueryWindow;
            }
        }

        public DcsMultiPopupsQueryWindowBase SparePartQueryWindow {
            get {
                if(this.sparePartMulitQueryWindow == null) {
                    this.sparePartMulitQueryWindow = DI.GetQueryWindow("SparePartMultiSelect") as DcsMultiPopupsQueryWindowBase;
                    this.sparePartMulitQueryWindow.SelectionDecided += SparePartMulitQueryWindow_SelectionDecided;
                }
                return this.sparePartMulitQueryWindow;
            }
        }


        private void SparePartDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var partsSpecialTreatyPrice = queryWindow.DataContext as PartsSpecialTreatyPrice;
            if(partsSpecialTreatyPrice == null)
                return;
            partsSpecialTreatyPrice.SparePartId = sparePart.Id;
            partsSpecialTreatyPrice.SparePartCode = sparePart.Code;
            partsSpecialTreatyPrice.SparePartName = sparePart.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }


        private void SparePartMulitQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>();
            if(sparePart == null)
                return;
            var dataContext = this.DataContext as PartsSpecialTreatyPriceForAddDataEditView;
            if(dataContext == null)
                return;

            var partsSpecialTreatyPrices = dataContext.PartsSpecialTreatyPrices;

            if(partsSpecialTreatyPrices.Any(r => sparePart.Any(item => item.Code == r.SparePartCode))) {
                UIHelper.ShowNotification("已存在相同编号的配件");
                return;
            }
            foreach(var spare in sparePart) {
                var partsSpecialTreatyPrice = new PartsSpecialTreatyPrice {
                    PartsSalesCategoryCode = "Empty",
                    PartsSalesCategoryName = "Empty",
                    Status = (int)DcsBaseDataStatus.有效,
                    SparePartId = spare.Id,
                    SparePartCode = spare.Code,
                    SparePartName = spare.Name,

                };
                partsSpecialTreatyPrices.Add(partsSpecialTreatyPrice);
            }

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }


        public RadWindow SelectRadWindow {
            get {
                if(selectRadWindow == null) {
                    selectRadWindow = new RadWindow();
                    selectRadWindow.Header = PartsSalesUIStrings.QueryPanel_Title_SparePart;
                    selectRadWindow.Content = this.SparePartQueryWindow;
                    selectRadWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                }
                return this.selectRadWindow;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            this.SelectRadWindow.ShowDialog();
        }


        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsSpecialTreatyPrices");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new DropDownTextBoxColumnItem {
                        Name = "SparePartCode",
                        DropDownContent = this.SparePartDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "TreatyPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSpecialTreatyPrice);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            ((GridViewDataColumn)this.GridView.Columns["TreatyPrice"]).DataFormatString = "c2";
        }



        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }


    }
}
