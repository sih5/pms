﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSpecialTreatyPriceForImportForEditDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Company.Code"
                    },
                    new ColumnItem {
                        Name = "Company.Name"
                    },
                    new ColumnItem {
                        Name = "SparePartCode"
                    },
                    new ColumnItem {
                        Name = "SparePartName"
                    },
                    new ColumnItem {
                        Name = "TreatyPrice",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.Numeric
                    },
                    new ColumnItem {
                        Name = "Remark"
                    },
                    new ColumnItem {
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSpecialTreatyPrice);
            }
        }
    }
}
