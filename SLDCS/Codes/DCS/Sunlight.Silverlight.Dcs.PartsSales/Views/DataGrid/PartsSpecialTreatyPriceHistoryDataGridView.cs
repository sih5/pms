﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsSpecialTreatyPriceHistoryDataGridView : DcsDataGridViewBase {
        public PartsSpecialTreatyPriceHistoryDataGridView() {
            this.DataContextChanged += this.PartsSalesPriceHistoryDataGridView_DataContextChanged;
        }

        private void PartsSalesPriceHistoryDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSpecialTreatyPrice = e.NewValue as PartsSpecialTreatyPrice;
            if(partsSpecialTreatyPrice == null || partsSpecialTreatyPrice.Id == default(int))
                return;
            var compositeFilter = new CompositeFilterItem();
            compositeFilter.Filters.Add(new FilterItem {
                MemberName = "SparePartId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsSpecialTreatyPrice.SparePartId
            });
            compositeFilter.Filters.Add(new FilterItem {
                MemberName = "PartsSalesCategoryId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsSpecialTreatyPrice.PartsSalesCategoryId
            });
            compositeFilter.Filters.Add(new FilterItem {
                MemberName = "PartsSpecialTreatyPriceId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsSpecialTreatyPrice.Id
            });
            this.FilterItem = compositeFilter;
            this.ExecuteQueryDelayed();
        }
        protected override Type EntityType {
            get {
                return typeof(PartsSpecialPriceHistory);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Company.Code",
                        IsDefaultGroup = true
                    }, new ColumnItem {
                        Name = "Company.Name"
                    }, new ColumnItem {
                        Name = "SparePart.Code",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePart.Name",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem {
                        Name = "TreatyPrice",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        Title=PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_AgreementPrice
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsSpecialPriceHistoryWithCompanies";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }

    }
}
