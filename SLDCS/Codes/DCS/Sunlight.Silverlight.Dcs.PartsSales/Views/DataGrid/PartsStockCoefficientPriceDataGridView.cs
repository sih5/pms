﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsStockCoefficientPriceDataGridView : DcsDataGridViewBase {
        public PartsStockCoefficientPriceDataGridView() {
            this.DataContextChanged += PartsStockCoefficientPriceDataGridView_DataContextChanged;
        }
        private void PartsStockCoefficientPriceDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsStockCoefficient = e.NewValue as PartsStockCoefficient;
            if(partsStockCoefficient == null)
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartsStockCoefficientId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int)
                };
            this.FilterItem.Value = partsStockCoefficient.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "MinPrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficientPrice_MinPrice
                    }, new ColumnItem{
                        Name = "MaxPrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficientPrice_MaxPrice
                    }, new ColumnItem{
                        Name = "Coefficient",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_Common_Coefficient
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsStockCoefficientPrice);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsStockCoefficientPrices";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
