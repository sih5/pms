﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsStockCoefficientPriceForEditDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(PartsStockCoefficientPrice);
            }
        }
        protected override bool UsePaging {
            get {
                return false;
            }
        }
        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="MinPrice",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficientPrice_MinPrice,
                    }, new ColumnItem{
                        Name = "MaxPrice",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficientPrice_MaxPrice
                    }, new ColumnItem{
                        Name = "Coefficient",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_Common_Coefficient
                    }
                };
            }
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsStockCoefficientPrices");
        }

        //private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
        //    var partsStockCoefficient = this.DataContext as PartsStockCoefficient;
        //    if(partsStockCoefficient == null)
        //        return;
        //    var detail = new PartsStockCoefficientPrice {
        //        MinPrice = null,
        //        Coefficient = null,
        //        MaxPrice =null,
              
        //    };
        //    partsStockCoefficient.PartsStockCoefficientPrices.Add(detail);
        //}

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            //this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            //this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

    }
}
