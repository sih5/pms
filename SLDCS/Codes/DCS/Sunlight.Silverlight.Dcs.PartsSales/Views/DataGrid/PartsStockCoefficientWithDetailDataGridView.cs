﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;


namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PartsStockCoefficientWithDetailDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "PartsStockCoefficientType", "ABCSetting_Type", "BaseData_Status"
        };
        public PartsStockCoefficientWithDetailDataGridView() {
           this.KeyValueManager.Register(this.KvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficient_Type
                    },  new KeyValuesColumnItem {
                        Name = "PRODUCTTYPE",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]],
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsStockCoefficient_ProductType
                    },  new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[2]],
                        Title=PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime",
                        TextAlignment = System.Windows.TextAlignment.Right,
                        MaskType = MaskType.DateTime
                    }                   
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPartsStockCoefficients";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                         "PartsStockCoefficientPrice"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
        }
        protected override Type EntityType {
            get {
                return typeof(PartsStockCoefficient);
            }
        }
    }
}
