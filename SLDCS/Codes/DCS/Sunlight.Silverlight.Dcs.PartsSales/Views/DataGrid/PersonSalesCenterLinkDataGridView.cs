﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PersonSalesCenterLinkDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status","PersonType"
        };
        public PersonSalesCenterLinkDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_Name,
                        Name = "Personnel.Name"
                    }, new KeyValuesColumnItem {
                        Name = "PersonType",
                       KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_CellNumber,
                        Name = "Personnel.CellNumber"
                    }, new ColumnItem {
                        Name = "Personnel.CorporationName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_CorporationName
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    },new ColumnItem {
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_Name,
                        Name = "PartsSalesCategory.Name"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
        protected override string OnRequestQueryName() {
            return "GetPersonSalesCenterLinkWithPersonnel";
        }
        protected override Type EntityType {
            get {
                return typeof(PersonSalesCenterLink);
            }
        }
    }
}
