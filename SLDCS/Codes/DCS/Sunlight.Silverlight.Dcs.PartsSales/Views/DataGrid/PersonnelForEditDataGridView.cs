﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PersonnelForEditDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "PersonType"
        };
        public PersonnelForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        private RadWindow radQueryWindow;
        private DcsMultiPopupsQueryWindowBase personnelQueryWindow;
        private DcsMultiPopupsQueryWindowBase PersonnelQueryWindow {
            get {
                if(this.personnelQueryWindow == null) {
                    this.personnelQueryWindow = DI.GetQueryWindow("PersonnelForMultiSelect") as DcsMultiPopupsQueryWindowBase;
                    this.personnelQueryWindow.SelectionDecided += personnelQueryWindow_SelectionDecided;
                    this.personnelQueryWindow.Loaded += personnelQueryWindow_Loaded;
                }
                return this.personnelQueryWindow;
            }
        }

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = PersonnelQueryWindow,
                    Header = PartsSalesUIStrings.QueryPanel_Title_PersonnelDropDownQueryWindow,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private void personnelQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var personnel = queryWindow.SelectedEntities.Cast<Personnel>();
            if(personnel == null)
                return;
            var dataEditView = this.DataContext as PersonSalesCenterLinkDataEditView;
            if(dataEditView == null)
                return;
            if(dataEditView.PersonSalesCenterLinks.Any(ex => personnel.Any(r => r.Id == ex.PersonId))) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Error_SparePart_PersonnelName);
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            var maxSerialNumber = dataEditView.PersonSalesCenterLinks.Count;
            foreach(var item in personnel) {
                domainContext.Load(domainContext.GetPersonnelsQuery().Where(d => d.Id == item.Id), loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var personnels = loadOp.Entities.SingleOrDefault();
                    maxSerialNumber++;
                    if(personnels != null) {
                        personnels.SequeueNumber = maxSerialNumber;
                        dataEditView.PersonSalesCenterLinks.Add(new PersonSalesCenterLink {
                            PersonId = item.Id,
                            Status = (int)DcsBaseDataStatus.有效,
                            Personnel = personnels
                        });
                    }

                }, null);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        private void personnelQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            var filterItem = new FilterItem();
            filterItem.MemberName = "CorporationId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = BaseApp.Current.CurrentUserData.EnterpriseId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "Personnel.SequeueNumber",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_SequeueNumber,
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "Personnel.Name",
                          IsReadOnly = true
                    }, new ColumnItem{
                        Name = "Personnel.CellNumber",
                         IsReadOnly = true
                    }, new ColumnItem{
                        Name = "Personnel.CorporationName",
                         IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "PersonType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }
        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PersonSalesCenterLinks");
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }
        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var dataEditView = this.DataContext as PersonSalesCenterLinkDataEditView;
            if(dataEditView == null)
                return;
            var serialNumber = 1;
            foreach(var personSalesCenterLink in dataEditView.PersonSalesCenterLinks) {
                personSalesCenterLink.Personnel.SequeueNumber = serialNumber;
                serialNumber++;
            }
            foreach(var personSalesCenterLink in e.Items.Cast<PersonSalesCenterLink>())
                if(personSalesCenterLink.Can删除人员与销售中心关系)
                    personSalesCenterLink.删除人员与销售中心关系();
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            RadQueryWindow.ShowDialog();
        }
        protected override Type EntityType {
            get {
                return typeof(PersonSalesCenterLink);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
