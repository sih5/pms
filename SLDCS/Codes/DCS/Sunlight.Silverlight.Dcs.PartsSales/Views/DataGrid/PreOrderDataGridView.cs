﻿
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class PreOrderDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PreOrderStatus"
        };

        public PreOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(PreOrder);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "PartCode"
                    }, new ColumnItem {
                        Name = "PartName"
                    }, new ColumnItem {
                        Name = "Qty",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Qty
                    },new ColumnItem {
                        Name = "PartsSalesCategoryCode"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetPreOrdersByStatus";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
