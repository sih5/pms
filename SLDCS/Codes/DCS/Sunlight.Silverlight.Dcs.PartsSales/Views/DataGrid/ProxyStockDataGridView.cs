﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class ProxyStockDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(VirtualPartsSalesPriceWithStock);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "AgencyCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_AgencyCode
                    }, new ColumnItem {
                        Name = "AgencyName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_AgencyName
                    }, new ColumnItem {
                        Name = "WarehouseCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseCode
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseName
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },new ColumnItem {
                        Name = "StockQuantity",
                        Title=PartsSalesUIStrings.DataGridView_Validation_VirtualPartsSalesPriceWithStock_StockQuantity
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "根据销售类型查询配件销售价带库存";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var cFilterItem = compositeFilterItem.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(cFilterItem == null)
                    return null;
                FilterItem filter;
                switch(parameterName) {
                    case "partsSalesCategoryId":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesCategoryId");
                        return filter == null ? 0 : filter.Value;
                    case "partsSalesOrderTypeId":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesOrderTypeId");
                        return filter == null ? 0 : filter.Value;
                    case "customerId":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "CustomerId");
                        return filter == null ? 0 : filter.Value;
                    case "warehouseId":
                        filter = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "WarehouseId");
                        return filter == null ? 0 : filter.Value;
                }
            }
            return null;
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach(var filter in compositeFilterItem.Filters.
                Where(item => (item.GetType() != typeof(CompositeFilterItem) ||
                              !((CompositeFilterItem)item).Filters.Any(cItem => (new string[] { "PartsSalesCategoryId", "PartsSalesOrderTypeId", "CustomerId", "WarehouseId" }).Contains(cItem.MemberName)))
                    ))
                newCompositeFilterItem.Filters.Add(filter);
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
