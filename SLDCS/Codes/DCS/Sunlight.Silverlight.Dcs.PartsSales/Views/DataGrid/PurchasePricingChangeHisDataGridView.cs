﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid
{
    public class PurchasePricingChangeHisDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };
        public PurchasePricingChangeHisDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualPurchasePricingChangeHi);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        Title=PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode,
                        Name = "SparePartCode"
                    }, new ColumnItem {
                         Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                        Name = "SparePartName"
                    }, new ColumnItem {
                         Title=PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_SihCode,
                        Name = "ReferenceCode"
                    },new ColumnItem {
                         Title=PartsSalesUIStrings.QueryPanel_Title_CustomerDirectSpareList_IsPrimary,
                        Name = "IsPrimary"
                    }, new ColumnItem {
                        Title=PartsSalesUIStrings.DataGridView_Title_VirtualPurchasePricingChangeHi_OldPurchasePrice,
                        Name = "OldPurchasePrice"
                    }, new ColumnItem {
                        Title=PartsSalesUIStrings.DataGridView_Title_VirtualPurchasePricingChangeHi_PurchasePrice,
                        Name = "PurchasePrice"
                    }, new ColumnItem {
                        Name = "ChangeRatio",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPurchasePricingChangeHi_ChangeRatio
                    }, new ColumnItem {
                        Name = "CenterPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Centerprice
                    }, new ColumnItem {
                        Name = "DistributionPrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsSalesPrice_SalesPrice
                    }, new ColumnItem {
                        Name = "RetailPrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsSalesPrice_RetailGuidePrice
                    }, new ColumnItem {
                        Name = "MaintenanceTime",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPurchasePricingChangeHi_MaintenanceTime
                    }, new ColumnItem {
                        Name = "ValidFrom",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ValidationTime
                    }, new ColumnItem {
                        Name = "ValidTo",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ExpireTime
                    }, new ColumnItem {
                        Name = "IncreaseRateGroupId",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesPriceChange_PriceType
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifyTime
                    },new ColumnItem {
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }
        }
        protected override string OnRequestQueryName()
        {
            return "查询采购价格变动情况记录";
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 20;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            ((GridViewDataColumn)this.GridView.Columns["ChangeRatio"]).DataFormatString = "p"; 
        }
    }
}
