﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class RetailerDeliveryDetailDataGridView : DcsDataGridViewBase {
        protected override string OnRequestQueryName() {
            return "GetRetailer_DeliveryDetailWithSparePart";
        }

        public RetailerDeliveryDetailDataGridView() {
            this.DataContextChanged += RetailerDeliveryDetailDataGridView_DataContextChanged;
        }

        private void RetailerDeliveryDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var retailerDelivery = e.NewValue as Retailer_Delivery;
            if(retailerDelivery == null || retailerDelivery.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "DeliveryId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = retailerDelivery.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "Sku_Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    }, new ColumnItem{
                        Name = "Item_Name",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Name
                    }, new ColumnItem{
                        Name = "Qty",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Qty
                    }, new ColumnItem{
                        Name = "ErrorMessage",
                        Title = PartsSalesUIStrings.DataGridView_Title_ERPDeliveryDetailVirtual_ErrorMessage
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(ERPDeliveryDetailVirtual);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }


        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
