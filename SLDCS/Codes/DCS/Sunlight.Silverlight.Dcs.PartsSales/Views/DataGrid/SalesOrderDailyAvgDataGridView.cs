﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SalesOrderDailyAvgDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
          "ABCSetting_Type"
        };
        public SalesOrderDailyAvgDataGridView() {
           this.KeyValueManager.Register(this.KvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Year",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesWeeklyBase_Year
                    },new ColumnItem {
                        Name = "CreateTime",
                         Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesWeeklyBase_CreateTime
                    },new ColumnItem {
                        Name = "WarehouseName",
                         Title=PartsSalesUIStrings.QueryPanel_Title_PartsSalesQueryOnWay_WarehouseId
                    },new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                         Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_Type
                    },new ColumnItem {
                        Name = "OrderCycle",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_OrderCycle
                    },new ColumnItem {
                        Name = "ArrivalCycle",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ArriveCycle
                    },  new ColumnItem {
                        Name = "SafeStockDays",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_SafeStockDays
                    },  new ColumnItem {
                        Name = "ReserveCoefficient",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ReserveCoefficient
                    },new ColumnItem {
                        Name = "StockUpperCoefficient",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_StockUpperCoefficient
                    },new ColumnItem {
                        Name = "PurchasePrice",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_PurchasePrice               
                    },new ColumnItem {
                        Name = "PackingQty",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_PackingCoefficient
                    },new ColumnItem {
                        Name = "UsableQty",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_UsableQuantity
                    } ,new ColumnItem {
                        Name = "OnLineQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_OnLineQty
                    },new ColumnItem{
                        Name="OweQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_OweQty
                    },new ColumnItem{
                        Name="SparePartCode",
                        Title=PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartCode
                    },new ColumnItem{
                        Name="SparePartName",
                        Title=PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartName
                    },new ColumnItem{
                        Name="T3WeekAvgQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_T3WeekAvgQty
                    },new ColumnItem{
                        Name="T2WeekAvgQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_T2WeekAvgQty
                    },new ColumnItem{
                        Name="T1WeekAvgQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_T1WeekAvgQty
                    },new ColumnItem{
                        Name="TWeekAvgQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_TWeekAvgQty
                    },new ColumnItem{
                        Name="TWeekActualAvgQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_TWeekActualAvgQty
                    },new ColumnItem{
                        Name="StandardQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_StandardQty
                    }                   
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "SalesOrderDailyAvgByCenter";
        }
    
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.DataPager.PageSize = 50;
        }
        protected override Type EntityType {
            get {
                return typeof(SalesOrderDailyAvg);
            }
        }
    }
}