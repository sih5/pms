﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SalesOrderRecommendForceDataGridView  : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
          "ABCSetting_Type"
        };
        public SalesOrderRecommendForceDataGridView() {
           this.KeyValueManager.Register(this.KvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Year",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesWeeklyBase_Year
                    },new ColumnItem {
                        Name = "CreateTime",
                         Title=PartsSalesUIStrings.DataGridView_Title_PartsSalesWeeklyBase_CreateTime
                    },new ColumnItem {
                        Name = "CenterName",
                         Title=PartsSalesUIStrings.QueryPanel_QueeryItem_Title_Company_Name
                    },new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                         Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_Type
                    },new ColumnItem {
                        Name = "ForceReserveQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ForceReserveQty
                    },new ColumnItem {
                        Name = "StockUpperQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_StockUpperQtys
                    },new ColumnItem {
                        Name = "ActualStockUpperQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_ActualStockUpperQty
                    },new ColumnItem {
                        Name = "PackingQty",
                        Title=PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_PackingCoefficient
                    },new ColumnItem {
                        Name = "UsableQty",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_UsableQuantity
                    } ,new ColumnItem {
                        Name = "OnLineQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_OnLineQty
                    },new ColumnItem{
                        Name="OweQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_OweQty
                    },new ColumnItem{
                        Name="SparePartCode",
                        Title=PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartCode
                    },new ColumnItem{
                        Name="SparePartName",
                        Title=PartsSalesUIStrings.QueryPanel_QueryItem_Tilte_VirtualPartsSalesPriceHistory_SparePartName
                    },new ColumnItem{
                        Name="RecommendQty",
                        Title=PartsSalesUIStrings.DataGridView_SalesOrderDailyAvg_RecommendQty
                    }                   
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "SalesOrderRecommendForceByCenter";
        }
    
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.DataPager.PageSize = 50;
        }
        protected override Type EntityType {
            get {
                return typeof(SalesOrderRecommendForce);
            }
        }
    }
}