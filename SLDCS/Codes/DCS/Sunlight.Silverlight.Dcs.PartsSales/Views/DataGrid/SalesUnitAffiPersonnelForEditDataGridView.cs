﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SalesUnitAffiPersonnelForEditDataGridView : DcsDataGridViewBase {
        private RadWindow selectRadWindow;
        private RadWindow SelectRadWindow {
            get {
                if(selectRadWindow == null) {
                    selectRadWindow = new RadWindow();
                    selectRadWindow.Header = PartsSalesUIStrings.QueryPanel_Title_Personal;
                    selectRadWindow.Content = this.PersonnelDropDownQueryWindow;
                    selectRadWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;
                }
                return this.selectRadWindow;
            }
        }
        private QueryWindowBase personnelDropDownQueryWindow;

        private QueryWindowBase PersonnelDropDownQueryWindow {
            get {
                if(this.personnelDropDownQueryWindow == null) {
                    this.personnelDropDownQueryWindow = DI.GetQueryWindow("PersonnelForSalesUnit");
                    this.personnelDropDownQueryWindow.SelectionDecided += this.PersonnelDropDownQueryWindow_SelectionDecided;
                    this.personnelDropDownQueryWindow.Loaded += this.PersonnelDropDownQueryWindow_Loaded;
                }
                return this.personnelDropDownQueryWindow;
            }
        }

        private void PersonnelDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null)
                return;
            var salesUnit = this.DataContext as SalesUnit;
            var filterItem = new FilterItem();
            filterItem.MemberName = "CorporationId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            if(salesUnit != null)
                filterItem.Value = salesUnit.OwnerCompanyId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("Status", typeof(int), FilterOperator.IsEqualTo, (int)DcsBaseDataStatus.有效));
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void PersonnelDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var personnel = queryWindow.SelectedEntities.Cast<Personnel>().SingleOrDefault();
            if(personnel == null)
                return;
            var salesUnit = this.DataContext as SalesUnit;
            if(salesUnit == null)
                return;

            if(salesUnit.SalesUnitAffiPersonnels.Any(r => r.PersonnelId == personnel.Id)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonnel_PersonnelId, personnel.Name));
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            DcsUtils.Confirm(PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonnel_IsAdd, () => domainContext.Load(domainContext.GetPersonnelsQuery().Where(entity => entity.Id == personnel.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    var salesUnitAffiPersonnel = new SalesUnitAffiPersonnel();
                    salesUnitAffiPersonnel.PersonnelId = loadOp.Entities.First().Id;
                    salesUnitAffiPersonnel.SalesUnitId = salesUnit.Id;
                    salesUnitAffiPersonnel.Personnel = loadOp.Entities.First();

                    domainContext.SalesUnitAffiPersonnels.Add(salesUnitAffiPersonnel);

                    this.DomainContext.SubmitChanges(submitOp => {
                        var parent = queryWindow.ParentOfType<RadWindow>();
                        if(parent != null)
                            parent.Close();
                        if(submitOp.HasError) {
                            if(!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                            this.DomainContext.RejectChanges();
                            return;
                        }
                        salesUnit.SalesUnitAffiPersonnels.Add(salesUnitAffiPersonnel);
                        if(parent != null)
                            parent.Close();
                    }, null);
                }
            }, null));
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Personnel.LoginId",
                        //DropDownContent = this.PersonnelDropDownQueryWindow,
                        IsReadOnly = true

                    }, new ColumnItem {
                        Name = "Personnel.Name",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Personnel.CellNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Personnel.CorporationName",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SalesUnitAffiPersonnel);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleting -= GridView_Deleting;
            this.GridView.Deleting += GridView_Deleting;
        }

        void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var salesUnit = this.DataContext as SalesUnit;
            if(salesUnit == null)
                return;
            e.Cancel = true;
            var salesUnitAffiPersonnel = e.Items.First() as SalesUnitAffiPersonnel;
            DcsUtils.Confirm(PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonnel_IsDelete, () => {
                var domainContext = this.DomainContext as DcsDomainContext;
                if(domainContext != null) {
                    domainContext.SalesUnitAffiPersonnels.Remove(salesUnitAffiPersonnel);
                    this.DomainContext.SubmitChanges(submitOp => {
                        if(submitOp.HasError) {
                            if(!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                            this.DomainContext.RejectChanges();
                        }
                    }, null);
                }
            });
        }

        void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var salesUnit = this.DataContext as SalesUnit;
            if(salesUnit == null)
                return;
            if(salesUnit.OwnerCompanyId != BaseApp.Current.CurrentUserData.EnterpriseId && salesUnit.SalesUnitAffiWarehouses.Any()) {
                e.Cancel = true;
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_SalesUnit_WarehouseOnlyOne);
            }
            e.Cancel = true;
            this.SelectRadWindow.ShowDialog();
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SalesUnitAffiPersonnels");
        }
    }
}
