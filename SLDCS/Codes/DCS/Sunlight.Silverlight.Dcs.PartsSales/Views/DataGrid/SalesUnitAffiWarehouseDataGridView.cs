﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SalesUnitAffiWarehouseDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Company_Type"
        };

        public SalesUnitAffiWarehouseDataGridView() {
            this.DataContextChanged += this.SalesUnitAffiWarehouseDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
        }

        private void SalesUnitAffiWarehouseDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var salesUnit = e.NewValue as SalesUnit;
            if(salesUnit == null || salesUnit.Id == default(int))
                return;
            var filterItem = new FilterItem {
                MemberName = "SalesUnitId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = salesUnit.Id
            };
            this.FilterItem = filterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Warehouse.Code"
                    }, 
                    new ColumnItem {
                        Name = "Warehouse.Name"
                    }, 
                    new ColumnItem {
                        Name = "Warehouse.Contact"
                    }, 
                    new ColumnItem {
                        Name = "Warehouse.PhoneNumber"
                    }, 
                    new ColumnItem {
                        Name = "Warehouse.Fax"
                    },
                    new KeyValuesColumnItem {
                        Name = "Warehouse.StorageCompanyType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SalesUnitAffiWarehouse);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSalesUnitAffiWarehousesWithWarehouse";
        }
    }
}
