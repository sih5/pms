﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SalesUnitAffiWarehouseForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase warehouseQueryWindow;
        private RadWindow selectRadWindow;

        private QueryWindowBase WarehouseForQueryWindow {
            get {
                if(this.warehouseQueryWindow == null) {
                    this.warehouseQueryWindow = DI.GetQueryWindow("WarehouseFor");
                    this.warehouseQueryWindow.SelectionDecided += this.WarehouseForQueryWindow_SelectionDecided;
                    this.warehouseQueryWindow.Loaded += this.WarehouseForQueryWindow_Loaded;
                }
                return this.warehouseQueryWindow;
            }
        }

        public RadWindow SelectRadWindow {
            get {
                if(selectRadWindow == null) {
                    selectRadWindow = new RadWindow();
                    selectRadWindow.Header = PartsSalesUIStrings.QueryPanel_Title_Warehouse;
                    selectRadWindow.Content = this.WarehouseForQueryWindow;
                    selectRadWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                }
                return this.selectRadWindow;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var salesUnit = this.DataContext as SalesUnit;
            if(salesUnit == null)
                return;
            e.Cancel = true;
            this.SelectRadWindow.ShowDialog();
        }

        private void WarehouseForQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var salesUnit = this.DataContext as SalesUnit;
            var filterItem = new FilterItem();
            filterItem.MemberName = "StorageCompanyId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            if(salesUnit != null)
                filterItem.Value = salesUnit.OwnerCompanyId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void WarehouseForQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var warehouse = queryWindow.SelectedEntities.Cast<Warehouse>().FirstOrDefault();
            if(warehouse == null)
                return;
            var salesUnit = this.DataContext as SalesUnit;
            if(salesUnit == null)
                return;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(salesUnit.SalesUnitAffiWarehouses.Any(r => r.WarehouseId == warehouse.Id)) {
                UIHelper.ShowNotification(string.Format(PartsSalesUIStrings.Notification_SalesUnitAffiWarehouse_WarehouseIdAndSalesUnitAffiWarehouseWarehouseIdCanNotSame, warehouse.Code));
                return;
            }
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            DcsUtils.Confirm(PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonnel_IsAddWarehouses, () => domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(entity => entity.Id == warehouse.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var salesUnitAffiWarehouse = new SalesUnitAffiWarehouse();
                salesUnitAffiWarehouse.WarehouseId = warehouse.Id;
                salesUnitAffiWarehouse.SalesUnitId = salesUnit.Id;
                salesUnitAffiWarehouse.Warehouse = loadOp.Entities.SingleOrDefault();
                domainContext.SalesUnitAffiWarehouses.Add(salesUnitAffiWarehouse);

                this.DomainContext.SubmitChanges(submitOp => {
                    if(parent != null)
                        parent.Close();
                    if(submitOp.HasError) {
                        if(!submitOp.IsErrorHandled)
                            submitOp.MarkErrorAsHandled();
                        var errMsg = submitOp.EntitiesInError.First().ValidationErrors.First().ErrorMessage;
                        if(errMsg.IndexOf(PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonnel_Only, System.StringComparison.Ordinal) != -1) {
                            if(parent != null)
                                parent.Close();
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonne_SameWarehouses);
                            this.DomainContext.RejectChanges();
                            return;
                        }
                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                        this.DomainContext.RejectChanges();
                        return;
                    }
                    salesUnit.SalesUnitAffiWarehouses.Add(salesUnitAffiWarehouse);
                    if(parent != null)
                        parent.Close();
                }, null);
            }, null));
        }

        private void GridView_Deleteing(object sender, GridViewDeletingEventArgs e) {
            var salesUnit = this.DataContext as SalesUnit;
            if(salesUnit == null)
                return;
            e.Cancel = true;
            var salesUnitAffiWarehouse = e.Items.First() as SalesUnitAffiWarehouse;
            DcsUtils.Confirm(PartsSalesUIStrings.DataGridView_Validation_SalesUnitAffiPersonne_DeleteWarehouses, () => {
                var domainContext = this.DomainContext as DcsDomainContext;
                if(domainContext != null) {
                    domainContext.SalesUnitAffiWarehouses.Remove(salesUnitAffiWarehouse);
                    this.DomainContext.SubmitChanges(submitOp => {
                        if(submitOp.HasError) {
                            if(!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                            this.DomainContext.RejectChanges();
                            return;
                        }
                    }, null);
                }
            }, () => {
            });
            return;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Warehouse.Code",
                        //DropDownContent = this.WarehouseForQueryWindow,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Warehouse.Name",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Warehouse.Contact",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Warehouse.PhoneNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Warehouse.Fax",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SalesUnitAffiWarehouse);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleting -= GridView_Deleteing;
            this.GridView.Deleting += GridView_Deleteing;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SalesUnitAffiWarehouses");
        }
    }
}
