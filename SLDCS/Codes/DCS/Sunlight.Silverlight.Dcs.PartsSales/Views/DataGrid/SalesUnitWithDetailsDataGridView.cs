﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SalesUnitWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SalesUnit_SubmitCompanyId"
        };

        public SalesUnitWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    },new ColumnItem {
                        Name = "BusinessCode"
                    }, new ColumnItem {
                        Name = "BusinessName"
                    }, new ColumnItem {
                        Name = "Company.Name",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SalesUnit_OwnerCompanyName
                    }, new ColumnItem {
                        Name = "AccountGroup.Name",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SalesUnit_AccountGroup
                    }, new KeyValuesColumnItem {
                        Name = "SubmitCompanyId",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SalesUnit_PartsSalesCategory
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SalesUnit);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetSalesUnitsWithCompanyAndAccountGroup";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SalesUnitAffiWarehouse"
                    }
                };
            }
        }
    }
}
