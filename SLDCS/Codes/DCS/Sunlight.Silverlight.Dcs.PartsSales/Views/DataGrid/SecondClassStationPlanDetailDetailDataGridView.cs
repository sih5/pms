﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SecondClassStationPlanDetailDetailDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = { "ProcessMode" };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    },new ColumnItem {
                        Name = "Quantity"
                    },new ColumnItem {
                        Name = "Price"
                    },new KeyValuesColumnItem {
                        Name = "ProcessMode",
                        KeyValueItems = this.KeyValueManager[KvNames[0]], 
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_ProcessMode
                    }, new ColumnItem {
                        Name = "Remark"
                    } 
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SecondClassStationPlanDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetSecondClassStationPlanDetails";
        }

        private void SecondClassStationPlanDetailDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var secondClassStationPlan = e.NewValue as SecondClassStationPlan;
            if(secondClassStationPlan == null || secondClassStationPlan.Id == default(int))
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "SecondClassStationPlanId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = secondClassStationPlan.Id;
            this.ExecuteQueryDelayed();
        }

        public SecondClassStationPlanDetailDetailDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
            this.DataContextChanged += this.SecondClassStationPlanDetailDetailDataGridView_DataContextChanged;
        }
    }
}
