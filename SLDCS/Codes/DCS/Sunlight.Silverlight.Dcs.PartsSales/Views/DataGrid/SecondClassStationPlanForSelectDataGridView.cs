﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SecondClassStationPlanForSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
              "SecondClassStationPlan_Status"
        };

        private ObservableCollection<KeyValuePair> kvPartsSalesCategories;
        private ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SecondClassStationPlan);
            }
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[]{
                        "SecondClassStationPlanForSelectDetail"
                    }
                };
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_Code
                    }, new ColumnItem {
                        Name = "SecondClassStationName",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_SecondClassStationName
                    }, new ColumnItem {
                        Name = "SecondClassStationCode",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_SecondClassStationCode
                    }, new ColumnItem {
                        Name = "FirstClassStationName",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_FirstClassStationName
                    }, new ColumnItem {
                        Name = "FirstClassStationCode",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_FirstClassStationCode
                    },new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new KeyValuesColumnItem {
                        Name = "PartsSalesCategoryId",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_PartsSalesCategoryName,
                        KeyValueItems = this.KvPartsSalesCategories
                    }
                };
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            else {
                ClientVar.ConvertTime(compositeFilterItem);
                foreach(var item in compositeFilterItem.Filters)
                    newCompositeFilterItem.Filters.Add(item);
            }
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "FirstClassStationId",
                MemberType = typeof(int),
                Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(newCompositeFilterItem);
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override string OnRequestQueryName() {
            return "查询二级站需求计划转销售订单";
        }

        public SecondClassStationPlanForSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            var dcsDomainContext = this.DomainContext as DcsDomainContext ?? new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                this.KvPartsSalesCategories.Clear();
                if(loadOp.Entities != null)
                    foreach(var entity in loadOp.Entities) {
                        this.KvPartsSalesCategories.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Name
                        });
                    }
            }, null);
        }
    }
}
