﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SecondClassStationPlanForSelectDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "ProcessMode"
        };

        protected override Type EntityType {
            get {
                return typeof(SecondClassStationPlanDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[]{
                    new ColumnItem{
                        Name="SparePartCode",
                         Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    },new ColumnItem{
                        Name="SparePartName",  
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },new ColumnItem{
                        Name="Quantity",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_Quantity
                    },new KeyValuesColumnItem{
                        Name="ProcessMode",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_ProcessMode
                    },new ColumnItem{
                        Name="Remark",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_Remark
                    }
                };
            }
        }

        private void SecondClassStationPlanForSelectDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var secondClassStationPlan = e.NewValue as SecondClassStationPlan;
            if(secondClassStationPlan == null || secondClassStationPlan.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SecondClassStationPlanId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = secondClassStationPlan.Id
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "ProcessMode",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsProcessMode.转销售订单
            });
            //var p = this.Parent as DcsDataGridViewBase;
            //if(p != null) {
            //    var p2 = p.Parent as DcsQueryWindowBase;
            //    if(p2 != null) {
            //        var partsSalesOrder = p2.DataContext as PartsSalesOrder;
            //        if(partsSalesOrder != null) {
            //            compositeFilterItem.Filters.Add(new FilterItem {
            //                MemberName = "PartsSalesCategoryId",
            //                MemberType = typeof(int),
            //                Operator = FilterOperator.IsEqualTo,
            //                Value = partsSalesOrder.InvoiceReceiveSaleCateId
            //            });
            //        }
            //    }
            //}

            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override string OnRequestQueryName() {
            return "GetSecondClassStationPlanDetails";
        }

        public SecondClassStationPlanForSelectDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.SecondClassStationPlanForSelectDetailDataGridView_DataContextChanged;
        }
    }
}
