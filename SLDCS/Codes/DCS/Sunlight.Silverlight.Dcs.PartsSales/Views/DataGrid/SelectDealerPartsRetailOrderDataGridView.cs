﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SelectDealerPartsRetailOrderDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "DealerPartsRetailOrder_RetailOrderType"
        };

        public SelectDealerPartsRetailOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "Status"
                    },new ColumnItem{
                        Name="Code",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_Code
                    }, new ColumnItem{
                        Name = "DealerCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_DealerCode
                    }, new ColumnItem{
                        Name = "DealerName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_DealerName
                    },  new KeyValuesColumnItem{
                         Name = "RetailOrderType",
                         KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "Customer",
                        Title= PartsSalesUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_Customer,
                    }, new ColumnItem{
                        Name = "CustomerPhone"
                    }, new ColumnItem{
                        Name = "SubDealerName",
                    }, new ColumnItem{
                        Name = "CustomerCellPhone",
                    }, new ColumnItem{
                        Name = "TotalAmount",
                    }, new ColumnItem{
                        Name = "ApprovalComment",
                    }, new ColumnItem{
                        Name = "CreatorName",
                    }, new ColumnItem{
                        Name = "CreateTime",
                    },new ColumnItem{
                        Name = "BranchName"
                    }, new ColumnItem{
                        Name = "SalesCategoryName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsRetailOrder_SalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsRetailOrder);
            }
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SelectDealerPartsRetailOrderDetail"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetDealerPartsRetailOrders";
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            if(FilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null) {
                compositeFilterItem = new CompositeFilterItem();
                compositeFilterItem.Filters.Add(this.FilterItem);
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                IsExact = true,
                MemberName = "DealerId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            return compositeFilterItem.ToFilterDescriptor();
        }
    }
}