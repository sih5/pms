﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class ServiceAgentsHistoryQueryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsSalesOrder_Status", "PartsShipping_Method"
        };

        public ServiceAgentsHistoryQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status,
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "SalesUnitOwnerCompanyCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_AgencyCode
                    },
                    new ColumnItem {
                        Name = "SalesUnitOwnerCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_AgencyStockQryFrmOrder_AgencyName
                    },
                    new ColumnItem {
                        Name = "SubmitCompanyCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_DealerCode
                    },
                    new ColumnItem {
                        Name = "SubmitCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_DealerName
                    },
                    new ColumnItem {
                        Name = "Code",
                        Title = PartsSalesUIStrings.DetailPanel_Text_PartsRetailOrder_Code
                    },
                    new ColumnItem {
                        Name = "PartsSalesOrderTypeName",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_PartsSalesOrderTypeName
                    },
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    },
                    new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },
                    new ColumnItem {
                        Name = "OrderedQuantity",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_OrderQuntity
                    },
                    new ColumnItem {
                        Name = "ApproveQuantity",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_ApproveQuantity
                    },
                    new ColumnItem {
                        Name = "OrderPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_DealerPartsRetailOrder_Price
                    },
                    new ColumnItem {
                        Name = "OrderSum",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesOrderForParts_TotalAmount
                    },
                    new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsSalesUIStrings.QueryPanel_Title_VehiclePartsSalesOrdercsQuery_WarehouseId
                    },
                    new ColumnItem {
                        Name = "IfDirectProvision",
                        Title = PartsSalesUIStrings.DetailPanel_Text_PartsSalesOrder_IfDirectProvision
                    },                   
                    new ColumnItem {
                        Name = "IsDebt",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_IsDebt
                    },
                    new ColumnItem {
                        Name = "TotalAmount",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_TotalAmount
                    },
                    new ColumnItem {
                        Name = "Remark",
                        Title = PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                    },
                    new KeyValuesColumnItem {
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_ShippingMethod,
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },
                    new ColumnItem {
                        Name = "ReceivingAddress",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_ReceivingAddress
                    },
                    new ColumnItem {
                        Name = "RequestedDeliveryTime",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_RequestedDeliveryTime
                    },
                    new ColumnItem {
                        Name = "StopComment",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_StopReason
                    },
                    new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName
                    },
                    new ColumnItem {
                        Name = "CreateTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime
                    },
                    new ColumnItem {
                        Name = "ModifierName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifierName
                    },
                    new ColumnItem {
                        Name = "ModifyTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifyTime
                    },
                    new ColumnItem {
                        Name = "SubmitterName",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_SubmitterName
                    },
                    new ColumnItem {
                        Name = "SubmitTime",
                        Title = PartsSalesUIStrings.QueryPanel_Title_ServiceAgentsHistoryQuery_SubmitTime
                    },
                    new ColumnItem {
                        Name = "ApproverName",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproverName
                    },
                    new ColumnItem {
                        Name = "ApproveTime",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproveTime
                    },
                      new ColumnItem {
                        Name = "FirstApproveTime",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_FirstApproveTime
                    },
                    new ColumnItem {
                        Name = "AbandonerName",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_AbandonerName
                    },
                    new ColumnItem {
                        Name = "AbandonTime",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_AbandonTime
                    },
                    new ColumnItem {
                        Name = "SalesCategoryName",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(ServiceAgentsHistoryQuery);
            }
        }

        protected override string OnRequestQueryName() {
            return "服务站向代理库提报订单查询";
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }
    }
}
