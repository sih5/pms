﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SettlementAutomaticTaskSetDataGridView  : DcsDataGridViewBase{

        private readonly string[] kvNames = { 
             "SettlementAutomaticTaskStatus"                               
        };

        public SettlementAutomaticTaskSetDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new ColumnItem[] { 
                new KeyValuesColumnItem{
                 Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status,
                 Name = "Status",
                 KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                },
                new ColumnItem{
                Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_AutomaticSettleTime,
                Name = "AutomaticSettleTime"
                },
                new ColumnItem{
                Title = PartsSalesUIStrings.DataGridView_Title_SettlementAutomaticTaskSet_SettleStartTime,
                Name = "SettleStartTime"
                },
                new ColumnItem{
                 Title = PartsSalesUIStrings.DataGridView_Title_SettlementAutomaticTaskSet_SettleEndTime,
                 Name = "SettleEndTime"
                },               
                new ColumnItem{
                 Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName,
                 Name = "CreatorName"
                },
                new ColumnItem{
                 Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime,
                 Name = "CreateTime"
                },
                new ColumnItem{
                 Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifierName,
                 Name = "ModifierName"
                },
                new ColumnItem{
                 Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifyTime,
                 Name = "ModifyTime"
                }, new ColumnItem{
                Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name,
                Name = "BrandName"
                }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SettlementAutomaticTaskSet);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSettlementAutomaticTaskSets";
        }
    }
}
