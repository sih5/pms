﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SparePart1DataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "MasterData_Status", "SparePart_PartType","SparePart_MeasureUnit"
        };

        public SparePart1DataGridView() {
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var item in this.KeyValueManager[kvNames[2]])
                    KvMeasureUnit.Add(item);
            });
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualSparePart);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status
                    },new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name"
                    },new ColumnItem{
                        Name="ReferenceCode",
                        Title=PartsSalesUIStrings.QueryPanel_Title_ReferenceCode
                    },new ColumnItem{
                        Name="OverseasPartsFigure",
                        Title=PartsSalesUIStrings.QueryPanel_Title_VirtualSparePartQuery_OverseasPartsFigure
                    }, new ColumnItem {
                        Name = "CADCode",
                        Title= PartsSalesUIStrings.DataGridView_Title_VirtualSparePart_CADCode
                    },  new KeyValuesColumnItem {
                        Name = "PartType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title =PartsSalesUIStrings.DataGridView_Title_VirtualSparePart_PartType
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsPackingTask_MeasureUnit
                    }, new ColumnItem {
                        Name = "Specification",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualSparePart_Specification
                    }, new ColumnItem {
                        Name = "Feature",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualSparePart_Feature
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSparePartWithPartsBranch2";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }

        private ObservableCollection<KeyValuePair> kvMeasureUnit;
        private ObservableCollection<KeyValuePair> KvMeasureUnit {
            get {
                return this.kvMeasureUnit ?? (this.kvMeasureUnit = new ObservableCollection<KeyValuePair>());
            }
        }

        //protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
        //    if(FilterItem == null)
        //        return base.OnRequestFilterDescriptor(queryName);
        //    var newFilterItem = new CompositeFilterItem();
        //    var compositeFilterItem = FilterItem as CompositeFilterItem;
        //    if(compositeFilterItem == null) {
        //        compositeFilterItem = new CompositeFilterItem();
        //        compositeFilterItem.Filters.Add(this.FilterItem);
        //    }
        //    foreach(var item in compositeFilterItem.Filters) {
        //        if(item.MemberName == "PartsSalesCategoryId") {
        //            var partsSalesCategoryId = item.Value as int?;
        //            if(partsSalesCategoryId.HasValue) {
        //                    newFilterItem.Filters.Add(new FilterItem("partsSaleCategoryId", typeof(int), Core.Model.FilterOperator.IsEqualTo, partsSalesCategoryId.Value));
        //            }
        //            continue;
        //        }
        //        newFilterItem.Filters.Add(item);
        //    }
        //    return newFilterItem.ToFilterDescriptor();
        //}

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "partsSaleCategoryId":
                        return filters.Filters.Single(item => item.MemberName == "PartsSalesCategoryId").Value;
                }
            }
            return null;
        }
    }
}
