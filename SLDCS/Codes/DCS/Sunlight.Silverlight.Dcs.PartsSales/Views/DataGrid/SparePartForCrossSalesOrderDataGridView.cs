﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SparePartForCrossSalesOrderDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        public SparePartForCrossSalesOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override string OnRequestQueryName() {
            return "跨区域销售查询配件销售价";
        }


        protected override Type EntityType {
            get {
                return typeof(VirtualSparePart);
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        /// <summary>
        /// 清除当前选中项
        /// </summary>
        public void ClearSelected() {
            if(this.GridView != null && this.GridView.SelectedItems != null)
                this.GridView.SelectedItems.Clear();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title="配件编号"
                    }, new ColumnItem {
                        Name = "Name",
                        Title="配件名称"
                    }, new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = "配件ABC分类"
                    }, new ColumnItem {
                        Name = "CenterPrice",
                        Title="中心库价"
                    }, new ColumnItem {
                        Name = "SalesPrice",
                        Title="经销价"
                    },new ColumnItem{
                        Name="RetailGuidePrice",
                        Title="建议售价"
                    } ,new ColumnItem {
                        Title = "价格类型",
                        Name = "PriceTypeName"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            ((GridViewDataColumn)this.GridView.Columns["CenterPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SalesPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
        }
    }
}
