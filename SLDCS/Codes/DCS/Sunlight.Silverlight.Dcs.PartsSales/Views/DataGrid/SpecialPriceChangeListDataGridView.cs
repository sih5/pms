﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SpecialPriceChangeListDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "WorkflowOfSimpleApproval_Status"
        };

        protected override Type EntityType {
            get {
                return typeof(SpecialPriceChangeList);
            }
        }

        public SpecialPriceChangeListDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += SpecialPriceChangeListDataGridView_DataContextChanged;
        }

        protected override string OnRequestQueryName() {
            return "GetSpecialPriceChangeListsRelation";
        }
        void SpecialPriceChangeListDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            if(specialTreatyPriceChange == null || specialTreatyPriceChange.Id == default(int))
                return;            
            var compositeFilter = new CompositeFilterItem();
            compositeFilter.Filters.Add(new FilterItem {
                MemberName = "SpecialTreatyPriceChangeId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = specialTreatyPriceChange.Id
            });
            this.FilterItem = compositeFilter;
            this.ExecuteQueryDelayed();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["SpecialTreatyPrice"]).DataFormatString = "c2";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                         Name = "SparePartName",
                         Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem {
                        Name = "SpecialTreatyPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_AgreementPrice
                    },new ColumnItem {
                        Name = "PurchasePrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_SpecialPriceChangeList_PurchasePrice
                    },new ColumnItem {
                        Name = "CenterPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Centerprice
                    },new ColumnItem {
                        Name = "SalesPrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualDealerPartsSalesPrice_SalesPrice
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        //protected override object OnRequestQueryParameter(string queryName, string parameterName) {
        //    switch(parameterName) {
        //        case "parentId":
        //            return id;
        //        default:
        //            return null;
        //    }
        //}
    }
}
