﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SpecialTreatyPriceChangeDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SpecialTreatyPriceChange_Status"
        };
        public SpecialTreatyPriceChangeDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(SpecialTreatyPriceChange);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        IsSortable = false,
                        Name = "IsUplodFile",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesPriceChange_IsUplodFile
                    },new ColumnItem {
                        Name = "Code",
                        Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesPriceChangeQuery_Code
                    }, new ColumnItem {
                        Name = "CorporationCode",
                        Title = PartsSalesUIStrings.QueryPanel_QueeryItem_Title_Company_Code
                    }, new ColumnItem {
                        Name = "CorporationName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_CorporationName
                    },  new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "ValidationTime",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ValidationTime
                    }, new ColumnItem {
                        Name = "ExpireTime",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ExpireTime
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "InitialApproverName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_InitialApproverName
                    }, new ColumnItem {
                        Name = "InitialApproveTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsSalesPriceHistory_InitialApproveTime
                    }, new ColumnItem {
                        Name = "InitialApproverComment",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_InitialApproverComment
                    }, new ColumnItem {
                        Name = "ApproverName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Approver
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ApproveTime
                    }, new ColumnItem {
                        Name = "ApproveComment",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ApproveComment
                    }, new ColumnItem {
                        Name = "CheckerName",
                        Title =PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproverName
                    }, new ColumnItem {
                        Name = "CheckerTime",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproveTime
                    }, new ColumnItem {
                        Name = "CheckerComment",
                        Title = PartsSalesUIStrings.DataEditView_Text_DealerPartsRetailOrder_Comment
                    }, new ColumnItem {
                        Name = "AbandonerName",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_AbandonerName
                    }, new ColumnItem {
                        Name = "AbandonerTime",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_AbandonTime
                    }, new ColumnItem {
                        Name = "BrandName",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetSpecialTreatyPriceChangesForOrder";
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SpecialTreatyPriceChange"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["InitialApproveTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CheckerTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ApproveTime"]).DataFormatString = "d";

        }
    }
}
