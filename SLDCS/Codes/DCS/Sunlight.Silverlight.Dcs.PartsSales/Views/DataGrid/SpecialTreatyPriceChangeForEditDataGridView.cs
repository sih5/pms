﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SpecialTreatyPriceChangeForEditDataGridView : DcsDataGridViewBase {

        private SparePartWithBranchDropDownQueryWindow sparePartPriceQueryWindow;
        private SparePartWithBranchDropDownQueryWindow SparePartPriceQueryWindow {
            get {
                if(this.sparePartPriceQueryWindow == null) {
                    this.sparePartPriceQueryWindow = DI.GetQueryWindow("SparePartWithBranchDropDown") as SparePartWithBranchDropDownQueryWindow;
                    this.sparePartPriceQueryWindow.SelectionDecided -= partsSalesPriceQueryPanel_SelectionDecided;
                    this.sparePartPriceQueryWindow.SelectionDecided += partsSalesPriceQueryPanel_SelectionDecided;

                    this.sparePartPriceQueryWindow.Loaded -= SparePartPriceQueryWindow_Loaded;
                    this.sparePartPriceQueryWindow.Loaded += SparePartPriceQueryWindow_Loaded;
                }
                return this.sparePartPriceQueryWindow;
            }
        }

        private void SparePartPriceQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as SparePartWithBranchDropDownQueryWindow;
            queryWindow.Visibility = Visibility.Visible;
            var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            if(specialTreatyPriceChange.BrandId == null || specialTreatyPriceChange.CorporationId == null) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_SpecialPriceChangeList_BrandId);
                //queryWindow.Visibility = Visibility.Collapsed;
            }
            if(specialTreatyPriceChange == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "PartsSalesCategoryId", specialTreatyPriceChange.BrandId
            });
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SpecialPriceChangeList);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SpecialPriceChangeLists");
        }

        void partsSalesPriceQueryPanel_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as SparePartWithBranchDropDownQueryWindow;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<VirtualSparePart>().FirstOrDefault();
            if(sparePart == null)
                return;
            var specialPriceChangeList = queryWindow.DataContext as SpecialPriceChangeList;
            if(specialPriceChangeList == null)
                return;
            var specialTreatyPriceChange = this.DataContext as SpecialTreatyPriceChange;
            if(specialTreatyPriceChange == null)
                return;
            specialPriceChangeList.SparePartId = sparePart.Id;
            specialPriceChangeList.SparePartCode = sparePart.Code;
            specialPriceChangeList.SparePartName = sparePart.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            dcsDomainContext.Load(dcsDomainContext.GetPartsPurchasePricingByPartIdQuery(sparePart.Id).Where(r => r.ValidFrom <= DateTime.Now && r.ValidTo > DateTime.Now && r.Status != (int)DcsPartsPurchasePricingStatus.作废 && r.PartsSalesCategoryId == specialTreatyPriceChange.BrandId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entities = loadOp.Entities;
                if(entities.Count() > 0)
                    specialPriceChangeList.PurchasePrice = entities.Min(r => r.PurchasePrice);
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesPricesQuery().Where(r => r.PartsSalesCategoryId == sparePart.PartsSalesCategoryId && r.SparePartId == sparePart.Id), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError) {
                    loadOp1.MarkErrorAsHandled();
                    return;
                }
                var salesPrice = loadOp1.Entities.FirstOrDefault();
                if(salesPrice != null) {
                    specialPriceChangeList.RetailPrice = salesPrice.SalesPrice;
                }
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetEnterprisePartsCostsQuery().Where(r => r.PartsSalesCategoryId == sparePart.PartsSalesCategoryId && r.SparePartId == sparePart.Id && r.OwnerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError) {
                    loadOp1.MarkErrorAsHandled();
                    return;
                }
                var costPrice = loadOp1.Entities.FirstOrDefault();
                if(costPrice != null) {
                    specialPriceChangeList.CostPrice = costPrice.CostPrice;
                }
            }, null);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode,
                        DropDownContent = this.SparePartPriceQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SpecialTreatyPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_AgreementPrice
                    },new ColumnItem {
                        Name = "RetailPrice",
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesPriceChangeApplication_Orderprice,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Ratio",
                        Title = PartsSalesUIStrings.DataGridView_Title_SpecialPriceChangeList_Ratio,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["SpecialTreatyPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["RetailPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Ratio"]).DataFormatString = "p";
        }
    }
}
