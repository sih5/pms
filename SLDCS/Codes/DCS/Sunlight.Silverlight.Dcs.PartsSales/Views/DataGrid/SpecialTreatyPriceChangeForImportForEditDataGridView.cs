﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SpecialTreatyPriceChangeForImportForEditDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem {
                        Name = "SequeueNumber",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_SequeueNumber
                    },new ColumnItem{
                        Name="CorporationCodeStr",
                        Title = PartsSalesUIStrings.QueryPanel_QueeryItem_Title_Company_Code
                    },new ColumnItem{
                        Name="CorporationNameStr",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_Personnel_CorporationName
                    },new ColumnItem{
                        Name= "BrandNameStr",
                        Title= PartsSalesUIStrings.DataEditPanel_Text_PartsSalesCategory_Name
                    },new ColumnItem{
                        Name= "ValidationTimeStr",
                        Title= PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ValidationTime
                    },new ColumnItem{
                        Name= "ExpireTimeStr",
                        Title= PartsSalesUIStrings.DataEditPanel_Text_SpecialTreatyPriceChange_ExpireTime
                    },new ColumnItem{
                        Name="SparePartCodeStr",
                        Title= PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    },new ColumnItem{
                        Name="SparePartNameStr",
                        Title= PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },new ColumnItem{
                        Name="SpecialTreatyPrice",
                        Title= PartsSalesUIStrings.DataEdit_Title_SpecialTreatyPriceChange_SpecialPrice
                    },new ColumnItem{
                        Name="RemarkStr",
                        Title= PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SpecialTreatyPriceChangeWithDetailExtend);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SpecialTreatyPriceChangeWithDetailExtends");
        }
    }
}