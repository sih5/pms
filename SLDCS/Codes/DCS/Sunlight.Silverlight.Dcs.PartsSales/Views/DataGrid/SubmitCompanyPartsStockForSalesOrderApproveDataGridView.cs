﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class SubmitCompanyPartsStockForSalesOrderApproveDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            base.OnControlsCreated();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "CompanyCode",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Code
                    }, new ColumnItem {
                        Name = "CompanyName",
                          Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDelivery_Shop_Name
                    },new ColumnItem {
                        Name = "SparePartCode",
                         Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_PartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                       Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    }, new ColumnItem {
                        Name = "Quantity",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesOrderDetail_StockQuantity
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CompanyPartsStock);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("CompanyPartsStocks");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
