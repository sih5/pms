﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class TowGradePartsNeedDataGridView : DcsDataGridViewBase {
        protected readonly string[] KvNames = {
            "SecondClassStationPlan_Status"
        };

        public TowGradePartsNeedDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(SecondClassStationPlan);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_Code
                    }, new ColumnItem {
                        Name = "SecondClassStationName",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_SecondClassStationName
                    }, new ColumnItem {
                        Name = "SecondClassStationCode",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_SecondClassStationCode
                    }, new ColumnItem {
                        Name = "FirstClassStationName",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_FirstClassStationName
                    }, new ColumnItem {
                        Name = "FirstClassStationCode",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_FirstClassStationCode
                    },new ColumnItem {
                        Name = "TotalAmount"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title=PartsSalesUIStrings.DataGridView_Text_SecondClassStationPlan_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }


        protected override string OnRequestQueryName() {
            return "GetSecondClassStationPlanWithDetail";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SecondClassStationPlanDetail"
                    }
                };
            }
        }
    }
}
