﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class TowGradePartsNeedForEditDataGridView : DcsDataGridViewBase {
        public DcsMultiPopupsQueryWindowBase partsSalesPriceQueryWindow;
        private RadWindow radQueryWindow;
        private ObservableCollection<SecondClassStationPlanDetail> secondClassStationPlanDetails;
        public ObservableCollection<SecondClassStationPlanDetail> SecondClassStationPlanDetails {
            get {
                return this.secondClassStationPlanDetails ?? (this.secondClassStationPlanDetails = new ObservableCollection<SecondClassStationPlanDetail>());
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SequeueNumber",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlan_SequeueNumber,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },new ColumnItem {
                        Name = "Quantity",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_Quantity
                    },new ColumnItem {
                        Name = "Price",
                        IsReadOnly = true
                    } ,new ColumnItem {
                        Name = "Remark"
                    } 
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SecondClassStationPlanDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.Deleted += this.GridView_Deleted;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Columns["Remark"].DataControl.MaxColumnWidth = 350;
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var master = this.DataContext as SecondClassStationPlan;
            if(master == null)
                return;
            int sequeueNumber = 1;
            foreach(var item in master.SecondClassStationPlanDetails)
                item.SequeueNumber = sequeueNumber++;
            master.TotalAmount = master.SecondClassStationPlanDetails.Any() ? master.SecondClassStationPlanDetails.Sum(r => r.Quantity * r.Price) : default(decimal);
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var master = this.DataContext as SecondClassStationPlan;
            if(master == null)
                return;
            if(master.PartsSalesCategoryId <= 0) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_CustomerDirectSpareList);
                e.Cancel = true;
                return;
            }
            RadQueryWindow.ShowDialog();
            e.Cancel = true;
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var master = this.DataContext as SecondClassStationPlan;
            if(master == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "Quantity":
                    master.TotalAmount = master.SecondClassStationPlanDetails.Sum(r => r.Quantity * r.Price);
                    break;
            }
        }

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.PartsSalesPriceQueryWindow,
                    Header = "",
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        public DcsMultiPopupsQueryWindowBase PartsSalesPriceQueryWindow {
            get {
                if(this.partsSalesPriceQueryWindow == null) {
                    this.partsSalesPriceQueryWindow = DI.GetQueryWindow("PartsSalesPrice") as DcsMultiPopupsQueryWindowBase;
                    this.partsSalesPriceQueryWindow.SelectionDecided += partsSalesPriceQueryWindow_SelectionDecided;
                    this.partsSalesPriceQueryWindow.Loaded += partsSalesPriceQueryWindow_Loaded;
                }
                return partsSalesPriceQueryWindow;
            }
        }   

        private void partsSalesPriceQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var secondClassStationPlan = this.DataContext as SecondClassStationPlan;
            if(queryWindow == null || secondClassStationPlan == null)
                return;

            var compositeFilterItem = new CompositeFilterItem {
                LogicalOperator = LogicalOperator.And,
                Filters = {
                        new FilterItem {
                            MemberType = typeof(bool),
                            MemberName = "IfDirectProvision",
                            Operator = FilterOperator.IsEqualTo,
                            Value = false,
                        },
                        new FilterItem {
                            MemberType = typeof(int),
                            MemberName = "PartsSalesCategoryId",
                            Operator = FilterOperator.IsEqualTo,  
                            Value = secondClassStationPlan.PartsSalesCategoryId,
                        },
                        new FilterItem {
                            MemberType = typeof(int),
                            MemberName = "CustomerId",
                            Operator = FilterOperator.IsEqualTo,
                            Value = secondClassStationPlan.FirstClassStationId,
                        }
                    }
            };
            this.PartsSalesPriceQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void partsSalesPriceQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var maxSerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<SecondClassStationPlanDetail>().Count() + 1 : 1;
            var virtualPartsSalesPrices = queryWindow.SelectedEntities.Cast<VirtualPartsSalesPrice>().ToList();
            if(virtualPartsSalesPrices == null)
                return;
            var secondClassStationPlan = this.DataContext as SecondClassStationPlan;
            if(secondClassStationPlan == null)
                return;
            foreach(var virtualPartsSalesPrice in virtualPartsSalesPrices) {
                if(secondClassStationPlan.SecondClassStationPlanDetails.Any(ex => ex.SparePartId == virtualPartsSalesPrice.SparePartId))
                    continue;
                secondClassStationPlan.SecondClassStationPlanDetails.Add(new SecondClassStationPlanDetail {
                    SequeueNumber = maxSerialNumber++,
                    SparePartId = virtualPartsSalesPrice.SparePartId,
                    SparePartCode = virtualPartsSalesPrice.SparePartCode,
                    SparePartName = virtualPartsSalesPrice.SparePartName,
                    Price = virtualPartsSalesPrice.Price
                });

            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SecondClassStationPlanDetails");
        }
    }
}
