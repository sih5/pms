﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class TowGradePartsNeedManagerDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SecondClassStationPlan_Status"
        };

        protected override Type EntityType {
            get {
                return typeof(SecondClassStationPlan);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        public TowGradePartsNeedManagerDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override string OnRequestQueryName() {
            return "GetSecondClassStationPlanWithDetail";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SecondClassStationPlanDetail"
                    }
                };
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem{
                        Name="Code",
                        Title= PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlan_Code
                    },new ColumnItem{
                        Name="SecondClassStationName",
                        Title= PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlan_SecondClassStationName
                    },new ColumnItem{
                        Name="SecondClassStationCode",
                        Title= PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlan_SecondClassStationCode
                    },new ColumnItem{
                        Name="FirstClassStationName",
                        Title= PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlan_FirstClassStationName
                    },new ColumnItem{
                        Name="FirstClassStationCode",
                        Title= PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlan_FirstClassStationCode
                    },new ColumnItem{
                        Name="TotalAmount"
                    },new ColumnItem{
                        Name="CreatorName"
                    }, new ColumnItem{
                        Name="CreateTime"
                    },new ColumnItem{
                        Name="ModifierName"
                    },new ColumnItem{
                        Name="ModifyTime"
                    },new ColumnItem{
                        Name="PartsSalesCategory.Name",
                        Title= PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_Name
                    }};
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
