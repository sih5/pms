﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class TowGradePartsNeedManagerForApproveEditDataGridView : TowGradePartsNeedManagerForEditDataGridView {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SequeueNumber",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlan_SequeueNumber,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },new ColumnItem {
                        Name = "Quantity",
                        IsReadOnly = true,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_Quantity
                    } ,new ColumnItem {
                        Name = "Price",
                        IsReadOnly = true,
                    } ,new ColumnItem{
                        Name = "DealerPartsStockQuantity",
                        Title=PartsSalesUIStrings.DataGridView_Title_TowGradePartsNeed_DealerPartsStockQuantity,
                        IsReadOnly = true,
                        IsSortable = false
                    },new KeyValuesColumnItem {
                        Name = "ProcessMode",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_ProcessMode
                    },new ColumnItem{
                        Name="Remark",
                        IsReadOnly=true
                    }
                };
            }
        }
    }
}
