﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class TowGradePartsNeedManagerForEditDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames ={
            "ProcessMode"
        };
        public TowGradePartsNeedManagerForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += TowGradePartsNeedManagerForEditDataGridView_DataContextChanged;
        }
        private void TowGradePartsNeedManagerForEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var secondClassStationPlan = this.DataContext as SecondClassStationPlan;
            if(secondClassStationPlan == null || secondClassStationPlan.Id == default(int))
                return;
            var domainContext = this.DomainContext as DcsDomainContext;
            domainContext.Load(domainContext.GetSecondClassStationPlanDetailsQuery().Where(s => s.SecondClassStationPlanId == secondClassStationPlan.Id), loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.ToList();
                int number = entity.Count, maxSerialNumber = 1;

                for(int i = 0; i < number; i++) {
                    entity[i].SequeueNumber = maxSerialNumber;
                    if(entity[i].ProcessMode == null)
                        entity[i].ProcessMode = (int)DcsProcessMode.本企业满足;
                    maxSerialNumber++;
                }

                var ids = entity.Select(r => r.SparePartId).ToArray();
                domainContext.Load(domainContext.GetDealerPartsStocksBySecondClassStationPlanAndSparePartIdsQuery(secondClassStationPlan.FirstClassStationId, secondClassStationPlan.PartsSalesCategoryId, ids), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError) {
                        if(!loadOption.IsErrorHandled)
                            loadOption.MarkErrorAsHandled();
                        return;
                    }
                    if(loadOption.Entities != null)
                        foreach(var detail in secondClassStationPlan.SecondClassStationPlanDetails) {
                            detail.DealerPartsStockQuantity = loadOption.Entities.FirstOrDefault(r => r.SparePartId == detail.SparePartId) == null ? 0 : loadOption.Entities.FirstOrDefault(r => r.SparePartId == detail.SparePartId).Quantity;
                        }
                }, null);

            }, null);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SequeueNumber",
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlan_SequeueNumber,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },new ColumnItem {
                        Name = "Quantity",
                        IsReadOnly = true,
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_Quantity
                    } ,new KeyValuesColumnItem {
                        Name = "ProcessMode",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_ProcessMode
                    },new ColumnItem{
                        Name="Remark",
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SecondClassStationPlanDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SecondClassStationPlanDetails");
        }
    }
}