﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid
{
    public class TowGradePartsRetailOrderDetailDataGridView : DcsDataGridViewBase
    {
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] { 
                    new ColumnItem{
                        Name = "SerialNumber"
                    }, new ColumnItem{
                        Name = "PartsCode"
                    }, new ColumnItem{
                        Name = "PartsName"
                    }, new ColumnItem{
                        Name = "Quantity"
                    }, new ColumnItem{
                        Name = "Price"
                    }, new ColumnItem{
                        Name = "Remark"
                    }
                };
            }
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
        }
        private void TowGradePartsRetailOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dealerPartsRetailOrder = e.NewValue as DealerPartsRetailOrder;
            if (dealerPartsRetailOrder == null || dealerPartsRetailOrder.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem
            {
                MemberName = "DealerPartsRetailOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = dealerPartsRetailOrder.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        public TowGradePartsRetailOrderDetailDataGridView()
        {
            this.DataContextChanged += TowGradePartsRetailOrderDetailDataGridView_DataContextChanged;
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return false;
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(DealerRetailOrderDetail);
            }
        }
        protected override string OnRequestQueryName()
        {
            return "GetDealerRetailOrderDetails";
        }


    }
}
