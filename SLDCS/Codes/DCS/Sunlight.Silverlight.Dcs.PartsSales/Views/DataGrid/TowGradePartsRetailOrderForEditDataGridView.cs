﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class TowGradePartsRetailOrderForEditDataGridView : DcsDataGridViewBase {
        private RadWindow radQueryWindow;
        private DcsMultiPopupsQueryWindowBase towGradePartsRetailOrderDownQueryWindow;

        private DcsMultiPopupsQueryWindowBase TowGradePartsRetailOrderDownQueryWindow {
            get {
                if(this.towGradePartsRetailOrderDownQueryWindow == null) {
                    this.towGradePartsRetailOrderDownQueryWindow = DI.GetQueryWindow("DealerPartsStock") as DcsMultiPopupsQueryWindowBase;
                    this.towGradePartsRetailOrderDownQueryWindow.SelectionDecided += this.TowGradePartsRetailOrderDownQueryWindow_SelectionDecided;
                    this.towGradePartsRetailOrderDownQueryWindow.Loaded += towGradePartsRetailOrderDownQueryWindow_Loaded;
                }
                return this.towGradePartsRetailOrderDownQueryWindow;
            }
        }

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.TowGradePartsRetailOrderDownQueryWindow,
                    Header = PartsSalesUIStrings.QueryPanel_Title_DealerPartsRetailOrderQueryWindow,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private void towGradePartsRetailOrderDownQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "DealerId", dealerPartsRetailOrder.DealerId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "DealerId", false
            });

            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SalesCategoryId", dealerPartsRetailOrder.PartsSalesCategoryId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "SalesCategoryId", false
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("SubDealerId", typeof(int), FilterOperator.IsEqualTo, dealerPartsRetailOrder.SubDealerId));
        }

        private void TowGradePartsRetailOrderDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            if(dealerPartsRetailOrder == null)
                return;

            var dealerPartsStock = queryWindow.SelectedEntities.Cast<DealerPartsStockForBussiness>();
            if(dealerPartsStock == null)
                return;

            if(dealerPartsRetailOrder.DealerRetailOrderDetails.Any(r => dealerPartsStock.Any(ex => ex.SparePartCode == r.PartsCode))) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataGridView_Validation_VirtualPartsPurchaseOrderDetail_SparePartCode);
                return;
            }

            foreach(var dealer in dealerPartsStock) {
                var serialNumber = dealerPartsRetailOrder.DealerRetailOrderDetails.Count();
                serialNumber++;
                var dealerRetailOrderDetail = new DealerRetailOrderDetail {
                    SerialNumber = serialNumber,
                    PartsId = dealer.SparePartId,
                    PartsCode = dealer.SparePartCode,
                    PartsName = dealer.SparePartName,
                    Quantity = 0,
                    Price = dealer.RetailGuidePrice
                };
                dealerPartsRetailOrder.DealerRetailOrderDetails.Add(dealerRetailOrderDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new ColumnItem[] {
                    new ColumnItem{
                        Name = "SerialNumber",
                        IsReadOnly=true
                    }, new DropDownTextBoxColumnItem{
                        Name = "PartsCode",
                        DropDownContent = this.TowGradePartsRetailOrderDownQueryWindow,
                        IsReadOnly = false,
                    }, new ColumnItem{
                        Name = "PartsName",
                        IsReadOnly = true,
                    }, new ColumnItem{
                        Name = "Quantity",
                        IsReadOnly = false,
                    }, new ColumnItem{
                        Name = "Price",
                        IsReadOnly = false,
                    }, new ColumnItem{
                        Name = "Remark",
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerRetailOrderDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerRetailOrderDetails");
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.CellEditEnded -= GridView_CellEditEnded;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";

        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            var serialNumber = 1;
            foreach(var dealerRetailOrderDetail in dealerPartsRetailOrder.DealerRetailOrderDetails.Where(entity => !e.Items.Contains(entity))) {
                dealerRetailOrderDetail.SerialNumber = serialNumber;
                serialNumber++;
            }
            dealerPartsRetailOrder.TotalAmount = dealerPartsRetailOrder.DealerRetailOrderDetails.Sum(ex => ex.Quantity * ex.Price);
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            e.Cancel = true;
            if(dealerPartsRetailOrder.BranchId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_DealerPartsRetailOrder_DealerNameIsNull);
                return;
            }
            if(dealerPartsRetailOrder.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsSalesUIStrings.DataEditView_Validation_DealerPartsRetailOrder_PartsSalesCategoryNameIsNull);
                return;
            }
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            RadQueryWindow.ShowDialog();
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var dealerPartsRetailOrder = this.DataContext as DealerPartsRetailOrder;
            if(dealerPartsRetailOrder == null)
                return;
            dealerPartsRetailOrder.TotalAmount = dealerPartsRetailOrder.DealerRetailOrderDetails.Sum(ex => ex.Quantity * ex.Price);
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }

}
