﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class VirtualPartsSalesSettlementDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "BillCode",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPartsSalesRtnSettlement_BillCode
                    },new ColumnItem {
                        Name = "BillBusinessType",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPartsSalesSettlement_BillBusinessType
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartCode
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_SecondClassStationPlanDetail_SparePartName
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_ERPDeliveryDetail_Item_Qty
                    },new ColumnItem {
                        Name = "SettlementPrice",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementDetail_SettlementPrice
                    },new ColumnItem {
                        Name = "SettlementAmount",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesRtnSettlementRef_SettlementAmount
                    },new ColumnItem {
                        Name = "PlanPrice",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPartsSalesRtnSettlement_PlanPrice
                    },new ColumnItem {
                        Name  ="MaterialCostVariance",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPartsSalesRtnSettlement_MaterialCostVariance
                    },new ColumnItem {
                        Name = "PlanAmount",
                        Title = PartsSalesUIStrings.DataGridView_Title_VirtualPartsSalesRtnSettlement_PlanAmount
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsSalesSettlement);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override string OnRequestQueryName() {
            return "查询虚拟配件销售结算成本";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                //var cFilterItem = filters.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                foreach(var composite in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                    var compositeInternal = composite as CompositeFilterItem;
                    if(compositeInternal == null)
                        continue;
                    switch(parameterName) {
                        case "partsSalesSettlementId":
                            return compositeInternal.Filters.Single(item => item.MemberName == "Id").Value;
                    }
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
