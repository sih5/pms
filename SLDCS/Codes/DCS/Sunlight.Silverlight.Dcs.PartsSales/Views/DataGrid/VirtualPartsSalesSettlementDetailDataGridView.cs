﻿using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class VirtualPartsSalesSettlementDetailDataGridView : PartsSalesSettlementDetailDataGridView {
        public VirtualPartsSalesSettlementDetailDataGridView() {
            this.DataContextChanged += VirtualPartsSalesSettlementDetailDataGridView_DataContextChanged;
        }

        private void VirtualPartsSalesSettlementDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsSalesSettlement = e.NewValue as PartsSalesSettlementEx;
            if(partsSalesSettlement == null || partsSalesSettlement.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesSettlementId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsSalesSettlement.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
    }
}
