﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class VirtualPartsSalesSettlementWithDetailsCIFDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsPurchaseSettleBill_SettlementPath", "PartsSalesSettlement_Status"
        };
        public VirtualPartsSalesSettlementWithDetailsCIFDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsSalesUIStrings.QueryPanel_Title_AgencyLogisticCompany_Status,
                    }, new ColumnItem {
                        Name = "Code",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlement_Code
                    }, new ColumnItem {
                        Name = "SalesCompanyName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlement_SalesCompanyName
                    }, new ColumnItem {
                        Name = "AccountGroupName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlement_AccountGroupName
                    }, new ColumnItem {
                        Name = "CustomerCompanyCode",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyCode
                    }, new ColumnItem {
                        Name = "CustomerCompanyName",
                        Title = PartsSalesUIStrings.DataEditPanel_Text_CustomerOrderPriceGrade_CustomerCompanyName
                    }, //new ColumnItem {
                        //Name = "TotalSettlementAmount",
                        //MaskType = MaskType.Numeric,
                        //TextAlignment = TextAlignment.Right,
                        //Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_TotalSettlementAmount,
                   // }, 
                   new AggregateColumnItem {
                        Name = "TotalSettlementAmount",
                        AggregateType = AggregateType.Sum,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsSalesUIStrings.DataEdit_Text_PartsSalesSettlement_TotalSettlementAmount,
                    }, new ColumnItem {
                        Name = "RebateAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_RebateAmount,
                    }, new ColumnItem {
                        Name = "TaxRate",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_TaxRate,
                    }, new ColumnItem {
                        Name = "Tax",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_Tax,
                    }, new KeyValuesColumnItem {
                        Name = "SettlementPath",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_SettlementPath,
                    }, new ColumnItem {
                        Name = "OffsettedSettlementBillCode",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_OffsettedSettlementBillCode,
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = PartsSalesUIStrings.DataEditView_Text_SecondClassStationPlan_Remark
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_CreatorName,
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesSettlementRef_SourceBillCreateTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifierName,
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePartsStockLevel_ModifyTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ApproverName",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproverName,
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsPackingPropertyAppQuery_ApproveTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "InvoiceRegistrationOperator",
                         Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_InvoiceRegistrationOperator
                    }, new ColumnItem {
                        Name = "InvoiceRegistrationTime",
                         Title = PartsSalesUIStrings.DataGridView_Title_PartsSalesSettlementEx_InvoiceRegistrationTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "AbandonerName",
                         Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_AbandonerName
                    }, new ColumnItem {
                        Name = "AbandonTime",
                        Title = PartsSalesUIStrings.DataGridView_Title_ServiceAgentsHistoryQuery_AbandonTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_PartsSalesCategory_Name
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsSalesSettlementEx);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsSalesSettlement", "VirtualPartsSalesSettlementDetail", "VirtualPartsSalesSettlementRef"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件销售结算单统计计划价合计采埃孚";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 100;  //配件销售结算管理每页显示100条
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["TotalSettlementAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Tax"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TaxRate"]).DataFormatString = "n2";
            ((GridViewDataColumn)this.GridView.Columns["RebateAmount"]).DataFormatString = "c2";
        }
    }
}
