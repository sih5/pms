﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class VirtualPartsStockForDeliver1DataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
            "Area_Category"
        };

        public VirtualPartsStockForDeliver1DataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem{
                        Name = "WarehouseAreaCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true,
                        Title = PartsSalesUIStrings.DataGridView_Title_PartsOutboundBillDetail_WarehouseAreaCategory
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsableQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsOutboundBillDetail_UsableQuantity,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "OutboundAmount",
                        Title=PartsSalesUIStrings.DataGridView_Title_PartsOutboundBillDetail_OutboundAmount,
                        IsReadOnly = false
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOutboundBillDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsOutboundBillDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            //this.GridView.RowLoaded += GridView_RowLoaded;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["UsableQuantity"]).DataFormatString = "d";
        }

        //private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
        //    var partsOutboundBillDetail = e.Row.DataContext as PartsOutboundBillDetail;
        //    if(partsOutboundBillDetail == null)
        //        return;
        //    var partsOutboundBillDataEditView = this.DataContext as PartsOutboundBillDataEditView;
        //    if(partsOutboundBillDataEditView == null)
        //        return;
        //    GridViewRow gridViewRow = this.GridView.GetRowForItem(partsOutboundBillDetail);

        //    if(partsOutboundBillDataEditView.PartsOutboundPlanDetailForDeliverDataGridView.SelectedEntities == null || !partsOutboundBillDataEditView.PartsOutboundPlanDetailForDeliverDataGridView.SelectedEntities.Any()) {
        //        if(gridViewRow != null) {
        //            gridViewRow.Background = partsOutboundBillDataEditView.PartsOutboundBillDetails.Count(r => r.SparePartId == partsOutboundBillDetail.SparePartId) > 1 ? new SolidColorBrush(Colors.Red) : new SolidColorBrush(Colors.White);
        //        }
        //    } else {
        //        var partsOutboundPlanDetail = partsOutboundBillDataEditView.PartsOutboundPlanDetailForDeliverDataGridView.SelectedEntities.Cast<PartsOutboundPlanDetail>().SingleOrDefault();
        //        if(partsOutboundPlanDetail != null && partsOutboundPlanDetail.SparePartId == partsOutboundBillDetail.SparePartId)
        //            if(gridViewRow != null)
        //                gridViewRow.Background = new SolidColorBrush(Colors.Blue);
        //    }

        //}

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(!e.Cell.Column.UniqueName.Equals("OutboundAmount"))
                return;
            var partsOutboundBillDetail = e.Cell.DataContext as PartsOutboundBillDetail;
            if(partsOutboundBillDetail == null)
                return;
            var quantityError = partsOutboundBillDetail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("OutboundAmount"));
            if(partsOutboundBillDetail.ValidationErrors.Contains(quantityError))
                partsOutboundBillDetail.ValidationErrors.Remove(quantityError);
            if(partsOutboundBillDetail.OutboundAmount < 0) {
                partsOutboundBillDetail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataGridView_Validation_PartsOutboundBillDetail_OutboundAmount, new[] {
                        "OutboundAmount"
                    }));
                return;
            }
            var agencyRetailerOrderForConfirmDataEditView = this.DataContext as AgencyRetailerOrderForConfirmDataEditView;
            if(agencyRetailerOrderForConfirmDataEditView == null)
                return;
            //剩余出库数量
            var agencyRetailerList = agencyRetailerOrderForConfirmDataEditView.AgencyRetailerOrder.AgencyRetailerLists.FirstOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId);
            var outboundAmount = agencyRetailerList.OrderedQuantity - (agencyRetailerList.ConfirmedAmount.HasValue ? agencyRetailerList.ConfirmedAmount.Value : 0);
            //本次出库数量
            var currentOutAmount = agencyRetailerOrderForConfirmDataEditView.PartsOutboundBillDetails.Where(r => r.SparePartId == partsOutboundBillDetail.SparePartId).Sum(r => r.OutboundAmount);


            if(outboundAmount < currentOutAmount) {
                partsOutboundBillDetail.ValidationErrors.Add(new ValidationResult(PartsSalesUIStrings.DataGridView_Validation_PartsOutboundBillDetail_CurrentAmount, new[] {
                        "OutboundAmount"
                    }));
                return;
            }
            agencyRetailerList.CurrentOutQuantity = currentOutAmount;//修改本次出库数量
        }

        //public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
        //    if(subject == "Blue") {
        //        var selectedItems = this.GridView.SelectedItems;
        //        var partsOutboundBillDataEditView = this.DataContext as PartsOutboundBillDataEditView;
        //        if(partsOutboundBillDataEditView == null)
        //            return null;
        //        if(selectedItems == null || !selectedItems.Any()) {
        //            foreach(var partsOutboundBillDetail in this.GridView.Items.Cast<PartsOutboundBillDetail>().Where(r => r.SparePartId != (int)contents[0])) {
        //                var gridViewRow = this.GridView.GetRowForItem(partsOutboundBillDetail);
        //                if(gridViewRow != null)
        //                    gridViewRow.Background = partsOutboundBillDataEditView.PartsOutboundBillDetails.Count(r => r.SparePartId == partsOutboundBillDetail.SparePartId) > 1 ? new SolidColorBrush(Colors.Red) : new SolidColorBrush(Colors.White);

        //            }
        //            foreach(PartsOutboundBillDetail partsOutboundBillDetail in this.GridView.Items.Cast<PartsOutboundBillDetail>().Where(r => r.SparePartId == (int)contents[0])) {
        //                GridViewRow gridViewRow = this.GridView.GetRowForItem(partsOutboundBillDetail);
        //                if(gridViewRow != null)
        //                    gridViewRow.Background = new SolidColorBrush(Colors.Blue);
        //            }
        //        }
        //    }
        //    return null;
        //}
    }
}
