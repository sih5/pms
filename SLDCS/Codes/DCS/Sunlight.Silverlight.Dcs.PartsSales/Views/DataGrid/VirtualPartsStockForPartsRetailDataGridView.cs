﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class VirtualPartsStockForPartsRetailDataGridView : DcsDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsStock);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                       Name = "SparePartCode"
                    }, new ColumnItem {
                       Name = "SparePartName"
                    }, new ColumnItem {
                       Title =PartsSalesUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_UsableQuantity,
                       Name = "UsableQuantity"
                    }                  
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询仓库库存";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            //TODO:定义为CompositeFilterItem集合类型，取值需要CompositeFilterItem强类型转换
            if((this.FilterItem as CompositeFilterItem) != null) {
                switch(parameterName) {
                    case "storageCompanyId":
                        foreach(var storageCompanyIdFiterItem in (this.FilterItem as CompositeFilterItem).Filters.OfType<CompositeFilterItem>().Select(filterItem => (filterItem).Filters.SingleOrDefault(v => v.MemberName.Equals("StorageCompanyId")))) {
                            return storageCompanyIdFiterItem != null ? storageCompanyIdFiterItem.Value : null;
                        }
                        break;
                    case "warehouseId":
                        foreach(var storageCompanyIdFiterItem in (this.FilterItem as CompositeFilterItem).Filters.OfType<CompositeFilterItem>().Select(filterItem => (filterItem).Filters.SingleOrDefault(v => v.MemberName.Equals("WarehouseId")))) {
                            return storageCompanyIdFiterItem != null ? storageCompanyIdFiterItem.Value : null;
                        }
                        break;
                    case "partIds":
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "StorageCompanyId" && filter.MemberName != "WarehouseId"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
