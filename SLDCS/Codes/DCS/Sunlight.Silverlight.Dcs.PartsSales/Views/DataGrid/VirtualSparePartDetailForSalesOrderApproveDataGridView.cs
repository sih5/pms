﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class VirtualSparePartDetailForSalesOrderApproveDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            base.OnControlsCreated();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "IsOrderable"
                    }, new ColumnItem {
                        Name = "IsSalable"
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualSparePart);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SparePartWithPartBranchInfoes");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
