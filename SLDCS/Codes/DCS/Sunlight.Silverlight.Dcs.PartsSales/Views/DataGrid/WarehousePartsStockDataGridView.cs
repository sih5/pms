﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class WarehousePartsStockDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseCode
                    },
                    new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseName
                    }, 
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartCode
                    }, 
                    new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartName
                    }, 
                    new ColumnItem {
                        Name = "UsableQuantity",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_UsableQuantity
                    }, new ColumnItem{
                        Name="PartsSalesPrice",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsSalesPrice
                    }, 
                    new ColumnItem {
                        Name = "BranchName",
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_BranchName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WarehousePartsStock);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        protected override string OnRequestQueryName() {
            return "查询仓库库存";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "storageCompanyId":
                        return BaseApp.Current.CurrentUserData.EnterpriseId;
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "partIds":
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
