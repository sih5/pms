﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class WarehouseSequenceDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        public WarehouseSequenceDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },  new ColumnItem{
                        Name = "CustomerCompanyCode"
                    }, new ColumnItem{
                        Name = "CustomerCompanyName"
                    },new ColumnItem{
                        Name = "IsAutoApprove"
                    },new ColumnItem{
                        Name = "DefaultWarehouseName"
                    },new ColumnItem{
                        Name = "DefaultOutWarehouseName"
                    },new ColumnItem{
                        Name = "FirstWarehouseName"
                    },new ColumnItem{
                        Name = "FirstOutWarehouseName"
                    },new ColumnItem{
                        Name = "SecoundWarehouseName"
                    },new ColumnItem{
                        Name = "SecoundOutWarehouseName"
                    },new ColumnItem{
                        Name = "ThirdWarehouseName"
                    },new ColumnItem{
                        Name = "ThirdOutWarehouseName"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName", 
                        Title = PartsSalesUIStrings.DataGridView_ColumnItem_Title_DealerPartsStock_SalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WarehouseSequence);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetWarehouseSequences";
        }
    }
}
