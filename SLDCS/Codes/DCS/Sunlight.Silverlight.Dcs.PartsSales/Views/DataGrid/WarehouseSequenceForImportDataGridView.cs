﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class WarehouseSequenceForImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem {
                        Name = "CustomerCompanyCode"
                    },new ColumnItem{
                        Name="CustomerCompanyName"
                    },new ColumnItem{
                        Name="PartsSalesCategoryName"
                    },new ColumnItem{
                        Name="IsAutoApprove"
                    },new ColumnItem{
                        Name = "DefaultWarehouseName"
                    },new ColumnItem{
                        Name = "DefaultOutWarehouseName"
                    },new ColumnItem{
                        Name = "FirstWarehouseName"
                    },new ColumnItem{
                        Name = "FirstOutWarehouseName"
                    },new ColumnItem{
                        Name = "SecoundWarehouseName"
                    },new ColumnItem{
                        Name = "SecoundOutWarehouseName"
                    },new ColumnItem{
                        Name = "ThirdWarehouseName"
                    },new ColumnItem{
                        Name = "ThirdOutWarehouseName"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WarehouseSequence);
            }
        }
    }
}