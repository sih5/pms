﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid {
    public class vehiclePartsStockLevelForImportForEditDataGirdView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="BrandCode",
                        Title = "品牌"
                    },new ColumnItem{
                        Name="WarehouseCode",
                       Title ="仓库编号"
                    },new ColumnItem{
                        Name="WarehouseName",
                       Title ="仓库名称"
                    },new ColumnItem{
                        Name="PartCode",
                       Title ="配件图号"
                    },new ColumnItem{
                        Name="PartName",
                       Title ="配件名称"
                    },new ColumnItem{
                        Name="StockMaximum",
                       Title ="库存高限"
                    },new ColumnItem{
                        Name="StockMinimum",
                       Title ="库存底限"
                    },new ColumnItem{
                        Name="SafeStock",
                       Title ="安全库存"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehiclePartsStockLevel);
            }
        }
    }
}
