﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsRetail", "DealerPartsRetailOrderForApprove", ActionPanelKeys = new[] {
        CommonActionKeys.APPROVE_ABANDON_EXPORT
    })]
    public class DealerPartsRetailOrderForApproveMamagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataApproveView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_APPROVE_VIEW = "_dataApproveView_";

        public DealerPartsRetailOrderForApproveMamagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_DealerPartsRetailOrderForApprove;
        }

        public DataGridViewBase DataGridView {
            get {
                return dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerPartsRetailOrderForApprove"));
            }
        }

        public DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("DealerPartsRetailOrderForApproveForApprove");
                    this.dataApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerPartsRetailOrderForApprove"
                };
            }
        }
        private void ResetEditView() {
            this.dataApproveView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                foreach(var item in compositeFilterItem.Filters)
                    newCompositeFilterItem.Filters.Add(item);
            } else
                newCompositeFilterItem.Filters.Add(filterItem);

            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.ABANDON:
                    var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    if(domainContext == null)
                        return;
                    var dealerPartsRetailOrder = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().First();
                    domainContext.Load(domainContext.GetPersonSalesCenterLinksQuery().Where(e => e.PersonId == BaseApp.Current.CurrentUserData.UserId && e.PartsSalesCategoryId == dealerPartsRetailOrder.PartsSalesCategoryId && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        if(!loadOp.Entities.Any()) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_PartsCategoryDisagree);
                            return;
                        }
                        if(uniqueId == CommonActionKeys.APPROVE) {
                            this.DataApproveView.SetObjectToEditById(dealerPartsRetailOrder.GetIdentity());
                            this.SwitchViewTo(DATA_APPROVE_VIEW);
                        } else if(uniqueId == CommonActionKeys.ABANDON) {
                            DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                                var entity = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().SingleOrDefault();
                                if(entity == null)
                                    return;
                                try {
                                    if(entity.Can作废服务站配件零售订单)
                                        entity.作废服务站配件零售订单();
                                    domainContext.SubmitChanges(submitOp => {
                                        if(submitOp.HasError) {
                                            if(!submitOp.IsErrorHandled)
                                                submitOp.MarkErrorAsHandled();
                                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                            domainContext.RejectChanges();
                                            return;
                                        }
                                        UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                        this.CheckActionsCanExecute();
                                    }, null);
                                } catch(Exception ex) {
                                    UIHelper.ShowAlertMessage(ex.Message);
                                }
                            });
                        }
                    }, null);
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().Select(r => r.Id).ToArray();
                        this.ExporttDealerPartsRetailOrdeForApprove(ids, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var dealerName = filterItem.Filters.Single(r => r.MemberName == "DealerName").Value as string;
                        var partsSalesCategoryId = filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var retailOrderType = filterItem.Filters.Single(e => e.MemberName == "RetailOrderType").Value as int?;
                        var customer = filterItem.Filters.Single(r => r.MemberName == "Customer").Value as string;
                        var subDealerName = filterItem.Filters.Single(r => r.MemberName == "SubDealerName").Value as string;
                        var dealerCode = filterItem.Filters.Single(r => r.MemberName == "DealerCode").Value as string;
                        var createTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExporttDealerPartsRetailOrdeForApprove(null, dealerName, partsSalesCategoryId, code, retailOrderType, customer, subDealerName, dealerCode, status, createTimeBegin, createTimeEnd);
                    }
                    break;
            }
        }
        private void ExporttDealerPartsRetailOrdeForApprove(int[] ids, string dealerName, int? partsSalesCategoryId, string code, int? retailOrderType, string customer, string subDealerName, string dealerCode, int? status, DateTime? beginCreateTime, DateTime? endCreateTime) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExporttDealerPartsRetailOrdeForApproveAsync(ids, dealerName, partsSalesCategoryId, code, retailOrderType, customer, subDealerName, dealerCode, status, beginCreateTime, endCreateTime);
            this.excelServiceClient.ExporttDealerPartsRetailOrdeForApproveCompleted -= excelServiceClient_ExporttDealerPartsRetailOrdeForApproveCompleted;
            this.excelServiceClient.ExporttDealerPartsRetailOrdeForApproveCompleted += excelServiceClient_ExporttDealerPartsRetailOrdeForApproveCompleted;
        }

        private void excelServiceClient_ExporttDealerPartsRetailOrdeForApproveCompleted(object sender, ExporttDealerPartsRetailOrdeForApproveCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
