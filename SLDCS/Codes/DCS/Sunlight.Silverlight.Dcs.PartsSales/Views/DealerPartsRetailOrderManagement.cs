﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsRetail", "DealerPartsRetailOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_EXPORT,CommonActionKeys.MERGEEXPORT,CommonActionKeys.PRINT
    })]
    public class DealerPartsRetailOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataApproveView;
        private DcsDomainContext dcsDomainContext = new DcsDomainContext(); 
        private const string DATA_APPROVE_VIEW = "_dataApproveView_";
        public DataGridViewBase DataGridView {
            get {
                return dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerPartsRetailOrder"));
            }
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("DealerPartsRetailOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        public DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("DealerPartsRetailOrderForApprove");
                    this.dataApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        public DealerPartsRetailOrderManagement() {
            Title = PartsSalesUIStrings.DataManagementView_Title_DealerPartsRetailOrder;
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerPartsRetailOrder"
                };
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataApproveView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var dealerPartsRetailOrder = this.DataEditView.CreateObjectToEdit<DealerPartsRetailOrder>();
                    dealerPartsRetailOrder.Status = (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                    dealerPartsRetailOrder.SubDealerlOrderType = 0;
                    dealerPartsRetailOrder.SubDealerId = -1;
                    dealerPartsRetailOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    dealerPartsRetailOrder.DealerId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    dealerPartsRetailOrder.DealerCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    dealerPartsRetailOrder.DealerName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    dealerPartsRetailOrder.RetailOrderType = (int)DcsDealerPartsRetailOrderRetailOrderType.普通客户;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废服务站配件零售订单)
                                entity.作废服务站配件零售订单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().FirstOrDefault();
                    if (selectedItem == null)
                        return;
                    SunlightPrinter.ShowPrinter("服务站/经销商零售出库单打印", "ReportPartsRetailOrder", null, true, new Tuple<string, string>("PartsRetailOrderId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName),
                         new Tuple<string, string>("CompanyId", BaseApp.Current.CurrentUserData.EnterpriseId.ToString()));
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var ids = this.DataGridView.SelectedEntities == null ? null : this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().Select(r => r.Id).ToArray();
                    var branchId = filterItem.Filters.Single(e => e.MemberName == "BranchId").Value as int?;
                    var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                    var retailOrderType = filterItem.Filters.Single(e => e.MemberName == "RetailOrderType").Value as int?;
                    var customer = filterItem.Filters.Single(r => r.MemberName == "Customer").Value as string;
                    var subDealerName = filterItem.Filters.Single(r => r.MemberName == "SubDealerName").Value as string;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;

                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        {
                            if (dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                        }
                    }

                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.ExportDealerPartsRetailOrderWithDetails(ids, branchId, partsSalesCategoryId, code, retailOrderType, customer, subDealerName,status,createTimeBegin,createTimeEnd, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                        {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                        {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            composite.Filters.Add(new FilterItem {
                MemberName = "DealerId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    return entities1[0].Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    return this.DataGridView.Entities != null && this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
