﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core;
using System.Windows.Browser;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "DealerPartsSalesPriceQuery", ActionPanelKeys = new[] { CommonActionKeys.EXPORT })]
    public class DealerPartsSalesPriceQueryManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        public DealerPartsSalesPriceQueryManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.Management_Title_DealerPartsSalesPriceQueryManagement;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerPartsSalesPriceQuery"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                this.DataGridView.FilterItem = compositeFilterItem;
            }
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerPartsSalesPriceQuery"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "PartCode").Value as string;
                    var sparePartName = filterItem.Filters.Single(e => e.MemberName == "PartName").Value as string;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    //DateTime? createTimeBegin = null;
                    //DateTime? createTimeEnd = null;
                    //foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                    //    var dateTime = filter as CompositeFilterItem;
                    //    {
                    //        if (dateTime.Filters.First().MemberName == "CreateTime") {
                    //            createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                    //            createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    //        }
                    //    }
                    //}

                    ShellViewModel.Current.IsBusy = true;
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualDealerPartsSalesPrice>().Select(r => r.Id).ToArray();
                        dcsDomainContext.ExportDealerPartsSalesPriceQuery(ids, sparePartCode, sparePartName, status, null,null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    } else {
                        dcsDomainContext.ExportDealerPartsSalesPriceQuery(null, sparePartCode, sparePartName, status, null, null, loadOp =>
                        {
                            if (loadOp.HasError)
                                return;
                            if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }


    }
}
