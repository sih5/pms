﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources; 
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsStore", "DealerPartsStock", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT,"DealerPartsStock"
    })]
    public class DealerPartsStockManagement : DcsDataManagementViewBase {
        private readonly DcsDomainContext domaincontext = new DcsDomainContext();
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private Company company;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT: {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                      //  var salesCategoryId = filterItem.Filters.Single(e => e.MemberName == "SalesCategoryId").Value as int?;
                        var dealerName = filterItem.Filters.Single(e => e.MemberName == "DealerName").Value as string;
                        var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.Single(e => e.MemberName == "SparePartName").Value as string;
                        var companyType = filterItem.Filters.Single(e => e.MemberName == "CompanyType").Value as int?;
                        var isDealerQuery = filterItem.Filters.SingleOrDefault(s => s.MemberName == "IsDealerQuery").Value as bool?;
                        var isZero = filterItem.Filters.SingleOrDefault(s => s.MemberName == "IsZero").Value as bool?;
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                            var ids = this.DataGridView.SelectedEntities.Cast<VirtualDealerPartsStock>().Select(r => r.Id).ToArray();
                            this.ExportDealerPartsStock(ids, null, null, sparePartCode, sparePartName, companyType, dealerName, isDealerQuery, isZero);
                        } else
                            this.ExportDealerPartsStock(null, null, null, sparePartCode, sparePartName, companyType, dealerName, isDealerQuery, isZero);
                        break;
                    }
                case "InAndOutRecordQuery":
                    this.InAndOutHistoryWindow.ShowDialog();
                    break;
            }
        }

        private void ExportDealerPartsStock(int[] ids, int? salesCategoryId, int? priceType, string sparePartCode, string sparePartName, int? companyType, string dealerName, bool? isDealerQuery, bool? isZero) {
            domaincontext.ExportVirtualDealerPartsStock(ids, salesCategoryId, priceType, sparePartCode, sparePartName,companyType,dealerName,isDealerQuery,isZero, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                    ShellViewModel.Current.IsBusy = false;
                }

                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    ShellViewModel.Current.IsBusy = false;
                }
            }, null);
            //this.excelServiceClient.ExportDealerPartsStockAsync(Ids, BaseApp.Current.CurrentUserData.EnterpriseId, salesCategoryId, sparePartCode, sparePartName);
            //this.excelServiceClient.ExportDealerPartsStockCompleted -= excelServiceClient_ExportDealerPartsStockCompleted;
            //this.excelServiceClient.ExportDealerPartsStockCompleted += excelServiceClient_ExportDealerPartsStockCompleted;
        }

        void excelServiceClient_ExportDealerPartsStockCompleted(object sender, ExportDealerPartsStockCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        public DealerPartsStockManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_DealerPartsStock;

        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerPartsStockForSelect"));
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "InAndOutRecordQuery":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualDealerPartsStock>().ToArray();
                    if(entities.Length >= 1)
                        return true;
                    return false;
                default:
                    return false;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);

            this.dcsDomainContext.Load(this.dcsDomainContext.GetCompaniesQuery().Where(e => e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if (loadOp.HasError)
                    return;
                company = loadOp.Entities.SingleOrDefault();
            }, null);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerPartsStockForSelect"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;

            var codes = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode"); 
            var isDealerQuery = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "IsDealerQuery").Value as bool?;
            if (!isDealerQuery.HasValue) isDealerQuery = false;
            if (company == null)
                return;
            if (isDealerQuery.Value && (company.Type == (int)DcsCompanyType.服务站 || company.Type == (int)DcsCompanyType.服务站兼代理库)) {
                if ((codes == null || codes.Value == null) || ((IEnumerable<string>)codes.Value).ToArray().Length > 20) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_DealerPartsStockManagement_CodeCount);
                    return;
                }
            }
            if(codes != null && codes.Value != null){  
                    compositeFilterItem.Filters.Add(new CompositeFilterItem {  
                        MemberName = "SparePartCode",  
                        MemberType = typeof(string),  
                        Value =string.Join(",",((IEnumerable<string>)codes.Value).ToArray()),  
                        Operator = FilterOperator.Contains  
                    });  
                    compositeFilterItem.Filters.Remove(codes);  
                }

            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }


        //出入库记录查询
        private RadWindow inAndOutHistoryWindow;
        private RadWindow InAndOutHistoryWindow {
            get {
                return this.inAndOutHistoryWindow ?? (this.inAndOutHistoryWindow = new RadWindow {
                    Content = ExportQueryWindow,
                    Header = PartsSalesUIStrings.DataEdit_Title_DealerPartsStock,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        private DcsExportQueryWindowBase exportQueryWindow;
        private DcsExportQueryWindowBase ExportQueryWindow {
            get {
                if(this.exportQueryWindow == null) {
                    exportQueryWindow = DI.GetQueryWindow("InAndOutHistoryForExport") as DcsExportQueryWindowBase;
                    exportQueryWindow.Loaded += historicalStorage_Loaded;
                }
                return this.exportQueryWindow;
            }
        }

        private void historicalStorage_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var warehousePartsStock = this.DataGridView.SelectedEntities.Cast<VirtualDealerPartsStock>().ToArray();
            var queryWindow = sender as DcsExportQueryWindowBase;
            if(queryWindow == null || warehousePartsStock == null || !warehousePartsStock.Any())
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("SparePartIds", typeof(int), FilterOperator.IsEqualTo, warehousePartsStock.Select(r => r.SparePartId).ToArray()));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }
    }
}
