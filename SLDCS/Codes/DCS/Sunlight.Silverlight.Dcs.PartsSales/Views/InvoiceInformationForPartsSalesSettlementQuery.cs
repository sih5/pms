﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSalesSettlement", "InvoiceInformationForPartsSalesSettlementQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT_PRINT
    })]
    public class InvoiceInformationForPartsSalesSettlementQuery : DcsDataManagementViewBase {
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("InvoiceInformationForPartsSalesSettlementQuery"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        public InvoiceInformationForPartsSalesSettlementQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_InvoiceInformation;
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "InvoiceInformationForPartsSalesSettlement"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                var status = compositeFilterItem.Filters.SingleOrDefault(filter => filter.MemberName == "Status");
                if(status != null && status.Value == null) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_StatusNotNull);
                    return;
                }
            } else {
                if(filterItem.MemberName == "Status")
                    if(filterItem.Value == null) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_StatusNotNull);
                        return;
                    }
                compositeFilterItem.Filters.Add(filterItem);
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "OwnerCompanyId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.PRINT:
                    //TODO:打印暂不实现
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualInvoiceInformation>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportInvoiceInformation(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var invoiceNumber = compositeFilterItem.Filters.Single(r => r.MemberName == "InvoiceNumber").Value as string;
                            var sourceCode = compositeFilterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                            var invoiceCompanyCode = compositeFilterItem.Filters.Single(r => r.MemberName == "InvoiceCompanyCode").Value as string;
                            var invoiceCompanyName = compositeFilterItem.Filters.Single(r => r.MemberName == "InvoiceCompanyName").Value as string;
                            var invoiceReceiveCompanyCode = compositeFilterItem.Filters.Single(r => r.MemberName == "InvoiceReceiveCompanyCode").Value as string;
                            var invoiceReceiveCompanyName = compositeFilterItem.Filters.Single(r => r.MemberName == "InvoiceReceiveCompanyName").Value as string;
                            var partssalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as Nullable<int>;
                            DateTime? createTimeStart = null;
                            DateTime? createTimeEnd = null;
                            DateTime? invoiceDateStart = null;
                            DateTime? invoiceDateEnd = null;
                            var dateTimeFilterItems = compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem));
                            foreach(var dateTimeFilterItem in dateTimeFilterItems) {
                                var dateTime = dateTimeFilterItem as CompositeFilterItem;
                                if(dateTime != null) {
                                    if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                        createTimeStart = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }
                                    if(dateTime.Filters.Any(r => r.MemberName == "InvoiceDate")) {
                                        invoiceDateStart = dateTime.Filters.First(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                        invoiceDateEnd = dateTime.Filters.Last(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                    }
                                }
                            }
                            var creatorName = compositeFilterItem.Filters.Single(r => r.MemberName == "CreatorName").Value as string;
                            var status = compositeFilterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                            this.dcsDomainContext.ExportInvoiceInformation(new int[] { }, partssalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId, (int)DcsInvoiceInformationInvoicePurpose.配件销售, invoiceNumber, sourceCode, invoiceCompanyCode, invoiceCompanyName, invoiceReceiveCompanyCode, invoiceReceiveCompanyName, createTimeStart, createTimeEnd, invoiceDateStart, invoiceDateEnd, creatorName, status, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    //TODO:打印暂不实现
                    return false;
                default:
                    return false;
            }
        }
    }
}
