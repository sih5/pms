﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "IvecoPriceChangeApp", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_SUBMIT_ABANDON_MERGEEXPORT,WorkflowActionKey.INITIALAPPROVE_FINALAPPROVE
    })]
    public class IvecoPriceChangeAppManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public IvecoPriceChangeAppManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.Management_Title_IvecoPriceChangeAppManagement;
        }
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditForApproveView;
        private const string DATA_EEDITFORAPPORVE_VIEW = "_DataEditForApproveView_";
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("IvecoPriceChangeApp");
                    this.dataEditView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private DataEditViewBase DataEditForApproveView {
            get {
                if(this.dataEditForApproveView == null) {
                    this.dataEditForApproveView = DI.GetDataEditView("IvecoPriceChangeAppForApprove");
                    this.dataEditForApproveView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditForApproveView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditForApproveView;
            }
        }

        private RadWindow abandodRadWindow;

        private RadWindow AbandodRadWindow {
            get {
                if(this.abandodRadWindow == null) {
                    this.abandodRadWindow = new RadWindow();
                    this.abandodRadWindow.CanClose = false;
                    this.abandodRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.abandodRadWindow.Content = this.AbandodDataEditView;
                    this.abandodRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.abandodRadWindow.Height = 200;
                    this.abandodRadWindow.Width = 400;
                    this.abandodRadWindow.Header = "";
                }
                return this.abandodRadWindow;
            }
        }
        private DataEditViewBase abandodDataEditView;

        private DataEditViewBase AbandodDataEditView {
            get {
                if(this.abandodDataEditView == null) {
                    this.abandodDataEditView = DI.GetDataEditView("IvecoPriceChangeAppForTerminate");
                    this.abandodDataEditView.EditCancelled += AbandodDataEditView_EditCancelled;
                    this.abandodDataEditView.EditSubmitted += AbandodDataEditView_EditSubmitted;
                }
                return this.abandodDataEditView;
            }
        }

        private void AbandodDataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.AbandodRadWindow.Close();
        }

        private void AbandodDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.AbandodRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditForApproveView = null;
            this.abandodDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("IvecoPriceChangeApp"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EEDITFORAPPORVE_VIEW, () => this.DataEditForApproveView);

        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "IvecoPriceChangeApp"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case WorkflowActionKey.INITIALAPPROVE:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<IvecoPriceChangeApp>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsIvecoPriceAppStatus.新增;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entitiess = this.DataGridView.SelectedEntities.Cast<IvecoPriceChangeApp>().ToArray();
                    if(entitiess.Length != 1)
                        return false;
                    return entitiess[0].Status == (int)DcsIvecoPriceAppStatus.新增 || entitiess[0].Status == (int)DcsIvecoPriceAppStatus.初审通过;

                case WorkflowActionKey.FINALAPPROVE:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<IvecoPriceChangeApp>().ToArray();
                    if(entitie.Length != 1)
                        return false;
                    return entitie[0].Status == (int)DcsIvecoPriceAppStatus.初审通过;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var ivecoPriceChangeApp = this.DataEditView.CreateObjectToEdit<IvecoPriceChangeApp>();
                    ivecoPriceChangeApp.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    ivecoPriceChangeApp.Status = (int)DcsIvecoPriceAppStatus.新增;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case WorkflowActionKey.FINALAPPROVE:
                    this.DataEditForApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORAPPORVE_VIEW);
                    break;
                case WorkflowActionKey.INITIALAPPROVE:
                    this.DataEditForApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORAPPORVE_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    this.AbandodDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.AbandodRadWindow.ShowDialog();
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<IvecoPriceChangeApp>().Select(r => r.Id).ToArray();
                        if(uniqueId == CommonActionKeys.MERGEEXPORT) {
                            this.ExportIvecoPriceChangeAppDetail(ids, null, null, null, null, null);
                        } else {
                            this.ExportIvecoPriceChangeApp(ids, null, null, null, null, null);
                        }
                    } else {
                        var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filtes == null)
                            return;
                        var code = filtes.Filters.SingleOrDefault(e => e.MemberName == "Code").Value as string;
                        var partsSalesCategoryId = filtes.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filtes.Filters.SingleOrDefault(e => e.MemberName == "Status").Value as int?;
                        var createTime = filtes.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        if(uniqueId == CommonActionKeys.MERGEEXPORT) {
                            this.ExportIvecoPriceChangeAppDetail(null, partsSalesCategoryId, code, status, createTimeBegin, createTimeEnd);
                        } else {
                            this.ExportIvecoPriceChangeApp(null, partsSalesCategoryId, code, status, createTimeBegin, createTimeEnd);
                        }
                    }
                    break;
            }
        }
        private void ExportIvecoPriceChangeApp(int[] ids, int? partsSalesCategoryId, string code, int? status, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd) {
            //ShellViewModel.Current.IsBusy = true;
            //this.excelServiceClient.ExportPartsSalesPriceChangeAsync(ids, partsSalesCategoryId, code, status, creatorTimeBegin, creatorTimeEnd);
            //this.excelServiceClient.ExportPartsSalesPriceChangeCompleted += ExcelServiceClient_ExportPartsSalesPriceChangeCompleted;
            //this.excelServiceClient.ExportPartsSalesPriceChangeCompleted += ExcelServiceClient_ExportPartsSalesPriceChangeCompleted;
        }

        //private void ExcelServiceClient_ExportIvecoPriceChangeAppCompleted(object sender, ExportPartsSalesPriceChangeCompletedEventArgs e) {
        //    ShellViewModel.Current.IsBusy = false;
        //    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        //}


        private void ExportIvecoPriceChangeAppDetail(int[] ids, int? partsSalesCategoryId, string code, int? status, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd) {
            //ShellViewModel.Current.IsBusy = true;
            //this.excelServiceClient.ExportPartsSalesPriceChangeDetailAsync(ids, partsSalesCategoryId, code, status, creatorTimeBegin, creatorTimeEnd);
            //this.excelServiceClient.ExportPartsSalesPriceChangeDetailCompleted -= ExcelServiceClient_ExportPartsSalesPriceChangeDetailCompleted;
            //this.excelServiceClient.ExportPartsSalesPriceChangeDetailCompleted += ExcelServiceClient_ExportPartsSalesPriceChangeDetailCompleted;
        }

        //private void ExcelServiceClient_ExportPartsSalesPriceChangeDetailCompleted(object sender, ExportPartsSalesPriceChangeDetailCompletedEventArgs e) {
        //    ShellViewModel.Current.IsBusy = false;
        //    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        //}
    }
}
