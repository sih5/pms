﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "OrderApproveWeekday", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT,"OrderApproveWeekday"
    })]
    public class OrderApproveWeekdayManagement : DcsDataManagementViewBase {
        private DataGridViewBase orderApproveWeekdayDataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase OrderApproveWeekdayDataGridView {
            get {
                return this.orderApproveWeekdayDataGridView ?? (this.orderApproveWeekdayDataGridView = DI.GetDataGridView("OrderApproveWeekday"));
            }
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("OrderApproveWeekday");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("OrderApproveWeekdayForImport");
                    this.dataEditViewImport.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.OrderApproveWeekdayDataGridView.FilterItem != null)
                this.OrderApproveWeekdayDataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public OrderApproveWeekdayManagement() {
            this.Title = PartsSalesUIStrings.Management_Title_OrderApproveWeekdayManagement;
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.OrderApproveWeekdayDataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.OrderApproveWeekdayDataGridView.FilterItem = filterItem;
            this.OrderApproveWeekdayDataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var salesUnit = this.DataEditView.CreateObjectToEdit<OrderApproveWeekday>();
                    salesUnit.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.OrderApproveWeekdayDataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.OrderApproveWeekdayDataGridView.SelectedEntities.Cast<OrderApproveWeekday>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废储备订单自动审核日与省份关系)
                                entity.作废储备订单自动审核日与省份关系();
                            var domainContext = this.OrderApproveWeekdayDataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.OrderApproveWeekdayDataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var weeksName = filterItem.Filters.Single(r => r.MemberName == "WeeksName").Value as int?;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    DateTime? modifyTimeBegin = null;
                    DateTime? modifyTimeEnd = null;
                    var dateTimeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem));
                    foreach(var dateTimeFilterItem in dateTimeFilterItems) {
                        var dateTime = dateTimeFilterItem as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.Any(r => r.MemberName == "ModifyTime")) {
                                modifyTimeBegin = dateTime.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?;
                                modifyTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?;
                            }
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportOrderApproveWeekday(partsSalesCategoryId, weeksName, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd, status);
                    break;
                case "ImportEdit":
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case "ImportEdit":
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.OrderApproveWeekdayDataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.OrderApproveWeekdayDataGridView.SelectedEntities.Cast<OrderApproveWeekday>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.OrderApproveWeekdayDataGridView.Entities != null && this.OrderApproveWeekdayDataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "OrderApproveWeekday"
                };
            }
        }

        private void ExportOrderApproveWeekday(int? partsSaleCategoryId, int? weeksName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, int? status) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportOrderapproveweekdayAsync(weeksName, partsSaleCategoryId, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd, status);
            this.excelServiceClient.ExportOrderapproveweekdayCompleted -= excelServiceClient_ExportOrderapproveweekdayCompleted;
            this.excelServiceClient.ExportOrderapproveweekdayCompleted += excelServiceClient_ExportOrderapproveweekdayCompleted;
        }

        void excelServiceClient_ExportOrderapproveweekdayCompleted(object sender, ExportOrderapproveweekdayCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
