﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsSales", "BacklogPartsPlatform", "OverstockPartsRecommendBill", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT,CommonActionKeys.EXPORT
    })]
    public class OverstockPartsRecommendBillMangement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public OverstockPartsRecommendBillMangement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.Management_Title_OverstockPartsRecommendBillMangement;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("OverstockPartsRecommendBillWithDetails"));
            }
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("OverstockPartsRecommendBill");
                    ((OverstockPartsRecommendBillDataEditView)this.dataEditView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            var dealerCode = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "DealerCode");
            var dealerName = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "DealerName");
            compositeFilterItem.Filters.Add(new CompositeFilterItem {
                MemberName = "DealerCode",
                MemberType = typeof(string),
                Value = dealerCode.Value,
                Operator = FilterOperator.Contains
            });
            compositeFilterItem.Filters.Remove(dealerCode);
            compositeFilterItem.Filters.Add(new CompositeFilterItem {
                MemberName = "DealerName",
                MemberType = typeof(string),
                Value = dealerName.Value,
                Operator = FilterOperator.Contains
            });
            compositeFilterItem.Filters.Remove(dealerName);
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.SUBMIT:
                    var partspackingEdit = this.DataGridView.SelectedEntities.Cast<OverstockPartsRecommendBill>().ToArray();
                    this.DataEditView.SetObjectToEditById(partspackingEdit);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var sparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                    var centerName = filterItem.Filters.Single(e => e.MemberName == "CenterName").Value as string;
                    var dealerName = filterItem.Filters.Single(e => e.MemberName == "DealerName").Value as string;
                    var dealerCode = filterItem.Filters.Single(e => e.MemberName == "DealerCode").Value as string;

                    ShellViewModel.Current.IsBusy = true;
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<OverstockPartsRecommendBill>().Select(r => r.Id).Distinct().ToArray();
                        dcsDomainContext.ExportOverstockPartsRecommendBills(ids, centerName,dealerCode, dealerName, sparePartCode, sparePartName, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    } else {
                        dcsDomainContext.ExportOverstockPartsRecommendBills(null, centerName,dealerCode, dealerName, sparePartCode, sparePartName, loadOp => {
                            if (loadOp.HasError)
                                return;
                            if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<OverstockPartsRecommendBill>().ToArray();
                    if(selectItems.Count() > 0 && (selectItems.All(r => r.CenterId == BaseApp.Current.CurrentUserData.EnterpriseId) || selectItems.All(r => r.DealerId == BaseApp.Current.CurrentUserData.EnterpriseId)))
                        return true;
                    return false;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "OverstockPartsRecommendBill"
                };
            }
        }
    }
}