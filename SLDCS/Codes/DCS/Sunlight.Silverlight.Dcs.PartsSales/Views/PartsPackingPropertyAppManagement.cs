﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "PartsPackingPropertyApp", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT_DETAIL,CommonActionKeys.APPROVE,CommonActionKeys.MERGEEXPORT})]
    public class PartsPackingPropertyAppManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataAuditView;
        private const string DATA_DETAIL_VIEW = "_dataDetailView_";
        private const string DATA_AUDIT_VIEW = "_dataEditView_";
        public PartsPackingPropertyAppManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsPackingProperty;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailView);
            this.RegisterView(DATA_AUDIT_VIEW, () => this.DataAuditView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsPackingPropertyApp"));
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsPackingPropertyApp");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private DataEditViewBase DataAuditView {
            get {
                if(this.dataAuditView == null) {
                    this.dataAuditView = DI.GetDataEditView("PartsPackingPropertyAppApprove");
                    this.dataAuditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataAuditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataAuditView;
            }
        }
        private DataEditViewBase DataDetailView {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsPackingPropertyAppForDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return dataDetailView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataAuditView = null;
            this.dataDetailView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPackingPropertyApp"
                };
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partspacking = this.DataEditView.CreateObjectToEdit<PartsPackingPropertyApp>();
                    partspacking.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partspacking.Status = (int)DcsPackingPropertyAppStatus.新增;
                    partspacking.FirIsBoxStandardPrint = 0;
                    partspacking.FirIsPrintPartStandard = 0;
                    partspacking.SecIsBoxStandardPrint = 0;
                    partspacking.SecIsPrintPartStandard = 0;
                    partspacking.ThidIsBoxStandardPrint = 0;
                    partspacking.ThidIsPrintPartStandard = 0;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    var partspackingEdit = this.DataGridView.SelectedEntities.Cast<PartsPackingPropertyAppQuery>().FirstOrDefault();
                    if((int)partspackingEdit.Status != (int)DcsPackingPropertyAppStatus.新增) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_PartsPackingPropertyAppManagement_partspackingEdit);
                        return;
                    }
                    this.DataEditView.SetObjectToEditById(partspackingEdit.GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.DETAIL:
                    this.DataDetailView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataAuditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_AUDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsPackingPropertyAppQuery>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废包装属性申请单)
                                entity.作废包装属性申请单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsPackingPropertyAppQuery>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交包装属性申请单)
                                entity.提交包装属性申请单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsPackingPropertyAppQuery>().Select(r => r.Id).ToArray();
                        this.ExportPartsPackingPropertyApp(ids, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var spareCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SpareCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SpareCode").Value as string;
                        var spareName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SpareName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SpareName").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var mainPackingType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "MainPackingType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "MainPackingType").Value as int?;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;

                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }

                        this.ExportPartsPackingPropertyApp(null, code, spareCode, spareName, status, mainPackingType, createTimeBegin, createTimeEnd);
                    }
                    break;

            }

        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public void ExportPartsPackingPropertyApp(int[] ids, string code, string spareCode, string spareName, int? status, int? mainPackingType,
            DateTime? createTimeBegin, DateTime? createTimeEnd) {

            this.excelServiceClient.ExportPartsPackingPropertyAppAsync(ids, code, spareCode, spareName, status, mainPackingType, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportPartsPackingPropertyAppCompleted -= ExcelServiceClient_ExportPartsPackingPropertyAppCompleted;
            this.excelServiceClient.ExportPartsPackingPropertyAppCompleted += ExcelServiceClient_ExportPartsPackingPropertyAppCompleted;
        }
        private void ExcelServiceClient_ExportPartsPackingPropertyAppCompleted(object sender, ExportPartsPackingPropertyAppCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.DETAIL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<PartsPackingPropertyAppQuery>().ToArray();
                    return entitie.Length == 1;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsPackingPropertyAppQuery>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPackingPropertyAppStatus.新增;
                case CommonActionKeys.MERGEEXPORT:
                    return true;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var audit = this.DataGridView.SelectedEntities.Cast<PartsPackingPropertyAppQuery>().ToArray();
                    if(audit.Length != 1)
                        return false;
                    return audit[0].Status == (int)DcsPackingPropertyAppStatus.提交;
                default:
                    return false;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}

