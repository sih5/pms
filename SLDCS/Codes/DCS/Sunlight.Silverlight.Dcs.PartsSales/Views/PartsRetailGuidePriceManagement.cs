﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "PartsRetailGuidePrice")]
    public class PartsRetailGuidePriceManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public PartsRetailGuidePriceManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsRetailGuidePrice;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsRetailGuidePriceWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            composite.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsRetailGuidePrice"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            throw new System.NotImplementedException();
        }
    }
}
