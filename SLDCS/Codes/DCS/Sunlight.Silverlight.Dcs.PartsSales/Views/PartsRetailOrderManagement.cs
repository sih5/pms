﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsRetail", "PartsRetailOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_MERGEEXPORT,CommonActionKeys.PRINT, "PartsRetailOrder"
    })]
    public class PartsRetailOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();


        public PartsRetailOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsRetailOrder;

        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsRetailOrderWithDetails"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsRetailOrder");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e)
        {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsRetailOrder"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem)
                newCompositeFilterItem = filterItem as CompositeFilterItem;
            else
                newCompositeFilterItem.Filters.Add(filterItem);
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SalesUnitOwnerCompanyId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                case "MakeInvoice":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsRetailOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId.Equals("MakeInvoice"))
                        return entities[0].Status == (int)DcsPartsRetailOrderStatus.审批;
                    return entities[0].Status == (int)DcsPartsRetailOrderStatus.新建;
                default:
                    return false;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    return this.DataGridView.Entities != null && this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsRetailOrder = this.DataEditView.CreateObjectToEdit<PartsRetailOrder>();
                    partsRetailOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsRetailOrder.Status = (int)DcsPartsRetailOrderStatus.新建;
                    partsRetailOrder.DiscountRate = 1;
                    partsRetailOrder.SalesUnitOwnerCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsRetailOrder.SalesUnitOwnerCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsRetailOrder.SalesUnitOwnerCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    //以下字段由服务端填充，赋值以避免验证不通过
                    partsRetailOrder.RetailCustomerCompanyCode = "empty";
                    partsRetailOrder.RetailCustomerCompanyName = "empty";
                    partsRetailOrder.SalesUnitCode = "empty";
                    partsRetailOrder.SalesUnitName = "empty";

                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsRetailOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can审批配件零售订单)
                                entity.审批配件零售订单();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_ApproveSuccess);

                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsRetailOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件零售订单)
                                entity.作废配件零售订单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "MakeInvoice":
                    var t = this.DataGridView.SelectedEntities.Cast<PartsRetailOrder>().SingleOrDefault();
                    if(t == null)
                        return;
                    var dcsDomainContext = new DcsDomainContext();
                    dcsDomainContext.Load(dcsDomainContext.GetPartsOutboundPlanByIdQuery(t.Code), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var partsOutboundPlan = loadOp.Entities.SingleOrDefault();
                        if(partsOutboundPlan == null) {
                            return;
                        }
                        if(partsOutboundPlan.Status == (int)DcsPartsOutboundPlanStatus.出库完成) {
                            DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_MakeInvoice, () => this.DataGridView.UpdateSelectedEntities(entity => {
                                ((PartsRetailOrder)entity).Status = (int)DcsPartsRetailOrderStatus.开票;
                                ((PartsRetailOrder)entity).InvoiceOperatorName = BaseApp.Current.CurrentUserData.UserName;
                                ((PartsRetailOrder)entity).InvoiceTime = DateTime.Now;
                            }, () => {
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_MakeInvoiceSuccess);
                                this.CheckActionsCanExecute();
                            }));
                        } else {
                            DcsUtils.Confirm(PartsSalesUIStrings.Management_Validation_PartsRetailOrderManagement_MakeInvoice, () => this.DataGridView.UpdateSelectedEntities(entity => {
                                ((PartsRetailOrder)entity).Status = (int)DcsPartsRetailOrderStatus.开票;
                                ((PartsRetailOrder)entity).InvoiceOperatorName = BaseApp.Current.CurrentUserData.UserName;
                                ((PartsRetailOrder)entity).InvoiceTime = DateTime.Now;
                            }, () => {
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_MakeInvoiceSuccess);
                                this.CheckActionsCanExecute();
                            }));
                        }

                    }, null);
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsRetailOrder>().Select(r => r.Id).ToArray();
                        this.ExportPartsRetailReturnBillWith(ids, null, null, null, null, null, null,null,null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var customerName = filterItem.Filters.Single(r => r.MemberName == "CustomerName").Value as string;
                        var customerCellPhone = filterItem.Filters.Single(r => r.MemberName == "CustomerCellPhone").Value as string;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var createTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportPartsRetailReturnBillWith(null, customerName, warehouseId, customerCellPhone, status,code,sparePartCode, createTimeBegin, createTimeEnd);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsRetailOrder>().FirstOrDefault();
                    if (selectedItem == null)
                        return;
                    SunlightPrinter.ShowPrinter(PartsSalesUIStrings.Management_Validation_PartsRetailOrderManagement_PrintTitle, "ReportPartsRetailOrder", null, true, new Tuple<string, string>("PartsRetailOrderId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName),
                         new Tuple<string, string>("CompanyId", BaseApp.Current.CurrentUserData.EnterpriseId.ToString()));
                    break;
            }
        }

        private void ExportPartsRetailReturnBillWith(int[] ids, string customerName, int? warehouseId, string customerCellPhone, int? status,string code,string sparePartCode, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsRetailOrderWithDetailAsync(ids, customerName, warehouseId, customerCellPhone, status,code,sparePartCode, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportPartsRetailOrderWithDetailCompleted -= excelServiceClient_ExportPartsRetailOrderWithDetailCompleted;
            this.excelServiceClient.ExportPartsRetailOrderWithDetailCompleted += excelServiceClient_ExportPartsRetailOrderWithDetailCompleted;
        }

        private void excelServiceClient_ExportPartsRetailOrderWithDetailCompleted(object sender, ExportPartsRetailOrderWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
    }
}