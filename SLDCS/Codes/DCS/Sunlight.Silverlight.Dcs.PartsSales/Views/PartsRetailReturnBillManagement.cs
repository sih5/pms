﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsRetail", "PartsRetailReturnBill", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_MERGEEXPORT, "PartsRetailReturn"
    })]
    public class PartsRetailReturnBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public PartsRetailReturnBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsRetailReturnBill;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsRetailReturnBillWithDetails"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsRetailReturnBill");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => DataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem)
                newCompositeFilterItem = filterItem as CompositeFilterItem;
            else
                newCompositeFilterItem.Filters.Add(filterItem);
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SalesUnitOwnerCompanyId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsRetailReturnBill"
                };
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsRetailReturnBill = this.DataEditView.CreateObjectToEdit<PartsRetailReturnBill>();
                    partsRetailReturnBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsRetailReturnBill.Status = (int)DcsPartsRetailReturnBillStatus.新建;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => this.DataGridView.UpdateSelectedEntities(entity => ((PartsRetailReturnBill)entity).Status = (int)DcsPartsRetailReturnBillStatus.作废, () => {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsRetailReturnBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can审批配件零售退货单)
                                entity.审批配件零售退货单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Refund":
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Refund, () => this.DataGridView.UpdateSelectedEntities(entity => ((PartsRetailReturnBill)entity).Status = (int)DcsPartsRetailReturnBillStatus.退款, () => {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_RefundSuccess);
                        this.CheckActionsCanExecute();
                    }));
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsRetailReturnBill>().Select(r => r.Id).ToArray();
                        if(ids.Any())
                            this.ExportPartsRetailReturnBillWith(ids, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var customerName = filterItem.Filters.Single(r => r.MemberName == "CustomerName").Value as string;
                        var customerCellPhone = filterItem.Filters.Single(r => r.MemberName == "CustomerCellPhone").Value as string;
                        var createTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportPartsRetailReturnBillWith(null, customerName, customerCellPhone, createTimeBegin, createTimeEnd);
                    }
                    break;
            }
        }
        private void ExportPartsRetailReturnBillWith(int[] ids, string customerName, string customerCellPhone, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsRetailReturnBillWithDetailAsync(ids, customerName, customerCellPhone, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportPartsRetailReturnBillWithDetailCompleted -= excelServiceClient_ExportPartsRetailReturnBillWithDetailCompleted;
            this.excelServiceClient.ExportPartsRetailReturnBillWithDetailCompleted += excelServiceClient_ExportPartsRetailReturnBillWithDetailCompleted;
        }

        private void excelServiceClient_ExportPartsRetailReturnBillWithDetailCompleted(object sender, ExportPartsRetailReturnBillWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsRetailReturnBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPartsRetailReturnBillStatus.新建;
                case "Refund":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<PartsRetailReturnBill>().ToArray();
                    if(entity.Length != 1)
                        return false;
                    return entity[0].Status == (int)DcsPartsRetailReturnBillStatus.审批;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
