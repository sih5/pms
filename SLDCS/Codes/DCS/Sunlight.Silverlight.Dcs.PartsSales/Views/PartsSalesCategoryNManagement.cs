﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "PartsSalesCategory", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT
    })]
    public class PartsSalesCategoryNManagement :DcsDataManagementViewBase{
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesCategoryN"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSalesCategoryN");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem!=null) 
                DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesCategoryN"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                   var partsSalesCategory= this.DataEditView.CreateObjectToEdit<PartsSalesCategory>();
                   partsSalesCategory.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                   partsSalesCategory.BranchCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                   partsSalesCategory.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
                   partsSalesCategory.Status = (int)DcsBaseDataStatus.有效;
                   partsSalesCategory.IsNotWarranty = false;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesCategory>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            entity.Status = (int)DcsBaseDataStatus.作废;
                            if(entity.Can作废配件销售类型)
                                entity.作废配件销售类型();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    //暂不实现
                    //((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsSalesCategory>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    //return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                    return false;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        public PartsSalesCategoryNManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataEdit_BusinessName_PartsSalesCategoryN;//PartsSalesUIStrings.DataManagementView_Title_PartsSalesCategory;
        }

    }
}
