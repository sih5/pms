﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "PartsSalesOrderForExport", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT_APPROVE_EXPORT,CommonActionKeys.AUDIT_TERMINATE
    })]
    public class PartsSalesOrderForExportManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView, dataApproveView;
        private const string DATA_APPROVE_VIEW = "DataApproveView";
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PartsSalesOrderForExportManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesOrderForExport;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesOrderForExport"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSalesOrderForExport");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        //审批
        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("PartsSalesOrderForExportApprove");
                    this.dataApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        //终止弹出
        private RadWindow terminateRadWindow;
        private RadWindow TerminateRadWindow {
            get {
                return this.terminateRadWindow ?? (this.terminateRadWindow = new RadWindow {
                    CanClose = false,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    Content = this.TerminateDataEditView,
                    ResizeMode = ResizeMode.NoResize,
                    Height = 200,
                    Width = 400,
                    Header = ""
                });
            }
        }

        private DataEditViewBase terminateDataEditView;

        private DataEditViewBase TerminateDataEditView {
            get {
                if(this.terminateDataEditView == null) {
                    this.terminateDataEditView = DI.GetDataEditView("PartsSalesOrderForTerminate");
                    this.terminateDataEditView.EditCancelled += terminateDataEditView_EditCancelled;
                    this.terminateDataEditView.EditSubmitted += terminateDataEditView_EditSubmitted;
                }
                return this.terminateDataEditView;
            }
        }

        private void terminateDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.TerminateRadWindow.Content = this.TerminateDataEditView;
            this.TerminateRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void terminateDataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.TerminateRadWindow.Content = this.TerminateDataEditView;
            this.TerminateRadWindow.Close();
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataApproveView = null;
            this.terminateDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "IsExport",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = 1,
                IsExact = true,
                IsCaseSensitive = true
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesOrderForExport"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsSalesOrder = this.DataEditView.CreateObjectToEdit<PartsSalesOrder>();
                    partsSalesOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsSalesOrder.IsExport = 1;
                    partsSalesOrder.IfAgencyService = false;
                    partsSalesOrder.IfDirectProvision = false;
                    partsSalesOrder.SalesActivityDiscountRate = 1;
                    partsSalesOrder.SalesActivityDiscountAmount = 0;
                    partsSalesOrder.WarehouseId = 0;
                    partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.新增;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件销售订单)
                                entity.作废配件销售订单();
                            var domainContextAbandon = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContextAbandon == null)
                                return;
                            domainContextAbandon.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContextAbandon.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.SUBMIT:
                    var entitys = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().SingleOrDefault();
                    if(entitys == null)
                        return;
                    var dataGridDomainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    if(dataGridDomainContext == null)
                        return;
                    dataGridDomainContext.Load(dataGridDomainContext.获取客户可用资金Query(entitys.Id), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }
                        }
                        if(loadOp.Entities != null && loadOp.Entities.Any()) {
                            var customerAccount = loadOp.Entities.First();
                            if(entitys.TotalAmount > customerAccount.OrderUseablePosition) {
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_TotalAmountMoreUseablePosition);
                            } else {
                                this.PartsSalesOrderSubmit(entitys, dataGridDomainContext);
                            }
                        }
                    }, null);
                    break;
                case CommonActionKeys.APPROVE:
                    var salesOrder = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().First();
                    if(salesOrder.IfDirectProvision) {
                        //this.DataApproveForDirectView.SetObjectToEditById(salesOrder.Id);
                        //this.SwitchViewTo(DATA_DIRECT_VIEW);
                    } else {
                        this.DataApproveView.SetObjectToEditById(salesOrder.Id);
                        this.SwitchViewTo(DATA_APPROVE_VIEW);
                    }
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var id = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().First().Id;
                        this.dcsDomainContext.ExportPartsSalesOrderForExport(id, null, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var contractCode = filterItem.Filters.Single(r => r.MemberName == "ContractCode").Value as string;
                        var partsSalesOrderTypeId = filterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderTypeId").Value as int?;
                        //var salesCategoryId = filterItem.Filters.Single(e => e.MemberName == "SalesCategoryId").Value as int?;
                        var receivingCompanyCode = filterItem.Filters.Single(r => r.MemberName == "ReceivingCompanyCode").Value as string;
                        var receivingCompanyName = filterItem.Filters.Single(r => r.MemberName == "ReceivingCompanyName").Value as string;
                        var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.dcsDomainContext.ExportPartsSalesOrderForExport(null, contractCode, receivingCompanyCode, receivingCompanyName, partsSalesOrderTypeId, null, status, createTimeBegin, createTimeEnd, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    }
                    break;
                case CommonActionKeys.TERMINATE:
                    this.TerminateDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.TerminateRadWindow.ShowDialog();
                    break;
            }
        }

        private void PartsSalesOrderSubmit(PartsSalesOrder entitys, DcsDomainContext dataGridDomainContext) {
            DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Submit, () => {
                ShellViewModel.Current.IsBusy = true;
                //最小起订金额 是 特急和紧急  这两个类型不校验
                if(entitys.PartsSalesOrderTypeName != PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_Urgent && entitys.PartsSalesOrderTypeName != PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_ExtraUrgent && entitys.IfDirectProvision) {
                    this.dcsDomainContext.校验订货配件起订金额是否满足(entitys, invokeOp => {
                        if(invokeOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(int.Parse(invokeOp.Value.ToString()) == 1) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_PartsSalesOrder_InitialAmount, 5);
                            return;
                        }
                        try {
                            if(entitys.Can提交配件销售订单)
                                entitys.提交配件销售订单();

                            dataGridDomainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    ShellViewModel.Current.IsBusy = false;
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    dataGridDomainContext.RejectChanges();
                                    return;
                                }
                                if(int.Parse(invokeOp.Value.ToString()) == 2) {
                                    UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_PartsSalesOrder_InitialAmountPart, 5);
                                } else {
                                    UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess);
                                }
                                ShellViewModel.Current.IsBusy = false;
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    }, null);
                } else {
                    try {
                        if(entitys.Can提交配件销售订单)
                            entitys.提交配件销售订单();
                        dataGridDomainContext.SubmitChanges(submitOp => {
                            if(submitOp.HasError) {
                                if(!submitOp.IsErrorHandled)
                                    submitOp.MarkErrorAsHandled();
                                ShellViewModel.Current.IsBusy = false;
                                DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                dataGridDomainContext.RejectChanges();
                                return;
                            }
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess);
                            ShellViewModel.Current.IsBusy = false;
                            this.CheckActionsCanExecute();
                        }, null);
                    } catch(Exception ex) {
                        ShellViewModel.Current.IsBusy = false;
                        UIHelper.ShowAlertMessage(ex.Message);
                    }
                }
            });
        }

        protected override
            bool OnRequestActionCanExecute
            (ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.TERMINATE:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId.Equals(CommonActionKeys.ABANDON))
                        return entities[0].Status == (int)DcsPartsSalesOrderStatus.新增;
                    if((String.CompareOrdinal(uniqueId, CommonActionKeys.EDIT) == 0))
                        return (entities[0].Status == (int)DcsPartsSalesOrderStatus.新增 || entities[0].Status == (int)DcsPartsSalesOrderStatus.撤单);
                    if(String.CompareOrdinal(uniqueId, CommonActionKeys.SUBMIT) == 0)
                        return (entities[0].Status == (int)DcsPartsSalesOrderStatus.新增);
                    if(String.CompareOrdinal(uniqueId, CommonActionKeys.TERMINATE) == 0)
                        return (entities[0].Status == (int)DcsPartsSalesOrderStatus.部分审批 || entities[0].Status == (int)DcsPartsSalesOrderStatus.提交);
                    return false;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().Any(r => r.Status == (int)DcsPartsSalesOrderStatus.提交 || r.Status == (int)DcsPartsSalesOrderStatus.部分审批);
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
