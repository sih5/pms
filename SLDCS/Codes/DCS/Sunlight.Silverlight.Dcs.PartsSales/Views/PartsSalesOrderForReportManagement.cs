﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "PartsSalesOrderForReport", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_SUBMIT_REVOKE_ABANDON_MERGEEXPORT,"LogisticsQuery"
    })]
    public class PartsSalesOrderForReportManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        private DataGridViewBase logisticsDataGridView;
        private const string DATA_LOGISTICS_VIEW = "_DATALOGISTICSVIEW_";
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PartsSalesOrderForReportManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesOrderForReport;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesOrderForReportWithDetails"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSalesOrderForReport");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private DataGridViewBase LogisticsDataGridView {
            get {
                return this.logisticsDataGridView ?? (this.logisticsDataGridView = DI.GetDataGridView("Logistics"));
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_LOGISTICS_VIEW, () => this.LogisticsDataGridView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = new CompositeFilterItem();
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SubmitCompanyId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                ClientVar.ConvertTime(compositeFilterItem);
                newCompositeFilterItem.Filters.Add(compositeFilterItem);
            }
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesOrderForReport"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsSalesOrder = this.DataEditView.CreateObjectToEdit<PartsSalesOrder>();
                    partsSalesOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsSalesOrder.SubmitCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsSalesOrder.SubmitCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsSalesOrder.SubmitCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsSalesOrder.InvoiceReceiveCompanyId = partsSalesOrder.SubmitCompanyId;
                    partsSalesOrder.InvoiceReceiveCompanyCode = partsSalesOrder.SubmitCompanyCode;
                    partsSalesOrder.InvoiceReceiveCompanyName = partsSalesOrder.SubmitCompanyName;
                    partsSalesOrder.IfAgencyService = false;
                    partsSalesOrder.IfDirectProvision = false;
                    partsSalesOrder.SalesActivityDiscountRate = 1;
                    partsSalesOrder.SalesActivityDiscountAmount = 0;
                    partsSalesOrder.ReceivingCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsSalesOrder.ReceivingCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsSalesOrder.ReceivingCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsSalesOrder.WarehouseId = 0;
                    partsSalesOrder.Status = (int)DcsPartsSalesOrderStatus.新增;
                    partsSalesOrder.AutoApproveStatus = (int)DcsAutoApproveStatus.未审核;
                    //获取企业类型
                    this.dcsDomainContext.Load(this.dcsDomainContext.GetCompaniesQuery().Where(e => e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                        if(loadOp.HasError)
                            return;
                        var company = loadOp.Entities.SingleOrDefault();
                        if(company == null)
                            return;
                        partsSalesOrder.InvoiceReceiveCompanyType = company.Type;
                        partsSalesOrder.CustomerType = company.Type;
                        partsSalesOrder.Province = company.ProvinceName;
                        partsSalesOrder.City = company.CityName;
                        if(partsSalesOrder.InvoiceReceiveCompanyType == (int)DcsCompanyType.代理库) {
                            this.dcsDomainContext.Load(this.dcsDomainContext.GetAgenciesQuery().Where(e => e.Id == company.Id), loadOpRegion => {
                                if(loadOpRegion.HasError)
                                    return;
                                var agency = loadOpRegion.Entities.SingleOrDefault();
                                if(agency != null) {
                                    partsSalesOrder.WarehouseId = agency.WarehouseId;
                                    partsSalesOrder.WarehouseName = agency.WarehouseName;
                                }
                            }, null);
                        } 
                        this.dcsDomainContext.Load(this.dcsDomainContext.GetRegionsQuery().Where(e => e.Type == 2 && e.Name == partsSalesOrder.Province), loadOpRegion => {
                            if(loadOpRegion.HasError)
                                return;
                            var province = loadOpRegion.Entities.SingleOrDefault();
                            if(province != null)
                                partsSalesOrder.ProvinceID = province.Id;
                            this.SwitchViewTo(DATA_EDIT_VIEW);
                        }, null);
                                               
                    }, null);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.SUBMIT:
                    var entitys = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().SingleOrDefault();
                    if(entitys == null)
                        return;
                    var dataGridDomainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    if(dataGridDomainContext == null)
                        return;
                    dataGridDomainContext.Load(dataGridDomainContext.获取客户可用资金Query(entitys.Id), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }
                        }
                        if(loadOp.Entities != null && loadOp.Entities.Any()) {
                            var customerAccount = loadOp.Entities.First();
                            if(entitys.TotalAmount > customerAccount.OrderUseablePosition) {
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataEdit_Validation_PartsSalesOrder_TotalAmountMoreUseablePosition);
                            } else {
                                this.PartsSalesOrderSubmit(entitys, dataGridDomainContext);
                            }
                        }
                    }, null);
                    break;
                case CommonActionKeys.REVOKE:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_RevokeByDeputy, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can撤销配件销售订单)
                                entity.撤销配件销售订单();
                            var domainContextSubmit = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContextSubmit == null)
                                return;
                            domainContextSubmit.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContextSubmit.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_RevokeByDeputySuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件销售订单)
                                entity.作废配件销售订单();
                            var domainContextAbandon = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContextAbandon == null)
                                return;
                            domainContextAbandon.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContextAbandon.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "LogisticsQuery":
                    this.SwitchViewTo(DATA_LOGISTICS_VIEW);
                    var saleOrder = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().First();
                    if(string.IsNullOrWhiteSpace(saleOrder.Code))
                        return;
                    var compositeFilterItemQuery = new CompositeFilterItem();
                    var filterItemQuery = new FilterItem();
                    filterItemQuery.MemberName = "OrderCode";
                    filterItemQuery.Value = saleOrder.Code;
                    filterItemQuery.Operator = FilterOperator.IsEqualTo;
                    filterItemQuery.MemberType = typeof(string);
                    compositeFilterItemQuery.Filters.Add(filterItemQuery);
                    this.LogisticsDataGridView.FilterItem = compositeFilterItemQuery;
                    this.LogisticsDataGridView.ExecuteQueryDelayed();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var id = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().First().Id;
                        this.dcsDomainContext.ExportPartsSalesOrderForReport(id, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null, null, null, null, null, null
                            , null, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                    } else {
                        var compositeFilterItem = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var code = compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var partsSalesOrderTypeId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderTypeId").Value as int?;
                            var submitCompanyName = compositeFilterItem.Filters.Single(r => r.MemberName == "SalesUnitOwnerCompanyName").Value as string;
                            //var salesCategoryId = compositeFilterItem.Filters.Single(e => e.MemberName == "SalesCategoryId").Value as int?;
                            var warehouseId = compositeFilterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                            var sparePartCode = compositeFilterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                            var sparePartName = compositeFilterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                            var isDebt = compositeFilterItem.Filters.Single(e => e.MemberName == "IsDebt").Value as bool?;
                            var status = compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            var ifDirectProvision = compositeFilterItem.Filters.Single(r => r.MemberName == "IfDirectProvision").Value as bool?;
                            var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? createTimeBegin = null;
                            DateTime? createTimeEnd = null;
                            if(createTime != null) {
                                createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            this.dcsDomainContext.ExportPartsSalesOrderForReport(null, BaseApp.Current.CurrentUserData.EnterpriseId, code, submitCompanyName, partsSalesOrderTypeId, null, status, warehouseId, isDebt, ifDirectProvision, createTimeBegin, createTimeEnd, sparePartCode, sparePartName, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
            }
        }

        private void PartsSalesOrderSubmit(PartsSalesOrder entitys, DcsDomainContext dataGridDomainContext) {
            DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Submit, () => {
                ShellViewModel.Current.IsBusy = true;
                //最小起订金额 是 特急和紧急  这两个类型不校验
                if(entitys.PartsSalesOrderTypeName != PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_Urgent && entitys.PartsSalesOrderTypeName != PartsSalesUIStrings.DataEdit_Text_PartsSalesOrder_Type_ExtraUrgent && entitys.IfDirectProvision) {
                    this.dcsDomainContext.校验订货配件起订金额是否满足(entitys, invokeOp => {
                        if(invokeOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(int.Parse(invokeOp.Value.ToString()) == 1) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_PartsSalesOrder_InitialAmount, 5);
                            return;
                        }
                        try {
                            if(entitys.Can提交配件销售订单)
                                entitys.提交配件销售订单();

                            dataGridDomainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    ShellViewModel.Current.IsBusy = false;
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    dataGridDomainContext.RejectChanges();
                                    return;
                                }
                                if(int.Parse(invokeOp.Value.ToString()) == 2) {
                                    UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_PartsSalesOrder_InitialAmountPart, 5);
                                } else {
                                    UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess);
                                }
                                ShellViewModel.Current.IsBusy = false;
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    }, null);
                } else {
                    try {
                        if(entitys.Can提交配件销售订单)
                            entitys.提交配件销售订单();
                        dataGridDomainContext.SubmitChanges(submitOp => {
                            if(submitOp.HasError) {
                                if(!submitOp.IsErrorHandled)
                                    submitOp.MarkErrorAsHandled();
                                ShellViewModel.Current.IsBusy = false;
                                DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                dataGridDomainContext.RejectChanges();
                                return;
                            }
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess);
                            ShellViewModel.Current.IsBusy = false;
                            this.CheckActionsCanExecute();
                        }, null);
                    } catch(Exception ex) {
                        ShellViewModel.Current.IsBusy = false;
                        UIHelper.ShowAlertMessage(ex.Message);
                    }
                }
            });
        }

        protected override
            bool OnRequestActionCanExecute
            (ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.REVOKE:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId.Equals(CommonActionKeys.ABANDON))
                        return entities[0].Status == (int)DcsPartsSalesOrderStatus.新增;
                    if((String.CompareOrdinal(uniqueId, CommonActionKeys.EDIT) == 0))
                        return (entities[0].Status == (int)DcsPartsSalesOrderStatus.新增 || entities[0].Status == (int)DcsPartsSalesOrderStatus.撤单);
                    if(String.CompareOrdinal(uniqueId, CommonActionKeys.SUBMIT) == 0)
                        return (entities[0].Status == (int)DcsPartsSalesOrderStatus.新增);
                    if(String.CompareOrdinal(uniqueId, CommonActionKeys.REVOKE) == 0)
                        return (entities[0].Status == (int)DcsPartsSalesOrderStatus.提交);
                    return false;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "LogisticsQuery":
                    return !(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1);
                default:
                    return false;
            }
        }
    }
}
