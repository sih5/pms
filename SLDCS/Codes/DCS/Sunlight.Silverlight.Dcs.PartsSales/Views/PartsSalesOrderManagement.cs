﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "PartsSalesOrder", ActionPanelKeys = new[] {
        CommonActionKeys.MAINEDIT_APPROVE_SENIORAPPROVE_ABANDON_TERMINATE_MERGEEXPORT, "PartsSalesOrder","LogisticsQuery", CommonActionKeys.SCHEDULEREEXPORT
    })]
    public class PartsSalesOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataMainEditView;
        private DataEditViewBase dataEditDetailView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataSeniorApproveView;
        private DataEditViewBase dataApproveForDirectView;
        private DataEditViewBase dataApproveForRebateView;
        private DataGridViewBase logisticsDataGridView;
        private const string DATA_DIRECT_VIEW = "_DataDirectView_";
        private const string DATA_EDITDETAIL_VIEW = "_DataEditDetailView_";
        private const string DATA_APPROVE_VIEW = "DataApproveView";
        private const string DATA_SENIORAPPROVE_VIEW = "DataSeniorApproveView";
        private const string DATA_REBATE_VIEW = "_DataRebateView_";
        private const string DATA_LOGISTICS_VIEW = "_DATALOGISTICSVIEW_";
        private const string DATA_MAINEDIT_VIEW = "_DataMainEditView_";
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PartsSalesOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesOrder;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesOrderWithDetails"));
            }
        }
        private DataGridViewBase LogisticsDataGridView {
            get {
                return this.logisticsDataGridView ?? (this.logisticsDataGridView = DI.GetDataGridView("Logistics"));
            }
        }
        private DataEditViewBase DataMainEditView {
            get {
                if(this.dataMainEditView == null) {
                    this.dataMainEditView = DI.GetDataEditView("PartsSalesOrderMainEdit");
                    this.dataMainEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataMainEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataMainEditView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSalesOrder");
                    var item = (PartsSalesOrderDataEditView)this.dataEditView;
                    item.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        //审批正常
        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("PartsSalesOrderForTemplateApprove");
                    this.dataApproveView.EditSubmitted += this.DataApproveView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        //高级审批
        private DataEditViewBase DataSeniorApproveView {
            get {
                if(this.dataSeniorApproveView == null) {
                    this.dataSeniorApproveView = DI.GetDataEditView("PartsSalesOrderForSeniorApprove");
                    this.dataSeniorApproveView.EditSubmitted += this.DataApproveView_EditSubmitted;
                    this.dataSeniorApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataSeniorApproveView;
            }
        }

        //增加清单修改功能
        private DataEditViewBase DataEditDetailView {
            get {
                if(this.dataEditDetailView == null) {
                    this.dataEditDetailView = DI.GetDataEditView("PartsSalesOrderForEditDetail");
                    this.dataEditDetailView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditDetailView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditDetailView;
            }
        }

        private DataEditViewBase DataApproveForDirectView {
            get {
                if(this.dataApproveForDirectView == null) {
                    this.dataApproveForDirectView = DI.GetDataEditView("PartsSalesOrderForSupplyApprove");
                    this.dataApproveForDirectView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveForDirectView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveForDirectView;
            }
        }

        private DataEditViewBase DataApproveForRebateView {
            get {
                if(this.dataApproveForRebateView == null) {
                    this.dataApproveForRebateView = DI.GetDataEditView("PartsSalesOrderForRebateApprove");
                    this.dataApproveForRebateView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveForRebateView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveForRebateView;
            }
        }

        //终止弹出
        private RadWindow terminateRadWindow;
        private RadWindow TerminateRadWindow {
            get {
                return this.terminateRadWindow ?? (this.terminateRadWindow = new RadWindow {
                    CanClose = false,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    Content = this.TerminateDataEditView,
                    ResizeMode = ResizeMode.NoResize,
                    Height = 200,
                    Width = 400,
                    Header = ""
                });
            }
        }

        private DataEditViewBase terminateDataEditView;

        private DataEditViewBase TerminateDataEditView {
            get {
                if(this.terminateDataEditView == null) {
                    this.terminateDataEditView = DI.GetDataEditView("PartsSalesOrderForTerminate");
                    this.terminateDataEditView.EditCancelled += terminateDataEditView_EditCancelled;
                    this.terminateDataEditView.EditSubmitted += terminateDataEditView_EditSubmitted;
                }
                return this.terminateDataEditView;
            }
        }

        //驳回弹出
        private RadWindow rejectRadWindow;

        private RadWindow RejectRadWindow {
            get {
                if(this.rejectRadWindow == null) {
                    this.rejectRadWindow = new RadWindow();
                    this.rejectRadWindow.CanClose = false;
                    this.rejectRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.rejectRadWindow.Content = this.RejectDataEditView;
                    this.rejectRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.rejectRadWindow.Height = 200;
                    this.rejectRadWindow.Width = 400;
                    this.rejectRadWindow.Header = "";
                }
                return this.rejectRadWindow;
            }
        }

        private DataEditViewBase rejectDataEditView;

        private DataEditViewBase RejectDataEditView {
            get {
                if(this.rejectDataEditView == null) {
                    this.rejectDataEditView = DI.GetDataEditView("PartsSalesOrderForReject");
                    this.rejectDataEditView.EditCancelled += rejectDataEditView_EditCancelled;
                    this.rejectDataEditView.EditSubmitted += rejectDataEditView_EditSubmitted;
                }
                return this.rejectDataEditView;
            }
        }

        private void rejectDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.RejectRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void rejectDataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.RejectRadWindow.Close();
        }

        private void terminateDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.TerminateRadWindow.Content = this.TerminateDataEditView;
            this.TerminateRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void terminateDataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.TerminateRadWindow.Content = this.TerminateDataEditView;
            this.TerminateRadWindow.Close();
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => DataEditView);
            this.RegisterView(DATA_MAINEDIT_VIEW, () => dataMainEditView);
            this.RegisterView(DATA_EDITDETAIL_VIEW, () => DataEditView);
            this.RegisterView(DATA_REBATE_VIEW, () => this.DataApproveForRebateView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_SENIORAPPROVE_VIEW, () => this.DataSeniorApproveView);
            this.RegisterView(DATA_EDITDETAIL_VIEW, () => this.DataEditDetailView);
            this.RegisterView(DATA_DIRECT_VIEW, () => this.DataApproveForDirectView);
            this.RegisterView(DATA_LOGISTICS_VIEW, () => this.LogisticsDataGridView);
        }

        //审批之后不重新查询单据，只是更改审批单据状态
        private void DataApproveView_EditSubmitted(object sender, EventArgs e) {
            var partsSalesOrder = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().FirstOrDefault();
            if(partsSalesOrder == null)
                return;
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            this.SwitchViewTo(DATA_GRID_VIEW);
            domainContext.Load(domainContext.根据审批人员查询销售订单Query(BaseApp.Current.CurrentUserData.UserId).Where(r => r.Id == partsSalesOrder.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                }
            }, null);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataMainEditView = null;
            this.dataEditDetailView = null;
            this.dataApproveView = null;
            this.dataSeniorApproveView = null;
            this.dataApproveForDirectView = null;
            this.dataApproveForRebateView = null;
            this.terminateDataEditView = null;
            this.rejectDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();

            ResetEditView();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesOrder"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "AddByDeputy":
                    var partsSalesOrder = this.DataEditView.CreateObjectToEdit<PartsSalesOrder>();
                    var domainContext = new DcsDomainContext();
                    domainContext.Load(domainContext.GetPersonSubDealerWithDetailQuery().Where(e => e.PersonId == BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        var personSubDealer = loadOp.Entities.FirstOrDefault();
                        if(personSubDealer != null) {
                            partsSalesOrder.FirstClassStationId = personSubDealer.SubDealer.Id;
                            partsSalesOrder.FirstClassStationCode = personSubDealer.SubDealer.Code;
                            partsSalesOrder.FirstClassStationName = personSubDealer.SubDealer.Name;
                        }
                    }, null);
                    partsSalesOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsSalesOrder.IfAgencyService = true;
                    partsSalesOrder.SalesActivityDiscountRate = 1;
                    partsSalesOrder.SalesActivityDiscountAmount = 0;
                    partsSalesOrder.IfDirectProvision = false;
                    partsSalesOrder.AutoApproveStatus = (int)DcsAutoApproveStatus.未审核;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "RevokeByDeputy":
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_RevokeByDeputy, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can撤销配件销售订单)
                                entity.撤销配件销售订单();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_RevokeByDeputySuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "RebateApprove":
                    this.DataApproveForRebateView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_REBATE_VIEW);
                    break;
                case "EditDetail":
                    this.DataEditDetailView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDITDETAIL_VIEW);
                    break;
                case "CheckStock":
                    var tempEntities = this.DataGridView.SelectedEntities;
                    if(tempEntities == null || tempEntities.Any() == false) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_PartsSalesOrder_NoData);
                        return;
                    }
                    var partsSalesOrderIds = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().Select(r => r.Id).ToArray();
                    var checkDomainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    ShellViewModel.Current.IsBusy = true;
                    checkDomainContext.校验销售清单品牌库存(partsSalesOrderIds, invokeOp => {
                        if(invokeOp.HasError) {
                            DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            invokeOp.MarkErrorAsHandled();
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        ShellViewModel.Current.IsBusy = false;
                        this.DataGridView.ExchangeData(null, null, invokeOp.Value);
                    }, null);
                    break;
                case CommonActionKeys.APPROVE:
                    var salesOrder = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().First();
                    if(salesOrder.IfDirectProvision) {
                        this.DataApproveForDirectView.SetObjectToEditById(salesOrder.Id);
                        this.SwitchViewTo(DATA_DIRECT_VIEW);
                    } else {
                        this.DataApproveView.SetObjectToEditById(salesOrder.Id);
                        this.SwitchViewTo(DATA_APPROVE_VIEW);
                    }
                    break;
                case CommonActionKeys.SENIORAPPROVE:
                    var seniorSalesOrder = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().First();
                    if(seniorSalesOrder.IfDirectProvision) {
                        this.DataApproveForDirectView.SetObjectToEditById(seniorSalesOrder.Id);
                        this.SwitchViewTo(DATA_DIRECT_VIEW);
                    } else {
                        this.DataSeniorApproveView.SetObjectToEditById(seniorSalesOrder.Id);
                        this.SwitchViewTo(DATA_SENIORAPPROVE_VIEW);
                    }
                    break;
                case CommonActionKeys.BATCHAPPROVAL:
                    var ids = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().Select(r => r.Id).ToArray();
                    var gridViewDomainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    ShellViewModel.Current.IsBusy = true;
                    gridViewDomainContext.批量审核配件销售订单(ids, loadOp => {
                        if(loadOp.HasError) {
                            gridViewDomainContext.RejectChanges();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            loadOp.MarkErrorAsHandled();
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        ShellViewModel.Current.IsBusy = false;
                        this.DataGridView.ExecuteQueryDelayed();
                        this.CheckActionsCanExecute();
                        UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_PartsSalesOrder_BitachApproceSuccess);
                    }, null);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件销售订单)
                                entity.作废配件销售订单();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.TERMINATE:
                    this.terminateDataEditView = null;
                    this.terminateRadWindow = null;
                    this.TerminateDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.TerminateRadWindow.ShowDialog();
                    break;
                case CommonActionKeys.REJECT:
                    this.RejectDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.RejectRadWindow.ShowDialog();
                    break;
                case CommonActionKeys.MERGEEXPORT: {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                            this.dcsDomainContext.ExportPartsSalesOrderWithDetailsByPersonnel(this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().Select(r => r.Id).ToArray(), null, BaseApp.Current.CurrentUserData.UserId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, /*null,*/ null, null,null, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        } else {
                            var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            //var eRPSourceOrderCode = filterItem.Filters.Single(r => r.MemberName == "ERPSourceOrderCode").Value as string;
                            var submitCompanyCode = filterItem.Filters.Single(r => r.MemberName == "SubmitCompanyCode").Value as string;
                            var submitCompanyName = filterItem.Filters.Single(r => r.MemberName == "SubmitCompanyName").Value as string;
                            var ifDirectProvision = filterItem.Filters.Single(e => e.MemberName == "IfDirectProvision").Value as bool?;
                            //var salesCategoryId = filterItem.Filters.Single(e => e.MemberName == "SalesCategoryId").Value as int?;
                            //  var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                            //var isDebt = filterItem.Filters.Single(e => e.MemberName == "IsDebt").Value as bool?;
                            //var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                            //var provinceId = filterItem.Filters.Single(e => e.MemberName == "ProvinceName").Value as int?;
                            //var cityId = filterItem.Filters.Single(r => r.MemberName == "CityName").Value as int?;
                            //var isAutoApprove = filterItem.Filters.Single(e => e.MemberName == "IsAutoApprove").Value as bool?;
                            //var isVehicleOutage = filterItem.Filters.Single(e => e.MemberName == "IsVehicleOutage").Value as bool?; //车辆是否停运  SIH_ISS20180523002
                            //var ifInnerDirectProvision = filterItem.Filters.Single(e => e.MemberName == "IfInnerDirectProvision").Value as bool?;
                            //var overseasDemandSheetNo = filterItem.Filters.Single(r => r.MemberName == "OverseasDemandSheetNo").Value as string;
                            //var cpPartsPurchaseOrderCode = filterItem.Filters.Single(e => e.MemberName == "CPPartsPurchaseOrderCode").Value as string;
                            //var cpPartsInboundCheckCode = filterItem.Filters.Single(e => e.MemberName == "CPPartsInboundCheckCode").Value as string;

                            var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                            var autoApproveStatus = filterItem.Filters.Single(e => e.MemberName == "AutoApproveStatus").Value as int?;

                            DateTime? createTimeBegin = null;
                            DateTime? createTimeEnd = null;
                            DateTime? submitTimeBegin = null;
                            DateTime? submitTimeEnd = null;
                            DateTime? abandonTimeBegin = null;
                            DateTime? abandonTimeEnd = null;
                            DateTime? approveTimeBegin = null;
                            DateTime? approveTimeEnd = null;
                            List<int> statusList = new List<int>();
                            var dateTimeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem));
                            foreach(var dateTimeFilterItem in dateTimeFilterItems) {
                                var dateTime = dateTimeFilterItem as CompositeFilterItem;
                                if(dateTime != null) {
                                    if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                        createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }
                                    if(dateTime.Filters.Any(r => r.MemberName == "SubmitTime")) {
                                        submitTimeBegin = dateTime.Filters.First(r => r.MemberName == "SubmitTime").Value as DateTime?;
                                        submitTimeEnd = dateTime.Filters.Last(r => r.MemberName == "SubmitTime").Value as DateTime?;
                                    }
                                    if(dateTime.Filters.Any(r => r.MemberName == "AbandonTime")) {
                                        abandonTimeBegin = dateTime.Filters.First(r => r.MemberName == "AbandonTime").Value as DateTime?;
                                        abandonTimeEnd = dateTime.Filters.Last(r => r.MemberName == "AbandonTime").Value as DateTime?;
                                    }
                                    if(dateTime.Filters.Any(r => r.MemberName == "ApproveTime")) {
                                        approveTimeBegin = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                        approveTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                    }
                                    if (dateTime.Filters.Any(r => r.MemberName == "Status")) {
                                        statusList = dateTime.Filters.Where(r => r.MemberName == "Status").Select(r => (int)r.Value).ToList();
                                    }
                                }
                            }
                            //if(createTimeBegin == null || createTimeEnd == null) {
                            //    UIHelper.ShowAlertMessage("请选择创建时间");
                            //    return;
                            //}
                            //var time = createTimeEnd - createTimeBegin;
                            //if(((TimeSpan)time).Days > 62) {
                            //    UIHelper.ShowAlertMessage("查询创建时长不可超出2个月");
                            //    return;
                            //}
                            var partsSalesOrderTypeId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesOrderTypeId").Value as int?;
                            this.dcsDomainContext.ExportPartsSalesOrderWithDetailsByPersonnel(new int[] { }, null, BaseApp.Current.CurrentUserData.UserId, null, null, sparePartCode, code, submitCompanyCode, submitCompanyName, partsSalesOrderTypeId, 221, statusList, null, null, ifDirectProvision, createTimeBegin, createTimeEnd, submitTimeBegin, submitTimeEnd, abandonTimeBegin, abandonTimeEnd, approveTimeBegin, approveTimeEnd, null, null, null, /*isVehicleOutage,*/ null, null,autoApproveStatus, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                        break;
                    }
                case CommonActionKeys.SCHEDULEREEXPORT: {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var submitCompanyName = filterItem.Filters.Single(r => r.MemberName == "SubmitCompanyName").Value as string;
                        var ifDirectProvision = filterItem.Filters.Single(e => e.MemberName == "IfDirectProvision").Value as bool?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var partsSalesOrderTypeId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesOrderTypeId").Value as int?;
                        var exportParams = new Dictionary<string, string> {
                        {
                            "submitCompanyName", (submitCompanyName ?? string.Empty)
                        }, {
                            "ifDirectProvision", (ifDirectProvision ?? (object)string.Empty).ToString()
                        }, {
                            "status", (status ?? (object)string.Empty).ToString()
                        }, {
                            "createTimeBegin", (createTimeBegin ?? (object)string.Empty).ToString()
                        }, {
                            "createTimeEnd", (createTimeEnd ?? (object)string.Empty).ToString()
                        }, {
                            "partsSalesOrderTypeId", (partsSalesOrderTypeId ?? (object)string.Empty).ToString()
                        },
                    };
                        dcsDomainContext.AddExportJobToScheduler("ScheduleExportPartsSalesOrderWithDetails", exportParams, PartsSalesUIStrings.DataEditPanel_Text_PartsSalesReturnBill_PartsSalesOrderCode, invokeOp => {
                            if(invokeOp.Value)
                                UIHelper.ShowNotification("导出任务已成功加入队列，请明天下载。");
                            else
                                UIHelper.ShowAlertMessage("导出任务未能加入队列，请联系管理员。");
                        }, null);
                        break;
                    }
                case "LogisticsQuery": {
                        this.SwitchViewTo(DATA_LOGISTICS_VIEW);
                        var saleOrder = this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().First();
                        if(string.IsNullOrWhiteSpace(saleOrder.Code))
                            return;
                        var compositeFilterItem = new CompositeFilterItem();
                        var filterItem = new FilterItem();
                        filterItem.MemberName = "OrderCode";
                        filterItem.Value = saleOrder.Code;
                        filterItem.Operator = FilterOperator.IsEqualTo;
                        filterItem.MemberType = typeof(string);
                        compositeFilterItem.Filters.Add(filterItem);
                        this.LogisticsDataGridView.FilterItem = compositeFilterItem;
                        this.LogisticsDataGridView.ExecuteQueryDelayed();
                        break;
                    }
                case CommonActionKeys.MAINEDIT:
                    this.DataMainEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_MAINEDIT_VIEW);
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case "AddByDeputy":
                    return true;
                case "RevokeByDeputy":
                case "RebateApprove":
                case CommonActionKeys.TERMINATE:
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.SENIORAPPROVE:
                case CommonActionKeys.MAINEDIT:
                case CommonActionKeys.ABANDON:
                case "EditDetail":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    if(CommonActionKeys.ABANDON.Equals(uniqueId))
                        return this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().Any(r => r.Status == (int)DcsPartsSalesOrderStatus.新增);
                    if("RevokeByDeputy".Equals(uniqueId) || "RebateApprove".Equals(uniqueId) || CommonActionKeys.REJECT.Equals(uniqueId) || CommonActionKeys.MAINEDIT.Equals(uniqueId))
                        return this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().Any(r => r.Status == (int)DcsPartsSalesOrderStatus.提交);
                    if("EditDetail".Equals(uniqueId))
                        return this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().Any(r => (r.Status == (int)DcsPartsSalesOrderStatus.提交 || r.Status == (int)DcsPartsSalesOrderStatus.部分审批) && (!r.IsTransSuccess.HasValue || !(bool)r.IsTransSuccess));
                    return this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().Any(r => r.Status == (int)DcsPartsSalesOrderStatus.提交 || r.Status == (int)DcsPartsSalesOrderStatus.部分审批);
                case CommonActionKeys.MERGEEXPORT:
                case "CheckStock":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.SCHEDULEREEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.BATCHAPPROVAL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(!this.DataGridView.SelectedEntities.Any())
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().All(r => r.Status == (int)DcsPartsSalesOrderStatus.提交 || r.Status == (int)DcsPartsSalesOrderStatus.部分审批);
                case "LogisticsQuery":
                    return !(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1);

                default:
                    return false;
            }
        }
    }
}