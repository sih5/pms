﻿
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "PartsSalesOrderQuery", ActionPanelKeys = new[] {
        CommonActionKeys.MERGEEXPORT
    })]
    public class PartsSalesOrderQuery : DcsDataManagementViewBase {
        //private ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesOrderForQuery"));
            }
        }

        public PartsSalesOrderQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesOrderForQuery;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesOrderForQuery"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                var status = compositeFilterItem.Filters.SingleOrDefault(filter => filter.MemberName == "Status");
                if(status != null && status.Value == null) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_StatusNotNull);
                    return;
                }
            } else {
                if(filterItem.MemberName == "Status")
                    if(filterItem.Value == null) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_StatusNotNull);
                        return;
                    }
                compositeFilterItem.Filters.Add(filterItem);
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SalesUnitOwnerCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            //var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            //var filterTtem = this.DataGridView.FilterItem as CompositeFilterItem;
            //if(filterTtem != null) {
            //    var code = filterTtem.Filters.Single(r => r.MemberName == "Code").Value as string;
            //    var orderType = filterTtem.Filters.Single(r => r.MemberName == "OrderType").Value as int?;
            //    var dealerName = filterTtem.Filters.Single(r => r.MemberName == "DealerName").Value as string;
            //    var vehicleOrderStatus = filterTtem.Filters.Single(r => r.MemberName == "Status").Value as int?;
            //    var yearOfPlan = filterTtem.Filters.Single(r => r.MemberName == "YearOfPlan").Value;
            //    var monthOfPlan = filterTtem.Filters.Single(r => r.MemberName == "MonthOfPlan").Value;
            //    var composite = filterTtem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
            //    DateTime? beginCreateTime = null;
            //    DateTime? finishCreateTime = null;
            //    if(composite != null) {
            //        beginCreateTime = composite.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
            //        finishCreateTime = composite.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
            //    }
            //    switch(uniqueId) {
            //        case CommonActionKeys.MERGEEXPORT:
            //            //this.ExportPartsSalesOrder(code, orderType, dealerName, vehicleOrderStatus, yearOfPlan, monthOfPlan, null, beginCreateTime, finishCreateTime, 1);
            //            break;
            //    }
            //}
        }
        //private void ExportPartsSalesOrder(string code, int? orderType, string dealerName, int? vehicleOrderStatus, object yearOfPlan, object monthOfPlan, int? vehicleOrderDetailStatus, DateTime? beginCreateTime, DateTime? finishCreateTime, int exportType) {
        //    ShellViewModel.Current.IsBusy = true;
        //    this.excelServiceClient.ExportPartsSalesOrderWithDetailsAsync(code, orderType, null, null, null, DateTime.Now, null);
        //    this.excelServiceClient.ExportPartsSalesOrderWithDetailsCompleted -= this.excelServiceClient_ExportPartsSalesOrderWithDetailsCompleted;
        //    this.excelServiceClient.ExportPartsSalesOrderWithDetailsCompleted += this.excelServiceClient_ExportPartsSalesOrderWithDetailsCompleted;
        //}

        //void excelServiceClient_ExportPartsSalesOrderWithDetailsCompleted(object sender, ExportPartsSalesOrderWithDetailsCompletedEventArgs e) {
        //    ShellViewModel.Current.IsBusy = false;
        //    //UIHelper.ShowNotification(e.Error.Message);
        //    //HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.e));
        //}



        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.MERGEEXPORT:
                    return true;
                default:
                    return false;
            }
        }

    }
}
