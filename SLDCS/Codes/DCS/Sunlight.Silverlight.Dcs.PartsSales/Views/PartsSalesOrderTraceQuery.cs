﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "PartsSalesOrderTraceQuery", ActionPanelKeys = new[] { CommonActionKeys.EXPORT })]
    public class PartsSalesOrderTraceQuery : DcsDataManagementViewBase {

        public PartsSalesOrderTraceQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.Management_Title_PartsSalesOrderTraceQuery;
        }

        private DataGridViewBase dataGridView;
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesOrderTraceQueryWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesOrderTrace"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            DateTime? submitTimeBegin = null;
            DateTime? submitTimeEnd = null;
            var dateTimeFilterItems = compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem));
            if(dateTimeFilterItems.Any()) {
                foreach(var dateTimeFilterItem in dateTimeFilterItems) {
                    var dateTime = dateTimeFilterItem as CompositeFilterItem;
                    if(dateTime != null) {
                        if(dateTime.Filters.Any(r => r.MemberName == "SubmitTime")) {
                            submitTimeBegin = dateTime.Filters.First(r => r.MemberName == "SubmitTime").Value as DateTime?;
                            submitTimeEnd = dateTime.Filters.Last(r => r.MemberName == "SubmitTime").Value as DateTime?;
                            if(submitTimeBegin == null || submitTimeEnd == null) {
                                UIHelper.ShowNotification("请选择提交时间范围");
                                return;
                            } else {
                                if(submitTimeBegin > submitTimeEnd) {
                                    UIHelper.ShowNotification("提交开始时间不能大于提交结束时间");
                                    return;
                                } else {
                                    TimeSpan sp = submitTimeEnd.Value.Subtract(submitTimeBegin.Value);
                                    if(sp.Days >= 1) {
                                        UIHelper.ShowNotification("只允许查询一天内提交的数据");
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                UIHelper.ShowNotification("请选择提交时间范围");
                return;
            }
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT: {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var dcsDomainContext = new DcsDomainContext();
                        DateTime? submitTimeBegin = null;
                        DateTime? submitTimeEnd = null;
                        var dateTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        if(dateTime != null) {
                            submitTimeBegin = dateTime.Filters.First(r => r.MemberName == "SubmitTime").Value as DateTime?;
                            submitTimeEnd = dateTime.Filters.Last(r => r.MemberName == "SubmitTime").Value as DateTime?;
                        }
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                            var ids = this.DataGridView.SelectedEntities.Cast<VehiclePartsSalesOrdercs>().Select(r => r.DetailId).ToArray();
                            var code = this.DataGridView.SelectedEntities.Cast<VehiclePartsSalesOrdercs>().Select(r => r.PartsSalesOrderCode).First();
                            dcsDomainContext.ExportPartsSalesOrderTraceQuery(ids, code, null, null, null, null, null, null, submitTimeBegin, submitTimeEnd, null, null, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        } else {
                            var code = filterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderCode").Value as string;
                            var submitCompanyCode = filterItem.Filters.Single(r => r.MemberName == "SubmitCompanyCode").Value as string;
                            var submitCompanyName = filterItem.Filters.Single(r => r.MemberName == "SubmitCompanyName").Value as string;
                            var partCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                            var partName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                            var salesCategoryId = filterItem.Filters.Single(e => e.MemberName == "SalesCategoryId").Value as int?;
                            var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                            var salesUnitOwnerCompanyId = filterItem.Filters.Single(e => e.MemberName == "SalesUnitOwnerCompanyId").Value as int?;
                            var erpSourceOrderCode = filterItem.Filters.Single(e => e.MemberName == "ERPSourceOrderCode").Value as string;
                            dcsDomainContext.ExportPartsSalesOrderTraceQuery(new int[] { }, code, submitCompanyCode, submitCompanyName, partCode, partName, salesCategoryId, warehouseId, submitTimeBegin, submitTimeEnd, salesUnitOwnerCompanyId, erpSourceOrderCode, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                        break;
                    }
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
