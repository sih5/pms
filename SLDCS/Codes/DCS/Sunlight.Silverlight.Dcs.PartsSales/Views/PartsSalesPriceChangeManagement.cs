﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "PartsSalesPriceChange", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT_EXPORT_MERGEEXPORT_PRINT,"PartsSalesPriceChange"
    })]
    public class PartsSalesPriceChangeManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataEditViewBase dataDetailView;
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        public PartsSalesPriceChangeManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesPriceChangey;
        }
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditForApproveView;
        private const string DATA_EEDITFORAPPORVE_VIEW = "_DataEditForApproveView_";
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSalesPriceChange");
                    this.dataEditView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private DataEditViewBase DataEditForApproveView {
            get {
                if(this.dataEditForApproveView == null) {
                    this.dataEditForApproveView = DI.GetDataEditView("PartsSalesPriceChangeForApprove");
                    this.dataEditForApproveView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditForApproveView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditForApproveView;
            }
        }

        private RadWindow abandodRadWindow;

        private RadWindow AbandodRadWindow {
            get {
                if(this.abandodRadWindow == null) {
                    this.abandodRadWindow = new RadWindow();
                    this.abandodRadWindow.CanClose = false;
                    this.abandodRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.abandodRadWindow.Content = this.AbandodDataEditView;
                    this.abandodRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.abandodRadWindow.Height = 200;
                    this.abandodRadWindow.Width = 400;
                    this.abandodRadWindow.Header = "";
                }
                return this.abandodRadWindow;
            }
        }
        private DataEditViewBase abandodDataEditView;

        private DataEditViewBase AbandodDataEditView {
            get {
                if(this.abandodDataEditView == null) {
                    this.abandodDataEditView = DI.GetDataEditView("PartsSalesPriceChangeForTerminate");
                    this.abandodDataEditView.EditCancelled += AbandodDataEditView_EditCancelled;
                    this.abandodDataEditView.EditSubmitted += AbandodDataEditView_EditSubmitted;
                }
                return this.abandodDataEditView;
            }
        }

        private void AbandodDataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.AbandodRadWindow.Close();
        }

        private void AbandodDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.AbandodRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditForApproveView = null;
            this.abandodDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                if (this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsSalesPriceChange");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if (this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsSalesPriceChangeDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EEDITFORAPPORVE_VIEW, () => this.DataEditForApproveView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);

        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesPriceChange"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                     if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var submit = this.DataGridView.SelectedEntities.Cast<PartsSalesPriceChange>().ToArray();
                    if(submit.Length != 1)
                        return false;
                    return submit[0].Status == (int)DcsPartsSalesPriceChangeStatus.新建;
                case "FirstApprove":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsSalesPriceChange>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPartsSalesPriceChangeStatus.提交;                               
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entitiess = this.DataGridView.SelectedEntities.Cast<PartsSalesPriceChange>().ToArray();
                    if(entitiess.Length != 1)
                        return false;
                    return entitiess[0].Status == (int)DcsPartsSalesPriceChangeStatus.新建 ;
                case WorkflowActionKey.INITIALAPPROVE:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entitie2 = this.DataGridView.SelectedEntities.Cast<PartsSalesPriceChange>().ToArray();
                    if(entitie2.Length != 1)
                        return false;
                    return entitie2[0].Status == (int)DcsPartsSalesPriceChangeStatus.初审通过;
                case WorkflowActionKey.FINALAPPROVE:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<PartsSalesPriceChange>().ToArray();
                    if(entitie.Length != 1)
                        return false;
                    return entitie[0].Status == (int)DcsPartsSalesPriceChangeStatus.审核通过;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsSalesPriceChange = this.DataEditView.CreateObjectToEdit<PartsSalesPriceChange>();
                    partsSalesPriceChange.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsSalesPriceChange.IfClaim = false;
                    partsSalesPriceChange.Status = (int)DcsPartsSalesPriceChangeStatus.新建;
                    partsSalesPriceChange.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case WorkflowActionKey.FINALAPPROVE:
                    this.DataEditForApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORAPPORVE_VIEW);
                    break;
                case WorkflowActionKey.INITIALAPPROVE:
                    this.DataEditForApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORAPPORVE_VIEW);
                    break;
                case "FirstApprove":
                    this.DataEditForApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORAPPORVE_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    this.AbandodDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.AbandodRadWindow.ShowDialog();
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsSalesPriceChange>().Select(r => r.Id).ToArray();
                        if(uniqueId == CommonActionKeys.MERGEEXPORT) {
                            this.ExportPartsSalesPriceChangeDetail(ids, null, null, null, null, null,null,null);
                        } else {
                            this.ExportPartsSalesPriceChange(ids, null, null, null, null, null,null,null);
                        }
                    } else {
                        var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filtes == null)
                            return;
                        var code = filtes.Filters.SingleOrDefault(e => e.MemberName == "Code").Value as string;
                        //var partsSalesCategoryId = filtes.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filtes.Filters.SingleOrDefault(e => e.MemberName == "Status").Value as int?;
                        var createTime = filtes.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        var sparePartCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filtes.Filters.SingleOrDefault(e => e.MemberName == "SparePartName").Value as string;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        if(uniqueId == CommonActionKeys.MERGEEXPORT) {
                            this.ExportPartsSalesPriceChangeDetail(null, 221, code, status,sparePartCode,sparePartName, createTimeBegin, createTimeEnd);
                        } else {
                            this.ExportPartsSalesPriceChange(null, 221, code, status,sparePartCode,sparePartName, createTimeBegin, createTimeEnd);
                        }
                    }
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesPriceChange>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交配件销售价变更申请)
                                entity.提交配件销售价变更申请();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }
        private void ExportPartsSalesPriceChange(int[] ids, int? partsSalesCategoryId, string code, int? status,string sparePartCode,string sparePartName, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsSalesPriceChangeAsync(ids, partsSalesCategoryId, code, status,sparePartCode,sparePartName, creatorTimeBegin, creatorTimeEnd);
            this.excelServiceClient.ExportPartsSalesPriceChangeCompleted += ExcelServiceClient_ExportPartsSalesPriceChangeCompleted;
            this.excelServiceClient.ExportPartsSalesPriceChangeCompleted += ExcelServiceClient_ExportPartsSalesPriceChangeCompleted;
        }

        private void ExcelServiceClient_ExportPartsSalesPriceChangeCompleted(object sender, ExportPartsSalesPriceChangeCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        private void ExportPartsSalesPriceChangeDetail(int[] ids, int? partsSalesCategoryId, string code, int? status,string sparePartCode,string sparePartName, DateTime? creatorTimeBegin, DateTime? creatorTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsSalesPriceChangeDetailAsync(ids, partsSalesCategoryId, code, status,sparePartCode,sparePartName, creatorTimeBegin, creatorTimeEnd);
            this.excelServiceClient.ExportPartsSalesPriceChangeDetailCompleted -= ExcelServiceClient_ExportPartsSalesPriceChangeDetailCompleted;
            this.excelServiceClient.ExportPartsSalesPriceChangeDetailCompleted += ExcelServiceClient_ExportPartsSalesPriceChangeDetailCompleted;
        }

        private void ExcelServiceClient_ExportPartsSalesPriceChangeDetailCompleted(object sender, ExportPartsSalesPriceChangeDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
