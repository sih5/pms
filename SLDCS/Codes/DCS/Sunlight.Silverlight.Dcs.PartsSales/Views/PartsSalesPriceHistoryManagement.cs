﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {

    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "PartsSalesPriceHistory", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PartsSalesPriceHistoryManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesPriceHistoryForSelect"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesPriceHistoryForSelect"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var name = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                  //  var partsSalesCategoryId = filterItem.Filters.Single(r => r.MemberName == "PartsSalesPriceChange.PartsSalesCategoryId").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    var BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "PartsSalesPriceChange.CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "PartsSalesPriceChange.CreateTime").Value as DateTime?;
                    }
                    dcsDomainContext.ExportPartsSalesPriceHistory(BranchId, code, name, null, createTimeBegin, createTimeEnd, loadOp => {
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }
                    }, null);
                    break;
            }
        }

        //private void ExportPartsSalesPriceHistory(int BranchId, string code, string name, int? partsSalesCategoryid, DateTime? createTimeBegin, DateTime? createTimeEnd) {
        //    ShellViewModel.Current.IsBusy = true;
        //    this.excelServiceClient.ExportPartsSalesPriceHistory(BranchId, code, name, partsSalesCategoryid, createTimeBegin, createTimeEnd);
        //    this.excelServiceClient.ExportPartsSalesPriceHistoryCompleted -= excelServiceClient_ExportPartsSalesPriceHistoryCompleted;
        //    this.excelServiceClient.ExportPartsSalesPriceHistoryCompleted += excelServiceClient_ExportPartsSalesPriceHistoryCompleted;
        //}

        //private void excelServiceClient_ExportPartsSalesPriceHistoryCompleted(object sender, ExportPartsPlannedPriceCompletedEventArgs e) {
        //    ShellViewModel.Current.IsBusy = false;
        //    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        //}

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            //composite.Filters.Add(new FilterItem {
            //    MemberName = "PartsSalesPriceChange.BranchId",
            //    MemberType = typeof(int),
            //    Operator = FilterOperator.IsEqualTo,
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId
            //});
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        public PartsSalesPriceHistoryManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesPriceHistory;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
    }
}
