﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "PartsSalesPrice", ActionPanelKeys = new[] {
       "PartsSalesPrice"
    })]
    public class PartsSalesPriceManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PartsSalesPriceManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesPriceQuery;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesPriceWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            ClientVar.ConvertTime(composite);
            composite.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            var codes = composite.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
            if (codes != null && codes.Value != null) {
                composite.Filters.Add(new CompositeFilterItem {
                    MemberName = "SparePartCode",
                    MemberType = typeof(string),
                    Value = string.Join(",", ((IEnumerable<string>)codes.Value).ToArray()),
                    Operator = FilterOperator.Contains
                });
                composite.Filters.Remove(codes);
            }
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesPrice"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "ExactExport":
                    var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filtes == null)
                        return false;
                    var sparePartCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "SparePartCode").Value as string;
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any() && !string.IsNullOrEmpty(sparePartCode);
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                case "ExactExport":
                    bool ixExactExport = false;
                    if (uniqueId.Equals("ExactExport")) {
                        ixExactExport = true;
                    }
                    var filtes = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filtes == null)
                        return;
                    var sparePartCode = filtes.Filters.SingleOrDefault(e => e.MemberName == "SparePartCode").Value as string;
                    if (ixExactExport && string.IsNullOrEmpty(sparePartCode)) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_PartsSalesPriceManagement_SparePartCodeIsNull);
                        return;
                    }
                    var sparePartName = filtes.Filters.SingleOrDefault(e => e.MemberName == "SparePartName").Value as string;
                  //  var partsSalesCategoryId = filtes.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var priceType = filtes.Filters.SingleOrDefault(e => e.MemberName == "PriceType").Value as int?;
                    var status = filtes.Filters.SingleOrDefault(e => e.MemberName == "Status").Value as int?;
                    var isSalable = filtes.Filters.SingleOrDefault(e => e.MemberName == "isSale").Value as bool?;
                    var isOrderable = filtes.Filters.SingleOrDefault(e => e.MemberName == "isOrderable").Value as bool?;
                    var branchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    DateTime? modifyTimeBegin = null;
                    DateTime? modifyTimeEnd = null;
                    var createTime = filtes.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem; 
                    if(createTime != null) { 
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?; 
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?; 
                    } 
                    var modifierTime = filtes.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "ModifyTime")) as CompositeFilterItem; 
                    if(modifierTime != null) { 
                        modifyTimeBegin = modifierTime.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?; 
                        modifyTimeEnd = modifierTime.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?; 
                    }
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsSalesPrice>().Select(r => r.Id).ToArray();
                        dcsDomainContext.ExportPartsSalesPrice(ids, branchId, sparePartCode, sparePartName, null, priceType, status, isSalable, isOrderable, createTimeBegin, createTimeEnd,modifyTimeBegin,modifyTimeEnd,ixExactExport, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    } else {
                        dcsDomainContext.ExportPartsSalesPrice(null, branchId, sparePartCode, sparePartName, null, priceType, status, isSalable, isOrderable, createTimeBegin, createTimeEnd,modifyTimeBegin,modifyTimeEnd,ixExactExport, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    }
                    break;
            }
        }
    }
}
