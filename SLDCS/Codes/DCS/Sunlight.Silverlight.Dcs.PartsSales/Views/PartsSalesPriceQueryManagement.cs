﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "AgencyPartsSalesPriceQuery",ActionPanelKeys = new[]{
      CommonActionKeys.EXPORT
    })]
    public class PartsSalesPriceQueryManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PartsSalesPriceQueryManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.QueryPanel_Title_PartsSalesPriceQuery;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgencyPartsSalesPriceWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT: {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;

                        var sparePartCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartName").Value as string;
                        //var branchId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "BranchId").Value as int?;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status").Value as int?;
                        var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        //var priceType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PriceType").Value as int?;
                        
                        var isSalable = filterItem.Filters.SingleOrDefault(r => r.MemberName == "isSale").Value as bool?;
                        var isOrderable = filterItem.Filters.SingleOrDefault(r => r.MemberName == "isOrderable").Value as bool?;
                       
                    //var createTime = filterItem.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    //    DateTime? createTimeBegin = null;
                    //    DateTime? createTimeEnd = null;
                    //    if(createTime != null) {
                    //        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                    //        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    //    }
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                            dcsDomainContext.ExportPartsSalesPriceRetailGuidePriceByBranchType(this.DataGridView.SelectedEntities.Cast<PartsSalesPriceWithRetailGuidePrice>().Select(r => r.Id).ToArray(), null, null, null, null, null, null, null, null, null, null, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        } else {
                            dcsDomainContext.ExportPartsSalesPriceRetailGuidePriceByBranchType(null, sparePartCode, sparePartName, null, status, partsSalesCategoryId, null, isOrderable, isSalable, null, null, loadOp2 => {
                                if(loadOp2.HasError)
                                    return;
                                if(loadOp2.Value == null || string.IsNullOrEmpty(loadOp2.Value)){
                                    UIHelper.ShowNotification(loadOp2.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }
                                if(loadOp2.Value != null && !string.IsNullOrEmpty(loadOp2.Value)){
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp2.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            },null);
                        }
                        break;
                    }
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            //var branchFilter = composite.Filters.First(v => v.MemberName == "BranchId");
            //if(branchFilter == null || branchFilter.Value == null) {
            //    UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_AgencyPartsRetail_BranchId);
            //    return;
            //}
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesPriceQuery"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId){
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
