﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "PartsSalesReturnBillForReport", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT_MERGEEXPORT_IMPORT
    })]
    public class PartsSalesReturnBillForReportManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public PartsSalesReturnBillForReportManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesReturnBillForReport;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }
        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("PartsSalesReturnBillForReportForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }

        private DataGridViewBase DataGridView {
            get {
                return dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesReturnBillWithDetailsForReport"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSalesReturnBillForReport");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesReturnBillForReport"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPartsSalesReturnBillStatus.新增;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD: {
                        var partsSalesReturnBill = this.DataEditView.CreateObjectToEdit<PartsSalesReturnBill>();
                        partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.新增;
                        partsSalesReturnBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                        partsSalesReturnBill.ReturnCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                        partsSalesReturnBill.ReturnCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                        partsSalesReturnBill.ReturnCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                        partsSalesReturnBill.InvoiceReceiveCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                        partsSalesReturnBill.InvoiceReceiveCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                        partsSalesReturnBill.InvoiceReceiveCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                        partsSalesReturnBill.SubmitCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                        partsSalesReturnBill.SubmitCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                        partsSalesReturnBill.SubmitCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                        partsSalesReturnBill.DiscountRate = 1m;
                       // partsSalesReturnBill.ReturnType = (int)DcsPartsSalesReturnBillReturnType.问题件退货;
                        var ss = this.DataEditView as PartsSalesReturnBillForReportDataEditView;
                        ss.FileUploadDataEditPanels.FilePath = null;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.Load(domainContext.GetSubChannelRelationsQuery().Where(ex => ex.SubChanelId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError)
                                return;
                            var subChannelRelation = loadOp.Entities.SingleOrDefault();
                            if(subChannelRelation == null)
                                return;
                            domainContext.Load(domainContext.GetDealersQuery().Where(ex => ex.Id == subChannelRelation.ParentChanelId), LoadBehavior.RefreshCurrent, loadOp1 => {
                                if(loadOp1.HasError)
                                    return;
                                var dealer = loadOp1.Entities.SingleOrDefault();

                                partsSalesReturnBill.FirstClassStationName = dealer.Name;
                                partsSalesReturnBill.FirstClassStationCode = dealer.Code;
                                partsSalesReturnBill.FirstClassStationId = subChannelRelation.ParentChanelId;
                            }, null);
                        }, null);
                        this.SwitchViewTo(DATA_EDIT_VIEW);
                        break;
                    }
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById((this.DataGridView.SelectedEntities.FirstOrDefault() as PartsSalesReturnBill).Id);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    //DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => this.DataGridView.UpdateSelectedEntities(entity => ((PartsSalesReturnBill)entity).Status = (int)DcsPartsSalesReturnBillStatus.作废, () => {
                    //    var tempPartsSalesReturnBill = this.DataGridView.SelectedEntities.FirstOrDefault() as PartsSalesReturnBill;
                    //    if(tempPartsSalesReturnBill == null) {
                    //        return;
                    //    }
                    //    tempPartsSalesReturnBill.作废配件销售退货单();
                    //    UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                    //    this.CheckActionsCanExecute();
                    //}));
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件销售退货单)
                                entity.作废配件销售退货单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });


                    break;
                //case CommonActionKeys.SUBMIT:
                //    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Submit, () => this.DataGridView.UpdateSelectedEntities(entity => ((PartsSalesReturnBill)entity).Status = (int)DcsPartsSalesReturnBillStatus.提交, () => {
                //        UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess);
                //        this.CheckActionsCanExecute();
                //    }));
                //    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交配件销售退货单)
                                entity.提交配件销售退货单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var salesUnitName = filterItem.Filters.Single(r => r.MemberName == "SalesUnitName").Value as string;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var partsSalesOrderCode = filterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderCode").Value as string;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var returnType = filterItem.Filters.Single(r => r.MemberName == "ReturnType").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                   
                    var entities = this.DataGridView.SelectedEntities == null ? null : this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().ToArray();
                    int? id = null;
                    if(entities != null && entities.Count() == 1) {
                        id = entities.First().Id;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportPartsSalesReturnBillWithDetail(id, null, null, null, null, status, createTimeBegin, createTimeEnd, returnType, salesUnitName, 2, BaseApp.Current.CurrentUserData.EnterpriseId, null, code, partsSalesOrderCode);
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.DataGridView.DomainContext.RejectChanges();
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                newCompositeFilterItem = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(newCompositeFilterItem);
            } else
                newCompositeFilterItem.Filters.Add(filterItem);
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "ReturnCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private void ExportPartsSalesReturnBillWithDetail(int? id, int? personnelId, string returnCompanyName, int? returnCompanyId, int? ownerCompanyId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? returnType, string salesUnitName, int inType, int PersonnelOrCompanyId, string eRPOrderCode, string code, string partsSalesOrderCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsSalesReturnBillWithDetailAsync(id, personnelId, returnCompanyName, returnCompanyId, ownerCompanyId, status, createTimeBegin, createTimeEnd, returnType, salesUnitName, inType, PersonnelOrCompanyId, eRPOrderCode, code, partsSalesOrderCode);
            this.excelServiceClient.ExportPartsSalesReturnBillWithDetailCompleted -= excelServiceClient_ExportPartsSalesReturnBillWithDetailCompleted;
            this.excelServiceClient.ExportPartsSalesReturnBillWithDetailCompleted += excelServiceClient_ExportPartsSalesReturnBillWithDetailCompleted;
        }

        private void excelServiceClient_ExportPartsSalesReturnBillWithDetailCompleted(object sender, ExportPartsSalesReturnBillWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
