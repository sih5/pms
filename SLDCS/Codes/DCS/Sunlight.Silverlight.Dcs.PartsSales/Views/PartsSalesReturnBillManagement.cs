﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "PartsSalesReturnBill", ActionPanelKeys = new[] {
            CommonActionKeys.INITIALAPPROVE_MERGEEXPORT_REJECT_TERMINATE_ABANDON,"PartsSalesReturnBill"
    })]
    public class PartsSalesReturnBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase uploadDataEditView;
        private DataEditViewBase finalTrialView;
        private DataEditViewBase firstTrialView;
        private const string DATA_APPROVE_VIEW = "DataApproveView";
        protected const string Upload_DATA_EDIT_VIEW = "_UploadDataEditView_";
        private const string DATA_FIRSTTRIAL_VIEW = "DataFirstTrialView";
        private const string DATA_FINALTRIAL_VIEW = "DataFinalTrialView";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public PartsSalesReturnBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesReturnBill;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesReturnBillWithDetails"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSalesReturnBillForAddByDeputy");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataFinalTrialView {
            get {
                if(this.finalTrialView == null) {
                    this.finalTrialView = DI.GetDataEditView("PartsSalesReturnBillForFinalTrial");
                    this.finalTrialView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.finalTrialView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.finalTrialView;
            }
        }
        private DataEditViewBase DataFirstTrialView {
            get {
                if(this.firstTrialView == null) {
                    this.firstTrialView = DI.GetDataEditView("PartsSalesReturnBillForFirstTrial");
                    this.firstTrialView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.firstTrialView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.firstTrialView;
            }
        }

        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("PartsSalesReturnBillForApprove");
                    this.dataApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }
        private DataEditViewBase UploadDataEditView {
            get {
                if(this.uploadDataEditView == null) {
                    this.uploadDataEditView = DI.GetDataEditView("PartsSalesReturnBillForUpload");
                    this.uploadDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.uploadDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.uploadDataEditView;
            }
        }

        #region 驳回弹出
        private RadWindow rejectRadWindow;

        private RadWindow RejectRadWindow {
            get {
                if(this.rejectRadWindow == null) {
                    this.rejectRadWindow = new RadWindow();
                    this.rejectRadWindow.CanClose = false;
                    this.rejectRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.rejectRadWindow.Content = this.RejectDataEditView;
                    this.rejectRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.rejectRadWindow.Height = 200;
                    this.rejectRadWindow.Width = 400;
                    this.rejectRadWindow.Header = "";
                }
                return this.rejectRadWindow;
            }
        }

        private DataEditViewBase rejectDataEditView;

        private DataEditViewBase RejectDataEditView {
            get {
                if(this.rejectDataEditView == null) {
                    this.rejectDataEditView = DI.GetDataEditView("PartsSalesReturnBillForReject");
                    this.rejectDataEditView.EditCancelled += rejectDataEditView_EditCancelled;
                    this.rejectDataEditView.EditSubmitted += rejectDataEditView_EditSubmitted;
                }
                return this.rejectDataEditView;
            }
        }

        void rejectDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.RejectRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        void rejectDataEditView_EditCancelled(object sender, EventArgs e) {
            this.RejectRadWindow.Close();
        }
        #endregion

        #region 终止弹出
        private RadWindow terminateRadWindow;

        private RadWindow TerminateRadWindow {
            get {
                if(this.terminateRadWindow == null) {
                    this.terminateRadWindow = new RadWindow();
                    this.terminateRadWindow.CanClose = false;
                    this.terminateRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.terminateRadWindow.Content = this.TerminateDataEditView;
                    this.terminateRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.terminateRadWindow.Height = 200;
                    this.terminateRadWindow.Width = 400;
                    this.terminateRadWindow.Header = "";
                }
                return this.terminateRadWindow;
            }
        }

        private DataEditViewBase terminateDataEditView;

        private DataEditViewBase TerminateDataEditView {
            get {
                if(this.terminateDataEditView == null) {
                    this.terminateDataEditView = DI.GetDataEditView("PartsSalesReturnBillForTerminate");
                    this.terminateDataEditView.EditCancelled += terminateDataEditView_EditCancelled;
                    this.terminateDataEditView.EditSubmitted += terminateDataEditView_EditSubmitted;
                }
                return this.terminateDataEditView;
            }
        }

        void terminateDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.TerminateRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        void terminateDataEditView_EditCancelled(object sender, EventArgs e) {
            this.TerminateRadWindow.Close();
        }
        #endregion

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => DataEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(Upload_DATA_EDIT_VIEW, () => this.UploadDataEditView);
            this.RegisterView(DATA_FIRSTTRIAL_VIEW, () => this.DataFirstTrialView);
            this.RegisterView(DATA_FINALTRIAL_VIEW, () => this.DataFinalTrialView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataApproveView = null;
            this.uploadDataEditView = null;
            this.finalTrialView = null;
            this.firstTrialView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                //var status = compositeFilterItem.Filters.FirstOrDefault(filter => filter.MemberName == "Status");
                //if(status != null && status.Value == null) {
                //    UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_StatusNotNull);
                //    return;
                //}
            } else {
                if(filterItem.MemberName == "Status")
                    //if(filterItem.Value == null) {
                    //    UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_StatusNotNull);
                    //    return;
                    //}
                    compositeFilterItem.Filters.Add(filterItem);
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesReturnBill"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "AddByDeputy":
                    var partsSalesReturnBill = this.DataEditView.CreateObjectToEdit<PartsSalesReturnBill>();
                    partsSalesReturnBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsSalesReturnBill.WarehouseId = null;
                    partsSalesReturnBill.WarehouseCode = "";
                    partsSalesReturnBill.WarehouseName = "";
                    partsSalesReturnBill.Status = (int)DcsPartsSalesReturnBillStatus.提交;
                    partsSalesReturnBill.DiscountRate = 1m;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.INITIALAPPROVE:
                    this.DataFirstTrialView.SetObjectToEditById((this.DataGridView.SelectedEntities.FirstOrDefault() as PartsSalesReturnBill).Id);
                    this.SwitchViewTo(DATA_FIRSTTRIAL_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    //DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => this.DataGridView.UpdateSelectedEntities(entity => ((PartsSalesReturnBill)entity).Status = (int)DcsPartsSalesReturnBillStatus.作废, () => {
                    //    UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                    //    this.CheckActionsCanExecute();
                    //}));
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().FirstOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件销售退货单)
                                entity.作废配件销售退货单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                    var returnCompanyName = filterItem.Filters.Single(e => e.MemberName == "ReturnCompanyName").Value as string;
                    var salesUnitName = filterItem.Filters.Single(e => e.MemberName == "PartsSalesOrderCode").Value as string;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var returnType = filterItem.Filters.Single(e => e.MemberName == "ReturnType").Value as int?;
                    var createTime = filterItem.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    var eRPOrderCode = filterItem.Filters.Single(e => e.MemberName == "PartsSalesReturnBillDetails.PartsSalesOrder.ERPSourceOrderCode").Value as string;
                    var entities = this.DataGridView.SelectedEntities == null ? null : this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().ToArray();
                    int? id = null;
                    if(entities != null && entities.Count() == 1)
                        id = entities.First().Id;
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportPartsSalesReturnBill(id, null, returnCompanyName, null, null, status, createTimeBegin, createTimeEnd, returnType, salesUnitName, 1, BaseApp.Current.CurrentUserData.UserId, eRPOrderCode, code);
                    break;
                case CommonActionKeys.REJECT:
                    this.RejectDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().First().Id);
                    this.RejectRadWindow.ShowDialog();
                    break;
                case CommonActionKeys.TERMINATE:
                    this.TerminateDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().First().Id);
                    this.TerminateRadWindow.ShowDialog();
                    break;
                case CommonActionKeys.UPLOAD:
                    this.UploadDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().First().Id);
                    this.SwitchViewTo(Upload_DATA_EDIT_VIEW);
                    break;
                case "FirstTrial":
                    this.DataApproveView.SetObjectToEditById((this.DataGridView.SelectedEntities.FirstOrDefault() as PartsSalesReturnBill).Id);
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                case "FinalTrial":
                    this.DataFinalTrialView.SetObjectToEditById((this.DataGridView.SelectedEntities.FirstOrDefault() as PartsSalesReturnBill).Id);
                    this.SwitchViewTo(DATA_FINALTRIAL_VIEW);
                    break;
                //批量审核
                case "BatchApprove":

                    DcsUtils.Confirm(PartsSalesUIStrings.Management_Validation_PartsSalesReturnBillManagement_BatchApprove, () => {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().Select(e => e.Id).ToArray();
                        var dcsDomainContext = new DcsDomainContext();
                        dcsDomainContext.批量审核退货单(ids, loadOpv => {
                            if(loadOpv.HasError) {
                                if(!loadOpv.IsErrorHandled)
                                    loadOpv.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                ShellViewModel.Current.IsBusy = false;
                                return;
                            }
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_PartsSalesReturnBillManagement_BatchApproveSuccess);
                            this.DataGridView.ExecuteQuery();
                        }, null);
                    });

                    break;

            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case "AddByDeputy":
                    return true;
                case CommonActionKeys.INITIALAPPROVE:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.TERMINATE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    if(String.CompareOrdinal(uniqueId, CommonActionKeys.INITIALAPPROVE) == 0 || CommonActionKeys.REJECT.Equals(uniqueId) || CommonActionKeys.TERMINATE.Equals(uniqueId))
                        return this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().All(r => r.Status == (int)DcsPartsSalesReturnBillStatus.提交);
                    return this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().All(r => r.Status == (int)DcsPartsSalesReturnBillStatus.新增);
                case CommonActionKeys.REJECT:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().All(r => r.Status == (int)DcsPartsSalesReturnBillStatus.提交 && !r.IsBarter);
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.UPLOAD:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return true;
                case "FirstTrial":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().All(r => r.Status == (int)DcsPartsSalesReturnBillStatus.初审通过);
                case "FinalTrial":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().All(r => r.Status == (int)DcsPartsSalesReturnBillStatus.审核通过);
                case "BatchApprove":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() == 0)
                        return false;
                    var batchDetail = this.DataGridView.SelectedEntities.Cast<PartsSalesReturnBill>().ToArray();
                    var returnCompanyId = batchDetail.First().ReturnCompanyId;
                    foreach(var item in batchDetail) {
                        if(item.Status != (int)DcsPartsSalesReturnBillStatus.提交 || returnCompanyId != item.ReturnCompanyId || item.WarehouseId == null) {
                            return false;
                        }

                    }
                    return true;
                default:
                    return false;
            }
        }

        private void ExportPartsSalesReturnBill(int? id, int? personnelId, string returnCompanyName, int? returnCompanyId, int? ownerCompanyId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? returnType, string salesUnitName, int inType, int PersonnelOrCompanyId, string eRPOrderCode, string code) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsSalesReturnBillWithDetailAsync(id, personnelId, returnCompanyName, returnCompanyId, ownerCompanyId, status, createTimeBegin, createTimeEnd, returnType, salesUnitName, inType, PersonnelOrCompanyId, eRPOrderCode, code,null);
            this.excelServiceClient.ExportPartsSalesReturnBillWithDetailCompleted -= excelServiceClient_ExportPartsSalesReturnBillWithDetailCompleted;
            this.excelServiceClient.ExportPartsSalesReturnBillWithDetailCompleted += excelServiceClient_ExportPartsSalesReturnBillWithDetailCompleted;
        }

        private void excelServiceClient_ExportPartsSalesReturnBillWithDetailCompleted(object sender, ExportPartsSalesReturnBillWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
