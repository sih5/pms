﻿using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    /// <summary>
    /// 配件销售退货查询
    /// </summary>
    [PageMeta("PartsSales", "PartsSales", "PartsSalesReturnBillQuery")]
    public class PartsSalesReturnBillQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsSalesReturnBillWithDetailsForQuery");
                }
                return dataGridView;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesReturnBillForQuery"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(compositeFilterItem);
                var status = compositeFilterItem.Filters.SingleOrDefault(filter => filter.MemberName == "Status");
                if(status != null && status.Value == null) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_StatusNotNull);
                    return;
                }
            } else {
                if(filterItem.MemberName == "Status")
                    if(filterItem.Value == null) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_StatusNotNull);
                        return;
                    }
                compositeFilterItem.Filters.Add(filterItem);
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SalesUnitOwnerCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            this.SwitchViewTo(DATA_EDIT_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.dataGridView.ExecuteQueryDelayed();
        }

        public PartsSalesReturnBillQuery() {
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesReturnBillQuery;
            this.Initializer.Register(this.Initialize);
        }
    }
}
