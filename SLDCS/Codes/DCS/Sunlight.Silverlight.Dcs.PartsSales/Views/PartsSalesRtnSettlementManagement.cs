﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSalesSettlement", "PartsSalesRtnSettlement", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_EXPORT_MERGEEXPORT_PRINT_GCPRINT, "PartsSalesRtnSettlement"
    })]
    public class PartsSalesRtnSettlementManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataInvoiceRegisterView;
        private const string DATA_INVOICEREGISTER_VIEW = "_dataInvoiceRegisterView_";
        private DataEditViewBase dataEditView;

        public PartsSalesRtnSettlementManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesRtnSettlement;
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSalesRtnSettlement");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataInvoiceRegisterView {
            get {
                if(this.dataInvoiceRegisterView == null) {
                    this.dataInvoiceRegisterView = DI.GetDataEditView("PartsSalesRtnSettlementForInvoiceRegister");
                    this.dataInvoiceRegisterView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataInvoiceRegisterView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataInvoiceRegisterView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataInvoiceRegisterView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesRtnSettlementWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_INVOICEREGISTER_VIEW, () => this.DataInvoiceRegisterView);
        }

        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsSalesRtnSettlement = this.DataEditView.CreateObjectToEdit<PartsSalesRtnSettlement>();
                    partsSalesRtnSettlement.TaxRate = 0.13;
                    partsSalesRtnSettlement.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsSalesRtnSettlement.SalesCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsSalesRtnSettlement.SalesCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsSalesRtnSettlement.SalesCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsSalesRtnSettlement.Status = (int)DcsPartsSalesRtnSettlementStatus.新建;
                    partsSalesRtnSettlement.InvoicePath = (int)DcsPartsPurchaseRtnSettleBillInvoicePath.反开销售发票;
                    partsSalesRtnSettlement.SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算;
                    partsSalesRtnSettlement.SettleType = (int)DcsSalesOrderType_SettleType.销售结算;
                    partsSalesRtnSettlement.BusinessType = (int)DcsSalesOrderType_BusinessType.国内销售;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        //var ids = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().Select(r => r.Id).ToArray();
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().Select(r => r.Id).ToArray();
                        if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                            domainContext.ExportPartsSalesRtnSettlement(ids, null, null, null, null, null, null, null, null, null, null, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                }
                            }, null);
                        }
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                            this.excelServiceClient.ExportPartsSalesRtnSettlementWithDetailsAsync(ids, null, null, null, null, null, null, null, null, null,null,null,null,null);
                            this.excelServiceClient.ExportPartsSalesRtnSettlementWithDetailsCompleted -= excelServiceClient_ExportPartsSalesRtnSettlementWithDetailsCompleted;
                            this.excelServiceClient.ExportPartsSalesRtnSettlementWithDetailsCompleted += excelServiceClient_ExportPartsSalesRtnSettlementWithDetailsCompleted;
                        }
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var customerCompanyCode = filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyCode").Value as string;
                        var creatorName = filterItem.Filters.Single(r => r.MemberName == "CreatorName").Value as string;
                        var customerCompanyName = filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyName").Value as string;
                        //var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var accountGroupId = filterItem.Filters.Single(e => e.MemberName == "AccountGroupId").Value as int?;
                        var salesCompanyId = filterItem.Filters.Single(e => e.MemberName == "SalesCompanyId").Value as int?;
                        var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var settleType = filterItem.Filters.Single(r => r.MemberName == "SettleType").Value as int?;
                        var businessType = filterItem.Filters.Single(r => r.MemberName == "BusinessType").Value as int?;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? invoiceDateBegin = null;
                        DateTime? invoiceDateEnd = null;
                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                       {
                          var dateTime = filter as CompositeFilterItem;
                          if (dateTime != null)
                          {
                            if (dateTime.Filters.First().MemberName == "CreateTime")
                            {
                                createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if (dateTime.Filters.First().MemberName == "InvoiceDate")
                            {
                                invoiceDateBegin = dateTime.Filters.First(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                invoiceDateEnd = dateTime.Filters.Last(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                            }
                          }
                      }
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                            this.excelServiceClient.ExportPartsSalesRtnSettlementWithDetailsAsync(new int[] { }, code, customerCompanyCode, null, customerCompanyName, accountGroupId, status, createTimeBegin, createTimeEnd, creatorName, invoiceDateBegin, invoiceDateEnd, settleType, businessType);
                            this.excelServiceClient.ExportPartsSalesRtnSettlementWithDetailsCompleted -= excelServiceClient_ExportPartsSalesRtnSettlementWithDetailsCompleted;
                            this.excelServiceClient.ExportPartsSalesRtnSettlementWithDetailsCompleted += excelServiceClient_ExportPartsSalesRtnSettlementWithDetailsCompleted;
                        }
                        if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                            domainContext.ExportPartsSalesRtnSettlement(null, salesCompanyId, null, code, customerCompanyCode, customerCompanyName, accountGroupId, createTimeBegin, createTimeEnd, creatorName, status, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                }
                            }, null);
                        }
                    }
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        //var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().SingleOrDefault();
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件销售退货结算单)
                                entity.作废配件销售退货结算单();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        //var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().SingleOrDefault();
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            //if(entity.Can审批配件销售退货结算单)
                            //    entity.审批配件销售退货结算单();
                            if(entity.Can审批虚拟配件销售退货结算单)
                                entity.审批虚拟配件销售退货结算单();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "AntiSettlement":
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Notification_AntiSettlement, () => {
                        //var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().SingleOrDefault();
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can反结算配件销售退货结算单)
                                entity.反结算配件销售退货结算单();
                            this.ExecuteSerivcesMethod(PartsSalesUIStrings.DataManagementView_Notification_AntiSettlementSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "InvoiceRegister":
                    this.DataInvoiceRegisterView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INVOICEREGISTER_VIEW);
                    break;
                case CommonActionKeys.PRINT:
                    //var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().FirstOrDefault();
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new PartsSalesRtnSettlementPrintWindow {
                        Header = PartsSalesUIStrings.DataManagementView_PrintWindow_Title_PartsSalesRtnSettlement,
                        //PartsSalesRtnSettlement = selectedItem
                        PartsSalesRtnSettlementEx = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case CommonActionKeys.GCPRINT:
                    //var printedItem = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().FirstOrDefault();
                    var printedItem = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().FirstOrDefault();
                    if(printedItem == null)
                        return;
                    BasePrintWindow gcPrintWindow = new PartsSalesRtnSettlementGCPrintWindow {
                        Header = PartsSalesUIStrings.DataManagementView_PrintWindow_Title_GCPartsSalesRtnSettlement,
                        //PartsSalesRtnSettlement = printedItem
                        PartsSalesRtnSettlementEx = printedItem
                    };
                    gcPrintWindow.ShowDialog();
                    break;
                case "SettlementCostSearch":
                    this.WindowDataBase.ShowDialog();
                    break;

            }
        }

        private void excelServiceClient_ExportPartsSalesRtnSettlementWithDetailsCompleted(object sender, ExportPartsSalesRtnSettlementWithDetailsCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private DcsExportQueryWindowBase settlementCostSearchQueryWindow;
        private DcsExportQueryWindowBase SettlementCostSearchQueryWindow {
            get {
                if(this.settlementCostSearchQueryWindow == null) {
                    this.settlementCostSearchQueryWindow = DI.GetQueryWindow("SettlementCostSearch") as DcsExportQueryWindowBase;
                    this.settlementCostSearchQueryWindow.Loaded += settlementCostSearchQueryWindow_Loaded;
                }
                return this.settlementCostSearchQueryWindow;
            }
        }

        private void settlementCostSearchQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            //var partsSalesRtnSettlement = this.DataGridView.SelectedEntities.First() as PartsSalesRtnSettlement;
            var partsSalesRtnSettlement = this.DataGridView.SelectedEntities.First() as PartsSalesRtnSettlementEx;
            if(partsSalesRtnSettlement == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Code", partsSalesRtnSettlement.Code
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Code", false
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PartsSalesRtnSettlementId", typeof(int), FilterOperator.IsEqualTo, partsSalesRtnSettlement.Id));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private RadWindow windowDataBase;
        private RadWindow WindowDataBase {
            get {
                return this.windowDataBase ?? (this.windowDataBase = new RadWindow {
                    Content = this.SettlementCostSearchQueryWindow,
                    Header = PartsSalesUIStrings.Action_Title_SettlementCostSearch,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesRtnSettlement"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    //return this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().All(r => r.Status == (int)DcsPartsSalesRtnSettlementStatus.新建);
                   return this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().All(r => r.Status == (int)DcsPartsSalesRtnSettlementStatus.新建);
                case "InvoiceRegister":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    //var entitie = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().ToArray();
                    var entitie = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().ToArray();
                    if(entitie[0].SettlementPath == (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算)
                        return false;
                    //return this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().All(r => r.Status == (int)DcsPartsSalesRtnSettlementStatus.已审批);
                    return this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().All(r => r.Status == (int)DcsPartsSalesRtnSettlementStatus.已审批);
                case "AntiSettlement":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    //var entities = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().ToArray();
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().ToArray();
                    if(entities[0].SettlementPath == (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算)
                        return false;
                    return entities[0].Status == (int)DcsPartsSalesRtnSettlementStatus.发票登记 || entities[0].Status == (int)DcsPartsSalesRtnSettlementStatus.已审批;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                case CommonActionKeys.GCPRINT:
                case "SettlementCostSearch":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    //var selectItems = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().ToArray();
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().ToArray();
                    return selectItems.Length == 1;

                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilter = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem)
                newCompositeFilter = filterItem as CompositeFilterItem;
            else
                newCompositeFilter.Filters.Add(filterItem);
            newCompositeFilter.Filters.Add(new FilterItem {
                MemberName = "SalesCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            ClientVar.ConvertTime(newCompositeFilter);
            this.DataGridView.FilterItem = newCompositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
