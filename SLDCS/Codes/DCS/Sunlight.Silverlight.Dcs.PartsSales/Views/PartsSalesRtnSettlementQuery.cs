﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSalesSettlement", "PartsSalesRtnSettlementQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT_MERGEEXPORT
    })]
    public class PartsSalesRtnSettlementQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesRtnSettlementWithDetails"));
            }
        }

        public PartsSalesRtnSettlementQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesRtnSettlementQuery;
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesRtnSettlementForQuery"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var customerCompanyCode = filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyCode").Value as string;
                    var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var customerCompanyName = filterItem.Filters.Single(e => e.MemberName == "CustomerCompanyName").Value as string;
                    var accountGroupId = filterItem.Filters.Single(e => e.MemberName == "AccountGroupId").Value as int?;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        //var ids = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlement>().Select(r => r.Id).ToArray();
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlementEx>().Select(r => r.Id).ToArray();
                        this.excelServiceClient.ExportPartsSalesRtnSettlementQueryWithDetailsAsync(ids, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null, null, null, null);
                        this.excelServiceClient.ExportPartsSalesRtnSettlementQueryWithDetailsCompleted -= excelServiceClient_ExportPartsSalesRtnSettlementQueryWithDetailsCompleted;
                        this.excelServiceClient.ExportPartsSalesRtnSettlementQueryWithDetailsCompleted += excelServiceClient_ExportPartsSalesRtnSettlementQueryWithDetailsCompleted;


                    } else {
                        this.excelServiceClient.ExportPartsSalesRtnSettlementQueryWithDetailsAsync(null, BaseApp.Current.CurrentUserData.EnterpriseId, code, customerCompanyCode, partsSalesCategoryId, customerCompanyName, accountGroupId, status, createTimeBegin, createTimeEnd);
                        this.excelServiceClient.ExportPartsSalesRtnSettlementQueryWithDetailsCompleted -= excelServiceClient_ExportPartsSalesRtnSettlementQueryWithDetailsCompleted;
                        this.excelServiceClient.ExportPartsSalesRtnSettlementQueryWithDetailsCompleted += excelServiceClient_ExportPartsSalesRtnSettlementQueryWithDetailsCompleted;

                        ShellViewModel.Current.IsBusy = false;
                    }
                    break;

            }
        }

        void excelServiceClient_ExportPartsSalesRtnSettlementQueryWithDetailsCompleted(object sender, ExportPartsSalesRtnSettlementQueryWithDetailsCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilter = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                newCompositeFilter = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(newCompositeFilter);
            } else
                newCompositeFilter.Filters.Add(filterItem);
            newCompositeFilter.Filters.Add(new FilterItem {
                MemberName = "CustomerCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            this.DataGridView.FilterItem = newCompositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

    }
}
