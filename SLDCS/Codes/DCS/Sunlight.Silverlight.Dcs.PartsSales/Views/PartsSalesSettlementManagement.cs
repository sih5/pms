﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSalesSettlement", "PartsSalesSettlement", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_EXPORT_MERGEEXPORT_PRINT, "PartsSalesSettlement"
    })]
    public class PartsSalesSettlementManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewInvoiceRegister;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private const string DATA_EDIT_VIEW_INVOICEREGISTER = "_DataEditViewInvoiceRegister_";
        private const string DATA_EDIT_VIEW_APPOVEREBATE = "_DataEditViewAppoveRebate_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public PartsSalesSettlementManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesSettlement;
        }
        //PartsSalesSettlementForAppoveRebate
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VirtualPartsSalesSettlementWithDetails"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSalesSettlement");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase partsSalesSettlementForAppoveRebate;
        private DataEditViewBase PartsSalesSettlementForAppoveRebate {
            get {
                if(this.partsSalesSettlementForAppoveRebate == null) {
                    this.partsSalesSettlementForAppoveRebate = DI.GetDataEditView("PartsSalesSettlementForAppoveRebate");
                    this.partsSalesSettlementForAppoveRebate.EditCancelled += this.DataEditView_EditCancelled;
                    this.partsSalesSettlementForAppoveRebate.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.partsSalesSettlementForAppoveRebate;
            }
        }

        private DataEditViewBase DataEditViewInvoiceRegister {
            get {
                if(this.dataEditViewInvoiceRegister == null) {
                    this.dataEditViewInvoiceRegister = DI.GetDataEditView("PartsSalesSettlementForInvoiceRegister");
                    this.dataEditViewInvoiceRegister.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditViewInvoiceRegister.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewInvoiceRegister;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_APPOVEREBATE, () => this.PartsSalesSettlementForAppoveRebate);
            this.RegisterView(DATA_EDIT_VIEW_INVOICEREGISTER, () => this.DataEditViewInvoiceRegister);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewInvoiceRegister = null;
            this.partsSalesSettlementForAppoveRebate = null;
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsSalesSettlement = this.DataEditView.CreateObjectToEdit<PartsSalesSettlement>();
                    partsSalesSettlement.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsSalesSettlement.TaxRate = 0.13;
                    //partsSalesSettlement.RebateMethod = (int)DcsPartsSalesSettlementRebateMethod.返利池;
                    partsSalesSettlement.Status = (int)DcsPartsSalesSettlementStatus.新建;
                    partsSalesSettlement.SettlementPath = (int)DcsPartsPurchaseSettleBillSettlementPath.正常结算;
                    partsSalesSettlement.SalesCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsSalesSettlement.SalesCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsSalesSettlement.SalesCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsSalesSettlement.SettleType = (int)DcsSalesOrderType_SettleType.销售结算;
                    partsSalesSettlement.BusinessType = (int)DcsSalesOrderType_BusinessType.国内销售;
                    // 新增单据，保存后切换到编辑主界面
                    this.DataEditView.EditSubmitted -= this.DataEditView_EditSubmitted;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "AppoveRebate":
                    this.PartsSalesSettlementForAppoveRebate.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_APPOVEREBATE);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    // 修改单据，保存后切换到查询主界面
                    this.DataEditView.EditSubmitted -= this.DataEditView_EditSubmitted;
                    this.DataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlementEx>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件销售结算单)
                                entity.作废配件销售结算单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlementEx>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can审批虚拟配件销售结算单)
                                entity.审批虚拟配件销售结算单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "InvoiceRegister":
                    this.DataEditViewInvoiceRegister.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_INVOICEREGISTER);
                    break;
                case "AntiSettlement":
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_AntiSettlement, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlementEx>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can反结算配件销售结算单)
                                entity.反结算配件销售结算单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AntiSettlementSuccess);
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlementEx>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new PartsSalesSettlementPrintWindow {
                        Header = "配件销售结算明细打印",
                        PartsSalesSettlement = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case "PrintInvoice":
                    var selectedItems = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlementEx>().FirstOrDefault();
                    if(selectedItems == null)
                        return;
                    this.dcsDomainContext.Load(this.dcsDomainContext.GetInvoiceInformationsQuery().Where(r => r.SourceCode == selectedItems.Code && r.Status != (int)DcsInvoiceInformationStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOp.Entities != null && loadOp.Entities.Count() > 1) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_PartsSalesReturnBillManagement_PartsSalesSettlementEx);
                            return;
                        }
                        BasePrintWindow printWindow2 = new InvoiceInformationPrintWindow {
                            Header = PartsSalesUIStrings.Management_Validation_PartsSalesReturnBillManagement_PrintTitle,
                            PartsSalesSettlementEx = selectedItems
                        };
                        printWindow2.ShowDialog();
                    }, null);
                    break;
                case "SettlementCost":
                    this.WindowSettlementCost.ShowDialog();
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var creatorName = filterItem.Filters.Single(r => r.MemberName == "CreatorName").Value as string;
                    var customerCompanyCode = filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyCode").Value as string;
                    //var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var customerCompanyName = filterItem.Filters.Single(e => e.MemberName == "CustomerCompanyName").Value as string;
                    var accountGroupId = filterItem.Filters.Single(e => e.MemberName == "AccountGroupId").Value as int?;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var settleType = filterItem.Filters.Single(r => r.MemberName == "SettleType").Value as int?;
                    var businessType = filterItem.Filters.Single(r => r.MemberName == "BusinessType").Value as int?;
                    var businessCode = filterItem.Filters.Single(e => e.MemberName == "BusinessCode").Value as string;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    DateTime? invoiceDateBegin = null;
                    DateTime? invoiceDateEnd = null;
                    foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var dateTime = filter as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.First().MemberName == "CreateTime") {
                                createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.First().MemberName == "InvoiceDate") {
                                invoiceDateBegin = dateTime.Filters.First(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                                invoiceDateEnd = dateTime.Filters.Last(r => r.MemberName == "InvoiceDate").Value as DateTime?;
                            }
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlementEx>().Select(r => r.Id).ToArray();
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                            this.excelServiceClient.ExportPartsSalesSettlementWithDetailsAsync(this.DataGridView.SelectedEntities.Cast<PartsSalesSettlementEx>().First().Id, BaseApp.Current.CurrentUserData.UserId, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            this.excelServiceClient.ExportPartsSalesSettlementWithDetailsCompleted -= excelServiceClient_ExportPartsSalesSettlementWithDetailsCompleted;
                            this.excelServiceClient.ExportPartsSalesSettlementWithDetailsCompleted += excelServiceClient_ExportPartsSalesSettlementWithDetailsCompleted;
                        }
                        if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                            this.dcsDomainContext.ExportPartsSalesSettlement(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, loadOp1 => {
                                if(loadOp1.HasError)
                                    return;
                                if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                                    UIHelper.ShowNotification(loadOp1.Value);
                                }
                                if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value))
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                            }, null);
                            ShellViewModel.Current.IsBusy = false;
                        }
                    } else {
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                            this.excelServiceClient.ExportPartsSalesSettlementWithDetailsAsync(null, BaseApp.Current.CurrentUserData.UserId, BaseApp.Current.CurrentUserData.EnterpriseId, code, customerCompanyCode, null, customerCompanyName, accountGroupId, status, createTimeBegin, createTimeEnd, creatorName, invoiceDateBegin, invoiceDateEnd, settleType, businessType);
                            this.excelServiceClient.ExportPartsSalesSettlementWithDetailsCompleted -= excelServiceClient_ExportPartsSalesSettlementWithDetailsCompleted;
                            this.excelServiceClient.ExportPartsSalesSettlementWithDetailsCompleted += excelServiceClient_ExportPartsSalesSettlementWithDetailsCompleted;
                        }
                        if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                            this.dcsDomainContext.ExportPartsSalesSettlement(new int[] {
                            }, BaseApp.Current.CurrentUserData.EnterpriseId, null, code, customerCompanyCode, customerCompanyName, accountGroupId, createTimeBegin, createTimeEnd, creatorName, status, settleType, businessType, businessCode, loadOp1 => {
                                if(loadOp1.HasError)
                                    return;
                                if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                                    UIHelper.ShowNotification(loadOp1.Value);
                                }
                                if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value))
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                            }, null);
                        }
                        ShellViewModel.Current.IsBusy = false;
                    }
                    break;
            }
        }

        private void excelServiceClient_ExportPartsSalesSettlementWithDetailsCompleted(object sender, ExportPartsSalesSettlementWithDetailsCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private RadWindow windowSettlementCost;

        private RadWindow WindowSettlementCost {
            get {
                return this.windowSettlementCost ?? (this.windowSettlementCost = new RadWindow {
                    Content = VirtualPartsSalesSettlementQueryWindow,
                    Header = PartsSalesUIStrings.Management_Header_VirtualPartsSalesSettlementQueryWindow,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        private DcsExportQueryWindowBase virtualPartsSalesSettlementQueryWindow;

        private DcsExportQueryWindowBase VirtualPartsSalesSettlementQueryWindow {
            get {
                if(this.virtualPartsSalesSettlementQueryWindow == null) {
                    this.virtualPartsSalesSettlementQueryWindow = DI.GetQueryWindow("VirtualPartsSalesSettlement") as DcsExportQueryWindowBase;
                    var partsSalesSettlementQueryWindow = this.virtualPartsSalesSettlementQueryWindow;
                    if(partsSalesSettlementQueryWindow != null)
                        partsSalesSettlementQueryWindow.Loaded += this.virtualPartsSalesSettlementQueryWindow_Loaded;
                }
                return this.virtualPartsSalesSettlementQueryWindow;
            }
        }

        private void virtualPartsSalesSettlementQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var partsSalesSettlement = this.DataGridView.SelectedEntities.First() as PartsSalesSettlementEx;
            if(partsSalesSettlement == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Code", partsSalesSettlement.Code
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Code", false
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("Id", typeof(int), FilterOperator.IsEqualTo, partsSalesSettlement.Id));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                case "InvoiceRegister":
                case "AntiSettlement":
                case "AppoveRebate":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlementEx>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == "AntiSettlement")
                        return ((entities[0].Status == (int)DcsPartsSalesSettlementStatus.发票登记 && entities[0].SettlementPath != (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算) || (entities[0].Status == (int)DcsPartsSalesSettlementStatus.已审批 && entities[0].SettlementPath != (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算));
                    if(uniqueId == "InvoiceRegister")
                        return (entities[0].Status == (int)DcsPartsSalesSettlementStatus.已审批 && entities[0].SettlementPath != (int)DcsPartsPurchaseSettleBillSettlementPath.反冲结算);
                    return entities[0].Status == (int)DcsPartsSalesSettlementStatus.新建;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlementEx>().ToArray();
                    return selectItems.Length == 1;
                case "SettlementCost":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlementEx>().ToArray();
                    if(entities1.Length == 1)
                        return true;
                    return false;
                case "PrintInvoice":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities2 = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlementEx>().ToArray();
                    if(entities2.Length != 1)
                        return false;
                    return entities2[0].Status == (int)DcsPartsSalesSettlementStatus.发票登记;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilter = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                newCompositeFilter = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(newCompositeFilter);
            } else
                newCompositeFilter.Filters.Add(filterItem);
            //newCompositeFilter.Filters.Add(new FilterItem {
            //    MemberName = "SalesCompanyId",
            //    MemberType = typeof(int),
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
            //    Operator = FilterOperator.IsEqualTo
            //});
            ClientVar.ConvertTime(newCompositeFilter);
            this.DataGridView.FilterItem = newCompositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesSettlement"
                };
            }
        }
    }
}