﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSalesSettlement", "PartsSalesSettlementQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT_MERGEEXPORT
    })]
    public class PartsSalesSettlementQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSalesSettlementWithDetails"));
            }
        }
        public PartsSalesSettlementQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSalesSettlementQuery;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesSettlementForQuery"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilter = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                newCompositeFilter = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(newCompositeFilter);
            } else
                newCompositeFilter.Filters.Add(filterItem);
            newCompositeFilter.Filters.Add(new FilterItem {
                MemberName = "CustomerCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            this.DataGridView.FilterItem = newCompositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var customerCompanyCode = filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyCode").Value as string;
                    var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var customerCompanyName = filterItem.Filters.Single(e => e.MemberName == "CustomerCompanyName").Value as string;
                    var accountGroupId = filterItem.Filters.Single(e => e.MemberName == "AccountGroupId").Value as int?;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsSalesSettlement>().Select(r => r.Id).ToArray();
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                            this.excelServiceClient.ExportPartsSalesSettlementForQueryWithDetailsAsync(ids, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null, null, null, null);
                            this.excelServiceClient.ExportPartsSalesSettlementForQueryWithDetailsCompleted -= excelServiceClient_ExportPartsSalesSettlementForQueryWithDetailsCompleted;
                            this.excelServiceClient.ExportPartsSalesSettlementForQueryWithDetailsCompleted += excelServiceClient_ExportPartsSalesSettlementForQueryWithDetailsCompleted;
                        }
                    } else {
                        this.excelServiceClient.ExportPartsSalesSettlementForQueryWithDetailsAsync(new int[] { }, BaseApp.Current.CurrentUserData.EnterpriseId, code, customerCompanyCode, partsSalesCategoryId, customerCompanyName, accountGroupId, status, createTimeBegin, createTimeEnd);
                        this.excelServiceClient.ExportPartsSalesSettlementForQueryWithDetailsCompleted -= excelServiceClient_ExportPartsSalesSettlementForQueryWithDetailsCompleted;
                        this.excelServiceClient.ExportPartsSalesSettlementForQueryWithDetailsCompleted += excelServiceClient_ExportPartsSalesSettlementForQueryWithDetailsCompleted;
                    }
                    break;
            }
        }

        private void excelServiceClient_ExportPartsSalesSettlementForQueryWithDetailsCompleted(object sender, ExportPartsSalesSettlementForQueryWithDetailsCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
