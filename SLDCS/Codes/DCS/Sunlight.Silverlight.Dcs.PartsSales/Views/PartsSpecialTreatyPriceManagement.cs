﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "PartsSpecialTreatyPrice", ActionPanelKeys = new[] {
        //CommonActionKeys.ADD_EDIT_ABANDON_EXPORT_IMPORT
        CommonActionKeys.ABANDON_EXPORT,"PartsSpecialTreatyPrice"
    })]
    public class PartsSpecialTreatyPriceManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport, dataEditViewImportAbandon;
        private DataEditViewBase partsSpecialTreatyPriceForEditDataEditView;
        private const string ADD_DATA_EDIT_VIEW = "_ADD_DataEditView_";
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private const string DATA_EDIT_VIEW_IMPORTABANDON = "_DataEditViewImportAbandon_";
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PartsSpecialTreatyPriceManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PartsSpecialTreatyPrice;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsSpecialTreatyPrice"));
            }
        }

        private DataEditViewBase DataEditViewForAdd {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSpecialTreatyPriceForAdd");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.partsSpecialTreatyPriceForEditDataEditView == null) {
                    this.partsSpecialTreatyPriceForEditDataEditView = DI.GetDataEditView("PartsSpecialTreatyPriceForEdit");
                    this.partsSpecialTreatyPriceForEditDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.partsSpecialTreatyPriceForEditDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.partsSpecialTreatyPriceForEditDataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("PartsSpecialTreatyPriceForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }

        private DataEditViewBase DataEditViewImportAbandon {
            get {
                if(this.dataEditViewImportAbandon == null) {
                    this.dataEditViewImportAbandon = DI.GetDataEditView("PartsSpecialTreatyPriceForImportAbandon");
                    this.dataEditViewImportAbandon.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewImportAbandon;
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(ADD_DATA_EDIT_VIEW, () => this.DataEditViewForAdd);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
            this.RegisterView(DATA_EDIT_VIEW_IMPORTABANDON, () => this.DataEditViewImportAbandon);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
            this.dataEditViewImportAbandon = null;
            this.partsSpecialTreatyPriceForEditDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            ClientVar.ConvertTime(composite);
            composite.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSpecialTreatyPrice"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    this.SwitchViewTo(ADD_DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsSpecialTreatyPrice>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件特殊协议价)
                                entity.作废配件特殊协议价();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "BatchAbandon":
                    var domainContext1 = new DcsDomainContext();
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsSpecialTreatyPrice>().Select(r => r.Id).ToArray();
                        try {
                            domainContext1.批量作废配件特殊协议价(ids, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                            UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsSpecialTreatyPrice>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportPartsSpecialTreatyPrice(ids, null, null, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var companyCode = compositeFilterItem.Filters.Single(r => r.MemberName == "Company.Code").Value as string;
                            var companyName = compositeFilterItem.Filters.Single(r => r.MemberName == "Company.Name").Value as string;
                          //  var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var status = compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            var sparePartsCode = compositeFilterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                            var sparePartsName = compositeFilterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                            var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? startDateTime = null;
                            DateTime? endDateTime = null;
                            if(createTime != null) {
                                startDateTime = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                endDateTime = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            this.dcsDomainContext.ExportPartsSpecialTreatyPrice(new int[] { }, BaseApp.Current.CurrentUserData.EnterpriseId, companyCode, companyName, null, status, sparePartsCode, sparePartsName, startDateTime, endDateTime, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
                case "ImportAbandon":
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORTABANDON);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case "ImportAbandon":
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsSpecialTreatyPrice>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.IMPORT:
                    return true;
                case "BatchAbandon":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<PartsSpecialTreatyPrice>().ToArray();
                    if(entities1.Length == 1)
                        return false;
                    return entities1.All(r => r.Status == (int)DcsBaseDataStatus.有效);
                default:
                    return false;
            }
        }
    }
}
