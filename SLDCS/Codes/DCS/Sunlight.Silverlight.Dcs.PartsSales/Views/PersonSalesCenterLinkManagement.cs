﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views
{
    [PageMeta("Common", "EnterpriseAndInternalOrganization", "PersonnelBrand", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_DELETE_EXPORT
    })]
    public class PersonSalesCenterLinkManagement : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private DataEditViewBase DataEditView
        {
            get
            {
                if (this.dataEditView == null)
                {
                    this.dataEditView = DI.GetDataEditView("PersonSalesCenterLink");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e)
        {
            this.ResetEditView();
            if (this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e)
        {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PersonSalesCenterLink"));
            }
        }

        public PersonSalesCenterLinkManagement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_PersonSalesCenterLink;
        }

        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PersonSalesCenterLink"
                };
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.DELETE:
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PersonSalesCenterLink>().ToArray();
                    if (entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if (composite == null)
                return;
            composite.Filters.Add(new FilterItem
            {
                MemberName = "CompanyId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.ADD:
                    this.DataEditView.CreateObjectToEdit<PersonSalesCenterLink>();
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<PersonSalesCenterLink>().First().PartsSalesCategoryId);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.DELETE:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Delete, () =>
                    {
                        var entity = this.DataGridView.SelectedEntities.Cast<PersonSalesCenterLink>().SingleOrDefault();
                        if (entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if (domainContext == null)
                            return;
                        try
                        {
                            if (entity.Can删除人员与销售中心关系)
                                entity.删除人员与销售中心关系();
                            domainContext.SubmitChanges(submitOp =>
                            {
                                if (submitOp.HasError)
                                {
                                    if (!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_DeleteSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        }
                        catch (Exception ex)
                        {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    //((DcsDataGridViewBase)this.DataGridView).ExportData();
                    //break;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PersonSalesCenterLink>().Select(e => e.Id).ToArray();
                        this.ExportPersonSalesCenterLinkComplete(ids, null, null, null, null, null);
                    } else {
                        var filterItem1 = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem1 == null)
                            return;
                        var partsSalesCategoryId = filterItem1.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filterItem1.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var personName = filterItem1.Filters.Single(e => e.MemberName == "Personnel.Name").Value as string;
                        var createTime = filterItem1.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }

                        this.ExportPersonSalesCenterLinkComplete(new int[] { }, partsSalesCategoryId, status, personName, createTimeBegin, createTimeEnd);
                    }
                    break;
            }
        }

        private void ExportPersonSalesCenterLinkComplete(int[] ids, int? partsSalesCategoryId,int? status, string personName, DateTime? createTimeBegin, DateTime? createTimeEnd)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPersonSalesCenterLinkAsync(ids, partsSalesCategoryId, status, personName, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportPersonSalesCenterLinkCompleted -= excelServiceClient_ExportPersonSalesCenterLinkCompleted;
            this.excelServiceClient.ExportPersonSalesCenterLinkCompleted += excelServiceClient_ExportPersonSalesCenterLinkCompleted;
        }

        private void excelServiceClient_ExportPersonSalesCenterLinkCompleted(object sender, ExportPersonSalesCenterLinkCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
