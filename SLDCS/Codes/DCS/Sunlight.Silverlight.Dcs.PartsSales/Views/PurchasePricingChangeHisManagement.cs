﻿
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using System;
using System.ServiceModel.DomainServices.Client;
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views
{
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "PurchasePricingChangeHis", ActionPanelKeys = new[] {
        CommonActionKeys.ABANDON_EXPORT})]
    public class PurchasePricingChangeHisManagement : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        public PurchasePricingChangeHisManagement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.QueryPanel_Title_PurchasePricingChangeHiStock;
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);         
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PurchasePricingChangeHis"));
            }
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PurchasePricingChangeHis"
                };
            }
        } protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var partCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var referenceCode = filterItem.Filters.Single(r => r.MemberName == "ReferenceCode").Value as string;
                    var partName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? validFromBegin = null;
                    DateTime? validFromEnd = null;
                    if(createTime != null) {
                        validFromBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        validFromEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var detailId = this.DataGridView.SelectedEntities.Cast<VirtualPurchasePricingChangeHi>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportPurchasePricingChangeHis(detailId, null, null,null, null, null, loadOp =>
                        {
                            ShellViewModel.Current.IsBusy = false;
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);
                    } else {
                        this.dcsDomainContext.ExportPurchasePricingChangeHis(null, partCode, partName,referenceCode, validFromBegin, validFromEnd, loadOp =>
                        {
                            ShellViewModel.Current.IsBusy = false;
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);
                    }
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () =>
                    {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualPurchasePricingChangeHi>().SingleOrDefault();
                        if (entity == null)
                            return;
                        ((IEditableObject)entity).EndEdit();
                        try
                        {
                            if (entity.CanabandonVirtualPurchasePricingChangeHi)
                            {
                                entity.abandonVirtualPurchasePricingChangeHi();
                            }
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if (domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp =>
                            {
                                if (submitOp.HasError)
                                {
                                    if (!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                                this.DataGridView.ExecuteQuery();
                            }, null);
                        }
                        catch (Exception ex)
                        {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return true;
                case CommonActionKeys.ABANDON:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1)
                    {
                        var boxupTask = DataGridView.SelectedEntities.Cast<VirtualPurchasePricingChangeHi>().FirstOrDefault();
                        if (boxupTask.Status == (int)DcsMasterDataStatus.有效)
                        {
                            return true;
                        }
                    }
                    return false;
                default:
                    return false;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
