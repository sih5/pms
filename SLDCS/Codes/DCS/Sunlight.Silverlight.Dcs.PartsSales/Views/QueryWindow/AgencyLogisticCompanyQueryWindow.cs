﻿

using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    //配件分品牌商务信息查询
    public class AgencyLogisticCompanyQueryWindow : DcsPopupsQueryWindowBase {

        public AgencyLogisticCompanyQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem {
                MemberName = "AgencyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
        }

        public override string QueryPanelKey {
            get {
                return "AgencyLogisticCompany";
            }
        }

        public override string DataGridViewKey {
            get {
                return "AgencyLogisticCompany";
            }
        }
    }
}
