﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "AgentsPartsSalesOrder", ActionPanelKeys = new[] {
        CommonActionKeys.MERGEEXPORT
    })]
    public class AgentsPartsSalesOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditDetailView;
        private DataGridViewBase logisticsDataGridView;
        private const string DATA_EDITDETAIL_VIEW = "_DataEditDetailView_";
        private const string DATA_LOGISTICS_VIEW = "_DATALOGISTICSVIEW_";
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public AgentsPartsSalesOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.QueryWindow_Title_AgentsPartsSalesOrderManagement;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgencyPartsSalesOrderWithDetails"));
            }
        }
        private DataGridViewBase LogisticsDataGridView {
            get {
                return this.logisticsDataGridView ?? (this.logisticsDataGridView = DI.GetDataGridView("Logistics"));
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsSalesOrder");
                    var item = (PartsSalesOrderDataEditView)this.dataEditView;
                    item.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        //增加清单修改功能
        private DataEditViewBase DataEditDetailView {
            get {
                if(this.dataEditDetailView == null) {
                    this.dataEditDetailView = DI.GetDataEditView("PartsSalesOrderForEditDetail");
                    this.dataEditDetailView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditDetailView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditDetailView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => DataEditView);
            this.RegisterView(DATA_EDITDETAIL_VIEW, () => DataEditView);
            this.RegisterView(DATA_EDITDETAIL_VIEW, () => this.DataEditDetailView);
            this.RegisterView(DATA_LOGISTICS_VIEW, () => this.LogisticsDataGridView);
        }


        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsSalesOrder"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.MERGEEXPORT: {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                            this.dcsDomainContext.ExportPartsSalesOrderWithDetailsByPersonnelIsAgency(this.DataGridView.SelectedEntities.Cast<PartsSalesOrder>().Select(r => r.Id).ToArray(), null, BaseApp.Current.CurrentUserData.UserId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        } else {
                            var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var eRPSourceOrderCode = filterItem.Filters.Single(r => r.MemberName == "ERPSourceOrderCode").Value as string;
                            var submitCompanyCode = filterItem.Filters.Single(r => r.MemberName == "SubmitCompanyCode").Value as string;
                            var submitCompanyName = filterItem.Filters.Single(r => r.MemberName == "SubmitCompanyName").Value as string;
                            var ifDirectProvision = filterItem.Filters.Single(e => e.MemberName == "IfDirectProvision").Value as bool?;
                            var salesCategoryId = filterItem.Filters.Single(e => e.MemberName == "SalesCategoryId").Value as int?;
                            var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                            var isDebt = filterItem.Filters.Single(e => e.MemberName == "IsDebt").Value as bool?;
                            var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                            var provinceId = filterItem.Filters.Single(e => e.MemberName == "ProvinceName").Value as int?;
                            var cityId = filterItem.Filters.Single(r => r.MemberName == "CityName").Value as int?;
                            var isAutoApprove = filterItem.Filters.Single(e => e.MemberName == "IsAutoApprove").Value as bool?;
                            var ifInnerDirectProvision = filterItem.Filters.Single(e => e.MemberName == "IfInnerDirectProvision").Value as bool?;
                            var overseasDemandSheetNo = filterItem.Filters.Single(r => r.MemberName == "OverseasDemandSheetNo").Value as string;
                            DateTime? createTimeBegin = null;
                            DateTime? createTimeEnd = null;
                            DateTime? submitTimeBegin = null;
                            DateTime? submitTimeEnd = null;
                            DateTime? abandonTimeBegin = null;
                            DateTime? abandonTimeEnd = null;
                            DateTime? approveTimeBegin = null;
                            DateTime? approveTimeEnd = null;
                            var dateTimeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem));
                            foreach(var dateTimeFilterItem in dateTimeFilterItems) {
                                var dateTime = dateTimeFilterItem as CompositeFilterItem;
                                if(dateTime != null) {
                                    if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                        createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }
                                    if(dateTime.Filters.Any(r => r.MemberName == "SubmitTime")) {
                                        submitTimeBegin = dateTime.Filters.First(r => r.MemberName == "SubmitTime").Value as DateTime?;
                                        submitTimeEnd = dateTime.Filters.Last(r => r.MemberName == "SubmitTime").Value as DateTime?;
                                    }
                                    if(dateTime.Filters.Any(r => r.MemberName == "AbandonTime")) {
                                        abandonTimeBegin = dateTime.Filters.First(r => r.MemberName == "AbandonTime").Value as DateTime?;
                                        abandonTimeEnd = dateTime.Filters.Last(r => r.MemberName == "AbandonTime").Value as DateTime?;
                                    }
                                    if(dateTime.Filters.Any(r => r.MemberName == "ApproveTime")) {
                                        approveTimeBegin = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                        approveTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                    }
                                }
                            }
                            //if(createTimeBegin == null || createTimeEnd == null) {
                            //    UIHelper.ShowAlertMessage("请选择创建时间");
                            //    return;
                            //}
                            //var time = createTimeEnd - createTimeBegin;
                            //if(((TimeSpan)time).Days > 62) {
                            //    UIHelper.ShowAlertMessage("查询创建时长不可超出2个月");
                            //    return;
                            //}
                            var partsSalesOrderTypeId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesOrderTypeId").Value as int?;
                            this.dcsDomainContext.ExportPartsSalesOrderWithDetailsByPersonnelIsAgency(new int[] { }, eRPSourceOrderCode, BaseApp.Current.CurrentUserData.UserId, provinceId, cityId, code, submitCompanyCode, submitCompanyName, partsSalesOrderTypeId, salesCategoryId, status, warehouseId, isDebt, ifDirectProvision, createTimeBegin, createTimeEnd, submitTimeBegin, submitTimeEnd, abandonTimeBegin, abandonTimeEnd, approveTimeBegin, approveTimeEnd, isAutoApprove, ifInnerDirectProvision, overseasDemandSheetNo, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                        break;
                    }

            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();

                default:
                    return false;
            }
        }
    }
}