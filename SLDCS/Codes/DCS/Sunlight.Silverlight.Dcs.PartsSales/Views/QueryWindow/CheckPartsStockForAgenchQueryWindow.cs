﻿
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 代理库库存查询
    /// 必须参数：销售订单id(partsSalesOrderId)
    /// </summary>
    public class CheckPartsStockForAgenchQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "CheckPartsStockForAgench";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CheckPartsStockForAgench";
            }
        }
    }
}
