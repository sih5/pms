﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Dcs.PartsSales.Views.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    public class DealerPartsRetailOrderQueryWindow : DcsMultiSelectQueryWindowBase {
        protected override UserControlBase CustomerView {
            get {
                return this.customerView ?? (this.customerView = new PartsOutboundBillDetailForPartsRetailDealerOrderView());
            }
        }

        public override string[] DataGridViewKeies {
            get {
                return new[] {
                    ""
                };
            }
        }

        public override string[] GridViewTitles {
            get {
                return new[] {
                    ""
                };
            }
        }

        public override string QueryPanelKey {
            get {
                return "DealerPartsRetailOrderDelaer";
            }
        }

        protected override void QueryPanel_ExecutingQuery(object sender, FilterPanelRibbonGroup.ExecutingQueryEventArgs e) {
            if(CustomerView != null) {
                var multiSelectView = this.CustomerView as PartsOutboundBillDetailForPartsRetailDealerOrderView;
                if(multiSelectView != null) {
                    multiSelectView.ExecuteQuery(e.Filter);
                }
            }
        }

        public override IEnumerable<Entity> SelectedEntities {
            get {
                var multiSelectView = this.customerView as PartsOutboundBillDetailForPartsRetailDealerOrderView;
                if(multiSelectView != null)
                    return multiSelectView.SelectedEntities;
                return Enumerable.Empty<DealerRetailOrderDetail>();
            }
        }
    }
}
