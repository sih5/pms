﻿
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 服务站零售选择配件
    /// 必须参数：参数经销商Id（DealerId）,配件销售类型Id（SalesCategoryId）
    /// </summary>
    public class DealerPartsStockQueryWindow : DcsMultiPopupsQueryWindowBase {

        public override string QueryPanelKey {
            get {
                return "DealerPartsStock";
            }
        }

        public override string DataGridViewKey {
            get {
                return "DealerPartsStock";
            }
        }
    }
}
