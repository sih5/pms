﻿using System;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 出入库记录查询窗体
    /// </summary>
    public class InAndOutHistoryForExportQueryWindow : DcsExportQueryWindowBase {
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        public InAndOutHistoryForExportQueryWindow() {
            this.Export -= RepairItemForExportQueryWindow_Export;
            this.Export += RepairItemForExportQueryWindow_Export;
        }

        private void RepairItemForExportQueryWindow_Export(object sender, EventArgs e) {
            var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
            if(filterItem == null) {
                UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_InAndOutHistory_Query);
                return;
            }
            if(this.DataGridView.Entities == null || !this.DataGridView.Entities.Any()) {
                UIHelper.ShowNotification(PartsSalesUIStrings.Management_Validation_InAndOutHistory_NoData);
                return;

            }
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                var billCodes = this.DataGridView.SelectedEntities.Cast<DealerPartsStockOutInRecord>().Select(ex => ex.code.ToUpper()).ToArray();
                int[] partsIds = null;
                var sparepartIds = filterItem.Filters.LastOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(sparepartIds != null) {
                    FilterItem partIdsfilterItem = sparepartIds.Filters.SingleOrDefault(item => item.MemberName == "SparePartIds");
                    if(partIdsfilterItem == null)
                        sparepartIds = null;
                    partsIds = partIdsfilterItem.Value as int[];
                }
                this.dcsDomainContext.ExportDealerPartsStockOutInRecord(billCodes, null, partsIds, null, null, loadOp => {
                    if(loadOp.HasError)
                        return;
                    if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                        UIHelper.ShowNotification(loadOp.Value);
                    }
                    if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                        HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    }
                }, null);
            } else {
                int[] partsIds = null;
                string code = null;
                var dateFilter = filterItem.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                DateTime? dateTimeBegin = null;
                DateTime? dateTimeEnd = null;
                if(dateFilter != null) {
                    var existTimeFilter = dateFilter.Filters.FirstOrDefault(r => r.MemberName == "CreateTime");
                    if(existTimeFilter != null) {
                        dateTimeBegin = dateFilter.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        dateTimeEnd = dateFilter.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                }
                FilterItem filterItem1 = filterItem.Filters.SingleOrDefault(item => item.MemberName == "code");
                if(filterItem1 != null) {
                    code = filterItem1.Value as string;
                }
                var sparepartIds = filterItem.Filters.LastOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(sparepartIds != null) {
                    FilterItem partIdsfilterItem = sparepartIds.Filters.SingleOrDefault(item => item.MemberName == "SparePartIds");
                    if(partIdsfilterItem == null)
                        sparepartIds = null;
                    partsIds = partIdsfilterItem.Value as int[];
                }
                this.dcsDomainContext.ExportDealerPartsStockOutInRecord(null, code, partsIds, dateTimeBegin, dateTimeEnd, loadOp => {
                    if(loadOp.HasError)
                        return;
                    if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                        UIHelper.ShowNotification(loadOp.Value);
                    }
                    if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                        HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    }
                }, null);
            }
        }

        public override string QueryPanelKey {
            get {
                return "InAndOutHistoryForExport";
            }
        }


        public override string DataGridViewKey {
            get {
                return "InAndOutHistoryForExport";
            }
        }
    }
}
