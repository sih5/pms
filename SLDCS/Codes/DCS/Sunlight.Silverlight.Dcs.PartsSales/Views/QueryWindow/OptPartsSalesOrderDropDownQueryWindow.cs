﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择配件销售订单
    /// </summary>
    public class OptPartsSalesOrderDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public OptPartsSalesOrderDropDownQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem {
                MemberName = "InvoiceReceiveCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.QueryWindow_Title_OptPartsSalesOrderDrop;
            }
        }

        public override string DataGridViewKey {
            get {
                return "OptPartsSalesOrder";
            }
        }

        public override string QueryPanelKey {
            get {
                return "OptPartsSalesOrder";
            }
        }
    }
}
