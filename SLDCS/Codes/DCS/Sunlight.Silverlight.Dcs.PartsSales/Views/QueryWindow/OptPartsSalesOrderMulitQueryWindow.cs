﻿using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择配件销售订单(多选)
    /// </summary>
    public class OptPartsSalesOrderMulitQueryWindow : DcsMultiPopupsQueryWindowBase {

        public OptPartsSalesOrderMulitQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem {
                MemberName = "InvoiceReceiveCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
        }

        public override string QueryPanelKey {
            get {
                return "OptPartsSalesOrder";
            }
        }

        public override string DataGridViewKey {
            get {
                return "OptPartsSalesOrder";
            }
        }
    }
}
