﻿
using Sunlight.Silverlight.Core.Model;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择配件销售订单（单选）
    /// </summary>
    public class OptPartsSalesOrderQueryWindow : DcsQueryWindowBase {

        public OptPartsSalesOrderQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem {
                MemberName = "InvoiceReceiveCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
        }
        public override string DataGridViewKey {
            get {
                return "OptPartsSalesOrder";
            }
        }

        public override string QueryPanelKey {
            get {
                return "OptPartsSalesOrder";
            }
        }
    }
}
