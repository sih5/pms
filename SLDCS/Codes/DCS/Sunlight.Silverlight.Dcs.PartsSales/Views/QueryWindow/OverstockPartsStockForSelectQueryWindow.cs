﻿
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    public class OverstockPartsStockForSelectQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "OverstockPartsStockForQuery";
            }
        }

        public override string QueryPanelKey {
            get {
                return "OverstockPartsStockForQuery";
            }
        }
    }
}
