﻿

using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    //配件分品牌商务信息查询
    public class PartsBranchQueryWindow : DcsMultiPopupsQueryWindowBase {

        public PartsBranchQueryWindow() {
            //this.SetDefaultFilterItem(new FilterItem {
            //    MemberName = "InvoiceReceiveCompanyId",
            //    MemberType = typeof(int),
            //    Operator = FilterOperator.IsEqualTo,
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId
            //});
            //this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            //this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string QueryPanelKey {
            get {
                return "PartsBranchQuery";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsBranchQuery";
            }
        }
    }
}
