﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 查询仓库库存
    /// </summary>
    public class PartsBranchSelectQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return PartsSalesUIStrings.QueryWindow_Title_PartsBranchSelectQuery;
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsBranchQuery";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsBranchQuery";
            }
        }
    }
}
