﻿
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择配件销售订单(多选)
    /// </summary>
    public class PartsOutboundBillDetailForMulitQueryDetailQueryWindow : DcsMultiPopupsQueryWindowBase {
        public override string QueryPanelKey {
            get {
                return "PartsOutboundBillDetailForQueryDetail";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsOutboundBillDetailForQueryDetail";
            }
        }

        public void SetQueryPanelParameters(int partsSalesCategoryId) {
            this.QueryPanel.ExchangeData(null, null, new object[] {
                partsSalesCategoryId
            });
        }
    }
}
