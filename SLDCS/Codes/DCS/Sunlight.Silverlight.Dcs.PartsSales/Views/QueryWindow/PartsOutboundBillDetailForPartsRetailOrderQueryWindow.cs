﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Dcs.PartsSales.Views.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 根据配件零售订单选择出库明细
    /// </summary>
    public class PartsOutboundBillDetailForPartsRetailOrderQueryWindow : DcsMultiSelectQueryWindowBase {
        protected override UserControlBase CustomerView {
            get {
                return this.customerView ?? (this.customerView = new PartsOutboundBillDetailForPartsRetailOrderView());
            }
        }

        public override string[] DataGridViewKeies {
            get {
                return new[] {
                    ""
                };
            }
        }

        public override string[] GridViewTitles {
            get {
                return new[] {
                    ""
                };
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsOutboundBillDetailForPartsRetailOrder";
            }
        }

        protected override void QueryPanel_ExecutingQuery(object sender, FilterPanelRibbonGroup.ExecutingQueryEventArgs e) {
            if(CustomerView != null) {
                var multiSelectView = this.CustomerView as PartsOutboundBillDetailForPartsRetailOrderView;
                if(multiSelectView != null) {
                    multiSelectView.ExecuteQuery(e.Filter);
                }
            }
        }

        public override IEnumerable<Entity> SelectedEntities {
            get {
                var multiSelectView = this.customerView as PartsOutboundBillDetailForPartsRetailOrderView;
                if(multiSelectView != null)
                    return multiSelectView.SelectedEntities;
                return Enumerable.Empty<PartsOutboundBillDetail>();
            }
        }
    }
}
