﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Views.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 根据配件订单选择出库明细
    /// </summary>
    public class PartsOutboundBillDetailForPartsSalesOrderQueryWindow : DcsMultiSelectQueryWindowBase {
        public PartsOutboundBillDetailForPartsSalesOrderQueryWindow() {
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.LogicalOperator = LogicalOperator.Or;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsPartsSalesOrderStatus.部分审批
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsPartsSalesOrderStatus.审批完成
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsPartsSalesOrderStatus.终止
            });
            this.SetDefaultFilterItem(compositeFilterItem);
        }

        public override string[] DataGridViewKeies {
            get {
                return new[] {
                    ""
                };
            }
        }

        protected override UserControlBase CustomerView {
            get {
                return this.customerView ?? (this.customerView = new PartsOutboundBillDetailForPartsSalesOrderCustomView());
            }
        }

        public override string[] GridViewTitles {
            get {
                return new[] {
                    ""
                };
            }
        }

        protected override void QueryPanel_ExecutingQuery(object sender, FilterPanelRibbonGroup.ExecutingQueryEventArgs e) {
            if(CustomerView != null) {
                var multiSelectView = this.CustomerView as PartsOutboundBillDetailForPartsSalesOrderCustomView;
                if(multiSelectView != null) {
                    var compositeFilterItem = new CompositeFilterItem();
                    if(this.DefaultFilterItem != null)
                        compositeFilterItem.Filters.Add(this.DefaultFilterItem);
                    if(this.AdditionalFilterItem != null)
                        compositeFilterItem.Filters.Add(this.AdditionalFilterItem);
                    var eCompositeFilterItem = e.Filter as CompositeFilterItem;
                    if(eCompositeFilterItem != null)
                        ClientVar.ConvertTime(eCompositeFilterItem);
                    compositeFilterItem.Filters.Add(eCompositeFilterItem);
                    multiSelectView.ExecuteQuery(compositeFilterItem);
                }
            }
        }

        public void SetQueryPanelParameters(int partsSalesCategoryId) {
            this.QueryPanel.ExchangeData(null, null, new object[] {
               partsSalesCategoryId
            });
        }

        public override string QueryPanelKey {
            get {
                return "PartsOutboundBillDetailForPartsSalesOrder";
            }
        }

        public override IEnumerable<Entity> SelectedEntities {
            get {
                if(customerView != null) {
                    var multiSelectView = this.CustomerView as PartsOutboundBillDetailForPartsSalesOrderCustomView;
                    if(multiSelectView != null) {
                        return multiSelectView.SelectedEntities;
                    }
                }
                return Enumerable.Empty<Entity>();
            }
        }
    }
}
