﻿
using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 配件零售清单选择
    /// </summary>
    public class PartsRetailDetailQueryWindow : DcsMultiPopupsQueryWindowBase {

        public override string QueryPanelKey {
            get {
                return "PartsRetailDetailForSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsRetailDetailForSelect";
            }
        }
    }
}
