﻿
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 配件销售加价率选择(单选)
    /// </summary>
    public class PartsSalePriceIncreaseRateForSelectQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "PartsSalePriceIncreaseRateForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSalePriceIncreaseRateForSelect";
            }
        }
    }
}
