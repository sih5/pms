﻿
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    public class PartsSalesPriceApplicationQueryWindow : DcsPopupsQueryWindowBase {
        /// <summary>
        /// 配件价格跨库查询
        /// </summary>
        
        public override string DataGridViewKey {
            get {
                return "PartsSalesBrandPrice";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSalesBrandPrice";
            }
        }



    }
}
