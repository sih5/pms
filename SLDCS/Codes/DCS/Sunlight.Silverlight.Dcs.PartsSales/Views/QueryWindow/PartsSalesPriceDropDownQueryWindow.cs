﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 配件选择界面
    /// </summary>
    public class PartsSalesPriceDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "PartsSalesPriceForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSalesPriceForSelect";
            }
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.QueryPanel_Title_PartsSalesPrice_PartsSelect;
            }
        }
    }
}
