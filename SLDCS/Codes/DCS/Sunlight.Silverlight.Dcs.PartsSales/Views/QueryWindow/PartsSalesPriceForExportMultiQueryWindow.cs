﻿

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择配件界面(多选)
    /// </summary>
    public class PartsSalesPriceForExportMultiQueryWindow : DcsMultiPopupsQueryWindowBase {

        public override string QueryPanelKey {
            get {
                return "PartsSalesPriceForExportSelect";

            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsSalesPriceForExportSelect";
            }
        }
    }
}