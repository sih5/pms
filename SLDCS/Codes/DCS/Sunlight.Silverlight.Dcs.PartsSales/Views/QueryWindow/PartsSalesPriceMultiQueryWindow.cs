﻿

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择配件界面(多选)
    /// </summary>
    public class PartsSalesPriceMultiQueryWindow : DcsMultiPopupsQueryWindowBase {

        public override string QueryPanelKey {
            get {
                return "PartsSalesPriceForSelect";

            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsSalesPriceForSelect";
            }
        }
    }
}