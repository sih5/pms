﻿namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 配件选择界面
    /// </summary>
    public class PartsSalesPriceQueryWindow : DcsMultiPopupsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "PartsSalesPriceForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsSalesPriceForSelect";
            }
        }

    }
}
