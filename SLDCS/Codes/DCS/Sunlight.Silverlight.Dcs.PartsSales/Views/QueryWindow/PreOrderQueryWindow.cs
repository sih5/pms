﻿
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 预订单选择
    /// </summary>
    public class PreOrderQueryWindow : DcsMultiPopupsQueryWindowBase {
        public override string QueryPanelKey {
            get {
                return "PreOrder";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PreOrder";
            }
        }
    }
}
