﻿
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择零售客户
    /// </summary>
    public class RetailOrderCustomerDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "RetailOrderCustomer";
            }
        }

        public override string QueryPanelKey {
            get {
                return "RetailOrderCustomer";
            }
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.QueryPanel_Title_RetailOrderCustomer;
            }
        }

    }
}
