﻿
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择零售客户
    /// </summary>
    public class RetailOrderCustomerQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "RetailOrderCustomer";
            }
        }

        public override string QueryPanelKey {
            get {
                return "RetailOrderCustomer";
            }
        }

    }
}
