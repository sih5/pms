﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 二级站配件需求选择
    /// </summary>
    public class SecondClassStationPlanDropDwonQueryWindow : DcsDropDownQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "SecondClassStationPlanForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SecondClassStationPlanForSelect";
            }
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.QueryPanel_Title_SecondClassStationPlan_PartsSelect;
            }
        }
    }
}
