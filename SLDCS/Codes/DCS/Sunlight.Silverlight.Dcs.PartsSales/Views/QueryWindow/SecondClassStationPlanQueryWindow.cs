﻿
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 二级站配件需求选择
    /// </summary>
    public class SecondClassStationPlanQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "SecondClassStationPlanForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SecondClassStationPlanForSelect";
            }
        }
    }
}
