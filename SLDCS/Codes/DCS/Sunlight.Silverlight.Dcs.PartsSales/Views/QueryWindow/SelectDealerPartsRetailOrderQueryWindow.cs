﻿namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择服务站配件零售订单
    /// </summary>
    public class SelectDealerPartsRetailOrderQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "SelectDealerPartsRetailOrder";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SelectDealerPartsRetailOrder";
            }
        }

    }
}
