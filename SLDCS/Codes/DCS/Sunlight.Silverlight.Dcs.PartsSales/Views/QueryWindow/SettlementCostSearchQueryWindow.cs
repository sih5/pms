﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    public class SettlementCostSearchQueryWindow : DcsExportQueryWindowBase {
        public SettlementCostSearchQueryWindow() {
            this.Export -= SettlementCostSearchQueryWindow_Export;
            this.Export += SettlementCostSearchQueryWindow_Export;
        }

        private void SettlementCostSearchQueryWindow_Export(object sender, EventArgs e) {
            ((DcsDataGridViewBase)this.DataGridView).ExportData();
        }

        public override string DataGridViewKey {
            get {
                return "SettlementCostSearch";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SettlementCostSearch";
            }
        }
    }
}
