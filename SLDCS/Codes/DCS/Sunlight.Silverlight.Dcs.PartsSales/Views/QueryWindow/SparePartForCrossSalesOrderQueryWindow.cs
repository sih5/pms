﻿using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    public class SparePartForCrossSalesOrderQueryWindow : DcsDropDownQueryWindowBase {
        public SparePartForCrossSalesOrderQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }
        public override string Title {
            get {
                return "配件选择";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SparePartForCrossSalesOrder";

            }
        }

        public override string DataGridViewKey {
            get {
                return "SparePartForCrossSalesOrder";
            }
        }
    }
}
