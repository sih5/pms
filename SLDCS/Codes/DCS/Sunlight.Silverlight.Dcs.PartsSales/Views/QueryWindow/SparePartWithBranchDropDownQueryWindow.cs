﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择配件信息
    /// </summary>
    public class SparePartWithBranchDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public SparePartWithBranchDropDownQueryWindow() {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsMasterDataStatus.有效);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.QueryWindow_Title_SparePartWithBranchDrop;
            }
        }

        public override string DataGridViewKey {
            get {
                return "SparePart1";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SparePart1";
            }
        }
    }
}
