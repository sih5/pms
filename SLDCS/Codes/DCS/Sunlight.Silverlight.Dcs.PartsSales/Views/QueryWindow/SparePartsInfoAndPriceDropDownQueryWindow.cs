﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 销售价维护选择配件信息
    /// </summary>
    public class SparePartsInfoAndPriceDropDownQueryWindow : DcsMultiDataDropDownQueryWindowBase {
        private readonly Grid grid = new Grid();
        private readonly RadMaskedNumericInput ratioBox = new RadMaskedNumericInput();

        public SparePartsInfoAndPriceDropDownQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);

            grid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            grid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = GridLength.Auto
            });
            grid.Children.Add(new TextBlock {
                Text = PartsSalesUIStrings.DataEditPanel_Text_Common_Coefficient,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Margin = new Thickness(5, 0, 5, 0)
            });
            ratioBox.Width = 120;
            ratioBox.Value = 1;
            ratioBox.Mask = "n2";
            ratioBox.Margin = new Thickness(5, 0, 5, 0);
            ratioBox.HorizontalContentAlignment = HorizontalAlignment.Right;
            ratioBox.HorizontalAlignment = HorizontalAlignment.Center;
            ratioBox.VerticalAlignment = VerticalAlignment.Center;
            ratioBox.SetValue(Grid.ColumnProperty, 1);
            grid.Children.Add(ratioBox);
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.QueryPanel_Title_SparePartsInfoAndPrice;
            }
        }

        public override string[] DataGridViewKeies {
            get {
                return new[] {
                    "PartsRetailGuidePriceForSelect", "PartsPurchasePricingDetailForSelect"
                };
            }
        }

        public override string QueryPanelKey {
            get {
                return "SparePartsInfoAndPrice";
            }
        }

        public override string[] GridViewTitles {
            get {
                return new[] {
                    PartsSalesUIStrings.DataGridView_Title_PartsPurchasingOrderDetail, PartsSalesUIStrings.DataGridView_Title_PartsContractPrice
                };
            }
        }

        protected override Grid AdditionalComponent {
            get {
                return grid;
            }
        }

        protected override IEnumerable<Entity> SelectedEntitiesProcessing(IEnumerable<Entity> entities) {
            decimal ratio;
            decimal.TryParse(ratioBox.Value.ToString(), out ratio);
            var enumerable = entities as Entity[] ?? entities.ToArray();
            foreach(var entity in enumerable) {
                if(entity is PartsRetailGuidePrice) {
                    ((PartsRetailGuidePrice)entity).Ratio = ratio;
                } else if(entity is PartsPurchasePricingDetail) {
                    ((PartsPurchasePricingDetail)entity).Ratio = ratio;
                }
            }
            return base.SelectedEntitiesProcessing(enumerable);
        }

        protected override FilterItem OnRequestFilterItem(string gridViewKey, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(gridViewKey == "PartsPurchasePricingDetailForSelect") {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var item in originalFilterItem.Filters) {
                        if(item.MemberName == "Status")
                            continue;
                        compositeFilterItem.Filters.Add(item);
                    }
            }
            return compositeFilterItem;
        }
    }
}
