﻿using System;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 配件销售结算成本查询
    /// </summary>
    public class VirtualPartsSalesSettlementQueryWindow : DcsExportQueryWindowBase {
        public VirtualPartsSalesSettlementQueryWindow() {
            this.Export -= VirtualPartsSalesSettlementQueryWindow_Export;
            this.Export += VirtualPartsSalesSettlementQueryWindow_Export;
        }

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private void VirtualPartsSalesSettlementQueryWindow_Export(object sender, EventArgs e) {
            var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                foreach(var composite in compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                    var compositeInternal = composite as CompositeFilterItem;
                    var id = (int)compositeInternal.Filters.First(r => r.MemberName == "Id").Value;
                    this.dcsDomainContext.ExportVirtualPartsSalesSettlement(new int[] { }, id, loadOp => {
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                }
            }
        }

        public override string DataGridViewKey {
            get {
                return "VirtualPartsSalesSettlement";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VirtualPartsSalesSettlement";
            }
        }
    }
}
