﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择零售配件
    /// </summary>
    public class VirtualPartsStockForPartsRetailDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "VirtualPartsStockForPartsRetail";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VirtualPartsStockForPartsRetail";
            }
        }

        public override string Title {
            get {
                return PartsSalesUIStrings.QueryPanel_Title_VirtualPartsStock;
            }
        }
    }
}
