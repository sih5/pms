﻿namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 选择零售配件
    /// </summary>
    public class VirtualPartsStockForPartsRetailQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "VirtualPartsStockForPartsRetail";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VirtualPartsStockForPartsRetail";
            }
        }
    }
}
