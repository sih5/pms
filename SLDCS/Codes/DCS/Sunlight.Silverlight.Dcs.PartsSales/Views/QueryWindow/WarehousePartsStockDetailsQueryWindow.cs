﻿

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 查询库存明细
    /// </summary>
    public class WarehousePartsStockDetailsQueryWindow : DcsMultiPopupsQueryWindowBase {


        public override string QueryPanelKey {
            get {
                return "WarehousePartsStock";
            }
        }

        public override string DataGridViewKey {
            get {
                return "WarehousePartsStockDetails";
            }
        }
    }
}
