﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 查询仓库库存
    /// </summary>
    public class WarehousePartsStockDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return PartsSalesUIStrings.QueryPanel_Title_WarehousePartsStock;
            }
        }

        public override string DataGridViewKey {
            get {
                return "WarehousePartsStock";
            }
        }

        public override string QueryPanelKey {
            get {
                return "WarehousePartsStock";
            }
        }
    }
}
