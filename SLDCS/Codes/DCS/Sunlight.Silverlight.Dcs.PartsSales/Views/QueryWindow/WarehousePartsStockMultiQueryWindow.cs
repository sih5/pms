﻿

namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 查询仓库库存(多选)
    /// </summary>
    public class WarehousePartsStockMultiQueryWindow : DcsMultiPopupsQueryWindowBase {

        public override string QueryPanelKey {
            get {
                return "WarehousePartsStock";
            }
        }

        public override string DataGridViewKey {
            get {
                return "WarehousePartsStock";
            }
        }
    }
}
