﻿namespace Sunlight.Silverlight.Dcs.PartsSales.Views.QueryWindow {
    /// <summary>
    /// 查询仓库库存
    /// </summary>
    public class WarehousePartsStockQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "WarehousePartsStock";
            }
        }

        public override string QueryPanelKey {
            get {
                return "WarehousePartsStock";
            }
        }
    }
}
