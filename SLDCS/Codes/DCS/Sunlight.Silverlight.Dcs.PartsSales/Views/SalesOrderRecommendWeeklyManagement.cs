﻿using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using System;
using System.ServiceModel.DomainServices.Client;
namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
      [PageMeta("PartsSales", "IntelligentPrediction", "SalesOrderRecommendWeekly", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT})]
    public class SalesOrderRecommendWeeklyManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        public SalesOrderRecommendWeeklyManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.Management_Title_SalesOrderRecommendWeekly;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SalesOrderRecommendWeekly"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SalesOrderRecommendWeekly"
                };
            }
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var centerName = filterItem.Filters.Single(e => e.MemberName == "CenterName").Value as string;
                    var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    DateTime? bCreateTime = null;
                    DateTime? eCreateTime = null;
                    var dateTimeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem));
                            foreach(var dateTimeFilterItem in dateTimeFilterItems) {
                                var dateTime = dateTimeFilterItem as CompositeFilterItem;
                                if(dateTime != null) {
                                    if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                        bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }
                                  
                                }
                            }
                    ShellViewModel.Current.IsBusy = true;
                    this.dcsDomainContext.导出销售周订单推荐(centerName, sparePartCode, type, bCreateTime, eCreateTime, loadOp => {
                        if(loadOp.HasError) {
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;                
            }

        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}

