﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("Common", "Dealer", "SalesUnit", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT,"SalesUnit"
    })]
    public class SalesUnitManagement : DcsDataManagementViewBase {
        private const string MAINTAIN_DATA_EDIT_VIEW = "_MaintainDataEditView_";
        private DataGridViewBase salesUnitDataGridView;

        private DataEditViewBase salesUnitDataEditView;

        private DataGridViewBase SalesUnitDataGridView {
            get {
                return this.salesUnitDataGridView ?? (this.salesUnitDataGridView = DI.GetDataGridView("SalesUnitWithDetails"));
            }
        }

        private DataEditViewBase SalesUnitDataEditView {
            get {
                if(this.salesUnitDataEditView == null) {
                    this.salesUnitDataEditView = DI.GetDataEditView("SalesUnit");
                    this.salesUnitDataEditView.EditCancelled += this.SalesUnitDataEditView_EditCancelled;
                    this.salesUnitDataEditView.EditSubmitted += this.SalesUnitDataEditView_EditSubmitted;
                }
                return this.salesUnitDataEditView;
            }
        }

        private DataEditViewBase salesUnitForMainTainDataEditView;

        private DataEditViewBase SalesUnitForMainTainDataEditView {
            get {
                if(this.salesUnitForMainTainDataEditView == null) {
                    this.salesUnitForMainTainDataEditView = DI.GetDataEditView("SalesUnitForMainTain");
                    this.salesUnitForMainTainDataEditView.EditCancelled += this.SalesUnitDataEditView_EditCancelled;
                    this.salesUnitForMainTainDataEditView.EditSubmitted += this.SalesUnitDataEditView_EditSubmitted;
                }
                return this.salesUnitForMainTainDataEditView;
            }
        }
        private void ResetEditView() {
            this.salesUnitDataEditView = null;
            this.salesUnitForMainTainDataEditView = null;
        }
        private void SalesUnitDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.SalesUnitDataGridView.FilterItem != null)
                this.SalesUnitDataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void SalesUnitDataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public SalesUnitManagement() {
            this.Title = PartsSalesUIStrings.DataManagementView_Title_SalesUnit;
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.SalesUnitDataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.SalesUnitDataEditView);
            this.RegisterView(MAINTAIN_DATA_EDIT_VIEW, () => this.SalesUnitForMainTainDataEditView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                var status = compositeFilterItem.Filters.SingleOrDefault(filter => filter.MemberName == "Status");
                if(status != null && status.Value == null) {
                    UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_StatusNotNull);
                    return;
                }
            } else {
                if(filterItem.MemberName == "Status")
                    if(filterItem.Value == null) {
                        UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_StatusNotNull);
                        return;
                    }
                compositeFilterItem.Filters.Add(filterItem);
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.SalesUnitDataGridView.FilterItem = compositeFilterItem;
            this.SalesUnitDataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var salesUnit = this.SalesUnitDataEditView.CreateObjectToEdit<SalesUnit>();
                    salesUnit.Status = (int)DcsMasterDataStatus.有效;
                    salesUnit.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    salesUnit.OwnerCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.SalesUnitDataEditView.SetObjectToEditById(this.SalesUnitDataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "Maintain":
                    this.SalesUnitForMainTainDataEditView.SetObjectToEditById(this.SalesUnitDataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(MAINTAIN_DATA_EDIT_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case "Maintain":
                    if(this.SalesUnitDataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.SalesUnitDataGridView.SelectedEntities.Cast<SalesUnit>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SalesUnit"
                };
            }
        }


    }
}
