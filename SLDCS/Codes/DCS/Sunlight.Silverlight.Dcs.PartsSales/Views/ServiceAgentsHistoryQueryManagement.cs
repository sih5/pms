﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "ServiceAgentsHistoryQuery", ActionPanelKeys = new[]{
    CommonActionKeys.EXPORT
    })]
    public class ServiceAgentsHistoryQueryManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        public ServiceAgentsHistoryQueryManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.QueryPanel_Title_ServiceAgentsHistoryQuery;
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("ServiceAgentsHistoryQuery"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "ServiceAgentsHistoryQuery"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var branchId = filterItem.Filters.Single(e => e.MemberName == "BranchId").Value as int?;
                    var salesCategoryId = filterItem.Filters.Single(e => e.MemberName == "SalesCategoryId").Value as int?;
                    var salesUnitOwnerCompanyId = filterItem.Filters.Single(e => e.MemberName == "SalesUnitOwnerCompanyId").Value as int?;
                    var partsSalesOrderTypeId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesOrderTypeId").Value as int?;
                    var submitCompanyCode = filterItem.Filters.Single(r => r.MemberName == "SubmitCompanyCode").Value as string;
                    var submitCompanyName = filterItem.Filters.Single(r => r.MemberName == "SubmitCompanyName").Value as string;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                    DateTime? startCreateTime = null;
                    DateTime? endCreateTime = null;
                    DateTime? startSubmitTime = null;
                    DateTime? endSubmitTime = null;
                    foreach(var dateTimeFilterItem in compositeFilterItems) {
                        var dateTime = dateTimeFilterItem as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                startCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                endCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.Any(r => r.MemberName == "SubmitTime")) {
                                startSubmitTime = dateTime.Filters.First(r => r.MemberName == "SubmitTime").Value as DateTime?;
                                endSubmitTime = dateTime.Filters.Last(r => r.MemberName == "SubmitTime").Value as DateTime?;
                            }
                        }
                    }
                    this.dcsDomainContext.ExportServiceAgentsHistoryQuery(branchId, salesCategoryId, salesUnitOwnerCompanyId, partsSalesOrderTypeId, submitCompanyCode, submitCompanyName, code, startCreateTime, endCreateTime, startSubmitTime, endSubmitTime, loadOp => {
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
