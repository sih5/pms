﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSalesSettlement", "SettlementAutomaticTaskSet", ActionPanelKeys = new[]{
        CommonActionKeys.ADD_EDIT_EXPORT
    })]
    public class SettlementAutomaticTaskSetManagement : DcsDataManagementViewBase {
         private DataEditViewBase dataEditView;
         private DataGridViewBase dataGridView;
         private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
         private DataEditViewBase DataEditView {
             get {
                 if(this.dataEditView == null) {
                     this.dataEditView = DI.GetDataEditView("SettlementAutomaticTaskSet");
                     this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                     this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                 }
                 return dataEditView;
             }
         }
        private void ResetEditView() {
            this.dataEditView = null;
        }
         private void dataEditView_EditSubmitted(object sender, EventArgs e) {
             this.ResetEditView();
             if(this.DataGridView.FilterItem != null)
                 this.DataGridView.ExecuteQueryDelayed();
             this.SwitchViewTo(DATA_GRID_VIEW);
         }

         private DataGridViewBase DataGridView {
             get {
                 return dataGridView ?? (this.dataGridView = DI.GetDataGridView("SettlementAutomaticTaskSet"));
             }
         }

         private void dataEditView_EditCancelled(object sender, EventArgs e) {
             this.ResetEditView();
             if(this.DataGridView.FilterItem != null)
                 this.DataGridView.ExecuteQueryDelayed();
             this.SwitchViewTo(DATA_GRID_VIEW);
         }

         private void Initialize() {
             this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
             this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
         }

         protected override IEnumerable<string> QueryPanelKeys {
             get {
                 return new[]{
                 "SettlementAutomaticTaskSet"
             };
             }
         }

         protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
             switch(uniqueId) {
                 case CommonActionKeys.ADD:
                     var settlementAutomaticTaskSet = this.DataEditView.CreateObjectToEdit<SettlementAutomaticTaskSet>();
                     settlementAutomaticTaskSet.Status = (int)DcsSettlementAutomaticTaskStatus.待执行;
                     settlementAutomaticTaskSet.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                     settlementAutomaticTaskSet.AutomaticSettleTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                     this.SwitchViewTo(DATA_EDIT_VIEW);
                     break;
                 case CommonActionKeys.EDIT:
                     this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                     this.SwitchViewTo(DATA_EDIT_VIEW);
                     break;
                 case CommonActionKeys.EXPORT:
                     if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                         var ids = this.DataGridView.SelectedEntities.Cast<SettlementAutomaticTaskSet>().Select(r => r.Id).ToArray();
                         this.excelServiceClient.ExportSettlementAutomaticTaskSetAsync(ids, null, null, null, null, null);
                         this.excelServiceClient.ExportSettlementAutomaticTaskSetCompleted -= excelServiceClient_ExportSettlementAutomaticTaskSetCompleted;
                         this.excelServiceClient.ExportSettlementAutomaticTaskSetCompleted += excelServiceClient_ExportSettlementAutomaticTaskSetCompleted;

                     } else {
                         var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                         if(filterItem == null)
                             return;
                         var brandId = filterItem.Filters.Single(r => r.MemberName == "BrandId").Value as int?;
                         var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                         var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                         DateTime? createTimeBegin = null;
                         DateTime? createTimeEnd = null;
                         if(createTime != null) {
                             createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                             createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                         }
                         this.excelServiceClient.ExportSettlementAutomaticTaskSetAsync(new int[] { },brandId, status, createTimeBegin, createTimeEnd);
                         this.excelServiceClient.ExportSettlementAutomaticTaskSetCompleted -= excelServiceClient_ExportSettlementAutomaticTaskSetCompleted;
                         this.excelServiceClient.ExportSettlementAutomaticTaskSetCompleted += excelServiceClient_ExportSettlementAutomaticTaskSetCompleted;
                     }
                     break;
                 default:
                     break;
             }
         }
         private void excelServiceClient_ExportSettlementAutomaticTaskSetCompleted(object sender, ExportSettlementAutomaticTaskSetCompletedEventArgs e) {
             ShellViewModel.Current.IsBusy = false;
             HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
         }
         protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
             if(this.CurrentViewKey == DATA_EDIT_VIEW)
                 return false;
             switch(uniqueId) {
                 case CommonActionKeys.ADD:
                     return true;
                 case CommonActionKeys.EDIT:
                     if(this.DataGridView.SelectedEntities == null)
                         return false;
                     var entities = this.DataGridView.SelectedEntities.Cast<SettlementAutomaticTaskSet>().ToArray();
                     if(entities.Length != 1)
                         return false;
                     return entities[0].Status == (int)DcsSettlementAutomaticTaskStatus.待执行;
                 case CommonActionKeys.EXPORT:
                     return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                 default:
                     return false;
             }
         }

         protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
             this.SwitchViewTo(DATA_GRID_VIEW);
             this.DataGridView.FilterItem = filterItem;
             this.DataGridView.ExecuteQueryDelayed();
         }

         public SettlementAutomaticTaskSetManagement() {
             this.Initializer.Register(this.Initialize);
             this.Title = PartsSalesUIStrings.Management_Title_SettlementAutomaticTaskSetManagement;
         }
    }
}
