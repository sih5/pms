﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "SparePartDirectory")]
    public class SparePartDirectoryManagement : DcsDataManagementViewBase {
        public SparePartDirectoryManagement() {
            Title = "查询电子目录图册";
        }
        string addUrlParameter;

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SparePartDirectory"
                };
            }
        }
        protected override void OnExecutingAction(Silverlight.View.ActionPanelBase actionPanel, string uniqueId) {

        }

        protected override void OnExecutingQuery(Silverlight.View.QueryPanelBase queryPanel, FilterItem filterItem) {
            addUrlParameter = "";
            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;
            var userCode = BaseApp.Current.CurrentUserData.UserCode;
            addUrlParameter += "user=" + Uri.EscapeUriString(userCode);

            var epcBrand = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "EPCBrandId");
            if(epcBrand != null && epcBrand.Value != null && !string.IsNullOrWhiteSpace(epcBrand.Value.ToString())) {
                addUrlParameter += "&brandSelect=" + Uri.EscapeUriString(epcBrand.Value.ToString());
            } else {
                UIHelper.ShowNotification("PMS品牌和EPC品牌查询条件不能为空");
                return;
            }
            var dealerCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
            addUrlParameter += "&DEALER_CODE=" + Uri.EscapeUriString(dealerCode);


            var pmsBrand = newCompositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "PMSBrandId");
            if(pmsBrand == null || pmsBrand.Value == null) {
                UIHelper.ShowNotification("PMS品牌和EPC品牌查询条件不能为空");
                return;
            }
            var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPMSEPCBRANDMAPPINGsQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.PMSBrandId == (int)pmsBrand.Value), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    var entity = loadOp.Entities.FirstOrDefault();
            //    if(entity == null) {
            //        //  UIHelper.ShowNotification("与EPC品牌映射关系不存在，请联系系统管理员。");
            //        return;
            //    }
            //    string paramBrand = entity.PMSBrandCode.ToString(CultureInfo.InvariantCulture);
            //    addUrlParameter += "&PMSBRAND=" + Uri.EscapeUriString(paramBrand);
            //    ShellViewModel.Current.IsBusy = false;
            //    var url = "http://epc.foton.com.cn/PMSServlet?" + addUrlParameter;
            //    HtmlPage.Window.Navigate(new Uri(url, UriKind.Absolute), "_blank");
            //}, null);
        }
    }
}
