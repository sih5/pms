﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "SpecialTreatyPriceChange", ActionPanelKeys = new[] {
        "SpecialTreatyPriceChange"
    })]
    public class SpecialTreatyPriceChangeManagement : DcsDataManagementViewBase {
        public SpecialTreatyPriceChangeManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.Management_Title_SpecialTreatyPriceChangeManagement;
        }
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditForApproveView;
        private DataEditViewBase dataEditForInitialApproveView;
        private DataEditViewBase dataEditForFirstApproveView;
        private DataEditViewBase dataEditViewImport;
        private DataEditViewBase dataDetailView; 
        private const string DATA_EEDITFORINITIALAPPORVE_VIEW = "_DataEditForInitialApproveView_";
        private const string DATA_EEDITFORFIRSTAPPORVE_VIEW = "_DataEditForFirstApproveView_";
        private const string DATA_EEDITFORAPPORVE_VIEW = "_DataEditForApproveView_";
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_"; 


        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SpecialTreatyPriceChange");
                    this.dataEditView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewDetail {
            get {
                if (this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("SpecialTreatyPriceChangeDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        //初审界面
        private DataEditViewBase DataEditForFirstApproveView
        {
            get
            {
                if (this.dataEditForFirstApproveView == null)
                {
                    this.dataEditForFirstApproveView = DI.GetDataEditView("SpecialTreatyPriceChangeForFirstApprove");
                    this.dataEditForFirstApproveView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditForFirstApproveView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditForFirstApproveView;
            }
        }
        //审核界面
        private DataEditViewBase DataEditForInitialApproveView
        {
            get
            {
                if (this.dataEditForInitialApproveView == null)
                {
                    this.dataEditForInitialApproveView = DI.GetDataEditView("SpecialTreatyPriceChangeForInitialApprove");
                    this.dataEditForInitialApproveView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditForInitialApproveView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditForInitialApproveView;
            }
        }
        //终审界面
        private DataEditViewBase DataEditForApproveView {
            get {
                if(this.dataEditForApproveView == null) {
                    this.dataEditForApproveView = DI.GetDataEditView("SpecialTreatyPriceChangeForApprove");
                    this.dataEditForApproveView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditForApproveView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditForApproveView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("SpecialTreatyPriceChangeForImport");
                    this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditViewImport.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewImport;
            }
        }

         private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) { 
            if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) { 
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity()); 
                this.SwitchViewTo(DATA_DETAIL_VIEW); 
            } 
        } 

        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditForApproveView = null;
            this.dataEditForInitialApproveView = null;
            this.dataEditViewImport = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                if (this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("SpecialTreatyPriceChange");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick; 
                }
                return this.dataGridView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EEDITFORAPPORVE_VIEW, () => this.DataEditForApproveView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
            this.RegisterView(DATA_EEDITFORINITIALAPPORVE_VIEW,()=>this.DataEditForInitialApproveView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.dataDetailView); 
            this.RegisterView(DATA_EEDITFORFIRSTAPPORVE_VIEW,()=>this.DataEditForFirstApproveView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SpecialTreatyPriceChange"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entitiesEdit = this.DataGridView.SelectedEntities.Cast<SpecialTreatyPriceChange>().ToArray();
                    if(entitiesEdit.Length != 1)
                        return false;
                    return entitiesEdit[0].Status == (int)DcsSpecialTreatyPriceChangeStatus.新建;
                case "InitialApprove"://审核
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<SpecialTreatyPriceChange>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsSpecialTreatyPriceChangeStatus.初审通过;
                case "FirstApprove"://初审
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities3 = this.DataGridView.SelectedEntities.Cast<SpecialTreatyPriceChange>().ToArray();
                    if(entities3.Length != 1)
                        return false;
                    return entities3[0].Status == (int)DcsSpecialTreatyPriceChangeStatus.已提交;
                case CommonActionKeys.APPROVE:
                //case CommonActionKeys.BATCHAPPROVAL:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<SpecialTreatyPriceChange>().ToArray();
                    if(entities1.Any(r => r.Status != (int)DcsSpecialTreatyPriceChangeStatus.审核通过))
                        return false;
                    return true;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entitiess = this.DataGridView.SelectedEntities.Cast<SpecialTreatyPriceChange>().ToArray();
                    if(entitiess.Length != 1)
                        return false;
                    return entitiess[0].Status == (int)DcsSpecialTreatyPriceChangeStatus.新建;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var specialTreatyPriceChange = this.DataEditView.CreateObjectToEdit<SpecialTreatyPriceChange>();
                    specialTreatyPriceChange.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    specialTreatyPriceChange.Status = (int)DcsSpecialTreatyPriceChangeStatus.新建;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SpecialTreatyPriceChange>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        try {
                            if(entity.Can提交配件配件特殊协议价格变更申请)
                                entity.提交配件配件特殊协议价格变更申请();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification("所选数据，提交成功");
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "FirstApprove"://初审
                    this.DataEditForFirstApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORFIRSTAPPORVE_VIEW);
                    break;
                case "InitialApprove"://审核
                    this.DataEditForInitialApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORINITIALAPPORVE_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataEditForApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EEDITFORAPPORVE_VIEW);
                    #region 取消快速操作
                    //case CommonActionKeys.BATCHAPPROVAL:
                    //DcsUtils.Confirm("确定要审核所选单据吗？", () => {
                    //    try {
                    //        ShellViewModel.Current.IsBusy = true;
                    //        var entitys = this.DataGridView.SelectedEntities.Cast<SpecialTreatyPriceChange>().ToList();
                    //        if(entitys == null || entitys.Count == 0)
                    //            return;
                    //        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    //        if(domainContext == null)
                    //        return;
                    //        domainContext.批量审核配件配件特殊协议价格变更申请(entitys, t => {
                    //            if(t.HasError) {
                    //                if(!t.IsErrorHandled)
                    //                    t.MarkErrorAsHandled();
                    //                DcsUtils.ShowDomainServiceOperationWindow(t);
                    //                domainContext.RejectChanges();
                    //                ShellViewModel.Current.IsBusy = false;
                    //                return;
                    //            }
                    //            this.DataGridView.ExecuteQueryDelayed();
                    //            ShellViewModel.Current.IsBusy = false;
                    //        }, null);
                    //    } catch(Exception ex) {
                    //        UIHelper.ShowAlertMessage(ex.Message);
                    //        ShellViewModel.Current.IsBusy = false;
                    //    }
                    //});
                    //break; 
                    #endregion
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SpecialTreatyPriceChange>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        try {
                            if(entity.Can作废配件配件特殊协议价格变更申请)
                                entity.作废配件配件特殊协议价格变更申请();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        //var ids = this.DataGridView.SelectedEntities.Cast<PartsSalesRtnSettlement>().Select(r => r.Id).ToArray();
                        var ids = this.DataGridView.SelectedEntities.Cast<SpecialTreatyPriceChange>().Select(r => r.Id).ToArray();
                        if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                            this.excelServiceClient.ExportSpecialTreatyPriceChangeAsync(ids, null, null, null, null, null);
                            this.excelServiceClient.ExportSpecialTreatyPriceChangeCompleted -= excelServiceClient_ExportSpecialTreatyPriceChangeCompleted;
                            this.excelServiceClient.ExportSpecialTreatyPriceChangeCompleted += excelServiceClient_ExportSpecialTreatyPriceChangeCompleted;
                        }
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                            this.excelServiceClient.MergeExportSpecialTreatyPriceChangeAsync(ids, null, null, null, null, null);
                            this.excelServiceClient.MergeExportSpecialTreatyPriceChangeCompleted -= excelServiceClient_MergeExportSpecialTreatyPriceChangeCompleted;
                            this.excelServiceClient.MergeExportSpecialTreatyPriceChangeCompleted += excelServiceClient_MergeExportSpecialTreatyPriceChangeCompleted;
                        }
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                       // var brandId = filterItem.Filters.Single(e => e.MemberName == "BrandId").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                            this.excelServiceClient.MergeExportSpecialTreatyPriceChangeAsync(new int[] { }, code, null, status, createTimeBegin, createTimeEnd);
                            this.excelServiceClient.MergeExportSpecialTreatyPriceChangeCompleted -= excelServiceClient_MergeExportSpecialTreatyPriceChangeCompleted;
                            this.excelServiceClient.MergeExportSpecialTreatyPriceChangeCompleted += excelServiceClient_MergeExportSpecialTreatyPriceChangeCompleted;
                        }
                        if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                            this.excelServiceClient.ExportSpecialTreatyPriceChangeAsync(new int[] { }, code, null, status, createTimeBegin, createTimeEnd);
                            this.excelServiceClient.ExportSpecialTreatyPriceChangeCompleted -= excelServiceClient_ExportSpecialTreatyPriceChangeCompleted;
                            this.excelServiceClient.ExportSpecialTreatyPriceChangeCompleted += excelServiceClient_ExportSpecialTreatyPriceChangeCompleted;
                        }
                    }
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
            }
        }

        private void excelServiceClient_MergeExportSpecialTreatyPriceChangeCompleted(object sender, MergeExportSpecialTreatyPriceChangeCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void excelServiceClient_ExportSpecialTreatyPriceChangeCompleted(object sender, ExportSpecialTreatyPriceChangeCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
