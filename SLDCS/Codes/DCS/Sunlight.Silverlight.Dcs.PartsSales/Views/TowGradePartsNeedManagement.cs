﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;


namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsSales", "TowGradePartsNeed", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT
     }
    )]
    public class TowGradePartsNeedManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private int subDealerId;
        private string name, code;

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("TowGradePartsNeed");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("TowGradePartsNeed"));
            }
        }

        public TowGradePartsNeedManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsSalesUIStrings.DataManagementView_Title_TowGradePartsNeed;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPersonSubDealersQuery().Where(e => e.PersonId == BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var personSubDealer in loadOp.Entities) {
                    subDealerId = personSubDealer.SubDealerId;
                }
            }, null);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "TowGradePartsNeed"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<SecondClassStationPlan>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsSecondClassStationPlanStatus.新建;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var secondClassStationPlan = this.DataEditView.CreateObjectToEdit<SecondClassStationPlan>();
                    var ddomainContext = new DcsDomainContext();
                    ddomainContext.Load(ddomainContext.GetSubDealersQuery().Where(e => e.Id == subDealerId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        foreach(var subDealer in loadOp.Entities) {
                            name = subDealer.Name;
                            code = subDealer.Code;
                        }
                        secondClassStationPlan.Code = GlobalVar.ASSIGNED_BY_SERVER;
                        secondClassStationPlan.SecondClassStationId = subDealerId;
                        secondClassStationPlan.SecondClassStationCode = code;
                        secondClassStationPlan.SecondClassStationName = name;
                        secondClassStationPlan.FirstClassStationCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                        secondClassStationPlan.FirstClassStationId = BaseApp.Current.CurrentUserData.EnterpriseId;
                        secondClassStationPlan.FirstClassStationName = BaseApp.Current.CurrentUserData.EnterpriseName;
                        secondClassStationPlan.Status = (int)DcsSecondClassStationPlanStatus.新建;
                    }, null);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<SecondClassStationPlan>().First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SecondClassStationPlan>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        try {
                            if(entity.Can作废二级站需求计划)
                                entity.作废二级站需求计划();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SecondClassStationPlan>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        try {
                            if(entity.Can提交二级站需求计划)
                                entity.提交二级站需求计划();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_SubmitSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            //if(composite == null)
            //    return;
            //composite.Filters.Add(new FilterItem {
            //    MemberName = "SecondClassStationId",
            //    MemberType = typeof(int),
            //    Value = subDealerId,
            //    Operator = FilterOperator.IsEqualTo
            //});
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            //this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
