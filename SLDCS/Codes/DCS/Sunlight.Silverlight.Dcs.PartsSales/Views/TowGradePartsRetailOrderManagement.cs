﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsSales", "PartsRetail", "TowGradePartsRetailOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_EXPORT
    })]

    public class TowGradePartsRetailOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataApproveView;
        private const string DATA_APPROVE_VIEW = "_dataApproveView_";
        public DataGridViewBase DataGridView {
            get {
                return dataGridView ?? (this.dataGridView = DI.GetDataGridView("TowGradePartsRetailOrder"));
            }
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("TowGradePartsRetailOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        public DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("TowGradePartsRetailOrderForApprove");
                    this.dataApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        public TowGradePartsRetailOrderManagement() {
            Title = PartsSalesUIStrings.DataManagementView_Title_TowGradePartsRetailOrder;
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "TowGradePartsRetailOrder"
                };
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataApproveView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var towGradePartsRetailOrder = this.DataEditView.CreateObjectToEdit<DealerPartsRetailOrder>();
                    towGradePartsRetailOrder.Status = (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                    towGradePartsRetailOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    towGradePartsRetailOrder.SubDealerlOrderType = (int)DcsDealerPartsRetailOrderSubDealerlOrderType.配件零售;
                    towGradePartsRetailOrder.DealerId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    towGradePartsRetailOrder.DealerCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    towGradePartsRetailOrder.DealerName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    var domainContext = new DcsDomainContext();
                    domainContext.Load(domainContext.GetSubDealerByPersonIdQuery(BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        var subDealer = loadOp.Entities.SingleOrDefault();
                        if(subDealer == null) {
                            UIHelper.ShowNotification(PartsSalesUIStrings.Validation_SubDealer_SubDealerIsNull);
                            return;
                        }
                        towGradePartsRetailOrder.SubDealerId = subDealer.Id;
                        towGradePartsRetailOrder.SubDealerCode = subDealer.Code;
                        towGradePartsRetailOrder.SubDealerName = subDealer.Name;
                        this.SwitchViewTo(DATA_EDIT_VIEW);
                    }, null);

                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废服务站配件零售订单)
                                entity.作废服务站配件零售订单();
                            var domainContexts = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContexts == null)
                                return;
                            domainContexts.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContexts.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    return entities1[0].Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建;

                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities2 = this.DataGridView.SelectedEntities.Cast<DealerPartsRetailOrder>().ToArray();
                    if(entities2.Length != 1)
                        return false;
                    return entities2[0].Status == (int)DcsWorkflowOfSimpleApprovalStatus.新建;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
