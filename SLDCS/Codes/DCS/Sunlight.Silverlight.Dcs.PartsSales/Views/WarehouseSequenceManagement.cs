﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsSales.Views {
    [PageMeta("PartsBaseInfo", "PartsSalesPrice", "WarehouseSequence", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT,"WarehouseSequence"
    })]
    public class WarehouseSequenceManagement : DcsDataManagementViewBase {
        private DataGridViewBase warehouseSequenceDataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase WarehouseSequenceDataGridView {
            get {
                return this.warehouseSequenceDataGridView ?? (this.warehouseSequenceDataGridView = DI.GetDataGridView("WarehouseSequence"));
            }
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("WarehouseSequence");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("WarehouseSequenceForImport");
                    this.dataEditViewImport.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.WarehouseSequenceDataGridView.FilterItem != null)
                this.WarehouseSequenceDataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public WarehouseSequenceManagement() {
            this.Title = "出库仓库优先顺序管理";
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.WarehouseSequenceDataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.WarehouseSequenceDataGridView.FilterItem = filterItem;
            this.WarehouseSequenceDataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var warehouseSequence = this.DataEditView.CreateObjectToEdit<WarehouseSequence>();
                    warehouseSequence.Status = (int)DcsMasterDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.WarehouseSequenceDataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.WarehouseSequenceDataGridView.SelectedEntities.Cast<WarehouseSequence>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废出库仓库优先顺序)
                                entity.作废出库仓库优先顺序();
                            var domainContext = this.WarehouseSequenceDataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.WarehouseSequenceDataGridView.SelectedEntities != null && this.WarehouseSequenceDataGridView.SelectedEntities.Any()) {
                        var ids = this.WarehouseSequenceDataGridView.SelectedEntities.Cast<WarehouseSequence>().Select(e => e.Id).ToArray();
                        this.ExportWarehouseSequence(ids, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.WarehouseSequenceDataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var customerCompanyCode = filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyCode").Value as string;
                        var customerCompanyName = filterItem.Filters.Single(r => r.MemberName == "CustomerCompanyName").Value as string;
                        var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var isAutoApprove = filterItem.Filters.Single(r => r.MemberName == "IsAutoApprove").Value as bool?;
                        var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        var dateTimeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem));
                        foreach(var dateTimeFilterItem in dateTimeFilterItems) {
                            var dateTime = dateTimeFilterItem as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportWarehouseSequence(new int[] { }, customerCompanyCode, customerCompanyName, partsSalesCategoryId, status, isAutoApprove, createTimeBegin, createTimeEnd);
                    }
                    break;
                case "ImportEdit":
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case "ImportEdit":
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.WarehouseSequenceDataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.WarehouseSequenceDataGridView.SelectedEntities.Cast<WarehouseSequence>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.WarehouseSequenceDataGridView.Entities != null && this.WarehouseSequenceDataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "WarehouseSequence"
                };
            }
        }

        private void ExportWarehouseSequence(int[] ids, string customerCompanyCode, string customerCompanyName, int? partsSalesCategoryId, int? status, bool? isAutoApprove, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportWarehouseSequenceAsync(ids, customerCompanyCode, customerCompanyName, partsSalesCategoryId, status, isAutoApprove, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportWarehouseSequenceCompleted -= excelServiceClient_ExportWarehouseSequenceCompleted;
            this.excelServiceClient.ExportWarehouseSequenceCompleted += excelServiceClient_ExportWarehouseSequenceCompleted;
        }

        void excelServiceClient_ExportWarehouseSequenceCompleted(object sender, ExportWarehouseSequenceCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
