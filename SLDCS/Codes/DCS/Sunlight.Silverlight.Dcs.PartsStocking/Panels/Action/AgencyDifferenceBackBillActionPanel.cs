﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class AgencyDifferenceBackBillActionPanel : DcsActionPanelBase {
        public AgencyDifferenceBackBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = "中心库收货差异处理",
                ActionItems = new[] {
                       new ActionItem {
                        UniqueId = CommonActionKeys.ADD,
                        Title = PartsStockingUIStrings.Action_Title_Add,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = CommonActionKeys.EDIT,
                        Title = PartsStockingUIStrings.Action_Title_Edit,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = CommonActionKeys.ABANDON,
                        Title = PartsStockingUIStrings.Action_Title_Abandon,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = CommonActionKeys.SUBMIT,
                        Title = PartsStockingUIStrings.Action_Title_Submit,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Submit.png"),
                        CanExecute = false
                    }, 
                    new ActionItem {
                        UniqueId = CommonActionKeys.TERMINATE,
                        Title =PartsStockingUIStrings.Action_Title_Stop,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "Determine",
                        Title = "判定",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/RebateApprove.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = CommonActionKeys.CONFIRM,
                        Title =PartsStockingUIStrings.Action_Title_Confirm,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "Handle",
                        Title = "处理",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ServiceEdit.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = CommonActionKeys.APPROVE,
                        Title = PartsStockingUIStrings.Action_Title_InitialApprove,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = CommonActionKeys.AUDIT,
                        Title = PartsStockingUIStrings.Action_Title_FinalApprove,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PickConfirm.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "Export",
                        Title = PartsStockingUIStrings.Button_Text_Common_Export,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    }
                }

            };
        }
    }
}