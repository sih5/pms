﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class AgencyPartsShippingOrderActionPanel : DcsActionPanelBase {
        public AgencyPartsShippingOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsShippingOrder,
                ActionItems = new[] {
                    //   new ActionItem {
                    //    UniqueId = "SendingConfirm",
                    //    Title = PartsStockingUIStrings.Action_Title_SendingConfirm,
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                    //    CanExecute = false
                    //},
                    //new ActionItem {
                    //    UniqueId = "SendingConfirmBatch",
                    //    Title = PartsStockingUIStrings.Action_Title_SendingConfirmBatch,
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                    //    CanExecute = false
                    //},
                    //new ActionItem {
                    //    UniqueId = "SendConfirm",
                    //    Title = PartsStockingUIStrings.Action_Title_SendConfirm,
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                    //    CanExecute = false
                    //},
                    //new ActionItem {
                    //    UniqueId = "SendConfirmBatch",
                    //    Title = PartsStockingUIStrings.Action_Title_SendConfirmBatch,
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                    //    CanExecute = false
                    //}, 
                    new ActionItem {
                        UniqueId = "Sending",
                        Title =PartsStockingUIStrings.Action_Title_Sending,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "Send",
                        Title = PartsStockingUIStrings.Action_Title_Send,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "ReceivingConfirm",
                        Title =PartsStockingUIStrings.Action_Title_ReceivingConfirm,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "ExtendedBackConfirm",
                        Title = PartsStockingUIStrings.Action_Title_ExtendedBackConfirm,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "GPSLocationRegister",
                        Title = PartsStockingUIStrings.Action_Title_GPSLocationRegister,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Register.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "Print",
                        Title = PartsStockingUIStrings.Action_Title_Print,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "Export",
                        Title = PartsStockingUIStrings.Button_Text_Common_Export,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "MergeExport",
                        Title = PartsStockingUIStrings.Action_Title_MergeExport,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }
                }

            };
        }
    }
}