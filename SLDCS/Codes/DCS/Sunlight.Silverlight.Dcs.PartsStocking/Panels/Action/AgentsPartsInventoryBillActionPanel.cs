﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class AgentsPartsInventoryBillActionPanel : DcsActionPanelBase {
        public AgentsPartsInventoryBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "AgentsPartsInventoryBill",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsInventoryBill,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_InitialApprove,
                        UniqueId = "Audit",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/InitialApprove.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_FinalApprove,
                        UniqueId = "FinalApprove",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false
                    },
                     new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_AdvancedApprove,
                        UniqueId = "AdvancedAudit",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/FinalApprove.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_ViewDetail,
                        UniqueId = "ViewDetail",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        CanExecute = false
                    }, new ActionItem{
                        UniqueId = "InventoryCoverage",
                        CanExecute = false,
                        Title = PartsStockingUIStrings.Action_Title_InventoryCovers,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/InventoryCoverage.png")
                    }, new ActionItem{
                        UniqueId = "ResultsInput",
                        CanExecute = false,
                        Title = PartsStockingUIStrings.Action_Title_ResultsInput,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/ResultsInput.png")
                    },new ActionItem {
                        UniqueId = "DetailExport",
                        CanExecute = false,
                        Title = PartsStockingUIStrings.Action_Title_MergeExport,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportDetail.png")
                    },new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Reject,
                        UniqueId = "Reject",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/Reject.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
