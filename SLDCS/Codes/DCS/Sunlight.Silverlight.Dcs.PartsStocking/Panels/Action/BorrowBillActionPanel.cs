﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.Common.Panels.Action {
    public class BorrowBillActionPanel : DcsActionPanelBase {
        public BorrowBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "BorrowBill",
                Title = PartsStockingUIStrings.ActionPanel_Title_BorrowBill,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_DeliverParts2,
                        UniqueId = "DeliverParts",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/DeliverParts.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_ReturnSubmit,
                        UniqueId = "ReturnSubmit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Submit.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_ReturnApprove,
                        UniqueId = "ReturnApprove",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_MergeExport,
                        UniqueId = "MergeExport",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
