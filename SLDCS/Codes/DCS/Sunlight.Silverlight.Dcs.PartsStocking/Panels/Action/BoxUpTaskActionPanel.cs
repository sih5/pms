﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class BoxUpTaskActionPanel : DcsActionPanelBase {
        public BoxUpTaskActionPanel()
        {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "BoxUpTask",
                Title = PartsStockingUIStrings.ActionPanel_Title_BusinessOperation,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_CreateBoxUpTask,
                        UniqueId = "Create",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_DoBoxUp,
                        UniqueId = "BoxUp",
                        ImageUri = Utils.MakeServerUri("Client/DCS/Images/Menu/PartsStocking/PartsPacking.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Stop,
                        UniqueId = "Stop",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        CanExecute = false
                    }
                    ,new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_A4Print,
                        UniqueId = "A4Print",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
