﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class DealerPartsInventoryBillActionPanel : DcsActionPanelBase {
        public DealerPartsInventoryBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "DealerPartsInventoryBill",
                Title = PartsStockingUIStrings.ActionPanel_Title_DealerPartsInventoryBill,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = "AdvancedApprove",
                        Title = PartsStockingUIStrings.Action_Title_AdvancedApprove,
                        ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"), 
                        CanExecute = false
                    }, new ActionItem { 
                        UniqueId = CommonActionKeys.SENIORAPPROVE, 
                        Title = PartsStockingUIStrings.Action_Title_SeniorApprove,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/FinalApprove.png"),
                        CanExecute = false 
                    },new ActionItem {
                        UniqueId = "DetailExport",
                        Title = PartsStockingUIStrings.Action_Title_MergeExport,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    },new ActionItem { 
                        Title = PartsStockingUIStrings.Action_Title_UploadFile, 
                        UniqueId = CommonActionKeys.UPLOAD, 
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Upload.png"), 
                        CanExecute = false 
                    } 
                }
            };
        }
    }
}