﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class DealerPartsTransferOrder : DcsActionPanelBase {
        public DealerPartsTransferOrder() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsStockingUIStrings.ActionPanel_Title_Common,
                ActionItems = new[]{
                  new ActionItem{
                        Title = PartsStockingUIStrings.Action_Title_InitialApprove,
                        UniqueId ="Approve",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute=false
                    }, new ActionItem{
                        Title = PartsStockingUIStrings.Action_Title_Reject,
                        UniqueId ="Reject",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Reject.png"),
                        CanExecute=false
                    }, new ActionItem{
                        Title = PartsStockingUIStrings.Button_Text_Common_Export,
                        UniqueId ="Export",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute=false
                    }, new ActionItem{
                        Title = PartsStockingUIStrings.Action_Title_MergeExport,
                        UniqueId ="MergeExport",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}