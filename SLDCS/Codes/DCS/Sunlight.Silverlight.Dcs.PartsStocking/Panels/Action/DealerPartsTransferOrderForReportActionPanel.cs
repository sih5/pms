﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class DealerPartsTransferOrderForReportActionPanel : DcsActionPanelBase {
        public DealerPartsTransferOrderForReportActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsStockingUIStrings.ActionPanel_Title_Common,
                ActionItems = new[]{
                    new ActionItem{
                        Title = PartsStockingUIStrings.Action_Title_Add,
                        UniqueId ="Add",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute=false
                    }, new ActionItem{
                        Title = PartsStockingUIStrings.Action_Title_Edit,
                        UniqueId ="Edit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute=false
                    }, new ActionItem{
                        Title = PartsStockingUIStrings.Action_Title_Abandon,
                        UniqueId ="Abandon",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute=false
                    }, new ActionItem{
                        Title = PartsStockingUIStrings.Action_Title_Submit,
                        UniqueId ="Submit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Submit.png"),
                        CanExecute=false
                    }, new ActionItem{
                        Title = PartsStockingUIStrings.Button_Text_Common_Export,
                        UniqueId ="Export",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute=false
                    }, new ActionItem{
                        Title = PartsStockingUIStrings.Action_Title_MergeExport,
                        UniqueId ="MergeExport",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}
