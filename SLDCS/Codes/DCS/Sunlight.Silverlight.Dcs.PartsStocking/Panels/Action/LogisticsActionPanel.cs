﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class LogisticsActionPanel : DcsActionPanelBase {
        public LogisticsActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "LogisticsTracking",
                Title = PartsStockingUIStrings.ActionPanel_Title_Common,
                ActionItems = new[]{
                    new ActionItem{
                        Title =PartsStockingUIStrings.Action_Title_Input,
                        UniqueId ="Input",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute=false
                    }, new ActionItem{
                        Title =PartsStockingUIStrings.Action_Title_InputByDeputy,
                        UniqueId ="InputByDeputy",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/EditProductCategory.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}
