﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class OverstockPartsAdjustBillActionPanel : DcsActionPanelBase {
        public OverstockPartsAdjustBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "OverstockPartsAdjustBill",
                Title = PartsStockingUIStrings.ActionPanel_Title_OverstockPartsAdjustBill,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = "Submit",
                        Title = PartsStockingUIStrings.Action_Title_Submit,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Submit.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "Confirm",
                        Title = PartsStockingUIStrings.Action_Title_Confirm,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
