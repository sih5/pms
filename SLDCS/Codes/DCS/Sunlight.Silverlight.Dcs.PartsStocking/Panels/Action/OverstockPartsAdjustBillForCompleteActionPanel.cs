﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class OverstockPartsAdjustBillForCompleteActionPanel : DcsActionPanelBase {
        public OverstockPartsAdjustBillForCompleteActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "OverstockPartsAdjustBillForComplete",
                Title = PartsStockingUIStrings.ActionPanel_Title_OverstockPartsAdjustBill,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = "Complete",
                        Title = PartsStockingUIStrings.Action_Title_Complete,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Complete.png"),
                        CanExecute = false
                    }, new ActionItem {
                        UniqueId = "AdjustKanban",
                        Title = PartsStockingUIStrings.Action_Title_AdjustKanban,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AdjustKanban.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
