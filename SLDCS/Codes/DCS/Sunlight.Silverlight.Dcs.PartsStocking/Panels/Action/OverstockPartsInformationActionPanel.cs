﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class OverstockPartsInformationActionPanel : DcsActionPanelBase {
        public OverstockPartsInformationActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "OverstockPartsInformation",
                Title = PartsStockingUIStrings.ActionPanel_Title_OverstockPartsInformation,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = "Remove",
                        Title = PartsStockingUIStrings.Action_Title_Remove,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Remove.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
