﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class OverstockPartsPlatFormBillActionPanel : DcsActionPanelBase {
        public OverstockPartsPlatFormBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "OverstockPartsPlatFormBill",
                Title = PartsStockingUIStrings.Management_Title_OverstockPartsPlatFormBill,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = "Buy",
                        Title = PartsStockingUIStrings.Action_Title_Buy,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/GenerateOuterPurchase.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Delete,
                        UniqueId = "Delete",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Delete.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "Upload",
                        Title = PartsStockingUIStrings.Action_Title_UploadFile,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Upload.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
