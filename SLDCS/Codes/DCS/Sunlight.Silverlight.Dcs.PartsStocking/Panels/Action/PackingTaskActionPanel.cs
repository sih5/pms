﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action
{
    public class PackingTaskActionPanel : DcsActionPanelBase
    {
        public PackingTaskActionPanel()
        {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PackingTask",
                Title = PartsStockingUIStrings.ActionPanel_Title_PackingTask,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PackingTask,
                        UniqueId = "Packing",
                        ImageUri = Utils.MakeServerUri("Client/DCS/Images/Menu/PartsStocking/PartsPacking.png")
                    }
                }
            };
        }
    }
}
