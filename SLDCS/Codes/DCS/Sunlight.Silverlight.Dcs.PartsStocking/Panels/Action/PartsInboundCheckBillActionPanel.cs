﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsInboundCheckBillActionPanel : DcsActionPanelBase {
        public PartsInboundCheckBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsInboundCheckBill",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsInboundCheckBill,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_CheckInbound,
                        UniqueId = "CheckInbound",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/CheckInbound.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
