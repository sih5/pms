﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action
{
    public class PartsInboundCheckBillQueryActionPanel : DcsActionPanelBase
    {
        public PartsInboundCheckBillQueryActionPanel()
        {
            this.ActionItemGroup = new ActionItemGroup
            {
                UniqueId = "PartsInboundCheckBillQuery",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsInboundCheckBill,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.ActionPanel_Title_PartsInboundPackingDetail,
                        UniqueId = "PackingDetail",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_NotSettle,
                        UniqueId = "NotSettle",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AntiSettlement.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Settle,
                        UniqueId = "Settle",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AntiSettlement.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =PartsStockingUIStrings.Action_Title_ChPrint,
                        UniqueId = "ChPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =PartsStockingUIStrings.Action_Title_EnPrint,
                        UniqueId = "EnPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =PartsStockingUIStrings.Action_Title_PcPrint,
                        UniqueId = "PcPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =PartsStockingUIStrings.Action_Title_PcEnPrint,
                        UniqueId = "PcEnPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
