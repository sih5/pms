﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsInboundCheckQueryPrintLabelActionPanel : DcsActionPanelBase {
        public PartsInboundCheckQueryPrintLabelActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsStockingUIStrings.ActionPanel_Title_Common,
                ActionItems = new[] {
                     new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Print,
                        UniqueId =CommonActionKeys.PRINT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_LablePrintOld,
                        UniqueId = "UsedPrintLabel",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_LablePrint,
                        UniqueId = "PrintLabel",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_WMSLablePrintOld,
                        UniqueId = "UsedWMSPrintLabel",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_WMSLablePrint,
                        UniqueId = "WMSPrintLabel",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PrintByArea,
                        UniqueId = "Partition",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PrintWithStandard,
                        UniqueId = "StandardPrintLabel",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
