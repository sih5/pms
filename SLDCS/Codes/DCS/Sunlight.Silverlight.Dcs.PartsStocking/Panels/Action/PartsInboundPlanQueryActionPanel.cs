﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsInboundPlanQueryActionPanel : DcsActionPanelBase {
        public PartsInboundPlanQueryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsStockingUIStrings.ActionPanel_Title_Common,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Stop,
                        UniqueId = CommonActionKeys.TERMINATE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsStockingUIStrings.Button_Text_Common_Export,
                        UniqueId = CommonActionKeys.EXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    }, new ActionItem {
                        UniqueId = CommonActionKeys.MERGEEXPORT,
                        Title = PartsStockingUIStrings.Action_Title_MergeExport,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Print,
                        UniqueId =CommonActionKeys.PRINT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_LablePrint,
                        UniqueId = "PrintLabel",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_WMSLablePrint,
                        UniqueId = "WMSPrintLabel",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_WMSLablePrintForOther,
                        UniqueId = "OMWMSPrintLabel",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PrintByArea,
                        UniqueId = "Partition",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PrintWithStandard,
                        UniqueId = "StandardPrintLabel",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
