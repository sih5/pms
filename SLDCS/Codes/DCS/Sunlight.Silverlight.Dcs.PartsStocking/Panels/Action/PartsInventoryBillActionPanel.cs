﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsInventoryBillActionPanel : DcsActionPanelBase {

        public PartsInventoryBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsInventoryBillReported",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsInventoryBill,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_ViewDetail,
                        UniqueId = "ViewDetail",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        CanExecute = false
                    },new ActionItem{
                        UniqueId = "ResultsInput",
                        CanExecute = false,
                        Title = PartsStockingUIStrings.Action_Title_ResultsEntry,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/ResultsInput.png")
                    }, new ActionItem{
                        UniqueId = "InventoryCoverage",
                        CanExecute = false,
                        Title = PartsStockingUIStrings.Action_Title_InventoryCovers,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/InventoryCoverage.png")
                    }, new ActionItem{
                        UniqueId = "DetailExport",
                        CanExecute = false,
                        Title = PartsStockingUIStrings.Action_Title_DetailExport,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/DetailExport.png")
                    }
                }
            };
        }
    }
}
