﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsInventoryBillForMngActionPanel : DcsActionPanelBase {
        public PartsInventoryBillForMngActionPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsInventoryBill",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsInventoryBillForMng,
                ActionItems = new[]{
                    new ActionItem{
                        UniqueId = "ResultsInput",
                        CanExecute = false,
                        Title = PartsStockingUIStrings.Action_Title_ResultsEntry,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/ResultsInput.png")
                    }, new ActionItem{
                        UniqueId = "InventoryCoverage",
                        CanExecute = false,
                        Title = PartsStockingUIStrings.Action_Title_InventoryCovers,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/InventoryCoverage.png")
                    }, new ActionItem{
                        UniqueId = "FirstApprove",
                        CanExecute = false,
                        Title = PartsStockingUIStrings.DataEditView_Title_InitialApprove,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png")
                    },  new ActionItem{
                        UniqueId = "Audit",
                        CanExecute = false,
                        Title = PartsStockingUIStrings.Action_Title_InitialApprove,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png")
                    }, new ActionItem{
                        UniqueId = "Approve",
                        CanExecute = false,
                        Title = PartsStockingUIStrings.Action_Title_FinalApprove,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/FinalApprove.png")
                    }, new ActionItem{
                        UniqueId = "BFirstApprove",
                        CanExecute = false,
                        Title = "批量初审",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png")
                    },  new ActionItem{
                        UniqueId = "BAudit",
                        CanExecute = false,
                        Title = "批量审核",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/InitialApprove.png")
                    }, new ActionItem{
                        UniqueId = "BApprove",
                        CanExecute = false,
                        Title = "批量审批",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/FinalApprove.png")
                    }
                }
            };
        }
    }
}
