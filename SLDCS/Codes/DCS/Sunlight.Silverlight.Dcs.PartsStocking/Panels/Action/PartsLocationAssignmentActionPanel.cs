﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsLocationAssignmentActionPanel : DcsActionPanelBase {
        public PartsLocationAssignmentActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsLocationAssignment",
                Title = PartsStockingUIStrings.Action_Title_ImportAreaAllot,
                ActionItems = new[] {
                      new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Import,
                        UniqueId = "ImportArea",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
