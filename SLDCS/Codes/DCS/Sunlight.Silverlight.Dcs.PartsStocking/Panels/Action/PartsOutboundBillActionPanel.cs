﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsOutboundBillActionPanel : DcsActionPanelBase {
        public PartsOutboundBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsOutboundBill",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsOutboundBill,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_DeliverParts,
                        UniqueId = "DeliverParts",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/DeliverParts.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title = PartsStockingUIStrings.Action_Title_PrintByArea,
                        UniqueId = "PartitionPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title = PartsStockingUIStrings.Action_Title_NotPartitionPrint,
                        UniqueId = "NotPartitionPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
