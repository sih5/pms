﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsOutboundBillQueryActionPanel : DcsActionPanelBase {
        public PartsOutboundBillQueryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsOutboundBillQuery",
                Title = PartsStockingUIStrings.Action_Title_PrintForGC,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Print,
                        UniqueId = "PrintForGC",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PrintResume,
                        UniqueId = "PrintResume",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Resume.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =PartsStockingUIStrings.Action_Title_AgencyPrint,
                        UniqueId = "AgencyPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
