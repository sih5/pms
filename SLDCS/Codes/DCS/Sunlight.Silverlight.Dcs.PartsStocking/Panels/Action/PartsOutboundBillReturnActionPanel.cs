﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsOutboundBillReturnActionPanel : DcsActionPanelBase {
        public PartsOutboundBillReturnActionPanel() {
            this.ActionItemGroup = new ActionItemGroup() {
                UniqueId = "PartsOutboundBillReturn",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsOutboundBillReturn,
                ActionItems = new[]{
                new ActionItem{
                    Title= PartsStockingUIStrings.ActionPanel_Title_ReturnBound,
                    UniqueId = "ReturnBound",
                    ImageUri = Utils.MakeServerUri("Cilent/Dcs/Images/Operations/PartsStocking/ReturnBound.png"),
                    CanExecute =false
                }
              }
            };
        }
    }
}
