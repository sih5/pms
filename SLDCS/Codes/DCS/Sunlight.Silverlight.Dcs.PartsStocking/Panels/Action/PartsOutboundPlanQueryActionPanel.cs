﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsOutboundPlanQueryActionPanel : DcsActionPanelBase {
        public PartsOutboundPlanQueryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsOutboundPlanQuery",
               // Title = PartsStockingUIStrings.Action_Title_PartitionPrint,
                Title = PartsStockingUIStrings.ActionPanel_Title_Pick,
                ActionItems = new[] {
                    //new ActionItem {
                    //    UniqueId = "PartitionPrint",
                    //    Title = PartsStockingUIStrings.Action_Title_PartitionPrint,
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                    //    CanExecute = false
                    //},new ActionItem {
                    //    UniqueId = "NotWarehousePrint",
                    //    Title = PartsStockingUIStrings.Action_Title_NotWarehousePrint,
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                    //    CanExecute = false
                    //},new ActionItem {
                    //    UniqueId = "NotPartitionPrint",
                    //    Title = PartsStockingUIStrings.Action_Title_NotWarehousePartitionPrint,
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                    //    CanExecute = false
                    //},
                       new ActionItem {
                        UniqueId = "Packing",
                        Title = PartsStockingUIStrings.Action_Title_PartsOutboundPlanQuery_Pick,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PickConfirm.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
