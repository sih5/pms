﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsPackingActionPanel : DcsActionPanelBase {
        public PartsPackingActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsPacking",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsPacking,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PackingDetail,
                        UniqueId = "PackingDetail",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsPackingDetail.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
