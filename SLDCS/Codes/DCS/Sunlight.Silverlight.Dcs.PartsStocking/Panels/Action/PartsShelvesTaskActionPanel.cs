﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsShelvesTaskActionPanel : DcsActionPanelBase {
        public PartsShelvesTaskActionPanel()
        {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsShelvesTask",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsShelvesTask,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.ActionPanel_Title_Shelves,
                        UniqueId = "Shelves",
                        ImageUri = Utils.MakeServerUri("Client/DCS/Images/Menu/PartsStocking/PartsShelves.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
