﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
 

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsShiftOrderActionPanel : DcsActionPanelBase {
        public PartsShiftOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsStockingUIStrings.ActionPanel_Title_Common,
                ActionItems = new[]{
                    new ActionItem {                        Title = PartsStockingUIStrings.Action_Title_MergeExport,                        UniqueId = CommonActionKeys.MERGEEXPORT,                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),                        CanExecute = false                    },                    new ActionItem {                        Title = PartsStockingUIStrings.Action_Title_Print,                        UniqueId = CommonActionKeys.PRINT,                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),                        CanExecute = false                    },                    new ActionItem {  
                            Title = PartsStockingUIStrings.Action_Title_InitialApprove,  
                            UniqueId = CommonActionKeys.AUDIT,  
                            ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/InitialApprove.png"),  
                            CanExecute = false  
                        },
                    new ActionItem {  
                        Title = PartsStockingUIStrings.Action_Title_FinalApprove,  
                        UniqueId = CommonActionKeys.APPROVE,  
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/FinalApprove.png"),  
                        CanExecute = false  
                    },
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_UploadFile,
                        UniqueId = CommonActionKeys.UPLOAD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Upload.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = "下架",
                        UniqueId = "Down",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ManualProcessing.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = "上架",
                        UniqueId = "Up",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Marketing.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = "下架打印",
                        UniqueId = "DownPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = "上架打印",
                        UniqueId = "UpPrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = "终止",
                        UniqueId = CommonActionKeys.TERMINATE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}