﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsShippingOrderActionPanel : DcsActionPanelBase {
        public PartsShippingOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsShippingOrder",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsShippingOrder,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_ShippingConfirm,
                        UniqueId = "ShippingConfirm",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_ReceiptConfirm,
                        UniqueId = "ReceiptConfirm",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_TransportLossesDispose,
                        UniqueId = "TransportLossesDispose", 
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/TransportLossesDispose.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_GPSLocationQuery,
                        UniqueId = "GPSLocationQuery",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/GPSLocationQuery.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PrintNoPrice,
                        UniqueId = "PrintNoPrice",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },new ActionItem{
                        Title=PartsStockingUIStrings.Action_Title_A4Print,
                        UniqueId="A4Print",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
