﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsShippingOrderForConfirmActionPanel : DcsActionPanelBase {
        public PartsShippingOrderForConfirmActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsShippingOrderForConfirm",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsShippingOrderForConfirm,
                ActionItems = new[] {
                    new ActionItem {
                        UniqueId = "ReceivingConfirm",
                        Title = PartsStockingUIStrings.Action_Title_ReceivingConfirm,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =  PartsStockingUIStrings.Action_Title_Print,
                        UniqueId = "Print",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_MergeExport,
                        UniqueId = CommonActionKeys.MERGEEXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_GPSLocationQuery,
                        UniqueId = "GPSLocationQuery",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/GPSLocationQuery.png"),
                        CanExecute = false
                    }
                }

            };
        }
    }
}
