﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsShippingOrderForLogisticActionPanel : DcsActionPanelBase {
        public PartsShippingOrderForLogisticActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsShippingOrderForLogistic",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsShippingOrder,
                ActionItems = new[] {
                    //new ActionItem {
                    //    UniqueId = "DeliveryConfirm",
                    //    Title = PartsStockingUIStrings.Action_Title_DeliveryConfirm,
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                    //    CanExecute = false
                    //},
                    new ActionItem {
                        UniqueId = "SendingConfirm",
                        Title = PartsStockingUIStrings.Action_Title_SendingConfirm,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "SendingConfirmBatch",
                        Title = PartsStockingUIStrings.Action_Title_SendingConfirmBatch,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "SendConfirm",
                        Title = PartsStockingUIStrings.Action_Title_SendConfirm,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        UniqueId = "SendConfirmBatch",
                        Title = PartsStockingUIStrings.Action_Title_SendConfirmBatch,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }, new ActionItem {
                        UniqueId = "ExtendedBackConfirm",
                        Title = PartsStockingUIStrings.Action_Title_ExtendedBackConfirm,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Confirm.png"),
                        CanExecute = false
                    },new ActionItem{
                        UniqueId="GPSLocationRegister",
                        Title= PartsStockingUIStrings.Action_Title_GPSLocationRegister,
                        ImageUri= Utils.MakeServerUri("Client/Dcs/Images/Operations/Register.png"),
                        CanExecute=false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_GPSLocationQuery,
                        UniqueId = "GPSLocationQuery",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/GPSLocationQuery.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PrintNoPrice,
                        UniqueId = "PrintNoPrice",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }

                }

            };
        }
    }
}
