﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsStockDetailQueryActionPanel : DcsActionPanelBase {
        public PartsStockDetailQueryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsStockDetailQuery",
                Title = PartsStockingUIStrings.ActionPanel_Title_Common,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Print,
                        UniqueId = "Print",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Delete,
                        UniqueId = "Delete",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Delete.png"),
                        CanExecute = false
                    },new ActionItem {
                        UniqueId = "HistoricalStorage",
                        Title =PartsStockingUIStrings.Action_Title_HistoricalStorage,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Query.png"),
                        CanExecute = false
                    },new ActionItem {
                        UniqueId = "ExactExport",
                        Title = PartsStockingUIStrings.Action_Title_ExactExport,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
