﻿using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsStockQueryActionPanel : DcsActionPanelBase {
        public PartsStockQueryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = PartsStockingUIStrings.ActionPanel_Title_InOutHistoryQuery,
                ActionItems = new[] {
                    new ActionItem {
                       Title = PartsStockingUIStrings.Button_Text_Common_Export,
                        UniqueId = CommonActionKeys.EXPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    },new ActionItem { 
                        UniqueId = "ExactExport", 
                        Title = PartsStockingUIStrings.Action_Title_ExactExport, 
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/MergeExport.png"), 
                        CanExecute = false 
                    } , new ActionItem {
                        UniqueId = "HistoricalStorage",
                        Title =PartsStockingUIStrings.ActionPanel_Title_InOutHistoryQuery,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Query.png"),
                        CanExecute = false
                    }, new ActionItem {
                        UniqueId = "LocketUnit",
                        Title =PartsStockingUIStrings.Action_Title_LocketUnit,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Query.png"),
                        CanExecute = false
                    }, new ActionItem {
                        UniqueId = "PartsStockSync",
                        Title =PartsStockingUIStrings.Action_Title_PartsStockSync,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ManualProcessing.png"),
                        CanExecute = false
                    }, new ActionItem {
                        UniqueId = "PcPrint",
                        Title =PartsStockingUIStrings.Action_Title_PcPrint,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }, new ActionItem {
                        UniqueId = "ChPrint",
                        Title =PartsStockingUIStrings.Action_Title_ChPrint,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
