﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class PartsTransferOrderActionPanel : DcsActionPanelBase {
        public PartsTransferOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsTransferOrder",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsTransferOrder,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PartsTransferOrderKanBan,
                        UniqueId = "TransferKanBan",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PartsStocking/TransferKanBan.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
