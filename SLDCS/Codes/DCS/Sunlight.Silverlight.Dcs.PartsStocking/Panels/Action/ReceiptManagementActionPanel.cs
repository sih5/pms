﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class ReceiptManagementActionPanel : DcsActionPanelBase {
        public ReceiptManagementActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "ReceiptManagement",
                Title = PartsStockingUIStrings.ActionPanel_Title_PartsShippingOrderForConfirm,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_ReceivingConfirm,
                        UniqueId = "Receipt",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/CheckInbound.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_BatchReceipt,
                        UniqueId = "BatchReceipt",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/CheckInbound.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_Mandatory,
                        UniqueId = "Mandatory",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PrintReceipt,
                        UniqueId = "Print",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
