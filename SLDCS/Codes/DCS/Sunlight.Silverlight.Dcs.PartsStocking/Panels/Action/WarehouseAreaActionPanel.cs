﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Action {
    public class WarehouseAreaActionPanel : DcsActionPanelBase {
        public WarehouseAreaActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "WarehouseArea",
                Title = PartsStockingUIStrings.ActionPanel_Title_WarehouseArea,
                ActionItems = new[] {
                    new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_ImportWarehouse,
                        UniqueId = "ImportWarehouse",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    },
                      new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_ImportArea,
                        UniqueId = "ImportArea",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = PartsStockingUIStrings.Action_Title_PrintWarehouseArea,
                        UniqueId = "Print",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
