﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.View;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit {
    public partial class AgentsPartsInventoryBillInitialApproveDataEditPanel {

        private ObservableCollection<KeyValuePair> kvIsPasseds;
        public ObservableCollection<KeyValuePair> KvIsPasseds
        {
            get
            {
                return this.kvIsPasseds ?? (this.kvIsPasseds = new ObservableCollection<KeyValuePair>());
            }
        }

        public AgentsPartsInventoryBillInitialApproveDataEditPanel()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI()
        {
            this.kvIsPasseds.Add(new KeyValuePair
            {
                Key = 1,
                Value = PartsStockingUIStrings.DataEditPanel_Text_ApprovePassed
            });
            this.kvIsPasseds.Add(new KeyValuePair
            {
                Key = 0,
                Value = PartsStockingUIStrings.DataEditPanel_Text_ApproveNotPassed
            });
        }

    }
}
