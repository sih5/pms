﻿
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit {
    public partial class CompanyTransferOrderDataEditPanel {
        private readonly string[] kvNames = {
            "CompanyTransferOrder_Type","TransferDirection"
        };
        public CompanyTransferOrderDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += CompanyTransferOrderDataEditPanel_DataContextChanged;
        }

        private void CompanyTransferOrderDataEditPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            if(companyTransferOrder == null)
                return;
            companyTransferOrder.PropertyChanged -= companyTransferOrder_PropertyChanged;
            companyTransferOrder.PropertyChanged += companyTransferOrder_PropertyChanged;
            companyTransferOrder.CompanyTransferOrderDetails.EntityRemoved -= CompanyTransferOrderDetails_EntityRemoved;
            companyTransferOrder.CompanyTransferOrderDetails.EntityRemoved += CompanyTransferOrderDetails_EntityRemoved;
        }

        private void CompanyTransferOrderDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<CompanyTransferOrderDetail> e) {
            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            if(companyTransferOrder == null)
                return;
            companyTransferOrder.TotalAmount = companyTransferOrder.CompanyTransferOrderDetails.Sum(item => item.PlannedAmount * item.Price);
        }

        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;

        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<KeyValuePair> kvOriginalWarehouses;

        public ObservableCollection<KeyValuePair> KvOriginalWarehouses {
            get {
                return this.kvOriginalWarehouses ?? (this.kvOriginalWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        public object KvCompanyTransferOrderType {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvTransferDirections {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        private void companyTransferOrder_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            if(companyTransferOrder == null)
                return;
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                case "OriginalCompanyId":
                    if(companyTransferOrder.CompanyTransferOrderDetails.Any()) {
                        DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Confirm_CompanyTransferOrderDetail, () => {
                            foreach(var item in companyTransferOrder.CompanyTransferOrderDetails)
                                companyTransferOrder.CompanyTransferOrderDetails.Remove(item);
                        }, () => {
                        });
                    }
                    var domainContext = new DcsDomainContext();
                    domainContext.Load(domainContext.GetWarehouseWithOriginalCompanyIdAndPartsSalesCategoryIdQuery(companyTransferOrder.OriginalCompanyId, companyTransferOrder.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        companyTransferOrder.OriginalWarehouseId = null;
                        this.KvOriginalWarehouses.Clear();
                        var warehouse = loadOp.Entities;
                        if(warehouse == null || !warehouse.Any())
                            return;
                        if(warehouse.Count() == 1) {
                            companyTransferOrder.AgencyWarehouseId = warehouse.First().WarehouseId;
                        }
                        foreach(var item in loadOp.Entities) {
                            this.KvOriginalWarehouses.Add(new KeyValuePair {
                                Key = item.WarehouseId,
                                Value = item.Warehouse.Name
                            });
                        }

                    }, null);
                    break;
                case "TransferDirection":
                    foreach(var item in companyTransferOrder.CompanyTransferOrderDetails)
                        companyTransferOrder.CompanyTransferOrderDetails.Remove(item);
                    break;
            }
        }

        private void CreateUI() {
            this.KvPartsSalesCategorys.Clear();
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name,
                        UserObject = item
                    });
                }
            }, null);
            var queryWindow = DI.GetQueryWindow("CompanySecond");
            queryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
            this.ptOriginalCompanyCode.PopupContent = queryWindow;
        }

        private void QueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var company = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if(company == null)
                return;

            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            if(companyTransferOrder == null)
                return;

            companyTransferOrder.OriginalCompanyCode = company.Code;
            companyTransferOrder.OriginalCompanyId = company.Id;
            companyTransferOrder.OriginalCompanyName = company.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
