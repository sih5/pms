﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit {
    public partial class CompanyTransferOrderForApproveDataEditPanel {
        public CompanyTransferOrderForApproveDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
        private readonly string[] kvNames = {
            "CompanyTransferOrder_Type","TransferDirection"
        };
    }
}
