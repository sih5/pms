﻿
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit {
    public partial class DealerPartsInventoryBillForApproveDataEditPanel {
        private ObservableCollection<KeyValuePair> kvPartsSalesCategoryName;

        public ObservableCollection<KeyValuePair> KvPartsSalesCategoryName {
            get {
                return this.kvPartsSalesCategoryName ?? (this.kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>());
            }
        }
        public DealerPartsInventoryBillForApproveDataEditPanel() {
            this.InitializeComponent();
            //this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesByDealerServiceInfoDealerIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.KvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
        }
    }
}
