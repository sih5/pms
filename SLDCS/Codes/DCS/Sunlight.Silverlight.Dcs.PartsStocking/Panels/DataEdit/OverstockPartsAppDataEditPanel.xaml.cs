﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit {
    public partial class OverstockPartsAppDataEditPanel {
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        private void Initialize() {
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(b => b.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.RadComboBoxBranch.ItemsSource = loadOp.Entities;
            }, null);
        }

        private void RadComboBoxBranch_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if(e.AddedItems.Count > 0) {
                var selectedItem = e.AddedItems.Cast<Branch>().First();
                var overstockPartsApp = this.DataContext as OverstockPartsApp;
                if(overstockPartsApp == null)
                    return;
                overstockPartsApp.BranchName = selectedItem.Name;
                overstockPartsApp.BranchCode = selectedItem.Code;
                overstockPartsApp.PartsSalesCategoryId = default(int);
                dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoryByBreachIdQuery(selectedItem.Id).Where(b => b.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.RadComboBoxPartsSalesCategory.ItemsSource = loadOp.Entities;
                }, null);
            }
        }

        public OverstockPartsAppDataEditPanel() {
            this.InitializeComponent();
            this.Initializer.Register(this.Initialize);
            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError) {
                    loadOption.MarkErrorAsHandled();
                    return;
                }
                if(loadOption.Entities != null && loadOption.Entities.Any()) {
                    var company = loadOption.Entities.First();
                    this.comboCompanyType.IsEnabled = company.Type == (int)DcsCompanyType.服务站兼代理库;
                }
            }, null);
        }
    }
}
