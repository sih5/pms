﻿namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit {
    public partial class OverstockPartsAppForBranchDataEditPanel {
        private readonly string[] kvNames = {
            "Company_Type"
        };

        public OverstockPartsAppForBranchDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
