﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit {
    public partial class PartsShippingOrderForConfirmDataEditPanel {
        private readonly string[] kvNames = {
            "PartsShippingOrder_Type", "PartsShipping_Method", "PartsShippingOrder_BillingMethod"
        };

        public PartsShippingOrderForConfirmDataEditPanel() {
            this.KeyValueManager.Register(this.kvNames);
            InitializeComponent();
        }

        public ObservableCollection<KeyValuePair> KvBillingMethod {
            get {
                return this.KeyValueManager[this.kvNames[2]];
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsShippingMethod {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

      
    }
}
