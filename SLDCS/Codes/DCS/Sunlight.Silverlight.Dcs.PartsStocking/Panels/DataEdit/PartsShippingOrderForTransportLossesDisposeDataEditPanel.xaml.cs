﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit {
    public partial class PartsShippingOrderForTransportLossesDisposeDataEditPanel {
        public PartsShippingOrderForTransportLossesDisposeDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        private readonly string[] kvNames = {
            "PartsShipping_Method", "PartsShippingOrder_Type","PartsShippingOrder_BillingMethod"
        };

    }
}
