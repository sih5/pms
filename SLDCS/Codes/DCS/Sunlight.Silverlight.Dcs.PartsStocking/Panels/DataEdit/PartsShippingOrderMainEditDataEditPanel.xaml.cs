﻿using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit {
    public partial class PartsShippingOrderMainEditDataEditPanel {
        public PartsShippingOrderMainEditDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }
        private readonly string[] kvNames = {
            "PartsShipping_Method", "PartsShippingOrder_Type","PartsShippingOrder_BillingMethod"
        };
        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("LogisticCompany");
            queryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
            queryWindow.Loaded += this.QueryWindow_Loaded;
            this.ptbLogisticCompany.PopupContent = queryWindow;

            var queryWindowExpress = DI.GetQueryWindow("Express");
            queryWindowExpress.SelectionDecided += this.QueryWindowExpress_SelectionDecided;
            queryWindowExpress.Loaded += this.QueryWindowExpress_Loaded;
            this.ptExpress.PopupContent = queryWindowExpress;
        }

        void QueryWindowExpress_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            var filterItem = new FilterItem();
            filterItem.MemberName = "Status";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = (int)DcsMasterDataStatus.有效;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
        }

        private void QueryWindowExpress_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var express = queryWindow.SelectedEntities.Cast<Express>().FirstOrDefault();
            if(express == null)
                return;
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            partsShippingOrder.ExpressCompanyId = express.Id;
            partsShippingOrder.ExpressCompanyCode = express.ExpressCode;
            partsShippingOrder.ExpressCompany = express.ExpressName;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void QueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(queryWindow == null || partsShippingOrder == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new object[] {
                new FilterItem("BranchId", typeof(int), FilterOperator.IsEqualTo, partsShippingOrder.BranchId)
            });
        }

        private void QueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var logisticCompanyServiceRange = queryWindow.SelectedEntities.Cast<LogisticCompanyServiceRange>().FirstOrDefault();
            if(logisticCompanyServiceRange == null || logisticCompanyServiceRange.LogisticCompany == null)
                return;

            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;

            partsShippingOrder.LogisticCompanyCode = logisticCompanyServiceRange.LogisticCompany.Code;
            partsShippingOrder.LogisticCompanyId = logisticCompanyServiceRange.LogisticCompanyId;
            partsShippingOrder.LogisticCompanyName = logisticCompanyServiceRange.LogisticCompany.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public object KvShippingMethod {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

    }
}
