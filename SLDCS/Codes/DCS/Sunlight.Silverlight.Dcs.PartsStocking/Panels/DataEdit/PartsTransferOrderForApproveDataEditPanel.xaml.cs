﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit {
    public partial class PartsTransferOrderForApproveDataEditPanel {
        private readonly string[] kvNames = {
            "PartsTransferOrder_Type", "PartsShipping_Method"
        };

        public PartsTransferOrderForApproveDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
