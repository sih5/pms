﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit {
    public partial class WarehouseDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvTypes = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvStorageCompanies = new ObservableCollection<KeyValuePair>();
        private int companyType;
        private readonly string[] kvNames = {
            "Storage_Center", "Storage_Strategy","Warehouse_Type"
        };

        public WarehouseDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += WarehouseDataEditPanel_DataContextChanged;

        }

        private void WarehouseDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var warehouse = this.DataContext as Warehouse;
            if(warehouse == null)
                return;
            warehouse.PropertyChanged -= warehouse_PropertyChanged;
            warehouse.PropertyChanged += warehouse_PropertyChanged;
            //if(companyType == (int)DcsCompanyType.分公司) {
            //    this.isCentralizedPurchase.IsEnabled = true;

            //} else if(companyType == (int)DcsCompanyType.代理库) {
            //    warehouse.IsCentralizedPurchase = false;
            //    this.isCentralizedPurchase.IsChecked = false;
            //    this.isCentralizedPurchase.IsEnabled = false;

            //}
        }
        //
        private void warehouse_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var warehouse = this.DataContext as Warehouse;
            if(warehouse == null)
                return;
            switch(e.PropertyName) {
                case "StorageCompanyId":
                    var company = this.kvStorageCompanies.First(v => v.Key == warehouse.StorageCompanyId).UserObject as Company;
                    if(company == null)
                        return;
                    if(company.Type == (int)DcsCompanyType.代理库)
                        warehouse.StorageCompanyType = (int)DcsCompanyType.代理库;
                    foreach(var operators in warehouse.WarehouseOperators)
                        warehouse.WarehouseOperators.Remove(operators);
                    break;
            }
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            // var domainContextx = new DcsDomainContext();
            domainContext.Load(domainContext.GetCompaniesQuery().Where(e => e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                companyType = loadOp.Entities.First().Type;
                if(companyType == (int)DcsCompanyType.分公司) {
                  //  this.isCentralizedPurchase.IsEnabled = true;
                    domainContext.Load(domainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效 && e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), loadOpa => {
                        if(loadOpa.HasError)
                            return;
                        this.kvBranches.Clear();
                        foreach(var branch in loadOpa.Entities)
                            this.kvBranches.Add(new KeyValuePair {
                                Key = branch.Id,
                                Value = branch.Name,
                            });
                    }, null);
                } else {
                    //this.isCentralizedPurchase.IsEnabled = false;
                   // this.isCentralizedPurchase.IsChecked = false;
                    domainContext.Load(domainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), loadOpa => {
                        if(loadOpa.HasError)
                            return;
                        this.kvBranches.Clear();
                        foreach(var branch in loadOpa.Entities)
                            this.kvBranches.Add(new KeyValuePair {
                                Key = branch.Id,
                                Value = branch.Name,
                            });
                    }, null);
                }

            }, null);


            domainContext.Load(domainContext.GetCompaniesAndAgenciesByLoginCompanyQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvStorageCompanies.Clear();
                foreach(var company in loadOp.Entities) {
                    this.kvStorageCompanies.Add(new KeyValuePair {
                        Key = company.Id,
                        Value = company.Name,
                        UserObject = company
                    });
                }
                this.kvTypes.Clear();
                foreach(var keyValue in this.KeyValueManager[this.kvNames[2]].Where(entity => entity.Key != (int)DcsWarehouseType.虚拟库))
                    this.kvTypes.Add(keyValue);
            }, null);
            domainContext.Load(domainContext.GetCompanyWithBranchIdsQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.First();
               if(company.Type!=(int)DcsCompanyType.分公司){
                   this.CkIsSales.IsEnabled = false;
                   this.CkIsPurchase.IsEnabled = false;
                   this.CkIsAutoApp.IsEnabled = false;
               } else {
                   this.CkIsSales.IsEnabled = true;
                   this.CkIsPurchase.IsEnabled = true;
                   this.CkIsAutoApp.IsEnabled = true;
               }
            }, null);    
        }

        public ObservableCollection<KeyValuePair> KvStorageCompanies {
            get {
                return this.kvStorageCompanies;
            }
        }

        public ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public ObservableCollection<KeyValuePair> KvTypes {
            get {
                return this.kvTypes;
            }
        }

        public object KvStorageCenters {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvStorageStrategies {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
    }
}
