﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail {
    public class AgencyPartsShippingOrderForPartsOutboundDetailPanel : AgencyPartsShippingOrderForPartsOutboundDataGridView, IDetailPanel {
        private int[] shippingOrderIds;

        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsStockingUIStrings.DataEditPanel_Title_ShippingInfomation;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAgencyPartsShippingOrdersByIds";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "ids":
                    return shippingOrderIds.Length > 0 ? shippingOrderIds : new int[] { };
            }
            return null;
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        public AgencyPartsShippingOrderForPartsOutboundDetailPanel() {
            this.DataContextChanged += PartsShippingOrderForPartsOutboundDetailPanel_DataContextChanged;
        }

        private void PartsShippingOrderForPartsOutboundDetailPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsOutboundBill = e.NewValue as AgencyPartsOutboundBillWithOtherInfo;
            if(partsOutboundBill == null)
                return;
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetAgencyPartsShippingOrderRefsQuery().Where(ex => ex.PartsOutboundBillId == partsOutboundBill.PartsOutboundBillId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities;
                if(entity == null)
                    return;
                shippingOrderIds = loadOp.Entities.Select(r => r.PartsShippingOrderId).ToArray();
                this.ExecuteQueryDelayed();
            }, null);
        }
    }
}
