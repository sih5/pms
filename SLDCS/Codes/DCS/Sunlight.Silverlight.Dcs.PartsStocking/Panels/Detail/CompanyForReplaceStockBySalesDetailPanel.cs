﻿using System;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail {
    public class CompanyForReplaceStockBySalesDetailPanel : CompanyStockForReplaceBySalesDataGridView, IDetailPanel {
        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsStockingUIStrings.DetailPanel_GroupTitle_CompanyStockForReplace;
            }
        }
    }
}
