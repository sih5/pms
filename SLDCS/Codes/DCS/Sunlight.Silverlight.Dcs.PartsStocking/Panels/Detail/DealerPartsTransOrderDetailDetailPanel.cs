﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail {
    public class DealerPartsTransOrderDetailDetailPanel : DealerPartsTransOrderDetailDataGridView, IDetailPanel {

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.DomainDataSource.PageSize = 10;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        public string Title {
            get {
                return PartsStockingUIStrings.DataEditPanel_Title_DealerPartsTransOrderDetail;
            }
        }
    }
}
