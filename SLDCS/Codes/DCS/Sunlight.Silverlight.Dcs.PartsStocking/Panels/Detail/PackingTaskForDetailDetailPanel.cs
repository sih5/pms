﻿using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail
{
    public class PackingTaskForDetailDetailPanel : PackingTaskForDetailDataGridView, IDetailPanel
    {

        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public Uri Icon
        {
            get
            {
                return null;
            }
        }

        public string Title
        {
            get
            {
                return PartsStockingUIStrings.DataEditPanel_Title_PackingTaskDetails;
            }
        }
    }

}
