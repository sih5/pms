﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail {
    public class PartsInboundPlanDetailDetailPanel : PartsInboundPlanDetailDataGridView, IDetailPanel {

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public string Title {
            get {
                return Utils.GetEntityLocalizedName(typeof(PartsInboundPlan), "PartsInboundPlanDetails");
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }
    }
}
