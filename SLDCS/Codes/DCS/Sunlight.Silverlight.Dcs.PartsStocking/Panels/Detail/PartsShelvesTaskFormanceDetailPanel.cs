﻿using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail {
    public class PartsShelvesTaskFormanceDetailPanel : PartsShelvesTaskFormanceDataGridView, IDetailPanel {

        public System.Uri Icon {
            get {
                return null;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public string Title {
            get {
                return "已上架清单";
            }
        }
    }
}
