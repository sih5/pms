﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail {
    public class PartsShippingOrderDetailDetailPanel : PartsShippingOrderDetailDataGridView, IDetailPanel {
        public PartsShippingOrderDetailDetailPanel() {
            this.DataContextChanged += this.PartsShippingOrderDetailDetailPanel_DataContextChanged;
        }

        private void PartsShippingOrderDetailDetailPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsShippingOrder = e.NewValue as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartsShippingOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = partsShippingOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.DomainDataSource.PageSize = 10;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsStockingUIStrings.DetailPanel_Title_PartsShippingOrderDetail;
            }
        }
    }
}
