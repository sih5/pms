﻿using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail {
    public partial class PartsShippingOrderDetailPanel {
        private readonly string[] kvNames = {
            "PartsShippingOrder_Type", "PartsShipping_Method", "PartsShippingOrder_Status", "PartsShippingOrder_BillingMethod","ArrivalMode"
        };

        public PartsShippingOrderDetailPanel() {
            this.KeyValueManager.Register(this.kvNames);
            InitializeComponent();
        }
        public override string Title {
            get {
                return PartsStockingUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
