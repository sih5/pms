﻿using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail {
    public class PartsTransferOrderDetailDetailPanel : PartsTransferOrderDetailDataGridView, IDetailPanel {

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        public System.Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return Utils.GetEntityLocalizedName(typeof(PartsTransferOrder), "PartsTransferOrderDetails");
            }
        }
    }
}
