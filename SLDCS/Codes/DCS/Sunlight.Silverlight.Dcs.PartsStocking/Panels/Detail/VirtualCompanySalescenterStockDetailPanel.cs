﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using System.Windows;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail {
    public class VirtualCompanySalescenterStockDetailPanel : DcsDataGridViewBase,IDetailPanel {
        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return PartsStockingUIStrings.DetailPanel_Title_VirtualCompanySalescenterStock;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartsCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_SparePartsCode
                    }, new ColumnItem {
                        Name = "SparePartsName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_SparePartsName
                    }, new ColumnItem {
                        Name = "FactAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_FactAmount
                    }, new ColumnItem {
                        Name = "UsableAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_UsableAmount
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_BranchName
                    },new KeyValuesColumnItem{
                        Name="PartsSalesCategoryName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_PartsSalesCategory
                    }, new ColumnItem {
                        Name = "CompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_CompanyCode
                    },new ColumnItem {
                        Name = "CompanyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_CompanyName
                    },new ColumnItem {
                        Name = "CompanyType",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_CompanyType
                    }
                };
            }
        }


        private void PartsPurchasePricingDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var virtualCompanySalescenterStock = e.NewValue as VirtualCompanySalescenterStock;
            if(virtualCompanySalescenterStock == null || virtualCompanySalescenterStock.SparePartsId == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "SparePartsId",
                    MemberType = typeof(int),
                    Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = virtualCompanySalescenterStock.SparePartsId;
            this.ExecuteQueryDelayed();
        }

        protected override string OnRequestQueryName() {
            return "查询替互换件企业销售中心库存";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            switch(parameterName) {
                case "companyId":
                    var filterItem1 = BaseApp.Current.CurrentUserData.EnterpriseId;
                    return filterItem1;
                case "partsSalesCategoryId":
                    var dataContext = this.DataContext as VirtualCompanySalescenterStock;
                    var filterItem2 = dataContext.PartsSalesCategoryId;
                    return filterItem2;
                case "sparePartsId":
                    var filterItem3 = filter.Filters.SingleOrDefault(item => item.MemberName == "SparePartsId");
                    if(filterItem3 == null)
                        break;
                    return filterItem3.Value;
            }
            return null;
        }

        protected override System.Type EntityType {
            get {
                return typeof(VirtualCompanySalescenterStock);
            }
        }
    }
}