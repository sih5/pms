﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail {
    public partial class WarehouseAreaDetailPanel {
        private readonly string[] kvNames = {
            "Area_Kind", "Area_Category"
        };

        public WarehouseAreaDetailPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.CreateUI();
        }
        private DcsDataGridViewBase warehouseAreaManagerDataGridView;
        private DcsDataGridViewBase WarehouseAreaManagerDataGridView {
            get {
                return this.warehouseAreaManagerDataGridView ?? (this.warehouseAreaManagerDataGridView = DI.GetDataGridView("WarehouseAreaManager") as DcsDataGridViewBase);
            }
        }
        private void CreateUI() {
            this.ManagerGridView.Content = this.WarehouseAreaManagerDataGridView;

        }
    }
}
