﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Detail {
    public class WarehouseOperatorDetailPanel:WarehouseOperatorDataGridView,IDetailPanel{
        public string Title {
            get {
                return PartsStockingUIStrings.DetailPanel_Title_Personnel;
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
    }
}
