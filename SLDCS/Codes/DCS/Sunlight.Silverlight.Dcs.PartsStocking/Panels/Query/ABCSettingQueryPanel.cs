﻿using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class ABCSettingQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "ABCSetting_Type"
        };
        public ABCSettingQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.Title_ABCSettingQuery,
                    EntityType = typeof(ABCSetting),
                    QueryItems = new[] {
                         new KeyValuesQueryItem {
                            ColumnName = "Type",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],                            
                            Title = PartsStockingUIStrings.DataGridView_Title_ABCSetting_Type
                        }
                    }
                }
            };
        }
    }
}
