﻿using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class AccurateTraceForDownQueryPanel : DcsQueryPanelBase {

        public AccurateTraceForDownQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {        

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "可下架标签码查询",
                    EntityType = typeof(TraceWarehouseAge),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SIHLabelCode",
                            Title = "SIH件码"
                        }, new QueryItem {
                            ColumnName = "BoxCode",
                            Title = "SIH箱码"
                        }
                    }
                }
            };
        }
    }
}
