﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class AgencyDifferenceBackBillForSparepartQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehous = new ObservableCollection<KeyValuePair>();


        public AgencyDifferenceBackBillForSparepartQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var item in loadOp.Entities)
                    this.kvWarehous.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = "可差异退货配件查询",
                    EntityType = typeof(AgencyDifferenceBackBillForSparepart),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "SparePartCode",
                            Title="配件编号",
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "SparePartName",
                            Title="配件名称",
                            DataType=typeof(string)
                        },new CustomQueryItem{
                            ColumnName = "PartsInboundPlanCode",
                            DataType = typeof(string),
                            Title = "入库计划单号"
                        },new CustomQueryItem{
                            ColumnName = "PartsSalesOrderCode",
                            DataType = typeof(string),
                            Title = "销售单号"
                        }, new KeyValuesQueryItem {
                            ColumnName = "InWarehouseId",
                            KeyValueItems = this.kvWarehous,
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            Title="仓库"
                        }   ,new CustomQueryItem{
                            ColumnName = "PartsShippingCode",
                            DataType = typeof(string),
                            Title = "发运单号"
                        }                     
                        , new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title="入库计划单创建时间",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                    }
                }

            };
        }
    }
}
