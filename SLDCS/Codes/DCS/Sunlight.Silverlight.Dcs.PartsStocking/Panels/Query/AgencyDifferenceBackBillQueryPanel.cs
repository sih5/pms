﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class AgencyDifferenceBackBillQueryPanel  : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvResTypes = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "AgencyDifferenceBackBillStatus","ResponsibleMembersResTem"
        };

        public AgencyDifferenceBackBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            var a = new PartsInboundCheckBill();
            domainContext.Load(domainContext.GetResRelationshipsQuery().Where(e =>  e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Id), LoadBehavior.RefreshCurrent, loadOp => {
                foreach(var entity in loadOp.Entities) {
                    this.kvResTypes.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.ResType
                    });
                }
                    this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = "中心库收货差异处理查询",
                        EntityType = typeof(AgencyDifferenceBackBill),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "StorageCompanyCode",
                                Title="中心库编号"
                            },new QueryItem {
                                ColumnName = "StorageCompanyName",
                                Title="中心库名称"
                            }, new QueryItem {
                                ColumnName = "SparePartCode",
                                Title="配件图号"
                            },new QueryItem {
                                ColumnName = "SparePartName",
                                Title="配件名称"
                            },new QueryItem {
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Custom_BillCode,
                                ColumnName = "Code"
                            },new KeyValuesQueryItem {
                                ColumnName = "Status",
                                Title="状态",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            },new KeyValuesQueryItem {
                                Title = "责任类型",
                                ColumnName = "ResRelationshipId",
                                KeyValueItems = this.kvResTypes
                            },new KeyValuesQueryItem {
                                ColumnName = "ResTem",
                                Title="责任组",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                            },new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    DateTime.Now.Date, DateTime.Now.Date
                                },
                                Title="创建时间"
                           },new DateTimeRangeQueryItem {
                                ColumnName = "SubmitTime",
                                Title="提交时间"
                           }
                        }
                    }
                };
            }, null);
        }
    }
}
