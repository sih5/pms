﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class AgencyPartsOutboundBillQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "Parts_OutboundType", "Parts_SettlementStatus"
        };

        public AgencyPartsOutboundBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && (e.Type == (int)DcsWarehouseType.总库 || e.Type == (int)DcsWarehouseType.分库)).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesbyPersonnelIdQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetCompanyWithBranchIdsQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                if(company == null)
                    return;
                if(company.Type == (int)DcsCompanyType.分公司) {
                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                } else {
                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsOutboundBill,
                    EntityType = typeof(PartsOutboundBillWithOtherInfo),
                    QueryItems = new[] {
                        new QueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsOutboundBill_Code,
                            ColumnName = "Code"
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundCheckBill_PartsSalesCategoryName,
                            KeyValueItems = this.kvPartsSalesCategoryName
                        },
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsOutboundBill_WarehouseName,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "OutboundType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = PartsStockingUIStrings.QueryPanel_Title_OutboundType
                        },
                        new QueryItem {
                            ColumnName = "CounterpartCompanyCode",
                            Title=PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyCode
                        },
                        new QueryItem {
                            ColumnName = "CounterpartCompanyName",
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyName
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "SettlementStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                           Title = PartsStockingUIStrings.QueryPanel_Title_SettlementStatus
                        },
                        new QueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_SalesOrderType,
                            ColumnName = "PartsSalesOrderTypeName"
                        },
                        new QueryItem{
                          Title = PartsStockingUIStrings.QueryPanel_Title_PartsOutboundPlan_Code,
                          ColumnName = "PartsOutboundPlanCode"
                        },
                        new QueryItem {
                           ColumnName = "ZpNumber",
                            Title = PartsStockingUIStrings.QueryPanel_Title_ZpNumber
                        },new QueryItem {
                            ColumnName = "ProvinceName",
                            Title = PartsStockingUIStrings.QueryPanel_Title_ProvinceName
                        },new QueryItem {
                           ColumnName = "ContractCode",
                            Title = PartsStockingUIStrings.QueryPanel_Title_ContractCode
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                             Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime,
                            DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(1 - DateTime.Now.Day).Date, DateTime.Now.Date
                            }
                        },new QueryItem {
                            ColumnName = "ERPSourceOrderCode",
                            Title = PartsStockingUIStrings.QueryPanel_Title_ERPSourceOrderCode
                        },new QueryItem {
                           ColumnName = "SAPPurchasePlanCode",
                           Title = PartsStockingUIStrings.QueryPanel_Title_SAPPurchasePlanCode
                        } ,new QueryItem {
                           ColumnName = "OutboundPackPlanCode",
                           Title = PartsStockingUIStrings.QueryPanel_Title_OutboundPackPlanCode
                        } 
                    }
                }
            };
        }
    }
}
