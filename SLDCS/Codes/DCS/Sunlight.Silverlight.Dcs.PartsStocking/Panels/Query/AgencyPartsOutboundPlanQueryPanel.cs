﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class AgencyPartsOutboundPlanQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "Parts_OutboundType", "PartsOutboundPlan_Status"
        };

        public AgencyPartsOutboundPlanQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesbyPersonnelIdQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);


            dcsDomainContext.Load(dcsDomainContext.GetCompanyWithBranchIdsQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                if(company == null)
                    return;
                if(company.Type == (int)DcsCompanyType.分公司) {
                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                } else {
                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsOutboundPlan,
                    EntityType = typeof(AgencyPartsOutboundPlan),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundPlan_Code
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            Title = PartsStockingUIStrings.DataEditPanel_Text_CompanyTransferOrder_PartsSalesCategory,
                            KeyValueItems = this.kvPartsSalesCategoryName
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        },
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundPlan_WarehouseName,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses,
                            IsExact = false
                        },
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_OutboundType,
                            ColumnName = "OutboundType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            IsExact = false
                        },
                        new QueryItem {
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyCode,
                            ColumnName = "CounterpartCompanyCode"
                        },
                        new QueryItem {
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyName,
                            ColumnName = "CounterpartCompanyName"
                        },
                        new QueryItem {
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_SourceCode,
                            ColumnName = "SourceCode"
                        },
                        new QueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_SalesOrderType,
                            ColumnName = "PartsSalesOrderTypeName"
                       } ,
                        new QueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_ERPSourceOrderCode,
                            ColumnName = "ERPSourceOrderCode"
                       } ,
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            },
                            IsExact = false
                        }
                    }
                }
            };
        }
    }
}
