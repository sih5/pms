﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class AgencyPartsShippingOrderQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvClosedLoopStatus = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "PartsShippingOrder_Type", "PartsShippingOrder_Status"
        };

        public AgencyPartsShippingOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);

        }

        private void Initialize() {
            this.kvClosedLoopStatus.Add(new KeyValuePair() {
                Key = 1,
                Value = PartsStockingUIStrings.QueryPanel_Title_NoClosedLoop
            });
            this.kvClosedLoopStatus.Add(new KeyValuePair() {
                Key = 2,
                Value = PartsStockingUIStrings.QueryPanel_Title_ClosedLoop
            });
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                if(company == null)
                    return;
                if(company.Type == (int)DcsCompanyType.分公司) {
                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                } else {
                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                }
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(AgencyPartsShippingOrder),
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsShippingOrder,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_Code
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Type",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_Type,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        },
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_OutWarehouse,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "RequestedArrivalDate",
                            //DefaultValue = new[] {
                            //    DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                            //}
                        },
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_ClosedLoopStatus,
                            ColumnName = "ClosedLoopStatus",
                            KeyValueItems = this.kvClosedLoopStatus
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                            }
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "ConfirmedReceptionTime",
                            //DefaultValue = new[] {
                            //    DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                            //}
                        },
                        new QueryItem {
                            ColumnName = "ReceivingCompanyCode"
                        },
                        new QueryItem {
                            ColumnName = "ReceivingCompanyName"
                        },
                        new QueryItem {
                            ColumnName = "ReceivingWarehouseName"
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_PartsSalesCategoryName,
                            KeyValueItems = this.kvPartsSalesCategoryName
                        },
                        new QueryItem {
                            ColumnName = "ExpressCompanyCode",
                            Title = PartsStockingUIStrings.DataEditPanel_Text_ExpressCompanyCode
                        },
                        new QueryItem {
                            ColumnName = "ExpressCompany",
                            Title = PartsStockingUIStrings.DataEditPanel_Text_ExpressCompanyName
                        },
                        new QueryItem {
                            ColumnName = "LogisticCompanyName"
                        },
                        new QueryItem {
                            ColumnName = "ERPSourceOrderCodeQuery",
                            Title =PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ERPOrderCode
                        },
                        new QueryItem {
                            ColumnName = "GPSCode"
                        }
                    }
                }
            };

        }
    }
}