﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class AgencyPartsStockForDealerQueryQueryPanel : DcsQueryPanelBase
    {
        private void Initialize() {
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_VirtualAgencyPartsStockQuery,
                        EntityType = typeof(VirtualAgencyPartsStock),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "CompanyCode",
                                Title = PartsStockingUIStrings.DataEditPanel_Text_OverstockPartsApp_StorageCompanyCode
                            }, new QueryItem {
                                ColumnName = "CompanyName",
                                Title = PartsStockingUIStrings.DataEditPanel_Text_OverstockPartsApp_StorageCompanyName
                            }
                            ,new CustomControlQueryItem { 
                                ColumnName = "SparePartCode", 
                                Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode, 
                                CreateControl=type =>new Controls.MultipleTextQueryControl(type,"SparePartCode") 
                            }
                            , new QueryItem {
                                ColumnName = "SparePartName",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_SparePartName
                            }
                            , new CustomQueryItem {
                                ColumnName = "GreaterThanZero",
                                DataType = typeof(bool),
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_GreaterThanZero,
                                DefaultValue = true
                            }
                        }
                    }
            };
        }

        public AgencyPartsStockForDealerQueryQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
        }
    }
}
