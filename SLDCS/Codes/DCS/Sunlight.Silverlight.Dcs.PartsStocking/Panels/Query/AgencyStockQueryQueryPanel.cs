﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class AgencyStockQueryQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();

        public AgencyStockQueryQueryPanel() {
            Initializer.Register(Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOpKey => {
                if(loadOpKey.HasError) {
                    if(!loadOpKey.IsErrorHandled)
                        loadOpKey.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpKey);
                }
                this.kvBranches.Clear();
                foreach(var branch in loadOpKey.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Code
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.kvPartsSalesCategory.Clear();
                foreach(var warehouse in loadOp2.Entities) {
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);

            dcsDomainContext.Load(dcsDomainContext.GetCompanyWithBranchIdsQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyStockQuery,
                        EntityType = typeof(AgencyStockQuery),
                        QueryItems = new QueryItem[] {
                            new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_VirtualCompanySalescenterStock_PartsSalesCategory,
                                ColumnName = "PartsSalesCategoryId",
                                KeyValueItems = this.kvPartsSalesCategory
                            },
                            new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyCode,
                                ColumnName = "AgencyCode",
                                DefaultValue = company != null && company.Type == (int)DcsCompanyType.代理库 ? company.Code : null,
                                IsEnabled = company != null && company.Type != (int)DcsCompanyType.代理库
                            },
                            new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyName,
                                ColumnName = "AgencyName",
                                DefaultValue = company != null && company.Type == (int)DcsCompanyType.代理库 ? company.Name : null,
                                IsEnabled = company != null && company.Type != (int)DcsCompanyType.代理库
                            },
                            new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_BranchName,
                                ColumnName = "BranchId",
                                KeyValueItems = kvBranches,
                                IsEnabled =BaseApp.Current.CurrentUserData.UserCode=="ReportAdmin",
                                DefaultValue = company != null && company.Type == (int)DcsCompanyType.分公司 ? company.Id : 0,
                            },
                            new QueryItem {
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode,
                                ColumnName = "PartCode",
                            },
                            new QueryItem {
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName,
                                ColumnName = "PartName"
                            }
                        }

                    }
                };
            }, null);
        }
    }
}
