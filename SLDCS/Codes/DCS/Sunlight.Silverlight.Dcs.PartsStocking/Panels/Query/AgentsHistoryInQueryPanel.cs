﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class AgentsHistoryInQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvWarehouse = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvName = new[] {
            "Parts_InboundType"
        };
        public AgentsHistoryInQueryPanel() {
            Initializer.Register(Initialize);
            this.KeyValueManager.Register(this.kvName);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOpKey => {
                if(loadOpKey.HasError) {
                    if(!loadOpKey.IsErrorHandled)
                        loadOpKey.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpKey);
                }
                this.kvBranches.Clear();
                foreach(var branch in loadOpKey.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Code
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.kvPartsSalesCategory.Clear();
                foreach(var warehouse in loadOp2.Entities) {
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesWithBranchIdsQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.kvWarehouse.Clear();
                foreach(var item in loadOp2.Entities) {
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
                }
            }, null);

            dcsDomainContext.Load(dcsDomainContext.GetCompanyWithBranchIdsQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_AgencyInHistoryQuery,
                        EntityType = typeof(VehicleAgentsHistoryIn),
                        QueryItems = new QueryItem[] {
                               new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_BranchName,
                                ColumnName = "BranchId",
                                KeyValueItems = kvBranches,
                                DefaultValue = company != null && company.Type == (int)DcsCompanyType.分公司 ? company.Id : 0,
                                IsEnabled = BaseApp.Current.CurrentUserData.UserCode == "ReportAdmin"
                            },new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyTransferOrder_PartsSalesCategoryName,
                                ColumnName = "PartsSalesCategoryId",
                                KeyValueItems = this.kvPartsSalesCategory
                            }, new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_WarehouseName,
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouse
                            },  new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_InboundType,
                                ColumnName = "InboundType",
                               KeyValueItems = this.KeyValueManager[this.kvName[0]]
                            },  new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyCode,
                                ColumnName = "StorageCompanyCode",
                            }, new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyName,
                                ColumnName = "StorageCompanyName"
                            },  new QueryItem {
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode,
                                ColumnName = "PartCode",
                            }, new QueryItem {
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName,
                                ColumnName = "PartName"
                            },new DateTimeRangeQueryItem {
                                ColumnName = "PartsInboundPlanTime",
                                Title=PartsStockingUIStrings.QueryPanel_QueryItem_QueryTime
                          }
                        }
                    }
                };
            }, null);
        }
    }
}
