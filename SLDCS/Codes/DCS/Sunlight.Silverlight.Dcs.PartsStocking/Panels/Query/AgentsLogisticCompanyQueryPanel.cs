﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class AgentsLogisticCompanyQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public AgentsLogisticCompanyQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =PartsStockingUIStrings.QueryPanel_Title_AgentsLogisticCompany,
                    EntityType = typeof(AgencyLogisticCompany),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Name"
                        }, new QueryItem {
                            ColumnName = "Code"
                        }
                        , new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            IsEnabled = true
                        }
                    }
                }
            };
        }
    }
}
