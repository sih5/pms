﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class AgentsPartsInventoryBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsInventoryBill_Status","Area_Category"
        };
        private readonly ObservableCollection<KeyValuePair> kvType = new ObservableCollection<KeyValuePair>();


        public AgentsPartsInventoryBillQueryPanel() {           
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
             var domainContext = new DcsDomainContext();
             bool insert = true;
            domainContext.Load(domainContext.GetMultiLevelApproveConfigsQuery().Where(t=>t.Type==(int)DCSMultiLevelApproveConfigType.中心库配件盘点单&& t.Status==(int)DcsBaseDataStatus.有效).OrderBy(r=>r.MinApproveFee), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvType.Clear();
                foreach(var item in loadOp.Entities) {
                    foreach(var kv in kvType) {
                        if(kv.Value.StartsWith(item.MinApproveFee.ToString()) && kv.Value.EndsWith(item.MaxApproveFee.ToString())) {
                            insert = false;
                            continue;
                        } else insert = true;
                    }
                    if(insert) {
                        var keyType = new KeyValuePair();
                        keyType.Key = item.Id;
                        keyType.Value = item.MinApproveFee + "~" + item.MaxApproveFee;
                        kvType.Add(keyType);
                    }

                }
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_AgencyPartsInventoryBill,
                        EntityType = typeof(PartsInventoryBillEx),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "StorageCompanyCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyCode
                            },new QueryItem {
                                ColumnName = "StorageCompanyName",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyName
                            },new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_WarehouseName,
                                ColumnName = "WarehouseName"
                            }, new QueryItem {
                                ColumnName = "Code",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_Code
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                Title=PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_Status
                            }, new KeyValuesQueryItem {
                                ColumnName = "WarehouseAreaCategory",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_WarehouseAreaCategory
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CreateTime,
                                DefaultValue = new[] {
                                        new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                            }, new KeyValuesQueryItem {
                                ColumnName = "AmountDifferenceRange",
                                KeyValueItems = this.kvType,
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostDifference,
                                
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
