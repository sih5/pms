﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class BorrowBillQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouse = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
            "BorrowBill_Status", "BorrowBill_Type"
        };

        public BorrowBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesWithStorageCompanyQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && !e.WmsInterface && e.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var _entities = loadOp.Entities.OrderBy(i => i.Name);
                foreach(var warehouse in _entities) {
                    this.kvWarehouse.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);
            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId = "Common",
                    EntityType = typeof(BorrowBill),
                    Title = PartsStockingUIStrings.QueryPanel_Title_BorrowBill,
                    QueryItems = new[]{
                        new QueryItem{
                            ColumnName = "Code"
                        },new KeyValuesQueryItem{
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouse,
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_BorrowBill_WarehouseName
                        }, new KeyValuesQueryItem{
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem{
                            ColumnName = "Type",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "ApproveTime"
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "OutBoundTime"
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "ExpectReturnTime"
                        }
                    }
                }
            };
        }
    }
}
