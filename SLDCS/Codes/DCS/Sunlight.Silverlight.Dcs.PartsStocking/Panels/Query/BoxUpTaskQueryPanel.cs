﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class BoxUpTaskQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "BoxUpTaskStatus"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        public BoxUpTaskQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        public void Initialize()
        {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair
                    {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });
            }, null);

            dcsDomainContext.Load(dcsDomainContext.GetCompanyWithBranchIdsQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                if (company == null)
                    return;
                if (company.Type == (int)DcsCompanyType.代理库)
                {

                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 =>
                    {
                        if (loadOp1.HasError)
                            return;
                        foreach (var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategorys.Add(new KeyValuePair
                            {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                }
                else
                {
                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp1 =>
                    {
                        if (loadOp1.HasError)
                            return;
                        foreach (var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategorys.Add(new KeyValuePair
                            {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                }
            }, null);

                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_BoxUpTask,
                    EntityType = typeof(VirtualBoxUpTask),
                    QueryItems = new[] {
                        new QueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_BoxUpTask_Code,
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem {
                            Title=PartsStockingUIStrings.QueryPanel_Title_WarehouseName,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        }
                        //,new KeyValuesQueryItem {
                        //    Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategory,
                        //    ColumnName = "PartsSalesCategoryId",
                        //    KeyValueItems = this.kvPartsSalesCategorys
                        //}
                        ,new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_Status,
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                        , new CustomQueryItem{
                            Title =PartsStockingUIStrings.QueryPanel_Title_SourceCode,
                            ColumnName = "SourceCode",
                            DataType = typeof(string)
                        }
                        ,new CustomQueryItem {
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                            ColumnName = "SparePartCode",
                            DataType = typeof(string)
                        }
                        ,new CustomQueryItem {
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName,
                            ColumnName = "SparePartName",
                            DataType = typeof(string)
                        }
                        ,new CustomQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_PartsOutboundPlan_Code,
                            ColumnName = "PartsOutboundPlanCode",
                            DataType = typeof(string)
                        }
                        //,new CustomQueryItem {
                        //    Title = "对方单位编号",
                        //    ColumnName = "CounterpartCompanyCode",
                        //    DataType = typeof(string)
                        //}
                        ,new CustomQueryItem {
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyName,
                            ColumnName = "CounterpartCompanyName",
                            DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime,
                            DefaultValue = new[] {
                               DateTime.Now.AddDays(-7).Date, DateTime.Now.Date
                            }
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ModifyTime",
                            Title = PartsStockingUIStrings.QueryPanel_Title_BoxUpFinishTime
                        }
                    }
                }
            };
        }
    }
}
