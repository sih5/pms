﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class CompanyStockQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvBranchs = new ObservableCollection<KeyValuePair>();

        public CompanyStockQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranchs.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_CompanyPartsStock,
                    EntityType = typeof(CompanyPartsStock),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartCode
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartName
                        }, new KeyValuesQueryItem {
                            ColumnName = "BranchId",
                            KeyValueItems = this.kvBranchs,
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_Branch
                        }
                    }
                }
            };
        }
    }
}
