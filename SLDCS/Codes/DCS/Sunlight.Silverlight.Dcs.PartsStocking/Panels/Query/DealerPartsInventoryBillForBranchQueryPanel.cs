﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class DealerPartsInventoryBillForBranchQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "DealerPartsInventoryBill_Status"                    
        };
        private readonly ObservableCollection<KeyValuePair> kvType = new ObservableCollection<KeyValuePair>();


        public DealerPartsInventoryBillForBranchQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            bool insert = true;
            domainContext.Load(domainContext.GetMultiLevelApproveConfigsQuery().Where(t => t.Type == (int)DCSMultiLevelApproveConfigType.服务站配件盘点单 && t.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.MinApproveFee), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvType.Clear();
                foreach(var item in loadOp.Entities) {
                    foreach(var kv in kvType) {
                        if(kv.Value.StartsWith(item.MinApproveFee.ToString()) && kv.Value.EndsWith(item.MaxApproveFee.ToString())) {
                            insert = false;
                            continue;
                        } else insert = true;
                    }
                    if(insert) {
                        var keyType = new KeyValuePair();
                        keyType.Key = item.Id;
                        keyType.Value = item.MinApproveFee + "~" + item.MaxApproveFee;
                        kvType.Add(keyType);
                    }

                }
                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_DealerPartsInventoryBill,
                    EntityType = typeof(DealerPartsInventoryBill),
                    QueryItems = new QueryItem[] {
                        new QueryItem{
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsInventoryBill_Code
                        }, new QueryItem{
                            ColumnName = "AgencyName",
                            Title = "中心库名称"
                        }, new QueryItem{
                            ColumnName = "StorageCompanyCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsInventoryBill_StorageCompanyCode
                        }, new QueryItem{
                            ColumnName = "StorageCompanyName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsInventoryBill_StorageCompanyName
                        }
                        ,new KeyValuesQueryItem {
                            ColumnName = "Status",
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                                ColumnName = "AmountDifferenceRange",
                                KeyValueItems = this.kvType,
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostDifference,
                                
                            }
                        ,new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
            }, null);
        }
    }
}
