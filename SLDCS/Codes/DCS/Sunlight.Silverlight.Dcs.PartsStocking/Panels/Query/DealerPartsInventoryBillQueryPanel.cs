﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class DealerPartsInventoryBillQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "DealerPartsInventoryBill_Status"                    
        };


        public DealerPartsInventoryBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetDealerServiceInfoesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.DealerId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                var dealerCompany = loadOp2.Entities.Select(r => r.PartsSalesCategoryId).ToArray();
                dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesByDealerServiceInfoOrAgencyAffiBranchListQuery(dealerCompany), LoadBehavior.RefreshCurrent, dealerCompanys => {
                    if(dealerCompanys.HasError)
                        return;
                    foreach(var partsSalesCategory in dealerCompanys.Entities)
                        this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                            Key = partsSalesCategory.Id,
                            Value = partsSalesCategory.Name
                        });
                }, null);
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_DealerPartsInventoryBill,
                    EntityType = typeof(DealerPartsInventoryBill),
                    QueryItems = new QueryItem[] {
                        new QueryItem{
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsInventoryBill_Code
                        }, new QueryItem{
                            ColumnName = "StorageCompanyCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsInventoryBill_StorageCompanyCode
                        }, new QueryItem{
                            ColumnName = "StorageCompanyName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsInventoryBill_StorageCompanyName
                        }, new KeyValuesQueryItem{
                            ColumnName = "SalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategoryName,
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsInventoryBill_SalesCategoryName
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime,
                            DefaultValue = new[] {
                               DateTime.Now.AddDays(-7).Date, DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}
