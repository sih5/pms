﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class DealerPartsStockQueryViewForWindowQueryPanel : DcsQueryPanelBase {

        public DealerPartsStockQueryViewForWindowQueryPanel() {
            Initializer.Register(Initialize);
        }
        private void Initialize() {
            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsTransferOrderStock,
                    EntityType = typeof(DealerPartsStockQueryView),
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "NoWarrantyBrandName",
                            Title = PartsStockingUIStrings.QueryPanel_Title_NoWarrantyBrandName,
                            DataType=typeof(string)
                        },
                        new QueryItem {
                            ColumnName = "PartCode",
                            Title=PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode
                        },
                        new CustomQueryItem {
                            ColumnName = "WarrantyBrandName",
                            Title = PartsStockingUIStrings.QueryPanel_Title_WarrantyBrandName,
                            DataType=typeof(string)
                        },
                        new QueryItem {
                            ColumnName = "PartName"
                        },
                    }
                }
            };
        }
    }
}