﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class DealerPartsStockQueryViewQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "ABQualityInformationStatus"
        };

        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public DealerPartsStockQueryViewQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var entity in loadOp.Entities) {
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);

            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(DealerPartsStockQueryView),
                    Title = PartsStockingUIStrings.QueryPanel_Title_DealerPartsStockQueryView,
                    QueryItems = new[] {
                       new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys,
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStockQueryView_PartsSalesCategoryId
                        },new QueryItem {
                            ColumnName = "DealerCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStockQueryView_DealerCode,
                            IsExact = true,
                            IsCaseSensitive = true
                        },new QueryItem {
                            ColumnName = "PartCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStockQueryView_PartCode ,
                            IsExact = true,
                            IsCaseSensitive = true
                        },new QueryItem {
                            ColumnName = "IsStoreMoreThanZero"
                            
                        }
                    }
                }
            };
        }
    }
}