﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class DealerPartsStockServiceQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> KvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        public DealerPartsStockServiceQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_DealerPartsStock,
                    EntityType = typeof(DealerPartsStock),
                    QueryItems = new [] {
                        new QueryItem{
                            ColumnName = "SparePartCode",
                        }, new CustomQueryItem{
                            ColumnName = "SparePart.Name",
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName,
                            DataType = typeof(string)
                        }, new QueryItem{
                            ColumnName = "DealerCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_DealerCode,
                            IsEnabled=false
                        }, new QueryItem{
                            ColumnName = "DealerName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_DealerName,
                            IsEnabled=false
                        }, new KeyValuesQueryItem{
                            ColumnName = "SalesCategoryId",
                            KeyValueItems=this.KvPartsSalesCategorys,
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategory,
                            IsEnabled=false
                        }
                    }
                }
            };
        }
    }
}