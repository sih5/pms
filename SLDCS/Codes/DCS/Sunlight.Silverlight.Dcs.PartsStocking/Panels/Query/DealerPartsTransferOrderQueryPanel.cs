﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class DealerPartsTransferOrderQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvNoWarrantyBrand = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvWarrantyBrand = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvName = new[] {
            "DealerPartsTransferOrderStatus"
        };

        public DealerPartsTransferOrderQueryPanel() {
            Initializer.Register(Initialize);
            this.KeyValueManager.Register(this.kvName);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvNoWarrantyBrand.Clear();
                this.kvWarrantyBrand.Clear();
                foreach(var partsSalesCategory in loadOp.Entities) {
                    if(partsSalesCategory.IsNotWarranty) {
                        this.kvNoWarrantyBrand.Add(new KeyValuePair {
                            Key = partsSalesCategory.Id,
                            Value = partsSalesCategory.Name
                        });
                    } else {
                        this.kvWarrantyBrand.Add(new KeyValuePair {
                            Key = partsSalesCategory.Id,
                            Value = partsSalesCategory.Name
                        });
                    }

                }
            }, null);
            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_DealerPartsTransferOrder,
                    EntityType = typeof(DealerPartsTransferOrder),
                    QueryItems = new [] {
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_NoWarrantyBrandName,
                            ColumnName = "NoWarrantyBrandId",
                            KeyValueItems = this.kvNoWarrantyBrand
                        },
                        new QueryItem {
                            ColumnName = "Code"
                        },
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_WarrantyBrandName,
                            ColumnName = "WarrantyBrandId",
                            KeyValueItems = this.kvWarrantyBrand
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvName[0]]
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}