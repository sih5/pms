﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class EnterprisePartsCostQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvName = new[] {
            "ABCStrategy_Category"
        };
        public EnterprisePartsCostQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvName);
        }

        public void Initialize()
        { this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_EnterprisePartsCost,
                    EntityType = typeof(VirtualEnterprisePartsCost),
                    QueryItems = new[] {
                        new QueryItem {
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode,
                            ColumnName = "SparePartCode"
                        }, new CustomQueryItem {
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName,
                            ColumnName = "SparePartName",
                            DataType = typeof(string)
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreateTime
                          
                        },new DateTimeRangeQueryItem {
                            ColumnName = "MofidyTime",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime
                        },new CustomQueryItem {
                            ColumnName = "QuantityMoreThanZero",
                            DataType = typeof(bool),
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_GreaterThanZero,
                            DefaultValue = true
                        },new KeyValuesQueryItem {
                            ColumnName = "PartABC",
                            KeyValueItems = this.KeyValueManager[this.kvName[0]],
                            Title = PartsStockingUIStrings.QueryPanel_Title_PartABC
                        }
                    }
                }
            };
        }
    }
}
