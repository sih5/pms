﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class ExpressQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };
        public ExpressQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_Express,
                    EntityType = typeof(Express),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "ExpressCode",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressCode,
                            IsExact = false
                        },new QueryItem {
                            ColumnName = "ExpressName",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressName,
                            IsExact = false
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            DefaultValue = (int)DcsMasterDataStatus.有效,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        }
                    }
                }
            };
        }
    }
}
