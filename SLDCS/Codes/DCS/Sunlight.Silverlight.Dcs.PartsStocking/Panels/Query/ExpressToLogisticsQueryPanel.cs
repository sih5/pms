﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class ExpressToLogisticsQueryPanel : DcsQueryPanelBase {
        public ExpressToLogisticsQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
        private readonly string[] kvNames = {
            "MasterData_Status","Storage_Center"
        };
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_ExpressToLogistic,
                    EntityType = typeof(ExpressToLogistic),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "ExpressCode",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressCode,
                            IsExact = false
                        },new QueryItem {
                            ColumnName = "ExpressName",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressName,
                            IsExact = false
                        },new QueryItem {
                            ColumnName = "LogisticsCompanyCode",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_ExpressToLogistic_LogisticsCompanyCode,
                            IsExact = false
                        },new QueryItem {
                            ColumnName = "LogisticsCompanyName",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_ExpressToLogistic_LogisticsCompanyName,
                            IsExact = false
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsMasterDataStatus.有效
                        },new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_ExpressToLogistic_FocufingCoreName,
                            ColumnName = "FocufingCoreId",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }
                    }
                }
            };
        }
    }
}
