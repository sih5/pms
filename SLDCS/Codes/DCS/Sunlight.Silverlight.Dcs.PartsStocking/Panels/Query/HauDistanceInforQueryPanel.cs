﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class HauDistanceInforQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        public HauDistanceInforQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Name), LoadBehavior.RefreshCurrent,
                loadOp => {
                    if (loadOp.HasError)
                        return;
                    foreach (var entity in loadOp.Entities) {
                        this.kvWarehouses.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Name
                        });
                    }
                }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_HauDistanceInfor,
                    EntityType = typeof(HauDistanceInfor),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_Company_Code,
                            ColumnName = "Company.Code",
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_Company_Name,
                            ColumnName = "Company.Name",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_Company1_Code,
                            ColumnName = "Company1.Code",
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_Company1_Name,
                            ColumnName = "Company1.Name",
                            DataType = typeof(string)
                        },new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses,
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_HauDistanceInfor_Warehouse
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            DefaultValue = (int)DcsBaseDataStatus.有效,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",DefaultValue=new []{
                            DateTime.Now.Date.AddDays(1-DateTime.Now.Day), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)
                            }
                        }
                    }
                }};
        }
    }
}
