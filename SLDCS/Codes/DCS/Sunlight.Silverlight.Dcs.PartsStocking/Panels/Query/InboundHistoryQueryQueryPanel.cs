﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class InboundHistoryQueryQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvWarehouse = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvName = new[] {
            "Parts_InboundType","Parts_SettlementStatus","PartsPurchaseOrder_OrderType"
        };
        public InboundHistoryQueryQueryPanel() {
            Initializer.Register(Initialize);
            this.KeyValueManager.Register(this.kvName);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOpKey => {
                if(loadOpKey.HasError) {
                    if(!loadOpKey.IsErrorHandled)
                        loadOpKey.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpKey);
                }
                this.kvBranches.Clear();
                foreach(var branch in loadOpKey.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Code
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.kvPartsSalesCategory.Clear();
                foreach(var warehouse in loadOp2.Entities) {
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesWithBranchIdsQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.kvWarehouse.Clear();
                foreach(var item in loadOp2.Entities) {
                    this.kvWarehouse.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
                }
            }, null);

            dcsDomainContext.Load(dcsDomainContext.GetCompanyWithBranchIdsQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var company = loadOp.Entities.SingleOrDefault();
                QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_InboundHistoryQuery,
                        EntityType = typeof(VirtualInboundHistoryQuery),
                        QueryItems = new QueryItem[] {
                               new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_BranchName,
                                ColumnName = "BranchId",
                                KeyValueItems = kvBranches,
                                DefaultValue = company != null && company.Type == (int)DcsCompanyType.分公司 ? company.Id : 0,
                                IsEnabled = BaseApp.Current.CurrentUserData.UserCode == "ReportAdmin"
                            },  new QueryItem {
                                Title =  PartsStockingUIStrings.QueryPanel_QueryItem_InboundBill_Code,
                                ColumnName = "PartsInboundOrderCode"
                            },  new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_InboundType,
                                ColumnName = "InboundType",
                               KeyValueItems = this.KeyValueManager[this.kvName[0]]
                            },new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_PartsSalesCategoryName,
                                ColumnName = "PartsSalesCategoryId",
                                KeyValueItems = this.kvPartsSalesCategory
                            },  new KeyValuesQueryItem {
                               Title = PartsStockingUIStrings.QueryPanel_QueryItem_PurchaseOrderType,
                                ColumnName = "PartsPurchaseOrderTypeId",
                                 KeyValueItems = this.KeyValueManager[this.kvName[2]]
                            },  new QueryItem {
                               Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_SourceCode,
                                ColumnName = "OriginalRequirementBillCode",
                            }, new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OutboundAndInboundBill_Warehouse,
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouse
                            }, new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_Title_SettlementStatus,
                                ColumnName = "Settlementstatus",
                                 KeyValueItems = this.KeyValueManager[this.kvName[1]]
                            },  new QueryItem {
                                Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                                ColumnName = "PartCode",
                            }, new QueryItem {
                                Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName,
                                ColumnName = "PartName"
                            }, new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_CompanyName,
                                ColumnName = "CompanyName"
                            } , new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                Title=PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CreateTime,
                                 DefaultValue = new[] {
                                    DateTime.Now.AddMonths(-1), DateTime.Now
                                }
                            
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
