﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class LogisticsQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames ={
                                               "PartsShipping_Method"
                                           };
        public ObservableCollection<KeyValuePair> kvSignStatus = new ObservableCollection<KeyValuePair>();
        public LogisticsQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
            this.Loaded += LogisticsQueryPanel_Loaded;
        }

        void LogisticsQueryPanel_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            kvSignStatus.Clear();
            kvSignStatus.Add(new KeyValuePair {
                Key = 1,
                Value = PartsStockingUIStrings.QueryPanel_QueryItem_InLine
            });
            kvSignStatus.Add(new KeyValuePair {
                Key = 2,
                Value = PartsStockingUIStrings.QueryPanel_QueryItem_Received
            });

        }

        private void Initialize() {
            this.QueryItemGroups = new[]{
               new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(Logistic),
                    Title = PartsStockingUIStrings.QueryPanel_QueryItem_LogisticsDetail,
                    QueryItems = new[] {
                         new QueryItem {
                            ColumnName = "OrderCode",
                            Title =PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_Code
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ShippingDate",
                           Title=PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingDate
                        },new QueryItem {
                            ColumnName = "ShippingCode",
                            Title =PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_Code
                        },new KeyValuesQueryItem {
                            ColumnName = "SignStatus",
                            Title =PartsStockingUIStrings.QueryPanel_QueryItem_SignStatus,
                            KeyValueItems = this.kvSignStatus
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                           Title=PartsStockingUIStrings.QueryPanel_QueryItem_ApproveTime
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "RequestedArrivalDate",
                           Title=PartsStockingUIStrings.DataEditPanel_Text_Logistics_RequestedArrivalDate
                        },new QueryItem {
                            ColumnName = "WarehouseName",
                            Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_WarehouseName
                        },new QueryItem {
                            ColumnName = "LogisticName",
                            Title =PartsStockingUIStrings.DataEditPanel_Text_Logistics_LogisticName
                        }
                    }
               }
            };
        }

    }
}
