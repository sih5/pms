﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class OutboundAndInboundBillQueryPanel : DcsQueryPanelBase {

        private ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        public OutboundAndInboundBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_CompanyPartsStock,
                    EntityType = typeof(OutboundAndInboundBill),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                           ColumnName = "WarehouseName",
                           KeyValueItems = this.kvWarehouses,
                           Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OutboundAndInboundBill_Warehouse
                       },new CustomQueryItem {
                            ColumnName ="EndTime",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OutboundAndInboundBill_EndTime,
                            DataType = typeof(DateTime),
                            DefaultValue =  DateTime.Now
                        }
                    }
                }
            };
        }
    }
}