﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class OverstockPartsAdjustBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "Company_Type", "DealerPartsTransferOrderStatus"
        };

        private readonly ObservableCollection<KeyValuePair> kvCompanyTypes = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvBranchs = new ObservableCollection<KeyValuePair>();

        public OverstockPartsAdjustBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_OverstockPartsAdjustBill,
                    EntityType = typeof(OverstockTransferOrder),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            //DefaultValue = (int)DCSOverstockPartsTransferOrderStatus.新建
                        }, new QueryItem {
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueeyPanel_OverstockTransferOrder_Title_Code
                        }, new QueryItem {
                            ColumnName = "TransferInCorpName",
                            Title = PartsStockingUIStrings.QueeyPanel_OverstockTransferOrder_Title_TransferInCorpName
                        }, new QueryItem {
                            ColumnName = "TransferInWarehouseCode",
                            Title = PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferInWarehouseCode
                        }, new QueryItem {
                            ColumnName = "TransferOutCorpName",
                            Title = PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferOutCorpName
                        }, new QueryItem {
                            ColumnName = "TransferOutWarehouseName",
                            Title = PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferOutWarehouseName
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}