﻿

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class OverstockPartsAppForBranchQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvCompanyTypes;
        private readonly string[] kvNames = {
            "Company_Type", "OverstockPartsApp_Status"
        };

        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        public ObservableCollection<KeyValuePair> KvCompanyTypes {
            get {
                return kvCompanyTypes ?? (this.kvCompanyTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategory in loadOp.Entities)
            //        this.kvPartsSalesCategoryName.Add(new KeyValuePair {
            //            Key = partsSalesCategory.Id,
            //            Value = partsSalesCategory.Name
            //        });
            //}, null);
            this.KeyValueManager.LoadData(() => {
                this.KvCompanyTypes.Clear();
                foreach(var kvItem in this.KeyValueManager[this.kvNames[0]].Where(e => e.Key == (int)DcsCompanyType.代理库 || e.Key == (int)DcsCompanyType.服务站))
                    this.KvCompanyTypes.Add(kvItem);
            });
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(OverstockPartsApp),
                    Title = PartsStockingUIStrings.QueryPanel_Title_OverstockPartsAppForBranch,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "StorageCompanyCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsAppForBranch_StorageCompanyCode,
                            IsExact = false
                        }, new QueryItem {
                            ColumnName = "StorageCompanyName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsAppForBranch_StorageCompanyName,
                            IsExact = false
                        }, new KeyValuesQueryItem {
                            ColumnName = "StorageCompanyType",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsAppForBranch_StorageCompanyType,
                            KeyValueItems = this.KvCompanyTypes
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new QueryItem {
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsAppForBranch_Code,                        
                            IsExact = false
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                DateTime.Now.AddDays(1 - DateTime.Now.Day), DateTime.Now
                            }
                        }
                        //, new KeyValuesQueryItem {
                        //    ColumnName = "PartsSalesCategoryId",
                        //    KeyValueItems = this.kvPartsSalesCategoryName,
                        //    Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_PartsSalesCategoryName
                        //}
                    }
                }
            };
        }

        public OverstockPartsAppForBranchQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}