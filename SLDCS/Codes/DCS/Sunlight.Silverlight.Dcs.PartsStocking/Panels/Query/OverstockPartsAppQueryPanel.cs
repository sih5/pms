﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class OverstockPartsAppQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "OverstockPartsApp_Status"
        };

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchByCustomerInformationQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                    return;
                foreach (var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if (loadOp.HasError)
                    return;
                foreach (var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_OverstockPartsApp,
                    EntityType = typeof(OverstockPartsApp),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_OverstockPartsApp_BranchName,
                            ColumnName = "BranchId",
                            KeyValueItems = this.kvBranches
                        }, new QueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_OverstockPartsApp_Code,
                            ColumnName = "Code"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new [] {
                               DateTime.Now.Date.AddDays(1-DateTime.Now.Day), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59)
                            }
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategoryName,
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_PartsSalesCategoryName
                        }
                    }
                }
            };
        }

        public OverstockPartsAppQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
