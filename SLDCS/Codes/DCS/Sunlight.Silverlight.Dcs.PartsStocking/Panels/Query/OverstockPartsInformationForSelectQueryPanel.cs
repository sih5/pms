﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class OverstockPartsInformationForSelectQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        public OverstockPartsInformationForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_OverstockPartsStock,
                    EntityType = typeof(OverstockPartsStock),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode"
                        }, new QueryItem {
                            ColumnName = "SparePartName"
                        }, new QueryItem {
                            ColumnName = "StorageCompanyCode"
                        }, new QueryItem {
                            ColumnName = "StorageCompanyName"
                        }, new QueryItem {
                            ColumnName = "ProvinceName"
                        }, new KeyValuesQueryItem {
                             Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_BranchId,
                            ColumnName = "BranchId",
                            KeyValueItems = this.kvBranches,
                            IsExact = false
                        }, new KeyValuesQueryItem {
                             ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategoryName,
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_PartsSalesCategoryName,
                            IsExact = false
                        }, new QueryItem {
                            ColumnName = "CityName"
                        }
                        , new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false
                        }
                        , new QueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_Company_CityName,
                            ColumnName = "CountyName"
                        }
                    }
                }
            };
        }
    }
}
