﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class OverstockPartsInformationQueryPanel : DcsQueryPanelBase {
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        public OverstockPartsInformationQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            //var dcsDomainContext = new DcsDomainContext();
            //dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategory in loadOp.Entities)
            //        this.kvPartsSalesCategoryName.Add(new KeyValuePair {
            //            Key = partsSalesCategory.Id,
            //            Value = partsSalesCategory.Name
            //        });
            //}, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_OverstockPartsInformation,
                    EntityType = typeof(OverstockPartsStock),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_SparePartCode
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_SparePartName
                        }, new QueryItem {
                            ColumnName = "StorageCompanyCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_StorageCompanyCode
                        }, new QueryItem {
                            ColumnName = "StorageCompanyName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_StorageCompanyName
                        }
                        //, new KeyValuesQueryItem{
                        //    ColumnName="PartsSalesCategoryId",
                        //    Title=PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_PartsSalesCategoryName,
                        //    KeyValueItems=this.kvPartsSalesCategoryName
                        //}
                        , new QueryItem {
                            ColumnName = "ProvinceName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_ProvinceName
                        }, new QueryItem {
                            ColumnName = "CityName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CityName
                        }, new QueryItem {
                            ColumnName = "CountyName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CountyName
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CreateTime
                        }
                    }
                }
            };
        }
    }
}
