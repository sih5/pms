﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class OverstockPartsPlatFormBillQueryPanel : DcsQueryPanelBase {
        public OverstockPartsPlatFormBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_OverstockPartsPlatFormBill,
                    EntityType = typeof(VirtualOverstockPartsPlatFormBill),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "CenterName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyName
                        }, new QueryItem {
                            ColumnName = "DealerName",
                            Title = PartsStockingUIStrings.DataGridView_Column_ServiceName
                        },new CustomControlQueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_SparePartCode,
                            CreateControl=type =>new Controls.MultipleTextQueryControl(type,"SparePartCode") 
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_SparePartName
                        }
                    }
                }
            };
        }
    }
}
