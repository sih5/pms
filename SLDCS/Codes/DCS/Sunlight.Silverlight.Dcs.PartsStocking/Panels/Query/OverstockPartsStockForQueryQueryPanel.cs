﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class OverstockPartsStockForQueryQueryPanel : DcsQueryPanelBase {
        public OverstockPartsStockForQueryQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        //private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        public void Initialize() {
            var domainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    foreach(var partsSalesCategory in loadOp.Entities)
            //        this.kvPartsSalesCategoryName.Add(new KeyValuePair {
            //            Key = partsSalesCategory.Id,
            //            Value = partsSalesCategory.Name
            //        });
            //}, null);
            //domainContext.Load(domainContext.GetCustomerInformationsQuery().Where(entity => entity.CustomerCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadCustomer => {
            //    if(loadCustomer.HasError)
            //        return;
            //    var salesCompanyIds = loadCustomer.Entities.Select(customer => customer.SalesCompanyId).ToArray();
            //    domainContext.Load(domainContext.GetBranchesByIdsQuery(salesCompanyIds), LoadBehavior.RefreshCurrent, loadOp => {
            //        foreach(var entity in loadOp.Entities) {
            //            this.kvBranches.Add(new KeyValuePair {
            //                Key = entity.Id,
            //                Value = entity.Name,
            //                UserObject = entity
            //            });
            //        }

            //    }, null);
            //}, null);
            domainContext.Load(domainContext.GetBranchByCustomerInformationQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                        new QueryItemGroup {
                            UniqueId = "Common",
                            Title = PartsStockingUIStrings.QueryPanel_Title_OverstockPartsStock,
                            EntityType = typeof(OverstockPartsStock),
                            QueryItems = new[] {
                                new QueryItem {
                                    ColumnName = "SparePartCode"
                                }, new QueryItem {
                                    ColumnName = "SparePartName"
                                }, new QueryItem {
                                    ColumnName = "StorageCompanyCode"
                                }, new QueryItem {
                                    ColumnName = "StorageCompanyName"
                                }, new KeyValuesQueryItem {
                                    Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStockQuery_BranchName,
                                    ColumnName = "BranchId",
                                    KeyValueItems = this.kvBranches
                                }, new QueryItem {
                                    ColumnName = "ProvinceName"
                                }
                                //, new KeyValuesQueryItem {
                                //    Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_PartsSalesCategoryName,
                                //    ColumnName = "PartsSalesCategoryId",
                                //    KeyValueItems = this.kvPartsSalesCategoryName
                                //}
                                , new QueryItem {
                                    ColumnName = "CityName"
                                }, new QueryItem {
                                    Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStockQuery_CountyName,
                                    ColumnName = "CountyName"
                                }, new DateTimeRangeQueryItem {
                                    ColumnName = "CreateTime",
                                    IsExact = false
                                }
                            }
                        }
                    };
        }
    }
}


