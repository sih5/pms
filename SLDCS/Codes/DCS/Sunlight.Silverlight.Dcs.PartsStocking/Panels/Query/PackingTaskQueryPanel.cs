﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query
{
    public class PackingTaskQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = {
           "PackingTaskStatus","Parts_InboundType"
        };
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        public PackingTaskQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
      
        public void Initialize()
        {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair
                    {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });
            }, null);
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_PackingTask,
                        EntityType = typeof(PackingTask),
                        QueryItems = new[] {
                            new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_PackingTaskCode,
                                ColumnName = "Code"
                            }, new QueryItem {
                                ColumnName = "ReferenceCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_ReferenceCode                            
                            }, new QueryItem {
                                ColumnName = "PartsInboundPlanCode",
                                 Title = PartsStockingUIStrings.QueryPanel_QueryItem_PartsInboundPlanCode,
                            }, new QueryItem {
                                ColumnName = "PartsInboundCheckBillCode",
                                Title =  PartsStockingUIStrings.QueryPanel_QueryItem_PartsInboundCheckBillCode
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]                           
                            }, new KeyValuesQueryItem {
                                Title=PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsOutboundBill_WarehouseName,
                                ColumnName = "WarehouseId",
                                 KeyValueItems = this.kvWarehouses
                            }
                            ,new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartCode,
                                ColumnName = "SparePartCode"
                            }, new QueryItem {
                                ColumnName = "SparePartName",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartName                             
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                DateTime.Now.AddDays(1 - DateTime.Now.Day).Date, DateTime.Now.Date
                                }
                            },new DateTimeRangeQueryItem {
                                ColumnName = "ModifyTime",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_PackingFinishTime
                            }, new KeyValuesQueryItem {
                                Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_InboundType,
                                ColumnName = "InboundType",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]]                           
                            }, new QueryItem {
                                Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName,
                                ColumnName = "ModifierName"                          
                            },new CustomQueryItem {
                                ColumnName = "HasDifference",
                                DataType = typeof(bool),
                                Title = PartsStockingUIStrings.QueryPanel_Title_HasDifference
                            }
                        }
                    }
                };
        }
    }
}