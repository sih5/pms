﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsDifferenceBackBillDtlPartsQueryPanel : DcsQueryPanelBase {
        public PartsDifferenceBackBillDtlPartsQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsDifferenceBackBillDtl,
                    EntityType = typeof(VirtualPartsDifferenceBackBillDtl),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartCode
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartName
                        },new QueryItem {
                            ColumnName = "SourceCode",
                            Title = PartsStockingUIStrings.QueryPanel_Title_SourceCode
                        }
                    }
                }
            };
        }
    }
}
