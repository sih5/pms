﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsDifferenceBackBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames ={
                                               "PartsDifferenceBackBillStatus","PartsDifferenceBackBillType"
                                           };
        public PartsDifferenceBackBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsDifferenceBackBill,
                    EntityType = typeof(PartsDifferenceBackBill),
                     QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "Code",
                             DataType = typeof(string),
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsDifferenceBackBill_Code
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems =  this.KeyValueManager[this.kvNames[0]],
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_Status
                        },
                         new KeyValuesQueryItem {
                            ColumnName = "Type",
                            KeyValueItems =  this.KeyValueManager[this.kvNames[1]],
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBill_Type
                        },new CustomQueryItem {
                            ColumnName = "TaskCode",
                            DataType = typeof(string),
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_TaskCode
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                DateTime.Now.AddDays(1 - DateTime.Now.Day).Date, DateTime.Now.Date
                                }
                        },new DateTimeRangeQueryItem {
                            ColumnName = "SubmitTime",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsDifferenceBackBill_SubmitTime
                        }
                    }
                }
            };
        }
    }
}
