﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsDifferenceBackBillSourceCodeQueryPanel : DcsQueryPanelBase {
        public PartsDifferenceBackBillSourceCodeQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId = "Common",
                    EntityType = typeof(VirtualPartsDifferenceBackBillSourceCode),
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsDifferenceBackBill_SourceCode,
                    QueryItems = new[]{
                        new QueryItem{
                            ColumnName = "SourceCode",
                            Title = PartsStockingUIStrings.QueryPanel_Title_SourceCode
                        },
                        new QueryItem{
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_BoxUpTaskOrPicking_TaskCode,
                        },
                        // new DateTimeRangeQueryItem{
                        //    ColumnName = "CreateTime",
                        //    Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime,
                        //    IsEnabled=false,
                        //    DefaultValue = new[] {
                        //        DateTime.Now.AddDays(1 - DateTime.Now.Day).Date, DateTime.Now.Date
                        //        }
                                
                        //}
                    }
                }
            };
        }
    }
}
