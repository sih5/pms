﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsInboundCheckBillForSupplierQueryPanel : DcsQueryPanelBase {

        public PartsInboundCheckBillForSupplierQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        public void Initialize() {
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsInboundCheckBill,
                        EntityType = typeof(PartsInboundCheckBill),
                        QueryItems = new[] {
                            new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBill_Code,
                                ColumnName = "Code"
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    DateTime.Now.AddDays(1 - DateTime.Now.Day).Date, DateTime.Now.Date
                                }
                            },  new QueryItem {
                                ColumnName = "CounterpartCompanyCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_SupplierCode,
                                DefaultValue = BaseApp.Current.CurrentUserData.EnterpriseCode,
                                IsEnabled = false
                            }, new QueryItem {
                                ColumnName = "CounterpartCompanyName",
                                 Title = PartsStockingUIStrings.QueryPanel_QueryItem_SupplierName,
                                DefaultValue = BaseApp.Current.CurrentUserData.EnterpriseName,
                                IsEnabled = false
                            }, new QueryItem {
                                ColumnName = "CPPartsPurchaseOrderCode",
                                Title =  PartsStockingUIStrings.QueryPanel_QueryItem_CPPartsPurchaseOrderCode
                            }, new QueryItem {
                                ColumnName = "CPPartsInboundCheckCode",
                                Title =  PartsStockingUIStrings.QueryPanel_QueryItem_CPPartsInboundCheckCode
                            }
                        }
                    }
                };
        }
    }
}
