﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsInboundCheckBillQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "Parts_InboundType","Parts_SettlementStatus"
        };

        public PartsInboundCheckBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            var a = new PartsInboundCheckBill();
            domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                foreach(var entity in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                    foreach(var item in loadOp1.Entities) {
                        this.kvPartsSalesCategorys.Add(new KeyValuePair {
                            Key = item.Id,
                            Value = item.Name
                        });
                    }
                    this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsInboundCheckBill,
                        EntityType = typeof(PartsInboundCheckBill),
                        QueryItems = new[] {
                            new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBill_Code,
                                ColumnName = "Code"
                            }, new CustomQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBill_PartsInboundPlanCode,
                                ColumnName = "PartsInboundPlan.Code",
                                 DataType = typeof(string)
                            },new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategory,
                                ColumnName = "PartsSalesCategoryId",
                                KeyValueItems = this.kvPartsSalesCategorys
                            },new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBill_WarehouseName,
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses
                            },new KeyValuesQueryItem {
                                ColumnName = "InboundType",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            },new KeyValuesQueryItem {
                                ColumnName = "SettlementStatus",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                            }, new CustomQueryItem{
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ERPOrderCode,
                                ColumnName = "ERPSourceOrderCode",
                                DataType = typeof(string)
                            },new CustomQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_Title_SAPPurchasePlanCode,
                                ColumnName = "SAPPurchasePlanCode",
                                DataType = typeof(string)
                            }, new CustomQueryItem{
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsAdjustBill_SourceCode,
                                ColumnName = "PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.Code",
                                DataType = typeof(string)
                            },new QueryItem {
                                ColumnName = "CounterpartCompanyCode"
                            },new QueryItem {
                                ColumnName = "CounterpartCompanyName"
                            },new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    DateTime.Now.Date, DateTime.Now.Date
                                }
                           },new QueryItem {
                               ColumnName="OriginalRequirementBillCode",
                               Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_OriginalRequirementBillCode
                           }
                        }
                    }
                };
                }, null);
            }, null);
        }
    }
}
