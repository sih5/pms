﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsInboundCheckBillReceiptQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "Parts_InboundType","PartsInboundPlan_Status"
        };

        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public PartsInboundCheckBillReceiptQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesWithBranchIdsQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && (e.Type == (int)DcsWarehouseType.总库 || e.Type == (int)DcsWarehouseType.分库)).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                foreach (var entity in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);
            //dcsDomainContext.Load(dcsDomainContext.GetCompanyWithBranchIdsQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if (loadOp.HasError)
            //        return;
            //    var company = loadOp.Entities.SingleOrDefault();
            //    if (company == null)
            //        return;
            //    if (company.Type == (int)DcsCompanyType.代理库) {


            //        dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
            //            if (loadOp1.HasError)
            //                return;
            //            foreach (var partsSalesCategory in loadOp1.Entities)
            //                this.kvPartsSalesCategorys.Add(new KeyValuePair {
            //                    Key = partsSalesCategory.Id,
            //                    Value = partsSalesCategory.Name
            //                });
            //        }, null);
            //    } else {
            //        dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesWithBranchIdQuery(), LoadBehavior.RefreshCurrent, loadOp1 => {
            //            if (loadOp1.HasError)
            //                return;
            //            foreach (var partsSalesCategory in loadOp1.Entities)
            //                this.kvPartsSalesCategorys.Add(new KeyValuePair {
            //                    Key = partsSalesCategory.Id,
            //                    Value = partsSalesCategory.Name
            //                });
            //        }, null);
            //    }
            //}, null);
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsInboundPlan,
                        EntityType = typeof(PartsInboundPlan),
                        QueryItems = new[] {
                            new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundPlan_Code,
                                ColumnName = "Code"
                            }
                            //,new KeyValuesQueryItem {
                            //    Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategory,
                            //    ColumnName = "PartsSalesCategoryId",
                            //    KeyValueItems = this.kvPartsSalesCategorys
                            //}
                            , new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundPlan_WarehouseName,
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses
                            }
                            , new QueryItem {
                                ColumnName = "SparePartCode",
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartCode
                            }
                            , new QueryItem {
                                ColumnName = "SparePartName",
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName
                            }
                            , new KeyValuesQueryItem {
                                ColumnName = "InboundType",
                                KeyValueItems = KeyValueManager[this.kvNames[0]]
                            },new KeyValuesQueryItem{
                                ColumnName="Status",
                                Title=PartsStockingUIStrings.QueryPanel_QueryItem_CheckStatus,
                                KeyValueItems=KeyValueManager[this.kvNames[1]]
                            }
                            //, new QueryItem {
                            //    ColumnName = "CounterpartCompanyCode"
                            //}
                            , new QueryItem {
                                ColumnName = "SourceCode"
                            }, new QueryItem {
                                ColumnName = "OriginalRequirementBillCode"
                            }, new QueryItem {
                                ColumnName = "CounterpartCompanyCode"
                            }, new QueryItem {
                                ColumnName = "CounterpartCompanyName"
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "PlanDeliveryTime",
                                //DefaultValue = new[] {
                                //    DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date}
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date}
                            },
                        }
                    }
                };

        }
    }
}
