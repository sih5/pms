﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsInboundCheckBillWithQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly PartsInboundCheckBillWithQueryPanelViewModel viewModel = new PartsInboundCheckBillWithQueryPanelViewModel();

        private readonly string[] kvNames = {
            "Parts_InboundType", "Parts_SettlementStatus", "ERPInvoiceInformation_Type"
        };

        public PartsInboundCheckBillWithQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && (e.Type == (int)DcsWarehouseType.总库 || e.Type == (int)DcsWarehouseType.分库)).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
            domainContext.Load(domainContext.GetWarehousesbyPersonnelIdQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                foreach(var entity in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsInboundCheckBill,
                        EntityType = typeof(PartsInboundCheckBill),
                        QueryItems = new[] {
                            new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBill_Code,
                                ColumnName = "Code"
                            },
                            new CustomQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBill_PartsInboundPlanCode,
                                ColumnName = "PartsInboundPlan.Code",
                                DataType = typeof(string)
                            },
                            new ComboQueryItem {
                                ColumnName = "PartsSalesCategoryId",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyTransferOrder_PartsSalesCategoryName,
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name",
                                ItemsSource = this.viewModel.KvPartsSalesCategorys,
                                SelectedItemBinding = new Binding("SelectedPartsSalesCategory") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }
                            },
                            new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBill_WarehouseName,
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "InboundType",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "SettlementStatus",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                            },
                            new CustomQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ERPOrderCode,
                                ColumnName = "PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.ERPSourceOrderCode",
                                DataType = typeof(string)
                            },
                            new CustomQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsAdjustBill_SourceCode,
                                ColumnName = "PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.Code",
                                DataType = typeof(string)
                            },
                            new QueryItem {
                                ColumnName = "CounterpartCompanyCode"
                            },
                            new QueryItem {
                                ColumnName = "CounterpartCompanyName"
                            },
                            new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    DateTime.Now.Date, DateTime.Now.Date
                                }
                            },
                            new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_InvoiceType,
                                ColumnName = "PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.InvoiceType",
                                KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                            },
                            new QueryItem {
                                ColumnName = "OriginalRequirementBillCode",
                                Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_OriginalRequirementBillCode

                            },
                            new ComboQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_PurchaseOrderType,
                                ColumnName = "PartsPurchaseOrderTypeId",
                                ItemsSource = this.viewModel.PartsPurchaseOrderTypes,
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name"
                            }
                        }
                    }
                };
            }, null);
        }


        public class PartsInboundCheckBillWithQueryPanelViewModel : ViewModelBase {

            public ObservableCollection<PartsSalesCategory> kvPartsSalesCategorys;
            private PartsSalesCategory partsSalesCategory;
            private PagedCollectionView partsPurchaseOrderTypes;

            public readonly ObservableCollection<PartsPurchaseOrderType> allPartsPurchaseOrderTypes = new ObservableCollection<PartsPurchaseOrderType>();

            public ObservableCollection<PartsSalesCategory> KvPartsSalesCategorys {
                get {
                    if(this.kvPartsSalesCategorys == null)
                        this.kvPartsSalesCategorys = new ObservableCollection<PartsSalesCategory>();
                    return kvPartsSalesCategorys;
                }
            }

            public PartsSalesCategory SelectedPartsSalesCategory {
                get {
                    return this.partsSalesCategory;
                }
                set {
                    if(this.partsSalesCategory == value)
                        return;
                    this.partsSalesCategory = value;
                    this.NotifyOfPropertyChange("PartsSalesCategory");
                    this.PartsPurchaseOrderTypes.Refresh();
                }
            }

            public PagedCollectionView PartsPurchaseOrderTypes {
                get {
                    if(this.partsPurchaseOrderTypes == null) {
                        this.partsPurchaseOrderTypes = new PagedCollectionView(this.allPartsPurchaseOrderTypes);
                        this.partsPurchaseOrderTypes.Filter = o => ((PartsPurchaseOrderType)o).PartsSalesCategoryId == (this.SelectedPartsSalesCategory != null ? this.SelectedPartsSalesCategory.Id : int.MinValue);
                    }
                    return this.partsPurchaseOrderTypes;
                }
            }

            public override void Validate() {
                //
            }

            public void Initialize() {
                var domainContext = new DcsDomainContext();
                domainContext.Load(domainContext.GetPartsPurchaseOrderTypesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var partsPurchaseOrderTypes in loadOp.Entities)
                        this.allPartsPurchaseOrderTypes.Add(partsPurchaseOrderTypes);
                }, null);
                domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    this.KvPartsSalesCategorys.Clear();
                    foreach(var partsSalesCategories in loadOp.Entities)
                        this.KvPartsSalesCategorys.Add(partsSalesCategories);
                }, null);
            }
        }
    }
}