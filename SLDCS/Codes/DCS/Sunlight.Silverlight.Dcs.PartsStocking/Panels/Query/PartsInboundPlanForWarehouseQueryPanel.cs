﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query
{
    public class PartsInboundPlanForWarehouseQueryPanel: DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "Parts_InboundType", "PartsInboundPlan_Status", "PartsPurchaseOrder_OrderType"
        };

        private readonly PartsInboundPlanQueryQueryPanelViewModel viewModel = new PartsInboundPlanQueryQueryPanelViewModel();
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public PartsInboundPlanForWarehouseQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.viewModel.Initialize);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            //var domainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetWarehousesbyPersonnelIdQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
            //    foreach(var entity in loadOp.Entities) {
            //        this.kvWarehouses.Add(new KeyValuePair {
            //            Key = entity.Id,
            //            Value = entity.Name
            //        });
            //    }
            //}, null);

                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsInboundPlan,
                        EntityType = typeof(PartsInboundPlan),
                        QueryItems = new[] {
                            new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundPlan_Code,
                                ColumnName = "Code"
                            },
                            new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundPlan_WarehouseName,
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses
                            },                           
                             new QueryItem {
                                ColumnName = "SparePartCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartCode
                            },
                            new QueryItem {
                                ColumnName = "SparePartName",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartName
                            },
                            new QueryItem {
                                ColumnName = "CounterpartCompanyName",
                                Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyName
                            },
                            new CustomQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_OriginalRequirementBillCode,
                                ColumnName = "OriginalRequirementBillCode",
                                DataType = typeof(string)
                            },
                            new CustomQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_Code,
                                ColumnName = "PartsSalesOrderCode",
                                DataType = typeof(string)
                            },
                            new QueryItem {
                                ColumnName = "SourceCode",
                                Title=PartsStockingUIStrings.QueryPanel_Title_SourceCode
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "InboundType",
                                KeyValueItems = KeyValueManager[this.kvNames[0]],
                                Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_InboundType
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "Status",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_CheckStatus,
                                KeyValueItems = KeyValueManager[this.kvNames[1]]
                            },
                            new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    DateTime.Now.Date, DateTime.Now.Date
                                },
                                Title=PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CreateTime
                            },new DateTimeRangeQueryItem {
                                ColumnName = "PlanDeliveryTime",
                                Title=PartsStockingUIStrings.DataEditPanel_Text_ExpectedPlaceDate,
                            }
                        }
                    }
                };
        }
    }
}