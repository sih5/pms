﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsInboundPlanNoPriceQueryQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "Parts_InboundType", "PartsInboundPlan_Status", "PartsPurchaseOrder_OrderType"
        };

        private readonly PartsInboundPlanQueryQueryPanelViewModel viewModel = new PartsInboundPlanQueryQueryPanelViewModel();
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public PartsInboundPlanNoPriceQueryQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.viewModel.Initialize);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
             domainContext.Load(domainContext.GetWarehousesbyPersonnelIdQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.Type != (int)DcsWarehouseType.虚拟库).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                foreach(var entity in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsInboundPlan,
                        EntityType = typeof(PartsInboundPlan),
                        QueryItems = new[] {
                            new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundPlan_Code,
                                ColumnName = "Code"
                            },
                            new ComboQueryItem {
                                ColumnName = "PartsSalesCategoryId",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyTransferOrder_PartsSalesCategoryName,
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name",
                                ItemsSource = this.viewModel.KvPartsSalesCategorys,
                                SelectedItemBinding = new Binding("SelectedPartsSalesCategory") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }
                            },
                            new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundPlan_WarehouseName,
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "InboundType",
                                KeyValueItems = KeyValueManager[this.kvNames[0]]
                            },
                            new QueryItem {
                                ColumnName = "CounterpartCompanyCode"
                            },
                            new QueryItem {
                                ColumnName = "CounterpartCompanyName"
                            },
                            new CustomQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ERPOrderCode,
                                ColumnName = "PartsSalesReturnBill.PartsSalesOrder.ERPSourceOrderCode",
                                DataType = typeof(string)
                            },
                            new CustomQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsAdjustDetail_SourceCode,
                                ColumnName = "PartsSalesReturnBill.PartsSalesOrder.Code",
                                DataType = typeof(string)
                            },
                            new QueryItem {
                                ColumnName = "SourceCode"
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "Status",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_CheckStatus,
                                KeyValueItems = KeyValueManager[this.kvNames[1]]
                            },
                            new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    DateTime.Now.Date, DateTime.Now.Date
                                }
                            },
                            new QueryItem {
                                ColumnName = "CreatorName"
                            },
                            new ComboQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_PurchaseOrderType,
                                ColumnName = "PartsPurchaseOrderTypeId",
                                ItemsSource = this.viewModel.PartsPurchaseOrderTypes,
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name"
                            }
                        }
                    }
                };
            }, null);
        }
    }

 
}