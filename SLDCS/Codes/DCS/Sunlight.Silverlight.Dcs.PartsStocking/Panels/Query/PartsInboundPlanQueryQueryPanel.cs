﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsInboundPlanQueryQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "Parts_InboundType", "PartsInboundPlan_Status", "PartsPurchaseOrder_OrderType"
        };

        private readonly PartsInboundPlanQueryQueryPanelViewModel viewModel = new PartsInboundPlanQueryQueryPanelViewModel();
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public PartsInboundPlanQueryQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.viewModel.Initialize);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && (e.Type == (int)DcsWarehouseType.总库 || e.Type == (int)DcsWarehouseType.分库)).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
            //domainContext.Load(domainContext.GetWarehousesbyPersonnelIdQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
            //    foreach(var entity in loadOp.Entities) {
            //        this.kvWarehouses.Add(new KeyValuePair {
            //            Key = entity.Id,
            //            Value = entity.Name
            //        });
            //    }
            //}, null);

                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsInboundPlan,
                        EntityType = typeof(PartsInboundPlan),
                        QueryItems = new[] {
                            new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundPlan_Code,
                                ColumnName = "Code"
                            },
                            //new KeyValuesQueryItem {
                            //    Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundPlan_WarehouseName,
                            //    ColumnName = "WarehouseId",
                            //    KeyValueItems = this.kvWarehouses
                            //},
                            
                             new QueryItem {
                                ColumnName = "SparePartCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartCode
                            },
                            new QueryItem {
                                ColumnName = "SparePartName",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartName
                            },
                            new QueryItem {
                                ColumnName = "CounterpartCompanyName"
                            },
                            new CustomQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_OriginalRequirementBillCode,
                                ColumnName = "OriginalRequirementBillCode",
                                DataType = typeof(string)
                            },
                            new QueryItem {
                                ColumnName = "SourceCode"
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "InboundType",
                                KeyValueItems = KeyValueManager[this.kvNames[0]]
                            },
                            new KeyValuesQueryItem {
                                ColumnName = "Status",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_CheckStatus,
                                KeyValueItems = KeyValueManager[this.kvNames[1]]
                            },
                            new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    DateTime.Now.Date, DateTime.Now.Date
                                }
                            },new DateTimeRangeQueryItem {
                                ColumnName = "PlanDeliveryTime",
                                Title=PartsStockingUIStrings.DataEditPanel_Text_ExpectedPlaceDate,
                            },new CustomQueryItem { 
                                ColumnName = "HasDifference", 
                                DataType = typeof(bool), 
                                Title = PartsStockingUIStrings.QueryPanel_Title_HasDifference 
                            } 
                        }
                    }
                };
        }
    }

    public class PartsInboundPlanQueryQueryPanelViewModel : ViewModelBase {

        public ObservableCollection<PartsSalesCategory> kvPartsSalesCategorys;
        private PartsSalesCategory partsSalesCategory;
        private PagedCollectionView partsPurchaseOrderTypes;

        public readonly ObservableCollection<PartsPurchaseOrderType> allPartsPurchaseOrderTypes = new ObservableCollection<PartsPurchaseOrderType>();

        public ObservableCollection<PartsSalesCategory> KvPartsSalesCategorys {
            get {
                if(this.kvPartsSalesCategorys == null)
                    this.kvPartsSalesCategorys = new ObservableCollection<PartsSalesCategory>();
                return kvPartsSalesCategorys;
            }
        }

        public PartsSalesCategory SelectedPartsSalesCategory {
            get {
                return this.partsSalesCategory;
            }
            set {
                if(this.partsSalesCategory == value)
                    return;
                this.partsSalesCategory = value;
                this.NotifyOfPropertyChange("PartsSalesCategory");
                this.PartsPurchaseOrderTypes.Refresh();
            }
        }

        public PagedCollectionView PartsPurchaseOrderTypes {
            get {
                if(this.partsPurchaseOrderTypes == null) {
                    this.partsPurchaseOrderTypes = new PagedCollectionView(this.allPartsPurchaseOrderTypes);
                    this.partsPurchaseOrderTypes.Filter = o => ((PartsPurchaseOrderType)o).PartsSalesCategoryId == (this.SelectedPartsSalesCategory != null ? this.SelectedPartsSalesCategory.Id : int.MinValue);
                }
                return this.partsPurchaseOrderTypes;
            }
        }

        public override void Validate() {
            //
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsPurchaseOrderTypesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsPurchaseOrderTypes in loadOp.Entities)
                    this.allPartsPurchaseOrderTypes.Add(partsPurchaseOrderTypes);
            }, null);
            //domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    this.KvPartsSalesCategorys.Clear();
            //    foreach(var partsSalesCategories in loadOp.Entities)
            //        this.KvPartsSalesCategorys.Add(partsSalesCategories);
            //}, null);
        }
    }
}