﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Linq;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsInventoryBillQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouse = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsInventoryBill_Status = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
           "Area_Category"
        };

        public PartsInventoryBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {

            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesWithStorageCompanyQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && !e.WmsInterface && e.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var _entities = loadOp.Entities.OrderBy(i => i.Name);
                foreach(var warehouse in _entities) {
                    this.kvWarehouse.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);

            domainContext.Load(domainContext.GetKeyValueItemsQuery().Where(e => e.Name == "PartsInventoryBill_Status"), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var _entities = loadOp.Entities.OrderBy(i => i.Key);
                foreach(var warehouse in _entities) {
                    if(warehouse.Key != 7) {
                        this.kvPartsInventoryBill_Status.Add(new KeyValuePair {
                            Key = warehouse.Key,
                            Value = warehouse.Value
                        });
                    }

                }
            }, null);

            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId = "Common",
                    EntityType = typeof(PartsInventoryBillEx),
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsInventoryBill,
                    QueryItems = new[]{
                        new KeyValuesQueryItem{
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouse,
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInventoryBill_WarehouseName
                        }, new QueryItem{
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInventoryBill_Code
                        }, new KeyValuesQueryItem{
                            ColumnName = "Status",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Status,
                            KeyValueItems =kvPartsInventoryBill_Status
                        }, new KeyValuesQueryItem{
                            ColumnName = "WarehouseAreaCategory",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInventoryBill_WarehouseAreaCategory
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime,
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            },
                        }
                    }
                }
            };
        }
    }
}
