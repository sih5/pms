﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsInventoryBillReportedQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "PartsInventoryBill_Status","Area_Category"
        };

        public PartsInventoryBillReportedQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId).OrderBy(r => r.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsInventoryBillReported,
                        EntityType = typeof(PartsOutboundBill),
                        QueryItems = new[] {
                            new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_WarehouseName,
                                ColumnName = "WarehouseId",
                                KeyValueItems=this.kvWarehouses
                            }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title=PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_Status
                        }, new KeyValuesQueryItem {
                                ColumnName = "WarehouseAreaCategory",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_WarehouseAreaCategory
                            }, new QueryItem {
                                ColumnName = "Code",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_Code
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new[] {
                                    DateTime.Now.Date.AddDays(-7), DateTime.Now.Date
                                }
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
