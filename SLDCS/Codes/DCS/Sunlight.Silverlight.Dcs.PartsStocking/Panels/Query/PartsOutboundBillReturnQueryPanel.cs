﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsOutboundBillReturnQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        public PartsOutboundBillReturnQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomianContext = new DcsDomainContext();
            dcsDomianContext.Load(dcsDomianContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 || e.Type == (int)DcsWarehouseType.分库 || e.Type == (int)DcsWarehouseType.总库).OrderBy(r => r.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
                this.QueryItemGroups = new[]{
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsOutboundReturn,
                    EntityType = typeof(PartsOutboundBill),
                    QueryItems = new []{
                         new QueryItem{
                            ColumnName="Code",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundReturn_Code
                         },new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            DefaultValue = new []{
                               DateTime.Now.Date,DateTime.Now.Date,
                            },IsExact=false
                         },new QueryItem{
                            ColumnName="CounterpartCompanyCode"
                         },new QueryItem{
                            ColumnName="CounterpartCompanyName"
                         },new KeyValuesQueryItem{
                            Title = PartsStockingUIStrings.QueryPanel_Title_PartsOutboundReturn_Warehouse,
                            ColumnName="WarehouseId",
                            KeyValueItems = this.kvWarehouses,
                            IsExact = false
                         }
                    }
                }
                };
            }, null);
        }
    }
}
