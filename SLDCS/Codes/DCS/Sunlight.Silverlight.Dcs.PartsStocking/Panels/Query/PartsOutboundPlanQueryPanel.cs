﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsOutboundPlanQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
      //  private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "Parts_OutboundType", "PartsOutboundPlan_Status"
        };

        public PartsOutboundPlanQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && (e.Type == (int)DcsWarehouseType.总库 || e.Type == (int)DcsWarehouseType.分库)).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsOutboundPlan,
                    EntityType = typeof(PartsOutboundPlan),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundPlan_Code
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            AllowMultiSelect = true
                        },
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundPlan_WarehouseName,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses,
                            IsExact = false
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "OutboundType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            IsExact = false
                        },
                        new QueryItem {
                            ColumnName = "CounterpartCompanyName"
                        },
                        new QueryItem {
                            ColumnName = "SourceCode"
                        },new QueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartCode
                        },
                          new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            },
                            IsExact = false
                        },
                        new QueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_SalesOrderType,
                            ColumnName = "PartsSalesOrderTypeName"
                        }
                    }
                }
            };
        }
    }
}
