﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsPackingQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public PartsPackingQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsPacking,
                        EntityType = typeof(PartsPacking),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "SparePartCode"
                            }, new QueryItem {
                                ColumnName = "SparePartName"
                            }, new QueryItem {
                                ColumnName = "WarehouseAreaCode"
                            }, new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsPacking_WarehouseName,
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
