﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsShelvesTaskQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "Parts_InboundType","ShelvesTaskStatus"
        };
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public PartsShelvesTaskQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsShelvesTask,
                    EntityType = typeof(PartsShelvesTask),
                    QueryItems = new[] {
                        //new QueryItem {
                        //    Title = "上架任务单号",
                        //    ColumnName = "Code"
                        //},
                        new QueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_SourceCode,
                            ColumnName = "SourceBillCode"
                        },new CustomQueryItem {
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_PartsInboundPlanCode,
                            ColumnName = "PartsInboundPlanCode" ,
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = PartsStockingUIStrings.PackingTask_PartsInboundPlanCode,
                            ColumnName = "PartsInboundCheckBillCode",
                             DataType = typeof(string)
                        },new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_InboundType,
                            ColumnName = "InboundType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new CustomQueryItem {
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_WarehouseArea,
                            ColumnName = "WarehouseArea" ,
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_WarehouseAreaCode,
                            ColumnName = "WarehouseAreaCode" ,
                            DataType = typeof(string)
                        },new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_Status,
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            AllowMultiSelect = true,
                             DefaultValue=new System.Collections.Generic.List<int>{(int)DcsShelvesTaskStatus.部分上架,(int)DcsShelvesTaskStatus.新增},
                        },new CustomQueryItem {
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode,
                            ColumnName = "SparePartCode" ,
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName,
                            ColumnName = "SparePartName",
                             DataType = typeof(string)
                        },new KeyValuesQueryItem {
                            Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseName,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                            //DefaultValue = new[] {
                            //    DateTime.Now.Date, DateTime.Now.Date
                            //}
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ShelvesFinishTime",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_PartsShelvesFinishTime
                        },new CustomQueryItem {
                            ColumnName = "NoWarehouseArea",
                            DataType = typeof(bool),
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_NoWarehouseAreaTask
                        },new CustomQueryItem {
                            ColumnName = "HasDifference",
                            DataType = typeof(bool),
                            Title = PartsStockingUIStrings.QueryPanel_Title_HasDifference
                        }
                    }
                }
            };
        }
    }
}
