﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsShiftOrderQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsShiftOrder_Type","PartsShiftOrder_Status","PartsShiftOrderShiftStatus"
        };

        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public PartsShiftOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        EntityType = typeof(PartsShiftOrder),
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsShiftOrder,
                        QueryItems = new[] {
                            new KeyValuesQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShiftOrder_Warehouse,
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses
                            }, new QueryItem {
                                ColumnName = "Code",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShiftOrder_Code
                            }, new KeyValuesQueryItem {
                                ColumnName = "Type",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                            }, new KeyValuesQueryItem {
                                ColumnName = "ShiftStatus",
                                KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShiftOrder_ShiftStatus
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime"
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
