﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsShippingOrderForConfirmQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvClosedLoopStatus = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "PartsShippingOrder_Type", "PartsShippingOrder_Status"
        };

        public PartsShippingOrderForConfirmQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.kvClosedLoopStatus.Add(new KeyValuePair() {
                Key = 1,
                Value = PartsStockingUIStrings.QueryPanel_Title_NoClosedLoop
            });
            this.kvClosedLoopStatus.Add(new KeyValuePair() {
                Key = 2,
                Value = PartsStockingUIStrings.QueryPanel_Title_ClosedLoop
            });
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, LoadOp1 => {
                if(LoadOp1.HasError)
                    return;
                var company = LoadOp1.Entities.SingleOrDefault();
                if(company.Type == (int)DcsCompanyType.分公司) {
                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                } else {
                    dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        foreach(var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name
                            });
                    }, null);
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PartsShippingOrder),
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsShippingOrder,
                    QueryItems = new[] {
                        // new KeyValuesQueryItem {
                        //    ColumnName = "PartsSalesCategoryId",
                        //    Title =  PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_PartsSalesCategoryName,
                        //    KeyValueItems = this.kvPartsSalesCategoryName
                        //},
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_Code
                        },new QueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_Title_OutWarehouse,
                                ColumnName = "WarehouseName",
                                IsExact=false
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_ClosedLoopStatus,
                            ColumnName = "ClosedLoopStatus",
                            KeyValueItems = this.kvClosedLoopStatus
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "RequestedArrivalDate",
                            //DefaultValue = new[] {
                            //    DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                            //}
                        }, new QueryItem {
                            ColumnName = "LogisticCompanyName"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                            }
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ConfirmedReceptionTime",
                            //DefaultValue = new[] {
                            //    DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                            //}
                        }, new QueryItem {
                            ColumnName = "ShippingCompanyCode"
                        }, new QueryItem {
                            ColumnName = "ShippingCompanyName"
                        }
                        //new QueryItem {
                        //    ColumnName = "LogisticCompanyCode"
                        //}
                        //
                        //, new QueryItem {
                        //    ColumnName = "ExpressCompanyCode",
                        //    Title = "快递公司编号"
                        //}
                        //, new QueryItem {
                        //    ColumnName = "ExpressCompany",
                        //    Title = "快递公司名称"
                        //}
                        //, new KeyValuesQueryItem {
                        //    ColumnName = "Type",
                        //    Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_Type,
                        //    KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        //},new QueryItem{
                        //    ColumnName="GPSCode"
                        //}
                    }
                }
            };
        }
    }
}
