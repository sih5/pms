﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsShippingOrderForLogisticQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvClosedLoopStatus = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "PartsShippingOrder_Type", "PartsShippingOrder_Status", ""
        };

        public PartsShippingOrderForLogisticQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);

        }

        private void Initialize() {
            this.kvClosedLoopStatus.Add(new KeyValuePair() {
                Key = 1,
                Value = PartsStockingUIStrings.QueryPanel_Title_NoClosedLoop
            });
            this.kvClosedLoopStatus.Add(new KeyValuePair() {
                Key = 2,
                Value = PartsStockingUIStrings.QueryPanel_Title_ClosedLoop
            });
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效).OrderBy(r => r.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
            }, null);          

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(PartsShippingOrder),
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsShippingOrder,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_Code
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Type",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_Type,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        },
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_OutWarehouse,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "RequestedArrivalDate",
                            //DefaultValue = new[] {
                            //    DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                            //}
                        },
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_Title_ClosedLoopStatus,
                            ColumnName = "ClosedLoopStatus",
                            KeyValueItems = this.kvClosedLoopStatus
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                            }
                        },
                        new DateTimeRangeQueryItem {
                            ColumnName = "ConfirmedReceptionTime",
                            //DefaultValue = new[] {
                            //    DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                            //}
                        },
                        new QueryItem {
                            ColumnName = "ReceivingCompanyCode"
                        },
                        new QueryItem {
                            ColumnName = "ReceivingCompanyName"
                        }

                    }
                }
            };

        }
    }
}