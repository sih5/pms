﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query
{
    public class PartsStockDetailQueryQueryPanel : DcsQueryPanelBase
    {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = new[] {
            "Area_Category","PartsSalesPrice_PriceType"
        };

        private void Initialize()
        {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesWithBranchIdsQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.Type != (int)DcsWarehouseType.虚拟库).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var warehouse in loadOp.Entities)
                {
                    this.kvWarehouses.Add(new KeyValuePair
                    {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsStockDetailQuery,
                        EntityType = typeof(VirtualPartsStock),
                        QueryItems = new[] {
                            new KeyValuesQueryItem {
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses,
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseName
                            }, new KeyValuesQueryItem {
                                ColumnName = "WarehouseAreaCategory",
                                KeyValueItems = this.KeyValueManager[kvNames[0]]
                            },
                            new CustomControlQueryItem {
                                ColumnName = "SparePartCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_SparePartCode,
                                CreateControl=type =>new Controls.MultipleTextQueryControl(type,"SparePartCode"),
                                IsExact =true
                            }
                            , new QueryItem {
                                ColumnName = "SparePartName",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_SparePartName
                            }, new QueryItem {
                                ColumnName = "WarehouseAreaCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaCode
                            }, new QueryItem {
                                ColumnName = "WarehouseRegionCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseRegionCode
                            }
                            //, new KeyValuesQueryItem {
                            //    ColumnName = "PriceType",
                            //    Title = "价格类型",
                            //    KeyValueItems = this.KeyValueManager[kvNames[1]],
                            //    DefaultValue = (int)DcsPartsSalesPricePriceType.基准销售价
                            //}
                            ,new CustomQueryItem {
                            ColumnName = "GreaterThanZero",
                            DataType = typeof(bool),
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_PartsStockGreaterThanZero,
                            DefaultValue = true
                            }
                        }
                    }
                };
            }, null);
        }

        public PartsStockDetailQueryQueryPanel()
        {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
