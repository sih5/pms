﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsStockForWarrantyQueryPanel : DcsQueryPanelBase {

        private readonly PartsStockForWarrantyViewModel viewModel = new PartsStockForWarrantyViewModel();

        private void Initialize() {
            this.QueryItemGroups = new[]{
                    new QueryItemGroup{
                        UniqueId="Common",
                        Title=PartsStockingUIStrings.QueryPanel_Title_PartsStockForWarrantyQuery,
                        EntityType =typeof(WarehousePartsStock),
                        QueryItems =new []{
                          new QueryItem{
                        ColumnName = "SparePartCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartCode
                     } ,new QueryItem {
                         ColumnName="SparePartName",
                         Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_SparePartName
                     },new ComboQueryItem {
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_PartsSalesCategoryName,
                                ColumnName = "PartsSalesCategoryId",
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name",
                                ItemsSource = this.viewModel.PartsSalesCategories,
                                SelectedItemBinding = new Binding("PartsSalesCategory") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }
                     } ,new ComboQueryItem {
                              Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_Warehouse,
                                 ColumnName = "WarehouseId",
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name",
                                ItemsSource = this.viewModel.VirtualWarehouses,
                    }
                   }
                }
            };
        }

        public PartsStockForWarrantyQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
        }
    }
    public class PartsStockForWarrantyViewModel : ViewModelBase {
        private readonly ObservableCollection<PartsSalesCategory> partsSalesCategories = new ObservableCollection<PartsSalesCategory>();
        private readonly ObservableCollection<VirtualWarehouse> allWarehouses = new ObservableCollection<VirtualWarehouse>();
        private PagedCollectionView virtualWarehouse;
        private PartsSalesCategory selectedPartsSalesCategory;

        public ObservableCollection<PartsSalesCategory> PartsSalesCategories {
            get {
                return this.partsSalesCategories;
            }
        }
        public PartsSalesCategory PartsSalesCategory {
            get {
                return this.selectedPartsSalesCategory;
            }
            set {
                if(this.selectedPartsSalesCategory == value)
                    return;
                this.selectedPartsSalesCategory = value;
                this.NotifyOfPropertyChange("PartsSalesCategory");
                this.VirtualWarehouses.Refresh();
            }
        }
        public PagedCollectionView VirtualWarehouses {
            get {
                if(this.virtualWarehouse == null) {
                    this.virtualWarehouse = new PagedCollectionView(this.allWarehouses);
                    this.virtualWarehouse.Filter = o => ((VirtualWarehouse)o).PartsSalesCategoryId == (this.PartsSalesCategory != null ? this.PartsSalesCategory.Id : int.MinValue);
                }
                return this.virtualWarehouse;
            }
        }

        public override void Validate() {
            //
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.IsNotWarranty), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.partsSalesCategories.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.partsSalesCategories.Add(entity);
                }
            }, null);
            domainContext.Load(domainContext.GetWarehousesWithPartsSalesCategoryQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.allWarehouses.Clear();
                foreach(var warehouse in loadOp.Entities)
                    this.allWarehouses.Add(warehouse);
            }, null);
        }
    }
}
