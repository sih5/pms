﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsStockHistoryQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "Area_Category"
        };
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        public PartsStockHistoryQueryPanel()
        {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        public void Initialize()
        {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var warehouse in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair
                    {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                    });
            }, null);

                this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsStockChangeLog,
                    EntityType = typeof(VirtualPartsStockHistory),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            Title=PartsStockingUIStrings.QueryPanel_Title_WarehouseName,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses
                        },
                        new QueryItem {
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                            ColumnName = "SparePartCode"
                        }, new QueryItem {
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName,
                            ColumnName = "SparePartName"
                        },
                         new QueryItem {
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_WarehouseAreaCode,
                            ColumnName = "WarehouseAreaCode"
                        },
                        new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseAreaCategory,
                            ColumnName = "WarehouseAreaCategory",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        }
                        ,new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CreateTime,
                            DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                                }
                        }
                    }
                }
            };
        }
    }
}
