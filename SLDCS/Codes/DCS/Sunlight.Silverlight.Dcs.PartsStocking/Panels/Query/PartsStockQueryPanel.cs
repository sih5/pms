﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsStockQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "Area_Category"
        };

        public PartsStockQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsStock,
                    EntityType = typeof(PartsStock),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses,
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseName,
                        }, 
                        new KeyValuesQueryItem {
                            ColumnName = "WarehouseAreaCategory.Category",
                             KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_Category
                        },new CustomQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_SparePartCode,
                            ColumnName = "SparePart.Code",
                            DataType = typeof(string),
                            IsExact = true
                        }, new CustomQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_SparePartName,
                            ColumnName = "SparePart.Name",
                            DataType = typeof(string),
                            IsExact = true
                        }, new CustomQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaCode,
                            ColumnName = "WarehouseArea.Code",
                            DataType = typeof(string),
                            IsExact = true
                        }
                    }
                }
            };
        }
    }
}
