﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsStockQueryQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "Storage_Center"
        };

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            domainContext.Load(domainContext.GetWarehousesOrderByNameWithBranchIdQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                //设置combobox下拉框的值
                foreach(var warehouse in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
                this.QueryItemGroups = new[]{
                    new QueryItemGroup{
                        UniqueId="Common",
                        Title=PartsStockingUIStrings.QueryPanel_Title_PartsStockQuery,
                        EntityType =typeof(WarehousePartsStock),
                        QueryItems =new []{
                          new QueryItem{
                        ColumnName = "SparePartCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartCode
                     } ,new QueryItem {
                         ColumnName="SparePartName",
                         //IsExact = true,
                         Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_SparePartName
                     },new KeyValuesQueryItem{
                         ColumnName = "WarehouseId",
                         KeyValueItems = this.kvWarehouses,
                         Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_Warehouse
                     },new KeyValuesQueryItem{
                         ColumnName = "StorageCenter",
                         KeyValueItems=KeyValueManager[this.kvNames[0]],
                         Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_ExpressToLogistic_FocufingCoreName
                     }
                   }
                }
            };
            }, null);
        }
        public PartsStockQueryQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
