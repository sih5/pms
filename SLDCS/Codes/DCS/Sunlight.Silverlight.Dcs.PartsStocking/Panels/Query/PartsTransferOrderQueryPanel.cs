﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PartsTransferOrderQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsTransferOrder_Status", "PartsTransferOrder_Type"
        };

        private readonly ObservableCollection<KeyValuePair> kvOriginalWarehouseNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvDestWarehouseNames = new ObservableCollection<KeyValuePair>();

        public PartsTransferOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            this.kvDestWarehouseNames.Clear();
            this.kvOriginalWarehouseNames.Clear();
            //domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && (e.Type == (int)DcsWarehouseType.总库 || e.Type == (int)DcsWarehouseType.分库)).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
            domainContext.Load(domainContext.GetWarehousesbyPersonnelIdQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities) {
                    this.kvOriginalWarehouseNames.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                    this.kvDestWarehouseNames.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsTransferOrder,
                    EntityType = typeof(PartsTransferOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsTransferOrder_Code
                        }, new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsTransferOrder_OriginalWarehouseName,
                            ColumnName = "OriginalWarehouseId",
                            KeyValueItems = this.kvOriginalWarehouseNames,
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                        //, new QueryItem {
                        //    ColumnName = "OriginalBillCode"
                        //}
                        ,new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsTransferOrder_DestWarehouseName,
                            ColumnName = "DestWarehouseId",
                            KeyValueItems = this.kvDestWarehouseNames,
                        }, new KeyValuesQueryItem {
                            ColumnName = "Type",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }
                        //, new QueryItem {
                        //    Title="SAP采购计划单号",
                        //    ColumnName = "SAPPurchasePlanCode"
                        //}
                        , new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            },
                            IsExact = true
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                            IsExact = true
                        }
                        //, new QueryItem {
                        //    Title="平台单号",
                        //    ColumnName = "ERPSourceOrderCode"
                        //} 
                    }
                }
            };
        }
    }
}
