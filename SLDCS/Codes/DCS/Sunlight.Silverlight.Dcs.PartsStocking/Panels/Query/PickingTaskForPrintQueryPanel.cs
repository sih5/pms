﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PickingTaskForPrintQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        public PickingTaskForPrintQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.queryWarehouseAreaForPersonelQuery(), loadOp => {
                if(loadOp.HasError)
                    return;
                    foreach(var warehouse in loadOp.Entities) {
                        this.kvWarehouses.Add(new KeyValuePair {
                            Key = warehouse.Id,
                            Value = warehouse.Code
                        });
                    }
               
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PartsStock,
                    EntityType = typeof(PickingTaskForPrint),
                    QueryItems = new QueryItem[] {
                         new QueryItem { 
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_Title_PickingTaskCode
                         }
                        // },
                        //new KeyValuesQueryItem {
                        //    ColumnName = "WarehouseArea",
                        //    KeyValueItems = this.kvWarehouses,
                        //    Title = "库区",
                        //}
                    }
                }
            };
        }
    }
}
