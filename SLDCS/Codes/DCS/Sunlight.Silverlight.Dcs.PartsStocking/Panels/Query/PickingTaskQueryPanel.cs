﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query
{
    public class PickingTaskQueryPanel : DcsQueryPanelBase
    {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> allPartsSalesOrderTypes = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "PickingTaskStatus"
        };

        public PickingTaskQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize()
        {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetWarehousesbyPersonnelIdQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var warehouse in loadOp.Entities)
                {
                    this.kvWarehouses.Add(new KeyValuePair
                    {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);


            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesOrderTypeByQuery(), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var entity in loadOp.Entities)
                {
                    this.allPartsSalesOrderTypes.Add(new KeyValuePair
                    {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PickingTask,
                    EntityType = typeof(PickingTask),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = PartsStockingUIStrings.QueryPanel_Title_PickingTaskCode
                        }, new KeyValuesQueryItem {
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundPlan_WarehouseName,
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses,
                            IsExact = false
                        }, new QueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName
                        }                      
                        , new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            AllowMultiSelect = true,
                            DefaultValue=new System.Collections.Generic.List<int>{(int)DcsPickingTaskStatus.部分拣货,(int)DcsPickingTaskStatus.新建},

                        },new QueryItem {
                            ColumnName = "SourceCode",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_SourceCode
                        },new QueryItem {
                            ColumnName = "PartsOutboundPlanCode",
                            Title = PartsStockingUIStrings.QueryPanel_Title_PartsOutboundPlan_Code
                        }
                        ,new QueryItem {
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyName,
                            ColumnName = "CounterpartCompanyName"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        },new DateTimeRangeQueryItem {
                            Title=PartsStockingUIStrings.QueryPanel_Title_Pickingfinishtime,
                            ColumnName = "Pickingfinishtime",                           
                        } 
                        ,new KeyValuesQueryItem {
                            ColumnName = "OrderTypeId",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_PartsSalesOrderTypeName,
                            KeyValueItems = this.allPartsSalesOrderTypes
                        }
                       
                    }
                }
            };
        }
    }
}

