﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class PostSaleClaimsTmpQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
           "PostSaleClaimsTmp_Type"                  
        };


        public PostSaleClaimsTmpQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_PostSaleClaimsTmp,
                    EntityType = typeof(VirtualPostSaleClaimsTmp),
                    QueryItems = new QueryItem[] {
                        new QueryItem{
                            ColumnName = "ClaimBillCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_ClaimBillCode
                        }, new QueryItem{
                            ColumnName = "SparePartCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartCode
                        }, new QueryItem{
                            ColumnName = "SparePartName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartName
                        }, new QueryItem{
                            ColumnName = "DealerCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsInventoryBill_StorageCompanyCode
                        }, new QueryItem{
                            ColumnName = "DealerName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsInventoryBill_StorageCompanyName
                        }
                        , new KeyValuesQueryItem {
                            ColumnName = "Type",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Type
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime
                        }
                    }
                }
            };
        }
    }
}
