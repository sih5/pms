﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class VirtualCompanySalescenterStockQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> KvBranchs = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> KvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public VirtualCompanySalescenterStockQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.DataContextChanged += VirtualCompanySalescenterStockQueryPanel_DataContextChanged;
        }

        private void VirtualCompanySalescenterStockQueryPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            if(!(this.DataContext is VirtualCompanySalescenterStock))
                return;
            var virtualCompanySalescenterStock = this.DataContext as VirtualCompanySalescenterStock;
            virtualCompanySalescenterStock.PropertyChanged += virtualCompanySalescenterStock_PropertyChanged;
        }

        private void virtualCompanySalescenterStock_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            if(e.PropertyName.Equals("BranchId")) {
                var domainContext = new DcsDomainContext();
                var dataContext = this.DataContext as VirtualCompanySalescenterStock;
                var selectedBranch = this.KvBranchs.First(v => v.Key == dataContext.BranchId).UserObject as Branch;
                domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(p => p.Status == (int)DcsMasterDataStatus.有效 && p.BranchId == (int)selectedBranch.Id), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    foreach(var entity in loadOp.Entities) {
                        this.KvPartsSalesCategorys.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Name
                        });
                    }
                }, null);
            }
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name,
                        UserObject = partsSalesCategory
                    });
            }, null);
            domainContext.Load(domainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.KvBranchs.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                        UserObject = branch
                    });
            }, null);

            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_VirtualCompanySalescenterStock,
                        EntityType = typeof(VirtualCompanySalescenterStock),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "SparePartsCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_VirtualCompanySalescenterStock_SparePartCode
                            }, new QueryItem {
                                ColumnName = "SparePartsName",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_VirtualCompanySalescenterStock_SparePartName
                            }, new KeyValuesQueryItem {
                                ColumnName = "BranchId",
                                KeyValueItems = this.KvBranchs,
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_VirtualCompanySalescenterStock_Branch
                            }, new KeyValuesQueryItem {
                                ColumnName = "PartsSalesCategoryId",
                                KeyValueItems = this.KvPartsSalesCategorys,
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_VirtualCompanySalescenterStock_PartsSalesCategory
                            }
                        }
                    }
                };
         }
    }
}