﻿
using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class VirtualOutboundAndInboundBillHistoricalDataBaseQueryPanel : DcsQueryPanelBase {
        private readonly PartsSalesOrderQueryPanelViewModel viewModel = new PartsSalesOrderQueryPanelViewModel();

        public VirtualOutboundAndInboundBillHistoricalDataBaseQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.viewModel.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_InOutHistoryQuery,
                    EntityType = typeof(VirtualOutboundAndInboundBill),
                    QueryItems = new[] {
                        new QueryItem { 
                            ColumnName = "SparePartCode",
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode
                        }, new QueryItem { 
                            ColumnName = "SparePartName",
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName
                        },new QueryItem {
                            ColumnName = "WarehouseName",
                            Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_WarehouseName
                        },new CustomQueryItem {
                            ColumnName = "WarehouseAreaCode",
                            DataType = typeof(string),
                            Title = PartsStockingUIStrings.QueryPanel_Title_WarehouseArea2
                        },new ComboQueryItem {
                                Title = PartsStockingUIStrings.DataEditPanel_Text_CompanyTransferOrder_PartsSalesCategory,
                                ColumnName = "SalesCategoryId",
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name",
                                ItemsSource = this.viewModel.SalesCategories,
                                SelectedItemBinding = new Binding("SelectedSalesCategory") {
                                    Source = this.viewModel,
                                    Mode = BindingMode.TwoWay
                                }
                        } , new ComboQueryItem {
                                Title =PartsStockingUIStrings.QueryPanel_Title_SalesOrderType,
                                ColumnName = "PartsSalesOrderTypeId",
                                SelectedValuePath = "Id",
                                DisplayMemberPath = "Name",
                                ItemsSource = this.viewModel.SalesOrderTypes,
                            }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime,
                             DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now
                            }
                        } 
                    }
                }
            };
        }
    }

    public class PartsSalesOrderQueryPanelViewModel : ViewModelBase {
        private readonly ObservableCollection<PartsSalesCategory> partsSalesCategories = new ObservableCollection<PartsSalesCategory>();
        private readonly ObservableCollection<PartsSalesOrderType> allPartsSalesOrderTypes = new ObservableCollection<PartsSalesOrderType>();
        private PagedCollectionView partsSalesOrderTypes;
        private PartsSalesCategory selectedSalesCategory;

        public ObservableCollection<PartsSalesCategory> SalesCategories {
            get {
                return this.partsSalesCategories;
            }
        }

        public PagedCollectionView SalesOrderTypes {
            get {
                if(this.partsSalesOrderTypes == null) {
                    this.partsSalesOrderTypes = new PagedCollectionView(this.allPartsSalesOrderTypes);
                    this.partsSalesOrderTypes.Filter = o => ((PartsSalesOrderType)o).PartsSalesCategoryId == (this.SelectedSalesCategory != null ? this.SelectedSalesCategory.Id : int.MinValue);
                }
                return this.partsSalesOrderTypes;
            }
        }

        public PartsSalesCategory SelectedSalesCategory {
            get {
                return this.selectedSalesCategory;
            }
            set {
                if(this.selectedSalesCategory == value)
                    return;
                this.selectedSalesCategory = value;
                this.NotifyOfPropertyChange("PartsSalesCategory");
                this.SalesOrderTypes.Refresh();
            }
        }

        public override void Validate() {
            //
        }

        public void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesOrderTypeByBranchIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var entity in loadOp.Entities)
                    this.allPartsSalesOrderTypes.Add(entity);
            }, null);

            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSaleCategory in loadOp.Entities)
                    this.partsSalesCategories.Add(partsSaleCategory);
            }, null);
        }
    }
}
