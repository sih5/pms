﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class VirtualPartsStockForSelectQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = new[] {
            "Area_Category"
        };

        public VirtualPartsStockForSelectQueryPanel() {
            this.KeyValueManager.Register(kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_VirtualPartsStock,
                        EntityType = typeof(VirtualPartsStock),
                        QueryItems = new[] {
                            new KeyValuesQueryItem {
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses,
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseName
                            }, new KeyValuesQueryItem {
                                ColumnName = "WarehouseAreaCategory",
                                KeyValueItems = this.KeyValueManager[kvNames[0]]
                            },new QueryItem{
                                ColumnName = "WarehouseRegionCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseRegionCode
                            }, new QueryItem {
                                ColumnName = "WarehouseAreaCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaCode
                            }, new QueryItem {
                                ColumnName = "SparePartCode",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_SparePartCode
                            }, new QueryItem {
                                ColumnName = "SparePartName",
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_SparePartName
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
