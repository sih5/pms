﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class VirtualPartsStockQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public VirtualPartsStockQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            ShellViewModel.Current.IsBusy = true;
            domainContext.Load(domainContext.GetWarehousesWithBranchIdsQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.Type != (int)DcsWarehouseType.虚拟库 && !(e.WmsInterface)).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                ShellViewModel.Current.IsBusy = false;
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_VirtualPartsStock,
                    EntityType = typeof(VirtualPartsStock),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            ColumnName = "WarehouseId",
                            KeyValueItems = this.kvWarehouses,
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseName
                        },
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_SparePartCode
                        },
                        new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_SparePartName
                        },
                        new QueryItem {
                            ColumnName = "WarehouseAreaCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaCode
                        }
                    }
                }
            };
        }
    }
}
