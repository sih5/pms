﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class VirtualWarehousePartsStockQueryPanel : DcsQueryPanelBase {

        public VirtualWarehousePartsStockQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(WarehousePartsStock),
                    Title =PartsStockingUIStrings.QueryPanel_Title_PartsStockQueryForTransfer,
                    QueryItems = new[] {
                         new QueryItem {
                            ColumnName = "SparePartCode",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartCode
                        }, new QueryItem {
                            ColumnName = "SparePartName",
                            Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartName
                        }
                    }
                }
            };
        }
    }
}
