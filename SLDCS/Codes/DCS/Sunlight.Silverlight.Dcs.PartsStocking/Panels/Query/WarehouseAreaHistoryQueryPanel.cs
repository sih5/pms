﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class WarehouseAreaHistoryQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "Area_Category"
        };

        public WarehouseAreaHistoryQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId).OrderBy(t => t.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var item in loadOp.Entities)
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = PartsStockingUIStrings.QueryPanel_Title_WarehouseAreaHistory,
                        EntityType = typeof(WarehouseAreaHistory),
                        QueryItems = new QueryItem[] {
                            new KeyValuesQueryItem {
                                ColumnName = "WarehouseId",
                                KeyValueItems = this.kvWarehouses,
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_Warehouse
                            }, new KeyValuesQueryItem {
                                ColumnName = "WarehouseAreaCategoryId",
                                KeyValueItems=this.KeyValueManager[this.kvNames[0]],
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_WarehouseAreaCategory
                            }, new CustomQueryItem{
                                ColumnName="SparePart.Code",
                                DataType=typeof(string),
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_SparePartCode
                            }, new CustomQueryItem{
                                ColumnName="SparePart.Name",
                                DataType=typeof(string),
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_SparePartName
                            }, new CustomQueryItem{
                                ColumnName="WarehouseArea.Code",
                                DataType=typeof(string),
                                Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_WarehouseAreaCode
                            }
                        }
                    }
            };
        }
    }
}