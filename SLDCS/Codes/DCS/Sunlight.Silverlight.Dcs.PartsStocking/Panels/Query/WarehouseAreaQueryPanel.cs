﻿using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels.Query {
    public class WarehouseAreaQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvAreaKind = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "Area_Category", "Area_Kind", "BaseData_Status"
        };

        private readonly int[] excludeStatus = new[] {
            (int)DcsAreaKind.仓库
        };

        public WarehouseAreaQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(() => {
                foreach(var status in this.KeyValueManager[this.kvNames[1]].Where(kv => !this.excludeStatus.Contains(kv.Key)))
                    this.kvAreaKind.Add(status);
            });
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_WarehouseArea,
                    EntityType = typeof(WarehouseArea),
                    QueryItems = new [] {
                        new CustomQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(Warehouse), "Name"),
                            ColumnName = "Warehouse.Name",
                            DataType = typeof(string),
                            IsExact = true
                        }, new QueryItem {
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(WarehouseAreaCategory), "Category"),
                            ColumnName = "WarehouseAreaCategory.Category",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "AreaKind",
                            KeyValueItems = this.kvAreaKind
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }
                    }
                }
            };
        }
    }
}
