﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Panels {
    public class WarehouseQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "BaseData_Status", "Storage_Center"
        };

        public WarehouseQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = PartsStockingUIStrings.QueryPanel_Title_Warehouse,
                    EntityType = typeof(Warehouse),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        }, new QueryItem {
                            ColumnName = "Name"
                        }, new KeyValuesQueryItem {
                            ColumnName = "BranchId",
                            KeyValueItems = this.kvBranches,
                            DefaultValue = BaseApp.Current.CurrentUserData.EnterpriseId,
                            IsEnabled = false
                        }, new KeyValuesQueryItem {
                            ColumnName = "StorageCenter",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }, new QueryItem {
                            ColumnName = "OperatorName",
                            Title= PartsStockingUIStrings.QueryPanel_Title_PersonelName
                        }
                    }
                }
            };
        }
    }
}
