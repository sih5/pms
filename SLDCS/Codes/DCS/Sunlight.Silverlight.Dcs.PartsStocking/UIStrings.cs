﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsStocking {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }

        public string CustomView_Text_WareHouseArea_Title_WarehouseAreaManager {
            get {
                return Resources.PartsStockingUIStrings.CustomView_Text_WareHouseArea_Title_WarehouseAreaManager;
            }
        }

        public string Button_Text_Common_PartsShelves {
            get {
                return Resources.PartsStockingUIStrings.Button_Text_Common_PartsShelves;
            }
        }

        public string DataEditView_Text_PartsShiftOrder_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_WarehouseName;
            }
        }

        public string DataEditView_GroupTitle_PartsShiftOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_GroupTitle_PartsShiftOrder;
            }
        }

        public string DataEditViewl_GroupTitle_PartsInboundCheckBill {
            get {
                return Resources.PartsStockingUIStrings.DataEditViewl_GroupTitle_PartsInboundCheckBill;
            }
        }

        public string DataEditView_Text_PartsInboundCheckBill_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInboundCheckBill_WarehouseName;
            }
        }

        public string DataEditView_Text_PartsOutboundBill_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsOutboundBill_WarehouseName;
            }
        }

        public string DataEditView_Content_PartsInboundPackingDetail_BatchNumber {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Content_PartsInboundPackingDetail_BatchNumber;
            }
        }

        public string DataEditPanel_Text_OverstockPartsApp_Code {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_OverstockPartsApp_Code;
            }
        }

        public string DataEditPanel_Text_OverstockPartsApp_StorageCompanyCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_OverstockPartsApp_StorageCompanyCode;
            }
        }

        public string DataEditPanel_Text_OverstockPartsApp_StorageCompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_OverstockPartsApp_StorageCompanyName;
            }
        }

        public string DataEditPanel_Text_OverstockPartsApp_StorageCompanyType {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_OverstockPartsApp_StorageCompanyType;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsAdjustBill_SourceCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsAdjustBill_SourceCode;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsAdjustDetail_ShippingAmount {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsAdjustDetail_ShippingAmount;
            }
        }

        public string DataEditView_Text_PartsShiftOrder_VirtualPartsStock_DestWarehouseAreaCategory {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_DestWarehouseAreaCategory;
            }
        }

        public string DataEditView_Text_PartsShiftOrder_VirtualPartsStock_OriginalWarehouseAreaCategory {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_OriginalWarehouseAreaCategory;
            }
        }

        public string DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode;
            }
        }

        public string DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName;
            }
        }

        public string DataEditView_Text_PartsShiftOrder_VirtualPartsStock_WarehouseAreaCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_WarehouseAreaCode;
            }
        }

        public string DataEditView_Text_PartsShiftOrder_VirtualPartsStock_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_WarehouseName;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsStock_UsableQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_UsableQuantity;
            }
        }

        public string DataEditView_Title_WarehouseArea {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_WarehouseArea;
            }
        }

        public string DataEditPanel_Text_OverstockPartsApp_BranchName {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_OverstockPartsApp_BranchName;
            }
        }

        public string Button_Text_Common_PrintsLabels {
            get {
                return Resources.PartsStockingUIStrings.Button_Text_Common_PrintsLabels;
            }
        }

        public string DataEditView_Text_PartsPacking_Quantity {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsPacking_Quantity;
            }
        }

        public string DataEditView_Text_PrintNumber {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PrintNumber;
            }
        }

        public string DataEditView_Title_ApprovePartsTransferOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ApprovePartsTransferOrder;
            }
        }

        public string DataDetailView_PartsTransferOrderDetail_Constant_CheckBill {
            get {
                return Resources.PartsStockingUIStrings.DataDetailView_PartsTransferOrderDetail_Constant_CheckBill;
            }
        }

        public string DataDetailView_PartsTransferOrderDetail_Constant_OutboundBillBill {
            get {
                return Resources.PartsStockingUIStrings.DataDetailView_PartsTransferOrderDetail_Constant_OutboundBillBill;
            }
        }

        public string DataDetailView_PartsTransferOrderDetail_Constant_ShippingBill {
            get {
                return Resources.PartsStockingUIStrings.DataDetailView_PartsTransferOrderDetail_Constant_ShippingBill;
            }
        }

        public string DataEditPanel_Text_PartsTransferOrder_DestWarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_PartsTransferOrder_DestWarehouseName;
            }
        }

        public string DataEditPanel_Text_PartsTransferOrder_OriginalWarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_PartsTransferOrder_OriginalWarehouseName;
            }
        }

        public string DataEditPanel_Text_PartsShippingOrder_Type {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_PartsShippingOrder_Type;
            }
        }

        public string QueryPanel_Title_LogisticCompany {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_LogisticCompany;
            }
        }

        public string Button_Text_Common_SearchTransportPartsOutboundDetail {
            get {
                return Resources.PartsStockingUIStrings.Button_Text_Common_SearchTransportPartsOutboundDetail;
            }
        }

        public string Button_Text_Common_Shipping {
            get {
                return Resources.PartsStockingUIStrings.Button_Text_Common_Shipping;
            }
        }

        public string DataEditView_Text_Common_Span_During {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_Common_Span_During;
            }
        }

        public string DataEditView_Text_PartsOutboundBill_Code {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsOutboundBill_Code;
            }
        }

        public string DataEditView_Text_Warehouse_Name {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_Warehouse_Name;
            }
        }

        public string DataEditPanel_Text_Warehouse_StorageCompany {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Warehouse_StorageCompany;
            }
        }

        public string Button_Text_Common_Search {
            get {
                return Resources.PartsStockingUIStrings.Button_Text_Common_Search;
            }
        }

        public string Button_Text_Common_PartsShift {
            get {
                return Resources.PartsStockingUIStrings.Button_Text_Common_PartsShift;
            }
        }

        public string Button_Text_Common_PartsOutbound {
            get {
                return Resources.PartsStockingUIStrings.Button_Text_Common_PartsOutbound;
            }
        }

        public string QueryPanel_QueryItem_Title_OverstockPartsStock_BranchName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_BranchName;
            }
        }

        public string QueryPanel_Title_CompanyStockBySales {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_CompanyStockBySales;
            }
        }

        public string DataGridView_ColumnItem_Title_CompanyPartsStock_PartsSalesCategoryName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_PartsSalesCategoryName;
            }
        }

        public string DataEditView_Text_PartsInboundCheckBill_PartsSalesCategoryName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInboundCheckBill_PartsSalesCategoryName;
            }
        }

        public string DataEditView_Text_PartsOutboundBill_PartsSalesCategoryName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsOutboundBill_PartsSalesCategoryName;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerPartsInventoryBill_Code {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_Code;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerPartsInventoryBill_PartsSalesCategoryName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_PartsSalesCategoryName;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerPartsInventoryBill_StorageCompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_StorageCompanyName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryBill_Code {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_Code;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseAreaCategory {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseAreaCategory;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryDetails_CurrentBatchNumber {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_CurrentBatchNumber;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartCode;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryDetails_WarehouseAreaCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_WarehouseAreaCode;
            }
        }

        public string DataManagementView_Title_PartsInventoryBillReporte {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PartsInventoryBillReporte;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBill_Code {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_Code;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBill_WarehouseAreaCategory {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_WarehouseAreaCategory;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBill_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_WarehouseName;
            }
        }

        public string ActionPanel_Title_PartsInventoryBill {
            get {
                return Resources.PartsStockingUIStrings.ActionPanel_Title_PartsInventoryBill;
            }
        }

        public string Action_Title_ViewDetail {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_ViewDetail;
            }
        }

        public string Action_Title_Reject {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Reject;
            }
        }

        public string DataEitView_Text_PartsInventoryBill_WarehouseAreaCategory {
            get {
                return Resources.PartsStockingUIStrings.DataEitView_Text_PartsInventoryBill_WarehouseAreaCategory;
            }
        }

        public string DataEitView_Text_PartsInventoryBill_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEitView_Text_PartsInventoryBill_WarehouseName;
            }
        }

        public string QueryPanel_Title_PartsInventoryBillReported {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsInventoryBillReported;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsOutboundBill_Status {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_Status;
            }
        }

        public string DataEditView_Text_PartsInventoryBillReported_BranchName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInventoryBillReported_BranchName;
            }
        }

        public string DataEditView_Text_PartsInventoryBillReported_StorageCompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInventoryBillReported_StorageCompanyName;
            }
        }

        public string DataEditView_Text_PartsInventoryBillReported_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInventoryBillReported_WarehouseName;
            }
        }

        public string DataGridView_Notification_PartsInventoryBill_WarehouseId {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Notification_PartsInventoryBill_WarehouseId;
            }
        }

        public string BusinessName_PartsInventoryBillReportedDetail {
            get {
                return Resources.PartsStockingUIStrings.BusinessName_PartsInventoryBillReportedDetail;
            }
        }

        public string QueryPanel_Title_DealerPartsStock {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_DealerPartsStock;
            }
        }

        public string DataGridView_ColumnItem_Title_VirtualPartsStock_BranchName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_BranchName;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehousePartsStock_PartsSalesPrice {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsSalesPrice;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartCode;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartName;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehousePartsStock_UsableQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_UsableQuantity;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseCode;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseName;
            }
        }

        public string QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartCode;
            }
        }

        public string QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartName;
            }
        }

        public string QueryPanel_QueryItem_Title_WarehousePartsStock_Warehouse {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_Warehouse;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerPartsStock_DealerCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_DealerCode;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerPartsStock_DealerName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_DealerName;
            }
        }

        public string QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategory {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStock_SalesCategory;
            }
        }

        public string DataGridView_ColumnItem_Title_CompanyPartsStock_SalesCategoryName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SalesCategoryName;
            }
        }

        public string DataGridView_ColumnItem_Title_CompanyPartsStock_QuantityNumber {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_QuantityNumber;
            }
        }

        public string DataEditPanel_Text_CompanyTransferOrder_OriginalCompanyCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_CompanyTransferOrder_OriginalCompanyCode;
            }
        }

        public string DataEditPanel_Text_CompanyTransferOrder_OriginalCompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_CompanyTransferOrder_OriginalCompanyName;
            }
        }

        public string DataEditPanel_Text_CompanyTransferOrder_PartsSalesCategory {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_CompanyTransferOrder_PartsSalesCategory;
            }
        }

        public string DataEditPanel_Text_OverstockPartsApp_PartsSalesCategoryName {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_OverstockPartsApp_PartsSalesCategoryName;
            }
        }

        public string DataManagementView_Title_AgentsPartsInventoryBill {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_AgentsPartsInventoryBill;
            }
        }

        public string QueryPanel_Title_AgentsPartsInventoryBill {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_AgentsPartsInventoryBill;
            }
        }

        public string DataEditView_Text_OverstockPartsAdjustBill_Code {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_Code;
            }
        }

        public string DataEditView_Text_OverstockPartsAdjustBill_DestStorageCompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_DestStorageCompanyName;
            }
        }

        public string DataEditView_Text_OverstockPartsAdjustBill_OriginalStorageCompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_OriginalStorageCompanyName;
            }
        }

        public string DataEditView_Text_OverstockPartsAdjustBill_Remark {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_Remark;
            }
        }

        public string DataEditView_Text_OverstockPartsAdjustBill_SourceCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_SourceCode;
            }
        }

        public string DataEditView_Text_OverstockPartsAdjustBill_StorageCompanyType {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_StorageCompanyType;
            }
        }

        public string DataEditView_HauDistanceInfor_Update {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_HauDistanceInfor_Update;
            }
        }

        public string DataEditView_Notification_NotEquals {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Notification_NotEquals;
            }
        }

        public string DataEditView_Text_HauDistanceInfor_Company1Name {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_HauDistanceInfor_Company1Name;
            }
        }

        public string DataEditView_Text_HauDistanceInfor_CompanyAddress {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_HauDistanceInfor_CompanyAddress;
            }
        }

        public string DataEditView_Text_HauDistanceInfor_CompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_HauDistanceInfor_CompanyName;
            }
        }

        public string QueryPanel_Title_PartsLocationChanges {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsLocationChanges;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehouseAreaHistory_SparePartCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_SparePartCode;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehouseAreaHistory_SparePartName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_SparePartName;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehouseAreaHistory_WarehouseAreaCategoryCategory {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_WarehouseAreaCategoryCategory;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehouseAreaHistory_WarehouseAreaCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_WarehouseAreaCode;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehouseAreaHistory_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_WarehouseName;
            }
        }

        public string QueryPanel_Title_WarehouseAreaHistory {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_WarehouseAreaHistory;
            }
        }

        public string QueryPanel_QueryItem_Title_WarehouseAreaHistory_SparePartCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_SparePartCode;
            }
        }

        public string QueryPanel_QueryItem_Title_WarehouseAreaHistory_SparePartName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_SparePartName;
            }
        }

        public string QueryPanel_QueryItem_Title_WarehouseAreaHistory_Warehouse {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_Warehouse;
            }
        }

        public string QueryPanel_QueryItem_Title_WarehouseAreaHistory_WarehouseAreaCategory {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_WarehouseAreaCategory;
            }
        }

        public string QueryPanel_QueryItem_Title_WarehouseAreaHistory_WarehouseAreaCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_WarehouseAreaCode;
            }
        }

        public string DataEditPanel_PartsOutboundBill_Code {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_PartsOutboundBill_Code;
            }
        }

        public string Button_Text_Common_PartsOutboundBill {
            get {
                return Resources.PartsStockingUIStrings.Button_Text_Common_PartsOutboundBill;
            }
        }

        public string DataEditView_Title_PartsOutboundBillDetail {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartsOutboundBillDetail;
            }
        }

        public string DataEditView_Title_PartsOutboundBillReturnDetail {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartsOutboundBillReturnDetail;
            }
        }

        public string DataEditView_Validation_PartsOutboundBill_CanReturnAmountIsMax {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_CanReturnAmountIsMax;
            }
        }

        public string DataEditView_Validation_PartsOutboundBill_CanReturnAmountIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_CanReturnAmountIsNull;
            }
        }

        public string DataEditView_Validation_PartsOutboundBill_PartsOutboundBillDetailsIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_PartsOutboundBillDetailsIsNull;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsOutboundBillDetail_CanReturnAmount {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBillDetail_CanReturnAmount;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsOutboundBillDetail_PartsWarhouseManageGranularity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBillDetail_PartsWarhouseManageGranularity;
            }
        }

        public string Action_Title_ResultsInput {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_ResultsInput;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehousePartsStock_PartsPurchasePricing {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsPurchasePricing;
            }
        }

        public string QueryPanel_Title_PurchaseWarehousePartsStock {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PurchaseWarehousePartsStock;
            }
        }

        public string DataEditView_Title_InventoryCovers {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_InventoryCovers;
            }
        }

        public string DataEditView_Title_PartsInventoryBill {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartsInventoryBill;
            }
        }

        public string DataEditView_Title_ResultEntry {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ResultEntry;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsStock_LinkMan {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_LinkMan;
            }
        }

        public string DataGridView_ColumnItem_Title_OverstockPartsStock_LinkPhone {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_LinkPhone;
            }
        }

        public string QueryPanel_Title_PartsStockQuery {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsStockQuery;
            }
        }

        public string QueryPanel_Title_OverstockPartsStockForQuantity {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_OverstockPartsStockForQuantity;
            }
        }

        public string QueryPanel_Title_LogisticCompany2 {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_LogisticCompany2;
            }
        }

        public string DataManagementView_PrintWindow_Title_PartsOutboundBill {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsOutboundBill;
            }
        }

        public string DataManagementView_PrintWindow_Title_PartsInventoryBill {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsInventoryBill;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShippingOrder_TransportLossesDisposeMethod {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrder_TransportLossesDisposeMethod;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_PartsSalesOrderTypeName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_PartsSalesOrderTypeName;
            }
        }

        public string DataEditPanel_GroupTitle_PartsShippingOrderForConfirm {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_GroupTitle_PartsShippingOrderForConfirm;
            }
        }

        public string Action_Title_RemovePartsLRelation {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_RemovePartsLRelation;
            }
        }

        public string DataEditView_Title_PartsShippingOrderForDeliveryConfirm {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartsShippingOrderForDeliveryConfirm;
            }
        }

        public string DataEditView_Notification_CompletedImport {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Notification_CompletedImport;
            }
        }

        public string DataEditView_Notification_CompletedImportWithError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Notification_CompletedImportWithError;
            }
        }

        public string DataGridView_Validation_OverstockPartsAppDetail_StorageCompanyTypeIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Validation_OverstockPartsAppDetail_StorageCompanyTypeIsNull;
            }
        }

        public string DataEditView_Validation_HauDistanceInfor_CompanyAddressId {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_HauDistanceInfor_CompanyAddressId;
            }
        }

        public string QueryPanel_Title_PartsStockForWarrantyQuery {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsStockForWarrantyQuery;
            }
        }

        public string DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode;
            }
        }

        public string DataEditView_Text_PartsInventoryBillReported_ReceivingWarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInventoryBillReported_ReceivingWarehouseName;
            }
        }

        public string Action_Title_ExtendedBackConfirm {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_ExtendedBackConfirm;
            }
        }

        public string Action_Title_SendConfirm {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_SendConfirm;
            }
        }

        public string Action_Title_SendConfirmBatch {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_SendConfirmBatch;
            }
        }

        public string Action_Title_SendingConfirm {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_SendingConfirm;
            }
        }

        public string Action_Title_SendingConfirmBatch {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_SendingConfirmBatch;
            }
        }

        public string DataEditView_Title_PartsShippingOrderForExtendedBackConfirm {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartsShippingOrderForExtendedBackConfirm;
            }
        }

        public string DataEditView_Title_PartsShippingOrderForSendingConfirm {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartsShippingOrderForSendingConfirm;
            }
        }

        public string DataEditView_Title_PartsShippingOrderForSendingConfirm3 {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartsShippingOrderForSendingConfirm3;
            }
        }

        public string DataEditView_Title_PartsShippingOrderForSendConfirm {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartsShippingOrderForSendConfirm;
            }
        }

        public string DataGridView_ColumnItem_Title_Express_Contacts {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Contacts;
            }
        }

        public string DataGridView_ColumnItem_Title_Express_ContactsNumber {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ContactsNumber;
            }
        }

        public string DataGridView_ColumnItem_Title_Express_EMail {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_EMail;
            }
        }

        public string DataGridView_ColumnItem_Title_Express_ExpressCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressCode;
            }
        }

        public string DataGridView_ColumnItem_Title_Express_ExpressName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressName;
            }
        }

        public string DataGridView_ColumnItem_Title_Express_FixedTelephone {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_FixedTelephone;
            }
        }

        public string DataGridView_ColumnItem_Title_Express_Status {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Status;
            }
        }

        public string DataGridView_ColumnItem_Title_Express_Address {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Address;
            }
        }

        public string DataGridView_ColumnItem_Title_ExpressToLogistic_FocufingCoreName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_ExpressToLogistic_FocufingCoreName;
            }
        }

        public string DataGridView_ColumnItem_Title_ExpressToLogistic_LogisticsCompanyCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_ExpressToLogistic_LogisticsCompanyCode;
            }
        }

        public string DataGridView_ColumnItem_Title_ExpressToLogistic_LogisticsCompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_ExpressToLogistic_LogisticsCompanyName;
            }
        }

        public string QueryPanel_Title_ExpressToLogistic {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_ExpressToLogistic;
            }
        }

        public string DataEditView_Validation_FocufingCoreNameNotNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_FocufingCoreNameNotNull;
            }
        }

        public string DataEditView_Validation_LogisticsCompanyCodeNotNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_LogisticsCompanyCodeNotNull;
            }
        }

        public string DataEditView_Validation_LogisticsCompanyNameNotNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_LogisticsCompanyNameNotNull;
            }
        }

        public string DataManagementView_Title_GCCPartsOutboundBillForQuery {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_GCCPartsOutboundBillForQuery;
            }
        }

        public string QueryPanel_Title_GCCPartsOutboundBill {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_GCCPartsOutboundBill;
            }
        }

        public string DataEditView_Text_PartsShippingOrder_Logistic {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShippingOrder_Logistic;
            }
        }

        public string DataManagementView_Title_PartsInboundCheckBillNoPrice {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PartsInboundCheckBillNoPrice;
            }
        }

        public string DataManagementView_Title_PartsInboundPlanNoPrice {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PartsInboundPlanNoPrice;
            }
        }

        public string DataManagementView_Title_PartsInboundCheckBillMNoPrice {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PartsInboundCheckBillMNoPrice;
            }
        }

        public string DataEditPanel_Text_Logistics_ApproveTime {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Logistics_ApproveTime;
            }
        }

        public string DataEditPanel_Text_Logistics_LogisticName {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Logistics_LogisticName;
            }
        }

        public string DataEditPanel_Text_Logistics_OrderCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Logistics_OrderCode;
            }
        }

        public string DataEditPanel_Text_Logistics_ReceivingAddress {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Logistics_ReceivingAddress;
            }
        }

        public string DataEditPanel_Text_Logistics_RequestedArrivalDate {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Logistics_RequestedArrivalDate;
            }
        }

        public string DataEditPanel_Text_Logistics_ShippingCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingCode;
            }
        }

        public string DataEditPanel_Text_Logistics_ShippingDate {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingDate;
            }
        }

        public string DataEditPanel_Text_Logistics_ShippingMethod {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingMethod;
            }
        }

        public string DataEditPanel_Text_Logistics_TransportDriverPhone {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Logistics_TransportDriverPhone;
            }
        }

        public string DataEditPanel_Text_Logistics_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Logistics_WarehouseName;
            }
        }

        public string DataEditView_Text_PartsInboundReceipt_CounterpartCompanyCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyCode;
            }
        }

        public string DataEditView_Text_PartsInboundReceipt_CounterpartCompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyName;
            }
        }

        public string DataEditView_Text_PartsInboundReceipt_PartsInboundPlanCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_PartsInboundPlanCode;
            }
        }

        public string DataEditView_Text_PartsInboundReceipt_PartsSalesCategoryName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_PartsSalesCategoryName;
            }
        }

        public string DataEditView_Text_PartsInboundReceipt_ShippingCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_ShippingCode;
            }
        }

        public string DataEditView_Text_PartsInboundReceipt_WarehouseCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_WarehouseCode;
            }
        }

        public string DataEditView_Text_PartsInboundReceipt_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_WarehouseName;
            }
        }

        public string DataManagementView_Title_PartsShelvesTask {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PartsShelvesTask;
            }
        }

        public string QueryPanel_Title_PartsShelvesTask {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsShelvesTask;
            }
        }

        public string ActionPanel_Title_PackingTask {
            get {
                return Resources.PartsStockingUIStrings.ActionPanel_Title_PackingTask;
            }
        }

        public string Action_Title_PackingTask {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_PackingTask;
            }
        }

        public string QueryPanel_Title_PackingTask {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PackingTask;
            }
        }

        public string PackingTask_PartsInboundPlanCode {
            get {
                return Resources.PartsStockingUIStrings.PackingTask_PartsInboundPlanCode;
            }
        }

        public string Action_Title_PartsOutboundPlanQuery_Pick {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_PartsOutboundPlanQuery_Pick;
            }
        }

        public string Action_Title_PartsOutboundPlanQuery_Title {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_PartsOutboundPlanQuery_Title;
            }
        }

        public string DataManagementView_Title_BoxUpTask {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_BoxUpTask;
            }
        }

        public string ActionPanel_Title_PickingTask {
            get {
                return Resources.PartsStockingUIStrings.ActionPanel_Title_PickingTask;
            }
        }

        public string DataEditView_Validation_PckingTask_CurrentQuntity_IsMustLessThanPlannedAmount {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PckingTask_CurrentQuntity_IsMustLessThanPlannedAmount;
            }
        }

        public string QueryPanel_Title_PickingTask {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PickingTask;
            }
        }

        public string ActionPanel_Title_PickingTaskQuery {
            get {
                return Resources.PartsStockingUIStrings.ActionPanel_Title_PickingTaskQuery;
            }
        }

        public string DataEditView_Title_PartsInboundPlan_ERPSourceOrderCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartsInboundPlan_ERPSourceOrderCode;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryBill_InitiatorName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_InitiatorName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryBill_InventoryReason {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_InventoryReason;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryBill_RejectComment {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_RejectComment;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryBill_Remark {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_Remark;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInventoryBill_Status {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_Status;
            }
        }

        public string Action_Title_MergeExport {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_MergeExport;
            }
        }

        public string Action_Title_Send {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Send;
            }
        }

        public string Action_Title_Sending {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Sending;
            }
        }

        public string ActionPanel_Title_BusinessOperation {
            get {
                return Resources.PartsStockingUIStrings.ActionPanel_Title_BusinessOperation;
            }
        }

        public string ActionPanel_Title_Common {
            get {
                return Resources.PartsStockingUIStrings.ActionPanel_Title_Common;
            }
        }

        public string ActionPanel_Title_DealerPartsInventoryBill {
            get {
                return Resources.PartsStockingUIStrings.ActionPanel_Title_DealerPartsInventoryBill;
            }
        }

        public string ActionPanel_Title_InOutHistoryQuery {
            get {
                return Resources.PartsStockingUIStrings.ActionPanel_Title_InOutHistoryQuery;
            }
        }

        public string ActionPanel_Title_PartsShelvesTask {
            get {
                return Resources.PartsStockingUIStrings.ActionPanel_Title_PartsShelvesTask;
            }
        }

        public string ActionPanel_Title_Pick {
            get {
                return Resources.PartsStockingUIStrings.ActionPanel_Title_Pick;
            }
        }

        public string ActionPanel_Title_Shelves {
            get {
                return Resources.PartsStockingUIStrings.ActionPanel_Title_Shelves;
            }
        }

        public string Action_Title_A4Print {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_A4Print;
            }
        }

        public string Action_Title_Abandon {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Abandon;
            }
        }

        public string Action_Title_Add {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Add;
            }
        }

        public string Action_Title_AgencyPrint {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_AgencyPrint;
            }
        }

        public string Action_Title_Allot {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Allot;
            }
        }

        public string Action_Title_BatchReceipt {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_BatchReceipt;
            }
        }

        public string Action_Title_ChPrint {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_ChPrint;
            }
        }

        public string Action_Title_CreateBoxUpTask {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_CreateBoxUpTask;
            }
        }

        public string Action_Title_Delete {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Delete;
            }
        }

        public string Action_Title_DeliverParts2 {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_DeliverParts2;
            }
        }

        public string Action_Title_DetailExport {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_DetailExport;
            }
        }

        public string Action_Title_DetailView {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_DetailView;
            }
        }

        public string Action_Title_DoBoxUp {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_DoBoxUp;
            }
        }

        public string Action_Title_Edit {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Edit;
            }
        }

        public string Action_Title_EnPrint {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_EnPrint;
            }
        }

        public string Action_Title_ExactExport {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_ExactExport;
            }
        }

        public string Action_Title_HistoricalStorage {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_HistoricalStorage;
            }
        }

        public string Action_Title_Input {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Input;
            }
        }

        public string Action_Title_InputByDeputy {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_InputByDeputy;
            }
        }

        public string Action_Title_LablePrint {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_LablePrint;
            }
        }

        public string Action_Title_LablePrintOld {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_LablePrintOld;
            }
        }

        public string Action_Title_LocketUnit {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_LocketUnit;
            }
        }

        public string Action_Title_NotPartitionPrint {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_NotPartitionPrint;
            }
        }

        public string Action_Title_PartsStockSync {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_PartsStockSync;
            }
        }

        public string Action_Title_PcEnPrint {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_PcEnPrint;
            }
        }

        public string Action_Title_PcPrint {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_PcPrint;
            }
        }

        public string Action_Title_Picking {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Picking;
            }
        }

        public string Action_Title_PrintByArea {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_PrintByArea;
            }
        }

        public string Action_Title_PrintNoPrice {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_PrintNoPrice;
            }
        }

        public string Action_Title_PrintReceipt {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_PrintReceipt;
            }
        }

        public string Action_Title_PrintWarehouseArea {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_PrintWarehouseArea;
            }
        }

        public string Action_Title_PrintWithStandard {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_PrintWithStandard;
            }
        }

        public string Action_Title_ReturnApprove {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_ReturnApprove;
            }
        }

        public string Action_Title_ReturnSubmit {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_ReturnSubmit;
            }
        }

        public string Action_Title_Stop {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Stop;
            }
        }

        public string Action_Title_Submit {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Submit;
            }
        }

        public string Action_Title_WMSLablePrint {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_WMSLablePrint;
            }
        }

        public string Action_Title_WMSLablePrintForOther {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_WMSLablePrintForOther;
            }
        }

        public string Action_Title_WMSLablePrintOld {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_WMSLablePrintOld;
            }
        }

        public string DataEditPanel_Text_AgencyWarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_AgencyWarehouseName;
            }
        }

        public string DataEditPanel_Text_ApproveComment {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_ApproveComment;
            }
        }

        public string DataEditPanel_Text_ApproveNotPassed {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_ApproveNotPassed;
            }
        }

        public string DataEditPanel_Text_ApprovePassed {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_ApprovePassed;
            }
        }

        public string DataEditPanel_Text_ChooseExpressCompanyCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_ChooseExpressCompanyCode;
            }
        }

        public string DataEditPanel_Text_ClosedLoop {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_ClosedLoop;
            }
        }

        public string DataEditPanel_Text_Dealer_Code {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Dealer_Code;
            }
        }

        public string DataEditPanel_Text_Dealer_Name {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Dealer_Name;
            }
        }

        public string DataEditPanel_Text_ExpectedPlaceDate {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_ExpectedPlaceDate;
            }
        }

        public string DataEditPanel_Text_ExpressCompanyCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_ExpressCompanyCode;
            }
        }

        public string DataEditPanel_Text_ExpressCompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_ExpressCompanyName;
            }
        }

        public string DataEditPanel_Text_HiddenMain {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_HiddenMain;
            }
        }

        public string DataEditPanel_Text_IsTransportLosses {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_IsTransportLosses;
            }
        }

        public string DataEditPanel_Text_PartsInventoryBill_Code {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_PartsInventoryBill_Code;
            }
        }

        public string DataEditPanel_Text_PartsShiftOrder_Code {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_PartsShiftOrder_Code;
            }
        }

        public string DataEditPanel_Text_TransferDirection {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_TransferDirection;
            }
        }

        public string DataEditPanel_Text_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_WarehouseName;
            }
        }

        public string DataEditPanel_Title_AgencyPartsInventoryDetail {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Title_AgencyPartsInventoryDetail;
            }
        }

        public string DataEditPanel_Title_BoxupTaskDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Title_BoxupTaskDetails;
            }
        }

        public string DataEditPanel_Title_DealerPartsTransOrderDetail {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Title_DealerPartsTransOrderDetail;
            }
        }

        public string DataEditPanel_Title_LogisticsDetail {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Title_LogisticsDetail;
            }
        }

        public string DataEditPanel_Title_PackingTaskDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Title_PackingTaskDetails;
            }
        }

        public string DataEditPanel_Title_PartTask {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Title_PartTask;
            }
        }

        public string DataEditPanel_Title_PickingTaskDetail {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Title_PickingTaskDetail;
            }
        }

        public string DataEditPanel_Title_ShippingInfomation {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Title_ShippingInfomation;
            }
        }

        public string DataEditPanel_Title_Volume {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Title_Volume;
            }
        }

        public string DataEditPanel_Title_WarehouseAreaManager {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Title_WarehouseAreaManager;
            }
        }

        public string DataEditView_Text_ArrivalTime {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_ArrivalTime;
            }
        }

        public string DataEditView_Text_ArrivalTimeGreaterThan {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_ArrivalTimeGreaterThan;
            }
        }

        public string DataEditView_Text_ArrivalTimeIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_ArrivalTimeIsNull;
            }
        }

        public string DataEditView_Text_CargoTerminalMsg {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_CargoTerminalMsg;
            }
        }

        public string DataEditView_Text_ExportTemplate {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_ExportTemplate;
            }
        }

        public string DataEditView_Text_FlowFeedback {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_FlowFeedback;
            }
        }

        public string DataEditView_Text_GPSCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_GPSCode;
            }
        }

        public string DataEditView_Text_ImportFile {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_ImportFile;
            }
        }

        public string DataEditView_Text_ShippingTime {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_ShippingTime;
            }
        }

        public string DataEditView_Title_BatchConfirmDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_BatchConfirmDetails;
            }
        }

        public string DataEditView_Validation_ArrivalModeIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ArrivalModeIsNull;
            }
        }

        public string DataEditView_Validation_GPSCodeIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_GPSCodeIsNull;
            }
        }

        public string DataEditView_Validation_RequestedArrivalDateIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_RequestedArrivalDateIsNull;
            }
        }

        public string DataEditView_Validation_ShippingTimeError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ShippingTimeError;
            }
        }

        public string DataEditView_Validation_ShippingTimeIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ShippingTimeIsNull;
            }
        }

        public string QueryPanel_QueryItem_AgencyCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_AgencyCode;
            }
        }

        public string QueryPanel_QueryItem_AgencyName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_AgencyName;
            }
        }

        public string QueryPanel_QueryItem_AgencyStockQuery {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_AgencyStockQuery;
            }
        }

        public string QueryPanel_QueryItem_ApproveTime {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_ApproveTime;
            }
        }

        public string QueryPanel_QueryItem_BranchName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_BranchName;
            }
        }

        public string QueryPanel_QueryItem_BusinessCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_BusinessCode;
            }
        }

        public string QueryPanel_QueryItem_CheckStatus {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_CheckStatus;
            }
        }

        public string QueryPanel_QueryItem_ClaimBillCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_ClaimBillCode;
            }
        }

        public string QueryPanel_QueryItem_CompanyName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_CompanyName;
            }
        }

        public string QueryPanel_QueryItem_CPPartsInboundCheckCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_CPPartsInboundCheckCode;
            }
        }

        public string QueryPanel_QueryItem_CPPartsPurchaseOrderCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_CPPartsPurchaseOrderCode;
            }
        }

        public string QueryPanel_QueryItem_GreaterThanZero {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_GreaterThanZero;
            }
        }

        public string QueryPanel_QueryItem_InboundBill_Code {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_InboundBill_Code;
            }
        }

        public string QueryPanel_QueryItem_InboundType {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_InboundType;
            }
        }

        public string QueryPanel_QueryItem_InLine {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_InLine;
            }
        }

        public string QueryPanel_QueryItem_InvoiceType {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_InvoiceType;
            }
        }

        public string QueryPanel_QueryItem_IsAgencyQuery {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_IsAgencyQuery;
            }
        }

        public string QueryPanel_QueryItem_LogisticsDetail {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_LogisticsDetail;
            }
        }

        public string QueryPanel_QueryItem_NoWarehouseAreaTask {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_NoWarehouseAreaTask;
            }
        }

        public string QueryPanel_QueryItem_OriginalRequirementBillCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_OriginalRequirementBillCode;
            }
        }

        public string QueryPanel_QueryItem_PackingFinishTime {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_PackingFinishTime;
            }
        }

        public string QueryPanel_QueryItem_PackingTaskCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_PackingTaskCode;
            }
        }

        public string QueryPanel_QueryItem_PartsInboundCheckBillCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_PartsInboundCheckBillCode;
            }
        }

        public string QueryPanel_QueryItem_PartsInboundPlanCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_PartsInboundPlanCode;
            }
        }

        public string QueryPanel_QueryItem_PartsShelvesFinishTime {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_PartsShelvesFinishTime;
            }
        }

        public string QueryPanel_QueryItem_PartsStockGreaterThanZero {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_PartsStockGreaterThanZero;
            }
        }

        public string QueryPanel_QueryItem_PartsSupplierCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_PartsSupplierCode;
            }
        }

        public string QueryPanel_QueryItem_PurchaseOrderType {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_PurchaseOrderType;
            }
        }

        public string QueryPanel_QueryItem_QueryTime {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_QueryTime;
            }
        }

        public string QueryPanel_QueryItem_Received {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Received;
            }
        }

        public string QueryPanel_QueryItem_ReferenceCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_ReferenceCode;
            }
        }

        public string QueryPanel_QueryItem_SignStatus {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_SignStatus;
            }
        }

        public string QueryPanel_QueryItem_SupplierCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_SupplierCode;
            }
        }

        public string QueryPanel_QueryItem_SupplierName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_SupplierName;
            }
        }

        public string QueryPanel_QueryItem_Type {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Type;
            }
        }

        public string QueryPanel_QueryItem_WarehouseAreaCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_WarehouseAreaCode;
            }
        }

        public string QueryPanel_QueryItem_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_WarehouseName;
            }
        }

        public string QueryPanel_Title_AgencyInHistoryQuery {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_AgencyInHistoryQuery;
            }
        }

        public string QueryPanel_Title_AgencyOutHistoryQuery {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_AgencyOutHistoryQuery;
            }
        }

        public string QueryPanel_Title_AgencyPartsInventoryBill {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_AgencyPartsInventoryBill;
            }
        }

        public string QueryPanel_Title_AgencyPartsStockQuery {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_AgencyPartsStockQuery;
            }
        }

        public string QueryPanel_Title_BoxUpFinishTime {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_BoxUpFinishTime;
            }
        }

        public string QueryPanel_Title_BoxUpTask {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_BoxUpTask;
            }
        }

        public string QueryPanel_Title_BoxUpTask_Code {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_BoxUpTask_Code;
            }
        }

        public string QueryPanel_Title_ClosedLoop {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_ClosedLoop;
            }
        }

        public string QueryPanel_Title_ClosedLoopStatus {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_ClosedLoopStatus;
            }
        }

        public string QueryPanel_Title_ContractCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_ContractCode;
            }
        }

        public string QueryPanel_Title_DealerPartsTransferOrder {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_DealerPartsTransferOrder;
            }
        }

        public string QueryPanel_Title_EnterprisePartsCost {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_EnterprisePartsCost;
            }
        }

        public string QueryPanel_Title_ERPsourceCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_ERPsourceCode;
            }
        }

        public string QueryPanel_Title_ERPSourceOrderCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_ERPSourceOrderCode;
            }
        }

        public string QueryPanel_Title_InboundHistoryQuery {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_InboundHistoryQuery;
            }
        }

        public string QueryPanel_Title_InOutHistoryQuery {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_InOutHistoryQuery;
            }
        }

        public string QueryPanel_Title_NoClosedLoop {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_NoClosedLoop;
            }
        }

        public string QueryPanel_Title_NoWarrantyBrandName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_NoWarrantyBrandName;
            }
        }

        public string QueryPanel_Title_OutboundPackPlanCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_OutboundPackPlanCode;
            }
        }

        public string QueryPanel_Title_OutboundType {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_OutboundType;
            }
        }

        public string QueryPanel_Title_OutWarehouse {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_OutWarehouse;
            }
        }

        public string QueryPanel_Title_PartABC {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartABC;
            }
        }

        public string QueryPanel_Title_PartsOutboundPlan_Code {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsOutboundPlan_Code;
            }
        }

        public string QueryPanel_Title_PartsStockChangeLog {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsStockChangeLog;
            }
        }

        public string QueryPanel_Title_PartsStockQueryForTransfer {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsStockQueryForTransfer;
            }
        }

        public string QueryPanel_Title_PartsTransferOrderStock {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsTransferOrderStock;
            }
        }

        public string QueryPanel_Title_PersonelName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PersonelName;
            }
        }

        public string QueryPanel_Title_Pickingfinishtime {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_Pickingfinishtime;
            }
        }

        public string QueryPanel_Title_PickingTaskCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PickingTaskCode;
            }
        }

        public string QueryPanel_Title_PostSaleClaimsTmp {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PostSaleClaimsTmp;
            }
        }

        public string QueryPanel_Title_ProvinceName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_ProvinceName;
            }
        }

        public string QueryPanel_Title_SalesOrderType {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_SalesOrderType;
            }
        }

        public string QueryPanel_Title_SAPPurchasePlanCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_SAPPurchasePlanCode;
            }
        }

        public string QueryPanel_Title_SettlementStatus {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_SettlementStatus;
            }
        }

        public string QueryPanel_Title_SourceCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_SourceCode;
            }
        }

        public string QueryPanel_Title_Status {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_Status;
            }
        }

        public string QueryPanel_Title_WarehouseArea2 {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_WarehouseArea2;
            }
        }

        public string QueryPanel_Title_WarehouseName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_WarehouseName;
            }
        }

        public string QueryPanel_Title_WarrantyBrandName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_WarrantyBrandName;
            }
        }

        public string QueryPanel_Title_ZpNumber {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_ZpNumber;
            }
        }

        public string DataEditView_Title_BorrowBillDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_BorrowBillDetails;
            }
        }

        public string DataEditView_Title_ContactMethod {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ContactMethod;
            }
        }

        public string DataEditView_Title_ImportWarehouseArea {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ImportWarehouseArea;
            }
        }

        public string DataEditView_Title_RejectAgencyPartsInventoryBill {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_RejectAgencyPartsInventoryBill;
            }
        }

        public string DataEditView_Validation_ApproveCommentIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ApproveCommentIsNull;
            }
        }

        public string DataEditView_Validation_RejectReasonIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_RejectReasonIsNull;
            }
        }

        public string DataEditView_Text_ContainerNumber {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_ContainerNumber;
            }
        }

        public string DataEditView_Text_CounterpartCompanyCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_CounterpartCompanyCode;
            }
        }

        public string DataEditView_Text_IsBoxUpOver {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_IsBoxUpOver;
            }
        }

        public string DataEditView_Text_No {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_No;
            }
        }

        public string DataEditView_Text_Outbound {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_Outbound;
            }
        }

        public string DataEditView_Text_PickFinished {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PickFinished;
            }
        }

        public string DataEditView_Text_PickingTaskCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PickingTaskCode;
            }
        }

        public string DataEditView_Text_Refrech {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_Refrech;
            }
        }

        public string DataEditView_Text_Yes {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_Yes;
            }
        }

        public string DataEditView_Title_AbandonDealerPartsInventoryBill {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_AbandonDealerPartsInventoryBill;
            }
        }

        public string DataEditView_Title_ApproveDealerPartsInventoryBillDetail {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ApproveDealerPartsInventoryBillDetail;
            }
        }

        public string DataEditView_Title_BoxUp {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_BoxUp;
            }
        }

        public string DataEditView_Title_DealerPartsInventoryBillDetail {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_DealerPartsInventoryBillDetail;
            }
        }

        public string DataEditView_Title_DealerPartsInventoryBillError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_DealerPartsInventoryBillError;
            }
        }

        public string DataEditView_Title_FinalApproveDealerPartsInventoryBillDetail {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_FinalApproveDealerPartsInventoryBillDetail;
            }
        }

        public string DataEditView_Title_ImportDealerPartsInventoryBill {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ImportDealerPartsInventoryBill;
            }
        }

        public string DataEditView_Title_ImportDealerPartsInventoryBillDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ImportDealerPartsInventoryBillDetails;
            }
        }

        public string DataEditView_Title_InitialApproveDealerPartsInventoryBillDetail {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_InitialApproveDealerPartsInventoryBillDetail;
            }
        }

        public string DataEditView_Title_PartsBoxUpTask {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartsBoxUpTask;
            }
        }

        public string DataEditView_Validation_AbandonReasonIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_AbandonReasonIsNull;
            }
        }

        public string DataEditView_Validation_BeforeQtyIsZero {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_BeforeQtyIsZero;
            }
        }

        public string DataEditView_Validation_BoxUpTaskDetailsIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_BoxUpTaskDetailsIsNull;
            }
        }

        public string DataEditView_Validation_CantGreaterThan {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_CantGreaterThan;
            }
        }

        public string DataEditView_Validation_ConfirmBeforeQtyGreaterThanConfirmingQty {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ConfirmBeforeQtyGreaterThanConfirmingQty;
            }
        }

        public string DataEditView_Validation_ConfirmedAmountAllError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ConfirmedAmountAllError;
            }
        }

        public string DataEditView_Validation_ContainerNumberIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ContainerNumberIsNull;
            }
        }

        public string DataEditView_Validation_NowBoxUpQuantityAllZero {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_NowBoxUpQuantityAllZero;
            }
        }

        public string DataEditView_Validation_NowOutboundAmountIsZero {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_NowOutboundAmountIsZero;
            }
        }

        public string DataEditView_Validation_PickingTaskError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PickingTaskError;
            }
        }

        public string DataEditView_Validation_PickingTaskIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PickingTaskIsNull;
            }
        }

        public string DataEditView_Text_ChooseCompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_ChooseCompanyName;
            }
        }

        public string DataEditView_Text_PrintPartLable {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PrintPartLable;
            }
        }

        public string DataEditView_Text_SalesOrderType {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_SalesOrderType;
            }
        }

        public string DataEditView_Title_A {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_A;
            }
        }

        public string DataEditView_Title_ActivePackingMaterial {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ActivePackingMaterial;
            }
        }

        public string DataEditView_Title_ApproveDealerPartsTransferOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ApproveDealerPartsTransferOrder;
            }
        }

        public string DataEditView_Title_ApproveOpinion {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ApproveOpinion;
            }
        }

        public string DataEditView_Title_BranchIdIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_BranchIdIsNull;
            }
        }

        public string DataEditView_Title_ClearOnBranchChange {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ClearOnBranchChange;
            }
        }

        public string DataEditView_Title_ClearOnNoWarrantyBrandChange {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ClearOnNoWarrantyBrandChange;
            }
        }

        public string DataEditView_Title_ClearOnWarrantyBrandChange {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ClearOnWarrantyBrandChange;
            }
        }

        public string DataEditView_Title_DiffPrice {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_DiffPrice;
            }
        }

        public string DataEditView_Title_DiffPrice2 {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_DiffPrice2;
            }
        }

        public string DataEditView_Title_ExportTemplate {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ExportTemplate;
            }
        }

        public string DataEditView_Title_FinishOverstock {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_FinishOverstock;
            }
        }

        public string DataEditView_Title_Import {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_Import;
            }
        }

        public string DataEditView_Title_ImportExpress {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ImportExpress;
            }
        }

        public string DataEditView_Title_ImportExpressDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ImportExpressDetails;
            }
        }

        public string DataEditView_Title_ImportExpressToLogisticDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ImportExpressToLogisticDetails;
            }
        }

        public string DataEditView_Title_ImportExpressToLogistics {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ImportExpressToLogistics;
            }
        }

        public string DataEditView_Title_NoWarrantyBrandIdIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_NoWarrantyBrandIdIsNull;
            }
        }

        public string DataEditView_Title_NoWarrantyBrandName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_NoWarrantyBrandName;
            }
        }

        public string DataEditView_Title_OutboundHasPackingFinish {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_OutboundHasPackingFinish;
            }
        }

        public string DataEditView_Title_Packing {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_Packing;
            }
        }

        public string DataEditView_Title_PackingMaterial {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PackingMaterial;
            }
        }

        public string DataEditView_Title_PackingNumber {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PackingNumber;
            }
        }

        public string DataEditView_Title_PackingOne {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PackingOne;
            }
        }

        public string DataEditView_Title_PackingQty {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PackingQty;
            }
        }

        public string DataEditView_Title_PackingTask {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PackingTask;
            }
        }

        public string DataEditView_Title_PackingTaskCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PackingTaskCode;
            }
        }

        public string DataEditView_Title_PackingTaskInformation {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PackingTaskInformation;
            }
        }

        public string DataEditView_Title_PackingThree {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PackingThree;
            }
        }

        public string DataEditView_Title_PackingTwo {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PackingTwo;
            }
        }

        public string DataEditView_Title_PlanAmount {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PlanAmount;
            }
        }

        public string DataEditView_Title_PlanPackingMaterial {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PlanPackingMaterial;
            }
        }

        public string DataEditView_Title_ProductCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ProductCode;
            }
        }

        public string DataEditView_Title_ProductInformation {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ProductInformation;
            }
        }

        public string DataEditView_Title_ProductInformationIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ProductInformationIsNull;
            }
        }

        public string DataEditView_Title_RejectOverstock {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_RejectOverstock;
            }
        }

        public string DataEditView_Title_RejectReasonIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_RejectReasonIsNull;
            }
        }

        public string DataEditView_Title_ScanNumber {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ScanNumber;
            }
        }

        public string DataEditView_Title_ScanProductAndPressEnter {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ScanProductAndPressEnter;
            }
        }

        public string DataEditView_Title_SourceCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_SourceCode;
            }
        }

        public string DataEditView_Title_WaitRepairQuery {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_WaitRepairQuery;
            }
        }

        public string DataEditView_Title_WarrantyBrandIdIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_WarrantyBrandIdIsNull;
            }
        }

        public string DataEditView_Title_WarrantyBrandName {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_WarrantyBrandName;
            }
        }

        public string DataEditView_Validation_ApproveOpinionIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ApproveOpinionIsNull;
            }
        }

        public string DataEditView_Validation_DetailsAmountIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_DetailsAmountIsNull;
            }
        }

        public string DataEditView_Validation_EmaiError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_EmaiError;
            }
        }

        public string DataEditView_Validation_FinishQuantityError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_FinishQuantityError;
            }
        }

        public string DataEditView_Validation_HasRepeatPart {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_HasRepeatPart;
            }
        }

        public string DataEditView_Validation_PartNoSalePrice {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PartNoSalePrice;
            }
        }

        public string DataEditView_Validation_PartsPackingPropIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PartsPackingPropIsNull;
            }
        }

        public string DataEditView_Validation_PartsTransferOrderDetailsInNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrderDetailsInNull;
            }
        }

        public string DataEditView_Validation_RejectDealerPartsTransferOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_RejectDealerPartsTransferOrder;
            }
        }

        public string DataEditView_Validation_SparePartCodeHasError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_SparePartCodeHasError;
            }
        }

        public string DataEditView_Validation_SparePartCodeHasError2 {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_SparePartCodeHasError2;
            }
        }

        public string DataEditView_Validation_SparePartHasRepeat {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_SparePartHasRepeat;
            }
        }

        public string DataEditView_Validation_ThisPackingQuantityError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ThisPackingQuantityError;
            }
        }

        public string DataEditView_Validation_ThisPackingQuantityError2 {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ThisPackingQuantityError2;
            }
        }

        public string DataEditView_Validation_ThisPackingTaskIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ThisPackingTaskIsNull;
            }
        }

        public string DataEditView_Validation_TimeIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_TimeIsNull;
            }
        }

        public string DataEditView_Validation_TransferQuantityError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_TransferQuantityError;
            }
        }

        public string DataEditView_Notification_ChooseOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Notification_ChooseOrder;
            }
        }

        public string DataEditView_Notification_ChoosePart {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Notification_ChoosePart;
            }
        }

        public string DataEditView_Notification_IsPrintOutbound {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Notification_IsPrintOutbound;
            }
        }

        public string DataEditView_Notification_OnlyDeleteStockWithQuantityIsZero {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Notification_OnlyDeleteStockWithQuantityIsZero;
            }
        }

        public string DataEditView_Text_FinalApprovePartsShiftOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_FinalApprovePartsShiftOrder;
            }
        }

        public string DataEditView_Text_InitApprovePartsShiftOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_InitApprovePartsShiftOrder;
            }
        }

        public string DataEditView_Text_IsPrintShelves {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_IsPrintShelves;
            }
        }

        public string DataEditView_Text_PartsShiftOrderQuestionType {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShiftOrderQuestionType;
            }
        }

        public string DataEditView_Text_PartsShiftOrderQuestionTypeIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShiftOrderQuestionTypeIsNull;
            }
        }

        public string DataEditView_Text_PartsShippingOrderPrint {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShippingOrderPrint;
            }
        }

        public string DataEditView_Text_PrintOutbound {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PrintOutbound;
            }
        }

        public string DataEditView_Text_PrintShelves {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PrintShelves;
            }
        }

        public string DataEditView_Text_ReceiptPartDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_ReceiptPartDetails;
            }
        }

        public string DataEditView_Title_ImportBatchShippingConfirm {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ImportBatchShippingConfirm;
            }
        }

        public string DataEditView_Title_ImportBatchShippingConfirmDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_ImportBatchShippingConfirmDetails;
            }
        }

        public string DataEditView_Title_InitApprovePartsInventoryBill {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_InitApprovePartsInventoryBill;
            }
        }

        public string DataEditView_Title_TerminateOutboundPlan {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_TerminateOutboundPlan;
            }
        }

        public string DataEditView_Title_UpdateMainOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_UpdateMainOrder;
            }
        }

        public string DataEditView_Validation_DetailQuantityGreaterThanZero {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_DetailQuantityGreaterThanZero;
            }
        }

        public string DataEditView_Validation_DifferenceClassificationIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_DifferenceClassificationIsNull;
            }
        }

        public string DataEditView_Validation_ImportBatchArrived {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ImportBatchArrived;
            }
        }

        public string DataEditView_Validation_ImportBatchArrivedDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_ImportBatchArrivedDetails;
            }
        }

        public string DataEditView_Validation_OutboundWarehouseNeedRefresh {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_OutboundWarehouseNeedRefresh;
            }
        }

        public string DataEditView_Validation_PartsShelvesTaskIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PartsShelvesTaskIsNull;
            }
        }

        public string DataEditView_Validation_PartsShippingOrderShippingError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrderShippingError;
            }
        }

        public string DataEditView_Validation_RequestedArrivalDateIsNotNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_RequestedArrivalDateIsNotNull;
            }
        }

        public string DataEditView_Validation_TerminateReasonIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_TerminateReasonIsNull;
            }
        }

        public string DataEditView_Validation_WarehouseAreaCodeIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_WarehouseAreaCodeIsNull;
            }
        }

        public string DataEditView_Text_BigLableChPrint {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_BigLableChPrint;
            }
        }

        public string DataEditView_Text_CheckDetailsInfo {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_CheckDetailsInfo;
            }
        }

        public string DataEditView_Text_Clear {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_Clear;
            }
        }

        public string DataEditView_Text_FinishPicking {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_FinishPicking;
            }
        }

        public string DataEditView_Text_InboundPlanDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_InboundPlanDetails;
            }
        }

        public string DataEditView_Text_OperationSuccess {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_OperationSuccess;
            }
        }

        public string DataEditView_Text_PartDetailInfo {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartDetailInfo;
            }
        }

        public string DataEditView_Text_PrintSparePartLable {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PrintSparePartLable;
            }
        }

        public string DataEditView_Text_RejectSuccess {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_RejectSuccess;
            }
        }

        public string DataEditView_Text_WarehouseArea {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_WarehouseArea;
            }
        }

        public string DataEditView_Title_AbandonPartsTransferOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_AbandonPartsTransferOrder;
            }
        }

        public string DataEditView_Title_NoDatas {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_NoDatas;
            }
        }

        public string DataEditView_Title_PickingTaskDetails {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PickingTaskDetails;
            }
        }

        public string DataEditView_Title_PleaseChoose {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PleaseChoose;
            }
        }

        public string DataEditView_Title_PleaseChooseShortPickingReason {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PleaseChooseShortPickingReason;
            }
        }

        public string DataEditView_Title_SureFinishPicking {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_SureFinishPicking;
            }
        }

        public string DataEditView_Validation_DetailsCountGreaterThanFourHundred {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_DetailsCountGreaterThanFourHundred;
            }
        }

        public string DataEditView_Validation_PartNoCostPrice {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PartNoCostPrice;
            }
        }

        public string DataEditView_Validation_PartUsableQuantityError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PartUsableQuantityError;
            }
        }

        public string DataEditView_Validation_PlanQuantityError {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_PlanQuantityError;
            }
        }

        public string DataGridView_ColumnItem_ConfirmQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ConfirmQuantity;
            }
        }

        public string DataGridView_ColumnItem_ConsigneeName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ConsigneeName;
            }
        }

        public string DataGridView_ColumnItem_ConsigneeTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ConsigneeTime;
            }
        }

        public string DataGridView_ColumnItem_DifferenceClassification {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_DifferenceClassification;
            }
        }

        public string DataGridView_ColumnItem_InterfaceRecordId {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_InterfaceRecordId;
            }
        }

        public string DataGridView_ColumnItem_InTransitDamageLossAmount {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_InTransitDamageLossAmount;
            }
        }

        public string DataGridView_ColumnItem_OutboundWarehouseName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_OutboundWarehouseName;
            }
        }

        public string DataGridView_ColumnItem_Price {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Price;
            }
        }

        public string DataGridView_ColumnItem_ReceiptConfirmorName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ReceiptConfirmorName;
            }
        }

        public string DataGridView_ColumnItem_ReceiptConfirmTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ReceiptConfirmTime;
            }
        }

        public string DataGridView_ColumnItem_SettlementPrice {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_SettlementPrice;
            }
        }

        public string DataGridView_ColumnItem_ShippingDate {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ShippingDate;
            }
        }

        public string DataGridView_ColumnItem_ShippingQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ShippingQuantity;
            }
        }

        public string DataGridView_ColumnItem_ShippingTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ShippingTime;
            }
        }

        public string DataGridView_ColumnItem_Stoper {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Stoper;
            }
        }

        public string DataGridView_ColumnItem_StopTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_StopTime;
            }
        }

        public string DataGridView_ColumnItem_TerminateReason {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_TerminateReason;
            }
        }

        public string DataGridView_ColumnItem_TotalAmount {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount;
            }
        }

        public string DataGridView_ColumnItem_TransportVehiclePlate {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_TransportVehiclePlate;
            }
        }

        public string DataGridView_ColumnItem_Abandoner {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Abandoner;
            }
        }

        public string DataGridView_ColumnItem_AbandonTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_AbandonTime;
            }
        }

        public string DataGridView_ColumnItem_ActualAll {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ActualAll;
            }
        }

        public string DataGridView_ColumnItem_ActualAvailableStock {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ActualAvailableStock;
            }
        }

        public string DataGridView_ColumnItem_ApproverName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ApproverName;
            }
        }

        public string DataGridView_ColumnItem_ApproveTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ApproveTime;
            }
        }

        public string DataGridView_ColumnItem_BeforeQty {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_BeforeQty;
            }
        }

        public string DataGridView_ColumnItem_BoxUpQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_BoxUpQuantity;
            }
        }

        public string DataGridView_ColumnItem_CenterPrice {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_CenterPrice;
            }
        }

        public string DataGridView_ColumnItem_CompanyCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_CompanyCode;
            }
        }

        public string DataGridView_ColumnItem_ConfirmBeforeQty {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ConfirmBeforeQty;
            }
        }

        public string DataGridView_ColumnItem_ConfirmedReceptionTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ConfirmedReceptionTime;
            }
        }

        public string DataGridView_ColumnItem_CostAmount {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_CostAmount;
            }
        }

        public string DataGridView_ColumnItem_CostPrice {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_CostPrice;
            }
        }

        public string DataGridView_ColumnItem_Createtime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Createtime;
            }
        }

        public string DataGridView_ColumnItem_CurrentOutboundAmount {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_CurrentOutboundAmount;
            }
        }

        public string DataGridView_ColumnItem_CurrentStorage {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_CurrentStorage;
            }
        }

        public string DataGridView_ColumnItem_DealAmount {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_DealAmount;
            }
        }

        public string DataGridView_ColumnItem_DealerPrice {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_DealerPrice;
            }
        }

        public string DataGridView_ColumnItem_HauDistance {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_HauDistance;
            }
        }

        public string DataGridView_ColumnItem_InboundType {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_InboundType;
            }
        }

        public string DataGridView_ColumnItem_InventoryRecordOperatorName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_InventoryRecordOperatorName;
            }
        }

        public string DataGridView_ColumnItem_InventoryRecordTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_InventoryRecordTime;
            }
        }

        public string DataGridView_ColumnItem_IsExistShippingOrder {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_IsExistShippingOrder;
            }
        }

        public string DataGridView_ColumnItem_MeasureUnit {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_MeasureUnit;
            }
        }

        public string DataGridView_ColumnItem_MemoRemark {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_MemoRemark;
            }
        }

        public string DataGridView_ColumnItem_NowBoxUpQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_NowBoxUpQuantity;
            }
        }

        public string DataGridView_ColumnItem_OutTotalAmount {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_OutTotalAmount;
            }
        }

        public string DataGridView_ColumnItem_PackingMaterial {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PackingMaterial;
            }
        }

        public string DataGridView_ColumnItem_PackingQty {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PackingQty;
            }
        }

        public string DataGridView_ColumnItem_PackNum {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PackNum;
            }
        }

        public string DataGridView_ColumnItem_PartABC {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PartABC;
            }
        }

        public string DataGridView_ColumnItem_PartsInboundCheckBillCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PartsInboundCheckBillCode;
            }
        }

        public string DataGridView_ColumnItem_PointName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PointName;
            }
        }

        public string DataGridView_ColumnItem_PointTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PointTime;
            }
        }

        public string DataGridView_ColumnItem_QuantityAll {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_QuantityAll;
            }
        }

        public string DataGridView_ColumnItem_RejectComment {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_RejectComment;
            }
        }

        public string DataGridView_ColumnItem_RejecterName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_RejecterName;
            }
        }

        public string DataGridView_ColumnItem_RejectTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_RejectTime;
            }
        }

        public string DataGridView_ColumnItem_ScanName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ScanName;
            }
        }

        public string DataGridView_ColumnItem_ScanTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ScanTime;
            }
        }

        public string DataGridView_ColumnItem_Signatory {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Signatory;
            }
        }

        public string DataGridView_ColumnItem_SihCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_SihCode;
            }
        }

        public string DataGridView_ColumnItem_StockQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_StockQuantity;
            }
        }

        public string DataGridView_ColumnItem_StorageAfterInventory {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_StorageAfterInventory;
            }
        }

        public string DataGridView_ColumnItem_StorageDifference {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_StorageDifference;
            }
        }

        public string DataGridView_ColumnItem_SumCostAfterInventory {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_SumCostAfterInventory;
            }
        }

        public string DataGridView_ColumnItem_SumCostBeforeInventory {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_SumCostBeforeInventory;
            }
        }

        public string DataGridView_ColumnItem_SumCostDifference {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_SumCostDifference;
            }
        }

        public string DataGridView_ColumnItem_TaskQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_TaskQuantity;
            }
        }

        public string DataGridView_ColumnItem_TotalJH {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_TotalJH;
            }
        }

        public string DataGridView_ColumnItem_TradePrice {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_TradePrice;
            }
        }

        public string DataGridView_Notification_WaitShipping {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Notification_WaitShipping;
            }
        }

        public string DataGridView_Validation_DealPriceValidation1 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Validation_DealPriceValidation1;
            }
        }

        public string DataGridView_Validation_DealPriceValidation2 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Validation_DealPriceValidation2;
            }
        }

        public string DataGridView_Validation_NowBoxUpQuantityError1 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Validation_NowBoxUpQuantityError1;
            }
        }

        public string DataGridView_Validation_NowBoxUpQuantityError2 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Validation_NowBoxUpQuantityError2;
            }
        }

        public string DataGridView_ColumnItem_PhyPackingMaterial {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PhyPackingMaterial;
            }
        }

        public string DataGridView_ColumnItem_PhyQty {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PhyQty;
            }
        }

        public string DataGridView_ColumnItem_PickNum {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PickNum;
            }
        }

        public string DataGridView_ColumnItem_RecomPackingMaterial {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_RecomPackingMaterial;
            }
        }

        public string DataGridView_ColumnItem_RecomQty {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_RecomQty;
            }
        }

        public string DataEditView_Title_PartInformation {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartInformation;
            }
        }

        public string DataGridView_ColumnItem_AlreadyInspectedQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_AlreadyInspectedQuantity;
            }
        }

        public string DataGridView_ColumnItem_CheckerName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_CheckerName;
            }
        }

        public string DataGridView_ColumnItem_CheckTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_CheckTime;
            }
        }

        public string DataGridView_ColumnItem_InspectedQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_InspectedQuantity;
            }
        }

        public string DataGridView_ColumnItem_OriginalRequirementBillCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_OriginalRequirementBillCode;
            }
        }

        public string DataGridView_ColumnItem_OverseasPartsFigure {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_OverseasPartsFigure;
            }
        }

        public string DataGridView_ColumnItem_PlanOrderStatus {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PlanOrderStatus;
            }
        }

        public string DataGridView_ColumnItem_PlanSource {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_PlanSource;
            }
        }

        public string DataGridView_ColumnItem_RecordOperatorName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_RecordOperatorName;
            }
        }

        public string DataGridView_ColumnItem_RecordTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_RecordTime;
            }
        }

        public string DataGridView_ColumnItem_ReturnType {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ReturnType;
            }
        }

        public string DataGridView_ColumnItem_ShelvesAmount {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_ShelvesAmount;
            }
        }

        public string DataGridView_ColumnItem_SpareOrderRemark {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_SpareOrderRemark;
            }
        }

        public string DataGridView_ColumnItem_SparePart_MeasureUnit {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_SparePart_MeasureUnit;
            }
        }

        public string DataGridView_ColumnItem_SparePart_SettlementPrice {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_SparePart_SettlementPrice;
            }
        }

        public string DataGridView_ColumnItem_TaskCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_TaskCode;
            }
        }

        public string DataGridView_ColumnItem_TaskStatus {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_TaskStatus;
            }
        }

        public string DataGridView_ColumnItem_Terminater {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Terminater;
            }
        }

        public string DataGridView_ColumnItem_TerminateTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_TerminateTime;
            }
        }

        public string DataGridView_ColumnItem_TotalCount {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_TotalCount;
            }
        }

        public string DataGridView_Column_FinalApprover {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_FinalApprover;
            }
        }

        public string DataGridView_Column_FinalApproveTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_FinalApproveTime;
            }
        }

        public string DataGridView_Column_InitApprover {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_InitApprover;
            }
        }

        public string DataGridView_Column_InitApproveTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_InitApproveTime;
            }
        }

        public string DataGridView_Column_NowShelvesQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_NowShelvesQuantity;
            }
        }

        public string DataGridView_Column_PartsShelvesTaskCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_PartsShelvesTaskCode;
            }
        }

        public string DataGridView_Notification_NowShelvesQuantityError {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Notification_NowShelvesQuantityError;
            }
        }

        public string DataGridView_Notification_NowShelvesQuantityError2 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Notification_NowShelvesQuantityError2;
            }
        }

        public string DataGridView_Notification_WarehouseAreaHasSameData {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Notification_WarehouseAreaHasSameData;
            }
        }

        public string DataGridView_Title_WarehouseAreaQuery {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Title_WarehouseAreaQuery;
            }
        }

        public string DataManagementView_Confirm_AbandonExpress {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Confirm_AbandonExpress;
            }
        }

        public string DataManagementView_Confirm_ImportWarehouseAreaConfirm {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Confirm_ImportWarehouseAreaConfirm;
            }
        }

        public string DataManagementView_Confirm_ImportWarehouseConfirm {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Confirm_ImportWarehouseConfirm;
            }
        }

        public string DataManagementView_Confirm_PauseExpress {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Confirm_PauseExpress;
            }
        }

        public string DataManagementView_Confirm_ResumeExpress {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Confirm_ResumeExpress;
            }
        }

        public string DataManagementView_Confirm_SetManager {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Confirm_SetManager;
            }
        }

        public string DataManagementView_Confirm_SetSuccess {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Confirm_SetSuccess;
            }
        }

        public string DataManagementView_Confirm_StopBoxUp {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Confirm_StopBoxUp;
            }
        }

        public string DataManagementView_Notification_CategoryIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_CategoryIsNull;
            }
        }

        public string DataManagementView_Notification_ChooseNewPartsOutboundPlan {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_ChooseNewPartsOutboundPlan;
            }
        }

        public string DataManagementView_Notification_ChooseWarehouse {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_ChooseWarehouse;
            }
        }

        public string DataManagementView_Notification_ClearPartsShiftOrder {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_ClearPartsShiftOrder;
            }
        }

        public string DataManagementView_Notification_CreatePickingTaskSuccess {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_CreatePickingTaskSuccess;
            }
        }

        public string DataManagementView_Notification_DeleteSuccess {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_DeleteSuccess;
            }
        }

        public string DataManagementView_Notification_ImportWarehouseAreaValidation {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_ImportWarehouseAreaValidation;
            }
        }

        public string DataManagementView_Notification_NotAllowedAbandon {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_NotAllowedAbandon;
            }
        }

        public string DataManagementView_Notification_NotAllowedAbandonWarehouse {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_NotAllowedAbandonWarehouse;
            }
        }

        public string DataManagementView_Notification_NotAllowedUpdate {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_NotAllowedUpdate;
            }
        }

        public string DataManagementView_Notification_PartCodeRequired {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_PartCodeRequired;
            }
        }

        public string DataManagementView_Notification_PartsCodeIsNotNull {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_PartsCodeIsNotNull;
            }
        }

        public string DataManagementView_Notification_PartsOutboundPlanValidation {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_PartsOutboundPlanValidation;
            }
        }

        public string DataManagementView_Notification_QueryTimeIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_QueryTimeIsNull;
            }
        }

        public string DataManagementView_Notification_ShippingConfirmNotification {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_ShippingConfirmNotification;
            }
        }

        public string DataManagementView_Notification_ShippingConfirmSuccess {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_ShippingConfirmSuccess;
            }
        }

        public string DataManagementView_Text_BatchReceiptSuccess {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_BatchReceiptSuccess;
            }
        }

        public string DataManagementView_Text_ChoosePersonel {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_ChoosePersonel;
            }
        }

        public string DataManagementView_Text_HistoryOutInBoundQuery {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_HistoryOutInBoundQuery;
            }
        }

        public string DataManagementView_Text_IsManualSet {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_IsManualSet;
            }
        }

        public string DataManagementView_Text_ManualSetSuccess {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_ManualSetSuccess;
            }
        }

        public string DataManagementView_Text_PrintBoxUpTask {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_PrintBoxUpTask;
            }
        }

        public string DataManagementView_Text_PrintInboundShelves {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_PrintInboundShelves;
            }
        }

        public string DataManagementView_Text_StockExplain {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_StockExplain;
            }
        }

        public string DataManagementView_Text_StockExplainDetail {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_StockExplainDetail;
            }
        }

        public string DataManagementView_Text_StopBoxUpSuccess {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_StopBoxUpSuccess;
            }
        }

        public string DataManagementView_Title_AgencyPartsStockDetailQuery {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_AgencyPartsStockDetailQuery;
            }
        }

        public string DataManagementView_Title_ClaimApproverStrategy {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_ClaimApproverStrategy;
            }
        }

        public string DataManagementView_Title_EnterprisePartsCost {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_EnterprisePartsCost;
            }
        }

        public string DataManagementView_Title_LogisticsTracking {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_LogisticsTracking;
            }
        }

        public string DataManagementView_Title_PartsInboundCheckBillForWarehouseQuery {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PartsInboundCheckBillForWarehouseQuery;
            }
        }

        public string DataManagementView_Title_PartsInboundPlanForWarehouseQuery {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PartsInboundPlanForWarehouseQuery;
            }
        }

        public string DataManagementView_Title_PartsOutboundBillForWarehouseQuery {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PartsOutboundBillForWarehouseQuery;
            }
        }

        public string DataManagementView_Title_PartsOutboundPlanForWarehouseQuery {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PartsOutboundPlanForWarehouseQuery;
            }
        }

        public string DataManagementView_Title_PartsShippingOrderForLogistic {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PartsShippingOrderForLogistic;
            }
        }

        public string DataManagementView_Title_PickingTaskQuery {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PickingTaskQuery;
            }
        }

        public string DataManagementView_Title_PostSaleClaimsTmp {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PostSaleClaimsTmp;
            }
        }

        public string DataGridView_ColumnItem_WarehouseArea {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_WarehouseArea;
            }
        }

        public string DataManagementView_Text_StockExplainDetail2 {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_StockExplainDetail2;
            }
        }

        public string DataManagementView_Text_StockExplainDetail3 {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Text_StockExplainDetail3;
            }
        }

        public string DataEditView_Text_ContainNumber {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_ContainNumber;
            }
        }

        public string DetailPanel_GroupTitle_Delete {
            get {
                return Resources.PartsStockingUIStrings.DetailPanel_GroupTitle_Delete;
            }
        }

        public string DetailPanel_GroupTitle_File {
            get {
                return Resources.PartsStockingUIStrings.DetailPanel_GroupTitle_File;
            }
        }

        public string DetailPanel_GroupTitle_Upload {
            get {
                return Resources.PartsStockingUIStrings.DetailPanel_GroupTitle_Upload;
            }
        }

        public string DataGridView_ColumnItem_IsUploadFile {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_IsUploadFile;
            }
        }

        public string PartsStockQueryPickTime {
            get {
                return Resources.PartsStockingUIStrings.PartsStockQueryPickTime;
            }
        }

        public string PartsStockQueryForPcPrint_Title {
            get {
                return Resources.PartsStockingUIStrings.PartsStockQueryForPcPrint_Title;
            }
        }

        public string PartsStockQueryNumber {
            get {
                return Resources.PartsStockingUIStrings.PartsStockQueryNumber;
            }
        }

        public string PartsStockQueryNumberError {
            get {
                return Resources.PartsStockingUIStrings.PartsStockQueryNumberError;
            }
        }

        public string DataEditView_Title_InitialApprovePartsTransferOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_InitialApprovePartsTransferOrder;
            }
        }

        public string DataEditView_Validation_RejectCommentIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_RejectCommentIsNull;
            }
        }

        public string DataGridView_Column_DiscountedPrice {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_DiscountedPrice;
            }
        }

        public string DataGridView_Column_DiscountRate {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_DiscountRate;
            }
        }

        public string DataGridView_Column_EntityStatus {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_EntityStatus;
            }
        }

        public string DataGridView_Column_OverQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_OverQuantity;
            }
        }

        public string DataGridView_Column_ServiceCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_ServiceCode;
            }
        }

        public string DataGridView_Column_ServiceName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Column_ServiceName;
            }
        }

        public string Management_Confirm_Buy {
            get {
                return Resources.PartsStockingUIStrings.Management_Confirm_Buy;
            }
        }

        public string Management_Title_OverstockPartsPlatFormBill {
            get {
                return Resources.PartsStockingUIStrings.Management_Title_OverstockPartsPlatFormBill;
            }
        }

        public string Management_Validation_PartCodeMoreThan {
            get {
                return Resources.PartsStockingUIStrings.Management_Validation_PartCodeMoreThan;
            }
        }

        public string QueryPanel_Title_OverstockPartsPlatFormBill {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_OverstockPartsPlatFormBill;
            }
        }

        public string DataGridView_Title_ABCSetting_IsAutoAdjust {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Title_ABCSetting_IsAutoAdjust;
            }
        }

        public string DataGridView_Title_ABCSetting_Name {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Title_ABCSetting_Name;
            }
        }

        public string DataGridView_Title_ABCSetting_NoSaleMonth {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Title_ABCSetting_NoSaleMonth;
            }
        }

        public string DataGridView_Title_ABCSetting_SafeStockCoefficient {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Title_ABCSetting_SafeStockCoefficient;
            }
        }

        public string DataGridView_Title_ABCSetting_SaleFee {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Title_ABCSetting_SaleFee;
            }
        }

        public string DataGridView_Title_ABCSetting_SaleFrequency {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Title_ABCSetting_SaleFrequency;
            }
        }

        public string DataGridView_Title_ABCSetting_Type {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Title_ABCSetting_Type;
            }
        }

        public string DataEdit_Error_ABCSetting_Validation1 {
            get {
                return Resources.PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation1;
            }
        }

        public string DataEdit_Error_ABCSetting_Validation2 {
            get {
                return Resources.PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation2;
            }
        }

        public string DataEdit_Error_ABCSetting_Validation3 {
            get {
                return Resources.PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation3;
            }
        }

        public string DataEdit_Error_ABCSetting_Validation4 {
            get {
                return Resources.PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation4;
            }
        }

        public string DataEdit_Error_ABCSetting_Validation5 {
            get {
                return Resources.PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation5;
            }
        }

        public string DataEdit_Error_ABCSetting_Validation6 {
            get {
                return Resources.PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation6;
            }
        }

        public string Title_ABCSetting {
            get {
                return Resources.PartsStockingUIStrings.Title_ABCSetting;
            }
        }

        public string Title_ABCSettingQuery {
            get {
                return Resources.PartsStockingUIStrings.Title_ABCSettingQuery;
            }
        }

        public string DataGridView_Title_ABCSetting_IsReserve {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Title_ABCSetting_IsReserve;
            }
        }

        public string DataGridView_ColumnItem_UpperCheckerName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_UpperCheckerName;
            }
        }

        public string DataGridView_ColumnItem_UpperCheckTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_UpperCheckTime;
            }
        }

        public string QueryPanel_Title_HasDifference {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_HasDifference;
            }
        }

        public string Action_Title_AdvancedApprove {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_AdvancedApprove;
            }
        }

        public string Action_Title_Buy {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Buy;
            }
        }

        public string Action_Title_UploadFile {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_UploadFile;
            }
        }

        public string Action_Title_Excut_Query {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Excut_Query;
            }
        }

        public string DataEditView_Title_PartsShiftOrder_Type {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Title_PartsShiftOrder_Type;
            }
        }

        public string DataEditView_Validation_Type {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_Type;
            }
        }

        public string Management_Tips_Send {
            get {
                return Resources.PartsStockingUIStrings.Management_Tips_Send;
            }
        }

        public string QueeyPanel_OverstockTransferOrder_Title_Code {
            get {
                return Resources.PartsStockingUIStrings.QueeyPanel_OverstockTransferOrder_Title_Code;
            }
        }

        public string QueeyPanel_OverstockTransferOrder_Title_TransferInCorpName {
            get {
                return Resources.PartsStockingUIStrings.QueeyPanel_OverstockTransferOrder_Title_TransferInCorpName;
            }
        }

        public string QueryPanel_OverstockTransferOrder_Title_TransferInWarehouseCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferInWarehouseCode;
            }
        }

        public string QueryPanel_OverstockTransferOrder_Title_TransferOutCorpName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferOutCorpName;
            }
        }

        public string QueryPanel_OverstockTransferOrder_Title_TransferOutWarehouseName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferOutWarehouseName;
            }
        }

        public string QueryPanel_Title_VirtualAgencyPartsStockQuery {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_VirtualAgencyPartsStockQuery;
            }
        }

        public string Upload_Error1 {
            get {
                return Resources.PartsStockingUIStrings.Upload_Error1;
            }
        }

        public string Upload_Error2 {
            get {
                return Resources.PartsStockingUIStrings.Upload_Error2;
            }
        }

        public string Upload_Error3 {
            get {
                return Resources.PartsStockingUIStrings.Upload_Error3;
            }
        }

        public string Upload_IsUploading {
            get {
                return Resources.PartsStockingUIStrings.Upload_IsUploading;
            }
        }

        public string Upload_Success {
            get {
                return Resources.PartsStockingUIStrings.Upload_Success;
            }
        }

        public string DataEditPanel_Text_Logistics_PromisedDeliveryTime {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Text_Logistics_PromisedDeliveryTime;
            }
        }

        public string DataEditView_OverstockPartsApp_Modify {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_OverstockPartsApp_Modify;
            }
        }

        public string DataEditView_PartsInventoryDetails_Null {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_PartsInventoryDetails_Null;
            }
        }

        public string DataEditView_PartsShippingOrder_RequestedArrivalDate {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_PartsShippingOrder_RequestedArrivalDate;
            }
        }

        public string DataEditView_Text_HauDistanceInfor_IsInternalAllocationBill {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_HauDistanceInfor_IsInternalAllocationBill;
            }
        }

        public string DataEditView_Text_HauDistanceInfor_TransferInCorpCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_HauDistanceInfor_TransferInCorpCode;
            }
        }

        public string DataEditView_Text_PartsShiftOrder_VirtualPartsStock_BorrowQty {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_BorrowQty;
            }
        }

        public string DataEditView_Text_PartsShippingOrder_LogisticNo {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsShippingOrder_LogisticNo;
            }
        }

        public string DataEditView_Validate_OverstockPartsApp_Details {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validate_OverstockPartsApp_Details;
            }
        }

        public string DataEditView_Validate_OverstockPartsApp_DiscountRate {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validate_OverstockPartsApp_DiscountRate;
            }
        }

        public string DataEditView_Validate_OverstockPartsApp_DiscountRateNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validate_OverstockPartsApp_DiscountRateNull;
            }
        }

        public string DataEditView_Validate_OverstockPartsApp_PhoneNumber {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validate_OverstockPartsApp_PhoneNumber;
            }
        }

        public string DataEditView_Validation_BorrowBill_WarehouseAreaCategoryIsNull {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Validation_BorrowBill_WarehouseAreaCategoryIsNull;
            }
        }

        public string DataGridView_ColumnItem_Title_CompanyTransferOrderDetail_EnoughQty {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyTransferOrderDetail_EnoughQty;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInboundPlan_PlannedPriceTotalAmount {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_PlannedPriceTotalAmount;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsInboundPlan_WarehouseAreaCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_WarehouseAreaCode;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountedPrice {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountedPrice;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountedPrice2 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountedPrice2;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountRate {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountRate;
            }
        }

        public string DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountRate2 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountRate2;
            }
        }

        public string DataGridView_Notification_PartsInventoryDetail_DestWarehouseArea {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_DestWarehouseArea;
            }
        }

        public string DataGridView_Notification_PartsInventoryDetail_OriginalWarehouseArea {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_OriginalWarehouseArea;
            }
        }

        public string DataGridView_Notification_PartsInventoryDetail_Over {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_Over;
            }
        }

        public string DataManagementView_Notification_Delete {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Notification_Delete;
            }
        }

        public string Management_Packingtask_Status {
            get {
                return Resources.PartsStockingUIStrings.Management_Packingtask_Status;
            }
        }

        public string QueeyPanel_OverstockTransferOrder_Title_CustomerType {
            get {
                return Resources.PartsStockingUIStrings.QueeyPanel_OverstockTransferOrder_Title_CustomerType;
            }
        }

        public string QueeyPanel_OverstockTransferOrder_Title_TotalAmount {
            get {
                return Resources.PartsStockingUIStrings.QueeyPanel_OverstockTransferOrder_Title_TotalAmount;
            }
        }

        public string QueryPanel_OverstockTransferOrder_Title_TransferInWarehouseName {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferInWarehouseName;
            }
        }

        public string QueryPanel_OverstockTransferOrder_Title_TransferOutCorpCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferOutCorpCode;
            }
        }

        public string QueryPanel_OverstockTransferOrder_Title_TransferOutWarehouseCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferOutWarehouseCode;
            }
        }

        public string DataEdit_PartsOutboundPlan_CounterpartCompanyName {
            get {
                return Resources.PartsStockingUIStrings.DataEdit_PartsOutboundPlan_CounterpartCompanyName;
            }
        }

        public string DataGridView_ColumnItem_SeniorName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_SeniorName;
            }
        }

        public string DataGridView_ColumnItem_SeniorTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_SeniorTime;
            }
        }      
        public string DataGridView_ColumnItem_Title_PartsDifferenceBackBill_Type {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBill_Type;
            }
        }

        public string DataManagementView_Title_PartsDifferenceBackBill {
            get {
                return Resources.PartsStockingUIStrings.DataManagementView_Title_PartsDifferenceBackBill;
            }
        }

        public string DataEditView_Notification_PartsDifferenceBackBill_SourceCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Notification_PartsDifferenceBackBill_SourceCode;
            }
        }

        public string Action_Title_Upload {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_Upload;
            }
        }         

        public string Action_Title_SeniorApprove {
            get {
                return Resources.PartsStockingUIStrings.Action_Title_SeniorApprove;
            }
        }

        public string DataEditView_Text_PartsDifferenceBackBillDtl_DiffQuantity {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsDifferenceBackBillDtl_DiffQuantity;
            }
        }

        public string QueryPanel_Title_PartsDifferenceBackBillDtl {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsDifferenceBackBillDtl;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsDifferenceBackBillDtl_ErrorType {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBillDtl_ErrorType;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsDifferenceBackBill_Code {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsDifferenceBackBill_Code;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsDifferenceBackBill_SubmitTime {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsDifferenceBackBill_SubmitTime;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsDifferenceBackBill_SubmitterName {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBill_SubmitterName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsDifferenceBackBill_SubmitTime {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBill_SubmitTime;
            }
        }

        public string QueryPanel_Title_PartsDifferenceBackBill {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsDifferenceBackBill;
            }
        }

        public string DataEditView_Text_PartsDifferenceBackBillCode {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_PartsDifferenceBackBillCode;
            }
        }

        public string QueryPanel_Title_PartsDifferenceBackBill_SourceCode {
            get {
                return Resources.PartsStockingUIStrings.QueryPanel_Title_PartsDifferenceBackBill_SourceCode;
            }
        }

        public string DataEditPanel_Title_PartsDifferenceBackBillDtl {
            get {
                return Resources.PartsStockingUIStrings.DataEditPanel_Title_PartsDifferenceBackBillDtl;
            }
        }
         

        public string DataGridView_ColumnItem_UpperCheckerName1 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_UpperCheckerName1;
            }
        }

        public string DataGridView_ColumnItem_UpperCheckTime1 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_UpperCheckTime1;
            }
        }

        public string DataEditView_Text_DownPartsShiftOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_DownPartsShiftOrder;
            }
        }

        public string DataEditView_Text_UpPartsShiftOrder {
            get {
                return Resources.PartsStockingUIStrings.DataEditView_Text_UpPartsShiftOrder;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_CourrentDownShelfQty {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_CourrentDownShelfQty;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_CourrentDownSIHCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_CourrentDownSIHCode;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_CourrentSIHCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_CourrentSIHCode;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_CourrentUpShelfQty {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_CourrentUpShelfQty;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DownShelfQty {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DownShelfQty;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DownSIHCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DownSIHCode;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_SIHCode {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_SIHCode;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_TraceProperty {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_TraceProperty;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_UpShelfQty {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_UpShelfQty;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation10 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation10;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation11 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation11;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation12 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation12;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation2 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation2;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation3 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation3;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation4 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation4;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation5 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation5;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation6 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation6;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation7 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation7;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation8 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation8;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation9 {
            get {
                return Resources.PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation9;
            }
        }

    }
}
