﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
     [PageMeta("PartsStockingAndLogistics", "WarehouseExecution", "AgencyDifferenceBackBill", ActionPanelKeys = new[] {
        "AgencyDifferenceBackBill"
    })]
    public class AgencyDifferenceBackBillManagement : DcsDataManagementViewBase {
        private const string DATA_APPROVEE_VIEW = "_dataApproveView_";
        private const string DATA_RETURNSUBMIT_VIEW = "_DataReturnSubmitView_";
        private const string DATA_RETURNAPPROVE_VIEW = "_DataReturnApproveView_";
        private const string DATA_INITIALAPPROVE_VIEW = "_DataInitialApproveView_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_DETERMINE_VIEW = "_DataDetermineView_";
        private const string DATA_TERMINE_VIEW = "_DataTermineView_";
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataInitialApproveView;
        private DataEditViewBase dataReturnSubmitView;
        private DataEditViewBase dataReturnApproveView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataDetermineEditView;
        private DataEditViewBase dataTermineEditView;

        public AgencyDifferenceBackBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "中心库收货差异处理";
        }

        public DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("AgencyDifferenceBackBill");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick; 

                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        } 
     //详情
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("AgencyDifferenceBackBillDetail");
                    this.dataDetailView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
         //终止
        private DataEditViewBase DataTermineEditView {
            get {
                if(this.dataTermineEditView == null) {
                    this.dataTermineEditView = DI.GetDataEditView("AgencyDifferenceBackBillTermine");
                    ((AgencyDifferenceBackBillTermineDataEditView)this.dataTermineEditView).EditSubmitted += this.dataEditView_EditSubmitted; 
                    this.dataTermineEditView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataTermineEditView;
            }
        }
         //新增，修改
        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("AgencyDifferenceBackBill");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        //判定
        public DataEditViewBase DataDetermineEditView {
            get {
                if(this.dataDetermineEditView == null) {
                    this.dataDetermineEditView = DI.GetDataEditView("AgencyDifferenceBackBillDetermine");
                    ((AgencyDifferenceBackBillDetermineDataEditView)this.dataDetermineEditView).EditSubmitted += this.dataEditView_EditSubmitted; 
                    this.dataDetermineEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataDetermineEditView;
            }
        }
         //审核
        public DataEditViewBase DataInitialApproveView {
            get {
                if(this.dataInitialApproveView == null) {
                    this.dataInitialApproveView = DI.GetDataEditView("AgencyDifferenceBackBillApprove");
                    ((AgencyDifferenceBackBillApproveDataEditView)this.dataInitialApproveView).EditSubmitted += dataEditView_EditSubmitted;
                    this.dataInitialApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataInitialApproveView;
            }
        }
         //审批
        public DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("AgencyDifferenceBackBillAudit");
                    ((AgencyDifferenceBackBillAuditDataEditView)this.dataApproveView).EditSubmitted += dataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }
         //确认
        public DataEditViewBase DataReturnSubmitView {
            get {
                if(this.dataReturnSubmitView == null) {
                    this.dataReturnSubmitView = DI.GetDataEditView("AgencyDifferenceBackBillConfirm");
                    ((AgencyDifferenceBackBillConfirmDataEditView)this.dataReturnSubmitView).EditSubmitted += dataEditView_EditSubmitted;
                    this.dataReturnSubmitView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataReturnSubmitView;
            }
        }
         //处理
        public DataEditViewBase DataReturnApproveView {
            get {
                if(this.dataReturnApproveView == null) {
                    this.dataReturnApproveView = DI.GetDataEditView("AgencyDifferenceBackBillHandle");
                    ((AgencyDifferenceBackBillHandleDataEditView)this.dataReturnApproveView).EditSubmitted += dataEditView_EditSubmitted;
                    this.dataReturnApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataReturnApproveView;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]{
                    "AgencyDifferenceBackBill"
                };
            }
        }
        private void ResetEditView() {
            this.dataEditView = null; 
            this.dataApproveView = null; 
            this.dataInitialApproveView = null; 
            this.dataReturnSubmitView = null; 
            this.dataReturnApproveView = null;
            this.dataDetailView = null;
            this.dataDetermineEditView = null;
            this.dataTermineEditView = null;
            this.dataDetailView = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_INITIALAPPROVE_VIEW, () => this.DataInitialApproveView);
            this.RegisterView(DATA_APPROVEE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_RETURNSUBMIT_VIEW, () => this.DataReturnSubmitView);
            this.RegisterView(DATA_RETURNAPPROVE_VIEW, () => this.DataReturnApproveView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
            this.RegisterView(DATA_DETERMINE_VIEW, () => this.DataDetermineEditView);
            this.RegisterView(DATA_TERMINE_VIEW, () => this.DataTermineEditView);
        }

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD://新增
                    var agencyDifferenceBackBill = this.DataEditView.CreateObjectToEdit<AgencyDifferenceBackBill>();
                    agencyDifferenceBackBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    agencyDifferenceBackBill.Status = (int)DCSAgencyDifferenceBackBillStatus.新增;
                    agencyDifferenceBackBill.StorageCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    agencyDifferenceBackBill.StorageCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    agencyDifferenceBackBill.StorageCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseName;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT://修改
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON://作废
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<AgencyDifferenceBackBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废中心库收货差异处理单)
                                entity.作废中心库收货差异处理单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.SUBMIT://提交
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<AgencyDifferenceBackBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交中心库收货差异处理单)
                                entity.提交中心库收货差异处理单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                this.CheckActionsCanExecute();
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.TERMINATE://终止
                    this.DataTermineEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_TERMINE_VIEW);
                    break;
                case "Determine": //判定
                    this.DataDetermineEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_DETERMINE_VIEW);
                    break;
                case CommonActionKeys.CONFIRM: //确认
                    this.DataReturnSubmitView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_RETURNSUBMIT_VIEW);
                    break;
                case "Handle": //处理
                    this.DataReturnApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_RETURNAPPROVE_VIEW);
                    break;
                case CommonActionKeys.APPROVE: //审核
                    this.DataInitialApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INITIALAPPROVE_VIEW);
                    break;
               
                case CommonActionKeys.AUDIT: //审批
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVEE_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<BorrowBill>().Select(r => r.Id).ToArray();
                        this.ExportBorrowBill(ids, null, null, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var resRelationshipId = filterItem.Filters.Single(e => e.MemberName == "ResRelationshipId").Value as int?;
                        var resTem = filterItem.Filters.Single(e => e.MemberName == "ResTem").Value as int?;
                        var storageCompanyCode = filterItem.Filters.Single(e => e.MemberName == "StorageCompanyCode").Value as string;
                        var storageCompanyName = filterItem.Filters.Single(e => e.MemberName == "StorageCompanyName").Value as string;
                        var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.Single(e => e.MemberName == "SparePartName").Value as string;
                        var SesTem = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var createTime = filterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).ToArray();
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? SubmitTimeBegin = null;
                        DateTime? SubmitTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "CreateTime").Value : null);
                            createTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.LastOrDefault(r => r.MemberName == "CreateTime").Value : null);

                            SubmitTimeBegin = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "SubmitTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "SubmitTime").Value : null);
                            SubmitTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "SubmitTime") != null ? createTime.LastOrDefault(r => r.MemberName == "SubmitTime").Value : null);

                        }
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportBorrowBill(null, resRelationshipId, resTem, code, storageCompanyCode, storageCompanyName, sparePartCode, sparePartName, status,createTimeBegin, createTimeEnd, SubmitTimeBegin, SubmitTimeEnd);
                    }
                    break;
            }
        }

        private void ExportBorrowBill(int[] ids, int? resRelationshipId, int? resTem, string code, string storageCompanyCode, string storageCompanyName, string sparePartCode, string sparePartName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? SubmitTimeBegin, DateTime? SubmitTimeEnd) {
            this.dcsDomainContext.导出中心库收货差异处理(ids, resRelationshipId, resTem, code, storageCompanyCode, storageCompanyName, sparePartCode, sparePartName,status, createTimeBegin,createTimeEnd, SubmitTimeBegin, SubmitTimeEnd, loadOp => {
                if(loadOp.HasError) {
                    ShellViewModel.Current.IsBusy = false;
                    return;
                }
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                    ShellViewModel.Current.IsBusy = false;
                }
                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    ShellViewModel.Current.IsBusy = false;
                }
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.AUDIT: //审批
                case CommonActionKeys.APPROVE: //审核
                case CommonActionKeys.TERMINATE://终止
                case "Determine": //判定
                case CommonActionKeys.CONFIRM: //确认
                case "Handle": //处理
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<AgencyDifferenceBackBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == CommonActionKeys.AUDIT)
                        return entities[0].Status == (int)DCSAgencyDifferenceBackBillStatus.审核通过;
                    if(uniqueId == CommonActionKeys.APPROVE)
                        return entities[0].Status == (int)DCSAgencyDifferenceBackBillStatus.处理通过;
                    if(uniqueId == "Determine")
                        return entities[0].Status == (int)DCSAgencyDifferenceBackBillStatus.提交;
                    if(uniqueId == "Handle")
                        return entities[0].Status == (int)DCSAgencyDifferenceBackBillStatus.确认通过 ;
                    if(uniqueId == CommonActionKeys.CONFIRM)
                        return entities[0].Status == (int)DCSAgencyDifferenceBackBillStatus.判定通过;
                    if(uniqueId == CommonActionKeys.TERMINATE)
                        return entities[0].Status != (int)DCSAgencyDifferenceBackBillStatus.新增 && entities[0].Status != (int)DCSAgencyDifferenceBackBillStatus.作废 && entities[0].Status != (int)DCSAgencyDifferenceBackBillStatus.终止 && entities[0].Status != (int)DCSAgencyDifferenceBackBillStatus.审批通过;
                    return entities[0].Status == (int)DCSAgencyDifferenceBackBillStatus.新增;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
