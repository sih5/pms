﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsLogisticsRelated", "AgencyPartsShippingOrder", ActionPanelKeys = new[] {
        "AgencyPartsShippingOrder"
    })]

    public class AgencyPartsShippingOrderManagemenet : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_EDIT_VIEW_FOR_SENDINGCONFIRM = "_dataEditViewForSendingConfirm_";
        private const string DATA_EDIT_VIEW_FOR_SENDCONFIRM = "_dataEditViewForSendConfirm_";
        private const string DATA_EDIT_VIEW_FOR_EXTENDEDBACKCONFIRM = "_dataEditViewForExtendedBackConfirm_";
        private const string DATA_EDIT_VIEW_FOR_GPS = "_dataEditViewForGPS_";

        public AgencyPartsShippingOrderManagemenet() {
            this.Initializer.Register(this.Initialize);
            this.Title = "配件发运管理-代理库随车行";
        }

        private DataEditViewBase dataEditViewForGPS;
        private DataEditViewBase DataEditViewForGPS {
            get {
                if(this.dataEditViewForGPS == null) {
                    this.dataEditViewForGPS = DI.GetDataEditView("AgencyPartsShippingOrderForGPS");
                    this.dataEditViewForGPS.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditViewForGPS.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewForGPS;
            }
        }

        private DataEditViewBase dataSendingConfirmView;
        private DataEditViewBase DataSendingConfirmView {
            get {
                if(this.dataSendingConfirmView == null) {
                    this.dataSendingConfirmView = DI.GetDataEditView("AgencyPartsShippingOrderForSendingConfirm");
                    this.dataSendingConfirmView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataSendingConfirmView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataSendingConfirmView;
            }
        }

        private DataEditViewBase dataSendConfirmView;
        private DataEditViewBase DataSendConfirmView {
            get {
                if(this.dataSendConfirmView == null) {
                    this.dataSendConfirmView = DI.GetDataEditView("AgencyPartsShippingOrderForSendConfirm");
                    this.dataSendConfirmView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataSendConfirmView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataSendConfirmView;
            }
        }

        private DataEditViewBase dataExtendedBackConfirmView;
        private DataEditViewBase DataExtendedBackConfirmView {
            get {
                if(this.dataExtendedBackConfirmView == null) {
                    this.dataExtendedBackConfirmView = DI.GetDataEditView("AgencyPartsShippingOrderForExtendedBackConfirm");
                    this.dataExtendedBackConfirmView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataExtendedBackConfirmView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataExtendedBackConfirmView;
            }
        }
        private void ResetEditView() {
            this.dataEditViewForGPS = null;
            this.dataSendingConfirmView = null;
            this.dataSendConfirmView = null;
            this.dataExtendedBackConfirmView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgencyPartsShippingOrder"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW_FOR_SENDINGCONFIRM, () => this.DataSendingConfirmView);//发货确认
            this.RegisterView(DATA_EDIT_VIEW_FOR_SENDCONFIRM, () => this.DataSendConfirmView);//送达确认
            this.RegisterView(DATA_EDIT_VIEW_FOR_EXTENDEDBACKCONFIRM, () => this.DataExtendedBackConfirmView);//超期或电商运单流水反馈
            this.RegisterView(DATA_EDIT_VIEW_FOR_GPS, () => this.DataEditViewForGPS);//GPS定位器设备号登记
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AgencyPartsShippingOrder"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "ExtendedBackConfirm": //超期或电商运单流水反馈
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    var entitie3 = this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().ToArray();
                    return (entitie3[0].Status == (int)DcsPartsShippingOrderStatus.新建 || entitie3[0].Status == (int)DcsPartsShippingOrderStatus.已发货 || entitie3[0].Status == (int)DcsPartsShippingOrderStatus.待提货 || entitie3[0].Status == (int)DcsPartsShippingOrderStatus.待收货);
                case "GPSLocationRegister":
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems1 = this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().ToArray();
                    return selectItems1.Length == 1;
                case "Sending": //发货
                case "Send": //送达
                case "ReceivingConfirm": //收货确认
                    if(this.DataGridView.Entities != null && this.DataGridView.Entities.Any() && this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().Any()) {
                        if(uniqueId == "Sending")
                            return this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().All(r => r.Status == (int)DcsPartsShippingOrderStatus.新建 && r.ShippingCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId);
                        if(uniqueId == "Send")
                            return this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().All(r => r.Status == (int)DcsPartsShippingOrderStatus.已发货 && r.ShippingCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId);
                        if(uniqueId == "ReceivingConfirm")
                            return this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().All(r => (r.Status == (int)DcsPartsShippingOrderStatus.已发货 || r.Status == (int)DcsPartsShippingOrderStatus.待收货 || r.Status == (int)DcsPartsShippingOrderStatus.新建) && r.ReceivingCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId);
                    }
                    return false;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.MERGEEXPORT:
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().Select(r => r.Id).ToArray();
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                            this.MergeExport(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }
                        if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                            this.Export(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var closedLoopStatus = filterItem.Filters.Single(e => e.MemberName == "ClosedLoopStatus").Value as int?;
                        var receivingCompanyCode = filterItem.Filters.Single(e => e.MemberName == "ReceivingCompanyCode").Value as string;
                        var ERPSourceOrderCodeQuery = filterItem.Filters.Single(e => e.MemberName == "ERPSourceOrderCodeQuery").Value as string;
                        var receivingCompanyName = filterItem.Filters.Single(e => e.MemberName == "ReceivingCompanyName").Value as string;
                        var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var logisticCompanyName = filterItem.Filters.Single(e => e.MemberName == "LogisticCompanyName").Value as string;
                        var expressCompany = filterItem.Filters.Single(e => e.MemberName == "ExpressCompany").Value as string;
                        var expressCompanyCode = filterItem.Filters.Single(e => e.MemberName == "ExpressCompanyCode").Value as string;
                        DateTime? beginCreateTime = null;
                        DateTime? endCreateTime = null;

                        DateTime? beginRequestedArrivalDate = null;
                        DateTime? endRequestedArrivalDate = null;

                        DateTime? beginShippingDate = null;
                        DateTime? endShippingDate = null;

                        DateTime? beginConfirmedReceptionTime = null;
                        DateTime? endConfirmedReceptionTime = null;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    beginCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    endCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "RequestedArrivalDate") {
                                    beginRequestedArrivalDate = dateTime.Filters.First(r => r.MemberName == "RequestedArrivalDate").Value as DateTime?;
                                    endRequestedArrivalDate = dateTime.Filters.Last(r => r.MemberName == "RequestedArrivalDate").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "ShippingDate") {
                                    beginShippingDate = dateTime.Filters.First(r => r.MemberName == "ShippingDate").Value as DateTime?;
                                    endShippingDate = dateTime.Filters.Last(r => r.MemberName == "ShippingDate").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "ConfirmedReceptionTime") {
                                    beginConfirmedReceptionTime = dateTime.Filters.First(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                    endConfirmedReceptionTime = dateTime.Filters.Last(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                }
                            }
                        }
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId))
                            this.MergeExport(new int[] {
                            }, null, code, type, status, warehouseId, null, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, null, partsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId, null, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, ERPSourceOrderCodeQuery);
                        if(CommonActionKeys.EXPORT.Equals(uniqueId))
                            this.Export(new int[] {
                            }, null, code, type, status, warehouseId, null, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, null, partsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId, null, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, ERPSourceOrderCodeQuery);
                    }
                    break;
                case "ExtendedBackConfirm": //超期或电商运单流水反馈
                    this.DataExtendedBackConfirmView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_FOR_EXTENDEDBACKCONFIRM);
                    break;
                case "GPSLocationRegister":
                    this.DataEditViewForGPS.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_FOR_GPS);
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new AgencyPartsShippingOrderPrintWindow {
                        Header = "配件发运单打印-代理库随车行",
                        AgencyPartsShippingOrder = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case "Sending": //发货
                    DcsUtils.Confirm("你确定发货所选发运单?", () => {
                        try {
                            var ids = this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().Select(r => r.Id).ToArray();
                            if(ids.Any()) {
                                ShellViewModel.Current.IsBusy = false;
                                DcsDomainContext.配件发运单代理库发货(ids, invokeOp => {
                                    ShellViewModel.Current.IsBusy = true;
                                    if(invokeOp.HasError) {
                                        if(!invokeOp.IsErrorHandled)
                                            invokeOp.MarkErrorAsHandled();
                                        var error = invokeOp.ValidationErrors.First();
                                        if(error != null)
                                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                                        else
                                            DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                        return;
                                    }
                                    UIHelper.ShowNotification(PartsStockingUIStrings.Management_Tips_Send);
                                    this.DataGridView.ExecuteQueryDelayed();
                                    ShellViewModel.Current.IsBusy = false;
                                }, null);

                            }
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Send": //送达
                    DcsUtils.Confirm("你确定送达所选发运单?", () => {
                        try {
                            var ids = this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().Select(r => r.Id).ToArray();
                            if(ids.Any()) {
                                ShellViewModel.Current.IsBusy = false;
                                DcsDomainContext.配件发运单代理库送达(ids, invokeOp => {
                                    ShellViewModel.Current.IsBusy = true;
                                    if(invokeOp.HasError) {
                                        if(!invokeOp.IsErrorHandled)
                                            invokeOp.MarkErrorAsHandled();
                                        var error = invokeOp.ValidationErrors.First();
                                        if(error != null)
                                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                                        else
                                            DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                        return;
                                    }
                                    UIHelper.ShowNotification("所选数据送达成功.");
                                    this.DataGridView.ExecuteQueryDelayed();
                                    ShellViewModel.Current.IsBusy = false;

                                }, null);

                            }
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "ReceivingConfirm": //收货确认
                    DcsUtils.Confirm("你确定收货确认所选发运单?", () => {
                        try {
                            var ids = this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().Select(r => r.Id).ToArray();
                            if(ids.Any()) {
                                ShellViewModel.Current.IsBusy = false;
                                DcsDomainContext.配件发运单代理库收货确认(ids, invokeOp => {
                                    ShellViewModel.Current.IsBusy = true;
                                    if(invokeOp.HasError) {
                                        if(!invokeOp.IsErrorHandled)
                                            invokeOp.MarkErrorAsHandled();
                                        var error = invokeOp.ValidationErrors.First();
                                        if(error != null)
                                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                                        else
                                            DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                        return;
                                    }
                                    UIHelper.ShowNotification("所选数据收货确认成功.");
                                    this.DataGridView.ExecuteQueryDelayed();
                                    ShellViewModel.Current.IsBusy = false;

                                }, null);

                            }
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }
        private DcsDomainContext dcsDomainContext;

        private DcsDomainContext DcsDomainContext {
            get {
                return this.dcsDomainContext ?? (this.dcsDomainContext = new DcsDomainContext());
            }
        }

        //private void ExecuteQueryDelayed() {
        //    var composite = new CompositeFilterItem();
        //    this.DataGridView.FilterItem = composite;
        //    this.DataGridView.ExecuteQueryDelayed();
        //}
        private void MergeExport(int[] ids, int? settlementCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginConfirmedReceptionTime, DateTime? endConfirmedReceptionTime, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE) {
            this.excelServiceClient.ExportAgencyPartsShippingOrderAsync(ids, settlementCompanyId, null, code, type, status, warehouseId, warehouseName, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, logisticCompanyId, logisticCompanyCode, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, eRPSOURCEORDERCODE);
            this.excelServiceClient.ExportAgencyPartsShippingOrderCompleted -= excelServiceClient_ExportPartsShippingOrderCompleted;
            this.excelServiceClient.ExportAgencyPartsShippingOrderCompleted += excelServiceClient_ExportPartsShippingOrderCompleted;
        }

        private void Export(int[] ids, int? settlementCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginConfirmedReceptionTime, DateTime? endConfirmedReceptionTime, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string eRPSOURCEORDERCODE) {
            this.excelServiceClient.ExportAgencyPartsShippingOrderNotWithDetailAsync(ids, settlementCompanyId, null, code, type, status, warehouseId, warehouseName, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, logisticCompanyId, logisticCompanyCode, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, eRPSOURCEORDERCODE);
            this.excelServiceClient.ExportAgencyPartsShippingOrderNotWithDetailCompleted -= excelServiceClient_ExportPartsShippingOrderNotWithDetailCompleted;
            this.excelServiceClient.ExportAgencyPartsShippingOrderNotWithDetailCompleted += excelServiceClient_ExportPartsShippingOrderNotWithDetailCompleted;
        }

        private void excelServiceClient_ExportPartsShippingOrderCompleted(object sender, ExportAgencyPartsShippingOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void excelServiceClient_ExportPartsShippingOrderNotWithDetailCompleted(object sender, ExportAgencyPartsShippingOrderNotWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                var timeFilters = compositeFilterItem.Filters.Where(filter => filter is CompositeFilterItem && ((CompositeFilterItem)filter).Filters.Any(item => (new[] {
                        "CreateTime", "ConfirmedReceptionTime", "RequestedArrivalDate", "ShippingDate"
                    }).Contains(item.MemberName))).Cast<CompositeFilterItem>();
                if(timeFilters != null && timeFilters.Any()) {
                    foreach(var timeFilter in timeFilters) {
                        DateTime endTime;
                        if(timeFilter.Filters.ElementAt(1).Value is DateTime && DateTime.TryParse(timeFilter.Filters.ElementAt(1).Value.ToString(), out endTime)) {
                            timeFilter.Filters.ElementAt(1).Value = new DateTime(endTime.Year, endTime.Month, endTime.Day, 23, 59, 59);
                        }
                    }
                }
            } else
                compositeFilterItem.Filters.Add(filterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
