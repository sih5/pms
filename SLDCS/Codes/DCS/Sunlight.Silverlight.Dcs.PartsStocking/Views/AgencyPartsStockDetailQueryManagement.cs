﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsStore", "AgencyPartsStockDetailQuery", ActionPanelKeys = new[] { CommonActionKeys.EXPORT})]
    public class AgencyPartsStockDetailQueryManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private Company company;
        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgencyPartsStockDetailQuery"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);

            this.dcsDomainContext.Load(this.dcsDomainContext.GetCompaniesQuery().Where(e => e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if (loadOp.HasError)
                    return;
                company = loadOp.Entities.SingleOrDefault();
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var codes = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode"); 
                var isAgencyQuery = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "IsAgencyQuery").Value as bool?; 
                if (isAgencyQuery != null && isAgencyQuery.Value == true && company != null && company.Type != (int)DcsCompanyType.分公司) {
                    if ((codes == null || codes.Value == null) || ((IEnumerable<string>)codes.Value).ToArray().Length > 20) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_PartCodeRequired);
                        return;
                    }
                }

                if(codes != null && codes.Value != null){  
                    compositeFilterItem.Filters.Add(new CompositeFilterItem {  
                        MemberName = "SparePartCode",  
                        MemberType = typeof(string),  
                        Value =string.Join(",",((IEnumerable<string>)codes.Value).ToArray()),  
                        Operator = FilterOperator.Contains  
                    });  
                    compositeFilterItem.Filters.Remove(codes);  
                }
                this.DataGridView.FilterItem = compositeFilterItem;
            }
            this.DataGridView.ExecuteQueryDelayed();
        }


        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var companyCode = filterItem.Filters.Single(e => e.MemberName == "CompanyCode").Value as string;
                    var companyName = filterItem.Filters.Single(e => e.MemberName == "CompanyName").Value as string;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var sparePartName = filterItem.Filters.Single(e => e.MemberName == "SparePartName").Value as string;
                    var greaterThanZero = filterItem.Filters.Single(e => e.MemberName == "GreaterThanZero").Value as bool?;
                    var isAgencyQuery = filterItem.Filters.Single(s => s.MemberName == "IsAgencyQuery").Value as bool?;

                    ShellViewModel.Current.IsBusy = true;
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualAgencyPartsStock>().Select(r => r.Id).ToArray();
                        dcsDomainContext.ExportAgencyPartsStockDetailQuery(ids, companyCode, companyName, sparePartCode, sparePartName, greaterThanZero,isAgencyQuery, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    } else {
                        dcsDomainContext.ExportAgencyPartsStockDetailQuery(null, companyCode, companyName, sparePartCode, sparePartName, greaterThanZero,isAgencyQuery, loadOp => {
                            if (loadOp.HasError)
                                return;
                            if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }


        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] { 
                    "AgencyPartsStockDetailQuery"
                };
            }
        }

        public AgencyPartsStockDetailQueryManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_AgencyPartsStockDetailQuery;
        }
    }
}
