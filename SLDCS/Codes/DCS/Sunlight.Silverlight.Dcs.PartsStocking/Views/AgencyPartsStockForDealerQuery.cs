﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
     [PageMeta("PartsStockingAndLogistics", "PartsStore", "AgencyPartsStockForDealerQuery", ActionPanelKeys = new[] { CommonActionKeys.EXPORT })]
    public class AgencyPartsStockForDealerQuery  : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgencyPartsStockForDealerQuery"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);         
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }


        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var companyCode = filterItem.Filters.Single(e => e.MemberName == "CompanyCode").Value as string;
                    var companyName = filterItem.Filters.Single(e => e.MemberName == "CompanyName").Value as string;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var sparePartName = filterItem.Filters.Single(e => e.MemberName == "SparePartName").Value as string;
                    var greaterThanZero = filterItem.Filters.Single(e => e.MemberName == "GreaterThanZero").Value as bool?;

                    ShellViewModel.Current.IsBusy = true;
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualAgencyPartsStock>().Select(r => r.Id).ToArray();
                        dcsDomainContext.ExportAgencyPartsStockForDealerQuery(ids, companyCode, companyName, sparePartCode, sparePartName, greaterThanZero, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    } else {
                        dcsDomainContext.ExportAgencyPartsStockForDealerQuery(null, companyCode, companyName, sparePartCode, sparePartName, greaterThanZero, loadOp => {
                            if (loadOp.HasError)
                                return;
                            if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }


        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] { 
                    "AgencyPartsStockForDealerQuery"
                };
            }
        }

        public AgencyPartsStockForDealerQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.QueryPanel_Title_VirtualAgencyPartsStockQuery;
        }
    }
}
