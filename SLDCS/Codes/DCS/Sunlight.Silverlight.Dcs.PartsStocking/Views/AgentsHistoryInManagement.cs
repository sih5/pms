﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsInStore", "AgentsHistoryIn", ActionPanelKeys = new[] { CommonActionKeys.EXPORT })]
    public class AgentsHistoryInManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public AgentsHistoryInManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "代理库入库记录查询";
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgentsHistoryIn"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AgentsHistoryIn"
                };
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()){
                        var codes = this.DataGridView.SelectedEntities.Cast<VehicleAgentsHistoryIn>().Select(e => e.PartsInboundPlanCode).ToArray();
                        this.导出代理库入库记录查询(codes, null, null, null, null, null, null, null, null, null, null);
                    }else{
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var branchId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "BranchId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "BranchId").Value as int?;
                        var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var inboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InboundType").Value as int?;
                        var storageCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "StorageCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "StorageCompanyCode").Value as string;
                        var storageCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "storageCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "storageCompanyName").Value as string;
                        var partCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartCode").Value as string;
                        var partName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartName").Value as string;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if (createTime != null){
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "PartsInboundPlanTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "PartsInboundPlanTime").Value as DateTime?;
                        }
                        this.导出代理库入库记录查询(new string[] { }, branchId, partsSalesCategoryId, warehouseId, inboundType,storageCompanyCode, storageCompanyName, partCode, partName, createTimeBegin, createTimeEnd);
                        
                    }
                    break;
            }
        }

        private void 导出代理库入库记录查询(string[] codes, int? branchId, int? partsSalesCategoryId, int? warehouseId, int? inboundType, string storageCompanyCode, string storageCompanyName, string partCode, string partName, DateTime? startCreateTime, DateTime? endCreateTimeout){
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.导出代理库入库记录查询Async(codes, branchId, partsSalesCategoryId, warehouseId, inboundType, storageCompanyCode, storageCompanyName, partCode, partName, startCreateTime, endCreateTimeout);
            this.excelServiceClient.导出代理库入库记录查询Completed -= excelServiceClient_导出代理库入库记录查询Completed;
            this.excelServiceClient.导出代理库入库记录查询Completed += excelServiceClient_导出代理库入库记录查询Completed;
        }

        private void excelServiceClient_导出代理库入库记录查询Completed(object sender, 导出代理库入库记录查询CompletedEventArgs e){
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }



        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
