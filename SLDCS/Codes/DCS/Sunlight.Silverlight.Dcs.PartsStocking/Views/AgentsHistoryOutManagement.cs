﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsOutStore", "AgentsHistoryOut", ActionPanelKeys = new[] { CommonActionKeys.EXPORT })]
    public class AgentsHistoryOutManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public AgentsHistoryOutManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "代理库出库记录查询";
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgentsHistoryOut"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AgentsHistoryOut"
                };
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var branchId = filterItem.Filters.Single(e => e.MemberName == "BranchId").Value as int?;
                    var brandId = filterItem.Filters.Single(e => e.MemberName == "BrandId").Value as int?;

                    var WarehouseId = filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;

                    var outboundType = filterItem.Filters.Single(e => e.MemberName == "OutboundType").Value as int?;

                    var storageCompanyCode = filterItem.Filters.Single(r => r.MemberName == "StorageCompanyCode").Value as string;
                    var StorageCompanyName = filterItem.Filters.Single(r => r.MemberName == "StorageCompanyName").Value as string;
                    var partCode = filterItem.Filters.Single(r => r.MemberName == "PartCode").Value as string;
                    var partName = filterItem.Filters.Single(r => r.MemberName == "PartName").Value as string;

                    var businessode = filterItem.Filters.Single(r => r.MemberName == "Businessode").Value as string;
                    var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                    DateTime? startCreateTime = null;
                    DateTime? endCreateTime = null;

                    foreach(var dateTimeFilterItem in compositeFilterItems) {
                        var dateTime = dateTimeFilterItem as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.Any(r => r.MemberName == "OutboundTime")) {
                                startCreateTime = dateTime.Filters.First(r => r.MemberName == "OutboundTime").Value as DateTime?;
                                endCreateTime = dateTime.Filters.Last(r => r.MemberName == "OutboundTime").Value as DateTime?;
                            }

                        }
                    }
                    this.dcsDomainContext.导出代理库出库记录查询(branchId, brandId, WarehouseId, outboundType, storageCompanyCode, StorageCompanyName, partCode, partName, businessode, startCreateTime, endCreateTime, loadOp =>{
                        if(loadOp.HasError)
                            return;
                        if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                            UIHelper.ShowNotification(loadOp.Value);
                            ShellViewModel.Current.IsBusy = false;
                        }

                        if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            ShellViewModel.Current.IsBusy = false;
                        }
                    }, null);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            var compositeFilterItems = compositeFilterItem.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
            if(compositeFilterItems == null) {
                UIHelper.ShowAlertMessage("查询时间不能为空！");
                return;
            }
            var createBeginTime = compositeFilterItems.Filters.First(r => r.MemberName == "OutboundTime").Value as DateTime?;
            var createEndTime = compositeFilterItems.Filters.Last(r => r.MemberName == "OutboundTime").Value as DateTime?;
            if(createBeginTime.Value == DateTime.MinValue || createEndTime.Value == DateTime.MaxValue) {
                UIHelper.ShowAlertMessage("查询时间不能为空！");
                return;
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
