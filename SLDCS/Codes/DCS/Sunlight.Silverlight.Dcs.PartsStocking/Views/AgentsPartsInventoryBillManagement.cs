﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsSales", "PartsOuterPurchase", "AgentsPartsInventoryBill", ActionPanelKeys = new[]{
        CommonActionKeys.INITIALAPPROVE_APPROVE_FINALAPPROVE_ABANDON_EXPORT_REJECT_MERGEEXPORT_PRINT,"AgentsPartsInventoryBill"
    })]
    public class AgentsPartsInventoryBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataInitialApproveView;
        private DataEditViewBase dataFinalApproveView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataInventoryCoverageView;
        private DataEditViewBase dataResultsInputView;
        private const string DATA_APPROVE_VIEW = "_dataApproveView_";
        private const string DATA_INITIALAPPROVE_VIEW = "_dataInitialApproveView_";
        private const string DATA_FINALAPPROVE_VIEW = "_dataFinalApproveView_";
        private const string DATA_DETAIL_VIEW = "_dataDetailView_";
        private const string DATA_INVENTORYCOVERS_VIEW = "_DataInventoryCoverageView_";
        private const string DATA_RESULTSINPUT_VIEW = "_DataResultsInputView_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public AgentsPartsInventoryBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_AgentsPartsInventoryBill;
        }

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgentsPartsInventoryBill"));
            }
        }

        public DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("AgentsPartsInventoryBillForApprove");
                    ((AgentsPartsInventoryBillForApproveDataEditView)this.dataApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        public DataEditViewBase DataInitialApproveView {
            get {
                if(this.dataInitialApproveView == null) {
                    this.dataInitialApproveView = DI.GetDataEditView("AgentsPartsInventoryBillForInitialApprove");
                    ((AgentsPartsInventoryBillForInitialApproveDataEditView)this.dataInitialApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataInitialApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataInitialApproveView;
            }
        }

        public DataEditViewBase DataFinalApproveView {
            get {
                if(this.dataFinalApproveView == null) {
                    this.dataFinalApproveView = DI.GetDataEditView("AgentsPartsInventoryBillForFinalApprove");
                    ((AgentsPartsInventoryBillForFinalApproveDataEditView)this.dataFinalApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataFinalApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataFinalApproveView;
            }
        }
        public DataEditViewBase DataDetailView {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("AgentsPartsInventoryBillDetail");
                    this.dataDetailView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }

        public DataEditViewBase DataInventoryCoverageView {
            get {
                if(this.dataInventoryCoverageView == null) {
                    this.dataInventoryCoverageView = DI.GetDataEditView("AgentsPartsInventoryBillForInventoryCovers");
                    this.dataInventoryCoverageView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataInventoryCoverageView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataInventoryCoverageView;
            }
        }

        public DataEditViewBase DataResultsInputView {
            get {
                if(this.dataResultsInputView == null) {
                    this.dataResultsInputView = DI.GetDataEditView("PartsInventoryBillForResultEntry");
                    this.dataResultsInputView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataResultsInputView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataResultsInputView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_INITIALAPPROVE_VIEW, () => this.DataInitialApproveView);
            this.RegisterView(DATA_FINALAPPROVE_VIEW, () => this.DataFinalApproveView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailView);
            this.RegisterView(DATA_INVENTORYCOVERS_VIEW, () => this.DataInventoryCoverageView);
            this.RegisterView(DATA_RESULTSINPUT_VIEW, () => this.DataResultsInputView);
        }
        private void ResetEditView() {
            this.dataApproveView = null;
            this.dataInitialApproveView = null;
            this.dataFinalApproveView = null;
            this.dataDetailView = null;
            this.dataInventoryCoverageView = null;
            this.dataResultsInputView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "ViewDetail":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray().Count() == 1;
                case "Audit"://审核
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPartsInventoryBillStatus.初审通过;
                case CommonActionKeys.INITIALAPPROVE://初审
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var es1 = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(es1.Length != 1)
                        return false;
                    return es1[0].Status == (int)DcsPartsInventoryBillStatus.已结果录入;
                case "FinalApprove"://审批
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var es2 = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(es2.Length != 1)
                        return false;
                    return es2[0].Status == (int)DcsPartsInventoryBillStatus.审核通过;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    //return entities1[0].Status == (int)DcsPartsInventoryBillStatus.新建;
                    return entities1[0].Status == (int)DcsPartsInventoryBillStatus.新建 || entities1[0].Status == (int)DcsPartsInventoryBillStatus.审批通过 || entities1[0].Status == (int)DcsPartsInventoryBillStatus.已结果录入;
                case "ResultsInput"://结果录入
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities2 = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(entities2.Length != 1)
                        return false;
                    return entities2[0].Status == (int)DcsPartsInventoryBillStatus.新建;
                case "InventoryCoverage"://库存覆盖
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities3 = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(entities3.Length != 1)
                        return false;
                    return entities3[0].Status == (int)DcsPartsInventoryBillStatus.高级审核通过;
                case "DetailExport":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "Reject"://驳回
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities4 = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(entities4.Length != 1)
                        return false;
                    return entities4[0].Status == (int)DcsPartsInventoryBillStatus.已结果录入;
                case CommonActionKeys.PRINT://打印
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    return selectItems.Length == 1;
                case "AdvancedAudit":
                    //高级审核
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities5 = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(entities5.Length != 1)
                        return false;
                    return entities5[0].Status == (int)DcsPartsInventoryBillStatus.审批通过;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "ViewDetail":
                    this.DataDetailView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entityAbandon = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().FirstOrDefault();
                        if(entityAbandon == null)
                            return;
                        dcsDomainContext.Load(dcsDomainContext.GetPartsInventoryBillsQuery().Where(ex => ex.Id == entityAbandon.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            var items = loadOp.Entities.SingleOrDefault();
                            if(items == null)
                                return;
                            try {
                                if(items.Can作废配件盘点单)
                                    items.作废配件盘点单();
                                dcsDomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        dcsDomainContext.RejectChanges();
                                        return;
                                    }
                                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                    this.CheckActionsCanExecute();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case "Audit":
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                case CommonActionKeys.INITIALAPPROVE:
                    this.DataInitialApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INITIALAPPROVE_VIEW);
                    break;
                case "FinalApprove":
                case "AdvancedAudit":
                    this.DataFinalApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_FINALAPPROVE_VIEW);
                    break;
                case "ResultsInput"://结果录入
                    this.DataResultsInputView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_RESULTSINPUT_VIEW);
                    break;
                case "InventoryCoverage"://库存覆盖
                    this.DataInventoryCoverageView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INVENTORYCOVERS_VIEW);
                    break;
                case "DetailExport":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var storageCompanyCode = filterItem.Filters.Single(r => r.MemberName == "StorageCompanyCode").Value as string;
                    var storageCompanyName = filterItem.Filters.Single(r => r.MemberName == "StorageCompanyName").Value as string;
                    var warehouseName = filterItem.Filters.Single(r => r.MemberName == "WarehouseName").Value as string;
                    var warehouseAreaCategory = filterItem.Filters.Single(r => r.MemberName == "WarehouseAreaCategory").Value as int?;
                    //var createTime = filterItem.Filters.First(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    var amountDifferenceRange = filterItem.Filters.Single(r => r.MemberName == "AmountDifferenceRange").Value as int?;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(filterItem.Filters.Any(r => r.GetType() == typeof(CompositeFilterItem))) {
                        var cTime = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                        foreach(var dateTime in cTime) {
                            var createTime = dateTime as CompositeFilterItem;
                            if(createTime != null) {
                                if(createTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }
                    }

                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var branchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    var storageCompanyId = branchId;

                    //DateTime? createTimeBegin = null;
                    //DateTime? createTimeEnd = null;
                    //if(createTime != null) {
                    //    createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                    //    createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    //}
                    ShellViewModel.Current.IsBusy = true;
                    var storageCompanyTypes = new[]{
                        (int)DcsCompanyType.代理库, (int)DcsCompanyType.服务站兼代理库
                    };
                    var ids = new int[] { };
                    if(this.DataGridView.SelectedEntities != null) {
                        var dealerPartsInventoryBills = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>();
                        ids = dealerPartsInventoryBills.Select(r => r.Id).ToArray();
                    }
                    this.ExportAgentPartsInventoryBillWithDetailForBranch(ids, storageCompanyTypes, storageCompanyId, storageCompanyCode, storageCompanyName, null, warehouseName, code, warehouseAreaCategory, status, createTimeBegin, createTimeEnd, branchId, amountDifferenceRange);
                    break;
                case "Reject":
                    this.RejectDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.RejectWindow.ShowDialog();
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    dcsDomainContext.Load(dcsDomainContext.GetPartsInventoryBillsQuery().Where(ex => ex.Id == selectedItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var items = loadOp.Entities.SingleOrDefault();
                        if(items == null)
                            return;
                        SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsInventoryBill, "ReportPartsInventoryBill", null, true, new Tuple<string, string>("partsInventoryBillId", items.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));
                    }, null);
                    break;
            }
        }

        private void ExportAgentPartsInventoryBillWithDetailForBranch(int[] ids, int[] storageCompanyType, int? storageCompanyId, string storageCompanyCode, string storageCompanyName, int? warehouseId, string warehouseName, string code, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? branchId, int? amountDifferenceRange) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportAgentPartsInventoryBillWithDetailForBranchAsync(ids, storageCompanyType, storageCompanyId, storageCompanyCode, storageCompanyName, warehouseId, warehouseName, code, warehouseAreaCategory, status, createTimeBegin, createTimeEnd, branchId, amountDifferenceRange);
            this.excelServiceClient.ExportAgentPartsInventoryBillWithDetailForBranchCompleted -= excelServiceClient_ExportAgentPartsInventoryBillWithDetailForBranchCompleted;
            this.excelServiceClient.ExportAgentPartsInventoryBillWithDetailForBranchCompleted += excelServiceClient_ExportAgentPartsInventoryBillWithDetailForBranchCompleted;
        }

        private void excelServiceClient_ExportAgentPartsInventoryBillWithDetailForBranchCompleted(object sender, ExportAgentPartsInventoryBillWithDetailForBranchCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AgentsPartsInventoryBill"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            if(compositeFilterItem != null) {
                var compositeFilterItem2 = new CompositeFilterItem();
                var compositeFilterItem3 = new CompositeFilterItem();
                compositeFilterItem2.LogicalOperator = LogicalOperator.Or;
                compositeFilterItem3.LogicalOperator = LogicalOperator.And;
                compositeFilterItem2.Filters.Add(new FilterItem {
                    MemberName = "StorageCompanyType",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = (int)DcsCompanyType.代理库
                });
                compositeFilterItem2.Filters.Add(new FilterItem {
                    MemberName = "StorageCompanyType",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = (int)DcsCompanyType.服务站兼代理库
                });
                compositeFilterItem3.Filters.Add(new FilterItem {
                    MemberName = "BranchId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                });
                compositeFilterItem3.Filters.Add(new FilterItem {
                    MemberName = "StorageCompanyId",
                    Operator = FilterOperator.IsNotEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                });
                compositeFilterItem.Filters.Add(compositeFilterItem2);
                compositeFilterItem.Filters.Add(compositeFilterItem3);
            }
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        //驳回功能
        private RadWindow rejectWindow;
        private RadWindow RejectWindow {
            get {
                if(this.rejectWindow == null) {
                    this.rejectWindow = new RadWindow();
                    this.rejectWindow.CanClose = false;
                    this.rejectWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.rejectWindow.Content = this.RejectDataEditView;
                    this.rejectWindow.ResizeMode = ResizeMode.NoResize;
                    this.rejectWindow.Height = 200;
                    this.rejectWindow.Width = 400;
                    this.rejectWindow.Header = "";
                }
                return this.rejectWindow;
            }
        }

        private DataEditViewBase rejectDataEditView;
        private DataEditViewBase RejectDataEditView {
            get {
                if(this.rejectDataEditView == null) {
                    this.rejectDataEditView = DI.GetDataEditView("AgentsPartsInventoryBillForReject");
                    this.rejectDataEditView.EditCancelled += rejectDataEditView_EditCancelled;
                    this.rejectDataEditView.EditSubmitted += rejectDataEditView_EditSubmitted;
                }
                return this.rejectDataEditView;
            }
        }

        private void rejectDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.RejectWindow.Close();
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void rejectDataEditView_EditCancelled(object sender, EventArgs e) {
            this.RejectWindow.Close();
        }
    }
}
