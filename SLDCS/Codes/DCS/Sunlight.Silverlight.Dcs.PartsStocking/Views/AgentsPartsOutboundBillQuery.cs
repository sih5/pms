﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsOutStore", "AgentsPartsOutboundBillQuery", ActionPanelKeys = new[] {
       CommonActionKeys.EXPORT_MERGEEXPORT_PRINT
    })]
    public class AgentsPartsOutboundBillQuery : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public AgentsPartsOutboundBillQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsOutboundBillForQuery + "-代理库随车行";
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgencyPartsOutboundBillWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AgencyPartsOutboundBill"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<AgencyPartsOutboundBillWithOtherInfo>().Select(e => e.PartsOutboundBillId).ToArray();
                        this.ExportAgencyPartsOutboundBill(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem1 = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem1 == null)
                            return;
                        var code = filterItem1.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var warehouseId = filterItem1.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var partsSalesCategoryId = filterItem1.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var outboundType = filterItem1.Filters.Single(e => e.MemberName == "OutboundType").Value as int?;
                        var counterpartCompanyCode = filterItem1.Filters.Single(e => e.MemberName == "CounterpartCompanyCode").Value as string;
                        var counterpartCompanyName = filterItem1.Filters.Single(e => e.MemberName == "CounterpartCompanyName").Value as string;

                        var partsOutboundPlanCode = filterItem1.Filters.Single(e => e.MemberName == "PartsOutboundPlanCode").Value as string;
                        var provinceName = filterItem1.Filters.Single(e => e.MemberName == "ProvinceName").Value as string;

                        var createTime = filterItem1.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var settlementStatus = filterItem1.Filters.Single(e => e.MemberName == "SettlementStatus").Value as int?;
                        var partsSalesOrderTypeName = filterItem1.Filters.Single(e => e.MemberName == "PartsSalesOrderTypeName").Value as string;

                        var outboundPackPlanCode = filterItem1.Filters.Single(e => e.MemberName == "OutboundPackPlanCode").Value as string;
                        var contractCode = filterItem1.Filters.Single(e => e.MemberName == "ContractCode").Value as string;
                        var ERPSourceOrderCode = filterItem1.Filters.Single(e => e.MemberName == "ERPSourceOrderCode").Value as string;
                        this.ExportAgencyPartsOutboundBill(new int[] { }, partsSalesCategoryId, code, warehouseId, counterpartCompanyCode, counterpartCompanyName, outboundType, settlementStatus, partsSalesOrderTypeName, partsOutboundPlanCode, outboundPackPlanCode, contractCode, provinceName, createTimeBegin, createTimeEnd, ERPSourceOrderCode);
                    }
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<AgencyPartsOutboundBillWithOtherInfo>().Select(e => e.PartsOutboundBillId).ToArray();
                        this.ExportAgencyPartsOutboundBillWithDetail(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var partsSalesCategoryId = filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var outboundType = filterItem.Filters.Single(e => e.MemberName == "OutboundType").Value as int?;
                        var counterpartCompanyCode = filterItem.Filters.Single(e => e.MemberName == "CounterpartCompanyCode").Value as string;
                        var counterpartCompanyName = filterItem.Filters.Single(e => e.MemberName == "CounterpartCompanyName").Value as string;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var settlementStatus = filterItem.Filters.Single(e => e.MemberName == "SettlementStatus").Value as int?;
                        var partsSalesOrderTypeName = filterItem.Filters.Single(e => e.MemberName == "PartsSalesOrderTypeName").Value as string;

                        var partsOutboundPlanCode = filterItem.Filters.Single(e => e.MemberName == "PartsOutboundPlanCode").Value as string;
                        var provinceName = filterItem.Filters.Single(e => e.MemberName == "ProvinceName").Value as string;

                        var outboundPackPlanCode = filterItem.Filters.Single(e => e.MemberName == "OutboundPackPlanCode").Value as string;
                        var contractCode = filterItem.Filters.Single(e => e.MemberName == "ContractCode").Value as string;
                        var ERPSourceOrderCode = filterItem.Filters.Single(e => e.MemberName == "ERPSourceOrderCode").Value as string;
                        this.ExportAgencyPartsOutboundBillWithDetail(new int[] { }, partsSalesCategoryId, code, warehouseId, counterpartCompanyCode, counterpartCompanyName, outboundType, settlementStatus, partsSalesOrderTypeName, partsOutboundPlanCode, outboundPackPlanCode, contractCode, provinceName, createTimeBegin, createTimeEnd, ERPSourceOrderCode);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<AgencyPartsOutboundBillWithOtherInfo>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    dcsDomainContext.Load(dcsDomainContext.GetAgencyPartsOutboundBillsQuery().Where(ex => ex.Id == selectedItem.PartsOutboundBillId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entity = loadOp.Entities.FirstOrDefault();
                        if(entity == null)
                            return;
                        BasePrintWindow printWindow = new AgentsPartsOutboundBillPrintWindow {
                            Header = PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsOutboundBill,
                            AgencyPartsOutboundBill = entity
                        };
                        printWindow.ShowDialog();
                    }, null);

                    break;
            }
        }

        private void ExportAgencyPartsOutboundBillWithDetail(int[] ids, int? partsSalesCategoryId, string partsInboundCheckBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? inboundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutboundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string ERPSourceOrderCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportAgencyPartsOutboundBillWithDetailAsync(ids, BaseApp.Current.CurrentUserData.UserId, partsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId, partsInboundCheckBillCode, warehouseId, counterpartcompanycode, counterpartcompanyname, inboundType, settlementStatus, partsSalesOrderTypeName, partsOutboundPlanCode, outboundPackPlanCode, contractCode, provinceName, createTimeBegin, createTimeEnd, ERPSourceOrderCode);
            this.excelServiceClient.ExportAgencyPartsOutboundBillWithDetailCompleted -= excelServiceClient_ExportAgencyPartsOutboundBillWithDetailCompleted;
            this.excelServiceClient.ExportAgencyPartsOutboundBillWithDetailCompleted += excelServiceClient_ExportAgencyPartsOutboundBillWithDetailCompleted;
        }

        private void ExportAgencyPartsOutboundBill(int[] ids, int? partsSalesCategoryId, string partsInboundCheckBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? inboundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutBoundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string ERPSourceOrderCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportAgencyPartsOutboundBillAsync(ids, BaseApp.Current.CurrentUserData.UserId, partsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId, partsInboundCheckBillCode, warehouseId, counterpartcompanycode, counterpartcompanyname, inboundType, settlementStatus, partsSalesOrderTypeName, partsOutBoundPlanCode, outboundPackPlanCode, contractCode, provinceName, createTimeBegin, createTimeEnd, ERPSourceOrderCode);
            this.excelServiceClient.ExportAgencyPartsOutboundBillCompleted -= excelServiceClient_ExportPartsOutboundBillCompleted;
            this.excelServiceClient.ExportAgencyPartsOutboundBillCompleted += excelServiceClient_ExportPartsOutboundBillCompleted;
        }

        private void excelServiceClient_ExportPartsOutboundBillCompleted(object sender, ExportAgencyPartsOutboundBillCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        protected void excelServiceClient_ExportAgencyPartsOutboundBillWithDetailCompleted(object sender, ExportAgencyPartsOutboundBillWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItemForPrint = this.DataGridView.SelectedEntities.Cast<AgencyPartsOutboundBillWithOtherInfo>().ToArray();
                    return selectItemForPrint.Length == 1;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
