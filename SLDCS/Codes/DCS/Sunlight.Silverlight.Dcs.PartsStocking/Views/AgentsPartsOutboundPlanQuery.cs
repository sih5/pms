﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsOutStore", "AgentsPartsOutboundPlanQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT_MERGEEXPORT_PRINT
    })]
    public class AgentsPartsOutboundPlanQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public AgentsPartsOutboundPlanQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsOutboundPlan + "-代理库随车行";
        }

        private DataGridViewBase DataGridView {
            get {
                return dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgencyPartsOutboundPlanWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AgencyPartsOutboundPlan"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<AgencyPartsOutboundPlan>().Select(r => r.Id).ToArray();
                        this.dcsDomainContext.ExportAgencyPartsOutboundPlan(ids, null, null, null, null, null, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var code = compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var sourceCode = compositeFilterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                            var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var status = compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            var warehouseId = compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                            var outboundType = compositeFilterItem.Filters.Single(r => r.MemberName == "OutboundType").Value as int?;
                            var counterpartCompanyCode = compositeFilterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyCode").Value as string;
                            var counterpartCompanyName = compositeFilterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyName").Value as string;
                            var partsSalesOrderTypeName = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderTypeName").Value as string;
                            var ERPSourceOrderCode = compositeFilterItem.Filters.Single(r => r.MemberName == "ERPSourceOrderCode").Value as string;
                            var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? begianDate = null;
                            DateTime? endDate = null;
                            if(createTime != null) {
                                begianDate = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                endDate = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            this.dcsDomainContext.ExportAgencyPartsOutboundPlan(new int[] { }, partsSalesCategoryId, code, sourceCode, warehouseId, status, outboundType, begianDate, endDate, counterpartCompanyCode, counterpartCompanyName, partsSalesOrderTypeName, ERPSourceOrderCode, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 合并导出参数为 出库计划ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<AgencyPartsOutboundPlan>().Select(e => e.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null, null, null, null, null);
                    }
                        //否则 合并导出参数为 出库计划编号 源单据编号 仓库名称	出库类型	创建时间	对方单位编号	对方单位名称
                    else {

                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var outboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OutboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OutboundType").Value as int?;
                        var counterpartCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyCode").Value as string;
                        var counterpartCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyName").Value as string;
                        var ERPSourceOrderCode = filterItem.Filters.Single(r => r.MemberName == "ERPSourceOrderCode").Value as string;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? begianDate = null;
                        DateTime? endDate = null;
                        if(createTime != null) {
                            begianDate = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            endDate = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var partsSalesOrderTypeName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesOrderTypeName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderTypeName").Value as string;
                        this.ExecuteMergeExport(new int[] { }, partsSalesCategoryId, code, status, sourceCode, warehouseId, outboundType, begianDate, endDate, counterpartCompanyCode, counterpartCompanyName, partsSalesOrderTypeName, ERPSourceOrderCode);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<AgencyPartsOutboundPlan>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow;
                    printWindow = new AgentsPartsOutboundPlanPrintWindow {
                        Header = "出库计划单打印",
                        AgencyPartsOutboundPlan = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;


            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<AgencyPartsOutboundPlan>().ToArray();
                    return selectItems.Length == 1;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        /// <summary>
        /// 合并导出配件出库计划主清单
        /// </summary>
        private void ExecuteMergeExport(int[] ids, int? partsSalesCategoryId, string code, int? status, string sourceCode, int? warehouseId, int? outboundType, DateTime? begianDate, DateTime? endDate, string counterpartCompanyCode, string counterpartCompanyName, string partsSalesOrderTypeName, string ERPSourceOrderCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsOutboundPlanWithDetailForQueryAsync(ids, BaseApp.Current.CurrentUserData.UserId, partsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId, code, status, sourceCode, warehouseId, outboundType, begianDate, endDate, counterpartCompanyCode, counterpartCompanyName, partsSalesOrderTypeName, ERPSourceOrderCode);
            this.excelServiceClient.ExportPartsOutboundPlanWithDetailForQueryCompleted -= this.ExcelServiceClient_ExportPartsOutboundPlanWithDetailForQueryCompleted;
            this.excelServiceClient.ExportPartsOutboundPlanWithDetailForQueryCompleted += this.ExcelServiceClient_ExportPartsOutboundPlanWithDetailForQueryCompleted;
        }

        private void ExcelServiceClient_ExportPartsOutboundPlanWithDetailForQueryCompleted(object sender, ExportPartsOutboundPlanWithDetailForQueryCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
