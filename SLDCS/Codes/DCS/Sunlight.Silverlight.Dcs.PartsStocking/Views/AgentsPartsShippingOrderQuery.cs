﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsLogisticsRelated", "AgentsPartsShippingOrder", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT_MERGEEXPORT
    })]
    public class AgentsPartsShippingOrderQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public AgentsPartsShippingOrderQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsShippingOrder + "-代理库";
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("AgencyPartsShippingOrderWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "AgencyPartsShippingOrder"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.dataGridView.SelectedEntities != null && this.dataGridView.SelectedEntities.Any() && this.DataGridView.Entities.Any(); 
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.MERGEEXPORT:
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<AgencyPartsShippingOrder>().Select(r => r.Id).ToArray();
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                            this.MergeExport(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }
                        if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                            this.Export(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var settlementCompanyId = filterItem.Filters.Single(e => e.MemberName == "SettlementCompanyId").Value as int?;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var closedLoopStatus = filterItem.Filters.Single(e => e.MemberName == "ClosedLoopStatus").Value as int?;
                        var receivingCompanyCode = filterItem.Filters.Single(e => e.MemberName == "ReceivingCompanyCode").Value as string;
                        var receivingCompanyName = filterItem.Filters.Single(e => e.MemberName == "ReceivingCompanyName").Value as string;
                        var receivingWarehouseName = filterItem.Filters.Single(e => e.MemberName == "ReceivingWarehouseName").Value as string;
                        var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var logisticCompanyName = filterItem.Filters.Single(e => e.MemberName == "LogisticCompanyName").Value as string;
                        var expressCompany = filterItem.Filters.Single(e => e.MemberName == "ExpressCompany").Value as string;
                        var expressCompanyCode = filterItem.Filters.Single(e => e.MemberName == "ExpressCompanyCode").Value as string;
                        var gpscode = filterItem.Filters.Single(e => e.MemberName == "GPSCode").Value as string;
                        DateTime? beginCreateTime = null;
                        DateTime? endCreateTime = null;

                        DateTime? beginRequestedArrivalDate = null;
                        DateTime? endRequestedArrivalDate = null;

                        DateTime? beginConfirmedReceptionTime = null;
                        DateTime? endConfirmedReceptionTime = null;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    beginCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    endCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "RequestedArrivalDate") {
                                    beginRequestedArrivalDate = dateTime.Filters.First(r => r.MemberName == "RequestedArrivalDate").Value as DateTime?;
                                    endRequestedArrivalDate = dateTime.Filters.Last(r => r.MemberName == "RequestedArrivalDate").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "ConfirmedReceptionTime") {
                                    beginConfirmedReceptionTime = dateTime.Filters.First(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                    endConfirmedReceptionTime = dateTime.Filters.Last(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                }
                            }
                        }
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId))
                            this.MergeExport(new int[] { }, settlementCompanyId, code, type, status, warehouseId, null, beginRequestedArrivalDate, endRequestedArrivalDate, null, null, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, null, null, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, gpscode);
                        if(CommonActionKeys.EXPORT.Equals(uniqueId))
                            this.Export(new int[] { }, settlementCompanyId, code, type, status, warehouseId, null, beginRequestedArrivalDate, endRequestedArrivalDate, null, null, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, null, null, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, gpscode);
                    }
                    break;
            }
        }

        private void MergeExport(int[] ids, int? settlementCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginConfirmedReceptionTime, DateTime? endConfirmedReceptionTime, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string gpscode) {
            this.excelServiceClient.ExportAgencyPartsShippingOrderAsync(ids, settlementCompanyId, null, code, type, status, warehouseId, warehouseName, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, logisticCompanyId, logisticCompanyCode, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, gpscode);
            this.excelServiceClient.ExportAgencyPartsShippingOrderCompleted -= excelServiceClient_ExportAgencyPartsShippingOrderCompleted;
            this.excelServiceClient.ExportAgencyPartsShippingOrderCompleted += excelServiceClient_ExportAgencyPartsShippingOrderCompleted;
        }

        private void Export(int[] ids, int? settlementCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginConfirmedReceptionTime, DateTime? endConfirmedReceptionTime, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string gpscode) {
            this.excelServiceClient.ExportAgencyPartsShippingOrderNotWithDetailAsync(ids, settlementCompanyId, null, code, type, status, warehouseId, warehouseName, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, logisticCompanyId, logisticCompanyCode, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, gpscode);
            this.excelServiceClient.ExportAgencyPartsShippingOrderNotWithDetailCompleted -= excelServiceClient_ExportAgencyPartsShippingOrderNotWithDetailCompleted;
            this.excelServiceClient.ExportAgencyPartsShippingOrderNotWithDetailCompleted += excelServiceClient_ExportAgencyPartsShippingOrderNotWithDetailCompleted;
        }

        private void excelServiceClient_ExportAgencyPartsShippingOrderCompleted(object sender, ExportAgencyPartsShippingOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void excelServiceClient_ExportAgencyPartsShippingOrderNotWithDetailCompleted(object sender, ExportAgencyPartsShippingOrderNotWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                var timeFilters = compositeFilterItem.Filters.Where(filter => filter is CompositeFilterItem && ((CompositeFilterItem)filter).Filters.Any(item => (new[] {
                        "CreateTime", "ConfirmedReceptionTime", "RequestedArrivalDate", "ShippingDate"
                    }).Contains(item.MemberName))).Cast<CompositeFilterItem>();
                if(timeFilters != null && timeFilters.Any()) {
                    foreach(var timeFilter in timeFilters) {
                        DateTime endTime;
                        if(timeFilter.Filters.ElementAt(1).Value is DateTime && DateTime.TryParse(timeFilter.Filters.ElementAt(1).Value.ToString(), out endTime)) {
                            timeFilter.Filters.ElementAt(1).Value = new DateTime(endTime.Year, endTime.Month, endTime.Day, 23, 59, 59);
                        }
                    }
                }
            } else
                compositeFilterItem.Filters.Add(filterItem);
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SettlementCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}