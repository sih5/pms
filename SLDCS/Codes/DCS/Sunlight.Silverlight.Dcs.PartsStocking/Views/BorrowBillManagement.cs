﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "WarehouseExecution", "BorrowBill", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT_AUDIT_APPROVE_EXPORT,"BorrowBill"
    })]
    public class BorrowBillManagement : DcsDataManagementViewBase {
        private const string DATA_APPROVEE_VIEW = "_dataApproveView_";
        private const string DATA_RETURNSUBMIT_VIEW = "_DataReturnSubmitView_";
        private const string DATA_RETURNAPPROVE_VIEW = "_DataReturnApproveView_";
        private const string DATA_INITIALAPPROVE_VIEW = "_DataInitialApproveView_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataInitialApproveView;
        private DataEditViewBase dataReturnSubmitView;
        private DataEditViewBase dataReturnApproveView;
        private DataEditViewBase dataDetailView;

        public BorrowBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_BorrowBill;
        }

        public DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("BorrowBill");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }

        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("BorrowBillWithDetails");
                    this.dataDetailView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("BorrowBill");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        public DataEditViewBase DataInitialApproveView {
            get {
                if(this.dataInitialApproveView == null) {
                    this.dataInitialApproveView = DI.GetDataEditView("BorrowBillForInitialApprove");
                    ((BorrowBillForInitialApproveDataEditView)this.dataInitialApproveView).EditSubmitted += dataEditView_EditSubmitted;
                    this.dataInitialApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataInitialApproveView;
            }
        }

        public DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("BorrowBillForApprove");
                    ((BorrowBillForApproveDataEditView)this.dataApproveView).EditSubmitted += dataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        public DataEditViewBase DataReturnSubmitView {
            get {
                if(this.dataReturnSubmitView == null) {
                    this.dataReturnSubmitView = DI.GetDataEditView("BorrowBillForReturnSubmit");
                    this.dataReturnSubmitView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataReturnSubmitView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataReturnSubmitView;
            }
        }

        public DataEditViewBase DataReturnApproveView {
            get {
                if(this.dataReturnApproveView == null) {
                    this.dataReturnApproveView = DI.GetDataEditView("BorrowBillForReturnApprove");
                    this.dataReturnApproveView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataReturnApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataReturnApproveView;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]{
                    "BorrowBill"
                };
            }
        }
        private void ResetEditView() {
            this.dataEditView = null; 
            this.dataApproveView = null; 
            this.dataInitialApproveView = null; 
            this.dataReturnSubmitView = null; 
            this.dataReturnApproveView = null;
            this.dataDetailView = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_INITIALAPPROVE_VIEW, () => this.DataInitialApproveView);
            this.RegisterView(DATA_APPROVEE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_RETURNSUBMIT_VIEW, () => this.DataReturnSubmitView);
            this.RegisterView(DATA_RETURNAPPROVE_VIEW, () => this.DataReturnApproveView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
        }

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var borrowBill = this.DataEditView.CreateObjectToEdit<BorrowBill>();
                    borrowBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    borrowBill.Status = (int)DcsBorrowBillStatus.新增;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<BorrowBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废配件借用单)
                                entity.作废配件借用单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<BorrowBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交配件借用单)
                                entity.提交配件借用单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                this.CheckActionsCanExecute();
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "DeliverParts": //出库
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_DeliverPart, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<BorrowBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can出库配件借用单)
                                entity.出库配件借用单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_DeliverPartSuccess);
                                this.CheckActionsCanExecute();
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.AUDIT: //初审
                    this.DataInitialApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INITIALAPPROVE_VIEW);
                    break;
                case CommonActionKeys.APPROVE: //终审
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVEE_VIEW);
                    break;
                case "ReturnSubmit": //归还提交 
                    this.DataReturnSubmitView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_RETURNSUBMIT_VIEW);
                    break;
                case "ReturnApprove": //归还审核 
                    this.DataReturnApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_RETURNAPPROVE_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<BorrowBill>().Select(r => r.Id).ToArray();
                        this.ExportBorrowBill(ids, null, null, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                        var createTime = filterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).ToArray();
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? ApproveTimeBegin = null;
                        DateTime? ApproveTimeEnd = null;
                        DateTime? OutBoundTimeBegin = null;
                        DateTime? OutBoundTimeEnd = null;
                        DateTime? ExpectReturnTimeBegin = null;
                        DateTime? ExpectReturnTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "CreateTime").Value : null);
                            createTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.LastOrDefault(r => r.MemberName == "CreateTime").Value : null);

                            ApproveTimeBegin = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "ApproveTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "ApproveTime").Value : null);
                            ApproveTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "ApproveTime") != null ? createTime.LastOrDefault(r => r.MemberName == "ApproveTime").Value : null);

                            OutBoundTimeBegin = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "OutBoundTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "OutBoundTime").Value : null);
                            OutBoundTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "OutBoundTime") != null ? createTime.LastOrDefault(r => r.MemberName == "OutBoundTime").Value : null);

                            ExpectReturnTimeBegin = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "ExpectReturnTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "ExpectReturnTime").Value : null);
                            ExpectReturnTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "ExpectReturnTime") != null ? createTime.LastOrDefault(r => r.MemberName == "ExpectReturnTime").Value : null);
                        }

                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        this.ExportBorrowBill(null, code, warehouseId, type, status, createTimeBegin, createTimeEnd, ApproveTimeBegin, ApproveTimeEnd, OutBoundTimeBegin, OutBoundTimeEnd, ExpectReturnTimeBegin, ExpectReturnTimeEnd);
                    }
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<BorrowBill>().Select(r => r.Id).ToArray();
                        this.ExportBorrowBillWithDetail(ids, null, null, null, null, null, null, null, null, null, null, null, null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                        var createTime = filterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).ToArray();
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? ApproveTimeBegin = null;
                        DateTime? ApproveTimeEnd = null;
                        DateTime? OutBoundTimeBegin = null;
                        DateTime? OutBoundTimeEnd = null;
                        DateTime? ExpectReturnTimeBegin = null;
                        DateTime? ExpectReturnTimeEnd = null;
                        if (createTime != null)
                        {
                            createTimeBegin = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "CreateTime").Value : null);
                            createTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.LastOrDefault(r => r.MemberName == "CreateTime").Value : null);

                            ApproveTimeBegin = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "ApproveTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "ApproveTime").Value : null);
                            ApproveTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "ApproveTime") != null ? createTime.LastOrDefault(r => r.MemberName == "ApproveTime").Value : null);

                            OutBoundTimeBegin = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "OutBoundTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "OutBoundTime").Value : null);
                            OutBoundTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "OutBoundTime") != null ? createTime.LastOrDefault(r => r.MemberName == "OutBoundTime").Value : null);

                            ExpectReturnTimeBegin = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "ExpectReturnTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "ExpectReturnTime").Value : null);
                            ExpectReturnTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "ExpectReturnTime") != null ? createTime.LastOrDefault(r => r.MemberName == "ExpectReturnTime").Value : null);
                        }

                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        this.ExportBorrowBillWithDetail(null, code, warehouseId, type, status, createTimeBegin, createTimeEnd, ApproveTimeBegin, ApproveTimeEnd, OutBoundTimeBegin, OutBoundTimeEnd, ExpectReturnTimeBegin, ExpectReturnTimeEnd);
                    }
                    break;
            }
        }

        private void ExportBorrowBill(int[] ids, string Code, int? WarehouseId, int? Type, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? ApproveTimeBegin, DateTime? ApproveTimeEnd, DateTime? OutBoundTimeBegin, DateTime? OutBoundTimeEnd, DateTime? ExpectReturnTimeBegin, DateTime? ExpectReturnTimeEnd) {
            this.dcsDomainContext.ExportBorrowBill(ids, Code, WarehouseId, Type, status, createTimeBegin, createTimeEnd, ApproveTimeBegin, ApproveTimeEnd, OutBoundTimeBegin, OutBoundTimeEnd, ExpectReturnTimeBegin, ExpectReturnTimeEnd, loadOp => {
                if(loadOp.HasError) {
                    ShellViewModel.Current.IsBusy = false;
                    return;
                }
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                    ShellViewModel.Current.IsBusy = false;
                }
                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    ShellViewModel.Current.IsBusy = false;
                }
            }, null);
        }
        private void ExportBorrowBillWithDetail(int[] ids, string Code, int? WarehouseId, int? Type, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? ApproveTimeBegin, DateTime? ApproveTimeEnd, DateTime? OutBoundTimeBegin, DateTime? OutBoundTimeEnd, DateTime? ExpectReturnTimeBegin, DateTime? ExpectReturnTimeEnd)
        {
            this.dcsDomainContext.ExportBorrowBillWithDetail(ids, Code, WarehouseId, Type, status, createTimeBegin, createTimeEnd, ApproveTimeBegin, ApproveTimeEnd, OutBoundTimeBegin, OutBoundTimeEnd, ExpectReturnTimeBegin, ExpectReturnTimeEnd, loadOp =>
            {
                if (loadOp.HasError)
                {
                    ShellViewModel.Current.IsBusy = false;
                    return;
                }
                if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                {
                    UIHelper.ShowNotification(loadOp.Value);
                    ShellViewModel.Current.IsBusy = false;
                }
                if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    ShellViewModel.Current.IsBusy = false;
                }
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.AUDIT: //初审
                case CommonActionKeys.APPROVE: //终审
                case "DeliverParts": //出库
                case "ReturnSubmit": //归还提交
                case "ReturnApprove": //归还审核
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<BorrowBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == CommonActionKeys.AUDIT)
                        return entities[0].Status == (int)DcsBorrowBillStatus.提交;
                    if(uniqueId == CommonActionKeys.APPROVE)
                        return entities[0].Status == (int)DcsBorrowBillStatus.初审通过;
                    if(uniqueId == "DeliverParts")
                        return entities[0].Status == (int)DcsBorrowBillStatus.终审通过;
                    if(uniqueId == "ReturnSubmit")
                        return entities[0].Status == (int)DcsBorrowBillStatus.已出库 || entities[0].Status == (int)DcsBorrowBillStatus.部分确认;
                    if(uniqueId == "ReturnApprove")
                        return entities[0].Status == (int)DcsBorrowBillStatus.归还待确认;
                    return entities[0].Status == (int)DcsBorrowBillStatus.新增;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
