﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsOutStore", "BoxUpTask", ActionPanelKeys = new[]{
        "BoxUpTask","AbandonMergeExport"
    })]
    public class BoxUpTaskManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataDetailEditView;
        private const string DATA_DETAIL_VIEW = "_dataDetailView_";


        public BoxUpTaskManagement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_BoxUpTask;
        }

        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailEditView);
        }

        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("BoxUpTaskWithDetails"));
            }
        }

        private DataEditViewBase DataEditView
        {
            get
            {
                if (this.dataEditView == null)
                {
                    this.dataEditView = DI.GetDataEditView("BoxUpTask");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEdtiView_EditCancelled;
                }
                return this.dataEditView;
            }

        }

        private DataEditViewBase DataDetailEditView
        {
            get
            {
                if (this.dataDetailEditView == null)
                {
                    this.dataDetailEditView = DI.GetDataEditView("BoxUpTaskDetail");
                    this.dataDetailEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataDetailEditView.EditCancelled += this.DataEdtiView_EditCancelled;
                }
                return this.dataDetailEditView;
            }

        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataDetailEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e)
        {
            this.ResetEditView();
            if (this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);

        }

        private void DataEdtiView_EditCancelled(object sender, EventArgs e)
        {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }


        protected override IEnumerable<string> QueryPanelKeys
        {
            get{
                return new[] {
                    "BoxUpTask"
                };
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch(uniqueId){
                case "Create":
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "BoxUp":
                    this.DataDetailEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                      DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualBoxUpTask>().SingleOrDefault();
                        if(entity == null)
                            return;
                        ((IEditableObject)entity).EndEdit();
                        try {
                            if (entity.CanabandonBoxUpTask) {
                                entity.abandonBoxUpTask();
                            }
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                                this.DataGridView.ExecuteQuery();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Stop":
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_StopBoxUp, () =>
                    {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualBoxUpTask>().SingleOrDefault();
                        if (entity == null)
                            return;
                        ((IEditableObject)entity).EndEdit();
                        try
                        {
                            if (entity.CanstopBoxUpTask) {
                                entity.stopBoxUpTask();
                            }
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if (domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp =>
                            {
                                if (submitOp.HasError)
                                {
                                    if (!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Text_StopBoxUpSuccess);
                                this.CheckActionsCanExecute();
                                this.DataGridView.ExecuteQuery();
                            }, null);
                        }
                        catch (Exception ex)
                        {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;

                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 合并导出参数为 装箱任务单ID 
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualBoxUpTask>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null, null, null,null,null,null,null);
                    }
                    //否则 合并导出参数为 装箱任务单号，仓库名称，品牌id，状态，原单据编号，出库计划单号，对方单位编号，对方单位名称，创建时间，修改时间
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        var partsOutboundPlanCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsOutboundPlanCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsOutboundPlanCode").Value as string;
                        var counterpartCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyCode").Value as string;
                        var counterpartCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyName").Value as string;
                        var sparePartCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? modifyTimeBegin = null;
                        DateTime? modifyTimeEnd = null;
                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                        {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if (dateTime.Filters.First().MemberName == "CreateTime")
                                {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if (dateTime.Filters.First().MemberName == "ModifyTime")
                                {
                                    modifyTimeBegin = dateTime.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?;
                                    modifyTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?;
                                }
                            }
                        }


                        this.ExecuteMergeExport(null, code, warehouseId, partsSalesCategoryId, status, sourceCode, partsOutboundPlanCode, counterpartCompanyCode, counterpartCompanyName, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd,sparePartCode,sparePartName);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<VirtualBoxUpTask>().FirstOrDefault();
                    if (selectedItem == null)
                        return;
                    //BoxUpTaskPrintWindow printWindow = new BoxUpTaskPrintWindow
                    //{
                    //    Header = "装箱任务单打印",
                    //    VirtualBoxUpTask = selectedItem
                    //};
                    //printWindow.ShowDialog();
                    SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataManagementView_Text_PrintBoxUpTask, "ReportBoxUpTask", null, true, new Tuple<string, string>("boxUpTaskId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

                    break;
                case "A4Print":
                      var selectedItemA4 = this.DataGridView.SelectedEntities.Cast<VirtualBoxUpTask>().FirstOrDefault();
                      if(selectedItemA4 == null)
                        return;
                      SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataManagementView_Text_PrintBoxUpTask, "ReportBoxUpTaskForA4", null, true, new Tuple<string, string>("boxUpTaskId", selectedItemA4.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));
                    break;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "Create":
                    return true;
                case "BoxUp":
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1) {
                        var boxupTask = DataGridView.SelectedEntities.Cast<VirtualBoxUpTask>().FirstOrDefault();
                        if (boxupTask.Status == (int)DcsBoxUpTaskStatus.新建 || boxupTask.Status == (int)DcsBoxUpTaskStatus.部分装箱 || boxupTask.Status == (int)DcsBoxUpTaskStatus.停用)
                        {
                            return true;
                        }
                    }
                    return  false;
                case "Stop":
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1)
                    {
                        var boxupTask = DataGridView.SelectedEntities.Cast<VirtualBoxUpTask>().FirstOrDefault();
                        if (boxupTask.Status == (int)DcsBoxUpTaskStatus.停用) {
                            return true;
                        }
                    }
                    return false;
                case CommonActionKeys.ABANDON:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1)
                    {
                        var boxupTask = DataGridView.SelectedEntities.Cast<VirtualBoxUpTask>().FirstOrDefault();
                        if (boxupTask.Status == (int)DcsBoxUpTaskStatus.新建)
                        {
                            return true;
                        }
                    }
                    return false;
                case "A4Print":
                case CommonActionKeys.PRINT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int[] ids, string code, int? warehouseId, int? partsSalesCategoryId, int? status, string sourceCode, string partsOutboundPlanCode, string counterpartCompanyCode, string counterpartCompanyName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd,string sparePartCode,string sparePartName)
        {
            ShellViewModel.Current.IsBusy = false;
            this.excelServiceClient.ExportBoxUpTaskAsync(ids, code, warehouseId, partsSalesCategoryId, status, sourceCode, partsOutboundPlanCode, counterpartCompanyCode, counterpartCompanyName, createTimeBegin, createTimeEnd, modifyTimeBegin, modifyTimeEnd,sparePartCode,sparePartName);
            this.excelServiceClient.ExportBoxUpTaskCompleted -= this.ExcelServiceClient_ExportBoxUpTaskCompleted;
            this.excelServiceClient.ExportBoxUpTaskCompleted += this.ExcelServiceClient_ExportBoxUpTaskCompleted;
        }

        private void ExcelServiceClient_ExportBoxUpTaskCompleted(object sender, ExportBoxUpTaskCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
