﻿using System;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.Custom {
    public partial class PartsInboundPackingDetailView {
        private DataGridViewBase partsInboundPackingDetailDataGridView;

        public PartsInboundPackingDetailView() {
            InitializeComponent();
            this.CreateUI();
        }

        private DataGridViewBase PartsInboundPackingDetailDataGridView {
            get {
                if(this.partsInboundPackingDetailDataGridView == null) {
                    this.partsInboundPackingDetailDataGridView = DI.GetDataGridView("PartsInboundPackingDetail");
                    this.partsInboundPackingDetailDataGridView.DataContext = this.DataContext;
                }
                return this.partsInboundPackingDetailDataGridView;
            }
        }

        private void CreateUI() {
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(Utils.GetEntityLocalizedName(typeof(PartsInboundCheckBill), "PartsInboundPackingDetails"), null, () => this.PartsInboundPackingDetailDataGridView);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            dcsDetailGridView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/export.png", UriKind.Relative),
                Command = new DelegateCommand(this.ExportData)
            });
            this.Root.Children.Add(dcsDetailGridView);
        }

        private void ExportData() {
            ((DcsDataGridViewBase)this.PartsInboundPackingDetailDataGridView).ExportData();
        }
    }
}
