﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.Custom {
    public partial class PartsPackingView : IBaseView, INotifyPropertyChanged {
        private VirtualPartsStock selectedvirtualPartsStock = new VirtualPartsStock();
        private DataGridViewBase virtualPartsStockForPartsPackingDataGridView;
        public event PropertyChangedEventHandler PropertyChanged;
        private DcsDomainContext domainContext;
        private bool printPaperNumberIsEnabled;
        private int printNumber = 1;

        public PartsPackingView() {
            InitializeComponent();
            this.CreateUI();
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Content = this.VirtualPartsStockForPartsPackingDataGridView,
                Header = PartsStockingUIStrings.DataGridView_Title_VirtualPartsStock
            });
            this.Root.Children.Add(tabControl);
            this.PrintsLabels.Click += this.PrintsLabels_Click;
        }

        private void VirtualPartsStockForPartsPackingDataGridView_SelectionChanged(Object Sender, EventArgs e) {
            var selectData = ((DcsDataGridViewBase)Sender).SelectedEntities.FirstOrDefault();
            if(selectData == null) {
                this.PrintPaperNumberIsEnabled = false;
                return;
            }
            this.PrintPaperNumberIsEnabled = true;
            var virtualPartsStock = selectData as VirtualPartsStock;
            this.selectedvirtualPartsStock = virtualPartsStock;
            var partsPacking = new PartsPacking();
            if(virtualPartsStock != null) {
                partsPacking.SparePartCode = virtualPartsStock.SparePartCode;
                partsPacking.SparePartName = virtualPartsStock.SparePartName;
                if(virtualPartsStock.PackingAmount != null)
                    partsPacking.Quantity = (int)virtualPartsStock.PackingAmount;
            }
            this.DataContext = partsPacking;
        }

        private void PrintsLabels_Click(object sender, RoutedEventArgs e) {
            var partsPacking = this.DataContext as PartsPacking;
            if(partsPacking == null)
                return;
            partsPacking.ValidationErrors.Clear();
            if(partsPacking.Quantity <= default(int))
                partsPacking.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.CustomView_Validation_VirtualPartsStock_QuantityIsNotNull, new[] {
                        "Quantity"
                    }));
            if(PrintNumber <= default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.CustomView_Validation_PrintNumberIsNotNull);
                return;
            }
            if(partsPacking.HasValidationErrors)
                return;
            DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Submit, () => {
                partsPacking.SparePartCode = selectedvirtualPartsStock.SparePartCode;
                partsPacking.SparePartName = selectedvirtualPartsStock.SparePartName;
                partsPacking.Quantity = this.selectedvirtualPartsStock.PackingAmount.GetValueOrDefault();
                partsPacking.BatchNumber = GlobalVar.ASSIGNED_BY_SERVER;
                partsPacking.PartsStockId = selectedvirtualPartsStock.PartsStockId;
                partsPacking.WarehouseId = selectedvirtualPartsStock.WarehouseId;
                partsPacking.WarehouseCode = selectedvirtualPartsStock.WarehouseCode;
                partsPacking.WarehouseName = selectedvirtualPartsStock.WarehouseName;
                partsPacking.StorageCompanyId = selectedvirtualPartsStock.StorageCompanyId;
                partsPacking.StorageCompanyType = selectedvirtualPartsStock.StorageCompanyType;
                partsPacking.SparePartId = selectedvirtualPartsStock.SparePartId;
                partsPacking.WarehouseAreaId = selectedvirtualPartsStock.WarehouseAreaId;
                partsPacking.WarehouseAreaCode = selectedvirtualPartsStock.WarehouseAreaCode;
                try {
                    if(!this.DomainContext.PartsPackings.Contains(partsPacking))
                        this.DomainContext.PartsPackings.Add(partsPacking);
                    if(!partsPacking.Can生成配件包装单)
                        return;
                    partsPacking.生成配件包装单();
                    this.DomainContext.SubmitChanges(submitOp => {
                        if(submitOp.HasError) {
                            if(!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                            this.DomainContext.PartsPackings.Detach(partsPacking);
                            return;
                        }
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_SubmitSuccess);
                        var virtualPartsStock = this.VirtualPartsStockForPartsPackingDataGridView.SelectedEntities.FirstOrDefault() as VirtualPartsStock;
                        if(this.DomainContext.VirtualPartsStocks.Contains(virtualPartsStock)) {
                            this.DomainContext.VirtualPartsStocks.Detach(virtualPartsStock);
                        }
                        if(virtualPartsStock != null)
                            virtualPartsStock.UsableQuantity = selectedvirtualPartsStock.UsableQuantity - partsPacking.Quantity;
                    }, null);
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            });
            //todo：打印暂未实现
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            return null;
        }

        public int PrintNumber {
            get {
                return this.printNumber;
            }
            set {
                this.printNumber = value;
                this.OnPropertyChanged("PrintNumber");
            }
        }

        public bool PrintPaperNumberIsEnabled {
            get {
                return this.printPaperNumberIsEnabled;
            }
            set {
                this.printPaperNumberIsEnabled = value;
                this.OnPropertyChanged("PrintPaperNumberIsEnabled");
            }
        }

        public DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        public DataGridViewBase VirtualPartsStockForPartsPackingDataGridView {
            get {
                if(this.virtualPartsStockForPartsPackingDataGridView == null) {
                    this.virtualPartsStockForPartsPackingDataGridView = DI.GetDataGridView("VirtualPartsStockForPartsPacking");
                    virtualPartsStockForPartsPackingDataGridView.SelectionChanged += this.VirtualPartsStockForPartsPackingDataGridView_SelectionChanged;
                    virtualPartsStockForPartsPackingDataGridView.DomainContext = this.DomainContext;
                }
                return this.virtualPartsStockForPartsPackingDataGridView;
            }
        }
    }
}
