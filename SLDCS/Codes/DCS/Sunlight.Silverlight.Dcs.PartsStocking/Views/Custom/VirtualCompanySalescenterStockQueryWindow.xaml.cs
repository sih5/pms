﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.RibbonView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.Custom {
    public partial class VirtualCompanySalescenterStockQueryWindow : IBaseView {
        private Grid filterContainer;
        private bool initialize;
        private ICommand searchCommand;
        private DataGridViewBase dataGridView;
        private DcsDomainContext domainContext;
        private DcsComboBox partsSalesCategoryIdCombobox;
        private DcsComboBox branchIdCombobox;
        public event EventHandler<DataGridViewRowDoubleClickEventArgs> RowDoubleClick;
        private ObservableCollection<PartsSalesCategory> kvPartsSalesCategorys;
        private ObservableCollection<Branch> kvBranchs;
        private CompositeFilterItem compositeFilterItem;


        private ICommand SearchCommand {
            get {
                if(this.searchCommand == null)
                    searchCommand = new Core.Command.DelegateCommand(this.QueryButtonExecute);
                return searchCommand;
            }
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VirtualCompanySalescenterStock"));
            }
        }


        private DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        private ObservableCollection<PartsSalesCategory> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<PartsSalesCategory>());
            }
        }

        private CompositeFilterItem CompositeFilter {
            get {
                return this.compositeFilterItem ?? (this.compositeFilterItem = new CompositeFilterItem());
            }
        }

        private ObservableCollection<Branch> KvBranchs {
            get {
                return this.kvBranchs ?? (this.kvBranchs = new ObservableCollection<Branch>());
            }
        }
        private IDictionary<string, object> GetFilterValues(Grid filterGrid) {
            var result = new Dictionary<string, object>();
            foreach(var control in filterGrid.Children.ToArray()) {
                var itemName = ((FrameworkElement)control).Tag as string;
                if(itemName == null)
                    continue;

                object value = null;
                if(control is RadComboBox && (itemName == "BrandId" || itemName == "PartsSalesCategoryId")) {
                    value = ((RadComboBox)control).SelectedValue ?? string.Empty;
                }
                if(control is RadComboBox && itemName != "BrandId" && itemName != "PartsSalesCategoryId")
                    value = ((RadComboBox)control).SelectedValue ?? -1;
                else if(control is RadMaskedNumericInput)
                    value = ((RadMaskedNumericInput)control).Value;
                result.Add(itemName, value);
            }
            return result;
        }

        private void QueryButtonExecute() {
            var filterItems = this.GetFilterValues(filterContainer);
            var sparePartsCode = Convert.ToString(filterItems["SparePartsCode"]);
            var sparePartsName = Convert.ToString(filterItems["SparePartsName"]);
            var branchId = Convert.ToString(filterItems["BranchId"]);
            var partsSalesCategoryId = Convert.ToString(filterItems["PartsSalesCategoryId"]);
            CompositeFilter.Filters.Clear();
            CompositeFilter.LogicalOperator = LogicalOperator.And;
            if(filterItems.ContainsKey("SparePartsCode"))
                CompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "SparePartsCode",
                    MemberType = typeof(string),
                    Operator = FilterOperator.Contains,
                    Value = sparePartsCode
                });
            if(filterItems.ContainsKey("SparePartsName"))
                CompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "SparePartsName",
                    MemberType = typeof(string),
                    Operator = FilterOperator.Contains,
                    Value = sparePartsName
                });

            if(filterItems.ContainsKey("BranchId"))
                CompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "BranchId",
                    Operator = FilterOperator.IsEqualTo,
                    Value = branchId
                });
            if(filterItems.ContainsKey("PartsSalesCategoryId"))
                CompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "PartsSalesCategoryId",
                    Operator = FilterOperator.IsEqualTo,
                    Value = partsSalesCategoryId
                });
            this.DataGridView.FilterItem = CompositeFilter;
            this.DataGridView.ExecuteQueryDelayed();

        }

        #region 初始化查询条件组件
        private void InitControl() {
            if(initialize)
                return;
            initialize = true;
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(partsSalesCategory);
            }, null);
            domainContext.Load(domainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.KvBranchs.Add(branch);
            }, null);

            var sparePartsCodeLabel = new TextBlock {
                Text = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_SparePartsCode,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(5, 10, 4, 5)
            };
            var sparePartsCodeEdit = new RadMaskedNumericInput {
                Tag = "SparePartsCode",
                Height = 22,
                Width = 120,
                Margin = new Thickness(2, 10, 0, 2),
                UpdateValueEvent = UpdateValueEvent.PropertyChanged
            };
            var sparePartsNameLabel = new TextBlock {
                Text = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_SparePartsName,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(5, 10, 4, 5)
            };

            var sparePartsNameEdit = new RadMaskedNumericInput {
                Tag = "SparePartsName",
                Height = 22,
                Width = 120,
                Margin = new Thickness(2, 10, 0, 2),
                UpdateValueEvent = UpdateValueEvent.PropertyChanged
            };
            var branchIdLabel = new TextBlock {
                Text = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_BranchName,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(5, 10, 4, 5)
            };

            branchIdCombobox = new DcsComboBox {
                Tag = "BranchId",
                Height = 22,
                Width = 120,
                IsEnabled = true,
                Margin = new Thickness(2, 10, 0, 2),
                ItemsSource = this.KvBranchs,
                CanKeyboardNavigationSelectItems = true,
                ClearSelectionButtonVisibility = Visibility.Visible,
                ClearSelectionButtonContent = PartsStockingUIStrings.Action_Title_Remove,
                IsEditable = false,
                IsMouseWheelEnabled = true,
                DisplayMemberPath = "Name",
                SelectedValuePath = "Id"
            };
            var partsSalesCategoryIdLabel = new TextBlock {
                Text = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_PartsSalesCategory,
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Right,
                Margin = new Thickness(5, 10, 4, 5)
            };
            partsSalesCategoryIdCombobox = new DcsComboBox {
                Tag = "PartsSalesCategoryId",
                Height = 22,
                Width = 120,
                IsEnabled = true,
                Margin = new Thickness(2, 10, 0, 2),
                ItemsSource = this.KvPartsSalesCategorys,
                CanKeyboardNavigationSelectItems = true,
                ClearSelectionButtonVisibility = Visibility.Visible,
                ClearSelectionButtonContent = PartsStockingUIStrings.Action_Title_Remove,
                IsEditable = false,
                IsMouseWheelEnabled = true,
                DisplayMemberPath = "Name",
                SelectedValuePath = "Id"
            };

            this.branchIdCombobox.SelectionChanged += branchIdCombobox_SelectionChanged;

            filterContainer = new Grid();
            filterContainer.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(0, GridUnitType.Auto)
            });
            filterContainer.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(0, GridUnitType.Auto)
            });
            filterContainer.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            filterContainer.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            filterContainer.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            filterContainer.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            Grid.SetRow(sparePartsCodeLabel, 0);
            Grid.SetColumn(sparePartsCodeLabel, 0);
            Grid.SetRow(sparePartsCodeEdit, 0);
            Grid.SetColumn(sparePartsCodeEdit, 1);
            Grid.SetRow(sparePartsNameLabel, 1);
            Grid.SetColumn(sparePartsNameLabel, 0);
            Grid.SetRow(sparePartsNameEdit, 1);
            Grid.SetColumn(sparePartsNameEdit, 1);
            Grid.SetRow(branchIdLabel, 0);
            Grid.SetColumn(branchIdLabel, 2);
            Grid.SetRow(branchIdCombobox, 0);
            Grid.SetColumn(branchIdCombobox, 3);
            Grid.SetRow(partsSalesCategoryIdLabel, 1);
            Grid.SetColumn(partsSalesCategoryIdLabel, 2);
            Grid.SetRow(partsSalesCategoryIdCombobox, 1);
            Grid.SetColumn(partsSalesCategoryIdCombobox, 3);

            filterContainer.Children.Add(sparePartsCodeLabel);
            filterContainer.Children.Add(sparePartsCodeEdit);
            filterContainer.Children.Add(sparePartsNameLabel);
            filterContainer.Children.Add(sparePartsNameEdit);
            filterContainer.Children.Add(branchIdLabel);
            filterContainer.Children.Add(branchIdCombobox);
            filterContainer.Children.Add(partsSalesCategoryIdLabel);
            filterContainer.Children.Add(partsSalesCategoryIdCombobox);

            var queryButton = new RadRibbonButton {
                Text = PartsStockingUIStrings.Action_Title_Excut_Query,
                Size = ButtonSize.Large,
                LargeImage = new BitmapImage(new Uri("/Sunlight.Silverlight.Shell;component/Images/query.png", UriKind.Relative)),
                Command = SearchCommand

            };

            var queryGroup = new RadRibbonGroup {
                FontSize = 14,
            };
            queryGroup.Items.Add(filterContainer);
            queryGroup.Items.Add(new Rectangle {
                Width = 2,
                Margin = new Thickness(2),
                HorizontalAlignment = HorizontalAlignment.Center,
                Fill = new LinearGradientBrush {
                    StartPoint = new Point(0.5, 0),
                    EndPoint = new Point(0.5, 1),
                    GradientStops = new GradientStopCollection {
                                new GradientStop {
                                    Offset = 0,
                                    Color = Color.FromArgb(0x10, 0x0, 0x0, 0x0)
                                },
                                new GradientStop {
                                    Offset = 0.25,
                                    Color = Color.FromArgb(0x40, 0x0, 0x0, 0x0)
                                },
                                new GradientStop {
                                    Offset = 0.75,
                                    Color = Color.FromArgb(0x40, 0x0, 0x0, 0x0)
                                },
                                new GradientStop {
                                    Offset = 1,
                                    Color = Color.FromArgb(0x10, 0x0, 0x0, 0x0)
                                },
                            },
                }
            });
            queryGroup.Items.Add(queryButton);
            this.LayoutRoot.Children.Add(queryGroup);
            this.DataGridView.SetValue(Grid.RowProperty, 1);
            this.DataGridView.SetValue(Grid.ColumnSpanProperty, 2);
            this.DataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
            this.LayoutRoot.Children.Add(this.DataGridView);

        }

        private void branchIdCombobox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var comBox = sender as RadComboBox;
            if(branchIdCombobox.SelectedValue == null)
                partsSalesCategoryIdCombobox.ItemsSource = null;
            var branch = (Branch)comBox.SelectedItem;
            if(branch == null) {
                this.KvPartsSalesCategorys.Clear();
                return;
            }
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(p => p.Status == (int)DcsMasterDataStatus.有效 && p.BranchId == branch.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategorys.Clear();
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(partsSalesCategory);
            }, null);
            partsSalesCategoryIdCombobox.ItemsSource = this.KvPartsSalesCategorys;
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.RowDoubleClick != null)
                this.RowDoubleClick.Invoke(sender, e);
        }

        #endregion

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotImplementedException();
        }

        public VirtualCompanySalescenterStockQueryWindow() {
            InitializeComponent();
            this.InitControl();
        }
    }
}

