﻿
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.Custom {
    public partial class VirtualOutboundAndInboundBillCustomView {
        public VirtualOutboundAndInboundBillCustomView() {
            InitializeComponent();
            this.Initialize();
        }

        private DataGridViewBase virtualPartsStockHistoricalStorageDataGridView;
        private DataGridViewBase virtualOutboundAndInboundBillHistoricalDataBaseDataGridView;

        private void Initialize() {
            this.VirtualPartsStockHistoricalStorageDataGridView.SetValue(MinHeightProperty, 80.0);
            this.LayoutRoot.Children.Add(this.VirtualPartsStockHistoricalStorageDataGridView);
            this.VirtualOutboundAndInboundBillHistoricalDataBaseDataGridView.SetValue(MinHeightProperty, 80.0);
            this.VirtualOutboundAndInboundBillHistoricalDataBaseDataGridView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(this.VirtualOutboundAndInboundBillHistoricalDataBaseDataGridView);
        }
        private DcsDomainContext domainContext;
        private DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }
        public DataGridViewBase VirtualPartsStockHistoricalStorageDataGridView {
            get {
                if(this.virtualPartsStockHistoricalStorageDataGridView == null) {
                    this.virtualPartsStockHistoricalStorageDataGridView = DI.GetDataGridView("VirtualPartsStockHistoricalStorage");
                    this.virtualPartsStockHistoricalStorageDataGridView.DomainContext = this.DomainContext;
                }
                return this.virtualPartsStockHistoricalStorageDataGridView;
            }
        }


        public DataGridViewBase VirtualOutboundAndInboundBillHistoricalDataBaseDataGridView {
            get {
                if(this.virtualOutboundAndInboundBillHistoricalDataBaseDataGridView == null) {
                    this.virtualOutboundAndInboundBillHistoricalDataBaseDataGridView = DI.GetDataGridView("VirtualOutboundAndInboundBillHistoricalDataBase");
                    this.virtualPartsStockHistoricalStorageDataGridView.DomainContext = this.DomainContext;
                }
                return this.virtualOutboundAndInboundBillHistoricalDataBaseDataGridView;
            }
        }


        public void ExecuteQuery(FilterItem filterItem) {
            this.VirtualPartsStockHistoricalStorageDataGridView.FilterItem = filterItem;
            this.VirtualOutboundAndInboundBillHistoricalDataBaseDataGridView.FilterItem = filterItem;
            this.VirtualPartsStockHistoricalStorageDataGridView.ExecuteQuery();
            this.VirtualOutboundAndInboundBillHistoricalDataBaseDataGridView.ExecuteQuery();
        }


        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            return null;
        }
    }
}
