﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.TransitionEffects;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.Custom {
    public partial class WarehouseAreaView : IBaseView, INotifyPropertyChanged {
        private WarehouseArea currentSelectedItem;
        private DcsDomainContext domainContext;
        private WarehouseAreaDataTreeView warehouseAreaTreeView;
        private RadTransitionControl radTransitionControl;
        private Grid grid;

        public Grid Grid {
            get {
                return this.grid ?? (this.grid = new Grid());
            }
        }

        public RadTransitionControl RadTransitionControl {
            get {
                if(this.radTransitionControl == null)
                    this.radTransitionControl = new RadTransitionControl {
                        Duration = TimeSpan.FromMilliseconds(500),
                        Transition = new PixelateTransition {
                            IsPixelLED = false,
                        },
                    };
                return this.radTransitionControl;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public WarehouseAreaView() {
            InitializeComponent();
            this.CreateUI();
        }

        private void CreateUI() {
            this.Grid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            this.Grid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(0, GridUnitType.Auto)
            });
            this.Grid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(100, GridUnitType.Star)
            });
            var r = new Rectangle();
            r.SetValue(Grid.ColumnProperty, 1);
            this.Grid.Children.Add(r);
            this.WarehouseAreaDataTreeView.SetValue(Grid.RowSpanProperty, 2);
            this.Root.Children.Add(this.WarehouseAreaDataTreeView);
            this.RadTransitionControl.SetValue(Grid.ColumnProperty, 2);
            this.RadTransitionControl.SetValue(Grid.ColumnSpanProperty, 2);
            this.RadTransitionControl.SetValue(Grid.RowSpanProperty, 2);
            this.Root.Children.Add(this.RadTransitionControl);
        }

        private void WarehouseAreaDataTreeView_OnTreeViewItemClick(object sender, RadRoutedEventArgs radRoutedEventArgs) {
            var selectedItem = ((RadTreeView)sender).SelectedItem as RadTreeViewItem;
            if(selectedItem == null || !(selectedItem.Tag is WarehouseArea))
                return;
            var warehouseArea = selectedItem.Tag as WarehouseArea;
            this.CurrentSelectedItem = warehouseArea;
            var WarehouseAreaDetailPanel = this.Grid.ChildrenOfType<DcsDetailPanelBase>().First();
            if(WarehouseAreaDetailPanel == null)
                return;
            WarehouseAreaDetailPanel.SetValue(DataContextProperty, this.CurrentSelectedItem);
        }

        public object GetObjectById(int ParintId) {
            return this.WarehouseAreaDataTreeView.GetObjectById(ParintId);
        }

        public RadTreeViewItem SelectItem {
            get {
                return this.WarehouseAreaDataTreeView.SelectedItem as RadTreeViewItem;
            }
        }

        public bool TreeIsEnabled {
            set {
                this.WarehouseAreaDataTreeView.SetValue(IsEnabledProperty, value);
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void WarehouseAreaDataTreeView_OnTreeViewDataLoaded() {
            var firstNode = this.WarehouseAreaDataTreeView.GetFirstTreeViewItem();
            if(firstNode != null && firstNode.Tag is WarehouseArea) {
                firstNode.IsSelected = true;
                this.CurrentSelectedItem = (firstNode.Tag as WarehouseArea);
                var WarehouseAreaDetailPanel = this.Grid.ChildrenOfType<DcsDetailPanelBase>().First();
                if(WarehouseAreaDetailPanel == null)
                    return;
                WarehouseAreaDetailPanel.SetValue(DataContextProperty, this.CurrentSelectedItem);
            }
        }


        private WarehouseAreaDataTreeView WarehouseAreaDataTreeView {
            get {
                if(this.warehouseAreaTreeView == null) {
                    this.warehouseAreaTreeView = new WarehouseAreaDataTreeView();
                    this.warehouseAreaTreeView.IsExpanderFirstNode = false;
                    this.warehouseAreaTreeView.DomainContext = this.DomainContext;
                    this.warehouseAreaTreeView.EntityQuery = this.warehouseAreaTreeView.DomainContext.GetWarehouseAreasWithRootQuery();
                    this.warehouseAreaTreeView.OnTreeViewDataLoaded += this.WarehouseAreaDataTreeView_OnTreeViewDataLoaded;
                    this.warehouseAreaTreeView.OnTreeViewItemClick += this.WarehouseAreaDataTreeView_OnTreeViewItemClick;
                    this.warehouseAreaTreeView.OnTreeViewItemExpanded += warehouseAreaTreeView_OnTreeViewItemExpanded;
                    this.warehouseAreaTreeView.RefreshDataTree();
                }
                return this.warehouseAreaTreeView;
            }
        }
        //树展开事件
        public void warehouseAreaTreeView_OnTreeViewItemExpanded(object sender, RadRoutedEventArgs e) {
            var item = e.OriginalSource as RadTreeViewItem;
            if(isLoadTreeNodeItems(item))
                return;
            if(item == null)
                return;
            var region = item.Tag as WarehouseArea;
            if(region == null)
                return;
            this.DomainContext.Load(DomainContext.GetWarehouseAreasByParentIdQuery(region.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                item.Items.Clear();
                foreach(var nextNode in loadOp.Entities) {
                    var radTreeView = new RadTreeViewItem {
                        Header = nextNode.Code,
                        Tag = nextNode
                    };
                    radTreeView.Items.Add(new RadTreeViewItem());
                    item.Items.Add(radTreeView);
                }
            }, null);
        }

        /// <summary>
        /// 是否需要加载树节点
        /// </summary>
        /// <param name="tvItem">当前展开的节点</param>
        /// <returns></returns>
        private bool isLoadTreeNodeItems(RadTreeViewItem tvItem) {
            if(tvItem.Items.Count > 0) {
                var tv = tvItem.Items.First() as RadTreeViewItem;
                if(tv.Header == null) {
                    tvItem.Items.Clear();
                    return false;
                }
                return true;
            } else {
                return true;
            }
        }

        public ObservableCollection<DcsMenuItem> RadMenuItems {
            get {
                return this.WarehouseAreaDataTreeView.RadMenuItems;
            }
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            return null;
        }

        public void CancelTreeItem(int id) {
            this.WarehouseAreaDataTreeView.CancelTreeItem(id);
            this.CurrentSelectedItem = null;
        }

        public DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        public WarehouseArea CurrentSelectedItem {
            get {
                return this.currentSelectedItem;
            }
            set {
                this.currentSelectedItem = value;
                this.OnPropertyChanged("CurrentSelectedItem");
            }
        }

        public void RefreshTreeView() {
            this.WarehouseAreaDataTreeView.RefreshDataTree();
        }
    }
}
