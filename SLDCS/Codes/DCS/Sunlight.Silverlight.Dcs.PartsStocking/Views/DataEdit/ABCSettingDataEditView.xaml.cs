﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class ABCSettingDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private readonly ObservableCollection<KeyValuePair> kvABCTypes = new ObservableCollection<KeyValuePair>();
        public event PropertyChangedEventHandler PropertyChanged;
        private int? type;
        private readonly string[] kvNames = {
           "ABCSetting_Type"
        };
        protected override string BusinessName {
            get {
                return PartsStockingUIStrings.DataGridView_Title_ABCSetting_Name;
            }
        }
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public ABCSettingDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void CreateUI() {
            this.KeyValueManager.LoadData(() => {
                this.kvABCTypes.Clear();
                foreach(var kvPair in this.KeyValueManager[kvNames[0]]) {
                    kvABCTypes.Add(kvPair);
                }
            });
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetABCSettingsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvABCTypes {
            get {
                return this.kvABCTypes;
            }
        }
        public int? Type {
            get {
                return type;
            }
            set {
                this.type = value;
                this.OnPropertyChanged("Type");
            }
        }
      
        protected override void OnEditSubmitting() {
            var aBCSetting = this.DataContext as ABCSetting;
            if(aBCSetting == null)
                return;
            aBCSetting.ValidationErrors.Clear();

            if(!aBCSetting.Type.HasValue) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_Type);
                return;
            }
            if((aBCSetting.SaleFrequencyFrom.HasValue && !aBCSetting.SaleFrequencyTo.HasValue) || (aBCSetting.SaleFrequencyFrom.HasValue && !aBCSetting.SaleFrequencyTo.HasValue)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation1);
                return;
            }
            if(aBCSetting.SaleFrequencyFrom.HasValue && aBCSetting.SaleFrequencyTo.HasValue && (aBCSetting.SaleFrequencyFrom >= aBCSetting.SaleFrequencyTo || aBCSetting.SaleFrequencyFrom < 0 || aBCSetting.SaleFrequencyTo > 100 )) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation2);
                return;
            }
            if((aBCSetting.SaleFeeFrom.HasValue && !aBCSetting.SaleFeeTo.HasValue) || (aBCSetting.SaleFeeFrom.HasValue && !aBCSetting.SaleFeeTo.HasValue)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation3);
                return;
            }
            if(aBCSetting.SaleFeeFrom.HasValue && aBCSetting.SaleFeeTo.HasValue && (aBCSetting.SaleFeeFrom >= aBCSetting.SaleFeeTo || aBCSetting.SaleFeeFrom < 0 || aBCSetting.SaleFeeTo > 100)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation4);
                return;
            }
            if(aBCSetting.NoSaleMonth.HasValue && aBCSetting.NoSaleMonth < 0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation5);
                return;
            }
            if(aBCSetting.SafeStockCoefficient.HasValue && aBCSetting.SafeStockCoefficient < 0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEdit_Error_ABCSetting_Validation6);
                return;
            }
            ((IEditableObject)aBCSetting).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(aBCSetting.Can新增配件ABC分类设置) {
                        aBCSetting.新增配件ABC分类设置();
                    }
                } else {
                    if(aBCSetting.Can更新配件ABC分类设置) {
                        aBCSetting.更新配件ABC分类设置();
                    }
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
        }
        protected override void OnEditCancelled() {
            base.OnEditCancelled();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
