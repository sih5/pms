﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class ABCSettingDetailDataEditView {
        private KeyValueManager keyValueManager;
        private readonly ObservableCollection<KeyValuePair> kvABCTypes = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
           "ABCSetting_Type"
        };
        public ABCSettingDetailDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }
        protected override string BusinessName {
            get {
                return PartsStockingUIStrings.DataGridView_Title_ABCSetting_Name;
            }
        }
       
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvWarehouseAreaCategorys {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        private void CreateUI() {
            this.KeyValueManager.LoadData(() => {
                this.kvABCTypes.Clear();
                foreach(var kvPair in this.KeyValueManager[kvNames[0]]) {
                    kvABCTypes.Add(kvPair);
                }
            });
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        public object KvABCTypes {
            get {
                return this.kvABCTypes;
            }
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetABCSettingsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }
    }

}
