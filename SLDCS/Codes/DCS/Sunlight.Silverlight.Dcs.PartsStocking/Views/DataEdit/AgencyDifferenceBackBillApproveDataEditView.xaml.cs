﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using System.Collections.Generic;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;
using Sunlight.Silverlight.Core.Command;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgencyDifferenceBackBillApproveDataEditView: INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private DcsDomainContext domainContext = new DcsDomainContext();
        private readonly ObservableCollection<KeyValuePair> kvResTypes = new ObservableCollection<KeyValuePair>();
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvResTems; 
        private readonly string[] kvNames = {
            "ResponsibleMembersResTem"
        };
        public ObservableCollection<KeyValuePair> KvResTypes {
            get {
                return this.kvResTypes;
            }
        }
        public ObservableCollection<KeyValuePair> KvResTems {
            get {
                return this.kvResTems ?? (kvResTems = new ObservableCollection<KeyValuePair>());
            }
        } 
        private void AgencyDifferenceBackBillDetermineDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetResRelationshipsQuery().Where(r =>  r.Status == (int)DcsBaseDataStatus.有效 ), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                foreach(var warehouse in loadOp.Entities) {
                    this.kvResTypes.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.ResType,
                        UserObject = warehouse
                    });
                }
                if(this.KvResTypes.Count() > 0) {
                    this.CbShippingMethod.ItemsSource = this.KvResTypes;
                }
            }, null);
        }
        public AgencyDifferenceBackBillApproveDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.Loaded += AgencyDifferenceBackBillDetermineDataEditView_Loaded;
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAgencyDifferenceBackBillsQuery().Where(t=>t.Id==id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    this.DataEditPanels.FilePath = entity.Path;
                    this.TxtOrderPrice.Text = entity.OrderPrice.Value.ToString("C");
                    this.TxtDifferSum.Text = entity.DifferSum.Value.ToString("C");
                }
            }, null);
        }

        private void CreateUI() {
            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvResTems.Add(keyValuePair);
                }              
            });
            this.CbResTem.ItemsSource = KvResTems;
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = "驳回",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            }, true); 
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 6);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 30, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 4);
            this.DataEditPanels.isHiddenButtons = true;
            this.Root.Children.Add(DataEditPanels);
        }

       
        private FileUploadAgencyDifferenceBackBillDataEditPanel productDataEditPanels;

        public FileUploadAgencyDifferenceBackBillDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadAgencyDifferenceBackBillDataEditPanel)DI.GetDataEditPanel("FileUploadAgencyDifferenceBackBill"));
            }
        }


        protected override string Title {
            get {
                return "审核中心库收货差异处理";
            }
        }
        protected override void OnEditSubmitting() {
            var agencyDifferenceBackBill = this.DataContext as AgencyDifferenceBackBill;
            if(agencyDifferenceBackBill == null)
                return;
            agencyDifferenceBackBill.RejectMemo = null;
            agencyDifferenceBackBill.ValidationErrors.Clear();
            ((IEditableObject)agencyDifferenceBackBill).EndEdit();
            try {
                if(agencyDifferenceBackBill.Can审核中心库收货差异处理单)
                    agencyDifferenceBackBill.审核中心库收货差异处理单();
                ExecuteSerivcesMethod("审核成功");
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            } 
        }
        private void RejectCurrentData() {
            var agencyDifferenceBackBill = this.DataContext as AgencyDifferenceBackBill;
            if(agencyDifferenceBackBill == null)
                return;
            if(string.IsNullOrEmpty(this.TApproveMent.Text)) {
                UIHelper.ShowNotification("请填写驳回原因");
                return;
            }
            agencyDifferenceBackBill.RejectMemo = this.TApproveMent.Text;

            ((IEditableObject)agencyDifferenceBackBill).EndEdit();
            try {
                if(agencyDifferenceBackBill.Can驳回中心库收货差异处理单)
                    agencyDifferenceBackBill.驳回中心库收货差异处理单();
                ExecuteSerivcesMethod("驳回生成");
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        } 
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        } 
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.kvResTypes.Clear();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.kvResTypes.Clear();
        }



    }
}