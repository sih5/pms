﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Telerik.Windows.Controls;
using System.Collections.Generic;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgencyDifferenceBackBillDataEditView: INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private DcsDomainContext domainContext = new DcsDomainContext();

        public AgencyDifferenceBackBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAgencyDifferenceBackBillsQuery().Where(t=>t.Id==id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {

                    this.DomainContext.Load(this.DomainContext.查询可差异退货配件Query(entity.PartsInboundPlanCode, entity.InWarehouseId, entity.PartsShippingCode, null, entity.SparePartCode,null,null,null), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            if(!loadOp1.IsErrorHandled)
                                loadOp1.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                            return;
                        }
                        var entity1 = loadOp1.Entities.SingleOrDefault();
                        if(entity1 != null) {
                            entity.CanDIffer = entity1.CanDIffer + entity.DifferQty.Value;
                        }
                        else
                        {
                            entity.CanDIffer = entity.DifferQty.Value;
                        }
                        this.SetObjectToEdit(entity);
                        this.DataEditPanels.FilePath = entity.Path;  
                    }, null);
                   
                }
            }, null);
        }

        private void CreateUI() {
            var queryWindowExpress = DI.GetQueryWindow("AgencyDifferenceBackBill");
            queryWindowExpress.SelectionDecided += this.sparePartCodes_SelectionDecided;
            queryWindowExpress.Loaded += this.settlementCostSearchQueryWindow_Loaded;
            this.PopWarehouseArea.PopupContent = queryWindowExpress;
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 30, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 4);
            this.Root.Children.Add(DataEditPanels);
        }
        private FileUploadAgencyDifferenceBackBillDataEditPanel productDataEditPanels;

        public FileUploadAgencyDifferenceBackBillDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadAgencyDifferenceBackBillDataEditPanel)DI.GetDataEditPanel("FileUploadAgencyDifferenceBackBill"));
            }
        }
        private void settlementCostSearchQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null)
                return;          
        }
        private void sparePartCodes_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var agencyDifferenceBackBill = this.DataContext as AgencyDifferenceBackBill;
            if(agencyDifferenceBackBill == null)
                return;
            var spareParts = queryWindow.SelectedEntities.Cast<AgencyDifferenceBackBillForSparepart>().FirstOrDefault();
            if(spareParts==null)
                return;
            agencyDifferenceBackBill.SparePartId = spareParts.SparePartId;
            agencyDifferenceBackBill.SparePartCode = spareParts.SparePartCode;
            agencyDifferenceBackBill.SparePartName = spareParts.SparePartName;
            agencyDifferenceBackBill.MeasureUnit = spareParts.MeasureUnit;
            agencyDifferenceBackBill.PartsInboundPlanId = spareParts.PartsInboundPlanId;
            agencyDifferenceBackBill.PartsInboundPlanCode = spareParts.PartsInboundPlanCode;
            agencyDifferenceBackBill.InWarehouseId = spareParts.InWarehouseId;
            agencyDifferenceBackBill.InWarehouseCode = spareParts.InWarehouseCode;
            agencyDifferenceBackBill.InWarehouseName = spareParts.InWarehouseName;
            agencyDifferenceBackBill.PartsShippingCode = spareParts.PartsShippingCode;
            agencyDifferenceBackBill.PartsShippingId = spareParts.PartsShippingId;
            agencyDifferenceBackBill.PartsSalesOrderId = spareParts.PartsSalesOrderId;
            agencyDifferenceBackBill.PartsSalesOrderCode = spareParts.PartsSalesOrderCode;
            agencyDifferenceBackBill.InboundQty = spareParts.InboundQty;
            agencyDifferenceBackBill.CanDIffer = spareParts.CanDIffer;
            agencyDifferenceBackBill.OrderPrice = spareParts.OrderPrice;
            agencyDifferenceBackBill.DifferQty = 0;
            agencyDifferenceBackBill.DifferSum = 0;
            agencyDifferenceBackBill.ActQty = 0;
            agencyDifferenceBackBill.StorageCompanyId = spareParts.StorageCompanyId;
            agencyDifferenceBackBill.StorageCompanyCode = spareParts.StorageCompanyCode;
            agencyDifferenceBackBill.StorageCompanyName = spareParts.StorageCompanyName;
            this.TxtOrderPrice.Text = agencyDifferenceBackBill.OrderPrice.Value.ToString("C");
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.Close();
            }


        }
       
        protected override string BusinessName {
            get {
                return "中心库收货差异处理";
            }
        }
        protected override void OnEditSubmitting() {
            var agencyDifferenceBackBill = this.DataContext as AgencyDifferenceBackBill;
            if(agencyDifferenceBackBill == null)
                return;
            if(!agencyDifferenceBackBill.ActQty.HasValue) {
                UIHelper.ShowNotification("请填写实收量");
                return;
            }
            if(string.IsNullOrEmpty( agencyDifferenceBackBill.DIfferReason)) {
                UIHelper.ShowNotification("请填写差异原因");
                return;
            }
            if (agencyDifferenceBackBill.ActQty < 0 || agencyDifferenceBackBill.ActQty >= agencyDifferenceBackBill.CanDIffer)
            {
                UIHelper.ShowNotification("实收量必须大于等于0，且小于可差异处理量");
                return;
            }
            agencyDifferenceBackBill.ValidationErrors.Clear();
            agencyDifferenceBackBill.Path = DataEditPanels.FilePath;
            ((IEditableObject)agencyDifferenceBackBill).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(agencyDifferenceBackBill.Can新建中心库收货差异处理单)
                        agencyDifferenceBackBill.新建中心库收货差异处理单();
                } else {
                    if(agencyDifferenceBackBill.Can修改中心库收货差异处理单)
                        agencyDifferenceBackBill.修改中心库收货差异处理单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            } 
            base.OnEditSubmitting(); 
        }
 

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e) {
            if(string.IsNullOrEmpty(this.TxActQty.Text) )
                return;
            var agencyDifferenceBackBill = this.DataContext as AgencyDifferenceBackBill;
            if(agencyDifferenceBackBill == null)
                return;
            if(Convert.ToInt32(this.TxActQty.Text) < 0 || Convert.ToInt32(this.TxActQty.Text) >= agencyDifferenceBackBill.CanDIffer) {
                UIHelper.ShowNotification("实收量必须大于等于0，且小于可差异处理量");
                return;
            }
            agencyDifferenceBackBill.ActQty = Convert.ToInt32(this.TxActQty.Text);
            agencyDifferenceBackBill.DifferQty = agencyDifferenceBackBill.CanDIffer - agencyDifferenceBackBill.ActQty;
            agencyDifferenceBackBill.DifferSum = (agencyDifferenceBackBill.CanDIffer - agencyDifferenceBackBill.ActQty) * agencyDifferenceBackBill.OrderPrice;
            this.TxtDifferSum.Text = agencyDifferenceBackBill.DifferSum.Value.ToString("C");
        }

    }
}
