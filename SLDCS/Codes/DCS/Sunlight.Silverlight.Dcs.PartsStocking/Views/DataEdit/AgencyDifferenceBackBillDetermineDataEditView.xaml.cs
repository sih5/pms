﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Telerik.Windows.Controls;
using System.Collections.Generic;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgencyDifferenceBackBillDetermineDataEditView: INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private DcsDomainContext domainContext = new DcsDomainContext();
        private readonly ObservableCollection<KeyValuePair> kvResTypes = new ObservableCollection<KeyValuePair>();
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvResTems; 
        private readonly string[] kvNames = {
            "ResponsibleMembersResTem"
        };
        public ObservableCollection<KeyValuePair> KvResTypes {
            get {
                return this.kvResTypes;
            }
        }
        public ObservableCollection<KeyValuePair> KvResTems {
            get {
                return this.kvResTems ?? (kvResTems = new ObservableCollection<KeyValuePair>());
            }
        } 
        private void AgencyDifferenceBackBillDetermineDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetResRelationshipsQuery().Where(r =>  r.Status == (int)DcsBaseDataStatus.有效 ), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                this.KvResTypes.Clear();
                foreach(var warehouse in loadOp.Entities) {
                    this.KvResTypes.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.ResType,
                        UserObject = warehouse
                    });
                }
                if(this.KvResTypes.Count()>0){
                    this.CbShippingMethod.ItemsSource = this.KvResTypes;
                }
            }, null);
        }
        public AgencyDifferenceBackBillDetermineDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.Loaded += AgencyDifferenceBackBillDetermineDataEditView_Loaded;
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAgencyDifferenceBackBillsQuery().Where(t=>t.Id==id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    this.DataEditPanels.FilePath = entity.Path;
                    this.TxtOrderPrice.Text = entity.OrderPrice.Value.ToString("C");
                    this.TxtDifferSum.Text = entity.DifferSum.Value.ToString("C");
                }
            }, null);
        }

        private void CreateUI() {
            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvResTems.Add(keyValuePair);
                }              
            });
            this.CbResTem.ItemsSource = KvResTems;
            //附件
            this.DataEditPanels.SetValue(Grid.RowProperty, 5);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 0);
            this.DataEditPanels.Margin = new Thickness(0, 30, 0, 0);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Left;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Top;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 4);
            this.DataEditPanels.isHiddenButtons = true;
            this.Root.Children.Add(DataEditPanels);
        }
        private FileUploadAgencyDifferenceBackBillDataEditPanel productDataEditPanels;

        public FileUploadAgencyDifferenceBackBillDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadAgencyDifferenceBackBillDataEditPanel)DI.GetDataEditPanel("FileUploadAgencyDifferenceBackBill"));
            }
        }


        protected override string Title {
            get {
                return "判定中心库收货差异处理";
            }
        }
        protected override void OnEditSubmitting() {
            var agencyDifferenceBackBill = this.DataContext as AgencyDifferenceBackBill;
            if(agencyDifferenceBackBill == null)
                return;
            if(!agencyDifferenceBackBill.ResRelationshipId.HasValue) {
                UIHelper.ShowNotification("请选择责任类型");
                return;
            }          
            agencyDifferenceBackBill.ValidationErrors.Clear();
            ((IEditableObject)agencyDifferenceBackBill).EndEdit();
            try {
                if(agencyDifferenceBackBill.Can判定中心库收货差异处理单)
                    agencyDifferenceBackBill.判定中心库收货差异处理单();
                ExecuteSerivcesMethod("判定成功");
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            } 
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        } 
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.kvResTypes.Clear();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.kvResTypes.Clear();
        }

        private void CbShippingMethod_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var borrowBill = this.DataContext as AgencyDifferenceBackBill;
            if(borrowBill == null)
                return;
            var borrowBillInfo = comboBox.SelectedItem as KeyValuePair;
            if(borrowBillInfo == null)
                return;
            borrowBill.ResRelationshipId = ((ResRelationship)borrowBillInfo.UserObject).Id;
            borrowBill.ResType = ((ResRelationship)borrowBillInfo.UserObject).ResType;
            borrowBill.ResTem = ((ResRelationship)borrowBillInfo.UserObject).ResTem;

        }


    }
}