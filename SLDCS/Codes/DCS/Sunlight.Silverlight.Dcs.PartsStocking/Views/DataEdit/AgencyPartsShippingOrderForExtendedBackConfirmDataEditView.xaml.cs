﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgencyPartsShippingOrderForExtendedBackConfirmDataEditView {
        public AgencyPartsShippingOrderForExtendedBackConfirmDataEditView() {
            InitializeComponent();
        }


        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsShippingOrderForExtendedBackConfirm;
            }
        }

        protected override void OnEditSubmitting() {
            var agencyPartsShippingOrder = this.DataContext as AgencyPartsShippingOrder;
            if(agencyPartsShippingOrder == null)
                return;

            if(!agencyPartsShippingOrder.RequestedArrivalDate.HasValue) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_RequestedArrivalDateIsNull);
                return;
            }

            ((IEditableObject)agencyPartsShippingOrder).EndEdit();
            if(agencyPartsShippingOrder.Can超期或电商运单流水反馈代理库)
                agencyPartsShippingOrder.超期或电商运单流水反馈代理库();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAgencyPartsShippingOrdersQuery().Where(e => e.Id.Equals(id)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    entity.FlowFeedback = "";
                    if(!entity.RequestedArrivalDate.HasValue)
                        entity.RequestedArrivalDate = DateTime.Now;
                    if(!entity.ExpectedPlaceDate.HasValue)
                        entity.ExpectedPlaceDate = DateTime.Now;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }


    }
}
