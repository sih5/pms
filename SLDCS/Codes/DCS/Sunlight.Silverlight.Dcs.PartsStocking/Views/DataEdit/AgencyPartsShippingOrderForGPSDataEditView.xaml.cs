﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgencyPartsShippingOrderForGPSDataEditView {
        public AgencyPartsShippingOrderForGPSDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_TitlePartsShippingOrderForGPS;
            }

        }
        protected override void OnEditSubmitting() {
            var agencyPartsShippingOrder = this.DataContext as AgencyPartsShippingOrder;
            if(agencyPartsShippingOrder == null)
                return;
            if(string.IsNullOrEmpty(agencyPartsShippingOrder.GPSCode)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_GPSCodeIsNull);
                return;
            }
            ((IEditableObject)agencyPartsShippingOrder).EndEdit();
            if(agencyPartsShippingOrder.CanGPS定位器设备号登记代理库)
                agencyPartsShippingOrder.GPS定位器设备号登记代理库();

            base.OnEditSubmitting();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAgencyPartsShippingOrdersQuery().Where(e => e.Id.Equals(id)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    //entity.RequestedArrivalDate = DateTime.Now;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        private void CreateUI() {

        }
    }
}
