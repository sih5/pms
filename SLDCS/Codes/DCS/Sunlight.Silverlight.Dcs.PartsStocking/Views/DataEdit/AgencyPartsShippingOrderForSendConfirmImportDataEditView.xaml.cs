﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgencyPartsShippingOrderForSendConfirmImportDataEditView {
        private DataGridViewBase partsshippingorderforsendconfirmimportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出批量送达确认模板.xlsx";
        private ICommand exportFileCommand;
        public AgencyPartsShippingOrderForSendConfirmImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return "导入批量送达确认";
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                return this.partsshippingorderforsendconfirmimportDataGridView ?? (this.partsshippingorderforsendconfirmimportDataGridView = DI.GetDataGridView("PartsShippingOrderForSendConfirmImport"));
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = PartsStockingUIStrings.DataEditView_Title_BatchConfirmDetails,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.批量送达确认代理库Async(fileName);
            this.ExcelServiceClient.批量送达确认代理库Completed -= ExcelServiceClient_批量送达确认代理库Completed;
            this.ExcelServiceClient.批量送达确认代理库Completed += ExcelServiceClient_批量送达确认代理库Completed;
        }

        private void ExcelServiceClient_批量送达确认代理库Completed(object sender, 批量送达确认代理库CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_Code,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_ArrivalTime,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_CargoTerminalMsg
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                }, null);
            });
        }
    }
}
