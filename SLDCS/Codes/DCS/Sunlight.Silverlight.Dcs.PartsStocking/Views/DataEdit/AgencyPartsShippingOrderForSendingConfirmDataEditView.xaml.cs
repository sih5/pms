﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgencyPartsShippingOrderForSendingConfirmDataEditView {
        private KeyValueManager keyValueManager;

        private readonly string[] kvNames = {
            "ArrivalMode"
        };

        public AgencyPartsShippingOrderForSendingConfirmDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public ObservableCollection<KeyValuePair> KvArrivalMode {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsShippingOrderForSendingConfirm;
            }
        }

        protected override void OnEditSubmitting() {
            var agencyPartsShippingOrder = this.DataContext as AgencyPartsShippingOrder;
            if(agencyPartsShippingOrder == null)
                return;

            if(!agencyPartsShippingOrder.ShippingTime.HasValue) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ShippingTimeIsNull);
                return;
            } else {
                if(agencyPartsShippingOrder.CreateTime.HasValue && agencyPartsShippingOrder.ShippingTime.Value.Date < agencyPartsShippingOrder.CreateTime.Value.Date) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ShippingTimeError);
                    return;
                }
            }

            if(!agencyPartsShippingOrder.ArrivalMode.HasValue) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ArrivalModeIsNull);
                return;
            }

            ((IEditableObject)agencyPartsShippingOrder).EndEdit();
            if(agencyPartsShippingOrder.Can发货确认代理库)
                agencyPartsShippingOrder.发货确认代理库();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAgencyPartsShippingOrdersQuery().Where(e => e.Id.Equals(id)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    entity.ShippingTime = DateTime.Now;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            var queryWindowExpress = DI.GetQueryWindow("Express");
            queryWindowExpress.SelectionDecided += this.QueryWindowExpress_SelectionDecided;
            queryWindowExpress.Loaded += this.QueryWindowExpress_Loaded;
            this.ptExpress.PopupContent = queryWindowExpress;

        }

        private void QueryWindowExpress_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            var filterItem = new FilterItem();
            filterItem.MemberName = "Status";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = (int)DcsMasterDataStatus.有效;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
        }

        private void QueryWindowExpress_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var express = queryWindow.SelectedEntities.Cast<Express>().FirstOrDefault();
            if(express == null)
                return;
            var agencyPartsShippingOrder = this.DataContext as AgencyPartsShippingOrder;
            if(agencyPartsShippingOrder == null)
                return;
            agencyPartsShippingOrder.ExpressCompanyId = express.Id;
            agencyPartsShippingOrder.ExpressCompanyCode = express.ExpressCode;
            agencyPartsShippingOrder.ExpressCompany = express.ExpressName;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

    }
}