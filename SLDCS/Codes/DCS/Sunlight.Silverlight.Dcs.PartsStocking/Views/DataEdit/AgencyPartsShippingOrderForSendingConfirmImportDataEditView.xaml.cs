﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgencyPartsShippingOrderForSendingConfirmImportDataEditView {
        private DataGridViewBase partsshippingorderforsendingconfirmimportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出批量发货确认代理库模板.xlsx";
        private ICommand exportFileCommand;
        public AgencyPartsShippingOrderForSendingConfirmImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return "导入批量发货确认代理库";
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                return this.partsshippingorderforsendingconfirmimportDataGridView ?? (this.partsshippingorderforsendingconfirmimportDataGridView = DI.GetDataGridView("PartsShippingOrderForSendingConfirmImport"));
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = "批量发货确认导入清单",
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.批量发货确认代理库Async(fileName);
            this.ExcelServiceClient.批量发货确认代理库Completed -= ExcelServiceClient_批量发货确认代理库Completed;
            this.ExcelServiceClient.批量发货确认代理库Completed += ExcelServiceClient_批量发货确认代理库Completed;
        }

        private void ExcelServiceClient_批量发货确认代理库Completed(object sender, 批量发货确认代理库CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = "发运单编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "发货日期",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "承运商"
                                            },
                                            new ImportTemplateColumn {
                                                Name = "快递单号"
                                            },
                                            new ImportTemplateColumn {
                                                Name = "到货模式",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "查询网址"
                                            },
                                            new ImportTemplateColumn {
                                                Name = "快递公司名称"
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}