﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgentsLogisticCompanyDataEditView {
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private ObservableCollection<KeyValuePair> kvBranchs;
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = new[] {
            "Area_Category"
        };
        public AgentsLogisticCompanyDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        public ObservableCollection<KeyValuePair> KvBranchs {
            get {
                if(this.kvBranchs == null)
                    this.kvBranchs = new ObservableCollection<KeyValuePair>();
                return this.kvBranchs;
            }
        }
        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                if(this.kvWarehouses == null)
                    this.kvWarehouses = new ObservableCollection<KeyValuePair>();
                return this.kvWarehouses;
            }
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvWarehouseAreaCategorys {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }


        private void CreateUI() {
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var agencyLogisticCompany = this.DataContext as AgencyLogisticCompany;
            if(agencyLogisticCompany == null)
                return;
            if(string.IsNullOrEmpty(agencyLogisticCompany.Code)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_LogisticCompanyCodeIsNull);
                return;
            }
            if(string.IsNullOrEmpty(agencyLogisticCompany.Name)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_LogisticCompanyNameIsNull);
                return;
            }
            ((IEditableObject)agencyLogisticCompany).EndEdit();
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAgencyLogisticCompanyByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
    }

}
