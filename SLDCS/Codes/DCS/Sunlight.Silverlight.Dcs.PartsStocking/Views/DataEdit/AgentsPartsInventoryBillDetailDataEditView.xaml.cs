﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgentsPartsInventoryBillDetailDataEditView {
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private ObservableCollection<KeyValuePair> kvBranchs;
        private KeyValueManager keyValueManager;
        private DataGridViewBase partsInventoryBillForApproveDataGridView;
        private readonly string[] kvNames = new[] {
            "Area_Category"
        };
        public AgentsPartsInventoryBillDetailDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }
        private AgentsPartsInventoryBillForUploadDataEditPanel fileUploadDataEditPanels;
        public AgentsPartsInventoryBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (AgentsPartsInventoryBillForUploadDataEditPanel)DI.GetDataEditPanel("AgentsPartsInventoryBillForUpload"));
            }
        }
        public ObservableCollection<KeyValuePair> KvBranchs {
            get {
                if(this.kvBranchs == null)
                    this.kvBranchs = new ObservableCollection<KeyValuePair>();
                return this.kvBranchs;
            }
        }
        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                if(this.kvWarehouses == null)
                    this.kvWarehouses = new ObservableCollection<KeyValuePair>();
                return this.kvWarehouses;
            }
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvWarehouseAreaCategorys {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        public DataGridViewBase PartsInventoryBillForApproveDataGridView {
            get {
                if(this.partsInventoryBillForApproveDataGridView == null) {
                    this.partsInventoryBillForApproveDataGridView = DI.GetDataGridView("PartsInventoryBillForEditApprove");
                    this.partsInventoryBillForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsInventoryBillForApproveDataGridView;
            }
        }

        private void CreateUI() {
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvWarehouses.Clear();
                foreach(var warehouse in loadOp.Entities)
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
            }, null);
            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranchs.Clear();
                foreach(var branchs in loadOp.Entities)
                    this.KvBranchs.Add(new KeyValuePair {
                        Key = branchs.Id,
                        Value = branchs.Name,
                        UserObject = branchs
                    });
            }, null);

            FileUploadDataEditPanels.isHiddenButtons = true;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 10);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);

            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsInventoryBill), "PartsInventoryDetails"), null, () => this.PartsInventoryBillForApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 4);
            this.Root.Children.Add(detailDataEditView);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInventoryBillsWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null) { 
                    this.SetObjectToEdit(entity);
                    FileUploadDataEditPanels.FilePath = entity.Path;
                }
            }, null);
        }
    }

}
