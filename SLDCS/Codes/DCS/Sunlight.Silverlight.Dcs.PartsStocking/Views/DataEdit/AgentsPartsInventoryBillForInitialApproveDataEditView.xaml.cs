﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgentsPartsInventoryBillForInitialApproveDataEditView {
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private ObservableCollection<KeyValuePair> kvBranchs;
        private KeyValueManager keyValueManager;
        private DataGridViewBase partsInventoryBillForApproveDataGridView;
        private ButtonItem rejectBtn;
        private readonly string[] kvNames = new[] {
            "Area_Category"
        };
        public AgentsPartsInventoryBillForInitialApproveDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        public ObservableCollection<KeyValuePair> KvBranchs {
            get {
                if(this.kvBranchs == null)
                    this.kvBranchs = new ObservableCollection<KeyValuePair>();
                return this.kvBranchs;
            }
        }
        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                if(this.kvWarehouses == null)
                    this.kvWarehouses = new ObservableCollection<KeyValuePair>();
                return this.kvWarehouses;
            }
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvWarehouseAreaCategorys {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        public DataGridViewBase PartsInventoryBillForApproveDataGridView {
            get {
                if(this.partsInventoryBillForApproveDataGridView == null) {
                    this.partsInventoryBillForApproveDataGridView = DI.GetDataGridView("PartsInventoryBillForEditApprove");
                    this.partsInventoryBillForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsInventoryBillForApproveDataGridView;
            }
        }

        private AgentsPartsInventoryBillForUploadDataEditPanel fileUploadDataEditPanels;
        public AgentsPartsInventoryBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (AgentsPartsInventoryBillForUploadDataEditPanel)DI.GetDataEditPanel("AgentsPartsInventoryBillForUpload"));
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        private void CreateUI() {
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvWarehouses.Clear();
                foreach(var warehouse in loadOp.Entities)
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
            }, null);
            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranchs.Clear();
                foreach(var branchs in loadOp.Entities)
                    this.KvBranchs.Add(new KeyValuePair {
                        Key = branchs.Id,
                        Value = branchs.Name,
                        UserObject = branchs
                    });
            }, null);
            //var dataEditPanel = DI.GetDataEditPanel("AgentsPartsInventoryBillInitialApprove");
            //dataEditPanel.SetValue(Grid.RowProperty, 0);
            //dataEditPanel.SetValue(Grid.ColumnProperty, 1);
            //this.Root.Children.Add(dataEditPanel);
            FileUploadDataEditPanels.isHiddenButtons = true;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 10);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);

            this.VerticalLineRoot.Children.Add(this.CreateVerticalLine(1));

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsInventoryBill), "PartsInventoryDetails"), null, () => this.PartsInventoryBillForApproveDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(detailDataEditView);
            this.rejectBtn = new ButtonItem {
                Command = new DelegateCommand(this.RejecrCurrentData),
                Title = PartsStockingUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            };
            this.RegisterButton(rejectBtn, true);
        }
        protected override void OnEditSubmitting() {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            partsInventoryBill.ApprovalId = partsInventoryBill.ApprovalId == null ? 1 : partsInventoryBill.ApprovalId;
            if (partsInventoryBill.ApprovalId == 0 && string.IsNullOrEmpty(partsInventoryBill.RejectComment)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ApproveCommentIsNull);
                return;
            }
            ((IEditableObject)partsInventoryBill).EndEdit();
            try {
                if(partsInventoryBill.Can初审配件盘点单)
                    partsInventoryBill.初审配件盘点单();
                ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_OperationSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            //base.OnEditSubmitting();
        }

        //驳回
        private void RejecrCurrentData() {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            if (string.IsNullOrEmpty(partsInventoryBill.RejectComment)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_RejectReasonIsNull);
                return;
            }
            ((IEditableObject)partsInventoryBill).EndEdit();
            try {
                if(partsInventoryBill.Can驳回配件盘点单)
                    partsInventoryBill.驳回配件盘点单();
                 ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_RejectSuccess);  
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }   
            //base.OnEditSubmitting();
        }

        private void ExecuteSerivcesMethod(string notifyMessage) { 
            DomainContext.SubmitChanges(submitOp => { 
                if (submitOp.HasError) { 
                    if (!submitOp.IsErrorHandled) 
                        submitOp.MarkErrorAsHandled(); 
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp); 
                    DomainContext.RejectChanges(); 
                    return; 
                } 
                this.NotifyEditSubmitted(); 
                this.OnCustomEditSubmitted(); 
                UIHelper.ShowNotification(notifyMessage); 
            }, null); 
        }  
        public void OnCustomEditSubmitted() {  
            this.DataContext = null;  
        }  
  
        public new event EventHandler EditSubmitted;  
        private void NotifyEditSubmitted() {  
            var handler = this.EditSubmitted;  
            if (handler != null)  
                handler(this, EventArgs.Empty);  
        } 

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInventoryBillsWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if (entity != null) { 
                    entity.ApprovalId = 1;
                    entity.RejectComment = null;
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
    }
}
