﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgentsPartsInventoryBillForInventoryCoversDataEditView {
        private DataGridViewBase partsInventoryDetailForInventoryCovers;

        public DataGridViewBase PartsInventoryDetailForInventoryCovers {
            get {
                if(this.partsInventoryDetailForInventoryCovers == null) {
                    this.partsInventoryDetailForInventoryCovers = DI.GetDataGridView("PartsInventoryDetailForInventoryCovers");
                    this.partsInventoryDetailForInventoryCovers.DomainContext = this.DomainContext;
                }
                return this.partsInventoryDetailForInventoryCovers;
            }
        }

        public AgentsPartsInventoryBillForInventoryCoversDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var detail = new DcsDetailDataEditView();
            detail.UnregisterButton(detail.InsertButton);
            detail.UnregisterButton(detail.DeleteButton);
            detail.Register(PartsStockingUIStrings.DataEditView_GroupTitle_PartsInventoryDetail, null, this.PartsInventoryDetailForInventoryCovers);
            this.LayoutRoot.Children.Add(detail);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInventoryBillsWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var partsInventoryBill = loadOp.Entities.SingleOrDefault();
                if(partsInventoryBill == null)
                    return;
                foreach(var detail in partsInventoryBill.PartsInventoryDetails) {
                    detail.Ifcover = true;
                }
                this.SetObjectToEdit(partsInventoryBill);
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsInventoryDetailForInventoryCovers.CommitEdit())
                return;
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            if(partsInventoryBill.PartsInventoryDetails.Any(e => e.StorageAfterInventory < 0)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Validation_PartsInventoryDetail_PartsInventoryDetailMustGreaterThanZero);
                return;
            }
            ((IEditableObject)partsInventoryBill).EndEdit();
            try {
                if(partsInventoryBill.Can库存覆盖配件盘点单)
                    partsInventoryBill.库存覆盖配件盘点单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
    }
}
