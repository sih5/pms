﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class AgentsPartsInventoryBillForRejectDataEditView {
        public AgentsPartsInventoryBillForRejectDataEditView() {
            InitializeComponent();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInventoryBillsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            partsInventoryBill.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(partsInventoryBill.RejectComment))
                partsInventoryBill.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_RejectReasonIsNull, new[] {
                    "RejectComment"
                }));

            if(partsInventoryBill.HasValidationErrors)
                return;

            try {
                if(partsInventoryBill.Can驳回配件盘点单)
                    partsInventoryBill.驳回配件盘点单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_RejectAgencyPartsInventoryBill;
            }
        }
    }
}
