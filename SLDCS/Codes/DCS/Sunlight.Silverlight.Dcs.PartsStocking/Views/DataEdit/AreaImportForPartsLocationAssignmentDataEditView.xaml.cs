﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Common.Views.DataEdit {
    public partial class AreaImportForPartsLocationAssignmentDataEditView {
        private DataGridViewBase partsLocationAssignmentForImportArea;
        private const string EXPORT_DATA_FILE_NAME = "导出库位分配模板.xlsx";
        private ICommand exportFileCommand;
        private int warehouseAreaId;
        public AreaImportForPartsLocationAssignmentDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_ImportWarehouseArea;
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.partsLocationAssignmentForImportArea == null) {
                    this.partsLocationAssignmentForImportArea = DI.GetDataGridView("PartsLocationAssignmentForImportArea");
                    this.partsLocationAssignmentForImportArea.DataContext = this.DataContext;
                }
                return this.partsLocationAssignmentForImportArea;
            }
        }

        private ObservableCollection<WarehouseAreaForStockExtend> warehouseAreaForStockExtends;

        public ObservableCollection<WarehouseAreaForStockExtend> WarehouseAreaForStockExtends {
            get {
                if(this.warehouseAreaForStockExtends == null) {
                    this.warehouseAreaForStockExtends = new ObservableCollection<WarehouseAreaForStockExtend>();
                } return warehouseAreaForStockExtends;
            }
        }

        private void CreateUI() {
            this.HideCancelButton();
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportWarehouseAreaForPartsStockAsync(fileName, warehouseAreaId);
            this.ExcelServiceClient.ImportWarehouseAreaForPartsStockCompleted -= ExcelServiceClientOnImportWarehouseAreaForPartsStockCompleted;
            this.ExcelServiceClient.ImportWarehouseAreaForPartsStockCompleted += ExcelServiceClientOnImportWarehouseAreaForPartsStockCompleted;
        }

        private void ExcelServiceClientOnImportWarehouseAreaForPartsStockCompleted(object sender, ImportWarehouseAreaForPartsStockCompletedEventArgs e) {
            var dataEditView = this.DataContext as AreaImportForPartsLocationAssignmentDataEditView;
            if(dataEditView == null)
                return;
            if(e.rightData != null) {
                foreach(var data in e.rightData) {
                    dataEditView.WarehouseAreaForStockExtends.Add(new WarehouseAreaForStockExtend {
                        WarehouseAreaCode = data.WarehouseAreaCodeStr,
                        PartCode = data.PartCodeStr,
                        PartName = data.PartNameStr,
                    });
                }
            }
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_WarehouseAreaCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        public override object ExchangeData(Core.View.IBaseView sender, string subject, params object[] contents) {
            this.warehouseAreaId = (int)contents[0];
            return null;
        }
    }
}
