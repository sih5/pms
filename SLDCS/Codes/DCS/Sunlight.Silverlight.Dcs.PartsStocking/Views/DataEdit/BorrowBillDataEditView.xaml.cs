﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class BorrowBillDataEditView {
        private readonly string[] kvNames = {
            "BorrowBill_Type"
        };
        private KeyValueManager keyValueManager;
        private DataGridViewBase borrowBillDetailForEditDataGridView;

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();

        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories;
            }
        }

        public object KvTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        private BorrowBillForUploadDataEditPanel fileUploadDataEditPanels;
        public BorrowBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (BorrowBillForUploadDataEditPanel)DI.GetDataEditPanel("BorrowBillForUpload"));
            }
        }
        #region 导入清单

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var borrowBill = this.DataContext as BorrowBill;
                        if(borrowBill == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportBorrowBillDetailAsync(e.HandlerData.CustomData["Path"].ToString(), 0, borrowBill.WarehouseId);
                        this.excelServiceClient.ImportBorrowBillDetailCompleted -= ExcelServiceClient_ImportBorrowBillDetailCompleted;
                        this.excelServiceClient.ImportBorrowBillDetailCompleted += ExcelServiceClient_ImportBorrowBillDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void ExcelServiceClient_ImportBorrowBillDetailCompleted(object sender, ImportBorrowBillDetailCompletedEventArgs e) {
            var borrowBill = this.DataContext as BorrowBill;
            if(borrowBill == null)
                return;
            var detailList = borrowBill.BorrowBillDetails.ToList();
            foreach(var detail in detailList) {
                borrowBill.BorrowBillDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    var partsSalesOrderDetail = new BorrowBillDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        SihCode = data.SihCode,
                        MeasureUnit = data.MeasureUnit,
                        BorrowQty = data.BorrowQty,
                        WarehouseAreaId = data.WarehouseAreaId,
                        WarehouseAreaCode = data.WarehouseAreaCode,
                    };
                    borrowBill.BorrowBillDetails.Add(partsSalesOrderDetail);
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));

        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsStockingUIStrings.Upload_Error3);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        private ICommand exportFileCommand;

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private const string EXPORT_DATA_FILE_NAME = "导出借用清单模板.xlsx";

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName,
                    },
                    //new ImportTemplateColumn {
                    //    Name = "红岩号",
                    //    IsRequired = true
                    //},
                    new ImportTemplateColumn {
                        Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_BorrowQty,
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        #endregion

        public BorrowBillDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.PartsSalesOrderForExportDataEditView_Loaded;
        }

        private void PartsSalesOrderForExportDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.KvPartsSalesCategories.Clear();
                foreach (var partsSalesCategory in loadOp2.Entities) {
                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name,
                        UserObject = partsSalesCategory
                    });
                }
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
            this.DomainContext.Load(this.DomainContext.GetWarehousesWithStorageCompanyQuery().Where(r => r.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效 && !r.WmsInterface && r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                foreach(var warehouse in loadOp.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
                }
            }, null);
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private DataGridViewBase BorrowBillDetailForEditDataGridView {
            get {
                if(this.borrowBillDetailForEditDataGridView == null) {
                    this.borrowBillDetailForEditDataGridView = DI.GetDataGridView("BorrowBillDetailForEdit");
                    this.borrowBillDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.borrowBillDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register("借用单清单", null, () => this.BorrowBillDetailForEditDataGridView);
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
                //Command = new DelegateCommand(() => this.Uploader.ShowFileDialog())
            });
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Title_ExportTemplate,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            this.SubRoot.Children.Add(detailEditView);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 8);
            FileUploadDataEditPanels.SetValue(Grid.RowSpanProperty, 4);
            FileUploadDataEditPanels.Margin = new Thickness(20, 0, 0, 0);
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);
        }

        private void ShowFileDialog() {
            var borrowBill = this.DataContext as BorrowBill;
            if(borrowBill.WarehouseId == null) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryBill_WarehouseId);
                return;
            }
            this.Uploader.ShowFileDialog();
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.KvPartsSalesCategories.Clear();
            this.KvWarehouses.Clear();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.KvPartsSalesCategories.Clear();
            this.KvWarehouses.Clear();
        }

        protected override void OnEditSubmitting() {
            var borrowBill = this.DataContext as BorrowBill;
            if(borrowBill == null || !this.BorrowBillDetailForEditDataGridView.CommitEdit())
                return;
            borrowBill.ValidationErrors.Clear();
            foreach(var item in borrowBill.BorrowBillDetails) {
                item.ValidationErrors.Clear();
            }
            if(string.IsNullOrEmpty(borrowBill.PartsSalesCategoryName)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_BorrowBill_PartsSalesCategoryNameIsNull);
                return;
            }
            if(borrowBill.WarehouseId == default(int) || borrowBill.WarehouseId == null) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_BorrowBill_WarehouseIsNull);
                return;
            }
            if(string.IsNullOrEmpty(borrowBill.BorrowDepartmentName)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_BorrowBill_BorrowDepartmentNameIsNull);
                return;
            }
            if(string.IsNullOrEmpty(borrowBill.BorrowName)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_BorrowBill_BorrowNameIsNull);
                return;
            }
            if(borrowBill.Type == default(int) || borrowBill.Type == null) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_BorrowBill_TypeIsNull);
                return;
            }
            if(string.IsNullOrEmpty(borrowBill.ContactMethod)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_BorrowBill_ContactMethodIsNull);
                return;
            }
            if(!borrowBill.BorrowBillDetails.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_BorrowBill_BorrowBillDetailsIsEmpty);
                return;
            }
            if(borrowBill.BorrowBillDetails.Any(r => r.BorrowQty <= 0)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_BorrowBillDetails_BorrowQtyIsNotLessThanZero);
                return;
            }
            if(borrowBill.HasValidationErrors || borrowBill.BorrowBillDetails.Any(e => e.HasValidationErrors))
                return;
            borrowBill.Path = FileUploadDataEditPanels.FilePath;
            ((IEditableObject)borrowBill).EndEdit();
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetBorrowBillForDetailsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                FileUploadDataEditPanels.FilePath = entity.Path;
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return PartsStockingUIStrings.BusinessName_BorrowBill;
            }
        }

        public void OnCustomEditSubmitted() {
            this.KvPartsSalesCategories.Clear();
            this.KvWarehouses.Clear();
        }

        private void DcsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var borrowBill = this.DataContext as BorrowBill;
            if(borrowBill == null)
                return;
            if(e.RemovedItems != null && e.RemovedItems.Count == 1 && e.AddedItems != null && e.AddedItems.Count == 1) {
                if(borrowBill.BorrowBillDetails != null && borrowBill.BorrowBillDetails.Any()) {
                    DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Confirm_BorrowBill_ClearWarehouse, () => {
                        foreach(var detail in borrowBill.BorrowBillDetails.ToArray()) {
                            borrowBill.BorrowBillDetails.Remove(detail);
                        }

                    }, () => {
                        comboBox.SelectionChanged -= this.DcsComboBox_SelectionChanged;
                        comboBox.SelectedItem = e.RemovedItems[0];
                        comboBox.SelectionChanged += this.DcsComboBox_SelectionChanged;
                    });
                }
            }
            var borrowBillInfo = comboBox.SelectedItem as KeyValuePair;
            if(borrowBillInfo == null)
                return;
            borrowBill.WarehouseId = ((Warehouse)borrowBillInfo.UserObject).Id;
            borrowBill.WarehouseCode = ((Warehouse)borrowBillInfo.UserObject).Code;
            borrowBill.WarehouseName = ((Warehouse)borrowBillInfo.UserObject).Name;
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}