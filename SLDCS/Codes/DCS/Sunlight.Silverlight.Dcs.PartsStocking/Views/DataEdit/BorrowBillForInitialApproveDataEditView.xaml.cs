﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class BorrowBillForInitialApproveDataEditView {
        private readonly string[] kvNames = {
            "BorrowBill_Type"
        };
        private KeyValueManager keyValueManager;
        private DataGridViewBase borrowBillDetailForApproveDataGridView;

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();

        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories;
            }
        }

        public object KvTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public BorrowBillForInitialApproveDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.PartsSalesOrderForExportDataEditView_Loaded;
        }

        private void PartsSalesOrderForExportDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp2.Entities) {
                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name,
                        UserObject = partsSalesCategory
                    });
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetWarehousesWithStorageCompanyQuery().Where(r => r.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效 && !r.WmsInterface && r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                foreach(var warehouse in loadOp.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
                }
            }, null);
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private DataGridViewBase BorrowBillDetailForApproveDataGridView {
            get {
                if(this.borrowBillDetailForApproveDataGridView == null) {
                    this.borrowBillDetailForApproveDataGridView = DI.GetDataGridView("BorrowBillDetailForApprove");
                    this.borrowBillDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.borrowBillDetailForApproveDataGridView;
            }
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.UnregisterButton(detailEditView.DeleteButton);
            detailEditView.Register(PartsStockingUIStrings.DataEditView_Title_BorrowBillDetails, null, () => this.BorrowBillDetailForApproveDataGridView);
            this.SubRoot.Children.Add(detailEditView);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = PartsStockingUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            }, true);
        }

        //驳回
        private void RejectCurrentData() {
            var borrowBill = this.DataContext as BorrowBill;
            if(borrowBill == null)
                return;
            ((IEditableObject)borrowBill).EndEdit();
            try {
                if(borrowBill.Can初审驳回配件借用单)
                    borrowBill.初审驳回配件借用单();
                ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_RejectSuccess);  
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            //base.OnEditSubmitting();
        }

        private void ExecuteSerivcesMethod(string notifyMessage) { 
            DomainContext.SubmitChanges(submitOp => { 
                if (submitOp.HasError) { 
                    if (!submitOp.IsErrorHandled) 
                        submitOp.MarkErrorAsHandled(); 
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp); 
                    DomainContext.RejectChanges(); 
                    return; 
                } 
                this.NotifyEditSubmitted(); 
                this.OnCustomEditSubmitted(); 
                UIHelper.ShowNotification(notifyMessage); 
            }, null); 
        }  
  
        public new event EventHandler EditSubmitted;  
        private void NotifyEditSubmitted() {  
            var handler = this.EditSubmitted;  
            if (handler != null)  
                handler(this, EventArgs.Empty);  
        } 

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.KvPartsSalesCategories.Clear();
        }

        protected override void OnEditSubmitting() {
            var borrowBill = this.DataContext as BorrowBill;
            if(borrowBill == null || !this.BorrowBillDetailForApproveDataGridView.CommitEdit())
                return;
            borrowBill.ValidationErrors.Clear();
            foreach(var item in borrowBill.BorrowBillDetails) {
                item.ValidationErrors.Clear();
            }
            ((IEditableObject)borrowBill).EndEdit();
            try {
                if(borrowBill.Can初审配件借用单)
                    borrowBill.初审配件借用单();
                ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_OperationSuccess);  
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            //base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetBorrowBillForDetailsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_InitialApprove;
            }
        }

        public void OnCustomEditSubmitted() {
            this.KvPartsSalesCategories.Clear();
            this.DataContext = null;
        }
    }
}