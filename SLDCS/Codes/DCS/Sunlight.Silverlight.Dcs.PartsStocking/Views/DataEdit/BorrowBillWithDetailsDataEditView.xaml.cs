﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;
using System.Windows.Controls;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class BorrowBillWithDetailsDataEditView  {
        private readonly string[] kvNames = {
            "BorrowBill_Type"
        };
        private KeyValueManager keyValueManager;
        private DataGridViewBase borrowBillDetailForEditDataGridView;

        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();

        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories;
            }
        }

        public object KvTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        private BorrowBillForUploadDataEditPanel fileUploadDataEditPanels;
        public BorrowBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (BorrowBillForUploadDataEditPanel)DI.GetDataEditPanel("BorrowBillForUpload"));
            }
        }
   
        public BorrowBillWithDetailsDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.Loaded += this.PartsSalesOrderForExportDataEditView_Loaded;
        }

        private void PartsSalesOrderForExportDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.KvPartsSalesCategories.Clear();
                foreach (var partsSalesCategory in loadOp2.Entities) {
                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name,
                        UserObject = partsSalesCategory
                    });
                }
                this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            }, null);
            this.DomainContext.Load(this.DomainContext.GetWarehousesWithStorageCompanyQuery().Where(r => r.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效 && !r.WmsInterface && r.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                foreach(var warehouse in loadOp.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
                }
            }, null);
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private DataGridViewBase BorrowBillDetailForEditDataGridView {
            get {
                if(this.borrowBillDetailForEditDataGridView == null) {
                    this.borrowBillDetailForEditDataGridView = DI.GetDataGridView("BorrowBillDetailForEdit");
                    this.borrowBillDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.borrowBillDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register("借用单清单", null, () => this.BorrowBillDetailForEditDataGridView);
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.UnregisterButton(detailEditView.DeleteButton);
            this.SubRoot.Children.Add(detailEditView);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 8);
            FileUploadDataEditPanels.SetValue(Grid.RowSpanProperty, 4);
            FileUploadDataEditPanels.Margin = new Thickness(20, 0, 0, 0);
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);
            this.FileUploadDataEditPanels.isHiddenButtons = true;   
        }
     

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.KvPartsSalesCategories.Clear();
            this.KvWarehouses.Clear();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.KvPartsSalesCategories.Clear();
            this.KvWarehouses.Clear();
        }

      
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetBorrowBillForDetailsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                FileUploadDataEditPanels.FilePath = entity.Path;
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return PartsStockingUIStrings.BusinessName_BorrowBill;
            }
        }

        public void OnCustomEditSubmitted() {
            this.KvPartsSalesCategories.Clear();
            this.KvWarehouses.Clear();
        }      

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}