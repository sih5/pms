﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.ViewModel;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class BoxUpTaskDataEditView : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private DateTime? createTimeFrom, createTimeTo;
        private DateTime? pickingFinishTimeFrom, pickingFinishTimeTo;
        private string code, counterpartCompanyCode, counterpartCompanyName;
        private DataGridViewBase pickingTaskDataGridView;

        private ObservableCollection<VirtualPickingTaskForBoxUp> pickingTasks = new ObservableCollection<VirtualPickingTaskForBoxUp>();

        public BoxUpTaskDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged +=BoxUpTaskDataEditView_DataContextChanged;
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void CreateUI()
        {

            this.radButtonSearch.Click += this.RadButtonSearch_Click;
            this.CreateTimeFrom = DateTime.Now.Date.AddDays(-7);
            this.CreateTimeTo = DateTime.Now.Date;
            this.PickingFinishTimeFrom = DateTime.Now.Date.AddDays(-7);
            this.PickingFinishTimeTo = DateTime.Now.Date;

            this.gdShow.Children.Add(PickingTaskDataGridView);
        }

        private void BoxUpTaskDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.Code = this.CounterpartCompanyCode = this.CounterpartCompanyName = default(string);
            this.CreateTimeFrom = this.CreateTimeTo = default(DateTime?);

            this.gdSearch.Visibility = Visibility.Visible;
            this.gdShow.Visibility = Visibility.Visible;
        }

        private void RadButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            this.PickingTasks.Clear();

            if (this.CreateTimeTo.HasValue)
            {
                this.CreateTimeTo = new DateTime(this.CreateTimeTo.Value.Year, this.CreateTimeTo.Value.Month, this.CreateTimeTo.Value.Day, 23, 59, 59);
            }
            
            if (this.PickingFinishTimeTo.HasValue)
            {
                this.PickingFinishTimeTo = new DateTime(this.PickingFinishTimeTo.Value.Year, this.PickingFinishTimeTo.Value.Month, this.PickingFinishTimeTo.Value.Day, 23, 59, 59);
            }
            
            this.DomainContext.Load(this.DomainContext.getPickingTaskForBoxUpQuery(code, (int)DcsPickingTaskStatus.拣货完成, counterpartCompanyCode, counterpartCompanyName, createTimeFrom, createTimeTo, pickingFinishTimeFrom, PickingFinishTimeTo)
            , LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }

                foreach (var entity in loadOp.Entities)
                {
                    this.pickingTasks.Add(entity);
                }
            }, null);
        }

        private DataGridViewBase PickingTaskDataGridView
        {
            get
            {
                if (this.pickingTaskDataGridView == null)
                {
                    this.pickingTaskDataGridView = DI.GetDataGridView("PickingTaskForBoxUp");
                    this.pickingTaskDataGridView.DomainContext = this.DomainContext;
                    this.pickingTaskDataGridView.DataContext = this;
                }
                return this.pickingTaskDataGridView;
            }

        }


        public ObservableCollection<VirtualPickingTaskForBoxUp> PickingTasks
        {
            get
            {
                return this.pickingTasks ?? (this.pickingTasks = new ObservableCollection<VirtualPickingTaskForBoxUp>());
            }
            set
            {
                this.pickingTasks = value;
                this.OnPropertyChanged("PickingTasks");
            }
        }

        protected override void OnEditSubmitting()
        {
            if (!this.PickingTaskDataGridView.CommitEdit())
                return;
            if (this.PickingTaskDataGridView.SelectedEntities == null || this.PickingTaskDataGridView.SelectedEntities.Count()==0)
            {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PickingTaskIsNull);
                return;
            }
            
            var virtualPickingTaskForBoxUps = this.PickingTaskDataGridView.SelectedEntities.Cast<VirtualPickingTaskForBoxUp>().ToArray();

            var partsSalesCategoryIds = virtualPickingTaskForBoxUps.Select(r => r.PartsSalesCategoryId).Distinct().ToList();
            var warehouseIds = virtualPickingTaskForBoxUps.Select(r => r.WarehouseId).Distinct().ToList();
            var receivingAddress = virtualPickingTaskForBoxUps.Select(r => r.ReceivingAddress).Distinct().ToList();
            var orderTypeIds = virtualPickingTaskForBoxUps.Select(r => r.OrderTypeId).Distinct().ToList();
            var shippingMethods = virtualPickingTaskForBoxUps.Select(r => r.ShippingMethod).Distinct().ToList();
            var counterpartCompanyIds = virtualPickingTaskForBoxUps.Select(r => r.CounterpartCompanyId).Distinct().ToList();
            var receivingWarehouseIds = virtualPickingTaskForBoxUps.Select(r => r.ReceivingWarehouseId).Distinct().ToList();
            if (partsSalesCategoryIds.Count() != 1 || warehouseIds.Count() != 1 || receivingAddress.Count() != 1 || orderTypeIds.Count() != 1 || shippingMethods.Count() != 1 || counterpartCompanyIds.Count() != 1 || receivingWarehouseIds.Count() > 1)
            {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PickingTaskError);
                return;
            }
            if (this.DomainContext.IsBusy)
                return;
            ShellViewModel.Current.IsBusy = true;
            this.DomainContext.CreateBoxUpTask(virtualPickingTaskForBoxUps, invokeOp =>
            {
                if (invokeOp.HasError)
                {
                    ShellViewModel.Current.IsBusy = false;
                    if (!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    var error = invokeOp.ValidationErrors.First();
                    if (error != null)
                        UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                    else
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    return;
                }
                this.PickingTasks.Clear();
                //base.OnEditSubmitting();
                //重新查询
                if (this.CreateTimeTo.HasValue) {
                    this.CreateTimeTo = new DateTime(this.CreateTimeTo.Value.Year, this.CreateTimeTo.Value.Month, this.CreateTimeTo.Value.Day, 23, 59, 59);
                }

                if (this.PickingFinishTimeTo.HasValue) {
                    this.PickingFinishTimeTo = new DateTime(this.PickingFinishTimeTo.Value.Year, this.PickingFinishTimeTo.Value.Month, this.PickingFinishTimeTo.Value.Day, 23, 59, 59);
                }
                this.DomainContext.Load(this.DomainContext.getPickingTaskForBoxUpQuery(code, (int)DcsPickingTaskStatus.拣货完成, counterpartCompanyCode, counterpartCompanyName, createTimeFrom, createTimeTo, pickingFinishTimeFrom, PickingFinishTimeTo), LoadBehavior.RefreshCurrent, loadOp => {
                    if (loadOp.HasError) {
                        if (!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    foreach (var entity in loadOp.Entities) {
                        this.pickingTasks.Add(entity);
                    }
                    ShellViewModel.Current.IsBusy = false;
                }, null);
            }, null);
          
        }

        protected override void OnEditSubmitted()
        {
            base.OnEditSubmitted();
        }
        public DateTime? PickingFinishTimeFrom
        {
            get
            {
                return this.pickingFinishTimeFrom;
            }
            set
            {
                this.pickingFinishTimeFrom = value;
                this.OnPropertyChanged("PickingFinishTimeFrom");
            }
        }

        public DateTime? PickingFinishTimeTo
        {
            get
            {
                return this.pickingFinishTimeTo;
            }
            set
            {
                this.pickingFinishTimeTo = value;
                this.OnPropertyChanged("PickingFinishTimeTo");
            }
        }
        public DateTime? CreateTimeFrom
        {
            get
            {
                return this.createTimeFrom;
            }
            set
            {
                this.createTimeFrom = value;
                this.OnPropertyChanged("CreateTimeFrom");
            }
        }

        public DateTime? CreateTimeTo
        {
            get
            {
                return this.createTimeTo;
            }
            set
            {
                this.createTimeTo = value;
                this.OnPropertyChanged("CreateTimeTo");
            }
        }

        public string Code
        {
            get
            {
                return this.code;
            }
            set
            {
                this.code = value;
                this.OnPropertyChanged("Code");
            }
        }
        public string CounterpartCompanyCode
        {
            get
            {
                return this.counterpartCompanyCode;
            }
            set
            {
                this.counterpartCompanyCode = value;
                this.OnPropertyChanged("CounterpartCompanyCode");
            }
        }

        public string CounterpartCompanyName
        {
            get
            {
                return this.counterpartCompanyName;
            }
            set
            {
                this.counterpartCompanyName = value;
                this.OnPropertyChanged("CounterpartCompanyName");
            }
        }

        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }


        protected override string BusinessName {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsBoxUpTask;
            }
        }

    }
}
