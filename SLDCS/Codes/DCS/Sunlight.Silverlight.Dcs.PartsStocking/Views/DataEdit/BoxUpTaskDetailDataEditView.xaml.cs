﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class BoxUpTaskDetailDataEditView : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private DataGridViewBase boxUpTaskDetailDataGridView;
        private ObservableCollection<VirtualBoxUpTaskDetail> boxUpTaskDetails;
        private string boxUpTaskCode, partsSalesCategoryName, warehouseCode, warehouseName, counterpartCompanyCode, counterpartCompanyName, containerNumber, remark;
        public BoxUpTaskDetailDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void CreateUI()
        {
            this.gdShow.Children.Add(BoxUpTaskDetailDataGridView);
        }

        private DataGridViewBase BoxUpTaskDetailDataGridView
        {
            get
            {
                if (this.boxUpTaskDetailDataGridView == null)
                {
                    this.boxUpTaskDetailDataGridView = DI.GetDataGridView("BoxUpTaskDetail");
                    this.boxUpTaskDetailDataGridView.DomainContext = this.DomainContext;
                    this.boxUpTaskDetailDataGridView.DataContext = this;
                }
                return this.boxUpTaskDetailDataGridView;
            }

        }


        public ObservableCollection<VirtualBoxUpTaskDetail> BoxUpTaskDetails
        {
            get
            {
                return this.boxUpTaskDetails ?? (this.boxUpTaskDetails = new ObservableCollection<VirtualBoxUpTaskDetail>());
            }
        }

        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
            {
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            }
            else
                this.LoadEntityToEdit((int)id);

        }

        private void LoadEntityToEdit(int id)
        {
            this.BoxUpTaskDetails.Clear();
            this.DomainContext.Load(this.DomainContext.getBoxUpTaskByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var boxUpTask = loadOp.Entities.FirstOrDefault();
                this.BoxUpTaskCode = boxUpTask.Code;
                this.PartsSalesCategoryName = boxUpTask.PartsSalesCategoryName;
                this.WarehouseCode=boxUpTask.WarehouseCode;
                this.WarehouseName=boxUpTask.WarehouseName;
                this.CounterpartCompanyCode=boxUpTask.CounterpartCompanyCode;
                this.CounterpartCompanyName=boxUpTask.CounterpartCompanyName;
                //this.ContainerNumber=boxUpTask.ContainerNumber;
                this.Remark=boxUpTask.Remark;
            }, null);

            this.DomainContext.Load(this.DomainContext.getBoxUpTaskDetailsByTaskIdQuery(id), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }

                foreach (var entity in loadOp.Entities)
                {
                    var boxUpQty = entity.BoxUpQty ?? 0;
                    if (boxUpQty != entity.PlanQty) {
                        this.boxUpTaskDetails.Add(entity);
                    }
                }
            }, null);
        }

        protected override void OnEditCancelled()
        {
            this.txtContainerNumber.Text = "";
            base.OnEditCancelled();
        }
        protected override void OnEditSubmitted()
        {
            this.txtContainerNumber.Text = "";
            base.OnEditSubmitted();
        }
        protected override void OnEditSubmitting(){
            if (!this.BoxUpTaskDetailDataGridView.CommitEdit())
                return;
            if (this.boxUpTaskDetails == null || !this.boxUpTaskDetails.Any()){
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_BoxUpTaskDetailsIsNull);
                return;
            }
            var boxUpTaskDetails1 = boxUpTaskDetails.Where(r => r.NowBoxUpQuantity > 0).ToArray();   
            if (string.IsNullOrEmpty(this.containerNumber) && boxUpTaskDetails1.Any()){
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ContainerNumberIsNull);
                return;
            }
            if (this.DomainContext.IsBusy)
                return;
            //如果全部清单的已装箱数量等于计划量，直接走装箱完成不用再提示询问
            var boxup = boxUpTaskDetails.Count(r => r.BoxUpQty + r.NowBoxUpQuantity == r.PlanQty) ;
            var plan = boxUpTaskDetails.Count();
            if (boxup == plan)
            {
                foreach (var boxUpTaskDetail in boxUpTaskDetails)
                {
                    boxUpTaskDetail.ValidationErrors.Clear();
                }
                this.DomainContext.doBoxUp(boxUpTaskDetails1, containerNumber, remark, false, invokeOp =>
                {
                    if (invokeOp.HasError)
                    {
                        if (!invokeOp.IsErrorHandled)
                            invokeOp.MarkErrorAsHandled();
                        var error = invokeOp.ValidationErrors.First();
                        if (error != null)
                            UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                        else
                            DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                        return;
                    }
                    base.OnEditSubmitting();
                }, null);
            }
            else {

                DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Text_IsBoxUpOver, () =>
                {
                    var boxUpTaskDetails2 = boxUpTaskDetails.Distinct().ToArray();
                    
                    foreach (var boxUpTaskDetail in boxUpTaskDetails)
                    {
                        boxUpTaskDetail.ValidationErrors.Clear();
                    }

                    this.DomainContext.doBoxUp(boxUpTaskDetails2, containerNumber, remark, true, invokeOp =>
                    {
                        if (invokeOp.HasError)
                        {
                            if (!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.First();
                            if (error != null)
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                            else
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            return;
                        }
                        base.OnEditSubmitting();
                    }, null);
                }, () =>
                {
                    if (boxUpTaskDetails1 == null || !boxUpTaskDetails1.Any())
                    {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_NowBoxUpQuantityAllZero);
                        return;
                    }
                    foreach (var boxUpTaskDetail in boxUpTaskDetails)
                    {
                        boxUpTaskDetail.ValidationErrors.Clear();
                    }
                    this.DomainContext.doBoxUp(boxUpTaskDetails1, containerNumber, remark, false, invokeOp =>
                    {
                        if (invokeOp.HasError)
                        {
                            if (!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.First();
                            if (error != null)
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                            else
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            return;
                        }
                        base.OnEditSubmitting();
                    }, null);
                }, PartsStockingUIStrings.DataEditView_Text_Yes, PartsStockingUIStrings.DataEditView_Text_No);
           
            }
            
        }

        //完成装箱
        //private void RadButtonFinish_Click(object sender, RoutedEventArgs e)
        //{
        //    if (this.containerNumber == null || this.containerNumber.Equals(""))
        //    {
        //        UIHelper.ShowNotification("箱号不可为空");
        //        return;
        //    }
        //    if (this.boxUpTaskDetails == null || this.boxUpTaskDetails.Count() == 0)
        //    {
        //        UIHelper.ShowNotification("装箱清单不可为空");
        //        return;
        //    }
        //     foreach (var boxUpTaskDetail in boxUpTaskDetails)
        //    {
        //        boxUpTaskDetail.ValidationErrors.Clear();
        //    }
        //    //var boxUpTaskDetails1 = boxUpTaskDetails.Where(r => r.NowBoxUpQuantity > 0).ToArray();

        //    //if (boxUpTaskDetails1 == null || boxUpTaskDetails1.Count() == 0)
        //    //{
        //    //    UIHelper.ShowNotification("本次装箱量不可都为空");
        //    //    return;
        //    //}
        //    var boxUpTaskDetails1 = boxUpTaskDetails.ToArray();
        //    DcsUtils.Confirm("确定要完成装箱吗", () =>
        //    {
        //        var dcsDomainContext = new DcsDomainContext();
        //        dcsDomainContext.doBoxUp(boxUpTaskDetails1, containerNumber, remark, true, invokeOp =>
        //        {
        //            if (invokeOp.HasError)
        //            {
        //                if (!invokeOp.IsErrorHandled)
        //                    invokeOp.MarkErrorAsHandled();
        //                var error = invokeOp.ValidationErrors.First();
        //                if (error != null)
        //                    UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
        //                else
        //                    DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
        //                return;
        //            }
        //        }, null);
        //    });
        //}

        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }


         public string BoxUpTaskCode
        {
            get
            {
                return this.boxUpTaskCode;
            }
            set
            {
                this.boxUpTaskCode = value;
                this.OnPropertyChanged("BoxUpTaskCode");
            }
        }
         public string PartsSalesCategoryName
        {
            get
            {
                return this.partsSalesCategoryName;
            }
            set
            {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }
         public string WarehouseCode
        {
            get
            {
                return this.warehouseCode;
            }
            set
            {
                this.warehouseCode = value;
                this.OnPropertyChanged("WarehouseCode");
            }
        }
        public string WarehouseName
        {
            get
            {
                return this.warehouseName;
            }
            set
            {
                this.warehouseName = value;
                this.OnPropertyChanged("WarehouseName");
            }
        }
        public string CounterpartCompanyCode
        {
            get
            {
                return this.counterpartCompanyCode;
            }
            set
            {
                this.counterpartCompanyCode = value;
                this.OnPropertyChanged("CounterpartCompanyCode");
            }
        }
        public string CounterpartCompanyName
        {
            get
            {
                return this.counterpartCompanyName;
            }
            set
            {
                this.counterpartCompanyName = value;
                this.OnPropertyChanged("CounterpartCompanyName");
            }
        }
         public string ContainerNumber
        {
            get
            {
                return this.containerNumber;
            }
            set
            {
                this.containerNumber = value;
                this.OnPropertyChanged("ContainerNumber");
            }
        }
         public string Remark
        {
            get
            {
                return this.remark;
            }
            set
            {
                this.remark = value;
                this.OnPropertyChanged("Remark");
            }
        }
         protected override string Title
         {
             get
             {
                 return PartsStockingUIStrings.DataEditView_Title_BoxUp;
             }
         }

    }
}
