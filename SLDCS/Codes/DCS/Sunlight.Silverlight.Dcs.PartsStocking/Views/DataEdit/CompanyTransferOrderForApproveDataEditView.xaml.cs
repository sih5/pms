﻿
using System;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class CompanyTransferOrderForApproveDataEditView {
        public event EventHandler CustomEditSubmitted;
        public CompanyTransferOrderForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private DataGridViewBase dataGridView;

        private void NotifyEditSubmitted() {
            var handler = this.CustomEditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_GroupTitle_CompanyTransferOrder;
            }

        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("CompanyTransferOrderDetailForApprove");
                    this.dataGridView.DomainContext = this.DomainContext;
                }
                return dataGridView;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetCompanyTransferOrderWithDetailQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("CompanyTransferOrderForApprove"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            this.HideSaveButton();
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(PartsStockingUIStrings.DataEditView_GroupTitle_CompanyTransferOrderDetail, null, () => this.DataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.LayoutRoot.Children.Add(detailDataEditView);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.DeliverParts),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/DeliverParts.png", UriKind.Relative),
                Title = "出库"
            }, true);
        }

        private DataEditViewBase dataEditView;

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("CompanyTransferOrderForPartsOutboundBill");
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.Radwindow.Close();
            this.NotifyEditSubmitted();
            this.dataEditView = null;
            this.radwindow.Content = null;
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.Radwindow.Close();
            this.dataEditView = null;
            this.radwindow.Content = null;
        }

        private RadWindow radwindow;

        private RadWindow Radwindow {
            get {
                if(this.radwindow == null) {
                    this.radwindow = new RadWindow {
                        Content = this.DataEditViewFactory(),
                        Header = PartsStockingUIStrings.DataEditView_Text_Outbound,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen,
                        CanClose = false,
                    };
                }
                return this.radwindow;
            }
        }

        private Func<UIElement> DataEditViewFactory {
            get;
            set;
        }
        //出库
        private void DeliverParts() {
            this.DataEditViewFactory = () => this.DataEditView;
            if(!this.DataGridView.CommitEdit())
                return;
            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            if(companyTransferOrder == null)
                return;
            //FTDCS_ISS20150131002 确认量可以为0
            if(companyTransferOrder.CompanyTransferOrderDetails.Any(r => r.ConfirmedAmount < 0)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ConfirmedAmountAllError);
                return;
            }
            var partsOutboundBill = this.DataEditView.CreateObjectToEdit<PartsOutboundBill>();
            var eidview = this.DataEditView as CompanyTransferOrderForPartsOutboundBillDataEditView;
            if(eidview == null)
                return;
            partsOutboundBill.OutboundType = (int)DcsPartsOutboundType.配件调拨;
            partsOutboundBill.PartsSalesCategoryId = companyTransferOrder.PartsSalesCategoryId;
            partsOutboundBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
            if(companyTransferOrder.OriginalWarehouseId != null)
                partsOutboundBill.WarehouseId = (int)companyTransferOrder.OriginalWarehouseId;
            partsOutboundBill.StorageCompanyId = companyTransferOrder.OriginalCompanyId;
            partsOutboundBill.StorageCompanyCode = companyTransferOrder.OriginalCompanyCode;
            partsOutboundBill.StorageCompanyName = companyTransferOrder.OriginalCompanyName;
            partsOutboundBill.StorageCompanyType = (int)DcsCompanyType.服务站兼代理库;
            partsOutboundBill.CounterpartCompanyId = companyTransferOrder.DestCompanyId;
            partsOutboundBill.CounterpartCompanyCode = companyTransferOrder.DestCompanyCode;
            partsOutboundBill.CounterpartCompanyName = companyTransferOrder.DestCompanyName;
            partsOutboundBill.ReceivingCompanyId = companyTransferOrder.DestCompanyId;
            partsOutboundBill.ReceivingCompanyCode = companyTransferOrder.DestCompanyCode;
            partsOutboundBill.ReceivingCompanyName = companyTransferOrder.DestCompanyName;
            partsOutboundBill.CustomerAccountId = null;
            partsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.不结算;
            partsOutboundBill.OriginalRequirementBillId = companyTransferOrder.Id;
            partsOutboundBill.OriginalRequirementBillType = (int)DcsOriginalRequirementBillType.企业间配件调拨;
            partsOutboundBill.OriginalRequirementBillCode = companyTransferOrder.Code;
            foreach(var companyTransferOrderDetail in companyTransferOrder.CompanyTransferOrderDetails) {
                if(companyTransferOrderDetail.ConfirmedAmount != null)
                    companyTransferOrderDetail.CurrentOutboundAmount = (int)companyTransferOrderDetail.ConfirmedAmount;
                eidview.CompanyTransferOrderDetails.Add(companyTransferOrderDetail);
            }
            this.DataEditView.SetObjectToEditById(companyTransferOrder.Id);
            if(this.radwindow != null && this.radwindow.Content == null) {
                this.radwindow.Content = this.DataEditViewFactory();
            }
            this.Radwindow.ShowDialog();
        }

        protected override void Reset() {
            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            if(companyTransferOrder != null && this.DomainContext.CompanyTransferOrders.Contains(companyTransferOrder))
                this.DomainContext.CompanyTransferOrders.Detach(companyTransferOrder);
        }
    }
}
