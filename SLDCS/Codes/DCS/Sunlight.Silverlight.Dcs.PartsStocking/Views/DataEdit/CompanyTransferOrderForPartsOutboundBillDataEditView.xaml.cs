﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class CompanyTransferOrderForPartsOutboundBillDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private ObservableCollection<CompanyTransferOrderDetail> companyTransferOrderDetails;
        private ObservableCollection<PartsOutboundBillDetail> partsOutboundBillDetails;
        private DataGridViewBase companyTransferOrderDetailsDataGridView;
        private DataGridViewBase virtualPartsStockForDeliverDataGridView;
        private string partsSalesCategoryName;
        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        public string OutboundType {
            get {
                return DcsPartsOutboundType.配件调拨.ToString();
            }
        }

        public CompanyTransferOrderForPartsOutboundBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public DataGridViewBase CompanyTransferOrderDetailsDataGridView {
            get {
                if(this.companyTransferOrderDetailsDataGridView == null) {
                    this.companyTransferOrderDetailsDataGridView = DI.GetDataGridView("CompanyTransferOrderDetails");
                    this.companyTransferOrderDetailsDataGridView.DomainContext = this.DomainContext;
                    this.companyTransferOrderDetailsDataGridView.DataContext = this;
                }
                return this.companyTransferOrderDetailsDataGridView;
            }
        }

        private DataGridViewBase VirtualPartsStockForDeliverDataGridView {
            get {
                if(this.virtualPartsStockForDeliverDataGridView == null) {
                    this.virtualPartsStockForDeliverDataGridView = DI.GetDataGridView("VirtualPartsStockForDeliver");
                    this.virtualPartsStockForDeliverDataGridView.DomainContext = this.DomainContext;
                    this.virtualPartsStockForDeliverDataGridView.DataContext = this;
                }
                return this.virtualPartsStockForDeliverDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            var tbPartsOutboundPlanDetails = new RadTabControl();
            tbPartsOutboundPlanDetails.BackgroundVisibility = System.Windows.Visibility.Collapsed;
            tbPartsOutboundPlanDetails.Items.Add(new RadTabItem {
                Content = this.CompanyTransferOrderDetailsDataGridView,
                Header = PartsStockingUIStrings.DataEditView_Title_PartsOutboundPlanDetail
            });
            tbPartsOutboundPlanDetails.SetValue(Grid.ColumnProperty, 1);
            this.gvPartsOutboundPlanDetail.Children.Add(tbPartsOutboundPlanDetails);
            var tbVirtualPartsStock = new DcsDetailDataEditView();
            tbVirtualPartsStock.Register(PartsStockingUIStrings.DataEditView_Title_VirtualPartsStock, null, this.VirtualPartsStockForDeliverDataGridView);
            tbVirtualPartsStock.UnregisterButton(tbVirtualPartsStock.DeleteButton);
            tbVirtualPartsStock.UnregisterButton(tbVirtualPartsStock.InsertButton);
            tbVirtualPartsStock.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Text_Refrech,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.Refresh)
            });
            tbVirtualPartsStock.SetValue(Grid.ColumnProperty, 1);
            this.gvVirtualPartsStock.Children.Add(tbVirtualPartsStock);
            this.KeyValueManager.LoadData();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            var partsOutboundBill = this.DataContext as PartsOutboundBill;
            if(partsOutboundBill == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Id == partsOutboundBill.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError) {
                    loadOption.MarkErrorAsHandled();
                    return;
                }
                var partsSalesCategory = loadOption.Entities.First();
                if(partsSalesCategory == null)
                    return;
                PartsSalesCategoryName = partsSalesCategory.Name;
                partsOutboundBill.BranchId = partsSalesCategory.BranchId;
                partsOutboundBill.BranchCode = partsSalesCategory.BranchCode;
                partsOutboundBill.BranchName = partsSalesCategory.BranchName;
            }, null);
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(r => r.Id == partsOutboundBill.WarehouseId), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError) {
                    loadOption.MarkErrorAsHandled();
                    return;
                }
                var warehouse = loadOption.Entities.SingleOrDefault();
                if(warehouse == null)
                    return;
                partsOutboundBill.WarehouseName = warehouse.Name;
                partsOutboundBill.WarehouseCode = warehouse.Code;

            }, null);
            Refresh();
        }

        private void Refresh() {
            var partsOutboundBill = this.DataContext as PartsOutboundBill;
            if(partsOutboundBill == null)
                return;
            this.PartsOutboundBillDetails.Clear();
            this.DomainContext.Load(this.DomainContext.查询企业间调拨库位库存Query(partsOutboundBill.OriginalRequirementBillId, null), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var virtualPartsStocks = loadOp.Entities.ToArray();
                if(virtualPartsStocks == null)
                    return;
                foreach(var companyTransferOrderDetail in this.CompanyTransferOrderDetails) {
                    var data = virtualPartsStocks.Where(r => r.SparePartId == companyTransferOrderDetail.PartsId);
                    var currentOutboundAmount = companyTransferOrderDetail.CurrentOutboundAmount;//本次出库数量
                    foreach(var virtualPartsStock in data) {
                        var partsOutboundBillDetail = new PartsOutboundBillDetail();
                        partsOutboundBillDetail.SparePartId = virtualPartsStock.SparePartId;
                        partsOutboundBillDetail.SparePartCode = virtualPartsStock.SparePartCode;
                        partsOutboundBillDetail.SparePartName = virtualPartsStock.SparePartName;
                        partsOutboundBillDetail.WarehouseAreaCode = virtualPartsStock.WarehouseAreaCode;
                        partsOutboundBillDetail.WarehouseAreaCategory = virtualPartsStock.WarehouseAreaCategory;
                        partsOutboundBillDetail.UsableQuantity = virtualPartsStock.UsableQuantity;
                        partsOutboundBillDetail.WarehouseAreaId = virtualPartsStock.WarehouseAreaId;
                        partsOutboundBillDetail.BatchNumber = virtualPartsStock.BatchNumber;
                        partsOutboundBillDetail.SettlementPrice = companyTransferOrderDetail.Price;
                        //剩余出库数量
                        if(currentOutboundAmount == 0) {
                            partsOutboundBillDetail.OutboundAmount = 0;
                        } else {
                            partsOutboundBillDetail.OutboundAmount = currentOutboundAmount > partsOutboundBillDetail.UsableQuantity ? partsOutboundBillDetail.UsableQuantity : currentOutboundAmount;
                            currentOutboundAmount = currentOutboundAmount - partsOutboundBillDetail.OutboundAmount;
                        }
                        this.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
                    }
                }
            }, null);

        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsOutboundBillDeliverParts;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.CompanyTransferOrderDetailsDataGridView.CommitEdit())
                return;
            if(!this.VirtualPartsStockForDeliverDataGridView.CommitEdit())
                return;
            var partsOutboundBill = this.DataContext as PartsOutboundBill;
            if(partsOutboundBill == null)
                return;
            partsOutboundBill.ValidationErrors.Clear();

            if(string.IsNullOrWhiteSpace(partsOutboundBill.WarehouseName))
                partsOutboundBill.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_WarehouseNameIsNull, new[] {
                    "WarehouseName"
                }));
            if(partsOutboundBill.OutboundType == default(int))
                partsOutboundBill.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_OutboundTypeIsNull, new[] {
                    "OutboundType"
                }));
            var outboundBillDetails = partsOutboundBill.PartsOutboundBillDetails.ToArray();
            foreach(var partsOutboundBillDetail in outboundBillDetails)
                partsOutboundBill.PartsOutboundBillDetails.Remove(partsOutboundBillDetail);
            foreach(var partsOutboundBillDetail in this.PartsOutboundBillDetails.Where(r => r.OutboundAmount != 0))
                partsOutboundBill.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
            if(!partsOutboundBill.PartsOutboundBillDetails.Any()) {
               // UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_PartsOutboundBillDetailsIsEmpty);
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_NowOutboundAmountIsZero);
                return;
            }
            if(partsOutboundBill.HasValidationErrors || partsOutboundBill.PartsOutboundBillDetails.Any(e => e.HasValidationErrors))
                return;
            if(partsOutboundBill.Can审核服务站兼代理库调拨2)
                partsOutboundBill.审核服务站兼代理库调拨2();
            ((IEditableObject)partsOutboundBill).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<CompanyTransferOrderDetail> CompanyTransferOrderDetails {
            get {
                return this.companyTransferOrderDetails ?? (this.companyTransferOrderDetails = new ObservableCollection<CompanyTransferOrderDetail>());
            }
        }

        public ObservableCollection<PartsOutboundBillDetail> PartsOutboundBillDetails {
            get {
                return this.partsOutboundBillDetails ?? (this.partsOutboundBillDetails = new ObservableCollection<PartsOutboundBillDetail>());
            }
        }

        protected override void Reset() {
            this.CompanyTransferOrderDetails.Clear();
            var partsOutboundBill = this.DataContext as PartsOutboundBill;
            if(partsOutboundBill == null)
                return;
            if(this.DomainContext.PartsOutboundBills.Contains(partsOutboundBill))
                this.DomainContext.PartsOutboundBills.Detach(partsOutboundBill);
            this.PartsOutboundBillDetails.Clear();

        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
