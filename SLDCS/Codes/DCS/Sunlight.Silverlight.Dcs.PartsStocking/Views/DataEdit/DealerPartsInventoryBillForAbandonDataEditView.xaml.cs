﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class DealerPartsInventoryBillForAbandonDataEditView {
        public DealerPartsInventoryBillForAbandonDataEditView() {
            InitializeComponent();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealerPartsInventoryBillByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var dealerPartsInventoryBill = this.DataContext as DealerPartsInventoryBill;
            if(dealerPartsInventoryBill == null)
                return;
            dealerPartsInventoryBill.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(dealerPartsInventoryBill.CancelReason))
                dealerPartsInventoryBill.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_AbandonReasonIsNull, new[] {
                    "CancelReason"
                }));

            if(dealerPartsInventoryBill.HasValidationErrors)
                return;

            try {
                if(dealerPartsInventoryBill.Can盘点单作废)
                    dealerPartsInventoryBill.盘点单作废();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_AbandonDealerPartsInventoryBill;
            }
        }
    }
}
