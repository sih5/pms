﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class DealerPartsInventoryBillForInitialApproveDataEditView {
        private ButtonItem rejectBtn;

        public DealerPartsInventoryBillForInitialApproveDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override bool OnRequestCanSubmit()
        {
            return true;
        }

        private DataGridViewBase dealerPartsInventoryBillDetailForApproveDataEditView;

        private DataGridViewBase DealerPartsInventoryBillDetailForApproveDataEditView {
            get {
                if(this.dealerPartsInventoryBillDetailForApproveDataEditView == null) {
                    this.dealerPartsInventoryBillDetailForApproveDataEditView = DI.GetDataGridView("DealerPartsInventoryBillDetailForApprove");
                    this.dealerPartsInventoryBillDetailForApproveDataEditView.DomainContext = this.DomainContext;
                }
                return this.dealerPartsInventoryBillDetailForApproveDataEditView;
            }
        }

        private PartsShiftOrderForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsShiftOrderForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsShiftOrderForUploadDataEditPanel)DI.GetDataEditPanel("PartsShiftOrderForUpload"));
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("DealerPartsInventoryBillForApprove"));

            FileUploadDataEditPanels.isHiddenButtons = true;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 1);
            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            this.Root.Children.Add(FileUploadDataEditPanels);

            this.Root.Children.Add(this.CreateVerticalLine(1,0,0,2));
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsStockingUIStrings.DataEditView_Title_DealerPartsInventoryBillDetail, null, () => this.DealerPartsInventoryBillDetailForApproveDataEditView);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            dcsDetailGridView.SetValue(Grid.ColumnProperty, 2);
            dcsDetailGridView.SetValue(Grid.RowSpanProperty, 2);
            this.Root.Children.Add(dcsDetailGridView);

            this.rejectBtn = new ButtonItem {
                Command = new DelegateCommand(this.RejecrCurrentData),
                Title = PartsStockingUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            };
            this.RegisterButton(rejectBtn);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealerPartsInventoryBillWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                FileUploadDataEditPanels.FilePath = entity.Path;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_InitialApproveDealerPartsInventoryBillDetail;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.DealerPartsInventoryBillDetailForApproveDataEditView.CommitEdit())
                return;
            var dealerPartsInventoryBill = this.DataContext as DealerPartsInventoryBill;
            if(dealerPartsInventoryBill == null)
                return;
            dealerPartsInventoryBill.ValidationErrors.Clear();
           
            ((IEditableObject)dealerPartsInventoryBill).EndEdit();
            try {
               if(dealerPartsInventoryBill.Can初审服务站配件盘点单)
                    dealerPartsInventoryBill.初审服务站配件盘点单();
                ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_OperationSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            //base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var dealerPartsInventoryBill = this.DataContext as DealerPartsInventoryBill;
            if(dealerPartsInventoryBill != null && this.DomainContext.DealerPartsInventoryBills.Contains(dealerPartsInventoryBill))
                this.DomainContext.DealerPartsInventoryBills.Detach(dealerPartsInventoryBill);
        }
        
        private void RejecrCurrentData() {
            var dealerPartsInventoryBill = this.DataContext as DealerPartsInventoryBill;
            if(dealerPartsInventoryBill == null)
                return;
            if (string.IsNullOrEmpty(dealerPartsInventoryBill.ApproveComment)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_RejectCommentIsNull);
                return;
            }
            ((IEditableObject)dealerPartsInventoryBill).EndEdit();
            try {
               if(dealerPartsInventoryBill.Can驳回服务站配件盘点单)
                    dealerPartsInventoryBill.驳回服务站配件盘点单();
                 ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_RejectSuccess);  
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            //base.OnEditSubmitting();
        }

        private void ExecuteSerivcesMethod(string notifyMessage) { 
            DomainContext.SubmitChanges(submitOp => { 
                if (submitOp.HasError) { 
                    if (!submitOp.IsErrorHandled) 
                        submitOp.MarkErrorAsHandled(); 
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp); 
                    DomainContext.RejectChanges(); 
                    return; 
                } 
                this.NotifyEditSubmitted(); 
                this.OnCustomEditSubmitted(); 
                UIHelper.ShowNotification(notifyMessage); 
            }, null); 
        }  
        public void OnCustomEditSubmitted() {  
            this.DataContext = null;  
        }  
  
        public new event EventHandler EditSubmitted;  
        private void NotifyEditSubmitted() {  
            var handler = this.EditSubmitted;  
            if (handler != null)  
                handler(this, EventArgs.Empty);  
        } 
    }
}
