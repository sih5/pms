﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class DealerPartsInventoryBillForReportImportDataEditView {
        private ICommand exportFileCommand;
        private ICommand importCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出服务站配件盘点模板.xlsx";
        private DataGridViewBase dealerPartsInventoryBillForReportImportDataGridView;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategoryName;

        public ObservableCollection<KeyValuePair> KvPartsSalesCategoryName {
            get {
                return this.kvPartsSalesCategoryName ?? (this.kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>());
            }
        }

        private DataGridViewBase DealerPartsInventoryBillForReportImportDataGridView {
            get {
                return this.dealerPartsInventoryBillForReportImportDataGridView ?? (this.dealerPartsInventoryBillForReportImportDataGridView = DI.GetDataGridView("DealerPartsInventoryBillForReportImport"));
            }
        }

        public DealerPartsInventoryBillForReportImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        #region 导入
        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var dealerPartsInventoryBill = this.DataContext as DealerPartsInventoryBill;
                        if(dealerPartsInventoryBill == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportDealerPartsInventoryBillForReportAsync(BaseApp.Current.CurrentUserData.EnterpriseId, dealerPartsInventoryBill.SalesCategoryId, e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportDealerPartsInventoryBillForReportCompleted -= ExcelServiceClient_ImportDealerPartsInventoryBillForReportCompleted;
                        this.excelServiceClient.ImportDealerPartsInventoryBillForReportCompleted += ExcelServiceClient_ImportDealerPartsInventoryBillForReportCompleted;
                    };
                }
                return this.uploader;
            }
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        void ExcelServiceClient_ImportDealerPartsInventoryBillForReportCompleted(object sender, ImportDealerPartsInventoryBillForReportCompletedEventArgs e) {
            var dealerPartsInventoryBi = this.DataContext as DealerPartsInventoryBill;
            if(dealerPartsInventoryBi != null) {
                foreach(var detail in dealerPartsInventoryBi.DealerPartsInventoryDetails) {
                    dealerPartsInventoryBi.DealerPartsInventoryDetails.Remove(detail);
                }
                var serialNumber = 1;
                if(e.rightData != null && e.rightData.Length > 0) {
                    foreach(var data in e.rightData) {
                        dealerPartsInventoryBi.DealerPartsInventoryDetails.Add(new Web.DealerPartsInventoryDetail {
                            SerialNumber = serialNumber++,
                            SparePartId = data.SparePartId,
                            SparePartCode = data.SparePartCode,
                            SparePartName = data.SparePartName,
                            CurrentStorage = data.CurrentStorage,
                            StorageAfterInventory = data.StorageAfterInventory,
                            StorageDifference = data.StorageAfterInventory - data.CurrentStorage,
                            Memo = data.Memo,
                            DealerPrice = data.DealerPrice
                        });
                    }
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            this.ErrorFileName = errorFileName;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(ErrorFileName));
        }
        protected string ErrorFileName {
            get;
            set;
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage("请关闭选择的文件后再进行导入");
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }
        #endregion

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_ImportDealerPartsInventoryBill;
            }
        }

        private void CreateUI() {
            var detail = new DcsDetailDataEditView();
            detail.UnregisterButton(detail.InsertButton);
            detail.Register(PartsStockingUIStrings.DataEditView_Title_ImportDealerPartsInventoryBillDetails, null, () => this.DealerPartsInventoryBillForReportImportDataGridView);
            detail.SetValue(Grid.RowProperty, 2);
            this.LayoutRoot.Children.Add(detail);
            var dealerPartsInventoryBi = this.DataContext as DealerPartsInventoryBill;
            if(dealerPartsInventoryBi == null)
                return;
            var domainContext = new DcsDomainContext();

            domainContext.Load(domainContext.GetPartsSalesCategoriesByDealerServiceInfoDealerIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.KvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
                var branchId = loadOp.Entities.Select(r => r.BranchId).FirstOrDefault();
                dealerPartsInventoryBi.BranchId = branchId;
                //dealerPartsInventoryBi.SalesCategoryId = this.KvPartsSalesCategoryName.First().Key;
            }, null);
            this.ShowSaveButton();
        }

        protected override void OnEditSubmitting() {
            if(!this.DealerPartsInventoryBillForReportImportDataGridView.CommitEdit())
                return;
            var dealerPartsInventoryBi = this.DataContext as DealerPartsInventoryBill;
            if(dealerPartsInventoryBi == null)
                return;
            if(!dealerPartsInventoryBi.DealerPartsInventoryDetails.Any())
                return;
            dealerPartsInventoryBi.ValidationErrors.Clear();
            if(dealerPartsInventoryBi.SalesCategoryId == default(int)) {
                dealerPartsInventoryBi.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_CompanyTransferOrder_PartsSalesCategoryIdIsNull, new[] { "SalesCategoryId" }));
            }
            foreach(var dealerPartsInventoryDetail in dealerPartsInventoryBi.DealerPartsInventoryDetails) {
                if(dealerPartsInventoryDetail.StorageAfterInventory < 0)
                    dealerPartsInventoryBi.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_DealerPartsInventoryBill_StorageAfterInventoryGreaterthanZero, new[] {
                    "StorageAfterInventory"
                }));
            }


            if(dealerPartsInventoryBi.HasValidationErrors) {
                foreach(var err in dealerPartsInventoryBi.ValidationErrors) {
                    UIHelper.ShowAlertMessage(err.ErrorMessage);
                }
                return;
            }
            var spareids = dealerPartsInventoryBi.DealerPartsInventoryDetails.Select(r => r.SparePartId).ToArray();
            this.DomainContext.校验盘点单重复盘点(spareids, dealerPartsInventoryBi.StorageCompanyId, dealerPartsInventoryBi.SalesCategoryId, invokeOp => {
                if(invokeOp.HasError) {
                    return;
                }
                if(invokeOp.Value) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Title_DealerPartsInventoryBillError);
                    return;
                }
                ((IEditableObject)dealerPartsInventoryBi).EndEdit();
                base.OnEditSubmitting();
            }, null);

        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = "配件图号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "配件名称"
                                            },
                                            new ImportTemplateColumn {
                                                Name = "盘点后库存",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "备注",
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        public ICommand ImportCommand {
            get {
                return this.importCommand ?? (this.importCommand = new Core.Command.DelegateCommand(() => {
                    var dealerPartsInventoryBill = this.DataContext as DealerPartsInventoryBill;
                    if(dealerPartsInventoryBill == null)
                        return;
                    if(dealerPartsInventoryBill.SalesCategoryId == default(int)) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_CompanyTransferOrder_PartsSalesCategoryIdIsNull);
                        return;
                    }
                    this.Uploader.ShowFileDialog();
                }));
            }
        }

        private void getBranchId(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var domainContext = new DcsDomainContext();
            var dealerPartsInventoryBi = this.DataContext as DealerPartsInventoryBill;
            if(dealerPartsInventoryBi == null)
                return;
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.Id == dealerPartsInventoryBi.SalesCategoryId), loadOp => {
                if(loadOp.HasError)
                    return;
                var branchId = loadOp.Entities.Select(r => r.BranchId).FirstOrDefault();
                dealerPartsInventoryBi.BranchId = branchId;
            }, null);
        }
    }
}
