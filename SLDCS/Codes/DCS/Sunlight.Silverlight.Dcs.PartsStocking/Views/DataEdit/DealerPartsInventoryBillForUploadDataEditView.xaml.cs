﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit
{
    public partial class DealerPartsInventoryBillForUploadDataEditView 
    {
        public DealerPartsInventoryBillForUploadDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI()
        {
            this.Attachment.Children.Add(FileUploadDataEditPanels);
        }
        private void LoadEntityToEdit(int id)
        {
            this.DomainContext.Load(this.DomainContext.GetDealerPartsInventoryBillByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null)
                {
                    this.FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private PartsShiftOrderForUploadDataEditPanel fileUploadDataEditPanels;
        protected PartsShiftOrderForUploadDataEditPanel FileUploadDataEditPanels
        {
            get
            {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsShiftOrderForUploadDataEditPanel)DI.GetDataEditPanel("PartsShiftOrderForUpload"));
            }
        }
        protected override string Title
        {
            get
            {
                return PartsStockingUIStrings.Action_Title_UploadFile;
            }
        }
        protected override void OnEditSubmitting()
        {
            var dealerPartsInventoryBill = this.DataContext as DealerPartsInventoryBill;
            if (dealerPartsInventoryBill == null)
                return;
            dealerPartsInventoryBill.ValidationErrors.Clear();
            dealerPartsInventoryBill.Path = FileUploadDataEditPanels.FilePath;
            ((IEditableObject)dealerPartsInventoryBill).EndEdit();
            try
            {
                if (dealerPartsInventoryBill.Can上传服务站盘点单附件)
                    dealerPartsInventoryBill.上传服务站盘点单附件();
            }
            catch (Exception ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override bool OnRequestCanSubmit()
        {
            return true;
        }
    }
}
