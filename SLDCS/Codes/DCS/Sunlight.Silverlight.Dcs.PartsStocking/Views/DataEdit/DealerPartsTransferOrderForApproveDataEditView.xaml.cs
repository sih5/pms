﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class DealerPartsTransferOrderForApproveDataEditView {
        private DataGridViewBase dataGridView;

        public DealerPartsTransferOrderForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_ApproveDealerPartsTransferOrder;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealerPartsTransferOrderByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        private void CreateUI() {
            var detail = new DcsDetailDataEditView();
            detail.Register(PartsStockingUIStrings.DataEditPanel_Title_DealerPartsTransOrderDetail, null, () => this.DataGridView);
            detail.SetValue(Grid.ColumnProperty, 2);
            detail.SetValue(Grid.RowSpanProperty, 10);
            this.LayoutRoot.Children.Add(detail);
        }

        protected override void OnEditSubmitting() {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;

            if(dealerPartsTransferOrder.DealerPartsTransOrderDetails.GroupBy(r => new {
                r.SparePartId
            }).Select(r => new {
                r.Key.SparePartId,
                count = r.Count()
            }).Any(r => r.count > 1)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_HasRepeatPart);
                return;
            }

            if(string.IsNullOrWhiteSpace(dealerPartsTransferOrder.CheckOpinion)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ApproveOpinionIsNull);
                return;
            }

            foreach(var dealerPartsTransOrderDetail in dealerPartsTransferOrder.DealerPartsTransOrderDetails) {
                if(dealerPartsTransOrderDetail.Amount <= 0 || dealerPartsTransOrderDetail.Amount > dealerPartsTransOrderDetail.Stock) {
                    UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_TransferQuantityError, dealerPartsTransOrderDetail.SparePartName));
                    return;
                }
            }
            ((IEditableObject)dealerPartsTransferOrder).EndEdit();
            try {
                if(dealerPartsTransferOrder.Can审核服务站配件调拨单)
                    dealerPartsTransferOrder.审核服务站配件调拨单();
                base.OnEditSubmitting();
            } catch(ValidationException ex) {
                UIHelper.ShowNotification(ex.Message);
            }
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("DealerPartsTransOrderDetailForDetail");
                    this.dataGridView.DomainContext = this.DomainContext;
                }
                return this.dataGridView;
            }
        }
    }
}