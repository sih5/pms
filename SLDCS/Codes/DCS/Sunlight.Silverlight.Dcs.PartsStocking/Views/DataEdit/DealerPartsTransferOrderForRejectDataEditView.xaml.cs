﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class DealerPartsTransferOrderForRejectDataEditView {
        private DataGridViewBase dataGridView;

        public DealerPartsTransferOrderForRejectDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Validation_RejectDealerPartsTransferOrder;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealerPartsTransferOrderByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        private void CreateUI() {
            var detail = new DcsDetailDataEditView();
            detail.Register(PartsStockingUIStrings.DataEditPanel_Title_DealerPartsTransOrderDetail, null, () => this.DataGridView);
            detail.SetValue(Grid.ColumnProperty, 2);
            detail.SetValue(Grid.RowSpanProperty, 10);
            this.LayoutRoot.Children.Add(detail);
        }

        protected override void OnEditSubmitting() {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            try {
                if(dealerPartsTransferOrder.Can驳回服务站配件调拨单)
                    dealerPartsTransferOrder.驳回服务站配件调拨单();
                base.OnEditSubmitting();
            } catch(ValidationException ex) {
                UIHelper.ShowNotification(ex.Message);
            }
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("DealerPartsTransOrderDetailForDetail");
                    this.dataGridView.DomainContext = this.DomainContext;
                }
                return this.dataGridView;
            }
        }
    }
}