﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using System.Linq;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class DealerPartsTransferOrderForReportDataEditView {
        private string strFileName;
        private RadUpload uploader;

        private int branchId, warrantyBrandId, noWarrantyBrandId = 0;
        private DataGridViewBase dataGridView;
        private ObservableCollection<Branch> kvBranch;
        private ObservableCollection<PartsSalesCategory> kvNoWarrantyBrand;
        private ObservableCollection<PartsSalesCategory> kvWarrantyBrandName;
        public ObservableCollection<PartsSalesCategory> kvPartsSalesCategory;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public DealerPartsTransferOrderForReportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += DealerPartsTransferOrderForReportDataEditView_DataContextChanged;
            this.Loaded += DealerPartsTransferOrderForReportDataEditView_Loaded;
        }

        private void DealerPartsTransferOrderForReportDataEditView_Loaded(object sender, RoutedEventArgs e) {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            if(this.KvBranch.Count() == 1 && dealerPartsTransferOrder.BranchId == default(int)) {
                dealerPartsTransferOrder.BranchId = KvBranch.First().Id;
                dealerPartsTransferOrder.BranchIdName = KvBranch.First().Name;
                dealerPartsTransferOrder.BranchIdCode = KvBranch.First().Code;
                this.RadComboBoxBranch.IsEditable = true;
            }
            this.RadComboBoxBranch.IsEditable = false;
        }

        private void DealerPartsTransferOrderForReportDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            dealerPartsTransferOrder.PropertyChanged -= dealerPartsTransferOrder_PropertyChanged;
            dealerPartsTransferOrder.PropertyChanged += dealerPartsTransferOrder_PropertyChanged;
        }

        private void dealerPartsTransferOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            switch(e.PropertyName) {
                case "BranchId":
                    RadComboBoxChanged(e.PropertyName, PartsStockingUIStrings.DataEditView_Title_ClearOnBranchChange);
                    break;
                case "WarrantyBrandId":
                    RadComboBoxChanged(e.PropertyName, PartsStockingUIStrings.DataEditView_Title_ClearOnWarrantyBrandChange);
                    break;
                case "NoWarrantyBrandId":
                    RadComboBoxChanged(e.PropertyName, PartsStockingUIStrings.DataEditView_Title_ClearOnNoWarrantyBrandChange);
                    break;
            }
        }

        private void RadComboBoxChanged(string propertyName, string text) {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            if(dealerPartsTransferOrder.BranchId == branchId && dealerPartsTransferOrder.WarrantyBrandId == warrantyBrandId && dealerPartsTransferOrder.NoWarrantyBrandId == noWarrantyBrandId)
                return;
            if(dealerPartsTransferOrder.DealerPartsTransOrderDetails.Any()) {
                DcsUtils.Confirm(text, () => {
                    switch(propertyName) {
                        case "BranchId":
                            dealerPartsTransferOrder.WarrantyBrandId = 0;
                            dealerPartsTransferOrder.WarrantyBrandCode = String.Empty;
                            dealerPartsTransferOrder.WarrantyBrandName = String.Empty;
                            dealerPartsTransferOrder.NoWarrantyBrandId = 0;
                            dealerPartsTransferOrder.NoWarrantyBrandCode = String.Empty;
                            dealerPartsTransferOrder.NoWarrantyBrandName = String.Empty;
                            branchId = dealerPartsTransferOrder.WarrantyBrandId;
                            this.KvWarrantyBrandName.Clear();
                            this.KvNoWarrantyBrand.Clear();
                            foreach(var item in KvPartsSalesCategory) {
                                if(item.IsNotWarranty) {
                                    this.KvNoWarrantyBrand.Add(item);
                                } else {
                                    this.KvWarrantyBrandName.Add(item);
                                }
                            }
                            break;
                        case "WarrantyBrandId":
                            warrantyBrandId = dealerPartsTransferOrder.WarrantyBrandId;
                            break;
                        case "NoWarrantyBrandId":
                            noWarrantyBrandId = dealerPartsTransferOrder.NoWarrantyBrandId;
                            break;
                    }
                    SetProperty(propertyName);
                    var dealerPartsTransOrderDetails = dealerPartsTransferOrder.DealerPartsTransOrderDetails.ToArray();
                    foreach(var dealerPartsTransOrderDetail in dealerPartsTransOrderDetails) {
                        dealerPartsTransferOrder.DealerPartsTransOrderDetails.Remove(dealerPartsTransOrderDetail);
                    }
                }, () => {
                    switch(propertyName) {
                        case "BranchId":
                            dealerPartsTransferOrder.WarrantyBrandId = branchId;
                            break;
                        case "WarrantyBrandId":
                            dealerPartsTransferOrder.WarrantyBrandId = warrantyBrandId;
                            break;
                        case "NoWarrantyBrandId":
                            dealerPartsTransferOrder.NoWarrantyBrandId = noWarrantyBrandId;
                            break;
                    }
                    SetProperty(propertyName);
                });
            } else {
                SetProperty(propertyName);
                if(propertyName == "BranchId") {
                    this.KvWarrantyBrandName.Clear();
                    this.KvNoWarrantyBrand.Clear();
                    foreach(var item in KvPartsSalesCategory) {
                        if(item.IsNotWarranty) {
                            this.KvNoWarrantyBrand.Add(item);
                        } else {
                            this.KvWarrantyBrandName.Add(item);
                        }
                    }
                }
            }
        }

        private void SetProperty(string propertyName) {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            switch(propertyName) {
                case "BranchId":
                    var branch = this.KvBranch.FirstOrDefault(r => r.Id == dealerPartsTransferOrder.BranchId);
                    if(branch != null) {
                        dealerPartsTransferOrder.BranchIdName = branch.Name;
                        dealerPartsTransferOrder.BranchIdCode = branch.Code;
                    }
                    break;
                case "WarrantyBrandId":
                    var warrantyBrand = this.KvWarrantyBrandName.FirstOrDefault(r => r.Id == dealerPartsTransferOrder.WarrantyBrandId);
                    if(warrantyBrand != null) {
                        dealerPartsTransferOrder.WarrantyBrandName = warrantyBrand.Name;
                        dealerPartsTransferOrder.WarrantyBrandCode = warrantyBrand.Code;
                    }
                    break;
                case "NoWarrantyBrandId":
                    var noWarrantyBrand = this.KvNoWarrantyBrand.FirstOrDefault(r => r.Id == dealerPartsTransferOrder.NoWarrantyBrandId);
                    if(noWarrantyBrand != null) {
                        dealerPartsTransferOrder.NoWarrantyBrandName = noWarrantyBrand.Name;
                        dealerPartsTransferOrder.NoWarrantyBrandCode = noWarrantyBrand.Code;
                    }
                    break;
            }
        }
        protected override void Reset() {
            branchId = default(int);
            warrantyBrandId = default(int);
            noWarrantyBrandId = default(int);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DealerPartsTransferOrder;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealerPartsTransferOrderByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                branchId = entity.BranchId;
                warrantyBrandId = entity.WarrantyBrandId;
                noWarrantyBrandId = entity.NoWarrantyBrandId;
                this.SetObjectToEdit(entity);
            }, null);
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("DealerPartsTransOrderDetailForEdit");
                    this.dataGridView.DomainContext = this.DomainContext;
                }
                return this.dataGridView;
            }
        }

        private void CreateUI() {
            this.RadComboBoxBranch.ItemsSource = this.KvBranch;
            this.RadComboBoxNoWarrantyBrand.ItemsSource = this.KvNoWarrantyBrand;
            this.RadComboBoxWarrantyBrand.ItemsSource = this.KvWarrantyBrandName;
            var querWindow = DI.GetQueryWindow("RepairOrderForLackPart");
            querWindow.Loaded += querWindow_Loaded;
            querWindow.SelectionDecided += querWindow_SelectionDecided;
            this.ptSource.PopupContent = querWindow;
            var detail = new DcsDetailDataEditView();
            detail.Register(PartsStockingUIStrings.DataEditPanel_Title_DealerPartsTransOrderDetail, null, () => this.DataGridView);
            detail.SetValue(Grid.ColumnProperty, 2);
            detail.SetValue(Grid.RowSpanProperty, 9);
            detail.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Title_ExportTemplate,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/export.png", UriKind.Relative),
                Command = new DelegateCommand(this.Export)
            });
            detail.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/import.png", UriKind.Relative),
                Command = new DelegateCommand(this.Import)
            });
            this.LayoutRoot.Children.Add(detail);
            this.DomainContext.Load(DomainContext.GetBranchByDealerServiceInfoQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var item in loadOp.Entities) {
                    this.KvBranch.Add(item);
                }
            }, null);
            this.DomainContext.Load(DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var item in loadOp.Entities) {
                    this.KvPartsSalesCategory.Add(item);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void querWindow_SelectionDecided(object sender, EventArgs e) {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var repairOrder = queryWindow.SelectedEntities.Cast<RepairOrder>().FirstOrDefault();
            if(repairOrder == null)
                return;
            //dealerPartsTransferOrder.SourceCode = repairOrder.Code;
            //dealerPartsTransferOrder.SourceId = repairOrder.Id;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void querWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null)
                return;
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            if(dealerPartsTransferOrder.BranchId == default(int)) {
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Title_BranchIdIsNull);
                return;
            }

            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new object[] {
                new FilterItem("BranchId", typeof(int), FilterOperator.IsEqualTo, dealerPartsTransferOrder.BranchId)
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "PartsSalesCategoryName", dealerPartsTransferOrder.WarrantyBrandName
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "PartsSalesCategoryName", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Status", false
            });
        }
        private void Import() {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            if(dealerPartsTransferOrder.BranchId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Title_BranchIdIsNull);
                return;
            }
            if(dealerPartsTransferOrder.NoWarrantyBrandId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Title_NoWarrantyBrandIdIsNull);
                return;
            }
            if(dealerPartsTransferOrder.WarrantyBrandId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Title_WarrantyBrandIdIsNull);
                return;
            }
            this.Uploader.ShowFileDialog();
        }
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
                        if(dealerPartsTransferOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportDealerPartsTransferOrderDetailAsync(e.HandlerData.CustomData["Path"].ToString(), dealerPartsTransferOrder.NoWarrantyBrandId, dealerPartsTransferOrder.WarrantyBrandId, dealerPartsTransferOrder.BranchId);
                        this.excelServiceClient.ImportDealerPartsTransferOrderDetailCompleted -= this.ExcelServiceClient_ImportDealerPartsTransferOrderDetailCompleted;
                        this.excelServiceClient.ImportDealerPartsTransferOrderDetailCompleted += this.ExcelServiceClient_ImportDealerPartsTransferOrderDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.DataGridView.CommitEdit())
                return;
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            if(!dealerPartsTransferOrder.DealerPartsTransOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrderDetailsInNull);
                return;
            }
            if(dealerPartsTransferOrder.DealerPartsTransOrderDetails.GroupBy(r => new {
                r.SparePartId
            }).Select(r => new {
                r.Key.SparePartId,
                count = r.Count()
            }).Any(r => r.count > 1)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_HasRepeatPart);
                return;
            }
            foreach(var dealerPartsTransOrderDetail in dealerPartsTransferOrder.DealerPartsTransOrderDetails) {
                if(dealerPartsTransOrderDetail.Amount <= 0 || dealerPartsTransOrderDetail.Amount > dealerPartsTransOrderDetail.Stock) {
                    UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_TransferQuantityError, dealerPartsTransOrderDetail.SparePartName));
                    return;
                }
            }
            if(dealerPartsTransferOrder.DealerPartsTransOrderDetails.Any(r => r.WarrantyPrice == default(int) || r.NoWarrantyPrice == default(int))) {
                DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Validation_PartNoSalePrice, () => {
                    ((IEditableObject)dealerPartsTransferOrder).EndEdit();
                    try {
                        if(this.EditState == DataEditState.Edit) {
                            if(dealerPartsTransferOrder.Can更新服务站配件调拨单)
                                dealerPartsTransferOrder.更新服务站配件调拨单();
                        }
                        base.OnEditSubmitting();
                    } catch(ValidationException ex) {
                        UIHelper.ShowNotification(ex.Message);

                    }
                });
            } else {
                ((IEditableObject)dealerPartsTransferOrder).EndEdit();
                try {
                    if(this.EditState == DataEditState.Edit) {
                        if(dealerPartsTransferOrder.Can更新服务站配件调拨单)
                            dealerPartsTransferOrder.更新服务站配件调拨单();
                    }
                    base.OnEditSubmitting();
                } catch(ValidationException ex) {
                    UIHelper.ShowNotification(ex.Message);
                }
            }
        }

        private void ExcelServiceClient_ImportDealerPartsTransferOrderDetailCompleted(object sender, ImportDealerPartsTransferOrderDetailCompletedEventArgs e) {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            var detailList = dealerPartsTransferOrder.DealerPartsTransOrderDetails.ToList();
            foreach(var detail in detailList) {
                dealerPartsTransferOrder.DealerPartsTransOrderDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    dealerPartsTransferOrder.DealerPartsTransOrderDetails.Add(new DealerPartsTransOrderDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        Amount = data.Amount,
                        WarrantyPrice = data.WarrantyPrice,
                        NoWarrantyPrice = data.NoWarrantyPrice,
                        Stock = data.Quantity,
                        DiffPrice = (data.NoWarrantyPrice ?? 0) - (data.WarrantyPrice ?? 0),
                        SumWarrantyPrice = (data.WarrantyPrice ?? 0) * data.Amount,
                        SumNoWarrantyPrice = (data.NoWarrantyPrice ?? 0) * data.Amount,
                    });
                }
            }
            dealerPartsTransferOrder.DiffPrice = dealerPartsTransferOrder.DealerPartsTransOrderDetails.Sum(r => r.DiffPrice);
            dealerPartsTransferOrder.SumWarrantyPrice = dealerPartsTransferOrder.DealerPartsTransOrderDetails.Sum(r => r.SumWarrantyPrice);
            dealerPartsTransferOrder.SumNoWarrantyPrice = dealerPartsTransferOrder.DealerPartsTransOrderDetails.Sum(r => r.SumNoWarrantyPrice);
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsStockingUIStrings.Upload_Error3);
                    e.Handled = true;
                }
            }
        }
        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }
        private void Export() {
            var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_QuantityNumber,
                                                IsRequired = true
                                            }
                                        };
            this.DomainContext.GetImportTemplateFileWithFormat("服务站配件调拨单清单导入模板.xlsx", columnItemsValues, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                }
                if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
            }, null);
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        public ObservableCollection<Branch> KvBranch {
            get {
                return this.kvBranch ?? (this.kvBranch = new ObservableCollection<Branch>());
            }
        }
      
        public ObservableCollection<PartsSalesCategory> KvPartsSalesCategory {
            get {
                return this.kvPartsSalesCategory ?? (this.kvPartsSalesCategory = new ObservableCollection<PartsSalesCategory>());
            }
        }
        public ObservableCollection<PartsSalesCategory> KvNoWarrantyBrand {
            get {
                return this.kvNoWarrantyBrand ?? (this.kvNoWarrantyBrand = new ObservableCollection<PartsSalesCategory>());
            }
        }

        public ObservableCollection<PartsSalesCategory> KvWarrantyBrandName {
            get {
                return this.kvWarrantyBrandName ?? (this.kvWarrantyBrandName = new ObservableCollection<PartsSalesCategory>());
            }
        }
    }
}