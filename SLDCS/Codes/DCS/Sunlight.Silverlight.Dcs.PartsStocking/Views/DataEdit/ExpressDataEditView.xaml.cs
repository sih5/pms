﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class ExpressDataEditView  {
        public ExpressDataEditView() {
            this.InitializeComponent();
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetExpressesQuery().Where(r=>r.Id==id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.txtexpressCode.IsReadOnly = true;
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var express = this.DataContext as Express;
            if(express == null)
                return;
            if(string.IsNullOrEmpty(express.ExpressCode)) {
                UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_ExpressCodeNotNull));
                return;
            }
            if(string.IsNullOrEmpty(express.ExpressName)) {
                UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_ExpressNameNotNull));
                return;
            }
            if(!string.IsNullOrEmpty(express.E_Mail)) {
                string strEmail = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                Regex regexstEmail = new Regex(strEmail);
                if(regexstEmail.IsMatch(express.E_Mail) == false) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_EmaiError);
                    return;
                }
            }
            ((IEditableObject)express).EndEdit();
            base.OnEditSubmitting();
            this.txtexpressCode.IsReadOnly = false;
        }

        protected override void OnEditCancelled() {
            this.txtexpressCode.IsReadOnly = false;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        
    }
}
