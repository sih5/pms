﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class ExpressToLogisticsDataEditView {
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvFocufingCore;
        public ExpressToLogisticsDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var item in this.KeyValueManager[kvNames[0]].Where(p => p.Value != "虚拟储运中心"))
                    KvFocufingCore.Add(item);
            });
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        private readonly string[] kvNames = new[] {
            "Storage_Center"
        };

        public ObservableCollection<KeyValuePair> KvFocufingCore {
            get {
                return this.kvFocufingCore ?? (this.kvFocufingCore = new ObservableCollection<KeyValuePair>());
            }
        }


        private void CreateUI() {
            var queryWindowExpress = DI.GetQueryWindow("Express");
            queryWindowExpress.SelectionDecided += this.QueryWindowExpress_SelectionDecided;
            queryWindowExpress.Loaded += this.QueryWindowExpress_Loaded;
            this.ptExpress.PopupContent = queryWindowExpress;

            var queryWindowLogisticsCompany = DI.GetQueryWindow("LogisticsCompany");
            queryWindowLogisticsCompany.SelectionDecided += this.QueryWindowLogisticsCompany_SelectionDecided;
            queryWindowLogisticsCompany.Loaded += this.QueryWindowLogisticsCompany_Load;
            this.ptLogisticsCompany.PopupContent = queryWindowLogisticsCompany;
        }

        void QueryWindowExpress_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            var filterItem = new FilterItem();
            filterItem.MemberName = "Status";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = (int)DcsMasterDataStatus.有效;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
        }

        private void QueryWindowExpress_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var express = queryWindow.SelectedEntities.Cast<Express>().FirstOrDefault();
            if(express == null)
                return;
            var expressToLogistic = this.DataContext as ExpressToLogistic;
            if(expressToLogistic == null)
                return;
            expressToLogistic.ExpressId = express.Id;
            expressToLogistic.ExpressCode = express.ExpressCode;
            expressToLogistic.ExpressName = express.ExpressName;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        void QueryWindowLogisticsCompany_Load(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            var filterItem = new FilterItem();
            filterItem.MemberName = "Status";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = (int)DcsMasterDataStatus.有效;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
        }

        private void QueryWindowLogisticsCompany_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var logisticCompany = queryWindow.SelectedEntities.Cast<LogisticCompany>().FirstOrDefault();
            if(logisticCompany == null)
                return;
            var expressToLogistic = this.DataContext as ExpressToLogistic;
            if(expressToLogistic == null)
                return;
            expressToLogistic.LogisticsCompanyId = logisticCompany.Id;
            expressToLogistic.LogisticsCompanyCode = logisticCompany.Code;
            expressToLogistic.LogisticsCompanyName = logisticCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void OnEditSubmitting() {
            var expressToLogistic = this.DataContext as ExpressToLogistic;
            if(expressToLogistic == null)
                return;
            if(string.IsNullOrEmpty(expressToLogistic.FocufingCoreName)) {
                UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_FocufingCoreNameNotNull));
                return;
            }
            if(string.IsNullOrEmpty(expressToLogistic.ExpressCode)) {
                UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_ExpressCodeNotNull));
                return;
            }
            if(string.IsNullOrEmpty(expressToLogistic.ExpressName)) {
                UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_ExpressCodeNotNull));
                return;
            }
            if(string.IsNullOrEmpty(expressToLogistic.LogisticsCompanyCode)) {
                UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_LogisticsCompanyCodeNotNull));
                return;
            }
            if(string.IsNullOrEmpty(expressToLogistic.LogisticsCompanyName)) {
                UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_LogisticsCompanyNameNotNull));
                return;
            }

            ((IEditableObject)expressToLogistic).EndEdit();
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetExpressToLogisticsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var expressToLogistic = this.DataContext as ExpressToLogistic;
            if(expressToLogistic == null)
                return;
            var focufingCore = e.AddedItems.Cast<KeyValuePair>().FirstOrDefault();
            if(focufingCore == null)
                return;
            if(expressToLogistic.FocufingCoreName == "北京CDC") {
                expressToLogistic.FocufingCoreCode = "BJ-A";
            } else if(expressToLogistic.FocufingCoreName == "山东CDC") {
                expressToLogistic.FocufingCoreCode = "SD";
            } else if(expressToLogistic.FocufingCoreName == "广东CDC") {
                expressToLogistic.FocufingCoreCode = "GD";
            } else if(expressToLogistic.FocufingCoreName == "河南CDC") {
                expressToLogistic.FocufingCoreCode = "HN";
            }
        }
    }
}
