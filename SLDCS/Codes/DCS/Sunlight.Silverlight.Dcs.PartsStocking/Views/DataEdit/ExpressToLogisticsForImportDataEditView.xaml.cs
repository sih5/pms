﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class ExpressToLogisticsForImportDataEditView {
         private DataGridViewBase expressForImportForEdit;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出快递物流关系.xlsx";
        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_ImportExpressToLogistics;
            }
        }
        public ExpressToLogisticsForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(CreateUI);
        }
        protected DataGridViewBase ImportDataGridView {
            get {
                return this.expressForImportForEdit ?? (this.expressForImportForEdit = DI.GetDataGridView("ExpressToLogisticsForImportForEditForEdit"));
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = PartsStockingUIStrings.DataEditView_Title_ImportExpressToLogisticDetails,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }
        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.快递物流关系导入Async(fileName);
            this.ExcelServiceClient.快递物流关系导入Completed -= ExcelServiceClient_快递物流关系导入Completed;
            this.ExcelServiceClient.快递物流关系导入Completed += ExcelServiceClient_快递物流关系导入Completed;
        }

        private void ExcelServiceClient_快递物流关系导入Completed(object sender, 快递物流关系导入CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!HasImportingError) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_CompletedImport, 10);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }
        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = "快递公司编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "储运中心",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "物流公司编号"
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
