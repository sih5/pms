﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class HauDistanceInforDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private string companyName;
        private ObservableCollection<KeyValuePair> kvWarehouses, kvCompanyAddress;
        public HauDistanceInforDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return PartsStockingUIStrings.BusinessName_HauDistanceInfor;
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvCompanyAddress {
            get {
                return this.kvCompanyAddress ?? (this.kvCompanyAddress = new ObservableCollection<KeyValuePair>());
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetWarehousesOrderByNameQuery().Where(w => w.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && w.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehouse in loadOp.Entities)
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
            }, null);
            var queryWindowHauDistanceInfor = DI.GetQueryWindow("Company");
            queryWindowHauDistanceInfor.SelectionDecided += this.QueryWindowHauDistanceInfor_SelectionDecided;
            this.ptCompanyName.PopupContent = queryWindowHauDistanceInfor;
        }

        public string CompanyName {
            get {
                return this.companyName;
            }
            set {
                this.companyName = value;
                this.OnPropertyChanged("CompanyName");
            }
        }

        public string StorageCompanyName {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseName;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetHauDistanceInforWithDetailsQuery().Where(e => e.Id == id && e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                this.CompanyName = entity.Company.Name;
                if(entity != null)
                    companyNameChange(entity.Company, entity);
                this.SetObjectToEdit(entity);
            }, null);
        }

        private void QueryWindowHauDistanceInfor_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var company = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if(company == null)
                return;
            var hauDistanceInfor = this.DataContext as HauDistanceInfor;
            if(hauDistanceInfor == null)
                return;
            if(this.radComboBoxCompanyAddress.SelectedValue != null)
                DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_HauDistanceInfor_Update, () => {
                    companyNameChange(company, hauDistanceInfor);
                });
            else {
                companyNameChange(company, hauDistanceInfor);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void companyNameChange(Company company, HauDistanceInfor hauDistanceInfor) {
            this.radComboBoxCompanyAddress.Text = string.Empty;
            hauDistanceInfor.CompanyId = company.Id;
            this.CompanyName = company.Name;
            this.DomainContext.Load(this.DomainContext.GetCompanyAddressesQuery().Where(s => s.Status == (int)DcsBaseDataStatus.有效 && s.CompanyId == company.Id), loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvCompanyAddress.Clear();
                foreach(var companyAddress in loadOp.Entities)
                    this.KvCompanyAddress.Add(new KeyValuePair {
                        Key = companyAddress.Id,
                        Value = companyAddress.DetailAddress,
                        UserObject = companyAddress
                    });
            }, null);
        }

        protected override void OnEditCancelled() {
            this.CompanyName = string.Empty;
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            this.CompanyName = string.Empty;
            base.OnEditSubmitted();
        }

        protected override void OnEditSubmitting() {
            var hauDistanceInfor = this.DataContext as HauDistanceInfor;
            if(hauDistanceInfor == null)
                return;
            hauDistanceInfor.ValidationErrors.Clear();
            if(hauDistanceInfor.CompanyId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_HauDistanceInfor_CompanyIdIsNull);
                return;
            }
            if(hauDistanceInfor.CompanyAddressId == null || hauDistanceInfor.CompanyAddressId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_HauDistanceInfor_CompanyAddressId);
                return;
            }
            if(hauDistanceInfor.WarehouseId == default(int))
                hauDistanceInfor.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_HauDistanceInfor_WarehouseNameIsNull, new[] {
                    "WarehouseId"
                }));
            if(hauDistanceInfor.HauDistance <= 0) {
                hauDistanceInfor.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_HauDistanceInfor_HauDistance, new[] {
                    "HauDistance"
                }));
                return;
            }

            if(hauDistanceInfor.ValidationErrors.Any())
                return;
            if(hauDistanceInfor.StorageCompanyId == hauDistanceInfor.CompanyId) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_NotEquals);
                return;
            }
            ((IEditableObject)hauDistanceInfor).EndEdit();
            base.OnEditSubmitting();
        }
    }
}
