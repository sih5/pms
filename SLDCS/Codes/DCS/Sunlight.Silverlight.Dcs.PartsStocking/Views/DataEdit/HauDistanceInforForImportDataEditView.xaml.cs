﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class HauDistanceInforForImportDataEditView {
        private DataGridViewBase partsSupplierRelationForImportDataGridView;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出运距信息模板.xlsx";

        public HauDistanceInforForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return "导入运距信息";
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.partsSupplierRelationForImportDataGridView == null) {
                    this.partsSupplierRelationForImportDataGridView = DI.GetDataGridView("HauDistanceInforForImport");
                    this.partsSupplierRelationForImportDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsSupplierRelationForImportDataGridView;
            }
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = "运距信息导入清单",
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        private void UploadFileProcessing(string fileName) {
            this.ExcelServiceClient.ImportHauDistanceInforAsync(fileName);
            this.ExcelServiceClient.ImportHauDistanceInforCompleted -= ExcelServiceClient_ImportHauDistanceInforCompleted;
            this.ExcelServiceClient.ImportHauDistanceInforCompleted += ExcelServiceClient_ImportHauDistanceInforCompleted;
        }

        private void ExcelServiceClient_ImportHauDistanceInforCompleted(object sender, ImportHauDistanceInforCompletedEventArgs e) {
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification("导入成功", 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification("导入完成，但部分数据有问题", 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                    new ImportTemplateColumn {
                        Name = "仓储企业编号",
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = "仓储企业名称",
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = "仓库名称",
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = "客户企业编号",
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = "客户企业名称",
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = "收货地址",
                        IsRequired = true
                    },
                    new ImportTemplateColumn {
                        Name = "运距",
                        IsRequired = true
                    }
                };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
