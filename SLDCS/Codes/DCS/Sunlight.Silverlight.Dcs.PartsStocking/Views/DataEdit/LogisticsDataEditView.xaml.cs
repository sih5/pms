﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class LogisticsDataEditView {
        private readonly string[] kvNames = {
            "PartsShipping_Method","PartsSalesOrder_Status"
        };
        private KeyValueManager keyValueManager;
        private DataGridViewBase logisticsForEditDataGridView;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public LogisticsDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsStockingUIStrings.DataEditPanel_Title_LogisticsDetail, null, () => this.LogisticsForEditDataGridView);
            dcsDetailGridView.SetValue(Grid.ColumnProperty, 2);

            this.Root.Children.Add(dcsDetailGridView);
            this.KeyValueManager.LoadData();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        private DataGridViewBase LogisticsForEditDataGridView {
            get {
                if(this.logisticsForEditDataGridView == null) {
                    this.logisticsForEditDataGridView = DI.GetDataGridView("LogisticsForEdit");
                    this.logisticsForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.logisticsForEditDataGridView;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.查询物流信息Query().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.DomainContext.Load(this.DomainContext.GetPartsSalesOrdersQuery().Where(r => r.Code == entity.OrderCode), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    var order = loadOp1.Entities.SingleOrDefault();
                    if(order != null) {
                        entity.PartsSalesOrderStatus = order.Status;
                    }
                    int SequeueNumber = 1;
                    foreach(var logistics in entity.LogisticsDetails)
                        logistics.Number = SequeueNumber++;
                    this.SetObjectToEdit(entity);
                }, null);

            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_Logistics;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.LogisticsForEditDataGridView.CommitEdit())
                return;

            var logistic = this.DataContext as Logistic;
            if(logistic == null || logistic.LogisticsDetails == null)
                return;
            if(logistic.LogisticsDetails.Any(r => string.IsNullOrWhiteSpace(r.PointName) || !r.PointTime.HasValue)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_TimeIsNull);
                return;
            }
            logistic.ValidationErrors.Clear();
            foreach(var logisticsDetail in logistic.LogisticsDetails)
                logisticsDetail.ValidationErrors.Clear();

            ((IEditableObject)logistic).EndEdit();
            base.OnEditSubmitting();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }


    }
}
