﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public class OverstockPartsAdjustBillDataEditView : OverstockPartsAdjustBillForCompleteDataEditView {
        protected override void OnEditSubmitting() {
            if(!this.OverstockPartsAdjustBillForDeatil.CommitEdit())
                return;
            var overstockPartsAdjustBill = this.DataContext as OverstockPartsAdjustBill;
            if(overstockPartsAdjustBill == null)
                return;
            if(overstockPartsAdjustBill.OverstockPartsAdjustDetails.Count <= 0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_DetailsAmountIsNull);
                return;
            }
            if(overstockPartsAdjustBill.OverstockPartsAdjustDetails != null)
                if(overstockPartsAdjustBill.OverstockPartsAdjustDetails.Any(r => r.Quantity < r.finishQuantity)) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_FinishQuantityError);
                    return;
                }
            ((IEditableObject)overstockPartsAdjustBill).EndEdit();
            try {
                if(this.EditState == DataEditState.Edit) {
                    if(overstockPartsAdjustBill.Can调入单位完成积压件调剂单)
                        overstockPartsAdjustBill.调入单位完成积压件调剂单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        private DataGridViewBase overstockPartsAdjustBillForDeatil;

        private DataGridViewBase OverstockPartsAdjustBillForDeatil {
            get {
                if(this.overstockPartsAdjustBillForDeatil == null) {
                    this.overstockPartsAdjustBillForDeatil = DI.GetDataGridView("OverstockPartsAdjustBillForCompleteDetail");
                    this.overstockPartsAdjustBillForDeatil.DomainContext = this.DomainContext;
                }
                return this.overstockPartsAdjustBillForDeatil;
            }
        }
        protected override void CreateUI() {
            var dcsDetailDataEditView = new DcsDetailDataEditView();
            dcsDetailDataEditView.Register(PartsStockingUIStrings.DetailPanel_GroupTitle_OverstockPartsAdjustDetail, null, () => this.OverstockPartsAdjustBillForDeatil);
            dcsDetailDataEditView.UnregisterButton(dcsDetailDataEditView.InsertButton);
            dcsDetailDataEditView.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(dcsDetailDataEditView);
        }
    }
}
