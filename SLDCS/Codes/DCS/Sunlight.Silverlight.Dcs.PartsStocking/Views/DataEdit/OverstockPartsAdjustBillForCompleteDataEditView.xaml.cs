﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class OverstockPartsAdjustBillForCompleteDataEditView {
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = new[] {
            "Company_Type"
        };
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvStorageCompanyTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        private DataGridViewBase overstockPartsAdjustBillForDeatil;

        private DataGridViewBase OverstockPartsAdjustBillForDeatil {
            get {
                if(this.overstockPartsAdjustBillForDeatil == null) {
                    this.overstockPartsAdjustBillForDeatil = DI.GetDataGridView("OverstockPartsAdjustBillForDetail");
                    this.overstockPartsAdjustBillForDeatil.DomainContext = this.DomainContext;
                }
                return this.overstockPartsAdjustBillForDeatil;
            }
        }

        protected virtual void CreateUI() {
            var dcsDetailDataEditView = new DcsDetailDataEditView();
            dcsDetailDataEditView.Register("积压件调拨单清单", null, () => this.OverstockPartsAdjustBillForDeatil);
            dcsDetailDataEditView.UnregisterButton(dcsDetailDataEditView.InsertButton);
            dcsDetailDataEditView.UnregisterButton(dcsDetailDataEditView.DeleteButton);
            dcsDetailDataEditView.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(dcsDetailDataEditView);
        }
        
        protected override void OnEditSubmitting() {
            if(!this.OverstockPartsAdjustBillForDeatil.CommitEdit())
                return;
            var overstockTransferOrder = this.DataContext as OverstockTransferOrder;
            if(overstockTransferOrder == null)
                return;
            if(overstockTransferOrder.OverstockTransferOrderDetails.Count <= 0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_DetailsAmountIsNull);
                return;
            }
            if(overstockTransferOrder.OverstockTransferOrderDetails.Any(r => r.TransferQty == null || r.TransferQty.Value < 0)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_DetailsAmountIsNull);
                return;
            }
            ((IEditableObject)overstockTransferOrder).EndEdit();

            if (this.DomainContext.IsBusy)
                return;
            this.DomainContext.修改积压件调拨单(overstockTransferOrder,overstockTransferOrder.OverstockTransferOrderDetails.ToArray(), invokeOp =>
            {
                if (invokeOp.HasError)
                {
                    if (!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    var error = invokeOp.ValidationErrors.First();
                    if (error != null)
                        UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                    else
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    return;
                }
                base.OnEditSubmitting();
            }, null);
            
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetOverstockTransferOrderWithDetailsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public OverstockPartsAdjustBillForCompleteDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return "提交积压件调拨单";
            }
        }

    }
}
