﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class OverstockPartsAdjustDetailForDetailDataEditView {
        private DataGridViewBase overstockPartsAdjustDetailKanBanDataGridView, overstockPartsAdjustBillDetailDataGridView;
        private ObservableCollection<object> overstockPartsAdjustDetails;
        private ObservableCollection<SparePartStatisticsForKanBan> sparePartStatisticsForKanBans;

        public OverstockPartsAdjustDetailForDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase OverstockPartsAdjustDetailKanBanDataGridView {
            get {
                if(this.overstockPartsAdjustDetailKanBanDataGridView == null) {
                    this.overstockPartsAdjustDetailKanBanDataGridView = DI.GetDataGridView("OverstockPartsAdjustDetailKanBan");
                    this.overstockPartsAdjustDetailKanBanDataGridView.DataContext = this;
                    this.overstockPartsAdjustDetailKanBanDataGridView.DomainContext = this.DomainContext;
                }
                return this.overstockPartsAdjustDetailKanBanDataGridView;
            }
        }

        private DataGridViewBase OverstockPartsAdjustBillDetailDataGridView {
            get {
                if(this.overstockPartsAdjustBillDetailDataGridView == null) {
                    this.overstockPartsAdjustBillDetailDataGridView = DI.GetDataGridView("OverstockPartsAdjustBillDetail");
                    this.overstockPartsAdjustBillDetailDataGridView.DataContext = this;
                    this.overstockPartsAdjustBillDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.overstockPartsAdjustBillDetailDataGridView;
            }
        }

        private void CreateUI() {
            var tabControls = new RadTabControl();
            tabControls.BackgroundVisibility = Visibility.Collapsed;
            tabControls.Items.Add(new RadTabItem {
                Header = PartsStockingUIStrings.DataEditView_GroupTitle_OverstockPartsAdjustBillDetail,
                Content = this.OverstockPartsAdjustBillDetailDataGridView
            });
            tabControls.Items.Add(new RadTabItem {
                Header = PartsStockingUIStrings.DataEditView_GroupTitle_OverstockPartsAdjustDetail,
                Content = this.OverstockPartsAdjustDetailKanBanDataGridView
            });
            this.LayoutRoot.Children.Add(tabControls);
        }

        private void LoadEntityToEdit(int id) {
            this.OverstockPartsAdjustDetails.Clear();
            this.SparePartStatisticsForKanBans.Clear();
            this.DomainContext.Load(this.DomainContext.查询调剂看板Query(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
                foreach(var detail in entity.OverstockPartsAdjustDetails) {
                    var sparePartStatistics = new SparePartStatisticsForKanBan {
                        SparePartId = detail.SparePartId,
                        SparePartCode = detail.SparePartCode,
                        SparePartName = detail.SparePartName,
                        PartsAdjustAmount = entity.OverstockPartsAdjustDetails.Where(e => e.SparePartId == detail.SparePartId).Sum(e => e.Quantity)
                    };
                    this.SparePartStatisticsForKanBans.Add(sparePartStatistics);
                    if(entity.PartsOutboundBills.Any())
                        sparePartStatistics.PartsOutboundAmount = entity.PartsOutboundBills.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单 && v.PartsOutboundBillDetails.Any(e => e.SparePartId == detail.SparePartId)).SelectMany(e => e.PartsOutboundBillDetails).Where(e => e.SparePartId == detail.SparePartId).Sum(e => e.OutboundAmount);
                    if(entity.PartsShippingOrders.Any())
                        sparePartStatistics.PartsShippingAmount = entity.PartsShippingOrders.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单 && v.PartsShippingOrderDetails.Any(e => e.SparePartId == detail.SparePartId)).SelectMany(e => e.PartsShippingOrderDetails).Where(e => e.SparePartId == detail.SparePartId).Sum(e => e.ShippingAmount);
                    if(entity.PartsInboundCheckBills.Any())
                        sparePartStatistics.PartsInboundAmount = entity.PartsInboundCheckBills.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单 && v.PartsInboundCheckBillDetails.Any(e => e.SparePartId == detail.SparePartId)).SelectMany(e => e.PartsInboundCheckBillDetails).Where(e => e.SparePartId == detail.SparePartId).Sum(e => e.InspectedQuantity);
                }

                var partsOutboundBills = entity.PartsOutboundBills.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单).ToArray();
                var partsShippingOrders = entity.PartsShippingOrders.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单).ToArray();
                var partsInboundCheckBills = entity.PartsInboundCheckBills.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单).ToArray();
                var newBills1 = from partsOutboundBill in partsOutboundBills
                                select new {
                                    BillCode = partsOutboundBill.Code,
                                    BillType = PartsStockingUIStrings.DataDetailView_PartsTransferOrderDetail_Constant_OutboundBillBill
                                };
                var newBills2 = from partsShippingOrder in partsShippingOrders
                                select new {
                                    BillCode = partsShippingOrder.Code,
                                    BillType = PartsStockingUIStrings.DataDetailView_PartsTransferOrderDetail_Constant_ShippingBill
                                };
                var newBills3 = from partsInboundCheckBill in partsInboundCheckBills
                                select new {
                                    BillCode = partsInboundCheckBill.Code,
                                    BillType = PartsStockingUIStrings.DataDetailView_PartsTransferOrderDetail_Constant_CheckBill
                                };
                foreach(var bill in newBills1)
                    this.OverstockPartsAdjustDetails.Add(new KeyValuePair<string, string>(bill.BillCode, bill.BillType));
                foreach(var bill in newBills2)
                    this.OverstockPartsAdjustDetails.Add(new KeyValuePair<string, string>(bill.BillCode, bill.BillType));
                foreach(var bill in newBills3)
                    this.OverstockPartsAdjustDetails.Add(new KeyValuePair<string, string>(bill.BillCode, bill.BillType));

                //var sparePartsId = entity.OverstockPartsAdjustDetails.Select(e => e.SparePartId).Distinct();
                //var serialNumber = 1;
                //foreach(var i in sparePartsId) {
                //    var adjustDetail = entity.OverstockPartsAdjustDetails.First(e => e.SparePartId == i);
                //    var statisticAdjustDetail = new OverstockPartsAdjustDetail {
                //        SerialNumber = serialNumber++,
                //        SparePartId = i,
                //        SparePartCode = adjustDetail.SparePartCode,
                //        SparePartName = adjustDetail.SparePartName,
                //        Quantity = entity.OverstockPartsAdjustDetails.Where(e => e.SparePartId == i).Sum(e => e.Quantity)
                //    };
                //    statisticAdjustDetail.OutboundAmount = entity.PartsOutboundBills.Any() ? entity.PartsOutboundBills.SelectMany(e => e.PartsOutboundBillDetails).Where(e => e.SparePartId == adjustDetail.SparePartId).Sum(e => e.OutboundAmount) : 0;
                //    statisticAdjustDetail.InboundAmount = entity.PartsInboundCheckBills.Any() ? entity.PartsInboundCheckBills.SelectMany(e => e.PartsInboundCheckBillDetails).Where(e => e.SparePartId == adjustDetail.SparePartId).Sum(e => e.InspectedQuantity) : 0;
                //    statisticAdjustDetail.ShippingAmount = entity.PartsShippingOrders.Any() ? entity.PartsShippingOrders.SelectMany(e => e.PartsShippingOrderDetails).Where(e => e.SparePartId == adjustDetail.SparePartId).Sum(e => e.ShippingAmount) : 0;
                //    this.OverstockPartsAdjustDetails.Add(statisticAdjustDetail);
                //}
            }, null);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_AdjustKanban;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<object> OverstockPartsAdjustDetails {
            get {
                return this.overstockPartsAdjustDetails ?? (this.overstockPartsAdjustDetails = new ObservableCollection<object>());
            }
        }

        public ObservableCollection<SparePartStatisticsForKanBan> SparePartStatisticsForKanBans {
            get {
                return this.sparePartStatisticsForKanBans ?? (this.sparePartStatisticsForKanBans = new ObservableCollection<SparePartStatisticsForKanBan>());
            }
        }
    }
}
