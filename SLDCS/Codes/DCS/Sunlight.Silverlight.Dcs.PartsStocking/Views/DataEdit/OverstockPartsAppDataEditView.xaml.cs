﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class OverstockPartsAppDataEditView {
        private KeyValueManager keyValueManager;
        private RadUpload uploader;
        private string strFileName;
        private const string EXPORT_DATA_FILE_NAME = "积压申请单清单模板.xlsx";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected Action<string> UploadFileSuccessedProcessing;
        private readonly ObservableCollection<KeyValuePair> kvStorageCompanyTypes = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
            "Company_Type"
        };
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvStorageCompanyTypes {
            get {
                return kvStorageCompanyTypes;
            }
        }

        private DataGridViewBase overstockPartsAppDetailForEditDataGridView;
        private DataGridViewBase OverstockPartsAppDetailForEditDataGridView {
            get {
                if(this.overstockPartsAppDetailForEditDataGridView == null) {
                    this.overstockPartsAppDetailForEditDataGridView = DI.GetDataGridView("OverstockPartsAppDetailForEdit");
                    this.overstockPartsAppDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.overstockPartsAppDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            //根据登陆企业加载企业类型数据源
            DomainContext.Load(DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError) {
                    loadOption.MarkErrorAsHandled();
                    return;
                }
                if(loadOption.Entities != null && loadOption.Entities.Any()) {
                    var company = loadOption.Entities.First();
                    this.KeyValueManager.LoadData(() => {
                        kvStorageCompanyTypes.Clear();
                        if(company.Type == (int)DcsCompanyType.服务站兼代理库)
                            foreach(var kvPair in this.KeyValueManager[kvNames[0]].Where(e => e.Key == (int)DcsCompanyType.服务站 || e.Key == (int)DcsCompanyType.代理库)) {
                                kvStorageCompanyTypes.Add(kvPair);
                            } else {
                            foreach(var kvPair in this.KeyValueManager[kvNames[0]]) {
                                kvStorageCompanyTypes.Add(kvPair);
                            }
                        }
                    });
                }
            }, null);
            this.Root.Children.Add(DI.GetDataEditPanel("OverstockPartsApp"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(OverstockPartsApp), "OverstockPartsAppDetails"), null, () => this.OverstockPartsAppDetailForEditDataGridView);

            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Action_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new Sunlight.Silverlight.Core.Command.DelegateCommand(() => this.Uploader.ShowFileDialog())
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Title_ExportTemplate,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = new Sunlight.Silverlight.Core.Command.DelegateCommand(() => {
                    var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode,
                                                IsRequired = true
                                            },new ImportTemplateColumn{
                                                Name=PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName,
                                                IsRequired=false
                                            }

                                        };
                    this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                        }
                        if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                            this.ExportFile(loadOp.Value);
                    }, null);
                })
            });

            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            this.Root.DataContextChanged -= this.OverstockPartsApp_DataContextChanged;
            this.Root.DataContextChanged += this.OverstockPartsApp_DataContextChanged;
        }
        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }
        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsStockingUIStrings.Upload_Error3);
                    e.Handled = true;
                }
            }
        }
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var overstockPartsApp = this.DataContext as OverstockPartsApp;
                        if(overstockPartsApp == null || overstockPartsApp.BranchId == default(int)) {
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Validation_OverstockPartsAppDetail_BranchIsNull);
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        if(overstockPartsApp.PartsSalesCategoryId == default(int)) {
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Validation_OverstockPartsAppDetail_PartsSalesCategoryIsNull);
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        ShellViewModel.Current.IsBusy = true;
                        if(e != null)
                            this.excelServiceClient.ImportOverstockPartsAppDetailAsync(overstockPartsApp.BranchId, overstockPartsApp.PartsSalesCategoryId, overstockPartsApp.StorageCompanyType, e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportOverstockPartsAppDetailCompleted -= excelServiceClient_ImportOverstockPartsAppDetailCompleted;
                        this.excelServiceClient.ImportOverstockPartsAppDetailCompleted += excelServiceClient_ImportOverstockPartsAppDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        void excelServiceClient_ImportOverstockPartsAppDetailCompleted(object sender, ImportOverstockPartsAppDetailCompletedEventArgs e) {
            var overstockPartsApp = this.DataContext as OverstockPartsApp;
            if(overstockPartsApp == null)
                return;
            foreach(var detail in overstockPartsApp.OverstockPartsAppDetails) {
                overstockPartsApp.OverstockPartsAppDetails.Remove(detail);
            }
            var serialNumber = 1;
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    overstockPartsApp.OverstockPartsAppDetails.Add(new OverstockPartsAppDetail {
                        SerialNumber = serialNumber,
                        SparePartId = data.Id,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        SalePrice = data.PartsSalesPrice,
                        DealPrice = data.PartsSalesPrice,
                        PartsSalesPrice=data.PartsSalesPrice
                    });
                    serialNumber++;
                }

            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }
        protected string ErrorFileName {
            get;
            set;
        }
        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            this.ErrorFileName = errorFileName;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(ErrorFileName));
        }
        private void OverstockPartsApp_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var overstockPartsApp = this.DataContext as OverstockPartsApp;
            if(overstockPartsApp == null)
                return;
            overstockPartsApp.PropertyChanged -= this.OverstockPartsApp_PropertyChanged;
            overstockPartsApp.PropertyChanged += this.OverstockPartsApp_PropertyChanged;
        }

        private void OverstockPartsApp_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var overstockPartsApp = this.DataContext as OverstockPartsApp;
            if(overstockPartsApp == null)
                return;
            switch(e.PropertyName) {
                case "BranchId":
                    if(!overstockPartsApp.OverstockPartsAppDetails.Any())
                        return;
                    if(!this.OverstockPartsAppDetailForEditDataGridView.CommitEdit())
                        return;
                    DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Validation_OverstockPartsApp_DetailRemove, () => {
                        foreach(var item in overstockPartsApp.OverstockPartsAppDetails)
                            overstockPartsApp.OverstockPartsAppDetails.Remove(item);
                    });
                    break;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetOverstockPartsAppsWithDetailsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    var serialNumber = 0;
                    foreach(var overstockPartsAppDetail in entity.OverstockPartsAppDetails) {

                        overstockPartsAppDetail.SerialNumber = ++serialNumber;
                        overstockPartsAppDetail.PartsSalesPrice = overstockPartsAppDetail.DealPrice;
                    }
                    this.SetObjectToEdit(entity);

                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.OverstockPartsAppDetailForEditDataGridView.CommitEdit())
                return;
            var overstockPartsApp = this.DataContext as OverstockPartsApp;
            if(overstockPartsApp == null)
                return;
            overstockPartsApp.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(overstockPartsApp.Code))
                overstockPartsApp.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_OverstockPartsApp_CodeIsNull, new[] {
                    "Code"
                }));
            if(overstockPartsApp.BranchId == 0)
                overstockPartsApp.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_OverstockPartsApp_BranchIdIsNull, new[] {
                    "BranchId"
                }));
            if(!overstockPartsApp.OverstockPartsAppDetails.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validate_OverstockPartsApp_Details);
                return;
            }
            var details = overstockPartsApp.OverstockPartsAppDetails.GroupBy(d => d.SparePartId).Where(d => d.Count() > 1);
            var validationErrors = new List<string>();
            if(details != null && details.Any())
                validationErrors.Add(PartsStockingUIStrings.DataEditView_Validation_OverstockPartsAppDetails_SparePartIdNotAllowedRepeat);
            if(validationErrors.Count > 0) {
                UIHelper.ShowNotification(string.Join(Environment.NewLine, validationErrors));
                return;
            }

            const string PATTERN = @"[^0-9\(\)\*\-]";
            if(!string.IsNullOrEmpty(overstockPartsApp.ContactPhone))
                if(Regex.IsMatch(overstockPartsApp.ContactPhone, PATTERN)) {
                    overstockPartsApp.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validate_OverstockPartsApp_PhoneNumber, new[] {
                        "ContactPhone"
                    }));
                }
            if(overstockPartsApp.HasValidationErrors)
                return;
            ((IEditableObject)overstockPartsApp).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_OverstockPartsApp;
            }
        }

        public OverstockPartsAppDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);

        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
