﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class OverstockPartsAppForApproveDataEditView {
        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("OverstockPartsAppDetailForBranchEdit");
                    this.dataGridView.DomainContext = this.DomainContext;
                }
                return this.dataGridView;
            }
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_OverstockPartsApp;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetOverstockPartsAppWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                this.DataContext = null;
                if(entity != null) {
                    var serialNumber = 0;
                    foreach(var detail in entity.OverstockPartsAppDetails) {
                        detail.IfConfirmed = true;
                        detail.SerialNumber = ++serialNumber;
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("OverstockPartsAppForBranch"));
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(OverstockPartsAppDetail), PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsAppDetail), null, () => this.DataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        protected override void OnEditSubmitting() {
            var overstockPartsApp = this.DataContext as OverstockPartsApp;
            if(overstockPartsApp == null || !this.DataGridView.CommitEdit())
                return;
            ((IEditableObject)overstockPartsApp).EndEdit();
            try {
                if(overstockPartsApp.Can审批积压件申请单)
                    overstockPartsApp.审批积压件申请单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public OverstockPartsAppForApproveDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
