﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class OverstockPartsAppForRejectDataEditView {
        public OverstockPartsAppForRejectDataEditView() {
            InitializeComponent();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetOverstockPartsAppsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var overstockPartsApp = this.DataContext as OverstockPartsApp;
            if(overstockPartsApp == null)
                return;
            overstockPartsApp.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(overstockPartsApp.RejectComment))
                overstockPartsApp.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Title_RejectReasonIsNull, new[] {
                    "RejectComment"
                }));

            if(overstockPartsApp.HasValidationErrors)
                return;

            try {
                if(overstockPartsApp.Can驳回积压件申请单)
                    overstockPartsApp.驳回积压件申请单();
                //todo:调用驳回方法
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_RejectOverstock;
            }
        }
    }
}
