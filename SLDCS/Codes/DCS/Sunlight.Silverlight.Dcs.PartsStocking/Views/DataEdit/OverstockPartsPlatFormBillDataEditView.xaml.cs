﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.ViewModel;
using System;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class OverstockPartsPlatFormBillDataEditView {
       
        private ObservableCollection<OverstockPartsPlatFormBill> overstockPartsPlatFormBills;
        public ObservableCollection<OverstockPartsPlatFormBill> OverstockPartsPlatFormBills {
            get {
                return this.overstockPartsPlatFormBills ?? (this.overstockPartsPlatFormBills = new ObservableCollection<OverstockPartsPlatFormBill>());
            }

        }
        private DataGridViewBase overstockPartsPlatFormBillForDeatil;

        private DataGridViewBase OverstockPartsPlatFormBillForDeatil {
            get {
                if(this.overstockPartsPlatFormBillForDeatil == null) {
                    this.overstockPartsPlatFormBillForDeatil = DI.GetDataGridView("OverstockPartsPlatFormBillForDeatil");
                    this.overstockPartsPlatFormBillForDeatil.DomainContext = this.DomainContext;
                    this.overstockPartsPlatFormBillForDeatil.DataContext = this;
                }
                return this.overstockPartsPlatFormBillForDeatil;
            }
        }

        protected virtual void CreateUI() {
            var dcsDetailDataEditView = new DcsDetailDataEditView();
            dcsDetailDataEditView.Register("", null, () => this.OverstockPartsPlatFormBillForDeatil);
            dcsDetailDataEditView.UnregisterButton(dcsDetailDataEditView.InsertButton);
            dcsDetailDataEditView.UnregisterButton(dcsDetailDataEditView.DeleteButton);
            dcsDetailDataEditView.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(dcsDetailDataEditView);
        }
        
        protected override void OnEditSubmitting() {
            if(!this.OverstockPartsPlatFormBillForDeatil.CommitEdit())
                return;
            if(!OverstockPartsPlatFormBills.Any())
                return;
            foreach(var item in OverstockPartsPlatFormBills) {
                if((item.DiscountRate ?? 0) <= 0) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validate_OverstockPartsApp_DiscountRateNull);
                    return;
                }
                if((item.DiscountRate ?? 0) > 0.7d) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validate_OverstockPartsApp_DiscountRate);
                    return;
                }
            }
            var overstockPartsPlatFormBill = overstockPartsPlatFormBills.First();
            ((IEditableObject)overstockPartsPlatFormBill).EndEdit();
            try
            {
                if (overstockPartsPlatFormBill.CanEditOverstockPartsPlatFormBill)
                    overstockPartsPlatFormBill.EditOverstockPartsPlatFormBill();
            }
            catch (Exception ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
            
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetOverstockPartsPlatFormBillsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var item in loadOp.Entities) {
                    OverstockPartsPlatFormBills.Add(item);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public OverstockPartsPlatFormBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_OverstockPartsApp_Modify;
            }
        }

    }
}
