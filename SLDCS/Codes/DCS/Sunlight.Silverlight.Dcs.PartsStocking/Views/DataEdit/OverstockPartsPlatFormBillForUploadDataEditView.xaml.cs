﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.Common.Views.QueryWindow;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit
{
    public partial class OverstockPartsPlatFormBillForUploadDataEditView 
    {
        public OverstockPartsPlatFormBillForUploadDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI()
        {
            this.Attachment.Children.Add(FileUploadDataEditPanels);
        }
        private void LoadEntityToEdit(int id)
        {
            this.DomainContext.Load(this.DomainContext.GetOverstockPartsPlatFormBillsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null)
                {
                    this.FileUploadDataEditPanels.FilePath = entity.Attachment;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private PartsShiftOrderForUploadDataEditPanel fileUploadDataEditPanels;
        protected PartsShiftOrderForUploadDataEditPanel FileUploadDataEditPanels
        {
            get
            {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsShiftOrderForUploadDataEditPanel)DI.GetDataEditPanel("PartsShiftOrderForUpload"));
            }
        }
        protected override string Title
        {
            get
            {
                return "上传附件";
            }
        }
        protected override void OnEditSubmitting()
        {
            var overstockPartsPlatFormBill = this.DataContext as OverstockPartsPlatFormBill;
            if (overstockPartsPlatFormBill == null)
                return;
            overstockPartsPlatFormBill.ValidationErrors.Clear();
            overstockPartsPlatFormBill.Attachment = FileUploadDataEditPanels.FilePath;
            ((IEditableObject)overstockPartsPlatFormBill).EndEdit();
            try
            {
                if (overstockPartsPlatFormBill.Can上传积压件明细附件)
                    overstockPartsPlatFormBill.上传积压件明细附件();
            }
            catch (Exception ex)
            {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override bool OnRequestCanSubmit()
        {
            return true;
        }
    }
}
