﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Core.Model;
using System.Collections.ObjectModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PackingTaskDataEditView {
        int scanerNum = 0;
        int packNum = 0;
        public PackingTaskDataEditView() {
            this.InitializeComponent();
        }          
    
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else {
                this.LoadEntityToEdit((int)id);
                this.ProductCode.Focus();
            }
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPackingTasksQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    //判断包装单是否已生成了修正单
                    this.DomainContext.Load(this.DomainContext.GetPackingTaskTraceQuery(entity.Id), LoadBehavior.RefreshCurrent, loadOp3 => {
                        if(loadOp3.HasError) {
                            if(!loadOp3.IsErrorHandled)
                                loadOp3.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp3);
                            return;
                        }
                        var traces = loadOp3.Entities.ToArray(); ;
                        if(traces.Count() > 0) {
                            UIHelper.ShowNotification("已生成供应商零件永久性标识条码修正单，不能包装");
                            return;
                        }
                        this.SetObjectToEdit(entity);
                        this.DomainContext.Load(this.DomainContext.GetSparePartsQuery().Where(r => r.Id == entity.SparePartId), LoadBehavior.RefreshCurrent, loadOp1 => {
                            if(loadOp1.HasError) {
                                if(!loadOp1.IsErrorHandled)
                                    loadOp1.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                return;
                            }
                            var sp = loadOp1.Entities.FirstOrDefault();
                            if(sp != null) {
                                entity.TracePropertyInt = sp.TraceProperty;
                                if(sp.TraceProperty.HasValue && sp.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                    entity.TraceProperty = "精确追溯";
                                } else if(sp.TraceProperty.HasValue && sp.TraceProperty.Value == (int)DCSTraceProperty.批次追溯) {
                                    entity.TraceProperty = "批次追溯";
                                    this.DomainContext.Load(this.DomainContext.GetAccurateTracesQuery().Where(r => r.PartId == entity.SparePartId && r.SourceBillId == entity.PartsInboundPlanId && r.Status == (int)DCSAccurateTraceStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                                        if(loadOp1.HasError) {
                                            if(!loadOp2.IsErrorHandled)
                                                loadOp2.MarkErrorAsHandled();
                                            DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                            return;
                                        }
                                        var trace = loadOp2.Entities.FirstOrDefault();
                                        if(trace != null) {
                                            entity.TraceCode = trace.TraceCode;
                                            this.TraceCode.Text = trace.TraceCode;
                                            this.TraceCode.IsEnabled = false;
                                        } else {
                                            this.TraceCode.IsEnabled = true;
                                        }
                                    }, null);
                                }

                            }
                        }, null);
                    }, null);
                }
            }, null);
        }
        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.DataContext = null;
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var packingTask = this.DataContext as PackingTask;
            if(packingTask == null)
                return;
            if(string.IsNullOrEmpty(this.ProductCode.Text)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Title_ProductInformationIsNull);
                return;
            }
            if(string.IsNullOrEmpty(this.SIHCode.Text)) {
                UIHelper.ShowNotification("请填写SIH件码");
                return;
            }
            if(packingTask.ScanNumber<=0) {
                UIHelper.ShowNotification("包装数量必须大于0");
                return;
            }
            packingTask.ProductCode = this.ProductCode.Text;
            if(packingTask.PackingNumber <= 0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Title_ScanProductAndPressEnter);
                return;
            }
            var sihCodes = this.SIHCode.Text.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            var traceCodes = this.TraceCode.Text.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            if(packingTask.TracePropertyInt.HasValue && sihCodes.Count() > 0 && traceCodes.Count()>0) {
                if(!sihCodes.All(t => t.EndsWith("J")||t.EndsWith("X"))) {
                    UIHelper.ShowNotification("SIH件码错误，请重新扫描");
                    return;
                }
                if((sihCodes.Count() != packingTask.ScanNumber || sihCodes.Distinct().Count() != packingTask.ScanNumber) && packingTask.TracePropertyInt == (int)DCSTraceProperty.精确追溯) {
                    UIHelper.ShowNotification("SIH件码数量不够,存在重复的SIH标签码");
                    return;
                }
                if((traceCodes.Count() != packingTask.ScanNumber || traceCodes.Distinct().Count() != packingTask.ScanNumber) && packingTask.TracePropertyInt == (int)DCSTraceProperty.精确追溯) {
                    UIHelper.ShowNotification("追溯码数量不够,或存在重复的追溯码");
                    return;
                }
                if(sihCodes.Count()==0) {
                    UIHelper.ShowNotification("请填写SIH件码");
                    return;
                }
                if(traceCodes.Count() == 0) {
                    UIHelper.ShowNotification("请填写追溯码");
                    return;
                }
                if(traceCodes.Any(t => t.Length != 17 && packingTask.TracePropertyInt == (int)DCSTraceProperty.精确追溯)) {
                    UIHelper.ShowNotification("精确追溯的追溯码的长度必须是17");
                    return;
                }
                if(traceCodes.Any(t => t.Length != 8 && packingTask.TracePropertyInt == (int)DCSTraceProperty.批次追溯)) {
                    UIHelper.ShowNotification("批次追溯的追溯码的长度必须是8");
                    return;
                }
            }
            packingTask.SIHCode = this.SIHCode.Text;
            packingTask.TraceCode = this.TraceCode.Text;           
            if(packingTask.TracePropertyInt == null || string.IsNullOrEmpty(packingTask.TraceCode)||!this.TraceCode.IsEnabled) {
                packingTask.BoxCode = this.ProductCode.Text;
            }
            ((IEditableObject)packingTask).EndEdit();
            try {
                if(packingTask.Can包装登记)
                    packingTask.包装登记();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        //根据配件查找包材信息
        private void setDefaultValue(string partsInboundCheckBillCode, string sparePartCode, bool bar) {
            var isShow = true;
            this.DomainContext.Load(this.DomainContext.GetPackingTasksQuery().Where(r => r.PartsInboundCheckBillCode == partsInboundCheckBillCode && r.SparePartCode == sparePartCode), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entities = loadOp.Entities.ToArray();

                if(entities.Length != 0) {
                    if(entities.Where(s => s.Status == (int)DcsPackingTaskStatus.新增 || s.Status == (int)DcsPackingTaskStatus.部分包装).ToArray().Length == 0) {
                        DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Title_OutboundHasPackingFinish, () => {
                        });
                        return;
                    }
                    var entity = entities.Where(r => r.Status == (int)DcsPackingTaskStatus.新增 || r.Status == (int)DcsPackingTaskStatus.部分包装).FirstOrDefault();
                    //判断包装单是否已生成了修正单
                    this.DomainContext.Load(this.DomainContext.GetPackingTaskTraceQuery(entity.Id), LoadBehavior.RefreshCurrent, loadOp3 => {
                        if(loadOp3.HasError) {
                            if(!loadOp3.IsErrorHandled)
                                loadOp3.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp3);
                            return;
                        }
                        var traces = loadOp3.Entities.ToArray(); ;
                        if(traces.Count() > 0) {
                            UIHelper.ShowNotification("已生成供应商零件永久性标识条码修正单，不能包装");
                            isShow = false;
                            return;
                        }
                        this.DomainContext.Load(this.DomainContext.GetSparePartsQuery().Where(r => r.Id == entity.SparePartId), LoadBehavior.RefreshCurrent, loadOp1 => {
                            if(loadOp1.HasError) {
                                if(!loadOp1.IsErrorHandled)
                                    loadOp1.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                return;
                            }
                            var sp = loadOp1.Entities.FirstOrDefault();
                            if(sp != null) {
                                entity.TracePropertyInt = sp.TraceProperty;
                                if(sp.TraceProperty.HasValue && sp.TraceProperty.Value == (int)DCSTraceProperty.精确追溯) {
                                    entity.TraceProperty = "精确追溯";
                                } else if(sp.TraceProperty.HasValue && sp.TraceProperty.Value == (int)DCSTraceProperty.批次追溯) {
                                    entity.TraceProperty = "批次追溯";
                                    this.DomainContext.Load(this.DomainContext.GetAccurateTracesQuery().Where(r => r.PartId == entity.SparePartId && r.SourceBillId == entity.PartsInboundPlanId && r.Status == (int)DCSAccurateTraceStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                                        if(loadOp2.HasError) {
                                            if(!loadOp2.IsErrorHandled)
                                                loadOp2.MarkErrorAsHandled();
                                            DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
                                            return;
                                        }
                                        var trace = loadOp2.Entities.FirstOrDefault();
                                        if(trace != null) {
                                            entity.TraceCode = trace.TraceCode;
                                            this.TraceCode.Text = trace.TraceCode;
                                            this.TraceCode.IsEnabled = false;
                                        } else {
                                            this.TraceCode.IsEnabled = true;
                                        }
                                    }, null);
                                } else {
                                    this.TraceCode.IsEnabled = false;
                                }
                            }
                        }, null);
                    }, null);
                    if(bar) {//在输入单品码的时候才带出包材信息
                        if(null == entity.PackingQty) {
                            entity.PackingQty = 0;
                        }
                        this.setMeatial((int)entity.SparePartId, ((int)entity.PlanQty - (int)entity.PackingQty));
                    }
                   // scanerNum = scanerNum + 1;
                    //if(scanerNum > (entity.PlanQty - (entity.PackingQty == null ? 0 : entity.PackingQty))) {
                    //    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ThisPackingQuantityError);
                    //  //  this.ScanNumber.Text = (scanerNum - 1).ToString();
                    //    this.PackingNumber.Text = packNum.ToString();
                    //    return;
                    //}
                    String boxNo = this.BoxNo.Text;
                    if(isShow) {
                        this.SetObjectToEdit(entity);
                    }                    
                    this.BoxNo.Text = boxNo;
                   // this.ScanNumber.Text = scanerNum.ToString();
                    this.BoxNo.Focus();

                } else {
                    this.PartsInboundPlanCode.Text = "";
                    this.Code.Text = "";
                    this.WarehouseCode.Text = "";
                    this.PlanQty.Text = "";
                    this.SparePartName.Text = "";
                    this.PackingQty.Text = "";
                    this.PartsInboundCheckBillCode.Focus();
                    DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Validation_ThisPackingTaskIsNull, () => {
                    });
                }
            }, null);
            this.SIHCode.Focus();
        }
        //按照配件包装属性最大层级的包装级别带入
        private void setMeatial(int sparePartId, int qty) {
            this.DomainContext.Load(this.DomainContext.GetPartsBranchPackingPropsQuery().Where(r => r.SparePartId == sparePartId).OrderBy(r => r.PackingType), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError || loadOp.Entities == null || !loadOp.Entities.Any()) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsPackingPropIsNull);
                    return;
                }
                var entitys = loadOp.Entities.LastOrDefault();
                if(entitys != null) {
                    this.PackingNumber.Text = entitys.PackingCoefficient.ToString();
                    var num = (Math.Ceiling((Double.Parse(qty.ToString()) / Double.Parse(entitys.PackingCoefficient.ToString())))).ToString();
                    packNum = (int)entitys.PackingCoefficient;
                    //箱数=本次包装数量/箱包装数
                    int copies = (int)(Math.Ceiling(Double.Parse(scanerNum.ToString()) / Double.Parse(packNum.ToString())));
                    var meterial = entitys.PackingMaterial;
                    if(null == meterial) {
                        meterial = "";
                    }
                    if(entitys.PackingType == (int)DcsPackingUnitType.一级包装) {
                        this.FirstPackingMaterialSj.Text = meterial;
                        this.FirstPackingMaterial.Text = meterial;
                        this.FirstNumber.Text = num;
                        this.SecPackingMaterialSj.IsEnabled = false;
                        this.ThidPackingMaterialSj.IsEnabled = false;
                        this.FirstNumberSj.Text = copies.ToString();
                    } else if(entitys.PackingType == (int)DcsPackingUnitType.二级包装) {
                        this.SecPackingMaterialSj.Text = meterial;
                        this.SecPackingMaterial.Text = meterial;
                        this.SecNumber.Text = num;
                        this.FirstPackingMaterialSj.IsEnabled = false;
                        this.ThidPackingMaterialSj.IsEnabled = false;
                        this.SecNumberSj.Text = copies.ToString();
                    } else if(entitys.PackingType == (int)DcsPackingUnitType.三级包装) {
                        this.ThidPackingMaterialSj.Text = meterial;
                        this.ThidPackingMaterial.Text = meterial;
                        this.ThidNumber.Text = num;
                        this.FirstPackingMaterialSj.IsEnabled = false;
                        this.SecPackingMaterialSj.IsEnabled = false;
                        this.ThidNumberSj.Text = copies.ToString();
                    }
                }
            }, null);
        }
        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_Packing;
            }
        }
        private void BoxNo_OnKeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Enter) {
                string boxNo = this.BoxNo.Text;
                string[] noAndNumber = boxNo.Split('|');
                this.BoxNo.Text = noAndNumber[0];
                if(!string.IsNullOrEmpty(this.PartsInboundCheckBillCode.Text)) {
                    if(!string.IsNullOrEmpty(this.SparePartCode.Text) && !this.SparePartCode.Text.Equals(noAndNumber[0])) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_SparePartCodeHasError);
                        return;
                    }
                    this.setDefaultValue(this.PartsInboundCheckBillCode.Text, noAndNumber[0], false);
                }
                this.ProductCode.Focus();
            }
        }
        private void PartsInboundCheckBillCode_OnKeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Enter) {
                this.ProductCode.Focus();
            }
        }
        private void ProductCode_OnKeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Enter) {
                string productCode = this.ProductCode.Text;
                if(!string.IsNullOrEmpty(productCode)){
                    if(!productCode.EndsWith("X") ) {
                        UIHelper.ShowNotification("SIH箱码请扫描X结尾的箱码", 5);
                        this.ProductCode.Text = "";
                        return;
                    }
                }
                string[] codeAndNum = productCode.Split('|');
                if(string.IsNullOrEmpty(this.SparePartCode.Text)) {
                    this.SparePartCode.Text = codeAndNum[0];
                }
                this.ProductCode.Text = productCode;
                if(!string.IsNullOrEmpty(this.PartsInboundCheckBillCode.Text)) {
                    if(!string.IsNullOrEmpty(this.SparePartCode.Text) && !this.SparePartCode.Text.Equals(codeAndNum[0])) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_SparePartCodeHasError2);
                        return;
                    }
                    this.setDefaultValue(this.PartsInboundCheckBillCode.Text, this.SparePartCode.Text, true);
                }
                
            }
        }
        private void SIHCode_OnKeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Enter) {
                var packingTask = this.DataContext as PackingTask;
                if(packingTask == null)
                    return;
                if(string.IsNullOrEmpty(this.TraceCode.Text) && !string.IsNullOrEmpty(packingTask.SIHCode)) {
                    return;
                }
                if(!string.IsNullOrEmpty(packingTask.SIHCode) && packingTask.SIHCode.Equals(this.SIHCode.Text.ToString())) {
                    return;
                }
                if( string.IsNullOrEmpty(this.ProductCode.Text)){
                    UIHelper.ShowNotification("请先扫描SIH箱码", 5);
                    return;
                }
                string sIHcode = this.SIHCode.Text;
                if(sIHcode.Equals("")) {
                    this.SIHCode.Text = "";
                    scanerNum = 0;
                    this.ScanNumber.Text = scanerNum.ToString();
                    packingTask.SIHCode = null;
                    return;
                } else {
                    if(!sIHcode.EndsWith("X") && !sIHcode.EndsWith("J")) {
                        UIHelper.ShowNotification("SIH件码格式错误，请扫标签码",5);
                        return;
                    }
                    if(scanerNum < (packingTask.PlanQty - (packingTask.PackingQty == null ? 0 : packingTask.PackingQty))) {
                        scanerNum = scanerNum + 1;
                        this.ScanNumber.Text = scanerNum.ToString();
                    } else {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ThisPackingQuantityError);
                       // this.SIHCode.Text = packingTask.SIHCode;
                        return;
                    }
                    this.SIHCode.Text = ";" + sIHcode;
                  
                } 
                
                 packingTask.SIHCode = this.SIHCode.Text.ToString();
                 this.TraceCode.Focus();
            }
        }
        private void TraceCode_OnKeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Enter) {
                var packingTask = this.DataContext as PackingTask;
                if(packingTask == null)
                    return;
                string traceCodes = this.TraceCode.Text;
                if(traceCodes.Equals("")) {
                    this.TraceCode.Text = "";
                } else {
                    if(!string.IsNullOrEmpty(packingTask.BoxCode) && packingTask.BoxCode.Equals(this.ProductCode.Text.ToString())) {
                        return;
                    }
                    this.TraceCode.Text = ";" + traceCodes;
                }
                packingTask.BoxCode = this.ProductCode.Text + ";" + packingTask.BoxCode;
                this.SIHCode.Focus();
            }
        }

        private void ScanNumber_TextChanged(object sender, TextChangedEventArgs e) {
            if(string.IsNullOrEmpty(this.PackingNumber.Text) || "0".Equals(this.PackingNumber.Text))
                return;
            if(string.IsNullOrEmpty(this.ScanNumber.Text) || "0".Equals(this.ScanNumber.Text))
                return;
            if(Convert.ToInt32(this.ScanNumber.Text) > (Convert.ToInt32(this.PlanQty.Text) - (Convert.ToInt32("".Equals(this.PackingQty.Text) ? "0" : this.PackingQty.Text)))) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ThisPackingQuantityError2);
                this.ScanNumber.Text = scanerNum.ToString();
                this.PackingNumber.Text = packNum.ToString();
                return;
            }
            string copies = (Math.Ceiling((Double.Parse(this.ScanNumber.Text.ToString()) / Double.Parse(this.PackingNumber.Text.ToString())))).ToString();
            if(Convert.ToInt32(this.FirstNumber.Text) > 0)
                this.FirstNumberSj.Text = copies;
            else if(Convert.ToInt32(this.SecNumber.Text) > 0)
                this.SecNumberSj.Text = copies;
            else if(Convert.ToInt32(this.ThidNumber.Text) > 0)
                this.ThidNumberSj.Text = copies;
            scanerNum = Convert.ToInt32(this.ScanNumber.Text);
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e) {
            this.PartsInboundCheckBillCode.Focus();
        }

       
    }
}
