﻿using System;
using System.Windows;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Controls;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsDifferenceBackBillApproveDataEditView {
        private readonly string[] kvNames = {
            "PartsDifferenceBackBillType"
        };
        private DataGridViewBase partsDifferenceBackBilldtlDataGridView;
        private ButtonItem rejectBtn;
        public KeyValueManager KeyValueManager = new KeyValueManager();
        public PartsDifferenceBackBillApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
        }

        private PartsDifferenceBackBillForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsDifferenceBackBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsDifferenceBackBillForUploadDataEditPanel)DI.GetDataEditPanel("PartsDifferenceBackBillForUpload"));
            }
        }

        public object KvPartsDifferenceBackBillTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        protected override string Title {
            get {
                var partsDifferenceBackBill = this.DataContext as PartsDifferenceBackBill;
                var message = partsDifferenceBackBill == null || partsDifferenceBackBill.Status == (int)DCSPartsDifferenceBackBillStatus.提交 ? "初审" :
                    partsDifferenceBackBill.Status == (int)DCSPartsDifferenceBackBillStatus.初审通过 ? "审核" : "审批";
                return message + DcsUIStrings.BusinessName_PartsDifferenceBackBill;
            }
        }

        private DataGridViewBase PartsDifferenceBackBilldtlDataGridView {
            get {
                if(this.partsDifferenceBackBilldtlDataGridView == null) {
                    this.partsDifferenceBackBilldtlDataGridView = DI.GetDataGridView("PartsDifferenceBackBillDtl");
                    this.partsDifferenceBackBilldtlDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsDifferenceBackBilldtlDataGridView;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var domainContext = new DcsDomainContext();
            var partsDifferenceBackBill = this.DataContext as PartsDifferenceBackBill;
            if(partsDifferenceBackBill == null)
                return;

            this.DomainContext.审核配件差异回退单(partsDifferenceBackBill.Id, partsDifferenceBackBill.Status, invokeOp => {
                if(invokeOp.HasError) {
                    if(!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    var error = invokeOp.ValidationErrors.FirstOrDefault();
                    if(error != null) {
                        UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                    } else {
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    }
                    DomainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification("审核成功");
                base.OnEditSubmitting();
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsDifferenceBackBillByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    FileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();

            this.rejectBtn = new ButtonItem {
                Command = new Sunlight.Silverlight.Core.Command.DelegateCommand(this.RejecrCurrentData),
                Title = PartsStockingUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            };
            this.RegisterButton(rejectBtn);

            FileUploadDataEditPanels.FilePath = null;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 1);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 1);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 4);
            FileUploadDataEditPanels.isHiddenButtons = true;
            this.LayoutRoot.Children.Add(fileUploadDataEditPanels);

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register("配件差异回退清单", null, () => this.PartsDifferenceBackBilldtlDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 2);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 8);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Width = 1200;
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        protected override void OnEditCancelled() {
            this.DataContext = null;
            base.OnEditCancelled();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        //驳回
        private void RejecrCurrentData() {
            var domainContext = new DcsDomainContext();
            var partsDifferenceBackBill = this.DataContext as PartsDifferenceBackBill;
            if(partsDifferenceBackBill == null)
                return;
            ((IEditableObject)partsDifferenceBackBill).EndEdit();
            this.DomainContext.驳回配件差异回退单(partsDifferenceBackBill.Id, partsDifferenceBackBill.Status, invokeOp => {
                if(invokeOp.HasError) {
                    if(!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    var error = invokeOp.ValidationErrors.FirstOrDefault();
                    if(error != null) {
                        UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                    } else {
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    }
                    DomainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification("驳回成功");
                base.OnEditSubmitting();
            }, null);
        }
    }
}
