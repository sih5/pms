﻿using System;
using System.Windows;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Controls;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {

    public partial class PartsDifferenceBackBillDataEditView {
        public KeyValueManager KeyValueManager = new KeyValueManager();
        private DataGridViewBase partsDifferenceBackBilldtlDataGridView;
        private PartsDifferenceBackBillForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsDifferenceBackBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsDifferenceBackBillForUploadDataEditPanel)DI.GetDataEditPanel("PartsDifferenceBackBillForUpload"));
            }
        }

        public PartsDifferenceBackBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsDifferenceBackBill;
            }
        }

        private readonly string[] kvNames = {
            "PartsDifferenceBackBillType"
        };

        private DataGridViewBase PartsDifferenceBackBilldtlDataGridView {
            get {
                if(this.partsDifferenceBackBilldtlDataGridView == null) {
                    this.partsDifferenceBackBilldtlDataGridView = DI.GetDataGridView("PartsDifferenceBackBillDtlForEdit");
                    this.partsDifferenceBackBilldtlDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsDifferenceBackBilldtlDataGridView;
            }
        }

        public object KvPartsDifferenceBackBillTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var partsDifferenceBackBill = this.DataContext as PartsDifferenceBackBill;
            if(partsDifferenceBackBill == null)
                return;
            if(!this.PartsDifferenceBackBilldtlDataGridView.CommitEdit())
                return;
            partsDifferenceBackBill.Path = fileUploadDataEditPanels.FilePath;
            partsDifferenceBackBill.IsAttach = !string.IsNullOrWhiteSpace(partsDifferenceBackBill.Path);
            partsDifferenceBackBill.ValidationErrors.Clear();
            foreach(var item in partsDifferenceBackBill.PartsDifferenceBackBillDtls)
                item.ValidationErrors.Clear();
            if(!partsDifferenceBackBill.PartsDifferenceBackBillDtls.Any()) {
                UIHelper.ShowNotification("配件差异回退单清单不能为空");
                return;
            }
            foreach(var partsDifferenceBackBillDtl in partsDifferenceBackBill.PartsDifferenceBackBillDtls) {
                if(partsDifferenceBackBillDtl.DiffQuantity <= 0) {
                    UIHelper.ShowNotification("差异数量必须大于0");
                    return;
                }
                if(partsDifferenceBackBillDtl.DiffQuantity > partsDifferenceBackBillDtl.OnlyDiffQuantity) {
                    UIHelper.ShowNotification("差异数量必须小于等于所选源单据的未完成部分配件");
                    return;
                }
                if(partsDifferenceBackBillDtl.ErrorType == 0) {
                    UIHelper.ShowNotification("错误类型为必填");
                    return;
                }
                if(partsDifferenceBackBillDtl.TraceProperty.HasValue && !string.IsNullOrEmpty(partsDifferenceBackBillDtl.TraceCode)) {
                    var traces = partsDifferenceBackBillDtl.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                    if(partsDifferenceBackBillDtl.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        if(traces.Count() != partsDifferenceBackBillDtl.DiffQuantity) {
                            UIHelper.ShowNotification("配件" + partsDifferenceBackBillDtl.SparePartCode + "的追溯码数量不等于差异数量");
                            return;
                        } else if(traces.Count() != traces.Distinct().Count()) {
                            UIHelper.ShowNotification("配件" + partsDifferenceBackBillDtl.SparePartCode + "的追溯码有重复数据");
                            return;
                        }
                    } else {
                        if(traces.Count() != 1) {
                            UIHelper.ShowNotification("批次追溯配件" + partsDifferenceBackBillDtl.SparePartCode + "只允许填写一个追溯码");
                            return;
                        }
                    }
                }
            }
            if(partsDifferenceBackBill.HasValidationErrors || partsDifferenceBackBill.PartsDifferenceBackBillDtls.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsDifferenceBackBill).EndEdit();
            if(EditState == DataEditState.New)
                partsDifferenceBackBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsDifferenceBackBillByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    fileUploadDataEditPanels.FilePath = entity.Path;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();

            var queryWindow = DI.GetQueryWindow("PartsDifferenceBackBillSourceCode");
            queryWindow.SelectionDecided += this.queryWindow_SelectionDecided;
            queryWindow.Loaded += QueryWindow_Loaded;
            this.ptSourceCode.PopupContent = queryWindow;

            FileUploadDataEditPanels.FilePath = null;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 2);
            FileUploadDataEditPanels.SetValue(Grid.ColumnProperty, 1);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 4);
            this.LayoutRoot.Children.Add(fileUploadDataEditPanels);

            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register("配件差异回退清单", null, () => this.PartsDifferenceBackBilldtlDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 3);
            detailDataEditView.SetValue(Grid.ColumnSpanProperty, 8);
            detailDataEditView.Width = 1200;
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        protected override void OnEditCancelled() {
            this.DataContext = null;
            base.OnEditCancelled();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsDifferenceBackBillSourceCode = queryWindow.SelectedEntities.Cast<VirtualPartsDifferenceBackBillSourceCode>().FirstOrDefault();
            if(partsDifferenceBackBillSourceCode == null)
                return;
            var partsDifferenceBackBill = this.DataContext as PartsDifferenceBackBill;
            if(partsDifferenceBackBill == null)
                return;
            partsDifferenceBackBill.SourceCode = partsDifferenceBackBillSourceCode.SourceCode;
            partsDifferenceBackBill.PartsInboundCheckBillCode = partsDifferenceBackBillSourceCode.PartsInboundCheckBillCode;
            partsDifferenceBackBill.PartsSupplierId = partsDifferenceBackBillSourceCode.PartsSupplierId;
            partsDifferenceBackBill.PartsSupplierCode = partsDifferenceBackBillSourceCode.PartsSupplierCode;
            partsDifferenceBackBill.PartsSupplierName = partsDifferenceBackBillSourceCode.PartsSupplierName;
            foreach(var item in partsDifferenceBackBill.PartsDifferenceBackBillDtls) {
                partsDifferenceBackBill.PartsDifferenceBackBillDtls.Remove(item);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.Close();
            }
        }

        private void QueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsDifferenceBackBill = this.DataContext as PartsDifferenceBackBill;
            if(queryWindow == null || partsDifferenceBackBill == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new object[] {
                new FilterItem("Type", typeof(int), FilterOperator.IsEqualTo, partsDifferenceBackBill.Type)
            });
        }

        private bool isOneLoding = true;
        private void ComboBoxType_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            var partsDifferenceBackBill = this.DataContext as PartsDifferenceBackBill;
            if(!isOneLoding) {
                partsDifferenceBackBill.SourceCode = null;
                partsDifferenceBackBill.PartsInboundCheckBillCode = null;
                partsDifferenceBackBill.PartsSupplierId = null;
                partsDifferenceBackBill.PartsSupplierCode = null;
                partsDifferenceBackBill.PartsSupplierName = null;
                foreach(var item in partsDifferenceBackBill.PartsDifferenceBackBillDtls) {
                    partsDifferenceBackBill.PartsDifferenceBackBillDtls.Remove(item);
                }
            }
            isOneLoding = false;
        }
    }
}