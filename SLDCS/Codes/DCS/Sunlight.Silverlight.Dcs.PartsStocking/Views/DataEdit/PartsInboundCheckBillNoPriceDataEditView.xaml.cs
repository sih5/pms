﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.Custom;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsInboundCheckBillNoPriceDataEditView {
        private DataGridViewBase partsInboundCheckBillDetailDataGridView;
        private ObservableCollection<KeyValuePair> kvPartsSalesCategories;

        public ObservableCollection<KeyValuePair> KvPartsSalesCategories {
            get {
                return this.kvPartsSalesCategories ?? (this.kvPartsSalesCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        public PartsInboundCheckBillNoPriceDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsInboundCheckBillDataEditView_DataContextChanged;
        }

        private void PartsInboundCheckBillDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if(this.DataContext is PartsInboundCheckBill)
                this.textBoxBatchNumber.Text = string.Empty;
        }

        private PartsLogisticBatch PartsLogisticBatch {
            get;
            set;
        }

        private int partsPlanId {
            get;
            set;
        }

        private DataGridViewBase PartsInboundCheckBillDetailDataGridView {
            get {
                if(this.partsInboundCheckBillDetailDataGridView == null) {
                    this.partsInboundCheckBillDetailDataGridView = DI.GetDataGridView("PartsInboundCheckBillDetailForEditNoPrice");
                    this.partsInboundCheckBillDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsInboundCheckBillDetailDataGridView;
            }
        }

        private RadWindow RadWindow {
            get {
                return new RadWindow {
                    Header = PartsStockingUIStrings.DataEditView_Title_Examine_PartsInboundPackingDetails,
                    Content = new PartsInboundPackingDetailView {
                        DataContext = this.DataContext
                    },
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    Height = 480,
                    Width = 640,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize
                };
            }
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvPartsSalesCategories.Clear();
                foreach(var partsSalesCategories in loadOp.Entities)
                    this.KvPartsSalesCategories.Add(new KeyValuePair {
                        Key = partsSalesCategories.Id,
                        Value = partsSalesCategories.Name,
                    });
            }, null);
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(Utils.GetEntityLocalizedName(typeof(PartsInboundCheckBill), "PartsInboundCheckBillDetails"), null, () => this.PartsInboundCheckBillDetailDataGridView);
            dcsDetailGridView.SetValue(Grid.ColumnSpanProperty, 4);
            dcsDetailGridView.SetValue(Grid.RowProperty, 4);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            this.Root.Children.Add(dcsDetailGridView);
            this.textBoxBatchNumber.KeyDown += this.textBoxBatchNumber_KeyDown;
            this.radButtonBatchNumber.Click += radButtonBatchNumber_Click;
        }

        protected override void Reset() {
            foreach(var partsInboundCheckBill in this.DomainContext.PartsInboundCheckBills.ToArray()) {
                this.DomainContext.PartsInboundCheckBills.Detach(partsInboundCheckBill);
            }
            foreach(var partsInboundBill in this.DomainContext.PartsInboundPlans.ToArray()) {
                this.DomainContext.PartsInboundPlans.Detach(partsInboundBill);
            }
        }

        private void textBoxBatchNumber_KeyDown(object sender, KeyEventArgs e) {
            if(e.Key == Key.Enter) {
                if(this.PartsLogisticBatch == null)
                    return;
                //写在这里目的是防止，每次触发键盘事件，就会作判断
                var partsInboundCheckBill = this.DataContext as PartsInboundCheckBill;
                if(partsInboundCheckBill == null)
                    return;

                var batchNumber = this.textBoxBatchNumber.Text;
                if(string.IsNullOrWhiteSpace(batchNumber)) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsLogisticBatchItemDetail_BatchNumberIsNull);
                    return;
                }
                if(this.PartsLogisticBatch.PartsLogisticBatchItemDetails.All(r => r.BatchNumber != batchNumber)) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsLogisticBatchItemDetail_BatchNumberIsNotExist);
                    return;
                }
                if(partsInboundCheckBill.PartsInboundPackingDetails.Any(entity => entity.BatchNumber == batchNumber)) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsLogisticBatchItemDetail_BatchNumberIsAlreadyHave);
                    return;
                }

                //已经存在该批次的配件物流批次清单
                var partsLogisticBatchItemDetail = this.PartsLogisticBatch.PartsLogisticBatchItemDetails.FirstOrDefault(r => r.BatchNumber == batchNumber);
                if(partsLogisticBatchItemDetail != null) {
                    var checkBillDetail = partsInboundCheckBill.PartsInboundCheckBillDetails.SingleOrDefault(entity => entity.SparePartId == partsLogisticBatchItemDetail.SparePartId);
                    if(checkBillDetail == null)
                        return;
                    // 1.1）入库包装单清单 新增一条数据
                    var packingDetail = new PartsInboundPackingDetail {
                        BatchNumber = batchNumber,
                        Quantity = partsLogisticBatchItemDetail.OutboundAmount,
                        SparePartId = partsLogisticBatchItemDetail.SparePartId,
                        SparePartCode = partsLogisticBatchItemDetail.SparePartCode,
                        SparePartName = partsLogisticBatchItemDetail.SparePartName,
                        WarehouseAreaId = checkBillDetail.WarehouseAreaId,
                        WarehouseAreaCode = checkBillDetail.WarehouseAreaCode,
                        SerialNumber = partsInboundCheckBill.PartsInboundPackingDetails.Any() ? partsInboundCheckBill.PartsInboundPackingDetails.Max(entity => entity.SerialNumber) + 1 : 1
                    };
                    partsInboundCheckBill.PartsInboundPackingDetails.Add(packingDetail);
                    // 1.2）根据 入库包装单清单.配件Id=入库检验单清单.配件Id 更新 检验量=检验量+入库包装单清单.数量
                    checkBillDetail.InspectedQuantity += packingDetail.Quantity;
                }
            }
        }

        private void radButtonBatchNumber_Click(object sender, RoutedEventArgs e) {
            this.RadWindow.ShowDialog();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInboundPlanWithDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                //如库计划单ID
                partsPlanId = id;
                //创建当前操作的检验入库单
                var partsInboundCheckBill = this.CreateObjectToEdit<PartsInboundCheckBill>();
                var partsInboundPlan = loadOp.Entities.FirstOrDefault();
                if(partsInboundPlan == null)
                    return;
                partsInboundCheckBill.WarehouseName = partsInboundPlan.WarehouseName;
                partsInboundCheckBill.InboundType = partsInboundPlan.InboundType;
                partsInboundCheckBill.WarehouseCode = partsInboundPlan.WarehouseCode;
                partsInboundCheckBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                partsInboundCheckBill.PartsInboundPlanId = partsInboundPlan.Id;
                partsInboundCheckBill.WarehouseId = partsInboundPlan.WarehouseId;
                partsInboundCheckBill.StorageCompanyId = partsInboundPlan.StorageCompanyId;
                partsInboundCheckBill.StorageCompanyCode = partsInboundPlan.StorageCompanyCode;
                partsInboundCheckBill.StorageCompanyName = partsInboundPlan.StorageCompanyName;
                partsInboundCheckBill.StorageCompanyType = partsInboundPlan.StorageCompanyType;
                partsInboundCheckBill.BranchId = partsInboundPlan.BranchId;
                partsInboundCheckBill.BranchCode = partsInboundPlan.BranchCode;
                partsInboundCheckBill.BranchName = partsInboundPlan.BranchName;
                partsInboundCheckBill.CounterpartCompanyId = partsInboundPlan.CounterpartCompanyId;
                partsInboundCheckBill.CounterpartCompanyCode = partsInboundPlan.CounterpartCompanyCode;
                partsInboundCheckBill.CounterpartCompanyName = partsInboundPlan.CounterpartCompanyName;
                partsInboundCheckBill.CustomerAccountId = partsInboundPlan.CustomerAccountId;
                partsInboundCheckBill.OriginalRequirementBillId = partsInboundPlan.OriginalRequirementBillId;
                partsInboundCheckBill.OriginalRequirementBillCode = partsInboundPlan.OriginalRequirementBillCode;
                partsInboundCheckBill.OriginalRequirementBillType = partsInboundPlan.OriginalRequirementBillType;
                partsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.新建;
                partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                partsInboundCheckBill.GPMSPurOrderCode = partsInboundPlan.GPMSPurOrderCode;
                partsInboundCheckBill.PartsSalesCategoryId = partsInboundPlan.PartsSalesCategoryId;
                partsInboundCheckBill.PartsSalesCategory = partsInboundPlan.PartsSalesCategory;
                partsInboundCheckBill.PartsSalesCategoryName = partsInboundPlan.PartsSalesCategory.Name;

                //if(partsInboundCheckBill.InboundType != (int)DcsPartsInboundType.内部领入) {
                this.PartsLogisticBatch = partsInboundPlan.PartsLogisticBatchBillDetails.Select(v => v.PartsLogisticBatch).FirstOrDefault();
                //    if(this.PartsLogisticBatch == null) {
                //        UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsInboundPlan_PartsLogisticBatchIsNotExist);
                //        return;
                //   // }
                //}
                //查询库区负责人库区库位
                this.DomainContext.Load(this.DomainContext.GetWarehouseAreasWithAreaManagersQuery(BaseApp.Current.CurrentUserData.UserId).Where(r => r.AreaKind == (int)DcsAreaKind.库位 &&
                    r.WarehouseId == partsInboundCheckBill.WarehouseId && r.WarehouseAreaCategory.Category == (int)DcsAreaType.检验区 && r.Status != (int)DcsBaseDataStatus.作废), LoadBehavior.RefreshCurrent, loadOption => {
                        if(loadOption.HasError) {
                            if(!loadOption.IsErrorHandled)
                                loadOption.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOption);
                            return;
                        }
                        var warehouseArea = loadOption.Entities.OrderBy(v => v.Code).FirstOrDefault();
                        if(this.PartsLogisticBatch == null) {
                            foreach(var entityDetail in partsInboundPlan.PartsInboundPlanDetails.Where(p => p.PlannedAmount > 0)) {
                                partsInboundCheckBill.PartsInboundCheckBillDetails.Add(new PartsInboundCheckBillDetail {
                                    PartsInboundCheckBill = partsInboundCheckBill,
                                    SparePartId = entityDetail.SparePartId,
                                    SparePartCode = entityDetail.SparePartCode,
                                    SparePartName = entityDetail.SparePartName,
                                    PlannedAmount = entityDetail.PlannedAmount,
                                    InspectedQuantity = entityDetail.PlannedAmount,
                                    BatchInformation = PartsStockingUIStrings.DataEditView_PartsInboundCheckBillDetail_Constant_UnNeedBatch,
                                    SettlementPrice = entityDetail.Price,
                                    WarehouseAreaId = warehouseArea != null ? warehouseArea.Id : default(int),
                                    WarehouseAreaCode = warehouseArea != null ? warehouseArea.Code : default(string),
                                    POCode = entityDetail.POCode
                                });
                            }
                        } else {
                            foreach(var entityDetail in partsInboundPlan.PartsInboundPlanDetails.Where(p=>p.PlannedAmount>0)) {
                                if(PartsLogisticBatch.PartsLogisticBatchItemDetails.Any(r => r.BatchNumber != null && entityDetail.SparePartId == r.SparePartId)) {
                                    partsInboundCheckBill.PartsInboundCheckBillDetails.Add(new PartsInboundCheckBillDetail {
                                        PartsInboundCheckBill = partsInboundCheckBill,
                                        SparePartId = entityDetail.SparePartId,
                                        SparePartCode = entityDetail.SparePartCode,
                                        SparePartName = entityDetail.SparePartName,
                                        PlannedAmount = entityDetail.PlannedAmount,
                                        InspectedQuantity = 0,
                                        //InspectedQuantity = this.SetInspectedQuantityValueInternal(entityDetail),
                                        BatchInformation = PartsStockingUIStrings.DataEditView_PartsInboundCheckBillDetail_Constant_NeedBatch,
                                        SettlementPrice = entityDetail.Price,
                                        WarehouseAreaId = warehouseArea != null ? warehouseArea.Id : default(int),
                                        WarehouseAreaCode = warehouseArea != null ? warehouseArea.Code : default(string),
                                        POCode = entityDetail.POCode
                                    });
                                } else {
                                    partsInboundCheckBill.PartsInboundCheckBillDetails.Add(new PartsInboundCheckBillDetail {
                                        PartsInboundCheckBill = partsInboundCheckBill,
                                        SparePartId = entityDetail.SparePartId,
                                        SparePartCode = entityDetail.SparePartCode,
                                        SparePartName = entityDetail.SparePartName,
                                        PlannedAmount = entityDetail.PlannedAmount,
                                        InspectedQuantity = entityDetail.PlannedAmount,
                                        BatchInformation = PartsStockingUIStrings.DataEditView_PartsInboundCheckBillDetail_Constant_UnNeedBatch,
                                        SettlementPrice = entityDetail.Price,
                                        WarehouseAreaId = warehouseArea != null ? warehouseArea.Id : default(int),
                                        WarehouseAreaCode = warehouseArea != null ? warehouseArea.Code : default(string),
                                        POCode = entityDetail.POCode
                                    });
                                }
                            }
                        }
                    }, null);
            }, null);
        }

        private string SetBatchInfoValueInternal(PartsInboundPlanDetail partsInboundPlanDetail) {
            var filterBatch = this.PartsLogisticBatch.PartsLogisticBatchItemDetails.Where(r => r.SparePartId == partsInboundPlanDetail.SparePartId).ToArray();
            if(filterBatch.Any(e => !string.IsNullOrWhiteSpace(e.BatchNumber)))
                return PartsStockingUIStrings.DataEditView_PartsInboundCheckBillDetail_Constant_NeedBatch;
            if(filterBatch.Count() == 1 && filterBatch.All(e => string.IsNullOrWhiteSpace(e.BatchNumber)))
                return PartsStockingUIStrings.DataEditView_PartsInboundCheckBillDetail_Constant_UnNeedBatch;
            return default(string);
        }

        private int SetInspectedQuantityValueInternal(PartsInboundPlanDetail partsInboundPlanDetail) {
            var filterBatch = this.PartsLogisticBatch.PartsLogisticBatchItemDetails.Where(r => r.SparePartId == partsInboundPlanDetail.SparePartId).ToArray();
            if(filterBatch.Any(e => !string.IsNullOrWhiteSpace(e.BatchNumber)))
                return default(int);
            if(filterBatch.Count() == 1 && filterBatch.All(e => string.IsNullOrWhiteSpace(e.BatchNumber)))
                return partsInboundPlanDetail.PlannedAmount;
            return default(int);
        }

        protected override void OnEditSubmitting() {
            var partsInboundCheckBill = this.DataContext as PartsInboundCheckBill;
            if(partsInboundCheckBill == null || !this.PartsInboundCheckBillDetailDataGridView.CommitEdit())
                return;
            if(partsInboundCheckBill.ValidationErrors.Any())
                return;

            //if(partsInboundCheckBill.PartsInboundCheckBillDetails.Any(r => r.InspectedQuantity != r.PlannedAmount)) {
            //    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsInboundCheckBill_InspectedQuantityIsNotValid);
            //    return;
            //}
            foreach(var items in partsInboundCheckBill.PartsInboundCheckBillDetails.GroupBy(ex => ex.SparePartCode).Where(obj => obj.Count() > 1)) {
                UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_SparePartHasRepeat, items.FirstOrDefault().SparePartCode));
                return;
            }
            if(partsInboundCheckBill.PartsInboundCheckBillDetails.Any(r => r.InspectedQuantity > r.PlannedAmount)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsInboundCheckBill_PlannedAmountIsNotValid);
                return;
            }
            if(partsInboundCheckBill.PartsInboundCheckBillDetails.Sum(r => r.InspectedQuantity) == 0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsInboundCheckBill_InspectedQuantityIsNull);
                return;
            }
            foreach(var t in partsInboundCheckBill.PartsInboundCheckBillDetails) {
                if(t.InspectedQuantity == 0) {
                    partsInboundCheckBill.PartsInboundCheckBillDetails.Remove(t);
                }
            }
            ((IEditableObject)partsInboundCheckBill).EndEdit();
            try {
                if(partsInboundCheckBill.Can生成配件入库检验单)
                    partsInboundCheckBill.生成配件入库检验单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Text_PrintPartLable, () => {
                this.PrintLabelForPartsInboundCheckDataEditView.SetObjectToEditById(partsPlanId);
                this.LabelPrintWindow.ShowDialog();
                base.OnEditSubmitting();
            }, () => base.OnEditSubmitting(),PartsStockingUIStrings.DataEditView_Text_Yes,PartsStockingUIStrings.DataEditView_Text_No);
        }

        private RadWindow labelPrintWindow;
        private DataEditViewBase printLabelForPartsInboundCheckDataEditView;

        private DataEditViewBase PrintLabelForPartsInboundCheckDataEditView {
            get {
                return this.printLabelForPartsInboundCheckDataEditView ?? (this.printLabelForPartsInboundCheckDataEditView = DI.GetDataEditView("PrintLabelForPartsInboundCheck"));
            }
        }

        private RadWindow LabelPrintWindow {
            get {
                return this.labelPrintWindow ?? (this.labelPrintWindow = new RadWindow {
                    Content = this.PrintLabelForPartsInboundCheckDataEditView,
                    Header = PartsStockingUIStrings.DataEditView_Title_PartInformation,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsInboundCheckBill;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
