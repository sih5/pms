﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsInboundPlanDetailDataEditView {
        private ObservableCollection<PartsInboundPlanDetail> partsInboundPlanDetails;
        private DataGridViewBase partsInboundPlanDetailDataGridView;

        public PartsInboundPlanDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase PartsInboundPlanDetailDataGridView {
            get {
                if(this.partsInboundPlanDetailDataGridView == null) {
                    this.partsInboundPlanDetailDataGridView = DI.GetDataGridView("PartsInboundPlanDetailForDetail");
                    this.partsInboundPlanDetailDataGridView.DomainContext = this.DomainContext;
                    this.partsInboundPlanDetailDataGridView.DataContext = this;
                }
                return this.partsInboundPlanDetailDataGridView;
            }
        }

        private void CreateUI() {
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            dcsDetailGridView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/export.png", UriKind.Relative),
                Command = new DelegateCommand(this.ExportData)
            });
            dcsDetailGridView.Register(Utils.GetEntityLocalizedName(typeof(PartsInboundPlan), "PartsInboundPlanDetails"), null, () => this.PartsInboundPlanDetailDataGridView);
            dcsDetailGridView.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(dcsDetailGridView);
        }

        private void ExportData() {
            ((DcsDataGridViewBase)this.PartsInboundPlanDetailDataGridView).ExportData();
        }

        private void LoadEntityToEdit(int id) {
            this.PartsInboundPlanDetails.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsInboundPlanDetailsQuery().Where(e => e.PartsInboundPlanId == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var serialNumber = 1;
                foreach(var entity in loadOp.Entities) {
                    this.PartsInboundPlanDetails.Add(new PartsInboundPlanDetail {
                        SerialNumber = serialNumber++,
                        SparePartCode = entity.SparePartCode,
                        SparePartName = entity.SparePartName,
                        Remark = entity.Remark,
                        PlannedAmount = entity.PlannedAmount,
                        InspectedQuantity = entity.InspectedQuantity
                    });
                }
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsInboundPlanDetail;
            }
        }

        public ObservableCollection<PartsInboundPlanDetail> PartsInboundPlanDetails {
            get {
                return this.partsInboundPlanDetails ?? (this.partsInboundPlanDetails = new ObservableCollection<PartsInboundPlanDetail>());
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
