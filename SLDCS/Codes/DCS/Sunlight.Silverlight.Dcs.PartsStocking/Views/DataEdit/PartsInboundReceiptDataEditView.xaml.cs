﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.Custom;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsInboundReceiptDataEditView {
        private DataGridViewBase partsInboundCheckBillDataGridView;
        private int partsPlanId {
            get;
            set;
        }

        public PartsInboundReceiptDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override void OnEditSubmitting() {
            var partsInboundCheckBill = this.DataContext as PartsInboundCheckBill;
            if(partsInboundCheckBill == null || !this.PartsInboundCheckBillDataGridView.CommitEdit())
                return;
            if(partsInboundCheckBill.PartsInboundCheckBillDetails.Any(r => r.ThisInspectedQuantity + r.InspectedQuantity > r.PlannedAmount)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsInboundReceipt_PlannedAmountIsNotValid);
                return;
            }
            if(partsInboundCheckBill.PartsInboundCheckBillDetails.Sum(r => r.ThisInspectedQuantity) == 0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsInboundReceipt_ThisInspectedQuantityIsNull);
                return;
            }
            foreach(var t in partsInboundCheckBill.PartsInboundCheckBillDetails) {
                if(!t.ThisInspectedQuantity.HasValue || t.ThisInspectedQuantity.Value == 0) {
                    partsInboundCheckBill.PartsInboundCheckBillDetails.Remove(t);
                }
            }
            ((IEditableObject)partsInboundCheckBill).EndEdit();
            try {
                if(partsInboundCheckBill.Can生成配件收货单)
                    partsInboundCheckBill.生成配件收货单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
        protected override void OnEditSubmitted() {
            var partsInboundCheckBill = this.DataContext as PartsInboundCheckBill;
            if(partsInboundCheckBill == null)
                return;
            DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Text_IsPrintShelves, () => {
                //BasePrintWindow printWindow = new PartsInboundCheckBillPrintWindow
                //{
                //    Header = "上架指示单打印",
                //    PartsInboundCheckBillId = partsInboundCheckBill.Id
                //};
                //printWindow.ShowDialog();
                SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataEditView_Text_PrintShelves, "ReportPartsInboundCheckBillNew", null, true, new Tuple<string, string>("paraPartsInboundCheckBillId", partsInboundCheckBill.Id.ToString()));

            });

        }
        private void CreateUI() {
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsStockingUIStrings.DataEditView_Text_ReceiptPartDetails, null, () => this.PartsInboundCheckBillDataGridView);
            dcsDetailGridView.SetValue(Grid.ColumnSpanProperty, 4);
            dcsDetailGridView.SetValue(Grid.RowProperty, 4);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            this.Root.Children.Add(dcsDetailGridView);
            //this.textBoxBatchNumber.KeyDown += this.textBoxBatchNumber_KeyDown;
            //this.radButtonBatchNumber.Click += radButtonBatchNumber_Click;
        }
        private DataGridViewBase PartsInboundCheckBillDataGridView {
            get {
                if(this.partsInboundCheckBillDataGridView == null) {
                    this.partsInboundCheckBillDataGridView = DI.GetDataGridView("PartsInboundCheckBillForReceipt");
                    this.partsInboundCheckBillDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsInboundCheckBillDataGridView;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInboundPlanReceiptDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                //如库计划单ID
                partsPlanId = id;
                //创建当前操作的检验入库单
                var partsInboundCheckBill = this.CreateObjectToEdit<PartsInboundCheckBill>();
                var partsInboundPlan = loadOp.Entities.FirstOrDefault();
                if(partsInboundPlan == null)
                    return;
                partsInboundCheckBill.WarehouseName = partsInboundPlan.WarehouseName;
                partsInboundCheckBill.InboundType = partsInboundPlan.InboundType;
                partsInboundCheckBill.WarehouseCode = partsInboundPlan.WarehouseCode;
                partsInboundCheckBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                partsInboundCheckBill.PartsInboundPlanId = partsInboundPlan.Id;
                partsInboundCheckBill.PartsInboundPlanCode = partsInboundPlan.Code;
                partsInboundCheckBill.SourceCode = partsInboundPlan.SourceCode;
                partsInboundCheckBill.WarehouseId = partsInboundPlan.WarehouseId;
                partsInboundCheckBill.StorageCompanyId = partsInboundPlan.StorageCompanyId;
                partsInboundCheckBill.StorageCompanyCode = partsInboundPlan.StorageCompanyCode;
                partsInboundCheckBill.StorageCompanyName = partsInboundPlan.StorageCompanyName;
                partsInboundCheckBill.StorageCompanyType = partsInboundPlan.StorageCompanyType;
                partsInboundCheckBill.BranchId = partsInboundPlan.BranchId;
                partsInboundCheckBill.BranchCode = partsInboundPlan.BranchCode;
                partsInboundCheckBill.BranchName = partsInboundPlan.BranchName;
                partsInboundCheckBill.CounterpartCompanyId = partsInboundPlan.CounterpartCompanyId;
                partsInboundCheckBill.CounterpartCompanyCode = partsInboundPlan.CounterpartCompanyCode;
                partsInboundCheckBill.CounterpartCompanyName = partsInboundPlan.CounterpartCompanyName;
                partsInboundCheckBill.CustomerAccountId = partsInboundPlan.CustomerAccountId;
                partsInboundCheckBill.OriginalRequirementBillId = partsInboundPlan.OriginalRequirementBillId;
                partsInboundCheckBill.OriginalRequirementBillCode = partsInboundPlan.OriginalRequirementBillCode;
                partsInboundCheckBill.OriginalRequirementBillType = partsInboundPlan.OriginalRequirementBillType;
                partsInboundCheckBill.Status = (int)DcsPartsInboundCheckBillStatus.新建;
                partsInboundCheckBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                partsInboundCheckBill.GPMSPurOrderCode = partsInboundPlan.GPMSPurOrderCode;
                partsInboundCheckBill.PartsSalesCategoryId = partsInboundPlan.PartsSalesCategoryId;
                partsInboundCheckBill.PartsSalesCategory = partsInboundPlan.PartsSalesCategory;
                partsInboundCheckBill.PartsSalesCategoryName = partsInboundPlan.PartsSalesCategory.Name;
                partsInboundCheckBill.SAPPurchasePlanCode = partsInboundPlan.SAPPurchasePlanCode;//SAP采购计划单号
                partsInboundCheckBill.ERPSourceOrderCode = partsInboundPlan.ERPSourceOrderCode;

                var isPacking = partsInboundPlan.WarehouseName != "重庆库" && partsInboundPlan.WarehouseName != "马钢库";
                foreach(var entityDetail in partsInboundPlan.PartsInboundPlanDetails.Where(p => p.PlannedAmount - (p.InspectedQuantity ?? 0) > 0)) {
                    var type=entityDetail.SparePart.PartType==(int)DcsSparePartPartType.包材?false:true;
                    partsInboundCheckBill.PartsInboundCheckBillDetails.Add(new PartsInboundCheckBillDetail {
                        PartsInboundCheckBill = partsInboundCheckBill,
                        SparePartId = entityDetail.SparePartId,
                        SparePartCode = entityDetail.SparePartCode,
                        SparePartName = entityDetail.SparePartName,
                        PlannedAmount = entityDetail.PlannedAmount,
                        BatchInformation = PartsStockingUIStrings.DataEditView_PartsInboundCheckBillDetail_Constant_UnNeedBatch,
                        SettlementPrice = entityDetail.Price,
                        POCode = entityDetail.POCode,
                        WarehouseAreaId = default(int),
                        WarehouseAreaCode = "-",
                        InspectedQuantity = (entityDetail.InspectedQuantity.HasValue ? entityDetail.InspectedQuantity.Value : default(int)),
                        IsPacking = (isPacking && type)? true : false,
                        SupplierPartCode = entityDetail.SupplierPartCode,
                        //Remark=entityDetail.SparePart.PartType.ToString()
                        //ThisInspectedQuantity = entityDetail.PlannedAmount - (entityDetail.InspectedQuantity.HasValue ? entityDetail.InspectedQuantity.Value : default(int)),
                    });
                };
                if (partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.销售退货) {
                    foreach (var detail in partsInboundCheckBill.PartsInboundCheckBillDetails) {
                        detail.ThisInspectedQuantity = detail.PlannedAmount - detail.InspectedQuantity;
                    }
                }
                
            }, null);
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsInboundReceipt;
            }
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

    }
}
