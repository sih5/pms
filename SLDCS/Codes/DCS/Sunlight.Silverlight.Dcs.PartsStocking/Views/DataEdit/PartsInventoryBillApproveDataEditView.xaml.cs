﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsInventoryBillApproveDataEditView {
        public KeyValueManager KeyValueManager = new KeyValueManager();
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private DataGridViewBase partsInventoryBillForApproveDataGridView;
        private readonly string[] kvNames = new[] {
            "Area_Category"
        };

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                if(this.kvWarehouses == null)
                    this.kvWarehouses = new ObservableCollection<KeyValuePair>();
                return this.kvWarehouses;
            }
        }

        public DataGridViewBase PartsInventoryBillForApproveDataGridView {
            get {
                if(this.partsInventoryBillForApproveDataGridView == null) {
                    this.partsInventoryBillForApproveDataGridView = DI.GetDataGridView("PartsInventoryBillForEditApprove");
                    this.partsInventoryBillForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsInventoryBillForApproveDataGridView;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsInventoryBill;
            }
        }

        public PartsInventoryBillApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

         private PartsInventoryBillForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsInventoryBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsInventoryBillForUploadDataEditPanel)DI.GetDataEditPanel("PartsInventoryBillForUpload"));
            }
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();
            this.DomainContext.Load(this.DomainContext.GetWarehousesWithStorageCompanyQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var warehouse in loadOp.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
                }
            }, null);

            FileUploadDataEditPanels.isHiddenButtons = true;
            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 5);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.Root.Children.Add(FileUploadDataEditPanels);

            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(PartsStockingUIStrings.DataEditView_GroupTitle_PartsInventoryDetail, null, () => this.PartsInventoryBillForApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 4);
            this.LayoutRoot.Children.Add(detailDataEditView);
            this.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = PartsStockingUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            }, true);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInventoryBillsWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null) { 
                    this.SetObjectToEdit(entity);
                    FileUploadDataEditPanels.FilePath = entity.Path;
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            ((IEditableObject)partsInventoryBill).EndEdit();
            try {
                if(partsInventoryBill.Can总部审批配件盘点单)
                    partsInventoryBill.总部审批配件盘点单();
                ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_OperationSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }

        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        } 
        //驳回
        private void RejectCurrentData() {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            ((IEditableObject)partsInventoryBill).EndEdit();
            try {
                if(partsInventoryBill.Can总部终审驳回配件盘点单)
                    partsInventoryBill.总部终审驳回配件盘点单();
                ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_RejectSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }
    }
}
