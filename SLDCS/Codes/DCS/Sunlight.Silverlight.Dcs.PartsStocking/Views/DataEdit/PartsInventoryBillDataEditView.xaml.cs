﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsInventoryBillDataEditView {
        public KeyValueManager KeyValueManager = new KeyValueManager();
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private ObservableCollection<KeyValuePair> KvNewAreaCategories = new ObservableCollection<KeyValuePair>();
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private string strFileName;
        protected Action<string> UploadFileSuccessedProcessing;
        private DataGridViewBase partsInventoryDetailForEditDataGridView;
        private readonly string[] kvNames = {
            "Area_Category"
        };
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsInventoryBill = this.DataContext as PartsInventoryBill;
                        if(partsInventoryBill == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPartsInventoryDetailAsync(e.HandlerData.CustomData["Path"].ToString(), partsInventoryBill.WarehouseId, partsInventoryBill.WarehouseAreaCategory, BaseApp.Current.CurrentUserData.EnterpriseId);
                        this.excelServiceClient.ImportPartsInventoryDetailCompleted -= excelServiceClient_ImportPartsInventoryDetailCompleted;
                        this.excelServiceClient.ImportPartsInventoryDetailCompleted += excelServiceClient_ImportPartsInventoryDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void excelServiceClient_ImportPartsInventoryDetailCompleted(object sender, ImportPartsInventoryDetailCompletedEventArgs e) {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            foreach(var detail in partsInventoryBill.PartsInventoryDetails) {
                partsInventoryBill.PartsInventoryDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    partsInventoryBill.PartsInventoryDetails.Add(new PartsInventoryDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        WarehouseAreaId = data.WarehouseAreaId,
                        WarehouseAreaCode = data.WarehouseAreaCode,
                        CurrentStorage = data.CurrentStorage
                    });
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }
        private PartsInventoryBillForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsInventoryBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsInventoryBillForUploadDataEditPanel)DI.GetDataEditPanel("PartsInventoryBillForUpload"));
            }
        }
        public DataGridViewBase PartsInventoryDetailForEditDataGridView {
            get {
                if(this.partsInventoryDetailForEditDataGridView == null) {
                    this.partsInventoryDetailForEditDataGridView = DI.GetDataGridView("PartsInventoryDetailForEdit");
                    this.partsInventoryDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsInventoryDetailForEditDataGridView;
            }
        }

        public ObservableCollection<KeyValuePair> KvAreaCategories {
            get {
                return KvNewAreaCategories;
            }
        }

        public PartsInventoryBillDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsInventoryBillDataEditView_DataContextChanged;

            this.KeyValueManager.LoadData(() => {
                foreach(var keyValue in this.KeyValueManager[this.kvNames[0]].Where(entity => entity.Key == (int)DcsAreaType.保管区 || entity.Key == (int)DcsAreaType.问题区)) {
                    KvNewAreaCategories.Add(keyValue);
                }
            });
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsInventoryBill;
            }
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsStockingUIStrings.Upload_Error3);
                    e.Handled = true;
                }
            }
        }

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        private int warehouseId = 0, warehouseAreaCategory = 0;
        private void PartsInventoryBillDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            this.warehouseId = partsInventoryBill.WarehouseId;
            this.warehouseAreaCategory = partsInventoryBill.WarehouseAreaCategory;
            partsInventoryBill.PropertyChanged += partsInventoryBill_PropertyChanged;
        }

        private void partsInventoryBill_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            switch(e.PropertyName) {
                case "WarehouseId":
                    if(partsInventoryBill.PartsInventoryDetails.Any()) {
                        if(partsInventoryBill.WarehouseId != this.warehouseId)
                            DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Notification_PartsInventoryBill_NotifyClearDetail, () => {
                                foreach(var detail in partsInventoryBill.PartsInventoryDetails.ToArray()) {
                                    partsInventoryBill.PartsInventoryDetails.Remove(detail);
                                }
                                this.warehouseId = partsInventoryBill.WarehouseId;
                                var kvWarehouse = this.KvWarehouses.SingleOrDefault(obj => obj.Key == partsInventoryBill.WarehouseId);
                                if(kvWarehouse == null)
                                    return;
                                var warehouse = kvWarehouse.UserObject as Warehouse;
                                if(warehouse == null)
                                    return;
                                partsInventoryBill.BranchId = warehouse.BranchId;
                                partsInventoryBill.WarehouseId = warehouse.Id;
                                partsInventoryBill.WarehouseCode = warehouse.Code;
                                partsInventoryBill.WarehouseName = warehouse.Name;
                                partsInventoryBill.StorageCompanyId = warehouse.StorageCompanyId;
                                partsInventoryBill.StorageCompanyCode = warehouse.Company.Code;
                                partsInventoryBill.StorageCompanyName = warehouse.Company.Name;
                                partsInventoryBill.StorageCompanyType = warehouse.StorageCompanyType;
                            }, () => {
                                partsInventoryBill.WarehouseId = this.warehouseId;
                            });
                    } else {
                        this.warehouseId = partsInventoryBill.WarehouseId;
                        var kvWarehouse = this.KvWarehouses.SingleOrDefault(obj => obj.Key == partsInventoryBill.WarehouseId);
                        if(kvWarehouse == null)
                            return;
                        var warehouse = kvWarehouse.UserObject as Warehouse;
                        if(warehouse == null)
                            return;
                        partsInventoryBill.BranchId = warehouse.BranchId;
                        partsInventoryBill.WarehouseId = warehouse.Id;
                        partsInventoryBill.WarehouseCode = warehouse.Code;
                        partsInventoryBill.WarehouseName = warehouse.Name;
                        partsInventoryBill.StorageCompanyId = warehouse.StorageCompanyId;
                        partsInventoryBill.StorageCompanyCode = warehouse.Company.Code;
                        partsInventoryBill.StorageCompanyName = warehouse.Company.Name;
                        partsInventoryBill.StorageCompanyType = warehouse.StorageCompanyType;
                    }
                    break;
                case "WarehouseAreaCategory":
                    if(partsInventoryBill.PartsInventoryDetails.Any()) {
                        if(partsInventoryBill.WarehouseAreaCategory != this.warehouseAreaCategory)
                            DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Notification_PartsInventoryBill_NotifyClearDetail, () => {
                                foreach(var detail in partsInventoryBill.PartsInventoryDetails.ToArray()) {
                                    partsInventoryBill.PartsInventoryDetails.Remove(detail);
                                }
                                this.warehouseAreaCategory = partsInventoryBill.WarehouseAreaCategory;
                            }, () => {
                                partsInventoryBill.WarehouseAreaCategory = this.warehouseAreaCategory;
                            });
                    } else {
                        this.warehouseAreaCategory = partsInventoryBill.WarehouseAreaCategory;
                    }
                    break;
            }
        }

        private void CreateUI() {
            this.DomainContext.Load(this.DomainContext.GetWarehousesWithStorageCompanyQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && !e.WmsInterface && e.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var _entities = loadOp.Entities.OrderBy(i => i.Name);
                foreach(var warehouse in _entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
                }
            }, null);
            this.KeyValueManager.LoadData();

            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 5);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.Root.Children.Add(FileUploadDataEditPanels);

            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1));
            var detail = new DcsDetailDataEditView();
            detail.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detail.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Title_ExportTemplate,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            detail.Register(PartsStockingUIStrings.DataEditView_GroupTitle_PartsInventoryDetail, null, this.PartsInventoryDetailForEditDataGridView);
            Grid.SetColumn(detail, 2);
            this.LayoutRoot.Children.Add(detail);
        }

        private const string EXPORT_DATA_FILE_NAME = "导出配件盘点单清单模板.xlsx";
        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_WarehouseAreaCode,
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }
        private ICommand exportFileCommand;
        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }


        private void ShowFileDialog() {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill.WarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryBill_WarehouseId);
                return;
            }
            if(partsInventoryBill.WarehouseAreaCategory == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_AreaCategoryNotNull);
                return;
            }
            this.Uploader.ShowFileDialog();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInventoryBillsWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var partsInventoryBill = loadOp.Entities.SingleOrDefault();
                if(partsInventoryBill == null)
                    return;
                FileUploadDataEditPanels.FilePath = partsInventoryBill.Path;
                this.DataContext = partsInventoryBill;
                this.SetObjectToEdit(partsInventoryBill);
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!PartsInventoryDetailForEditDataGridView.CommitEdit())
                return;
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            if(partsInventoryBill.PartsInventoryDetails.GroupBy(r => new {
                r.SparePartId,
                r.WarehouseAreaId
            }).Any(r => r.Count() > 1)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_Over);
                return;
            }
            foreach(var item in partsInventoryBill.PartsInventoryDetails){
                item.Ifcover = true;
            }
            partsInventoryBill.Path = FileUploadDataEditPanels.FilePath;
            partsInventoryBill.ValidationErrors.Clear();
            if(partsInventoryBill.WarehouseId == default(int))
                partsInventoryBill.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_BorrowBill_WarehouseIsNull, new[] { "WarehouseId" }));
            if(partsInventoryBill.WarehouseAreaCategory == default(int))
                partsInventoryBill.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_BorrowBill_WarehouseAreaCategoryIsNull, new[] { "WarehouseAreaCategory" }));
            if(!partsInventoryBill.PartsInventoryDetails.Any())
                partsInventoryBill.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_PartsInventoryDetails_Null, new[] { "PartsInventoryDetails" }));
            if(partsInventoryBill.HasValidationErrors) {
                UIHelper.ShowNotification(string.Join(Environment.NewLine, partsInventoryBill.ValidationErrors));
                return;
            }
            ((IEditableObject)partsInventoryBill).EndEdit();
            base.OnEditSubmitting();
        }
    }
}
