﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsInventoryBillForInitialApproveDataEditView {
         private ObservableCollection<KeyValuePair> kvWarehouses;
        private ObservableCollection<KeyValuePair> kvBranchs;
        private KeyValueManager keyValueManager;
        private DataGridViewBase partsInventoryBillForApproveDataGridView;
        private ButtonItem rejectBtn;
        private readonly string[] kvNames = new[] {
            "Area_Category"
        };
        public PartsInventoryBillForInitialApproveDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_InitApprovePartsInventoryBill;
            }
        }

        public ObservableCollection<KeyValuePair> KvBranchs {
            get {
                if(this.kvBranchs == null)
                    this.kvBranchs = new ObservableCollection<KeyValuePair>();
                return this.kvBranchs;
            }
        }
        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                if(this.kvWarehouses == null)
                    this.kvWarehouses = new ObservableCollection<KeyValuePair>();
                return this.kvWarehouses;
            }
        }
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public object KvWarehouseAreaCategorys {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
        public DataGridViewBase PartsInventoryBillForApproveDataGridView {
            get {
                if(this.partsInventoryBillForApproveDataGridView == null) {
                    this.partsInventoryBillForApproveDataGridView = DI.GetDataGridView("PartsInventoryBillForEditApprove");
                    this.partsInventoryBillForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsInventoryBillForApproveDataGridView;
            }
        }
        private AgentsPartsInventoryBillForUploadDataEditPanel fileUploadDataEditPanels;
        public AgentsPartsInventoryBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (AgentsPartsInventoryBillForUploadDataEditPanel)DI.GetDataEditPanel("AgentsPartsInventoryBillForUpload"));
            }
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        private void CreateUI() {
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvWarehouses.Clear();
                foreach(var warehouse in loadOp.Entities)
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
            }, null);
            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranchs.Clear();
                foreach(var branchs in loadOp.Entities)
                    this.KvBranchs.Add(new KeyValuePair {
                        Key = branchs.Id,
                        Value = branchs.Name,
                        UserObject = branchs
                    });
            }, null);

            FileUploadDataEditPanels.isHiddenButtons = true;
            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 7);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);

            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsInventoryBill), "PartsInventoryDetails"), null, () => this.PartsInventoryBillForApproveDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 4);
            this.Root.Children.Add(detailDataEditView);
            this.rejectBtn = new ButtonItem {
                Command = new DelegateCommand(this.RejecrCurrentData),
                Title = PartsStockingUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            };
            this.RegisterButton(rejectBtn);
        }
        protected override void OnEditSubmitting() {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            ((IEditableObject)partsInventoryBill).EndEdit();
            try {
                if(partsInventoryBill.Status == (int)DcsPartsInventoryBillStatus.已结果录入) {
                    if(partsInventoryBill.Can总部初审配件盘点单)
                        partsInventoryBill.总部初审配件盘点单();
                    ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_OperationSuccess);
                }else if(partsInventoryBill.Status == (int)DcsPartsInventoryBillStatus.初审通过) {
                    if(partsInventoryBill.Can总部审核配件盘点单)
                        partsInventoryBill.总部审核配件盘点单();
                    ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_OperationSuccess);
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }

        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        } 
        //驳回
        private void RejecrCurrentData() {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            ((IEditableObject)partsInventoryBill).EndEdit();
            try {
                if(partsInventoryBill.Can驳回配件盘点单)
                    partsInventoryBill.驳回配件盘点单();
                ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_RejectSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInventoryBillsWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if (entity != null) { 
                    this.SetObjectToEdit(entity);
                    FileUploadDataEditPanels.FilePath = entity.Path;
                }
            }, null);
        }
    }
}
