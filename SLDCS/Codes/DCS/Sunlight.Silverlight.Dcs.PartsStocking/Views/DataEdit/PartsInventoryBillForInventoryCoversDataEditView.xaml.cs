﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.Command;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsInventoryBillForInventoryCoversDataEditView {
        private DataGridViewBase partsInventoryDetailForInventoryCovers;
        private ButtonItem rejectBtn;

        public DataGridViewBase PartsInventoryDetailForInventoryCovers {
            get {
                if(this.partsInventoryDetailForInventoryCovers == null) {
                    this.partsInventoryDetailForInventoryCovers = DI.GetDataGridView("PartsInventoryDetailForInventoryCovers");
                    this.partsInventoryDetailForInventoryCovers.DomainContext = this.DomainContext;
                }
                return this.partsInventoryDetailForInventoryCovers;
            }
        }

        public PartsInventoryBillForInventoryCoversDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var detail = new DcsDetailDataEditView();
            detail.UnregisterButton(detail.InsertButton);
            detail.UnregisterButton(detail.DeleteButton);
            detail.Register(PartsStockingUIStrings.DataEditView_GroupTitle_PartsInventoryDetail, null, this.PartsInventoryDetailForInventoryCovers);
            this.LayoutRoot.Children.Add(detail);
            this.rejectBtn = new ButtonItem {
                Command = new DelegateCommand(this.RejecrCurrentData),
                Title = PartsStockingUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            };
            this.RegisterButton(rejectBtn);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_InventoryCovers;
            }
        }
        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }
        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }

        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
        private void RejecrCurrentData() {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            ((IEditableObject)partsInventoryBill).EndEdit();
            try {
                if(partsInventoryBill.Can总部库存覆盖驳回配件盘点单)
                    partsInventoryBill.总部库存覆盖驳回配件盘点单();
                ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_RejectSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
         //   base.OnEditSubmitting();
        }


        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInventoryBillsWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var partsInventoryBill = loadOp.Entities.SingleOrDefault();
                if(partsInventoryBill == null)
                    return;
                foreach(var detail in partsInventoryBill.PartsInventoryDetails) {
                    detail.Ifcover = true;
                }
                this.SetObjectToEdit(partsInventoryBill);
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        protected override void OnEditSubmitting() {
            if(!this.PartsInventoryDetailForInventoryCovers.CommitEdit())
                return;
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            //if(partsInventoryBill.PartsInventoryDetails.Any(e => e.StorageAfterInventory < 0)) {
            //    UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Validation_PartsInventoryDetail_PartsInventoryDetailMustGreaterThanZero);
            //    return;
            //}
            ((IEditableObject)partsInventoryBill).EndEdit();
            try {
                if(partsInventoryBill.Can库存覆盖配件盘点单)
                    partsInventoryBill.库存覆盖配件盘点单();
                ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_OperationSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
          // base.OnEditSubmitting();
        }
    }
}
