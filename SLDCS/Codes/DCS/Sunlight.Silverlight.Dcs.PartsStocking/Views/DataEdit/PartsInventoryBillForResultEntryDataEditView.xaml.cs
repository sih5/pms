﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsInventoryBillForResultEntryDataEditView {
        private DataGridViewBase partsInventoryDetailForResultEntry;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public DataGridViewBase PartsInventoryDetailForResultEntry {
            get {
                if(this.partsInventoryDetailForResultEntry == null) {
                    this.partsInventoryDetailForResultEntry = DI.GetDataGridView("PartsInventoryDetailForResultEntry");
                    this.partsInventoryDetailForResultEntry.DomainContext = this.DomainContext;
                }
                return this.partsInventoryDetailForResultEntry;
            }
        }
        private RadUpload uploader;
        protected Action<string> UploadFileSuccessedProcessing;
        private string strFileName;

        public PartsInventoryBillForResultEntryDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_ResultEntry;
            }
        }

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsInventoryBill = this.DataContext as PartsInventoryBill;
                        if(partsInventoryBill == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        var item = this.DataContext as PartsInventoryBill;
                        if(item == null)
                            return;
                        this.excelServiceClient.ImportPartsInventoryDetailByBillIdAsync(item.Id, e.HandlerData.CustomData["Path"].ToString(), item.WarehouseId, item.WarehouseAreaCategory, BaseApp.Current.CurrentUserData.EnterpriseId);
                        this.excelServiceClient.ImportPartsInventoryDetailByBillIdCompleted -= excelServiceClient_ImportPartsInventoryDetailByBillIdCompleted;
                        this.excelServiceClient.ImportPartsInventoryDetailByBillIdCompleted += excelServiceClient_ImportPartsInventoryDetailByBillIdCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void excelServiceClient_ImportPartsInventoryDetailByBillIdCompleted(object sender, ImportPartsInventoryDetailByBillIdCompletedEventArgs e) {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            foreach(var detail in partsInventoryBill.PartsInventoryDetails) {
                partsInventoryBill.PartsInventoryDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    partsInventoryBill.PartsInventoryDetails.Add(new PartsInventoryDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        WarehouseAreaId = data.WarehouseAreaId,
                        WarehouseAreaCode = data.WarehouseAreaCode,
                        CurrentStorage = data.CurrentStorage,
                        StorageDifference = data.StorageDifference,
                        StorageAfterInventory = data.StorageAfterInventory,
                        Remark = data.Remark,
                        StorageAfterInventoryNew = data.StorageAfterInventory,
                        Ifcover=true
                    });
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }
        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsStockingUIStrings.Upload_Error3);
                    e.Handled = true;
                }
            }
        }

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        private void CreateUI() {
            var detail = new DcsDetailDataEditView();
            detail.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Action_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.Uploader.ShowFileDialog())
            });
            detail.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.ExportPartsInventoryBillWithDetail())
            });
            detail.UnregisterButton(detail.InsertButton);
            detail.UnregisterButton(detail.DeleteButton);
            detail.Register(PartsStockingUIStrings.DataEditView_GroupTitle_PartsInventoryDetail, null, this.PartsInventoryDetailForResultEntry);
            this.LayoutRoot.Children.Add(detail);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInventoryBillsWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var partsInventoryBill = loadOp.Entities.SingleOrDefault();
                if(partsInventoryBill == null)
                    return;
                foreach(var partsInventoryDetail in partsInventoryBill.PartsInventoryDetails) {
                    if(partsInventoryDetail.StorageAfterInventory!=0) {
                        partsInventoryDetail.StorageAfterInventoryNew = partsInventoryDetail.StorageAfterInventory;
                        partsInventoryDetail.StorageDifference = partsInventoryDetail.StorageAfterInventory - partsInventoryDetail.CurrentStorage;
                    }
                }
                this.SetObjectToEdit(partsInventoryBill);
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsInventoryDetailForResultEntry.CommitEdit())
                return;
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            if(!partsInventoryBill.PartsInventoryDetails.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Validation_PartsInventoryDetail_PartsInventoryDetailIsNull);
                return;
            }
            if(partsInventoryBill.PartsInventoryDetails.Any(e => e.StorageAfterInventory < 0)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Validation_PartsInventoryDetail_PartsInventoryDetailMustGreaterThanZero);
                return;
            }
            foreach(var partsInventoryDetail in partsInventoryBill.PartsInventoryDetails) {
                if(partsInventoryDetail.StorageAfterInventoryNew==null) {
                    UIHelper.ShowNotification("请填写"+partsInventoryDetail.SparePartCode+"盘点后的库存");
                    return;
                } else {
                    partsInventoryDetail.StorageAfterInventory = partsInventoryDetail.StorageAfterInventoryNew.Value;
                    partsInventoryDetail.StorageDifference = partsInventoryDetail.StorageAfterInventory - partsInventoryDetail.CurrentStorage;
                }

            }
            ((IEditableObject)partsInventoryBill).EndEdit();
            try {
                if(partsInventoryBill.Can结果录入配件盘点单)
                    partsInventoryBill.结果录入配件盘点单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        protected override void Reset() {
            var partsInventoryDetail = this.DomainContext.PartsInventoryBills.ToArray();
            if(partsInventoryDetail.Any())
                foreach(var rate in partsInventoryDetail)
                    this.DomainContext.PartsInventoryBills.Detach(rate);
        }
        private void ExportPartsInventoryBillWithDetail() {
            ShellViewModel.Current.IsBusy = true;
            var item = this.DataContext as PartsInventoryBill;
            if(item == null)
                return;
            this.excelServiceClient.ExportPartsInventoryDetailByBillIdAsync(item.Id);
            this.excelServiceClient.ExportPartsInventoryDetailByBillIdCompleted -= excelServiceClient_ExportPartsInventoryDetailByBillIdCompleted;
            this.excelServiceClient.ExportPartsInventoryDetailByBillIdCompleted += excelServiceClient_ExportPartsInventoryDetailByBillIdCompleted;
        }

        private void excelServiceClient_ExportPartsInventoryDetailByBillIdCompleted(object sender, ExportPartsInventoryDetailByBillIdCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
