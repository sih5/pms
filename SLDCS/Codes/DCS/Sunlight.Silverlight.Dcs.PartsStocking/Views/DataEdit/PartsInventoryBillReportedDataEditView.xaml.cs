﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsInventoryBillReportedDataEditView : INotifyPropertyChanged {
        private string branchName;
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private readonly ObservableCollection<KeyValuePair> kvWarehouseAreaCategorys = new ObservableCollection<KeyValuePair>();        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = {
            "Area_Category"
        };
        private string strFileName;
        protected Action<string> UploadFileSuccessedProcessing;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase partsInventoryBillReportedForEditDataGridView;
        private RadUpload uploader;
        public PartsInventoryBillReportedDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsInventoryBillReportedDataEditView_DataContextChanged;
        }

        private void PartsInventoryBillReportedDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            partsInventoryBill.PropertyChanged += partsInventoryBill_PropertyChanged;
        }

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsInventoryBill = this.DataContext as PartsInventoryBill;
                        if(partsInventoryBill == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportPartsInventoryDetailAsync(e.HandlerData.CustomData["Path"].ToString(), partsInventoryBill.WarehouseId, partsInventoryBill.WarehouseAreaCategory, BaseApp.Current.CurrentUserData.EnterpriseId);
                        this.excelServiceClient.ImportPartsInventoryDetailCompleted -= excelServiceClient_ImportPartsInventoryDetailCompleted;
                        this.excelServiceClient.ImportPartsInventoryDetailCompleted += excelServiceClient_ImportPartsInventoryDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsStockingUIStrings.Upload_Error3);
                    e.Handled = true;
                }
            }
        }

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        private void excelServiceClient_ImportPartsInventoryDetailCompleted(object sender, ImportPartsInventoryDetailCompletedEventArgs e) {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            foreach(var detail in partsInventoryBill.PartsInventoryDetails) {
                partsInventoryBill.PartsInventoryDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    partsInventoryBill.PartsInventoryDetails.Add(new PartsInventoryDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        WarehouseAreaId = data.WarehouseAreaId,
                        WarehouseAreaCode = data.WarehouseAreaCode,
                        CurrentStorage = data.CurrentStorage
                    });
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        public ObservableCollection<KeyValuePair> KvWarehouseAreaCategorys {
            get {
                return this.kvWarehouseAreaCategorys;            }
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public DataGridViewBase PartsInventoryBillReportedForEditDataGridView {
            get {
                if(this.partsInventoryBillReportedForEditDataGridView == null) {
                    this.partsInventoryBillReportedForEditDataGridView = DI.GetDataGridView("PartsInventoryBillReportedForEdit");
                    this.partsInventoryBillReportedForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsInventoryBillReportedForEditDataGridView;
            }
        }

        private AgentsPartsInventoryBillForUploadDataEditPanel fileUploadDataEditPanels;
        public AgentsPartsInventoryBillForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (AgentsPartsInventoryBillForUploadDataEditPanel)DI.GetDataEditPanel("AgentsPartsInventoryBillForUpload"));
            }
        }

        private void CreateUI() {
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 9);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            FileUploadDataEditPanels.HorizontalAlignment = HorizontalAlignment.Right;
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);

            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsInventoryBill), "PartsInventoryDetails"), null, () => this.PartsInventoryBillReportedForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 4);
            this.Root.Children.Add(detailDataEditView);
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && (r.PwmsInterface == false || r.PwmsInterface == null)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvWarehouses.Clear();
                foreach(var warehouse in loadOp.Entities)
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
            }, null);
            this.KeyValueManager.LoadData(() => {
                this.KvWarehouseAreaCategorys.Clear();
                foreach (var value in this.keyValueManager[this.kvNames[0]]) { 
                    if(value.Key == (int)DcsAreaType.保管区 || value.Key == (int)DcsAreaType.问题区)
                    this.kvWarehouseAreaCategorys.Add(value);
                }
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Title_ExportTemplate,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
        }
        private ICommand exportFileCommand;
        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private const string EXPORT_DATA_FILE_NAME = "导出配件盘点单清单模板.xlsx";
        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartName,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseAreaHistory_WarehouseAreaCode,
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void ShowFileDialog() {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            if(partsInventoryBill.WarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryBill_WarehouseId);
                return;
            }
            if(partsInventoryBill.WarehouseAreaCategory == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_AreaCategoryNotNull);
                return;
            }
            this.Uploader.ShowFileDialog();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsInventoryBillsWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var partsInventoryBill = loadOp.Entities.SingleOrDefault();
                if(partsInventoryBill == null)
                    return;
                this.DataContext = partsInventoryBill;
                FileUploadDataEditPanels.FilePath = partsInventoryBill.Path;
                this.SetObjectToEdit(partsInventoryBill);
            }, null);
        }

        private void partsInventoryBill_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            switch(e.PropertyName) {
                case "WarehouseId":
                case "WarehouseAreaCategory":
                    if(e.PropertyName == "WarehouseId") {
                        var kvWarehouse = this.KvWarehouses.SingleOrDefault(obj => obj.Key == partsInventoryBill.WarehouseId);
                        if(kvWarehouse == null)
                            return;
                        var warehouse = kvWarehouse.UserObject as Warehouse;
                        if(warehouse == null)
                            return;
                        partsInventoryBill.BranchId = warehouse.BranchId;
                        partsInventoryBill.WarehouseId = warehouse.Id;
                        partsInventoryBill.WarehouseCode = warehouse.Code;
                        partsInventoryBill.WarehouseName = warehouse.Name;
                        partsInventoryBill.StorageCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                        partsInventoryBill.StorageCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                        partsInventoryBill.StorageCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                        partsInventoryBill.StorageCompanyType = warehouse.StorageCompanyType;
                    }
                    if(partsInventoryBill.PartsInventoryDetails.Any()) {
                        DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Notification_PartsInventoryBill_NotifyClearDetail, () => {
                            foreach(var detail in partsInventoryBill.PartsInventoryDetails.ToArray()) {
                                partsInventoryBill.PartsInventoryDetails.Remove(detail);
                            }
                        });
                    }
                    break;
            }
        }

        protected override void OnEditSubmitting() {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null || !this.PartsInventoryBillReportedForEditDataGridView.CommitEdit())
                return;
            ((IEditableObject)partsInventoryBill).EndEdit();
            partsInventoryBill.Path = FileUploadDataEditPanels.FilePath;
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsInventoryBillReported;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        public string BranchName {
            get {
                return this.branchName;
            }
            set {
                this.branchName = value;
                this.OnPropertyChanged("BranchName");
            }
        }
        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        private void DcsComboBox_SelectionChanged_1(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {

            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            var temp = sender as RadComboBox;
            if(temp == null)
                return;
            var warehouse = temp.SelectedItem as KeyValuePair;
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(warehouse == null || partsInventoryBill == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(v => v.Id == ((Warehouse)warehouse.UserObject).BranchId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entitys = loadOp.Entities.FirstOrDefault();
                if(entitys != null) {
                    partsInventoryBill.BranchId = entitys.Id;
                    BranchName = entitys.Name;
                }
            }, null);
        }
    }
}
