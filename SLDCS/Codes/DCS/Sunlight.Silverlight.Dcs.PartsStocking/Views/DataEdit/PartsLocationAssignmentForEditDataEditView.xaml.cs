﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsLocationAssignmentForEditDataEditView {
        private WarehouseAreaDataTreeView warehouseAreaDataTreeView;
        private DataGridViewBase partsStockForEditDataGridView;
        private DataGridViewBase warehouseAreaWithPartsStockDataGridView;
        private ObservableCollection<PartsStock> partsStocks;
        private ObservableCollection<WarehouseAreaWithPartsStock> warehouseAreaWithPartsStocks;

        public PartsLocationAssignmentForEditDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public WarehouseAreaDataTreeView WarehouseAreaDataTreeView {
            get {
                if(this.warehouseAreaDataTreeView == null) {
                    this.warehouseAreaDataTreeView = new WarehouseAreaDataTreeView();
                    this.warehouseAreaDataTreeView.DomainContext = this.DomainContext;
                    this.warehouseAreaDataTreeView.EnableSearchTextBox();
                    this.warehouseAreaDataTreeView.IsExpanderFirstNode = false;
                    this.warehouseAreaDataTreeView.OnTreeViewItemClick -= WarehouseAreaDataTreeView_OnTreeViewItemClick;
                    this.warehouseAreaDataTreeView.OnTreeViewItemClick += WarehouseAreaDataTreeView_OnTreeViewItemClick;
                    this.warehouseAreaDataTreeView.OnTreeViewItemExpanded += warehouseAreaDataTreeView_OnTreeViewItemExpanded;
                    this.warehouseAreaDataTreeView.EntityQuery = this.warehouseAreaDataTreeView.DomainContext.GetWarehouseAreasWithRootWithoutPositionByOperatorIdQuery(BaseApp.Current.CurrentUserData.UserId);
                    this.warehouseAreaDataTreeView.RefreshDataTree();
                }
                return this.warehouseAreaDataTreeView;
            }
        }

        private void warehouseAreaDataTreeView_OnTreeViewItemExpanded(object sender, RadRoutedEventArgs e) {
            var item = e.OriginalSource as RadTreeViewItem;
            if(isLoadTreeNodeItems(item))
                return;
            if(item == null)
                return;
            var region = item.Tag as WarehouseArea;
            if(region == null)
                return;

            this.DomainContext.Load(DomainContext.查询库区库位根据父节点IdQuery(region.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                item.Items.Clear();
                if(!loadOp.Entities.Any()) {
                    item.Items.Clear();
                    return;
                }
                foreach(var nextNode in loadOp.Entities) {
                    if(nextNode.AreaKind >= 3)
                        continue;
                    var radTreeView = new RadTreeViewItem {
                        Header = nextNode.Code,
                        Tag = nextNode
                    };
                    radTreeView.Items.Add(new RadTreeViewItem());
                    item.Items.Add(radTreeView);
                }
            }, null);
        }

        private bool isLoadTreeNodeItems(RadTreeViewItem tvItem) {
            if(tvItem.Items.Count > 0) {
                var tv = tvItem.Items.First() as RadTreeViewItem;
                if(tv != null && tv.Header == null) {
                    tvItem.Items.Clear();
                    return false;
                }
                return true;
            }
            return true;
        }

        private DataGridViewBase PartsStockForEditDataGridView {
            get {
                if(this.partsStockForEditDataGridView == null) {
                    this.partsStockForEditDataGridView = DI.GetDataGridView("PartsStockForEditFor");
                    this.partsStockForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsStockForEditDataGridView.DataContext = this;
                }
                return this.partsStockForEditDataGridView;
            }
        }

        private DataGridViewBase WarehouseAreaWithPartsStockDataGridView {
            get {
                if(this.warehouseAreaWithPartsStockDataGridView == null) {
                    this.warehouseAreaWithPartsStockDataGridView = DI.GetDataGridView("WarehouseAreaWithPartsStock");
                    this.warehouseAreaWithPartsStockDataGridView.DomainContext = this.DomainContext;
                    this.warehouseAreaWithPartsStockDataGridView.DataContext = this;
                    this.warehouseAreaWithPartsStockDataGridView.RowDoubleClick += this.WarehouseAreaWithPartsStockDataGridView_RowDoubleClick;
                }
                return this.warehouseAreaWithPartsStockDataGridView;
            }
        }

        private void WarehouseAreaWithPartsStockDataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            var selectedItem = ((DataGridViewBase)sender).SelectedEntities.First();
            if(selectedItem == null || !(selectedItem is WarehouseAreaWithPartsStock))
                return;
            var item = selectedItem as WarehouseAreaWithPartsStock;
            //库区库位类型 != 库位 不做处理
            if(item.AreaKind != (int)DcsAreaKind.库位) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_SelectedWarehouseAreaIsNotPosition);
                return;
            }
            if(this.PartsStockForEditDataGridView.SelectedEntities == null || !this.PartsStockForEditDataGridView.SelectedEntities.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_ChoosePart);
                return;
            }
            var partsStock = this.PartsStockForEditDataGridView.SelectedEntities.FirstOrDefault() as PartsStock;
            if(partsStock == null) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_PartsStock_SelectedIsInvalid);
                return;
            }
            if(item.SparePartId == default(int)) {
                //配件ID = 空
                if(partsStock.WarehouseArea == null) {
                    //库位编号 = 空
                    item.SparePartId = partsStock.SparePart.Id;
                    item.SparePartCode = partsStock.SparePart.Code;
                    item.SparePartName = partsStock.SparePart.Name;
                    this.GetWarehouseAreaById(item.WarehouseAreaId, partsStock);
                } else {
                    //库位编号 != 空
                    item.SparePartId = partsStock.SparePart.Id;
                    item.SparePartCode = partsStock.SparePart.Code;
                    item.SparePartName = partsStock.SparePart.Name;
                    var newPartsStock = new PartsStock {
                        SparePart = partsStock.SparePart,
                        Quantity = 0,
                        Remark = null
                    };
                    this.GetWarehouseAreaById(item.WarehouseAreaId, newPartsStock);
                    this.PartsStocks.Add(newPartsStock);
                }
            } else {
                //配件ID != 空
                if(partsStock.WarehouseArea == null)
                    //库位编号 = 空
                    DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Confirm_WarehouseAreaHasDistributed, this.AddWarehouseAreaWithPartsStock);
                else
                    //库位编号 != 空
                    DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Confirm_WarehouseAreaHasDistributed, this.AddWarehouseAreaWithPartsStockAndPartsStock);
            }
        }

        private void GetWarehouseAreaById(int id, PartsStock partsStock) {
            if(partsStock == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetWarehouseAreasWithCategoryByIdsQuery(new[] { id }), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                partsStock.WarehouseAreaId = entity.Id;
                partsStock.WarehouseId = entity.WarehouseId;
                partsStock.StorageCompanyId = entity.Warehouse.StorageCompanyId;
                partsStock.StorageCompanyType = entity.Warehouse.StorageCompanyType;
                partsStock.BranchId = entity.Warehouse.BranchId;
                partsStock.Quantity = 0;
                partsStock.Remark = null;
                partsStock.WarehouseAreaCategoryId = entity.AreaCategoryId;
            }, null);
        }

        private void AddWarehouseAreaWithPartsStockAndPartsStock() {
            var selectWarehouseAreaWithPartsStock = this.WarehouseAreaWithPartsStockDataGridView.SelectedEntities.First();
            if(selectWarehouseAreaWithPartsStock == null || !(selectWarehouseAreaWithPartsStock is WarehouseAreaWithPartsStock))
                return;
            var warehouseAreaWithPartsStock = selectWarehouseAreaWithPartsStock as WarehouseAreaWithPartsStock;
            var partsStock = this.PartsStockForEditDataGridView.SelectedEntities.FirstOrDefault() as PartsStock;
            if(partsStock == null) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_PartsStock_SelectedIsInvalid);
                return;
            }
            if(PartsStocks.Any(e => e.WarehouseArea != null && e.WarehouseArea.Id == warehouseAreaWithPartsStock.WarehouseAreaId && e.SparePart.Id == partsStock.SparePart.Id)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_PartsStock_SparePartAndWarehouseAreaMustOnly);
                return;
            }
            WarehouseAreaWithPartsStocks.Add(new WarehouseAreaWithPartsStock {
                WarehouseAreaId = warehouseAreaWithPartsStock.WarehouseAreaId,
                WarehouseId = warehouseAreaWithPartsStock.WarehouseId,
                WarehouseAreaParentId = warehouseAreaWithPartsStock.WarehouseAreaParentId,
                WarehouseAreaCode = warehouseAreaWithPartsStock.WarehouseAreaCode,
                TopLevelWarehouseAreaId = warehouseAreaWithPartsStock.TopLevelWarehouseAreaId,
                AreaCategory = warehouseAreaWithPartsStock.AreaCategory,
                AreaCategoryId = warehouseAreaWithPartsStock.AreaCategoryId,
                AreaKind = warehouseAreaWithPartsStock.AreaKind,
                StorageCompanyId = warehouseAreaWithPartsStock.StorageCompanyId,
                StorageCompanyType = warehouseAreaWithPartsStock.StorageCompanyType,
                BranchId = warehouseAreaWithPartsStock.BranchId,
                Quantity = 0,
                Remark = null,
                WarehouseCode = warehouseAreaWithPartsStock.WarehouseCode,
                WarehouseName = warehouseAreaWithPartsStock.WarehouseName,
                WarehouseType = warehouseAreaWithPartsStock.WarehouseType,
                SparePartId = partsStock.SparePart.Id,
                SparePartCode = partsStock.SparePart.Code,
                SparePartName = partsStock.SparePart.Name,
            });
            var newPartsStock = new PartsStock {
                SparePart = partsStock.SparePart,
            };
            this.GetWarehouseAreaById(warehouseAreaWithPartsStock.WarehouseAreaId, newPartsStock);
            this.PartsStocks.Add(newPartsStock);
        }

        private void AddWarehouseAreaWithPartsStock() {
            var selectWarehouseAreaWithPartsStock = this.WarehouseAreaWithPartsStockDataGridView.SelectedEntities.First();
            if(selectWarehouseAreaWithPartsStock == null || !(selectWarehouseAreaWithPartsStock is WarehouseAreaWithPartsStock))
                return;
            var warehouseAreaWithPartsStock = selectWarehouseAreaWithPartsStock as WarehouseAreaWithPartsStock;
            var partsStock = this.PartsStockForEditDataGridView.SelectedEntities.FirstOrDefault() as PartsStock;
            if(partsStock == null) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_PartsStock_SelectedIsInvalid);
                return;
            }
            if(this.PartsStocks.Any(e => e.WarehouseArea != null && e.WarehouseArea.Id == warehouseAreaWithPartsStock.WarehouseAreaId && e.SparePart.Id == partsStock.SparePart.Id)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_PartsStock_SparePartAndWarehouseAreaMustOnly);
                return;
            }
            WarehouseAreaWithPartsStocks.Add(new WarehouseAreaWithPartsStock {
                WarehouseAreaId = warehouseAreaWithPartsStock.WarehouseAreaId,
                WarehouseAreaParentId = warehouseAreaWithPartsStock.WarehouseAreaParentId,
                WarehouseAreaCode = warehouseAreaWithPartsStock.WarehouseAreaCode,
                TopLevelWarehouseAreaId = warehouseAreaWithPartsStock.TopLevelWarehouseAreaId,
                AreaCategory = warehouseAreaWithPartsStock.AreaCategory,
                AreaCategoryId = warehouseAreaWithPartsStock.AreaCategoryId,
                AreaKind = warehouseAreaWithPartsStock.AreaKind,
                WarehouseId = warehouseAreaWithPartsStock.WarehouseId,
                StorageCompanyId = warehouseAreaWithPartsStock.StorageCompanyId,
                StorageCompanyType = warehouseAreaWithPartsStock.StorageCompanyType,
                BranchId = warehouseAreaWithPartsStock.BranchId,
                Quantity = 0,
                Remark = null,
                WarehouseCode = warehouseAreaWithPartsStock.WarehouseCode,
                WarehouseName = warehouseAreaWithPartsStock.WarehouseName,
                WarehouseType = warehouseAreaWithPartsStock.WarehouseType,
                SparePartId = partsStock.SparePart.Id,
                SparePartCode = partsStock.SparePart.Code,
                SparePartName = partsStock.SparePart.Name,
            });
            this.GetWarehouseAreaById(warehouseAreaWithPartsStock.WarehouseAreaId, partsStock);
        }

        private void WarehouseAreaDataTreeView_OnTreeViewItemClick(object sender, RadRoutedEventArgs radRoutedEventArgs) {
            var selectedItem = ((RadTreeView)sender).SelectedItem as RadTreeViewItem;
            if(selectedItem == null || !(selectedItem.Tag is WarehouseArea))
                return;
            this.WarehouseAreaWithPartsStocks.Clear();
            var warehouseArea = selectedItem.Tag as WarehouseArea;
            //更新当前选中的库区信息，用于判断导入库位分配按钮是否可用
            warehouseAreaDataTreeView.CurrentSelectedItem = warehouseArea;
            this.DomainContext.Load(this.DomainContext.GetWarehouseAreaWithPartsStocksQuery().Where(d => d.WarehouseAreaParentId == warehouseArea.Id && d.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities;
                var areaWithPartsStocks = entity as WarehouseAreaWithPartsStock[] ?? entity.ToArray();
                if(!areaWithPartsStocks.Any())
                    return;
                foreach(var item in areaWithPartsStocks) {
                    if(this.DomainContext.WarehouseAreaWithPartsStocks.Contains(item))
                        this.DomainContext.WarehouseAreaWithPartsStocks.Detach(item);
                    this.WarehouseAreaWithPartsStocks.Add(item);
                }
            }, null);
        }

        private void CreateUI() {
            this.WarehouseAreaDataTreeView.SetValue(Grid.RowProperty, 2);
            this.RTCWarehouseArea.Children.Add(this.WarehouseAreaDataTreeView);
            //TODO:清单导入
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsStockingUIStrings.DataGridView_Title_PartsStock, null, () => this.PartsStockForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(detailDataEditView);
            var detail = new DcsDetailDataEditView();
            detail.Register(PartsStockingUIStrings.DataEditView_Title_WarehouseAreaWithPartsStock, null, () => this.WarehouseAreaWithPartsStockDataGridView);
            detail.SetValue(Grid.RowProperty, 1);
            detail.SetValue(Grid.ColumnProperty, 1);
            detail.UnregisterButton(detail.InsertButton);
            detail.UnregisterButton(detail.DeleteButton);
            //清除配件库位关系
            detail.RegisterButton(new ButtonItem {
                Command = new DelegateCommand(this.CPartsLocationRelation),
                Icon = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                Title = PartsStockingUIStrings.Action_Title_RemovePartsLRelation
            });
            this.Root.Children.Add(detail);
        }

        private void PartsStocks_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if(e.OldItems != null) {
                var removeWarehouseAreaIds = e.OldItems.Cast<PartsStock>().Where(entity => this.PartsStocks.Count(r => r.WarehouseAreaId == entity.WarehouseAreaId) == 0).Select(entity => entity.WarehouseAreaId);
                foreach(var id in removeWarehouseAreaIds) {
                    var warehouseArea = this.WarehouseAreaWithPartsStocks.FirstOrDefault(entity => entity.WarehouseAreaId == id);
                    if(warehouseArea == null)
                        continue;
                    warehouseArea.SparePartId = default(int);
                    warehouseArea.SparePartCode = string.Empty;
                    warehouseArea.SparePartName = string.Empty;
                    warehouseArea.Quantity = default(int?);
                }
            }
        }

        protected override void OnEditSubmitting() {
            if(!PartsStockForEditDataGridView.CommitEdit())
                return;
            if(!PartsStocks.Any())
                return;
            if(PartsStocks.Any(e => e.WarehouseArea == null)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_WarehouseAreaIsNull);
                return;
            }
            if(PartsStocks.Any(e => e.HasValidationErrors))
                return;
            foreach(var item in PartsStocks)
                this.DomainContext.PartsStocks.Add(item);
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitted() {
            UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_PartsStock_SaveSuccess);
            this.PartsStocks.Clear();
        }

        protected override void OnEditCancelled() {
            UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_PartsStock_CancelSuccess);
            this.PartsStocks.Clear();
            this.WarehouseAreaWithPartsStocks.Clear();
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsLocationAssignment;
            }
        }

        /// <summary>
        /// 通过传入的配件Id，填充配件库存集合(填充前，先清空集合)
        /// </summary>
        /// <param name="partIds">配件Id</param>
        public void AddNewPartsStock(int[] partIds) {
            this.PartsStocks.Clear();
            Action loadPartsInfo = () => this.DomainContext.Load(this.DomainContext.GetSparePartsByIdsQuery(partIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var spareparts = loadOp.Entities;
                var spareParts = spareparts as SparePart[] ?? spareparts.ToArray();
                if(spareparts == null || !spareParts.Any())
                    return;
                foreach(var partId in partIds) {
                    this.PartsStocks.Add(new PartsStock {
                        Quantity = 0,
                        SparePart = spareParts.SingleOrDefault(e => e.Id == partId)
                    });
                }
            }, null);
            if(this.DomainContext == null)
                this.Initializer.Register(loadPartsInfo);
            else
                loadPartsInfo.Invoke();

        }

        //清除配件库位关系
        private void CPartsLocationRelation() {
            if(!this.WarehouseAreaWithPartsStocks.Any())
                return;
            if(this.WarehouseAreaWithPartsStockDataGridView.SelectedEntities == null) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_ChooseOrder);
                return;
            }
            if(this.WarehouseAreaWithPartsStockDataGridView.SelectedEntities != null && this.WarehouseAreaWithPartsStockDataGridView.SelectedEntities.Any()) {
                var warehouseAreaWithPartsStock = this.WarehouseAreaWithPartsStockDataGridView.SelectedEntities.Cast<WarehouseAreaWithPartsStock>().ToList();
                var paramer = new List<decimal>();
                foreach(var item in warehouseAreaWithPartsStock) {
                    var spartPartId = Convert.ToDecimal(item.SparePartId);
                    var wareHouseAreaId = Convert.ToDecimal(item.WarehouseAreaId);
                    paramer.Add(spartPartId + wareHouseAreaId / 10000000000);
                }
                if(warehouseAreaWithPartsStock.Any(e => e.Quantity != 0 && e.Quantity !=null)) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_OnlyDeleteStockWithQuantityIsZero);
                } else {
                    this.DomainContext.删除配件库存(paramer.ToArray(), callBack => {
                        if (callBack.HasError)
                        {
                            if (!callBack.IsErrorHandled)
                                callBack.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(callBack);
                            return;
                        }
                        foreach(var deletePartsStock in warehouseAreaWithPartsStock) {
                            this.WarehouseAreaWithPartsStocks.Remove(deletePartsStock);
                        }
                    }, null);
                }
            }
        }

        public ObservableCollection<PartsStock> PartsStocks {
            get {
                if(this.partsStocks == null) {
                    this.partsStocks = new ObservableCollection<PartsStock>();
                    this.partsStocks.CollectionChanged -= this.PartsStocks_CollectionChanged;
                    this.partsStocks.CollectionChanged += this.PartsStocks_CollectionChanged;
                }
                return partsStocks;
            }
        }

        public ObservableCollection<WarehouseAreaWithPartsStock> WarehouseAreaWithPartsStocks {
            get {
                return warehouseAreaWithPartsStocks ?? (this.warehouseAreaWithPartsStocks = new ObservableCollection<WarehouseAreaWithPartsStock>());
            }
        }
    }
}
