﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsOutboundBillDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private ObservableCollection<PartsOutboundPlanDetail> partsOutboundPlanDetails;
        private ObservableCollection<PartsOutboundBillDetail> partsOutboundBillDetails;
        private DataGridViewBase partsOutboundBillDetailForEditDataGridView;
        private DataGridViewBase partsOutboundPlanDetailForDeliverDataGridView;
        private DataGridViewBase virtualPartsStockForDeliverDataGridView;
        private string partsSalesCategoryName;
        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public string PartsSalesCategoryName {
            get {
                return this.partsSalesCategoryName;
            }
            set {
                this.partsSalesCategoryName = value;
                this.OnPropertyChanged("PartsSalesCategoryName");
            }
        }

        private readonly string[] kvNames = {
            "Parts_OutboundType"
        };

        public PartsOutboundBillDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);

        }

        private DataGridViewBase PartsOutboundBillDetailForEditDataGridView {
            get {
                if(this.partsOutboundBillDetailForEditDataGridView == null) {
                    this.partsOutboundBillDetailForEditDataGridView = DI.GetDataGridView("PartsOutboundBillDetailForEdit");
                    this.partsOutboundBillDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsOutboundBillDetailForEditDataGridView;
            }
        }

        public DataGridViewBase PartsOutboundPlanDetailForDeliverDataGridView {
            get {
                if(this.partsOutboundPlanDetailForDeliverDataGridView == null) {
                    this.partsOutboundPlanDetailForDeliverDataGridView = DI.GetDataGridView("PartsOutboundPlanDetailForDeliver");
                    this.partsOutboundPlanDetailForDeliverDataGridView.DomainContext = this.DomainContext;
                    this.partsOutboundPlanDetailForDeliverDataGridView.DataContext = this;
                    this.partsOutboundPlanDetailForDeliverDataGridView.SelectionChanged += this.PartsOutboundPlanDetailForDeliverDataGridView_SelectionChanged;

                }
                return this.partsOutboundPlanDetailForDeliverDataGridView;
            }
        }

        private DataGridViewBase VirtualPartsStockForDeliverDataGridView {
            get {
                if(this.virtualPartsStockForDeliverDataGridView == null) {
                    this.virtualPartsStockForDeliverDataGridView = DI.GetDataGridView("VirtualPartsStockForDeliver");
                    this.virtualPartsStockForDeliverDataGridView.DomainContext = this.DomainContext;
                    this.virtualPartsStockForDeliverDataGridView.DataContext = this;
                }
                return this.virtualPartsStockForDeliverDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            var tbPartsOutboundPlanDetails = new RadTabControl();
            tbPartsOutboundPlanDetails.BackgroundVisibility = Visibility.Collapsed;
            tbPartsOutboundPlanDetails.Items.Add(new RadTabItem {
                Content = this.PartsOutboundPlanDetailForDeliverDataGridView,
                Header = PartsStockingUIStrings.DataEditView_Title_PartsOutboundPlanDetail
            });
            tbPartsOutboundPlanDetails.SetValue(Grid.ColumnProperty, 1);
            this.gvPartsOutboundPlanDetail.Children.Add(tbPartsOutboundPlanDetails);
            var tbVirtualPartsStock = new DcsDetailDataEditView();
            tbVirtualPartsStock.Register(PartsStockingUIStrings.DataEditView_Title_VirtualPartsStock, null, this.VirtualPartsStockForDeliverDataGridView);
            tbVirtualPartsStock.UnregisterButton(tbVirtualPartsStock.DeleteButton);
            tbVirtualPartsStock.UnregisterButton(tbVirtualPartsStock.InsertButton);
            tbVirtualPartsStock.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Text_Refrech,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.Refresh)
            });
            tbVirtualPartsStock.SetValue(Grid.ColumnProperty, 1);
            this.gvVirtualPartsStock.Children.Add(tbVirtualPartsStock);
            this.KeyValueManager.LoadData();
        }

        private void Refresh() {
            var partsOutboundBill = this.DataContext as PartsOutboundBill;
            if(partsOutboundBill == null)
                return;
            if(this.PartsOutboundBillDetails.Any()) {
                foreach(var partsOutboundPlanDetail in this.PartsOutboundPlanDetails)
                    partsOutboundPlanDetail.CurrentOutboundAmount = 0;
                foreach(var partsOutboundBillDetail in this.PartsOutboundBillDetails)
                    partsOutboundBillDetail.ValidationErrors.Clear();
            }
            this.DomainContext.Load(this.DomainContext.查询配件库位库存Query(partsOutboundBill.PartsOutboundPlanId, null, null, BaseApp.Current.CurrentUserData.UserId, null, null, null).Where(r => r.WarehouseAreaCategory == (int)DcsAreaType.保管区 && r.UsableQuantity > 0), LoadBehavior.RefreshCurrent, loadOpVirtualPartsStock => {
                if(loadOpVirtualPartsStock.HasError) {
                    loadOpVirtualPartsStock.MarkErrorAsHandled();
                    return;
                }
                var virtualPartsStocks = loadOpVirtualPartsStock.Entities.ToArray();
                this.PartsOutboundBillDetails.Clear();
                if(virtualPartsStocks == null || !virtualPartsStocks.Any())
                    return;
                foreach(var partsOutboundPlanDetail in this.PartsOutboundPlanDetails) {
                    var data = virtualPartsStocks.Where(r => r.SparePartId == partsOutboundPlanDetail.SparePartId);
                    foreach(var virtualPartsStock in data) {
                        var partsOutboundBillDetail = new PartsOutboundBillDetail();
                        partsOutboundBillDetail.SparePartId = virtualPartsStock.SparePartId;
                        partsOutboundBillDetail.SparePartCode = virtualPartsStock.SparePartCode;
                        partsOutboundBillDetail.SparePartName = virtualPartsStock.SparePartName;
                        partsOutboundBillDetail.WarehouseAreaCode = virtualPartsStock.WarehouseAreaCode;
                        partsOutboundBillDetail.WarehouseAreaCategory = virtualPartsStock.WarehouseAreaCategory;
                        partsOutboundBillDetail.UsableQuantity = virtualPartsStock.UsableQuantity;
                        partsOutboundBillDetail.WarehouseAreaId = virtualPartsStock.WarehouseAreaId;
                        partsOutboundBillDetail.BatchNumber = virtualPartsStock.BatchNumber;
                        partsOutboundBillDetail.SettlementPrice = partsOutboundPlanDetail.Price;
                        partsOutboundBillDetail.OriginalPrice = partsOutboundPlanDetail.OriginalPrice;
                        //剩余出库数量
                        var currentOutboundAmount = partsOutboundPlanDetail.PlannedAmount - partsOutboundPlanDetail.OutboundFulfillment - partsOutboundPlanDetail.CurrentOutboundAmount ?? 0;
                        if(currentOutboundAmount == 0) {
                            partsOutboundBillDetail.OutboundAmount = 0;
                        } else {
                            partsOutboundBillDetail.OutboundAmount = currentOutboundAmount > partsOutboundBillDetail.UsableQuantity ? partsOutboundBillDetail.UsableQuantity : currentOutboundAmount;
                            partsOutboundPlanDetail.CurrentOutboundAmount = partsOutboundPlanDetail.CurrentOutboundAmount + partsOutboundBillDetail.OutboundAmount;
                        }
                        this.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
                    }
                }
            }, null);
        }


        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void PartsOutboundPlanDetailForDeliverDataGridView_SelectionChanged(object sender, EventArgs e) {
            var dataGrid = sender as DcsDataGridViewBase;
            if(dataGrid == null || dataGrid.SelectedEntities == null)
                return;
            var partsOutboundPlanDetail = dataGrid.SelectedEntities.Cast<PartsOutboundPlanDetail>().FirstOrDefault();
            if(partsOutboundPlanDetail == null)
                return;
            var sparePartIds = new object[] {
                partsOutboundPlanDetail.SparePartId
            };
            VirtualPartsStockForDeliverDataGridView.ExchangeData(null, "Blue", sparePartIds);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsOutboundPlansQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                var newPartsOutboundBill = this.CreateObjectToEdit<PartsOutboundBill>();
                newPartsOutboundBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                newPartsOutboundBill.WarehouseName = entity.WarehouseName;
                newPartsOutboundBill.OutboundType = entity.OutboundType;
                newPartsOutboundBill.WarehouseCode = entity.WarehouseCode;
                newPartsOutboundBill.PartsOutboundPlanId = entity.Id;
                newPartsOutboundBill.WarehouseId = entity.WarehouseId;
                newPartsOutboundBill.StorageCompanyId = entity.StorageCompanyId;
                newPartsOutboundBill.StorageCompanyCode = entity.StorageCompanyCode;
                newPartsOutboundBill.StorageCompanyName = entity.StorageCompanyName;
                newPartsOutboundBill.StorageCompanyType = entity.StorageCompanyType;
                newPartsOutboundBill.BranchId = entity.BranchId;
                newPartsOutboundBill.BranchCode = entity.BranchCode;
                newPartsOutboundBill.BranchName = entity.BranchName;
                newPartsOutboundBill.GPMSPurOrderCode = entity.GPMSPurOrderCode;
                newPartsOutboundBill.CounterpartCompanyId = entity.CounterpartCompanyId;
                newPartsOutboundBill.CounterpartCompanyCode = entity.CounterpartCompanyCode;
                newPartsOutboundBill.CounterpartCompanyName = entity.CounterpartCompanyName;
                newPartsOutboundBill.CustomerAccountId = entity.CustomerAccountId;
                newPartsOutboundBill.OriginalRequirementBillId = entity.OriginalRequirementBillId;
                newPartsOutboundBill.OriginalRequirementBillType = entity.OriginalRequirementBillType;
                newPartsOutboundBill.OriginalRequirementBillCode = entity.OriginalRequirementBillCode;
                newPartsOutboundBill.ReceivingCompanyId = entity.ReceivingCompanyId;
                newPartsOutboundBill.ReceivingCompanyCode = entity.ReceivingCompanyCode;
                newPartsOutboundBill.ReceivingCompanyName = entity.ReceivingCompanyName;
                newPartsOutboundBill.ReceivingWarehouseId = entity.ReceivingWarehouseId;
                newPartsOutboundBill.ReceivingWarehouseCode = entity.ReceivingWarehouseCode;
                newPartsOutboundBill.ReceivingWarehouseName = entity.ReceivingWarehouseName;
                newPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                newPartsOutboundBill.ShippingMethod = entity.ShippingMethod;
                newPartsOutboundBill.PartsSalesCategoryId = entity.PartsSalesCategoryId;
                newPartsOutboundBill.PartsSalesOrderTypeId = entity.PartsSalesOrderTypeId;
                newPartsOutboundBill.PartsSalesOrderTypeName = entity.PartsSalesOrderTypeName;
                newPartsOutboundBill.PartsSalesOrderTypeId = entity.PartsSalesOrderTypeId;
                newPartsOutboundBill.PartsSalesOrderTypeName = entity.PartsSalesOrderTypeName;
                newPartsOutboundBill.OrderApproveComment = entity.OrderApproveComment;
                newPartsOutboundBill.Remark = entity.Remark;
                newPartsOutboundBill.SAPPurchasePlanCode = entity.SAPPurchasePlanCode;
                newPartsOutboundBill.ERPSourceOrderCode = entity.ERPSourceOrderCode;
                this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.Id == entity.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError) {
                        loadOption.MarkErrorAsHandled();
                        return;
                    }
                    if(loadOption.Entities == null)
                        return;
                    PartsSalesCategoryName = loadOption.Entities.First().Name;
                }, null);
                this.PartsOutboundPlanDetails.Clear();
                this.DomainContext.Load(this.DomainContext.GetPartsOutboundPlanDetailsByWarehouseAreaManagerQuery(entity.Id, BaseApp.Current.CurrentUserData.UserId).Where(r => r.OutboundFulfillment == null || r.PlannedAmount != r.OutboundFulfillment), LoadBehavior.RefreshCurrent, loadOption1 => {
                    if(loadOption1.HasError) {
                        loadOption1.MarkErrorAsHandled();
                        return;
                    }
                    foreach(var item in loadOption1.Entities.OrderBy(r => r.SparePartCode)) {
                        this.PartsOutboundPlanDetails.Add(new PartsOutboundPlanDetail {
                            PartsOutboundPlanId = item.PartsOutboundPlanId,
                            SparePartId = item.SparePartId,
                            SparePartCode = item.SparePartCode,
                            SparePartName = item.SparePartName,
                            PlannedAmount = item.PlannedAmount,
                            OutboundFulfillment = item.OutboundFulfillment.GetValueOrDefault(),
                            Price = item.Price,
                            OriginalPrice = item.OriginalPrice,
                            Remark = item.Remark,
                            CurrentOutboundAmount = 0
                        });
                    }
                    this.Refresh();
                }, null);
            }, null);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsOutboundBillDeliverParts;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsOutboundBillDetailForEditDataGridView.CommitEdit())
                return;
            if(!this.PartsOutboundPlanDetailForDeliverDataGridView.CommitEdit())
                return;
            if(!this.VirtualPartsStockForDeliverDataGridView.CommitEdit())
                return;

            var partsOutboundBill = this.DataContext as PartsOutboundBill;
            if(partsOutboundBill == null)
                return;
            partsOutboundBill.ValidationErrors.Clear();

            if(string.IsNullOrWhiteSpace(partsOutboundBill.WarehouseName))
                partsOutboundBill.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_WarehouseNameIsNull, new[] {
                    "WarehouseName"
                }));
            if(partsOutboundBill.OutboundType == default(int))
                partsOutboundBill.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_OutboundTypeIsNull, new[] {
                    "OutboundType"
                }));
            var outboundBillDetails = partsOutboundBill.PartsOutboundBillDetails.ToArray();
            foreach(var partsOutboundBillDetail in outboundBillDetails)
                partsOutboundBill.PartsOutboundBillDetails.Remove(partsOutboundBillDetail);
            foreach(var partsOutboundBillDetail in this.PartsOutboundBillDetails.Where(r => r.OutboundAmount != 0))
                partsOutboundBill.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
            if(!partsOutboundBill.PartsOutboundBillDetails.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_PartsOutboundBillDetailsIsEmpty);
                return;
            }
            partsOutboundBill.EcommerceMoney = partsOutboundBill.PartsOutboundBillDetails.Sum(r => r.OriginalPrice * r.OutboundAmount);
            if(partsOutboundBill.HasValidationErrors || partsOutboundBill.PartsOutboundBillDetails.Any(e => e.HasValidationErrors))
                return;

            ((IEditableObject)partsOutboundBill).EndEdit();
            try {
                if(partsOutboundBill.Can生成配件出库单)
                    partsOutboundBill.生成配件出库单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();



        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            var partsOutboundBill = this.DataContext as PartsOutboundBill;
            if(partsOutboundBill == null)
                return;
            DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Notification_IsPrintOutbound, () => {
                this.DomainContext.Load(this.DomainContext.GetPartsOutboundBillsQuery().Where(ex => ex.Id == partsOutboundBill.Id), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var entity = loadOp.Entities.FirstOrDefault();
                    if(entity == null)
                        return;
                    BasePrintWindow printWindow;
                    printWindow = new PartsOutboundBillPrintWindow {
                        Header = PartsStockingUIStrings.DataEditView_Text_PrintOutbound,
                        PartsOutboundBill = entity
                    };
                    printWindow.ShowDialog();
                }, null);
            });
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<PartsOutboundPlanDetail> PartsOutboundPlanDetails {
            get {
                return this.partsOutboundPlanDetails ?? (this.partsOutboundPlanDetails = new ObservableCollection<PartsOutboundPlanDetail>());
            }
        }

        public ObservableCollection<PartsOutboundBillDetail> PartsOutboundBillDetails {
            get {
                return this.partsOutboundBillDetails ?? (this.partsOutboundBillDetails = new ObservableCollection<PartsOutboundBillDetail>());
            }
        }

        protected override void Reset() {
            this.PartsOutboundPlanDetails.Clear();
            var partsOutboundBill = this.DataContext as PartsOutboundBill;
            if(partsOutboundBill == null)
                return;
            if(this.DomainContext.PartsOutboundBills.Contains(partsOutboundBill))
                this.DomainContext.PartsOutboundBills.Detach(partsOutboundBill);
            this.PartsOutboundPlanDetails.Clear();
            this.PartsOutboundBillDetails.Clear();

        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
