﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.View;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;
using System.ComponentModel;
using Sunlight.Silverlight.Core;
using System.ComponentModel.DataAnnotations;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsOutboundBillReturnDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private PartsOutboundBillDetail partsOutboundBillDetail;
        private bool customIsEnabled, batchNumberIsEnabled;
        private string batchNum, code, warehouseAreaCode;
        private int amount;

        private DataGridViewBase partsOutboundBillDetailReturnDataGridView, partsOutboundBillDetailForDetailDataGridView;

        public bool CustomIsEnabled {
            get {
                return this.customIsEnabled;
            }
            set {
                if(this.customIsEnabled == value)
                    return;
                this.customIsEnabled = value;
                this.OnPropertyChanged("CustomIsEnabled");
            }
        }

        public bool BatchNumberIsEnabled {
            get {
                return this.batchNumberIsEnabled;
            }
            set {
                if(this.batchNumberIsEnabled == value)
                    return;
                this.batchNumberIsEnabled = value;
                this.OnPropertyChanged("BatchNumberIsEnabled");
            }
        }

        public string BatchNum {
            get {
                return this.batchNum;
            }
            set {
                this.batchNum = value;
                this.OnPropertyChanged("BatchNum");
            }
        }

        public int Amount {
            get {
                return this.amount;
            }
            set {
                this.amount = value;
                this.OnPropertyChanged("Amount");
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsOutboundBillReturn;
            }
        }

        private DataGridViewBase PartsOutboundBillDetailReturnDataGridView {
            get {
                if(this.partsOutboundBillDetailReturnDataGridView == null) {
                    this.partsOutboundBillDetailReturnDataGridView = DI.GetDataGridView("PartsOutboundBillDetailReturn");
                    this.partsOutboundBillDetailReturnDataGridView.DomainContext = this.DomainContext;
                    this.partsOutboundBillDetailReturnDataGridView.SelectionChanged += this.partsOutboundBillDetailReturnDataGridView_SelectionChanged;
                }
                return this.partsOutboundBillDetailReturnDataGridView;
            }
        }

        private DataGridViewBase PartsOutboundBillDetailForDetailDataGridView {
            get {
                if(this.partsOutboundBillDetailForDetailDataGridView == null) {
                    this.partsOutboundBillDetailForDetailDataGridView = DI.GetDataGridView("PartsOutboundBillDetailForDetail");
                    this.partsOutboundBillDetailForDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsOutboundBillDetailForDetailDataGridView;
            }
        }

        private void partsOutboundBillDetailReturnDataGridView_SelectionChanged(object sender, EventArgs e) {
            var dataGrid = sender as DcsDataGridViewBase;
            if(dataGrid == null || dataGrid.SelectedEntities == null)
                return;
            this.partsOutboundBillDetail = dataGrid.SelectedEntities.Cast<PartsOutboundBillDetail>().FirstOrDefault() as PartsOutboundBillDetail;
            this.BatchNum = partsOutboundBillDetail.BatchNumber;
            this.Amount = partsOutboundBillDetail.CanReturnAmount;
            this.warehouseAreaCode = partsOutboundBillDetail.WarehouseAreaCode;
            this.DomainContext.Load(this.DomainContext.GetPartsStocksQuery().Where(v => v.PartId == partsOutboundBillDetail.SparePartId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var partsStock = loadOp.Entities.FirstOrDefault();
                this.DomainContext.Load(this.DomainContext.GetWarehouseAreasQuery().Where(r => r.Id == partsStock.WarehouseAreaId), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    var warehouseArea = loadOp1.Entities.FirstOrDefault();
                    this.code = warehouseArea.Code;

                }, null);
            }, null);

            //判断
            if(warehouseAreaCode.Equals(DcsPartsBranchPartsWarhouseManageGranularity .序列号管理)) {
                this.CustomIsEnabled = false;
                this.BatchNumberIsEnabled = true;
            }
            if(warehouseAreaCode.Equals(DcsPartsBranchPartsWarhouseManageGranularity.批次管理)) {
                this.CustomIsEnabled = true;
                this.BatchNumberIsEnabled = false;
            }
            if(warehouseAreaCode.Equals(DcsPartsBranchPartsWarhouseManageGranularity.无批次管理)) {
                this.CustomIsEnabled = true;
                this.BatchNumberIsEnabled = true;
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("PartsOutboundBillReturn"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsStockingUIStrings.DataEditView_Title_PartsOutboundBillDetail, null, () => this.PartsOutboundBillDetailReturnDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(detailDataEditView);
            var partsOutboundBillDetail = new DcsDetailDataEditView();
            partsOutboundBillDetail.Register(PartsStockingUIStrings.DataEditView_Title_PartsOutboundBillReturnDetail, null, () => this.PartsOutboundBillDetailForDetailDataGridView);
            this.Root.Children.Add(partsOutboundBillDetail);
        }

        private void btnOutbound_Click(object sender, RoutedEventArgs e) {
            if(this.Amount==0 && this.BatchNum==null) 
                return;
            var partsOutboundBill = this.DataContext as PartsOutboundBill;
            var partsOutboundBillDetail = new PartsOutboundBillDetail();
            partsOutboundBillDetail.SparePartId = this.partsOutboundBillDetail.SparePartId;
            partsOutboundBillDetail.SparePartName = this.partsOutboundBillDetail.SparePartName;
            partsOutboundBillDetail.SparePartCode = this.partsOutboundBillDetail.SparePartCode;
            partsOutboundBillDetail.WarehouseAreaId = this.partsOutboundBillDetail.WarehouseAreaId;
            partsOutboundBillDetail.WarehouseAreaCode = code;
            partsOutboundBillDetail.PartsOutboundBillId = this.partsOutboundBillDetail.PartsOutboundBillId;
            partsOutboundBillDetail.SettlementPrice = this.partsOutboundBillDetail.SettlementPrice;
            partsOutboundBillDetail.Remark = this.partsOutboundBillDetail.Remark;
            partsOutboundBillDetail.BatchNumber = this.BatchNum;
            partsOutboundBillDetail.OutboundAmount = this.Amount;
            partsOutboundBill.PartsOutboundBillDetails.Add(partsOutboundBillDetail);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.查询可退库配件出库单及配件Query(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.PartsOutboundBillDetailReturnDataGridView.DataContext = entity;

                var newPartsOutboundBill = this.ObjectToAdd as PartsOutboundBill;
                newPartsOutboundBill.Code = entity.Code;
                newPartsOutboundBill.WarehouseName = entity.WarehouseName;
                newPartsOutboundBill.OutboundType = entity.OutboundType;
                newPartsOutboundBill.WarehouseCode = entity.WarehouseCode;
                newPartsOutboundBill.PartsOutboundPlanId = entity.Id;
                newPartsOutboundBill.WarehouseId = entity.WarehouseId;
                newPartsOutboundBill.StorageCompanyId = entity.StorageCompanyId;
                newPartsOutboundBill.StorageCompanyCode = entity.StorageCompanyCode;
                newPartsOutboundBill.StorageCompanyName = entity.StorageCompanyName;
                newPartsOutboundBill.StorageCompanyType = entity.StorageCompanyType;
                newPartsOutboundBill.BranchId = entity.BranchId;
                newPartsOutboundBill.BranchCode = entity.BranchCode;
                newPartsOutboundBill.BranchName = entity.BranchName;
                newPartsOutboundBill.CounterpartCompanyId = entity.CounterpartCompanyId;
                newPartsOutboundBill.CounterpartCompanyCode = entity.CounterpartCompanyCode;
                newPartsOutboundBill.CounterpartCompanyName = entity.CounterpartCompanyName;
                newPartsOutboundBill.CustomerAccountId = entity.CustomerAccountId;
                newPartsOutboundBill.OriginalRequirementBillId = entity.OriginalRequirementBillId;
                newPartsOutboundBill.OriginalRequirementBillType = entity.OriginalRequirementBillType;
                newPartsOutboundBill.ReceivingCompanyId = entity.ReceivingCompanyId;
                newPartsOutboundBill.ReceivingCompanyCode = entity.ReceivingCompanyCode;
                newPartsOutboundBill.ReceivingCompanyName = entity.ReceivingCompanyName;
                newPartsOutboundBill.ReceivingWarehouseId = entity.ReceivingWarehouseId;
                newPartsOutboundBill.ReceivingWarehouseCode = entity.ReceivingWarehouseCode;
                newPartsOutboundBill.ReceivingWarehouseName = entity.ReceivingWarehouseName;
                newPartsOutboundBill.SettlementStatus = (int)DcsPartsSettlementStatus.待结算;
                newPartsOutboundBill.ShippingMethod = entity.ShippingMethod;
                newPartsOutboundBill.PartsSalesCategoryId = entity.PartsSalesCategoryId;
                newPartsOutboundBill.Remark = entity.Code;
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsOutboundBill = this.DataContext as PartsOutboundBill;
            partsOutboundBill.Code = GlobalVar.ASSIGNED_BY_SERVER;

            if(partsOutboundBill == null)
                return;
            if(!partsOutboundBill.PartsOutboundBillDetails.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_PartsOutboundBillDetailsIsNull);
                return;
            }
            if(this.Amount > this.partsOutboundBillDetail.CanReturnAmount) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_CanReturnAmountIsMax);
                return;
            }
            if(this.Amount == 0 && this.partsOutboundBillDetail.CanReturnAmount==0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_CanReturnAmountIsNull);
                return;
            }

            ((IEditableObject)partsOutboundBill).EndEdit();
            try {
                if(partsOutboundBill.Can生成负数配件出库单)
                    partsOutboundBill.生成负数配件出库单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }

            base.OnEditSubmitting();
        }

        public PartsOutboundBillReturnDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.btnOutbound.Click += btnOutbound_Click;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
