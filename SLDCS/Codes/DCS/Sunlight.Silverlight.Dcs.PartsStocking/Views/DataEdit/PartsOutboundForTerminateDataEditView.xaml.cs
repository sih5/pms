﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsOutboundForTerminateDataEditView {
        public PartsOutboundForTerminateDataEditView() {
            InitializeComponent();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsOutboundByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsOutboundPlan = this.DataContext as PartsOutboundPlan;
            if(partsOutboundPlan == null)
                return;
            partsOutboundPlan.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(partsOutboundPlan.StopComment))
                partsOutboundPlan.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_TerminateReasonIsNull, new[] {
                    "StopComment"
                }));

            if(partsOutboundPlan.HasValidationErrors)
                return;

            try {
                if(partsOutboundPlan.Can终止配件出库计划)
                    partsOutboundPlan.终止配件出库计划();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_TerminateOutboundPlan;
            }
        }

    }
}
