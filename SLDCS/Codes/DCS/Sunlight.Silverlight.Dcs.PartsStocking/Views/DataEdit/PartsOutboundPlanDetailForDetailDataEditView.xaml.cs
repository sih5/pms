﻿using System;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsOutboundPlanDetailForDetailDataEditView {
        private DataGridViewBase partsOutboundPlanDetailForDetailDataGridView;

        public PartsOutboundPlanDetailForDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase PartsOutboundPlanDetailForDetailDataGridView {
            get {
                if(this.partsOutboundPlanDetailForDetailDataGridView == null) {
                    this.partsOutboundPlanDetailForDetailDataGridView = DI.GetDataGridView("PartsOutboundPlanDetailForDetail");
                    this.partsOutboundPlanDetailForDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsOutboundPlanDetailForDetailDataGridView;
            }
        }

        private void ExportData() {
            ((DcsDataGridViewBase)this.PartsOutboundPlanDetailForDetailDataGridView).ExportData();
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsOutboundPlan), "PartsOutboundPlanDetails"), null, this.PartsOutboundPlanDetailForDetailDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/export.png", UriKind.Relative),
                Command = new DelegateCommand(this.ExportData)
            });
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsOutboundPlanWithDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
                var serialNumber = 1;
                foreach(var item in entity.PartsOutboundPlanDetails)
                    item.SerialNumber = serialNumber++;
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsOutboundPlanDetail;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
