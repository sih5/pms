﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsPackingDetailDataEditView {
        private DataGridViewBase partsStockBatchDetailPartsPackingDataGridView;
        private ObservableCollection<PartsStockBatchDetail> partsStockBatchDetails;

        private DataGridViewBase PartsStockBatchDetailPartsPackingDataGridView {
            get {
                if(this.partsStockBatchDetailPartsPackingDataGridView == null) {
                    this.partsStockBatchDetailPartsPackingDataGridView = DI.GetDataGridView("PartsStockBatchDetailPartsPacking");
                    this.partsStockBatchDetailPartsPackingDataGridView.DomainContext = this.DomainContext;
                    this.partsStockBatchDetailPartsPackingDataGridView.DataContext = this;
                }
                return this.partsStockBatchDetailPartsPackingDataGridView;
            }
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsStockingUIStrings.DataGridView_Title_PartsStockBatchDetail, null, this.PartsStockBatchDetailPartsPackingDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/new-bill.png", UriKind.Relative),
                Command = new DelegateCommand(this.ExportData)
            });
            this.Root.Children.Add(detailDataEditView);
        }

        private void ExportData() {
            ((DcsDataGridViewBase)this.PartsStockBatchDetailPartsPackingDataGridView).ExportData();
        }

        private void LoadEntityToEdit(IList<object> filterParam) {
            var sparePartId = filterParam[0] == null ? null : (int?)filterParam[0];
            var warehouseId = filterParam[1] == null ? null : (int?)filterParam[1];
            this.PartsStockBatchDetails.Clear();
            this.DomainContext.Load(this.DomainContext.查询配件库存及批次明细Query(sparePartId).Where(e => e.WarehouseId == warehouseId), LoadBehavior.RefreshCurrent, loadOpDetail => {
                if(loadOpDetail.HasError) {
                    if(!loadOpDetail.IsErrorHandled)
                        loadOpDetail.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpDetail);
                    return;
                }
                if(loadOpDetail.Entities == null || !loadOpDetail.Entities.Any())
                    return;
                var entities = loadOpDetail.Entities;
                var serialNo = 1;
                foreach(var entity in entities) {
                    entity.SerialNumber = serialNo++;
                    this.PartsStockBatchDetails.Add(entity);
                }
            }, null);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_ViewDetail_PartsPacking;
            }
        }

        public ObservableCollection<PartsStockBatchDetail> PartsStockBatchDetails {
            get {
                return this.partsStockBatchDetails ?? (this.partsStockBatchDetails = new ObservableCollection<PartsStockBatchDetail>());
            }
        }

        public PartsPackingDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object partId) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((object[])partId));
            else
                this.LoadEntityToEdit((object[])partId);
        }
    }
}
