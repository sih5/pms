﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShelvesDataEditView : INotifyPropertyChanged {
        private bool isCanShelves;
        private PartsShiftOrder partsShiftOrder;
        private ObservableCollection<PartsShiftOrderDetail> partsShiftOrderDetails;
        private DataGridViewBase dataGridView;
        private PartsShiftOrderDetailForEditDataGridView partsShiftOrderDetailForEditDataGridView;
        private RadWindow allotWarehouseAreaWindow;
        private PartsLocationAssignmentDataEditView partsLocationAssignmentEditView;
        public event PropertyChangedEventHandler PropertyChanged;

        public PartsShelvesDataEditView() {
            this.InitializeComponent();
            this.SetValue(DataContextProperty, this);
            this.Initializer.Register(this.CreateUI);
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e) {
            this.IsCanShelves = this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any();
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CreateUI() {
            this.DataGridView.Margin = new Thickness(0, 0, 0, 6);
            //待上架清单
            var dataEditView = new DcsDetailDataEditView();
            dataEditView.Register(" ", null, () => this.DataGridView);
            dataEditView.UnregisterButton(dataEditView.InsertButton);
            dataEditView.UnregisterButton(dataEditView.DeleteButton);
            dataEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_Export,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/merge.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.ExportVirtualPartsStockForShelves)
            });
            this.Root.Children.Add(dataEditView);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            this.Root.Children.Add(this.CreateVerticalLine(3));
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(PartsStockingUIStrings.DataGridView_Title_PartsShiftDetails, null, () => this.PartsShiftOrderDetailForEditDataGridView);
            detailEditView.SetValue(Grid.RowProperty, 3);
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_AllotWarehouseArea,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/distribute.png", UriKind.Relative),
                Command = new DelegateCommand(this.AllotWarehouseArea)
            });
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            this.PartsShiftOrderRoot.Children.Add(detailEditView);
            this.Initialize();
        }

        private void Initialize() {
            this.btnShelves.Click += (sender, e) => {
                var partsStocks = this.DataGridView.SelectedEntities.Cast<VirtualPartsStock>();
                if(partsStocks.Any(virtualPartsStock => this.PartsShiftOrderDetails.Count(entity => entity.SparePartId == virtualPartsStock.SparePartId &&
                    entity.OriginalWarehouseAreaId == virtualPartsStock.WarehouseAreaId &&
                    entity.BatchNumber == virtualPartsStock.BatchNumber) > 0)) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_PartsShelves_VirtualPartsStockIsRepeat);
                    return;
                }
                var partsStock = partsStocks.FirstOrDefault();
                if(partsStock == null)
                    return;
                if(this.PartsShiftOrder.WarehouseId == default(int) || this.PartsShiftOrder.WarehouseId != partsStock.WarehouseId) {
                    var userData = BaseApp.Current.CurrentUserData;
                    this.PartsShiftOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    this.PartsShiftOrder.BranchId = partsStock.BranchId;
                    this.PartsShiftOrder.WarehouseId = partsStock.WarehouseId;
                    this.PartsShiftOrder.WarehouseCode = partsStock.WarehouseCode;
                    this.PartsShiftOrder.WarehouseName = partsStock.WarehouseName;
                    this.PartsShiftOrder.StorageCompanyId = userData.EnterpriseId;
                    this.PartsShiftOrder.StorageCompanyCode = userData.EnterpriseCode;
                    this.PartsShiftOrder.StorageCompanyName = userData.EnterpriseName;
                    this.PartsShiftOrder.StorageCompanyType = partsStock.StorageCompanyType;
                    this.PartsShiftOrder.Type = (int)DcsPartsShiftOrderType.上架;
                }
                foreach(var stock in partsStocks) {
                    var item = new PartsShiftOrderDetail();
                    item.PartsShiftOrderId = this.partsShiftOrder.Id;
                    item.SparePartCode = stock.SparePartCode;
                    item.SparePartName = stock.SparePartName;
                    item.BatchNumber = stock.BatchNumber;
                    item.OriginalWarehouseAreaId = stock.WarehouseAreaId;
                    item.OriginalWarehouseAreaCode = stock.WarehouseAreaCode;
                    item.OriginalWarehouseAreaCategory = (int)DcsAreaType.检验区;
                    item.DestWarehouseAreaCategory = (int)DcsAreaType.保管区;
                    item.ShelvesAmount = stock.UsableQuantity;
                    item.Quantity = stock.UsableQuantity;
                    item.SparePartId = stock.SparePartId;
                    item.MeasureUnit = stock.MeasureUnit;
                    this.PartsShiftOrderDetails.Add(item);
                }
                this.GetWarehouseAreas();
            };
        }

        private void GetWarehouseAreas() {
            var sparePartIds = this.PartsShiftOrderDetails.Select(detail => detail.SparePartId).ToArray();
            this.PartsShiftOrderDetailForEditDataGridView.DicWarehouseAreas.Clear();
            this.DomainContext.Load(this.DomainContext.查询配件定位存储Query(sparePartIds).Where(s => s.WarehouseId == this.PartsShiftOrder.WarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                var partIds = loadOp.Entities.Select(e => e.PartId).ToArray();
                foreach(var partId in partIds) {
                    var id = partId;
                    var warehouseAreas = loadOp.Entities.Where(e => e.PartId == id && e.WarehouseArea != null && e.WarehouseAreaCategory != null && e.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区).Select(e => e.WarehouseArea).Distinct().ToList();
                    if(warehouseAreas == null || !warehouseAreas.Any())
                        continue;
                    if(this.PartsShiftOrderDetailForEditDataGridView.DicWarehouseAreas.All(v => v.Key != partId))
                        this.PartsShiftOrderDetailForEditDataGridView.DicWarehouseAreas.Add(partId, warehouseAreas);
                    else
                        this.PartsShiftOrderDetailForEditDataGridView.DicWarehouseAreas[partId] = warehouseAreas;


                }
                if(this.PartsShiftOrderDetailForEditDataGridView.DicWarehouseAreas.Any()) {
                    if(this.PartsShiftOrderDetails.Any()) {
                        foreach(var partsShiftOrderDetail in this.PartsShiftOrderDetails) {
                            var dicWarehouseAreas = this.PartsShiftOrderDetailForEditDataGridView.DicWarehouseAreas.SingleOrDefault(r => r.Key == partsShiftOrderDetail.SparePartId).Value;
                            if(dicWarehouseAreas != null && dicWarehouseAreas.Count == 1) {
                                partsShiftOrderDetail.DestWarehouseAreaId = dicWarehouseAreas[0].Id;
                                partsShiftOrderDetail.DestWarehouseAreaCode = dicWarehouseAreas[0].Code;
                            }
                        }
                    }
                }
            }, null);
        }

        /// <summary>
        /// 配件库位分配
        /// </summary>
        private void AllotWarehouseArea() {
            if(this.PartsShiftOrderDetailForEditDataGridView.SelectedEntities == null || !this.PartsShiftOrderDetailForEditDataGridView.SelectedEntities.Any())
                return;
            var ids = this.PartsShiftOrderDetailForEditDataGridView.SelectedEntities.Cast<PartsShiftOrderDetail>().Select(v => v.SparePartId).ToArray();
            this.PartsLocationAssignmentEditView.AddNewPartsStock(ids);
            var warehouseId = this.partsShiftOrder.WarehouseId;
            this.PartsLocationAssignmentEditView.GetWarehouseAreaByWarehouseId(warehouseId);
            this.AllotWarehouseAreaWindow.ShowDialog();
        }

        private void PartsLocationAssignmentEditView_EditSubmitted(object sender, EventArgs e) {
            this.AllotWarehouseAreaWindow.Close();
            this.GetWarehouseAreas();
        }

        private void PartsLocationAssignmentEditView_EditCancelled(object sender, EventArgs e) {
            this.AllotWarehouseAreaWindow.Close();
        }

        private PartsLocationAssignmentDataEditView PartsLocationAssignmentEditView {
            get {
                if(this.partsLocationAssignmentEditView == null) {
                    this.partsLocationAssignmentEditView = new PartsLocationAssignmentDataEditView();
                    this.partsLocationAssignmentEditView.EditSubmitted += this.PartsLocationAssignmentEditView_EditSubmitted;
                    this.partsLocationAssignmentEditView.EditCancelled += this.PartsLocationAssignmentEditView_EditCancelled;
                }
                return this.partsLocationAssignmentEditView;
            }
        }

        private RadWindow AllotWarehouseAreaWindow {
            get {
                return this.allotWarehouseAreaWindow ?? (this.allotWarehouseAreaWindow = new RadWindow {
                    CanClose = false,
                    CanMove = true,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    Content = this.PartsLocationAssignmentEditView,
                    Header = PartsStockingUIStrings.DataEditView_Title_PartsLocationAssignment,
                    Height = 480,
                    Width = 640
                });
            }
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsShelves;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsShiftOrderDetailForEditDataGridView.CommitEdit())
                return;
            if(!this.PartsShiftOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShiftOrder_PartsShiftOrderDetailsIsEmpty);
                return;
            }
            this.PartsShiftOrder.ValidationErrors.Clear();
            foreach(var partsShiftOrderDetail in this.PartsShiftOrderDetails)
                partsShiftOrderDetail.ValidationErrors.Clear();
            foreach(var partsShiftOrderDetail in this.PartsShiftOrder.PartsShiftOrderDetails)
                partsShiftOrderDetail.ValidationErrors.Clear();
            var errorDetails = this.PartsShiftOrderDetails.Where(v => v.OriginalWarehouseAreaId == v.DestWarehouseAreaId).ToArray();
            if(errorDetails != null)
                if(errorDetails.Any()) {
                    var msg = new StringBuilder();
                    for(var i = 0; i < errorDetails.Length; i++) {
                        msg.Append(string.Format(PartsStockingUIStrings.DataEditView_Validation_PartsShelves_DetailOriginalWarehouseAreaIsEqualToDestWarehouseArea, errorDetails[i].SparePartCode));
                        if(i != errorDetails.Length - 1)
                            msg.Append("\n");
                    }
                    UIHelper.ShowAlertMessage(msg.ToString());
                    return;
                }
            var detailList = PartsShiftOrder.PartsShiftOrderDetails.ToList();
            if(this.PartsShiftOrder.PartsShiftOrderDetails.Any())
                foreach(var partsShiftOrderDetail in detailList)
                    this.PartsShiftOrder.PartsShiftOrderDetails.Remove(partsShiftOrderDetail);
            foreach(var detail in this.PartsShiftOrderDetails)
                this.PartsShiftOrder.PartsShiftOrderDetails.Add(detail);
            if(!this.DomainContext.PartsShiftOrders.Contains(this.PartsShiftOrder))
                this.DomainContext.PartsShiftOrders.Add(this.PartsShiftOrder);
            try {
                    if(partsShiftOrder.Can生成配件移库单并且执行移库)
                          partsShiftOrder.生成配件移库单并且执行移库();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.PartsShiftOrderDetails.Clear();
            var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "WarehouseId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = 0
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQuery();
            this.PartsShiftOrderDetailForEditDataGridView.ExecuteQuery();
            UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Common_Notification_SaveSuccess);
        }

        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.PartsShiftOrderDetails.Clear();
        }

        public void ClearPartsShiftOrderInfo() {
            if(!this.PartsShiftOrderDetailForEditDataGridView.CommitEdit())
                return;
            this.PartsShiftOrderDetails.Clear();
            if(this.DomainContext.PartsShiftOrders.Contains(this.PartsShiftOrder))
                this.DomainContext.PartsShiftOrders.Detach(this.PartsShiftOrder);
            this.PartsShiftOrder = new PartsShiftOrder();
        }

        public bool IsCanShelves {
            get {
                return this.isCanShelves;
            }
            set {
                this.isCanShelves = value;
                this.OnPropertyChanged("IsCanShelves");
            }
        }

        public PartsShiftOrder PartsShiftOrder {
            get {
                return this.partsShiftOrder ?? (this.partsShiftOrder = new PartsShiftOrder());
            }
            set {
                this.partsShiftOrder = value;
                this.OnPropertyChanged("PartsShiftOrder");
            }
        }

        //待上架清单
        public DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("VirtualPartsStockForShelves");
                    this.dataGridView.DomainContext = this.DomainContext;
                    this.dataGridView.SelectionChanged += this.DataGridView_SelectionChanged;
                }
                return this.dataGridView;
            }
        }

        //导出待上架清单
        private void ExportVirtualPartsStockForShelves() {
            var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
            var virtualPartsStockForShelvesDataGridView = (VirtualPartsStockForShelvesDataGridView)DataGridView;
            if(filterItem == null || dataGridView == null)
                return;
            if(DataGridView.Entities == null || !DataGridView.Entities.Any())
                return;
            var sparePartCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
            var sparePartName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
            var warehouseAreaCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseAreaCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseAreaCode").Value as string;
            this.DomainContext.ExportVirtualPartsStockForShelves(virtualPartsStockForShelvesDataGridView.WarehosueId, sparePartCode, sparePartName, warehouseAreaCode, loadOp1 => {
                if(loadOp1.HasError)
                    return;
                if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                    UIHelper.ShowNotification(loadOp1.Value);
                }
                if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                }
            }, null);
        }

        public ObservableCollection<PartsShiftOrderDetail> PartsShiftOrderDetails {
            get {
                return this.partsShiftOrderDetails ?? (this.partsShiftOrderDetails = new ObservableCollection<PartsShiftOrderDetail>());
            }
        }

        public PartsShiftOrderDetailForEditDataGridView PartsShiftOrderDetailForEditDataGridView {
            get {
                if(this.partsShiftOrderDetailForEditDataGridView == null) {
                    this.partsShiftOrderDetailForEditDataGridView = new PartsShiftOrderDetailForEditDataGridView();
                    this.partsShiftOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                    this.partsShiftOrderDetailForEditDataGridView.DataContext = this;
                }
                return this.partsShiftOrderDetailForEditDataGridView;
            }
        }

        protected override void Reset() {
            ClearPartsShiftOrderInfo();
        }
    }
}
