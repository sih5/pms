﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.View;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Core;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using System;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit
{
    public partial class PartsShelvesTaskDetailDataEditView 
    {
        private DataGridViewBase dataGridView;

        private ObservableCollection<VirtualPartsShelvesTaskDetail> virtualPartsShelvesTaskDetails;

        public PartsShelvesTaskDetailDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase DataGridView
        {
            get
            {
                if (this.dataGridView == null)
                {
                    this.dataGridView = DI.GetDataGridView("PartsShelvesTaskForDetail");
                    this.dataGridView.DomainContext = this.DomainContext;
                    this.dataGridView.DataContext = this;
                }
                return this.dataGridView;
            }
        }
        private void LoadEntityToEdit(int[] ids)
        {
            this.VirtualPartsShelvesTaskDetails.Clear();
            this.DomainContext.Load(this.DomainContext.GetVirtualPartsShelvesTaskDetailByIdQuery(ids), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }

                foreach (var entity in loadOp.Entities)
                {
                    this.virtualPartsShelvesTaskDetails.Add(entity);
                }
                this.GetWarehouseAreas();
            }, null);
        }

        private void GetWarehouseAreas()
        {
            var sparePartIds = this.virtualPartsShelvesTaskDetails.Select(detail => detail.SparePartId).ToArray();
            var warehouseId = virtualPartsShelvesTaskDetails.Select(r => r.WarehouseId).FirstOrDefault();
            var partsShelvesTaskForDetailDataGridView = (PartsShelvesTaskForDetailDataGridView)this.dataGridView;
            partsShelvesTaskForDetailDataGridView.DicWarehouseAreas.Clear();

            this.DomainContext.Load(this.DomainContext.上架查询配件定位存储Query(warehouseId, sparePartIds), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                var partIds = loadOp.Entities.Select(e => e.PartId).Distinct().ToArray();
                foreach (var partId in partIds)
                {
                    var id = partId;
                    var warehouseAreas = loadOp.Entities.Where(e => e.PartId == id && e.WarehouseArea != null && e.WarehouseAreaCategory != null && e.WarehouseAreaCategory.Category == (int)DcsAreaType.保管区).Select(e => e.WarehouseArea).Distinct().ToList();
                    if (warehouseAreas == null || !warehouseAreas.Any())
                        continue;
                    if (partsShelvesTaskForDetailDataGridView.DicWarehouseAreas.All(v => v.Key != partId))
                        partsShelvesTaskForDetailDataGridView.DicWarehouseAreas.Add(partId, warehouseAreas);
                    else
                        partsShelvesTaskForDetailDataGridView.DicWarehouseAreas[partId] = warehouseAreas;


                }
                if (partsShelvesTaskForDetailDataGridView.DicWarehouseAreas.Any())
                {
                    if (this.virtualPartsShelvesTaskDetails.Any())
                    {
                        foreach (var virtualPartsShelvesTaskDetail in this.virtualPartsShelvesTaskDetails)
                        {
                            var dicWarehouseAreas = partsShelvesTaskForDetailDataGridView.DicWarehouseAreas.SingleOrDefault(r => r.Key == virtualPartsShelvesTaskDetail.SparePartId).Value;
                            if (dicWarehouseAreas != null)
                            {
                                var min = dicWarehouseAreas.Where(d => d.Id == dicWarehouseAreas.Min(r => r.Id)).FirstOrDefault();
                                virtualPartsShelvesTaskDetail.WarehouseAreaId = min.Id;
                                virtualPartsShelvesTaskDetail.WarehouseAreaCode = min.Code;
                            }
                        }
                    }
                }
            }, null);
        }

        private void CreateUI()
        {
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            dcsDetailGridView.Register("", null, () => this.DataGridView);
            dcsDetailGridView.SetValue(Grid.RowProperty, 3);
            this.LayoutRoot.Children.Add(dcsDetailGridView);
        }

        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null){
                this.Initializer.Register(() => this.LoadEntityToEdit((int[])id));
            }
            else
                this.LoadEntityToEdit((int[])id);
        }

        protected override string Title
        {
            get
            {
                return PartsStockingUIStrings.DataManagementView_Title_PartsShelves;
            }
        }

        protected override bool OnRequestCanSubmit()
        {
            return true;
        }

        public ObservableCollection<VirtualPartsShelvesTaskDetail> VirtualPartsShelvesTaskDetails
        {
            get
            {
                return this.virtualPartsShelvesTaskDetails ?? (this.virtualPartsShelvesTaskDetails = new ObservableCollection<VirtualPartsShelvesTaskDetail>());
            }
        }

        protected override void OnEditCancelled()
        {
            base.OnEditCancelled();
            this.VirtualPartsShelvesTaskDetails.Clear();
        }

        protected override void OnEditSubmitting()
        {
            if (!this.DataGridView.CommitEdit())
                return;
            if (!this.VirtualPartsShelvesTaskDetails.Any())
            {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShelvesTaskIsNull);
                return;
            }
            foreach (var item in virtualPartsShelvesTaskDetails)
            {
                if (item.WarehouseAreaId == null || "".Equals(item.WarehouseAreaId)) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_WarehouseAreaCodeIsNull);
                    return;
                }
                if(!string.IsNullOrEmpty(item.CurrSIHCodes)) {
                    var sihCodes = item.CurrSIHCodes.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                    if(item.TraceProperty.HasValue && item.TraceProperty == (int)DCSTraceProperty.精确追溯) {                       
                        if(sihCodes.Any(t=>t.Contains("X"))&& sihCodes.Any(t=>t.Contains("J"))) {
                            UIHelper.ShowNotification(item.SparePartCode+"不能同时输入件标和箱标");
                            return;
                        }
                        if(sihCodes.Count() != sihCodes.Distinct().Count()) {
                            UIHelper.ShowNotification(item.SparePartCode + "有重复的上架标签码");
                            return;
                        }
                        if(sihCodes.Any(t => t.Contains("J")) && sihCodes.Count() != item.NowShelvesAmount) {
                            UIHelper.ShowNotification(item.SparePartCode + "上架数量不等于标签码数量");
                            return;
                        }
                        if(!string.IsNullOrEmpty(item.SIHCodes) &&  sihCodes.Any(t => item.SIHCodes.Contains(t))) {
                            UIHelper.ShowNotification(item.SparePartCode + "存在已上架的标签码，请重新输入");
                            return;
                        }
                    } else {
                        if(sihCodes.Count()>1) {
                            UIHelper.ShowNotification(item.SparePartCode + "只能输入一个标签码");
                            return;
                        }
                        if(!string.IsNullOrEmpty(item.SIHCodes) && sihCodes[0] != item.SIHCodes) {
                            UIHelper.ShowNotification(item.SparePartCode + "只能输入标签码:" + item.SIHCodes);
                            return;
                        }
                    }
                }
            }
            foreach (var virtualPartsShelvesTaskDetail in this.virtualPartsShelvesTaskDetails) {
                virtualPartsShelvesTaskDetail.ValidationErrors.Clear();
            }
            
            if (this.DomainContext.IsBusy)
                return;
            this.DomainContext.DoPartsShelves(virtualPartsShelvesTaskDetails.ToArray(), invokeOp =>
            {
                if (invokeOp.HasError)
                {
                    if (!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    var error = invokeOp.ValidationErrors.First();
                    if (error != null)
                        UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                    else
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    return;
                }
                base.OnEditSubmitting();
            }, null);

        }
    }
}
