﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.ViewModel;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using System.Windows.Browser;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShiftOrderDataEditView : INotifyPropertyChanged {
        private KeyValueManager keyValueManager;
        private readonly ObservableCollection<KeyValuePair> kvWarehouseNames = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvDestWarehouseAreaCategoryTypes = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvWarehouseAreaCategoryTypes = new ObservableCollection<KeyValuePair>();
        private DataGridViewBase virtualPartsStockForPartsShiftOrderDataGridView;
        private int virtualPartsStockOriginalWarehouseAreaCategory;
        private int virtualPartsStockDestWarehouseAreaCategory;
        private int partsShiftOrderDetailOriginalWarehouseAreaCategory;
        private int partsShiftOrderDetailDestWarehouseAreaCategory;
        private int? questionType;
        private int virtualPartsStockWarehouseId;
        private int oldWarehouseId, oldOriginalWarehouseAreaCategory, oldDestWarehouseAreaCategory;
        private bool canShift;
        private ObservableCollection<Warehouse> warehouses;
        private PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView partsShiftOrderDetailForPartsShiftOrderForEditDataGridView;
        private PartsLocationAssignmentDataEditView partsLocationAssignmentEditView;
        private RadWindow allotWarehouseAreaWindow;
        private RadWindow warehouseAreaWindow;
        private readonly string[] kvNames = {
             "Area_Category","PartsShiftOrder_QuestionType"
        };
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出配件移库单清单模板.xlsx";

        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsShiftOrder = this.DataContext as PartsShiftOrder;
                        if(partsShiftOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImpPartsShiftOrderDetailAsync(e.HandlerData.CustomData["Path"].ToString(), partsShiftOrder.WarehouseId,this.VirtualPartsStockOriginalWarehouseAreaCategory,this.VirtualPartsStockDestWarehouseAreaCategory);
                        this.excelServiceClient.ImpPartsShiftOrderDetailCompleted -= ExcelServiceClient_ImpPartsShiftOrderDetailCompleted;
                        this.excelServiceClient.ImpPartsShiftOrderDetailCompleted += ExcelServiceClient_ImpPartsShiftOrderDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private void ExcelServiceClient_ImpPartsShiftOrderDetailCompleted(object sender, ImpPartsShiftOrderDetailCompletedEventArgs e) {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if (partsShiftOrder == null)
                return;
            foreach (var detail in partsShiftOrder.PartsShiftOrderDetails) {
                partsShiftOrder.PartsShiftOrderDetails.Remove(detail);
            }
            if (e.rightData != null) {
                foreach (var data in e.rightData) {
                    partsShiftOrder.PartsShiftOrderDetails.Add(new PartsShiftOrderDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        OriginalWarehouseAreaId = data.OriginalWarehouseAreaId,
                        OriginalWarehouseAreaCode = data.OriginalWarehouseAreaCode,
                        OriginalWarehouseAreaCategory = this.VirtualPartsStockOriginalWarehouseAreaCategory,
                        DestWarehouseAreaId = data.DestWarehouseAreaId,
                        DestWarehouseAreaCode = data.DestWarehouseAreaCode,
                        DestWarehouseAreaCategory = this.VirtualPartsStockDestWarehouseAreaCategory,
                        SourceStockAmount = data.OriginalWarehouseAreaQuantity,
                        Quantity = data.ShiftQuantity
                    });
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if (!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if (!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if (e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using (e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch (Exception) {
                    UIHelper.ShowAlertMessage(PartsStockingUIStrings.Upload_Error3);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        public List<VirtualPartsStock> VirtualPartsStocks = new List<VirtualPartsStock>();
        public event PropertyChangedEventHandler PropertyChanged;

        public PartsShiftOrderDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.SearchButton.Click += this.SearchButton_Click;
            this.PartsShiftButton.Click += this.PartsShiftButton_Click;
            this.PropertyChanged += this.PartsShiftOrderDataEditView_PropertyChanged;
            this.DataContextChanged += this.PartsShiftOrderDataEditView_DataContextChanged;
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView {
            get {
                if(this.partsShiftOrderDetailForPartsShiftOrderForEditDataGridView == null) {
                    this.partsShiftOrderDetailForPartsShiftOrderForEditDataGridView = new PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView();
                    this.partsShiftOrderDetailForPartsShiftOrderForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsShiftOrderDetailForPartsShiftOrderForEditDataGridView;
            }
        }

        private PartsLocationAssignmentDataEditView PartsLocationAssignmentDataEditView {
            get {
                if(this.partsLocationAssignmentEditView == null) {
                    this.partsLocationAssignmentEditView = new PartsLocationAssignmentDataEditView();
                    this.partsLocationAssignmentEditView.EditSubmitted += this.PartsLocationAssignmentEditView_EditSubmitted;
                    this.partsLocationAssignmentEditView.EditCancelled += this.PartsLocationAssignmentEditView_EditCancelled;
                }
                return this.partsLocationAssignmentEditView;
            }
        }

        private WarehouseAreaDataEditView warehouseAreaDataEditView;

        private WarehouseAreaDataEditView WarehouseAreaDataEditView {
            get {
                if(this.warehouseAreaDataEditView == null) {
                    this.warehouseAreaDataEditView = new WarehouseAreaDataEditView();
                    this.warehouseAreaDataEditView.EditSubmitted += this.WarehouseAreaDataEditView_EditSubmitted;
                    this.warehouseAreaDataEditView.EditCancelled += this.WarehouseAreaDataEditView_EditCancelled;
                }
                return this.warehouseAreaDataEditView;
            }
        }
        private void WarehouseAreaDataEditView_EditCancelled(object sender, EventArgs e) {
            this.WarehouseAreaWindow.Close();
        }

        private void WarehouseAreaDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.WarehouseAreaWindow.Close();
            //  this.GetWarehouseAreas();
        }
        private RadWindow AllotWarehouseAreaWindow {
            get {
                return this.allotWarehouseAreaWindow ?? (this.allotWarehouseAreaWindow = new RadWindow {
                    CanClose = false,
                    CanMove = true,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    Content = this.PartsLocationAssignmentDataEditView,
                    Header = PartsStockingUIStrings.DataEditView_Title_PartsLocationAssignment,
                    Height = 480,
                    Width = 640
                });
            }
        }


        private RadWindow WarehouseAreaWindow {
            get {
                return this.warehouseAreaWindow ?? (this.warehouseAreaWindow = new RadWindow {
                    CanClose = false,
                    CanMove = true,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    Content = this.WarehouseAreaDataEditView,
                    Header = PartsStockingUIStrings.Action_Title_Add,
                    Height = 480,
                    Width = 640
                });
            }
        }

        #region 界面事件

        private void PartsShiftOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null)
                return;
            oldDestWarehouseAreaCategory = oldOriginalWarehouseAreaCategory = oldWarehouseId = default(int);
            this.PartsShiftOrderDetailOriginalWarehouseAreaCategory = this.PartsShiftOrderDetailDestWarehouseAreaCategory = default(int);
            partsShiftOrder.PartsShiftOrderDetails.EntityRemoved -= PartsShiftOrderDetails_EntityRemoved;
            this.VirtualPartsStocks.Clear();
            partsShiftOrder.PartsShiftOrderDetails.EntityRemoved += PartsShiftOrderDetails_EntityRemoved;
            this.CanShift = false;
        }

        private void PartsShiftOrderDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<PartsShiftOrderDetail> e) {
            var partsShiftOrderDetail = sender as PartsShiftOrderDetail;
            if(partsShiftOrderDetail == null)
                return;
            var virtualPartsStock = this.VirtualPartsStocks.SingleOrDefault(r => r.WarehouseId == partsShiftOrderDetail.OriginalWarehouseAreaId && r.SparePartId == partsShiftOrderDetail.SparePartId);
            if(virtualPartsStock != null && this.VirtualPartsStocks.Contains(virtualPartsStock)) {
                this.VirtualPartsStocks.Remove(virtualPartsStock);
            }
        }

        private void PartsShiftOrderDataEditView_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null)
                return;
            switch(e.PropertyName) {
                case "VirtualPartsStockWarehouseId":
                    if(partsShiftOrder.PartsShiftOrderDetails.Any()) {
                        if(this.oldWarehouseId != default(int) && this.oldWarehouseId != this.VirtualPartsStockWarehouseId)
                            DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Confirm_PartsShiftOrder_ClearDetailsData, () => {
                                this.ClearPartsShiftOrderDetails();
                                this.oldWarehouseId = this.VirtualPartsStockWarehouseId;
                            }, () => {
                                this.VirtualPartsStockWarehouseId = this.oldWarehouseId;
                            });
                        else if(this.oldWarehouseId == default(int) && this.oldWarehouseId != this.VirtualPartsStockWarehouseId) {
                            this.ClearPartsShiftOrderDetails();
                            this.oldWarehouseId = this.VirtualPartsStockWarehouseId;
                        }
                    } else
                        this.oldWarehouseId = this.VirtualPartsStockWarehouseId;
                    break;
                case "VirtualPartsStockOriginalWarehouseAreaCategory":
                    if(this.VirtualPartsStockOriginalWarehouseAreaCategory == (int)DcsAreaType.检验区) {
                        VirtualPartsStockDestWarehouseAreaCategory = default(int);
                        KvDestnationWarehouseAreaAreaCategoryTypeTypes.Clear();
                        foreach(var value in keyValueManager[kvNames[0]].Where(key => key.Key != (int)DcsAreaType.保管区))
                            KvDestnationWarehouseAreaAreaCategoryTypeTypes.Add(value);
                        this.VirtualPartsStockDestWarehouseAreaCategory = 0;
                    } else if(this.oldOriginalWarehouseAreaCategory == (int)DcsAreaType.检验区) {
                        KvDestnationWarehouseAreaAreaCategoryTypeTypes.Clear();
                        foreach(var value in keyValueManager[kvNames[0]])
                            KvDestnationWarehouseAreaAreaCategoryTypeTypes.Add(value);
                        this.VirtualPartsStockDestWarehouseAreaCategory = 0;
                    }
                    if(partsShiftOrder.PartsShiftOrderDetails.Any()) {
                        if(this.oldOriginalWarehouseAreaCategory != default(int) && this.oldOriginalWarehouseAreaCategory != this.VirtualPartsStockOriginalWarehouseAreaCategory)
                            DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Confirm_PartsShiftOrder_ClearDetailsData, () => {
                                this.ClearPartsShiftOrderDetails();
                                this.oldOriginalWarehouseAreaCategory = this.VirtualPartsStockOriginalWarehouseAreaCategory;
                            }, () => {
                                this.VirtualPartsStockOriginalWarehouseAreaCategory = this.oldOriginalWarehouseAreaCategory;
                            });
                        else if(this.oldOriginalWarehouseAreaCategory == default(int) && this.oldOriginalWarehouseAreaCategory != this.VirtualPartsStockOriginalWarehouseAreaCategory) {
                            this.ClearPartsShiftOrderDetails();
                            this.oldOriginalWarehouseAreaCategory = this.VirtualPartsStockOriginalWarehouseAreaCategory;
                        }
                    } else
                        this.oldOriginalWarehouseAreaCategory = this.VirtualPartsStockOriginalWarehouseAreaCategory;
                    break;
                case "VirtualPartsStockDestWarehouseAreaCategory":
                    if(partsShiftOrder.PartsShiftOrderDetails.Any()) {
                        if(this.oldDestWarehouseAreaCategory != default(int) && this.oldDestWarehouseAreaCategory != this.VirtualPartsStockDestWarehouseAreaCategory)
                            DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Confirm_PartsShiftOrder_ClearDetailsData, () => {
                                this.ClearPartsShiftOrderDetails();
                                this.oldDestWarehouseAreaCategory = this.VirtualPartsStockDestWarehouseAreaCategory;
                            }, () => {
                                this.VirtualPartsStockDestWarehouseAreaCategory = this.oldDestWarehouseAreaCategory;
                            });
                        else if(this.oldDestWarehouseAreaCategory == default(int) && this.oldDestWarehouseAreaCategory != this.VirtualPartsStockDestWarehouseAreaCategory) {
                            this.ClearPartsShiftOrderDetails();
                            this.oldDestWarehouseAreaCategory = this.VirtualPartsStockDestWarehouseAreaCategory;
                        }
                    } else
                        this.oldDestWarehouseAreaCategory = this.VirtualPartsStockDestWarehouseAreaCategory;
                    break;
            }
        }

        private void PartsLocationAssignmentEditView_EditCancelled(object sender, EventArgs e) {
            this.AllotWarehouseAreaWindow.Close();
        }

        private void PartsLocationAssignmentEditView_EditSubmitted(object sender, EventArgs e) {
            this.AllotWarehouseAreaWindow.Close();
            this.GetWarehouseAreas();
        }

        private void PartsShiftButton_Click(object sender, RoutedEventArgs e) {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null)
                return;
            var partsStocks = this.VirtualPartsStockForPartsShiftOrderDataGridView.SelectedEntities.Cast<VirtualPartsStock>().ToArray();

            var warehouse = Warehouses.SingleOrDefault(r => r.Id == this.VirtualPartsStockWarehouseId);
            if(warehouse == null)
                return;
            partsShiftOrder.WarehouseId = warehouse.Id;
            partsShiftOrder.BranchId = warehouse.BranchId;
            partsShiftOrder.WarehouseCode = warehouse.Code;
            partsShiftOrder.StorageCompanyId = warehouse.StorageCompanyId;
            partsShiftOrder.StorageCompanyType = warehouse.StorageCompanyType;
            partsShiftOrder.WarehouseName = this.VirtualPartsStockWarehouseName;
            this.PartsShiftOrderDetailOriginalWarehouseAreaCategory = this.VirtualPartsStockOriginalWarehouseAreaCategory;
            this.PartsShiftOrderDetailDestWarehouseAreaCategory = this.VirtualPartsStockDestWarehouseAreaCategory;
            if(partsStocks.Any(virtualPartsStock => partsShiftOrder.PartsShiftOrderDetails.Count(entity => entity.SparePartId == virtualPartsStock.SparePartId &&
                entity.OriginalWarehouseAreaId == virtualPartsStock.WarehouseAreaId && entity.BatchNumber == virtualPartsStock.BatchNumber) > 0)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_PartsShiftOrder_VirtualPartsStockIsRepeat);
                return;
            }
            foreach(var stock in partsStocks) {
                partsShiftOrder.PartsShiftOrderDetails.Add(new PartsShiftOrderDetail {
                    SparePartId = stock.SparePartId,
                    SparePartCode = stock.SparePartCode,
                    SparePartName = stock.SparePartName,
                    BatchNumber = stock.BatchNumber,
                    OriginalWarehouseAreaId = stock.WarehouseAreaId,
                    OriginalWarehouseAreaCode = stock.WarehouseAreaCode,
                    OriginalWarehouseAreaCategory = this.VirtualPartsStockOriginalWarehouseAreaCategory,
                    DestWarehouseAreaCategory = this.VirtualPartsStockDestWarehouseAreaCategory,
                    SourceStockAmount = stock.Quantity - (stock.LockedQty ?? 0),
                    Quantity = stock.Quantity - (stock.LockedQty ?? 0)
                });
                this.VirtualPartsStocks.Add(stock);
            }
            this.GetWarehouseAreas();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e) {
            if(this.VirtualPartsStockWarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_Warehouse_NameIsNull);
                return;
            }
            if(this.VirtualPartsStockOriginalWarehouseAreaCategory == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_VirtualPartsStock_OriginalWarehouseAreaCategoryIsNull);
                return;
            }

            if(this.VirtualPartsStockDestWarehouseAreaCategory == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_VirtualPartsStock_DestWarehouseAreaCategoryIsNull);
                return;
            }
            var newCompositeFilter = new CompositeFilterItem();
            newCompositeFilter.Filters.Add(new FilterItem {
                MemberName = "WarehouseId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = this.VirtualPartsStockWarehouseId
            });
            newCompositeFilter.Filters.Add(new FilterItem {
                MemberName = "WarehouseAreaCategory",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = this.VirtualPartsStockOriginalWarehouseAreaCategory
            });
            if(!string.IsNullOrWhiteSpace(this.VirtualPartsStockSparePartCode))
                newCompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "SparePartCode",
                    MemberType = typeof(string),
                    Operator = FilterOperator.IsEqualTo,
                    Value = this.VirtualPartsStockSparePartCode
                });
            if(!string.IsNullOrWhiteSpace(this.VirtualPartsStockSparePartName))
                newCompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "SparePartName",
                    MemberType = typeof(string),
                    Operator = FilterOperator.IsEqualTo,
                    Value = this.VirtualPartsStockSparePartName
                });
            if(!string.IsNullOrWhiteSpace(this.VirtualPartsStockWarehouseAreaCode))
                newCompositeFilter.Filters.Add(new FilterItem {
                    MemberName = "WarehouseAreaCode",
                    MemberType = typeof(string),
                    Operator = FilterOperator.IsEqualTo,
                    Value = this.VirtualPartsStockWarehouseAreaCode
                });
            this.VirtualPartsStockForPartsShiftOrderDataGridView.FilterItem = newCompositeFilter;
            this.VirtualPartsStockForPartsShiftOrderDataGridView.ExecuteQuery();
        }

        private void VirtualPartsStockForPartsShiftOrderDataGridView_SelectionChanged(object sender, EventArgs e) {
            this.CanShift = this.VirtualPartsStockForPartsShiftOrderDataGridView.SelectedEntities != null && this.VirtualPartsStockForPartsShiftOrderDataGridView.SelectedEntities.Any();
        }
        #endregion

        private void GetWarehouseAreas() {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null)
                return;
            this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.DicWarehouseAreas.Clear();
            var sparePartIds = partsShiftOrder.PartsShiftOrderDetails.Select(detail => detail.SparePartId).ToArray();
            this.DomainContext.Load(this.DomainContext.查询配件定位存储Query(sparePartIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                var partIds = loadOp.Entities.Select(e => e.PartId).ToArray();
                foreach(var partId in partIds) {
                    var id = partId;
                    var warehouseAreas = loadOp.Entities.Where(e => e.PartId == id && e.WarehouseArea != null && e.WarehouseAreaCategory != null && e.WarehouseAreaCategory.Category == this.PartsShiftOrderDetailDestWarehouseAreaCategory).Select(e => e.WarehouseArea).Distinct().ToList();
                    if(warehouseAreas == null || !warehouseAreas.Any())
                        continue;
                    if(this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.DicWarehouseAreas.All(v => v.Key != partId))
                        this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.DicWarehouseAreas.Add(partId, warehouseAreas);
                    else
                        this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.DicWarehouseAreas[partId] = warehouseAreas;
                }
            }, null);
        }

        private void ClearPartsShiftOrderDetails() {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null)
                return;
            if(partsShiftOrder.PartsShiftOrderDetails.Count <= 0)
                return;
            foreach(var partsShiftOrderDetail in partsShiftOrder.PartsShiftOrderDetails)
                partsShiftOrder.PartsShiftOrderDetails.Remove(partsShiftOrderDetail);
            this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.CommitEdit();
        }

        private void CreateUI() {
            Action loadWarehouse = () => this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(e => e.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效 && (e.PwmsInterface == false || e.PwmsInterface == null)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.Warehouses.Clear();
                foreach(var warehouse in loadOp.Entities) {
                    this.KvWarehouseNames.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                    this.Warehouses.Add(warehouse);
                }
            }, null);
            if(this.DomainContext == null)
                this.Initializer.Register(loadWarehouse);
            else
                loadWarehouse.Invoke();
            this.VirtualPartsStockForPartsShiftOrderDataGridView.SetValue(Grid.ColumnProperty, 0);
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = PartsStockingUIStrings.DataGridView_Title_PartsStocks,
                Content = this.VirtualPartsStockForPartsShiftOrderDataGridView
            });
            this.Bottom.Children.Add(tabControl);
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(PartsStockingUIStrings.DataGridView_Title_PartsShiftOrderDetails, null, () => this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView);
            detailEditView.SetValue(Grid.RowProperty, 1);
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_AllotWarehouseArea,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/distribute.png", UriKind.Relative),
                Command = new DelegateCommand(this.AllotWarehouseArea)
            });
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Action_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ShowFileDialog)
            });
            detailEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Title_ExportTemplate,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            this.Left.Children.Add(detailEditView);
            this.KeyValueManager.LoadData(() => {
                this.KvDestnationWarehouseAreaAreaCategoryTypeTypes.Clear();
                foreach (var value in this.keyValueManager[this.kvNames[0]]) { 
                    if(value.Key == (int)DcsAreaType.保管区 || value.Key == (int)DcsAreaType.问题区)
                    this.KvDestnationWarehouseAreaAreaCategoryTypeTypes.Add(value);
                }
                this.KvWarehouseAreaCategoryTypes.Clear();
                foreach (var value in this.keyValueManager[this.kvNames[0]]) { 
                    if(value.Key == (int)DcsAreaType.保管区 || value.Key == (int)DcsAreaType.问题区)
                    this.KvWarehouseAreaCategoryTypes.Add(value);
                }
            });
        }

        private void ShowFileDialog() {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if (partsShiftOrder == null)
                return;
            var warehouse = Warehouses.SingleOrDefault(r => r.Id == this.VirtualPartsStockWarehouseId);
            if (warehouse == null) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_WarehouseNotNull);
                return;
            }
            if (this.VirtualPartsStockOriginalWarehouseAreaCategory == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_OriginalWarehouseArea);
                return;
            }
            if (this.VirtualPartsStockDestWarehouseAreaCategory == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_DestWarehouseArea);
                return;
            }
            partsShiftOrder.WarehouseId = warehouse.Id;
            partsShiftOrder.BranchId = warehouse.BranchId;
            partsShiftOrder.WarehouseCode = warehouse.Code;
            partsShiftOrder.StorageCompanyId = warehouse.StorageCompanyId;
            partsShiftOrder.StorageCompanyType = warehouse.StorageCompanyType;
            partsShiftOrder.WarehouseName = this.VirtualPartsStockWarehouseName;
            this.PartsShiftOrderDetailOriginalWarehouseAreaCategory = this.VirtualPartsStockOriginalWarehouseAreaCategory;
            this.PartsShiftOrderDetailDestWarehouseAreaCategory = this.VirtualPartsStockDestWarehouseAreaCategory;
            this.Uploader.ShowFileDialog();
        }

        public ICommand ExportTemplateCommand {
            get {
                if (this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_OriginalWarehouseAreaCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DestWarehouseAreaCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_ShiftQuantity,
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        private void AllotWarehouseArea() {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null)
                return;
            if(this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.SelectedEntities == null || !this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.SelectedEntities.Any())
                return;
            if(this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.SelectedEntities != null) {
                var partIds = this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.SelectedEntities.Cast<PartsShiftOrderDetail>().Select(e => e.SparePartId).ToArray();
                this.PartsLocationAssignmentDataEditView.AddNewPartsStock(partIds);
            }
            this.PartsLocationAssignmentDataEditView.GetWarehouseAreaByWarehouseId(this.VirtualPartsStockWarehouseId);
            this.AllotWarehouseAreaWindow.ShowDialog();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsShiftOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.CommitEdit())
                return;
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null && !this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.CommitEdit() && !this.VirtualPartsStockForPartsShiftOrderDataGridView.CommitEdit())
                return;
            if(!partsShiftOrder.PartsShiftOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShiftOrder_PartsShiftOrderDetailsIsEmpty);
                return;
            }
            partsShiftOrder.QuestionType = this.questionType;
            //问题区移库，必选移库问题类型
            if (partsShiftOrder.PartsShiftOrderDetails.Any(r => r.DestWarehouseAreaCategory == (int)DcsAreaType.问题区) || partsShiftOrder.PartsShiftOrderDetails.Any(r => r.OriginalWarehouseAreaCategory == (int)DcsAreaType.问题区)) {
                if (partsShiftOrder.QuestionType == null || partsShiftOrder.QuestionType == default(int)) { 
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Text_PartsShiftOrderQuestionTypeIsNull);
                    return;
                }
            }
            foreach(var partsShiftOrderDetail in partsShiftOrder.PartsShiftOrderDetails) {
                var virtualPartsStock = this.VirtualPartsStocks.SingleOrDefault(r => r.WarehouseAreaId == partsShiftOrderDetail.OriginalWarehouseAreaId && r.SparePartId == partsShiftOrderDetail.SparePartId);
                if(partsShiftOrderDetail.SourceStockAmount - partsShiftOrderDetail.Quantity != 0)
                    this.VirtualPartsStocks.Remove(virtualPartsStock);
            }
            partsShiftOrder.ValidationErrors.Clear();
            if(partsShiftOrder.PartsShiftOrderDetails.GroupBy(entity => new {
                entity.SparePartId,
                entity.BatchNumber,
                entity.OriginalWarehouseAreaId
            }).Any(array => array.Count() > 1)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShiftOrderDetails_DoubleSparePartIdAndBatchNumberAndOriginalWarehouseAreaId);
                return;
            }
            if(string.IsNullOrWhiteSpace(partsShiftOrder.WarehouseName))
                partsShiftOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_Warehouse_NameIsNull, new[] {
                    "WarehouseName"
                }));
            if(this.PartsShiftOrderDetailOriginalWarehouseAreaCategory == 0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_Warehouse_NameIsNull);
                return;
            }
            if(this.PartsShiftOrderDetailDestWarehouseAreaCategory == 0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_Warehouse_NameIsNull);
                return;
            }
            foreach(var detail in partsShiftOrder.PartsShiftOrderDetails) {
                detail.ValidationErrors.Clear();
                if(string.IsNullOrWhiteSpace(detail.DestWarehouseAreaCode))
                    detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_PartsShiftOrderDetail_DestWarehouseAreaCodeIsNull, new[] {
                        "DestWarehouseAreaCode"
                    }));
                if(detail.OriginalWarehouseAreaId == detail.DestWarehouseAreaId)
                    detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_PartsShiftOrderDetails_OriginalWarehouseAreaEqualDestWarehouseArea, new[] {
                        "DestWarehouseAreaCode"
                    }));
            }
            if(partsShiftOrder.HasValidationErrors || partsShiftOrder.PartsShiftOrderDetails.Any(relation => relation.HasValidationErrors))
                return;
            ((IEditableObject)partsShiftOrder).EndEdit();
            try {
                if (partsShiftOrder.EntityState == EntityState.New) {
                    if(partsShiftOrder.Can生成配件移库单)
                          partsShiftOrder.生成配件移库单();
                }
                ((IEditableObject)partsShiftOrder).EndEdit();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        public string VirtualPartsStockWarehouseName {
            get;
            set;
        }

        public int VirtualPartsStockOriginalWarehouseAreaCategory {
            get {
                return this.virtualPartsStockOriginalWarehouseAreaCategory;
            }
            set {
                this.virtualPartsStockOriginalWarehouseAreaCategory = value;
                this.OnPropertyChanged("VirtualPartsStockOriginalWarehouseAreaCategory");
            }
        }

        public string VirtualPartsStockSparePartCode {
            get;
            set;
        }

        public string VirtualPartsStockWarehouseAreaCode {
            get;
            set;
        }

        public int VirtualPartsStockDestWarehouseAreaCategory {
            get {
                return this.virtualPartsStockDestWarehouseAreaCategory;
            }
            set {
                this.virtualPartsStockDestWarehouseAreaCategory = value;
                this.OnPropertyChanged("VirtualPartsStockDestWarehouseAreaCategory");
            }
        }

        public string VirtualPartsStockSparePartName {
            get;
            set;
        }

        public int PartsShiftOrderDetailOriginalWarehouseAreaCategory {
            get {
                return this.partsShiftOrderDetailOriginalWarehouseAreaCategory;
            }
            set {
                this.partsShiftOrderDetailOriginalWarehouseAreaCategory = value;
                this.OnPropertyChanged("PartsShiftOrderDetailOriginalWarehouseAreaCategory");
            }
        }

        public int PartsShiftOrderDetailDestWarehouseAreaCategory {
            get {
                return this.partsShiftOrderDetailDestWarehouseAreaCategory;
            }
            set {
                this.partsShiftOrderDetailDestWarehouseAreaCategory = value;
                this.OnPropertyChanged("PartsShiftOrderDetailDestWarehouseAreaCategory");
            }
        }

        public int? QuestionType {
            get {
                return this.questionType;
            }
            set {
                this.questionType = value;
                this.OnPropertyChanged("QuestionType");
            }
        }

        public int VirtualPartsStockWarehouseId {
            get {
                return this.virtualPartsStockWarehouseId;
            }
            set {
                this.virtualPartsStockWarehouseId = value;
                this.OnPropertyChanged("VirtualPartsStockWarehouseId");
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouseAreaCategoryTypes {
            get {
                return this.kvDestWarehouseAreaCategoryTypes;
            }
        }

        public DataGridViewBase VirtualPartsStockForPartsShiftOrderDataGridView {
            get {
                if(this.virtualPartsStockForPartsShiftOrderDataGridView == null) {
                    this.virtualPartsStockForPartsShiftOrderDataGridView = DI.GetDataGridView("VirtualPartsStockForPartsShiftOrder");
                    this.virtualPartsStockForPartsShiftOrderDataGridView.DomainContext = new DcsDomainContext();
                    this.virtualPartsStockForPartsShiftOrderDataGridView.SelectionChanged += this.VirtualPartsStockForPartsShiftOrderDataGridView_SelectionChanged;
                }
                return this.virtualPartsStockForPartsShiftOrderDataGridView;
            }
        }

        public ObservableCollection<KeyValuePair> KvDestnationWarehouseAreaAreaCategoryTypeTypes {
            get {
                return this.kvDestWarehouseAreaCategoryTypes;
            }
        }

        
        public object KvQuestionTypes {
            get {
                return this.KeyValueManager[kvNames[1]];
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouseNames {
            get {
                return this.kvWarehouseNames;
            }
        }

        public ObservableCollection<Warehouse> Warehouses {
            get {
                return warehouses ?? (this.warehouses = new ObservableCollection<Warehouse>());
            }
            set {
                this.warehouses = value;
            }
        }

        protected override void Reset() {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null)
                return;
            if(this.DomainContext.PartsShiftOrders.Contains(partsShiftOrder)) {
                this.DomainContext.PartsShiftOrders.Detach(partsShiftOrder);
            }
        }

        public bool CanShift {
            get {
                return this.canShift;
            }
            set {
                this.canShift = value;
                this.OnPropertyChanged("CanShift");
            }
        }
    }
}
