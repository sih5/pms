﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShiftOrderDownDataEditView {
        public PartsShiftOrderDownDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private DataGridViewBase partsShiftOrderDetailForApproveDataEditView;

        private DataGridViewBase PartsShiftOrderDetailForApproveDataEditView {
            get {
                if(this.partsShiftOrderDetailForApproveDataEditView == null) {
                    this.partsShiftOrderDetailForApproveDataEditView = DI.GetDataGridView("PartsShiftOrderDetailForDown");
                    this.partsShiftOrderDetailForApproveDataEditView.DomainContext = this.DomainContext;
                }
                return this.partsShiftOrderDetailForApproveDataEditView;
            }
        }

        private PartsShiftOrderForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsShiftOrderForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsShiftOrderForUploadDataEditPanel)DI.GetDataEditPanel("PartsShiftOrderForUpload"));
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsShiftOrderForApprove"));
            FileUploadDataEditPanels.isHiddenButtons = true;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 2);
            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            this.Root.Children.Add(FileUploadDataEditPanels);
            this.Root.Children.Add(this.CreateVerticalLine(1, 1, 2, 0));
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsStockingUIStrings.DataGridView_Title_PartsShiftOrderDetails, null, () => this.PartsShiftOrderDetailForApproveDataEditView);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            dcsDetailGridView.SetValue(Grid.ColumnProperty, 2);
            dcsDetailGridView.SetValue(Grid.RowSpanProperty, 2);
            this.Root.Children.Add(dcsDetailGridView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShiftOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                FileUploadDataEditPanels.FilePath = entity.Path;
                foreach(var item in entity.PartsShiftOrderDetails){
                    item.CourrentDownShelfQty = item.Quantity-(item.DownShelfQty??0);
                }
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Text_DownPartsShiftOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsShiftOrderDetailForApproveDataEditView.CommitEdit())
                return;
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            var dcsDomainContext = new DcsDomainContext();
            if(partsShiftOrder == null)
                return;
            if(ShellViewModel.Current.IsBusy) {
                return;
            }
            if(partsShiftOrder.PartsShiftOrderDetails.All(t => t.CourrentDownShelfQty <= 0)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation9);
                return;
            }
            foreach(var item in partsShiftOrder.PartsShiftOrderDetails) {
                if(item.CourrentDownSIHCode != null && item.CourrentDownShelfQty.Value > 0 && !string.IsNullOrEmpty(item.CourrentDownSIHCode)) {
                    var sihCode = item.CourrentDownSIHCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                    if(item.TraceProperty.HasValue && item.TraceProperty == (int)DCSTraceProperty.精确追溯 && sihCode.Count() != item.CourrentDownShelfQty) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation10);
                        return;
                    } else if(sihCode.Count() != 1) {
                        UIHelper.ShowNotification(item.SparePartCode+ PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation11);
                        return;
                    }
                    if(item.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        foreach(var code in sihCode) {
                            if(!string.IsNullOrEmpty(item.DownSIHCode)&& item.DownSIHCode.Contains(code)) {
                                UIHelper.ShowNotification(code+"已下架，请重新输入");
                                return;
                            }
                        }
                    } 

                } 
                //else if(((item.CourrentDownShelfQty == null || item.CourrentDownShelfQty.Value == 0) && !string.IsNullOrEmpty(item.CourrentDownSIHCode)) || (item.CourrentDownShelfQty != null && item.CourrentDownShelfQty.Value > 0 && string.IsNullOrEmpty(item.CourrentDownSIHCode) || item.CourrentDownSIHCode==null)) {
                //    UIHelper.ShowNotification(item.SparePartCode + PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation12);
                //    return;
                //}
                item.ValidationErrors.Clear();
            }
            partsShiftOrder.ValidationErrors.Clear();

            ((IEditableObject)partsShiftOrder).EndEdit();
            ShellViewModel.Current.IsBusy = true;
            try {
                if(partsShiftOrder.Can下架配件移库单) {
                    partsShiftOrder.下架配件移库单();
                    ShellViewModel.Current.IsBusy = false;
                }
            } catch(ValidationException ex) {
                ShellViewModel.Current.IsBusy = false;
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
            ShellViewModel.Current.IsBusy = false;
        }
       
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder != null && this.DomainContext.PartsShiftOrders.Contains(partsShiftOrder))
                this.DomainContext.PartsShiftOrders.Detach(partsShiftOrder);
        }

    }
}
