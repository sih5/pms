﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShiftOrderForDetailsDataEditView {
        public PartsShiftOrderForDetailsDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase partsShiftOrderDetailForApproveDataEditView;

        private DataGridViewBase PartsShiftOrderDetailForApproveDataEditView {
            get {
                if(this.partsShiftOrderDetailForApproveDataEditView == null) {
                    this.partsShiftOrderDetailForApproveDataEditView = DI.GetDataGridView("PartsShiftOrderDetailForApprove");
                    this.partsShiftOrderDetailForApproveDataEditView.DomainContext = this.DomainContext;
                }
                return this.partsShiftOrderDetailForApproveDataEditView;
            }
        }

        private PartsShiftOrderForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsShiftOrderForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsShiftOrderForUploadDataEditPanel)DI.GetDataEditPanel("PartsShiftOrderForUpload"));
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsShiftOrderForDetail"));
            FileUploadDataEditPanels.isHiddenButtons = true;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 2);
            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            this.Root.Children.Add(FileUploadDataEditPanels);
            this.Root.Children.Add(this.CreateVerticalLine(1, 1, 2, 0));
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsStockingUIStrings.DataGridView_Title_PartsShiftOrderDetails, null, () => this.PartsShiftOrderDetailForApproveDataEditView);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            dcsDetailGridView.SetValue(Grid.ColumnProperty, 2);
            dcsDetailGridView.SetValue(Grid.RowSpanProperty, 2);
            this.Root.Children.Add(dcsDetailGridView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShiftOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                FileUploadDataEditPanels.FilePath = entity.Path;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string Title {
            get {
                return "查看移库单详情";
            }
        }


        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }     

    }
}