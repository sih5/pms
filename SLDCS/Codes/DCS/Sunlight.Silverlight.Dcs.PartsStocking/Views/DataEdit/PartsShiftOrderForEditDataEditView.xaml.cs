﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;
using System.Collections.Generic;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShiftOrderForEditDataEditView {
        private ObservableCollection<KeyValuePair> kvTypes;
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvQuestionType;
        private PartsShiftOrderDetailEditDataGridView partsShiftOrderDetailForPartsShiftOrderForEditDataGridView;

        private readonly string[] kvNames = {
           "PartsShiftOrder_Type","PartsShiftOrder_QuestionType"
        };
      

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsShiftOrder;
            }
        }
        public ObservableCollection<KeyValuePair> KvTypes {
            get {
                return this.kvTypes ?? (this.kvTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvQuestionTypes {
            get {
                return this.kvQuestionType ?? (this.kvQuestionType = new ObservableCollection<KeyValuePair>());
            }
        }


      

        protected override bool OnRequestCanSubmit()
        {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShiftOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {                    
                    this.SetObjectToEdit(entity);
                }
            }, null);


        }


        private PartsShiftOrderDetailEditDataGridView PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView {
            get {
                if(this.partsShiftOrderDetailForPartsShiftOrderForEditDataGridView == null) {
                    this.partsShiftOrderDetailForPartsShiftOrderForEditDataGridView = new PartsShiftOrderDetailEditDataGridView();
                    this.partsShiftOrderDetailForPartsShiftOrderForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsShiftOrderDetailForPartsShiftOrderForEditDataGridView;
            }
        }

        private void CreateUI() {
            if(!this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView.CommitEdit())
                return;
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsStockingUIStrings.DataGridView_Title_PartsShiftOrderDetails, null, () => this.PartsShiftOrderDetailForPartsShiftOrderForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);           
            this.Root.Children.Add(detailDataEditView);

            KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]]) {
                    this.KvTypes.Add(keyValuePair);
                }
                foreach(var keyValuePair in this.KeyValueManager[this.kvNames[1]]) {
                    this.KvQuestionTypes.Add(keyValuePair);
                }
            });
            PartsPurchaseOrderTypeName.ItemsSource = KvTypes;
            QuestionTypeName.ItemsSource = KvQuestionTypes;
          
        }


        protected override void OnEditSubmitting() {
            
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null)
                return;
            ((IEditableObject)partsShiftOrder).EndEdit();
            partsShiftOrder.ValidationErrors.Clear();
            if(partsShiftOrder.PartsShiftOrderDetails.Count()==0) {
                UIHelper.ShowNotification("清单不允许为空");
                return;
            }
            foreach(var item in partsShiftOrder.PartsShiftOrderDetails) {
                ((IEditableObject)item).EndEdit();
                if(item.OriginalWarehouseAreaId==item.DestWarehouseAreaId){
                    UIHelper.ShowNotification(item.SparePartCode+"目标库位与源库位不允许一样");
                    return;
                }
                item.ValidationErrors.Clear();
            }           
                try {

                    if(partsShiftOrder.Can修改移库单)
                        partsShiftOrder.修改移库单();
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
                base.OnEditSubmitting();
           
        }

        public PartsShiftOrderForEditDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.LoadData();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var partsPurchasePlan = this.DataContext as PartsPurchasePlan;
            if(this.DomainContext.PartsPurchasePlans.Contains(partsPurchasePlan))
                this.DomainContext.PartsPurchasePlans.Detach(partsPurchasePlan);
        }

    }
}