﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShiftOrderForInitialApproveDataEditView {
        private ButtonItem rejectBtn;

        public PartsShiftOrderForInitialApproveDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override bool OnRequestCanSubmit()
        {
            return true;
        }

        private DataGridViewBase partsShiftOrderDetailForApproveDataEditView;

        private DataGridViewBase PartsShiftOrderDetailForApproveDataEditView {
            get {
                if(this.partsShiftOrderDetailForApproveDataEditView == null) {
                    this.partsShiftOrderDetailForApproveDataEditView = DI.GetDataGridView("PartsShiftOrderDetailForApprove");
                    this.partsShiftOrderDetailForApproveDataEditView.DomainContext = this.DomainContext;
                }
                return this.partsShiftOrderDetailForApproveDataEditView;
            }
        }

        private PartsShiftOrderForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsShiftOrderForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsShiftOrderForUploadDataEditPanel)DI.GetDataEditPanel("PartsShiftOrderForUpload"));
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsShiftOrderForApprove"));
            FileUploadDataEditPanels.isHiddenButtons = true;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 2);
            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            this.Root.Children.Add(FileUploadDataEditPanels);
            this.Root.Children.Add(this.CreateVerticalLine(1,1,2,0));
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsStockingUIStrings.DataGridView_Title_PartsShiftOrderDetails, null, () => this.PartsShiftOrderDetailForApproveDataEditView);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            dcsDetailGridView.SetValue(Grid.ColumnProperty, 2);
            dcsDetailGridView.SetValue(Grid.RowSpanProperty, 2);
            this.Root.Children.Add(dcsDetailGridView);

            this.rejectBtn = new ButtonItem {
                Command = new DelegateCommand(this.RejecrCurrentData),
                Title = PartsStockingUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            };
            this.RegisterButton(rejectBtn);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShiftOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                FileUploadDataEditPanels.FilePath = entity.Path;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Text_InitApprovePartsShiftOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsShiftOrderDetailForApproveDataEditView.CommitEdit())
                return;
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null)
                return;
            partsShiftOrder.ValidationErrors.Clear();
           
            ((IEditableObject)partsShiftOrder).EndEdit();
            try {
               if(partsShiftOrder.Can初审配件移库单)
                    partsShiftOrder.初审配件移库单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder != null && this.DomainContext.PartsShiftOrders.Contains(partsShiftOrder))
                this.DomainContext.PartsShiftOrders.Detach(partsShiftOrder);
        }
        
        private void RejecrCurrentData() {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null)
                return;
            ((IEditableObject)partsShiftOrder).EndEdit();
            try {
               if(partsShiftOrder.Can驳回配件移库单)
                    partsShiftOrder.驳回配件移库单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }


    }
}
