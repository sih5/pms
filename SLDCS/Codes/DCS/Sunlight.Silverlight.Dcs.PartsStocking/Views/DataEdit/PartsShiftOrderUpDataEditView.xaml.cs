﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShiftOrderUpDataEditView {
        public PartsShiftOrderUpDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private DataGridViewBase partsShiftOrderDetailForApproveDataEditView;

        private DataGridViewBase PartsShiftOrderDetailForApproveDataEditView {
            get {
                if(this.partsShiftOrderDetailForApproveDataEditView == null) {
                    this.partsShiftOrderDetailForApproveDataEditView = DI.GetDataGridView("PartsShiftOrderDetailForUp");
                    this.partsShiftOrderDetailForApproveDataEditView.DomainContext = this.DomainContext;
                }
                return this.partsShiftOrderDetailForApproveDataEditView;
            }
        }

        private PartsShiftOrderForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsShiftOrderForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsShiftOrderForUploadDataEditPanel)DI.GetDataEditPanel("PartsShiftOrderForUpload"));
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsShiftOrderForApprove"));
            FileUploadDataEditPanels.isHiddenButtons = true;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 2);
            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            this.Root.Children.Add(FileUploadDataEditPanels);
            this.Root.Children.Add(this.CreateVerticalLine(1, 1, 2, 0));
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsStockingUIStrings.DataGridView_Title_PartsShiftOrderDetails, null, () => this.PartsShiftOrderDetailForApproveDataEditView);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            dcsDetailGridView.SetValue(Grid.ColumnProperty, 2);
            dcsDetailGridView.SetValue(Grid.RowSpanProperty, 2);
            this.Root.Children.Add(dcsDetailGridView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShiftOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                foreach(var item in entity.PartsShiftOrderDetails) {
                    item.CourrentUpShelfQty = item.DownShelfQty-(item.UpShelfQty??0);
                }
                FileUploadDataEditPanels.FilePath = entity.Path;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Text_UpPartsShiftOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsShiftOrderDetailForApproveDataEditView.CommitEdit())
                return;
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null)
                return;
            var dcsDomainContext = new DcsDomainContext();
            if(partsShiftOrder.PartsShiftOrderDetails.All(t => t.CourrentUpShelfQty <= 0)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation3);
                return;
            }
            foreach(var item in partsShiftOrder.PartsShiftOrderDetails) {
                if(item.CourrentUpShelfQty != null && item.CourrentUpShelfQty.Value > 0 && !string.IsNullOrEmpty(item.CourrentSIHCode)) {
                    var sihCode = item.CourrentSIHCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                    if(item.TraceProperty.HasValue && item.TraceProperty == (int)DCSTraceProperty.精确追溯 && sihCode.Count() != item.CourrentUpShelfQty) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation4);
                        return;
                    } else if(sihCode.Count() != 1) {
                        UIHelper.ShowNotification(item.SparePartCode+PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation5);
                        return;
                    }
                    if(item.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                        foreach(var code in sihCode) {
                            if(!string.IsNullOrEmpty(item.SIHCode) && item.SIHCode.Contains(code)) {
                                UIHelper.ShowNotification(code + "已上架，请重新输入");
                                return;
                            }
                        }
                    } 
                    //else if(!string.IsNullOrEmpty(item.SIHCode) && !item.SIHCode.Equals(sihCode[0])) {
                    //    UIHelper.ShowNotification(item.SparePartCode+"只能填写一个上架标签");
                    //    return;
                    //}
                } 
                //else if(((item.CourrentUpShelfQty == null || item.CourrentUpShelfQty.Value == 0) && !string.IsNullOrEmpty(item.CourrentSIHCode)) || (item.CourrentUpShelfQty != null && item.CourrentUpShelfQty.Value > 0 && string.IsNullOrEmpty(item.CourrentSIHCode) || item.CourrentSIHCode == null)) {
                //    UIHelper.ShowNotification(item.SparePartCode + PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation6);
                //    return;
                //}
                item.ValidationErrors.Clear();
            }
            partsShiftOrder.ValidationErrors.Clear();

            ((IEditableObject)partsShiftOrder).EndEdit();
            ShellViewModel.Current.IsBusy = true;
            try {
                if(partsShiftOrder.Can上架配件移库单) {
                    partsShiftOrder.上架配件移库单();
                    ShellViewModel.Current.IsBusy = false;
                }
            } catch(ValidationException ex) {
                ShellViewModel.Current.IsBusy = false;
                UIHelper.ShowAlertMessage(ex.Message);                
                return;
            }
            base.OnEditSubmitting();
            ShellViewModel.Current.IsBusy = false;

        }       

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder != null && this.DomainContext.PartsShiftOrders.Contains(partsShiftOrder))
                this.DomainContext.PartsShiftOrders.Detach(partsShiftOrder);
        }

    }
}
