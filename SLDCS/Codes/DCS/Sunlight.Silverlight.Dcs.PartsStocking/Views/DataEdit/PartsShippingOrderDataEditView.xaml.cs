﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShippingOrderDataEditView : INotifyPropertyChanged {
        private DataGridViewBase partsOutboundPlanForShippingDataGridView;
        private DataGridViewBase partsShippingOrderRefForOutBoundDataGridView;
        private TaskListPartsOutboundPlan partsOutboundPlan;
        private ObservableCollection<PartsOutboundPlan> partsOutboundPlanShippings = new ObservableCollection<PartsOutboundPlan>();
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<TaskListPartsOutboundPlan> partsOutboundPlans = new ObservableCollection<TaskListPartsOutboundPlan>();
        private readonly ObservableCollection<KeyValuePair> kvTypes = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvShippingMethods = new ObservableCollection<KeyValuePair>();
        private KeyValueManager keyValueManager;
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesOrderType = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();
        private string code, counterpartCompanyCode, counterpartCompanyName;
        private int? warehouseId, outboundType, partsSalesOrderTypeId, partsSalesCategoryId, shippingMethod;
        private DateTime? createTimeForm, createTimeTo;
        private string tCode, tCounterpartCompanyCode, tCounterpartCompanyName;
        private int? tWarehouseId, tOutboundType, tPartsSalesOrderTypeId, tPartsSalesCategoryId, tShippingMethod;
        private DateTime? tCreateTimeForm, tCreateTimeTo;
        private int shippingMethodByCustomer;
        private readonly string[] kvNames = {
            "Parts_OutboundType","PartsShipping_Method"
        };

        public PartsShippingOrderDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsShippingOrderDataEditView_DataContextChanged;
        }

        #region 界面事件

        private void PartsShippingOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            this.Code = this.CounterpartCompanyCode = this.CounterpartCompanyName = default(string);
            this.WarehouseId = this.ShippingMethod = this.OutboundType = this.PartsSalesOrderTypeId = this.PartsSalesCategoryId = default(int?);
            //this.CreateTimeForm = this.CreateTimeTo = default(DateTime?);
            if(this.DomainContext == null)
                this.Initializer.Register(() => {
                    if(partsShippingOrder.Id == default(int) && this.gdShow.Visibility == Visibility.Collapsed)
                        this.HideSaveButton();
                    else
                        this.ShowSaveButton();
                });
            else {
                if(partsShippingOrder.Id == default(int) && this.gdShow.Visibility == Visibility.Collapsed)
                    this.HideSaveButton();
                else
                    this.ShowSaveButton();
            }
            this.gdSearch.Visibility = Visibility.Visible;
            this.gdShow.Visibility = Visibility.Collapsed;
            partsShippingOrder.PropertyChanged += partsShippingOrder_PropertyChanged;
        }

        void partsShippingOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            if(e.PropertyName.Equals("ShippingMethod")) {
                if(partsShippingOrder.ShippingMethod == shippingMethodByCustomer)
                    partsShippingOrder.ValidationErrors.Clear();
            }
        }

        private void PartsOutboundPlanForShippingDataGridView_SelectionChanged(object sender, EventArgs e) {
            var dataGrid = sender as DcsDataGridViewBase;
            if(dataGrid == null || dataGrid.SelectedEntities == null || !dataGrid.SelectedEntities.Any()) {
                this.radButtondShipping.IsEnabled = false;
                partsOutboundPlan = null;
                return;
            }
            var selects=dataGrid.SelectedEntities.Cast<TaskListPartsOutboundPlan>().ToArray();
			var warehouseIdSelect=selects.Where(t=>t.ReceivingWarehouseId!=null).FirstOrDefault();
            if(warehouseIdSelect != null && warehouseIdSelect.ReceivingWarehouseId!=null && selects.Any(y => y.ReceivingWarehouseId != warehouseIdSelect.ReceivingWarehouseId)) {
				 this.radButtondShipping.IsEnabled = false;
                 return;
			 }
            this.partsOutboundPlan = dataGrid.SelectedEntities.Cast<TaskListPartsOutboundPlan>().FirstOrDefault();
            this.radButtondShipping.IsEnabled = this.partsOutboundPlan != null;
        }

        private void RadButtondShipping_Click(object sender, RoutedEventArgs e) {
            if(partsOutboundPlan == null)
                return;
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            
            var selectPop = this.PartsOutboundPlanForShippingDataGridView.SelectedEntities.Cast<TaskListPartsOutboundPlan>().ToArray();
            foreach(var pop in selectPop) {
                if(partsOutboundPlan != null && (partsOutboundPlan.WarehouseId != pop.WarehouseId || partsOutboundPlan.ReceivingAddress != pop.ReceivingAddress ||
                 partsOutboundPlan.PartsSalesOrderTypeName != pop.PartsSalesOrderTypeName || partsOutboundPlan.ShippingMethod != pop.ShippingMethod ||
                 partsOutboundPlan.CounterpartCompanyId != pop.CounterpartCompanyId || partsOutboundPlan.IsTurn!=pop.IsTurn)) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrderShippingError);
                    return;
                }
                if(partsShippingOrder.ShippingMethod == null) {
                    partsShippingOrder.ShippingMethod = pop.ShippingMethod;
                }
            }
            partsShippingOrder.ReceivingCompanyCode = partsOutboundPlan.ReceivingCompanyCode;
            partsShippingOrder.ReceivingCompanyName = partsOutboundPlan.ReceivingCompanyName;
            partsShippingOrder.ReceivingCompanyId = partsOutboundPlan.ReceivingCompanyId;
            partsShippingOrder.ReceivingWarehouseCode = partsOutboundPlan.ReceivingWarehouseCode;
            partsShippingOrder.ReceivingWarehouseName = partsOutboundPlan.ReceivingWarehouseName;
            partsShippingOrder.ReceivingWarehouseId = partsOutboundPlan.ReceivingWarehouseId;
            partsShippingOrder.BranchId = partsOutboundPlan.BranchId;
            partsShippingOrder.PartsSalesCategoryId = partsOutboundPlan.PartsSalesCategoryId;
            partsShippingOrder.OriginalRequirementBillId = partsOutboundPlan.OriginalRequirementBillId;
            partsShippingOrder.OriginalRequirementBillType = partsOutboundPlan.OriginalRequirementBillType;
            partsShippingOrder.OriginalRequirementBillCode = partsOutboundPlan.OriginalRequirementBillCode;
            partsShippingOrder.ShippingCompanyCode = partsOutboundPlan.StorageCompanyCode;
            partsShippingOrder.ShippingCompanyName = partsOutboundPlan.StorageCompanyName;
            partsShippingOrder.ShippingCompanyId = partsOutboundPlan.StorageCompanyId;
            partsShippingOrder.GPMSPurOrderCode = partsOutboundPlan.GPMSPurOrderCode;
            partsShippingOrder.OrderApproveComment = partsOutboundPlan.OrderApproveComment;
            partsShippingOrder.SAPPurchasePlanCode = partsOutboundPlan.SAPPurchasePlanCode;
            partsShippingOrder.ERPSourceOrderCode = partsOutboundPlan.ERPSourceOrderCode;
            partsShippingOrder.PartsSalesCategoryId= partsOutboundPlan.PartsSalesCategoryId;
            partsShippingOrder.PartsSalesOrderTypeName = partsOutboundPlan.PartsSalesOrderTypeName;
            partsShippingOrder.LinkName = partsOutboundPlan.ContactPerson;
            partsShippingOrder.LinkPhone = partsOutboundPlan.ContactPhone;
            partsShippingOrder.AppraiserName = partsOutboundPlan.ApproverName;
            
            partsShippingOrder.SettlementCompanyCode = partsOutboundPlan.StorageCompanyCode;
            partsShippingOrder.SettlementCompanyId = partsOutboundPlan.StorageCompanyId;
            partsShippingOrder.SettlementCompanyName = partsOutboundPlan.StorageCompanyName;
            partsShippingOrder.ReceivingAddress = partsOutboundPlan.ReceivingAddress;
            partsShippingOrder.Remark = partsOutboundPlan.Remark;

            if(partsOutboundPlan.OutboundType == (int)DcsPartsOutboundType.配件销售 || partsOutboundPlan.OutboundType == (int)DcsPartsOutboundType.配件零售)
                partsShippingOrder.Type = (int)DcsPartsShippingOrderType.销售;
            if(partsOutboundPlan.OutboundType == (int)DcsPartsOutboundType.采购退货 && partsOutboundPlan.OriginalRequirementBillType != (int)DcsOriginalRequirementBillType.积压件调剂单) {
                partsShippingOrder.Type = (int)DcsPartsShippingOrderType.退货;

                //如果收货单位企业类型=供应商，并且出库类型=采购退货， 将收货单位企业信息中的：地址，联系人，联系人手机 带入到发运单.地址中。
                this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == partsOutboundPlan.ReceivingCompanyId && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var entity = loadOp.Entities.FirstOrDefault();
                    if(entity == null)
                        return;
                    if(entity.Type != (int)DcsCompanyType.配件供应商)
                        return;
                    if(!string.IsNullOrEmpty(entity.ContactAddress))
                        partsShippingOrder.ReceivingAddress = entity.ContactAddress;
                    if(!string.IsNullOrEmpty(entity.ContactPerson))
                        partsShippingOrder.ReceivingAddress += string.IsNullOrEmpty(partsShippingOrder.ReceivingAddress) ? entity.ContactPerson : "," + entity.ContactPerson;
                    if(!string.IsNullOrEmpty(entity.ContactMobile))
                        partsShippingOrder.ReceivingAddress += string.IsNullOrEmpty(partsShippingOrder.ReceivingAddress) ? entity.ContactMobile : "," + entity.ContactMobile;

                    if(partsOutboundPlan.ReceivingWarehouseId.HasValue) {
                        this.DomainContext.Load(this.DomainContext.GetWarehousesByIdWithStorageCompanyIdQuery(partsOutboundPlan.ReceivingWarehouseId.Value), LoadBehavior.RefreshCurrent, loadOpWarehouses => {
                            if(loadOpWarehouses.HasError) {
                                if(!loadOpWarehouses.IsErrorHandled)
                                    loadOpWarehouses.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOpWarehouses);
                                return;
                            }
                            var entityWarehouses = loadOpWarehouses.Entities.FirstOrDefault();
                            if(entityWarehouses == null)
                                return;

                            if(!string.IsNullOrEmpty(entityWarehouses.Address))
                                partsShippingOrder.ReceivingAddress = entityWarehouses.Address;
                            if(!string.IsNullOrEmpty(entityWarehouses.Contact))
                                partsShippingOrder.ReceivingAddress += string.IsNullOrEmpty(partsShippingOrder.ReceivingAddress) ? entityWarehouses.Contact : "," + entityWarehouses.Contact;
                            if(!string.IsNullOrEmpty(entityWarehouses.PhoneNumber))
                                partsShippingOrder.ReceivingAddress += string.IsNullOrEmpty(partsShippingOrder.ReceivingAddress) ? entityWarehouses.PhoneNumber : "," + entityWarehouses.PhoneNumber;
                        }, null);
                    }
                }, null);
            }
            if(partsOutboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单){
                partsShippingOrder.ReceivingAddress = partsOutboundPlan.ReceivingAddress;
            }
            if(partsOutboundPlan.OutboundType == (int)DcsPartsOutboundType.采购退货 && partsOutboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单)
                partsShippingOrder.Type = (int)DcsPartsShippingOrderType.调剂;
            if(partsOutboundPlan.OutboundType == (int)DcsPartsOutboundType.配件调拨)
                partsShippingOrder.Type = (int)DcsPartsShippingOrderType.调拨;
            if(partsOutboundPlan.OutboundType == (int)DcsPartsOutboundType.质量件索赔)
                partsShippingOrder.Type = (int)DcsPartsShippingOrderType.质量件索赔;
            if(partsOutboundPlan.OutboundType == (int)DcsPartsOutboundType.内部领出)
                partsShippingOrder.Type = (int)DcsPartsShippingOrderType.领用;
            if(partsOutboundPlan.OutboundType == (int)DcsPartsOutboundType.积压件调剂)
                partsShippingOrder.Type = (int)DcsPartsShippingOrderType.调剂;
            if(partsShippingOrder.Type == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrder_DonSupperOutType);
                return;
            }
            //运输里程赋值
            this.DomainContext.Load(this.DomainContext.GetHauDistanceInforsQuery().Where(ex => ex.CompanyAddressId == partsOutboundPlan.CompanyAddressId && ex.CompanyId == partsOutboundPlan.CounterpartCompanyId && ex.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                partsShippingOrder.TransportMileage = entity.HauDistance;
            }, null);
            if(partsOutboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售订单) {

                this.DomainContext.Load(this.DomainContext.GetPartsSalesOrdersQuery().Where(ex => ex.Id == partsOutboundPlan.OriginalRequirementBillId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var entity = loadOp.Entities.FirstOrDefault();
                    if(entity == null)
                        return;
                    partsShippingOrder.ReceivingAddress = entity.ReceivingAddress;
                   // partsShippingOrder.ShippingMethod = entity.ShippingMethod;
                    //partsShippingOrder.RequestedArrivalDate = entity.RequestedDeliveryTime;
                    partsShippingOrder.RequestedArrivalDate = DateTime.Now.AddDays(3);
                }, null);
            }
            if(partsOutboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单) {
                this.DomainContext.Load(this.DomainContext.GetPartsTransferOrdersQuery().Where(ex => ex.Id == partsOutboundPlan.OriginalRequirementBillId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var entity = loadOp.Entities.FirstOrDefault();
                    if(entity == null)
                        return;
                    partsShippingOrder.ReceivingAddress = entity.ReceivingAddress;
                  //  partsShippingOrder.ShippingMethod = entity.ShippingMethod;
                }, null);
            }
            if(partsOutboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件销售退货单 || partsOutboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.积压件调剂单 || partsOutboundPlan.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件索赔单) {
                DomainContext.Load(DomainContext.GetWarehousesOrderByNameQuery().Where(ex => ex.Id == partsOutboundPlan.ReceivingWarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var entity = loadOp.Entities.FirstOrDefault();
                    if(entity == null)
                        return;
                    partsShippingOrder.ReceivingAddress = entity.Address;

                }, null);
            }
            partsShippingOrder.ShippingDate = null;
            partsShippingOrder.WarehouseId = partsOutboundPlan.WarehouseId;
            DomainContext.Load(DomainContext.GetWarehousesOrderByNameQuery().Where(ex => ex.Id == partsOutboundPlan.WarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                partsShippingOrder.WarehouseCode = entity.Code;
                partsShippingOrder.WarehouseName = entity.Name;
            }, null);       ///////////Load方法有延迟，如果客户操作较快的话(或者当时网络较慢的情况下)，就有可能造成WarehouseCode、WarehouseName 没有读取到的情况，所以在保存时加一个判断
            DomainContext.Load(DomainContext.GetWarehousesOrderByNameQuery().Where(ex => ex.Id == partsShippingOrder.ReceivingWarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                if(partsShippingOrder.Type == (int)DcsPartsShippingOrderType.调拨) {
                    partsShippingOrder.ReceivingAddress = entity.Address;
                }
            }, null);
            this.SearchPartsOutBoundBill(partsOutboundPlan.Id, partsShippingOrder.Id);
            this.gdSearch.Visibility = Visibility.Collapsed;
            this.gdShow.Visibility = Visibility.Visible;
            this.ShowSaveButton();
        }

        private void SearchPartsOutBoundBillCommand() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            if(partsShippingOrder.PartsShippingOrderRefs.Any()) {
                DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_ClearOutboundDetail, () => this.SearchPartsOutBoundBill(this.partsOutboundPlan.Id, partsShippingOrder.Id));
            } else {
                SearchPartsOutBoundBill(partsOutboundPlan.Id, partsShippingOrder.Id);
            }
        }

        private void SearchPartsOutBoundBill(int? planId, int? shippingId) {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            int[] ids = this.PartsOutboundPlanForShippingDataGridView.SelectedEntities.Cast<TaskListPartsOutboundPlan>().Select(r => r.TaskId).ToArray();
            DomainContext.Load(DomainContext.查询待运配件任务单Query(ids), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any())
                    return;

                foreach(var item in partsShippingOrder.PartsShippingOrderDetails)
                    partsShippingOrder.PartsShippingOrderDetails.Remove(item);
                partsShippingOrder.BoxUpTaskOrPickings.Clear();
                foreach(var boxUpTaskOrPicking in loadOp.Entities) {
                    partsShippingOrder.BoxUpTaskOrPickings.Add(boxUpTaskOrPicking);
                }
                foreach(var result in loadOp.Entities) {
                    var detail = new PartsShippingOrderDetail();
                    detail.PartsShippingOrderId = partsShippingOrder.Id;
                    detail.SparePartId = result.SparePartId;
                    detail.SparePartName = result.SparePartName;
                    detail.SparePartCode = result.SparePartCode;
                    detail.SettlementPrice = result.SettlementPrice;
                    detail.ShippingAmount = result.ShippingAmount;
                    detail.PartsOutboundPlanId = result.PartsOutboundPlanId;
                    detail.PartsOutboundPlanCode = result.PartsOutboundPlanCode;
                    detail.ContainerNumber = result.ContainerNumber;
                    detail.TaskId = result.TaskId;
                    detail.TaskType = result.Type;
                    //detail.ContainerNumber = result.ContainerNumber;
                    partsShippingOrder.PartsShippingOrderDetails.Add(detail);
                }
            }, null);
        }

        private void RadButtonSearch_Click(object sender, RoutedEventArgs e) {
            Addsearch();
        }

        private void Addsearch() {
            this.DomainContext.Load(this.DomainContext.GetKeyValueItemsQuery().Where(r => r.Name == "PartsShipping_Method" && r.Value == "客户自行运输"), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null && loadOp.Entities.Any()) {
                    shippingMethodByCustomer = loadOp.Entities.First().Key;
                }
            }, null);
            //创建时间过滤 装修任务单 或拣货任务单
            if(this.CreateTimeForm.HasValue) {
                this.CreateTimeForm = new DateTime(this.CreateTimeForm.Value.Year, this.CreateTimeForm.Value.Month, this.CreateTimeForm.Value.Day, 0, 00, 00);
            } else {
                this.tCreateTimeForm = null;
            }
            if(this.CreateTimeTo.HasValue) {
                this.CreateTimeTo = new DateTime(this.CreateTimeTo.Value.Year, this.CreateTimeTo.Value.Month, this.CreateTimeTo.Value.Day, 23, 59, 59);
            } else {
                this.tCreateTimeTo = null;
            }
            var entityQuery = DomainContext.仓库人员查询待发运出库计划Query(BaseApp.Current.CurrentUserData.UserId, CreateTimeForm, CreateTimeTo);
            if(this.OutboundType.HasValue) {
                entityQuery = entityQuery.Where(ex => ex.OutboundType == this.OutboundType);
                this.tOutboundType = this.OutboundType;
            } else {
                this.tOutboundType = null;
            }
            if(this.WarehouseId.HasValue) {
                entityQuery = entityQuery.Where(ex => ex.WarehouseId == this.WarehouseId);
                this.tWarehouseId = this.WarehouseId;
            } else {
                this.tWarehouseId = null;
            }

            if(this.ShippingMethod.HasValue) {
                entityQuery = entityQuery.Where(ex => ex.ShippingMethod == this.ShippingMethod);
                this.tShippingMethod = this.ShippingMethod;
            } else {
                this.tShippingMethod = null;
            }
            if(this.PartsSalesOrderTypeId.HasValue) {
                entityQuery = entityQuery.Where(ex => ex.PartsSalesOrderTypeId == this.PartsSalesOrderTypeId);
                this.tPartsSalesOrderTypeId = this.PartsSalesOrderTypeId;
            } else {
                this.tPartsSalesOrderTypeId = null;
            }
            if(this.PartsSalesCategoryId.HasValue) {
                entityQuery = entityQuery.Where(ex => ex.PartsSalesCategoryId == this.PartsSalesCategoryId);
                this.tPartsSalesCategoryId = this.PartsSalesCategoryId;
            } else {
                this.tPartsSalesCategoryId = null;
            }
            if(!string.IsNullOrEmpty(this.Code)) {
                entityQuery = entityQuery.Where(ex => ex.Code.IndexOf(this.Code) >= 0);
                this.tCode = this.Code;
            } else {
                this.tCode = null;
            }
            if(!string.IsNullOrEmpty(this.CounterpartCompanyCode)) {
                entityQuery = entityQuery.Where(ex => ex.CounterpartCompanyCode.IndexOf(this.CounterpartCompanyCode) >= 0);
                this.tCounterpartCompanyCode = this.CounterpartCompanyCode;
            } else {
                this.tCounterpartCompanyCode = null;
            }
            if(!string.IsNullOrEmpty(this.CounterpartCompanyName)) {
                entityQuery = entityQuery.Where(ex => ex.CounterpartCompanyName.IndexOf(this.CounterpartCompanyName) >= 0);
                this.tCounterpartCompanyName = this.CounterpartCompanyName;
            } else {
                this.tCounterpartCompanyName = null;
            }
            if(this.CreateTimeForm.HasValue) {
                this.tCreateTimeForm = this.CreateTimeForm;
            }
            if(this.CreateTimeTo.HasValue) {
                this.tCreateTimeTo = this.CreateTimeTo;
            }
            this.PartsOutboundPlans.Clear();
            this.DomainContext.Load(entityQuery, LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                if(loadOp.Entities == null || !loadOp.Entities.Any())
                    return;

                this.PartsOutboundPlans = new ObservableCollection<TaskListPartsOutboundPlan>(loadOp.Entities.Distinct(new TaskCodeComparer()));
            }, null);
        }
        #endregion
        //按任务单号去重
        public class TaskCodeComparer : System.Collections.Generic.IEqualityComparer<TaskListPartsOutboundPlan> {
            public bool Equals(TaskListPartsOutboundPlan x, TaskListPartsOutboundPlan y) {
                if(x == null)
                    return y == null;
                return x.TaskId == y.TaskId && x.Id == y.Id;
            }

            public int GetHashCode(TaskListPartsOutboundPlan obj) {
                if(obj == null)
                    return 0;
                int hCode = obj.TaskId ^ obj.Id;
                return hCode.GetHashCode();
            }
        }

        private DataGridViewBase PartsOutboundPlanForShippingDataGridView {
            get {
                if(this.partsOutboundPlanForShippingDataGridView == null) {
                    this.partsOutboundPlanForShippingDataGridView = DI.GetDataGridView("PartsOutboundPlanForShipping");
                    this.partsOutboundPlanForShippingDataGridView.DomainContext = this.DomainContext;
                    this.partsOutboundPlanForShippingDataGridView.DataContext = this;
                    this.partsOutboundPlanForShippingDataGridView.SelectionChanged += this.PartsOutboundPlanForShippingDataGridView_SelectionChanged;
                }
                return this.partsOutboundPlanForShippingDataGridView;
            }
        }

        private DataGridViewBase PartsShippingOrderRefForOutBoundDataGridView {
            get {
                if(this.partsShippingOrderRefForOutBoundDataGridView == null) {
                    this.partsShippingOrderRefForOutBoundDataGridView = DI.GetDataGridView("PartsShippingOrderRefForOutBound");
                    this.partsShippingOrderRefForOutBoundDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsShippingOrderRefForOutBoundDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private void CreateUI() {
            //this.KeyValueManager.LoadData();
            this.KeyValueManager.LoadData(() => {
                this.kvTypes.Clear();
                foreach(var kvPair in this.KeyValueManager[kvNames[0]].Where(e => e.Key == (int)DcsPartsOutboundType.积压件调剂 || e.Key == (int)DcsPartsOutboundType.配件调拨 || e.Key == (int)DcsPartsOutboundType.配件销售 || e.Key == (int)DcsPartsOutboundType.采购退货 || e.Key == (int)DcsPartsOutboundType.质量件索赔)) {
                    kvTypes.Add(kvPair);
                }
                this.kvShippingMethods.Clear();
                foreach(var kvPair in this.KeyValueManager[kvNames[1]]) {
                    kvShippingMethods.Add(kvPair);
                }
            });
            //待发运出库计划
            this.radButtonSearch.Click += this.RadButtonSearch_Click;
            this.radButtondShipping.Click += this.RadButtondShipping_Click;
            this.radButtondShipping.IsEnabled = false;
            this.CreateTimeForm = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day);
            this.CreateTimeTo = DateTime.Now.Date;
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = PartsStockingUIStrings.DataEditView_Title_PartsOutboundPlan,
                Content = this.PartsOutboundPlanForShippingDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 2);
            tabControl.SetValue(MaxWidthProperty, (double)1024);
            tabControl.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Left);
            this.gdSearch.Children.Add(tabControl);
            this.KvWarehouses.Clear();
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(w => w.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && w.Status == (int)DcsBaseDataStatus.有效 && (w.Type == (int)DcsWarehouseType.分库 || w.Type == (int)DcsWarehouseType.总库)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                foreach(var warehouse in loadOp.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp2.Entities) {
                    this.KvPartsSalesCategory.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name,
                        UserObject = partsSalesCategory
                    });
                }
            }, null);
            //发运单新增修改
            this.gdShow.Children.Add(DI.GetDataEditPanel("PartsShippingOrder"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsStockingUIStrings.DataEditView_Title_PartsShippingOrder_PartsShippingOrderWithPartsShippingOrderDetail, null, () => this.PartsShippingOrderRefForOutBoundDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_SearchTransportPartsOutboundDetail,
                Command = new DelegateCommand(this.SearchPartsOutBoundBillCommand),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/query.png", UriKind.Relative)
            });
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            this.gdShow.Children.Add(detailDataEditView);

        }

        private void PartsSalesCategory_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            if(!this.PartsSalesCategoryId.HasValue)
                return;
            this.kvPartsSalesOrderType.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsSalesOrderTypesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.PartsSalesCategoryId == this.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                foreach(var partsSalesOrderType in loadOp.Entities) {
                    this.KvPartsSalesOrderType.Add(new KeyValuePair {
                        Key = partsSalesOrderType.Id,
                        Value = partsSalesOrderType.Name
                    });
                }
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            this.ShowSaveButton();
            this.PartsOutboundPlans.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsShippingOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;

                if(entity.PartsShippingOrderRefs.Select(item => item.PartsOutboundBill).Select(item => item.PartsOutboundPlanId).Distinct().Count() > 1) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrder_PartsShippingOrderRefFromDifferntPartsOuntPlan);
                    return;
                }
                if(entity.PartsShippingOrderRefs.Any() && entity.PartsShippingOrderRefs.First().PartsOutboundBill != null) {
                    partsOutboundPlan = new TaskListPartsOutboundPlan();
                    partsOutboundPlan.Id = entity.PartsShippingOrderRefs.First().PartsOutboundBill.PartsOutboundPlanId;
                } else {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrder_PartsOutboundBillIsNull);
                }
                this.SetObjectToEdit(entity);
                this.gdSearch.Visibility = Visibility.Collapsed;
                this.gdShow.Visibility = Visibility.Visible;
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            partsShippingOrder.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(partsShippingOrder.WarehouseCode)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_OutboundWarehouseNeedRefresh);
                return;
            }

            if(!partsShippingOrder.BoxUpTaskOrPickings.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrder_PartsShippingOrderRefsLessThenZero);
                return;
            }

            if(string.IsNullOrWhiteSpace(partsShippingOrder.ReceivingAddress))
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_ReceivingAddressIsNull, new[] {
                    "ReceivingAddress"
                }));
            if(partsShippingOrder.ShippingMethod != shippingMethodByCustomer) {
                if(string.IsNullOrWhiteSpace(partsShippingOrder.LogisticCompanyCode))
                    partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_LogisticCompanyCodeIsNull, new[] {
                    "LogisticCompanyCode"
                }));
                if(partsShippingOrder.LogisticCompanyId == default(int))
                    partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_LogisticCompanyCodeIsNull, new[] {
                    "LogisticCompanyCode"
                }));
                if(string.IsNullOrWhiteSpace(partsShippingOrder.LogisticCompanyName))
                    partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_LogisticCompanyNameIsNull, new[] {
                    "LogisticCompanyName"
                }));
            }
            if(!partsShippingOrder.ShippingMethod.HasValue) {
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_ShippingMethodIsNull, new[] {
                    "ShippingMethod"
                }));
            }
            if(partsShippingOrder.RequestedArrivalDate.HasValue && partsShippingOrder.RequestedArrivalDate.Value < DateTime.Now.Date) {
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_RequestedArrivalDateLessThanNow, new[] {
                    "RequestedArrivalDate"
                }));
            }
            if(!partsShippingOrder.RequestedArrivalDate.HasValue) {
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_RequestedArrivalDateIsNotNull, new[] {
                    "RequestedArrivalDate"
                }));
            }
            //if(partsShippingOrder.ShippingDate.HasValue && partsShippingOrder.ShippingDate.Value < DateTime.Now.Date) {
            //    partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_ShippingDateLessThanNow, new[] {
            //        "ShippingDate"
            //    }));
            //}

            //if(partsShippingOrder.RequestedArrivalDate.HasValue && partsShippingOrder.ShippingDate.HasValue && partsShippingOrder.RequestedArrivalDate.Value < partsShippingOrder.ShippingDate.Value) {
            //    partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_ShippingDateLessThanShippingDate, new[] {
            //        "RequestedArrivalDate"
            //    }));
            //}

            if(partsShippingOrder.HasValidationErrors || partsShippingOrder.PartsShippingOrderRefs.Any(e => e.HasValidationErrors) || partsShippingOrder.PartsShippingOrderDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsShippingOrder).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(partsShippingOrder.Can生成配件发运单) {
                        partsShippingOrder.生成配件发运单();
                    }
                } else {
                    if(partsShippingOrder.Can修改配件发运单)
                        partsShippingOrder.修改配件发运单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowNotification(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditCancelled() {
            if(this.EditState == DataEditState.New) {
                this.HideSaveButton();
            } else {
                this.ShowSaveButton();
            }
            //this.HideSaveButton();
            this.radButtondShipping.IsEnabled = false;
            this.gdShow.Visibility = Visibility.Collapsed;
            this.gdSearch.Visibility = Visibility.Visible;
            this.PartsOutboundPlans.Clear();
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            if(this.EditState == DataEditState.New) {
                var partsShippingOrder = this.DataContext as PartsShippingOrder;
                if(partsShippingOrder == null)
                    return;
                DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Report_PartsShippingOrder, () => this.DomainContext.Load(this.DomainContext.GetPartsShippingOrdersQuery().Where(ex => ex.Id == partsShippingOrder.Id), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var entity = loadOp.Entities.SingleOrDefault();
                    if(entity == null)
                        return;
                    if(BaseApp.Current.CurrentUserData.EnterpriseCode == "2450") {
                        BasePrintWindow printWindow1 = new PartsShippingOrderPrintWindowFD {
                            Header = "配件发运单打印(福戴)",
                            PartsShippingOrder = entity
                        };
                        printWindow1.ShowDialog();
                    } else {
                        //BasePrintWindow printWindow = new PartsShippingOrderPrintWindow1 {
                        //    Header = "配件发运单打印",
                        //    PartsShippingOrder = entity
                        //};
                        //printWindow.ShowDialog();
                        //this.HideSaveButton();
                        SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataEditView_Text_PartsShippingOrderPrint, "ReportPartsShippingOrders", null, true, new Tuple<string, string>("partsShippingOrderId", entity.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

                    }
                }, null));
                var partsShippingOrder1 = this.CreateObjectToEdit<PartsShippingOrder>();
                this.HideSaveButton();
                partsShippingOrder1.Code = GlobalVar.ASSIGNED_BY_SERVER;
                partsShippingOrder1.TransportCostSettleStatus = false;
                partsShippingOrder1.Status = (int)DcsPartsShippingOrderStatus.新建;
                Addsearch();
            } else {
                this.ShowSaveButton();
            }
            //this.HideSaveButton();
            this.radButtondShipping.IsEnabled = false;
            this.gdShow.Visibility = Visibility.Collapsed;
            this.gdSearch.Visibility = Visibility.Visible;
            this.OutboundType = this.tOutboundType;
            this.WarehouseId = this.tWarehouseId;
            this.ShippingMethod = this.tShippingMethod;
            this.PartsSalesCategoryId = this.tPartsSalesCategoryId;
            this.PartsSalesOrderTypeId = this.tPartsSalesOrderTypeId;
            this.Code = this.tCode;
            this.CounterpartCompanyCode = this.tCounterpartCompanyCode;
            this.CounterpartCompanyName = this.tCounterpartCompanyName;
            this.CreateTimeForm = this.tCreateTimeForm;
            this.CreateTimeTo = this.tCreateTimeTo;
            this.PartsOutboundPlans.Clear();
            base.OnEditSubmitted();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsShippingOrder;
            }
        }

        public int? WarehouseId {
            get {
                return this.warehouseId;
            }
            set {
                this.warehouseId = value;
                this.OnPropertyChanged("WarehouseId");
            }
        }
        public int? PartsSalesOrderTypeId {
            get {
                return this.partsSalesOrderTypeId;
            }
            set {
                this.partsSalesOrderTypeId = value;
                this.OnPropertyChanged("PartsSalesOrderTypeId");
            }
        }

        public int? PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }
        public string Code {
            get {
                return this.code;
            }
            set {
                this.code = value;
                this.OnPropertyChanged("Code");
            }
        }

        public ObservableCollection<KeyValuePair> KvTypes {
            get {
                return this.kvTypes;
            }
        }

        public DateTime? CreateTimeForm {
            get {
                return this.createTimeForm;
            }
            set {
                this.createTimeForm = value;
                this.OnPropertyChanged("CreateTimeForm");
            }
        }

        public DateTime? CreateTimeTo {
            get {
                return this.createTimeTo;
            }
            set {
                this.createTimeTo = value;
                this.OnPropertyChanged("CreateTimeTo");
            }
        }

        public int? OutboundType {
            get {
                return outboundType;
            }
            set {
                this.outboundType = value;
                this.OnPropertyChanged("OutboundType");
            }
        }

        public string CounterpartCompanyCode {
            get {
                return this.counterpartCompanyCode;
            }
            set {
                this.counterpartCompanyCode = value;
                this.OnPropertyChanged("CounterpartCompanyCode");
            }
        }

        public string CounterpartCompanyName {
            get {
                return this.counterpartCompanyName;
            }
            set {
                this.counterpartCompanyName = value;
                this.OnPropertyChanged("CounterpartCompanyName");
            }
        }

        public object KvOutboundTypes {
            get {
                return this.kvTypes;
            }
        }

        public int? ShippingMethod {
            get {
                return shippingMethod;
            }
            set {
                this.shippingMethod = value;
                this.OnPropertyChanged("ShippingMethod");
            }
        }

        public object KvShippingMethods {
            get {
                return this.kvShippingMethods;
            }
        }

        public ObservableCollection<TaskListPartsOutboundPlan> PartsOutboundPlans {
            get {
                return this.partsOutboundPlans ?? (this.partsOutboundPlans = new ObservableCollection<TaskListPartsOutboundPlan>());
            }
            set {
                this.partsOutboundPlans = value;
                this.OnPropertyChanged("PartsOutboundPlans");
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }
        public ObservableCollection<KeyValuePair> KvPartsSalesOrderType {
            get {
                return this.kvPartsSalesOrderType;
            }
        }
        public ObservableCollection<KeyValuePair> KvPartsSalesCategory {
            get {
                return this.kvPartsSalesCategory;
            }
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void TextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
            if(e.Key == Key.Enter) {
                this.Code = this.textCode.Text;
                this.CounterpartCompanyCode = this.textCounterpartCompanyCode.Text;
                this.CounterpartCompanyName = this.textCounterpartCompanyName.Text;
                Addsearch();
            }
        }
    }
}
