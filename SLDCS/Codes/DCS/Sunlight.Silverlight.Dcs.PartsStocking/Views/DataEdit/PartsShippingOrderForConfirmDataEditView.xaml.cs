﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShippingOrderForConfirmDataEditView {
        private DataGridViewBase dataGridView;

        public PartsShippingOrderForConfirmDataEditView() {
            this.Initializer.Register(this.CreateUI);
            InitializeComponent();
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsShippingOrderDetailForConfirm");
                    this.dataGridView.DomainContext = this.DomainContext;
                    return this.dataGridView;
                }
                return this.dataGridView;
            }
        }

        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("PartsShippingOrderForConfirm"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(PartsStockingUIStrings.DetailPanel_Title_PartsShippingOrderDetail, null, this.DataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(detailDataEditView);

        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShippingOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                if(entity.PartsShippingOrderDetails.Any()) {
                    foreach(var partsShippingOrderDetail in entity.PartsShippingOrderDetails) {
                        partsShippingOrderDetail.PropertyChanged -= this.PartsShippingOrderDetail_PropertyChanged;
                        partsShippingOrderDetail.PropertyChanged += this.PartsShippingOrderDetail_PropertyChanged;
                        partsShippingOrderDetail.ConfirmedAmount = partsShippingOrderDetail.ShippingAmount;
                        partsShippingOrderDetail.InTransitDamageLossAmount = 0;
                    }
                }
                this.SetObjectToEdit(entity);
            }, null);
        }

        private void PartsShippingOrderDetail_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var partsShippingOrderDetail = sender as PartsShippingOrderDetail;
            if(partsShippingOrderDetail == null)
                return;
            switch(e.PropertyName) {
                case "ConfirmedAmount":
                    partsShippingOrderDetail.InTransitDamageLossAmount = partsShippingOrderDetail.ShippingAmount - partsShippingOrderDetail.ConfirmedAmount;
                    break;
            }
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsShippingOrder_ReceivingConfirm;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;

            if(partsShippingOrder == null || !this.DataGridView.CommitEdit())
                return;
            if(partsShippingOrder.PartsShippingOrderDetails.Any(entity => entity.ConfirmedAmount > entity.ShippingAmount)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrderDetail_ShippingAmountLessThenConfirmedAmount);
                return;
            }
            if(partsShippingOrder.PartsShippingOrderDetails.Any(entity => entity.ConfirmedAmount < 0)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrderDetail_ConfirmedAmountLessThenZero);
                return;
            }
            if(partsShippingOrder.PartsShippingOrderDetails.Any(entity => entity.ShippingAmount > entity.ConfirmedAmount && (entity.DifferenceClassification == null || entity.DifferenceClassification == 0))) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_DifferenceClassificationIsNull);
                return;
            }
            partsShippingOrder.IsTransportLosses = partsShippingOrder.PartsShippingOrderDetails.Sum(e => e.InTransitDamageLossAmount ?? 0) > 0;
            if(partsShippingOrder.PartsShippingOrderDetails.Sum(entity => entity.ConfirmedAmount) == 0) {
                DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrder_ConfirmedAmountIsZeroForConfirm, () => {
                    try {
                        ((IEditableObject)partsShippingOrder).EndEdit();
                        if(partsShippingOrder.Can收货确认配件发运单)
                            partsShippingOrder.收货确认配件发运单();
                        base.OnEditSubmitting();
                    } catch(ValidationException ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                        return;
                    }
                });
            } else {
                try {
                    ((IEditableObject)partsShippingOrder).EndEdit();
                    if(partsShippingOrder.Can收货确认配件发运单)
                        partsShippingOrder.收货确认配件发运单();
                    base.OnEditSubmitting();
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }

            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder != null && this.DomainContext.PartsShippingOrders.Contains(partsShippingOrder))
                this.DomainContext.PartsShippingOrders.Detach(partsShippingOrder);
        }
    }
}