﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShippingOrderForDataEditView : INotifyPropertyChanged {
        private DataGridViewBase partsShippingOrderRefForOutBoundDataGridView;
        //private PartsOutboundPlan partsOutboundPlan;
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<PartsOutboundPlan> partsOutboundPlans = new ObservableCollection<PartsOutboundPlan>();
        private readonly ObservableCollection<KeyValuePair> kvTypes = new ObservableCollection<KeyValuePair>();
        private KeyValueManager keyValueManager;
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private string code, counterpartCompanyCode, counterpartCompanyName;
        private int? warehouseId, outboundType;
        private DateTime? createTimeForm, createTimeTo;
        private readonly string[] kvNames = {
            "Parts_OutboundType"
        };

        public PartsShippingOrderForDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsShippingOrderDataEditView_DataContextChanged;
        }

        #region 界面事件

        private void PartsShippingOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            this.Code = this.CounterpartCompanyCode = this.CounterpartCompanyName = default(string);
            this.WarehouseId = this.OutboundType = default(int?);
            this.CreateTimeForm = this.CreateTimeTo = default(DateTime?);
            if(this.DomainContext == null)
                this.Initializer.Register(() => {
                    if(partsShippingOrder.Id == default(int) && this.gdShow.Visibility == Visibility.Collapsed)
                        this.HideSaveButton();
                    else
                        this.ShowSaveButton();
                });
            else {
                if(partsShippingOrder.Id == default(int) && this.gdShow.Visibility == Visibility.Collapsed)
                    this.HideSaveButton();
                else
                    this.ShowSaveButton();
            }
            this.gdShow.Visibility = Visibility.Collapsed;
        }

        private void SearchPartsOutBoundBillCommand() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            if(partsShippingOrder.PartsShippingOrderRefs.Any()) {
                DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_ClearOutboundDetail, () => this.SearchPartsOutBoundBill(null, partsShippingOrder.Id));
            } else {
                SearchPartsOutBoundBill(null, partsShippingOrder.Id);
            }
        }

        private void SearchPartsOutBoundBill(int? planId, int? shippingId) {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            int[] ids = partsShippingOrder.PartsShippingOrderDetails.Select(r => r.TaskId.Value).ToArray();
            DomainContext.Load(DomainContext.查询待运配件任务单Query(ids), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any())
                    return;

                foreach(var item in partsShippingOrder.PartsShippingOrderDetails)
                    partsShippingOrder.PartsShippingOrderDetails.Remove(item);
                partsShippingOrder.BoxUpTaskOrPickings.Clear();
                foreach(var boxUpTaskOrPicking in loadOp.Entities) {
                    partsShippingOrder.BoxUpTaskOrPickings.Add(boxUpTaskOrPicking);
                }
                foreach(var result in loadOp.Entities) {
                    var detail = new PartsShippingOrderDetail();
                    detail.PartsShippingOrderId = partsShippingOrder.Id;
                    detail.SparePartId = result.SparePartId;
                    detail.SparePartName = result.SparePartName;
                    detail.SparePartCode = result.SparePartCode;
                    detail.SettlementPrice = result.SettlementPrice;
                    detail.ShippingAmount = result.ShippingAmount;
                    detail.PartsOutboundPlanId = result.PartsOutboundPlanId;
                    detail.PartsOutboundPlanCode = result.PartsOutboundPlanCode;
                    detail.TaskId = result.TaskId;
                    detail.TaskType = result.Type;
                    detail.ContainerNumber = result.ContainerNumber;
                    partsShippingOrder.PartsShippingOrderDetails.Add(detail);
                }
            }, null);
        }
        #endregion

        private DataGridViewBase PartsShippingOrderRefForOutBoundDataGridView {
            get {
                if(this.partsShippingOrderRefForOutBoundDataGridView == null) {
                    this.partsShippingOrderRefForOutBoundDataGridView = DI.GetDataGridView("PartsShippingOrderRefForOutBound");
                    this.partsShippingOrderRefForOutBoundDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsShippingOrderRefForOutBoundDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private void CreateUI() {
            //this.KeyValueManager.LoadData();
            this.KeyValueManager.LoadData(() => {
                this.kvTypes.Clear();
                foreach(var kvPair in this.KeyValueManager[kvNames[0]].Where(e => e.Key == (int)DcsPartsOutboundType.积压件调剂 || e.Key == (int)DcsPartsOutboundType.配件调拨 || e.Key == (int)DcsPartsOutboundType.配件销售 || e.Key == (int)DcsPartsOutboundType.采购退货)) {
                    kvTypes.Add(kvPair);
                }
            });
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(w => w.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && w.Status == (int)DcsBaseDataStatus.有效 && (w.Type == (int)DcsWarehouseType.分库 || w.Type == (int)DcsWarehouseType.总库)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                foreach(var warehouse in loadOp.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
            }, null);
            //发运单新增修改
            this.gdShow.Children.Add(DI.GetDataEditPanel("PartsShippingOrder"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(PartsStockingUIStrings.DataEditView_Title_PartsShippingOrder_PartsShippingOrderWithPartsShippingOrderDetail, null, () => this.PartsShippingOrderRefForOutBoundDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_SearchTransportPartsOutboundDetail,
                Command = new DelegateCommand(this.SearchPartsOutBoundBillCommand),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/query.png", UriKind.Relative)
            });
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            this.gdShow.Children.Add(detailDataEditView);

        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void LoadEntityToEdit(int id) {
            this.ShowSaveButton();
            this.PartsOutboundPlans.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsShippingOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;

                //if(entity.PartsShippingOrderRefs.Select(item => item.PartsOutboundBill).Select(item => item.PartsOutboundPlanId).Distinct().Count() > 1) {
                //    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrder_PartsShippingOrderRefFromDifferntPartsOuntPlan);
                //    return;
                //}
                //if(entity.PartsShippingOrderRefs.Any() && entity.PartsShippingOrderRefs.First().PartsOutboundBill != null) {
                //    partsOutboundPlan = new PartsOutboundPlan();
                //    partsOutboundPlan.Id = entity.PartsShippingOrderRefs.First().PartsOutboundBill.PartsOutboundPlanId;
                //} else {
                //    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrder_PartsOutboundBillIsNull);
                //}
                this.SetObjectToEdit(entity);
                this.gdShow.Visibility = Visibility.Visible;
                //加载清单
                this.SearchPartsOutBoundBillCommand();
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            partsShippingOrder.ValidationErrors.Clear();

            if(!partsShippingOrder.BoxUpTaskOrPickings.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrder_PartsShippingOrderRefsLessThenZero);
                return;
            }

            if(string.IsNullOrWhiteSpace(partsShippingOrder.ReceivingAddress))
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_ReceivingAddressIsNull, new[] {
                    "ReceivingAddress"
                }));
            if(string.IsNullOrWhiteSpace(partsShippingOrder.LogisticCompanyCode))
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_LogisticCompanyCodeIsNull, new[] {
                    "LogisticCompanyCode"
                }));
            if(partsShippingOrder.LogisticCompanyId == default(int))
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_LogisticCompanyCodeIsNull, new[] {
                    "LogisticCompanyCode"
                }));
            if(string.IsNullOrWhiteSpace(partsShippingOrder.LogisticCompanyName))
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_LogisticCompanyNameIsNull, new[] {
                    "LogisticCompanyName"
                }));
            //if(!partsShippingOrder.ShippingDate.HasValue) {
            //    partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_ShippingDateIsNull, new[] {
            //        "ShippingDate"
            //    }));
            //}
            if(!partsShippingOrder.ShippingMethod.HasValue) {
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_ShippingMethodIsNull, new[] {
                    "ShippingMethod"
                }));
            }
            if(partsShippingOrder.RequestedArrivalDate.HasValue && partsShippingOrder.RequestedArrivalDate.Value < DateTime.Now.Date) {
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_RequestedArrivalDateLessThanNow, new[] {
                    "RequestedArrivalDate"
                }));
            }
            if(!partsShippingOrder.RequestedArrivalDate.HasValue) {
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_PartsShippingOrder_RequestedArrivalDate, new[] {
                    "RequestedArrivalDate"
                }));
            }
            //if(partsShippingOrder.ShippingDate.HasValue && partsShippingOrder.ShippingDate.Value < DateTime.Now.Date) {
            //    partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_ShippingDateLessThanNow, new[] {
            //        "ShippingDate"
            //    }));
            //}

            //if(partsShippingOrder.RequestedArrivalDate.HasValue && partsShippingOrder.ShippingDate.HasValue && partsShippingOrder.RequestedArrivalDate.Value < partsShippingOrder.ShippingDate.Value) {
            //    partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_ShippingDateLessThanShippingDate, new[] {
            //        "RequestedArrivalDate"
            //    }));
            //}

            if(partsShippingOrder.HasValidationErrors || partsShippingOrder.PartsShippingOrderRefs.Any(e => e.HasValidationErrors) || partsShippingOrder.PartsShippingOrderDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)partsShippingOrder).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(partsShippingOrder.Can生成配件发运单) {
                        partsShippingOrder.生成配件发运单();
                    }
                } else {
                    if(partsShippingOrder.Can修改配件发运单)
                        partsShippingOrder.修改配件发运单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowNotification(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditCancelled() {
            this.ShowSaveButton();
            //this.HideSaveButton();
            this.gdShow.Visibility = Visibility.Collapsed;
            this.PartsOutboundPlans.Clear();
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            this.ShowSaveButton();
            //this.HideSaveButton();
            this.gdShow.Visibility = Visibility.Collapsed;
            PartsOutboundPlans.Clear();
            base.OnEditSubmitted();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsShippingOrder;
            }
        }

        public int? WarehouseId {
            get {
                return this.warehouseId;
            }
            set {
                this.warehouseId = value;
                this.OnPropertyChanged("WarehouseId");
            }
        }

        public string Code {
            get {
                return this.code;
            }
            set {
                this.code = value;
                this.OnPropertyChanged("Code");
            }
        }

        public ObservableCollection<KeyValuePair> KvTypes {
            get {
                return this.kvTypes;
            }
        }

        public DateTime? CreateTimeForm {
            get {
                return this.createTimeForm;
            }
            set {
                this.createTimeForm = value;
                this.OnPropertyChanged("CreateTimeForm");
            }
        }

        public DateTime? CreateTimeTo {
            get {
                return this.createTimeTo;
            }
            set {
                this.createTimeTo = value;
                this.OnPropertyChanged("CreateTimeTo");
            }
        }

        public int? OutboundType {
            get {
                return outboundType;
            }
            set {
                this.outboundType = value;
                this.OnPropertyChanged("OutboundType");
            }
        }

        public string CounterpartCompanyCode {
            get {
                return this.counterpartCompanyCode;
            }
            set {
                this.counterpartCompanyCode = value;
                this.OnPropertyChanged("CounterpartCompanyCode");
            }
        }

        public string CounterpartCompanyName {
            get {
                return this.counterpartCompanyName;
            }
            set {
                this.counterpartCompanyName = value;
                this.OnPropertyChanged("CounterpartCompanyName");
            }
        }

        public object KvOutboundTypes {
            get {
                return this.kvTypes;
            }
        }

        public ObservableCollection<PartsOutboundPlan> PartsOutboundPlans {
            get {
                return this.partsOutboundPlans ?? (this.partsOutboundPlans = new ObservableCollection<PartsOutboundPlan>());
            }
            set {
                this.partsOutboundPlans = value;
                this.OnPropertyChanged("PartsOutboundPlans");
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
