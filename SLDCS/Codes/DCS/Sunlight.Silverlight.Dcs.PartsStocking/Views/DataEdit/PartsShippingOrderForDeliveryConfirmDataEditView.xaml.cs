﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShippingOrderForDeliveryConfirmDataEditView {
        private DataGridViewBase partsShippingOrderDetailForEditDataGridView;

        public PartsShippingOrderForDeliveryConfirmDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase PartsShippingOrderDetailForEditDataGridView {
            get {
                if(this.partsShippingOrderDetailForEditDataGridView == null) {
                    this.partsShippingOrderDetailForEditDataGridView = DI.GetDataGridView("PartsShippingOrderDetailForMainEdit");
                    this.partsShippingOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsShippingOrderDetailForEditDataGridView;
            }
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsShippingOrderForDeliveryConfirm;
            }
        }

        private void CreateUI() {
            var dataEditView = new DcsDetailDataEditView();
            dataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsShippingOrderDetail), PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderDetail), null, () => this.PartsShippingOrderDetailForEditDataGridView);
            dataEditView.SetValue(Grid.RowProperty, 1);
            dataEditView.UnregisterButton(dataEditView.InsertButton);
            dataEditView.UnregisterButton(dataEditView.DeleteButton);
            this.LayoutRoot.Children.Add(dataEditView);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShippingOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            if(partsShippingOrder.LogisticArrivalDate != null)
                partsShippingOrder.LogisticArrivalDate = new DateTime(partsShippingOrder.LogisticArrivalDate.Value.Year, partsShippingOrder.LogisticArrivalDate.Value.Month, partsShippingOrder.LogisticArrivalDate.Value.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            partsShippingOrder.ValidationErrors.Clear();
            ((IEditableObject)partsShippingOrder).EndEdit();
            base.OnEditSubmitting();
        }

    }
}
