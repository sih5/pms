﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShippingOrderForExtendedBackConfirmDataEditView {
        public PartsShippingOrderForExtendedBackConfirmDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);

        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsShippingOrderForExtendedBackConfirm;
            }
        }

        protected override void OnEditSubmitting() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;

            if(!partsShippingOrder.RequestedArrivalDate.HasValue) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_RequestedArrivalDateIsNull);
                return;
            }

            // 根据issue：FTDCS_ISS20160831013 取消提示
            //if(partsShippingOrder.RequestedArrivalDate < DateTime.Now.AddDays(-1)) {
            //    UIHelper.ShowNotification("预计到货日期不能小于当前日期");
            //    return;
            //}

            ((IEditableObject)partsShippingOrder).EndEdit();
            if(partsShippingOrder.Can超期或电商运单流水反馈)
                partsShippingOrder.超期或电商运单流水反馈();//累加 信息反馈
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShippingOrdersQuery().Where(e => e.Id.Equals(id)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    entity.FlowFeedback = "";
                    if (!entity.RequestedArrivalDate.HasValue)
                        entity.RequestedArrivalDate = DateTime.Now;
                    if (!entity.ExpectedPlaceDate.HasValue)
                        entity.ExpectedPlaceDate = DateTime.Now;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {

        }

    }
}
