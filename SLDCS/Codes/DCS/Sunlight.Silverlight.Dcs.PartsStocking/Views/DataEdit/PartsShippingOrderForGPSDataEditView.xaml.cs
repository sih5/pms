﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShippingOrderForGPSDataEditView {
        public PartsShippingOrderForGPSDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_TitlePartsShippingOrderForGPS;
            }

        }
        protected override void OnEditSubmitted()
       {
         	 base.OnEditSubmitted();
       }
        protected override void OnEditSubmitting() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            if(string.IsNullOrEmpty(partsShippingOrder.GPSCode)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_GPSCodeIsNull);
                return;
            }
            ((IEditableObject)partsShippingOrder).EndEdit();
            if(partsShippingOrder.CanGPS定位器设备号登记)
                partsShippingOrder.GPS定位器设备号登记();

            base.OnEditSubmitting();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShippingOrdersQuery().Where(e => e.Id.Equals(id)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    //entity.RequestedArrivalDate = DateTime.Now;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        private void CreateUI(){
        
        }
    }
}
