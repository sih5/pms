﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShippingOrderForSendConfirmDataEditView {
        public PartsShippingOrderForSendConfirmDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);

        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsShippingOrderForSendConfirm;
            }
        }

        protected override void OnEditSubmitting() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;

            if(!partsShippingOrder.ArrivalTime.HasValue) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_RequestedArrivalDateIsNotNull);
                return;
            } else {
                if(partsShippingOrder.ShippingDate.HasValue
                && partsShippingOrder.ArrivalTime.Value < partsShippingOrder.ShippingDate.Value) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Text_ArrivalTimeGreaterThan);
                    return;
                }
            }

            ((IEditableObject)partsShippingOrder).EndEdit();
            if(partsShippingOrder.Can送达确认)
                partsShippingOrder.送达确认();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShippingOrdersQuery().Where(e => e.Id.Equals(id)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    if(entity != null)
                        entity.ArrivalTime = DateTime.Now;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {

        }

    }
}
