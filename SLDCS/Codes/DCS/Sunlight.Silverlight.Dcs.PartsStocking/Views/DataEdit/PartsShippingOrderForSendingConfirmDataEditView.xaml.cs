﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShippingOrderForSendingConfirmDataEditView {
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = {
            "ArrivalMode"
        };

        public PartsShippingOrderForSendingConfirmDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public ObservableCollection<KeyValuePair> KvArrivalMode {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsShippingOrderForSendingConfirm;
            }
        }

        protected override void OnEditSubmitting() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;

            if(!partsShippingOrder.ShippingTime.HasValue) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ShippingTimeIsNull);
                return;
            } else {
                if(partsShippingOrder.CreateTime.HasValue
                && partsShippingOrder.ShippingTime.Value.Date < partsShippingOrder.CreateTime.Value.Date) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ShippingTimeError);
                    return;
                }
            }

            if(!partsShippingOrder.ArrivalMode.HasValue) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_ArrivalModeIsNull);
                return;
            }

            ((IEditableObject)partsShippingOrder).EndEdit();
            if(partsShippingOrder.Can发货确认)
                partsShippingOrder.发货确认();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShippingOrdersQuery().Where(e => e.Id.Equals(id)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    entity.ShippingTime = DateTime.Now;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            //this.KeyValueManager.LoadData(() => {
            //    this.KvArrivalMode.Clear();
            //    foreach(var kvPair in this.KeyValueManager[kvNames[0]]) {
            //        KvArrivalMode.Add(kvPair);
            //    }
            //});
            var queryWindowExpress = DI.GetQueryWindow("Express");
            queryWindowExpress.SelectionDecided += this.QueryWindowExpress_SelectionDecided;
            queryWindowExpress.Loaded += this.QueryWindowExpress_Loaded;
            this.ptExpress.PopupContent = queryWindowExpress;

        }
        void QueryWindowExpress_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            var filterItem = new FilterItem();
            filterItem.MemberName = "Status";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = (int)DcsMasterDataStatus.有效;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
        }

        private void QueryWindowExpress_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var express = queryWindow.SelectedEntities.Cast<Express>().FirstOrDefault();
            if(express == null)
                return;
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            partsShippingOrder.ExpressCompanyId = express.Id;
            partsShippingOrder.ExpressCompanyCode = express.ExpressCode;
            partsShippingOrder.ExpressCompany = express.ExpressName;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

    }
}
