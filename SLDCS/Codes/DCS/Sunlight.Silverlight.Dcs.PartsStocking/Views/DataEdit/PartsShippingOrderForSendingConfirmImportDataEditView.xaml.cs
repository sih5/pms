﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShippingOrderForSendingConfirmImportDataEditView {
        private DataGridViewBase partsshippingorderforsendingconfirmimportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出批量发货确认模板.xlsx";
        private ICommand exportFileCommand;
        public PartsShippingOrderForSendingConfirmImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_ImportBatchShippingConfirm;
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                return this.partsshippingorderforsendingconfirmimportDataGridView ?? (this.partsshippingorderforsendingconfirmimportDataGridView = DI.GetDataGridView("PartsShippingOrderForSendingConfirmImport"));
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = PartsStockingUIStrings.DataEditView_Title_ImportBatchShippingConfirmDetails,
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.批量发货确认Async(fileName);
            this.ExcelServiceClient.批量发货确认Completed -= ExcelServiceClient_批量发货确认Completed;
            this.ExcelServiceClient.批量发货确认Completed += ExcelServiceClient_批量发货确认Completed;
        }

        private void ExcelServiceClient_批量发货确认Completed(object sender, 批量发货确认CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_ShippingTime,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_PartsShippingOrder_Logistic
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_PartsShippingOrder_LogisticNo
                                            },
                                            new ImportTemplateColumn {
                                                Name = "到货模式",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "查询网址"
                                            },
                                            new ImportTemplateColumn {
                                                Name = "快递公司名称"
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
