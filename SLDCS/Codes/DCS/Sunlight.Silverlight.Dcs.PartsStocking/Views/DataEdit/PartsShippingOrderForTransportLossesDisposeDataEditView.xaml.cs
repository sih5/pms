﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShippingOrderForTransportLossesDisposeDataEditView {
        private DataGridViewBase partsShippingOrderDetailForEditDataGridView;
        public PartsShippingOrderForTransportLossesDisposeDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private DataGridViewBase PartsShippingOrderDetailForEditDataGridView {
            get {
                if(this.partsShippingOrderDetailForEditDataGridView == null) {
                    this.partsShippingOrderDetailForEditDataGridView = DI.GetDataGridView("PartsShippingOrderDetailForEdit");
                    this.partsShippingOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsShippingOrderDetailForEditDataGridView;
            }
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_TransportLosses;
            }
        }
        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("PartsShippingOrderForTransportLossesDispose"));
            var dataEditView = new DcsDetailDataEditView();
            dataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsShippingOrderDetail), PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderDetail), null, () => this.PartsShippingOrderDetailForEditDataGridView);
            dataEditView.SetValue(Grid.RowProperty, 1);
            dataEditView.UnregisterButton(dataEditView.InsertButton);
            dataEditView.UnregisterButton(dataEditView.DeleteButton);
            this.LayoutRoot.Children.Add(dataEditView);
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShippingOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null || !this.PartsShippingOrderDetailForEditDataGridView.CommitEdit())
                return;
            partsShippingOrder.ValidationErrors.Clear();
            foreach(var item in partsShippingOrder.PartsShippingOrderDetails) {
                item.ValidationErrors.Clear();
                if(item.InTransitDamageLossAmount != 0 && item.TransportLossesDisposeMethod == null) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsShippingOrder_TransportLossesDisposeMethod);
                    return;
                }
            }
            partsShippingOrder.IsTransportLosses = true;
            ((IEditableObject)partsShippingOrder).EndEdit();
            try {
                if(partsShippingOrder.Can路损处理)
                    partsShippingOrder.路损处理();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
    }
}
