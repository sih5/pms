﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsShippingOrderMainEditDataEditView {
        private DataGridViewBase partsShippingOrderDetailForEditDataGridView;

        public PartsShippingOrderMainEditDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase PartsShippingOrderDetailForEditDataGridView {
            get {
                if(this.partsShippingOrderDetailForEditDataGridView == null) {
                    this.partsShippingOrderDetailForEditDataGridView = DI.GetDataGridView("PartsShippingOrderDetailForMainEdit");
                    this.partsShippingOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsShippingOrderDetailForEditDataGridView;
            }
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_UpdateMainOrder;
            }
        }

        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("PartsShippingOrderMainEdit"));
            var dataEditView = new DcsDetailDataEditView();
            dataEditView.Register(Utils.GetEntityLocalizedName(typeof(PartsShippingOrderDetail), PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderDetail), null, () => this.PartsShippingOrderDetailForEditDataGridView);
            dataEditView.SetValue(Grid.RowProperty, 1);
            dataEditView.UnregisterButton(dataEditView.InsertButton);
            dataEditView.UnregisterButton(dataEditView.DeleteButton);
            this.LayoutRoot.Children.Add(dataEditView);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsShippingOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            partsShippingOrder.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(partsShippingOrder.LogisticCompanyName))
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_LogisticCompanyNameIsNull, new[] {
                        "LogisticCompanyName"
                    }));
            if(partsShippingOrder.RequestedArrivalDate.HasValue && partsShippingOrder.RequestedArrivalDate.Value < DateTime.Now.Date) {
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_partsShippingOrder_RequestedArrivalDateLessThanNow, new[] {
                        "RequestedArrivalDate"
                    }));
            }
            if(!partsShippingOrder.RequestedArrivalDate.HasValue) {
                partsShippingOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_RequestedArrivalDateIsNotNull, new[] {
                        "RequestedArrivalDate"
                    }));
            }
            if(partsShippingOrder.HasValidationErrors)
                return;

            ((IEditableObject)partsShippingOrder).EndEdit();
            //try {
            //    if(partsShippingOrder.Can修改配件发运单)
            //        partsShippingOrder.修改配件发运单();
            //} catch(ValidationException ex) {
            //    UIHelper.ShowNotification(ex.Message);
            //    return;
            //}
            base.OnEditSubmitting();
        }
    }
}
