﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using System.ComponentModel;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsStockQueryForPcPrintDataEditView : INotifyPropertyChanged {
        private string sparepartid;
       private DateTime? pickTime=Convert.ToDateTime("2018-09-30 00:00:00") ;
       private int? number;
       private string warehouseCode = "";
       public event PropertyChangedEventHandler PropertyChanged;
        public PartsStockQueryForPcPrintDataEditView() {
            InitializeComponent();
        }
        public override void SetObjectToEditById(object id) {
            if(null != id) {
                sparepartid = id.ToString().Split(';').ToArray()[0];
                warehouseCode = id.ToString().Split(';').ToArray()[1];
            } else return;
        }
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        protected override void OnEditSubmitting() {
            if(pickTime == null)
            {
                UIHelper.ShowNotification(PartsStockingUIStrings.PartsStockQueryPickTime);
                return;
            }
            if(number == null || number==0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.PartsStockQueryNumberError);
                return;
            }
            SunlightPrinter.ShowPrinter(PartsStockingUIStrings.PartsStockQueryForPcPrint_Title, "PartsStockQueryForPcPrint", null, true, new Tuple<string, string>("id", sparepartid), new Tuple<string, string>("time", pickTime.Value.ToString("yyyy-MM-dd")), new Tuple<string, string>("number", number.ToString()), new Tuple<string, string>("warehouseCode", warehouseCode.ToString()));
            base.OnEditSubmitting();
        }
        protected override string Title {
            get {
                return PartsStockingUIStrings.PartsStockQueryForPcPrint_Title;
            }
        }
        protected override bool OnRequestCanSubmit() {
                return true;          
        }
         public DateTime? PickTime
        {
            get
            {
                return this.pickTime;
            }
            set
            {
                this.pickTime = value;
                this.OnPropertyChanged("PickTime");
            }
        }
         public int? Number {
             get {
                 return this.number;
             }
             set {
                 this.number = value;
                 this.OnPropertyChanged("Number");
             }
         }
    }
}
