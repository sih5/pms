﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsTransferOrderDataEditView {
        private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvOriginalWarehouseIds;
        private ObservableCollection<KeyValuePair> kvDestWarehouseIds;
        private int destWarehouseId, originWarehouseId;
        private readonly string[] kvNames = {
            "PartsTransferOrder_Type", "PartsShipping_Method"
        };

        //private PartsTransferOrderDetail check;

        private enum EntityCustomItem {
            OriginWarehouseStock,
            DestinWarehouseStock
        }

        private DataGridViewBase partsTransferOrderDetaiForEditDataGridView;
        public int? PurOrderId;

        public PartsTransferOrderDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.PartsTransferOrderDataEditView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
            this.originWarehouseComobox.SelectionChanged += this.OriginWarehouseComobox_SelectionChanged;
            this.destWarehouseComobox.SelectionChanged += this.DestWarehouseComobox_SelectionChanged;
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        #region 界面事件
        private void DestWarehouseComobox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            Action setWarehouseInfo = () => {
                if(this.KvDestWarehouseIds.All(v => v.Key != partsTransferOrder.DestWarehouseId))
                    return;
                var destWarehouse = this.KvDestWarehouseIds.First(v => v.Key == partsTransferOrder.DestWarehouseId).UserObject as Warehouse;
                if(destWarehouse == null)
                    return;
                partsTransferOrder.DestWarehouseId = destWarehouse.Id;
                partsTransferOrder.DestWarehouseCode = destWarehouse.Code;
                partsTransferOrder.DestWarehouseName = destWarehouse.Name;
                partsTransferOrder.StorageCompanyId = destWarehouse.StorageCompanyId;
                partsTransferOrder.StorageCompanyType = destWarehouse.StorageCompanyType;
                partsTransferOrder.ReceivingAddress = destWarehouse.Address;
            };
            if(this.destWarehouseId != default(int) && partsTransferOrder.DestWarehouseId != default(int) && partsTransferOrder.PartsTransferOrderDetails.Any()) {
                if(partsTransferOrder.DestWarehouseId != this.destWarehouseId)
                    //DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Confirm_DestWarehouseChangedWillClearDetails, () => {
                    this.UpdatePartsTransferOrderWithDetail();
                this.destWarehouseId = partsTransferOrder.DestWarehouseId;
                partsTransferOrder.TotalAmount = default(decimal);
                partsTransferOrder.TotalPlanAmount = default(decimal);
                setWarehouseInfo.Invoke();
                //}, () => {
                //    partsTransferOrder.DestWarehouseId = this.destWarehouseId;
                //    setWarehouseInfo.Invoke();
                //});
            } else {
                this.destWarehouseId = partsTransferOrder.DestWarehouseId;
                setWarehouseInfo.Invoke();
            }
            //查询目标仓库的配件销售类型ID
            this.DomainContext.Load(DomainContext.GetSalesUnitByWarehouseIdQuery(partsTransferOrder.DestWarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                var salesUnit = loadOp.Entities.SingleOrDefault();
                if(salesUnit != null) {
                    partsTransferOrder.DestPartsSalesCategoryId = salesUnit.PartsSalesCategoryId;
                }
            }, null);
        }

        private void OriginWarehouseComobox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            Action setWarehouseInfo = () => {
                if(this.KvOriginalWarehouseIds.All(v => v.Key != partsTransferOrder.OriginalWarehouseId))
                    return;
                var originalWarehouse = this.KvOriginalWarehouseIds.First(v => v.Key == partsTransferOrder.OriginalWarehouseId).UserObject as Warehouse;
                if(originalWarehouse == null)
                    return;
                partsTransferOrder.OriginalWarehouseId = originalWarehouse.Id;
                partsTransferOrder.OriginalWarehouseCode = originalWarehouse.Code;
                partsTransferOrder.OriginalWarehouseName = originalWarehouse.Name;
            };
            if(partsTransferOrder.OriginalWarehouseId != default(int) && partsTransferOrder.PartsTransferOrderDetails.Any() && this.originWarehouseId != default(int)) {
                if(partsTransferOrder.OriginalWarehouseId != this.originWarehouseId) {
                    {
                        ShellViewModel.Current.IsBusy = true;
                        foreach(var item in partsTransferOrder.PartsTransferOrderDetails)
                            this.DomainContext.Load(this.DomainContext.调拨查询仓库库存Query(partsTransferOrder.DestWarehouseId).Where(r => r.SparePartId == item.SparePartId), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                var tmp = loadOp.Entities.Where(i => i.SparePartId == item.SparePartId).FirstOrDefault();
                                var tmpPrice = tmp == null ? 0 : tmp.PartsPlannedPrice;
                                if(tmpPrice == 0) {
                                    this.DomainContext.Load(this.DomainContext.GetPartsPlannedPriceBySparePartIdQuery(new int[] { item.SparePartId }, item.PartsSalesCategoryId ?? 0), LoadBehavior.RefreshCurrent, loadOp1 => {
                                        if(loadOp1.HasError) {
                                            if(!loadOp1.IsErrorHandled)
                                                loadOp1.MarkErrorAsHandled();
                                            DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                            ShellViewModel.Current.IsBusy = false;
                                            return;
                                        }
                                        var tmp1 = loadOp1.Entities.FirstOrDefault();
                                        item.PlannPrice = tmp1 == null ? 0 : tmp1.PlannedPrice;
                                    }, null);
                                } else
                                    item.PlannPrice = tmpPrice;
                                item.DestinWarehouseStock = tmp == null ? 0 : tmp.UsableQuantity;
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                    }
                    //DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Confirm_OriginalWarehouseChangedWillClearDetails, () => {
                    //    this.UpdatePartsTransferOrderWithDetail();
                    //    this.originWarehouseId = partsTransferOrder.OriginalWarehouseId;
                    //    partsTransferOrder.TotalAmount = default(decimal);
                    //    partsTransferOrder.TotalPlanAmount = default(decimal);
                    //    setWarehouseInfo.Invoke();
                    //}, () => {
                    //    partsTransferOrder.OriginalWarehouseId = this.originWarehouseId;
                    //    setWarehouseInfo.Invoke();
                    //});
                    setWarehouseInfo.Invoke();
                }
            } else {
                this.originWarehouseId = partsTransferOrder.OriginalWarehouseId;
                setWarehouseInfo.Invoke();
            }
            //查询源仓库的配件销售类型ID
            this.DomainContext.Load(DomainContext.GetSalesUnitByWarehouseIdQuery(partsTransferOrder.OriginalWarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                var salesUnit = loadOp.Entities.SingleOrDefault();
                if(salesUnit != null)
                    partsTransferOrder.OriginPartsSalesCategoryId = salesUnit.PartsSalesCategoryId;

                if(partsTransferOrder.PartsTransferOrderDetails != null && partsTransferOrder.PartsTransferOrderDetails.Count > 0) {
                    UpdatePartsTransferOrderDetail(EntityCustomItem.OriginWarehouseStock);
                    UpdatePartsTransferOrderDetail(EntityCustomItem.DestinWarehouseStock);
                    foreach(var PartsTransferOrderDetail in partsTransferOrder.PartsTransferOrderDetails)
                        this.DomainContext.Load(DomainContext.GetPartsSalesPricesQuery().Where(r => r.PartsSalesCategoryId == partsTransferOrder.OriginPartsSalesCategoryId && r.SparePartId == PartsTransferOrderDetail.SparePartId), LoadBehavior.RefreshCurrent, loadOp1 => {
                            if(loadOp1.HasError) {
                                loadOp1.MarkErrorAsHandled();
                                return;
                            }
                            var salesPrice = loadOp1.Entities.FirstOrDefault();
                            if(salesPrice != null) {
                                PartsTransferOrderDetail.Price = salesPrice.SalesPrice;
                            }
                        }, null);
                }
            }, null);

        }

        private void PartsTransferOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            this.destWarehouseId = partsTransferOrder.DestWarehouseId;
            this.originWarehouseId = partsTransferOrder.OriginalWarehouseId;
        }

        #endregion

        private void UpdatePartsTransferOrderWithDetail() {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null || partsTransferOrder.OriginalWarehouseId == default(int))
                return;
            var originalWarehouse = this.KvOriginalWarehouseIds.First(v => v.Key == partsTransferOrder.OriginalWarehouseId).UserObject as Warehouse;
            var destWarehouse = this.KvDestWarehouseIds.First(v => v.Key == partsTransferOrder.DestWarehouseId).UserObject as Warehouse;
            if(originalWarehouse != null) {
                partsTransferOrder.OriginalWarehouseId = originalWarehouse.Id;
                partsTransferOrder.OriginalWarehouseCode = originalWarehouse.Code;
                partsTransferOrder.OriginalWarehouseName = originalWarehouse.Name;
            }
            if(destWarehouse != null) {
                partsTransferOrder.DestWarehouseId = destWarehouse.Id;
                partsTransferOrder.DestWarehouseCode = destWarehouse.Code;
                partsTransferOrder.DestWarehouseName = destWarehouse.Name;
                partsTransferOrder.StorageCompanyId = destWarehouse.StorageCompanyId;
                partsTransferOrder.StorageCompanyType = destWarehouse.StorageCompanyType;
                partsTransferOrder.ReceivingAddress = destWarehouse.Address;
            }
            //if(partsTransferOrder.PartsTransferOrderDetails.Any())
            //    foreach(var partsTransferOrderDetail in partsTransferOrder.PartsTransferOrderDetails.Where(partsTransferOrderDetail => partsTransferOrder.PartsTransferOrderDetails.Contains(partsTransferOrderDetail)))
            //        partsTransferOrder.PartsTransferOrderDetails.Remove(partsTransferOrderDetail);
        }

        private PartsShiftOrderForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsShiftOrderForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsShiftOrderForUploadDataEditPanel)DI.GetDataEditPanel("PartsShiftOrderForUpload"));
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(Utils.GetEntityLocalizedName(typeof(PartsTransferOrder), "PartsTransferOrderDetails"), null, () => this.PartsTransferOrderDetaiForEditDataGridView);
            dcsDetailGridView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_SearchOriginalWarehouse,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/query.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.UpdatePartsTransferOrderDetail(EntityCustomItem.OriginWarehouseStock))
            });
            dcsDetailGridView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_SearchDestWarehouse,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/query.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.UpdatePartsTransferOrderDetail(EntityCustomItem.DestinWarehouseStock))
            });
            dcsDetailGridView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Action_Title_Import,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(() => {
                    var partsTransferOrder = this.DataContext as PartsTransferOrder;
                    if(partsTransferOrder == null)
                        return;
                    if(partsTransferOrder.OriginalWarehouseId == default(int)) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrder_OriginalWarehouseIdIsNull);
                        return;
                    }
                    if(partsTransferOrder.DestWarehouseId == default(int)) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrder_DestWarehouseIdIsNull);
                        return;
                    }
                    this.Uploader.ShowFileDialog();
                })
            });
            dcsDetailGridView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Title_ExportTemplate,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = new DelegateCommand(() => {
                    var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName,
                                                //IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_Quantity,
                                                IsRequired = true
                                            }
                                            //,new ImportTemplateColumn {
                                            //    Name = "PO单号"
                                            //}

                                        };
                    this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                        }
                        if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                            this.ExportFile(loadOp.Value);
                    }, null);
                })
            });
            dcsDetailGridView.SetValue(Grid.ColumnProperty, 2);
            //dcsDetailGridView.HorizontalContentAlignment="left";
            this.Root.Children.Add(dcsDetailGridView);
            this.InitializeData();

            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 9);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);
        }

        #region 导入 导出模板
        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string EXPORT_DATA_FILE_NAME = "配件调拨单清单模板.xlsx";
        private RadUpload uploader;

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var partsTransferOrder = this.DataContext as PartsTransferOrder;
                        if(partsTransferOrder == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportpartsTransferOrderDetailAsync(e.HandlerData.CustomData["Path"].ToString(), partsTransferOrder.OriginalWarehouseId, partsTransferOrder.StorageCompanyId);
                        this.excelServiceClient.ImportpartsTransferOrderDetailCompleted -= this.ExcelServiceClient_ImportpartsTransferOrderDetailCompleted;
                        this.excelServiceClient.ImportpartsTransferOrderDetailCompleted += this.ExcelServiceClient_ImportpartsTransferOrderDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            this.ErrorFileName = errorFileName;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(ErrorFileName));
        }

        protected string ErrorFileName {
            get;
            set;
        }

        private void ExcelServiceClient_ImportpartsTransferOrderDetailCompleted(object sender, ImportpartsTransferOrderDetailCompletedEventArgs e) {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            var detailList = partsTransferOrder.PartsTransferOrderDetails.ToList();
            foreach(var detail in detailList) {
                partsTransferOrder.PartsTransferOrderDetails.Remove(detail);
            }
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    partsTransferOrder.PartsTransferOrderDetails.Add(new PartsTransferOrderDetail {
                        SparePartId = data.SparePartId,
                        SparePartCode = data.SparePartCode,
                        SparePartName = data.SparePartName,
                        PlannedAmount = data.PlannedAmount,
                        OriginWarehouseStock = data.UsableQuantity,
                        Price = data.PlanedPrice,
                        PlannPrice = data.PlanedPrice,
                        POCode = data.POCode,
                        MInPackingAmount = data.MInPackingAmount,
                        PartsSalesCategoryId = data.PartsSalesCategoryId
                    });
                }
                partsTransferOrder.TotalAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(detail => detail.PlannedAmount * detail.Price);
                partsTransferOrder.TotalPlanAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(detail => detail.PlannedAmount * detail.PlannPrice);
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsStockingUIStrings.Upload_Error3);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }
        #endregion

        private void InitializeData() {
            this.KeyValueManager.LoadData();
            this.KvDestWarehouseIds.Clear();
            this.KvOriginalWarehouseIds.Clear();
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && (v.Type == (int)DcsWarehouseType.总库 || v.Type == (int)DcsWarehouseType.分库)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehosue in loadOp.Entities) {
                    this.KvDestWarehouseIds.Add(new KeyValuePair {
                        Key = warehosue.Id,
                        Value = warehosue.Name,
                        UserObject = warehosue
                    });
                    this.KvOriginalWarehouseIds.Add(new KeyValuePair {
                        Key = warehosue.Id,
                        Value = warehosue.Name,
                        UserObject = warehosue
                    });
                }
            }, null);
        }

        private DataGridViewBase PartsTransferOrderDetaiForEditDataGridView {
            get {
                if(this.partsTransferOrderDetaiForEditDataGridView == null) {
                    this.partsTransferOrderDetaiForEditDataGridView = DI.GetDataGridView("PartsTransferOrderDetailForEdit");
                    this.partsTransferOrderDetaiForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsTransferOrderDetaiForEditDataGridView;
            }
        }

        private void UpdatePartsTransferOrderDetail(object entityItem) {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;

            var partIds = partsTransferOrder.PartsTransferOrderDetails.Select(v => v.SparePartId).ToArray();
            if(partIds == null)
                return;
            var warehouseId = entityItem.Equals(EntityCustomItem.OriginWarehouseStock) ? partsTransferOrder.OriginalWarehouseId : partsTransferOrder.DestWarehouseId;
            if(partIds.Length > 0)
                this.DomainContext.Load(this.DomainContext.配件调拨查询仓库库存Query(BaseApp.Current.CurrentUserData.EnterpriseId, warehouseId, partIds), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    if(!loadOp.Entities.Any()) {
                        foreach(var partsTransferOrderDetail in partsTransferOrder.PartsTransferOrderDetails) {
                            if(entityItem.Equals(EntityCustomItem.OriginWarehouseStock))
                                partsTransferOrderDetail.OriginWarehouseStock = 0;
                            else
                                partsTransferOrderDetail.DestinWarehouseStock = 0;
                        }
                    } else {
                        foreach(var partsTransferOrderDetail in partsTransferOrder.PartsTransferOrderDetails) {
                            //此处防止开始的数据非正常录入导致的First报错
                            if(loadOp.Entities != null && loadOp.Entities.Any(v => v.SparePartId == partsTransferOrderDetail.SparePartId))
                                if(entityItem.Equals(EntityCustomItem.OriginWarehouseStock))
                                    partsTransferOrderDetail.OriginWarehouseStock = loadOp.Entities.First(v => v.SparePartId == partsTransferOrderDetail.SparePartId).UsableQuantity;
                                else
                                    partsTransferOrderDetail.DestinWarehouseStock = loadOp.Entities.First(v => v.SparePartId == partsTransferOrderDetail.SparePartId).UsableQuantity;
                        }
                    }

                }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsTransferOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.FileUploadDataEditPanels.FilePath = entity.Path;
                //由于源仓库品牌Id和目标仓库品牌Id在修改的时候重新查一遍
                //查询目标仓库的配件销售类型ID
                this.DomainContext.Load(DomainContext.GetSalesUnitByWarehouseIdQuery(entity.DestWarehouseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                    var salesUnit = loadOp1.Entities.SingleOrDefault();
                    if(salesUnit != null) {
                        entity.DestPartsSalesCategoryId = salesUnit.PartsSalesCategoryId;
                    }
                }, null);
                //查询源仓库的配件销售类型ID
                this.DomainContext.Load(DomainContext.GetSalesUnitByWarehouseIdQuery(entity.OriginalWarehouseId), LoadBehavior.RefreshCurrent, loadOp2 => {
                    if(loadOp2.HasError) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                    var salesUnit = loadOp2.Entities.SingleOrDefault();
                    if(salesUnit != null)
                        entity.OriginPartsSalesCategoryId = salesUnit.PartsSalesCategoryId;
                }, null);
                this.SetObjectToEdit(entity);
                this.UpdatePartsTransferOrderDetail(EntityCustomItem.OriginWarehouseStock);
                this.UpdatePartsTransferOrderDetail(EntityCustomItem.DestinWarehouseStock);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsTransferOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsTransferOrderDetaiForEditDataGridView.CommitEdit())
                return;
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            partsTransferOrder.Path = this.FileUploadDataEditPanels.FilePath;
            partsTransferOrder.ValidationErrors.Clear();
            if(partsTransferOrder.OriginalWarehouseId == default(int))
                partsTransferOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrder_OriginalWarehouseIdIsNull, new[] {
                    "OriginalWarehouseId"
                }));
            if(partsTransferOrder.DestWarehouseId == default(int))
                partsTransferOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrder_DestWarehouseIdIsNull, new[] {
                    "DestWarehouseId"
                }));
            if(partsTransferOrder.ShippingMethod.GetValueOrDefault() == default(int))
                partsTransferOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrder_ShippingMethodIsNull, new[] {
                    "ShippingMethod"
                }));
            if(partsTransferOrder.Type == default(int))
                partsTransferOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrder_TypeIsNull, new[] {
                    "Type"
                }));
            if(partsTransferOrder.OriginalWarehouseId == partsTransferOrder.DestWarehouseId) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrder_DestWarehouseIdIsNoEqualsOriginalWarehouseId);
                return;
            }
            if(partsTransferOrder.PartsTransferOrderDetails.GroupBy(item => new {
                item.SparePartId
            }).Any(entity => entity.Count() > 1)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrderDetail_SparePartIdIsRepeat);
                return;
            }
            //foreach(var partsTransferOrderDetail in partsTransferOrder.PartsTransferOrderDetails) {
            //    if(partsTransferOrderDetail.PlannedAmount < 1) {
            //        UIHelper.ShowNotification(String.Format(PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_PlannedAmountIsEmpty, partsTransferOrderDetail.SparePartName));
            //        return;
            //    }
            //}
            if(!partsTransferOrder.PartsTransferOrderDetails.Any()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Validation_PartsTransferOrder_PartsTransferOrderDetailsIsNull);
                return;
            }
            if(partsTransferOrder.PartsTransferOrderDetails.Any(r => r.PlannedAmount <= 0)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_DetailQuantityGreaterThanZero);
                return;
            }
            List<string> listerrors=new List<string>();
            foreach(var partsTransferOrderDetail in partsTransferOrder.PartsTransferOrderDetails) {
                if(partsTransferOrderDetail.Price == default(decimal) ) {
                    UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_PartNoCostPrice, partsTransferOrderDetail.SparePartCode));
                    return;
                }
                if(partsTransferOrderDetail.PlannedAmount > partsTransferOrderDetail.OriginWarehouseStock) {
                    listerrors.Add(partsTransferOrderDetail.SparePartCode);
                }
            }
            if(listerrors.Count > 0) {
                var itemerror = "";
                foreach(var item in listerrors) {
                    itemerror += string.Format(PartsStockingUIStrings.DataEditView_Validation_PartUsableQuantityError,item);
                }
                UIHelper.ShowNotification(itemerror);
                return;
            }


            foreach(var relation in partsTransferOrder.PartsTransferOrderDetails.Where(e => e.PlannedAmount > e.MaxPlannedAmount)) {
                UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Validation_PlanQuantityError, relation.SparePartCode, relation.MaxPlannedAmount));
                return;
            }

            if(partsTransferOrder.PartsTransferOrderDetails.Count > 400) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_DetailsCountGreaterThanFourHundred);
                return;
            }
            if(partsTransferOrder.DestWarehouseName.Contains("海外轻卡") && partsTransferOrder.PartsTransferOrderDetails.Any(r => string.IsNullOrWhiteSpace(r.POCode))) {
                UIHelper.ShowNotification("海外轻卡的目标仓库,清单PO单号不能为空");
                return;
            }
            partsTransferOrder.GPMSPurOrderCode = partsTransferOrder.SAPPurchasePlanCode;
            ////如果目标仓库配件销售类型Id<>源仓库配件销售类型Id时进行以下判断
            //if(partsTransferOrder.DestPartsSalesCategoryId != partsTransferOrder.OriginPartsSalesCategoryId) {
            //    //如果清单中源仓库计划价<>目标仓库计划价 提示：源仓库与目标仓库对应品牌的计划价不相等，不允许调拨，请调整计划价后再调拨
            //    var dictionary1 = new Dictionary<int, decimal>();
            //    var dictionary2 = new Dictionary<int, decimal>();
            //    var ids = partsTransferOrder.PartsTransferOrderDetails.Select(r => r.SparePartId).ToArray();
            //    //查询源仓库计划价
            //    this.DomainContext.Load(DomainContext.GetPartsPlannedPriceBySparePartIdQuery(ids, partsTransferOrder.OriginPartsSalesCategoryId), loadOp => {
            //        if(loadOp.HasError) {
            //            loadOp.MarkErrorAsHandled();
            //            return;
            //        }
            //        if(loadOp.Entities != null && loadOp.Entities.Any()) {
            //            foreach(var item in loadOp.Entities) {
            //                dictionary1.Add(item.SparePartId, item.PlannedPrice);
            //            }
            //            foreach(var detail in partsTransferOrder.PartsTransferOrderDetails) {
            //                if(!dictionary1.Any())
            //                    break;
            //                foreach(var key in dictionary1.Keys.Where(key => detail.SparePartId == key)) {
            //                    detail.Oprice = dictionary1[key];
            //                }
            //            }
            //        }
            //        //查询目标仓库计划价
            //        this.DomainContext.Load(DomainContext.GetPartsPlannedPriceBySparePartIdQuery(ids, partsTransferOrder.DestPartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp1 => {
            //            if(loadOp1.HasError) {
            //                loadOp1.MarkErrorAsHandled();
            //                return;
            //            }
            //            if(loadOp1.Entities != null && loadOp1.Entities.Any()) {
            //                foreach(var item in loadOp1.Entities) {
            //                    dictionary2.Add(item.SparePartId, item.PlannedPrice);
            //                }
            //                foreach(var detail1 in partsTransferOrder.PartsTransferOrderDetails) {
            //                    if(!dictionary2.Any())
            //                        break;
            //                    foreach(var key in dictionary2.Keys.Where(key => detail1.SparePartId == key)) {
            //                        detail1.Dprice = dictionary2[key];
            //                    }
            //                }
            //            }
            //            check = partsTransferOrder.PartsTransferOrderDetails.FirstOrDefault(r => r.Oprice != r.Dprice || (r.Oprice == 0 && r.Dprice == 0));
            //            if(check != null) {
            //                var code = check.SparePartCode;
            //                UIHelper.ShowNotification("配件" + code + "源仓库与目标仓库对应品牌的计划价不相等，不允许调拨，请调整计划价后再调拨");
            //                return;
            //            }
            //            if(partsTransferOrder.ValidationErrors.Any() || partsTransferOrder.PartsTransferOrderDetails.Any(detail => detail.HasValidationErrors))
            //                return;
            //            if(PurOrderId.HasValue) {
            //                this.DomainContext.Load(DomainContext.GetPartsPurchasePlanDetail_HWQuery().Where(i => i.PartsPurchasePlanId == PurOrderId.Value), LoadBehavior.RefreshCurrent, loadOp2 => {
            //                    if(loadOp2.HasError) {
            //                        loadOp2.MarkErrorAsHandled();
            //                        return;
            //                    }
            //                    if(partsTransferOrder.PartsTransferOrderDetails.Any(detail => loadOp2.Entities.Any(i => i.PartId == detail.SparePartId && i.UntFulfilledQty.HasValue && (i.UntFulfilledQty.Value - (i.EndAmount ?? 0)) < detail.PlannedAmount))) {
            //                        UIHelper.ShowNotification("配件调拨数量不能大于采购计划的可分解数量，请核对");
            //                        return;
            //                    }
            //                    ((IEditableObject)partsTransferOrder).EndEdit();
            //                    if(!string.IsNullOrEmpty(partsTransferOrder.PurOrderCode) && partsTransferOrder.Can调拨单更新采购计划清单)
            //                        partsTransferOrder.调拨单更新采购计划清单();
            //                    base.OnEditSubmitting();
            //                }, null);
            //            } else {
            //                ((IEditableObject)partsTransferOrder).EndEdit();
            //                if(!string.IsNullOrEmpty(partsTransferOrder.PurOrderCode) && partsTransferOrder.Can调拨单更新采购计划清单)
            //                    partsTransferOrder.调拨单更新采购计划清单();
            //                base.OnEditSubmitting();
            //            }



            //        }, null);
            //    }, null);
            //} else {
            //    if(partsTransferOrder.ValidationErrors.Any() || partsTransferOrder.PartsTransferOrderDetails.Any(detail => detail.HasValidationErrors))
            //        return;
            //    ((IEditableObject)partsTransferOrder).EndEdit();
            //    if(!string.IsNullOrEmpty(partsTransferOrder.PurOrderCode) && partsTransferOrder.Can调拨单更新采购计划清单)
            //        partsTransferOrder.调拨单更新采购计划清单();
            //    base.OnEditSubmitting();
            //}
            foreach(var partsTransferOrderDetail in partsTransferOrder.PartsTransferOrderDetails) {
                //计划价和销售价  都取配件成本价
                partsTransferOrderDetail.Price =(decimal) partsTransferOrderDetail.PlannPrice;
            }

            if(partsTransferOrder.ValidationErrors.Any() || partsTransferOrder.PartsTransferOrderDetails.Any(detail => detail.HasValidationErrors))
                    return;
                ((IEditableObject)partsTransferOrder).EndEdit();
                if(!string.IsNullOrEmpty(partsTransferOrder.PurOrderCode) && partsTransferOrder.Can调拨单更新采购计划清单)
                    partsTransferOrder.调拨单更新采购计划清单();
                base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public object KvType {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvShippingMethod {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public ObservableCollection<KeyValuePair> KvDestWarehouseIds {
            get {
                return this.kvDestWarehouseIds ?? (this.kvDestWarehouseIds = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvOriginalWarehouseIds {
            get {
                return this.kvOriginalWarehouseIds ?? (this.kvOriginalWarehouseIds = new ObservableCollection<KeyValuePair>());
            }
        }

        private void OriginWarehouseComobox_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            this.UpdatePartsTransferOrderDetail(EntityCustomItem.OriginWarehouseStock);
        }

        private void DestWarehouseComobox_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            this.UpdatePartsTransferOrderDetail(EntityCustomItem.DestinWarehouseStock);
        }
    }
}
