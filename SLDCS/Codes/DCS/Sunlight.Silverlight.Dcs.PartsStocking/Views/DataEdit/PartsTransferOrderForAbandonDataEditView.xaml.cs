﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsTransferOrderForAbandonDataEditView {
        public PartsTransferOrderForAbandonDataEditView() {
            InitializeComponent();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsTransferOrdersByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            partsTransferOrder.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(partsTransferOrder.AbandonComment))
                partsTransferOrder.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_AbandonReasonIsNull, new[] {
                    "AbandonComment"
                }));

            if(partsTransferOrder.HasValidationErrors)
                return;

            try {
                if(partsTransferOrder.Can作废配件调拨单)
                    partsTransferOrder.作废配件调拨单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_AbandonPartsTransferOrder;
            }
        }

    }
}
