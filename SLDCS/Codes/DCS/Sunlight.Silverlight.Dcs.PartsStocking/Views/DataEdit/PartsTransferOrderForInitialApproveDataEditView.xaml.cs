﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsTransferOrderForInitialApproveDataEditView {
        private ButtonItem rejectBtn;

        private enum EntityCustomItem {
            OriginWarehouseStock,
            DestinWarehouseStock
        }

        public PartsTransferOrderForInitialApproveDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase partsTransferOrderDetailForApproveDataGridView;

        private DataGridViewBase PartsTransferOrderDetailForApproveDataGridView {
            get {
                if(this.partsTransferOrderDetailForApproveDataGridView == null) {
                    this.partsTransferOrderDetailForApproveDataGridView = DI.GetDataGridView("PartsTransferOrderDetailForInitialApprove");
                    this.partsTransferOrderDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsTransferOrderDetailForApproveDataGridView;
            }
        }

        private PartsShiftOrderForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsShiftOrderForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsShiftOrderForUploadDataEditPanel)DI.GetDataEditPanel("PartsShiftOrderForUpload"));
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("PartsTransferOrderForApprove"));

            FileUploadDataEditPanels.isHiddenButtons = true;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 2);
            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            this.Root.Children.Add(FileUploadDataEditPanels);
            this.Root.Children.Add(this.CreateVerticalLine(1,1,1,2));

            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.SetValue(Grid.RowSpanProperty, 2);
            dcsDetailGridView.Register(Utils.GetEntityLocalizedName(typeof(PartsTransferOrder), "PartsTransferOrderDetails"), null, () => this.PartsTransferOrderDetailForApproveDataGridView);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            dcsDetailGridView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_SearchOriginalWarehouse,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/query.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.UpdatePartsTransferOrderDetail(EntityCustomItem.OriginWarehouseStock))
            });
            dcsDetailGridView.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.Button_Text_Common_SearchDestWarehouse,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/query.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.UpdatePartsTransferOrderDetail(EntityCustomItem.DestinWarehouseStock))
            });
            dcsDetailGridView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(dcsDetailGridView);

            this.rejectBtn = new ButtonItem {
                Command = new DelegateCommand(this.RejecrCurrentData),
                Title = PartsStockingUIStrings.Action_Title_Reject,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative)
            };
            this.RegisterButton(rejectBtn);
        }

        private void UpdatePartsTransferOrderDetail(object entityItem) {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;

            var partIds = partsTransferOrder.PartsTransferOrderDetails.Select(v => v.SparePartId).ToArray();
            if(partIds == null)
                return;

            var warehouseId = entityItem.Equals(EntityCustomItem.OriginWarehouseStock) ? partsTransferOrder.OriginalWarehouseId : partsTransferOrder.DestWarehouseId;
            this.DomainContext.WarehousePartsStocks.Clear();
            this.DomainContext.Load(this.DomainContext.配件调拨查询仓库库存Query(BaseApp.Current.CurrentUserData.EnterpriseId, warehouseId, partIds), loadOp => {
                if(loadOp.HasError )
                    return;
                foreach(var partsTransferOrderDetail in partsTransferOrder.PartsTransferOrderDetails) {
                    if(loadOp.Entities != null && loadOp.Entities.Any() && loadOp.Entities.Any(v => v.SparePartId == partsTransferOrderDetail.SparePartId)) {
                        if(entityItem.Equals(EntityCustomItem.OriginWarehouseStock)) {
                            partsTransferOrderDetail.OriginWarehouseStock = loadOp.Entities.First(v => v.SparePartId == partsTransferOrderDetail.SparePartId).UsableQuantity;
                            if(partsTransferOrderDetail.OriginWarehouseStock >= partsTransferOrderDetail.PlannedAmount) {
                                partsTransferOrderDetail.ConfirmedAmount = partsTransferOrderDetail.ConfirmedAmount;
                            } else {
                                partsTransferOrderDetail.ConfirmedAmount = partsTransferOrderDetail.OriginWarehouseStock;
                            }
                        } else {
                            partsTransferOrderDetail.DestinWarehouseStock = loadOp.Entities.First(v => v.SparePartId == partsTransferOrderDetail.SparePartId).UsableQuantity;
                        }
                    }
                    else {
                        if(entityItem.Equals(EntityCustomItem.OriginWarehouseStock)) {
                            partsTransferOrderDetail.ConfirmedAmount = 0;
                        } else {
                            partsTransferOrderDetail.DestinWarehouseStock = 0;
                        }
                    }
                }
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsTransferOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                FileUploadDataEditPanels.FilePath = entity.Path;
                foreach(var partsTransferOrderDetail in entity.PartsTransferOrderDetails) {
                    partsTransferOrderDetail.ConfirmedAmount = partsTransferOrderDetail.PlannedAmount;
                }
                this.SetObjectToEdit(entity);
                this.UpdatePartsTransferOrderDetail(EntityCustomItem.OriginWarehouseStock);
                this.UpdatePartsTransferOrderDetail(EntityCustomItem.DestinWarehouseStock);

            }, null);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_InitialApprovePartsTransferOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.PartsTransferOrderDetailForApproveDataGridView.CommitEdit())
                return;
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            partsTransferOrder.ValidationErrors.Clear();
            if(partsTransferOrder.PartsTransferOrderDetails.Any(e => e.ConfirmedAmount.GetValueOrDefault(0) > e.OriginWarehouseStock)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrderDetail_ConfirmedAmountIsLessThanOriginWarehouseStock);
                return;
            }
            if(partsTransferOrder.PartsTransferOrderDetails.Sum(e => e.ConfirmedAmount.GetValueOrDefault(0)) <= 0) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrderDetail_TotalConfirmedAmountIsLessThanOne);
                return;
            }
            foreach(var partsTransferOrderDetail in partsTransferOrder.PartsTransferOrderDetails) {
                if(partsTransferOrderDetail.PlannedAmount < partsTransferOrderDetail.ConfirmedAmount) {
                    UIHelper.ShowNotification(String.Format(PartsStockingUIStrings.DataEditView_Validation_PartsOutboundBill_PlannedAmountIsEmpty, partsTransferOrderDetail.SparePartCode));
                    return;
                }
            }
            if(partsTransferOrder.ValidationErrors.Any() || partsTransferOrder.PartsTransferOrderDetails.Any(v => v.HasValidationErrors))
                return;
            ((IEditableObject)partsTransferOrder).EndEdit();
            try {
                partsTransferOrder.ApprovalId = 0;
                if(partsTransferOrder.Can初审配件调拨单)
                    partsTransferOrder.初审配件调拨单();
                ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_OperationSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void Reset() {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder != null && this.DomainContext.PartsTransferOrders.Contains(partsTransferOrder))
                this.DomainContext.PartsTransferOrders.Detach(partsTransferOrder);
        }

        
        private void RejecrCurrentData() {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            partsTransferOrder.ApprovalId = 0;
            partsTransferOrder.ValidationErrors.Clear();
            ((IEditableObject)partsTransferOrder).EndEdit();
            try {
                if(partsTransferOrder.Can驳回配件调拨单)
                    partsTransferOrder.驳回配件调拨单();
                ExecuteSerivcesMethod(PartsStockingUIStrings.DataEditView_Text_RejectSuccess);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
                UIHelper.ShowNotification(notifyMessage);
            }, null);
        }

        public void OnCustomEditSubmitted() {
            this.DataContext = null;
        }

        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        protected override bool OnRequestCanSubmit()
        {
            return true;
        }
    }
}
