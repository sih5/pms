﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsTransferOrderForKanBanDataEditView {
        private DataGridViewBase partsTransferBillDetailDataGridView;
        private DataGridViewBase partsTransferOrderDetailForKanBanDataGridView;
        private ObservableCollection<KeyValuePair<string, string>> partsTransferBillDetails;
        private ObservableCollection<SparePartStatisticsForKanBan> sparePartStatisticsForKanBans;

        public PartsTransferOrderForKanBanDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase PartsTransferOrderDetailForKanBanDataGridView {
            get {
                if(this.partsTransferOrderDetailForKanBanDataGridView == null) {
                    this.partsTransferOrderDetailForKanBanDataGridView = DI.GetDataGridView("PartsTransferOrderDetailForKanBan");
                    this.partsTransferOrderDetailForKanBanDataGridView.DomainContext = this.DomainContext;
                    this.partsTransferOrderDetailForKanBanDataGridView.DataContext = this;
                }
                return this.partsTransferOrderDetailForKanBanDataGridView;
            }
        }

        private DataGridViewBase PartsTransferBillDetailDataGridView {
            get {
                if(this.partsTransferBillDetailDataGridView == null) {
                    this.partsTransferBillDetailDataGridView = DI.GetDataGridView("PartsTransferBillDetail");
                    this.partsTransferBillDetailDataGridView.DomainContext = this.DomainContext;
                    this.partsTransferBillDetailDataGridView.DataContext = this;
                }
                return this.partsTransferBillDetailDataGridView;
            }
        }

        private void CreateUI() {
            var radTabControl = new RadTabControl();
            radTabControl.BackgroundVisibility = Visibility.Collapsed;
            radTabControl.Items.Add(new RadTabItem {
                Header = PartsStockingUIStrings.DataEditView_Title_PartsTransferOrderKanBan,
                Content = this.PartsTransferOrderDetailForKanBanDataGridView
            });
            radTabControl.Items.Add(new RadTabItem {
                Header = PartsStockingUIStrings.DataEditView_Title_TransferOrderDetailBill,
                Content = this.PartsTransferBillDetailDataGridView
            });
            this.Root.Children.Add(radTabControl);
        }

        private void LoadEntityToEdit(int id) {
            this.PartsTransferBillDetails.Clear();
            this.SparePartStatisticsForKanBans.Clear();
            this.DomainContext.Load((EntityQuery)this.DomainContext.查询调拨看板Query(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.Cast<PartsTransferOrder>().SingleOrDefault();
                if(entity == null)
                    return;
                foreach(var detail in entity.PartsTransferOrderDetails) {
                    var sparePartStatistics = new SparePartStatisticsForKanBan {
                        SparePartId = detail.SparePartId,
                        SparePartCode = detail.SparePartCode,
                        SparePartName = detail.SparePartName,
                        PartsTransferAmount = entity.PartsTransferOrderDetails.Where(e => e.SparePartId == detail.SparePartId).Sum(e => e.ConfirmedAmount.GetValueOrDefault())
                    };
                    this.SparePartStatisticsForKanBans.Add(sparePartStatistics);
                    if(entity.PartsOutboundBills.Any())
                        sparePartStatistics.PartsOutboundAmount = entity.PartsOutboundBills.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单 && v.PartsOutboundBillDetails.Any(e => e.SparePartId == detail.SparePartId)).SelectMany(e => e.PartsOutboundBillDetails).Where(e => e.SparePartId == detail.SparePartId).Sum(e => e.OutboundAmount);
                    if(entity.PartsShippingOrders.Any())
                        sparePartStatistics.PartsShippingAmount = entity.PartsShippingOrders.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单 && v.PartsShippingOrderDetails.Any(e => e.SparePartId == detail.SparePartId)).SelectMany(e => e.PartsShippingOrderDetails).Where(e => e.SparePartId == detail.SparePartId).Sum(e => e.ShippingAmount);
                    if(entity.PartsInboundCheckBills.Any())
                        sparePartStatistics.PartsInboundAmount = entity.PartsInboundCheckBills.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单 && v.PartsInboundCheckBillDetails.Any(e => e.SparePartId == detail.SparePartId)).SelectMany(e => e.PartsInboundCheckBillDetails).Where(e => e.SparePartId == detail.SparePartId).Sum(e => e.InspectedQuantity);
                }

                var partsOutboundBills = entity.PartsOutboundBills.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单).ToArray();
                var partsShippingOrders = entity.PartsShippingOrders.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单).ToArray();
                var partsInboundCheckBills = entity.PartsInboundCheckBills.Where(v => v.OriginalRequirementBillId == entity.Id && v.OriginalRequirementBillType == (int)DcsOriginalRequirementBillType.配件调拨单).ToArray();
                var newBills1 = from partsOutboundBill in partsOutboundBills
                                select new {
                                    BillCode = partsOutboundBill.Code,
                                    BillType = PartsStockingUIStrings.DataDetailView_PartsTransferOrderDetail_Constant_OutboundBillBill
                                };
                var newBills2 = from partsShippingOrder in partsShippingOrders
                                select new {
                                    BillCode = partsShippingOrder.Code,
                                    BillType = PartsStockingUIStrings.DataDetailView_PartsTransferOrderDetail_Constant_ShippingBill
                                };
                var newBills3 = from partsInboundCheckBill in partsInboundCheckBills
                                select new {
                                    BillCode = partsInboundCheckBill.Code,
                                    BillType = PartsStockingUIStrings.DataDetailView_PartsTransferOrderDetail_Constant_CheckBill
                                };
                foreach(var bill in newBills1)
                    this.PartsTransferBillDetails.Add(new KeyValuePair<string, string>(bill.BillCode, bill.BillType));
                foreach(var bill in newBills2)
                    this.PartsTransferBillDetails.Add(new KeyValuePair<string, string>(bill.BillCode, bill.BillType));
                foreach(var bill in newBills3)
                    this.PartsTransferBillDetails.Add(new KeyValuePair<string, string>(bill.BillCode, bill.BillType));
            }, null);
        }

        protected override string Title {
            get {
                return PartsStockingUIStrings.DataEditView_Title_PartsTransferOrderKanBan;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public ObservableCollection<SparePartStatisticsForKanBan> SparePartStatisticsForKanBans {
            get {
                return this.sparePartStatisticsForKanBans ?? (this.sparePartStatisticsForKanBans = new ObservableCollection<SparePartStatisticsForKanBan>());
            }
        }

        public ObservableCollection<KeyValuePair<string, string>> PartsTransferBillDetails {
            get {
                return this.partsTransferBillDetails ?? (this.partsTransferBillDetails = new ObservableCollection<KeyValuePair<string, string>>());
            }
        }
    }
}
