﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using Sunlight.Silverlight.Dcs.PartsStocking.Panels.DataEdit;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PartsTransferOrderWithDetailsDataEditView {
       private KeyValueManager keyValueManager;
        private ObservableCollection<KeyValuePair> kvOriginalWarehouseIds;
        private ObservableCollection<KeyValuePair> kvDestWarehouseIds;
        private readonly string[] kvNames = {
            "PartsTransferOrder_Type", "PartsShipping_Method"
        };
     private enum EntityCustomItem {
            OriginWarehouseStock,
            DestinWarehouseStock
        }

        private DataGridViewBase partsTransferOrderDetaiForEditDataGridView;
        public int? PurOrderId;

        public PartsTransferOrderWithDetailsDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }        

        private PartsShiftOrderForUploadDataEditPanel fileUploadDataEditPanels;
        public PartsShiftOrderForUploadDataEditPanel FileUploadDataEditPanels {
            get {
                return this.fileUploadDataEditPanels ?? (this.fileUploadDataEditPanels = (PartsShiftOrderForUploadDataEditPanel)DI.GetDataEditPanel("PartsShiftOrderForUpload"));
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(Utils.GetEntityLocalizedName(typeof(PartsTransferOrder), "PartsTransferOrderDetails"), null, () => this.PartsTransferOrderDetaiForEditDataGridView);           
            dcsDetailGridView.SetValue(Grid.ColumnProperty, 2);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            this.Root.Children.Add(dcsDetailGridView);
            this.InitializeData();

            FileUploadDataEditPanels.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            FileUploadDataEditPanels.SetValue(Grid.RowProperty, 9);
            FileUploadDataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.LayoutRoot.Children.Add(FileUploadDataEditPanels);
            this.FileUploadDataEditPanels.isHiddenButtons = true;        
        }
        private void InitializeData() {
            this.KeyValueManager.LoadData();
            this.KvDestWarehouseIds.Clear();
            this.KvOriginalWarehouseIds.Clear();
            this.DomainContext.Load(this.DomainContext.GetWarehousesOrderByNameQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.StorageCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && (v.Type == (int)DcsWarehouseType.总库 || v.Type == (int)DcsWarehouseType.分库)), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var warehosue in loadOp.Entities) {
                    this.KvDestWarehouseIds.Add(new KeyValuePair {
                        Key = warehosue.Id,
                        Value = warehosue.Name,
                        UserObject = warehosue
                    });
                    this.KvOriginalWarehouseIds.Add(new KeyValuePair {
                        Key = warehosue.Id,
                        Value = warehosue.Name,
                        UserObject = warehosue
                    });
                }
            }, null);
        }

        private DataGridViewBase PartsTransferOrderDetaiForEditDataGridView {
            get {
                if(this.partsTransferOrderDetaiForEditDataGridView == null) {
                    this.partsTransferOrderDetaiForEditDataGridView = DI.GetDataGridView("PartsTransferOrderDetailForEdit");
                    this.partsTransferOrderDetaiForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.partsTransferOrderDetaiForEditDataGridView;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsTransferOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.FileUploadDataEditPanels.FilePath = entity.Path;
                //由于源仓库品牌Id和目标仓库品牌Id在修改的时候重新查一遍
                //查询目标仓库的配件销售类型ID
                this.DomainContext.Load(DomainContext.GetSalesUnitByWarehouseIdQuery(entity.DestWarehouseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                    var salesUnit = loadOp1.Entities.SingleOrDefault();
                    if(salesUnit != null) {
                        entity.DestPartsSalesCategoryId = salesUnit.PartsSalesCategoryId;
                    }
                }, null);
                //查询源仓库的配件销售类型ID
                this.DomainContext.Load(DomainContext.GetSalesUnitByWarehouseIdQuery(entity.OriginalWarehouseId), LoadBehavior.RefreshCurrent, loadOp2 => {
                    if(loadOp2.HasError) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                    var salesUnit = loadOp2.Entities.SingleOrDefault();
                    if(salesUnit != null)
                        entity.OriginPartsSalesCategoryId = salesUnit.PartsSalesCategoryId;
                }, null);
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_PartsTransferOrder;
            }
        }
  
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public object KvType {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvShippingMethod {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public ObservableCollection<KeyValuePair> KvDestWarehouseIds {
            get {
                return this.kvDestWarehouseIds ?? (this.kvDestWarehouseIds = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvOriginalWarehouseIds {
            get {
                return this.kvOriginalWarehouseIds ?? (this.kvOriginalWarehouseIds = new ObservableCollection<KeyValuePair>());
            }
        }

      
    }
}