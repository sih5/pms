﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Telerik.Windows.Controls;
using System.Collections.Generic;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PickingTaskForPickDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private DcsDomainContext domainContext = new DcsDomainContext();
        private ObservableCollection<VirtualPickingTaskDetail> pickingTaskDetails;
        private DataGridViewBase pickingTaskDetailForEditDataGridView;
        private string code, orderTypeName, warehouseName, counterpartCompanyCode, counterpartCompanyName, remark;
        private int partsSalesCategoryId;
        private int id;
        private DcsMultiPopupsQueryWindowBase warehouseAreaWindow;

        public PickingTaskForPickDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.PickingTaskDetailsds.Clear();
            this.DomainContext.Load(this.DomainContext.GetPickingTaskWithsDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.Code = entity.Code;
                    this.PartsSalesCategoryId = entity.PartsSalesCategoryId;
                    this.WarehouseName = entity.WarehouseName;
                    this.OrderTypeName = entity.OrderTypeName;
                    this.CounterpartCompanyCode = entity.CounterpartCompanyCode;
                    this.CounterpartCompanyName = entity.CounterpartCompanyName;
                    this.Remark = entity.Remark;
                    this.id = entity.Id;
                    this.SetObjectToEdit(entity);
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetPickingTaskDetailListsByIdAndWhouseAreaQuery(id, null), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }

                foreach(var entity in loadOp.Entities) {
                    this.PickingTaskDetailsds.Add(entity);
                }
            }, null);
        }

        private void CreateUI() {
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsStockingUIStrings.DataEditView_Title_PickingTaskDetails, null, () => this.PickingTaskDetailForEditDataGridView);
            //    dcsDetailGridView.SetValue(Grid.ColumnSpanProperty, 4);
            dcsDetailGridView.SetValue(Grid.RowProperty, 4);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            this.gdShow.Children.Add(dcsDetailGridView);

            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.RadComboBoxPartsSalesCategory.ItemsSource = loadOp.Entities;
            }, null);
            //选择库区
            this.PopWarehouseArea.PopupContent = this.SparePartQueryWindow;

        }
        private DcsMultiPopupsQueryWindowBase SparePartQueryWindow {
            get {
                if(this.warehouseAreaWindow == null) {
                    this.warehouseAreaWindow = DI.GetQueryWindow("PickingTaskForPick") as DcsMultiPopupsQueryWindowBase;
                    this.warehouseAreaWindow.SelectionDecided += this.sparePartCodes_SelectionDecided;
                    this.warehouseAreaWindow.Loaded += settlementCostSearchQueryWindow_Loaded;
                }
                return this.warehouseAreaWindow;
            }
        }
        private void settlementCostSearchQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Code", code
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Code", false
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PickingTaskId", typeof(int), FilterOperator.IsEqualTo, id));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
            queryWindow.ExchangeData(null, "ExecuteQuery", null);
        }
        private void setDetail(int[] topLevelWarehouseAreaIds) {
            this.PickingTaskDetailsds.Clear();
            this.DomainContext.Load(this.DomainContext.GetPickingTaskDetailListsByIdAndWhouseAreaQuery(id, topLevelWarehouseAreaIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }

                foreach(var entity in loadOp.Entities) {
                    this.PickingTaskDetailsds.Add(entity);
                }
            }, null);
        }
        private void sparePartCodes_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var spareParts = queryWindow.SelectedEntities.Cast<VirtualWarehouseArea>().ToArray();
            if(spareParts.Count() == 0)
                return;
            var warehouseAreaIds = spareParts.Select(r => r.Id).ToArray();
            var wareCodes = spareParts.Select(r => r.Code).ToArray();

            var warehouseAreaText = "";
            for(var i = 0; i < wareCodes.Count(); i++) {
                if(i == 0) {
                    warehouseAreaText += wareCodes[i];
                } else {
                    warehouseAreaText += "," + wareCodes[i];
                }
            }
            this.PopWarehouseArea.Text = warehouseAreaText;
            this.setDetail(warehouseAreaIds);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.Close();
                // this.warehouseArea = null;
            }

            // SunlightPrinter.ShowPrinter("拣货任务单打印", "ReportPickingTaskNew", null, true, new Tuple<string, string>("pickingTaskId", spareParts.FirstOrDefault().Id.ToString()), new Tuple<string, string>("warehouseAreaIds", billIds.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

        }
        private DataGridViewBase PickingTaskDetailForEditDataGridView {
            get {
                if(this.pickingTaskDetailForEditDataGridView == null) {
                    this.pickingTaskDetailForEditDataGridView = DI.GetDataGridView("PickingTaskDetailForEdit");
                    this.pickingTaskDetailForEditDataGridView.DomainContext = this.DomainContext;
                    this.pickingTaskDetailForEditDataGridView.DataContext = this;
                }
                return this.pickingTaskDetailForEditDataGridView;
            }
        }
        protected override string BusinessName {
            get {
                return PartsStockingUIStrings.ActionPanel_Title_Pick;
            }
        }
        protected override void OnEditSubmitting() {
            DcsUtils.Confirm("是否确认拣货？", () => {
            if(!this.pickingTaskDetailForEditDataGridView.CommitEdit())
                return;
            var dcsDomainContext = new DcsDomainContext();
            var alldetail = new List<string>();
            if(0 == pickingTaskDetails.ToArray().Count()) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Title_NoDatas);
                return;
            } else {
                foreach(var item in pickingTaskDetails) {
                    if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.TraceCode)) {
                        //if(item.TraceProperty.HasValue && string.IsNullOrEmpty(item.TraceCode)) {
                        //    UIHelper.ShowNotification("请填写" + item.SparePartCode + "的追溯码，并以分号隔开");
                        //    return;
                        //}
                        var traceCode = item.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
                        if(traceCode.Length != 0 && item.TraceProperty == (int)DCSTraceProperty.批次追溯) {
                            item.TraceCode = traceCode.First();
                        }
                        foreach(var code in traceCode) {
                            alldetail.Add(code);
                        }
                    }
                }
            }
            if(ShellViewModel.Current.IsBusy) {
                return;
            }
            

            if(alldetail.Count() > 0) {
                this.DomainContext.Load(this.DomainContext.GetAccurateTracesQuery().Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && t.Type == (int)DCSAccurateTraceType.入库 && ( t.OutQty==null || t.InQty != t.OutQty )), LoadBehavior.RefreshCurrent, loadOp1 => {
                    if(loadOp1.HasError) {
                        if(!loadOp1.IsErrorHandled)
                            loadOp1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                        return;
                    }
                    var errorrCode = "";
                    if(loadOp1.Entities.Count() > 0) {
                        foreach(var entity in pickingTaskDetails) {
                          ////  var outQty = 0;
                            if(entity.TraceProperty.HasValue && !string.IsNullOrEmpty(entity.TraceCode)) {
                                var traceCode = entity.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                                if(traceCode.Distinct().ToArray().Length != traceCode.Length) {
                                    UIHelper.ShowNotification("配件" + entity.SparePartCode + " 有重复的出库标签码");
                                    return;
                                }
                                foreach(var code in traceCode) {
                                    if(code.EndsWith("X") || code.EndsWith("J")) {
                                       // var plus = true;
                                        var detailTraceIn = loadOp1.Entities.Where(t => t.BoxCode == code && t.PartId == entity.SparePartId ).ToArray();
                                        if(code.EndsWith("J")) {
                                            detailTraceIn = loadOp1.Entities.Where(t => t.SIHLabelCode == code && t.PartId == entity.SparePartId).ToArray();
                                        }
                                        if(detailTraceIn.Count() == 0 && entity.CompanyType == (int)DcsCompanyType.分公司) {
                                            errorrCode = errorrCode + code + "不是配件" + entity.SparePartCode + "的出库标签,或已出库完成;\n";
                                         //   plus = false;
                                        } else {                                            
                                            foreach(var item  in detailTraceIn){
                                                //if(item.InQty==(item.OutQty??0)) {
                                                //    errorrCode = errorrCode + "配件" + entity.SparePartCode + "的标签码：" + code + "已出库完成;\n";
                                                //  //  plus = false;
                                                //}
                                                if(entity.TraceProperty == (int)DCSTraceProperty.批次追溯 && ((item.OutQty ?? 0) + entity.CourrentPicking > item.InQty) && entity.CompanyType == (int)DcsCompanyType.分公司) {
                                                    errorrCode = errorrCode  + "配件" + entity.SparePartCode + "的出库标签"+code+"未出库数量小于此次拣货数量;\n";
                                                  //  plus = false;
                                                }
                                            }
                                        }
                                        //if(plus) {
                                        //    if(entity.TraceProperty == ((int)DCSTraceProperty.精确追溯)) {
                                        //        outQty = outQty + detailTraceIn.Count();
                                        //    } else {
                                        //        outQty = entity.CourrentPicking.Value;
                                        //    }
                                        //}

                                    } else {
                                        UIHelper.ShowNotification("请输入正确的出库标签码：" + code);
                                        return;
                                    }
                                }
                            }
                            //if(outQty!=0 &&outQty != entity.CourrentPicking) {
                            //    UIHelper.ShowNotification("配件" + entity.SparePartCode + "的出库标签码对应的出库数量不等于拣货数量");
                            //    return;
                            //}
                        }
                    }
                    if(errorrCode != "") {
                        DcsUtils.Confirm(errorrCode + "是否确认拣货？", () => {
                            this.submit(dcsDomainContext, pickingTaskDetails.ToList(), id, remark);
                        });
                        ShellViewModel.Current.IsBusy = false;
                    } else {
                        this.submit(dcsDomainContext, pickingTaskDetails.ToList(), id, remark);
                    } 
                }, null);
            } else {
                this.submit(dcsDomainContext, pickingTaskDetails.ToList(), id, remark);
            }
            ShellViewModel.Current.IsBusy = false;
             });
        }

        public void submit(DcsDomainContext dcsDomainContext, List<VirtualPickingTaskDetail> pickingTaskDetails, int id, string remark) {
            ShellViewModel.Current.IsBusy = true;
            dcsDomainContext.拣货(pickingTaskDetails.ToArray(), id, remark, invokeOp => {
                ShellViewModel.Current.IsBusy = false;
                if(invokeOp.HasError) {
                    if(!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    if(0 == invokeOp.ValidationErrors.Count()) {
                        base.OnEditSubmitting();
                        return;
                    }
                    var error = invokeOp.ValidationErrors.First();
                    if(error != null)
                        UIHelper.ShowAlertMessage(invokeOp.ValidationErrors.First().ErrorMessage);
                    else
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    return;
                }
                base.OnEditSubmitting();
            }, null);
        }
        private void RadButton_Click(object sender, RoutedEventArgs e) {
            DcsUtils.Confirm(PartsStockingUIStrings.DataEditView_Title_SureFinishPicking, () => {
                if(!this.pickingTaskDetailForEditDataGridView.CommitEdit())
                    return;
                var dcsDomainContext = new DcsDomainContext();
                var alldetail = new List<string>();
                var details = pickingTaskDetails.ToArray();
                foreach(var item in details) {
                    if(null == item.CourrentPicking) {
                        item.CourrentPicking = 0;
                    }
                    if(null == item.PickingQty) {
                        item.PickingQty = 0;
                    }
                    if(item.PlanQty > (item.PickingQty + item.CourrentPicking) && null == item.ShortPickingReason) {
                        UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataEditView_Title_PleaseChooseShortPickingReason, item.SparePartCode));
                        return;
                    }
                    if(item.TraceProperty.HasValue && !string.IsNullOrEmpty(item.TraceCode)) {
                        //if(item.TraceProperty.HasValue && string.IsNullOrEmpty(item.TraceCode)) {
                        //    UIHelper.ShowNotification("请填写" + item.SparePartCode + "的追溯码，并以分号隔开");
                        //    return;
                        //}
                        var traceCode = item.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
                        if(traceCode.Length != 0 && item.TraceProperty == (int)DCSTraceProperty.批次追溯) {
                            item.TraceCode = traceCode.First();
                        }
                        foreach(var code in traceCode) {
                            alldetail.Add(code);
                        }

                    }
                }
                if(ShellViewModel.Current.IsBusy) {
                    return;
                }
               
                if(alldetail.Count() > 0) {
                    this.DomainContext.Load(this.DomainContext.GetAccurateTracesQuery().Where(t => t.Status == (int)DCSAccurateTraceStatus.有效 && t.CompanyId == BaseApp.Current.CurrentUserData.EnterpriseId && t.Type == (int)DCSAccurateTraceType.入库 && (t.OutQty == null || t.InQty != t.OutQty)), LoadBehavior.RefreshCurrent, loadOp1 => {
                        if(loadOp1.HasError) {
                            if(!loadOp1.IsErrorHandled)
                                loadOp1.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                            return;
                        }
                        var errorrCode = "";
                        if(loadOp1.Entities.Count() > 0) {
                            foreach(var entity in pickingTaskDetails) {
                              //  var outQty = 0;
                                if(entity.TraceProperty.HasValue && !string.IsNullOrEmpty(entity.TraceCode)) {
                                    var traceCode = entity.TraceCode.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                                    if(traceCode.Distinct().ToArray().Length != traceCode.Length) {
                                        UIHelper.ShowNotification("配件" + entity.SparePartCode + " 有重复的出库标签码");
                                        return;
                                    }
                                    foreach(var code in traceCode) {
                                        if(code.EndsWith("X") || code.EndsWith("J")) {
                                          //  var plus = true;
                                            var detailTraceIn = loadOp1.Entities.Where(t => t.BoxCode == code && t.PartId == entity.SparePartId).ToArray();
                                            if(code.EndsWith("J")) {
                                                detailTraceIn = loadOp1.Entities.Where(t => t.SIHLabelCode == code && t.PartId == entity.SparePartId).ToArray();
                                            }
                                            if(detailTraceIn.Count() == 0 && entity.CompanyType == (int)DcsCompanyType.分公司) {
                                                errorrCode = errorrCode + code + "不是配件" + entity.SparePartCode + "的出库标签,或已出库完成;\n";
                                             //   plus = false;
                                            } else {
                                                foreach(var item in detailTraceIn) {
                                                    //if(item.InQty == (item.OutQty ?? 0)) {
                                                    //    errorrCode = errorrCode + "配件" + entity.SparePartCode + "的标签码：" + code + "已出库完成;\n";
                                                    ////    plus = false;
                                                    //}
                                                    if(entity.TraceProperty == (int)DCSTraceProperty.批次追溯 && ((item.OutQty ?? 0) + entity.CourrentPicking > item.InQty) && entity.CompanyType == (int)DcsCompanyType.分公司) {
                                                        errorrCode = errorrCode + "配件" + entity.SparePartCode + "的出库标签" + code + "未出库数量小于此次拣货数量;\n";
                                                      //  plus = false;
                                                    }
                                                }
                                            }
                                            //if(plus) {
                                            //    if(entity.TraceProperty == ((int)DCSTraceProperty.精确追溯)) {
                                            //        outQty = outQty + detailTraceIn.Count();
                                            //    } else {
                                            //        outQty = entity.CourrentPicking.Value;
                                            //    }
                                            //}

                                        } else {
                                            UIHelper.ShowNotification("请输入正确的出库标签码：" + code);
                                            return;
                                        }
                                    }
                                }
                                //if(outQty != 0 && outQty != entity.CourrentPicking) {
                                //    UIHelper.ShowNotification("配件" + entity.SparePartCode + "的出库标签码对应的出库数量不等于拣货数量");
                                //    return;
                                //}
                            }
                        }
                        if(errorrCode != "") {
                            DcsUtils.Confirm(errorrCode+"是否确认拣货？", () => {

                                this.完成拣货(dcsDomainContext, details.ToList(), id, remark);
                            });
                            ShellViewModel.Current.IsBusy = false;
                        } else
                            this.完成拣货(dcsDomainContext, details.ToList(), id, remark);
                    }, null);
                } else {
                    this.完成拣货(dcsDomainContext, details.ToList(), id, remark);
                }

            });
            ShellViewModel.Current.IsBusy = false;
        }
        public void 完成拣货(DcsDomainContext dcsDomainContext, List<VirtualPickingTaskDetail> pickingTaskDetails, int id, string remark) {
            ShellViewModel.Current.IsBusy = true;
            dcsDomainContext.完成拣货(pickingTaskDetails.ToArray(), id, remark, invokeOp => {
                ShellViewModel.Current.IsBusy = false;
                if(invokeOp.HasError) {
                    if(!invokeOp.IsErrorHandled)
                        invokeOp.MarkErrorAsHandled();
                    if(0 == invokeOp.ValidationErrors.Count()) {
                        base.OnEditSubmitting();
                        return;
                    }
                    var error = invokeOp.ValidationErrors.First();
                    if(error != null)
                        UIHelper.ShowAlertMessage(invokeOp.ValidationErrors.First().ErrorMessage);
                    else
                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                    return;
                }
                base.OnEditSubmitting();
            }, null);
        }
        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public string Code {
            get {
                return this.code;
            }
            set {
                this.code = value;
                this.OnPropertyChanged("Code");
            }
        }
        public int PartsSalesCategoryId {
            get {
                return this.partsSalesCategoryId;
            }
            set {
                this.partsSalesCategoryId = value;
                this.OnPropertyChanged("PartsSalesCategoryId");
            }
        }
        public string OrderTypeName {
            get {
                return this.orderTypeName;
            }
            set {
                this.orderTypeName = value;
                this.OnPropertyChanged("OrderTypeName");
            }
        }
        public string WarehouseName {
            get {
                return this.warehouseName;
            }
            set {
                this.warehouseName = value;
                this.OnPropertyChanged("WarehouseName");
            }
        }
        public string CounterpartCompanyCode {
            get {
                return this.counterpartCompanyCode;
            }
            set {
                this.counterpartCompanyCode = value;
                this.OnPropertyChanged("CounterpartCompanyCode");
            }
        }
        public string CounterpartCompanyName {
            get {
                return this.counterpartCompanyName;
            }
            set {
                this.counterpartCompanyName = value;
                this.OnPropertyChanged("CounterpartCompanyName");
            }
        }
        public string Remark {
            get {
                return this.remark;
            }
            set {
                this.remark = value;
                this.OnPropertyChanged("Remark");
            }
        }
        public ObservableCollection<VirtualPickingTaskDetail> PickingTaskDetailsds {
            get {
                return this.pickingTaskDetails ?? (this.pickingTaskDetails = new ObservableCollection<VirtualPickingTaskDetail>());
            }
            set {
                this.pickingTaskDetails = value;
                this.OnPropertyChanged("PickingTaskDetailsds");
            }
        }

        private void RadButton_Click_1(object sender, RoutedEventArgs e) {
            this.PopWarehouseArea.Text = "";
            this.setDetail(null);
        }
    }
}
