﻿using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit
{
    public partial class PrintLabelForPartsInboundCheckBillEnDataEditView 
    {
        public PrintLabelForPartsInboundCheckBillEnDataEditView()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private void Print()
        {
            this.printLabelForPartsInboundCheckBillDataGridView.CommitEdit();
            ObservableCollection<VirtualPartsInboundCheckBillForPrint> print = new ObservableCollection<VirtualPartsInboundCheckBillForPrint>();
            if (this.PrintLabelForPartsInboundCheckBillDataGridView.SelectedEntities != null && this.PrintLabelForPartsInboundCheckBillDataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBillForPrint>().ToArray().Count() > 0)
            {
                foreach (var item in this.PrintLabelForPartsInboundCheckBillDataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBillForPrint>().ToArray())
                {
                    print.Add(item);
                }
            }
            else
            {
                print = PartsInboundPlanDetails;
            }

            var partsQuantity = print.Select(detail => detail.Id).ToList();
            var billId = print.Select(detail => detail.PartsInboundCheckBillId).Distinct().ToList();
            var details = "";
            var billIds = "";
            for (var i = 0; i < partsQuantity.Count(); i++)
            {
                details += partsQuantity[i] + ",";
            }
            for (var i = 0; i < billId.Count(); i++)
            {
                billIds += billId[i] + ",";
            }

            SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataEditView_Text_BigLableChPrint, "ReportPartsInboundCheckLabelForEn", null, true, new Tuple<string, string>("PararPartsTags", details.ToString()), new Tuple<string, string>("PartsInboundCheckBillId", billIds.ToString()));

            //BasePrintWindow printWindow1 = new PartsInBoundCheckBillForEnPrintWindow
            //{
            //    Header = "箱标英文版本打印",//带标准
            //    PartsInboundPlanDetails = print,
            //};
            //printWindow1.ShowDialog();

        }
        private void CreateUI()
        {
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsStockingUIStrings.DataEditView_Text_CheckDetailsInfo, null, () => this.PrintLabelForPartsInboundCheckBillDataGridView);
            dcsDetailGridView.SetValue(Grid.RowProperty, 1);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            this.LayoutRoot.Children.Add(dcsDetailGridView);
            this.Title = PartsStockingUIStrings.DataEditView_Text_PartDetailInfo;
            this.RegisterButton(new ButtonItem
            {
                Title = PartsStockingUIStrings.DataEditView_Text_BigLableChPrint,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/print.png", UriKind.Relative),
                Command = new DelegateCommand(this.Print)
            }, true);
            this.HideSaveButton();
            this.HideCancelButton();
        }
        public override void SetObjectToEditById(object id)
        {
            if (this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int[])id));
            else
                this.LoadEntityToEdit((int[])id);
        }

        private void LoadEntityToEdit(int[] id)
        {
            this.PartsInboundPlanDetails.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsInboundCheckBillForPrintQuery(id), LoadBehavior.RefreshCurrent, loadOps =>
            {
                if (loadOps.HasError)
                {
                    if (!loadOps.IsErrorHandled)
                        loadOps.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOps);
                    return;
                }
                if (!loadOps.Entities.Any())
                    return;
                foreach (var partsInboundPlanDetail in loadOps.Entities)
                {
                    this.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                }
            }, null);
        }
        private DataGridViewBase printLabelForPartsInboundCheckBillDataGridView;

        private DataGridViewBase PrintLabelForPartsInboundCheckBillDataGridView
        {
            get
            {
                if (this.printLabelForPartsInboundCheckBillDataGridView == null)
                {
                    this.printLabelForPartsInboundCheckBillDataGridView = DI.GetDataGridView("PrintLabelForPartsInboundCheckBillForQuery");
                    this.printLabelForPartsInboundCheckBillDataGridView.DomainContext = this.DomainContext;
                    this.printLabelForPartsInboundCheckBillDataGridView.DataContext = this;
                }
                return this.printLabelForPartsInboundCheckBillDataGridView;
            }
        }
        private ObservableCollection<VirtualPartsInboundCheckBillForPrint> partsInboundPlanDetails;

        public ObservableCollection<VirtualPartsInboundCheckBillForPrint> PartsInboundPlanDetails
        {
            get
            {
                return partsInboundPlanDetails ?? (this.partsInboundPlanDetails = new ObservableCollection<VirtualPartsInboundCheckBillForPrint>());
            }
        }
    }
}