﻿using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PrintLabelForPartsInboundCheckDataEditView {
        public Dictionary<int, int> idss;

        public PrintLabelForPartsInboundCheckDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);

        }

        private void Print() {
            this.PrintLabelForPartsInboundCheckBillDataGridView.CommitEdit();
            //BasePrintWindow printWindow1 = new SparePartLabelPrintWindow1 {
            //    Header = "配件标签打印",
            //    PartsInboundPlanDetails = PartsInboundPlanDetails,
            //};
            BasePrintWindow printWindow1 = new StandardPartsTagPrinttingForMorePrintWindow {
                Header =PartsStockingUIStrings.ActionPanel_Title_General,//带标准
                PartsInboundPlanDetails = PartsInboundPlanDetails,
            };
            printWindow1.ShowDialog();

        }

        private void CreateUI() {
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsStockingUIStrings.DataEditView_Text_CheckDetailsInfo, null, () => this.PrintLabelForPartsInboundCheckBillDataGridView);
            dcsDetailGridView.SetValue(Grid.RowProperty, 0);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            this.LayoutRoot.Children.Add(dcsDetailGridView);
            this.Title = PartsStockingUIStrings.DataEditView_Text_PartDetailInfo;
            this.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.ActionPanel_Title_General,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/print.png", UriKind.Relative),
                Command = new DelegateCommand(this.Print)
            }, true);
            this.HideSaveButton();
            this.HideCancelButton();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            const double tempNum = 1.0;
            this.PartsInboundPlanDetails.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsInboundPlanDetailByIdQuery(id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any())
                    return;
                foreach(var partsInboundPlanDetail in loadOp.Entities) {
                    if(idss.Any(r => r.Key == partsInboundPlanDetail.SparePartId)) {
                        partsInboundPlanDetail.PrintNumber = Convert.ToInt32(Math.Ceiling((double)(idss[partsInboundPlanDetail.SparePartId]) / (double)(partsInboundPlanDetail.SparePart.MInPackingAmount ?? tempNum)));
                        this.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                    }
                }
            }, null);
        }

        private DataGridViewBase printLabelForPartsInboundCheckBillDataGridView;

        private DataGridViewBase PrintLabelForPartsInboundCheckBillDataGridView {
            get {
                if(this.printLabelForPartsInboundCheckBillDataGridView == null) {
                    this.printLabelForPartsInboundCheckBillDataGridView = DI.GetDataGridView("PrintLabelForPartsInboundCheckBill");
                    this.printLabelForPartsInboundCheckBillDataGridView.DomainContext = this.DomainContext;
                    this.printLabelForPartsInboundCheckBillDataGridView.DataContext = this;
                }
                return this.printLabelForPartsInboundCheckBillDataGridView;
            }
        }

        private ObservableCollection<PartsInboundPlanDetail> partsInboundPlanDetails;

        public ObservableCollection<PartsInboundPlanDetail> PartsInboundPlanDetails {
            get {
                return partsInboundPlanDetails ?? (this.partsInboundPlanDetails = new ObservableCollection<PartsInboundPlanDetail>());
            }
        }
    }
}

