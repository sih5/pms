﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PrintLabelPartsInboundCheckBillNoPriceDataEditView {

        public PrintLabelPartsInboundCheckBillNoPriceDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);

        }

        private void Print() {
            this.PrintLabelForPartsInboundDetailNoPriceDataGridView.CommitEdit();
            BasePrintWindow printWindow1 = new SparePartLabelPrintWindow1 {
                Header = PartsStockingUIStrings.ActionPanel_Title_General,
                PartsInboundPlanDetails = PartsInboundPlanDetails,
            };
            printWindow1.ShowDialog();

        }

        private void CreateUI() {
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register(PartsStockingUIStrings.DataEditView_Text_InboundPlanDetails, null, () => this.PrintLabelForPartsInboundDetailNoPriceDataGridView);
            dcsDetailGridView.SetValue(Grid.RowProperty, 0);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            this.LayoutRoot.Children.Add(dcsDetailGridView);
            this.Title = PartsStockingUIStrings.DataEditView_Text_PartDetailInfo;
            this.RegisterButton(new ButtonItem {
                Title = PartsStockingUIStrings.DataEditView_Text_PrintSparePartLable,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/print.png", UriKind.Relative),
                Command = new DelegateCommand(this.Print)
            }, true);
            this.HideSaveButton();
            this.HideCancelButton();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int[])id));
            else
                this.LoadEntityToEdit((int[])id);
        }

        private void LoadEntityToEdit(int[] id) {
            this.PartsInboundPlanDetails.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsInboundPlanDetailByPartsInboundPlanIdQuery(id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any())
                    return;
                foreach(var partsInboundPlanDetail in loadOp.Entities) {
                    partsInboundPlanDetail.PrintNumber = partsInboundPlanDetail.PlannedAmount;
                    this.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                }
            }, null);
        }

        private DataGridViewBase printLabelForPartsInboundDetailNoPriceDataGridView;

        private DataGridViewBase PrintLabelForPartsInboundDetailNoPriceDataGridView {
            get {
                if(this.printLabelForPartsInboundDetailNoPriceDataGridView == null) {
                    this.printLabelForPartsInboundDetailNoPriceDataGridView = DI.GetDataGridView("PrintLabelForPartsInboundPlanNoPrice");
                    this.printLabelForPartsInboundDetailNoPriceDataGridView.DomainContext = this.DomainContext;
                    this.printLabelForPartsInboundDetailNoPriceDataGridView.DataContext = this;
                }
                return this.printLabelForPartsInboundDetailNoPriceDataGridView;
            }
        }

        private ObservableCollection<PartsInboundPlanDetail> partsInboundPlanDetails;

        public ObservableCollection<PartsInboundPlanDetail> PartsInboundPlanDetails {
            get {
                return partsInboundPlanDetails ?? (this.partsInboundPlanDetails = new ObservableCollection<PartsInboundPlanDetail>());
            }
        }
    }
}
