﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class PrintOMWmsLabelForPartsInboundPlanDataEditView {
        public PrintOMWmsLabelForPartsInboundPlanDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        private void Print() {
            this.PrintWmsLabelForPartsInboundPlanDataGridView.CommitEdit();
            BasePrintWindow printWindow = new PartsInboundPlanOrderPrintWmsOMLabelPrintWindow {
                Header = "配件标签打印(欧曼)",
                PartsInboundPlanDetails = PartsInboundPlanDetails
            };
            printWindow.ShowDialog();

        }

        private void PrintForOnePage() {
            this.PrintWmsLabelForPartsInboundPlanDataGridView.CommitEdit();
            BasePrintWindow printWindow = new PartsInboundPlanOrderPrintWmsLabelOMForOnePrintWindow {
                Header = "配件标签打印(欧曼)",
                PartsInboundPlanDetails = PartsInboundPlanDetails
            };
            printWindow.ShowDialog();

        }

        private void CreateUI() {
            var dcsDetailGridView = new DcsDetailDataEditView();
            dcsDetailGridView.Register("配件入库计划清单", null, () => this.PrintWmsLabelForPartsInboundPlanDataGridView);
            dcsDetailGridView.SetValue(Grid.RowProperty, 0);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.InsertButton);
            dcsDetailGridView.UnregisterButton(dcsDetailGridView.DeleteButton);
            this.LayoutRoot.Children.Add(dcsDetailGridView);
            this.Title = "配件详细信息";
            this.RegisterButton(new ButtonItem {
                Title = "每种一张",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/print.png", UriKind.Relative),
                Command = new DelegateCommand(this.PrintForOnePage)
            }, true);
            this.RegisterButton(new ButtonItem {
                Title = "按数打印",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/print.png", UriKind.Relative),
                Command = new DelegateCommand(this.Print)
            }, true);
            this.HideSaveButton();
            this.HideCancelButton();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int[])id));
            else
                this.LoadEntityToEdit((int[])id);
        }
        private void LoadEntityToEdit(int[] id) {
            this.PartsInboundPlanDetails.Clear();
            this.DomainContext.Load(this.DomainContext.GetPartsInboundPlanDetailsByIdQuery(id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any())
                    return;
                foreach(var partsInboundPlanDetail in loadOp.Entities) {
                    partsInboundPlanDetail.PrintNumber = partsInboundPlanDetail.PlannedAmount;
                    this.PartsInboundPlanDetails.Add(partsInboundPlanDetail);
                }
            }, null);
        }

        private DataGridViewBase printWmsLabelForPartsInboundPlanDataGridView;

        private DataGridViewBase PrintWmsLabelForPartsInboundPlanDataGridView {
            get {
                if(this.printWmsLabelForPartsInboundPlanDataGridView == null) {
                    this.printWmsLabelForPartsInboundPlanDataGridView = DI.GetDataGridView("PrintWmsLabelForPartsInboundPlan");
                    this.printWmsLabelForPartsInboundPlanDataGridView.DomainContext = this.DomainContext;
                    this.printWmsLabelForPartsInboundPlanDataGridView.DataContext = this;
                }
                return this.printWmsLabelForPartsInboundPlanDataGridView;
            }
        }

        private ObservableCollection<PartsInboundPlanDetail> partsInboundPlanDetails;

        public ObservableCollection<PartsInboundPlanDetail> PartsInboundPlanDetails {
            get {
                return partsInboundPlanDetails ?? (this.partsInboundPlanDetails = new ObservableCollection<PartsInboundPlanDetail>());
            }
        }
    }
}