﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class WarehouseAreaDataEditView : INotifyPropertyChanged {
        private DataGridViewBase warehouseAreaManagerForEditDataGridView;
        private ObservableCollection<KeyValuePair> areaKinds;
        private bool canEditAreaCategory;
        private bool canEditAreaKind;
        private KeyValueManager keyValueManager;
        public event PropertyChangedEventHandler PropertyChanged;
        public new event EventHandler EditSubmitted;
        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }
        // 
        public WarehouseArea ParintWarehouseArea {
            set {
                var warehouseArea = this.DataContext as WarehouseArea;
                if(warehouseArea == null)
                    return;
                this.CanEditAreaCategory = this.EditState == DataEditState.New && !warehouseArea.TopLevelWarehouseAreaId.HasValue;
                if(this.EditState == DataEditState.New) {
                    this.CanEditAreaKind = true;
                    if(value.AreaKind == (int)DcsAreaKind.仓库) {
                        this.AreaKinds = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key == (int)DcsAreaKind.库区));
                        warehouseArea.WarehouseAreaCategory = new WarehouseAreaCategory();
                    } else {
                        this.AreaKinds = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key != (int)DcsAreaKind.仓库));
                        warehouseArea.WarehouseAreaCategory = value.WarehouseAreaCategory;
                    }
                } else {
                    //库区库位类型 顶层库区不允许变更为库位（库区库位类型 不可编辑）
                    this.CanEditAreaKind = warehouseArea.TopLevelWarehouseAreaId.HasValue;
                    this.AreaKinds = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key != (int)DcsAreaKind.仓库));
                    if(warehouseArea.AreaKind == (int)DcsAreaKind.库位)
                        warehouseArea.WarehouseAreaCategory = value.WarehouseAreaCategory;
                }
            }
        }

        private readonly string[] kvNames = {
            "Area_Kind", "Area_Category"
        };

        public WarehouseAreaDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
            this.KeyValueManager.LoadData();
            this.Loaded += WarehouseAreaDataEditView_Loaded;
        }

        private void WarehouseAreaDataEditView_Loaded(object sender, RoutedEventArgs e) {
            var warehouseArea = this.DataContext as WarehouseArea;
            if(warehouseArea == null)
                return;
            //根据父节点Id获取节点
            this.DomainContext.Load(DomainContext.GetWarehouseAreaWithWarehouseAreaCategoryQuery((int)warehouseArea.ParentId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var parentWarehouseArea = loadOp.Entities.SingleOrDefault();
                if(parentWarehouseArea != null) {
                    if(this.EditState == DataEditState.New) {
                        //新增
                        this.CanEditAreaKind = true;
                        if(parentWarehouseArea.AreaKind == (int)DcsAreaKind.仓库) {
                            this.AreaKinds = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key == (int)DcsAreaKind.库区));
                            warehouseArea.WarehouseAreaCategory = new WarehouseAreaCategory();
                        } else {
                            this.AreaKinds = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key != (int)DcsAreaKind.仓库));
                            warehouseArea.WarehouseAreaCategory = parentWarehouseArea.WarehouseAreaCategory;
                            CategoryId = parentWarehouseArea.WarehouseAreaCategory.Category;
                        }
                    } else {
                        //修改
                        this.CanEditAreaKind = warehouseArea.TopLevelWarehouseAreaId.HasValue;
                        this.AreaKinds = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key != (int)DcsAreaKind.仓库));
                        if(warehouseArea.AreaKind == (int)DcsAreaKind.库位)
                            warehouseArea.WarehouseAreaCategory = parentWarehouseArea.WarehouseAreaCategory;
                    }
                    this.CanEditAreaCategory = this.EditState == DataEditState.New && !warehouseArea.TopLevelWarehouseAreaId.HasValue;
                }
            }, null);
        }
        private DataGridViewBase WarehouseAreaManagerForEditDataGridView {
            get {
                if(this.warehouseAreaManagerForEditDataGridView == null) {
                    this.warehouseAreaManagerForEditDataGridView = DI.GetDataGridView("WarehouseAreaManagerForEdit");
                    this.warehouseAreaManagerForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.warehouseAreaManagerForEditDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetWarehouseAreasWithAreaManagersAndDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    if(entity.Warehouse == null)
                        return;
                    entity.WarehouseName = entity.Warehouse.Name;
                    if(entity.WarehouseAreaCategory != null) {
                        CategoryId = entity.WarehouseAreaCategory.Category;
                    }
                    this.SetObjectToEdit(entity);
                    setEditContext();
                    //this.WarehouseAreaDataTreeView.RefreshDataTree();
                }
            }, null);
        }

        private void setEditContext() {
            var warehouseArea = this.DataContext as WarehouseArea;
            if(warehouseArea == null)
                return;
            //根据父节点Id获取节点
            this.DomainContext.Load(DomainContext.GetWarehouseAreaWithWarehouseAreaCategoryQuery((int)warehouseArea.ParentId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var parentWarehouseArea = loadOp.Entities.SingleOrDefault();
                if(parentWarehouseArea != null) {
                    //修改
                    this.CanEditAreaKind = warehouseArea.TopLevelWarehouseAreaId.HasValue;
                    this.AreaKinds = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key != (int)DcsAreaKind.仓库));
                    if(warehouseArea.AreaKind == (int)DcsAreaKind.库位)
                        warehouseArea.WarehouseAreaCategory = parentWarehouseArea.WarehouseAreaCategory;
                    this.CanEditAreaCategory = this.EditState == DataEditState.New && !warehouseArea.TopLevelWarehouseAreaId.HasValue;
                }
            }, null);
        }

        //private void WarehouseAreaDataTreeView_OnTreeViewDataLoaded() {
        //    var warehouseArea = this.DataContext as WarehouseArea;
        //    if(warehouseArea == null)
        //        return;
        //    //库区用途 只有顶层库区新增可编辑
        //    this.CanEditAreaCategory = this.EditState == DataEditState.New && !warehouseArea.TopLevelWarehouseAreaId.HasValue;
        //    if(EditState == DataEditState.New)
        //        if(warehouseArea.ParentId.HasValue)
        //            this.WarehouseAreaDataTreeView.SelectedItemId = warehouseArea.ParentId.Value;
        //        else
        //            return;
        //    else
        //        this.WarehouseAreaDataTreeView.SelectedItemId = warehouseArea.Id;
        //    var selectedItem = this.WarehouseAreaDataTreeView.SelectedItem as RadTreeViewItem;
        //    if(selectedItem == null || selectedItem.Tag == null || !(selectedItem.Tag is WarehouseArea))
        //        return;
        //    warehouseArea.Warehouse = ((WarehouseArea)selectedItem.Tag).Warehouse;
        //    if(!warehouseArea.ParentId.HasValue)
        //        return;
        //    var treeItem = this.WarehouseAreaDataTreeView.GetObjectById(warehouseArea.ParentId.Value) as RadTreeViewItem;
        //    if(treeItem == null)
        //        return;
        //    var parentWarehouseArea = treeItem.Tag as WarehouseArea;
        //    if(parentWarehouseArea == null)
        //        return;
        //    if(this.EditState == DataEditState.New) {
        //        this.CanEditAreaKind = true;
        //        if(parentWarehouseArea.AreaKind == (int)DcsAreaKind.仓库) {
        //            this.AreaKinds = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key == (int)DcsAreaKind.库区));
        //            warehouseArea.WarehouseAreaCategory = new WarehouseAreaCategory();
        //        } else {
        //            this.AreaKinds = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key != (int)DcsAreaKind.仓库));
        //            warehouseArea.WarehouseAreaCategory = parentWarehouseArea.WarehouseAreaCategory;
        //        }
        //    } else {
        //        //库区库位类型 顶层库区不允许变更为库位（库区库位类型 不可编辑）
        //        this.CanEditAreaKind = warehouseArea.TopLevelWarehouseAreaId.HasValue;
        //        this.AreaKinds = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key != (int)DcsAreaKind.仓库));
        //        if(warehouseArea.AreaKind == (int)DcsAreaKind.库位)
        //            warehouseArea.WarehouseAreaCategory = parentWarehouseArea.WarehouseAreaCategory;
        //    }
        //}

        private void CreateUI() {
            //this.WarehouseAreaDataTreeView.SetValue(Grid.RowSpanProperty, 2);
            //this.WarehouseAreaDataTreeView.SetValue(Grid.RowProperty, 0);
            //this.WarehouseAreaDataTreeView.SetValue(IsEnabledProperty, false);
            //this.Root.Children.Add(this.WarehouseAreaDataTreeView);
            //var verticalLine = this.CreateVerticalLine(1);
            //verticalLine.SetValue(Grid.ColumnProperty, 1);
            //verticalLine.SetValue(Grid.RowSpanProperty, 2);
            //this.Root.Children.Add(verticalLine);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(WarehouseArea), "WarehouseAreaManagers"), null, () => this.WarehouseAreaManagerForEditDataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(VerticalAlignmentProperty, VerticalAlignment.Stretch);
            this.Root.Children.Add(detailDataEditView);

        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_WarehouseArea;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.WarehouseAreaManagerForEditDataGridView.CommitEdit())
                return;
            var warehouseArea = this.DataContext as WarehouseArea;
            if(warehouseArea == null)
                return;

            warehouseArea.ValidationErrors.Clear();
            if(warehouseArea.AreaKind <= 0)
                warehouseArea.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_Warehouse_AreaKindIsNull, new[] {
                    "AreaKind"
                }));
            if(warehouseArea.WarehouseAreaCategory == null || CategoryId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_Warehouse_AreaCategoryIsNull);
                return;
            }
            warehouseArea.WarehouseAreaCategory.Category = CategoryId;
            if(warehouseArea.HasValidationErrors)
                return;
            ((IEditableObject)warehouseArea).EndEdit();
            try {
                if(EditState == DataEditState.Edit) {
                    if(warehouseArea.Can修改库区库位)
                        warehouseArea.修改库区库位();
                    foreach(var manager in warehouseArea.WarehouseAreaManagers.Where(entity => entity.EntityState == EntityState.New)) {
                        if(manager.Can生成库区负责人)
                            manager.生成库区负责人();
                    }
                    base.OnEditSubmitting();
                    this.NotifyEditSubmitted();
                    this.OnCustomEditSubmitted();
                }
                if(EditState == DataEditState.New) {
                    var managers = warehouseArea.WarehouseAreaManagers.Where(entity => entity.EntityState == EntityState.New).ToArray();
                    var areaCategory = warehouseArea.WarehouseAreaCategory;
                    var parentWarehouseArea = warehouseArea.ParentWarehouseArea;
                    this.DomainContext.新增库区库位(warehouseArea, managers, areaCategory, parentWarehouseArea, invokeOp =>
                    {
                        if (invokeOp.HasError)
                        {
                            if (!invokeOp.IsErrorHandled)
                                invokeOp.MarkErrorAsHandled();
                            var error = invokeOp.ValidationErrors.FirstOrDefault();
                            if (error != null)
                                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                            else
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                            return;
                        }
                        this.NotifyEditSubmitted();
                        this.OnCustomEditSubmitted();
                    }, null);
                }

            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public bool CanEditAreaCategory {
            get {
                return this.canEditAreaCategory;
            }
            set {
                this.canEditAreaCategory = value;
                this.OnPropertyChanged("CanEditAreaCategory");
            }
        }

        public bool CanEditAreaKind {
            get {
                return this.canEditAreaKind;
            }
            set {
                this.canEditAreaKind = value;
                this.OnPropertyChanged("CanEditAreaKind");
            }
        }

        public ObservableCollection<KeyValuePair> AreaKinds {
            get {
                return this.areaKinds ?? (this.areaKinds = new ObservableCollection<KeyValuePair>());
            }
            set {
                this.areaKinds = value;
                this.OnPropertyChanged("AreaKinds");
            }
        }

        private int categoryId;
        public int CategoryId {
            get {
                return this.categoryId;
            }
            set {
                this.categoryId = value;
                this.OnPropertyChanged("CategoryId");
            }
        }

        public object AreaCategories {
            get {
                return this.KeyValueManager[kvNames[1]];
            }
        }

        protected override void Reset() {
            var warehouseArea = this.DataContext as WarehouseArea;
            if(warehouseArea == null)
                return;
            if(this.DomainContext.WarehouseAreas.Contains(warehouseArea))
                this.DomainContext.WarehouseAreas.Detach(warehouseArea);
        }

        public void OnCustomEditSubmitted() {
            var warehouseArea = this.DataContext as WarehouseArea;
            if(warehouseArea == null)
                return;
            if(this.DomainContext.WarehouseAreas.Contains(warehouseArea))
                this.DomainContext.WarehouseAreas.Detach(warehouseArea);
        }
    }
}

