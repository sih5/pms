﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class WarehouseAreaForStorageDataEditView : INotifyPropertyChanged {
        public WarehouseAreaForStorageDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }
        private readonly string[] kvNames = {
            "Area_Kind", "Area_Category"
        };

        protected override string BusinessName {
            get {
                return PartsStockingUIStrings.QueryPanel_QueryItem_WarehouseAreaCode;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        public object AreaKinds {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }

        }
        private int categoryId;
        public int CategoryId {
            get {
                return this.categoryId;
            }
            set {
                this.categoryId = value;
                this.OnPropertyChanged("CategoryId");
            }
        }
        public object AreaCategories {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetWarehouseAreasWithAreaManagersAndDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    entity.WarehouseName = entity.Warehouse.Name;
                    CategoryId = entity.WarehouseAreaCategory.Category;
                    this.SetObjectToEdit(entity);

                }
            }, null);
        }
        protected override void Reset() {
            var warehouseArea = this.DataContext as WarehouseArea;
            if(warehouseArea == null)
                return;
            if(this.DomainContext.WarehouseAreas.Contains(warehouseArea))
                this.DomainContext.WarehouseAreas.Detach(warehouseArea);
        }
        protected override void OnEditSubmitting() {

            var warehouseArea = this.DataContext as WarehouseArea;
            if(warehouseArea == null)
                return;

            warehouseArea.ValidationErrors.Clear();
            if(warehouseArea.AreaKind <= 0)
                warehouseArea.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_Warehouse_AreaKindIsNull, new[] {
                    "AreaKind"
                }));
            if(warehouseArea.WarehouseAreaCategory == null || CategoryId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_Warehouse_AreaCategoryIsNull);
                return;
            }
            warehouseArea.WarehouseAreaCategory.Category = CategoryId;
            if(warehouseArea.HasValidationErrors)
                return;
            ((IEditableObject)warehouseArea).EndEdit();
            try {
                if(EditState == DataEditState.Edit) {
                    if(warehouseArea.Can修改库区库位)
                        warehouseArea.修改库区库位();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
    }
}
