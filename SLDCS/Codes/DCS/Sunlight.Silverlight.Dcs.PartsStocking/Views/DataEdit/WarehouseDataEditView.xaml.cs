﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit {
    public partial class WarehouseDataEditView {
        private DataGridViewBase warehouseOperatorForEditDataGridView;

        private DataGridViewBase WarehouseOperatorForEditDataGridView {
            get {
                if(this.warehouseOperatorForEditDataGridView == null) {
                    this.warehouseOperatorForEditDataGridView = DI.GetDataGridView("WarehouseOperatorForEdit");
                    this.warehouseOperatorForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.warehouseOperatorForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("Warehouse"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(Warehouse), "WarehouseOperators"), null, () => this.WarehouseOperatorForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            this.DataContext = null;
            this.DomainContext.Load(this.DomainContext.GetWarehousesWithDetailsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.WarehouseOperatorForEditDataGridView.CommitEdit())
                return;
            var warehouse = this.DataContext as Warehouse;
            if(warehouse == null)
                return;
            warehouse.ValidationErrors.Clear();
            foreach(var warehouseOperator in warehouse.WarehouseOperators)
                warehouseOperator.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(warehouse.Code))
                warehouse.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_Warehouse_CodeIsNull, new[] {
                    "Code"
                }));
            if(string.IsNullOrEmpty(warehouse.Name))
                warehouse.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_Warehouse_NameIsNull, new[] {
                    "Name"
                }));
            if(warehouse.BranchId == default(int))
                warehouse.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_Branch_NameIsNull, new[] {
                    "BranchId"
                }));
            foreach(var warehouseoperator in warehouse.WarehouseOperators.Where(e => e.OperatorId == default(int))) {
                warehouseoperator.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_WarehouseOperator_OperatorIdIsNull, new[] {
                    "OperatorId"
                }));
            }
            if(warehouse.HasValidationErrors || warehouse.WarehouseOperators.Any((e => e.HasValidationErrors))) {
                if(warehouse.WarehouseOperators.Any((e => e.HasValidationErrors)))
                    UIHelper.ShowAlertMessage(PartsStockingUIStrings.DataEditView_Validation_WarehouseOperator_OperatorIdIsNull);
                return;
            }
            ((IEditableObject)warehouse).EndEdit();
            if(EditState == DataEditState.New) {
                try {
                    int id = 0;
                    var warehouseArea = new WarehouseArea {
                        Id = id ++,
                        Code = warehouse.Code,
                        Status = (int)DcsBaseDataStatus.有效,
                        AreaKind = (int)DcsAreaKind.仓库
                    };
                    var newWarehouseArea2 = new WarehouseArea {
                        Id = id ++,
                        Code = "X1",
                        ParentId = warehouseArea.Id,
                        WarehouseAreaCategory = new WarehouseAreaCategory{
                            Category = (int)DcsAreaType.待发区
                        },
                        Status = 1,
                        AreaKind = (int)DcsAreaKind.库区
                    };
                     var newWarehouseArea3 = new WarehouseArea {
                         Id = id ++,
                         Code = "X1-01-01-01",
                         WarehouseAreaCategory = new WarehouseAreaCategory{
                            Category = (int)DcsAreaType.待发区
                        },
                        ParentId = newWarehouseArea2.Id,
                        TopLevelWarehouseAreaId = newWarehouseArea2.Id,
                        Status = 1,
                        AreaKind = (int)DcsAreaKind.库位
                    };
                    ((IEditableObject)warehouseArea).EndEdit();
                    warehouse.WarehouseAreas.Add(warehouseArea);
                    warehouse.WarehouseAreas.Add(newWarehouseArea2);
                    warehouse.WarehouseAreas.Add(newWarehouseArea3);
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
            }
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_Warehouse;
            }
        }

        public WarehouseDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
