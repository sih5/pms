﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using System.Windows;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class ABCSettingDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "ABCSetting_Type"
        };

        public ABCSettingDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(ABCSetting);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem{
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsStockingUIStrings.DataGridView_Title_ABCSetting_Type
                    }, new ColumnItem{
                        Name = "IsAutoAdjust",
                        Title = PartsStockingUIStrings.DataGridView_Title_ABCSetting_IsAutoAdjust
                    }, new ColumnItem{
                        Name = "SaleFrequency",
                        Title = PartsStockingUIStrings.DataGridView_Title_ABCSetting_SaleFrequency
                    },new ColumnItem{
                        Name = "SaleFee",
                        Title=PartsStockingUIStrings.DataGridView_Title_ABCSetting_SaleFee,
                    }, new ColumnItem{
                        Name = "NoSaleMonth",
                        Title=PartsStockingUIStrings.DataGridView_Title_ABCSetting_NoSaleMonth
                    }, new ColumnItem{
                        Name = "SafeStockCoefficient",
                        Title=PartsStockingUIStrings.DataGridView_Title_ABCSetting_SafeStockCoefficient
                    }, new ColumnItem{
                        Name = "Remark",
                        Title=PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_Remark
                    }, new ColumnItem{
                        Name = "IsReserve",
                        Title=PartsStockingUIStrings.DataGridView_Title_ABCSetting_IsReserve
                    },new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreatorName
                    },new ColumnItem {
                        Name = "CreateTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreateTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ModifierName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    },new ColumnItem {
                        Name = "ModifyTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "查询配件ABC分类设置";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 100;
        }
    }
}
