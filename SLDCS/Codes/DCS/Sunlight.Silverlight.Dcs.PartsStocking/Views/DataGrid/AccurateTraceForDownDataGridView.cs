﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AccurateTraceForDownDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "TraceProperty",
                        Title="追溯属性",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title="配件编号"
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title="配件名称"
                    }, new ColumnItem {
                        Title="SIH件码",
                        Name = "SIHLabelCode"
                    }, new ColumnItem {
                        Title="SIH箱码",
                        Name = "BoxCode"
                    },new ColumnItem {
                        Title="追溯码",
                        Name = "TraceCode"
                    }
                };
            }
        }
        private readonly string[] kvNames = {
            "TraceProperty"
        };

        public AccurateTraceForDownDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override Type EntityType {
            get {
                return typeof(TraceWarehouseAge);
            }
        }

      
        protected override string OnRequestQueryName() {
            return "查询可上架标签";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "partId":
                        if(filters.Filters.Any(item => item.MemberName == "PartId"))
                            return filters.Filters.Single(item => item.MemberName == "PartId").Value;
                        return 0;                   
                    case "sIHLabelCode":
                        if(filters.Filters.Any(item => item.MemberName == "SIHLabelCode"))
                            return filters.Filters.Single(item => item.MemberName == "SIHLabelCode").Value;
                        return null;
                    case "boxCode":
                        if(filters.Filters.Any(item => item.MemberName == "BoxCode"))
                            return filters.Filters.Single(item => item.MemberName == "BoxCode").Value;
                        return null;
                   
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "PartId" && filter.MemberName != "SIHLabelCode" && filter.MemberName != "BoxCode"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }


        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.ShowGroupPanel = false;
        }
    }
}
