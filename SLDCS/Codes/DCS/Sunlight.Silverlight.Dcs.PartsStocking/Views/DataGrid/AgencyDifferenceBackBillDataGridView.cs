﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AgencyDifferenceBackBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "AgencyDifferenceBackBillStatus", "ResponsibleMembersResTem" 
        };

        public AgencyDifferenceBackBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Custom_BillCode,
                        Name = "Code"
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                          Title = PartsStockingUIStrings.AgencyLogisticCompany_Status
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_IsUploadFile,
                        Name = "IsUplodFile"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyCode,
                        Name = "StorageCompanyCode"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyName,
                        Name = "StorageCompanyName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStockQueryView_PartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_SparePartName
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SparePart_MeasureUnit,
                        Name = "MeasureUnit"
                    },new ColumnItem {
                        Name = "PartsInboundPlanCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Code
                    },new ColumnItem {
                        Name = "InWarehouseCode",
                        Title = "入库仓库编号"
                    },new ColumnItem {
                        Name = "InWarehouseName",
                          Title = "入库仓库名称"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_ShippingCode,
                        Name = "PartsShippingCode"
                    },new ColumnItem {
                        Name = "PartsSalesOrderCode",
                          Title = "销售单号"
                    },new ColumnItem {
                        Title = "入库量",
                        Name = "InboundQty"
                    },new ColumnItem {
                        Title = "实收量",
                        Name = "ActQty"
                    },new ColumnItem {
                        Title = "差异量",
                        Name = "DifferQty"                      
                    },new ColumnItem {
                        Name = "OrderPrice",
                          Title ="订货价"
                    },new ColumnItem {
                        Name = "DifferSum",
                          Title = "差异总金额"
                    }, new ColumnItem {
                        Name = "DIfferReason",
                        Title = "差异原因"
                    },new ColumnItem {
                        Title = "责任类型",
                        Name = "ResType"
                    },new KeyValuesColumnItem {
                        Title = "责任组",
                        Name = "ResTem",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreatorName
                    },new ColumnItem {
                        Name = "CreateTime",
                          Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreateTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ModifierName",
                          Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    },new ColumnItem {
                        Name = "ModifyTime",
                          Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "AbandonerName",
                          Title = PartsStockingUIStrings.DataGridView_ColumnItem_Abandoner
                    },new ColumnItem {
                        Name = "AbandonTime",
                          Title = PartsStockingUIStrings.DataGridView_ColumnItem_AbandonTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "SubmitterName",
                          Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBill_SubmitterName
                    },new ColumnItem {
                        Name = "SubmitTime",
                          Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBill_SubmitTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "DetermineName",
                          Title = "判定人"
                    },new ColumnItem {
                        Name = "DetermineTime",
                        Title = "判定人时间",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ConfirmName",
                          Title = "确认人"
                    },new ColumnItem {
                        Name = "ConfirmTime",
                        Title = "确认时间",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "Handler",
                          Title = "处理人"
                    },new ColumnItem {
                        Name = "HandlTime",
                        Title = "处理时间",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "HandMemo",
                          Title = "处理意见"
                    },new ColumnItem {
                        Name = "CheckerName",
                          Title = "审核人"
                    },new ColumnItem {
                        Name = "CheckTime",
                        Title = "审核时间",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ApproverName",
                          Title = "审批人"
                    },new ColumnItem {
                        Name = "ApproveTime",
                        Title = "审批时间",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "RejectMemo",
                          Title = "驳回意见"
                    },new ColumnItem {
                        Name = "CloserName",
                          Title = "终止人"
                    },new ColumnItem {
                        Name = "CloseTime",
                        Title = "终止时间",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "CloseMemo",
                          Title = "终止意见"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAgencyDifferenceBackBillsByUser";
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyDifferenceBackBill);
            }
        }    

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 100;
            ((GridViewDataColumn)this.GridView.Columns["OrderPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DifferSum"]).DataFormatString = "c2";
        }

    }
}