﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AgencyDifferenceBackBillForSparepartDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public AgencyDifferenceBackBillForSparepartDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "PartsInboundPlanCode",
                        Title="入库计划单号"
                    }, new ColumnItem{
                        Name = "InWarehouseCode",
                        Title="入库仓库编号"
                    }, new ColumnItem{
                        Name = "InWarehouseName",
                        Title="入库仓库名称"
                    }, new ColumnItem{
                        Name = "PartsShippingCode",
                        Title="发运单号"
                    }, new ColumnItem{
                        Name = "PartsSalesOrderCode",
                        Title="销售单号"
                    }, new ColumnItem{
                        Name = "SparePartCode",
                        Title="配件编号"
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title="配件名称"
                    }, new ColumnItem{
                        Name = "MeasureUnit",
                        Title="计量单位"
                    }, new ColumnItem{
                        Name = "OrderPrice",
                        Title="订货价格"
                    }, new ColumnItem{
                        Name = "InboundQty",
                        Title="入库数量"
                    }, new ColumnItem{
                        Name = "CanDIffer",
                        Title="可差异处理量"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyDifferenceBackBillForSparepart);
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "CreateTime", "InWarehouseId", "SparePartCode", "SparePartName", "PartsInboundPlanCode", "PartsSalesOrderCode", "PartsShippingCode" };
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => !param.Contains(filter.MemberName))) {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "partsInboundPlanCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsInboundPlanCode").Value;
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "InWarehouseId").Value;
                    case "partsShippingCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsShippingCode").Value;
                    case "partsSalesOrderCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsSalesOrderCode").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                    case "bCreateTime":
                        var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "查询可差异退货配件";
        }
    }
}
