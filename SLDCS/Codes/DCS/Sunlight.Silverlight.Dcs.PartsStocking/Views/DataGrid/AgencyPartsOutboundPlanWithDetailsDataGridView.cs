﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AgencyPartsOutboundPlanWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Parts_OutboundType", "PartsOutboundPlan_Status"
        };

        public AgencyPartsOutboundPlanWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Status,
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_WarehouseCode,
                        Name = "WarehouseCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_WarehouseName,
                        Name = "WarehouseName"
                    }, new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_OutboundType,
                        Name = "OutboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount,
                        Name = "TotalAmount"
                    }, new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_OriginalRequirementBillCode
                    }, new ColumnItem {
                        Name = "ERPSourceOrderCode",
                        Title = PartsStockingUIStrings.QueryPanel_Title_ERPSourceOrderCode
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyCode,
                        Name = "CounterpartCompanyCode"
                    }, new ColumnItem {
                        Title =PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyName,
                        Name = "CounterpartCompanyName"
                    },  new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_ReceivingWarehouseCode,
                        Name = "ReceivingWarehouseCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_ReceivingWarehouseName,
                        Name = "ReceivingWarehouseName"
                    },  new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_Remark,
                        Name = "Remark"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TerminateReason,
                        Name = "StopComment"
                    } , new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreatorName,
                        Name = "CreatorName"
                     }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName,
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime,
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Stoper,
                        Name = "Stoper"
                    } ,new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_StopTime,
                        Name = "StopTime"
                    } , new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_BranchName,
                        Name = "BranchName"                 
                    }, new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_PartsSalesCategoryName
                     }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAgencyPartsOutboundPlansWithPartsCategory";
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyPartsOutboundPlan);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "AgencyPartsOutboundPlanDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 100;
        }
    }
}
