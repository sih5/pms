﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AgencyPartsShippingOrderDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsShippingOrder_TransportLossesDisposeMethod","DifferenceClassification"
        };

        public AgencyPartsShippingOrderDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartCode,
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartName,
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ShippingQuantity,
                        Name = "ShippingAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ConfirmQuantity,
                        Name = "ConfirmedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_InTransitDamageLossAmount,
                        Name = "InTransitDamageLossAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "DifferenceClassification",
                        KeyValueItems=this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_DifferenceClassification
                    } , new KeyValuesColumnItem {
                        Name = "TransportLossesDisposeMethod",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrder_TransportLossesDisposeMethod
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SettlementPrice,
                        Name = "SettlementPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark,
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(APartsShippingOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["ShippingAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ConfirmedAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["InTransitDamageLossAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAPartsShippingOrderDetails";
        }
    }
}
