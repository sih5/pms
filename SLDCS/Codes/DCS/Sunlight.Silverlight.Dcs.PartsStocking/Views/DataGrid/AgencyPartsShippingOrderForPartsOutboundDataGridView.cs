﻿using System;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AgencyPartsShippingOrderForPartsOutboundDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsShippingOrder_Status","PartsShippingOrder_TransportLossesDisposeStatus"
        };

        protected override System.Collections.Generic.IEnumerable<Core.Model.ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem{
                        Name = "LogisticCompanyCode"
                    }, new ColumnItem{
                        Name = "LogisticCompanyName"
                    }, new ColumnItem{
                        Name = "ReceivingAddress"
                   }, new ColumnItem{
                        Name = "ConfirmedReceptionTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ConfirmedReceptionTime
                    }, new KeyValuesColumnItem{
                        Name = "IsTransportLosses",
                        KeyValueItems = this.kvIsTransportLosses
                    }, new  KeyValuesColumnItem{
                        Name = "TransportLossesDisposeStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyPartsShippingOrder);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAgencyPartsShippingOrders";
        }

        private ObservableCollection<KeyValuePair> kvIsTransportLosses = new ObservableCollection<KeyValuePair>();

        public AgencyPartsShippingOrderForPartsOutboundDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.kvIsTransportLosses.Add(new KeyValuePair {
                Key = 0,
                Value = PartsStockingUIStrings.DataEditView_Text_No
            });
            this.kvIsTransportLosses.Add(new KeyValuePair {
                Key = 1,
                Value = PartsStockingUIStrings.DataEditView_Text_Yes
            });
        }
    }
}
