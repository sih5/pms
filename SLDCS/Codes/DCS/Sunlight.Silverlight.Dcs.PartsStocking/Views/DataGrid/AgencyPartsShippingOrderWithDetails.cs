﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AgencyPartsShippingOrderWithDetails : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsShippingOrder_Type", "PartsShippingOrder_TransportLossesDisposeStatus", "PartsShipping_Method", "PartsShippingOrder_Status"
        };

        private int[] PartsShippingOrder_Status = new[] {
        (int)DcsPartsShippingOrderStatus.新建,(int)DcsPartsShippingOrderStatus.已发货,(int)DcsPartsShippingOrderStatus.待提货,(int)DcsPartsShippingOrderStatus.待收货
        };

        public AgencyPartsShippingOrderWithDetails() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    },new ColumnItem {
                        Name = "Code"
                    },new ColumnItem {
                        Name = "ClosedLoop",
                        Title = PartsStockingUIStrings.QueryPanel_Title_ClosedLoopStatus
                    }, new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "OriginalRequirementBillCode"
                    }, new ColumnItem {
                        Name = "ShippingCompanyCode"
                    }, new ColumnItem {
                        Name = "ShippingCompanyName"
                    }, new ColumnItem {
                        Name = "ReceivingCompanyCode"
                    }, new ColumnItem {
                        Name = "ReceivingCompanyName"
                    },new ColumnItem{
                        Name="ReceivingWarehouseName"
                    }, new ColumnItem{
                        Name="TotalAmount",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount
                    }, new ColumnItem {
                        Name = "ReceivingAddress"
                    }, new ColumnItem {
                        Name = "IsTransportLosses",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrder_IsTransportLosses
                    }, new KeyValuesColumnItem {
                        Name = "TransportLossesDisposeStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrder_TransportLossesDisposeStatus
                    }, new ColumnItem {
                        Name = "LogisticCompanyCode"
                    }, new ColumnItem {
                        Name = "LogisticCompanyName"
                    }, new ColumnItem {
                         Name = "ExpressCompanyCode",
                            Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressCode
                    }, new ColumnItem {
                       Name = "ExpressCompany",
                            Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressName
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Name = "InterfaceRecordId"
                    }, new ColumnItem {
                        Name = "RequestedArrivalDate",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ShippingDate",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_ShippingDate
                    }, new ColumnItem {
                        Name = "LogisticArrivalDate",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ShippingTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_ShippingTime
                    }, new ColumnItem {
                        Name = "TransportDriverPhone"
                    }, new ColumnItem {
                        Name = "DeliveryBillNumber"
                    }, new ColumnItem{
                        Name = "WarehouseName",
                        Title =PartsStockingUIStrings.DataGridView_ColumnItem_OutboundWarehouseName
                    }, new ColumnItem{
                        Name = "TransportMileage"  
                    }, new ColumnItem{
                        Name = "TransportVehiclePlate",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TransportVehiclePlate
                    },new ColumnItem{
                        Name="GPSCode",
                        Title=PartsStockingUIStrings.DataEditView_Text_GPSCode
                    },new ColumnItem{
                        Name = "TransportDriver"
                    }, new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem{
                        Name="OrderApproveComment",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_OrderApproveComment
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name = "ModifierName"
                    }, new ColumnItem{
                        Name = "ModifyTime"
                    }, new ColumnItem{
                        Name = "ConsigneeName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ConsigneeName
                    }, new ColumnItem{
                        Name = "ConfirmedReceptionTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ConsigneeTime
                    }, new ColumnItem{
                        Name = "ReceiptConfirmorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ReceiptConfirmorName
                    }, new ColumnItem{
                        Name = "ReceiptConfirmTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ReceiptConfirmTime
                    }, new ColumnItem{
                        Name = "ERPSourceOrderCodeQuery",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ERPOrderCode
                    } , new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override void OnDataLoaded() {
            var partsShippingOrders = this.Entities.Cast<AgencyPartsShippingOrder>().ToArray();
            if(partsShippingOrders == null) {
                return;
            }
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(partsShippingOrders.Any(r => r.Type == (int)DcsPartsShippingOrderType.销售)) {
                dcsDomainContext.Load(dcsDomainContext.GetPartsSalesOrderByIdsQuery(partsShippingOrders.Where(r => r.Type == (int)DcsPartsShippingOrderType.销售).Select(r => r.OriginalRequirementBillId).ToArray()), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var partsSalesOrders = loadOp.Entities;
                    foreach(var partsShippingOrder in partsShippingOrders.Where(r => r.Type == (int)DcsPartsShippingOrderType.销售)) {
                        var partsSalesOrder = partsSalesOrders.FirstOrDefault(r => r.Code == partsShippingOrder.OriginalRequirementBillCode);
                        if(partsSalesOrder != null) {
                            partsShippingOrder.ERPSourceOrderCodeQuery = partsSalesOrder.ERPSourceOrderCode;
                        }
                    }
                }, null);

            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "closedLoopStatus":
                        return filters.Filters.FirstOrDefault(item => item.MemberName == "ClosedLoopStatus") == null ? null : filters.Filters.First(item => item.MemberName == "ClosedLoopStatus").Value;
                    case "ERPSourceOrderCodeQuery":
                        return filters.Filters.FirstOrDefault(item => item.MemberName == "ERPSourceOrderCodeQuery") == null ? null : filters.Filters.First(item => item.MemberName == "ERPSourceOrderCodeQuery").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var newCompositeFilterItem = new CompositeFilterItem();
                foreach(var filter in compositeFilterItem.Filters.Where(filter => filter.MemberName != "ClosedLoopStatus" && filter.MemberName != "ERPSourceOrderCodeQuery"))
                    newCompositeFilterItem.Filters.Add(filter);
                return newCompositeFilterItem.ToFilterDescriptor();
            }
            return base.OnRequestFilterDescriptor(queryName);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.DataPager.PageSize = 300;
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
            //foreach(var column in this.GridView.Columns)
            //    if("Status".Equals(column.UniqueName))
            //        column.CellStyleSelector = new BaleReportCreateDataGridViewCellStyleSelector(column.UniqueName);
            //因为 超期后背景显示浅红色导致订单状态看不清楚，需要将订单状态更改为黑色字体（和需求本身相反）

            //this.GridView.SelectionMode = SelectionMode.Multiple;
            //this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            //this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
        private GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is AgencyPartsShippingOrder) {
                        var partsshippingorder = item as AgencyPartsShippingOrder;
                        if(PartsShippingOrder_Status.Contains(partsshippingorder.Status) && partsshippingorder.RequestedArrivalDate < DateTime.Now)
                            return this.Resources["lightRedDataBackground"] as Style;
                    }
                    return null;
                }
            };
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyPartsShippingOrder);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "AgencyPartsShippingOrder", "AgencyPartsShippingOrderDetail", "AgencyPartsShippingOrderRef"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            //return "查询配件发运单仓库人员过滤";
            return "查询配件发运单代理库";
        }

        public class BaleReportCreateDataGridViewCellStyleSelector : StyleSelector {
            private readonly string gridViewColumnName;
            private Style redGridViewCellStyle;
            private Style greenGridViewCellStyle;

            private Style RedGridViewCellStyle {
                get {
                    if(this.redGridViewCellStyle == null) {
                        this.redGridViewCellStyle = new Style {
                            TargetType = typeof(GridViewCell)
                        };
                        this.redGridViewCellStyle.Setters.Add(new Setter(GridViewCell.ForegroundProperty, new SolidColorBrush(Colors.Orange)));
                    }
                    return this.redGridViewCellStyle;
                }
            }

            private Style GreenGridViewCellStyle {
                get {
                    if(this.greenGridViewCellStyle == null) {
                        this.greenGridViewCellStyle = new Style {
                            TargetType = typeof(GridViewCell)
                        };
                        this.greenGridViewCellStyle.Setters.Add(new Setter(GridViewCell.ForegroundProperty, new SolidColorBrush(Color.FromArgb(0xAA, 0x98, 0xCD, 0x98))));
                    }
                    return this.greenGridViewCellStyle;
                }
            }

            public BaleReportCreateDataGridViewCellStyleSelector(string gridViewColumnName) {
                this.gridViewColumnName = gridViewColumnName;
            }

            public override Style SelectStyle(object item, DependencyObject container) {
                var partsshippingorder = item as AgencyPartsShippingOrder;
                if(partsshippingorder == null)
                    return null;
                return partsshippingorder.Status == (int)DcsPartsShippingOrderStatus.待提货 || partsshippingorder.Status == (int)DcsPartsShippingOrderStatus.待收货 ?
                    RedGridViewCellStyle : (partsshippingorder.Status == (int)DcsPartsShippingOrderStatus.已发货 ? GreenGridViewCellStyle : null);
            }
        }
    }
}