﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AgencyPartsStockForDealerQueryDataGridView: DcsDataGridViewBase {
        private readonly string[] kvNames = {
                "Area_Category","ABCSetting_Type"
        };
   
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 100;
            ((GridViewDataColumn)this.GridView.Columns["CenterPrice"]).DataFormatString = "c2"; 
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                       new ColumnItem {
                        Name = "CompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_CompanyCode
                    },new ColumnItem {
                        Name = "CompanyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_CompanyName
                    },new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseCode
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseName
                    }
                    //, new KeyValuesColumnItem {
                    //    Name = "WarehouseAreaCategory",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    //    Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_WarehouseAreaCategory
                    //}, new ColumnItem {
                    //    Name = "WarehouseAreaCode",
                    //    Title = "库区"
                    //}, new ColumnItem {
                    //    Name = "WarehouseAreaPositionCode",
                    //    Title = "库位"
                    //}
                    , new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartName
                    }
                    , new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_PartABC
                    }, new ColumnItem {
                        Name = "CenterPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CenterPrice
                    }
                    , new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_StockQuantity
                    }, new ColumnItem {
                        Name = "QuantityAll",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_QuantityAll
                    }, new ColumnItem {
                        Name = "ActualAvailableStock",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ActualAvailableStock
                    }, new ColumnItem {
                        Name = "ActualAll",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ActualAll
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualAgencyPartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "大服务站库存查询";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "companyCode":
                        var filterCategory = filters.Filters.SingleOrDefault(item => item.MemberName == "CompanyCode");
                        return filterCategory == null ? null : filterCategory.Value;
                    case "companyName":
                        var filterCompanyName = filters.Filters.SingleOrDefault(item => item.MemberName == "CompanyName");
                        return filterCompanyName == null ? null : filterCompanyName.Value;
                    case "sparePartCode":
                        var filterSparePartCode = filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode");
                        return filterSparePartCode == null ? null : filterSparePartCode.Value;
                    case "sparePartName":
                        var filterSparePartName = filters.Filters.SingleOrDefault(item => item.MemberName == "SparePartName");
                        return filterSparePartName == null ? null : filterSparePartName.Value;
                    case "greaterThanZero":
                        var greaterThanZero = filters.Filters.SingleOrDefault(item => item.MemberName == "GreaterThanZero");
                        return greaterThanZero == null ? null : greaterThanZero.Value;                   
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                var param = new[] {"GreaterThanZero"};
                foreach(var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName)))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        public AgencyPartsStockForDealerQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
         
        }
    }
}
