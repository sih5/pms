﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AgencyStockQueryDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                        Name = "AgencyCode",
                        Title = "代理库编号"
                    },
                    new ColumnItem {
                        Name = "AgencyName",
                        Title = "代理库名称"
                    },
                    new ColumnItem {
                        Name = "WarehouseName",
                        Title = "仓库名称"
                    },
                    new ColumnItem {
                        Name = "PartCode",
                        Title = "配件图号"
                    },
                    new ColumnItem {
                        Name = "PartName",
                        Title = "配件名称"
                    },
                    new ColumnItem {
                        Name = "Qty",
                        Title = "库存"
                    },
                    new ColumnItem {
                        Name = "UsableQty",
                        Title = "可用库存"
                    },
                    new ColumnItem {
                        Name = "Saleprice",
                        Title = "批发价"
                    },
                    new ColumnItem {
                        Name = "Totalprice",
                        Title = "批发金额"
                    },
                    new ColumnItem {
                        Name = "PartsAttribution",
                        Title = "配件所属分类"
                    }, new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = "品牌"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyStockQuery);
            }
        }
        protected override string OnRequestQueryName() {
            return "代理库库存查询";
        }

    }
}
