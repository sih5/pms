﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AgentsHistoryInDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }
         private readonly string[] kvNames = new[] {
            "Parts_InboundType", "OriginalRequirementBill_Type","Parts_SettlementStatus"
        };

         public AgentsHistoryInDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {    
                    new KeyValuesColumnItem {
                        Name = "InboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = "入库类型"                   
                    },  new ColumnItem {
                        Name = "StorageCompanyCode",
                        Title = "仓储企业编号"
                    },  new ColumnItem {
                        Name = "StorageCompanyName",
                        Title = "仓储企业名称"
                    },  new ColumnItem {
                        Name = "ProvinceName",
                        Title = "省份"
                    },  new ColumnItem {
                        Name = "WarehouseCode",
                        Title = "仓库编号"
                    },  new ColumnItem {
                        Name = "WarehouseName",
                        Title = "仓库名称"
                    },  new ColumnItem {
                        Name = "PartsInboundPlanCode",
                        Title = "入库单编号"
                    },  new ColumnItem {
                        Name = "PartsInboundPlanTime",
                        Title = "入库时间"
                    },  new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                        Title = "原始需求单据编号"
                    },  new KeyValuesColumnItem {
                        Name = "OriginalRequirementBillType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = "原始需求单据类型"                   
                    },  new ColumnItem {
                        Name = "WarehouseAreaCode",
                        Title = "库位"
                    },  new ColumnItem {
                        Name = "PartCode",
                        Title = "配件编号"
                    },  new ColumnItem {
                        Name = "PartName",
                        Title = "配件名称"
                    },  new ColumnItem {
                        Name = "InspectedQuantity",
                        Title = "入库数量"
                    },  new ColumnItem {
                        Name = "SettlementPrice",
                        Title = "代理库批发价"
                    },  new ColumnItem {
                        Name = "Price",
                        Title = "入库金额"
                    },  new KeyValuesColumnItem {
                        Name = "SettlementStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        Title = "结算状态"                   
                    },  new ColumnItem {
                        Name = "CounterPartCompanyCode",
                        Title = "对方单位编号"
                    },  new ColumnItem {
                        Name = "CounterPartCompanyName",
                        Title = "对方单位名称"
                    },  new ColumnItem {
                        Name = "BranchName",
                        Title = "分公司名称"
                    },  new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = "品牌"
                    }
                   
                   
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleAgentsHistoryIn);
            }
        }
        protected override string OnRequestQueryName() {
            return "代理库入库记录查询";
        }

    }
}
