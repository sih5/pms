﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AgentsHistoryOutDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }
        private readonly string[] kvNames = new[] {
            "Parts_OutboundType", "OriginalRequirementBill_Type","Parts_SettlementStatus"
        };

        public AgentsHistoryOutDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {    
                    new KeyValuesColumnItem {
                        Name = "OutboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = "出库类型"                   
                    },  new ColumnItem {
                        Name = "StorageCompanyCode",
                        Title = "仓储企业编号"
                    },  new ColumnItem {
                        Name = "StorageCompanyName",
                        Title = "仓储企业名称"
                    },  new ColumnItem {
                        Name = "Businessode",
                        Title = "业务编码"
                    },  new ColumnItem {
                        Name = "ProvinceName",
                        Title = "省份"
                    },  new ColumnItem {
                        Name = "WarehouseCode",
                        Title = "仓库编号"
                    },  new ColumnItem {
                        Name = "WarehouseName",
                        Title = "仓库名称"
                    },  new ColumnItem {
                        Name = "OutboundCode",
                        Title = "出库单号"
                    },  new ColumnItem {
                        Name = "OutboundTime",
                        Title = "出库时间"
                    },  new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                        Title = "原始需求单据编号"
                    },  new KeyValuesColumnItem {
                        Name = "OriginalRequirementBillType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = "原始需求单据类型"                   
                    },  new ColumnItem {
                        Name = "ReceivingCompanyCode",
                        Title = "收货单位编号"
                    },  new ColumnItem {
                        Name = "ReceivingCompanyName",
                        Title = "收货单位名称" 
                     },  new ColumnItem {
                        Name = "ReceivingWarehouseCode",
                        Title = "收货仓库编号"
                    },  new ColumnItem {
                        Name = "ReceivingWarehouseName",
                        Title = "收货仓库名称"               
                    },  new ColumnItem {
                        Name = "WarehouseAreaCode",
                        Title = "库位"
                    },  new ColumnItem {
                        Name = "PartCode",
                        Title = "配件编号"
                    },  new ColumnItem {
                        Name = "PartName",
                        Title = "配件名称"  
                    },  new ColumnItem {
                        Name = "OutboundAmount",
                        Title = "出库数量"
                    },  new ColumnItem {
                        Name = "SettlementPrice",
                        Title = "代理库批发价"
                    },  new ColumnItem {
                        Name = "Price",
                        Title = "出库金额"
                    },  new KeyValuesColumnItem {
                        Name = "SettlementStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        Title = "结算状态"                   
                    },  new ColumnItem {
                        Name = "BranchName",
                        Title = "分公司名称"             
                    },  new ColumnItem {
                        Name = "BrandName",
                        Title = "品牌"
                    }                                   
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(VirtualAgentsHistoryOut);
            }
        }
        protected override string OnRequestQueryName() {
            return "代理库出库记录查询Linq";
        }

    }
}
