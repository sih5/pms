﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class AgentsPartsInventoryBillDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = new[] {
            "Area_Category", "PartsInventoryBill_Status"
        };

        public AgentsPartsInventoryBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.QueryPanel_Title_Status
                    },new ColumnItem { 
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_IsUploadFile, 
                        Name = "IsUplodFile" 
                    }
                    , new ColumnItem {
                        Name = "Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_Code
                    },new ColumnItem {
                        Name = "StorageCompanyCode",
                         Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyCode
                    },new ColumnItem {
                        Name = "StorageCompanyName",
                          Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyName
                    },new ColumnItem {
                        Name = "WarehouseName",
                         Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseName
                    }, new KeyValuesColumnItem {
                        Name = "WarehouseAreaCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseAreaCategory
                    },new ColumnItem{
                        Name = "InventoryReason",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_InventoryReason
                    }, new ColumnItem{
                        Name = "SumCostBeforeInventory",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostBeforeInventory
                    }, new ColumnItem{
                        Name = "SumCostAfterInventory",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostAfterInventory
                    }, new ColumnItem{
                        Name = "SumCostDifference",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostDifference
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CreateTime
                    },new ColumnItem {
                        Name = "InitialApproverName",
                        Title = "初审人"
                    },new ColumnItem {
                        Name = "InitialApproveTime",
                        Title = "初审时间"
                    },new ColumnItem {
                        Name = "CheckerName",
                        Title = "审核人"
                    },new ColumnItem {
                        Name = "CheckTime",
                        Title = "审核时间"
                    },new ColumnItem {
                        Name = "ApproverName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ApproverName
                    },new ColumnItem {
                        Name = "ApproveTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ApproveTime
                    }, new ColumnItem{
                        Title =PartsStockingUIStrings.DataGridView_ColumnItem_UpperCheckerName1,
                        Name = "AdvancedAuditName"
                    }, new ColumnItem{
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_UpperCheckTime1,
                        Name = "AdvancedAuditTime"
                    },new ColumnItem {
                        Name = "InventoryRecordOperatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_InventoryRecordOperatorName
                    },new ColumnItem {
                        Name = "InventoryRecordTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_InventoryRecordTime
                    },new ColumnItem {
                        Name = "InitiatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_InitiatorName
                    }, new ColumnItem{
                        Name = "RejecterName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejecterName
                    }, new ColumnItem{
                        Name = "RejectTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejectTime
                    }, new ColumnItem{
                        Name = "RejectComment",
                        Title = PartsStockingUIStrings.DataEditPanel_Text_ApproveComment
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInventoryBillEx);
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(parameterName == "amountDifference") {
                var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "AmountDifferenceRange");
                return filter != null ? filter.Value : null;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "AmountDifferenceRange" };
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => !param.Contains(filter.MemberName))) {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override string OnRequestQueryName() {
            return "GetPartsInventoryBillWithSumPrice";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                       "PartsInventoryBillDetailForBranchManagement"
                    }
                };
            }
        }
    }
}
