﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class BorrowBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "BorrowBill_Status", "BorrowBill_Type"
        };

        public BorrowBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(BorrowBill);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "IsUploadFile",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_IsUploadFile
                    }, new ColumnItem{
                        Name = "Code"
                    },new KeyValuesColumnItem{
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem{
                        Name = "BorrowDepartmentName"
                    }, new ColumnItem{
                        Name = "BorrowName"
                    }, new ColumnItem{
                        Name = "ContactMethod"
                    }, new ColumnItem{
                        Name = "WarehouseCode"
                    }, new ColumnItem{
                        Name = "WarehouseName"
                    }, new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    }, new ColumnItem{
                        Name = "SubmitterName"
                    }, new ColumnItem{
                        Name = "SubmitTime"
                    }, new ColumnItem{
                        Name = "InitialApproverName"
                    }, new ColumnItem{
                        Name = "InitialApproveTime"
                    }, new ColumnItem{
                        Name = "ApproverName"
                    }, new ColumnItem{
                        Name = "ApproveTime"
                    }, new ColumnItem{
                        Name = "OutBoundTime"
                    }, new ColumnItem{
                        Name = "ReturnerName"
                    }, new ColumnItem{
                        Name = "ReturnTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetBorrowBillForDetails";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] { 
                        "BorrowBillDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();
        }
    }
}
