﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class BorrowBillDetailDataGridView : DcsDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(BorrowBillDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new ColumnItem[] { 
                    new ColumnItem{
                        Name = "SparePartCode"
                    }, new ColumnItem{
                        Name = "SparePartName"
                    }, new ColumnItem{
                        Name = "SihCode"
                    }, new ColumnItem{
                        Name = "WarehouseAreaCode",
                        Title="库位编号"
                    }, new ColumnItem{
                        Name = "MeasureUnit"
                    }, new ColumnItem{
                        Name = "BorrowQty"
                    },new ColumnItem{
                        Name = "OutboundAmount"
                    }, new ColumnItem{
                        Name = "ConfirmingQty"
                    }, new ColumnItem{
                        Name = "ConfirmedQty"
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("BorrowBillDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
