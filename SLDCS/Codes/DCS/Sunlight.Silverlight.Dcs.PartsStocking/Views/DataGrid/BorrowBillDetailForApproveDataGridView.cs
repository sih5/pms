﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class BorrowBillDetailForApproveDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(BorrowBillDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="SparePartCode"
                        
                    }, new ColumnItem{
                        Name = "SparePartName"
                    }, new ColumnItem{
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true,
                        Title="库位编号"
                    }, new ColumnItem{
                        Name = "SihCode"
                    }, new ColumnItem{
                        Name = "BorrowQty"
                    }, new ColumnItem{
                        Name = "MeasureUnit"
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("BorrowBillDetails");
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
