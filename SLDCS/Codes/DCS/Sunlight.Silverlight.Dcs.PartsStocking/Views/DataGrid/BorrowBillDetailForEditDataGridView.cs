﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class BorrowBillDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase virtualPartsStockQueryWindow;

        private DcsMultiPopupsQueryWindowBase VirtualPartsStockQueryWindow {
            get {
                if(this.virtualPartsStockQueryWindow == null) {
                    this.virtualPartsStockQueryWindow = DI.GetQueryWindow("VirtualPartsStockDropDown") as DcsMultiPopupsQueryWindowBase;
                    this.virtualPartsStockQueryWindow.SelectionDecided += this.VirtualPartsStockQueryWindow_SelectionDecided;
                }
                return this.virtualPartsStockQueryWindow;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(BorrowBillDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="SparePartCode",
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true,
                        Title="库位编号"
                    }, new ColumnItem{
                        Name = "SihCode",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "BorrowQty"
                    }, new ColumnItem{
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("BorrowBillDetails");
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var borrowBill = this.DataContext as BorrowBill;
            if(borrowBill == null)
                return;
            if(borrowBill.WarehouseId == default(int) || borrowBill.WarehouseId == null) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_WarehouseNotNull);
                return;
            }
            this.VirtualPartsStockQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { 
                "Common", "WarehouseId", borrowBill.WarehouseId
            });
            this.VirtualPartsStockQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { 
                "Common", "WarehouseAreaCategory", (int)DcsAreaType.保管区 
            });
            this.VirtualPartsStockQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { 
                "Common", "WarehouseId", false
            });
            this.VirtualPartsStockQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { 
                "Common", "WarehouseAreaCategory", false
            });
            RadQueryWindow.ShowDialog();
        }

        private RadWindow radQueryWindow;
        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.VirtualPartsStockQueryWindow,
                    Header = PartsStockingUIStrings.QueryPanel_Title_VirtualPartsStock,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        private void VirtualPartsStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualPartsStocks = queryWindow.SelectedEntities.Cast<VirtualPartsStock>().ToArray();
            if(!virtualPartsStocks.Any())
                return;
            var borrowBill = this.DataContext as BorrowBill;
            if(borrowBill == null)
                return;
            foreach(var virtualPartsStock in virtualPartsStocks) {
                if (borrowBill.BorrowBillDetails.Any(t => t.SparePartCode == virtualPartsStock.SparePartCode && t.WarehouseAreaId == virtualPartsStock.WarehouseAreaId))
                {
                    continue;
                }
                var borrowBillDetail = new BorrowBillDetail {
                    SparePartId = virtualPartsStock.SparePartId,
                    SparePartCode = virtualPartsStock.SparePartCode,
                    SparePartName = virtualPartsStock.SparePartName,
                    MeasureUnit = virtualPartsStock.MeasureUnit,
                    WarehouseAreaCode = virtualPartsStock.WarehouseAreaCode,
                    WarehouseAreaId = virtualPartsStock.WarehouseAreaId,
                    BorrowQty = 0,
                    SihCode = virtualPartsStock.ReferenceCode
                };
                borrowBill.BorrowBillDetails.Add(borrowBillDetail);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
