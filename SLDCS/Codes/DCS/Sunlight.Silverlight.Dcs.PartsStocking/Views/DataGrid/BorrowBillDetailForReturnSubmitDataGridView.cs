﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class BorrowBillDetailForReturnSubmitDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(BorrowBillDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="SparePartCode",
                        IsReadOnly =true
                    }, new ColumnItem{
                        Name = "SparePartName",
                        IsReadOnly =true
                    }, new ColumnItem{
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true,
                        Title="库位编号"
                    }, new ColumnItem{
                        Name = "SihCode",
                        IsReadOnly =true
                    }, new ColumnItem{
                        Name = "BorrowQty",
                        IsReadOnly =true
                    }, new ColumnItem{
                        Name = "MeasureUnit",
                        IsReadOnly =true
                    }, new ColumnItem{
                        Name = "ConfirmingQty",
                        IsReadOnly =true
                    }, new ColumnItem{
                        Name = "BeforeQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_BeforeQty
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("BorrowBillDetails");
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
