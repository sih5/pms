﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class BoxUpTaskDetailDataGridView : DcsDataGridViewBase {
       
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;

            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.CellEditEnded += GridView_CellEditEnded;

            ((GridViewDataColumn)this.GridView.Columns["PlanQty"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["BoxUpQty"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["NowBoxUpQuantity"]).DataFormatString = "d";

        }
     

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e)
        {

            if (e.Cell.Column.UniqueName.Equals("NowBoxUpQuantity"))
            {
                var detail1 = e.Cell.DataContext as VirtualBoxUpTaskDetail;
                if (detail1 == null)
                    return;
                var quantityError = detail1.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("NowBoxUpQuantity"));
                if (quantityError != null)
                    detail1.ValidationErrors.Remove(quantityError);
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e)
        {
            if (!e.Cell.Column.UniqueName.Equals("NowBoxUpQuantity"))
                return;
            var detail = e.Cell.DataContext as VirtualBoxUpTaskDetail;
            if (detail == null)
                return;
            var quantityError = detail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("NowBoxUpQuantity"));
            if (quantityError != null)
                detail.ValidationErrors.Remove(quantityError);
            if (detail.NowBoxUpQuantity >= 0)
            {
                if (detail.NowBoxUpQuantity > detail.PlanQty - detail.BoxUpQty)
                    detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataGridView_Validation_NowBoxUpQuantityError1, new[] {
                        "NowBoxUpQuantity"
                    }));
            }
            else
                detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataGridView_Validation_NowBoxUpQuantityError2, new[] {
                    "NowBoxUpQuantity"
                }));

        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsOutboundPlan_Code,
                        Name = "PartsOutboundPlanCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode,
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName,
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SihCode,
                        Name = "SihCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_MeasureUnit,
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Title_PlanAmount,
                        Name = "PlanQty",
                         MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_BoxUpQuantity,
                        Name = "BoxUpQty",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                       Title = PartsStockingUIStrings.DataGridView_ColumnItem_NowBoxUpQuantity,
                       Name = "NowBoxUpQuantity",
                       MaskType = MaskType.Numeric,
                       TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding()
        {
            return new Binding("BoxUpTaskDetails");
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualBoxUpTaskDetail);
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }

        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }
    }
}