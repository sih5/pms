﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class BoxUpTaskDetailForMainDataGridView : DcsDataGridViewBase {

        public BoxUpTaskDetailForMainDataGridView()
        {
            this.DataContextChanged += BoxUpTaskDetailForMainDataGridView_DataContextChanged;
        }

        private int boxUpTaskId;
        private void BoxUpTaskDetailForMainDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var boxUpTask = e.NewValue as VirtualBoxUpTask;

            if (boxUpTask == null || boxUpTask.Id == default(int))
                return;
            this.boxUpTaskId = boxUpTask.Id;
            this.ExecuteQueryDelayed();
        }

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;

            ((GridViewDataColumn)this.GridView.Columns["PlanQty"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["BoxUpQty"]).DataFormatString = "d";
        }
     

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartsOutboundPlan_Code,
                        Name = "PartsOutboundPlanCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_PickingTaskCode,
                        Name = "PickingTaskCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode,
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName,
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SihCode,
                        Name = "SihCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_MeasureUnit,
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Title_PlanAmount,
                        Name = "PlanQty",
                         MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_BoxUpQuantity,
                        Name = "BoxUpQty",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_ContainerNumber,
                        Name = "ContainerNumber",
                        IsReadOnly = true
                    }
                };
            }
        }
        protected override string OnRequestQueryName()
        {
            return "getBoxUpTaskDetailsByTaskId";
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualBoxUpTaskDetail);
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }

        }
        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            switch (parameterName)
            {
                case "id":
                    return this.boxUpTaskId;
                default:
                    return null;
            }
        }
    }
}