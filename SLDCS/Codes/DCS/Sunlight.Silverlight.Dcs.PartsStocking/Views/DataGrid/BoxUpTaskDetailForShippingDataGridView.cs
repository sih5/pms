﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class BoxUpTaskDetailForShippingDataGridView : DcsDataGridViewBase {
        public BoxUpTaskDetailForShippingDataGridView() {
            this.DataContextChanged += this.BoxUpTaskDetailForShippingDataGridView_DataContextChanged;
        }

        private void BoxUpTaskDetailForShippingDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsOutboundPlan = e.NewValue as TaskListPartsOutboundPlan;
            if(partsOutboundPlan == null || partsOutboundPlan.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "TaskId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsOutboundPlan.TaskId
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "TaskType",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsOutboundPlan.TaskStatus == "装箱完成" ? 1 : 0
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "taskId":
                        return filters.Filters.Single(item => item.MemberName == "TaskId").Value;
                    case "taskType":
                        return filters.Filters.Single(item => item.MemberName == "TaskType").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "TaskId" && filter.MemberName != "TaskType"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override Type EntityType {
            get {
                return typeof(TaskListDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title= PartsStockingUIStrings.QueryPanel_Title_PartsOutboundPlan_Code,
                        Name = "PartsOutboundPlanCode"
                    },
                    new ColumnItem {
                        Title= PartsStockingUIStrings.QueryPanel_Title_SourceCode,
                        Name = "SourceCode"
                    },
                    new ColumnItem {
                         Title=PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                        Name = "SparePartCode"
                    }, new ColumnItem {
                         Title=PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                         Title=PartsStockingUIStrings.DataGridView_ColumnItem_SihCode,
                        Name = "SihCode"
                    }, new ColumnItem {
                         Title=PartsStockingUIStrings.DataGridView_ColumnItem_TaskQuantity,
                        Name = "TaskQty",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                         Title=PartsStockingUIStrings.DataEditView_Text_ContainerNumber,
                        Name = "ContainerNumber"
                    },
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "根据任务单号查询任务单信息";
        }


        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["TaskQty"]).DataFormatString = "d";
        }

    }
}
