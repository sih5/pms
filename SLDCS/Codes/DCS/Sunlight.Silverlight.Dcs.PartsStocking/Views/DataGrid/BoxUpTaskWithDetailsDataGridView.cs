﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class BoxUpTaskWithDetailsDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = new[] {
            "BoxUpTaskStatus"
        };
        public BoxUpTaskWithDetailsDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }


        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }


        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件装箱任务";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Status,
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },  new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_BoxUpTask_Code,
                        Name = "Code"
                    },  new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_PartsSalesOrderTypeName,
                        Name = "OrderTypeName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_WarehouseCode,
                        Name = "WarehouseCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_WarehouseName,
                        Name = "WarehouseName"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_IsExistShippingOrder,
                        Name = "IsExistShippingOrder"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyCode,
                        Name = "CounterpartCompanyCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyName,
                        Name = "CounterpartCompanyName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreatorName,
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreateTime,
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName,
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime,
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_BoxUpFinishTime,
                        Name = "BoxUpFinishTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditPanel_Text_CompanyTransferOrder_PartsSalesCategory,
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if (compositeFilterItem == null)
            {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            }
            else
            {
                foreach (var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "SourceCode" && filter.MemberName != "PartsOutboundPlanCode"
                     && filter.MemberName != "SparePartCode"  && filter.MemberName != "SparePartName"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "partsSalesCategoryId":
                        //return filters.Filters.Single(item => item.MemberName == "PartsSalesCategoryId").Value;
                        return null;
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "sourceCode":
                        return filters.Filters.Single(item => item.MemberName == "SourceCode").Value;
                    case "partsOutboundPlanCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsOutboundPlanCode").Value;
                    case "counterpartCompanyCode":
                        //return filters.Filters.Single(item => item.MemberName == "CounterpartCompanyCode").Value;
                        return null;
                    case "counterpartCompanyName":
                        return filters.Filters.Single(item => item.MemberName == "CounterpartCompanyName").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                    case "bCreateTime":
                        var createTime = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                    case "bModifyTime":
                        var modifyTime = date.FirstOrDefault(p => p.Filters.First().MemberName == "ModifyTime");
                        return modifyTime == null ? null : modifyTime.Filters.First(r => r.MemberName == "ModifyTime").Value;
                    case "eModifyTime":
                        var modifyTime1 = date.FirstOrDefault(p => p.Filters.First().MemberName == "ModifyTime");
                        return modifyTime1 == null ? null : modifyTime1.Filters.Last(item => item.MemberName == "ModifyTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualBoxUpTask);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider
        {
            get
            {
                return new TabControlRowDetailsTemplateProvider
                {
                    DetailPanelNames = new[] {
                        "BoxUpTaskDetailForMain"
                    }
                };
            }

        }
    }
}