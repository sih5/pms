﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class CompanyStockForReplaceDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Company_Type"
        };

        public CompanyStockForReplaceDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.CompanyStockForReplaceDataGridView_DataContextChanged;

        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }

        private void CompanyStockForReplaceDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var companyPartsStock = e.NewValue as CompanyPartsStock;
            if(companyPartsStock == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            if(this.FilterItem == null)
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "CompanyId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = companyPartsStock.CompanyId
                });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SparePartId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = companyPartsStock.SparePartId
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartName
                    }, new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_Quantity
                    }, new ColumnItem {
                        Name = "UsableQuantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_UsableQuantity
                    }, new ColumnItem {
                        Name = "CompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_CompanyCode
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_CompanyName
                    }, new KeyValuesColumnItem {
                        Name = "CompanyType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_CompanyType
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_BranchName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CompanyPartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询替互换件企业库存";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "companyId":
                        return filters.Filters.Single(item => item.MemberName == "CompanyId").Value;
                    case "sparePartId":
                        return filters.Filters.Single(item => item.MemberName == "SparePartId").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "CompanyId" && filter.MemberName != "SparePartId"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
