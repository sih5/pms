﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class CompanyStockWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Company_Type"
        };

        public CompanyStockWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartName
                    }, new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_Quantity
                    }, new ColumnItem {
                        Name = "UsableQuantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_UsableQuantity
                    }, new ColumnItem {
                        Name = "CompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_CompanyCode
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_CompanyName
                    }, new KeyValuesColumnItem {
                        Name = "CompanyType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_CompanyType
                    }, new ColumnItem{
                        Name = "PartsSalesPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_PartsSalesPrice
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_BranchName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CompanyPartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询企业库存";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "companyId":
                        return filters.Filters.Single(item => item.MemberName == "CompanyId").Value;
                    case "partIds":
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "CompanyId"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                       "CompanyStockForReplace"
                    }
                };
            }
        }
    }
}