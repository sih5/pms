﻿
using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class CompanyTransferOrderDetailDataGridView : DcsDataGridViewBase {
        void CompanyTransferOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var overstockPartsApp = e.NewValue as CompanyTransferOrder;
            if(overstockPartsApp == null)
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartsTransferOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = overstockPartsApp.Id;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(CompanyTransferOrderDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsCode"
                    }, new ColumnItem {
                        Name = "PartsName"
                    }, new ColumnItem {
                        Name = "PlannedAmount"
                    }, new ColumnItem {
                         Name = "ConfirmedAmount"
                    }, new ColumnItem {
                        Name = "Price"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCompanyTransferOrderDetails";
        }

        public CompanyTransferOrderDetailDataGridView() {
            this.DataContextChanged += CompanyTransferOrderDetailDataGridView_DataContextChanged;
        }

    }
}
