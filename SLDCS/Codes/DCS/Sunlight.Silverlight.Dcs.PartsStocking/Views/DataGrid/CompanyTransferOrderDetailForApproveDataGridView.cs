﻿
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class CompanyTransferOrderDetailForApproveDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsCode",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "PartsName",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyTransferOrderDetail_PlannedAmount,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyTransferOrderDetail_ConfirmedAmount
                    },new ColumnItem {
                        Name = "Price",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(CompanyTransferOrderDetail);
            }
        }

        protected override
        Binding OnRequestDataSourceBinding() {
            return new Binding("CompanyTransferOrderDetails");
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CellValidating += GridView_CellValidating;
        }

        void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var detail = e.Cell.DataContext as CompanyTransferOrderDetail;
            if(detail == null)
                return;
            switch(e.Cell.DataColumn.UniqueName) {
                case "ConfirmedAmount":
                    if((int)e.NewValue < 0 || (int)e.NewValue > detail.PlannedAmount) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsStockingUIStrings.DataGridView_Validation_PartsSalesOrderDetail_OrderedQuantityZeroError;
                    }
                    break;
            }
        }
    }
}
