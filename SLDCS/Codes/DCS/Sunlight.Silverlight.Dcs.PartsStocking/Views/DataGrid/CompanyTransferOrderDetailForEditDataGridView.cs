﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class CompanyTransferOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private RadWindow warehousePartsStockQueryWindow;
        private DcsMultiPopupsQueryWindowBase queryWindowBase;

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsCode",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "PartsName",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyTransferOrderDetail_PlannedAmount
                    }, new ColumnItem {
                        Name = "Price",
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        private DcsMultiPopupsQueryWindowBase QueryWindowBase {
            get {
                if(this.queryWindowBase == null) {
                    this.queryWindowBase = DI.GetQueryWindow("WarehousePartsStockLibraryMulit") as DcsMultiPopupsQueryWindowBase;
                    var dcsMultiPopupsQueryWindowBase = this.queryWindowBase;
                    if(dcsMultiPopupsQueryWindowBase != null) {
                        dcsMultiPopupsQueryWindowBase.Loaded += this.WarehousePartsStockQueryWindow_Loaded;
                        dcsMultiPopupsQueryWindowBase.SelectionDecided += this.WarehousePartsStockQueryWindow_SelectionDecided;
                    }
                }
                return this.queryWindowBase;
            }
        }

        private DcsMultiPopupsQueryWindowBase dealerPartsStockService;

        private DcsMultiPopupsQueryWindowBase DealerPartsStockService {
            get {
                if(this.dealerPartsStockService == null) {
                    this.dealerPartsStockService = DI.GetQueryWindow("DealerPartsStockService") as DcsMultiPopupsQueryWindowBase;
                    if(this.dealerPartsStockService != null) {
                        this.dealerPartsStockService.Loaded += dealerPartsStockService_Loaded;
                        this.dealerPartsStockService.SelectionDecided += dealerPartsStockService_SelectionDecided;
                    }
                }
                return this.dealerPartsStockService;
            }
        }

        private void dealerPartsStockService_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var dealerPartsStock = queryWindow.SelectedEntities.Cast<DealerPartsStock>().ToArray();
            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            if(dealerPartsStock == null || companyTransferOrder == null)
                return;
            if(companyTransferOrder.CompanyTransferOrderDetails.Any(entity => dealerPartsStock.Any(ex => entity.PartsId == ex.SparePartId))) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrderDetail_SparePartIdIsRepeat);
                return;
            }
            foreach(var item in dealerPartsStock) {
                var companyTransferOrderDetail = new CompanyTransferOrderDetail();
                companyTransferOrderDetail.PartsId = item.SparePartId;
                companyTransferOrderDetail.PartsCode = item.SparePartCode;
                companyTransferOrderDetail.PartsName = item.SparePart.Name;
                companyTransferOrderDetail.ConfirmedAmount = 0;
                companyTransferOrderDetail.PlannedAmount = 0;
                companyTransferOrderDetail.Price = item.BasicSalePrice;
                companyTransferOrder.CompanyTransferOrderDetails.Add(companyTransferOrderDetail);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();

        }

        private void dealerPartsStockService_Loaded(object sender, RoutedEventArgs e) {
            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(companyTransferOrder == null || queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SalesCategoryId", companyTransferOrder.PartsSalesCategoryId
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "DealerCode", companyTransferOrder.OriginalCompanyCode
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "DealerName", companyTransferOrder.OriginalCompanyName
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new object[] {
                new FilterItem("DealerId", typeof(int), FilterOperator.IsEqualTo, companyTransferOrder.OriginalCompanyId)
            });
        }

        private RadWindow dealerPartsStockServiceQueryWindow;
        private RadWindow DealerPartsStockServiceQueryWindow {
            get {
                if(this.dealerPartsStockServiceQueryWindow == null) {
                    this.dealerPartsStockServiceQueryWindow = new RadWindow {
                        Content = this.DealerPartsStockServiceFactory(),
                        Header = PartsStockingUIStrings.QueryPanel_Title_Customer_DealerPartsStockQueryWindow,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen
                    };
                }
                this.dealerPartsStockServiceQueryWindow.Closed += dealerPartsStockServiceQueryWindow_Closed;
                return this.dealerPartsStockServiceQueryWindow;
            }
        }

        private void dealerPartsStockServiceQueryWindow_Closed(object sender, WindowClosedEventArgs e) {
            if(this.dealerPartsStockService == null)
                return;
            this.dealerPartsStockService.Loaded -= dealerPartsStockService_Loaded;
            this.dealerPartsStockService.SelectionDecided -= dealerPartsStockService_SelectionDecided;
            this.dealerPartsStockService = null;
            this.dealerPartsStockServiceQueryWindow.Content = null;
        }


        private RadWindow WarehousePartsStockQueryWindow {
            get {
                if(this.warehousePartsStockQueryWindow == null) {
                    this.warehousePartsStockQueryWindow = new RadWindow {
                        Content = this.QueryWindowFactory(),
                        Header = PartsStockingUIStrings.QueryPanel_Title_Customer_WarehousePartsStockWindow,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen
                    };
                }
                this.warehousePartsStockQueryWindow.Closed += warehousePartsStockQueryWindow_Closed;
                return this.warehousePartsStockQueryWindow;
            }
        }

        private void warehousePartsStockQueryWindow_Closed(object sender, WindowClosedEventArgs e) {
            if(this.queryWindowBase == null)
                return;
            this.queryWindowBase.Loaded -= WarehousePartsStockQueryWindow_Loaded;
            this.queryWindowBase.SelectionDecided -= WarehousePartsStockQueryWindow_SelectionDecided;
            this.queryWindowBase = null;
            this.WarehousePartsStockQueryWindow.Content = null;
        }


        private void WarehousePartsStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(companyTransferOrder == null || queryWindow == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("StorageCompanyId", typeof(int), FilterOperator.IsEqualTo, companyTransferOrder.OriginalCompanyId));
            compositeFilterItem.Filters.Add(new FilterItem("PartsSalesCategoryId", typeof(int), FilterOperator.IsEqualTo, companyTransferOrder.PartsSalesCategoryId));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void WarehousePartsStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var warehousePartsStock = queryWindow.SelectedEntities.Cast<WarehousePartsStock>().ToArray();
            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            if(warehousePartsStock == null || companyTransferOrder == null)
                return;
            if(companyTransferOrder.CompanyTransferOrderDetails.Any(entity => warehousePartsStock.Any(ex => entity.PartsId == ex.SparePartId))) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrderDetail_SparePartIdIsRepeat);
                return;
            }
            foreach(var item in warehousePartsStock) {
                var companyTransferOrderDetail = new CompanyTransferOrderDetail();
                companyTransferOrderDetail.PartsId = item.SparePartId;
                companyTransferOrderDetail.PartsCode = item.SparePartCode;
                companyTransferOrderDetail.PartsName = item.SparePartName;
                companyTransferOrderDetail.ConfirmedAmount = 0;
                companyTransferOrderDetail.PlannedAmount = 0;
                companyTransferOrderDetail.Price = item.PartsSalesPrice;
                companyTransferOrder.CompanyTransferOrderDetails.Add(companyTransferOrderDetail);
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.CanUserInsertRows = true;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            if(companyTransferOrder == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "PlannedAmount":
                    companyTransferOrder.TotalAmount = companyTransferOrder.CompanyTransferOrderDetails.Sum(detail => detail.PlannedAmount * detail.Price);
                    break;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("CompanyTransferOrderDetails");
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var companyTransferOrder = this.DataContext as CompanyTransferOrder;
            if(companyTransferOrder == null)
                return;
            if(companyTransferOrder.Type == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_CompanyTransferOrder_TypeIsNull);
                return;
            }
            if(companyTransferOrder.OriginalCompanyId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_CompanyTransferOrder_OriginalCompanyIdIsNull);
                return;
            }
            if(companyTransferOrder.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_CompanyTransferOrder_PartsSalesCategoryIdIsNull);
                return;
            }
            if(companyTransferOrder.TransferDirection == default(int)) {
                UIHelper.ShowNotification("请选择调拨方向");
                return;
            }
            if(companyTransferOrder.TransferDirection == (int)DcsTransferDirection.代理库向服务站) {
                this.QueryWindowFactory = () => this.QueryWindowBase;
                if(this.WarehousePartsStockQueryWindow != null && this.WarehousePartsStockQueryWindow.Content == null) {
                    this.WarehousePartsStockQueryWindow.Content = this.QueryWindowFactory();
                }
                this.WarehousePartsStockQueryWindow.ShowDialog();
            } else if(companyTransferOrder.TransferDirection == (int)DcsTransferDirection.服务站向代理库) {
                this.DealerPartsStockServiceFactory = () => this.DealerPartsStockService;
                if(this.DealerPartsStockServiceQueryWindow != null && this.DealerPartsStockServiceQueryWindow.Content == null) {
                    this.DealerPartsStockServiceQueryWindow.Content = this.DealerPartsStockServiceFactory();
                }
                this.DealerPartsStockServiceQueryWindow.ShowDialog();
            }
            //if(companyTransferOrder.Type == (int)DcsCompanyTransferOrderType.一二级网点调拨) {
            //    //DealerPartsStockDropDownQueryWindow 服务站配件库存查询
            //    this.DealerPartsStockQueryWindow.ShowDialog();
            //} else
            //if(companyTransferOrder.Type == (int)DcsCompanyTransferOrderType.服务站兼代理库调拨) {
            //    //WarehousePartsStockDropDownQueryWindow 服务站配件库存查询
            //    this.WarehousePartsStockQueryWindow.ShowDialog();
            //}

        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(CompanyTransferOrderDetail);
            }
        }

        private Func<UIElement> QueryWindowFactory {
            get;
            set;
        }
        private Func<UIElement> DealerPartsStockServiceFactory {
            get;
            set;
        }
    }
}
