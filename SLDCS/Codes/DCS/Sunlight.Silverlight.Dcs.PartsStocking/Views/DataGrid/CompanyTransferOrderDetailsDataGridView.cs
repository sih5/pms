﻿
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class CompanyTransferOrderDetailsDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsCode"
                    }, new ColumnItem {
                        Name = "PartsName"
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }//, new ColumnItem {
                    //    Name = "ConfirmedAmount",
                    //    Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlanDetail_OutboundFulfillment2,
                    //    MaskType = MaskType.Numeric,
                    //    TextAlignment = TextAlignment.Right
                    //}
                    , new ColumnItem {
                        Name = "CurrentOutboundAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CurrentOutboundAmount
                    }, new ColumnItem {
                        Name = "Price",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_TradePrice
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }


        protected override Type EntityType {
            get {
                return typeof(CompanyTransferOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("CompanyTransferOrderDetails");
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PlannedAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CurrentOutboundAmount"]).DataFormatString = "d";
            //((GridViewDataColumn)this.GridView.Columns["ConfirmedAmount"]).DataFormatString = "d";
        }
    }
}
