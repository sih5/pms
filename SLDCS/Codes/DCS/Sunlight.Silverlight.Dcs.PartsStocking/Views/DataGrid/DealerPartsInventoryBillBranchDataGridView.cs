﻿using System;
using System.Linq;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using System.Windows;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class DealerPartsInventoryBillBranchDataGridView: DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "DealerPartsInventoryBill_Status"
        };
        public DealerPartsInventoryBillBranchDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new ColumnItem[] { 
                    new ColumnItem{
                        Name = "Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_Code
                    },new ColumnItem { 
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_IsUploadFile, 
                        Name = "IsUplodFile" 
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "AgencyName",
                        Title = "中心库名称"
                    }, new ColumnItem{
                        Name = "StorageCompanyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_StorageCompanyName
                    }, new ColumnItem{
                        Name = "StorageCompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_StorageCompanyCode
                    },new ColumnItem {
                        Name = "CancelReason"
                    }, new ColumnItem{
                        Name = "SumCostBeforeInventory",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostBeforeInventory
                    }, new ColumnItem{
                        Name = "SumCostAfterInventory",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostAfterInventory
                    }, new ColumnItem{
                        Name = "SumCostDifference",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostDifference
                    }, new ColumnItem{
                        Name = "CreatorName"
                    }, new ColumnItem{
                        Name = "CreateTime"
                    },new ColumnItem{
                        Name = "InitialApproverName",
                        Title = PartsStockingUIStrings.DataGridView_Column_InitApprover
                    },new ColumnItem{
                        Name = "InitialApproveTime",
                        Title = PartsStockingUIStrings.DataGridView_Column_InitApproveTime
                    },new ColumnItem{
                        Name = "ApproverName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CheckerName
                    },new ColumnItem{
                        Name = "ApproveTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CheckTime
                    },new ColumnItem{
                        Name = "CheckerName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ApproverName
                    },new ColumnItem{
                        Name = "CheckTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ApproveTime
                    },new ColumnItem{
                        Name = "UpperCheckerName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_UpperCheckerName
                    },new ColumnItem{
                        Name = "UpperCheckTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_UpperCheckTime
                    }, new ColumnItem{
                        Name = "SeniorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SeniorName
                    }, new ColumnItem{
                        Name = "SeniorTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SeniorTime
                    }, new ColumnItem{
                        Name = "RejecterName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejecterName
                    }, new ColumnItem{
                        Name = "RejectTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejectTime
                    }, new ColumnItem{
                        Name = "ApproveComment",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_RejectComment
                    }, new ColumnItem{
                        Name = "Branch.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_BranchName
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询盘点单";
        }

        //protected override void OnDataLoaded()
        //{
        //    var bills = this.Entities.Cast<DealerPartsInventoryBill>().ToArray();
        //    foreach (var bill in bills) {
        //        bill.SumCostBeforeInventory =(decimal) bill.DealerPartsInventoryDetails.Sum(r => r.DealerPrice * r.CurrentStorage);
        //        bill.SumCostAfterInventory =(decimal) bill.DealerPartsInventoryDetails.Sum(r => r.DealerPrice * r.StorageAfterInventory);
        //        bill.SumCostDifference = (decimal) bill.DealerPartsInventoryDetails.Sum(r => r.DealerPrice * r.StorageDifference);
        //    }
        //}

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(parameterName == "amountDifference") {
                var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "AmountDifferenceRange");
                return filter != null ? filter.Value : null;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "AmountDifferenceRange" };
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => !param.Contains(filter.MemberName))) {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        } 

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] { 
                        "DealerPartsInventoryDetail"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsInventoryBill);
            }
        }
    }
}
