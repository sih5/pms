﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class DealerPartsInventoryBillDetailForApproveDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartCode,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "CurrentStorage",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CurrentStorage
                    },new ColumnItem {
                        Name = "StorageAfterInventory",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_StorageAfterInventory
                    }, new ColumnItem{
                        Name = "StorageDifference",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_StorageDifference
                    },
                     new ColumnItem{
                        Name = "DealerPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_DealerPrice
                    }
                    ,new ColumnItem {
                        Name = "Memo",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_MemoRemark
                    }
                };
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(DealerPartsInventoryDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerPartsInventoryDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DataPager.PageSize = 100;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["DealerPrice"]).DataFormatString = "c2";
        }
    }
}
