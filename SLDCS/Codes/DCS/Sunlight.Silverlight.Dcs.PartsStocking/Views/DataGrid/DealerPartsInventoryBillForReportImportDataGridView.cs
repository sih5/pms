﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class DealerPartsInventoryBillForReportImportDataGridView : DcsDataGridViewBase {
        //protected readonly string[] kvNames = {
        //    "MasterData_Status"
        //};
        //public DealerPartsInventoryBillForReportImportDataGridView() {
        //this.KeyValueManager.Register(this.kvNames);
        //}
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },new ColumnItem{
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode,
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "SparePartName",
                        IsReadOnly = true
                    //}, new KeyValuesColumnItem {
                    //    Name = "Status",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "CurrentStorage",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "StorageAfterInventory",
                        IsReadOnly = true
                    }
                    , new ColumnItem{
                        Name = "DealerPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_DealerPrice,
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "Memo",
                        Title = PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_Remark,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(Web.DealerPartsInventoryDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerPartsInventoryDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            ((GridViewDataColumn)this.GridView.Columns["DealerPrice"]).DataFormatString = "c2";
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var dataEditView = this.DataContext as DealerPartsInventoryBill;
            if(dataEditView == null)
                return;
            var serialNumber = 1;
            foreach(var dealerPartsInventoryDetail in dataEditView.DealerPartsInventoryDetails) {
                dealerPartsInventoryDetail.SerialNumber = serialNumber;
                serialNumber++;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
