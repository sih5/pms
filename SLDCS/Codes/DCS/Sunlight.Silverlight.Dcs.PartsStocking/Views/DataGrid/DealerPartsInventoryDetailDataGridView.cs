﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class DealerPartsInventoryDetailDataGridView : DcsDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(DealerPartsInventoryDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new ColumnItem[] { 
                    new ColumnItem{
                        Name = "SparePartCode",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName
                    }, new ColumnItem{
                        Name = "CurrentStorage",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_CurrentStorage
                    }, new ColumnItem{
                        Name = "StorageAfterInventory",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_StorageAfterInventory
                    }, new ColumnItem{
                        Name = "StorageDifference",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_StorageDifference
                    },
                     new ColumnItem{
                        Name = "DealerPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_DealerPrice
                    }, new ColumnItem{
                        Name = "Memo",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerPartsInventoryDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["DealerPrice"]).DataFormatString = "c2";
        }
    }
}
