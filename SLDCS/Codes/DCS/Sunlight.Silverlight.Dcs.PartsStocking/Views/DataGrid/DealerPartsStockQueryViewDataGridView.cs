﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class DealerPartsStockQueryViewDataGridView : DcsDataGridViewBase {


        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   
                    new ColumnItem {
                        Name = "DealerCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_DealerCode
                    },
                    new ColumnItem {
                        Name = "DealerName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_DealerName
                    },
                    new ColumnItem {
                        Name = "CustomerType"
                    },
                    new ColumnItem {
                        Name = "ProvinceName",
                    },
                    new ColumnItem {
                        Name = "PartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode
                    },
                    new ColumnItem {
                        Name = "PartName"
                    },
                    new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_Quantity
                    },
                    new ColumnItem {
                        Name = "SalesPrice"
                    },
                    new ColumnItem {
                        Name = "SalesPriceAmount"
                    },
                    new ColumnItem {
                        Name = "StockMaximum"
                    }, new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartsSalesCategoryName
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(DealerPartsStockQueryView);
            }
        }
        protected override string OnRequestQueryName() {
            return "GetDealerPartsStockQueryViews";
        }

    }
}