﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class DealerPartsStockQueryViewForWindowDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartCode
                    },
                    new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartName
                    },
                    new ColumnItem {
                        Name = "NoWarrantyBrandName",
                        Title = "保外品牌名称"
                    },
                    new ColumnItem {
                        Name = "ValidStock",
                        Title = "可用库存"
                    },
                    new ColumnItem {
                        Name = "NoWarrantyPrice",
                        Title = "保外销售价格"
                    },
                    new ColumnItem {
                        Name = "WarrantyBrandName",
                        Title = "保内品牌名称"
                    },
                    new ColumnItem {
                        Name = "WarrantyPrice",
                        Title = "保内销售价格"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualDealerPartsStockQueryView);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询经销商保内外调拨配件库存";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var compositeFilterItem = filters.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                switch(parameterName) {
                    case "warrantyBrandId":
                        return compositeFilterItem.Filters.Single(item => item.MemberName == "WarrantyBrandId").Value;
                    case "noWarrantyBrandId":
                        return compositeFilterItem.Filters.Single(item => item.MemberName == "NoWarrantyBrandId").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "PartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "PartName").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}