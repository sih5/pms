﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class DealerPartsStockServiceDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Company_Type"
        };

        public DealerPartsStockServiceDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "DealerCode",
                        Title =PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_StorageCompanyCode
                    }, new ColumnItem {
                        Name = "DealerName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_StorageCompanyName
                    }, new ColumnItem {
                        Name = "SalesCategoryName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SalesCategoryName
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePart.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartName
                    },new ColumnItem{
                        Name="Quantity",
                    },new ColumnItem{
                        Name="BasicSalePrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsSalesPrice
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询经销商配件批发价";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "dealerId":
                    var dealerFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "DealerId");
                    return dealerFilterItem == null ? null : dealerFilterItem.Value;
                case "salesCategoryId":
                    var categoryFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SalesCategoryId");
                    return categoryFilterItem == null ? null : categoryFilterItem.Value;
                case "sparePartId":
                    return null;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "DealerId", "SalesCategoryId", "SparePartId" };
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => !param.Contains(filter.MemberName))) {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}