﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class DealerPartsTransOrderDetailDataGridView : DcsDataGridViewBase {

        public DealerPartsTransOrderDetailDataGridView() {
            this.DataContextChanged += DealerPartsTransOrderDetailDataGridView_DataContextChanged;
        }

        private void DealerPartsTransOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealerPartsTransferOrder = e.NewValue as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null || dealerPartsTransferOrder.Id == default(int))
                return;
            var filterItem = new FilterItem();
            filterItem.MemberName = "DealerPartsTransferOrderId";
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.MemberType = typeof(int);
            filterItem.Value = dealerPartsTransferOrder.Id;
            this.FilterItem = filterItem;
            this.ExecuteQueryDelayed();
        }



        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                    },
                    new ColumnItem {
                        Name = "SparePartName",
                    },
                    new ColumnItem {
                        Name = "NoWarrantyPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                    },
                    new ColumnItem {
                        Name = "Amount"
                    },
                    new ColumnItem {
                        Name = "WarrantyPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                    },
                    new ColumnItem {
                        Name = "SumNoWarrantyPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = "保外销售价格合计",
                    },
                    new ColumnItem {
                        Name = "SumWarrantyPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = "保内销售价格合计",
                    },
                    new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsTransOrderDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerPartsTransOrderDetails";
        }


        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            //this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            //((GridViewDataColumn)this.GridView.Columns["InspectedQuantity"]).DataFormatString = "d";
            //((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "c2";
        }
    }
}