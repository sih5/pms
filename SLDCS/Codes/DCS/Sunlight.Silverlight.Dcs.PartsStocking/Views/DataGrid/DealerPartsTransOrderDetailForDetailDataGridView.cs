﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class DealerPartsTransOrderDetailForDetailDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    },
                    new ColumnItem {
                        Name = "SparePartName"
                    },
                    new ColumnItem {
                        Name = "Amount"
                    },
                    new ColumnItem {
                        Name = "Stock",
                        Title = "可用库存"
                    },
                    new ColumnItem {
                        Name = "NoWarrantyPrice",
                        MaskType = MaskType.Numeric
                    },
                    new ColumnItem {
                        Name = "WarrantyPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "SumNoWarrantyPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = "保外销售价格合计"
                    },
                    new ColumnItem {
                        Name = "SumWarrantyPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = "保内销售价格合计"
                    },
                    new ColumnItem {
                        Name = "DiffPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsTransOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerPartsTransOrderDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
        }
    }
}