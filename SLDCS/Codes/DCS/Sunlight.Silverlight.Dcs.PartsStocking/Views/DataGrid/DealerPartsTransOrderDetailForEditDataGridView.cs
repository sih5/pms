﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class DealerPartsTransOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private RadWindow radQueryWindow;

        private QueryWindowBase dealerPartsStockQueryView;

        private QueryWindowBase DealerPartsStockQueryView {
            get {
                if(this.dealerPartsStockQueryView == null) {
                    this.dealerPartsStockQueryView = DI.GetQueryWindow("DealerPartsStockQueryView");
                    this.dealerPartsStockQueryView.SelectionDecided += dealerPartsStockQueryView_SelectionDecided;
                    this.dealerPartsStockQueryView.Loaded += dealerPartsStockQueryView_Loaded;
                }
                return this.dealerPartsStockQueryView;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                    },
                    new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true,
                    },
                    new ColumnItem {
                        Name = "Amount"
                    },
                    new ColumnItem {
                        Name = "Stock",
                        IsReadOnly = true,
                        Title = "可用库存"
                    },
                    new ColumnItem {
                        Name = "NoWarrantyPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true,
                    },
                    new ColumnItem {
                        Name = "WarrantyPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true,
                    },
                    new ColumnItem {
                        Name = "SumNoWarrantyPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = "保外销售价格合计",
                        IsReadOnly = true,
                    },
                    new ColumnItem {
                        Name = "SumWarrantyPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = "保内销售价格合计",
                        IsReadOnly = true,
                    },
                    new ColumnItem {
                        Name = "DiffPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true,
                    },
                    new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsTransOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerPartsTransOrderDetails");
        }

        private void dealerPartsStockQueryView_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null)
                return;
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("NoWarrantyBrandId", typeof(int), FilterOperator.IsEqualTo, dealerPartsTransferOrder.NoWarrantyBrandId));
            compositeFilterItem.Filters.Add(new FilterItem("WarrantyBrandId", typeof(int), FilterOperator.IsEqualTo, dealerPartsTransferOrder.WarrantyBrandId));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "NoWarrantyBrandName", dealerPartsTransferOrder.NoWarrantyBrandName
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "WarrantyBrandName", dealerPartsTransferOrder.WarrantyBrandName
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "NoWarrantyBrandName", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "WarrantyBrandName", false
            });
        }

        private void dealerPartsStockQueryView_SelectionDecided(object sender, EventArgs e) {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var virtualDealerPartsStockQueryView = queryWindow.SelectedEntities.Cast<VirtualDealerPartsStockQueryView>().FirstOrDefault();
            if(virtualDealerPartsStockQueryView == null)
                return;
            if(dealerPartsTransferOrder.DealerPartsTransOrderDetails.Any(r => r.SparePartId == virtualDealerPartsStockQueryView.SparePartId)) {
                UIHelper.ShowNotification(string.Format("配件{0}已存在", virtualDealerPartsStockQueryView.SparePartName));
                return;
            }
            var dealerPartsTransOrderDetail = new DealerPartsTransOrderDetail();
            dealerPartsTransOrderDetail.PropertyChanged += dealerPartsTransOrderDetail_PropertyChanged;
            dealerPartsTransOrderDetail.SparePartId = virtualDealerPartsStockQueryView.SparePartId;
            dealerPartsTransOrderDetail.SparePartCode = virtualDealerPartsStockQueryView.SparePartCode;
            dealerPartsTransOrderDetail.SparePartName = virtualDealerPartsStockQueryView.SparePartName;
            dealerPartsTransOrderDetail.WarrantyPrice = virtualDealerPartsStockQueryView.WarrantyPrice;
            dealerPartsTransOrderDetail.NoWarrantyPrice = virtualDealerPartsStockQueryView.NoWarrantyPrice;
            dealerPartsTransOrderDetail.Stock = (int)(virtualDealerPartsStockQueryView.ValidStock ?? 0);
            dealerPartsTransOrderDetail.DiffPrice = (dealerPartsTransOrderDetail.NoWarrantyPrice ?? 0) - (dealerPartsTransOrderDetail.WarrantyPrice ?? 0);
            dealerPartsTransferOrder.DealerPartsTransOrderDetails.Add(dealerPartsTransOrderDetail);
            dealerPartsTransferOrder.DiffPrice = dealerPartsTransferOrder.DealerPartsTransOrderDetails.Sum(r => r.DiffPrice);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void dealerPartsTransOrderDetail_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            var dealerPartsTransOrderDetail = sender as DealerPartsTransOrderDetail;
            if(dealerPartsTransOrderDetail == null)
                return;
            switch(e.PropertyName) {
                case "Amount":
                    dealerPartsTransOrderDetail.SumWarrantyPrice = dealerPartsTransOrderDetail.Amount * (dealerPartsTransOrderDetail.WarrantyPrice ?? 0);
                    dealerPartsTransOrderDetail.SumNoWarrantyPrice = dealerPartsTransOrderDetail.Amount * (dealerPartsTransOrderDetail.NoWarrantyPrice ?? 0);
                    dealerPartsTransferOrder.SumWarrantyPrice = dealerPartsTransferOrder.DealerPartsTransOrderDetails.Sum(r => r.SumWarrantyPrice);
                    dealerPartsTransferOrder.SumNoWarrantyPrice = dealerPartsTransferOrder.DealerPartsTransOrderDetails.Sum(r => r.SumNoWarrantyPrice);
                    break;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.RowLoaded -= GridView_RowLoaded;
            this.GridView.RowLoaded += GridView_RowLoaded;
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var dealerPartsTransOrderDetail = sender as DealerPartsTransOrderDetail;
            if(dealerPartsTransOrderDetail == null)
                return;
            dealerPartsTransOrderDetail.PropertyChanged -= dealerPartsTransOrderDetail_PropertyChanged;
            dealerPartsTransOrderDetail.PropertyChanged += dealerPartsTransOrderDetail_PropertyChanged;
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            dealerPartsTransferOrder.SumWarrantyPrice = dealerPartsTransferOrder.DealerPartsTransOrderDetails.Sum(r => r.SumWarrantyPrice);
            dealerPartsTransferOrder.SumNoWarrantyPrice = dealerPartsTransferOrder.DealerPartsTransOrderDetails.Sum(r => r.SumNoWarrantyPrice);
            dealerPartsTransferOrder.DiffPrice = dealerPartsTransferOrder.DealerPartsTransOrderDetails.Sum(r => r.DiffPrice);
        }

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.DealerPartsStockQueryView,
                    Header = "查询经销商保内外调拨配件库存",
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var dealerPartsTransferOrder = this.DataContext as DealerPartsTransferOrder;
            if(dealerPartsTransferOrder == null)
                return;
            if(dealerPartsTransferOrder.BranchId == default(int)) {
                UIHelper.ShowNotification("请先选择分公司");
                return;
            }
            if(dealerPartsTransferOrder.NoWarrantyBrandId == default(int)) {
                UIHelper.ShowNotification("请先选择保外品牌");
                return;
            }
            if(dealerPartsTransferOrder.WarrantyBrandId == default(int)) {
                UIHelper.ShowNotification("请先选择保内品牌");
                return;
            }
            RadQueryWindow.ShowDialog();
        }
    }
}