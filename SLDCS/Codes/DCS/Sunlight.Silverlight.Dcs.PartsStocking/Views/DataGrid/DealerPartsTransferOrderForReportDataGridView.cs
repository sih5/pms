﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class DealerPartsTransferOrderForReportDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }
        private readonly string[] kvNames = {
            "DealerPartsTransferOrderStatus"
        };

        public DealerPartsTransferOrderForReportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {    
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code",
                    },  new ColumnItem {
                        Name = "NoWarrantyBrandName",
                        Title = "保外品牌"
                    },  new ColumnItem {
                        Name = "WarrantyBrandName",
                        Title = "保内品牌"
                    },  new ColumnItem {
                        Name = "SourceCode"
                    },  new ColumnItem {
                        Name = "SumNoWarrantyPrice"
                    },  new ColumnItem {
                        Name = "SumWarrantyPrice"
                    },  new ColumnItem {
                        Name = "DiffPrice",
                        Title = "差异金额(保外-保内)"
                    },   new ColumnItem {
                        Name = "Remark"
                    },  new ColumnItem {
                        Name = "CreatorName"
                    },  new ColumnItem {
                        Name = "CreateTime"
                    },  new ColumnItem {
                        Name = "AbandonerName"
                    },  new ColumnItem {
                        Name = "AbandonTime"
                    },  new ColumnItem {
                        Name = "ModifierName"
                    },  new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "DealerPartsTransOrderDetail"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerPartsTransferOrder);
            }
        }
        protected override string OnRequestQueryName() {
            return "GetDealerPartsTransferOrders";
        }

    }
}