﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class EnterprisePartsCostDataGridView : DcsDataGridViewBase {
        private readonly string[] kvName = new[] {
            "ABCStrategy_Category"
        };
         public EnterprisePartsCostDataGridView() {
            this.KeyValueManager.Register(this.kvName);
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CostAmount"]).DataFormatString = "c2";
        }


        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }
        }

        protected override string OnRequestQueryName() {
            return "查询企业配件成本";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_DealerPartsStockQueryView_PartCode,
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_SparePartName,
                        Name = "SparePartName"
                    },new KeyValuesColumnItem {
                        Name = "PartABC",
                        KeyValueItems = this.KeyValueManager[this.kvName[0]],
                        Title = PartsStockingUIStrings.QueryPanel_Title_PartABC
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBillDetail_OutboundAmount,
                        Name = "Quantity"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CostPrice,
                        Name = "CostPrice"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CostAmount,
                        Name = "CostAmount"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreatorName,
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime,
                        Name = "CreateTime",
                        MaskType = Sunlight.Silverlight.Core.Model.MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName,
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime,
                        Name = "ModifyTime",
                        MaskType = Sunlight.Silverlight.Core.Model.MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_PartsSalesCategoryName,
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if (compositeFilterItem == null)
            {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            }
            else
            {
                var param = new[] { "QuantityMoreThanZero"};
                foreach (var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName)))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {                
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                    case "partABC":
                        return filters.Filters.Single(item => item.MemberName == "PartABC").Value;
                    case "quantity":
                        return filters.Filters.Single(item => item.MemberName == "QuantityMoreThanZero").Value;
                    case "costPrice":
                        return null;
                    case "costAmount":
                        return null;
                    case "bCreateTime":
                        var createTime = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                    case "bModifyTime":
                        var modifyTime = date.FirstOrDefault(p => p.Filters.First().MemberName == "ModifyTime");
                        return modifyTime == null ? null : modifyTime.Filters.First(r => r.MemberName == "ModifyTime").Value;
                    case "eModifyTime":
                        var modifyTime1 = date.FirstOrDefault(p => p.Filters.First().MemberName == "ModifyTime");
                        return modifyTime1 == null ? null : modifyTime1.Filters.Last(item => item.MemberName == "ModifyTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualEnterprisePartsCost);
            }
        }

    }
}