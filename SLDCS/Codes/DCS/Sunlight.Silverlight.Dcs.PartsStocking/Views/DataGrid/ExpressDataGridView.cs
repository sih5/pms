﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class ExpressDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MasterData_Status"
        };
        protected override Type EntityType {
            get {
                return typeof(Express);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        public ExpressDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Status
                    }
                    ,  new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressCode,
                        Name = "ExpressCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressName,
                        Name = "ExpressName"
                    }, new ColumnItem {
                        Name = "Contacts",
                       Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Contacts
                    }, new ColumnItem {
                        Name = "ContactsNumber",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ContactsNumber
                    }, new ColumnItem {
                        Name = "Address",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Address
                    }, new ColumnItem {
                        Name = "E_Mail",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_EMail
                    }, new ColumnItem {
                        Name = "FixedTelephone",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_FixedTelephone
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime
                    }, new ColumnItem {
                        Name = "CancelName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Abandoner
                    }, new ColumnItem {
                        Name = "CancelTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_AbandonTime
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetExpresses";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }
    }
}
