﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class ExpressToLogisticsForImportForEditForEditDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="ExpressCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressCode
                    },new ColumnItem{
                        Name="FocufingCoreName",
                       Title =PartsStockingUIStrings.DataGridView_ColumnItem_Title_ExpressToLogistic_FocufingCoreName
                    },new ColumnItem{
                        Name="LogisticsCompanyCode",
                       Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_ExpressToLogistic_LogisticsCompanyCode
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(ExpressToLogistic);
            }
        }
    }
}
