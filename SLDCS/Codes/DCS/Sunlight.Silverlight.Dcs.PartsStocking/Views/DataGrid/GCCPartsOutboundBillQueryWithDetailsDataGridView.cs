﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class GCCPartsOutboundBillQueryWithDetailsDataGridView  : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Parts_OutboundType", "Parts_SettlementStatus","Company_Type","ERPInvoiceInformation_Type"
        };

        public GCCPartsOutboundBillQueryWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "SettlementStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                          Title = Utils.GetEntityLocalizedName(typeof(PartsOutboundBill),"SettlementStatus")
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_Code,
                        Name = "Code"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseCode,
                        Name = "WarehouseCode"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_WarehouseName,
                        Name = "WarehouseName"
                    },new KeyValuesColumnItem {
                        Name = "OutboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = Utils.GetEntityLocalizedName(typeof(PartsOutboundBill),"OutboundType")
                    },new ColumnItem{
                        Name="TotalAmount",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount
                    },new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_OriginalRequirementBillCode
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_SalesOrderType,
                        Name = "PartsSalesOrderTypeName"
                    },new ColumnItem {
                        Name = "PartsOutboundPlanCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_PartsOutboundPlanCode
                    },new ColumnItem {
                        Name = "CounterpartCompanyCode",
                          Title = Utils.GetEntityLocalizedName(typeof(PartsOutboundBill),"CounterpartCompanyCode")
                    },new ColumnItem {
                        Name = "CounterpartCompanyName",
                          Title = Utils.GetEntityLocalizedName(typeof(PartsOutboundBill),"CounterpartCompanyName")
                    },new KeyValuesColumnItem {
                        Name = "CompanyType",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_CompanyType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_ReceivingWarehouseCode,
                        Name = "ReceivingWarehouseCode"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_ReceivingWarehouseName,
                        Name = "ReceivingWarehouseName"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_ProvinceName,
                        Name = "ProvinceName"
                    },new ColumnItem {
                        Name  = "OutboundPackPlanCode",
                          Title = Utils.GetEntityLocalizedName(typeof(PartsOutboundBill),"OutboundPackPlanCode")
                    },new ColumnItem {
                        Name  = "ContractCode",
                          Title = Utils.GetEntityLocalizedName(typeof(PartsOutboundBill),"ContractCode")
                    },new ColumnItem {
                        Name  = "GPMSPurOrderCode",
                          Title = Utils.GetEntityLocalizedName(typeof(PartsOutboundBill),"GPMSPurOrderCode")
                    },new ColumnItem {
                        Name = "InterfaceRecordId",
                          Title = Utils.GetEntityLocalizedName(typeof(PartsOutboundBill),"InterfaceRecordId")
                    },
                    new KeyValuesColumnItem {
                        Name = "InvoiceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]],
                          Title = PartsStockingUIStrings.QueryPanel_QueryItem_InvoiceType
                    },new ColumnItem {
                        Name = "ERPSourceOrderCode",
                          Title =PartsStockingUIStrings.QueryPanel_Title_ERPsourceCode
                    },new ColumnItem {
                        Name = "Remark",
                          Title =PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_Remark
                    }, new ColumnItem {
                        Name = "OrderApproveComment",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_OrderApproveComment
                    },new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreatorName
                    },new ColumnItem {
                        Name = "CreateTime",
                          Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreateTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ModifierName",
                          Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    },new ColumnItem {
                        Name = "ModifyTime",
                          Title =PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_PartsSalesCategoryName
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_BranchName,
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "工程车配件出库单查询";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOutboundBillWithOtherInfo);
            }
        }

        //protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
        //    get {
        //        return new TabControlRowDetailsTemplateProvider {
        //            DetailPanelNames = new[] {
        //                "PartsOutboundBillDetail","PartsShippingOrderForPartsOutbound"
        //            }
        //        };
        //    }
        //}


        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            //var filters = this.FilterItem as CompositeFilterItem;
            switch(parameterName) {
                case "operatorId":
                    return BaseApp.Current.CurrentUserData.UserId;
                default:
                    break;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        //protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
        //    var compositeFilterItem = this.FilterItem as CompositeFilterItem;
        //    var newCompositeFilterItem = new CompositeFilterItem();
        //    if(compositeFilterItem != null) {
        //        var param = new[] { "operatorId", "Code", "PartsSalesCategoryId", "WarehouseId", 
        //            "OutboundType", "CounterpartCompanyCode", "CounterpartCompanyName", 
        //            "SettlementStatus", "PartsSalesOrderTypeName", "PartsOutboundPlanCode","StarCreateTime", 
        //            "EndCreateTime", "ContractCode", "ProvinceName", "ERPSourceOrderCode"};
        //        foreach(var filterItem in compositeFilterItem.Filters.Where(filter => !param.Contains(filter.MemberName))) {
        //            newCompositeFilterItem.Filters.Add(filterItem);
        //        }
        //    }
        //    return newCompositeFilterItem.ToFilterDescriptor();
        //}

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 100;
            //((GridViewDataColumn)this.GridView.Columns["SumAmount"]).DataFormatString = "d";
        }

    }
}
