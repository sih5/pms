﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class HauDistanceInforForImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "StorageCompanyCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_Company1_Code
                    }, new ColumnItem {
                        Name = "StorageCompanyName",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_Company1_Name
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        Title=PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsOutboundBill_WarehouseName
                    },  new ColumnItem {
                        Name = "CompanyCode",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_Company_Code
                    }, new ColumnItem { 
                        Name = "CompanyName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_Company_Name
                    }, new ColumnItem {
                        Name = "DetailAddress",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyAddress_DetailAddress
                    }, new ColumnItem {
                        Name = "HauDistance",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_HauDistance
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(HauDistanceInforExtend);
            }
        }
    }
}
