﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class InboundHistoryQueryDataGridView : DcsDataGridViewBase {
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }
        private readonly string[] kvNames = new[] {
            "Parts_InboundType", "Parts_SettlementStatus","PartsPurchaseOrder_OrderType"
        };

        public InboundHistoryQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {    
                    new KeyValuesColumnItem {
                        Name = "settlementstatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.QueryPanel_Title_SettlementStatus
                    },new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_WarehouseCode
                    },  new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_WarehouseName
                    },  new ColumnItem {
                        Name = "PartsInboundOrderCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_InboundBill_Code          
                    },  new KeyValuesColumnItem {
                        Name = "InboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_InboundType              
                    },  new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_SourceCode
                    },  new KeyValuesColumnItem {
                        Name = "PartsPurchaseOrderTypeId",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_PurchaseOrderType,
                         KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                     },  new ColumnItem {
                        Name = "CompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CompanyCode
                    },  new ColumnItem {
                        Name = "CompanyName",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_CompanyName           
                    },  new ColumnItem {
                        Name = "Createtime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Createtime
                    },  new ColumnItem {
                        Name = "PartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartCode
                    },  new ColumnItem {
                        Name = "PartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartName              
                    },  new ColumnItem {
                        Name = "InQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsAdjustDetail_InboundAmount                       
                    },  new ColumnItem {
                        Name = "CostPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsPurchasePricing            
                    }, new ColumnItem {
                        Name = "TotalJH",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TotalJH            
                    } , new ColumnItem {
                        Name = "Price",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsPlannedPrice            
                    }, new ColumnItem {
                        Name = "Total",
                        Title = "计划金额"             
                    }, new ColumnItem {
                        Name = "JsdNum",
                        Title = "结算单号"             
                    
                    }  ,  new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = "品牌"
                    }                                
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualInboundHistoryQuery);
            }
        }

        protected override string OnRequestQueryName() {
            return "历史入库记录查询1";
        }

        //protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
        //    var compositeFilterItem = this.FilterItem as CompositeFilterItem;
        //    var newCompositeFilterItem = new CompositeFilterItem();

        //    if(compositeFilterItem == null) {
        //        if(this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId" && this.FilterItem.MemberName != "CompanyId")
        //            newCompositeFilterItem.Filters.Add(this.FilterItem);
        //    } 
        //    return newCompositeFilterItem.ToFilterDescriptor();
        //}

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "branchId":
                        return filters.Filters.Single(item => item.MemberName == "BranchId").Value;
                    case "partsInboundOrderCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsInboundOrderCode").Value;
                    case "inboundType":
                        return filters.Filters.Single(item => item.MemberName == "InboundType").Value;
                    case "partsSalesCategoryId":
                        return filters.Filters.Single(item => item.MemberName == "PartsSalesCategoryId").Value;
                    case "partsPurchaseOrderTypeId":
                        return filters.Filters.Single(item => item.MemberName == "PartsPurchaseOrderTypeId").Value;
                    case "originalRequirementBillCode":
                        return filters.Filters.Single(item => item.MemberName == "OriginalRequirementBillCode").Value;
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "settlementstatus":
                        return filters.Filters.Single(item => item.MemberName == "Settlementstatus").Value;
                    case "partCode":
                        return filters.Filters.Single(item => item.MemberName == "PartCode").Value;
                    case "partName":
                        return filters.Filters.Single(item => item.MemberName == "PartName").Value;
                    case "companyName":
                        return filters.Filters.Single(item => item.MemberName == "CompanyName").Value;
                    case "bCreateTime":
                        var createTime = filters.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                       return createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                       var createTime1 = filters.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                       return createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                     
                    }
                }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
