﻿
using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class LocketUnitQueryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsOutboundPlan_Status","Parts_OutboundType"
        };

        public LocketUnitQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                  new KeyValuesColumnItem {
                      Name = "Status",
                      KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                  },new ColumnItem {
                      Name = "Code"
                  },new KeyValuesColumnItem {
                      Name = "OutboundType",
                      KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                  },new ColumnItem {
                      Name = "SparePartCode"
                  },new ColumnItem {
                      Name = "SparePartName"
                  },new ColumnItem {
                      Name = "LocketQty"
                  },new ColumnItem {
                      Name = "CreatorName"
                  },new ColumnItem {
                      Name = "CreateTime"
                  },new ColumnItem {
                      Name = "WarehouseCode"
                  },new ColumnItem {
                      Name = "WarehouseName"
                  },new ColumnItem {
                      Name = "CounterpartCompanyCode"
                  },new ColumnItem {
                      Name = "CounterpartCompanyName"
                  }
              };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(LocketUnitQuery);
            }
        }
        protected override string OnRequestQueryName() {
            return "GetLocketUnitQueries";
        }
    }
}
