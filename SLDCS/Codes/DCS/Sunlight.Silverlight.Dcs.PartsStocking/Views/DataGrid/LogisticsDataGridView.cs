﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System.Linq;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class LogisticsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsShipping_Method", "PartsSalesOrder_Status"
        };

        private ObservableCollection<KeyValuePair> kvSignStatus = new ObservableCollection<KeyValuePair>();

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "OrderCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_Code
                    },
                    new KeyValuesColumnItem {
                        Name = "PartsSalesOrderStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.DataEditView_Text_SalesOrderType
                    },
                    new ColumnItem {
                        Name = "ApproveTime",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_ApproveTime
                    },
                    new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_WarehouseName
                    },
                    new ColumnItem {
                        Name = "ReceivingAddress",
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ReceivingAddress
                    },
                    new ColumnItem {
                        Name = "ShippingDate",
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingDate
                    },
                    new ColumnItem {
                        Name = "ShippingCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShippingOrder_Code
                    },
                    new ColumnItem {
                        Name = "RequestedArrivalDate",
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_RequestedArrivalDate
                    },
                    new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingMethod
                    },
                    new ColumnItem {
                        Name = "LogisticName",
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_LogisticName
                    },
                    new ColumnItem {
                        Name = "TransportDriverPhone",
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_TransportDriverPhone
                    },
                    new KeyValuesColumnItem {
                        Name = "SignStatus",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_SignStatus,
                        KeyValueItems = kvSignStatus
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualLogistic);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetLogisticsWithDetail";
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider() {
                    DetailPanelNames = new[] {
                        "LogisticsDetail"
                    }
                };
            }
        }

        public LogisticsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.Loaded += LogisticsDataGridView_Loaded;
            this.DataLoaded += LogisticsDataGridView_DataLoaded;
        }

        private void LogisticsDataGridView_DataLoaded(object sender, EventArgs e) {
            if(this.GridView.Items.Count == 0)
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_WaitShipping);
        }


        private void LogisticsDataGridView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            kvSignStatus.Clear();
            kvSignStatus.Add(new KeyValuePair {
                Key = 1,
                Value = PartsStockingUIStrings.QueryPanel_QueryItem_InLine
            });
            kvSignStatus.Add(new KeyValuePair {
                Key = 2,
                Value = PartsStockingUIStrings.QueryPanel_QueryItem_Received
            });
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["ApproveTime"]).DataFormatString = "yyyy年MM月dd日 HH时mm分";
            ((GridViewDataColumn)this.GridView.Columns["ShippingDate"]).DataFormatString = "yyyy年MM月dd日 HH时mm分";
            ((GridViewDataColumn)this.GridView.Columns["RequestedArrivalDate"]).DataFormatString = "yyyy年MM月dd日 HH时mm分";

            this.KeyValueManager.LoadData();

        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            var signStatusFilterItem = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SignStatus");
            switch(parameterName) {
                case "signStatus":
                    return signStatusFilterItem == null ? null : signStatusFilterItem.Value;
                case "isFilterItemNull":
                    return false;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => filter.MemberName != "SignStatus")) {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }

}