﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using System.Linq;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class LogisticsDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { "PartsShipping_Method" };
        public LogisticsDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += LogisticsDetailDataGridView_DataContextChanged;
        }

        private void LogisticsDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var logistics = e.NewValue as VirtualLogistic;
            if(logistics == null || logistics.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Logisticsid",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = logistics.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    //new ColumnItem {
                    //    Name = "Number",
                    //    Title="序号",
                    //    TextAlignment= TextAlignment.Center
                    //},
                    new ColumnItem {
                        Name = "PointTime",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_PointTime
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        Title=PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingMethod,
                         KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    }, new ColumnItem {
                        Name = "PointName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_PointName
                    }, new ColumnItem {
                        Name = "ScanTime",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_ScanTime
                    }, new ColumnItem{
                        Name = "ScanName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_ScanName
                    }, new ColumnItem{
                        Name = "Signatory",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Signatory
                    }, new ColumnItem {
                        Name = "Remark",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(LogisticsDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询物流信息清单";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["PointTime"]).DataFormatString = "yyyy年MM月dd日 HH时mm分";
            ((GridViewDataColumn)this.GridView.Columns["ScanTime"]).DataFormatString = "yyyy年MM月dd日 HH时mm分";
        }
        //protected override void OnDataLoaded() {
        //    var logisticsDetials = this.Entities.Cast<LogisticsDetail>().ToArray();
        //    if(logisticsDetials == null)
        //        return;
        //    int number = 1;
        //    foreach(var item in logisticsDetials)
        //        item.Number = number++;


        //}
    }
}
