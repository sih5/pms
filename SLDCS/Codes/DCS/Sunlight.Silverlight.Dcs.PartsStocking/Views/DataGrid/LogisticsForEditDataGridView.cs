﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class LogisticsForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
                                               "PartsShipping_Method"
                                           };
        public LogisticsForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Number",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Common_SerialNumber,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "PointTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_PointTime,
                        FormatString = ""
                    },
                    new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingMethod,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    },
                    new ColumnItem {
                        Name = "PointName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_PointName
                    },
                    new ColumnItem {
                        Name = "ScanTime",
                        Title =PartsStockingUIStrings.DataGridView_ColumnItem_ScanTime
                    },
                    new ColumnItem {
                        Name = "ScanName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ScanName
                    },
                    new ColumnItem {
                        Name = "Signatory",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Signatory
                    },
                    new ColumnItem {
                        Name = "Remark",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    }
                };
            }
        }


        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("LogisticsDetails");
        }

        protected override Type EntityType {
            get {
                return typeof(LogisticsDetail);
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DataPager.PageSize = 15;
            //this.GridView.CanUserInsertRows = true;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;

            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["PointTime"]).DataFormatString = "yyyy年MM月dd日 HH时mm分";
            ((GridViewDataColumn)this.GridView.Columns["ScanTime"]).DataFormatString = "yyyy年MM月dd日 HH时mm分";

            this.GridView.Deleting += GridView_Deleting;

            this.GridView.PreparingCellForEdit += GridView_PreparingCellForEdit;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.Deleted += GridView_Deleted;
        }

        void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var logisticsDetail = e.Row.DataContext as LogisticsDetail;
            if(logisticsDetail == null)
                return;
            //string fields = new string[] { "PointTime", "PointName", "ScanTime", "ScanName", "Signatory", "Remark" };

            e.Cancel = true;
            switch(e.Cell.Column.UniqueName) {
                case "PointTime":
                    e.Cancel = logisticsDetail.PointTime != null;
                    break;
                case "PointName":
                    e.Cancel = logisticsDetail.PointName != null && !string.IsNullOrWhiteSpace(logisticsDetail.PointName);
                    break;
                case "ScanTime":
                    e.Cancel = logisticsDetail.ScanTime != null;
                    break;
                case "ScanName":
                    e.Cancel = logisticsDetail.ScanName != null && !string.IsNullOrWhiteSpace(logisticsDetail.ScanName);
                    break;
                case "Signatory":
                    e.Cancel = logisticsDetail.Signatory != null && !string.IsNullOrWhiteSpace(logisticsDetail.Signatory);
                    break;
                case "Remark":
                    e.Cancel = logisticsDetail.Remark != null && !string.IsNullOrWhiteSpace(logisticsDetail.Remark);
                    break;
                case "ShippingMethod":
                    e.Cancel = false;
                    break;
            }


        }


        void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var logistic = this.DataContext as Logistic;
            if(logistic == null)
                return;
            int SequeueNumber = 1;
            foreach(var item in logistic.LogisticsDetails)
                item.Number = SequeueNumber++;

        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var logistic = this.DataContext as Logistic;
            if(logistic == null)
                return;
            int SequeueNumber = 1;
            foreach(var item in logistic.LogisticsDetails)
                item.Number = SequeueNumber++;
            //this.GridView.CanUserInsertRows = !logistic.LogisticsDetails.Any(r => string.IsNullOrWhiteSpace(r.PointName) || !r.PointTime.HasValue);
        }


        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            ;
        }

        private void GridView_PreparingCellForEdit(object sender, GridViewPreparingCellForEditEventArgs e) {
            if(e.Column.UniqueName.Equals("PointTime") || e.Column.UniqueName.Equals("ScanTime")) {
                var logisticsDetail = e.Row.DataContext as LogisticsDetail;
                if(logisticsDetail == null)
                    return;
                var date = e.EditingElement as RadDateTimePicker;
                date.InputMode = InputMode.DateTimePicker;
                date.DisplayFormat = DateTimePickerFormat.Long;
                date.TimeInterval = TimeSpan.FromMinutes(20);
            }

        }


        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var logistic = this.DataContext as Logistic;
            if(logistic == null || logistic.LogisticsDetails == null)
                return;
            int SequeueNumber = 1;
            foreach(var item in logistic.LogisticsDetails)
                item.Number = SequeueNumber++;

        }

    }
}
