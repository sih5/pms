﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OutboundAndInboundBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsLogisticBatchBillDetail_BillType"
        };
        public OutboundAndInboundBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.IsReadOnly = true;
        }


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "BillCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCode
                    }, new KeyValuesColumnItem {
                        Name = "BillType",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "SettlementAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_SettlementAmount
                    }, new ColumnItem {
                        Name = "BillCreateTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_Warehouse
                    }
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(OutboundAndInboundBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询待结算出入库明细";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            var compositeFilterItem = filter.Filters.SingleOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
            if(compositeFilterItem == null)
                return default(int);
            switch(parameterName) {
                case "storageCompanyId":
                    return BaseApp.Current.CurrentUserData.EnterpriseId;
                case "customerAccountId":
                    var customerAccount = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "CustomerAccountId");
                    return customerAccount != null ? customerAccount.Value : null;
                case "counterpartCompanyId":
                    var counterpartCompany = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "CustomerCompanyId");
                    return counterpartCompany != null ? counterpartCompany.Value : default(int);
                case "endTime":
                    var endTimeFilters = filter.Filters.FirstOrDefault(item => item.MemberName != null && item.MemberName == "EndTime");
                    if(endTimeFilters != null) {
                        var dateTime = endTimeFilters.Value as DateTime?;
                        if(dateTime.HasValue)
                            return new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                    }
                    return null;
                case "warehouseId":
                    var warehouse = filter.Filters.SingleOrDefault(e => e.MemberName == "WarehouseName");
                    return warehouse != null ? warehouse.Value : null;
                case "inboundTypes":
                    var inboundTypes = new[]{
                        (int)DcsPartsInboundType.销售退货
                    };
                    return inboundTypes;
                case "outboundTypes":
                    var outboundTypes = new[]{
                        (int)DcsPartsOutboundType.配件销售
                    };
                    return outboundTypes;
                case "cpPartsInboundCheckCodeNull":
                    var cpPartsInboundCheckCodeNull = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "cpPartsInboundCheckCodeNull");
                    if (cpPartsInboundCheckCodeNull == null)
                        return false;
                    else
                        return true;

            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var newCompositeFilterItem = new CompositeFilterItem();
                var compositeFilters = compositeFilterItem.Filters.Where
                    (item => (item.GetType() == typeof(FilterItem) && item.MemberName != "WarehouseName" && item.MemberName != "EndTime" && item.MemberName != "CustomerCompanyId" && item.MemberName != "CustomerAccountId" && item.MemberName != "cpPartsInboundCheckCodeNull"));
                var filterItems = compositeFilters as FilterItem[] ?? compositeFilters.ToArray();
                if(filterItems.Any())
                    foreach(var item in filterItems)
                        newCompositeFilterItem.Filters.Add(item);
                return newCompositeFilterItem.ToFilterDescriptor();
            }
            return this.FilterItem.ToFilterDescriptor();
        }


    }
}
