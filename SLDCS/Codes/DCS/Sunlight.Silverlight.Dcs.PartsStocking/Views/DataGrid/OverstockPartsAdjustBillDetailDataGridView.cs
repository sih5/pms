﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsAdjustBillDetailDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Key",
                        IsReadOnly = true,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Custom_BillCode
                    }, new ColumnItem {
                        Name = "Value",
                        IsReadOnly = true,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Custom_BillType
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(KeyValuePair<string, string>);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("OverstockPartsAdjustDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
