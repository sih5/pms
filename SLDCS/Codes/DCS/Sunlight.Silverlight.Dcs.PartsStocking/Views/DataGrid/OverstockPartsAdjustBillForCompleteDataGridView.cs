﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsAdjustBillForCompleteDataGridView : OverstockPartsAdjustBillWithDetailsDataGridView {
        protected override string OnRequestQueryName() {
            return "GetOverstockPartsAdjustBillsByDestStorageCompanyId";
        }
    }
}
