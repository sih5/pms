﻿
using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsAdjustBillForCompleteDetailDataGridView : DcsDataGridViewBase {

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[]{
                    new ColumnItem{
                        Name="SparePartCode",
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="SparePartName",
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="Quantity",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_SparePartStatisticsForKanBan_PartsAdjustAmount,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="finishQuantity",
                        Title=PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_finishQuantity,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="Remark",
                        IsReadOnly=false
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockPartsAdjustDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("OverstockPartsAdjustDetails");
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetOverstockPartsAdjustDetails";
        }
    }
}
