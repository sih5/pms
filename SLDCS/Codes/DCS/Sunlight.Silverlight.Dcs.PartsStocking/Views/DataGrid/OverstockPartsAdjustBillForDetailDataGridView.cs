﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsAdjustBillForDetailDataGridView : DcsDataGridViewBase {

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[]{
                    new ColumnItem{
                        Name="SparePartCode",
                        IsReadOnly=true,
                        Title =PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartCode
                    },new ColumnItem{
                        Name="SparePartName",
                        IsReadOnly=true,
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehousePartsStock_SparePartName
                    },new ColumnItem{
                        Name="TransferQty",
                        IsReadOnly=false,
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_SparePartStatisticsForKanBan_PartsTransferAmount
                    },new ColumnItem{
                        Name="RetailPrice",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_RetailguidePrice,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="DiscountRate",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountRate,
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="DiscountedPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountedPrice,
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockTransferOrderDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("OverstockTransferOrderDetails");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

    }
}
