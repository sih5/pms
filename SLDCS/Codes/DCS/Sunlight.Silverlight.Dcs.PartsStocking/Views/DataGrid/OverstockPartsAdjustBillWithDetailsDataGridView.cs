﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsAdjustBillWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Company_Type", "DealerPartsTransferOrderStatus","PartsShipping_Method"
        };

        public OverstockPartsAdjustBillWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Code",
                        Title = PartsStockingUIStrings.QueeyPanel_OverstockTransferOrder_Title_Code
                    }, new KeyValuesColumnItem {
                        Name = "CustomerType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.QueeyPanel_OverstockTransferOrder_Title_CustomerType
                    }, new ColumnItem {
                        Name = "TransferInCorpCode",
                        Title = PartsStockingUIStrings.DataEditView_Text_HauDistanceInfor_TransferInCorpCode
                    }, new ColumnItem {
                        Name = "TransferInCorpName",
                        Title = PartsStockingUIStrings.QueeyPanel_OverstockTransferOrder_Title_TransferInCorpName
                    }, new ColumnItem {
                        Name = "TransferInWarehouseCode",
                        Title = PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferInWarehouseCode
                    }, new ColumnItem {
                        Name = "TransferInWarehouseName",
                        Title = PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferInWarehouseName
                    }, new ColumnItem {
                        Name = "TransferOutCorpCode",
                        Title = PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferOutCorpCode
                    }, new ColumnItem {
                        Name = "TransferOutCorpName",
                        Title = PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferOutCorpName
                    }, new ColumnItem {
                        Name = "TransferOutWarehouseCode",
                        Title = PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferOutWarehouseCode
                    }, new ColumnItem {
                        Name = "TransferOutWarehouseName",
                        Title = PartsStockingUIStrings.QueryPanel_OverstockTransferOrder_Title_TransferOutWarehouseCode
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        Title =PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount,
                    }, new ColumnItem {
                        Name = "ReceivingAddress",
                        Title = PartsStockingUIStrings.DataEditView_Text_HauDistanceInfor_CompanyAddress
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        Title =PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingMethod
                    }, new ColumnItem {
                        Name = "PromisedDeliveryTime",
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_PromisedDeliveryTime
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }
                };

            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "OverstockPartsAdjustDetail"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockTransferOrder);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetOverstockTransferOrderByCurrentUser";
        }
    }
}