﻿
using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsAdjustDetailDataGridView : DcsDataGridViewBase {
        public OverstockPartsAdjustDetailDataGridView() {
            this.DataContextChanged += this.DataGridView_DataContextChanged;
        }

        private void DataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var overstockTransferOrder = e.NewValue as OverstockTransferOrder;
            if(overstockTransferOrder == null)
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "OverstockPartsTransferOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = overstockTransferOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName
                    }, new ColumnItem {
                        Name = "RetailPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_RetailguidePrice
                    }, new ColumnItem {
                        Name = "DiscountRate",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountRate2
                    }, new ColumnItem {
                        Name = "DiscountedPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_DiscountedPrice2
                    }, new ColumnItem {
                        Name = "TransferQty",   
                        Title =PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyTransferOrderDetail_PlannedAmount
                    }, new ColumnItem {
                        Name = "EnoughQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyTransferOrderDetail_EnoughQty
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockTransferOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetOverstockTransferOrderDetails";
        }
    }
}