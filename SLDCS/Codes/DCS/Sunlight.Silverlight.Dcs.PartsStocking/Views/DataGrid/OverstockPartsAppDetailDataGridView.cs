﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsAppDetailDataGridView : DcsDataGridViewBase {
        private void OverstockPartsAppDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var overstockPartsApp = e.NewValue as OverstockPartsApp;
            if(overstockPartsApp == null)
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "OverstockPartsAppId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = overstockPartsApp.Id;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockPartsAppDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "DealPrice",
                    }, new ColumnItem {
                        Name = "SalePrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TradePrice
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsAppDetail_IfConfirmed,
                        Name = "ConfirmResult"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetOverstockPartsAppDetails";
        }

        public OverstockPartsAppDetailDataGridView() {
            this.DataContextChanged += this.OverstockPartsAppDetailDataGridView_DataContextChanged;
        }
    }
}
