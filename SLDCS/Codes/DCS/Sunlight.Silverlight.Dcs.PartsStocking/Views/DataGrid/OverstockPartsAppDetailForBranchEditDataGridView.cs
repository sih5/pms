﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsAppDetailForBranchEditDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsAppDetail_IfConfirmed,
                        Name = "IfConfirmed"
                    },new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockPartsAppDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("OverstockPartsAppDetails");
        }
    }
}
