﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsAppDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase companyStockQueryWindow;
        private RadWindow radWindow;

        private DcsMultiPopupsQueryWindowBase CompanyStockQueryWindow {
            get {
                if(this.companyStockQueryWindow == null) {
                    this.companyStockQueryWindow = DI.GetQueryWindow("CompanyStockBySales") as DcsMultiPopupsQueryWindowBase;
                    this.companyStockQueryWindow.Loaded += CompanyStockQueryWindow_Loaded;
                    this.companyStockQueryWindow.SelectionDecided += this.CompanyStockQueryWindow_SelectionDecided;
                }
                return this.companyStockQueryWindow;
            }
        }

        private RadWindow SelectedWindow {
            get {
                return this.radWindow ?? (this.radWindow = new RadWindow {
                    Header = PartsStockingUIStrings.QueryPanel_Title_CompanyPartsStock,
                    Content = this.CompanyStockQueryWindow,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                });
            }
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var overstockPartsApp = this.DataContext as OverstockPartsApp;
            if(overstockPartsApp == null)
                return;
            this.SetSerialNumber(overstockPartsApp);
        }

        private void SetSerialNumber(OverstockPartsApp overstockPartsApp) {
            var serialNumber = 1;
            foreach(var overstockPartsAppDetail in overstockPartsApp.OverstockPartsAppDetails)
                overstockPartsAppDetail.SerialNumber = serialNumber++;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var overstockPartsApp = this.DataContext as OverstockPartsApp;
            if(overstockPartsApp == null || overstockPartsApp.BranchId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Validation_OverstockPartsAppDetail_BranchIsNull);
                return;
            }
            if(overstockPartsApp.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Validation_OverstockPartsAppDetail_PartsSalesCategoryIsNull);
                return;
            }
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError) {
                    loadOption.MarkErrorAsHandled();
                    return;
                }
                if(loadOption.Entities != null && loadOption.Entities.Any()) {
                    var company = loadOption.Entities.First();
                    if(company.Type == (int)DcsCompanyType.服务站兼代理库 && overstockPartsApp.StorageCompanyType == default(int)) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Validation_OverstockPartsAppDetail_StorageCompanyTypeIsNull);
                        return;
                    }
                    this.SelectedWindow.ShowDialog();
                }
            }, null);

        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(!e.Cell.Column.UniqueName.Equals("DealPrice"))
                return;
            var detail = e.Cell.DataContext as OverstockPartsAppDetail;
            if(detail == null)
                return;
            var quantityError = detail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("DealPrice"));
            if(quantityError != null)
                detail.ValidationErrors.Remove(quantityError);
            if(detail.DealPrice <= 0) {
                detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataGridView_Validation_DealPriceValidation1, new[] {
                    "DealPrice"
                }));
            } else if(detail.DealPrice > detail.PartsSalesPrice) {
                detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataGridView_Validation_DealPriceValidation2, new[] {
                    "DealPrice"
                }));
            } else {
                detail.ValidationErrors.Clear();
            }
        }


        private void CompanyStockQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var overstockPartsApp = this.DataContext as OverstockPartsApp;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(overstockPartsApp == null || queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common","BranchId",overstockPartsApp.BranchId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common","BranchId",false
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common","PartsSalesCategoryId",overstockPartsApp.PartsSalesCategoryId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common","PartsSalesCategoryId",false
            });
            //增加企业类型过滤条件
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("CompanyType", typeof(int), FilterOperator.IsEqualTo, overstockPartsApp.StorageCompanyType));
        }

        private void CompanyStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var overstockPartsApp = this.DataContext as OverstockPartsApp;
            if(overstockPartsApp == null)
                return;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var companyPartsStock = queryWindow.SelectedEntities.Cast<VirtualCompanySalescenterStock>();
            if(companyPartsStock == null)
                return;
            if(overstockPartsApp.OverstockPartsAppDetails.Any(r => companyPartsStock.Any(ex => ex.SparePartsId == r.SparePartId))) {
                UIHelper.ShowAlertMessage(string.Format(PartsStockingUIStrings.DataGridView_Validation_OverstockPartsAppDetail_SparePartCodeIsExist, companyPartsStock.FirstOrDefault().SparePartsCode));
                return;
            }
            foreach(var companyparts in companyPartsStock) {
                overstockPartsApp.StorageCompanyType = companyparts.CompanyType;
                overstockPartsApp.OverstockPartsAppDetails.Add(new OverstockPartsAppDetail {
                    SparePartId = companyparts.SparePartsId,
                    SparePartCode = companyparts.SparePartsCode,
                    SparePartName = companyparts.SparePartsName,
                    DealPrice = companyparts.PartsSalesPrice,
                    PartsSalesPrice = companyparts.PartsSalesPrice,
                    SalePrice = companyparts.PartsSalesPrice
                });
            }
            this.SetSerialNumber(overstockPartsApp);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("OverstockPartsAppDetails");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "DealPrice",
                    }, new ColumnItem {
                        Name = "SalePrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TradePrice,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockPartsAppDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.CellEditEnded += GridView_CellEditEnded;

        }



    }
}
