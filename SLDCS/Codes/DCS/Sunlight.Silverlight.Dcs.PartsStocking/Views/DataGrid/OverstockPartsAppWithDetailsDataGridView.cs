﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsAppWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "OverstockPartsApp_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsApp_StorageCompanyCode,
                        Name = "StorageCompanyCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsApp_StorageCompanyName,
                        Name = "StorageCompanyName"
                    }, new ColumnItem {
                        Name = "ContactPerson"
                    }, new ColumnItem {
                        Name = "ContactPhone"
                    },  new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem {
                        Name = "RejecterName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejecterName
                    }, new ColumnItem {
                        Name = "RejecterTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejectTime
                    }, new ColumnItem {
                        Name = "RejectComment",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejectComment
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsApp_BranchName,
                        Name = "BranchName"
                    },new ColumnItem{
                        Name="PartsSalesCategoryName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetOverstockPartsApps";
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockPartsApp);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "OverstockPartsAppDetail"
                    }
                };
            }
        }

        public OverstockPartsAppWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
