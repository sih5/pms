﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsAppWithDetailsForBranchDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Company_Type", "OverstockPartsApp_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "Code"
                    },new ColumnItem {
                        Name = "StorageCompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsApp_StorageCompanyCode
                    },new ColumnItem {
                        Name = "StorageCompanyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsApp_StorageCompanyName
                    },new KeyValuesColumnItem {
                        Name = "StorageCompanyType",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsApp_StorageCompanyType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "ContactPerson"
                    },new ColumnItem {
                        Name = "ContactPhone"
                    },new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ApproverName"
                    },new ColumnItem {
                        Name = "ApproveTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Name = "AbandonerName"
                    },new ColumnItem {
                        Name = "AbandonTime"
                   
                    },new ColumnItem {
                        Name = "RejecterName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_RejecterName
                    },new ColumnItem {
                        Name = "RejecterTime",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_RejectTime
                    },new ColumnItem {
                        Name = "RejectComment",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_RejectComment
                    },new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsApp_BranchName
                    },new ColumnItem{
                        Name="PartsSalesCategoryName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockPartsApp);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "OverstockPartsAppDetail"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetOverstockPartsApps";
        }

        public OverstockPartsAppWithDetailsForBranchDataGridView() {
            this.KeyValueManager.Register(this.kvNames);

        }
    }
}
