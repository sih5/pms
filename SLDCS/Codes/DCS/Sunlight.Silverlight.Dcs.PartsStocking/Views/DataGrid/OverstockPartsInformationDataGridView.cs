﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsInformationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Company_Type"
        };

        public OverstockPartsInformationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName
                    }, new ColumnItem {
                        Name = "UsableQuantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_UsableQuantity,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "StorageCompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_StorageCompanyCode
                    }, new ColumnItem {
                        Name = "StorageCompanyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_StorageCompanyName
                    }, new ColumnItem{
                        Name="PartsSalesCategoryName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_PartsSalesCategoryName
                    }, new ColumnItem {
                        Name = "ProvinceName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_ProvinceName
                    }, new ColumnItem {
                        Name = "CityName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_CityName
                    }, new ColumnItem {
                        Name = "CountyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_CountyName
                    }, new ColumnItem {
                        Name = "LinkMan",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_LinkMan
                    }, new ColumnItem {
                        Name = "LinkPhone",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_LinkPhone
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "StorageCompanyType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_StorageCompanyType
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockPartsStock);
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                if(this.FilterItem.MemberName == "ZeroStock") {
                    if(((bool?)this.FilterItem.Value).HasValue) {
                        newCompositeFilterItem.Filters.Add(new FilterItem {
                            MemberName = "UsableQuantity",
                            MemberType = typeof(int),
                            Operator = ((bool?)this.FilterItem.Value).Value ? FilterOperator.IsEqualTo : FilterOperator.IsNotEqualTo,
                            Value = 0
                        });
                    }
                } else if(this.FilterItem.MemberName != "CompanyId")
                    newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "CompanyId")) {
                    if(item.MemberName == "ZeroStock") {
                        if(((bool?)item.Value).HasValue) {
                            newCompositeFilterItem.Filters.Add(new FilterItem {
                                MemberName = "UsableQuantity",
                                MemberType = typeof(int),
                                Operator = ((bool?)item.Value).Value ? FilterOperator.IsEqualTo : FilterOperator.IsNotEqualTo,
                                Value = 0
                            });
                        }
                        continue;
                    }
                    newCompositeFilterItem.Filters.Add(item);

                }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "查询积压件库存";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "companyId":
                        return filters.Filters.Single(item => item.MemberName == "CompanyId").Value;
                    case "storageCompanyId":
                        return null;
                    case "storageCompanyType":
                        return null;
                    case "partsSalesCategoryId":
                        if(this.FilterItem is CompositeFilterItem) {
                            if(((CompositeFilterItem)this.FilterItem).Filters.Any(e => e.MemberName == "PartsSalesCategoryId"))
                                return ((CompositeFilterItem)this.FilterItem).Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value;
                        } else if(this.FilterItem.MemberName == "PartsSalesCategoryId")
                            return this.FilterItem.Value;
                        return base.OnRequestQueryParameter(queryName, parameterName);
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["UsableQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }
    }
}