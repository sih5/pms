﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsPlatFormBillDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(VirtualOverstockPartsPlatFormBill);
            }
        }
       
        public OverstockPartsPlatFormBillDataGridView() {
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CenterName",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_AgencyName
                    }, new ColumnItem {
                        Name = "DealerName",
                        Title = PartsStockingUIStrings.DataGridView_Column_ServiceName
                    }, new ColumnItem {
                        Name = "DealerCode",
                        Title = PartsStockingUIStrings.DataGridView_Column_ServiceCode
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartName
                    },new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseCode
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseName
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_Column_OverQuantity
                    },new ColumnItem {
                        Name = "RetailGuidePrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_RetailguidePrice
                    },new ColumnItem {
                        Name = "DiscountRate",
                        Title = PartsStockingUIStrings.DataGridView_Column_DiscountRate
                    }, new ColumnItem {
                        Name = "DiscountedPrice",
                        Title = PartsStockingUIStrings.DataGridView_Column_DiscountedPrice
                    }, new ColumnItem {
                        Name = "EntityStatus",
                        Title = PartsStockingUIStrings.DataGridView_Column_EntityStatus
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询积压件平台明细";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            ((GridViewDataColumn)this.GridView.Columns["Quantity"]).DataFormatString = "d";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null) {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem) && r.MemberName != "SparePartCode").Cast<CompositeFilterItem>().ToArray();
                switch (parameterName) {
                    case "sparePartCode":
                        var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
                        if (codes == null || codes.Value == null)
                            return null;
                        return codes.Value.ToString();
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
