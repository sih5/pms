﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsStockForQuantityDataGridView : DcsDataGridViewBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategoryName = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "Company_Type"
        };

        public OverstockPartsStockForQuantityDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }
        private void CreateUI() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategoryName.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {

                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "UsableQuantity"
                    }, new ColumnItem {
                        Name = "StorageCompanyCode"
                    }, new ColumnItem {
                        Name = "StorageCompanyName"
                    }, new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsInformation_StorageCompanyType,
                        Name = "StorageCompanyType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "ProvinceName"
                    }, new ColumnItem {
                        Name = "CityName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Company_CountyName,
                        Name = "CountyName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_LinkMan,
                        Name = "LinkMan"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_LinkPhone,
                        Name = "LinkPhone"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsInformation_BranchId,
                        Name = "BranchId",
                        KeyValueItems = this.kvBranches
                    }, new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_PartsSalesCategoryName,
                        Name = "PartsSalesCategoryId",
                        KeyValueItems = this.kvPartsSalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(OverstockPartsStock);
            }
        }
        protected override string OnRequestQueryName() {
            return "查询积压件库存";
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                if(this.FilterItem.MemberName != "CompanyId")
                    newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else
                foreach(var item in compositeFilterItem.Filters.Where(filter => !(new[] { "CompanyId", "StorageCompanyId", "StorageCompanyType" }).Contains(filter.MemberName))) {
                    if(item is CompositeFilterItem && ((CompositeFilterItem)item).Filters.Any(filter => (new[] { "CompanyId", "StorageCompanyId", "StorageCompanyType" }).Contains(filter.MemberName)))
                        continue;
                    newCompositeFilterItem.Filters.Add(item);
                }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                FilterItem filterItem;
                switch(parameterName) {
                    case "companyId":
                        filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "CompanyId");
                        if(filterItem != null)
                            return filterItem.Value;
                        var singleComposite = filters.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem));
                        if(singleComposite != null) {
                            var composite = singleComposite as CompositeFilterItem;
                            if(composite != null) {
                                var firstOrDefault = composite.Filters.FirstOrDefault(r => r.MemberName == "CompanyId");
                                if(firstOrDefault != null)
                                    return firstOrDefault.Value;
                            }
                        }
                        return base.OnRequestQueryParameter(queryName, parameterName);
                    case "storageCompanyId":
                        filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "StorageCompanyId");
                        if(filterItem != null)
                            return filterItem.Value;
                        var singleComposite1 = filters.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem));
                        if(singleComposite1 != null) {
                            var composite = singleComposite1 as CompositeFilterItem;
                            if(composite != null) {
                                var firstOrDefault = composite.Filters.FirstOrDefault(r => r.MemberName == "StorageCompanyId");
                                if(firstOrDefault != null)
                                    return firstOrDefault.Value;
                            }
                        }
                        return null;
                    case "storageCompanyType":
                        filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "StorageCompanyType");
                        if(filterItem != null)
                            return filterItem.Value;
                        var singleComposite2 = filters.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem));
                        if(singleComposite2 != null) {
                            var composite = singleComposite2 as CompositeFilterItem;
                            if(composite != null) {
                                var firstOrDefault = composite.Filters.FirstOrDefault(r => r.MemberName == "StorageCompanyType");
                                if(firstOrDefault != null)
                                    return firstOrDefault.Value;
                            }
                        }
                        return null;
                    case "partsSalesCategoryId":
                        filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "partsSalesCategoryId");
                        if(filterItem != null)
                            return filterItem.Value;
                        var singleComposite3 = filters.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem));
                        if(singleComposite3 != null) {
                            var composite = singleComposite3 as CompositeFilterItem;
                            if(composite != null) {
                                var firstOrDefault = composite.Filters.FirstOrDefault(r => r.MemberName == "partsSalesCategoryId");
                                if(firstOrDefault != null)
                                    return firstOrDefault.Value;
                            }
                        }
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["UsableQuantity"]).DataFormatString = "d";
        }
    }
}
