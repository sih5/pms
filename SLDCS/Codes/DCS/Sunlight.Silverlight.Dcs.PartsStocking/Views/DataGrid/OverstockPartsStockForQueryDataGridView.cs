﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class OverstockPartsStockForQueryDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(OverstockPartsStock);
            }
        }
        private readonly string[] kvNames = {
            "Company_Type"
        };

        public OverstockPartsStockForQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "UsableQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "StorageCompanyCode"
                    }, new ColumnItem {
                        Name = "StorageCompanyName"
                    }, new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsInformation_StorageCompanyType,
                        Name = "StorageCompanyType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Dealprice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_Dealprice
                    },new ColumnItem {
                        Name = "SalePrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TradePrice
                    },new ColumnItem {
                        Name = "DealAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_DealAmount
                    }, new ColumnItem {
                        Name = "ProvinceName"
                    }, new ColumnItem {
                        Name = "CityName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStockQuery_CountyName,
                        Name = "CountyName"
                    }, new ColumnItem {
                        Name = "LinkMan",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_LinkMan
                    }, new ColumnItem {
                        Name = "LinkPhone",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_LinkPhone
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem{
                        Name="PartsSalesCategoryName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            if(this.FilterItem is CompositeFilterItem) {
                var newCompositeFilter = new CompositeFilterItem();
                foreach(var filter in ((CompositeFilterItem)this.FilterItem).Filters.Where(e => e.MemberName != "CompanyId"))
                    newCompositeFilter.Filters.Add(filter);
                return newCompositeFilter.ToFilterDescriptor();
            }
            return this.FilterItem.MemberName != "CompanyId" ? this.FilterItem.ToFilterDescriptor() : null;
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "companyId":
                    if(this.FilterItem is CompositeFilterItem) {
                        if(((CompositeFilterItem)this.FilterItem).Filters.Any(e => e.MemberName == "CompanyId"))
                            return ((CompositeFilterItem)this.FilterItem).Filters.Single(e => e.MemberName == "CompanyId").Value;
                    } else if(this.FilterItem.MemberName == "CompanyId")
                        return this.FilterItem.Value;

                    return BaseApp.Current.CurrentUserData.EnterpriseId;
                case "storageCompanyId":
                    return null;
                case "storageCompanyType":
                    return null;
                case "partsSalesCategoryId":
                    if(this.FilterItem is CompositeFilterItem) {
                        //if(((CompositeFilterItem)this.FilterItem).Filters.Any(e => e.MemberName == "PartsSalesCategoryId"))
                        //    return ((CompositeFilterItem)this.FilterItem).Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value;
                        return 221;
                    } else if(this.FilterItem.MemberName == "PartsSalesCategoryId")
                        return this.FilterItem.Value;
                    return base.OnRequestQueryParameter(queryName, parameterName);
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return "查询积压件库存区分企业";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            //this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["UsableQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }
    }
}
