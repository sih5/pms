﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid
{
    public class PackingTaskDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
             "PackingTaskStatus","Parts_InboundType","TraceProperty"
        };

        public PackingTaskDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new KeyValuesColumnItem{
                        Name = "Status",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code",
                        Title = PartsStockingUIStrings.DataEditView_Title_PackingTaskCode
                    },new ColumnItem {
                        Name = "PartsInboundPlanCode",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_PartsInboundPlanCode
                    },new ColumnItem {
                        Name = "PartsInboundCheckBillCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_PartsInboundCheckBillCode
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartCode
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartName
                    },new KeyValuesColumnItem {
                        Name = "TraceProperty",
                        Title = "追溯属性",
                         KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    },new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseCode
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseName
                    },new ColumnItem {
                        Name = "PlanQty",
                       Title = PartsStockingUIStrings.DataEditView_Title_PlanAmount
                    },new ColumnItem {
                        Name = "PackingQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_PackingQty
                    },new ColumnItem {
                        Name = "BatchNumber",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetail_CurrentBatchNumber
                    },new ColumnItem {
                        Name = "SourceCode",
                        Title = PartsStockingUIStrings.QueryPanel_Title_SourceCode
                    },new ColumnItem {
                        Name = "CounterpartCompanyCode",
                        Title = PartsStockingUIStrings.DataEditView_Text_CounterpartCompanyCode
                    },new ColumnItem {
                        Name = "CounterpartCompanyName",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyName
                    },new ColumnItem {
                        Name = "PackingMaterial",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_PackingMaterial
                    },new ColumnItem {
                        Name = "ReferenceCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_ReferenceCode
                    },new ColumnItem {
                        Name = "PackNum",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_PickNum
                    },new KeyValuesColumnItem {
                        Name = "InboundType",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_InboundType,
                         KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem{
                        Name = "WarehouseAreaCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_WarehouseAreaCode
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_HasDifference,
                        Name = "HasDifference"
                    },new ColumnItem{
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime
                    }
                    ,new ColumnItem{
                        Name = "ModifierName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    }, new ColumnItem{
                        Name = "ModifyTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName()
        {
            return "GetPackingTaskLists";
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(PackingTaskQuery);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "sourceCode":
                        return filters.Filters.Single(item => item.MemberName == "ReferenceCode").Value;
                    case "partsInboundPlanCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsInboundPlanCode").Value;
                    case "partsInboundCheckBillCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsInboundCheckBillCode").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "originalRequirementBillCode":
                        return filters.Filters.Single(item => item.MemberName == "OriginalRequirementBillCode").Value;
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "inboundType":
                        return filters.Filters.Single(item => item.MemberName == "InboundType").Value;
                    case "bCreateTime":
                        var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                    case "bModifyTime":
                        var expectedPlaceDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ModifyTime");
                        return expectedPlaceDate == null ? null : expectedPlaceDate.Filters.First(r => r.MemberName == "ModifyTime").Value;
                    case "eModifyTime":
                        var expectedPlaceDate1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "ModifyTime");
                        return expectedPlaceDate1 == null ? null : expectedPlaceDate1.Filters.Last(item => item.MemberName == "ModifyTime").Value;
                    case "modifierName":
                        return filters.Filters.Single(item => item.MemberName == "ModifierName").Value;
                    case "hasDifference":
                        return filters.Filters.Single(item => item.MemberName == "HasDifference").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider
        {
            get
            {
                return new TabControlRowDetailsTemplateProvider
                {
                    DetailPanelNames = new[] {
                        "PackingTaskForDetail"
                    }
                };
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                foreach (var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "WarehouseId" && filter.MemberName != "HasDifference"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
        protected override void OnControlsCreated()
        {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();
            this.DataPager.PageSize = 100;
        }
    }
}
