﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid
{
    public class PackingTaskForDetailDataGridView: DcsDataGridViewBase
    {
        private void PackingTaskDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var packingTask = e.NewValue as PackingTaskQuery;
            if (packingTask == null || packingTask.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem
            {
                MemberName = "PackingTaskId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = packingTask.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
        public PackingTaskForDetailDataGridView()
        {
            this.DataContextChanged += this.PackingTaskDetailDataGridView_DataContextChanged;
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                        Name = "RecomPackingMaterial",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_RecomPackingMaterial
                    },new ColumnItem {
                        Name = "RecomQty",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_RecomQty
                    }, new ColumnItem {
                        Name = "PhyPackingMaterial",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_PhyPackingMaterial,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PhyQty",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_PhyQty
                    }
                };
            }
        }


        protected override Type EntityType
        {
            get
            {
                return typeof(PackingTaskDetail);
            }
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["RecomQty"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["PhyQty"]).DataFormatString = "d";

        }
        protected override string OnRequestQueryName()
        {
            return "GetPackingTaskDetailLists";
        }
    }
}
