﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsDifferenceBackBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
             "PartsDifferenceBackBillStatus","PartsDifferenceBackBillType"
        };

        public PartsDifferenceBackBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsDifferenceBackBill_Code,
                    },new KeyValuesColumnItem{
                        Name = "Status",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new KeyValuesColumnItem{
                        Name = "Type",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBill_Type,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "PartsSupplierCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_SupplierCode
                    },new ColumnItem {
                        Name = "PartsSupplierName",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_SupplierName
                    },new ColumnItem {
                        Name = "SourceCode",
                        Title = PartsStockingUIStrings.QueryPanel_Title_SourceCode
                    },new ColumnItem {
                        Name = "PartsInboundCheckBillCode",
                        Title = PartsStockingUIStrings.PackingTask_PartsInboundPlanCode
                    },new ColumnItem{
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime
                    },new ColumnItem{
                        Name = "SubmitterName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBill_SubmitterName
                    }, new ColumnItem{
                        Name = "SubmitTime",
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBill_SubmitTime
                    },new ColumnItem{
                        Name = "InitialApproverName",
                        Title = PartsStockingUIStrings.DataGridView_Column_InitApprover
                    }, new ColumnItem{
                        Name = "InitialApproveTime",
                        Title =  PartsStockingUIStrings.DataGridView_Column_InitApproveTime
                    },new ColumnItem{
                        Name = "CheckerName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CheckerName
                    }, new ColumnItem{
                        Name = "CheckTime",
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_CheckTime
                    },new ColumnItem{
                        Name = "ApproverName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ApproverName
                    }, new ColumnItem{
                        Name = "ApproveTime",
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_ApproveTime
                    },new ColumnItem{
                        Name = "RejecterName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejecterName
                    }, new ColumnItem{
                        Name = "RejectTime",
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_RejectTime
                    },new ColumnItem{
                        Name = "AbandonerName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Abandoner
                    }, new ColumnItem{
                        Name = "AbandonTime",
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_AbandonTime
                    },new ColumnItem{
                        Name = "ModifierName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    }, new ColumnItem{
                        Name = "ModifyTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsDifferenceBackBillConditions";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsDifferenceBackBill);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "type":
                        return filters.Filters.Single(item => item.MemberName == "Type").Value;
                    case "taskCode":
                        var shelvesCode = filters.Filters.FirstOrDefault(item => item.MemberName == "TaskCode");
                        return shelvesCode == null ? null : shelvesCode.Value;                   
                    case "beginCreateTime":
                        var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "endCreateTime":
                        var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                    case "beginSubmitTime":
                        var expectedPlaceDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "SubmitTime");
                        return expectedPlaceDate == null ? null : expectedPlaceDate.Filters.First(r => r.MemberName == "SubmitTime").Value;
                    case "endSubmitTime":
                        var expectedPlaceDate1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "SubmitTime");
                        return expectedPlaceDate1 == null ? null : expectedPlaceDate1.Filters.Last(item => item.MemberName == "SubmitTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                foreach(var item in compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).Where(filter => filter.MemberName != "TaskCode" ))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsDifferenceBackBillDtl"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
