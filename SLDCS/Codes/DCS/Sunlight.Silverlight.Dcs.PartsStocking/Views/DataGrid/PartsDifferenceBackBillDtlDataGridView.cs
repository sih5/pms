﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using System.Windows.Data;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsDifferenceBackBillDtlDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
           "PartsDifferenceBackBillDetailErrorType","TraceProperty"
        };
        public PartsDifferenceBackBillDtlDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "TaskCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TaskCode
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title= PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title= PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "DiffQuantity",
                        Title= PartsStockingUIStrings.DataEditView_Text_PartsDifferenceBackBillDtl_DiffQuantity,
                    },new KeyValuesColumnItem {
                        Name = "ErrorType",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBillDtl_ErrorType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    },new KeyValuesColumnItem {
                        Name = "TraceProperty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBillDtl_TraceProperty,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                    }, new ColumnItem {
                        Name = "TraceCode",
                        Title= PartsStockingUIStrings.DataEditView_Text_PartsDifferenceBackBillDtl_TraceCode,
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsDifferenceBackBillDtls");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsDifferenceBackBillDtl);
            }
        }
    }
}
