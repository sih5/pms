﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web.Entities;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsDifferenceBackBillDtlForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
           "PartsDifferenceBackBillDetailErrorType","TraceProperty"
        };
        public PartsDifferenceBackBillDtlForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        private DcsMultiPopupsQueryWindowBase partDropDownQueryWindow;
        private DcsMultiPopupsQueryWindowBase PartDropDownQueryWindow {
            get {
                if(this.partDropDownQueryWindow == null) {
                    this.partDropDownQueryWindow = DI.GetQueryWindow("PartsDifferenceBackBillDtl") as DcsMultiPopupsQueryWindowBase;
                    this.partDropDownQueryWindow.SelectionDecided += this.PartQueryWindow_SelectionDecided;
                    this.partDropDownQueryWindow.Loaded += this.PartQueryWindow_Loaded;
                }
                return this.partDropDownQueryWindow;
            }
        }
        private RadWindow radQueryWindow;

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.PartDropDownQueryWindow,
                    Header = "可回退配件差异回退单清单查询",
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.Manual
                });
            }
        }

        private void PartQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var details = queryWindow.SelectedEntities.Cast<VirtualPartsDifferenceBackBillDtl>().ToArray();
            if(details == null)
                return;
            var partsDifferenceBackBill = this.DataContext as PartsDifferenceBackBill;
            if(partsDifferenceBackBill == null)
                return;
            foreach(var detail in details) {
                if(!partsDifferenceBackBill.PartsDifferenceBackBillDtls.Any(o => o.SparePartId == detail.SparePartId && o.TaskId == detail.TaskId)) {
                    var newPartsDifferenceBackBillDtl = new PartsDifferenceBackBillDtl {
                        SparePartId = detail.SparePartId,
                        SparePartCode = detail.SparePartCode,
                        SparePartName = detail.SparePartName,
                        DiffQuantity = detail.DiffQuantity,
                        OnlyDiffQuantity = detail.DiffQuantity,
                        TaskId = detail.TaskId,
                        TaskCode = detail.TaskCode,
                        TraceProperty=detail.TraceProperty
                    };
                    partsDifferenceBackBill.PartsDifferenceBackBillDtls.Add(newPartsDifferenceBackBillDtl);
                }
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void PartQueryWindow_Loaded(object sender, RoutedEventArgs routedEventArgs) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            var partsDifferenceBackBill = this.DataContext as PartsDifferenceBackBill;
            if(partsDifferenceBackBill == null)
                return;

            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SourceCode", partsDifferenceBackBill.SourceCode
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "SourceCode", false
            });

            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("Type", typeof(int), FilterOperator.IsEqualTo, partsDifferenceBackBill.Type));
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "TaskCode",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_TaskCode,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title= PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title= PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "DiffQuantity",
                        Title= PartsStockingUIStrings.DataEditView_Text_PartsDifferenceBackBillDtl_DiffQuantity,
                    },new KeyValuesColumnItem {
                        Name = "ErrorType",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBillDtl_ErrorType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    },new KeyValuesColumnItem {
                        Name = "TraceProperty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBillDtl_TraceProperty,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "TraceCode",
                        Title= PartsStockingUIStrings.DataEditView_Text_PartsDifferenceBackBillDtl_TraceCode,
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsDifferenceBackBillDtls");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.CellValidating += GridView_CellValidating;
        }

        protected override Type EntityType {
            get {
                return typeof(PartsDifferenceBackBillDtl);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var partsDifferenceBackBill = this.DataContext as PartsDifferenceBackBill;
            if(partsDifferenceBackBill == null)
                return;
            if(string.IsNullOrWhiteSpace(partsDifferenceBackBill.SourceCode)) {
                UIHelper.ShowNotification("请先选择源单据.");
                e.Cancel = true;
                return;
            }
            this.RadQueryWindow.ShowDialog();
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var partsDifferenceBackBillDtl = e.Cell.DataContext as PartsDifferenceBackBillDtl;
            if(partsDifferenceBackBillDtl == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "DiffQuantity":
                    if(!(e.NewValue is int) || (int)e.NewValue < 0) {
                        e.IsValid = false;
                        e.ErrorMessage = "差异数量必须为大于等于0的数字";
                    }
                    break;
                case "ErrorType":
                    if(!(e.NewValue is int) || (int)e.NewValue < 0) {
                        e.IsValid = false;
                        e.ErrorMessage = "错误类型必填";
                    }
                    break;
            }
        }
    }
}
