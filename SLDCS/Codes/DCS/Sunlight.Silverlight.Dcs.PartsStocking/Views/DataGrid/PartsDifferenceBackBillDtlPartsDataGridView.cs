﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsDifferenceBackBillDtlPartsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
           "TraceProperty"
        };
        public PartsDifferenceBackBillDtlPartsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "TaskCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TaskCode
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode
                    },
                    new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName
                    },
                    new ColumnItem {
                        Name = "DiffQuantity",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsDifferenceBackBillDtl_DiffQuantity
                    },new KeyValuesColumnItem {
                        Name = "TraceProperty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBillDtl_TraceProperty,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "可回传配件差异回退单清单";
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsDifferenceBackBillDtl);
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch(parameterName) {
                    case "partCode":
                        var partCode = filters.Filters.FirstOrDefault(item => item.MemberName == "SparePartCode");
                        return partCode == null ? null : partCode.Value;
                    case "partName":
                        var partName = filters.Filters.FirstOrDefault(item => item.MemberName == "SparePartName");
                        return partName == null ? null : partName.Value;
                    case "code":
                        var code = filters.Filters.FirstOrDefault(item => item.MemberName == "SourceCode");
                        return code == null ? null : code.Value;
                    case "type":
                        var type = filters.Filters.FirstOrDefault(item => item.MemberName == "Type");
                        return type == null ? null : type.Value;
                    default:
                        return null;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.FullRow;
        }
    }
}

