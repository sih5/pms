﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsDifferenceBackBillSourceCodeDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Name = "SourceCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_SourceCode
                    },
                    new ColumnItem {
                        Name = "Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_BoxUpTaskOrPicking_TaskCode
                    }, new ColumnItem {
                        Name = "PartsInboundCheckBillCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_PartsInboundCheckBillCode
                    },new ColumnItem {
                        Name = "PartsSupplierCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_SupplierCode
                    },new ColumnItem {
                        Name = "PartsSupplierName",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_SupplierName
                    },new ColumnItem {
                        Name = "Status",
                        Title = PartsStockingUIStrings.AgencyLogisticCompany_Status
                    },
                    new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_WarehouseCode
                    },
                    new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_WarehouseName
                    },
                    new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreatorName
                    },
                    new ColumnItem {
                        Name = "CreateTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreateTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsDifferenceBackBillSourceCodes";
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsDifferenceBackBillSourceCode);
            }
        }

         protected override object OnRequestQueryParameter(string queryName, string parameterName) {
             var filters = this.FilterItem as CompositeFilterItem;
             if(filters != null) {
                 var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                 switch(parameterName) {
                     case "sourceCode":
                         return filters.Filters.First(item => item.MemberName == "SourceCode").Value;
                     case "code":
                         return filters.Filters.First(item => item.MemberName == "Code").Value;
                     case "type":
                         return filters.Filters.First(item => item.MemberName == "Type").Value;
                     //case "beginCreateTime":
                     //    var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                     //    return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                     //case "endCreateTime":
                     //    var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                     //    return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                     default:
                         return null;

                 }
             }
             return base.OnRequestQueryParameter(queryName, parameterName);
         }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
             var newCompositeFilterItem = new CompositeFilterItem();
             return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}

