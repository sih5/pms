﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInboundCheckBillDetailDataGridView : DcsDataGridViewBase {
        private int PartsInboundCheckBillId;
        private int WarehouseId;
        public PartsInboundCheckBillDetailDataGridView() {
            this.DataContextChanged += this.PartsInboundCheckBillDetailDataGridView_DataContextChanged;
        }

        private void PartsInboundCheckBillDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsInboundCheckBill = e.NewValue as PartsInboundCheckBill;
            if(partsInboundCheckBill != null && partsInboundCheckBill.Id != default(int)) {
                this.PartsInboundCheckBillId = partsInboundCheckBill.Id;
                this.WarehouseId = partsInboundCheckBill.WarehouseId;
            }
            var virtualInboundCheckBill = e.NewValue as VirtualPartsInboundCheckBill;
            if(virtualInboundCheckBill != null && virtualInboundCheckBill.Id != default(int)) {
                this.PartsInboundCheckBillId = virtualInboundCheckBill.Id;
                this.WarehouseId = virtualInboundCheckBill.WarehouseId;
            }
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "DestWarehouseAreaCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DestWarehouseAreaCode
                    },new ColumnItem {
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Name = "SparePartName"
                    },new ColumnItem {
                        Name = "SupplierPartCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_PartsSupplierCode
                    },new ColumnItem {
                        Name = "SparePart.OverseasPartsFigure",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_OverseasPartsFigure
                    },new ColumnItem {
                        Name = "WarehouseAreaCode"
                    },new ColumnItem {
                        Name = "InspectedQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "SettlementPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "SparePart.MeasureUnit",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SparePart_MeasureUnit
                    },new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {

                        Name = "SpareOrderRemark"
                    },new ColumnItem {
                        Name = "BatchNumber",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetail_CurrentBatchNumber
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInboundCheckBillDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsInboundCheckBillDetailsWithSparePart";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["InspectedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "id":
                    return this.PartsInboundCheckBillId;
                case "warehouseId":
                    return this.WarehouseId;
                default:
                    return null;
            }
        }
    }
}
