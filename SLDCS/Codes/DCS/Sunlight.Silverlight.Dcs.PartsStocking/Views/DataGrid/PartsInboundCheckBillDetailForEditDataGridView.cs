﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInboundCheckBillDetailForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase warehouseAreaDropDownQueryWindow;

        private QueryWindowBase WarehouseAreaDropDownQueryWindow {
            get {
                if(this.warehouseAreaDropDownQueryWindow == null) {
                    this.warehouseAreaDropDownQueryWindow = DI.GetQueryWindow("WarehouseAreaDropDown");
                    this.warehouseAreaDropDownQueryWindow.Loaded += this.WarehouseAreaDropDownQueryWindow_Loaded;
                    this.warehouseAreaDropDownQueryWindow.SelectionDecided += this.WarehouseAreaDropDownQueryWindow_SelectionDecided;
                }
                return this.warehouseAreaDropDownQueryWindow;
            }
        }

        private void WarehouseAreaDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var partsInboundCheckBill = this.DataContext as PartsInboundCheckBill;
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(partsInboundCheckBill == null || queryWindow == null)
                return;

            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Warehouse.Name", partsInboundCheckBill.WarehouseName
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "AreaKind", (int)DcsAreaKind.库位
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "WarehouseAreaCategory.Category", (int)DcsAreaType.检验区
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Status", (int)DcsBaseDataStatus.有效
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Warehouse.Name", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "AreaKind", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "WarehouseAreaCategory.Category", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Status", false
            });
        }

        private void WarehouseAreaDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var warehouseArea = queryWindow.SelectedEntities.Cast<WarehouseArea>().FirstOrDefault();
            if(warehouseArea == null)
                return;
            var partsInboundCheckBillDetail = queryWindow.DataContext as PartsInboundCheckBillDetail;
            if(partsInboundCheckBillDetail == null)
                return;
            partsInboundCheckBillDetail.WarehouseAreaId = warehouseArea.Id;
            partsInboundCheckBillDetail.WarehouseAreaCode = warehouseArea.Code;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsInboundCheckBillDetail = e.Row.DataContext as PartsInboundCheckBillDetail;
            if(partsInboundCheckBillDetail == null)
                return;
            var partsInboundCheckBill = this.DataContext as PartsInboundCheckBill;
            if(partsInboundCheckBill == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "InspectedQuantity":
                    if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨)
                        e.Cancel = true;
                    break;
            }
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var partsInboundCheckBillDetail = e.Cell.DataContext as PartsInboundCheckBillDetail;
            if(partsInboundCheckBillDetail == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "InspectedQuantity":
                    if(!(e.NewValue is int) || (int)e.NewValue > partsInboundCheckBillDetail.PlannedAmount || (int)e.NewValue < 0) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsStockingUIStrings.DataEditView_Validation_PartsInboundCheckBillDetail_InspectedQuantity_IsMustLessThanPlannedAmount;
                    }
                    break;
            }
        }

        private GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is PartsInboundCheckBillDetail) {
                        var partsInboundCheckBillDetail = item as PartsInboundCheckBillDetail;
                        if(partsInboundCheckBillDetail.InspectedQuantity > partsInboundCheckBillDetail.PlannedAmount)
                            return this.Resources["ErrorDataBackground"] as Style;
                        if(partsInboundCheckBillDetail.InspectedQuantity == partsInboundCheckBillDetail.PlannedAmount)
                            return this.Resources["CorrectDataBackground"] as Style;
                    }
                    return null;
                }
            };
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsInboundCheckBillDetails");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new DropDownTextBoxColumnItem {
                        Name = "WarehouseAreaCode",
                        DropDownContent = this.WarehouseAreaDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "InspectedQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right, 
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        Title = "销售价",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInboundCheckBillDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
            ((GridViewDataColumn)this.GridView.Columns["PlannedAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["InspectedQuantity"]).DataFormatString = "d";
        }
    }
}
