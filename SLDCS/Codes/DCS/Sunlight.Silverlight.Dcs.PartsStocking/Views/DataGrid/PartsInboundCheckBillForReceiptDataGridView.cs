﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInboundCheckBillForReceiptDataGridView : DcsDataGridViewBase {
        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsInboundCheckBillDetail = e.Row.DataContext as PartsInboundCheckBillDetail;
            if(partsInboundCheckBillDetail == null)
                return;
            var partsInboundCheckBill = this.DataContext as PartsInboundCheckBill;
            if(partsInboundCheckBill == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "InspectedQuantity":
                    if(partsInboundCheckBill.InboundType == (int)DcsPartsInboundType.配件调拨)
                        e.Cancel = true;
                    break;
            }
        }
        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var partsInboundCheckBillDetail = e.Cell.DataContext as PartsInboundCheckBillDetail;
            if(partsInboundCheckBillDetail == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "ThisInspectedQuantity":
                    if(!(e.NewValue is int) || (int)e.NewValue + partsInboundCheckBillDetail.InspectedQuantity > partsInboundCheckBillDetail.PlannedAmount || (int)e.NewValue < 0) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsStockingUIStrings.DataEditView_Validation_PartsInboundReceipt_InspectedQuantity_IsMustLessThanPlannedAmount;
                    }
                    break;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsInboundCheckBillDetails");
        }
        protected override Type EntityType {
            get {
                return typeof(PartsInboundCheckBillDetail);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SupplierPartCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_PartsSupplierCode,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "IsPacking",
                    },new ColumnItem {
                        Name = "PlannedAmount",
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "InspectedQuantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_AlreadyInspectedQuantity,
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "ThisInspectedQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["PlannedAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["InspectedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["PlannedAmount"]).MaxWidth = 450;
            ((GridViewDataColumn)this.GridView.Columns["Remark"]).MaxWidth = 300;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.CellValidating += GridView_CellValidating;
            //this.GridView.RowStyleSelector = this.GridViewStyleSelector();
        }
    }
}
