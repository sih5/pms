﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInboundCheckBillNewWithDetailsForWarehouseDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = new[] {
            "Parts_InboundType","Parts_SettlementStatus","ERPInvoiceInformation_Type","PartsSalesReturn_ReturnType"
        };

        public PartsInboundCheckBillNewWithDetailsForWarehouseDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null)
                if(parameterName == "partsPurchaseOrderTypeId") {
                    var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "PartsPurchaseOrderTypeId");
                    return filter != null ? filter.Value : null;
                }
            if(parameterName == "sparePartCode") {
                var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "SparePartCode");
                return filter != null ? filter.Value : null;
            }
            if(parameterName == "sparePartName") {
                var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "SparePartName");
                return filter != null ? filter.Value : null;
            }
            if(parameterName == "supplierPartCode") {
                var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "SupplierPartCode");
                return filter != null ? filter.Value : null;
            }
            if(parameterName == "batchNumber") {
                var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "BatchNumber");
                return filter != null ? filter.Value : null;
            }
            if(parameterName == "hasDifference") { 
                var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "HasDifference"); 
                return filter != null ? filter.Value : null; 
            } 
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "SettlementStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.QueryPanel_Title_SettlementStatus
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseName,
                        Name = "WarehouseName"
                    }, new KeyValuesColumnItem {
                        Name = "InboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_InboundType
                    }, new ColumnItem {
                        Name = "CounterpartCompanyCode",
                        Title = PartsStockingUIStrings.DataEditView_Text_CounterpartCompanyCode
                    }, new ColumnItem {
                        Name = "CounterpartCompanyName",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyName
                    },new ColumnItem{
                        Name="TotalAmountForWarehouse",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount
                    }, new ColumnItem {
                        Name = "PartsPurchaseOrderTypeNameQuery",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_PurchaseOrderType
                    },new ColumnItem{
                        Name = "OriginalRequirementBillCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_OriginalRequirementBillCode
                    }, new ColumnItem{
                        Name="PartsInboundPlanCode",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Code
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseCode,
                        Name = "WarehouseCode"
                    }, new ColumnItem {
                        Name = "PlanSourceQuery",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_PlanSource
                    }                   
                    , new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ReturnType,
                        Name = "ReturnType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    }                   
                    , new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime
                    },new ColumnItem {   
                        Title = PartsStockingUIStrings.QueryPanel_Title_HasDifference,   
                        Name = "HasDifference"   
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_PartsSalesCategoryName
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_BranchName,               
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsInboundCheckBill);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsInboundCheckBillDetailForWarehouse"
                    }
                };
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "SparePartCode", "SparePartName", "PartsPurchaseOrderTypeId", "SupplierPartCode", "BatchNumber" ,"HasDifference"};
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => !param.Contains(filter.MemberName))) {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "仓库人员查询配件入库检验单New";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DomainDataSource.PageSize = 100;
            //((GridViewDataColumn)this.GridView.Columns["SumAmount"]).DataFormatString = "d";
        }
    }
}
