﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInboundCheckBillWithDetailNoPricesDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = new[] {
            "Parts_InboundType","Parts_SettlementStatus","ERPInvoiceInformation_Type","PartsSalesReturn_ReturnType"
        };

        public PartsInboundCheckBillWithDetailNoPricesDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override void OnDataLoaded() {
            var partsInboundCheckBills = this.Entities.Cast<PartsInboundCheckBill>().ToArray();
            if(partsInboundCheckBills == null) {
                return;
            }
            if(partsInboundCheckBills.Any(r => r.InboundType == (int)DcsPartsInboundType.配件采购)) {
                var dcsDomainContext = this.DomainContext as DcsDomainContext;
                dcsDomainContext.Load(dcsDomainContext.GetPartsPurchaseOrderByIdsQuery(partsInboundCheckBills.Where(r => r.InboundType == (int)DcsPartsInboundType.配件采购).Select(r => r.OriginalRequirementBillId).ToArray()), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    foreach(var partsInboundCheckBill in partsInboundCheckBills.Where(r => r.InboundType == (int)DcsPartsInboundType.配件采购)) {
                        var partsPurchaseOrder = loadOp.Entities.FirstOrDefault(r => r.Id == partsInboundCheckBill.OriginalRequirementBillId);
                        if(partsPurchaseOrder != null) {
                            partsInboundCheckBill.PartsPurchaseOrderTypeNameQuery = partsPurchaseOrder.PartsPurchaseOrderType.Name;
                            partsInboundCheckBill.PlanSourceQuery = partsPurchaseOrder.PlanSource;
                        }
                    }
                }, null);
            }
        }



        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null)
                if(parameterName == "partsPurchaseOrderTypeId") {
                    var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "PartsPurchaseOrderTypeId");
                    return filter != null ? filter.Value : null;
                }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var newCompositeFilterItem = new CompositeFilterItem();
                foreach(var filter in compositeFilterItem.Filters.Where(filter => filter.MemberName != "PartsPurchaseOrderTypeId"))
                    newCompositeFilterItem.Filters.Add(filter);
                return newCompositeFilterItem.ToFilterDescriptor();
            }
            return base.OnRequestFilterDescriptor(queryName);
        }


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "SettlementStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseCode,
                        Name = "WarehouseCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseName,
                        Name = "WarehouseName"
                    }, new KeyValuesColumnItem {
                        Name = "InboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "PlanSourceQuery",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_PlanSource
                    }, 
                    new ColumnItem {
                        Name = "CounterpartCompanyCode"
                    }, new ColumnItem {
                        Name = "CounterpartCompanyName"
                    }
                    //}, new ColumnItem{
                    //    Name="TotalAmount",
                    //    Title="总金额"
                    , new ColumnItem {
                        Name = "PartsPurchaseOrderTypeNameQuery",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_PurchaseOrderType
                    },new ColumnItem {
                        Name = "Objid"
                    },new ColumnItem { 
                        Name = "GPMSPurOrderCode"
                    },  new KeyValuesColumnItem {
                        Name = "PartsInboundPlan.PartsSalesReturnBill.ReturnType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    }, new ColumnItem {
                        Name = "Remark"
                    },  new ColumnItem {
                        Name ="ERPSourceOrderCode",
                         Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ERPOrderCode
                    },  new ColumnItem {
                        Name ="SAPPurchasePlanCode",
                         Title = PartsStockingUIStrings.QueryPanel_Title_SAPPurchasePlanCode
                    },new ColumnItem {
                        Name = "PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.Code",
                         Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsAdjustBill_SourceCode
                    },new KeyValuesColumnItem {
                        Name = "PartsInboundPlan.PartsSalesReturnBill.PartsSalesOrder.InvoiceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                         Title = PartsStockingUIStrings.QueryPanel_QueryItem_InvoiceType
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem{
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_OriginalRequirementBillCode,
                        Name="OriginalRequirementBillCode"
                    },new ColumnItem{
                        Title=PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsInboundCheckBill_PartsInboundPlanCode,
                        Name="PartsInboundPlan.Code"
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_PartsSalesCategoryName
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_BranchName,               
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInboundCheckBill);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsInboundCheckBillDetailNoPrice"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "仓库人员查询配件入库检验单无价格";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DomainDataSource.PageSize = 100;
            //((GridViewDataColumn)this.GridView.Columns["SumAmount"]).DataFormatString = "d";
        }
    }
}
