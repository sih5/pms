﻿
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System.Linq;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInboundCheckBillWithDetailsForSupplierDataGridView : PartsInboundCheckBillWithDetailsDataGridView {
        protected override string OnRequestQueryName() {
            return "GetPartsInboundCheckBillWithDetailBySupplierId";
        }
       protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsInboundCheckBillDetailForSupplier"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var newCompositeFilterItem = new CompositeFilterItem();
                foreach(var filter in compositeFilterItem.Filters.Where(filter => filter.MemberName != "CounterpartCompanyName"))
                    newCompositeFilterItem.Filters.Add(filter);
                return newCompositeFilterItem.ToFilterDescriptor();
            }
            return base.OnRequestFilterDescriptor(queryName);
        }
    }
}
