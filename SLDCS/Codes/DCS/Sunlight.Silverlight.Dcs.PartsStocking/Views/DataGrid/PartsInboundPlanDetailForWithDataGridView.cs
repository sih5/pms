﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInboundPlanDetailForWithDataGridView : DcsDataGridViewBase {
        public PartsInboundPlanDetailForWithDataGridView() {
            this.DataContextChanged += PartsInboundPlanDetailDataGridView_DataContextChanged;
        }

        private int PartsInboundPlanId;
        private int WarehouseId;
        private void PartsInboundPlanDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsInboundPlan = e.NewValue as PartsInboundPlan;

            if(partsInboundPlan == null || partsInboundPlan.Id == default(int))
                return;
            this.PartsInboundPlanId = partsInboundPlan.Id;
            this.WarehouseId = partsInboundPlan.WarehouseId;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseAreaCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DestWarehouseAreaCode
                    },new ColumnItem {
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Name = "SparePartName"
                    },new ColumnItem {
                        Name = "PlannedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "InspectedQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "Price",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "SparePart.MeasureUnit"
                    },new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "SpareOrderRemark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInboundPlanDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsInboundPlanDetailForWithSparePart";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["PlannedAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["InspectedQuantity"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "id":
                    return this.PartsInboundPlanId;
                case "warehouseId":
                    return this.WarehouseId;
                default:
                    return null;
            }
        }
    }
}
