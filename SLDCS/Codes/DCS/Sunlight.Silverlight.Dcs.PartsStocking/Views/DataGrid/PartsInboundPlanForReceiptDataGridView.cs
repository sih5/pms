﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInboundPlanForReceiptDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "Parts_InboundType", "PartsInboundPlan_Status"
        };

        public PartsInboundPlanForReceiptDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

         protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.AgencyLogisticCompany_Status
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_WarehouseCode,
                        Name = "WarehouseCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_WarehouseName,
                        Name = "WarehouseName"
                    }, new KeyValuesColumnItem {
                        Name = "InboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_InboundType
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount
                    }, new ColumnItem {
                        Name = "CounterpartCompanyCode",
                        Title = PartsStockingUIStrings.DataEditView_Text_CounterpartCompanyCode
                    }, new ColumnItem {
                        Name = "CounterpartCompanyName",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyName
                    },  new ColumnItem {
                        Name = "SourceCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_SourceCode
                    },new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_OriginalRequirementBillCode
                    },new ColumnItem {
                        Title=PartsStockingUIStrings.DataEditView_Title_PartsInboundPlan_ERPSourceOrderCode,
                        Name = "ERPSourceOrderCode"
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime
                    },new ColumnItem{
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ArrivalDate,
                        Name="ArrivalDate"
                    },new ColumnItem{
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_PlanDeliveryTime,
                        Name="PlanDeliveryTime"
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_PartsSalesCategoryName
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_BranchName,
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsInboundPlan);
            }
        }

        protected override string OnRequestQueryName() {
            return "检验入库查询配件入库计划";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsInboundPlanVirtualDetail"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 100;
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
        }

        private GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is VirtualPartsInboundPlan) {
                        var partsInboundPlan = item as VirtualPartsInboundPlan;
                        if(partsInboundPlan.Priority == 1)
                            return this.Resources["lightRedDataBackground"] as Style;
                        if(partsInboundPlan.Priority == 2)
                            return this.Resources["BlueDataBackground"] as Style;
                    }
                    return null;
                }
            };
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "sparePartCode":
                    var sparePartCodeFilterItem = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(e => e.MemberName == "SparePartCode");
                    return sparePartCodeFilterItem == null ? null : sparePartCodeFilterItem.Value;
                case "sparePartName":
                    var sparePartNameFilterItem = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(e => e.MemberName == "SparePartName");
                    return sparePartNameFilterItem == null ? null : sparePartNameFilterItem.Value;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "SparePartCode", "SparePartName" };
                foreach(var item in (compositeFilterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Where(e => !param.Contains(e.MemberName))) {
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
