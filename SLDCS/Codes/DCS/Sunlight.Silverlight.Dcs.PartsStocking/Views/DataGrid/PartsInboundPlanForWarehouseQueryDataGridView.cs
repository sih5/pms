﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid
{
    public class PartsInboundPlanForWarehouseQueryDataGridView :  DcsDataGridViewBase
    {
        public PartsInboundPlanForWarehouseQueryDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override void OnDataLoaded() {
            var partsInboundPlans = this.Entities.Cast<PartsInboundPlan>().ToArray();
            if(partsInboundPlans == null) {
                return;
            }
            if(partsInboundPlans.Any(r => r.InboundType == (int)DcsPartsInboundType.配件采购)) {
                var dcsDomainContext = this.DomainContext as DcsDomainContext;
                dcsDomainContext.Load(dcsDomainContext.GetPartsPurchaseOrderByIdsQuery(partsInboundPlans.Where(r => r.InboundType == (int)DcsPartsInboundType.配件采购).Select(r => r.OriginalRequirementBillId).ToArray()), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    foreach(var partsInboundPlan in partsInboundPlans.Where(r => r.InboundType == (int)DcsPartsInboundType.配件采购)) {
                        var partsPurchaseOrder = loadOp.Entities.FirstOrDefault(r => r.Id == partsInboundPlan.OriginalRequirementBillId);
                        if(partsPurchaseOrder != null) {
                            partsInboundPlan.PartsPurchaseOrderTypeName1 = partsPurchaseOrder.PartsPurchaseOrderType.Name;
                            partsInboundPlan.PlanSource1 = partsPurchaseOrder.PlanSource;
                        }
                    }
                }, null);
            }
        }

        private readonly string[] kvNames = new[] {
            "Parts_InboundType", "PartsInboundPlan_Status", "PartsSalesReturn_ReturnType"
        };

        protected override string OnRequestQueryName() {
            return "仓库人员查询配件入库计划2";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if (compositeFilterItem != null)
            {
                if (parameterName == "partsPurchaseOrderTypeId")
                {
                    var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "PartsPurchaseOrderTypeId");
                    return filter != null ? filter.Value : null;
                }

                if (parameterName == "erpSourceOrderCode")
                {
                    var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "PartsSalesReturnBill.PartsSalesOrder.ERPSourceOrderCode");
                    return filter != null ? filter.Value : null;
                }

                if (parameterName == "partsSalesOrderCode")
                {
                    var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "PartsSalesReturnBill.PartsSalesOrder.Code");
                    return filter != null ? filter.Value : null;
                }

                if (parameterName == "sparepartCode")
                {
                    var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "SparePartCode");
                    return filter != null ? filter.Value : null;
                }

                if (parameterName == "sparepartName")
                {
                    var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "SparePartName");
                    return filter != null ? filter.Value : null;
                }
                if(parameterName == "hasDifference") { 
                    var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "HasDifference"); 
                    return filter != null ? filter.Value : null; 
                } 
            }


            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var newCompositeFilterItem = new CompositeFilterItem();
                foreach (var filter in compositeFilterItem.Filters.Where(filter => filter.MemberName != "PartsSalesOrderCode" && filter.MemberName != "SparePartCode" && filter.MemberName != "SparePartName"&& filter.MemberName != "HasDifference" ))
                    newCompositeFilterItem.Filters.Add(filter);
                return newCompositeFilterItem.ToFilterDescriptor();
            }
            return base.OnRequestFilterDescriptor(queryName);
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(PartsInboundPlan);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title="状态"
                    },  new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_WarehouseName,
                        Name = "WarehouseName"
                    }, new KeyValuesColumnItem {
                        Name = "InboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsStockingUIStrings.QueryPanel_QueryItem_InboundType
                    }, new ColumnItem {
                        Name = "CounterpartCompanyCode",
                        Title=PartsStockingUIStrings.DataEditView_Text_CounterpartCompanyCode
                    }, new ColumnItem {
                        Name = "CounterpartCompanyName",
                        Title=PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyName
                    }
                    , new ColumnItem {
                        Name = "TotalAmountForWarehouse",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount
                    }
                    , new ColumnItem {
                        Name = "PartsPurchaseOrderTypeName1",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_PurchaseOrderType
                    },new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_OriginalRequirementBillCode          
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_WarehouseCode,
                        Name = "WarehouseCode"
                    }, new ColumnItem {
                        Name = "PlanSource1",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_PlanSource
                    },  new ColumnItem {
                        Name = "SourceCode",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_SourceCode
                    }, new KeyValuesColumnItem {
                        Name = "ReturnType",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ReturnType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Name = "Remark",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    },  new ColumnItem {
                        Name ="ERPSourceOrderCode",
                        Title = PartsStockingUIStrings.DataEditView_Title_PartsInboundPlan_ERPSourceOrderCode,
                    },new ColumnItem {
                        Name = "PartsSalesOrderCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsAdjustBill_SourceCode
                    },new ColumnItem {
                        Name = "PlanDeliveryTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_PlanDeliveryTime
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        IsSortDescending=true,
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime
                    },new ColumnItem{
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ArrivalDate,
                        Name="ArrivalDate"
                    },new ColumnItem {  
                        Title = PartsStockingUIStrings.QueryPanel_Title_HasDifference,  
                        Name = "HasDifference"  
                    }
                    , new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_PartsSalesCategoryName
                    }
                    , new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_BranchName,
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsInboundPlanDetailForWithWarehouse"
                    }
                };
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 100;
        }
    }
}