﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInboundPlanWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "Parts_InboundType", "PartsInboundPlan_Status"
        };

        public PartsInboundPlanWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_WarehouseCode,
                        Name = "WarehouseCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_WarehouseName,
                        Name = "WarehouseName"
                    }, new KeyValuesColumnItem {
                        Name = "InboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount
                    }, new ColumnItem {
                        Name = "CounterpartCompanyCode"
                    }, new ColumnItem {
                        Name = "CounterpartCompanyName"
                    },  new ColumnItem {
                        Name = "SourceCode"
                    },new ColumnItem {
                        Name = "OriginalRequirementBillCode"
                    },new ColumnItem {
                        Title=PartsStockingUIStrings.DataEditView_Title_PartsInboundPlan_ERPSourceOrderCode,
                        Name = "ERPSourceOrderCode"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem{
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ArrivalDate,
                        Name="ArrivalDate"
                    },new ColumnItem{
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_PlanDeliveryTime,
                        Name="PlanDeliveryTime"
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_PartsSalesCategoryName
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_BranchName,
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInboundPlan);
            }
        }

        protected override string OnRequestQueryName() {
            return "检验区负责人查询配件入库计划";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsInboundPlanDetail"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 100;
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            switch(parameterName) {
                case "sparePartCode":
                    var sparePartCodeFilterItem = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(e => e.MemberName == "SparePartCode");
                    return sparePartCodeFilterItem == null ? null : sparePartCodeFilterItem.Value;
                case "sparePartName":
                    var sparePartNameFilterItem = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(e => e.MemberName == "SparePartName");
                    return sparePartNameFilterItem == null ? null : sparePartNameFilterItem.Value;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "SparePartCode", "SparePartName" };
                foreach(var item in (compositeFilterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Where(e => !param.Contains(e.MemberName))) {
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
