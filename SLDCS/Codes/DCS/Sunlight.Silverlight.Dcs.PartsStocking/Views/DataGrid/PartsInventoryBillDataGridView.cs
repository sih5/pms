﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInventoryBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "Area_Category", "PartsInventoryBill_Status"
        };

        public PartsInventoryBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInventoryBillEx);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem{
                        Name = "Status",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsOutboundBill_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {  
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_IsUploadFile,  
                        Name = "IsUplodFile"  
                    }, new ColumnItem{
                        Name = "Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_Code
                    }, new ColumnItem{
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseName
                    }, new KeyValuesColumnItem{
                        Name = "WarehouseAreaCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseAreaCategory
                    },  new ColumnItem{
                        Name = "InitiatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_InitiatorName
                    }, new ColumnItem{
                        Name = "InventoryReason",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_InventoryReason
                    }, new ColumnItem{
                        Name = "SumCostBeforeInventory",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostBeforeInventory
                    }, new ColumnItem{
                        Name = "SumCostAfterInventory",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostAfterInventory
                    }, new ColumnItem{
                        Name = "SumCostDifference",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostDifference
                    }, new ColumnItem{
                        Name = "Remark",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    }, new ColumnItem{
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime
                    }, new ColumnItem{
                        Name = "InitialApproverName",
                        Title = PartsStockingUIStrings.DataGridView_Column_InitApprover
                    }, new ColumnItem{
                        Name = "InitialApproveTime",
                        Title =  PartsStockingUIStrings.DataGridView_Column_InitApproveTime
                    }, new ColumnItem{
                        Name = "CheckerName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CheckerName
                    }, new ColumnItem{
                        Name = "CheckTime",
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_CheckTime
                    }, new ColumnItem{
                        Name = "ApproverName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ApproverName
                    }, new ColumnItem{
                        Name = "ApproveTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ApproveTime
                    }, new ColumnItem{
                        Name = "RejecterName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejecterName
                    }, new ColumnItem{
                        Name = "RejectTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejectTime
                    }, new ColumnItem{
                        Name = "ResultInputOperatorName",
                        Title = "结果录入人"
                    }, new ColumnItem{
                        Name = "ResultInputTime",
                        Title =  "结果录入时间"
                    }, new ColumnItem{
                        Name = "InventoryRecordOperatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RecordOperatorName
                    }, new ColumnItem{
                        Name = "InventoryRecordTime",
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_RecordTime
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsInventoryBillWithSumPriceByPersonnelId";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] { 
                        "PartsInventoryBillDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();
        }
    }
}
