﻿using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInventoryBillDetailForBranchManagementDataGridView : PartsInventoryBillDetailForReplaceReportedDataGridView {

        public PartsInventoryBillDetailForBranchManagementDataGridView() {
            this.DataContextChanged += this.CompanyStockForReplaceDataGridView_DataContextChanged;
        }
        private void CompanyStockForReplaceDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var companyPartsStock = e.NewValue as PartsInventoryBillEx;
            if(companyPartsStock == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            //if(this.FilterItem == null)
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "PartsInventoryBillId",
                    MemberType = typeof(int),
                    Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsEqualTo,
                    Value = companyPartsStock.Id
                });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

    }
}
