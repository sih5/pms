﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInventoryBillDetailForReplaceReportedDataGridView : DcsDataGridViewBase {

        public PartsInventoryBillDetailForReplaceReportedDataGridView() {
            this.DataContextChanged += this.CompanyStockForReplaceDataGridView_DataContextChanged;
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            base.OnControlsCreated();
        }

        private void CompanyStockForReplaceDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var companyPartsStock = e.NewValue as PartsInventoryBillEx;
            if(companyPartsStock == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            if(this.FilterItem == null)
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "PartsInventoryBillId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = companyPartsStock.Id
                });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartName
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode",
                    }, new ColumnItem {
                        Name = "CurrentStorage",
                    }, new ColumnItem {
                        Name = "StorageAfterInventory",
                    },new ColumnItem{
                        Name="StorageDifference",
                    }, new ColumnItem{
                        Name = "CostPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CostPrice
                    }, new KeyValuesColumnItem {
                        Name = "Remark",
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInventoryDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsInventoryDetails";
        }
    }
}
