﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInventoryBillForEditApproveDataGridView : DcsDataGridViewBase {

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsInventoryDetails");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true,
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartName
                    }, new ColumnItem {
                        Name = "NewStorageDifference",
                        IsReadOnly = true,
                        Title="库存差异"
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true,
                    },new ColumnItem {
                        Name = "CurrentStorage",
                        IsReadOnly = true,
                    }, new ColumnItem {
                        Name = "NewStorageAfterInventory",
                        Title="盘点后库存",
                        IsReadOnly = true,
                    }, new ColumnItem {
                        Name = "Ifcover",
                        IsReadOnly = true,
                    }, new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true,
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInventoryDetail);
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            base.OnControlsCreated();
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
