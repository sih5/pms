﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInventoryBillReportedDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = new[] {
            "Area_Category", "PartsInventoryBill_Status"
        };

        public PartsInventoryBillReportedDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_Code
                    },new ColumnItem { 
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_IsUploadFile, 
                        Name = "IsUplodFile" 
                    }, new ColumnItem {
                        Name = "WarehouseName",
                         Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseName
                    }, new KeyValuesColumnItem {
                        Name = "WarehouseAreaCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseAreaCategory
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_Status
                    }, new ColumnItem {
                        Name = "InitiatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_InitiatorName
                    },new ColumnItem{
                        Name="InventoryReason",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_InventoryReason
                    }, new ColumnItem{
                        Name = "RejectComment",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_RejectComment
                    }, new ColumnItem{
                        Name = "RejecterName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejecterName
                    }, new ColumnItem{
                        Name = "RejectTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_RejectTime
                    }, new ColumnItem{
                        Name = "SumCostBeforeInventory",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostBeforeInventory
                    }, new ColumnItem{
                        Name = "SumCostAfterInventory",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostAfterInventory
                    }, new ColumnItem{
                        Name = "SumCostDifference",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SumCostDifference
                    },new  ColumnItem {
                        Name = "Remark",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_Remark
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInventoryBillEx);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetAgencyPartsInventoryBill";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                       "PartsInventoryBillDetailForReplaceReported"
                    }
                };
            }
        }
    }
}
