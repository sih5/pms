﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInventoryBillReportedForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase virtualPartsStockQueryWindow;
        private DcsMultiPopupsQueryWindowBase virtualPartsStockMultiSelectQueryWindow;
        private RadWindow radQueryWindow;


        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.VirtualPartsStockMultiSelectQueryWindow,
                    Header = PartsStockingUIStrings.DataGridView_Title_WarehouseAreaQuery,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }
        private DcsMultiPopupsQueryWindowBase VirtualPartsStockMultiSelectQueryWindow {
            get {
                if(this.virtualPartsStockMultiSelectQueryWindow == null) {
                    this.virtualPartsStockMultiSelectQueryWindow = DI.GetQueryWindow("VirtualPartsStockMultiSelect") as DcsMultiPopupsQueryWindowBase;
                    this.virtualPartsStockMultiSelectQueryWindow.SelectionDecided += this.VirtualPartsStockMultiSelectQueryWindow_SelectionDecided;
                    this.virtualPartsStockMultiSelectQueryWindow.Loaded += VirtualPartsStockMultiSelectQueryWindow_Loaded;
                }
                return this.virtualPartsStockMultiSelectQueryWindow;
            }
        }

        public void VirtualPartsStockMultiSelectQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var multiSelectQueryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(multiSelectQueryWindow == null)
                return;
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.LogicalOperator = LogicalOperator.And;
            if(partsInventoryBill.WarehouseId != default(int)) {
                compositeFilterItem.Filters.Add(new FilterItem("WarehouseId", typeof(int), FilterOperator.IsEqualTo, partsInventoryBill.WarehouseId));
                multiSelectQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "WarehouseId", partsInventoryBill.WarehouseId });
                this.VirtualPartsStockMultiSelectQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "WarehouseId", false });
            }

            if(partsInventoryBill.WarehouseAreaCategory != default(int)) {
                compositeFilterItem.Filters.Add(new FilterItem("WarehouseAreaCategory", typeof(int), FilterOperator.IsEqualTo, partsInventoryBill.WarehouseAreaCategory));
                multiSelectQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "WarehouseAreaCategory", partsInventoryBill.WarehouseAreaCategory });
                this.VirtualPartsStockMultiSelectQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "WarehouseAreaCategory", false });
            }
            multiSelectQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void VirtualPartsStockMultiSelectQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            var multiSelectQueryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(multiSelectQueryWindow == null || multiSelectQueryWindow.SelectedEntities == null)
                return;
            var virtualPartsStock = multiSelectQueryWindow.SelectedEntities.Cast<VirtualPartsStock>();
            if(virtualPartsStock == null)
                return;

            foreach(var item in virtualPartsStock) {
                if(partsInventoryBill.PartsInventoryDetails.Any(ex => ex.SparePartId == item.SparePartId && ex.WarehouseAreaId == item.WarehouseAreaId)) {
                    UIHelper.ShowNotification(string.Format(PartsStockingUIStrings.DataGridView_Notification_WarehouseAreaHasSameData, item.WarehouseAreaCode,item.SparePartCode));
                    return;
                }
                partsInventoryBill.PartsInventoryDetails.Add(new PartsInventoryDetail {
                    SparePartId = item.SparePartId,
                    SparePartCode = item.SparePartCode,
                    SparePartName = item.SparePartName,
                    WarehouseAreaId = item.WarehouseAreaId,
                    WarehouseAreaCode = item.WarehouseAreaCode,
                    CurrentBatchNumber = item.BatchNumber,
                    CurrentStorage = item.Quantity,
                    StorageAfterInventory = 0,
                    StorageDifference = 0
                });
            }
        }

        private QueryWindowBase VirtualPartsStockQueryWindow {
            get {
                if(this.virtualPartsStockQueryWindow == null) {
                    this.virtualPartsStockQueryWindow = DI.GetQueryWindow("VirtualPartsStockDropDown");
                    this.virtualPartsStockQueryWindow.Loaded += virtualPartsStockQueryWindow_Loaded;
                    this.virtualPartsStockQueryWindow.SelectionDecided += this.VirtualPartsStockQueryWindow_SelectionDecided;
                }
                return this.virtualPartsStockQueryWindow;
            }
        }

        private void virtualPartsStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.LogicalOperator = LogicalOperator.And;
            if(partsInventoryBill.WarehouseId != default(int)) {
                compositeFilterItem.Filters.Add(new FilterItem("WarehouseId", typeof(int), FilterOperator.IsEqualTo, partsInventoryBill.WarehouseId));
                queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "WarehouseId", partsInventoryBill.WarehouseId });
                this.VirtualPartsStockQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "WarehouseId", false });
            }

            if(partsInventoryBill.WarehouseAreaCategory != default(int)) {
                compositeFilterItem.Filters.Add(new FilterItem("WarehouseAreaCategory", typeof(int), FilterOperator.IsEqualTo, partsInventoryBill.WarehouseAreaCategory));
                queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "WarehouseAreaCategory", partsInventoryBill.WarehouseAreaCategory });
                this.VirtualPartsStockQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "WarehouseAreaCategory", false });
            }
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInventoryDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new DropDownTextBoxColumnItem{
                        Name = "SparePartCode",
                        IsEditable = false,
                        DropDownContent = this.VirtualPartsStockQueryWindow
                    }, new ColumnItem{
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsInventoryDetails");
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            base.OnControlsCreated();
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            if(partsInventoryBill.WarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_WarehouseNotNull);
                e.Cancel = true;
                return;
            }
            if(partsInventoryBill.WarehouseAreaCategory == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_AreaCategoryNotNull);
                e.Cancel = true;
                return;
            }
            this.RadQueryWindow.ShowDialog();
            e.Cancel = true;
        }

        private void VirtualPartsStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualPartsStock = queryWindow.SelectedEntities.Cast<VirtualPartsStock>().FirstOrDefault();
            if(virtualPartsStock == null)
                return;

            var partsInventoryDetail = queryWindow.DataContext as PartsInventoryDetail;
            if(partsInventoryDetail == null)
                return;
            if(partsInventoryDetail.Id == default(int))
                partsInventoryDetail.Id = GlobalVar.ASSIGNED_BY_SERVER_INT;

            partsInventoryDetail.SparePartId = virtualPartsStock.SparePartId;
            partsInventoryDetail.SparePartCode = virtualPartsStock.SparePartCode;
            partsInventoryDetail.SparePartName = virtualPartsStock.SparePartName;
            partsInventoryDetail.WarehouseAreaId = virtualPartsStock.WarehouseAreaId;
            partsInventoryDetail.WarehouseAreaCode = virtualPartsStock.WarehouseAreaCode;
            partsInventoryDetail.CurrentBatchNumber = virtualPartsStock.BatchNumber;
            partsInventoryDetail.CurrentStorage = virtualPartsStock.Quantity;
            partsInventoryDetail.StorageAfterInventory = 0;
            partsInventoryDetail.StorageDifference = 0;
            //if(partsInventoryBill.PartsInventoryDetails.Any(r => r.SparePartId == virtualPartsStock.SparePartId)) {
            //    UIHelper.ShowAlertMessage(string.Format(PartsStockingUIStrings.DataGridView_Validation_OverstockPartsAppDetail_SparePartCodeIsExist, virtualPartsStock.SparePartCode));
            //    return;
            //}
            //partsInventoryBill.PartsInventoryDetails.Add(new PartsInventoryDetail {
            //    Id = GlobalVar.ASSIGNED_BY_SERVER_INT,
            //    PartsInventoryBillId = partsInventoryBill.Id,
            //    SparePartId = virtualPartsStock.SparePartId,
            //    SparePartCode = virtualPartsStock.SparePartCode,
            //    SparePartName = virtualPartsStock.SparePartName,
            //    WarehouseAreaId = virtualPartsStock.WarehouseAreaId,
            //    WarehouseAreaCode = virtualPartsStock.WarehouseAreaCode,
            //    CurrentBatchNumber = virtualPartsStock.BatchNumber,
            //    CurrentStorage = virtualPartsStock.Quantity,
            //    StorageAfterInventory = 0,
            //    StorageDifference = 0
            //});
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
