﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInventoryDetailDataGridView : DcsDataGridViewBase {
        public PartsInventoryDetailDataGridView() {
            this.DataContextChanged += PartsInventoryDetailDataGridView_DataContextChanged;
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInventoryDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SparePartCode"
                    }, new ColumnItem{
                        Name = "SparePartName"
                    }, new ColumnItem{
                        Name = "WarehouseAreaCode"
                    }, new ColumnItem{
                        Name = "CurrentStorage"
                    }, new ColumnItem{
                        Name = "NewStorageAfterInventory",
                        Title="盘点后库存"
                    }, new ColumnItem{
                        Name = "NewStorageDifference",
                        Title="库存差异"
                    }, new ColumnItem{
                        Name = "CostPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CostPrice
                    }, new ColumnItem{
                        Name = "Ifcover"
                    }, new ColumnItem{
                        Name = "Remark"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsInventoryDetailsDesc";
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
            base.OnControlsCreated();
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] { 
                        "TraceTempType"
                    }
                };
            }
        }
        private void PartsInventoryDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsInventoryBill = e.NewValue as PartsInventoryBillEx;
            if(partsInventoryBill == null || partsInventoryBill.Id == default(int))
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "PartsInventoryBillId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = partsInventoryBill.Id
                };
            this.ExecuteQueryDelayed();
        }
    }
}
