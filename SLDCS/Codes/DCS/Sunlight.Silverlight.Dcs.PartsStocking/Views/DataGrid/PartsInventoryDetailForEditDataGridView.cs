﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInventoryDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase virtualPartsStockQueryWindow;

        private DcsMultiPopupsQueryWindowBase VirtualPartsStockQueryWindow {
            get {
                if(this.virtualPartsStockQueryWindow == null) {
                    this.virtualPartsStockQueryWindow = DI.GetQueryWindow("VirtualPartsStockDropDown") as DcsMultiPopupsQueryWindowBase;
                    this.virtualPartsStockQueryWindow.SelectionDecided += this.VirtualPartsStockQueryWindow_SelectionDecided;
                }
                return this.virtualPartsStockQueryWindow;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInventoryDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="SparePartCode",
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsInventoryDetails");
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            if(partsInventoryBill.WarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_WarehouseNotNull);
                return;
            }
            if(partsInventoryBill.WarehouseAreaCategory == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_PartsInventoryDetail_AreaCategoryNotNull);
                return;
            }
            this.VirtualPartsStockQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { 
                "Common", "WarehouseId", partsInventoryBill.WarehouseId
            });
            this.VirtualPartsStockQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { 
                "Common", "WarehouseAreaCategory", partsInventoryBill.WarehouseAreaCategory
            });
            this.VirtualPartsStockQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { 
                "Common", "WarehouseId", false
            });
            this.VirtualPartsStockQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { 
                "Common", "WarehouseAreaCategory", false
            });
            RadQueryWindow.ShowDialog();
        }

        private RadWindow radQueryWindow;
        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.VirtualPartsStockQueryWindow,
                    Header = PartsStockingUIStrings.QueryPanel_Title_VirtualPartsStock,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private void VirtualPartsStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var virtualPartsStock = queryWindow.SelectedEntities.Cast<VirtualPartsStock>().ToArray();
            if(virtualPartsStock == null)
                return;
            var partsInventoryBill = this.DataContext as PartsInventoryBill;
            if(partsInventoryBill == null)
                return;
            foreach(var virtualPartsStocks in virtualPartsStock) {
                var partsInventoryDetails = new PartsInventoryDetail {
                    SparePartId = virtualPartsStocks.SparePartId,
                    SparePartCode = virtualPartsStocks.SparePartCode,
                    SparePartName = virtualPartsStocks.SparePartName,
                    WarehouseAreaId = virtualPartsStocks.WarehouseAreaId,
                    WarehouseAreaCode = virtualPartsStocks.WarehouseAreaCode,
                    CurrentBatchNumber = virtualPartsStocks.BatchNumber,
                    CurrentStorage = virtualPartsStocks.Quantity,
                    StorageAfterInventory = 0,
                    StorageDifference = 0
                };
                partsInventoryBill.PartsInventoryDetails.Add(partsInventoryDetails);
            }
            //if(partsInventoryDetail.PartsInventoryDetails.Any(r => r.SparePartId == virtualPartsStock.SparePartId)) {
            //    UIHelper.ShowAlertMessage(string.Format(PartsStockingUIStrings.DataGridView_Validation_OverstockPartsAppDetail_SparePartCodeIsExist, virtualPartsStock.SparePartCode));
            //    return;
            //}
            //partsInventoryBill.PartsInventoryDetails.Add(new PartsInventoryDetail {
            //    Id = GlobalVar.ASSIGNED_BY_SERVER_INT,
            //    PartsInventoryBillId = partsInventoryBill.Id,
            //    SparePartId = virtualPartsStock.SparePartId,
            //    SparePartCode = virtualPartsStock.SparePartCode,
            //    SparePartName = virtualPartsStock.SparePartName,
            //    WarehouseAreaId = virtualPartsStock.WarehouseAreaId,
            //    WarehouseAreaCode = virtualPartsStock.WarehouseAreaCode,
            //    CurrentBatchNumber = virtualPartsStock.BatchNumber,
            //    CurrentStorage = virtualPartsStock.Quantity,
            //    StorageAfterInventory = 0,
            //    StorageDifference = 0
            //});
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
