﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInventoryDetailForInventoryCoversDataGridView : DcsDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(PartsInventoryDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "Ifcover"
                    }, new ColumnItem{
                        Name = "StorageDifference",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "CurrentStorage",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "StorageAfterInventory",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsInventoryDetails");
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();
        }

    }
}
