﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsInventoryDetailForResultEntryDataGridView : DcsDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(PartsInventoryDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "StorageDifference",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "CurrentStorage",
                        IsReadOnly = true
                    }, new ColumnItem{
                        Name = "StorageAfterInventoryNew",
                        Title="盘点后库存"
                    }, new ColumnItem{
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsInventoryDetails");
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var detail = e.Cell.DataContext as PartsInventoryDetail;
            if(detail == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "StorageAfterInventoryNew":
                    detail.StorageDifference = (detail.StorageAfterInventoryNew??0) - detail.CurrentStorage;
                    break;
                default:
                    break;
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            //this.GridView.RowLoaded += GridView_RowLoaded;
            base.OnControlsCreated();
        }

        //private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
        //    var partsInventoryDetail = e.Row.DataContext as PartsInventoryDetail;
        //    if(partsInventoryDetail == null)
        //        return;
        //    partsInventoryDetail.StorageDifference = partsInventoryDetail.StorageAfterInventory - partsInventoryDetail.CurrentStorage;
        //}

    }
}
