﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsLocationAssignmentForImportAreaDataGridView : DcsDataGridViewBase {
        protected override Type EntityType {
            get {
                return typeof(WarehouseAreaForStockExtend);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                            Name = "WarehouseAreaCode",
                            Title=PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_WarehouseAreaCode
                        },new ColumnItem {
                            Name = "PartCode",
                            Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartCode
                        },new ColumnItem {
                            Name = "PartName",
                            Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SparePartName
                        }
                        
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("WarehouseAreaForStockExtends");
        }
    }
}
