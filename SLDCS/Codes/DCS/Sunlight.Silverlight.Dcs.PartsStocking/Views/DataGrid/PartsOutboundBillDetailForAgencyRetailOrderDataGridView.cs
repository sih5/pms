﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsOutboundBillDetailForAgencyRetailOrderDataGridView : DcsDataGridViewBase {

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsOutboundBillDetail = e.Row.Item as PartsOutboundBillDetail;
            if(partsOutboundBillDetail == null)
                return;
            if(!string.IsNullOrWhiteSpace(partsOutboundBillDetail.BatchNumber))
                e.Cancel = true;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OrderedQuantity",
                        Title = PartsStockingUIStrings.DataEditView_Title_PlanAmount,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlanDetail_OutboundFulfillment2,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "CurrentOutQuantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_CurrentOutboundAmount,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(AgencyRetailerList);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("AgencyRetailerLists");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
        }
    }
}
