﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsOutboundBillDetailForDetailDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OutboundAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBillDetail_OutboundAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                         IsReadOnly = true
                    //}, new ColumnItem {
                    //    Name = "BatchNumber",
                    //    IsReadOnly = true
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOutboundBillDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsOutboundBillDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["OutboundAmount"]).DataFormatString = "d";
        }

    }
}
