﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsOutboundBillDetailReturnDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                    }, new ColumnItem {
                        Name = "SparePartName",
                    }, new ColumnItem {
                        Name = "OutboundAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "CanReturnAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBillDetail_CanReturnAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode",
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOutboundBillDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsOutboundBillDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.CanUserInsertRows = true;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["OutboundAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CanReturnAmount"]).DataFormatString = "d";
        }

        public PartsOutboundBillDetailReturnDataGridView() {
        }
    }
}
