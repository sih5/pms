﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsOutboundBillReturnDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames ={
            "Parts_OutboundType","Parts_SettlementStatus"
        };
        public PartsOutboundBillReturnDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }
        protected override bool UsePaging {
            get {
                return true;
            }
        }
        protected override Type EntityType {
            get {
                return typeof(PartsOutboundBill);
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[]{
                   new KeyValuesColumnItem{
                        Name="SettlementStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundReturn_SettlementStatus
                   }, new ColumnItem{
                         Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundReturn_Code,
                         Name="Code"
                   },new ColumnItem{
                       Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundReturn_WarehouseCode,
                       Name="WarehouseCode"
                   },new ColumnItem{
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundReturn_WarehouseName,
                        Name="WarehouseName"
                   },new KeyValuesColumnItem{
                        Name="OutboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                   },new ColumnItem{
                        Name="CounterpartCompanyCode"
                   },new ColumnItem{
                        Name="CounterpartCompanyName"
                   },new ColumnItem{
                        Name="PartsOutboundPlan.SourceCode"
                   },new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem{
                        Name="BranchName",
                        Title =PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundReturn_BranchName
                   }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "仓库负责人查询可退库配件出库单";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "userId":
                        return BaseApp.Current.CurrentUserData.UserId;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }
    }
}
