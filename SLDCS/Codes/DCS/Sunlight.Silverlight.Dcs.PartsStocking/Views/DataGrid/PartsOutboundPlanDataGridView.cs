﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsOutboundPlanDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Parts_OutboundType", "PartsOutboundPlan_Status"
        };

        public PartsOutboundPlanDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOutboundPlan);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },  new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_WarehouseCode,
                        Name = "WarehouseCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_WarehouseName,
                        Name = "WarehouseName"
                    }, new KeyValuesColumnItem {
                        Name = "OutboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    } , new ColumnItem {
                        Name = "TotalAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_SalesOrderType,
                        Name = "PartsSalesOrderTypeName"
                    }, new ColumnItem {
                        Name = "CounterpartCompanyCode"
                    }, new ColumnItem {
                        Name = "CounterpartCompanyName"
                    },  new ColumnItem {
                        Name = "ReceivingWarehouseCode"
                    },  new ColumnItem {
                        Name = "ReceivingWarehouseName"
                    },new ColumnItem {
                        Name = "SourceCode"
                    }, new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "OrderApproveComment"
                    },new ColumnItem {
                        Name = "ERPSourceOrderCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsSalesOrder_ERPOrderCode
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_PartsSalesCategoryName
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_BranchName,
                        Name = "BranchName"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "保管区负责人查询配件出库计划";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "userinfoId":
                        return BaseApp.Current.CurrentUserData.UserId;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 100;
        }
    }
}
