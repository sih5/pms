﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsOutboundPlanDetailDataGridView : DcsDataGridViewBase {
        public PartsOutboundPlanDetailDataGridView() {
            this.DataContextChanged += this.PartsOutboundPlanDetailDataGridView_DataContextChanged;
        }

        private void PartsOutboundPlanDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsOutboundPlan = e.NewValue as PartsOutboundPlan;
            if(partsOutboundPlan == null || partsOutboundPlan.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsOutboundPlanId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsOutboundPlan.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                PartsOutboundPlanDetail a = new PartsOutboundPlanDetail();

                return typeof(PartsOutboundPlanDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlanDetail_OutboundFulfillment,
                        Name = "OutboundFulfillment",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Price",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },// new ColumnItem {
                    //    Name = "OriginalPrice",
                    //    MaskType = MaskType.Numeric,
                    //    TextAlignment = TextAlignment.Right,
                    //    Title = "电商价格"
                    //},
                    new ColumnItem {
                        Name = "SparePart.MeasureUnit",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SparePart_MeasureUnit
                    }, new ColumnItem {
                            Name = "Remark"
                        }
                    };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsOutboundPlanDetailsWithSparePart";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["PlannedAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OutboundFulfillment"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
        }
    }
}
