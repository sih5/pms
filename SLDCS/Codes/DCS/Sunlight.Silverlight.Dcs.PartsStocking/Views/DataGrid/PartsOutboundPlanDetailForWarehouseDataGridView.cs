﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.Web.Entities;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid
{
    public class PartsOutboundPlanDetailForWarehouseDataGridView: DcsDataGridViewBase {
        public PartsOutboundPlanDetailForWarehouseDataGridView()
        {
            this.DataContextChanged += this.PartsOutboundPlanDetailDataGridView_DataContextChanged;
        }

        private void PartsOutboundPlanDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsOutboundPlan = e.NewValue as VirtualPartsOutboundPlan;
            if(partsOutboundPlan == null || partsOutboundPlan.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsOutboundPlanId",
                MemberType = typeof(int),
                Operator = Sunlight.Silverlight.Core.Model.FilterOperator.IsEqualTo,
                Value = partsOutboundPlan.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                VirtualPartsOutboundPlanDetail a = new VirtualPartsOutboundPlanDetail();

                return typeof(VirtualPartsOutboundPlanDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title=PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title=PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartName
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsStockingUIStrings.DataEditView_Title_PlanAmount
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlanDetail_OutboundFulfillment,
                        Name = "OutboundFulfillment",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "Price",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Price
                    },
                    new ColumnItem {
                        Name = "MeasureUnit",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SparePart_MeasureUnit
                    }, new ColumnItem {
                            Name = "Remark",
                            Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                        }
                    };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsOutboundPlanDetailsWithSparePart2";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "partsOutboundPlanId":
                        return filters.Filters.Single(item => item.MemberName == "PartsOutboundPlanId").Value;                   
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["PlannedAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OutboundFulfillment"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
        }
    }
}
