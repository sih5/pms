﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsOutboundPlanForShippingDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Parts_OutboundType", "PartsOutboundPlan_Status","PartsShipping_Method"
        };

        public PartsOutboundPlanForShippingDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(TaskListPartsOutboundPlan);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                         Title = PartsStockingUIStrings.DataGridView_ColumnItem_PlanOrderStatus,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TaskCode,
                        Name = "TaskCode"
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TaskStatus,
                        Name = "TaskStatus"
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_Code,
                        Name = "Code"
                    },new ColumnItem {
                         Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyName,
                        Name = "CounterpartCompanyName"
                    }, new ColumnItem {
                        Name = "PartsSalesOrderTypeName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_PartsSalesOrderTypeName,
                    },  new ColumnItem {
                         Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_ReceivingWarehouseName,
                        Name = "ReceivingWarehouseName"
                    }, new ColumnItem {
                         Title = PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_Remark,
                        Name = "Remark"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_WarehouseCode,
                        Name = "WarehouseCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_WarehouseName,
                        Name = "WarehouseName"
                    }, new KeyValuesColumnItem {
                         Title = PartsStockingUIStrings.QueryPanel_Title_OutboundType,
                        Name = "OutboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_CounterpartCompanyCode,
                        Name = "CounterpartCompanyCode"
                    }, new KeyValuesColumnItem{
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingMethod
                    },new ColumnItem{
                        Name="OrderApproveComment",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_OrderApproveComment
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreatorName,
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreateTime,
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName,
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime,
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },  new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_BranchName,
                        Name = "BranchName"
                    },  new ColumnItem {
                        Title = "是否转单",
                        Name = "IsTurn"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Extended;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsOutboundPlans");
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "BoxUpTaskDetailForShipping","PartsOutboundPlanDetailForShipping"
                    }
                };
            }
        }
    }
}