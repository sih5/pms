﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsOutboundPlanWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Parts_OutboundType", "PartsOutboundPlan_Status","PartsShipping_Method"
        };

        public PartsOutboundPlanWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_Code,
                        Name = "Code"
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_WarehouseName,
                        Name = "WarehouseName"
                    },
                    new KeyValuesColumnItem {
                        Name = "OutboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },
                    new ColumnItem {
                        Name = "TotalAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TotalCount,
                        Name = "TotalCount"
                    },
                    new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_OriginalRequirementBillCode
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_SalesOrderType,
                        Name = "PartsSalesOrderTypeName"
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingCode,
                        Name = "PartsShippingOrderCode"
                    },
                    new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                         Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingMethod,
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    },
                    new ColumnItem {
                        Name = "CounterpartCompanyName"
                    },                   
                    new ColumnItem {
                        Name = "OrderApproveComment",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_OrderApproveComment
                    },
                    new ColumnItem {
                        Name = "ReceivingAddress",
                        Title = PartsStockingUIStrings.DataEditView_Text_HauDistanceInfor_CompanyAddress
                    },
                    new ColumnItem {
                        Name = "IsInternalAllocationBill",
                        Title = PartsStockingUIStrings.DataEditView_Text_HauDistanceInfor_IsInternalAllocationBill
                    },
                    new ColumnItem {
                        Name = "Remark"
                    },
                    new ColumnItem {
                        Name = "CreatorName"
                    },
                    new ColumnItem {
                        Name = "ModifierName"
                    },
                    new ColumnItem {
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "StopComment"
                    },
                    new ColumnItem {
                        Name = "Stoper"
                    },
                    new ColumnItem {
                        Name = "StopTime"
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_WarehouseCode,
                        Name = "WarehouseCode"
                    },
                    new ColumnItem {
                        Name = "CounterpartCompanyCode"
                    },
                    new ColumnItem {
                        Name = "ReceivingWarehouseCode"
                    },
                    new ColumnItem {
                        Name = "ReceivingWarehouseName"
                    } ,
                    new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_PartsSalesCategoryName
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_BranchName,
                        Name = "BranchName"
                    }, new ColumnItem {
                        Title = "是否转单",
                        Name = "IsTurn"
                    },
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsOutboundPlansWithPartsCategory";
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOutboundPlan);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsOutboundPlanDetail"
                    }
                };
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;         
            if (parameterName == "sparePartCode")
            {
                var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "SparePartCode");
                return filter != null ? filter.Value : null;
            }
            if (parameterName == "sparePartName")
            {
                var filter = compositeFilterItem.Filters.SingleOrDefault(filterItem => filterItem.MemberName == "SparePartName");
                return filter != null ? filter.Value : null;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }     
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 100;
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                var param = new[] { "SparePartCode", "SparePartName"};
                foreach (var filterItem in compositeFilterItem.Filters.Where(filter => !param.Contains(filter.MemberName)))
                {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
