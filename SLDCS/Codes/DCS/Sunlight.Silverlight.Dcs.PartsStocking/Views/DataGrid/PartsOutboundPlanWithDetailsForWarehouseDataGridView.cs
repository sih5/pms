﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid
{
    public class PartsOutboundPlanWithDetailsForWarehouseDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = {
            "Parts_OutboundType", "PartsOutboundPlan_Status","PartsShipping_Method"
        };

        public PartsOutboundPlanWithDetailsForWarehouseDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_Code,
                        Name = "Code"
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_WarehouseName,
                        Name = "WarehouseName"
                    },
                    new KeyValuesColumnItem {
                        Name = "OutboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=PartsStockingUIStrings.QueryPanel_Title_OutboundType
                    },
                    new ColumnItem {
                        Name = "TotalAmountForWarehouse",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TotalCount,
                        Name = "TotalCount"
                    },
                    new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CreateTime
                    },
                    new ColumnItem {
                        Name = "OriginalRequirementBillCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_OriginalRequirementBillCode
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_SalesOrderType,
                        Name = "PartsSalesOrderTypeName"
                    },
                    new ColumnItem { 
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingCode, 
                        Name = "PartsShippingOrderCode" 
                    }, 
                    new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                         Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingMethod,
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    },
                    new ColumnItem {
                        Name = "CounterpartCompanyName",
                        Title= PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_CounterpartCompanyName
                    },                   
                    new ColumnItem {
                        Name = "OrderApproveComment",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_OrderApproveComment
                    },
                    new ColumnItem {
                        Name = "ReceivingAddress",
                        Title = PartsStockingUIStrings.DataEditView_Text_HauDistanceInfor_CompanyAddress
                    },
                    new ColumnItem {
                        Name = "IsInternalAllocationBill",
                        Title = PartsStockingUIStrings.DataEditView_Text_HauDistanceInfor_IsInternalAllocationBill
                    } ,
                    new ColumnItem {
                        Name = "Remark",
                        Title=PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_Remark
                    },
                    new ColumnItem {
                        Name = "CreatorName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreatorName
                    },
                    new ColumnItem {
                        Name = "ModifierName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    },
                    new ColumnItem {
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime
                    },
                    new ColumnItem {
                        Name = "StopComment",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_TerminateReason
                    },
                    new ColumnItem {
                        Name = "Stoper",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Terminater
                    },
                    new ColumnItem {
                        Name = "StopTime",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_TerminateTime
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_WarehouseCode,
                        Name = "WarehouseCode"
                    },
                    new ColumnItem {
                        Name = "CounterpartCompanyCode",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyCode
                    },
                    new ColumnItem {
                        Name = "ReceivingWarehouseCode",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_ReceivingWarehouseCode
                    },
                    new ColumnItem {
                        Name = "ReceivingWarehouseName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_ReceivingWarehouseName
                    },
                    new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_PartsSalesCategoryName
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_BranchName,
                        Name = "BranchName"
                    }, new ColumnItem {
                        Title = "是否转单",
                        Name = "IsTurn"
                    },

                };
            }
        }

        protected override string OnRequestQueryName()
        {
            return "GetPartsOutboundPlansWithPartsCategory2";
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualPartsOutboundPlan);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider
        {
            get
            {
                return new TabControlRowDetailsTemplateProvider
                {
                    DetailPanelNames = new[] {
                        "PartsOutboundPlanDetailForWarehouse"
                    }
                };
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                    case "sourceCode":
                        return filters.Filters.Single(item => item.MemberName == "SourceCode").Value;
                    case "status":
                        string status = string.Empty;
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "Status"))
                                    status = string.Join(",", compositeFilterItem.Filters.Where(r => r.MemberName == "Status").Select(r => r.Value));
                            }
                        }
                        return status;
                        //return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "partsSalesOrderTypeName":
                        return filters.Filters.Single(item => item.MemberName == "PartsSalesOrderTypeName").Value;
                    case "counterpartCompanyName":
                        return filters.Filters.Single(item => item.MemberName == "CounterpartCompanyName").Value;
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "outboundType":
                        return filters.Filters.Single(item => item.MemberName == "OutboundType").Value;
                    case "bCreateTime":
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "CreateTime").First(r => r.MemberName == "CreateTime").Value;
                                }
                            }
                        }
                        return null;
                    case "eCreateTime":
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "CreateTime").Last(r => r.MemberName == "CreateTime").Value;
                                }
                            }
                        }
                        return null;
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            //this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 100;
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                var param = new[] { "SparePartCode", "SparePartName" };
                foreach (var filterItem in compositeFilterItem.Filters.Where(filter => !param.Contains(filter.MemberName)))
                {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}