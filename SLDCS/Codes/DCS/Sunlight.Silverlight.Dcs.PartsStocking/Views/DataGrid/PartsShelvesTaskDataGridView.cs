﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShelvesTaskDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = new[] {
            "ShelvesTaskStatus","Parts_InboundType"
        };
        public PartsShelvesTaskDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }


        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }


        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件上架任务";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new ColumnItem {
                        Title = PartsStockingUIStrings.PackingTask_PartsInboundPlanCode,
                        Name = "PartsInboundCheckBillCode"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Tilte_PartsOutboundBill_WarehouseName,
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartCode,
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartName,
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Title_PlanAmount, 
                        Name = "PlannedAmount"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ShelvesAmount,
                        Name = "ShelvesAmount"
                    }
                    , new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_WarehouseArea,
                        Name = "WarehouseArea"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_WarehouseAreaCode,
                        Name = "WarehouseAreaCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_PartsShelvesFinishTime,
                        Name = "ShelvesFinishTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_OverstockPartsAdjustBill_Remark,
                        Name = "Remark"
                    },
                     new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_InboundType,
                        Name = "InboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName,
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_CreateTime,
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName,
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime,
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },
                     new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Status,
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Title_PackingTaskCode,
                        Name = "PackingTaskCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsInboundReceipt_PartsInboundPlanCode,
                        Name = "PartsInboundPlanCode"
                    },new ColumnItem {
                       Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_SourceCode,
                       Name = "SourceBillCode"
                    }
                    , new ColumnItem {
                        Title = "上架标签码",
                        Name = "SIHCodes"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_WarehouseCode,
                        Name = "WarehouseCode"
                    },  new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetail_CurrentBatchNumber,
                        Name = "BatchNumber"
                    },
                    new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_HasDifference,
                        Name = "HasDifference"
                    },
                };
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "InboundType" && filter.MemberName != "NoWarehouseArea" && filter.MemberName != "HasDifference"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "code":
                        //return filters.Filters.Single(item => item.MemberName == "Code").Value;
                        return null;
                    case "sourceBillCode":
                        return filters.Filters.Single(item => item.MemberName == "SourceBillCode").Value;
                    case "partsInboundPlanCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsInboundPlanCode").Value;
                    case "partsInboundCheckBillCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsInboundCheckBillCode").Value;
                    case "inboundType":
                        return filters.Filters.Single(item => item.MemberName == "InboundType").Value;
                    case "status":
                        string status = string.Empty;
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "Status"))
                                    status = string.Join(",", compositeFilterItem.Filters.Where(r => r.MemberName == "Status").Select(r => r.Value));
                            }
                        }
                        return status;
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                    case "bCreateTime":
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "CreateTime").First(r => r.MemberName == "CreateTime").Value;
                                }
                            }
                        }
                        return null;
                    case "eCreateTime":
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "CreateTime").Last(r => r.MemberName == "CreateTime").Value;
                                }
                            }
                        }
                        return null;
                    case "bShelvesFinishTime":
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "ShelvesFinishTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "ShelvesFinishTime").First(r => r.MemberName == "ShelvesFinishTime").Value;
                                }
                            }
                        }
                        return null;
                    case "eShelvesFinishTime":
                        foreach(var filter in filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var compositeFilterItem = filter as CompositeFilterItem;
                            {
                                if(compositeFilterItem.Filters.Any(r => r.MemberName == "ShelvesFinishTime")) {
                                    return compositeFilterItem.Filters.Where(r => r.MemberName == "ShelvesFinishTime").Last(r => r.MemberName == "ShelvesFinishTime").Value;
                                }
                            }
                        }
                        return null;
                    case "noWarehouseArea":
                        return filters.Filters.Single(item => item.MemberName == "NoWarehouseArea").Value;
                    case "warehouseAreaCode":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseAreaCode").Value;
                    case "warehouseArea":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseArea").Value;
                    case "hasDifference":
                        return filters.Filters.Single(item => item.MemberName == "HasDifference").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsShelvesTask);
            }
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsShelvesTaskFormance"
                    }
                };
            }
        }
    }
}