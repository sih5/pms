﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShelvesTaskForDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "TraceProperty"
        };
        private QueryWindowBase partsSupplierDropDownQueryWindow;
        void partsSupplierDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as AccurateTraceQueryWindow;
            var window = sender as QueryWindowBase;
            var queryid = window.DataContext as VirtualPartsShelvesTaskDetail;
            if(queryWindow == null)
                return;

            var filterItem = new FilterItem();
            filterItem.MemberName = "PartId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            filterItem.Value = queryid.SparePartId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);

        }

        private QueryWindowBase PartsSupplierDropDownQueryWindow {
            get {
                if(this.partsSupplierDropDownQueryWindow == null) {
                    this.partsSupplierDropDownQueryWindow = DI.GetQueryWindow("AccurateTrace");
                    this.partsSupplierDropDownQueryWindow.Loaded += partsSupplierDropDownQueryWindow_Loaded;
                    this.partsSupplierDropDownQueryWindow.SelectionDecided += this.PartsSupplierDropDownQueryWindow_SelectionDecided;
                }
                return this.partsSupplierDropDownQueryWindow;
            }
        }
        private void PartsSupplierDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var traceWarehouseAge = queryWindow.SelectedEntities.Cast<TraceWarehouseAge>().FirstOrDefault();
            if(traceWarehouseAge == null)
                return;

            var partsPurchasePricingDetail = queryWindow.DataContext as VirtualPartsShelvesTaskDetail;
            if(partsPurchasePricingDetail == null)
                return;
            if(partsPurchasePricingDetail.TraceProperty.HasValue && partsPurchasePricingDetail.TraceProperty == (int)DCSTraceProperty.精确追溯) {
                if(!string.IsNullOrEmpty(partsPurchasePricingDetail.CurrSIHCodes) && !partsPurchasePricingDetail.CurrSIHCodes.Contains(traceWarehouseAge.SIHLabelCode)) {
                    partsPurchasePricingDetail.CurrSIHCodes += ";" + traceWarehouseAge.SIHLabelCode;
                } else
                    partsPurchasePricingDetail.CurrSIHCodes = traceWarehouseAge.SIHLabelCode;
            } else partsPurchasePricingDetail.CurrSIHCodes = traceWarehouseAge.SIHLabelCode;
        }

        public PartsShelvesTaskForDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        private Dictionary<int, List<WarehouseArea>> dicWarehouseAreas;

        public Dictionary<int, List<WarehouseArea>> DicWarehouseAreas {
            get {
                return this.dicWarehouseAreas ?? (this.dicWarehouseAreas = new Dictionary<int, List<WarehouseArea>>());
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;

            this.GridView.Columns["WarehouseAreaCode"].CellEditTemplate =
                                                      (DataTemplate)XamlReader.
                                                      Load("<DataTemplate xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' xmlns:telerik='http://schemas.telerik.com/2008/xaml/presentation'><telerik:RadComboBox IsEditable='False' DisplayMemberPath='Code' SelectedValuePath='Id' SelectedValue='{Binding WarehouseAreaId}' Text='{Binding WarehouseAreaCode}' /></DataTemplate>");

            this.GridView.PreparingCellForEdit += GridView_PreparingCellForEdit;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            ((GridViewDataColumn)this.GridView.Columns["ShelvesAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["PlannedAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["NowShelvesAmount"]).DataFormatString = "d";

        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var detail = e.Row.DataContext as VirtualPartsShelvesTaskDetail;
            if(detail == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "BatchNumber":
                    if(detail.BatchNumber != null && detail.BatchNumber != "")
                        e.Cancel = true;
                    break;
            }
        }
        private void GridView_PreparingCellForEdit(object sender, GridViewPreparingCellForEditEventArgs e) {
            if(e.Column.UniqueName.Equals("WarehouseAreaCode")) {
                var detail = e.Row.DataContext as VirtualPartsShelvesTaskDetail;
                if(detail == null)
                    return;
                var comboBox = e.EditingElement as RadComboBox;
                if(comboBox == null)
                    return;
                if(this.DicWarehouseAreas.Any(v => v.Key == detail.SparePartId))
                    comboBox.ItemsSource = this.DicWarehouseAreas.First(v => v.Key == detail.SparePartId).Value;
                else
                    comboBox.ItemsSource = Enumerable.Empty<WarehouseArea>();
            }

        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {

            if(e.Cell.Column.UniqueName.Equals("NowShelvesAmount")) {
                var detail1 = e.Cell.DataContext as VirtualPartsShelvesTaskDetail;
                if(detail1 == null)
                    return;
                var quantityError = detail1.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("NowShelvesAmount"));
                if(quantityError != null)
                    detail1.ValidationErrors.Remove(quantityError);
            }

            if(!e.Cell.Column.UniqueName.Equals("WarehouseAreaCode"))
                return;
            var partsShelvesTaskDetail = e.Row.DataContext as VirtualPartsShelvesTaskDetail;
            if(partsShelvesTaskDetail == null)
                return;
            var comboBox = e.EditingElement as RadComboBox;
            if(comboBox == null)
                return;
            var selectedWarehouseArea = comboBox.SelectedItem as WarehouseArea;
            if(selectedWarehouseArea == null)
                return;
            partsShelvesTaskDetail.WarehouseAreaId = selectedWarehouseArea.Id;
            partsShelvesTaskDetail.WarehouseAreaCode = selectedWarehouseArea.Code;
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(!e.Cell.Column.UniqueName.Equals("NowShelvesAmount"))
                return;
            var detail = e.Cell.DataContext as VirtualPartsShelvesTaskDetail;
            if(detail == null)
                return;
            var quantityError = detail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("NowShelvesAmount"));
            if(quantityError != null)
                detail.ValidationErrors.Remove(quantityError);
            if(detail.NowShelvesAmount >= 0) {
                if(detail.NowShelvesAmount > detail.PlannedAmount - detail.ShelvesAmount)
                    detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataGridView_Notification_NowShelvesQuantityError, new[] {
                        "NowShelvesAmount"
                    }));
            } else
                detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataGridView_Notification_NowShelvesQuantityError2, new[] {
                    "NowShelvesAmount"
                }));

        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsStockingUIStrings.PackingTask_PartsInboundPlanCode,
                        Name = "PartsInboundCheckBillCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_Column_PartsShelvesTaskCode,
                        Name = "Code",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Title_PackingTaskCode,
                        Name = "PackingTaskCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode,
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName,
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SparePart_MeasureUnit,
                        Name = "MeasureUnit",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetail_CurrentBatchNumber,
                        Name = "BatchNumber"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_WarehouseAreaCode,
                        Name = "WarehouseAreaCode"
                    },new KeyValuesColumnItem {
                        Title = "追溯属性",
                        Name = "TraceProperty",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Title_PlanAmount,
                        Name = "PlannedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ShelvesAmount,
                        Name = "ShelvesAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                       Title = PartsStockingUIStrings.DataGridView_Column_NowShelvesQuantity,
                       Name = "NowShelvesAmount",
                       MaskType = MaskType.Numeric,
                       TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = "已上架标签码",
                        Name = "SIHCodes",
                        IsReadOnly = true
                    }, new DropDownTextBoxColumnItem {
                       Title = "本次上架标签码",
                       Name = "CurrSIHCodes",
                       DropDownContent = this.PartsSupplierDropDownQueryWindow,
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark,
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VirtualPartsShelvesTaskDetails");
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsShelvesTaskDetail);
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }

        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}