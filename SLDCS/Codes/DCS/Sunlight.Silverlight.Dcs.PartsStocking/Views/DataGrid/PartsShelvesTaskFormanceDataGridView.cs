﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShelvesTaskFormanceDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "PartsInboundPer_EquipmentType" 
        };
        private void PackingTaskDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var shelvesTask = e.NewValue as VirtualPartsShelvesTask;
            if(shelvesTask == null || shelvesTask.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem
            {
                MemberName = "Code",
                MemberType = typeof(string),
                Operator = FilterOperator.IsEqualTo,
                Value = shelvesTask.Code
            });            
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
        public PartsShelvesTaskFormanceDataGridView()
        {
            this.DataContextChanged += this.PackingTaskDetailDataGridView_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                        Name = "ParentCode",
                        Title="库区"
                    },new ColumnItem {
                        Name = "WarehouseAreaCode",
                        Title="库位"
                    }, new ColumnItem {
                        Name = "Qty",
                        Title="上架数量",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "OperaterName",
                        Title="上架人"
                    }, new ColumnItem {
                        Name = "OperaterEndTime",
                        Title="上架时间"
                    }, new KeyValuesColumnItem {
                        Name = "Equipment",
                        Title="上架设备",
                         KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }


        protected override Type EntityType
        {
            get
            {
                return typeof(PartsInboundPerformanceQuery);
            }
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["Qty"]).DataFormatString = "d";

        }
        protected override string OnRequestQueryName()
        {
            return "查询上架任务单上架详情";
        }
    }
}
