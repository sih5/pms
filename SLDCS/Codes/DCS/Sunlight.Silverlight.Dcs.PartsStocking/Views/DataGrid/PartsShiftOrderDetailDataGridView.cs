﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShiftOrderDetailDataGridView : DcsDataGridViewBase {
        public PartsShiftOrderDetailDataGridView() {
            this.DataContextChanged += this.PartsShiftOrderDetailDataGridView_DataContextChanged;
        }

        private void PartsShiftOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsShiftOrder = e.NewValue as PartsShiftOrder;
            if(partsShiftOrder == null || partsShiftOrder.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsShiftOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsShiftOrder.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(PartsShiftOrderDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_OriginalWarehouseAreaCode,
                        Name = "OriginalWarehouseAreaCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DestWarehouseAreaCode,
                        Name = "DestWarehouseAreaCode"
                    }, new ColumnItem {
                        Name = "Quantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Title="已下架数量",
                        Name = "DownShelfQty"
                    }, new ColumnItem {
                        Title="已上架数量",
                        Name = "UpShelfQty"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsShiftOrderDetails";
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsShiftOrderDetailDown","PartsShiftOrderDetailUp"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["Quantity"]).DataFormatString = "d";
        }
    }
}
