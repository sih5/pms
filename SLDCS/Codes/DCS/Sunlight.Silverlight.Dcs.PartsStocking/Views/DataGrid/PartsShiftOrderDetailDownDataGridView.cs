﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShiftOrderDetailDownDataGridView : DcsDataGridViewBase {
        public PartsShiftOrderDetailDownDataGridView() {
            this.DataContextChanged += this.PartsShiftOrderDetailDataGridView_DataContextChanged;
        }

        private void PartsShiftOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsShiftOrderDetail = e.NewValue as PartsShiftOrderDetail;
            if(partsShiftOrderDetail == null || partsShiftOrderDetail.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsShiftOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsShiftOrderDetail.PartsShiftOrderId
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsShiftOrderDetailId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsShiftOrderDetail.Id
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Type",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DCSPartsShiftSIHDetailType.下架
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(PartsShiftSIHDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title="已下架标签码",
                        Name = "SIHCode"
                    }, new ColumnItem {
                        Title="已下架数量",
                        Name = "Quantity"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsShiftSIHDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["Quantity"]).DataFormatString = "d";
        }
    }
}
