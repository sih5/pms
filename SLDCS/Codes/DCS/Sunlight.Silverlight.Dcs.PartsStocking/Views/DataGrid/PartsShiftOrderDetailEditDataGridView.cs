﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShiftOrderDetailEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase warehouseAreaQueryWindow;
        private Dictionary<int, List<WarehouseArea>> dicWarehouseAreas;

        public Dictionary<int, List<WarehouseArea>> DicWarehouseAreas {
            get {
                return this.dicWarehouseAreas ?? (this.dicWarehouseAreas = new Dictionary<int, List<WarehouseArea>>());
            }
        }

        public QueryWindowBase WarehouseAreaQueryWindow {
            get {
                if(this.warehouseAreaQueryWindow == null) {
                    this.warehouseAreaQueryWindow = DI.GetQueryWindow("WarehouseAreaSparePartDropDown");
                    this.warehouseAreaQueryWindow.Loaded += warehouseAreaQueryWindow_Loaded;
                    this.warehouseAreaQueryWindow.SelectionDecided += warehouseAreaQueryWindow_SelectionDecided;
                }
                return this.warehouseAreaQueryWindow;
            }
        }

        private void warehouseAreaQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null)
                return;
            var partsShiftOrder = this.DataContext as PartsShiftOrder;
            if(partsShiftOrder == null) {
                return;
            }
            var partsShiftOrderDetail = queryWindow.DataContext as PartsShiftOrderDetail;
            if(partsShiftOrderDetail == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common","Warehouse.Name",false
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Warehouse.Name", partsShiftOrder.WarehouseName
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common","AreaKind",false
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "AreaKind", (int)DcsAreaKind.库位
            });

            //传入库区用途 禁用库区用途选项
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "WarehouseAreaCategory.Category", partsShiftOrderDetail.DestWarehouseAreaCategory });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common","WarehouseAreaCategory.Category",false
            });
            //传默认仓库ID查询条件
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new object[] {
                new FilterItem("WarehouseId", typeof(int), FilterOperator.IsEqualTo, partsShiftOrder.WarehouseId)
            });
        }

        private void warehouseAreaQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var warehouseArea = queryWindow.SelectedEntities.Cast<VirtualWarehouseAreaToQuery>().FirstOrDefault();
            if(warehouseArea == null)
                return;
            var partsShiftOrderDetail = queryWindow.DataContext as PartsShiftOrderDetail;
            if(partsShiftOrderDetail == null)
                return;
            partsShiftOrderDetail.DestWarehouseAreaId = warehouseArea.Id;
            partsShiftOrderDetail.DestWarehouseAreaCode = warehouseArea.Code;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var detail = e.Cell.DataContext as PartsShiftOrderDetail;
            if(detail == null)
                return;
            switch(e.Cell.Column.UniqueName) {
                case "DestWarehouseAreaCode":
                    foreach(var partsShiftOrderDetails in this.GridView.SelectedItems.Cast<PartsShiftOrderDetail>()) {
                        partsShiftOrderDetails.DestWarehouseAreaId = detail.DestWarehouseAreaId;
                        partsShiftOrderDetails.DestWarehouseAreaCode = detail.DestWarehouseAreaCode;
                    }
                    break;
                case "Quantity":
                    var quantityError = detail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("Quantity"));
                    if(detail.Quantity <= detail.SourceStockAmount) {
                        if(quantityError != null)
                            detail.ValidationErrors.Remove(detail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("Quantity")));
                    } else {
                        if(quantityError == null)
                            detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataGridView_Validation_PartsShiftOrderDetail_QuantityIsMoreThanSourceStockAmount, new[] {
                                "Quantity"
                            }));
                    }
                    break;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "OriginalWarehouseAreaCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_OriginalWarehouseAreaCode,
                        IsReadOnly = true
                    },new DropDownTextBoxColumnItem {
                        Name = "DestWarehouseAreaCode",
                        IsEditable = false,
                        DropDownContent = this.WarehouseAreaQueryWindow,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DestWarehouseAreaCode
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_ShiftQuantity,
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsShiftOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsShiftOrderDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;//一条一条删除 请勿注视
            ((GridViewDataColumn)this.GridView.Columns["Quantity"]).DataFormatString = "d";
        }
    }
}