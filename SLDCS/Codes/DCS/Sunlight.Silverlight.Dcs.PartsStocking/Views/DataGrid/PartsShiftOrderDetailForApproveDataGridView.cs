﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShiftOrderDetailForApproveDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Category"
        };
        public PartsShiftOrderDetailForApproveDataGridView(){
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartName
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_OriginalWarehouseAreaCode,
                        Name = "OriginalWarehouseAreaCode"
                    },
                     new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_OriginalWarehouseAreaCategory,
                        Name = "OriginalWarehouseAreaCategory",
                        KeyValueItems =  this.KeyValueManager[this.kvNames[0]]
                    }
                    , new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DestWarehouseAreaCode,
                        Name = "DestWarehouseAreaCode"
                    },
                    new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_DestWarehouseAreaCategory,
                        Name = "DestWarehouseAreaCategory",
                        KeyValueItems =  this.KeyValueManager[this.kvNames[0]]
                    }
                    , new ColumnItem {
                        Name = "Quantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_QuantityNumber
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    }
                };
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(PartsShiftOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsShiftOrderDetails");
        }

        
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DataPager.PageSize = 100;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }
    }
}
