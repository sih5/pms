﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShiftOrderDetailForDownDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Category","TraceProperty"
        };
        public PartsShiftOrderDetailForDownDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartCode,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_CompanyPartsStock_SparePartName,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_OriginalWarehouseAreaCode,
                        Name = "OriginalWarehouseAreaCode",
                        IsReadOnly=true
                    },
                     new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_OriginalWarehouseAreaCategory,
                        Name = "OriginalWarehouseAreaCategory",
                        KeyValueItems =  this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly=true
                    }
                    , new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DestWarehouseAreaCode,
                        Name = "DestWarehouseAreaCode",
                        IsReadOnly=true
                    },new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_DestWarehouseAreaCategory,
                        Name = "DestWarehouseAreaCategory",
                        KeyValueItems =  this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Quantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_QuantityNumber,
                        IsReadOnly=true
                    },new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_TraceProperty,
                        Name = "TraceProperty",
                        KeyValueItems =  this.KeyValueManager[this.kvNames[1]],                      
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "DownShelfQty",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DownShelfQty,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "CourrentDownShelfQty",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_CourrentDownShelfQty, 
                    }, new ColumnItem {
                        Name = "CourrentDownSIHCode",                      
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_CourrentDownSIHCode, 
                        IsReadOnly=false
                    }, new ColumnItem {
                        Name = "UpShelfQty",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_UpShelfQty,
                        IsReadOnly=true
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark,
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(PartsShiftOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsShiftOrderDetails");
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsShiftOrderDetailDown","PartsShiftOrderDetailUp"
                    }
                };
            }
        }
        
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DataPager.PageSize = 100;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CellValidating += GridView_CellValidating;
        }
        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            switch(e.Cell.DataColumn.UniqueName) {
                case "CourrentDownShelfQty":
                    if(!(e.NewValue is int) || (int)e.NewValue < 0) {
                        e.IsValid = false;
                        e.ErrorMessage = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation7;
                    } else {
                        var detail = e.Cell.DataContext as PartsShiftOrderDetail;
                        if(detail == null)
                            return;
                        if(((int)e.NewValue + detail.DownShelfQty??0) > detail.Quantity) {
                            e.IsValid = false;
                            e.ErrorMessage = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Validatation8;
                        }
                    }
                    break;
            }
        }
    }
}
