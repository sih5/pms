﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShiftOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private Dictionary<int, List<WarehouseArea>> dicWarehouseAreas;

        public Dictionary<int, List<WarehouseArea>> DicWarehouseAreas {
            get {
                return this.dicWarehouseAreas ?? (this.dicWarehouseAreas = new Dictionary<int, List<WarehouseArea>>());
            }
        }

        private void GridView_PreparingCellForEdit(object sender, GridViewPreparingCellForEditEventArgs e) {
            if(!e.Column.UniqueName.Equals("DestWarehouseAreaCode"))
                return;
            var detail = e.Row.DataContext as PartsShiftOrderDetail;
            if(detail == null)
                return;
            var comboBox = e.EditingElement as RadComboBox;
            if(comboBox == null)
                return;
            if(this.DicWarehouseAreas.Any(v => v.Key == detail.SparePartId))
                comboBox.ItemsSource = this.DicWarehouseAreas.First(v => v.Key == detail.SparePartId).Value;
            else
                comboBox.ItemsSource = Enumerable.Empty<WarehouseArea>();
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            if(!e.Cell.Column.UniqueName.Equals("DestWarehouseAreaCode"))
                return;
            var detailOrder = e.Row.DataContext as PartsShiftOrderDetail;
            if(detailOrder == null)
                return;
            var comboBox = e.EditingElement as RadComboBox;
            if(comboBox == null)
                return;
            var selectedWarehouseArea = comboBox.SelectedItem as WarehouseArea;
            if(selectedWarehouseArea == null)
                return;
            detailOrder.DestWarehouseAreaId = selectedWarehouseArea.Id;
            detailOrder.DestWarehouseAreaCode = selectedWarehouseArea.Code;
            //var details = this.GridView.SelectedItems.Cast<PartsShiftOrderDetail>().ToArray();
            //if(details == null || !details.Any())
            //    return;
            //foreach(var detail in from detail in details
            //                      let warehouseAreas = this.DicWarehouseAreas[detail.SparePartId]
            //                      where warehouseAreas.Contains(selectedWarehouseArea)
            //                      select detail) {
            //    detail.DestWarehouseAreaId = selectedWarehouseArea.Id;
            //    detail.DestWarehouseAreaCode = selectedWarehouseArea.Code;
            //}
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(!e.Cell.Column.UniqueName.Equals("Quantity"))
                return;
            var detail = e.Cell.DataContext as PartsShiftOrderDetail;
            if(detail == null)
                return;
            var quantityError = detail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("Quantity"));
            if(quantityError != null)
                detail.ValidationErrors.Remove(quantityError);
            if(detail.Quantity > 0) {
                if(detail.Quantity > detail.ShelvesAmount)
                    detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataGridView_Validation_PartsShiftOrderDetail_QuantityIsMoreThanShelvesAmount, new[] {
                        "Quantity"
                    }));
            } else
                detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataGridView_Validation_PartsShiftOrderDetail_ShelvesQuantityIsLessThanOne, new[] {
                    "Quantity"
                }));
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }//,new ColumnItem {
                    //    Name = "OriginalWarehouseAreaCode",
                    //    Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_OriginalWarehouseAreaCode,
                    //    IsReadOnly = true
                    //}
                    ,new ColumnItem {
                        Name = "MeasureUnit",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_SparePart_MeasureUnit,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "DestWarehouseAreaCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_DestWarehouseAreaCode
                    },new ColumnItem {
                        Name = "ShelvesAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_ShelvesAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrderDetail_Quantity,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsShiftOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsShiftOrderDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.Columns["DestWarehouseAreaCode"].CellEditTemplate =
                                                      (DataTemplate)XamlReader.
                                                      Load("<DataTemplate xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' xmlns:telerik='http://schemas.telerik.com/2008/xaml/presentation'><telerik:RadComboBox IsEditable='False' DisplayMemberPath='Code' SelectedValuePath='Id' SelectedValue='{Binding DestWarehouseAreaId}' Text='{Binding DestWarehouseAreaCode}' /></DataTemplate>");
            this.GridView.PreparingCellForEdit += GridView_PreparingCellForEdit;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            ((GridViewDataColumn)this.GridView.Columns["ShelvesAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["Quantity"]).DataFormatString = "d";
        }
    }
}
