﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShiftOrderWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "PartsShiftOrder_Type","PartsShiftOrder_Status","PartsShiftOrder_QuestionType","PartsShiftOrderShiftStatus"
        };

        public PartsShiftOrderWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrder_Code,
                        Name = "Code"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_IsUploadFile,
                        Name = "IsUplodFile"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShiftOrder_WarehouseName,
                        Name = "WarehouseName"
                    },new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new KeyValuesColumnItem {
                        Name = "QuestionType",
                        Title = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrderQuestionType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new KeyValuesColumnItem {
                        Name = "ShiftStatus",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_PartsShiftOrder_ShiftStatus,
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    }
                    ,new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ModifierName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    },new ColumnItem {
                        Name = "ModifyTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "InitialApproverName",
                        Title = PartsStockingUIStrings.DataGridView_Column_InitApprover
                    },new ColumnItem {
                        Name = "InitialApproveTime",
                        Title = PartsStockingUIStrings.DataGridView_Column_InitApproveTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ApproverName",
                        Title = PartsStockingUIStrings.DataGridView_Column_FinalApprover
                    },new ColumnItem {
                        Name = "ApproveTime",
                        Title = PartsStockingUIStrings.DataGridView_Column_FinalApproveTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "SubmitterName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBill_SubmitterName
                    },new ColumnItem {
                        Name = "SubmitTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsDifferenceBackBill_SubmitTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "CloserName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Terminater
                    },new ColumnItem {
                        Name = "CloseTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TerminateTime,
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsShiftOrder);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsShiftOrderWithDetails";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsShiftOrderDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
