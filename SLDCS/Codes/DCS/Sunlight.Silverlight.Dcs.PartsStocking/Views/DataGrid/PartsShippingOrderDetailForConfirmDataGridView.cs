﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShippingOrderDetailForConfirmDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
                  "DifferenceClassification"
        };
        public PartsShippingOrderDetailForConfirmDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ShippingAmount",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "InTransitDamageLossAmount",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "DifferenceClassification",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]],
                        Title =  PartsStockingUIStrings.DataGridView_ColumnItem_DifferenceClassification,
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsShippingOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.DataPager.PageSize = 100;
            ((GridViewDataColumn)this.GridView.Columns["ShippingAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ConfirmedAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["InTransitDamageLossAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var partsShippingOrder = this.DataContext as PartsShippingOrder;
            if(partsShippingOrder == null)
                return;
            if(e.Cell.DataColumn.DataMemberBinding.Path.Path == "ConfirmedAmount") {
                if(partsShippingOrder.Type == (int)DcsPartsShippingOrderType.调拨)
                    e.Cancel = true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsShippingOrderDetails");
        }
    }
}