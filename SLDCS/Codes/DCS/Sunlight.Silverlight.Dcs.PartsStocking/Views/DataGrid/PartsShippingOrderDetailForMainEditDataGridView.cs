﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShippingOrderDetailForMainEditDataGridView : DcsDataGridViewBase {
        public PartsShippingOrderDetailForMainEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        private readonly string[] kvNames = {
            "PartsShippingOrder_TransportLossesDisposeMethod", "DifferenceClassification"
        };
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ShippingAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "InTransitDamageLossAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "DifferenceClassification",
                        KeyValueItems=this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_DifferenceClassification,
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "TransportLossesDisposeMethod",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]],
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrder_TransportLossesDisposeMethod,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsShippingOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.Height = 170;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["ShippingAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ConfirmedAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["InTransitDamageLossAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "c2";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsShippingOrderDetails");
        }
    }
}
