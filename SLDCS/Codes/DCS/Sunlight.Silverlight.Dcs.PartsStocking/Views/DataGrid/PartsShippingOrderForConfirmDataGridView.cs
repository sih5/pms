﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShippingOrderForConfirmDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsShippingOrder_Type", "PartsShippingOrder_TransportLossesDisposeStatus", "PartsShipping_Method", "PartsShippingOrder_Status"
        };

        private int[] PartsShippingOrder_Status = new[] {
        (int)DcsPartsShippingOrderStatus.新建,(int)DcsPartsShippingOrderStatus.已发货,(int)DcsPartsShippingOrderStatus.待提货,(int)DcsPartsShippingOrderStatus.待收货
        };

        public PartsShippingOrderForConfirmDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {           
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem() {
                        Name = "ClosedLoop",
                        Title = PartsStockingUIStrings.QueryPanel_Title_ClosedLoopStatus,
                    }, new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },  new ColumnItem {
                        Name = "ShippingCompanyCode"
                    }, new ColumnItem {
                        Name = "ShippingCompanyName"
                    }, new ColumnItem {
                        Name = "ReceivingCompanyCode"
                    }, new ColumnItem {
                        Name = "ReceivingCompanyName"
                    }, new ColumnItem {
                        Name = "ReceivingWarehouseName"
                    },  new ColumnItem{
                        Name="TotalAmount",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_TotalAmount
                    }, new ColumnItem {
                        Name = "ReceivingAddress"
                    }, new ColumnItem {
                        Name = "IsTransportLosses",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrder_IsTransportLosses
                    }, new KeyValuesColumnItem {
                        Name = "TransportLossesDisposeStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrder_TransportLossesDisposeStatus
                    }, new ColumnItem {
                        Name = "LogisticCompanyCode"
                    }, new ColumnItem {
                        Name = "LogisticCompanyName"
                    }
                    //, new ColumnItem {
                    //    Name = "ExpressCompanyCode",
                    //     Title = "快递公司编号"
                    //}, new ColumnItem {
                    //    Name = "ExpressCompany",
                    //     Title = "快递公司名称"   
                    //}
                    , new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }
                    //, new ColumnItem {
                    //    Name = "InterfaceRecordId"
                    //}
                    , new ColumnItem {
                        Name = "RequestedArrivalDate",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ShippingDate",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "TransportDriverPhone"
                    }, new ColumnItem {
                        Name = "DeliveryBillNumber"
                    }, new ColumnItem{
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_OutboundWarehouseName
                    }, new ColumnItem{
                        Name = "TransportMileage"
                    }, new ColumnItem{
                        Name = "TransportVehiclePlate",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_TransportVehiclePlate
                    }, new ColumnItem{
                        Name = "TransportDriver"
                    }
                    //,new ColumnItem{
                    //    Name="GPSCode",
                    //    Title="GPS定位器设备号"
                    //}
                    ,new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem{
                        Name="OrderApproveComment",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_OrderApproveComment
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name = "ModifierName"
                    }, new ColumnItem{
                        Name = "ModifyTime"
                    }, new ColumnItem{
                        Name = "ConsigneeName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ConsigneeName
                    }, new ColumnItem{
                        Name = "ConfirmedReceptionTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ConsigneeTime
                    }, new ColumnItem{
                        Name = "ReceiptConfirmorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ReceiptConfirmorName
                    }, new ColumnItem{
                        Name = "ReceiptConfirmTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ReceiptConfirmTime
                    } ,new ColumnItem {
                      Name  = "PartsSalesCategory.Name",
                      Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_OverstockPartsStock_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.RowStyleSelector = this.GridViewStyleSelector();
            foreach(var column in this.GridView.Columns)
                if("Status".Equals(column.UniqueName))
                    column.CellStyleSelector = new BaleReportCreateDataGridViewCellStyleSelector(column.UniqueName);
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        private GridViewStyleSelector GridViewStyleSelector() {
            return new GridViewStyleSelector {
                ExecuteFunc = (item, container) => {
                    if(item is PartsShippingOrder) {
                        var partsshippingorder = item as PartsShippingOrder;
                        if(PartsShippingOrder_Status.Contains(partsshippingorder.Status) && partsshippingorder.RequestedArrivalDate < DateTime.Now)
                            return this.Resources["lightRedDataBackground"] as Style;
                    }
                    return null;
                }
            };
        }

        protected override Type EntityType {
            get {
                return typeof(PartsShippingOrder);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "PartsShippingOrder", "PartsShippingOrderDetail", "PartsShippingOrderRef"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件发运单";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "closedLoopStatus":
                        return filters.Filters.FirstOrDefault(item => item.MemberName == "ClosedLoopStatus") == null ? null : filters.Filters.First(item => item.MemberName == "ClosedLoopStatus").Value;
                    case "ERPSourceOrderCodeQuery":
                        return null;
                    case "PartsOutboundPlanCode":
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var newCompositeFilterItem = new CompositeFilterItem();
                foreach(var filter in compositeFilterItem.Filters.Where(filter => filter.MemberName != "ClosedLoopStatus"))
                    newCompositeFilterItem.Filters.Add(filter);
                return newCompositeFilterItem.ToFilterDescriptor();
            }
            return base.OnRequestFilterDescriptor(queryName);
        }

        public class BaleReportCreateDataGridViewCellStyleSelector : StyleSelector {
            private readonly string gridViewColumnName;
            private Style orangeGridViewCellStyle;
            private Style greenGridViewCellStyle;

            private Style OrangeGridViewCellStyle {
                get {
                    if(this.orangeGridViewCellStyle == null) {
                        this.orangeGridViewCellStyle = new Style {
                            TargetType = typeof(GridViewCell)
                        };
                        this.orangeGridViewCellStyle.Setters.Add(new Setter(GridViewCell.ForegroundProperty, new SolidColorBrush(Colors.Orange)));
                    }
                    return this.orangeGridViewCellStyle;
                }
            }

            private Style GreenGridViewCellStyle {
                get {
                    if(this.greenGridViewCellStyle == null) {
                        this.greenGridViewCellStyle = new Style {
                            TargetType = typeof(GridViewCell)
                        };
                        this.greenGridViewCellStyle.Setters.Add(new Setter(GridViewCell.ForegroundProperty, new SolidColorBrush(Color.FromArgb(0xAA, 0x98, 0xCD, 0x98))));
                    }
                    return this.greenGridViewCellStyle;
                }
            }

            public BaleReportCreateDataGridViewCellStyleSelector(string gridViewColumnName) {
                this.gridViewColumnName = gridViewColumnName;
            }

            public override Style SelectStyle(object item, DependencyObject container) {
                var partsshippingorder = item as PartsShippingOrder;
                if(partsshippingorder == null)
                    return null;
                return partsshippingorder.Status == (int)DcsPartsShippingOrderStatus.待提货 || partsshippingorder.Status == (int)DcsPartsShippingOrderStatus.待收货 ?
                    OrangeGridViewCellStyle : (partsshippingorder.Status == (int)DcsPartsShippingOrderStatus.已发货 ? GreenGridViewCellStyle : null);
            }
        }
    }
}
