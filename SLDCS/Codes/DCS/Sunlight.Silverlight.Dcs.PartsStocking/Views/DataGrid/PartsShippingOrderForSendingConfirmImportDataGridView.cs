﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShippingOrderForSendingConfirmImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingCode,
                        Name = "Code"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_ShippingTime,
                        Name = "ShippingTime"
                    }, new ColumnItem {
                        Title=  PartsStockingUIStrings.DataEditView_Text_PartsShippingOrder_Logistic,
                        Name = "Logistic"
                    }, new ColumnItem {
                        Title=  PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrder_DeliveryBillNumber,
                        Name = "DeliveryBillNumber"
                    } , new ColumnItem{
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrder_ArrivalMode,
                        Name = "ArrivalMode"
                    }, new ColumnItem{
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrder_QueryURL,
                        Name = "QueryURL"
                    }, new ColumnItem{
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_ExpressName,
                        Name = "ExpressCompany"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsShippingOrder);
            }
        }
    }
}
