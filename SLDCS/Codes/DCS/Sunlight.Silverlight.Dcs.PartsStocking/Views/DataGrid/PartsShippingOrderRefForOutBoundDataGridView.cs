﻿using System;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsShippingOrderRefForOutBoundDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Parts_OutboundType","PartsShipping_Method"
        };

        public PartsShippingOrderRefForOutBoundDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "TaskCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_BoxUpTaskOrPicking_TaskCode
                    }, new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_WarehouseCode
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_WarehouseName
                    }, 
                    new KeyValuesColumnItem {
                        Name = "OutboundType",
                        Title=  PartsStockingUIStrings.QueryPanel_Title_OutboundType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },
                    new ColumnItem {
                        Name = "OrderTypeName",
                       Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_PartsSalesOrderTypeName,
                    },
                    new ColumnItem {
                        Name = "OutboundTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_CreateTime,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem{
                        Name = "TotalAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_OutTotalAmount
                    },
                    new KeyValuesColumnItem{
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingMethod
                    }, new ColumnItem{
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartCode
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName
                    }, new ColumnItem{
                        Name = "ShippingAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ShippingQuantity
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    },
                    //new ColumnItem{
                    //    Name="PartsOutboundBill.OrderApproveComment",
                    //    Title="订单审批意见"
                    //}
                };
            }
        }
        protected override Type EntityType {
            get {
                return typeof(BoxUpTaskOrPicking);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            //((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OutboundTime"]).DataFormatString = "d";
        }


        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("BoxUpTaskOrPickings");
        }
    }
}