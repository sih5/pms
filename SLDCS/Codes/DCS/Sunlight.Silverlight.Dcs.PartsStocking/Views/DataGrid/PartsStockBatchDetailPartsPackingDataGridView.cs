﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsStockBatchDetailPartsPackingDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    }, new ColumnItem {
                        Name = "PartsStock.Warehouse.Code",
                    }, new ColumnItem {
                        Name = "PartsStock.Warehouse.Name"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStockBatchDetail_PartsStockWarehouseAreaCode,
                        Name = "PartsStock.WarehouseArea.Code"
                    }, new ColumnItem {
                        Name = "PartsStock.SparePart.Code"
                    }, new ColumnItem {
                        Name = "PartsStock.SparePart.Name"
                    }, new ColumnItem {
                        Name = "BatchNumber"
                    }, new ColumnItem {
                        Name = "Quantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsStockBatchDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["Quantity"]).DataFormatString = "d";
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsStockBatchDetails");
        }
    }
}
