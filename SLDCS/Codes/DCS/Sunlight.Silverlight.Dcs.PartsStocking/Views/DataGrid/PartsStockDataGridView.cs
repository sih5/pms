﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsStockDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Category"
        };

        public PartsStockDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Warehouse.Name",
                        Title =PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStock_WarehouseName
                    }, new KeyValuesColumnItem {
                        Name = "WarehouseAreaCategory.Category",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title =PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStock_WarehouseAreaCategory
                    }, new ColumnItem {
                        Name = "WarehouseArea.Code",
                        Title =PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStock_WarehouseAreaCode
                    }, new ColumnItem {
                        Name = "SparePart.Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePart.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStock_SparePartName
                    }, new ColumnItem {
                        Name = "Quantity"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询仓库库位库存";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "storageCompanyId":
                        return BaseApp.Current.CurrentUserData.EnterpriseId;
                    case "warehouseId":
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
