﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsStockDetailQueryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
                "Area_Category","PartsSalesPrice_PriceType"
        };
   
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 100;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                       new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_WarehouseName
                    }, new KeyValuesColumnItem {
                        Name = "WarehouseAreaCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_WarehouseAreaCategory
                    }, new ColumnItem {
                        Name = "WarehouseRegionCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseRegionCode
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_WarehouseAreaCode
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartName
                    }, new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_Quantity
                    }, new ColumnItem {
                        Name = "LockedQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStockHistory_LockedQty
                    }
                    //, new ColumnItem {
                    //    Name = "OutingQty",
                    //    Title = "待出量"
                    //}
                    //, new KeyValuesColumnItem {
                    //    Name = "PriceType",
                    //    KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                    //    Title = "价格类型"
                    //}
                    , new ColumnItem {
                        Name = "SalesPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsSalesPrice
                    }, new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件库位库存1";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "warehouseAreaCategoryValue":
                        var filterCategory = filters.Filters.SingleOrDefault(item => item.MemberName == "WarehouseAreaCategory");
                        return filterCategory == null ? null : filterCategory.Value;
                    case "partsOutPlanId":
                    case "operatorId":
                    case "managerId":
                    case "warehouseRegionCode":
                        var warehouseRegionCode = filters.Filters.SingleOrDefault(item => item.MemberName == "WarehouseRegionCode");
                        return warehouseRegionCode == null ? null : warehouseRegionCode.Value;
                    case "pricetype":
                        //var filterPricetype = filters.Filters.SingleOrDefault(item => item.MemberName == "PriceType");
                        //return filterPricetype == null ? null : filterPricetype.Value;
                        //return (int)DcsPartsSalesPricePriceType.基准销售价;
                        return null;
                    case "greaterThanZero":
                        var greaterThanZero = filters.Filters.SingleOrDefault(item => item.MemberName == "GreaterThanZero");
                        return greaterThanZero == null ? null : greaterThanZero.Value;
                    case "sparePartCodes":
                        var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
                        if(codes == null || codes.Value == null)
                            return null;
                        return codes.Value.ToString();
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                var param = new[] { "WarehouseAreaCategory", "PriceType", "GreaterThanZero" };
                foreach(var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName)))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        public PartsStockDetailQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
         
        }
    }
}
