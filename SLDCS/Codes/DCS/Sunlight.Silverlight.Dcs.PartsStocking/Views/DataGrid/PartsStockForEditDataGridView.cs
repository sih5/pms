﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsStockForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase sparePartQueryWindow;
        private RadWindow radWindow;

        private QueryWindowBase SparePartQueryWindow {
            get {
                if(this.sparePartQueryWindow == null) {
                    this.sparePartQueryWindow = DI.GetQueryWindow("SparePart");
                    this.sparePartQueryWindow.SelectionDecided += this.SparePartQueryWindow_SelectionDecided;
                }
                return this.sparePartQueryWindow;
            }
        }

        private void SparePartQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            var dataContext = this.DataContext as PartsLocationAssignmentDataEditView;
            var domainContext = this.DomainContext as DcsDomainContext;
            domainContext.Load(domainContext.GetSparePartsQuery().Where(d => d.Id == sparePart.Id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                if(dataContext != null)
                    dataContext.PartsStocks.Add(new PartsStock() {
                        SparePart = entity
                    });
            }, null);


            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private readonly string[] kvNames = {
            "Area_Category"
        };

        public PartsStockForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        private void GridView_DataLoaded(object sender, EventArgs e) {
            if(this.GridView.Items != null && this.GridView.Items.Count == 1)
                this.GridView.SelectedItem = this.GridView.Items[0];
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            SelectedWindow.ShowDialog();
        }

        private RadWindow SelectedWindow {
            get {
                return this.radWindow ?? (this.radWindow = new RadWindow {
                    Header = PartsStockingUIStrings.QueryPanel_Title_SparePart,
                    Content = this.SparePartQueryWindow,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }


        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            if(e.Items.Count() > 0) {
                var partsStock = e.Items.First() as PartsStock;
                if(partsStock == null)
                    return;
                this.GridView.Items.Remove(partsStock);
                if(((DcsDomainContext)this.DomainContext).PartsStocks.Any(r => r == partsStock))
                    ((DcsDomainContext)this.DomainContext).PartsStocks.Remove(partsStock);
            }

        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePart.Code",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePart.Name",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "WarehouseArea.Code",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseArea_AreaCategoryId,
                        Name = "WarehouseArea.WarehouseAreaCategory.Category",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsStock);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.DataLoaded += this.GridView_DataLoaded;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.Deleting += GridView_Deleting;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsStocks");
        }
    }
}
