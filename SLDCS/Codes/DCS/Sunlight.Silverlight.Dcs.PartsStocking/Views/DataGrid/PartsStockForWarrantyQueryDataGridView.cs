﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsStockForWarrantyQueryDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
            "PartsSalesPrice_PriceType"
        };

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 100;
            ((GridViewDataColumn)this.GridView.Columns["PartsSalesPrice"]).DataFormatString = "c2";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseCode
                    },
                    new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_WarehouseName
                    },
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartCode
                    },
                    new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartName
                    },
                    new KeyValuesColumnItem {
                        Name = "PriceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PriceType
                    },
                    new ColumnItem {
                        Name = "PartsSalesPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsSalesPrice
                    },
                    new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_Quantity
                    },
                    new ColumnItem {
                        Name = "UsableQuantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_UsableAmount
                    },
                    new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsApp_BranchName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WarehousePartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "只查询仓库库存保外";
        }

        //根据仓储企业ID=登录企业ID过滤并执行查询
        protected override object OnRequestQueryParameter(string queryname, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "storageCompanyId":
                        return BaseApp.Current.CurrentUserData.EnterpriseId;
                    case "warehouseId":
                        return null;
                    case "partIds":
                        return null;
                }
            }
            return null;
        }

        public PartsStockForWarrantyQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
