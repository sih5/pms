﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsStockHistoryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Category"
        };

        public PartsStockHistoryDataGridView() { 
           this.KeyValueManager.Register(this.kvNames);
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            //this.DataPager.PageSize = 100;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }


        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }
        }

        protected override string OnRequestQueryName() {
            return "查询库存变更日志";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                      new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseCode,
                        Name = "WarehouseCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseName,
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_WarehouseAreaCode,
                        Name = "WarehouseAreaCode"
                    }, new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_WarehouseAreaCategory,
                        Name = "WarehouseAreaCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartCode,
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartName,
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStockHistory_OldQuantity,
                        Name = "OldQuantity"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_UsableQuantity,
                        Name = "Quantity"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStockHistory_ChangedQuantity,
                        Name = "ChangedQuantity"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStock_LockedQty,
                        Name = "LockedQty"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark,
                        Name = "Remark"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName,
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime,
                        Name = "CreateTime",
                        MaskType = Sunlight.Silverlight.Core.Model.MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName,
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime,
                        Name = "ModifyTime",
                        MaskType = Sunlight.Silverlight.Core.Model.MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if (compositeFilterItem == null)
            {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            }
            else
            {
                var param = new[] {"SparePartCode","SparePartName","WarehouseAreaCode","WarehouseAreaCategory"};
                foreach (var item in compositeFilterItem.Filters.Where(e => !param.Contains(e.MemberName)))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                    case "warehouseAreaCode":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseAreaCode").Value;
                    case "warehouseAreaCategory":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseAreaCategory").Value;
                    case "bCreateTime":
                        var createTime = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualPartsStockHistory);
            }
        }

    }
}