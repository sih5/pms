﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using System.Linq;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid
{
    public class PartsStockQueryDataGridView : DcsDataGridViewBase
    {

        private readonly string[] kvNames = { "PartsSalesPrice_PriceType", "Storage_Center" };

        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 100;
            ((GridViewDataColumn)this.GridView.Columns["RetailguidePrice"]).DataFormatString = "c2";
        }

        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[]{
                
                  new ColumnItem{
                     Name="WarehouseCode",
                     Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseCode
                  },new ColumnItem{
                      Name = "WarehouseName",
                      Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_WarehouseName
                 },new KeyValuesColumnItem {
                        Name = "StorageCenter",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_ExpressToLogistic_FocufingCoreName
                 },new ColumnItem{
                      Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartCode
                 }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartName
                 }, new ColumnItem {
                        Name = "ReferenceCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskDetail_ReferenceCode
                 }, new ColumnItem {
                        Name = "RetailguidePrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_RetailguidePrice
                 },new ColumnItem{
                        Name ="TotalQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_StockQuantity
                 },new ColumnItem {
                        Name ="DfQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_DfQty
                 },new ColumnItem {
                        Name ="ActualQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_ActualQty
                 },new ColumnItem {
                        Name ="InlockQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_InlockQty
                 },new ColumnItem {
                        Name ="ShelfQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_ShelfQty
                 },new ColumnItem {
                        Name ="ActualAvailableStock",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_ActualAvailableStock
                 },new ColumnItem {
                        Name ="StockQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_StockQty
                 },new ColumnItem {
                        Name ="OrderlockQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_OrderlockQty
                 },new ColumnItem {
                        Name ="CgQty",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_CgQty
                 },new ColumnItem{
                      Name ="BranchName",
                      Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_BranchName
                 },new ColumnItem{
                      Name ="BranchCode",
                      Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_BranchCode
                 }, new ColumnItem {
                      Name = "Categoryname",
                      Title =  PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_PartsSalesCategoryName
                 }               
                };
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(WarehousePartsStock);
            }
        }

        protected override string OnRequestQueryName()
        {
            return "配件仓库库存查询";
        }

        //根据仓储企业ID=登录企业ID过滤并执行查询
        protected override object OnRequestQueryParameter(string queryname, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                switch (parameterName)
                {
                    case "storageCompanyId":
                        return BaseApp.Current.CurrentUserData.EnterpriseId;
                    case "sparePartCodes":
                        var codes = filters.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
                        if(codes == null || codes.Value == null)
                            return null;
                        return ((IEnumerable<string>)codes.Value).ToArray();
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                    case "storageCenter":
                        return filters.Filters.Single(item => item.MemberName == "StorageCenter").Value;
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "referenceCode":
                        return filters.Filters.Single(item => item.MemberName == "ReferenceCode").Value;
                }
            }
            return null;
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public PartsStockQueryDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize()
        {
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp =>
            {
                if (loadOp.HasError)
                    return;
                foreach (var partsSalesCategory in loadOp.Entities)
                    this.kvPartsSalesCategorys.Add(new KeyValuePair
                    {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name
                    });
            }, null);
        }

    }
}
