﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsTransferBillDetailDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Key",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Custom_BillCode
                    }, new ColumnItem {
                        Name = "Value",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Custom_BillType
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(KeyValuePair<string, string>);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsTransferBillDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
