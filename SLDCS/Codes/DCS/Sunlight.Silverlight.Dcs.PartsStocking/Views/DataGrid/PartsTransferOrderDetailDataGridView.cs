﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsTransferOrderDetailDataGridView : DcsDataGridViewBase {
        private int partsTransferOrderId;
        public PartsTransferOrderDetailDataGridView() {
            this.DataContextChanged += this.PartsTransferOrderDetailDataGridView_DataContextChanged;
        }

        private void PartsTransferOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsTransferOrder = e.NewValue as PartsTransferOrder;
            if(partsTransferOrder == null || partsTransferOrder.Id == default(int))
                return;
            //var compositeFilterItem = new CompositeFilterItem();
            //compositeFilterItem.Filters.Add(new FilterItem {
            //    MemberName = "PartsTransferOrderId",
            //    MemberType = typeof(int),
            //    Operator = FilterOperator.IsEqualTo,
            //    Value = partsTransferOrder.Id
            //});
            //this.FilterItem = compositeFilterItem;
            partsTransferOrderId = partsTransferOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            switch (parameterName)
            {
                case "partsTransferOrderId":
                    return this.partsTransferOrderId;
                default:
                    return null;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }                  
                    , new ColumnItem{
                        Name = "MInPackingAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_MInPackingAmount
                    }
                    , new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsTransferOrderDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsTransferOrderDetailsWithPartsTransferOrder";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["PlannedAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ConfirmedAmount"]).DataFormatString = "d";
            //((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            //((GridViewDataColumn)this.GridView.Columns["PlannPrice"]).DataFormatString = "c2";
            //((GridViewDataColumn)this.GridView.Columns["DetailPlanAmount"]).DataFormatString = "c2";
        }
    }
}
