﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsTransferOrderDetailForEditDataGridView : PartsTransferOrderDetailForApproveDataGridView {
        private RadWindow radQueryWindow;
        private DcsMultiPopupsQueryWindowBase warehousePartsStockQueryWindow;

        private DcsMultiPopupsQueryWindowBase WarehousePartsStockQueryWindow {
            get {
                if(this.warehousePartsStockQueryWindow == null) {
                    this.warehousePartsStockQueryWindow = DI.GetQueryWindow("VirtualWarehousePartsStock") as DcsMultiPopupsQueryWindowBase;
                    var dcsMultiPopupsQueryWindowBase = this.warehousePartsStockQueryWindow;
                    if(dcsMultiPopupsQueryWindowBase != null) {
                        dcsMultiPopupsQueryWindowBase.Loaded += this.WarehousePartsStockQueryWindow_Loaded;
                        dcsMultiPopupsQueryWindowBase.SelectionDecided += this.WarehousePartsStockQueryWindow_SelectionDecided;
                    }
                }
                return this.warehousePartsStockQueryWindow;
            }
        }

        private RadWindow RadWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.WarehousePartsStockQueryWindow,
                    Header = PartsStockingUIStrings.QueryPanel_Title_PartsStockQueryForTransfer,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }

        private void WarehousePartsStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(partsTransferOrder == null || queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("WarehouseId", typeof(int), FilterOperator.IsEqualTo, partsTransferOrder.OriginalWarehouseId));
        }

        protected virtual void WarehousePartsStockQueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var warehousePartsStock = queryWindow.SelectedEntities.Cast<WarehousePartsStock>();
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(warehousePartsStock == null || partsTransferOrder == null)
                return;
            if(partsTransferOrder.PartsTransferOrderDetails.Any(r => warehousePartsStock.Any(entity => entity.SparePartId == r.SparePartId))) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrderDetail_SparePartIdIsRepeat);
                return;
            }
            var warehousePartsStocks = warehousePartsStock as IList<WarehousePartsStock> ?? warehousePartsStock.ToList();
            foreach(var detail in warehousePartsStocks) {
                var partsTransferOrderDetail = new PartsTransferOrderDetail();
                partsTransferOrder.PartsTransferOrderDetails.Add(partsTransferOrderDetail);
                partsTransferOrderDetail.SparePartId = detail.SparePartId;
                partsTransferOrderDetail.SparePartCode = detail.SparePartCode;
                partsTransferOrderDetail.SparePartName = detail.SparePartName;
                //都改为成本价
                partsTransferOrderDetail.Price = detail.PartsPlannedPrice;
                partsTransferOrderDetail.PlannPrice = detail.PartsPlannedPrice;
                partsTransferOrderDetail.OriginWarehouseStock = detail.UsableQuantity;
                partsTransferOrderDetail.PartsSalesCategoryId = detail.PartsSalesCategoryId;
                partsTransferOrderDetail.MInPackingAmount = detail.MInPackingAmount;
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            e.Cancel = !this.GridView.SelectedItems.Any();
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            partsTransferOrder.TotalAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(detail => detail.PlannedAmount * detail.Price);
            partsTransferOrder.TotalPlanAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(detail => detail.PlannedAmount * detail.PlannPrice);
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            partsTransferOrder.TotalAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(detail => detail.PlannedAmount * detail.Price);
            partsTransferOrder.TotalPlanAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(detail => detail.PlannedAmount * detail.PlannPrice);
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "PlannedAmount":
                    partsTransferOrder.TotalAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(detail => detail.PlannedAmount * detail.Price);
                    partsTransferOrder.TotalPlanAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(detail => detail.PlannedAmount * detail.PlannPrice);
                    break;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            if(partsTransferOrder.OriginalWarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrder_OriginalWarehouseIdIsNull);
                return;
            }

            if(partsTransferOrder.DestWarehouseId == default(int)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Validation_PartsTransferOrder_DestWarehouseIdIsNull);
                return;
            }
            this.RadWindow.ShowDialog();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PlannedAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsTransferOrderDetail_PlannedAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "OriginWarehouseStock",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsTransferOrderDetail_OriginWarehouseStock,
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "DestinWarehouseStock",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsTransferOrderDetail_DestinWarehouseStock,
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "MInPackingAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_MInPackingAmount,
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }
                    //, new ColumnItem{
                    //    Name = "Price",
                    //    IsReadOnly = true,
                    //    TextAlignment = TextAlignment.Right
                    //},new ColumnItem{
                    //    Name = "PlannPrice",
                    //    IsReadOnly = true,
                    //    TextAlignment = TextAlignment.Right
                    //}
                    ,new ColumnItem {
                        Name = "Remark"
                    }
                    //,new ColumnItem {
                    //    Name = "OverseasPartsFigure",
                    //    Title = "海外配件图号"
                    //}
                    //,new ColumnItem {
                    //    Name = "POCode",
                    //    Title = "PO单号"
                    //}
                };
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DataPager.PageSize = 100;
            this.GridView.CanUserInsertRows = true;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            //((GridViewDataColumn)this.GridView.Columns["PlannPrice"]).DataFormatString = "c2";
        }
    }
}
