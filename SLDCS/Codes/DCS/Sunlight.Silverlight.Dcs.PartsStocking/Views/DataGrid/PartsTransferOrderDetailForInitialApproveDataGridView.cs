﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsTransferOrderDetailForInitialApproveDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PlannedAmount",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ConfirmedAmount",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "OriginWarehouseStock",
                         Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsTransferOrderDetail_OriginWarehouseStock,
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "DestinWarehouseStock",
                         Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsTransferOrderDetail_DestinWarehouseStock,
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name = "MInPackingAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_MInPackingAmount,
                        IsReadOnly = true
                    }
                    //, new ColumnItem{
                    //    Name = "Price",
                    //    IsReadOnly = true,
                    //    TextAlignment = TextAlignment.Right
                    //},new ColumnItem{
                    //    Name = "PlannPrice",
                    //    IsReadOnly = true,
                    //    TextAlignment = TextAlignment.Right
                    //}
                    //,new ColumnItem {
                    //    Name = "POCode",
                    //    Title = "PO单号"
                    //}
                    ,new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(PartsTransferOrderDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsTransferOrderDetails");
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var partsTransferOrder = this.DataContext as PartsTransferOrder;
            if(partsTransferOrder == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "ConfirmedAmount":
                    partsTransferOrder.TotalPlanAmount = partsTransferOrder.PartsTransferOrderDetails.Sum(detail => detail.ConfirmedAmount * detail.PlannPrice);
                    break;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DataPager.PageSize = 100;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            //if(this.GridView.Columns["ConfirmedAmount"] != null)
            //    ((GridViewDataColumn)this.GridView.Columns["ConfirmedAmount"]).DataFormatString = "d";
            //((GridViewDataColumn)this.GridView.Columns["PlannedAmount"]).DataFormatString = "d";
            //((GridViewDataColumn)this.GridView.Columns["OriginWarehouseStock"]).DataFormatString = "d";
            //((GridViewDataColumn)this.GridView.Columns["DestinWarehouseStock"]).DataFormatString = "d";
            //((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            //((GridViewDataColumn)this.GridView.Columns["PlannPrice"]).DataFormatString = "c2";
        }
    }
}
