﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsTransferOrderDetailForKanBanDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_SparePartStatisticsForKanBan_SparePartCode,
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_SparePartStatisticsForKanBan_SparePartName,
                    }, new ColumnItem {
                        Name = "PartsTransferAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_SparePartStatisticsForKanBan_PartsTransferAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "PartsOutboundAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_SparePartStatisticsForKanBan_PartsOutboundAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "PartsShippingAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_SparePartStatisticsForKanBan_PartsShippingAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "PartsInboundAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_SparePartStatisticsForKanBan_PartsInboundAmount,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(SparePartStatisticsForKanBan);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SparePartStatisticsForKanBans");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.Columns.Insert(0, new GridViewSerialNumberColumn {
                Header = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Common_SerialNumber
            });
            ((GridViewDataColumn)this.GridView.Columns["PartsTransferAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["PartsOutboundAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["PartsShippingAmount"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["PartsInboundAmount"]).DataFormatString = "d";
        }
    }
}
