﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PartsTransferOrderWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsTransferOrder_Status", "PartsTransferOrder_Type", "PartsShipping_Method"
        };

        public PartsTransferOrderWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsTransferOrderDetail_Code
                    },
                    new ColumnItem {
                        Name = "IsUploadFile",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_IsUploadFile
                    },
                    new ColumnItem {
                        Name = "OriginalWarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsTransferOrderDetail_OriginalWarehouseName
                    },
                    new ColumnItem {
                        Name = "DestWarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsTransferOrderDetail_DestWarehouseName
                    },                  
                    new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },
                    new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    },
                    //new ColumnItem {
                    //    Name = "TotalAmount",
                    //    TextAlignment = TextAlignment.Right
                    //},
                    //new ColumnItem {
                    //    Name = "TotalPlanAmount",
                    //    TextAlignment = TextAlignment.Right
                    //},
                    //new ColumnItem {
                    //    Name = "OriginalBillCode",
                    //},
                    //new ColumnItem {
                    //    Name = "ERPSourceOrderCode",
                    //    Title = "平台单号"
                    //},
                    //new ColumnItem {
                    //    Name = "GPMSPurOrderCode"
                    //},
                    //new ColumnItem {
                    //    Name = "SAPPurchasePlanCode",
                    //    Title = "SAP采购计划单号"
                    //},
                    new ColumnItem {
                        Name = "Remark"
                    },
                    new ColumnItem {
                        Name = "CreatorName"
                    },
                    new ColumnItem {
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "InitialApproverName",
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_CheckerName
                    },
                    new ColumnItem {
                        Name = "InitialApproveTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right,
                        Title=PartsStockingUIStrings.DataGridView_ColumnItem_CheckTime
                    },
                    new ColumnItem {
                        Name = "ApproverName"
                    },
                    new ColumnItem {
                        Name = "ApproveTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "AbandonerName"
                    },
                    new ColumnItem {
                        Name = "AbandonComment"
                    },
                    new ColumnItem {
                        Name = "AbandonTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },
                    new ColumnItem {
                        Name = "ModifierName"
                    },
                    new ColumnItem {
                        Name = "ModifyTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsTransferOrder);
            }
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider() {
                    DetailPanelNames = new[] {
                        "PartsTransferOrderDetail"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPartsTransferOrdersByWarehouseOperator";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            //((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "c2";
            //((GridViewDataColumn)this.GridView.Columns["TotalPlanAmount"]).DataFormatString = "c2";

        }
    }
}
