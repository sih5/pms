﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid
{
    public class PickingTaskDetailForEditDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
            "ShortPickingReason","TraceProperty"
        };


        public PickingTaskDetailForEditDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }     
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                        Name = "PartsOutboundPlanCode",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_PartsOutboundPlanCode,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "WarehouseAreaCode",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskDetail_WarehouseAreaCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ReferenceCode",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskDetail_ReferenceCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskDetail_MeasureUnit,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PlanQty",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title= PartsStockingUIStrings.DataEditView_Title_PlanAmount,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PickingQty",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskDetail_PickingQty,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "CourrentPicking",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskDetail_CourrentPicking,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "BatchNumber",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_PBatchNumber,
                    }, new KeyValuesColumnItem {
                        Name = "ShortPickingReason",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskDetail_ShortPickingReason
                    }, new KeyValuesColumnItem {
                        Name = "TraceProperty",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = "追溯属性",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "TraceCode",
                        Title= "标签码"
                    }
                };
            }
        }

        protected override bool ShowCheckBox
        {
            get
            {
                return false;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }
        }
        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualPickingTaskDetail);
            }
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 50;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["PlanQty"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["PickingQty"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CourrentPicking"]).DataFormatString = "d";
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
           
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e)
        {
            if(e.Cell.Column.UniqueName.Equals("CourrentPicking")) {
                var detail1 = e.Cell.DataContext as VirtualPickingTaskDetail;
                if(detail1 == null)
                    return;
                var quantityError = detail1.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("CourrentPicking"));
                detail1.ValidationErrors.Clear();              
            }
            
        }
        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e)
        {
            if(!e.Cell.Column.UniqueName.Equals("CourrentPicking"))
                return;
            var detail = e.Cell.DataContext as VirtualPickingTaskDetail;
            if(detail == null)
                return;
            var quantityError = detail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("CourrentPicking"));
            if(quantityError != null)
                detail.ValidationErrors.Remove(quantityError);
            if(null == detail.PickingQty) {
                detail.PickingQty = 0;
            }
            if(detail.PickingQty>0 &&((detail.PlanQty - detail.PickingQty) < detail.CourrentPicking)) {
                detail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataEditView_Validation_PckingTask_CurrentQuntity_IsMustLessThanPlannedAmount, new[] {
                        "CourrentPicking"
                    }));
            }           
        }

        protected override Binding OnRequestDataSourceBinding()
        {
            return new Binding("PickingTaskDetailsds");
        }
       
    }
}
