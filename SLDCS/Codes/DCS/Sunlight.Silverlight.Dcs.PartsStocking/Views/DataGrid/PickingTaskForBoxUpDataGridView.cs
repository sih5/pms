﻿
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using System;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PickingTaskForBoxUpDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
             "PartsShipping_Method"
        };
        public PickingTaskForBoxUpDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }

        protected override Binding OnRequestDataSourceBinding()
        {
            return new Binding("PickingTasks");
        }


        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataEditView_Text_PickingTaskCode,
                        Name = "Code"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseCode,
                        Name = "WarehouseCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseName,
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyCode,
                        Name = "CounterpartCompanyCode"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyName,
                        Name = "CounterpartCompanyName"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyAddress_DetailAddress,
                        Name = "ReceivingAddress"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsShippingOrderRef_PartsOutboundBill_PartsSalesOrderTypeName,
                        Name = "OrderTypeName"
                    },new KeyValuesColumnItem{
                        Name = "ShippingMethod",
                        Title = PartsStockingUIStrings.DataEditPanel_Text_Logistics_ShippingMethod,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreatorName,
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreateTime,
                        Name = "CreateTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.QueryPanel_Title_Pickingfinishtime,
                        Name = "PickingFinishTime",
                        MaskType = MaskType.DateTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_SalesCategoryName,
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }

        }
        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualPickingTaskForBoxUp);
            }
        }
    }
}