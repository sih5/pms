﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid
{
    public class PickingTaskForDetailDataGridView : DcsDataGridViewBase
    {
         private readonly string[] kvNames = new[] {
            "ShortPickingReason"
        };

         private void PickingTaskForDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
         {
             var pickingTask = e.NewValue as PickingTaskQuery;
             if (pickingTask == null || pickingTask.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PickingTaskId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = pickingTask.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
          public PickingTaskForDetailDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.PickingTaskForDetailDataGridView_DataContextChanged;
        }     
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                    new ColumnItem {
                        Name = "PartsOutboundPlanCode",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_PartsOutboundPlanCode,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "WarehouseAreaCode",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskDetail_WarehouseAreaCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true,
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsStockQueryView_PartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_SparePartName,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MeasureUnit",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskDetail_MeasureUnit,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PlanQty",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title= PartsStockingUIStrings.DataEditView_Title_PlanAmount,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PickingQty",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskDetail_PickingQty,
                        IsReadOnly = true
                    } ,new KeyValuesColumnItem{
                        Name = "ShortPickingReason",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskDetail_ShortPickingReason,
                        IsReadOnly = true,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

       
        protected override Type EntityType
        {
            get
            {
                return typeof(PickingTaskDetail);
            }
        }
        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["PlanQty"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["PickingQty"]).DataFormatString = "d";
           
        }
        protected override string OnRequestQueryName()
        {
            return "GetPickingTaskDetailLists";
        }
    }
}
