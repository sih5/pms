﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PickingTaskForPrintDataGridView : DcsDataGridViewBase {    
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskQuery_Code,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "WarehouseAreaName",
                        Title= PartsStockingUIStrings.DataEditView_Text_WarehouseArea,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PickingQty",
                        IsReadOnly = true,
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTask_PickingQty
                    }, new ColumnItem {
                        Name = "Varieties",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTask_Varieties,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PickingTaskForPrint);
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Multiple;

        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var FilterItem = filters.Filters.SingleOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                switch(parameterName) {
                    case "id":
                        //return filters.Filters.Single(item => item.MemberName == "PickingTaskId").Value;
                        return FilterItem.Filters.Single(item => item.MemberName == "PickingTaskId").Value;
                    case "warehouseArea":
                    //    return filters.Filters.SingleOrDefault(item => item.MemberName == "WarehouseArea").Value;
                        return null;

                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "GetPickingTaskForPrintId";
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}