﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using System.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid
{
    public class PickingTaskQueryDataGridView : DcsDataGridViewBase
    {
        private readonly string[] kvNames = new[] {
             "PickingTaskStatus"
        };
        public PickingTaskQueryDataGridView()
        {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                     new KeyValuesColumnItem{
                        Name = "Status",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name = "ResponsiblePersonName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskQuery_ResponsiblePersonName
                    },
                    new ColumnItem {
                        Name = "Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PickingTaskQuery_Code
                    }, new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseCode
                    }, new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseName
                    }, new ColumnItem {
                        Name = "CounterpartCompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyCode
                    }, new ColumnItem {
                        Name = "CounterpartCompanyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyName
                    }, new ColumnItem{
                        Name = "CreatorName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreatorName
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CreateTime
                    }, new ColumnItem{
                        Name = "ModifierName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    }, new ColumnItem{
                        Name = "ModifyTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime
                    }, new ColumnItem{
                        Name = "PickingFinishTime",
                        Title = PartsStockingUIStrings.QueryPanel_Title_Pickingfinishtime
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundPlan_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override string OnRequestQueryName()
        {
            return "getPickingTaskListsAll";
        }


        protected override Type EntityType
        {
            get
            {
                return typeof(PickingTaskQuery);
            }
        }
        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }

        protected override bool UsePaging
        {
            get
            {
                return true;
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName)
        {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if (compositeFilterItem != null)
            {
                var param = new[] { "SourceCode", "PartsOutboundPlanCode" };
                foreach (var filterItem in compositeFilterItem.Filters.Where(filter => !param.Contains(filter.MemberName)))
                {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName)
        {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null)
            {
                var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName)
                {
                    case "code":
                        return filters.Filters.Single(item => item.MemberName == "Code").Value;
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "partsSalesCategoryId":
                        return filters.Filters.Single(item => item.MemberName == "PartsSalesCategoryId").Value;
                    case "sourceCode":
                        return filters.Filters.Single(item => item.MemberName == "SourceCode").Value;
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "partsOutboundPlanCode":
                        return filters.Filters.Single(item => item.MemberName == "PartsOutboundPlanCode").Value;
                    case "orderTypeName":
                        return filters.Filters.Single(item => item.MemberName == "OrderTypeName").Value;
                    case "counterpartCompanyName":
                        return filters.Filters.Single(item => item.MemberName == "CounterpartCompanyName").Value;
                    case "bCreateTime":
                        var createTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                    case "bPickingfinishtime":
                        var expectedPlaceDate = composites.FirstOrDefault(p => p.Filters.First().MemberName == "Pickingfinishtime");
                        return expectedPlaceDate == null ? null : expectedPlaceDate.Filters.First(r => r.MemberName == "Pickingfinishtime").Value;
                    case "ePickingfinishtime":
                        var expectedPlaceDate1 = composites.FirstOrDefault(p => p.Filters.First().MemberName == "Pickingfinishtime");
                        return expectedPlaceDate1 == null ? null : expectedPlaceDate1.Filters.Last(item => item.MemberName == "Pickingfinishtime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider
        {
            get
            {
                return new TabControlRowDetailsTemplateProvider
                {
                    DetailPanelNames = new[] {
                        "PickingTaskForDetail"
                    }
                };
            }
        }
        protected override void OnControlsCreated()
        {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            base.OnControlsCreated();
        }
    }
}
