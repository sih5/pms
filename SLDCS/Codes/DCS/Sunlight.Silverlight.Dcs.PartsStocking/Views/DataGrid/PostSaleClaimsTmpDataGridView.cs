﻿using System;
using System.Linq;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PostSaleClaimsTmpDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
              "PostSaleClaimsTmp_Status","PostSaleClaimsTmp_Type"    
        };
        public PostSaleClaimsTmpDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new ColumnItem[] { 
                   new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Type
                    }, new ColumnItem{
                        Name = "ClaimBillCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_ClaimBillCode
                    }, new ColumnItem{
                        Name = "DealerCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_StorageCompanyCode
                    },new ColumnItem {
                        Name = "DealerName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_DealerPartsInventoryBill_StorageCompanyName
                    }, new ColumnItem{
                        Name = "IsClaim",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PostSaleClaimsTmp_IsClaim
                    }, new ColumnItem{
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_SparePartStatisticsForKanBan_SparePartCode
                    }, new ColumnItem{
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_SparePartStatisticsForKanBan_SparePartName
                    }, new ColumnItem{
                        Name = "Amount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_Quantity
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Express_Status
                    }, new ColumnItem{
                        Name = "CreateTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime
                    },new ColumnItem{
                        Name = "TransferTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PostSaleClaimsTmp_TransferTime
                    },new ColumnItem{
                        Name = "SalesPrice",
                        Title = "经销价"
                    },new ColumnItem{
                        Name = "RetailGuidePrice",
                        Title = "建议售价"
                    },new ColumnItem{
                        Name = "GroupCode",
                        Title = "价格属性"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "getPostSaleClaims";
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPostSaleClaimsTmp);
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if (filters != null) {
                var date = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
                switch (parameterName) {
                    case "claimBillCode":
                        return filters.Filters.Single(item => item.MemberName == "ClaimBillCode").Value;
                    case "sparePartCode":
                        return filters.Filters.Single(item => item.MemberName == "SparePartCode").Value;
                    case "sparePartName":
                        return filters.Filters.Single(item => item.MemberName == "SparePartName").Value;
                    case "dealerCode":
                        return filters.Filters.Single(item => item.MemberName == "DealerCode").Value;
                    case "dealerName":
                        return filters.Filters.Single(item => item.MemberName == "DealerName").Value;
                    case "type":
                        return filters.Filters.Single(item => item.MemberName == "Type").Value;
                    case "bCreateTime":
                        var createTime = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime == null ? null : createTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "eCreateTime":
                        var createTime1 = date.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return createTime1 == null ? null : createTime1.Filters.Last(item => item.MemberName == "CreateTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
