﻿using System;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid
{
    public class PrintLabelForPartsInboundCheckBillForQueryDataGridView : DcsDataGridViewBase
    {

        protected override System.Collections.Generic.IEnumerable<Core.Model.ColumnItem> ColumnItems
        {
            get
            {
                return new[] {
                   new ColumnItem {
                         Name = "Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_Code,
                        IsReadOnly = true
                    },new ColumnItem {
                         Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                         Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartName,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "InspectedQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBillForPrint_InspectedQuantity,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "FirPrintNumber",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBillForPrint_FirPrintNumber,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SecPrintNumber",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBillForPrint_SecPrintNumber,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ThidPrintNumber",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBillForPrint_ThidPrintNumber,
                        IsReadOnly = true
                    }, new ColumnItem {
                         Name = "BachMunber",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBillForPrint_BachMunber,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType
        {
            get
            {
                return typeof(VirtualPartsInboundCheckBillForPrint);
            }
        }

        protected override Binding OnRequestDataSourceBinding()
        {
            return new Binding("PartsInboundPlanDetails");
        }


        protected override void OnControlsCreated()
        {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.MaxHeight = 500;
            this.DomainDataSource.PageSize = 100;
        }

        protected override bool ShowCheckBox
        {
            get
            {
                return true;
            }
        }
    }

}

