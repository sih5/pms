﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PrintWmsLabelForPartsInboundPlanDataGridView : DcsDataGridViewBase {
        protected override System.Collections.Generic.IEnumerable<Core.Model.ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "PartsInboundPlan.Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Code,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartCode,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartName,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "PlannedAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBillDetail_OutboundAmount,
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "PrintNumber",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlanDetail_PrintNumber,
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsInboundPlanDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsInboundPlanDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
