﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class PurchaseWarehousePartsStockDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseCode
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseName
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartName
                    }, new ColumnItem {
                        Name = "UsableQuantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_UsableQuantity
                    }, new ColumnItem {
                        Name = "PartsPurchasePricing",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsPurchasePricing
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_BranchName
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            ((GridViewDataColumn)this.GridView.Columns["UsableQuantity"]).DataFormatString = "d";
        }

        protected override Type EntityType {
            get {
                return typeof(WarehousePartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "采购退货查询仓库库存";
        }

        protected override object OnRequestQueryParameter(string queryname, string parametername) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem != null) {
                var cFilterItem = filterItem.Filters.FirstOrDefault(item => item.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(cFilterItem == null)
                    return null;
                switch(parametername) {
                    case "storagecompanyId":
                        return BaseApp.Current.CurrentUserData.EnterpriseId;
                    case "warehouseId":
                        var filtersWarehouse = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "WarehouseId");
                        return filtersWarehouse == null ? null : filtersWarehouse.Value;
                    case "partsSaleCategoryId":
                        var filtersPartsSaleCategory = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesCategoryId");
                        return filtersPartsSaleCategory == null ? null : filtersPartsSaleCategory.Value;
                    case "partCode":
                        var filterPartCode = filterItem.Filters.SingleOrDefault(item => item.MemberName == "SparePartCode");
                        return filterPartCode == null ? null : filterPartCode.Value;
                    case "partName":
                        var filterPartName = filterItem.Filters.SingleOrDefault(item => item.MemberName == "SparePartName");
                        return filterPartName == null ? null : filterPartName.Value;
                    case "supplierId":
                        var filtersSupplier = cFilterItem.Filters.SingleOrDefault(item => item.MemberName == "PartsSupplierId");
                        return filtersSupplier == null ? null : filtersSupplier.Value;
                }
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryname) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] { "WarehouseCode", "WarehouseName", "BranchName", "SparePartCode", "SparePartName", "UsableQuantity", "PartsPurchasePricing" };
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => param.Contains(filter.MemberName))) {
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}

