﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class TraceTempTypeDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "TraceProperty"
        };
        public TraceTempTypeDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += PartsInventoryDetailDataGridView_DataContextChanged;
        }

        protected override Type EntityType {
            get {
                return typeof(TraceTempType);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name = "SIHLabelCode",
                        Title="SIH标签码"
                    },new ColumnItem{
                        Name = "Quantity",
                        Title="数量"
                    }, new KeyValuesColumnItem{
                        Name = "TraceProperty",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = "追溯属性"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetTraceTempTypes";
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
            base.OnControlsCreated();
        }
        private void PartsInventoryDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsInventoryBill = e.NewValue as PartsInventoryDetail;
            if(partsInventoryBill == null || partsInventoryBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SourceBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsInventoryBill.PartsInventoryBillId
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SourceBillDetailId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = partsInventoryBill.Id
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Type",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DCSTraceTempType.盘点
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsBaseDataStatus.有效
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }
    }
}