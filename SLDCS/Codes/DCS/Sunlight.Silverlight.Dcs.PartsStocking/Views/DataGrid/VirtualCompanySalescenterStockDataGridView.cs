﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Core;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class VirtualCompanySalescenterStockDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartsCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_SparePartsCode
                    }, new ColumnItem {
                        Name = "SparePartsName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_SparePartsName
                    }, new ColumnItem {
                        Name = "FactAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_FactAmount
                    }, new ColumnItem {
                        Name = "UsableAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_UsableAmount
                    }, new ColumnItem {
                        Name = "CompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_CompanyCode
                    },new ColumnItem {
                        Name = "CompanyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_CompanyName
                    },new ColumnItem {
                        Name = "CompanyType",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_CompanyType
                    },new ColumnItem {
                        Name = "PartsSalesPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_PartsSalesPrice
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_BranchName
                    },new KeyValuesColumnItem{
                        Name="PartsSalesCategoryName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualCompanySalescenterStock_PartsSalesCategory
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "VirtualCompanySalescenterStock"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
        }
        protected override string OnRequestQueryName() {
            return "查询企业销售中心库存";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            switch(parameterName) {
                case "companyId":
                    var filterItem1 = BaseApp.Current.CurrentUserData.EnterpriseId;
                    return filterItem1;
                case "partsSalesCategoryId":
                    var filterItem2 = filter.Filters.SingleOrDefault(item => item.MemberName == "PartsSalesCategoryId");
                    if(filterItem2 == null)
                        break;
                    return filterItem2.Value;
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var filters = compositeFilterItem.Filters;
                foreach(var item in filters) {
                    var filterItem = item as CompositeFilterItem;
                    if(filterItem != null)
                        if(filterItem.Filters.Any(filter => filter.MemberName != "PartsSalesCategoryId"))
                            newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override System.Type EntityType {
            get {
                return typeof(VirtualCompanySalescenterStock);
            }
        }
    }
}
