﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class VirtualOutboundAndInboundBillHistoricalDataBaseDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Parts_OutboundType"
        };

        public VirtualOutboundAndInboundBillHistoricalDataBaseDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseCode
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundCheckBill_WarehouseName
                    },new ColumnItem {
                        Name = "ReceivingWarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_ReceivingWarehouseCode
                    },new ColumnItem {
                        Name = "ReceivingWarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsOutboundBill_ReceivingWarehouseName
                    },new ColumnItem {
                        Name = "PartsSalesOrderTypeName",
                        Title = PartsStockingUIStrings.QueryPanel_Title_SalesOrderType
                    },new ColumnItem {
                        Name = "BillCode",
                        Title = PartsStockingUIStrings.DataEditPanel_PartsOutboundBill_Code
                    },new KeyValuesColumnItem {
                        Name = "BillBusinessType",
                        Title = PartsStockingUIStrings.QueryPanel_Title_OutboundType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    },new ColumnItem {
                        Name = "CounterpartCompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyCode
                    },new ColumnItem {
                        Name = "CounterpartCompanyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyName
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartCode
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartName
                    },new ColumnItem {
                        Name = "WarehouseAreaCode",
                        Title = PartsStockingUIStrings.QueryPanel_Title_WarehouseArea2
                    },new ColumnItem {
                        Name = "WholesalePrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_WholesalePrice
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_QuantityNumber
                    },new ColumnItem {
                        Name = "CreateTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime
                    },new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsApp_BranchName
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsStock_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var filteritem = filters.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                var fileitem = filters.Filters.LastOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                switch(parameterName) {
                    case "sparePartId":
                        return fileitem.Filters.Single(item => item.MemberName == "SparePartId").Value;
                    case "warehouseId":
                        return fileitem.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "warehouseAreaCode":
                        var warehouseAreaCode = filteritem.Filters.SingleOrDefault(item => item.MemberName == "WarehouseAreaCode");
                        return warehouseAreaCode == null ? null : warehouseAreaCode.Value;
                    case "partsSalesCategoryId":
                        var salesCategory = filteritem.Filters.SingleOrDefault(e => e.MemberName == "SalesCategoryId");
                        return salesCategory == null ? null : salesCategory.Value;
                    case "partsSalesOrderTypeId":
                        var saleType = filteritem.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesOrderTypeId");
                        return saleType == null ? null : saleType.Value;
                    case "billBusinessType":
                        return PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_billBusinessType;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var filters = compositeFilterItem.Filters;
                foreach(var item in filters) {
                    var filterItem = item as CompositeFilterItem;
                    if(filterItem != null)
                        foreach(var timefilter in filterItem.Filters.Where(filter => filter.MemberName == "CreateTime"))
                            newCompositeFilterItem.Filters.Add(timefilter);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualOutboundAndInboundBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询历史出入库记录";
        }
    }
}