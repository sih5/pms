﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class VirtualPartsStockDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Category"
        };

        public VirtualPartsStockDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_WarehouseName
                    }, new KeyValuesColumnItem {
                        Name = "WarehouseAreaCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_WarehouseAreaCategory
                    }, new ColumnItem{
                        Name = "WarehouseRegionCode",
                        Title = PartsStockingUIStrings.QueryPanel_QueryItem_Title_WarehouseRegionCode
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_WarehouseAreaCode
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartName
                    }, new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_Quantity
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件库位库存带营销信息";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "warehouseAreaCategoryValue":
                        var filterCategory = filters.Filters.SingleOrDefault(item => item.MemberName == "WarehouseAreaCategory");
                        if(filterCategory != null)
                            return filterCategory.Value;
                        return null;
                    case "managerId":
                        var filtermanagerId = filters.Filters.SingleOrDefault(item => item.MemberName == "BaseApp.Current.CurrentUserData.EnterpriseId");
                        if(filtermanagerId != null)
                            return filtermanagerId.Value;
                        return null;
                }
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "WarehouseAreaCategory"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
