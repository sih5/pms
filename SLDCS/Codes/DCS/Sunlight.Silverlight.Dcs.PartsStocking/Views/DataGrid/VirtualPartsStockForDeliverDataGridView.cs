﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class VirtualPartsStockForDeliverDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = {
            "Area_Category"
        };

        public VirtualPartsStockForDeliverDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem{
                        Name = "WarehouseAreaCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true,
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStock_WarehouseAreaCategory
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsableQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStock_UsableQuantity,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "OutboundAmount",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStock_OutboundAmount,
                        IsReadOnly = false
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsOutboundBillDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("PartsOutboundBillDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.RowLoaded += GridView_RowLoaded;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["UsableQuantity"]).DataFormatString = "d";
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var partsOutboundBillDetail = e.Row.DataContext as PartsOutboundBillDetail;
            if(partsOutboundBillDetail == null)
                return;
            var partsOutboundBillDataEditView = this.DataContext as PartsOutboundBillDataEditView;
            if(partsOutboundBillDataEditView == null)
                return;
            GridViewRow gridViewRow = this.GridView.GetRowForItem(partsOutboundBillDetail);

            if(partsOutboundBillDataEditView.PartsOutboundPlanDetailForDeliverDataGridView.SelectedEntities == null || !partsOutboundBillDataEditView.PartsOutboundPlanDetailForDeliverDataGridView.SelectedEntities.Any()) {
                if(gridViewRow != null) {
                    gridViewRow.Background = partsOutboundBillDataEditView.PartsOutboundBillDetails.Count(r => r.SparePartId == partsOutboundBillDetail.SparePartId) > 1 ? new SolidColorBrush(Colors.Red) : new SolidColorBrush(Colors.White);
                }
            } else {
                var partsOutboundPlanDetail = partsOutboundBillDataEditView.PartsOutboundPlanDetailForDeliverDataGridView.SelectedEntities.Cast<PartsOutboundPlanDetail>().SingleOrDefault();
                if(partsOutboundPlanDetail != null && partsOutboundPlanDetail.SparePartId == partsOutboundBillDetail.SparePartId)
                    if(gridViewRow != null)
                        gridViewRow.Background = new SolidColorBrush(Colors.Blue);
            }

        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(!e.Cell.Column.UniqueName.Equals("OutboundAmount"))
                return;
            var partsOutboundBillDetail = e.Cell.DataContext as PartsOutboundBillDetail;
            if(partsOutboundBillDetail == null)
                return;
            var quantityError = partsOutboundBillDetail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("OutboundAmount"));
            if(partsOutboundBillDetail.ValidationErrors.Contains(quantityError))
                partsOutboundBillDetail.ValidationErrors.Remove(quantityError);
            if(partsOutboundBillDetail.OutboundAmount < 0) {
                partsOutboundBillDetail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataGridView_Validation_PartsStock_OutboundAmountNotIs0, new[] {
                        "OutboundAmount"
                    }));
                return;
            }
            var partsOutboundBillDataEditView = this.DataContext as PartsOutboundBillDataEditView;
            if(partsOutboundBillDataEditView == null)
                return;
            var outboundAmount = partsOutboundBillDataEditView.PartsOutboundBillDetails.Where(r => r.SparePartId == partsOutboundBillDetail.SparePartId).Sum(r => r.OutboundAmount);
            //剩余出库数量
            var partsOutboundPlanDetail = partsOutboundBillDataEditView.PartsOutboundPlanDetails.SingleOrDefault(r => r.SparePartId == partsOutboundBillDetail.SparePartId);
            var currentOutboundAmount = partsOutboundPlanDetail.PlannedAmount - partsOutboundPlanDetail.OutboundFulfillment;
            if(currentOutboundAmount < outboundAmount) {
                partsOutboundBillDetail.ValidationErrors.Add(new ValidationResult(PartsStockingUIStrings.DataGridView_Validation_PartsStock_OutboundAmountNoMoreThanSurplus, new[] {
                        "OutboundAmount"
                    }));
                return;
            }
            partsOutboundPlanDetail.CurrentOutboundAmount = outboundAmount;//修改本次出库数量
        }

        public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            if(subject == "Blue") {
                var selectedItems = this.GridView.SelectedItems;
                var partsOutboundBillDataEditView = this.DataContext as PartsOutboundBillDataEditView;
                if(partsOutboundBillDataEditView == null)
                    return null;
                if(selectedItems == null || !selectedItems.Any()) {
                    foreach(var partsOutboundBillDetail in this.GridView.Items.Cast<PartsOutboundBillDetail>().Where(r => r.SparePartId != (int)contents[0])) {
                        var gridViewRow = this.GridView.GetRowForItem(partsOutboundBillDetail);
                        if(gridViewRow != null)
                            gridViewRow.Background = partsOutboundBillDataEditView.PartsOutboundBillDetails.Count(r => r.SparePartId == partsOutboundBillDetail.SparePartId) > 1 ? new SolidColorBrush(Colors.Red) : new SolidColorBrush(Colors.White);

                    }
                    foreach(PartsOutboundBillDetail partsOutboundBillDetail in this.GridView.Items.Cast<PartsOutboundBillDetail>().Where(r => r.SparePartId == (int)contents[0])) {
                        GridViewRow gridViewRow = this.GridView.GetRowForItem(partsOutboundBillDetail);
                        if(gridViewRow != null)
                            gridViewRow.Background = new SolidColorBrush(Colors.Blue);
                    }
                }
            }
            return null;
        }
    }
}
