﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class VirtualPartsStockForPartsPackingDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseCode"
                    },new ColumnItem {
                        Name = "WarehouseName"
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode"
                    }, new ColumnItem {
                        Name = "SparePartCode"
                    }, new ColumnItem {
                        Name = "SparePartName"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_UsableQuantityForPacking,
                        Name = "UsableQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询待包装配件库存";
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["UsableQuantity"]).DataFormatString = "d";
        }
    }
}
