﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class VirtualPartsStockForPartsShiftOrderDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartCode,
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_SparePartName,
                        Name = "SparePartName"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_WarehouseAreaCode,
                        Name = "WarehouseAreaCode"
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_UsableQuantity,
                        Name = "UsableQuantity",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsStock_LockedQty,
                        Name = "LockedQty",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询配件库位库存";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                switch(parameterName) {
                    case "warehouseAreaCategoryValue":
                        var filter = compositeFilterItem.Filters.SingleOrDefault(item => item.MemberName == "WarehouseAreaCategory");
                        return filter == null ? 0 : filter.Value;
                    case "managerId":
                        return BaseApp.Current.CurrentUserData.UserId;
                    case "partsOutPlanId":
                        return null;
                    case "operatorId":
                        return null;
                    case "warehouseRegionCode":
                        return null;
                    case "pricetype":
                        return null;
                    case "greaterThanZero":
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "WarehouseAreaCategory"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["UsableQuantity"]).DataFormatString = "d";
        }
    }
}
