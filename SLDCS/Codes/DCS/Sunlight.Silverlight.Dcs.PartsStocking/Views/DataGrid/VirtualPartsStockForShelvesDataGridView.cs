﻿using System;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class VirtualPartsStockForShelvesDataGridView : DcsDataGridViewBase {

        //导出待上架清单传参使用
        public int WarehosueId;
        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SparePartCode"
                    },new ColumnItem {
                        Name = "SparePartName"
                    },new ColumnItem {
                        Name = "UsableQuantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_Quantity,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    } 
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualPartsStock);
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            switch(parameterName) {
                case "warehouseId":
                    var compositeFilterItem = this.FilterItem as CompositeFilterItem;
                    if(compositeFilterItem != null) {
                        var filterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "WarehouseId");
                        var warehouseId = filterItem == null ? null : filterItem.Value;
                        if(filterItem != null)
                            compositeFilterItem.Filters.Remove(filterItem);
                        WarehosueId = warehouseId == null ? 0 : (int)warehouseId;
                        return warehouseId;
                    }
                    return this.FilterItem.MemberName == "WarehouseId" ? this.FilterItem.Value : null;
            }

            return base.OnRequestQueryParameter(queryName, parameterName);
        }
        protected override string OnRequestQueryName() {
            return "查询待上架配件库存";
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }


        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 100;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["UsableQuantity"]).DataFormatString = "d";
        }
    }
}
