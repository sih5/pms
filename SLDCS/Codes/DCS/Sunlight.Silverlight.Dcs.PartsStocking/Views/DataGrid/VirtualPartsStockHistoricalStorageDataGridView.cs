﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class VirtualPartsStockHistoricalStorageDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Parts_InboundType"
        };

        public VirtualPartsStockHistoricalStorageDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseCode
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseName
                    },new ColumnItem {
                        Name = "BillCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCode
                    },new KeyValuesColumnItem {
                        Name = "BillBusinessType",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_InboundType,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    },new ColumnItem {
                        Name = "CounterpartCompanyCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyCode
                    },new ColumnItem {
                        Name = "CounterpartCompanyName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_CounterpartCompanyName
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartCode
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryDetails_SparePartName
                    }
                    ,new ColumnItem {
                        Name = "WarehouseAreaCode",
                        Title = PartsStockingUIStrings.QueryPanel_Title_WarehouseArea2
                    }
                    ,new ColumnItem {
                        Name = "WholesalePrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsPlannedPrice
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_QuantityNumber
                    },new ColumnItem {
                        Name = "CreateTime",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OutboundAndInboundBill_BillCreateTime
                    },new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_OverstockPartsApp_BranchName
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                var fileitem = filters.Filters.LastOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                switch(parameterName) {
                    case "sparePartId":
                        return fileitem.Filters.Single(item => item.MemberName == "SparePartId").Value;
                    case "warehouseId":
                        return fileitem.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                    case "warehouseAreaCode":
                        return null;
                    case "billBusinessType":
                        return "入库";
                    case "partsSalesOrderTypeId":
                        return null;
                    case "partsSalesCategoryId":
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var filters = compositeFilterItem.Filters;
                foreach(var item in filters) {
                    var filterItem = item as CompositeFilterItem;
                    if(filterItem != null)
                        foreach(var timefilter in filterItem.Filters.Where(filter => filter.MemberName == "CreateTime"))
                            newCompositeFilterItem.Filters.Add(timefilter);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualOutboundAndInboundBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询历史出入库记录";
        }
    }
}
