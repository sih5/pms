﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class VirtualWarehousePartsStockDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseCode
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseName
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartCode
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartName
                   },new ColumnItem {
                        Name = "UsableQuantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_UsableQuantity
                    },new ColumnItem {
                        Name = "MInPackingAmount",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_MInPackingAmount
                    },new ColumnItem {
                       Name = "PartsSalesCategoryName",
                       Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_CompanyPartsStock_PartsSalesCategoryName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WarehousePartsStock);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        protected override string OnRequestQueryName() {
            return "调拨查询仓库库存";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "warehouseId":
                        return filters.Filters.Single(item => item.MemberName == "WarehouseId").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
