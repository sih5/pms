﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehouseAreaDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Kind", "Area_Category"
        };

        public WarehouseAreaDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Warehouse.Name"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseArea_ParentWarehouseAreaCode,
                        Name = "ParentWarehouseArea.Code"
                    }, new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseArea_ParentWarehouseAreaAreaKind,
                        Name = "ParentWarehouseArea.AreaKind",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code"
                    }, new KeyValuesColumnItem {
                        Name = "AreaKind",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Name = "WarehouseAreaCategory.Category",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WarehouseArea);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetWarehouseAreasWithAreaManagers";
        }

        // 外部传入ID参数为 PersonnelId 
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var personnelId = compositeFilterItem.Filters.FirstOrDefault(filter => filter.MemberName == "PersonnelId");
                if(personnelId != null) {
                    switch(parameterName) {
                        case "personnelId":
                            return personnelId.Value;
                    }
                }
            }
            return null;
        }

        protected override Telerik.Windows.Data.IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null)
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => filter.MemberName != "PersonnelId"))
                    newCompositeFilterItem.Filters.Add(filterItem);
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
