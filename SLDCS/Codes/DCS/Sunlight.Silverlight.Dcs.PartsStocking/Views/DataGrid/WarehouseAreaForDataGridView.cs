﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehouseAreaForDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Kind","Area_Category"
        };

        protected override Type EntityType {
            get {
                return typeof(VirtualWarehouseArea);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        public WarehouseAreaForDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseArea_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_WarehouseName,
                        Name = "WarehouseName"
                    }, new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseArea_AreaKind,
                        Name = "AreaKind",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_WarehouseAreaCategoryCategory,
                        Name = "AreaCategory",
                          KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    }
                };
            }
        }
        // 外部传入ID参数为 PersonnelId 
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var personnelId = compositeFilterItem.Filters.FirstOrDefault(filter => filter.MemberName == "ParentId");
                if(personnelId != null) {
                    switch(parameterName) {
                        case "parentId":
                            return personnelId.Value;
                    }
                }
            }
            return null;
        }

        protected override Telerik.Windows.Data.IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var newCompositeFilterItem = new CompositeFilterItem();
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "GetWarehouseAreasByParentIdIsPosition";
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
