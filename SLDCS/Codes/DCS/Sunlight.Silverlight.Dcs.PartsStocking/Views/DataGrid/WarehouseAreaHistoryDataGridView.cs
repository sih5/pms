﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehouseAreaHistoryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Category"
        };

        public WarehouseAreaHistoryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Warehouse.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_WarehouseName
                    }, new KeyValuesColumnItem {
                        Name = "WarehouseAreaCategory.Category",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_WarehouseAreaCategoryCategory
                    }, new ColumnItem {
                        Name = "WarehouseArea.Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_OriginalWarehouseAreaCode
                    }, new ColumnItem {
                        Name = "WarehouseArea1.Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_TargetWarehouseAreaCode
                    }, new ColumnItem {
                        Name = "SparePart.Code",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePart.Name",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_SparePartName
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_Quantity
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            //((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }

        protected override string OnRequestQueryName() {
            return "GetWarehouseAreaHistoryWithWarehouseDetailWarehouseAreaCategory";
        }

        protected override Type EntityType {
            get {
                return typeof(WarehouseAreaHistory);
            }
        }
    }
}
