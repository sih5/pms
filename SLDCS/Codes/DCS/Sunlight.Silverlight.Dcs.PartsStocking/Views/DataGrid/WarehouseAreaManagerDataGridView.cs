﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehouseAreaManagerDataGridView : DcsDataGridViewBase {
        public WarehouseAreaManagerDataGridView() {
            this.DataContextChanged += this.WarehouseAreaManagerDataGridView_DataContextChanged;
        }

        private void WarehouseAreaManagerDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var warehouseArea = e.NewValue as WarehouseArea;
            if(warehouseArea == null || warehouseArea.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "WarehouseAreaId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = warehouseArea.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Personnel.LoginId"
                    }, new ColumnItem {
                        Name = "Personnel.Name"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WarehouseAreaManager);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetWarehouseAreaManagerWithPersonnel";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }
    }
}
