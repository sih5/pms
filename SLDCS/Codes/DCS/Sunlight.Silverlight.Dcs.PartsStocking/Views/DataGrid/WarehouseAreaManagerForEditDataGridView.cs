﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehouseAreaManagerForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase personnelDropDownQueryWindow;

        private DcsMultiPopupsQueryWindowBase PersonnelDropDownQueryWindow {
            get {
                if(this.personnelDropDownQueryWindow == null) {
                    this.personnelDropDownQueryWindow = DI.GetQueryWindow("PersonnelForMultiSelect") as DcsMultiPopupsQueryWindowBase;
                    this.personnelDropDownQueryWindow.SelectionDecided += this.PersonnelDropDownQueryWindow_SelectionDecided;
                    this.personnelDropDownQueryWindow.Loaded += this.PersonnelDropDownQueryWindow_Loaded;
                }
                return this.personnelDropDownQueryWindow;
            }
        }
        private RadWindow radQueryWindow;

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = PersonnelDropDownQueryWindow,
                    Header = PartsStockingUIStrings.Action_Title_PersonnelSelection,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }
        private void PersonnelDropDownQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null)
                return;
            var warehouseArea = this.DataContext as WarehouseArea;
            var filterItem = new FilterItem();
            filterItem.MemberName = "CorporationId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            if(warehouseArea != null) {
                var warehouse = warehouseArea.Warehouse;
                if(warehouse == null)
                    filterItem.Value = warehouseArea.StorageCompanyId;
                else
                    filterItem.Value = warehouseArea.Warehouse.StorageCompanyId;

            }
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs eventArgs) {
            var warehouseAreaManager = eventArgs.Row.DataContext as WarehouseAreaManager;
            if(warehouseAreaManager == null)
                return;
            if(warehouseAreaManager.EntityState == EntityState.New || warehouseAreaManager.EntityState == EntityState.Detached)
                return;
            if(eventArgs.Cell.Column.UniqueName == "Personnel.LoginId")
                eventArgs.Cancel = true;
        }

        private void PersonnelDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            var warehouseArea = this.DataContext as WarehouseArea;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var personnels = queryWindow.SelectedEntities.Cast<Personnel>();
            //以后肯定要过滤重复，下面的可能有用
            foreach(var per in personnels) {
                if(warehouseArea.WarehouseAreaManagers.Any(r => r.Personnel.Id == per.Id)) {
                    UIHelper.ShowAlertMessage(string.Format(PartsStockingUIStrings.DataGridView_Validation_WarehouseAreaManager_PersonnelIsThere, per.Name));
                    return;
                }
            }

            var domainContext = this.DomainContext as DcsDomainContext;
            foreach(var per in personnels) {
                domainContext.Load(domainContext.GetPersonnelsQuery().Where(d => d.Id == per.Id), loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var warehouseAreaManager = new WarehouseAreaManager();
                    warehouseAreaManager.Personnel = loadOp.Entities.SingleOrDefault();
                    if(warehouseArea != null) {

                        warehouseArea.WarehouseAreaManagers.Add(warehouseAreaManager);
                    }
                }, null);
            }
            //var parent = queryWindow.ParentOfType<RadWindow>();
            //if(parent != null)
            //    parent.Close();
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            foreach(var item in e.Items.Cast<WarehouseAreaManager>().Where(item => domainContext.WarehouseAreaManagers.Contains(item)))
                domainContext.WarehouseAreaManagers.Remove(item);
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var warehouseArea = this.DataContext as WarehouseArea;
            if(warehouseArea == null || !warehouseArea.TopLevelWarehouseAreaId.HasValue) {
                this.RadQueryWindow.ShowDialog();
                return;
            }
            UIHelper.ShowNotification(PartsStockingUIStrings.DataGridView_Notification_Common_OnlyTopWarehouseAreaCanAddPerson);

        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
            this.GridView.Deleted += this.GridView_Deleted;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("WarehouseAreaManagers");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Personnel.LoginId",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Personnel.Name",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WarehouseAreaManager);
            }
        }
    }
}
