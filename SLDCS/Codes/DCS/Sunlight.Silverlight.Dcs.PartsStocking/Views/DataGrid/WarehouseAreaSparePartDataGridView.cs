﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehouseAreaSparePartDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Kind", "Area_Category"
        };

        public WarehouseAreaSparePartDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseName",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_WarehouseName
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseArea_ParentWarehouseAreaCode,
                        Name = "ParentWarehouseAreaCode"
                    }, new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseArea_ParentWarehouseAreaAreaKind,
                        Name = "ParentWarehouseAreaAreaKind",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseArea_Code,
                        Name = "Code"
                    }, new KeyValuesColumnItem {
                         Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseArea_AreaKind,
                        Name = "AreaKind",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseArea_AreaCategory,
                        Name = "AreaCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem{
                        Name="PartCode",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_SparePartCode
                    },new ColumnItem{
                        Name="PartName",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaHistory_SparePartName
                    }, new ColumnItem {
                        Name = "Remark",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_Remark
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreatorName
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInventoryBill_CreateTime
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifierName
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        Title= PartsStockingUIStrings.DataGridView_ColumnItem_Title_PartsInboundPlan_ModifyTime
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualWarehouseAreaToQuery);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询库区负责人库区库位带配件信息";
        }

        // 外部传入ID参数为 PersonnelId 
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var personnelId = compositeFilterItem.Filters.FirstOrDefault(filter => filter.MemberName == "PersonnelId");
                if(personnelId != null) {
                    switch(parameterName) {
                        case "personnelId":
                            return personnelId.Value;
                    }
                }
            }
            return null;
        }

        protected override Telerik.Windows.Data.IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null)
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => filter.MemberName != "PersonnelId")) {
                    if(filterItem.MemberName == "Warehouse.Name") {
                        newCompositeFilterItem.Filters.Add(new FilterItem("WarehouseName", typeof(string), FilterOperator.IsEqualTo, filterItem.Value));
                        continue;
                    }
                    if(filterItem.MemberName == "WarehouseAreaCategory.Category") {
                        newCompositeFilterItem.Filters.Add(new FilterItem("AreaCategory", typeof(int), FilterOperator.IsEqualTo, filterItem.Value));
                        continue;
                    }
                    newCompositeFilterItem.Filters.Add(filterItem);
                }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
