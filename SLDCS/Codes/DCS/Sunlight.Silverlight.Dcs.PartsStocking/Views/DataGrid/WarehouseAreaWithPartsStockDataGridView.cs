﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehouseAreaWithPartsStockDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Kind", "Area_Category"
        };

        public WarehouseAreaWithPartsStockDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaWithPartsStock_AreaCategoryId,
                        Name = "AreaCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "AreaKind",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "WarehouseAreaCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SparePartName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehouseAreaWithPartsStock_Quantity,
                        Name = "Quantity",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WarehouseAreaWithPartsStock);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 100;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["Quantity"]).DataFormatString = "d";
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("WarehouseAreaWithPartsStocks");
        }



    }
}
