﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehouseDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status", "Storage_Center","Warehouse_Type"
        };

        public WarehouseDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(Warehouse);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name"
                    }, new KeyValuesColumnItem {
                        Name = "StorageCenter",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "IsQualityWarehouse",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Warehouse_IsQualityWarehouse
                    }, new KeyValuesColumnItem{
                        Name="Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Name = "Address"
                    }, new ColumnItem {
                        Name = "PhoneNumber"
                    }, new ColumnItem {
                        Name = "Contact"
                    }, new ColumnItem {
                        Name = "Fax"
                    }, new ColumnItem {
                        Name = "Email"
                    }, new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem{
                        Name="WmsInterface"
                    },new ColumnItem{
                        Name="PwmsInterface",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Warehouse_PwmsInterface
                    },  new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "IfSync"
                    }, new ColumnItem {
                        Name = "Branch.Name"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetWarehousesWithBranch";
        }
    }
}
