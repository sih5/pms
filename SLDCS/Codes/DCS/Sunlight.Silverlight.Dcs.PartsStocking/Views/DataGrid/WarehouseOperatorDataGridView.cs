﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehouseOperatorDataGridView : DcsDataGridViewBase {
        public WarehouseOperatorDataGridView() {
            this.DataContextChanged += this.WarehouseOperatorDataGridView_DataContextChanged;
        }

        private void WarehouseOperatorDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var warehouse = e.NewValue as Warehouse;
            if(warehouse == null || warehouse.Id == default(int))
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "WarehouseId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = warehouse.Id;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(WarehouseOperator);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Personnel.LoginId"
                    }, new ColumnItem {
                        Name = "Personnel.Name"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetWarehouseOperatorsWithPersonnel";
        }
    }
}
