﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehouseOperatorForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase personnelDropDownQueryWindow;

        private QueryWindowBase PersonnelDropDownQueryWindow {
            get {
                if(this.personnelDropDownQueryWindow == null) {
                    this.personnelDropDownQueryWindow = DI.GetQueryWindow("PersonnelDropDown");
                    this.personnelDropDownQueryWindow.SelectionDecided += this.PersonnelDropDownQueryWindow_SelectionDecided;
                    this.personnelDropDownQueryWindow.Loaded += this.PersonnelDropDownQueryWindow_Loaded;
                }
                return this.personnelDropDownQueryWindow;
            }
        }

        private void PersonnelDropDownQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var warehouse = this.DataContext as Warehouse;
            var filterItem = new FilterItem();
            filterItem.MemberName = "CorporationId";
            filterItem.MemberType = typeof(int);
            filterItem.Operator = FilterOperator.IsEqualTo;
            if(warehouse != null)
                filterItem.Value = warehouse.StorageCompanyId;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void PersonnelDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var personnel = queryWindow.SelectedEntities.Cast<Personnel>().FirstOrDefault();
            var warehouseOperator = queryWindow.DataContext as WarehouseOperator;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(warehouseOperator == null || personnel == null || domainContext == null)
                return;
            domainContext.Load(domainContext.GetPersonnelsQuery().Where(d => d.Id == personnel.Id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                warehouseOperator.Personnel = loadOp.Entities.SingleOrDefault();
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var warehouseOperator = e.Row.DataContext as WarehouseOperator;
            if(warehouseOperator == null)
                return;
            if(warehouseOperator.EntityState == EntityState.New || warehouseOperator.EntityState == EntityState.Detached)
                return;
            if(e.Cell.Column.UniqueName == "Personnel.CorporationId")
                e.Cancel = true;
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            foreach(var item in e.Items.Cast<WarehouseOperator>().Where(item => domainContext.WarehouseOperators.Contains(item)))
                domainContext.WarehouseOperators.Remove(item);
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Deleted += this.GridView_Deleted;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("WarehouseOperators");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "Personnel.LoginId",
                        DropDownContent = this.PersonnelDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "Personnel.Name",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WarehouseOperator);
            }
        }
    }
}
