﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehousePartsStockLibraryDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "WarehouseCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseCode
                    },new ColumnItem {
                        Name = "WarehouseName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_WarehouseName
                    }, new ColumnItem {
                        Name = "SparePartCode",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_SparePartName
                    }, new ColumnItem {
                        Name = "UsableQuantity",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_UsableQuantity
                    }, new ColumnItem {
                        Name = "PartsSalesPrice",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_WarehousePartsStock_PartsSalesPrice
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_VirtualPartsStock_BranchName
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            ((GridViewDataColumn)this.GridView.Columns["UsableQuantity"]).DataFormatString = "d";
        }

        protected override Type EntityType {
            get {
                return typeof(WarehousePartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询仓库库存";
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            if(FilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null) {
                compositeFilterItem = new CompositeFilterItem();
                compositeFilterItem.Filters.Add(this.FilterItem);
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                IsExact = true,
                MemberName = "PartsSalesCategoryId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).SingleOrDefault(item => item.MemberName == "PartsSalesCategoryId").Value
            });
            return compositeFilterItem.ToFilterDescriptor();
        }


        protected override object OnRequestQueryParameter(string queryname, string parametername) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parametername) {
                    case "storageCompanyId":
                        var filtersStorageCompany = filters.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).SingleOrDefault(item => item.MemberName == "StorageCompanyId");
                        if(filtersStorageCompany != null)
                            return filtersStorageCompany.Value;
                        return null;
                    case "warehouseId":
                        var filtersWarehouse = filters.Filters.SingleOrDefault(item => item.MemberName == "WarehouseId");
                        if(filtersWarehouse != null)
                            return filtersWarehouse.Value;
                        return null;
                    case "partIds":
                        return null;
                }
            }
            return null;
        }
    }
}
