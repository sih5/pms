﻿using System;
using System.Linq;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.DataGrid {
    public class WarehouseWithOperatorsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status", "Storage_Center","Warehouse_Type"
        };

        public WarehouseWithOperatorsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(Warehouse);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },  new ColumnItem {
                        Name = "Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "Name"
                    }, new KeyValuesColumnItem {
                        Name = "StorageCenter",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "IsQualityWarehouse",
                        Title = PartsStockingUIStrings.DataGridView_ColumnItem_Title_Warehouse_IsQualityWarehouse
                    },new ColumnItem {
                        Name = "IsAutoApp",
                        Title = "是否自动审单"
                    }, new KeyValuesColumnItem{
                        Name="Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Name = "Address"
                    }, new ColumnItem {
                        Name = "PhoneNumber"
                    }, new ColumnItem {
                        Name = "Contact"
                    }, new ColumnItem {
                        Name = "Fax"
                    }, new ColumnItem {
                        Name = "Email"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "Branch.Name"
                    },new ColumnItem {
                        Name = "IsSales",
                        Title = "是否参与智能订货(销售部分)"
                    },new ColumnItem {
                        Name = "IsPurchase",
                        Title = "是否参与智能订货(采购部分)"
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            switch(parameterName) {
                case "name":
                    var filterItem2 = filter.Filters.SingleOrDefault(item => item.MemberName == "OperatorName");
                    if(filterItem2 == null)
                        break;
                    return filterItem2.Value;
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "OperatorName"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "WarehouseOperator"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetWarehousesByOperatorName";
        }
    }
}
