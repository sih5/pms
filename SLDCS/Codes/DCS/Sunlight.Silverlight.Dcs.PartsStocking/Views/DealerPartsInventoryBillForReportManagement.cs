﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "WarehouseExecution", "DealerPartsInventoryBillForReport", ActionPanelKeys = new[] {
        CommonActionKeys.APPROVE_ABANDON_IMPORT_EXPORT,"DealerPartsInventoryBill"
    })]
    public class DealerPartsInventoryBillForReportManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase importDataEditView;
        private const string DATA_IMPORT_VIEW = "_DataImportView_";
        private ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataEditViewBase uploadDataEditView;
        protected const string Upload_DATA_EDIT_VIEW = "_UploadDataEditView_";

        public DealerPartsInventoryBillForReportManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_DealerPartsInventoryBillForReport;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_IMPORT_VIEW, () => this.ImportDataEditView);
            this.RegisterView(Upload_DATA_EDIT_VIEW, () => this.UploadDataEditView);
        }

        public DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null)
                    this.dataGridView = DI.GetDataGridView("DealerPartsInventoryBill");
                return this.dataGridView;
            }
        }

        //附件上传 
        private DataEditViewBase UploadDataEditView 
        { 
            get 
            { 
                if (this.uploadDataEditView == null) 
                { 
                    this.uploadDataEditView = DI.GetDataEditView("DealerPartsInventoryBillForUpload"); 
                    this.uploadDataEditView.EditSubmitted += this.DataEditView_EditSubmitted; 
                    this.uploadDataEditView.EditCancelled += this.DataEditView_EditCancelled; 
                } 
                return this.uploadDataEditView; 
            } 
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] { 
                    "DealerPartsInventoryBill"
                };
            }
        }

        private DataEditViewBase ImportDataEditView {
            get {
                if(this.importDataEditView == null) {
                    this.importDataEditView = DI.GetDataEditView("DealerPartsInventoryBillForReportImport");
                    this.importDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.importDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.importDataEditView;
            }
        }
        private void ResetEditView() {
            this.importDataEditView = null;
            this.uploadDataEditView = null; 
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.IMPORT:
                    var dealerPartsInventoryBill = this.ImportDataEditView.CreateObjectToEdit<DealerPartsInventoryBill>();
                    dealerPartsInventoryBill.StorageCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    dealerPartsInventoryBill.StorageCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    dealerPartsInventoryBill.StorageCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    dealerPartsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.新建;
                    dealerPartsInventoryBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    this.SwitchViewTo(DATA_IMPORT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                     ShellViewModel.Current.IsBusy = true;
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().Select(e => e.Id).ToArray();
                        this.导出服务站配件盘点提报(ids, null, null, null, null, null, null, null,null,null);
                    } else
                    {
                        var filterItemExport = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItemExport == null)
                            return;
                        var codeExport = filterItemExport.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var salesCategoryIdExport= filterItemExport.Filters.Single(r => r.MemberName == "SalesCategoryId").Value as int?;
                        var storageCompanyCodeExport = filterItemExport.Filters.Single(r => r.MemberName == "StorageCompanyCode").Value as string;
                        var storageCompanyNameExport = filterItemExport.Filters.Single(r => r.MemberName == "StorageCompanyName").Value as string;
                        var statusExport = filterItemExport.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        foreach(var filter in filterItemExport.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }                                
                            }
                        }

                        this.导出服务站配件盘点提报(null, null, BaseApp.Current.CurrentUserData.EnterpriseId, codeExport, storageCompanyCodeExport, storageCompanyNameExport, salesCategoryIdExport, statusExport, createTimeBegin, createTimeEnd);
                    }
                    break;
                case CommonActionKeys.APPROVE:
                    var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    if(domainContext == null)
                        return;
                    var entities = this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().ToArray();
                    if(entities == null)
                        return;
                    try {
                        foreach(var entity in entities) {
                            if(entity.Can盘点单审核)
                                entity.盘点单审核();
                        }
                        domainContext.SubmitChanges(submitOp => {
                            if(submitOp.HasError) {
                                if(!submitOp.IsErrorHandled)
                                    submitOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                domainContext.RejectChanges();
                                return;
                            }
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_Complete);
                            this.CheckActionsCanExecute();
                        }, null);
                    } catch(Exception ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                    }
                    break;
                case CommonActionKeys.ABANDON:
                    var domainContextAbandon = this.DataGridView.DomainContext as DcsDomainContext;
                    if(domainContextAbandon == null)
                        return;
                    var entityAbandon = this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().FirstOrDefault();
                    if(entityAbandon == null)
                        return;
                    try {
                        if(entityAbandon.Can盘点单作废)
                            entityAbandon.盘点单作废();
                        domainContextAbandon.SubmitChanges(submitOp => {
                            if(submitOp.HasError) {
                                if(!submitOp.IsErrorHandled)
                                    submitOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                domainContextAbandon.RejectChanges();
                                return;
                            }
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                            this.CheckActionsCanExecute();
                        }, null);
                    } catch(Exception ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                    }
                    break;
                case "DetailExport":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var salesCategoryId = filterItem.Filters.Single(r => r.MemberName == "SalesCategoryId").Value as int?;
                    var storageCompanyCode = filterItem.Filters.Single(r => r.MemberName == "StorageCompanyCode").Value as string;
                    var storageCompanyName = filterItem.Filters.Single(r => r.MemberName == "StorageCompanyName").Value as string;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                     DateTime? createTimeBegin2 = null;
                     DateTime? createTimeEnd2 = null;
                     foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin2 = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd2 = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }                                
                            }
                        }
                    ShellViewModel.Current.IsBusy = true;
                    int[] Ids = new int[] { };
                    if(this.DataGridView.SelectedEntities != null) {
                        var dealerPartsInventoryBills = this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>();
                        Ids = dealerPartsInventoryBills.Select(r => r.Id).ToArray();
                    }
                    this.ExportDealerPartsInventoryBillWithDetail(Ids, null, BaseApp.Current.CurrentUserData.EnterpriseId, code, storageCompanyCode, storageCompanyName, salesCategoryId, status, createTimeBegin2, createTimeEnd2);
                    break;
                case CommonActionKeys.UPLOAD://附件上传 
                    this.UploadDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().Id); 
                    this.SwitchViewTo(Upload_DATA_EDIT_VIEW); 
                    break; 
                default:
                    break;
            }
        }

        private void 导出服务站配件盘点提报(int[] ids, int? branchId, int? storageCompanyId, string code, string storageCompanyCode, string storageCompanyName, int? salesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.导出服务站配件盘点提报Async(ids, branchId, storageCompanyId, code, storageCompanyCode, storageCompanyName, salesCategoryId, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.导出服务站配件盘点提报Completed -= excelServiceClient_导出服务站配件盘点提报Completed;
            this.excelServiceClient.导出服务站配件盘点提报Completed += excelServiceClient_导出服务站配件盘点提报Completed;
        }

        private void excelServiceClient_导出服务站配件盘点提报Completed(object sender, 导出服务站配件盘点提报CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void ExportDealerPartsInventoryBillWithDetail(int[] ids, int? branchId, int? storageCompanyId, string code, string storageCompanyCode, string storageCompanyName, int? salesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportDealerPartsInventoryBillAsync(ids, branchId, storageCompanyId, code, storageCompanyCode, storageCompanyName, salesCategoryId, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportDealerPartsInventoryBillCompleted -= excelServiceClient_ExportDealerPartsInventoryBillCompleted;
            this.excelServiceClient.ExportDealerPartsInventoryBillCompleted += excelServiceClient_ExportDealerPartsInventoryBillCompleted;
        }

        void excelServiceClient_ExportDealerPartsInventoryBillCompleted(object sender, ExportDealerPartsInventoryBillCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return true;
                case "DetailExport":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.UPLOAD: 
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any()) 
                        return false; 
                    return true; 
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "StorageCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

    }
}
