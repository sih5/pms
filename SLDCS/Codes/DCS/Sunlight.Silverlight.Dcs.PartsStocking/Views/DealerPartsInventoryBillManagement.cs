﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit; 

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsSales", "PartsOuterPurchase", "DealerPartsInventoryBill", ActionPanelKeys = new[] {
        CommonActionKeys.INITIALAPPROVE_APPROVE_AUDIT_ABANDON_EXPORT_REJECT_MERGEEXPORT_PRINT,"DealerPartsInventoryBill"
    })]
    public class DealerPartsInventoryBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataInitialApproveView;
        private DataEditViewBase dataFinalApproveView;
        private const string DATA_APPROVE_VIEW = "_DataApproveView_";
        private const string DATA_INITIALAPPROVE_VIEW = "_DataInitialApproveView_";
        private const string DATA_FINALAPPROVE_VIEW = "_DataFinalApproveView_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public DealerPartsInventoryBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_DealerPartsInventoryBill;
        }

        //作废弹出
        private RadWindow abandonRadWindow;
        private RadWindow AbandonRadWindow {
            get {
                if(this.abandonRadWindow == null) {
                    this.abandonRadWindow = new RadWindow();
                    this.abandonRadWindow.CanClose = false;
                    this.abandonRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.abandonRadWindow.Content = this.AbandonDataEditView;
                    this.abandonRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.abandonRadWindow.Height = 200;
                    this.abandonRadWindow.Width = 400;
                    this.abandonRadWindow.Header = "";
                }
                return this.abandonRadWindow;
            }
        }

        private DataEditViewBase abandonDataEditView;

        private DataEditViewBase AbandonDataEditView {
            get {
                if(this.abandonDataEditView == null) {
                    this.abandonDataEditView = DI.GetDataEditView("DealerPartsInventoryBillForAbandon");
                    this.abandonDataEditView.EditCancelled += abandonDataEditView_EditCancelled;
                    this.abandonDataEditView.EditSubmitted += abandonDataEditView_EditSubmitted;
                }
                return this.abandonDataEditView;
            }
        }

        void abandonDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.AbandonRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        void abandonDataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.AbandonRadWindow.Close();
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_INITIALAPPROVE_VIEW, () => this.DataInitialApproveView);
            this.RegisterView(DATA_FINALAPPROVE_VIEW, () => this.DataFinalApproveView);
        }

        public DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null)
                    this.dataGridView = DI.GetDataGridView("DealerPartsInventoryBillBranch");
                return this.dataGridView;
            }
        }

        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("DealerPartsInventoryBillForApprove");
                    ((DealerPartsInventoryBillForApproveDataEditView)this.dataApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

         private DataEditViewBase DataInitialApproveView {
            get {
                if(this.dataInitialApproveView == null) {
                    this.dataInitialApproveView = DI.GetDataEditView("DealerPartsInventoryBillForInitialApprove");
                    ((DealerPartsInventoryBillForInitialApproveDataEditView)this.dataInitialApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataInitialApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataInitialApproveView;
            }
        }

        private DataEditViewBase DataFinalApproveView {
            get {
                if(this.dataFinalApproveView == null) {
                    this.dataFinalApproveView = DI.GetDataEditView("DealerPartsInventoryBillForFinalApprove");
                    ((DealerPartsInventoryBillForFinalApproveDataEditView)this.dataFinalApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataFinalApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataFinalApproveView;
            }
        }
        private void ResetEditView() {
            this.dataApproveView = null;
            this.dataInitialApproveView = null;
            this.dataFinalApproveView = null;
            this.abandonDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }


        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] { 
                    "DealerPartsInventoryBillForBranch"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            switch(uniqueId) {
                case CommonActionKeys.AUDIT:
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    //try {
                    //    ShellViewModel.Current.IsBusy = true;
                    //    foreach(var entity in entities) {
                    //        if(entity.Can盘点单审核)
                    //            entity.盘点单审核();
                    //    }
                    //    domainContext.SubmitChanges(submitOp => {
                    //        if(submitOp.HasError) {
                    //            if(!submitOp.IsErrorHandled)
                    //                submitOp.MarkErrorAsHandled();
                    //            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    //            domainContext.RejectChanges();
                    //            ShellViewModel.Current.IsBusy = false;
                    //            return;
                    //        }
                    //        this.CheckActionsCanExecute();
                    //        ShellViewModel.Current.IsBusy = false;
                    //        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_Complete);
                    //    }, null);
                    //} catch(Exception ex) {
                    //    ShellViewModel.Current.IsBusy = false;
                    //    UIHelper.ShowAlertMessage(ex.Message);
                    //}
                    break;
                case CommonActionKeys.INITIALAPPROVE:
                    this.DataInitialApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().GetIdentity());
                    this.SwitchViewTo(DATA_INITIALAPPROVE_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataFinalApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().GetIdentity());
                    this.SwitchViewTo(DATA_FINALAPPROVE_VIEW);
                    break;
                //高级审核
                case "AdvancedApprove":
                    this.DataFinalApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().GetIdentity());
                    this.SwitchViewTo(DATA_FINALAPPROVE_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    this.AbandonDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().Id);
                    this.AbandonRadWindow.ShowDialog();
                    break;
                case "DetailExport":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    //var salesCategoryId = filterItem.Filters.Single(r => r.MemberName == "SalesCategoryId").Value as int?;
                    var storageCompanyCode = filterItem.Filters.Single(r => r.MemberName == "StorageCompanyCode").Value as string;
                    var storageCompanyName = filterItem.Filters.Single(r => r.MemberName == "StorageCompanyName").Value as string;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    var amountDifferenceRange = filterItem.Filters.Single(r => r.MemberName == "AmountDifferenceRange").Value as int?;
                    var agencyName = filterItem.Filters.Single(r => r.MemberName == "AgencyName").Value as string;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    int[] Ids = new int[] { };
                    if(this.DataGridView.SelectedEntities != null) {
                        var dealerPartsInventoryBills = this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>();
                        Ids = dealerPartsInventoryBills.Select(r => r.Id).ToArray();
                    }
                    this.ExportPartsPurchaseOrderWithDetail(Ids, BaseApp.Current.CurrentUserData.EnterpriseId, code, storageCompanyCode, storageCompanyName, null, status, createTimeBegin, createTimeEnd, amountDifferenceRange, agencyName);
                    break;
                case CommonActionKeys.SENIORAPPROVE:
                    this.DataFinalApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().GetIdentity());
                    this.SwitchViewTo(DATA_FINALAPPROVE_VIEW);
                    break;
                default:
                    break;
            }
        }

        private void ExportPartsPurchaseOrderWithDetail(int[] ids, int? branchId, string code, string storageCompanyCode, string storageCompanyName, int? salesCategoryId, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? amountDifferenceRange, string agencyName) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.导出经销商配件盘点单主单清单Async(ids, branchId, code, storageCompanyCode, storageCompanyName, salesCategoryId, status, createTimeBegin, createTimeEnd, amountDifferenceRange, agencyName);
            this.excelServiceClient.导出经销商配件盘点单主单清单Completed -= excelServiceClient_ExportDealerPartsInventoryBillCompleted;
            this.excelServiceClient.导出经销商配件盘点单主单清单Completed += excelServiceClient_ExportDealerPartsInventoryBillCompleted;
        }

        private void excelServiceClient_ExportDealerPartsInventoryBillCompleted(object sender, 导出经销商配件盘点单主单清单CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
               
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().Status == (int)DcsDealerPartsInventoryBillStatus.新建;
                case CommonActionKeys.AUDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().Status == (int)DcsDealerPartsInventoryBillStatus.初审通过;
                case CommonActionKeys.INITIALAPPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().Status == (int)DcsDealerPartsInventoryBillStatus.新建;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().Status == (int)DcsDealerPartsInventoryBillStatus.审核通过;
                case "AdvancedApprove":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().Status == (int)DcsDealerPartsInventoryBillStatus.审批通过;
                case "DetailExport":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.SENIORAPPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<DealerPartsInventoryBill>().First().Status == (int)DcsDealerPartsInventoryBillStatus.高级审核通过;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;
            //newCompositeFilterItem.Filters.Add(new FilterItem {
            //    MemberName = "BranchId",
            //    MemberType = typeof(int),
            //    Operator = FilterOperator.IsEqualTo,
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId
            //});
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
