﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsStore", "DealerPartsStockQueryView", ActionPanelKeys = new[] { CommonActionKeys.EXPORT })]
    public class DealerPartsStockQueryViewManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public DealerPartsStockQueryViewManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.QueryPanel_Title_DealerPartsStockQueryView;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerPartsStockQueryView"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerPartsStockQueryView"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var partsSalesCategoryIds = this.DataGridView.SelectedEntities.Cast<DealerPartsStockQueryView>().Select(r => r.PartsSalesCategoryId).Distinct().ToArray();
                        var dealerCodes = this.DataGridView.SelectedEntities.Cast<DealerPartsStockQueryView>().Select(r => r.DealerCode).Distinct().ToArray();
                        var partCodes = this.DataGridView.SelectedEntities.Cast<DealerPartsStockQueryView>().Select(r => r.PartCode).Distinct().ToArray();
                        this.ExportDealerPartsStockQueryView(partsSalesCategoryIds, dealerCodes, partCodes, null, null, null, null);
                    } else {
                        var dealerCode = filterItem.Filters.Single(r => r.MemberName == "DealerCode").Value as string;
                        var partCode = filterItem.Filters.Single(r => r.MemberName == "PartCode").Value as string;
                        var partsSalesCategoryId = filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var isStoreMoreThanZero = filterItem.Filters.Single(r => r.MemberName == "IsStoreMoreThanZero").Value as bool?;
                        this.ExportDealerPartsStockQueryView(null, null, null, partsSalesCategoryId, dealerCode, partCode, isStoreMoreThanZero);
                    }
                    break;
            }
        }

        private void ExportDealerPartsStockQueryView(int[] partsSalesCategoryIds, string[] dealerCodes, string[] partCodes, int? partsSalesCategoryId, string dealerCode, string partCode, bool? isStoreMoreThanZero) {
            this.excelServiceClient.ExportDealerPartsStockQueryViewAsync(partsSalesCategoryIds, dealerCodes, partCodes, BaseApp.Current.CurrentUserData.EnterpriseId, partsSalesCategoryId, dealerCode, partCode, isStoreMoreThanZero);
            this.excelServiceClient.ExportDealerPartsStockQueryViewCompleted -= excelServiceClient_ExportDealerPartsStockQueryViewCompleted;
            this.excelServiceClient.ExportDealerPartsStockQueryViewCompleted += excelServiceClient_ExportDealerPartsStockQueryViewCompleted;
        }

        private void excelServiceClient_ExportDealerPartsStockQueryViewCompleted(object sender, ExportDealerPartsStockQueryViewCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            if (!string.IsNullOrEmpty(e.errmesage)) {
                UIHelper.ShowNotification(e.errmesage);
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null || !compositeFilterItem.Filters.Any(r => r.MemberName == "PartsSalesCategoryId" && r.Value != null)) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_CategoryIsNull);
                return;
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}