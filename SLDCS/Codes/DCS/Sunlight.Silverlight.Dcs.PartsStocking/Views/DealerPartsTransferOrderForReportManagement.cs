﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "Accessories", "DealerPartsTransferOrderForReport", ActionPanelKeys = new[] {
        "DealerPartsTransferOrderForReport"
    })]
    public class DealerPartsTransferOrderForReportManagement : DcsDataManagementViewBase {
        public DealerPartsTransferOrderForReportManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_DealerPartsTransferOrderForReport;
        }

        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerPartsTransferOrderForReport"
                };
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var dealerPartsTransferOrder = this.DataEditView.CreateObjectToEdit<DealerPartsTransferOrder>();
                    dealerPartsTransferOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    dealerPartsTransferOrder.DealerId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    dealerPartsTransferOrder.DealerCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    dealerPartsTransferOrder.DealerName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    dealerPartsTransferOrder.Status = (int)DcsDealerPartsTransferOrderStatus.新增;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<DealerPartsTransferOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废服务站配件调拨单)
                                entity.作废服务站配件调拨单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<DealerPartsTransferOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交服务站配件调拨单)
                                entity.提交服务站配件调拨单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<DealerPartsTransferOrder>().Select(r => r.Id).ToArray();
                        this.ExportDealerPartsTransferOrder(ids, null, null, null, null, null, null, uniqueId);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var noWarrantyBrandId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "NoWarrantyBrandId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "NoWarrantyBrandId").Value as int?;
                        var warrantyBrandId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarrantyBrandId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarrantyBrandId").Value as int?;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportDealerPartsTransferOrder(null, noWarrantyBrandId, warrantyBrandId, code, status, createTimeBegin, createTimeEnd, uniqueId);
                    }
                    break;
            }
        }


        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public void ExportDealerPartsTransferOrder(int[] ids, int? noWarrantyBrandId, int? warrantyBrandId, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string uniqueId) {
            if(uniqueId == CommonActionKeys.EXPORT) {
                this.excelServiceClient.ExportDealerPartsTransferOrderAsync(ids, BaseApp.Current.CurrentUserData.EnterpriseId, noWarrantyBrandId, warrantyBrandId, code, status, createTimeBegin, createTimeBegin);
                this.excelServiceClient.ExportDealerPartsTransferOrderCompleted -= ExcelServiceClient_ExportDealerPartsTransferOrderCompleted;
                this.excelServiceClient.ExportDealerPartsTransferOrderCompleted += ExcelServiceClient_ExportDealerPartsTransferOrderCompleted;
            } else if(uniqueId == CommonActionKeys.MERGEEXPORT) {
                this.excelServiceClient.ExportDealerPartsTransferOrderAndDetailAsync(ids, BaseApp.Current.CurrentUserData.EnterpriseId, noWarrantyBrandId, warrantyBrandId, code, status, createTimeBegin, createTimeBegin);
                this.excelServiceClient.ExportDealerPartsTransferOrderAndDetailCompleted -= ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted;
                this.excelServiceClient.ExportDealerPartsTransferOrderAndDetailCompleted += ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted;
            }
        }

        private void ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted(object sender, ExportDealerPartsTransferOrderAndDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void ExcelServiceClient_ExportDealerPartsTransferOrderCompleted(object sender, ExportDealerPartsTransferOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(this.DataGridView.SelectedEntities.Cast<DealerPartsTransferOrder>().ToArray().Length != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<DealerPartsTransferOrder>().ToArray()[0].Status == (int)DcsDealerPartsTransferOrderStatus.新增;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null) {
                compositeFilterItem = new CompositeFilterItem();
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "DealerId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerPartsTransferOrderForReport"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("DealerPartsTransferOrderForReport");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
    }
}
