﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "Accessories", "DealerPartsTransferOrder", ActionPanelKeys = new[] {
        "DealerPartsTransferOrder"
    })]
    public class DealerPartsTransferOrderManagement : DcsDataManagementViewBase {
        public DealerPartsTransferOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_DealerPartsTransferOrder;
        }

        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditViewApprove;
        private DataEditViewBase dataEditViewReject;
        private const string _DataEditViewReject_ = "DATA_EDIT_VIEW_REJECT";


        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerPartsTransferOrder"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.APPROVE:
                    this.DataEditViewApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.REJECT:
                    this.DataEditViewReject.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(_DataEditViewReject_);
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<DealerPartsTransferOrder>().Select(r => r.Id).ToArray();
                        this.ExportDealerPartsTransferOrder(ids, null, null, null, null, null, null, uniqueId);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var noWarrantyBrandId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "NoWarrantyBrandId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "NoWarrantyBrandId").Value as int?;
                        var warrantyBrandId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarrantyBrandId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarrantyBrandId").Value as int?;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportDealerPartsTransferOrder(null, noWarrantyBrandId, warrantyBrandId, code, status, createTimeBegin, createTimeEnd, uniqueId);
                    }
                    break;
            }
        }


        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public void ExportDealerPartsTransferOrder(int[] ids, int? noWarrantyBrandId, int? warrantyBrandId, string code, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, string uniqueId) {
            if(uniqueId == CommonActionKeys.EXPORT) {
                this.excelServiceClient.ExportDealerPartsTransferOrderAsync(ids, null, noWarrantyBrandId, warrantyBrandId, code, status, createTimeBegin, createTimeBegin);
                this.excelServiceClient.ExportDealerPartsTransferOrderCompleted -= ExcelServiceClient_ExportDealerPartsTransferOrderCompleted;
                this.excelServiceClient.ExportDealerPartsTransferOrderCompleted += ExcelServiceClient_ExportDealerPartsTransferOrderCompleted;
            } else if(uniqueId == CommonActionKeys.MERGEEXPORT) {
                this.excelServiceClient.ExportDealerPartsTransferOrderAndDetailAsync(ids, null, noWarrantyBrandId, warrantyBrandId, code, status, createTimeBegin, createTimeBegin);
                this.excelServiceClient.ExportDealerPartsTransferOrderAndDetailCompleted -= ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted;
                this.excelServiceClient.ExportDealerPartsTransferOrderAndDetailCompleted += ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted;
            }
        }

        private void ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted(object sender, ExportDealerPartsTransferOrderAndDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void ExcelServiceClient_ExportDealerPartsTransferOrderCompleted(object sender, ExportDealerPartsTransferOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.REJECT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(this.DataGridView.SelectedEntities.Cast<DealerPartsTransferOrder>().ToArray().Length != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<DealerPartsTransferOrder>().ToArray()[0].Status == (int)DcsDealerPartsTransferOrderStatus.提交;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var comosite = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(comosite);
            this.DataGridView.FilterItem = comosite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditViewApprove);
            this.RegisterView(_DataEditViewReject_, () => this.DataEditViewReject);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerPartsTransferOrderForReport"));
            }
        }

        private DataEditViewBase DataEditViewApprove {
            get {
                if(this.dataEditViewApprove == null) {
                    this.dataEditViewApprove = DI.GetDataEditView("DealerPartsTransferOrderForApprove");
                    this.dataEditViewApprove.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewApprove.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewApprove;
            }
        }

        private DataEditViewBase DataEditViewReject {
            get {
                if(this.dataEditViewReject == null) {
                    this.dataEditViewReject = DI.GetDataEditView("DealerPartsTransferOrderForReject");
                    this.dataEditViewReject.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewReject.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewReject;
            }
        }
        private void ResetEditView() {
            this.dataEditViewApprove = null;
            this.dataEditViewReject = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
    }
}