﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
     [PageMeta("Common", "Company", "Express", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_PAUSE_RESUME_EXPORT_IMPORT
    })]
    public class ExpressManagement : DcsDataManagementViewBase {
         public ExpressManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_Express;
         }
         private DataGridViewBase dataGridView;
         private DataEditViewBase dataEditView;
         private DataEditViewBase dataEditViewImport;
         private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";

         
         protected override IEnumerable<string> QueryPanelKeys {
             get {
                 return new[] {
                    "Express"
                };
             }
         }
         protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
             switch(uniqueId) {
                 case CommonActionKeys.ADD:
                     var express = this.DataEditView.CreateObjectToEdit<Express>();
                     express.Status =(int)DcsMasterDataStatus.有效;
                     this.SwitchViewTo(DATA_EDIT_VIEW);
                     break;
                 case CommonActionKeys.EDIT:
                     this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                     this.SwitchViewTo(DATA_EDIT_VIEW);
                     break;
                 case CommonActionKeys.ABANDON:
                      DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_AbandonExpress, () => {
                        var id = this.DataGridView.SelectedEntities.Cast<Express>().Select(r => r.Id).First();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext != null) {
                            ShellViewModel.Current.IsBusy = true;
                            domainContext.作废快递公司(id, invokeOp => {
                                ShellViewModel.Current.IsBusy = false;
                                if(invokeOp.HasError)
                                    return;
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        }
                    });
                    break;
                 case CommonActionKeys.PAUSE:
                     DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_PauseExpress, () => {
                        var id = this.DataGridView.SelectedEntities.Cast<Express>().Select(r => r.Id).First();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext != null) {
                            ShellViewModel.Current.IsBusy = true;
                            domainContext.停用快递公司(id, invokeOp => {
                                ShellViewModel.Current.IsBusy = false;
                                if(invokeOp.HasError)
                                    return;
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_PauseSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        }
                    });
                    break;
                 case CommonActionKeys.RESUME:
                     DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_ResumeExpress, () => {
                        var id = this.DataGridView.SelectedEntities.Cast<Express>().Select(r => r.Id).First();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext != null) {
                            ShellViewModel.Current.IsBusy = true;
                            domainContext.恢复快递公司(id, invokeOp => {
                                ShellViewModel.Current.IsBusy = false;
                                if(invokeOp.HasError)
                                    return;
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_ResumeSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        }
                    });
                    break;
                 case CommonActionKeys.EXPORT:
                     if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                         var ids = this.DataGridView.SelectedEntities.Cast<Express>().Select(r => r.Id).ToArray();
                         this.导出快递公司单据(ids, null, null,null);
                     } else {
                         var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                         if(filterItem == null)
                             return;
                         var expressCode = filterItem.Filters.Single(r => r.MemberName == "ExpressCode").Value as string;
                         var expressName = filterItem.Filters.Single(r => r.MemberName == "ExpressName").Value as string;
                         var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                         this.导出快递公司单据(new int[] { }, expressCode, expressName, status);
                     }
                     break;
                 case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
             }
         }
         private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
         private void 导出快递公司单据(int[] ids, string expressCode,string expressName,int? status) {
             ShellViewModel.Current.IsBusy = true;
             this.excelServiceClient.快递公司单据导出Async(ids, expressCode, expressName, status);
             this.excelServiceClient.快递公司单据导出Completed -= excelServiceClient_快递公司单据导出Completed;
             this.excelServiceClient.快递公司单据导出Completed += excelServiceClient_快递公司单据导出Completed;
         }

         private void excelServiceClient_快递公司单据导出Completed(object sender, 快递公司单据导出CompletedEventArgs e) {
             ShellViewModel.Current.IsBusy = false;
             HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
         }
         protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
             if(this.CurrentViewKey != DATA_GRID_VIEW)
                 return false;
             switch(uniqueId) {
                 case CommonActionKeys.ADD:
                     return true;
                 case CommonActionKeys.PAUSE:
                 case CommonActionKeys.ABANDON:
                 case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    if(this.DataGridView.SelectedEntities.Cast<Express>().ToArray().Length != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<Express>().ToArray()[0].Status == (int)DcsMasterDataStatus.有效;
                case CommonActionKeys.RESUME:
                      if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                         return false;
                     if(this.DataGridView.SelectedEntities.Cast<Express>().ToArray().Length != 1)
                         return false;
                     return  this.DataGridView.SelectedEntities.Cast<Express>().ToArray()[0].Status == (int)DcsMasterDataStatus.停用;
                 case CommonActionKeys.EXPORT:
                     return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                 case CommonActionKeys.IMPORT:
                     return true;
                 default:
                     return false;
             }
         }
         protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
             this.SwitchViewTo(DATA_GRID_VIEW);
             var composite = filterItem as CompositeFilterItem;
             ClientVar.ConvertTime(composite);
             this.DataGridView.FilterItem = composite;
             this.DataGridView.ExecuteQueryDelayed();
         }

         
         private void Initialize() {
             this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
             this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
             this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
         }

         private DataGridViewBase DataGridView {
             get {
                 return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("Express"));
             }
         }

         //导入修改
         private DataEditViewBase DataEditViewImport {
             get {
                 if(this.dataEditViewImport == null) {
                     this.dataEditViewImport = DI.GetDataEditView("ExpressForImport");
                     this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                 }
                 return this.dataEditViewImport;
             }
         }
         private DataEditViewBase DataEditView {
             get {
                 if(this.dataEditView == null) {
                     this.dataEditView = DI.GetDataEditView("Express");
                     this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                     this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                 }
                 return this.dataEditView;
             }
         }
         private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
         private void DataEditView_EditSubmitted(object sender, EventArgs e) {
             this.ResetEditView();
             if(this.DataGridView.FilterItem != null)
                 this.DataGridView.ExecuteQueryDelayed();
             this.SwitchViewTo(DATA_GRID_VIEW);
         }

         private void DataEditView_EditCancelled(object sender, EventArgs e) {
             this.ResetEditView();
             this.SwitchViewTo(DATA_GRID_VIEW);
         }
     }
}
