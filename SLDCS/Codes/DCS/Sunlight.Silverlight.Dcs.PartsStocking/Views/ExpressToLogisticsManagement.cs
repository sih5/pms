﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
     [PageMeta("Common", "Company", "ExpressToLogistics", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_IMPORT_EXPORT
     })]
    public class ExpressToLogisticsManagement : DcsDataManagementViewBase {
         public ExpressToLogisticsManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_ExpressToLogistics;
         }
         private DataGridViewBase dataGridView;
         private DataEditViewBase dataEditView;
         private DataEditViewBase dataEditViewImport;
         private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";

         protected override IEnumerable<string> QueryPanelKeys {
             get {
                 return new[] {
                    "ExpressToLogistics"
                };
             }
         }

         protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
             switch(uniqueId) {
                 case CommonActionKeys.ADD:
                     var expressToLogistic = this.DataEditView.CreateObjectToEdit<ExpressToLogistic>();
                     expressToLogistic.Status = (int)DcsMasterDataStatus.有效;
                     this.SwitchViewTo(DATA_EDIT_VIEW);
                     break;
                 case CommonActionKeys.EDIT:
                     this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                     this.SwitchViewTo(DATA_EDIT_VIEW);
                     break;
                 case CommonActionKeys.EXPORT:
                     if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                         var ids = this.DataGridView.SelectedEntities.Cast<ExpressToLogistic>().Select(r => r.Id).ToArray();
                         this.导出快递物流关系(ids, null, null, null,null,null);
                     } else {
                         var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                         if(filterItem == null)
                             return;
                         var focufingCore = filterItem.Filters.Single(r => r.MemberName == "FocufingCoreId").Value as int?;
                         var logisticsCompanyCode = filterItem.Filters.Single(r => r.MemberName == "LogisticsCompanyCode").Value as string;
                         var logisticsCompanyName = filterItem.Filters.Single(r => r.MemberName == "LogisticsCompanyName").Value as string;
                         var expressCode = filterItem.Filters.Single(r => r.MemberName == "ExpressCode").Value as string;
                         var expressName = filterItem.Filters.Single(r => r.MemberName == "ExpressName").Value as string;
                         this.导出快递物流关系(new int[] { }, focufingCore, logisticsCompanyCode, logisticsCompanyName, expressCode, expressName);
                     }
                     break;
                 case CommonActionKeys.IMPORT:
                     this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                     break;
             }
         }
         private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
         private void 导出快递物流关系(int[] ids, int? focufingCore, string logisticsCompanyCode, string logisticsCompanyName, string expressCode, string expressName) {
             ShellViewModel.Current.IsBusy = true;
             this.excelServiceClient.导出快递物流关系Async(ids, focufingCore, logisticsCompanyCode, logisticsCompanyName, expressCode, expressName);
             this.excelServiceClient.导出快递物流关系Completed -= excelServiceClient_导出快递物流关系Completed;
             this.excelServiceClient.导出快递物流关系Completed += excelServiceClient_导出快递物流关系Completed;
         }

         private void excelServiceClient_导出快递物流关系Completed(object sender, 导出快递物流关系CompletedEventArgs e) {
             ShellViewModel.Current.IsBusy = false;
             HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
         }
         protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
             if(this.CurrentViewKey != DATA_GRID_VIEW)
                 return false;
             switch(uniqueId) {
                 case CommonActionKeys.ADD:
                     return true;
                 case CommonActionKeys.EDIT:
                     if(this.DataGridView.SelectedEntities == null)
                         return false;
                     if(this.DataGridView.SelectedEntities.Cast<ExpressToLogistic>().ToArray().Length != 1)
                         return false;
                     return this.DataGridView.SelectedEntities.Cast<ExpressToLogistic>().ToArray()[0].Status == (int)DcsMasterDataStatus.有效;
                 case CommonActionKeys.EXPORT:
                     return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                 case CommonActionKeys.IMPORT:
                     return true;
                 default:
                     return false;
             }
         }
         protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
             this.SwitchViewTo(DATA_GRID_VIEW);
             var composite = filterItem as CompositeFilterItem;
             ClientVar.ConvertTime(composite);
             this.DataGridView.FilterItem = composite;
             this.DataGridView.ExecuteQueryDelayed();
         }

         private void Initialize() {
             this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
             this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
             this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
         }

         private DataGridViewBase DataGridView {
             get {
                 return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("ExpressToLogistics"));
             }
         }

         private DataEditViewBase DataEditView {
             get {
                 if(this.dataEditView == null) {
                     this.dataEditView = DI.GetDataEditView("ExpressToLogistics");
                     this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                     this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                 }
                 return this.dataEditView;
             }
         }
         //导入修改
         private DataEditViewBase DataEditViewImport {
             get {
                 if(this.dataEditViewImport == null) {
                     this.dataEditViewImport = DI.GetDataEditView("ExpressToLogisticsForImport");
                     this.dataEditViewImport.EditCancelled += this.DataEditView_EditCancelled;
                 }
                 return this.dataEditViewImport;
             }
         }
         private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
         private void DataEditView_EditSubmitted(object sender, EventArgs e) {
             this.ResetEditView();
             if(this.DataGridView.FilterItem != null)
                 this.DataGridView.ExecuteQueryDelayed();
             this.SwitchViewTo(DATA_GRID_VIEW);
         }

         private void DataEditView_EditCancelled(object sender, EventArgs e) {
             this.ResetEditView();
             this.SwitchViewTo(DATA_GRID_VIEW);
         }
     }
}
