﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsInStore", "InboundHistoryQuery", ActionPanelKeys = new[] { CommonActionKeys.EXPORT })]
    public class InboundHistoryQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public InboundHistoryQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.QueryPanel_Title_InboundHistoryQuery;
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("InboundHistoryQuery"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "InboundHistoryQuery"
                };
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()){
                        var codes = this.DataGridView.SelectedEntities.Cast<VirtualInboundHistoryQuery>().Select(e => e.PartsInboundOrderCode).ToArray();
                        this.导出历史入库记录查询(codes, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    }else{
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var branchId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "BranchId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "BranchId").Value as int?;
                        var partsInboundOrderCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsInboundOrderCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsInboundOrderCode").Value as string;
                        var inboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InboundType").Value as int?;
                        var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var partsPurchaseOrderTypeId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsPurchaseOrderTypeId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsPurchaseOrderTypeId").Value as int?;
                        var originalRequirementBillCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OriginalRequirementBillCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OriginalRequirementBillCode").Value as string;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var settlementstatus = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Settlementstatus") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Settlementstatus").Value as int?;
                        var partCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartCode").Value as string;
                        var partName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartName").Value as string;
                        var companyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CompanyName").Value as string;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if (createTime != null){
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.导出历史入库记录查询(new string[] { }, branchId, partsInboundOrderCode, inboundType, partsSalesCategoryId,partsPurchaseOrderTypeId, originalRequirementBillCode, warehouseId, settlementstatus, partCode, partName, companyName, createTimeBegin, createTimeEnd);
                        
                    }
                    break;
            }
        }

        private void 导出历史入库记录查询(string[] codes, int? branchId, string partsInboundOrderCode, int? inboundType, int? partsSalesCategoryId, int? partsPurchaseOrderTypeId, string originalRequirementBillCode, int? warehouseId, int? settlementstatus, string partCode, string partName, string companyName, DateTime? createTimeBegin, DateTime? createTimeEnd){
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.导出历史入库记录查询Async(codes, branchId, partsInboundOrderCode, inboundType, partsSalesCategoryId, partsPurchaseOrderTypeId, originalRequirementBillCode, warehouseId, settlementstatus, partCode, partName, companyName, createTimeBegin, createTimeEnd);
            this.excelServiceClient.导出历史入库记录查询Completed -= excelServiceClient_导出历史入库记录查询Completed;
            this.excelServiceClient.导出历史入库记录查询Completed += excelServiceClient_导出历史入库记录查询Completed;
        }

        private void excelServiceClient_导出历史入库记录查询Completed(object sender, 导出历史入库记录查询CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            var parameters = filterItem.ToQueryParameters();
            var createBeginTime = parameters.ContainsKey("CreateTime") ? ((System.DateTime?[])(parameters["CreateTime"]))[0] : null;
            var createEndTime = parameters.ContainsKey("CreateTime") ? ((System.DateTime?[])(parameters["CreateTime"]))[1] : null;
            if(createBeginTime == null || createEndTime == null) {
                UIHelper.ShowAlertMessage(PartsStockingUIStrings.DataManagementView_Notification_QueryTimeIsNull);
                return;
            }
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
