﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsLogisticsRelated", "LogisticsTracking", ActionPanelKeys = new[] {
        "Logistics",CommonActionKeys.EXPORT_MERGEEXPORT
    })]
    public class LogisticsTrackingManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("Logistics"));
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("Logistics");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "Logistics"
                };
            }
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "Input":
                case "InputByDeputy":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualLogistic>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities.Any(e => e.Status == (int)DcsBaseDataStatus.有效 && e.CurrentOrderType == (int)DcsCurrentOrderType.物流信息主单);//entities[0].Status == (int)DcsBaseDataStatus.有效&&)效&&;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);


            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Input":
                case "InputByDeputy":
                    var entityForEdit = this.DataGridView.SelectedEntities.Cast<VirtualLogistic>().SingleOrDefault();
                    if(entityForEdit == null)
                        return;
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<VirtualLogistic>().First().Id);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        //var ids = this.DataGridView.SelectedEntities.Cast<VirtualLogistic>().Select(r => r.Id).ToArray();
                        var logisticIds = this.DataGridView.SelectedEntities.Cast<VirtualLogistic>().Where(r => r.CurrentOrderType == (int)DcsCurrentOrderType.物流信息主单).Select(r => r.Id).ToArray();
                        var processIds = this.DataGridView.SelectedEntities.Cast<VirtualLogistic>().Where(r => r.CurrentOrderType == (int)DcsCurrentOrderType.配件销售订单).Select(r => r.Id).ToArray();
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {

                            this.MergeExport(logisticIds, processIds, null, null, null, null, null, null, null, null, null, null, null);
                        }
                        if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                            this.Export(logisticIds, processIds, null, null, null, null, null, null, null, null, null, null, null);
                        }
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;

                        var shippingCode = filterItem.Filters.Single(r => r.MemberName == "ShippingCode").Value as string;
                        var orderCode = filterItem.Filters.Single(e => e.MemberName == "OrderCode").Value as string;
                        var warehouseName = filterItem.Filters.Single(e => e.MemberName == "WarehouseName").Value as string;
                        var logisticName = filterItem.Filters.Single(e => e.MemberName == "LogisticName").Value as string;
                        var signStatus = filterItem.Filters.Single(e => e.MemberName == "SignStatus").Value as int?;
                        DateTime? beginShippingDate = null;
                        DateTime? endShippingDate = null;
                        DateTime? beginApproveTime = null;
                        DateTime? endApproveTime = null;
                        DateTime? beginRequestedArrivalDate = null;
                        DateTime? endRequestedArrivalDate = null;


                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "ShippingDate") {
                                    beginShippingDate = dateTime.Filters.First(r => r.MemberName == "ShippingDate").Value as DateTime?;
                                    endShippingDate = dateTime.Filters.Last(r => r.MemberName == "ShippingDate").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "ApproveTime") {
                                    beginApproveTime = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                    endApproveTime = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "RequestedArrivalDate") {
                                    beginRequestedArrivalDate = dateTime.Filters.First(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                    endRequestedArrivalDate = dateTime.Filters.Last(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                }
                            }
                        }
                        ShellViewModel.Current.IsBusy = true;
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId))
                            this.MergeExport(null, null, shippingCode, orderCode, beginShippingDate, endShippingDate, beginApproveTime, endApproveTime, beginRequestedArrivalDate, endRequestedArrivalDate, warehouseName, logisticName, signStatus);
                        if(CommonActionKeys.EXPORT.Equals(uniqueId))
                            this.Export(null, null, shippingCode, orderCode, beginShippingDate, endShippingDate, beginApproveTime, endApproveTime, beginRequestedArrivalDate, endRequestedArrivalDate, warehouseName, logisticName, signStatus);
                    }
                    break;
            }
        }
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private void Export(int[] logisticIds, int[] processIds, string shippingCode, string orderCode, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginApproveTime, DateTime? endApproveTime, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, string warehouseName, string logisticName, int? signStatus) {
            this.dcsDomainContext.ExportVirtualLogistic(logisticIds, processIds, shippingCode, orderCode, beginShippingDate, endShippingDate, beginApproveTime, endApproveTime, beginRequestedArrivalDate, endRequestedArrivalDate, warehouseName, logisticName, signStatus, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                    ShellViewModel.Current.IsBusy = false;
                }

                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    ShellViewModel.Current.IsBusy = false;
                }
            }, null);
        }

        private void MergeExport(int[] logisticIds, int[] processIds, string shippingCode, string orderCode, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginApproveTime, DateTime? endApproveTime, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, string warehouseName, string logisticName, int? signStatus) {
            this.dcsDomainContext.ExportMergeVirtualLogistic(logisticIds, processIds, shippingCode, orderCode, beginShippingDate, endShippingDate, beginApproveTime, endApproveTime, beginRequestedArrivalDate, endRequestedArrivalDate, warehouseName, logisticName, signStatus, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                    UIHelper.ShowNotification(loadOp.Value);
                    ShellViewModel.Current.IsBusy = false;
                }

                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                    ShellViewModel.Current.IsBusy = false;
                }
            }, null);
        }


        public LogisticsTrackingManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_LogisticsTracking;
        }

    }
}
