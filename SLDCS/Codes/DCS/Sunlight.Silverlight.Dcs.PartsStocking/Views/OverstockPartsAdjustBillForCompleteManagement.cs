﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {

    [PageMeta("PartsSales", "BacklogPartsPlatform", "OverstockPartsAdjustBillForComplete", ActionPanelKeys = new[] {
        "OverstockPartsAdjustBillForComplete", CommonActionKeys.EXPORT_PRINT
    })]
    public class OverstockPartsAdjustBillForCompleteManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase kanBaneDataEditView;
        private DataEditViewBase cmpleteDataEditView;

        private const string DATA_EDIT_VIEW_COMPLETE = "_DataEditViewComplete_";

        public OverstockPartsAdjustBillForCompleteManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "积压件调剂完成";
        }


        private DataEditViewBase CompleteDataEditView {
            get {
                if(this.cmpleteDataEditView == null) {
                    this.cmpleteDataEditView = DI.GetDataEditView("OverstockPartsAdjustBill");
                    this.cmpleteDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.cmpleteDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.cmpleteDataEditView;
            }
        }

        private DataEditViewBase KanBaneDataEditView {
            get {
                if(this.kanBaneDataEditView == null) {
                    this.kanBaneDataEditView = DI.GetDataEditView("OverstockPartsAdjustDetailForDetail");
                    this.kanBaneDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.kanBaneDataEditView;
            }
        }
        private void ResetEditView() {
            this.kanBaneDataEditView = null;
            this.cmpleteDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            var entity = this.DataGridView.SelectedEntities.Cast<OverstockPartsAdjustBill>().SingleOrDefault();
            if(entity == null)
                return;
            switch(uniqueId) {
                case "Complete":
                    if(entity.Status != (int)DcsOverstockPartsAdjustBillStatus.确认) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_StatusIsNotConfirmCanNotComplete);
                        return;
                    }
                    this.CompleteDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_COMPLETE);
                    break;
                case "AdjustKanban":
                    this.KanBaneDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.PRINT:
                    //TODO:打印功能暂不实现
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilter = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilter);
            this.DataGridView.FilterItem = compositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.KanBaneDataEditView);
            this.RegisterView(DATA_EDIT_VIEW_COMPLETE, () => this.CompleteDataEditView);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("OverstockPartsAdjustBillForComplete"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "OverstockPartsAdjustBill"
                };
            }
        }
    }
}
