﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {

    [PageMeta("PartsSales", "BacklogPartsPlatform", "OverstockPartsAdjustBill", ActionPanelKeys = new[] {
        CommonActionKeys.EDIT_ABANDON_EXPORT,"OverstockPartsAdjustBill"
    })]
    public class OverstockPartsAdjustBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase cmpleteDataEditView;
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private const string DATA_EDIT_VIEW_COMPLETE = "_DataEditViewComplete_";

        public OverstockPartsAdjustBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_OverstockPartsAdjustBill;
        }

        private DataEditViewBase CompleteDataEditView {
            get {
                if (this.cmpleteDataEditView == null) {
                    this.cmpleteDataEditView = DI.GetDataEditView("OverstockPartsAdjustBillForComplete");
                    this.cmpleteDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.cmpleteDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.cmpleteDataEditView;
            }
        }
        private void ResetEditView() {
            this.cmpleteDataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if (this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("OverstockPartsAdjustBillWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW_COMPLETE, () => this.CompleteDataEditView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilter = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilter);
            this.DataGridView.FilterItem = compositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch (uniqueId) {
                case "Confirm":
                    var entity = this.DataGridView.SelectedEntities.Cast<OverstockTransferOrder>().FirstOrDefault();
                    if (entity == null)
                        return;
                    if (entity.Status != (int)DcsDealerPartsTransferOrderStatus.提交) {
                        UIHelper.ShowNotification("只能确认“提交状态的单据”");
                        return;
                    }
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Confirm, () => {
                        dcsDomainContext.确认积压件调拨单(entity, entity.OverstockTransferOrderDetails.ToArray(), invokeOp => {
                            if (invokeOp.HasError) {
                                if (!invokeOp.IsErrorHandled)
                                    invokeOp.MarkErrorAsHandled();
                                var error = invokeOp.ValidationErrors.First();
                                if (error != null)
                                    UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                                else
                                    DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                return;
                            }
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_Confirm);
                            if (this.DataGridView.FilterItem != null)
                                this.DataGridView.ExecuteQueryDelayed();
                        }, null);
                    });
                    break;
                case "Abandon":
                    var entity1 = this.DataGridView.SelectedEntities.Cast<OverstockTransferOrder>().FirstOrDefault();
                    if (entity1 == null)
                        return;
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        try {
                            ShellViewModel.Current.IsBusy = true;
                            if (entity1.Can作废积压件调拨单)
                                entity1.作废积压件调拨单();
                            var domainContext = this.DataGridView.DomainContext;
                            if (domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if (submitOp.HasError) {
                                    if (!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    ShellViewModel.Current.IsBusy = false;
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                        } catch (Exception ex) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Submit":
                    var entitys = this.DataGridView.SelectedEntities.Cast<OverstockTransferOrder>().ToArray();
                    if (!entitys.All(r => r.Status == (int)DCSOverstockPartsTransferOrderStatus.新建)) {
                        UIHelper.ShowNotification("只能提交新建状态的单据");
                        return;
                    }
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Submit, () => {
                        ShellViewModel.Current.IsBusy = true;
                        dcsDomainContext.提交积压件调拨单(entitys, invokeOp => {
                            if (invokeOp.HasError) {
                                if (!invokeOp.IsErrorHandled)
                                    invokeOp.MarkErrorAsHandled();
                                ShellViewModel.Current.IsBusy = false;
                                var error = invokeOp.ValidationErrors.First();
                                if (error != null)
                                    UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                                else
                                    DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                return;
                            }
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_SubmitSuccess);
                            if (this.DataGridView.FilterItem != null)
                                this.DataGridView.ExecuteQueryDelayed();
                        }, null);
                    });
                    break;
                case CommonActionKeys.EDIT:
                    var entity4 = this.DataGridView.SelectedEntities.Cast<OverstockTransferOrder>().FirstOrDefault();
                    if (entity4 == null)
                        return;
                    if (entity4.Status != (int)DcsOverstockPartsAdjustBillStatus.新建) {
                        UIHelper.ShowNotification("只能修改新建状态的单据");
                        return;
                    }
                    this.CompleteDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_COMPLETE);
                    break;
                case CommonActionKeys.EXPORT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<OverstockTransferOrder>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null);
                    }
                    else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var transferInCorpName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "TransferInCorpName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "TransferInCorpName").Value as string;
                        var transferInWarehouseCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "TransferInWarehouseCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "TransferInWarehouseCode").Value as string;
                        var transferOutCorpName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "TransferOutCorpName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "TransferOutCorpName").Value as string;
                        var transferOutWarehouseName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "TransferOutWarehouseName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "TransferOutWarehouseName").Value as string;

                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if (dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }

                        this.ExecuteMergeExport(null, code, status,transferInCorpName, transferInWarehouseCode,transferOutCorpName,transferOutWarehouseName, createTimeBegin, createTimeEnd);
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId) {
                case "Confirm":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<OverstockTransferOrder>().ToArray();
                    if (entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DCSOverstockPartsTransferOrderStatus.提交 && entities[0].TransferOutCorpId == BaseApp.Current.CurrentUserData.EnterpriseId; ;
                case "Submit":
                    if (this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() == 0)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<OverstockTransferOrder>().ToArray();
                    if (entity.All(r => r.Status == (int)DCSOverstockPartsTransferOrderStatus.新建 && r.TransferInCorpId == BaseApp.Current.CurrentUserData.EnterpriseId))
                        return true;
                    return false;
                case "Edit":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity3 = this.DataGridView.SelectedEntities.Cast<OverstockTransferOrder>().ToArray();
                    if (entity3.Length != 1)
                        return false;
                    return entity3[0].Status == (int)DCSOverstockPartsTransferOrderStatus.新建 && entity3[0].TransferInCorpId == BaseApp.Current.CurrentUserData.EnterpriseId;
                case "Abandon":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity2 = this.DataGridView.SelectedEntities.Cast<OverstockTransferOrder>().ToArray();
                    if (entity2.Length != 1)
                        return false;
                    return (entity2[0].Status == (int)DCSOverstockPartsTransferOrderStatus.新建 || entity2[0].Status == (int)DCSOverstockPartsTransferOrderStatus.提交) && entity2[0].TransferInCorpId == BaseApp.Current.CurrentUserData.EnterpriseId;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "OverstockPartsAdjustBill"
                };
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int[] ids, string code, int? status, string transferInCorpName, string transferInWarehouseCode, string transferOutCorpName, string transferOutWarehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = false;
            this.excelServiceClient.ExportOverstockTransferOrderAsync(ids, code, status, transferInCorpName, transferInWarehouseCode, transferOutCorpName, transferOutWarehouseName, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportOverstockTransferOrderCompleted -= this.ExcelServiceClient_ExportOverstockTransferOrderCompleted;
            this.excelServiceClient.ExportOverstockTransferOrderCompleted += this.ExcelServiceClient_ExportOverstockTransferOrderCompleted;
        }

        private void ExcelServiceClient_ExportOverstockTransferOrderCompleted(object sender, ExportOverstockTransferOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}