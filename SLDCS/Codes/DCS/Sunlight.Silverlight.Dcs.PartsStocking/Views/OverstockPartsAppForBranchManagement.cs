﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsSales", "BacklogPartsPlatform", "OverstockPartsAppForBranch", ActionPanelKeys = new[] {
      CommonActionKeys.APPROVE_ABANDON_EXPORT_PRINT_REJECT
    })]
    public class OverstockPartsAppForBranchManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("OverstockPartsAppForApprove");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("OverstockPartsAppWithDetailsForBranch"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "OverstockPartsAppForBranch"
                };
            }
        }
        #region 驳回弹出
        private RadWindow rejectRadWindow;

        private RadWindow RejectRadWindow {
            get {
                if(this.rejectRadWindow == null) {
                    this.rejectRadWindow = new RadWindow();
                    this.rejectRadWindow.CanClose = false;
                    this.rejectRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.rejectRadWindow.Content = this.RejectDataEditView;
                    this.rejectRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.rejectRadWindow.Height = 200;
                    this.rejectRadWindow.Width = 400;
                    this.rejectRadWindow.Header = "";
                }
                return this.rejectRadWindow;
            }
        }

        private DataEditViewBase rejectDataEditView;

        private DataEditViewBase RejectDataEditView {
            get {
                if(this.rejectDataEditView == null) {
                    this.rejectDataEditView = DI.GetDataEditView("OverstockPartsAppForReject");
                    this.rejectDataEditView.EditCancelled += rejectDataEditView_EditCancelled;
                    this.rejectDataEditView.EditSubmitted += rejectDataEditView_EditSubmitted;
                }
                return this.rejectDataEditView;
            }
        }

        void rejectDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.RejectRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        void rejectDataEditView_EditCancelled(object sender, EventArgs e) {
            this.RejectRadWindow.Close();
        }
        #endregion
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<OverstockPartsApp>().ToArray();
                    return entities.Length == 1;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    //TODO:打印功能暂不实现
                    return false;
                case CommonActionKeys.REJECT:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<OverstockPartsApp>().All(r => r.Status == (int)DcsOverstockPartsAppStatus.已提交);
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities.Cast<OverstockPartsApp>().Any(entity => entity.Status == (int)DcsOverstockPartsAppStatus.已提交)) {
                        this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW);
                    } else
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonApprove);
                    break;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities.Cast<OverstockPartsApp>().Any(entity => entity.Status == (int)DcsOverstockPartsAppStatus.新建)) {
                        DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                            var selectEntity = this.DataGridView.SelectedEntities.Cast<OverstockPartsApp>().First();
                            if(selectEntity == null)
                                return;
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            try {
                                if(selectEntity.Can作废积压件申请单)
                                    selectEntity.作废积压件申请单();
                                domainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowSubmitError(submitOp);
                                    } else
                                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                    this.CheckActionsCanExecute();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        });
                    } else
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonFailed);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.PRINT:
                    //TODO:打印功能暂不实现
                    break;
                case CommonActionKeys.REJECT:
                    this.RejectDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<OverstockPartsApp>().First().Id);
                    this.RejectRadWindow.ShowDialog();
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            CompositeFilterItem compositeFilter;

            if(!(filterItem is CompositeFilterItem)) {
                compositeFilter = new CompositeFilterItem();
                compositeFilter.Filters.Add(filterItem);
            } else
                compositeFilter = filterItem as CompositeFilterItem;
            compositeFilter.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilter);
            this.DataGridView.FilterItem = compositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        public OverstockPartsAppForBranchManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_OverstockPartsAppForBranch;
        }
    }
}
