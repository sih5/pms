﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
﻿using System.ServiceModel.DomainServices.Client;
﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsSales", "BacklogPartsPlatform", "OverstockPartsApp", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_SUBMIT_ABANDON_EXPORT_PRINT
    })]
    public class OverstockPartsAppManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("OverstockPartsAppWithDetails"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("OverstockPartsApp");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "StorageCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                var createTimeFilters = compositeFilter.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                }
                compositeFilterItem.Filters.Add(compositeFilter);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "OverstockPartsApp"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var overstockPartsApp = this.DataEditView.CreateObjectToEdit<OverstockPartsApp>();
                    overstockPartsApp.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    overstockPartsApp.Status = (int)DcsOverstockPartsAppStatus.新建;
                    overstockPartsApp.StorageCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    overstockPartsApp.StorageCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    overstockPartsApp.StorageCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    var dcsDomainContext = new DcsDomainContext();
                    dcsDomainContext.Load(dcsDomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption => {
                        if(loadOption.HasError) {
                            loadOption.MarkErrorAsHandled();
                            return;
                        }
                        if(loadOption.Entities != null && loadOption.Entities.Any()) {
                            var company = loadOption.Entities.First();
                            if(company.Type != (int)DcsCompanyType.服务站兼代理库)
                                overstockPartsApp.StorageCompanyType = company.Type;
                        }
                        this.SwitchViewTo(DATA_EDIT_VIEW);
                    }, null);

                    break;
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities.Cast<OverstockPartsApp>().Any(entity => entity.Status == (int)DcsOverstockPartsAppStatus.新建)) {
                        this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW);
                    } else
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_EDITFailed);
                    break;
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities.Cast<OverstockPartsApp>().Any(entity => entity.Status == (int)DcsOverstockPartsAppStatus.新建)) {
                        DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Submit, () => this.DataGridView.UpdateSelectedEntities(entity => ((OverstockPartsApp)entity).Status = (int)DcsOverstockPartsAppStatus.已提交, () => {
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_SubmitSuccess);
                            this.CheckActionsCanExecute();
                        }));
                    } else
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_SUBMITFailed);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities.Cast<OverstockPartsApp>().Any(entity => entity.Status == (int)DcsOverstockPartsAppStatus.新建)) {
                        DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Submit, () => this.DataGridView.UpdateSelectedEntities(entity => ((OverstockPartsApp)entity).Status = (int)DcsOverstockPartsAppStatus.作废, () => {
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                            this.CheckActionsCanExecute();
                        }));
                    } else
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonFailed);
                    break;
                case CommonActionKeys.PRINT:
                    //TODO:打印功能暂不实现
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<OverstockPartsApp>().ToArray();
                    return entities.Length == 1;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var overstockPartsApp = this.DataGridView.SelectedEntities.Cast<OverstockPartsApp>().ToArray();
                    return overstockPartsApp.Length == 1 && overstockPartsApp.First().Status == (int)DcsOverstockPartsAppStatus.新建;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    //TODO:打印功能暂不实现
                    return false;
                default:
                    return false;
            }
        }

        public OverstockPartsAppManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_OverstockPartsApp;
        }
    }
}
