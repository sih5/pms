﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsSales", "BacklogPartsPlatform", "OverstockPartsInformation", ActionPanelKeys = new[] {
        "OverstockPartsInformation", CommonActionKeys.EXPORT
    })]
    public class OverstockPartsInformationManagement : DcsDataManagementViewBase {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase dataGridView;

        public OverstockPartsInformationManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_OverstockPartsInformation;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("OverstockPartsStockForQuery"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        //根据选择的虚拟积压件库存删除积压件信息
        private void RemovetOverstockPartsInformation(IEnumerable<OverstockPartsStock> overstockPartsStocks) {
            var overstockSearchIds = (from stock in overstockPartsStocks
                                      select stock.OverstockPartsInformationId).ToArray();
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;

            domainContext.Load(domainContext.GetOverstockPartsInformationByArrayIdsQuery(overstockSearchIds), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var overstockPartsInformations = loadOp.Entities;
                foreach(var overstockPartsInformation in overstockPartsInformations)
                    domainContext.OverstockPartsInformations.Remove(overstockPartsInformation);
                try {
                    domainContext.SubmitChanges(submitOp => {
                        if(submitOp.HasError) {
                            if(!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                            domainContext.RejectChanges();
                            return;
                        }
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_Remove);
                        this.DataGridView.ExecuteQueryDelayed();
                        this.CheckActionsCanExecute();
                    }, null);
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                var createTimeFilters = compositeFilterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                }
            } else {
                compositeFilterItem.Filters.Add(filterItem);
            }
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "CompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Remove":
                    var overstockPartsStocks = this.DataGridView.SelectedEntities.Cast<OverstockPartsStock>().ToArray();
                    if(overstockPartsStocks == null)
                        return;
                    DcsUtils.Confirm(overstockPartsStocks.Any(entity => entity.UsableQuantity != 0) ? PartsStockingUIStrings.DataManagementView_Confirm_RemoveUsableQuantityThanZero :
                        PartsStockingUIStrings.DataManagementView_Confirm_Remove, () => this.RemovetOverstockPartsInformation(overstockPartsStocks));
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<OverstockPartsStock>().Select(e => e.OverstockPartsInformationId).ToArray();
                        this.导出积压件信息(ids, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var SparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var SparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        var StorageCompanyCode = filterItem.Filters.Single(e => e.MemberName == "StorageCompanyCode").Value as string;
                        var StorageCompanyName = filterItem.Filters.Single(e => e.MemberName == "StorageCompanyName").Value as string;
                        var ProvinceName = filterItem.Filters.Single(e => e.MemberName == "ProvinceName").Value as string;
                        var CityName = filterItem.Filters.Single(e => e.MemberName == "CityName").Value as string;
                        var CountyName = filterItem.Filters.Single(e => e.MemberName == "CountyName").Value as string;
                        //var PartsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.导出积压件信息(new int[] { }, SparePartCode, SparePartName, StorageCompanyCode, StorageCompanyName, ProvinceName, CityName, CountyName, null, createTimeBegin, createTimeEnd);
                    }
                    break;
            }
        }

        private void 导出积压件信息(int[] ids, string SparePartCode, string SparePartName, string StorageCompanyCode, string StorageCompanyName, string ProvinceName, string CityName, string CountyName, int? PartsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.导出积压件信息Async(ids, BaseApp.Current.CurrentUserData.EnterpriseId, SparePartCode, SparePartName, StorageCompanyCode, StorageCompanyName, ProvinceName, CityName, CountyName, PartsSalesCategoryId, createTimeBegin, createTimeEnd);
            this.excelServiceClient.导出积压件信息Completed -= excelServiceClient_导出积压件信息Completed;
            this.excelServiceClient.导出积压件信息Completed += excelServiceClient_导出积压件信息Completed;
        }

        private void excelServiceClient_导出积压件信息Completed(object sender, 导出积压件信息CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "Remove":
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any();
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "OverstockPartsInformation"
                };
            }
        }
    }
}