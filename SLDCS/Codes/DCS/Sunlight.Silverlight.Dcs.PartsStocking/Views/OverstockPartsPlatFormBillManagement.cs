﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsSales", "BacklogPartsPlatform", "OverstockPartsPlatFormBill", ActionPanelKeys = new[] {
        "OverstockPartsPlatFormBill", CommonActionKeys.EDIT_EXPORT
    })]
    public class OverstockPartsPlatFormBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase uploadDataEditView;
        private DataEditViewBase dataEditView;
        protected const string Upload_DATA_EDIT_VIEW = "_UploadDataEditView_";
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public OverstockPartsPlatFormBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.Management_Title_OverstockPartsPlatFormBill;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("OverstockPartsPlatFormBill"));
            }
        }
        private DataEditViewBase UploadDataEditView {
            get {
                if (this.uploadDataEditView == null) {
                    this.uploadDataEditView = DI.GetDataEditView("OverstockPartsPlatFormBillForUpload");
                    this.uploadDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.uploadDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.uploadDataEditView;
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if (this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("OverstockPartsPlatFormBill");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void ResetEditView() {
            this.uploadDataEditView = null;
            this.dataEditView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(Upload_DATA_EDIT_VIEW, () => this.UploadDataEditView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            var codes = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
            if (codes != null && codes.Value != null) {
                if (((IEnumerable<string>)codes.Value).ToArray().Length > 20) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.Management_Validation_PartCodeMoreThan);
                    return;
                }
                compositeFilterItem.Filters.Add(new CompositeFilterItem {
                    MemberName = "SparePartCode",
                    MemberType = typeof(string),
                    Value = string.Join(",", ((IEnumerable<string>)codes.Value).ToArray()),
                    Operator = FilterOperator.Contains
                });
                compositeFilterItem.Filters.Remove(codes);
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Buy":
                    DcsUtils.Confirm(PartsStockingUIStrings.Management_Confirm_Buy, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entitys = this.DataGridView.SelectedEntities.Cast<VirtualOverstockPartsPlatFormBill>().ToArray();
                        if (entitys == null || !entitys.Any())
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if (domainContext == null)
                            return;
                        domainContext.Buy(entitys, invokeOp => {
                            if (invokeOp.HasError) {
                                ShellViewModel.Current.IsBusy = false;
                                if (!invokeOp.IsErrorHandled)
                                    invokeOp.MarkErrorAsHandled();
                                var error = invokeOp.ValidationErrors.First();
                                if (error != null)
                                    UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                                else
                                    DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                return;
                            }
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Text_OperationSuccess);
                            this.CheckActionsCanExecute();
                            this.DataGridView.ExecuteQuery();
                        }, null);
                    });
                    break;
                case "Upload":
                    this.UploadDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<VirtualOverstockPartsPlatFormBill>().First().Id); 
                    this.SwitchViewTo(Upload_DATA_EDIT_VIEW); 
                    break; 
                case "Edit":
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<VirtualOverstockPartsPlatFormBill>().First().Id); 
                    this.SwitchViewTo(DATA_EDIT_VIEW); 
                    break; 
                case CommonActionKeys.DELETE:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Notification_Delete, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entitys = this.DataGridView.SelectedEntities.Cast<VirtualOverstockPartsPlatFormBill>().ToArray();
                        if (entitys == null || !entitys.Any())
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if (domainContext == null)
                            return;
                        domainContext.Delete(entitys, invokeOp => {
                            if (invokeOp.HasError) {
                                ShellViewModel.Current.IsBusy = false;
                                if (!invokeOp.IsErrorHandled)
                                    invokeOp.MarkErrorAsHandled();
                                var error = invokeOp.ValidationErrors.First();
                                if (error != null)
                                    UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
                                else
                                    DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                return;
                            }
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataEditView_Text_OperationSuccess);
                            this.CheckActionsCanExecute();
                            this.DataGridView.ExecuteQuery();
                        }, null);
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    var sparePartName = filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                    var centerName = filterItem.Filters.Single(e => e.MemberName == "CenterName").Value as string;
                    var dealerName = filterItem.Filters.Single(e => e.MemberName == "DealerName").Value as string;
                    ShellViewModel.Current.IsBusy = true;
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualOverstockPartsPlatFormBill>().Select(r => r.Id).Distinct().ToArray();
                        dcsDomainContext.ExportOverstockPartsPlatFormBills(ids, centerName, dealerName, sparePartCode, sparePartName, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    } else {
                        dcsDomainContext.ExportOverstockPartsPlatFormBills(null, centerName, dealerName, sparePartCode, sparePartName, loadOp => {
                            if (loadOp.HasError)
                                return;
                            if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "Buy":
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var buys = this.DataGridView.SelectedEntities.Cast<VirtualOverstockPartsPlatFormBill>().ToList();
                        if (buys.Any(r => r.DealerId == BaseApp.Current.CurrentUserData.EnterpriseId) || buys.Any(r => r.DealerId == null && r.CenterId == BaseApp.Current.CurrentUserData.EnterpriseId))
                            return false;
                        if (buys.Any(r => r.Quantity <= default(int)))
                            return false;
                        return true;
                    }
                    return false;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "Upload":
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var uploads = this.DataGridView.SelectedEntities.Cast<VirtualOverstockPartsPlatFormBill>().ToList();
                        if (uploads.Count() > 1) 
                            return false;
                        if (uploads.First().DealerId.HasValue && uploads.First().DealerId.Value == BaseApp.Current.CurrentUserData.EnterpriseId)
                            return true;
                        if (uploads.First().CenterId.HasValue && uploads.First().DealerId == null && uploads.First().CenterId.Value == BaseApp.Current.CurrentUserData.EnterpriseId)
                            return true;
                    }
                    return false;
                case "Edit":
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var entities = this.DataGridView.SelectedEntities.Cast<VirtualOverstockPartsPlatFormBill>().ToList();
                        if (entities.Count() > 1) 
                            return false;
                        if (entities.First().DealerId.HasValue && entities.First().DealerId.Value == BaseApp.Current.CurrentUserData.EnterpriseId)
                            return true;
                        if (entities.First().CenterId.HasValue && entities.First().DealerId == null && entities.First().CenterId.Value == BaseApp.Current.CurrentUserData.EnterpriseId)
                            return true;
                    }
                    return false;
                case "Delete":
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var entities = this.DataGridView.SelectedEntities.Cast<VirtualOverstockPartsPlatFormBill>().ToList();
                        if (entities.Count() > 1) 
                            return false;
                        if (entities.First().DealerId.HasValue && entities.First().DealerId.Value == BaseApp.Current.CurrentUserData.EnterpriseId)
                            return true;
                        if (entities.First().CenterId.HasValue && entities.First().DealerId == null && entities.First().CenterId.Value == BaseApp.Current.CurrentUserData.EnterpriseId)
                            return true;
                    }
                    return false;
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "OverstockPartsPlatFormBill"
                };
            }
        }
    }
}