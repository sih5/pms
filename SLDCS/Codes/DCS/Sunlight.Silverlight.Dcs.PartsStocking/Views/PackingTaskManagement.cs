﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsInStore", "PackingTask", ActionPanelKeys = new[] {
        "PackingTask", CommonActionKeys.MERGEEXPORT })]
    public class PackingTaskManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        public PackingTaskManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.QueryPanel_Title_PackingTask;
        }
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PackingTask"));
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PackingTask");
                    //去掉之后
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_EDIT_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void ResetEditView() {
            this.dataEditView = null;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PackingTask"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Packing":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any()) {
                        this.SwitchViewTo(DATA_EDIT_VIEW);
                        break;
                    } else {
                        var pickList = this.DataGridView.SelectedEntities.Cast<PackingTaskQuery>().FirstOrDefault();
                        if(pickList.Status != (int)DcsPackingTaskStatus.新增 && (int)pickList.Status != (int)DcsPackingTaskStatus.部分包装) {
                            UIHelper.ShowNotification(PartsStockingUIStrings.Management_Packingtask_Status);
                            return;
                        }
                        this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                        this.SwitchViewTo(DATA_EDIT_VIEW);
                        break;
                    }
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PackingTaskQuery>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.ExportDealerPartsTransferOrder(ids, null, null, null, null, null, null, null, null, null, null, null, null,null,null,null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var partsInboundPlanCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsInboundPlanCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsInboundPlanCode").Value as string;
                        var partsInboundCheckBillCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsInboundCheckBillCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsInboundCheckBillCode").Value as string;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "ReferenceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "ReferenceCode").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var sparePartCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        var inboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InboundType").Value as int?;
                        var modifierName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "ModifierName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "ModifierName").Value as string;
                        var hasDifference = filterItem.Filters.SingleOrDefault(r => r.MemberName == "HasDifference") == null ? null : filterItem.Filters.Single(r => r.MemberName == "HasDifference").Value as bool?; 

                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? expectedPlaceDateBegin = null;
                        DateTime? expectedPlaceDateEnd = null;

                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "ModifyTime") {
                                    expectedPlaceDateBegin = dateTime.Filters.First(r => r.MemberName == "ModifyTime").Value as DateTime?;
                                    expectedPlaceDateEnd = dateTime.Filters.Last(r => r.MemberName == "ModifyTime").Value as DateTime?;
                                }
                            }
                        }

                        this.ExportDealerPartsTransferOrder(null, code, partsInboundPlanCode, partsInboundCheckBillCode, sourceCode, status, expectedPlaceDateBegin, expectedPlaceDateEnd, warehouseId, createTimeBegin, createTimeEnd, sparePartCode, sparePartName, inboundType, modifierName,hasDifference);
                    }
                    break;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public void ExportDealerPartsTransferOrder(int[] ids, string code, string partsInboundPlanCode, string partsInboundCheckBillCode,
            string sourceCode, int? status, DateTime? expectedPlaceDateBegin, DateTime? expectedPlaceDateEnd,
            int? warehouseId, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, string sparePartName, int? inboundType, string modifierName,bool? hasDifference) {
                ShellViewModel.Current.IsBusy = true;
                this.excelServiceClient.ExportPackingTaskAsync(ids, code, partsInboundPlanCode, partsInboundCheckBillCode, sourceCode, status, expectedPlaceDateBegin, expectedPlaceDateEnd, warehouseId, createTimeBegin, createTimeEnd, sparePartCode, sparePartName, inboundType, modifierName,hasDifference);
            this.excelServiceClient.ExportPackingTaskCompleted -= ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted;
            this.excelServiceClient.ExportPackingTaskCompleted += ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted;
        }
        private void ExcelServiceClient_ExportDealerPartsTransferOrderAndDetailCompleted(object sender, ExportPackingTaskCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            else return true;
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
