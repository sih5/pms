﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "WarehouseExecution", "PartsDifferenceBackBill", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_SUBMIT_ABANDON_INITIALAPPROVE_AUDIT_APPROVE_MERGEEXPORT
    })]
    public class PartsDifferenceBackBillManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataApproveView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private const string DATA_APPROVE_VIEW = "_dataApproveView_";
        public PartsDifferenceBackBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsDifferenceBackBill;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsDifferenceBackBill");
                    // this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsDifferenceBackBill");
                    dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("PartsDifferenceBackBillApprove");
                    this.dataApproveView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void ResetEditView() {
            this.dataEditView = null;
            this.dataApproveView = null;
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsDifferenceBackBill"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsDifferenceBackBill = this.DataEditView.CreateObjectToEdit<PartsDifferenceBackBill>();
                    partsDifferenceBackBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsDifferenceBackBill.Type = (int)DCSPartsDifferenceBackBillType.包装;
                    partsDifferenceBackBill.Status = (int)DCSPartsDifferenceBackBillStatus.新增;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsDifferenceBackBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交配件差异回退单)
                                entity.提交配件差异回退单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                this.CheckActionsCanExecute();
                                if(this.DataGridView.FilterItem != null)
                                    this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entityAbandon = this.DataGridView.SelectedEntities.Cast<PartsDifferenceBackBill>().FirstOrDefault();
                        if(entityAbandon == null)
                            return;
                        dcsDomainContext.Load(dcsDomainContext.GetPartsDifferenceBackBillsQuery().Where(ex => ex.Id == entityAbandon.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            var items = loadOp.Entities.SingleOrDefault();
                            if(items == null)
                                return;
                            try {
                                if(items.Can作废配件差异回退单)
                                    items.作废配件差异回退单();
                                dcsDomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        dcsDomainContext.RejectChanges();
                                        return;
                                    }
                                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                    this.CheckActionsCanExecute();
                                    if(this.DataGridView.FilterItem != null)
                                        this.DataGridView.ExecuteQueryDelayed();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case CommonActionKeys.INITIALAPPROVE:
                case CommonActionKeys.AUDIT:
                case CommonActionKeys.APPROVE:
                    var conApprove = this.DataGridView.SelectedEntities.Cast<PartsDifferenceBackBill>().FirstOrDefault();
                    this.DataApproveView.SetObjectToEditById(conApprove.GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 合并导出参数为 出库计划ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsDifferenceBackBill>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null, null, null);
                    }
                    else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                       // var packingCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PackingCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PackingCode").Value as string;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var type = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Type") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Type").Value as int?;
                        //var inPlanCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InPlanCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InPlanCode").Value as string;
                        var shelvesCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "TaskCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "TaskCode").Value as string;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? beginSubmitTime = null;
                        DateTime? endSubmitTime = null;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "SubmitTime") {
                                    beginSubmitTime = dateTime.Filters.First(r => r.MemberName == "SubmitTime").Value as DateTime?;
                                    endSubmitTime = dateTime.Filters.Last(r => r.MemberName == "SubmitTime").Value as DateTime?;
                                }
                            }
                        }
                        this.ExecuteMergeExport(null, code, status,type, shelvesCode, null, null, createTimeBegin, createTimeEnd, beginSubmitTime, endSubmitTime);
                    }
                    break;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int[] ids, string code, int? status, int? type, string shelvesCode, string packingCode, string inPlanCode, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? bSubmitTime, DateTime? eSubmitTime) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsDifferenceBackBillWithDetailAsync(ids, code, status, type, shelvesCode, packingCode, inPlanCode, createTimeBegin, createTimeEnd, bSubmitTime, eSubmitTime);
            this.excelServiceClient.ExportPartsDifferenceBackBillWithDetailCompleted -= this.ExcelServiceClient_ExportPartsDifferenceBackBillWithDetailCompleted;
            this.excelServiceClient.ExportPartsDifferenceBackBillWithDetailCompleted += this.ExcelServiceClient_ExportPartsDifferenceBackBillWithDetailCompleted;
        }

        private void ExcelServiceClient_ExportPartsDifferenceBackBillWithDetailCompleted(object sender, ExportPartsDifferenceBackBillWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsDifferenceBackBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DCSPartsDifferenceBackBillStatus.新增;
                case CommonActionKeys.INITIALAPPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesIniti = this.DataGridView.SelectedEntities.Cast<PartsDifferenceBackBill>().ToArray();
                    return !entitiesIniti.Any(o => o.Status != (int)DCSPartsDifferenceBackBillStatus.提交) && entitiesIniti.Length > 0;
                case CommonActionKeys.AUDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesAud = this.DataGridView.SelectedEntities.Cast<PartsDifferenceBackBill>().ToArray();
                    return !entitiesAud.Any(o => o.Status != (int)DCSPartsDifferenceBackBillStatus.初审通过) && entitiesAud.Length > 0;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitiesApp = this.DataGridView.SelectedEntities.Cast<PartsDifferenceBackBill>().ToArray();
                    return !entitiesApp.Any(o => o.Status != (int)DCSPartsDifferenceBackBillStatus.审核通过) && entitiesApp.Length > 0;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
