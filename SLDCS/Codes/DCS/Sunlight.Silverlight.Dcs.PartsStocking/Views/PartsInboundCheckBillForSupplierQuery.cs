﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsInStore", "PartsInboundCheckBillForSupplierQuery", ActionPanelKeys = new[] { CommonActionKeys.EXPORT_MERGEEXPORT_PRINT })]
    public class PartsInboundCheckBillForSupplierQuery : DcsDataManagementViewBase {

        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsInboundCheckBillWithDetailsForSupplier"));
            }
        }

        public PartsInboundCheckBillForSupplierQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsInboundCheckBillForSupplierQuery;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsInboundCheckBillForSupplier"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsInboundCheckBill>().ToArray();
                    return selectItems.Length == 1;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {

                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    var entities = this.DataGridView.SelectedEntities;
                    var cpPartsPurchaseOrderCode = filterItem.Filters.Single(e => e.MemberName == "CPPartsPurchaseOrderCode").Value as string;
                    var cpPartsInboundCheckCode = filterItem.Filters.Single(e => e.MemberName == "CPPartsInboundCheckCode").Value as string;
                    int? id = null;
                    if(entities != null && entities.Any())
                        id = entities.Cast<PartsInboundCheckBill>().First().Id;
                    if(uniqueId.Equals(CommonActionKeys.EXPORT)) {
                        this.dcsDomainContext.ExportPartsInboundCheckBill(id != null ? new[] { id.Value } : new int[] { }, code, createTimeBegin, createTimeEnd, cpPartsPurchaseOrderCode, cpPartsInboundCheckCode, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    }
                    if(uniqueId.Equals(CommonActionKeys.MERGEEXPORT)) {
                        this.ExportPartsInboundCheckBillWithDetails(id != null ? new[] { id.Value } : new int[] { }, code, createTimeBegin, createTimeEnd, cpPartsPurchaseOrderCode, cpPartsInboundCheckCode);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsInboundCheckBill>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new PartsInboundCheckBillPrintWindow {
                        Header = PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsInboundCheckBill,
                        PartsInboundCheckBillId = selectedItem.Id
                    };
                    printWindow.ShowDialog();
                    break;

            }
        }

        private void ExportPartsInboundCheckBillWithDetails(int[] id, string partsInboundCheckBillCode, DateTime? createTimeBegin, DateTime? createTimeEnd, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsInboundCheckBillWithDetailForSupplierAsync(id, null, null, null, null, partsInboundCheckBillCode, null, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null, createTimeBegin, createTimeEnd, null, null, cpPartsPurchaseOrderCode, cpPartsInboundCheckCode,null,null);
            this.excelServiceClient.ExportPartsInboundCheckBillWithDetailForSupplierCompleted -= excelServiceClient_ExportPartsInboundCheckBillWithDetailForSupplierCompleted;
            this.excelServiceClient.ExportPartsInboundCheckBillWithDetailForSupplierCompleted += excelServiceClient_ExportPartsInboundCheckBillWithDetailForSupplierCompleted;
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected void excelServiceClient_ExportPartsInboundCheckBillWithDetailForSupplierCompleted(object sender, ExportPartsInboundCheckBillWithDetailForSupplierCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "CounterpartCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "StorageCompanyType",
                MemberType = typeof(int),
                Value = (int)DcsCompanyType.分公司,
                Operator = FilterOperator.IsEqualTo
            });
            compositeFilterItem.LogicalOperator = LogicalOperator.And;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
