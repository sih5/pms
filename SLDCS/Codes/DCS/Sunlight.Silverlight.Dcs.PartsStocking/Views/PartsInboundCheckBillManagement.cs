﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsInStore", "PartsInboundCheckBill", ActionPanelKeys = new[] {
        "PartsInboundCheckBill", CommonActionKeys.DETAIL_TERMINATE_MERGEEXPORT,"PartsInboundCheckQueryPrintLabel" })]
    public class PartsInboundCheckBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase checkInboundDataEdtiView;
        private DataEditViewBase dataDetailView;
        private const string DATA_DETAIL_VIEW = "_dataDetailView_";

        public PartsInboundCheckBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsInboundCheckBill;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsInboundPlanWithDetails"));
            }
        }

        private DataEditViewBase CheckInboundDataEdtiView {
            get {
                if(this.checkInboundDataEdtiView == null) {
                    this.checkInboundDataEdtiView = DI.GetDataEditView("PartsInboundCheckBill");
                    this.checkInboundDataEdtiView.EditSubmitted += this.CheckInboundDataEdtiView_EditSubmitted;
                    this.checkInboundDataEdtiView.EditCancelled += this.DataEdtiView_EditCancelled;
                }
                return this.checkInboundDataEdtiView;
            }
        }

        private DataEditViewBase DataDetailView {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsInboundPlanDetail");
                    this.dataDetailView.EditCancelled += this.DataEdtiView_EditCancelled;
                }
                return dataDetailView;
            }
        }
        private void ResetEditView() {
            this.checkInboundDataEdtiView = null;
            this.dataDetailView = null;
        }
        private void DataEdtiView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void CheckInboundDataEdtiView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.CheckInboundDataEdtiView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsInboundPlan"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.DETAIL:
                    this.DataDetailView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
                case CommonActionKeys.TERMINATE:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Terminate, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsInboundPlan>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can终止配件入库计划)
                                entity.终止配件入库计划();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_TerminateSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 合并导出参数为 出库计划ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsInboundPlan>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null,null,null,null);
                    }
                        //否则 合并导出参数为 出库计划编号 源单据编号 仓库名称	出库类型	创建时间	对方单位编号	对方单位名称
                    else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var inboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InboundType").Value as int?;
                        var counterpartCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyCode").Value as string;
                        var counterpartCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyName").Value as string;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }
                        this.ExecuteMergeExport(null, code, sourceCode, warehouseId, inboundType, createTimeBegin, createTimeEnd, counterpartCompanyCode, counterpartCompanyName,null,null,null);
                    }
                    break;
                case "CheckInbound":
                    this.CheckInboundDataEdtiView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.PRINT:
                case "Partition":
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsInboundPlan>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    if(uniqueId == CommonActionKeys.PRINT) {
                        BasePrintWindow printWindow = new PartsInboundPlanPrintWindow {
                            Header = PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsInboundPlan,
                            PartsInboundPlan = selectedItem
                        };
                        printWindow.ShowDialog();
                    } else {
                        BasePrintWindow printWindow = new PartsInboundPlanPartitionPrintWindow {
                            Header = PartsStockingUIStrings.Action_Title_PartitionPrint,
                            PartsInboundPlan = selectedItem
                        };
                        printWindow.ShowDialog();
                    }

                    break;
                case "PrintLabel":
                    var partsInboundPlanIds = this.DataGridView.SelectedEntities.Select(r => (int)r.GetIdentity()).ToArray();
                    this.PrintLabelForPartsInboundPlanDataEditView.SetObjectToEditById(partsInboundPlanIds);
                    LabelPrintWindow.ShowDialog();
                    break;
                case "UsedPrintLabel":
                    var usedpartsInboundPlanIds = this.DataGridView.SelectedEntities.Select(r => (int)r.GetIdentity()).ToArray();
                    this.usedPrintLabelForPartsInboundPlanDataEditView.SetObjectToEditById(usedpartsInboundPlanIds);
                    usedLabelPrintWindow.ShowDialog();
                    break;
                case "UsedWMSPrintLabel":
                case "WMSPrintLabel":
                case "StandardPrintLabel":
                    var partsInboundPlans = this.DataGridView.SelectedEntities.Select(r => (int)r.GetIdentity()).ToArray();
                    var view = this.PrintWmsLabelForPartsInboundPlanDataEditView as PrintWmsLabelForPartsInboundPlanDataEditView;
                    view.UniqueId = uniqueId;
                    this.PrintWmsLabelForPartsInboundPlanDataEditView.SetObjectToEditById(partsInboundPlans);
                    WmsLabelPrintWindow.ShowDialog();
                    break;
            }
        }


        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.DETAIL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItem1 = this.DataGridView.SelectedEntities.Cast<PartsInboundPlan>().ToArray();
                    return selectItem1.Length == 1;
                case CommonActionKeys.TERMINATE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<PartsInboundPlan>().ToArray();
                    if(entitie[0].InboundType == (int)DcsPartsInboundType.配件调拨)
                        return false;
                    return entitie[0].Status == (int)DcsPartsInboundPlanStatus.新建 || entitie[0].Status == (int)DcsPartsInboundPlanStatus.部分检验;
                case "CheckInbound":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsInboundPlan>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if((String.CompareOrdinal(uniqueId, CommonActionKeys.TERMINATE) == 0))
                        return entities[0].InboundType == (int)DcsPartsInboundType.内部领入;
                    return true;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                case "PrintLabel":
                case "UsedPrintLabel":
                case "Partition":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsInboundPlan>().ToArray();
                    return selectItems.Length == 1;
                case "WMSPrintLabel":
                case "UsedWMSPrintLabel":
                case "StandardPrintLabel":
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any();
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int[] ids, string code, string sourceCode, int? warehouseId, int? inboundType, DateTime? begainDate, DateTime? endDate, string counterpartCompanyCode, string counterpartCompanyName,string sparePartCode,string sparePartName,string originalRequirementBillCode ) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsInboundPlanWithDetailAsync(ids, null, BaseApp.Current.CurrentUserData.UserId, null, BaseApp.Current.CurrentUserData.EnterpriseId, code, sourceCode, warehouseId, inboundType, begainDate, endDate, null, null, counterpartCompanyCode, counterpartCompanyName,sparePartCode,sparePartName,originalRequirementBillCode);
            this.excelServiceClient.ExportPartsInboundPlanWithDetailCompleted -= this.ExcelServiceClient_ExportPartsInboundPlanWithDetailCompleted;
            this.excelServiceClient.ExportPartsInboundPlanWithDetailCompleted += this.ExcelServiceClient_ExportPartsInboundPlanWithDetailCompleted;
        }

        private void ExcelServiceClient_ExportPartsInboundPlanWithDetailCompleted(object sender, ExportPartsInboundPlanWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            //TODO:查询时间范围需要保留时间 23:59:59
            var newCompositeFilter = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                var createTimeFilters = compositeFilter.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                }
                newCompositeFilter.Filters.Add(compositeFilter);
            } else
                newCompositeFilter.Filters.Add(filterItem);
            //newCompositeFilter.Filters.Add(new FilterItem {
            //    MemberName = "StorageCompanyId",
            //    MemberType = typeof(int),
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
            //    Operator = FilterOperator.IsEqualTo
            //});
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = newCompositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }
        //
        #region 配件标签打印
        private RadWindow labelPrintWindow;
        private DataEditViewBase printLabelForPartsInboundPlanDataEditView;

        private DataEditViewBase PrintLabelForPartsInboundPlanDataEditView {
            get {
                return this.printLabelForPartsInboundPlanDataEditView ?? (this.printLabelForPartsInboundPlanDataEditView = DI.GetDataEditView("PrintLabelPartsInboundCheckBill"));
            }
        }

        private RadWindow LabelPrintWindow {
            get {
                return this.labelPrintWindow ?? (this.labelPrintWindow = new RadWindow {
                    Content = this.PrintLabelForPartsInboundPlanDataEditView,
                    Header = PartsStockingUIStrings.DataEditView_Title_PartInformation,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }
        #endregion

        #region 配件标签打印旧
        private RadWindow usedlabelPrintWindow;
        private DataEditViewBase usedprintLabelForPartsInboundPlanDataEditView;

        private DataEditViewBase usedPrintLabelForPartsInboundPlanDataEditView {
            get {
                return this.usedprintLabelForPartsInboundPlanDataEditView ?? (this.usedprintLabelForPartsInboundPlanDataEditView = DI.GetDataEditView("PrintLabelForPartsInboundPlan"));
            }
        }

        private RadWindow usedLabelPrintWindow {
            get {
                return this.usedlabelPrintWindow ?? (this.usedlabelPrintWindow = new RadWindow {
                    Content = this.usedPrintLabelForPartsInboundPlanDataEditView,
                    Header = PartsStockingUIStrings.DataEditView_Title_PartInformation,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }
        #endregion

        #region WMS配件标签打印
        private RadWindow wmsLabelPrintWindow;
        private DataEditViewBase printWmsLabelForPartsInboundPlanDataEditView;
        private DataEditViewBase PrintWmsLabelForPartsInboundPlanDataEditView {
            get {
                return this.printWmsLabelForPartsInboundPlanDataEditView ?? (this.printWmsLabelForPartsInboundPlanDataEditView = DI.GetDataEditView("PrintWmsLabelForPartsInboundPlan"));
            }
        }
        private RadWindow WmsLabelPrintWindow {
            get {
                return this.wmsLabelPrintWindow ?? (this.wmsLabelPrintWindow = new RadWindow {
                    Content = this.PrintWmsLabelForPartsInboundPlanDataEditView,
                    Header = PartsStockingUIStrings.DataEditView_Title_PartInformation,
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    CanClose = true
                });
            }
        }
        #endregion
    }
}
