﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using System.Windows;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views
{
    [PageMeta("PartsStockingAndLogistics", "PartsInStore", "PartsInboundCheckBillQuery", ActionPanelKeys = new[] {
       CommonActionKeys.EXPORT_MERGEEXPORT,  "PartsInboundCheckBillQuery",CommonActionKeys.PRINT
        //"PartsInboundCheckQueryPrintLabel"
    })]
    public class PartsInboundCheckBillQuery : DcsDataManagementViewBase
    {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        private DataEditViewBase DataEditView
        {
            get
            {
                if (this.dataEditView == null)
                {
                    this.dataEditView = DI.GetDataEditView("PartsInboundPackingDetailForDetail");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e)
        {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsInboundCheckBillNewWithDetails"));
            }
        }

        public PartsInboundCheckBillQuery()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsInboundCheckBillQuery;
        }

        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PartsInboundCheckBillNewWith"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "PackingDetail":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBill>().ToArray();
                    return entities.Length == 1;
                case "PcPrint":
                case "ChPrint":
                case "EnPrint":
                case "PcEnPrint":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    return true;
                case "PrintForGC":
                case CommonActionKeys.PRINT:
                     if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBill>().ToArray();
                    return selectItems.Length == 1;
                case "NotSettle":
                case "Settle":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItem = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBill>().ToArray();
                    if (selectItem.Length != 1)
                        return false;
                    if (uniqueId.Equals("NotSettle"))
                        return selectItem.First().SettlementStatus == (int)DcsPartsSettlementStatus.待结算 && selectItem.First().InboundType == (int)DcsPartsInboundType.配件采购;
                    return selectItem.First().SettlementStatus == (int)DcsPartsSettlementStatus.不结算 && selectItem.First().InboundType == (int)DcsPartsInboundType.配件采购;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var originalRequirementBillCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OriginalRequirementBillCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OriginalRequirementBillCode").Value as string;
                    var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                    var inboundType = filterItem.Filters.Single(e => e.MemberName == "InboundType").Value as int?;
                    var counterpartCompanyName = filterItem.Filters.Single(e => e.MemberName == "CounterpartCompanyName").Value as string;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    var sparePartCode = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var sparePartName = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SparePartName") == null ? null : filterItem.Filters.Single(e => e.MemberName == "SparePartName").Value as string;
                    var partsInboundPlanCode = filterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsInboundPlanCode") == null ? null : filterItem.Filters.Single(e => e.MemberName == "PartsInboundPlanCode").Value as string;
                    var batchNumber = filterItem.Filters.SingleOrDefault(e => e.MemberName == "BatchNumber") == null ? null : filterItem.Filters.Single(e => e.MemberName == "BatchNumber").Value as string;
                    var hasDifference = filterItem.Filters.SingleOrDefault(r => r.MemberName == "HasDifference") == null ? null : filterItem.Filters.Single(r => r.MemberName == "HasDifference").Value as bool?;   

                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if (createTime != null)
                    {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    var settlementStatus = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SettlementStatus") == null ? null : filterItem.Filters.Single(e => e.MemberName == "SettlementStatus").Value as int?;
                    var entities = this.DataGridView.SelectedEntities;
                    int[] id = null;
                    if (entities != null && entities.Any())
                    {
                        id = entities.Cast<VirtualPartsInboundCheckBill>().Select(r => r.Id).ToArray();
                    }
                    if (uniqueId.Equals(CommonActionKeys.EXPORT))
                    {
                        this.dcsDomainContext.ExportPartsInboundCheckBillNotSupplier(id, originalRequirementBillCode, code, warehouseId, inboundType, createTimeBegin, createTimeEnd, counterpartCompanyName, settlementStatus,sparePartName,sparePartName,partsInboundPlanCode,batchNumber, loadOp =>
                        {
                            if (loadOp.HasError)
                                return;
                            if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value))
                            {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                            {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    }
                    if (uniqueId.Equals(CommonActionKeys.MERGEEXPORT))
                    {
                        this.ExportPartsInboundCheckBillWithDetails(id, originalRequirementBillCode, code, warehouseId, counterpartCompanyName, inboundType, settlementStatus, createTimeBegin, createTimeEnd, sparePartCode, sparePartName, partsInboundPlanCode,batchNumber,hasDifference);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBill>().FirstOrDefault();
                    if (selectedItem == null)
                        return;
                    //BasePrintWindow printWindow = new PartsInboundCheckBillPrintWindow {
                    //    Header = PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsInboundCheckBill,
                    //    PartsInboundCheckBillId = selectedItem.Id
                    //};
                    //printWindow.ShowDialog();

                    SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataEditView_Text_PrintShelves, "ReportPartsInboundCheckBillNew", null, true, new Tuple<string, string>("paraPartsInboundCheckBillId", selectedItem.Id.ToString()));


                    break;
                case "PrintForGC":
                    var selectedItems = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBill>().FirstOrDefault();
                    if (selectedItems == null)
                        return;
                    BasePrintWindow printWindows = new PartsInboundCheckBillForGcPrintWindow
                    {
                        Header = PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsInboundCheckBill,
                        PartsInboundCheckBillId = selectedItems.Id
                    };
                    printWindows.ShowDialog();
                    break;
                case "PackingDetail":
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "NotSettle":
                case "Settle":
                    var virtualPartsInboundCheckBill = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBill>().SingleOrDefault();
                    if (virtualPartsInboundCheckBill == null)
                        return;
                    var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    if (domainContext == null)
                        return;
                    domainContext.Load(domainContext.GetPartsInboundCheckBillByIdQuery(virtualPartsInboundCheckBill.Id), LoadBehavior.RefreshCurrent, loadOp1 =>
                    {
                        if (loadOp1.HasError)
                            return;
                        if (loadOp1.Entities != null && loadOp1.Entities.Any())
                        {
                            var entity = loadOp1.Entities.FirstOrDefault();
                            if (uniqueId.Equals("NotSettle"))
                            {
                                try
                                {
                                    if (entity.Can更改不结算状态)
                                        entity.更改不结算状态();
                                    this.ExecuteSerivcesMethod();
                                }
                                catch (Exception ex)
                                {
                                    UIHelper.ShowAlertMessage(ex.Message);
                                }
                            }
                            else
                            {
                                try
                                {
                                    if (entity.Can更改待结算状态)
                                        entity.更改待结算状态();
                                    this.ExecuteSerivcesMethod();
                                }
                                catch (Exception ex)
                                {
                                    UIHelper.ShowAlertMessage(ex.Message);
                                }
                            }
                        }
                    }, null);
                    break;
                //箱标中文打印
                case "ChPrint":
                    var billCh = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBill>().ToArray();
                    if (billCh.Count() == 0)
                        return;
                    this.PrintLabelForPartsInboundCheckDataEditViewCh.SetObjectToEditById(billCh.Select(r=>r.Id).ToArray());
                    this.LabelPrintWindowCh.ShowDialog();
                    break;
                //箱标英文打印
                case "EnPrint":
                     var billChEn = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBill>().ToArray();
                     if (billChEn.Count() == 0)
                        return;
                     this.PrintLabelForPartsInboundCheckDataEditViewEn.SetObjectToEditById(billChEn.Select(r => r.Id).ToArray());
                    this.LabelPrintWindowEn.ShowDialog();
                    break;
                //件标打印
                case "PcPrint":
                    var billPc = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBill>().ToArray();
                    if (billPc.Count() == 0)
                        return;
                    this.PrintLabelForPartsInboundCheckDataEditViewCp.SetObjectToEditById(billPc.Select(r => r.Id).ToArray());
                    this.LabelPrintWindowCp.ShowDialog();
                    break;
                //件标英文打印
                case "PcEnPrint":
                     var billPcEn = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundCheckBill>().ToArray();
                     if(billPcEn.Count() == 0)
                        return;
                     this.PrintLabelForPartsInboundCheckDataEditViewCpEn.SetObjectToEditById(billPcEn.Select(r => r.Id).ToArray());
                    this.LabelPrintWindowCpEn.ShowDialog();
                    break;
            }
        }
        //件标打印
        private RadWindow labelPrintWindowCp;
        private DataEditViewBase printLabelForPartsInboundCheckDataEditViewCp;

        private DataEditViewBase PrintLabelForPartsInboundCheckDataEditViewCp
        {
            get
            {
                return this.printLabelForPartsInboundCheckDataEditViewCp ?? (this.printLabelForPartsInboundCheckDataEditViewCp = DI.GetDataEditView("PrintLabelForPartsInboundCheckBillCp"));
            }
        }
        private RadWindow LabelPrintWindowCp
        {
            get
            {
                return this.labelPrintWindowCp ?? (this.labelPrintWindowCp = new RadWindow
                {
                    Content = this.PrintLabelForPartsInboundCheckDataEditViewCp,
                    Header = PartsStockingUIStrings.DataEditView_Title_PartInformation,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                   
                });
            }
        }
        //件标英文打印
        private RadWindow labelPrintWindowCpEn;
        private DataEditViewBase printLabelForPartsInboundCheckDataEditViewCpEn;

        private DataEditViewBase PrintLabelForPartsInboundCheckDataEditViewCpEn {
            get {
                return this.printLabelForPartsInboundCheckDataEditViewCpEn ?? (this.printLabelForPartsInboundCheckDataEditViewCpEn = DI.GetDataEditView("PrintLabelForPartsInboundCheckBillCpEn"));
            }
        }
        private RadWindow LabelPrintWindowCpEn {
            get {
                return this.labelPrintWindowCpEn ?? (this.labelPrintWindowCpEn = new RadWindow {
                    Content = this.PrintLabelForPartsInboundCheckDataEditViewCpEn,
                    Header = PartsStockingUIStrings.DataEditView_Title_PartInformation,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,

                });
            }
        }
        // 中文打印
        private RadWindow labelPrintWindowCh;
        private DataEditViewBase printLabelForPartsInboundCheckDataEditViewCh;

        private DataEditViewBase PrintLabelForPartsInboundCheckDataEditViewCh
        {
            get
            {
                return this.printLabelForPartsInboundCheckDataEditViewCh ?? (this.printLabelForPartsInboundCheckDataEditViewCh = DI.GetDataEditView("PrintLabelForPartsInboundCheckBillCh"));
            }
        }
        private RadWindow LabelPrintWindowCh
        {
            get
            {
                return this.labelPrintWindowCh ?? (this.labelPrintWindowCh = new RadWindow
                {
                    Content = this.PrintLabelForPartsInboundCheckDataEditViewCh,
                    Header = PartsStockingUIStrings.DataEditView_Title_PartInformation,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                   
                });
            }
        }
        // 英文打印
        private RadWindow labelPrintWindowEn;
        private DataEditViewBase printLabelForPartsInboundCheckDataEditViewEn;

        private DataEditViewBase PrintLabelForPartsInboundCheckDataEditViewEn
        {
            get
            {
                return this.printLabelForPartsInboundCheckDataEditViewEn ?? (this.printLabelForPartsInboundCheckDataEditViewEn = DI.GetDataEditView("PrintLabelForPartsInboundCheckBillEn"));
            }
        }
        private RadWindow LabelPrintWindowEn
        {
            get
            {
                return this.labelPrintWindowEn ?? (this.labelPrintWindowEn = new RadWindow
                {
                    Content = this.PrintLabelForPartsInboundCheckDataEditViewEn,
                    Header = PartsStockingUIStrings.DataEditView_Title_PartInformation,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,                  
                });
            }
        }
        private void ExecuteSerivcesMethod()
        {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if (domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp =>
            {
                if (submitOp.HasError)
                {
                    if (!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                this.CheckActionsCanExecute();
            }, null);
        }

        private void ExportPartsInboundCheckBillWithDetails(int[] id, string originalRequirementBillCode, string partsInboundCheckBillCode, int? warehouseId, string counterpartcompanyname, int? inboundType, int? settlementStatus, DateTime? createTimeBegin, DateTime? createTimeEnd, string sparePartCode, string sparePartName, string partsInboundPlanCode, string batchNumber, bool? hasDifference)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsInboundCheckBillWithDetailAsync(id, originalRequirementBillCode, BaseApp.Current.CurrentUserData.UserId, BaseApp.Current.CurrentUserData.EnterpriseId, partsInboundCheckBillCode, warehouseId, null, counterpartcompanyname, inboundType, settlementStatus, createTimeBegin, createTimeEnd, sparePartCode, sparePartName, partsInboundPlanCode, batchNumber, hasDifference);
            this.excelServiceClient.ExportPartsInboundCheckBillWithDetailCompleted -= excelServiceClient_ExportPartsInboundCheckBillWithDetailCompleted;
            this.excelServiceClient.ExportPartsInboundCheckBillWithDetailCompleted += excelServiceClient_ExportPartsInboundCheckBillWithDetailCompleted;
        }

        protected void excelServiceClient_ExportPartsInboundCheckBillWithDetailCompleted(object sender, ExportPartsInboundCheckBillWithDetailCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.DataGridView.DomainContext.RejectChanges();
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            //compositeFilterItem.Filters.Add(new FilterItem {
            //    MemberName = "StorageCompanyId",
            //    MemberType = typeof(int),
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
            //    Operator = FilterOperator.IsEqualTo
            //});
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}