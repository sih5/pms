﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views
{
    [PageMeta("PartsStockingAndLogistics", "PartsInStore", "PartsInboundPlanForWarehouseQuery", ActionPanelKeys = new[] {
        "PartsInboundPlanQuery"
    })]
    public class PartsInboundPlanForWarehouseQuery : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;

        public PartsInboundPlanForWarehouseQuery()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsInboundPlanForWarehouseQuery;
        }

        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PartsInboundPlanQuery"
                };
            }
        }

        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsInboundPlanForWarehouseQuery"));
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                case "Partition":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsInboundPlan>().ToArray();
                    return selectItems.Length == 1;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();              
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {

                case CommonActionKeys.PRINT:
                case "Partition":
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsInboundPlan>().FirstOrDefault();
                    if (selectedItem == null)
                        return;
                    if (uniqueId == CommonActionKeys.PRINT)
                    {
                        SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsInboundPlan, "ReportPartsInboundPlan1", null, true, new Tuple<string, string>("partsInboundPlanId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

                    }
                    else
                    {
                        PartsInboundPlan print = new PartsInboundPlan
                        {
                            Id=selectedItem.Id
                        };
                        BasePrintWindow printWindow = new PartsInboundPlanPartitionPrintWindow
                        {
                            Header = PartsStockingUIStrings.Action_Title_PartitionPrint,
                            PartsInboundPlan = print
                        };
                        printWindow.ShowDialog();
                    }
                    break;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 合并导出参数为 入库计划ID 
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var id = this.DataGridView.SelectedEntities.Cast<PartsInboundPlan>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;

                        this.ExecuteMergeExport(id, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,null);
                    }
                    else
                    {

                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        //var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var inboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InboundType").Value as int?;
                        var counterpartCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyCode").Value as string;
                        var sparepartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                        var sparepartName = filterItem.Filters.Single(e => e.MemberName == "SparePartName").Value as string;
                        var originalRequirementBillCode = filterItem.Filters.Single(e => e.MemberName == "OriginalRequirementBillCode").Value as string;
                        var hasDifference = filterItem.Filters.SingleOrDefault(r => r.MemberName == "HasDifference") == null ? null : filterItem.Filters.Single(r => r.MemberName == "HasDifference").Value as bool?;   

                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? planDeliveryTimeBegin = null;
                        DateTime? planDeliveryTimeEnd = null;

                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                        {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if (dateTime.Filters.First().MemberName == "CreateTime")
                                {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if (dateTime.Filters.First().MemberName == "PlanDeliveryTime")
                                {
                                    planDeliveryTimeBegin = dateTime.Filters.First(r => r.MemberName == "PlanDeliveryTime").Value as DateTime?;
                                    planDeliveryTimeEnd = dateTime.Filters.Last(r => r.MemberName == "PlanDeliveryTime").Value as DateTime?;
                                }
                            }
                        }

                        this.ExecuteMergeExport(null, null, status, code, sourceCode, null, inboundType, createTimeBegin, createTimeEnd, counterpartCompanyCode, null, null, null, null, null, sparepartCode, sparepartName, originalRequirementBillCode, planDeliveryTimeBegin, planDeliveryTimeEnd, hasDifference);
                    }
                    break;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int[] id, int? partsSalesCategoryId, int? status, string code, string sourceCode, int? warehouseId, int? inboundType, DateTime? begianDate, DateTime? endDate, string counterpartCompanyCode, string counterpartCompanyName, string eRPOrderCode, string partsSalesOrderCode, int? partsPurchaseOrderTypeId, string SAPPurchasePlanCode, string sparepartCode, string sparepartName, string originalRequirementBillCode, DateTime? planDeliveryTimeBegin, DateTime? planDeliveryTimeEnd, bool? hasDifference)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsInboundPlanWithDetailForWarehouseAsync(id, partsSalesCategoryId, status, BaseApp.Current.CurrentUserData.UserId, BaseApp.Current.CurrentUserData.EnterpriseId, code, sourceCode, warehouseId, inboundType, begianDate, endDate, counterpartCompanyCode, counterpartCompanyName, eRPOrderCode, partsSalesOrderCode, partsPurchaseOrderTypeId, SAPPurchasePlanCode, sparepartCode, sparepartName, originalRequirementBillCode, planDeliveryTimeBegin, planDeliveryTimeEnd, hasDifference);
            this.excelServiceClient.ExportPartsInboundPlanWithDetailForWarehouseCompleted -= this.ExcelServiceClient_ExportPartsInboundPlanWithDetailForQueryCompleted;
            this.excelServiceClient.ExportPartsInboundPlanWithDetailForWarehouseCompleted += this.ExcelServiceClient_ExportPartsInboundPlanWithDetailForQueryCompleted;
        }


        private void ExcelServiceClient_ExportPartsInboundPlanWithDetailForQueryCompleted(object sender, ExportPartsInboundPlanWithDetailForWarehouseCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.DataGridView.DomainContext.RejectChanges();
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            compositeFilterItem.Filters.Add(new FilterItem
            {
                MemberName = "StorageCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

    }
}
