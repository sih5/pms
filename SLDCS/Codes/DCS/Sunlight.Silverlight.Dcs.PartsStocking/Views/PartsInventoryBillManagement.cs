﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using System.Windows.Navigation;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "WarehouseExecution", "PartsInventoryBill", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_DETAIL,CommonActionKeys.EXPORT_MERGEEXPORT,CommonActionKeys.PRINT,"PartsInventoryBillForMng"
    })]
    public class PartsInventoryBillManagement : DcsDataManagementViewBase {
        private const string DATA_APPROVEE_VIEW = "_dataApproveView_";
        private const string DATA_RESULTSENTRY_VIEW = "_DataResultEntryView_";
        private const string DATA_INVENTORYCOVERS_VIEW = "_DataInventoryCoversView_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_INITIALAPPROVE_VIEW = "_DataInitialApproveView_";
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataResultEntryView;
        private DataEditViewBase dataInventoryCoversView;
        private DataEditViewBase dataInitialApproveView;
        private DataEditViewBase dataDetailView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public PartsInventoryBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsInventoryBill;
        }

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsInventoryBill"));
            }
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsInventoryBill");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        public DataEditViewBase DataInitialApproveView {
            get {
                if(this.dataInitialApproveView == null) {
                    this.dataInitialApproveView = DI.GetDataEditView("PartsInventoryBillForInitialApprove");
                    ((PartsInventoryBillForInitialApproveDataEditView)this.dataInitialApproveView).EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataInitialApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataInitialApproveView;
            }
        }

        public DataEditViewBase DataResultEntryView {
            get {
                if(this.dataResultEntryView == null) {
                    this.dataResultEntryView = DI.GetDataEditView("PartsInventoryBillForResultEntry");
                    this.dataResultEntryView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataResultEntryView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataResultEntryView;
            }
        }

        public DataEditViewBase DataInventoryCoversView {
            get {
                if(this.dataInventoryCoversView == null) {
                    this.dataInventoryCoversView = DI.GetDataEditView("PartsInventoryBillForInventoryCovers");
                    ((PartsInventoryBillForInventoryCoversDataEditView)this.dataInventoryCoversView).EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataInventoryCoversView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataInventoryCoversView;
            }
        }

        public DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("PartsInventoryBillApprove");
                    ((PartsInventoryBillApproveDataEditView)this.dataApproveView).EditSubmitted += this.dataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        public DataEditViewBase DataDetailView {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsInventoryBillForDetail");
                    this.dataDetailView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]{
                    "PartsInventoryBill"
                };
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataApproveView = null;
            this.dataResultEntryView = null;
            this.dataInventoryCoversView = null;
            this.dataInitialApproveView = null;
            this.dataDetailView = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVEE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_RESULTSENTRY_VIEW, () => this.DataResultEntryView);
            this.RegisterView(DATA_INVENTORYCOVERS_VIEW, () => this.DataInventoryCoversView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailView);
            this.RegisterView(DATA_INITIALAPPROVE_VIEW, () => this.DataInitialApproveView);
        }

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsInventoryBill = this.DataEditView.CreateObjectToEdit<PartsInventoryBill>();
                    partsInventoryBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.新建;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entityAbandon = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().FirstOrDefault();
                        if(entityAbandon == null)
                            return;
                        dcsDomainContext.Load(dcsDomainContext.GetPartsInventoryBillsQuery().Where(ex => ex.Id == entityAbandon.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            var items = loadOp.Entities.SingleOrDefault();
                            if(items == null)
                                return;
                            try {
                                if(items.Can作废配件盘点单)
                                    items.作废配件盘点单();
                                dcsDomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        dcsDomainContext.RejectChanges();
                                        return;
                                    }
                                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                    this.CheckActionsCanExecute();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case CommonActionKeys.DETAIL:
                    this.DataDetailView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
                case "FirstApprove": //初审
                    this.DataInitialApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INITIALAPPROVE_VIEW);
                    break;
                case "Approve":
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVEE_VIEW);
                    break;
                case "Audit": //审核
                    this.DataInitialApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INITIALAPPROVE_VIEW);
                    break;
                case "ResultsInput"://结果录入
                    this.DataResultEntryView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_RESULTSENTRY_VIEW);
                    break;
                case "InventoryCoverage"://库存覆盖
                    this.DataInventoryCoversView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INVENTORYCOVERS_VIEW);
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    dcsDomainContext.Load(dcsDomainContext.GetPartsInventoryBillsQuery().Where(ex => ex.Id == selectedItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var items = loadOp.Entities.SingleOrDefault();
                        if(items == null)
                            return;
                        //BasePrintWindow printWindow = new PartsInventoryBillPrintWindow {
                        //    Header = PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsInventoryBill,
                        //    PartsInventoryBill = items
                        //};
                        SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsInventoryBill, "ReportPartsInventoryBill", null, true, new Tuple<string, string>("partsInventoryBillId", items.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

                        // printWindow.ShowDialog();
                    }, null);
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var id = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().Select(r => r.Id).First();
                        this.ExportPartsInventoryBillWithDetail(id, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var warehouseAreaCategory = filterItem.Filters.Single(e => e.MemberName == "WarehouseAreaCategory").Value as int?;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(filterItem.Filters.Any(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var cTime = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                            foreach(var dateTime in cTime) {
                                var createTime = dateTime as CompositeFilterItem;
                                if(createTime != null) {
                                    if(createTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }
                                }
                            }
                        }
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        this.ExportPartsInventoryBillWithDetail(null, BaseApp.Current.CurrentUserData.EnterpriseId, code, warehouseId, warehouseAreaCategory, status, createTimeBegin, createTimeEnd);
                    }
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var id = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().Select(r => r.Id).First();
                        this.ExportPartsInventoryBill(id, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var warehouseAreaCategory = filterItem.Filters.Single(e => e.MemberName == "WarehouseAreaCategory").Value as int?;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        this.ExportPartsInventoryBill(null, BaseApp.Current.CurrentUserData.EnterpriseId, code, warehouseId, warehouseAreaCategory, status, createTimeBegin, createTimeEnd);
                    }
                    break;
                case "BFirstApprove":
                    if(this.DataGridView.SelectedEntities.Count() == 0) {
                        return;
                    }
                    DcsUtils.Confirm("确认批量初审所选单据？", () => {
                        var Ids = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().Select(e => e.Id).ToArray();
                        try {
                            var gridDomainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(gridDomainContext == null)
                                return;
                            ShellViewModel.Current.IsBusy = true;
                            dcsDomainContext.批量初审盘点单(Ids, loadOpv => {
                                if(loadOpv.HasError) {
                                    if(!loadOpv.IsErrorHandled)
                                        loadOpv.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                    gridDomainContext.RejectChanges();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification("批量初审完成");
                                gridDomainContext.RejectChanges();
                                if(DataGridView != null && DataGridView.FilterItem != null) {
                                    DataGridView.ExecuteQueryDelayed();
                                }
                                CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                        } catch(Exception e) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    });
                    break;
                case "BAudit":
                    if(this.DataGridView.SelectedEntities.Count() == 0) {
                        return;
                    }
                    DcsUtils.Confirm("确认批量审核所选单据？", () => {
                        var Ids = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().Select(e => e.Id).ToArray();
                        try {
                            var gridDomainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(gridDomainContext == null)
                                return;
                            ShellViewModel.Current.IsBusy = true;
                            dcsDomainContext.批量审核盘点单(Ids, loadOpv => {
                                if(loadOpv.HasError) {
                                    if(!loadOpv.IsErrorHandled)
                                        loadOpv.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                    gridDomainContext.RejectChanges();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification("批量审核完成");
                                gridDomainContext.RejectChanges();
                                if(DataGridView != null && DataGridView.FilterItem != null) {
                                    DataGridView.ExecuteQueryDelayed();
                                }
                                CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                        } catch(Exception e) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    });
                    break;
                case "BApprove":
                    if(this.DataGridView.SelectedEntities.Count() == 0) {
                        return;
                    }
                    DcsUtils.Confirm("确认批量审批所选单据？", () => {
                        var Ids = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().Select(e => e.Id).ToArray();
                        try {
                            var gridDomainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(gridDomainContext == null)
                                return;
                            ShellViewModel.Current.IsBusy = true;
                            dcsDomainContext.批量审批盘点单(Ids, loadOpv => {
                                if(loadOpv.HasError) {
                                    if(!loadOpv.IsErrorHandled)
                                        loadOpv.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                    gridDomainContext.RejectChanges();
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification("批量审批完成");
                                gridDomainContext.RejectChanges();
                                if(DataGridView != null && DataGridView.FilterItem != null) {
                                    DataGridView.ExecuteQueryDelayed();
                                }
                                CheckActionsCanExecute();
                                ShellViewModel.Current.IsBusy = false;
                            }, null);
                        } catch(Exception e) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowAlertMessage(e.Message);
                        }
                    });
                    break;
            }
        }
        private void ExportPartsInventoryBillWithDetail(int? id, int storageCompanyId, string partsInventoryBillCode, int? warehouseId, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            if(this.NavigationContext.QueryString.ContainsKey("StorageCompanyType")) {
                var storageCompanyType = this.NavigationContext.QueryString.FirstOrDefault(e => e.Key == "StorageCompanyType").Value.Split(',');
                var storageCompanyTypeids = new int[storageCompanyType.Length];
                for(int i = 0; i < storageCompanyType.Length; i++) {
                    storageCompanyTypeids[i] = Convert.ToInt32(storageCompanyType[i]);
                }
                this.excelServiceClient.ExportPartsInventoryBillWithDetailAsync(id, storageCompanyId, partsInventoryBillCode, warehouseId, warehouseAreaCategory, status, createTimeBegin, createTimeEnd, storageCompanyTypeids);
            } else {
                this.excelServiceClient.ExportPartsInventoryBillWithDetailAsync(id, storageCompanyId, partsInventoryBillCode, warehouseId, warehouseAreaCategory, status, createTimeBegin, createTimeEnd, null);
            }

            this.excelServiceClient.ExportPartsInventoryBillWithDetailCompleted -= excelServiceClient_ExportPartsInventoryBillWithDetailCompleted;
            this.excelServiceClient.ExportPartsInventoryBillWithDetailCompleted += excelServiceClient_ExportPartsInventoryBillWithDetailCompleted;
        }

        private void excelServiceClient_ExportPartsInventoryBillWithDetailCompleted(object sender, ExportPartsInventoryBillWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        private void ExportPartsInventoryBill(int? id, int storageCompanyId, string partsInventoryBillCode, int? warehouseId, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            if(this.NavigationContext.QueryString.ContainsKey("StorageCompanyType")) {
                var storageCompanyType = this.NavigationContext.QueryString.FirstOrDefault(e => e.Key == "StorageCompanyType").Value.Split(',');
                var storageCompanyTypeids = new int[storageCompanyType.Length];
                for(int i = 0; i < storageCompanyType.Length; i++) {
                    storageCompanyTypeids[i] = Convert.ToInt32(storageCompanyType[i]);
                }
                this.excelServiceClient.ExportPartsInventoryBillAsync(id, storageCompanyId, partsInventoryBillCode, warehouseId, warehouseAreaCategory, status, createTimeBegin, createTimeEnd, storageCompanyTypeids);
            } else {
                this.excelServiceClient.ExportPartsInventoryBillAsync(id, storageCompanyId, partsInventoryBillCode, warehouseId, warehouseAreaCategory, status, createTimeBegin, createTimeEnd, null);
            }
            this.excelServiceClient.ExportPartsInventoryBillCompleted -= ExcelServiceClient_ExportPartsInventoryBillCompleted;
            this.excelServiceClient.ExportPartsInventoryBillCompleted += ExcelServiceClient_ExportPartsInventoryBillCompleted;
        }

        private void ExcelServiceClient_ExportPartsInventoryBillCompleted(object sender, ExportPartsInventoryBillCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.DETAIL:
                case "FirstApprove":
                case "Audit":
                case "ResultsInput"://结果录入
                case "InventoryCoverage"://库存覆盖
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == "InventoryCoverage")
                        return entities[0].Status == (int)DcsPartsInventoryBillStatus.审批通过;
                    if(uniqueId == CommonActionKeys.ABANDON) {
                        return entities[0].Status == (int)DcsPartsInventoryBillStatus.新建 || entities[0].Status == (int)DcsPartsInventoryBillStatus.已结果录入 || entities[0].Status == (int)DcsPartsInventoryBillStatus.审批通过;
                    }
                    if(uniqueId == CommonActionKeys.APPROVE)
                        return entities[0].Status == (int)DcsPartsInventoryBillStatus.审核通过;
                    if(uniqueId == "FirstApprove")
                        return entities[0].Status == (int)DcsPartsInventoryBillStatus.已结果录入;
                    if(uniqueId == "Audit")
                        return entities[0].Status == (int)DcsPartsInventoryBillStatus.初审通过;
                    if(uniqueId == CommonActionKeys.DETAIL)
                        return true;
                    return entities[0].Status == (int)DcsPartsInventoryBillStatus.新建;
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    return selectItems.Length == 1;
                case CommonActionKeys.MERGEEXPORT:
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "BFirstApprove":
                     if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var bFentities = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    return bFentities.All(t => t.Status == (int)DcsPartsInventoryBillStatus.已结果录入);
                case "BAudit":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var bSentities = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    return bSentities.All(t => t.Status == (int)DcsPartsInventoryBillStatus.初审通过);
                case "BApprove":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var bTentities = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    return bTentities.All(t => t.Status == (int)DcsPartsInventoryBillStatus.审核通过);
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;
            if(this.NavigationContext.QueryString.ContainsKey("StorageCompanyType")) {
                var storageCompanyType = this.NavigationContext.QueryString.FirstOrDefault(e => e.Key == "StorageCompanyType").Value.Split(',');
                if(storageCompanyType.Length > 0) {
                    var compositeFilterItem = new CompositeFilterItem();
                    compositeFilterItem.LogicalOperator = LogicalOperator.Or;
                    foreach(var item in storageCompanyType) {
                        var newFilterItem = new FilterItem {
                            MemberType = typeof(int),
                            MemberName = "StorageCompanyType",
                            Operator = FilterOperator.IsEqualTo,
                            Value = item,
                        };
                        compositeFilterItem.Filters.Add(newFilterItem);
                    }
                    newCompositeFilterItem.Filters.Add(compositeFilterItem);
                }
            }

            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "StorageCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
