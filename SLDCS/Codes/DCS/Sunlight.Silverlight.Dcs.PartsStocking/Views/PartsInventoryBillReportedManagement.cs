﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "WarehouseExecution", "AgentsPartsInventoryBillReported", ActionPanelKeys = new[]{
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE,"PartsInventoryBill"
    })]
    public class PartsInventoryBillReportedManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataResultsInputView;
        private DataEditViewBase dataInventoryCoverageView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_APPROVE_VIEW = "_dataApproveView_";
        private const string DATA_DETAIL_VIEW = "_dataDetailView_";
        private const string DATA_RESULTSENTRY_VIEW = "_DataResultsInputView_";
        private const string DATA_INVENTORYCOVERS_VIEW = "_DataInventoryCoverageView_";
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        public PartsInventoryBillReportedManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsInventoryBillReporte;
        }

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsInventoryBillReported"));
            }
        }

        public DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("PartsInventoryBillForApprove");
                    this.dataApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }
        public DataEditViewBase DataDetailView {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsInventoryBillReportedDetail");
                    this.dataDetailView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        public DataEditViewBase DataResultsInputView {
            get {
                if(this.dataResultsInputView == null) {
                    this.dataResultsInputView = DI.GetDataEditView("PartsInventoryBillForResultEntry");
                    this.dataResultsInputView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataResultsInputView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataResultsInputView;
            }
        }

        public DataEditViewBase DataInventoryCoverageView {
            get {
                if(this.dataInventoryCoverageView == null) {
                    this.dataInventoryCoverageView = DI.GetDataEditView("PartsInventoryBillForInventoryCovers");
                    this.dataInventoryCoverageView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataInventoryCoverageView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataInventoryCoverageView;
            }
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsInventoryBillReported");
                    dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailView);
            this.RegisterView(DATA_RESULTSENTRY_VIEW, () => this.DataResultsInputView);
            this.RegisterView(DATA_INVENTORYCOVERS_VIEW, () => this.DataInventoryCoverageView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataApproveView = null;
            this.dataDetailView = null;
            this.dataResultsInputView = null;
            this.dataInventoryCoverageView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case "ViewDetail":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities2 = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    return entities2.Length == 1;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPartsInventoryBillStatus.已结果录入;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities4 = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(entities4.Length != 1)
                        return false;
                    return entities4[0].Status == (int)DcsPartsInventoryBillStatus.新建 || entities4[0].Status == (int)DcsPartsInventoryBillStatus.审核通过 || entities4[0].Status == (int)DcsPartsInventoryBillStatus.已结果录入;
                case "ResultsInput"://结果录入
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    return entities1[0].Status == (int)DcsPartsInventoryBillStatus.新建;
                case "InventoryCoverage"://库存覆盖
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities3 = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().ToArray();
                    if(entities3.Length != 1)
                        return false;
                    return entities3[0].Status == (int)DcsPartsInventoryBillStatus.高级审核通过;
                case "DetailExport":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsInventoryBill = this.DataEditView.CreateObjectToEdit<PartsInventoryBill>();
                    partsInventoryBill.StorageCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsInventoryBill.StorageCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    partsInventoryBill.StorageCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsInventoryBill.Status = (int)DcsPartsInventoryBillStatus.新建;
                    partsInventoryBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "ViewDetail":
                    this.DataDetailView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entityAbandon = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>().FirstOrDefault();
                        if(entityAbandon == null)
                            return;
                        dcsDomainContext.Load(dcsDomainContext.GetPartsInventoryBillsQuery().Where(ex => ex.Id == entityAbandon.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            var items = loadOp.Entities.SingleOrDefault();
                            if(items == null)
                                return;
                            try {
                                if(items.Can作废配件盘点单)
                                    items.作废配件盘点单();
                                dcsDomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        dcsDomainContext.RejectChanges();
                                        return;
                                    }
                                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                    this.CheckActionsCanExecute();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    break;
                case "ResultsInput"://结果录入
                    this.DataResultsInputView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_RESULTSENTRY_VIEW);
                    break;
                case "InventoryCoverage"://库存覆盖
                    this.DataInventoryCoverageView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INVENTORYCOVERS_VIEW);
                    break;
                case "DetailExport":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var warehouseAreaCategory = filterItem.Filters.Single(r => r.MemberName == "WarehouseAreaCategory").Value as int?;
                    var warehouseId = filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                    var storageCompanyId = filterItem.Filters.Single(r => r.MemberName == "StorageCompanyId").Value as int?;
                    var createTime = filterItem.Filters.First(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    var status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    int[] Ids = new int[] { };
                    if(this.DataGridView.SelectedEntities != null) {
                        var dealerPartsInventoryBills = this.DataGridView.SelectedEntities.Cast<PartsInventoryBillEx>();
                        Ids = dealerPartsInventoryBills.Select(r => r.Id).ToArray();
                    }
                    this.ExportAgentPartsInventoryBillWithDetail(Ids, null, storageCompanyId, null, null, warehouseId, null, code, warehouseAreaCategory, status, createTimeBegin, createTimeEnd);
                    break;
            }
        }
        private void ExportAgentPartsInventoryBillWithDetail(int[] ids, int[] storageCompanyType, int? storageCompanyId, string storageCompanyCode, string storageCompanyName, int? warehouseId, string warehouseName, string code, int? warehouseAreaCategory, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportAgentPartsInventoryBillWithDetailAsync(ids, storageCompanyType, storageCompanyId, storageCompanyCode, storageCompanyName, warehouseId, warehouseName, code, warehouseAreaCategory, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportAgentPartsInventoryBillWithDetailCompleted -= excelServiceClient_ExportAgentPartsInventoryBillWithDetailCompleted;
            this.excelServiceClient.ExportAgentPartsInventoryBillWithDetailCompleted += excelServiceClient_ExportAgentPartsInventoryBillWithDetailCompleted;
        }

        private void excelServiceClient_ExportAgentPartsInventoryBillWithDetailCompleted(object sender, ExportAgentPartsInventoryBillWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsInventoryBillReported"
                };
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            if(compositeFilterItem != null) {
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "StorageCompanyId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                this.DataGridView.FilterItem = compositeFilterItem;
            }
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
