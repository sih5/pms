﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using System.Windows.Browser;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsStocking", "PartsLocationAssignment", ActionPanelKeys = new[] {
        "PartsLocationAssignment",CommonActionKeys.MERGEEXPORT
    })]
    public class PartsLocationAssignmentManagement : DcsDataManagementViewBase {
        private PartsLocationAssignmentForEditDataEditView dataEditView;
        private WarehouseArea currentSelectWarehouseArea;
        private DcsDomainContext domainContext;

        /// <summary>
        ///     查询数据库用的DomainContext 外部传入
        /// </summary>
        public DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
            set {
                this.domainContext = value;
            }
        }

        public PartsLocationAssignmentManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsLocationAssignment;
        }

        private void Initialize() {
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.SwitchViewTo(DATA_EDIT_VIEW);
        }

        //导入库位分配
        private RadWindow radWindow;
        private RadWindow RadWindow {
            get {
                if(this.radWindow == null) {
                    this.radWindow = new RadWindow {
                        Content = this.WindowEditView,
                        Width = 720,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen,
                        CanClose = true,
                        CanMove = true,
                        Header = ""
                    };
                    this.radWindow.Loaded += radWindow_Loaded;
                    this.radWindow.Closed += radWindow_Closed;
                }
                return this.radWindow;
            }
        }

        private void radWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            if(windowEditView != null)
                this.windowEditView.ExchangeData(null, null, currentSelectWarehouseArea.Id);
        }

        private void radWindow_Closed(object sender, WindowClosedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetWarehouseAreaWithPartsStocksQuery().Where(d => d.WarehouseAreaParentId == currentSelectWarehouseArea.Id && d.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities;
                var areaWithPartsStocks = entity as WarehouseAreaWithPartsStock[] ?? entity.ToArray();
                if(!areaWithPartsStocks.Any())
                    return;
                DataEditView.WarehouseAreaWithPartsStocks.Clear();
                foreach(var item in areaWithPartsStocks) {
                    this.DomainContext.WarehouseAreaWithPartsStocks.Detach(item);
                    DataEditView.WarehouseAreaWithPartsStocks.Add(item);
                }
            }, null);
        }

        private DataEditViewBase windowEditView;
        private DataEditViewBase WindowEditView {
            get {
                return this.windowEditView ?? (this.windowEditView = DI.GetDataEditView("AreaImportForPartsLocationAssignment"));
            }
        }

        private PartsLocationAssignmentForEditDataEditView DataEditView {
            get {
                return this.dataEditView ?? (this.dataEditView = (PartsLocationAssignmentForEditDataEditView)DI.GetDataEditView("PartsLocationAssignmentForEdit"));
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "ImportArea":
                    if(DataEditView.WarehouseAreaDataTreeView.SelectedItem == null || DataEditView.WarehouseAreaDataTreeView.CurrentSelectedItem.AreaKind != (int)DcsAreaKind.库区) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_ImportWarehouseAreaValidation);
                        return;
                    }
                    currentSelectWarehouseArea = DataEditView.WarehouseAreaDataTreeView.CurrentSelectedItem;
                    this.RadWindow.ShowDialog();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    var editview = this.DataEditView as PartsLocationAssignmentForEditDataEditView;
                    var warehousearea = editview.WarehouseAreaDataTreeView.CurrentSelectedItem;
                    if (warehousearea == null || warehousearea.AreaKind != (int)DcsAreaKind.仓库) { 
                      UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_ChooseWarehouse);
                      return;
                    }
                    this.ExecuteMergeExport(warehousearea.WarehouseId, (int)DcsAreaKind.仓库);
                    
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "ImportArea":
                    return true;
                case CommonActionKeys.MERGEEXPORT:
                    return true;
                default:
                    return false;
            }
        }

        
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExecuteMergeExport(int id, int areaKind)
        {
            ShellViewModel.Current.IsBusy = false;
            this.excelServiceClient.ExportPartsLocationAssignmentAsync(id, areaKind);

            this.excelServiceClient.ExportPartsLocationAssignmentCompleted-= this.ExcelServiceClient_ExportPartsLocationAssignmentCompleted;
            this.excelServiceClient.ExportPartsLocationAssignmentCompleted += this.ExcelServiceClient_ExportPartsLocationAssignmentCompleted;
        }

        private void ExcelServiceClient_ExportPartsLocationAssignmentCompleted(object sender, ExportPartsLocationAssignmentCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
