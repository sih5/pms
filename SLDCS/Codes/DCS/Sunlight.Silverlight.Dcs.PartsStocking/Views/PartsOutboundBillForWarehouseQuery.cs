﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views
{
    [PageMeta("PartsStockingAndLogistics", "PartsOutStore", "PartsOutboundBillForWarehouseQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT_MERGEEXPORT_PRINT
    })]
    public class PartsOutboundBillForWarehouseQuery : DcsDataManagementViewBase
    {
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PartsOutboundBillForWarehouseQuery()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsOutboundBillForWarehouseQuery;
        }

        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsOutboundBillForWarehouseQuery"));
            }
        }

        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PartsOutboundBill"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsOutboundBillWithOtherInfo>().Select(e => e.PartsOutboundBillId).ToArray();
                        //this.ExportPartsOutboundBill(ids, null, null, null, null, null, null, null, null, null, null, null, null);
                        this.ExportPartsOutboundBill(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    }
                    else
                    {
                        var filterItem1 = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem1 == null)
                            return;
                        var code = filterItem1.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var warehouseId = filterItem1.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var outboundType = filterItem1.Filters.Single(e => e.MemberName == "OutboundType").Value as int?;
                        var counterpartCompanyCode = filterItem1.Filters.Single(e => e.MemberName == "CounterpartCompanyCode").Value as string;
                        var counterpartCompanyName = filterItem1.Filters.Single(e => e.MemberName == "CounterpartCompanyName").Value as string;
                        var originalRequirementBillCode = filterItem1.Filters.Single(e => e.MemberName == "OriginalRequirementBillCode").Value as string;
                        var partsOutboundPlanCode = filterItem1.Filters.Single(e => e.MemberName == "PartsOutboundPlanCode").Value as string;

                        var createTime = filterItem1.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if (createTime != null)
                        {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var settlementStatus = filterItem1.Filters.Single(e => e.MemberName == "SettlementStatus").Value as int?;
                        var partsSalesOrderTypeName = filterItem1.Filters.Single(e => e.MemberName == "PartsSalesOrderTypeName").Value as string;
                        var sparePartCode = filterItem1.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem1.Filters.Single(r => r.MemberName == "SparePartName").Value as string;

                        this.ExportPartsOutboundBill(new int[] { }, null, null, code, warehouseId, counterpartCompanyCode, counterpartCompanyName, outboundType, settlementStatus, partsSalesOrderTypeName, partsOutboundPlanCode, null, null, null, createTimeBegin, createTimeEnd, null, originalRequirementBillCode, null, null);
                    }
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    {
                        if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                        {
                            var ids = this.DataGridView.SelectedEntities.Cast<PartsOutboundBillWithOtherInfo>().Select(e => e.PartsOutboundBillId).ToArray();
                            this.ExportPartsOutboundBillWithDetail(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }
                        else
                        {
                            var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                            if (filterItem == null)
                                return;
                            var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                            var outboundType = filterItem.Filters.Single(e => e.MemberName == "OutboundType").Value as int?;
                            var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? createTimeBegin = null;
                            DateTime? createTimeEnd = null;
                            if (createTime != null)
                            {
                                createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            var partsSalesOrderTypeName = filterItem.Filters.Single(e => e.MemberName == "PartsSalesOrderTypeName").Value as string;
                            var partsOutboundPlanCode = filterItem.Filters.Single(e => e.MemberName == "PartsOutboundPlanCode").Value as string;
                            var originalRequirementBillCode = filterItem.Filters.Single(e => e.MemberName == "OriginalRequirementBillCode").Value as string;
                            var sparePartCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                            var sparePartName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                            this.ExportPartsOutboundBillWithDetail(new int[] { }, null, null, code, warehouseId, null, null, outboundType, null, partsSalesOrderTypeName, partsOutboundPlanCode, null, null, null, createTimeBegin, createTimeEnd, null, originalRequirementBillCode, null, null, sparePartCode, sparePartName);
                        }
                        break;
                    }
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsOutboundBillWithOtherInfo>().FirstOrDefault();
                    if (selectedItem == null)
                        return;
                    dcsDomainContext.Load(dcsDomainContext.GetPartsOutboundBillsQuery().Where(ex => ex.Id == selectedItem.PartsOutboundBillId), LoadBehavior.RefreshCurrent, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            if (!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entity = loadOp.Entities.FirstOrDefault();
                        if (entity == null)
                            return;
                        SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataEditView_Text_PrintOutbound, "ReportPartsOutboundBill", null, true, new Tuple<string, string>("parapartsOutboundBillId", entity.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));
                    }, null);
                    break;
                case "AgencyPrint":
                    var selectedItems = this.DataGridView.SelectedEntities.Cast<PartsOutboundBillWithOtherInfo>().FirstOrDefault();
                    if (selectedItems == null)
                        return;
                    dcsDomainContext.Load(dcsDomainContext.GetPartsOutboundBillsQuery().Where(ex => ex.Id == selectedItems.PartsOutboundBillId), LoadBehavior.RefreshCurrent, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            if (!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entitys = loadOp.Entities.FirstOrDefault();
                        if (entitys == null)
                            return;
                        BasePrintWindow printWindows = new PartsOutboundBillQueryAgencyPrint
                        {
                            Header = "代理库打印",
                            PartsOutboundBill = entitys
                        };
                        printWindows.ShowDialog();
                    }, null);

                    break;
                case "PrintForGC":
                    var selected = this.DataGridView.SelectedEntities.Cast<PartsOutboundBillWithOtherInfo>().FirstOrDefault();
                    if (selected == null)
                        return;
                    dcsDomainContext.Load(dcsDomainContext.GetPartsOutboundBillsQuery().Where(ex => ex.Id == selected.PartsOutboundBillId), LoadBehavior.RefreshCurrent, loadOp =>
                    {
                        if (loadOp.HasError)
                        {
                            if (!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entity = loadOp.Entities.FirstOrDefault();
                        if (entity == null)
                            return;
                        entity.PrintTimes = 1;
                        dcsDomainContext.SubmitChanges(submitOp =>
                        {
                            if (submitOp.HasError)
                            {
                                if (!submitOp.IsErrorHandled)
                                    submitOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                dcsDomainContext.RejectChanges();
                                return;
                            }
                        }, null);
                        BasePrintWindow printWindowForGC = new PartsOutboundBillForGCPrintWindow
                        {
                            Header = PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsOutboundBillForGC,
                            PartsOutboundBill = selected
                        };
                        printWindowForGC.ShowDialog();
                        this.DataGridView.ExecuteQueryDelayed();
                    }, null);
                    break;
                case "PrintResume":
                    if (this.DataGridView.SelectedEntities.Cast<PartsOutboundBillWithOtherInfo>().All(entity => entity.PrintTimes != 0))
                    {
                        DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Resume, () =>
                        {
                            var selectedAll = this.DataGridView.SelectedEntities.Cast<PartsOutboundBillWithOtherInfo>().Select(t => t.PartsOutboundBillId).ToArray();
                            if (selectedAll == null)
                                return;
                            dcsDomainContext.Load(dcsDomainContext.GetPartsOutboundBillsByIdsQuery(selectedAll), LoadBehavior.RefreshCurrent, loadOpAll =>
                            {
                                if (loadOpAll.HasError)
                                {
                                    if (!loadOpAll.IsErrorHandled)
                                        loadOpAll.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOpAll);
                                    return;
                                }
                                if (!loadOpAll.Entities.Any())
                                    return;
                                foreach (var item in loadOpAll.Entities)
                                {
                                    item.PrintTimes = 0;
                                }
                                dcsDomainContext.SubmitChanges(submitOp =>
                                {
                                    if (submitOp.HasError)
                                    {
                                        if (!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        dcsDomainContext.RejectChanges();
                                        return;
                                    }
                                }, null);
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        });
                    }
                    break;
            }
        }

        private void ExportPartsOutboundBillWithDetail(int[] ids, string eRPSourceOrderCode, int? partsSalesCategoryId, string partsInboundCheckBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? inboundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutboundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string SAPPurchasePlanCode, string zpNumber, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode, string sparePartCode, string sparePartName)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsOutboundBillWithDetailForWarehouseAsync(ids, eRPSourceOrderCode, BaseApp.Current.CurrentUserData.UserId, partsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId, partsInboundCheckBillCode, warehouseId, counterpartcompanycode, counterpartcompanyname, inboundType, settlementStatus, partsSalesOrderTypeName, partsOutboundPlanCode, outboundPackPlanCode, contractCode, provinceName, createTimeBegin, createTimeEnd, SAPPurchasePlanCode, zpNumber, cpPartsPurchaseOrderCode, cpPartsInboundCheckCode, sparePartCode, sparePartName);
            this.excelServiceClient.ExportPartsOutboundBillWithDetailForWarehouseCompleted -= excelServiceClient_ExportPartsOutboundBillWithDetailCompleted;
            this.excelServiceClient.ExportPartsOutboundBillWithDetailForWarehouseCompleted += excelServiceClient_ExportPartsOutboundBillWithDetailCompleted;
        }

        private void ExportPartsOutboundBill(int[] ids, string eRPSourceOrderCode, int? partsSalesCategoryId, string partsInboundCheckBillCode, int? warehouseId, string counterpartcompanycode, string counterpartcompanyname, int? inboundType, int? settlementStatus, string partsSalesOrderTypeName, string partsOutBoundPlanCode, string outboundPackPlanCode, string contractCode, string provinceName, DateTime? createTimeBegin, DateTime? createTimeEnd, string SAPPurchasePlanCode, string zpNumber, string cpPartsPurchaseOrderCode, string cpPartsInboundCheckCode)
        {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsOutboundBillAsync(ids, eRPSourceOrderCode, BaseApp.Current.CurrentUserData.UserId, partsSalesCategoryId, BaseApp.Current.CurrentUserData.EnterpriseId, partsInboundCheckBillCode, warehouseId, counterpartcompanycode, counterpartcompanyname, inboundType, settlementStatus, partsSalesOrderTypeName, partsOutBoundPlanCode, outboundPackPlanCode, contractCode, provinceName, createTimeBegin, createTimeEnd, SAPPurchasePlanCode, zpNumber, cpPartsPurchaseOrderCode, cpPartsInboundCheckCode);
            this.excelServiceClient.ExportPartsOutboundBillCompleted -= excelServiceClient_ExportPartsOutboundBillCompleted;
            this.excelServiceClient.ExportPartsOutboundBillCompleted += excelServiceClient_ExportPartsOutboundBillCompleted;
        }

        private void excelServiceClient_ExportPartsOutboundBillCompleted(object sender, ExportPartsOutboundBillCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        protected void excelServiceClient_ExportPartsOutboundBillWithDetailCompleted(object sender, ExportPartsOutboundBillWithDetailForWarehouseCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch (uniqueId)
            {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItemForPrint = this.DataGridView.SelectedEntities.Cast<PartsOutboundBillWithOtherInfo>().ToArray();
                    return selectItemForPrint.Length == 1;
                case "AgencyPrint":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var agencyPrint = this.DataGridView.SelectedEntities.Cast<PartsOutboundBillWithOtherInfo>().ToArray();
                    return agencyPrint.Length == 1;
                case "PrintForGC":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsOutboundBillWithOtherInfo>().ToArray();
                    return selectItems.Length == 1 && (selectItems[0].PrintTimes == null || selectItems[0].PrintTimes == 0);
                case "PrintResume":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItem = this.DataGridView.SelectedEntities.Cast<PartsOutboundBillWithOtherInfo>().ToArray();
                    return selectItem.All(r => r.PrintTimes != 0 && r.PrintTimes != null);
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
