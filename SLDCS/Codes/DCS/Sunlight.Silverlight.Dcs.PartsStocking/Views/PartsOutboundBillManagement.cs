﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsOutStore", "PartsOutboundBill", ActionPanelKeys = new[] {
        "PartsOutboundBill",CommonActionKeys.DETAIL_TERMINATE_MERGEEXPORT
    })]
    public class PartsOutboundBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataDetailView;
        private const string DATA_DETAIL_VIEW = "_dataDetailView_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        //终止弹出
        private RadWindow terminateRadWindow;

        private RadWindow TerminateRadWindow {
            get {
                if(this.terminateRadWindow == null) {
                    this.terminateRadWindow = new RadWindow();
                    this.terminateRadWindow.CanClose = false;
                    this.terminateRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.terminateRadWindow.Content = this.TerminateDataEditView;
                    this.terminateRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.terminateRadWindow.Height = 200;
                    this.terminateRadWindow.Width = 400;
                    this.terminateRadWindow.Header = "";
                }
                return this.terminateRadWindow;
            }
        }

        private DataEditViewBase terminateDataEditView;

        private DataEditViewBase TerminateDataEditView {
            get {
                if(this.terminateDataEditView == null) {
                    this.terminateDataEditView = DI.GetDataEditView("PartsOutboundForTerminate");
                    this.terminateDataEditView.EditCancelled += TerminateDataEditView_EditCancelled;
                    this.terminateDataEditView.EditSubmitted += TerminateDataEditView_EditSubmitted;
                }
                return this.terminateDataEditView;
            }
        }

        public PartsOutboundBillManagement() {
            this.Initializer.Register(this.Initliaze);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsOutboundBill;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsOutboundPlan"));
            }
        }

        private DataEditViewBase DataDetailView {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsOutboundPlanDetailForDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return dataDetailView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsOutboundBill");
                    dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataDetailView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initliaze() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsOutboundPlan"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.DETAIL:
                    this.DataDetailView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
                case "DeliverParts":
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.TERMINATE:
                    this.TerminateDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.TerminateRadWindow.ShowDialog();
                    break;
                case CommonActionKeys.MERGEEXPORT: {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var outboundType = filterItem.Filters.Single(e => e.MemberName == "OutboundType").Value as int?;
                        var sourceCode = filterItem.Filters.Single(e => e.MemberName == "SourceCode").Value as string;
                        var counterpartCompanyCode = filterItem.Filters.Single(e => e.MemberName == "CounterpartCompanyCode").Value as string;
                        var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var counterpartCompanyName = filterItem.Filters.Single(e => e.MemberName == "CounterpartCompanyName").Value as string;
                        var ERPSourceOrderCode = filterItem.Filters.Single(e => e.MemberName == "ERPSourceOrderCode").Value as string;
                        var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        var partsSalesOrderTypeName = filterItem.Filters.Single(e => e.MemberName == "PartsSalesOrderTypeName").Value as string;
                        var entities = this.DataGridView.SelectedEntities;
                        int? id = null;
                        if(entities != null && entities.Any())
                            id = entities.Cast<PartsOutboundPlan>().First().Id;
                        this.ExportPartsOutboundPlanWithDetail(id, code, sourceCode, warehouseId, outboundType, createTimeBegin, createTimeEnd, counterpartCompanyCode, counterpartCompanyName, partsSalesOrderTypeName, partsSalesCategoryId, status, ERPSourceOrderCode);
                        break;
                    }
                case "PartitionPrint":
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new PartsOutboundPlanPartitionPrintWindow {
                        Header = PartsStockingUIStrings.Action_Title_PartitionPrint,
                        PartsOutboundPlan = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case "NotPartitionPrint":
                    var selectedItemNot = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().FirstOrDefault();
                    if(selectedItemNot == null)
                        return;
                    BasePrintWindow printWindowNot = new PartsOutboundNotPartitionPrintWindow {
                        Header = PartsStockingUIStrings.Action_Title_NotPartitionPrint,
                        PartsOutboundPlan = selectedItemNot
                    };
                    printWindowNot.ShowDialog();
                    break;
            }
        }

        private void ExportPartsOutboundPlanWithDetail(int? id, string partsOutboundPlanCode, string sourceCode, int? warehouseId, int? outboundType, DateTime? createTimeBegin, DateTime? createTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string partsSalesOrderTypeName, int? partsSalesCategoryId, int? status, string ERPSourceOrderCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsOutboundPlanWithDetailAsync(id, BaseApp.Current.CurrentUserData.UserId, BaseApp.Current.CurrentUserData.EnterpriseId, partsOutboundPlanCode, sourceCode, warehouseId, outboundType, createTimeBegin, createTimeEnd, counterpartCompanyCode, counterpartCompanyName, partsSalesOrderTypeName, partsSalesCategoryId, status, ERPSourceOrderCode);
            this.excelServiceClient.ExportPartsOutboundPlanWithDetailCompleted -= excelServiceClient_ExportPartsOutboundPlanWithDetailCompleted;
            this.excelServiceClient.ExportPartsOutboundPlanWithDetailCompleted += excelServiceClient_ExportPartsOutboundPlanWithDetailCompleted;
        }

        protected void excelServiceClient_ExportPartsOutboundPlanWithDetailCompleted(object sender, ExportPartsOutboundPlanWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.DETAIL:
                case "DeliverParts":
                case "PartitionPrint":
                case "NotPartitionPrint":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().ToArray();
                    return entity.Length == 1;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.TERMINATE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entityDetail = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().ToArray();
                    if(entityDetail.Length != 1)
                        return false;
                    return entityDetail[0].OriginalRequirementBillType != (int)DcsOriginalRequirementBillType.积压件调剂单;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "StorageCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void TerminateDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.TerminateRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void TerminateDataEditView_EditCancelled(object sender, EventArgs e) {
            this.TerminateRadWindow.Close();
        }
    }
}
