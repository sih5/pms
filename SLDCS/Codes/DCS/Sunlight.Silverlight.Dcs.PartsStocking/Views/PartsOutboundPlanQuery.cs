﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsOutStore", "PartsOutboundPlanQuery", ActionPanelKeys = new[] {
        "PartsOutboundPlanQuery",CommonActionKeys.EXPORT_MERGEEXPORT_PRINT_TERMINATE
    })]
    public class PartsOutboundPlanQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PartsOutboundPlanQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsOutboundPlan;
        }

        private DataGridViewBase DataGridView {
            get {
                return dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsOutboundPlanWithDetails"));
            }
        }

        private DataEditViewBase terminateDataEditView;
        private DataEditViewBase TerminateDataEditView {
            get {
                if(this.terminateDataEditView == null) {
                    this.terminateDataEditView = DI.GetDataEditView("PartsOutboundForTerminate");
                    this.terminateDataEditView.EditCancelled += TerminateDataEditView_EditCancelled;
                    this.terminateDataEditView.EditSubmitted += TerminateDataEditView_EditSubmitted;
                }
                return this.terminateDataEditView;
            }
        }
        private void ResetEditView() {
            this.terminateDataEditView = null;
        }
        private void TerminateDataEditView_EditSubmitted(object sender, EventArgs e) {
         //   this.ResetEditView();
            this.TerminateRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void TerminateDataEditView_EditCancelled(object sender, EventArgs e) {
        //    this.ResetEditView();
            this.TerminateRadWindow.Close();
        }
        //终止弹出
        private RadWindow terminateRadWindow;

        private RadWindow TerminateRadWindow {
            get {
                if(this.terminateRadWindow == null) {
                    this.terminateRadWindow = new RadWindow();
                    this.terminateRadWindow.CanClose = false;
                    this.terminateRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.terminateRadWindow.Content = this.TerminateDataEditView;
                    this.terminateRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.terminateRadWindow.Height = 200;
                    this.terminateRadWindow.Width = 400;
                    this.terminateRadWindow.Header = "";
                }
                return this.terminateRadWindow;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsOutboundPlan"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExportPlan(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var code = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var sourceCode = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                            // var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            //var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            var warehouseId = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                            var outboundType = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "OutboundType") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "OutboundType").Value as int?;
                            // var counterpartCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyCode").Value as string;
                            var counterpartCompanyName = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyName") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyName").Value as string;
                            var sapPurchasePlanCode = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "SAPPurchasePlanCode") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "SAPPurchasePlanCode").Value as string;
                            var eRPSourceOrderCode = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "ERPSourceOrderCode") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "ERPSourceOrderCode").Value as string;
                            var zPNUMBER = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "ZPNUMBER") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "ZPNUMBER").Value as string;
                            var sparePartCode = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartCode") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                            var sparePartName = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartName") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;

                            DateTime? begianDate = null;
                            DateTime? endDate = null;
                            string statusStr = string.Empty;
                            foreach(var filter in compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                                var dateTime = filter as CompositeFilterItem;
                                {
                                    if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "CreateTime")) {
                                        begianDate = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                        endDate = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    }
                                    if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "Status")) {
                                        statusStr = string.Join(",", dateTime.Filters.Where(r => r.MemberName == "Status").Select(r => r.Value));
                                    }
                                }
                            }
                            var partsSalesOrderTypeName = compositeFilterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesOrderTypeName") == null ? null : compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderTypeName").Value as string;

                            this.ExecuteMergeExportPlan(new int[] { }, code, statusStr, sourceCode, warehouseId, outboundType, begianDate, endDate, counterpartCompanyName, partsSalesOrderTypeName, sapPurchasePlanCode, zPNUMBER, eRPSourceOrderCode, sparePartCode, sparePartName);

                        }
                    }
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 合并导出参数为 出库计划ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().Select(e => e.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    }
                        //否则 合并导出参数为 出库计划编号 源单据编号 仓库名称	出库类型	创建时间	对方单位编号	对方单位名称
                    else {

                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                       // var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        //var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var outboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OutboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OutboundType").Value as int?;
                       // var counterpartCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyCode").Value as string;
                        var counterpartCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyName").Value as string;
                        var sapPurchasePlanCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SAPPurchasePlanCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SAPPurchasePlanCode").Value as string;
                        var eRPSourceOrderCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "ERPSourceOrderCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "ERPSourceOrderCode").Value as string;
                        var zPNUMBER = filterItem.Filters.SingleOrDefault(r => r.MemberName == "ZPNUMBER") == null ? null : filterItem.Filters.Single(r => r.MemberName == "ZPNUMBER").Value as string;
                        var sparePartCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        
                        DateTime? begianDate = null;
                        DateTime? endDate = null;
                        string statusStr = string.Empty;
                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if (((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "CreateTime")) {
                                    begianDate = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    endDate = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if (((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "Status")){
                                    statusStr = string.Join(",", dateTime.Filters.Where(r => r.MemberName == "Status").Select(r => r.Value));
                                }
                            }
                        }
                        var partsSalesOrderTypeName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesOrderTypeName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderTypeName").Value as string;
                        this.ExecuteMergeExport(new int[] { }, code, statusStr, sourceCode, warehouseId, outboundType, begianDate, endDate, counterpartCompanyName, partsSalesOrderTypeName, sapPurchasePlanCode, zPNUMBER, eRPSourceOrderCode, sparePartCode, sparePartName);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow;
                    printWindow = new PartsOutboundPlanPrintWindow {
                        Header = "出库计划单打印",
                        PartsOutboundPlan = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case "PartitionPrint":
                    var selected = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().FirstOrDefault();
                    if(selected == null)
                        return;
                    BasePrintWindow print = new PartsOutboundPlanPartitionPrintWindow {
                        Header = "分区打印",
                        PartsOutboundPlan = selected
                    };
                    print.ShowDialog();
                    break;
                case "NotWarehousePrint":
                    var selected1 = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().FirstOrDefault();
                    if(selected1 == null)
                        return;
                    BasePrintWindow print1 = new PartsOutboundPlanNotWarehousePrintWindow {
                        Header = "未出库打印",
                        PartsOutboundPlan = selected1
                    };
                    print1.ShowDialog();
                    break;
                case "NotPartitionPrint":
                    var selected2 = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().FirstOrDefault();
                    if(selected2 == null)
                        return;
                    BasePrintWindow print2 = new PartsOutboundPlanNotPartitionPrintWindow {
                        Header = "未出库分区打印",
                        PartsOutboundPlan = selected2
                    };
                    print2.ShowDialog();
                    break;
                case "Packing":
                    //判断仓库，收货地址，订单类型，发运方式,对方单位ID是否一致，不一致，不允许生成拣货单
                    //只有新建，部分分配的才能选择
                    if(this.DataGridView.SelectedEntities.Count() == 0) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_ChooseNewPartsOutboundPlan);
                        return;
                    }                    
                    var pickList = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().ToArray();
                    if (pickList.Any(t=>t.Status!=(int)DcsPartsOutboundPlanStatus.新建))
                    { 
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_ChooseNewPartsOutboundPlan);
                        return;
                     }
                    if (pickList.Count() > 1)
                    {
                        var detailCount = 0;
                        foreach (var item in pickList)
                        {
                            detailCount += item.PartsOutboundPlanDetails.Count();
                        }
                        if (detailCount>600)
                        {
                            UIHelper.ShowNotification("清单条目数不能超过600");
                            return;
                        }
                    }
                    string address = "";
                    int orderType = 0;
                    int shipType = 0;
                    int warehouse = 0;
                    int counterpartCompanyId = 0;
                    int receivingWarehouseId = 0;
                   var  isTurn = false;
                    for(int i = 0; i < pickList.Count(); i++) {                       
                        if(pickList.Count() > 1) {
                            if(i == 0) {
                                address = pickList[i].ReceivingAddress == null ? "" : pickList[i].ReceivingAddress;
                                orderType = pickList[i].PartsSalesOrderTypeId == null ? 0 : pickList[i].PartsSalesOrderTypeId.Value;
                                shipType = pickList[i].ShippingMethod == null ? 0 : pickList[i].ShippingMethod.Value;
                                warehouse = pickList[i].WarehouseId;
                                counterpartCompanyId = pickList[i].CounterpartCompanyId;
                                isTurn = pickList[i].IsTurn == null ? false : pickList[i].IsTurn.Value;
                                receivingWarehouseId = pickList[i].ReceivingWarehouseId == null ? 0 : pickList[i].ReceivingWarehouseId.Value;
                                continue;
                            } else if(!address.Equals(pickList[i].ReceivingAddress == null ? "" : pickList[i].ReceivingAddress) || (orderType != (pickList[i].PartsSalesOrderTypeId == null ? 0 : pickList[i].PartsSalesOrderTypeId.Value)) || (shipType != (pickList[i].ShippingMethod == null ? 0 : pickList[i].ShippingMethod.Value)) || warehouse != (int)pickList[i].WarehouseId || counterpartCompanyId != (int)pickList[i].CounterpartCompanyId || receivingWarehouseId != (pickList[i].ReceivingWarehouseId == null ? 0 : pickList[i].ReceivingWarehouseId.Value) || (isTurn != (pickList[i].IsTurn == null ? false : pickList[i].IsTurn.Value))) {
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_PartsOutboundPlanValidation);
                                this.DataGridView.DomainContext.RejectChanges();
                                return;
                            }
                        }
                    }
                    var pickIds = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().Select(e => e.Id).ToArray();
                    try {
                        var gridDomainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(gridDomainContext == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        dcsDomainContext.生成捡货单(pickIds, loadOpv => {
                            if(loadOpv.HasError) {
                                if(!loadOpv.IsErrorHandled)
                                    loadOpv.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                                gridDomainContext.RejectChanges();
                                ShellViewModel.Current.IsBusy = false;
                                return;
                            }
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_CreatePickingTaskSuccess);
                            gridDomainContext.RejectChanges();
                            if(DataGridView != null && DataGridView.FilterItem != null) {
                                DataGridView.ExecuteQueryDelayed();
                            }
                            CheckActionsCanExecute();
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    } catch(Exception e) {
                        ShellViewModel.Current.IsBusy = false;
                        UIHelper.ShowAlertMessage(e.Message);
                    }
                    break;
                case CommonActionKeys.TERMINATE:
                    this.TerminateDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.TerminateRadWindow.ShowDialog();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                case "PartitionPrint":
                case "NotWarehousePrint":
                case "NotPartitionPrint":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().ToArray();
                    return selectItems.Length == 1;
                case "Packing":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    else return true;
                case CommonActionKeys.TERMINATE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entityDetail = this.DataGridView.SelectedEntities.Cast<PartsOutboundPlan>().ToArray();
                    if(entityDetail.Length != 1)
                        return false;
                    return entityDetail[0].Status != (int)DcsPartsOutboundPlanStatus.出库完成 && entityDetail[0].Status != (int)DcsPartsOutboundPlanStatus.终止;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        /// <summary>
        /// 合并导出配件出库计划主清单
        /// </summary>
        private void ExecuteMergeExport(int[] ids,  string code, string status, string sourceCode, int? warehouseId, int? outboundType, DateTime? begianDate, DateTime? endDate , string counterpartCompanyName, string partsSalesOrderTypeName, string SAPPurchasePlanCode, string ZPNUMBER, string eRPSourceOrderCode,string sparePartCode,string sparePartName) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsOutboundPlanWithDetailForQuery1Async(ids, BaseApp.Current.CurrentUserData.UserId, BaseApp.Current.CurrentUserData.EnterpriseId, code, status, sourceCode, warehouseId, outboundType, begianDate, endDate, counterpartCompanyName, partsSalesOrderTypeName, SAPPurchasePlanCode, ZPNUMBER, eRPSourceOrderCode, sparePartCode, sparePartName);
            this.excelServiceClient.ExportPartsOutboundPlanWithDetailForQuery1Completed -= excelServiceClient_ExportPartsOutboundPlanWithDetailForQuery1Completed;
            this.excelServiceClient.ExportPartsOutboundPlanWithDetailForQuery1Completed += excelServiceClient_ExportPartsOutboundPlanWithDetailForQuery1Completed;
        }

        void excelServiceClient_ExportPartsOutboundPlanWithDetailForQuery1Completed(object sender, ExportPartsOutboundPlanWithDetailForQuery1CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        /// <summary>
        /// 合并导出配件出库计划主单
        /// </summary>
        private void ExecuteMergeExportPlan(int[] ids, string code, string status, string sourceCode, int? warehouseId, int? outboundType, DateTime? begianDate, DateTime? endDate, string counterpartCompanyName, string partsSalesOrderTypeName, string SAPPurchasePlanCode, string ZPNUMBER, string eRPSourceOrderCode, string sparePartCode, string sparePartName) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsOutboundPlanForQuery1Async(ids, BaseApp.Current.CurrentUserData.UserId, BaseApp.Current.CurrentUserData.EnterpriseId, code, status, sourceCode, warehouseId, outboundType, begianDate, endDate, counterpartCompanyName, partsSalesOrderTypeName, SAPPurchasePlanCode, ZPNUMBER, eRPSourceOrderCode, sparePartCode, sparePartName);
            this.excelServiceClient.ExportPartsOutboundPlanForQuery1Completed -= excelServiceClient_ExportPartsOutboundPlanForQuery1Completed;
            this.excelServiceClient.ExportPartsOutboundPlanForQuery1Completed += excelServiceClient_ExportPartsOutboundPlanForQuery1Completed;
        }

        void excelServiceClient_ExportPartsOutboundPlanForQuery1Completed(object sender, ExportPartsOutboundPlanForQuery1CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
