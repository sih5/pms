﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsInStore", "PartsPacking", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT, "PartsPacking"
    })]
    public class PartsPackingManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataPackingDetail;
        private PartsPackingView partsPackingView;
        private const string DATA_PACKING_DETAIL = "_dataPackingDetail_";
        private const string PARTSPACKINGVIEW = "_partsPackingView_";

        private DataEditViewBase DataPackingDetail {
            get {
                if(this.dataPackingDetail == null) {
                    this.dataPackingDetail = DI.GetDataEditView("PartsPackingDetail");
                    this.dataPackingDetail.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataPackingDetail;
            }
        }

        private PartsPackingView PartsPackingView {
            get {
                return this.partsPackingView ?? (this.partsPackingView = new PartsPackingView());
            }
        }

        public PartsPackingManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsPacking;
        }

        private void Initialize() {
            this.RegisterView(DATA_PACKING_DETAIL, () => this.DataPackingDetail);
            this.RegisterView(PARTSPACKINGVIEW, () => this.PartsPackingView);
            this.SwitchViewTo(PARTSPACKINGVIEW);
        }
        private void ResetEditView() {
            this.dataPackingDetail = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(PARTSPACKINGVIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(PARTSPACKINGVIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "StorageCompanyId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            }
            this.PartsPackingView.VirtualPartsStockForPartsPackingDataGridView.FilterItem = filterItem;
            this.PartsPackingView.VirtualPartsStockForPartsPackingDataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsPacking"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "PackingDetail":
                    var filter = this.PartsPackingView.VirtualPartsStockForPartsPackingDataGridView.SelectedEntities.Cast<VirtualPartsStock>().Select(entity => new object[] {
                        entity.SparePartId, entity.WarehouseId
                    }).First();
                    this.DataPackingDetail.SetObjectToEditById(filter);
                    this.SwitchViewTo(DATA_PACKING_DETAIL);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.PartsPackingView.VirtualPartsStockForPartsPackingDataGridView).ExportData();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != PARTSPACKINGVIEW)
                return false;

            switch(uniqueId) {
                case "PackingDetail":
                    if(this.PartsPackingView.VirtualPartsStockForPartsPackingDataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.PartsPackingView.VirtualPartsStockForPartsPackingDataGridView.SelectedEntities.Cast<VirtualPartsStock>().ToArray();
                    return entitie.Length == 1;
                case CommonActionKeys.EXPORT:
                    return this.PartsPackingView.VirtualPartsStockForPartsPackingDataGridView.Entities != null && this.PartsPackingView.VirtualPartsStockForPartsPackingDataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
