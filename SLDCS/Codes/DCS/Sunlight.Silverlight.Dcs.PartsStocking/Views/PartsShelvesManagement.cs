﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsInStore", "PartsShelves")]
    public class PartsShelvesManagement : DcsDataManagementViewBase {
        private PartsShelvesDataEditView partsShelvesDataEditView;
        private const string DATA_VIEW_PARTSSHELVESVIEW = "_PartsShelvesView_";

        public PartsShelvesManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsShelves;
        }

        public PartsShelvesDataEditView PartsShelvesDataEditView {
            get {
                if(this.partsShelvesDataEditView == null) {
                    this.partsShelvesDataEditView = new PartsShelvesDataEditView();
                    this.DataContext = this.DataContext;
                    this.partsShelvesDataEditView.EditCancelled += partsShelvesDataEditView_EditCancelled;
                }
                return this.partsShelvesDataEditView;
            }
        }
        private void ResetEditView() {
            this.partsShelvesDataEditView = null;
        }
        private void partsShelvesDataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_ClearPartsShiftOrder);
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VirtualPartsStock"
                };
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_VIEW_PARTSSHELVESVIEW, () => this.PartsShelvesDataEditView);
            this.SwitchViewTo(DATA_VIEW_PARTSSHELVESVIEW);
        }

        private void ExecuteQuery(FilterItem filterItem) {
            var compositeFilterItem = filterItem as CompositeFilterItem;
            var warehouseFilterItem = new FilterItem();
            // 仓库查询条件必填
            if(compositeFilterItem == null) {
                if(filterItem.MemberName != "WarehouseId") {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_WarehouseIsNull);
                    return;
                }
                warehouseFilterItem = filterItem;
            }
            if(compositeFilterItem != null) {
                if(compositeFilterItem.Filters.All(e => e.MemberName != "WarehouseId")) {
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_WarehouseIsNull);
                    return;
                }
                warehouseFilterItem = compositeFilterItem.Filters.Single(e => e.MemberName == "WarehouseId");
            }
            // 数据表格中有数据
            if(this.PartsShelvesDataEditView.DataGridView.Entities != null && this.PartsShelvesDataEditView.DataGridView.Entities.Any()) {
                // 判断移库单上仓库是否与查询面板上一致
                if((int)warehouseFilterItem.Value != this.PartsShelvesDataEditView.PartsShiftOrder.WarehouseId) {
                    // 仓库不同，提示将清空上架清单
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_WarehouseIsDifference, () => {
                        this.PartsShelvesDataEditView.ClearPartsShiftOrderInfo();
                        this.PartsShelvesDataEditView.DataGridView.FilterItem = filterItem;
                        this.PartsShelvesDataEditView.DataGridView.ExecuteQueryDelayed();
                    });
                } else {
                    // 仓库相同，继续查询
                    this.PartsShelvesDataEditView.DataGridView.FilterItem = filterItem;
                    this.PartsShelvesDataEditView.DataGridView.ExecuteQueryDelayed();
                }
            } else {
                // 数据表格中没有数据
                this.PartsShelvesDataEditView.DataGridView.FilterItem = filterItem;
                this.PartsShelvesDataEditView.DataGridView.ExecuteQueryDelayed();
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            var warehouseFilter = compositeFilterItem.Filters.First(v => v.MemberName == "WarehouseId");
            if(warehouseFilter == null || warehouseFilter.Value == null) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Error_WareHouseIsNull);
                return;
            }
            if(this.PartsShelvesDataEditView.PartsShiftOrderDetailForEditDataGridView.Entities == null || !this.PartsShelvesDataEditView.PartsShiftOrderDetailForEditDataGridView.Entities.Any())
                this.ExecuteQuery(filterItem);
            else if(this.PartsShelvesDataEditView.PartsShiftOrder.WarehouseId != 0 && Convert.ToInt32(warehouseFilter.Value) != this.PartsShelvesDataEditView.PartsShiftOrder.WarehouseId)
                DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_WareHouseIsChange, () => this.ExecuteQuery(filterItem));
        }
    }
}
