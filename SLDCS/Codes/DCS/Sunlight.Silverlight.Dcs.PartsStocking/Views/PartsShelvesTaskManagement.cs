﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsInStore", "PartsShelvesTask", ActionPanelKeys = new[]{
        CommonActionKeys.MERGEEXPORT,CommonActionKeys.PRINT,"PartsShelvesTask"
    })]
    public class PartsShelvesTaskManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataDetailEditView;
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();
        //private const string DATA_DETAIL_VIEW = "_dataDetailView_";

        public PartsShelvesTaskManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsShelvesTask;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsShelvesTask"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataDetailEditView == null) {
                    this.dataDetailEditView = DI.GetDataEditView("PartsShelvesTaskDetail");
                    this.dataDetailEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataDetailEditView.EditCancelled += this.DataEdtiView_EditCancelled;
                }
                return this.dataDetailEditView;
            }
        }
        private void ResetEditView() {
            this.dataDetailEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            //var partsShelvesTaskDetailDataEditView = this.dataDetailEditView as PartsShelvesTaskDetailDataEditView;
            //if (partsShelvesTaskDetailDataEditView == null)
            //    return;
            //if (partsShelvesTaskDetailDataEditView.VirtualPartsShelvesTaskDetails.Count > 0)
            //{
            //    var partsShelvesTaskDetails = partsShelvesTaskDetailDataEditView.VirtualPartsShelvesTaskDetails.ToArray();

            //    dcsDomainContext.DoPartsShelves(partsShelvesTaskDetails, invokeOp =>
            //    {
            //        if (invokeOp.HasError)
            //        {
            //            if (!invokeOp.IsErrorHandled)
            //                invokeOp.MarkErrorAsHandled();
            //            var error = invokeOp.ValidationErrors.First();
            //            if (error != null)
            //                UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage);
            //            else
            //                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
            //            return;
            //        }
            //        //UIHelper.ShowNotification("上架成功");
            //        if (this.DataGridView.FilterItem != null)
            //            this.DataGridView.ExecuteQueryDelayed();
            //        this.SwitchViewTo(DATA_GRID_VIEW);
            //    }, null);
            //}
            //else
            //{
            //    if (this.DataGridView.FilterItem != null)
            //        this.DataGridView.ExecuteQueryDelayed();
            //    this.SwitchViewTo(DATA_GRID_VIEW);
            //}
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEdtiView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsShelvesTask"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Shelves":
                    var checks = this.DataGridView.SelectedEntities.Cast<VirtualPartsShelvesTask>();

                    var checkedIds = checks.Select(r => r.Id).ToArray();
                    this.DataEditView.SetObjectToEditById(checkedIds);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 合并导出参数为 上架任务单ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualPartsShelvesTask>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null, null, null, null,null,null,null,null,null,null);
                    }
                        //否则 合并导出参数为 上架任务单号，原单据编号，入库计划单号，入库单号，入库类型，状态，仓库ID，创建时间
                    else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var sourceBillCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceBillCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceBillCode").Value as string;
                        var partsInboundPlanCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsInboundPlanCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsInboundPlanCode").Value as string;
                        var partsInboundCheckBillCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsInboundCheckBillCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsInboundCheckBillCode").Value as string;

                        var inboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InboundType").Value as int?;
                        //var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var sparePartCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        var noWarehouseArea = filterItem.Filters.SingleOrDefault(r => r.MemberName == "NoWarehouseArea") == null ? null : filterItem.Filters.Single(r => r.MemberName == "NoWarehouseArea").Value as bool?;
                        var warehouseAreaCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseAreaCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseAreaCode").Value as string;
                        var warehouseArea = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseArea") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseArea").Value as string;
                        var hasDifference = filterItem.Filters.SingleOrDefault(r => r.MemberName == "HasDifference") == null ? null : filterItem.Filters.Single(r => r.MemberName == "HasDifference").Value as bool?;

                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? shelvesFinishTimeBegin = null;
                        DateTime? shelvesFinishTimeEnd = null;
                        string status = string.Empty;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.Any(r => r.MemberName == "ShelvesFinishTime")) {
                                    shelvesFinishTimeBegin = dateTime.Filters.First(r => r.MemberName == "ShelvesFinishTime").Value as DateTime?;
                                    shelvesFinishTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ShelvesFinishTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.Any(r => r.MemberName == "Status"))
                                    status = string.Join(",", dateTime.Filters.Where(r => r.MemberName == "Status").Select(r => r.Value));
                            }
                        }

                        this.ExecuteMergeExport(null, code, sourceBillCode, partsInboundPlanCode, partsInboundCheckBillCode, inboundType, status, warehouseId, sparePartCode, sparePartName,noWarehouseArea,warehouseAreaCode,warehouseArea, hasDifference, createTimeBegin, createTimeEnd,shelvesFinishTimeBegin,shelvesFinishTimeEnd);
                    }
                    break;

            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            //compositeFilterItem.Filters.Add(new FilterItem
            //{
            //    MemberName = "StorageCompanyId",
            //    MemberType = typeof(int),
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
            //    Operator = FilterOperator.IsEqualTo
            //});
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "Shelves":
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() != 0) {
                        var selectItems = this.DataGridView.SelectedEntities.Cast<VirtualPartsShelvesTask>().ToArray();
                        bool flag = true;
                        foreach(var item in selectItems) {
                            if(item.Status == (int)DcsPartsShelvesTaskStatus.上架完成) {
                                flag = false;
                            }
                        }
                        var count = selectItems.Select(r => r.WarehouseId).Distinct().Count();
                        //只允许同时上架同一个仓库的上架单
                        if(count > 1) {
                            flag = false;
                        }
                        return flag;
                    }
                    return false;
                default:
                    return false;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExecuteMergeExport(int[] ids, string code, string sourceBillCode, string partsInboundPlanCode, string partsInboundCheckBillCode, int? inboundType, string status, int? warehouseId, string sparePartCode, string sparePartName,bool? noWarehouseArea,string warehouseAreaCode,string warehouseArea, bool? hasDifference, DateTime? begainDate, DateTime? endDate,DateTime? shelvesFinishTimeBegin, DateTime? shelvesFinishTimeEnd) {
            ShellViewModel.Current.IsBusy = false;
            this.excelServiceClient.ExportPartsShelvesTaskAsync(ids, code, sourceBillCode, partsInboundPlanCode, partsInboundCheckBillCode, inboundType, status, warehouseId, sparePartCode, sparePartName,noWarehouseArea,warehouseAreaCode,warehouseArea,hasDifference, begainDate, endDate,shelvesFinishTimeBegin,shelvesFinishTimeEnd);
            this.excelServiceClient.ExportPartsShelvesTaskCompleted -= this.ExcelServiceClient_ExportPartsShelvesTaskCompleted;
            this.excelServiceClient.ExportPartsShelvesTaskCompleted += this.ExcelServiceClient_ExportPartsShelvesTaskCompleted;
        }

        private void ExcelServiceClient_ExportPartsShelvesTaskCompleted(object sender, ExportPartsShelvesTaskCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

    }
}
