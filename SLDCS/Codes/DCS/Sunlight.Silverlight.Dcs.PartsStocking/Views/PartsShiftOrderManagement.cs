﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "WarehouseExecution", "PartsShiftOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT,"PartsShiftOrder"
    })]
    public class PartsShiftOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataInitialApproveView;
        private DataEditViewBase dataFinalApproveView;
        private DataEditViewBase uploadDataEditView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataUpEditView;
        private DataEditViewBase dataDownEditView;
        private DataEditViewBase dataEditEditView;
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";
        private const string DATA_INITIALAPPROVE_VIEW = "_DataInitialApproveView_";
        private const string DATA_FINALAPPROVE_VIEW = "_DataFinalApproveView_";
        protected const string Upload_DATA_EDIT_VIEW = "_UploadDataEditView_";
        private const string DATA_UP_VIEW = "_DataUpView_";
        private const string DATA_DOWN_VIEW = "_DataDownView_";
        private const string DATA_FOREDIT_VIEW = "_DataForEditView_";
        public PartsShiftOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsShiftOrder;
        }

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsShiftOrderWithDetails");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsShiftOrderForDetails");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }
        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsShiftOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private DataEditViewBase DataUpEditView {
             get {
                if(this.dataUpEditView == null) {
                    this.dataUpEditView = DI.GetDataEditView("PartsShiftOrderUp");
                    this.dataUpEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataUpEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataUpEditView;
            }
        }
        private DataEditViewBase DataDownEditView {
             get {
                if(this.dataDownEditView == null) {
                    this.dataDownEditView = DI.GetDataEditView("PartsShiftOrderDown");
                    this.dataDownEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataDownEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDownEditView;
            }
        }
        private DataEditViewBase DataEditEditView {
            get {
                if(this.dataEditEditView == null) {
                    this.dataEditEditView = DI.GetDataEditView("PartsShiftOrderForEdit");
                    this.dataEditEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditEditView;
            }
        }
        private DataEditViewBase DataInitialApproveView {
            get {
                if(this.dataInitialApproveView == null) {
                    this.dataInitialApproveView = DI.GetDataEditView("PartsShiftOrderForInitialApprove");
                    this.dataInitialApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataInitialApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataInitialApproveView;
            }
        }

        private DataEditViewBase DataFinalApproveView {
            get {
                if(this.dataFinalApproveView == null) {
                    this.dataFinalApproveView = DI.GetDataEditView("PartsShiftOrderForFinalApprove");
                    this.dataFinalApproveView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataFinalApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataFinalApproveView;
            }
        }

        //附件上传
        private DataEditViewBase UploadDataEditView {
            get {
                if(this.uploadDataEditView == null) {
                    this.uploadDataEditView = DI.GetDataEditView("PartsShiftOrderForUpload");
                    this.uploadDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.uploadDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.uploadDataEditView;
            }
        }

        private void ResetEditView() {
            this.dataEditView = null;
            this.dataInitialApproveView = null;
            this.dataFinalApproveView = null;
            this.uploadDataEditView = null;
            this.dataDetailView = null;
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            var partsShiftOrderDataEditView = this.DataEditView as PartsShiftOrderDataEditView;
            if(partsShiftOrderDataEditView == null)
                return;           
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_INITIALAPPROVE_VIEW, () => this.DataInitialApproveView);
            this.RegisterView(DATA_FINALAPPROVE_VIEW, () => this.DataFinalApproveView);
            this.RegisterView(Upload_DATA_EDIT_VIEW, () => this.UploadDataEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
            this.RegisterView(DATA_UP_VIEW, () => this.DataUpEditView);
            this.RegisterView(DATA_DOWN_VIEW, () => this.DataDownEditView);
            this.RegisterView(DATA_FOREDIT_VIEW, () => this.DataEditEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsShiftOrder"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null)
                foreach(var filter in compositeFilterItem.Filters) {
                    newCompositeFilterItem.Filters.Add(filter);
                }
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "StorageCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsShiftOrder = this.DataEditView.CreateObjectToEdit<PartsShiftOrder>();
                    partsShiftOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsShiftOrder.Type = (int)DcsPartsShiftOrderType.正常移库;
                    //根据需求文档此字段在服务端赋值
                    partsShiftOrder.StorageCompanyCode = "empty";
                    partsShiftOrder.StorageCompanyName = "empty";
                    partsShiftOrder.QuestionType = null;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_FOREDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                    var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var shiftStatus = filterItem.Filters.Single(e => e.MemberName == "ShiftStatus").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    var entities = this.DataGridView.SelectedEntities;
                    int? id = null;
                    if(entities != null && entities.Any())
                        id = entities.Cast<PartsShiftOrder>().First().Id;
                    this.ExportPartsShiftOrderWithDetail(id, BaseApp.Current.CurrentUserData.EnterpriseId, code, warehouseId, type, status, createTimeBegin, createTimeEnd, shiftStatus);
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow;
                    printWindow = new PartsShiftOrderPrintWindow {
                        Header = PartsStockingUIStrings.DataManagementView_Text_PrintInboundShelves,
                        PartsShiftOrder = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        ((IEditableObject)entity).EndEdit();
                        try {
                            if(entity.Can作废配件移库单) {
                                entity.作废配件移库单();
                            }
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    return;
                                }
                                ShellViewModel.Current.IsBusy = false;
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                                this.DataGridView.ExecuteQuery();
                            }, null);
                        } catch(Exception ex) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;

                case CommonActionKeys.AUDIT:
                    this.DataInitialApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_INITIALAPPROVE_VIEW);
                    break;
                case "Up":
                    this.DataUpEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_UP_VIEW);
                    break;
                case "Down":
                    this.DataDownEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_DOWN_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    this.DataFinalApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_FINALAPPROVE_VIEW);
                    break;
               
                case "UpPrint":
                    var print = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().FirstOrDefault();
                    SunlightPrinter.ShowPrinter("移库上架单打印", "PartsShiftOrderUpPrint", null, true, new Tuple<string, string>("partsShiftOrderId", print.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));
                    break;
                case "DownPrint":
                    var downprint = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().FirstOrDefault();
                    SunlightPrinter.ShowPrinter("移库下架单打印", "PartsShiftOrderDownPrint", null, true, new Tuple<string, string>("partsShiftOrderId", downprint.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));
                    break;
                case CommonActionKeys.UPLOAD://附件上传
                    this.UploadDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().First().Id);
                    this.SwitchViewTo(Upload_DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Submit, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        ((IEditableObject)entity).EndEdit();
                        try {
                            if(entity.Can提交配件移库单) {
                                entity.提交配件移库单();
                            }
                            ShellViewModel.Current.IsBusy = false;
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    return;
                                }
                                ShellViewModel.Current.IsBusy = false;
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_SubmitSuccess);
                                this.CheckActionsCanExecute();
                                this.DataGridView.ExecuteQuery();
                            }, null);
                        } catch(Exception ex) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.TERMINATE:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Terminate, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        ((IEditableObject)entity).EndEdit();
                        try {
                            if(entity.Can终止配件移库单) {
                                entity.终止配件移库单();
                                ShellViewModel.Current.IsBusy = false;
                            }
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    return;
                                }
                                ShellViewModel.Current.IsBusy = false;
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_TerminateSuccess);
                                this.CheckActionsCanExecute();
                                this.DataGridView.ExecuteQuery();
                            }, null);
                        } catch(Exception ex) {
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break; 
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "UpPrint":
                case "DownPrint":
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().ToArray();
                    return selectItems.Length == 1;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.AUDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems1 = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().ToArray();
                    return selectItems1.Length == 1 && selectItems1.First().Status == (int)DcsPartsShiftOrderStatus.提交;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems2 = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().ToArray();
                    return selectItems2.Length == 1 && selectItems2.First().Status == (int)DcsPartsShiftOrderStatus.初审通过;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT: 
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems3 = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().ToArray();
                    return selectItems3.Length == 1 && selectItems3.First().Status == (int)DcsPartsShiftOrderStatus.新建;              
                case CommonActionKeys.UPLOAD:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    return true;
                case "Up":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems4 = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().ToArray();
                    return selectItems4.Length == 1 && selectItems4.First().Status == (int)DcsPartsShiftOrderStatus.生效 && (selectItems4.First().ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.部分移库 || selectItems4.First().ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.待移库) && selectItems4.First().IsUp.HasValue && selectItems4.First().IsUp.Value;
                case "Down":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems5 = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().ToArray();
                    return selectItems5.Length == 1 && selectItems5.First().Status == (int)DcsPartsShiftOrderStatus.生效 && (selectItems5.First().ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.部分移库 || selectItems5.First().ShiftStatus == (int)DCSPartsShiftOrderShiftStatus.待移库) && selectItems5.First().IsDown.HasValue && selectItems5.First().IsDown.Value;
                case CommonActionKeys.TERMINATE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems6 = this.DataGridView.SelectedEntities.Cast<PartsShiftOrder>().ToArray();
                    return selectItems6.Length == 1 && (selectItems6.First().ShiftStatus != (int)DCSPartsShiftOrderShiftStatus.移库完成 && selectItems6.First().Status == (int)DcsPartsShiftOrderStatus.生效);
                default:
                    return false;
            }
        }
        private ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExportPartsShiftOrderWithDetail(int? id, int storageCompanyId, string partsShiftOrderCode, int? warehouseId, int? type, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, int? shiftStatus) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsShiftOrderWithDetailAsync(id, storageCompanyId, partsShiftOrderCode, warehouseId, type, status, createTimeBegin, createTimeEnd, shiftStatus);
            this.excelServiceClient.ExportPartsShiftOrderWithDetailCompleted -= excelServiceClient_ExportPartsShiftOrderWithDetailCompleted;
            this.excelServiceClient.ExportPartsShiftOrderWithDetailCompleted += excelServiceClient_ExportPartsShiftOrderWithDetailCompleted;
        }

        protected void excelServiceClient_ExportPartsShiftOrderWithDetailCompleted(object sender, ExportPartsShiftOrderWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


    }
}
