﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsLogisticsRelated", "PartsShippingOrderForConfirm", ActionPanelKeys = new[] {
        "PartsShippingOrderForConfirm"
    })]
    public class PartsShippingOrderForConfirmManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public PartsShippingOrderForConfirmManagement() {
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsShippingOrderForConfirm;
            this.Initializer.Register(this.Initialize);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsShippingOrderForConfirm"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsShippingOrderForConfirm");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsShippingOrderForConfirm"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "ReceivingConfirm":
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    //BasePrintWindow printWindow = new PartsShippingOrderPrintWindow {
                    //    Header = "配件发运单打印",
                    //    PartsShippingOrder = selectedItem
                    //};
                    //printWindow.ShowDialog();

                    SunlightPrinter.ShowPrinter("配件发运单打印", "ReportPartsShippingOrders", null, true, new Tuple<string, string>("partsShippingOrderId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

                    break;
                case "GPSLocationQuery":
                    HtmlPage.Window.Navigate(new Uri("http://www.xmsyhy.com", UriKind.RelativeOrAbsolute), "_blank");
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().Select(r => r.Id).ToArray();
                        this.MergeExport(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        //var receivingCompanyId = filterItem.Filters.Single(e => e.MemberName == "ReceivingCompanyId").Value as int?;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        //var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var closedLoopStatus = filterItem.Filters.Single(e => e.MemberName == "ClosedLoopStatus").Value as int?;
                        var shippingCompanyCode = filterItem.Filters.Single(e => e.MemberName == "ShippingCompanyCode").Value as string;
                        var shippingCompanyName = filterItem.Filters.Single(e => e.MemberName == "ShippingCompanyName").Value as string;
                        //var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var logisticCompanyName = filterItem.Filters.Single(e => e.MemberName == "LogisticCompanyName").Value as string;
                        //var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        //var logisticCompanyCode = filterItem.Filters.Single(e => e.MemberName == "LogisticCompanyCode").Value as string;
                        //var expressCompany = filterItem.Filters.Single(e => e.MemberName == "ExpressCompany").Value as string;
                        //var expressCompanyCode = filterItem.Filters.Single(e => e.MemberName == "ExpressCompanyCode").Value as string;
                        var warehouseName = filterItem.Filters.Single(e => e.MemberName == "WarehouseName").Value as string;
                        DateTime? beginCreateTime = null;
                        DateTime? endCreateTime = null;

                        DateTime? beginRequestedArrivalDate = null;
                        DateTime? endRequestedArrivalDate = null;



                        DateTime? beginConfirmedReceptionTime = null;
                        DateTime? endConfirmedReceptionTime = null;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    beginCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    endCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "RequestedArrivalDate") {
                                    beginRequestedArrivalDate = dateTime.Filters.First(r => r.MemberName == "RequestedArrivalDate").Value as DateTime?;
                                    endRequestedArrivalDate = dateTime.Filters.Last(r => r.MemberName == "RequestedArrivalDate").Value as DateTime?;
                                }

                                if(dateTime.Filters.First().MemberName == "ConfirmedReceptionTime") {
                                    beginConfirmedReceptionTime = dateTime.Filters.First(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                    endConfirmedReceptionTime = dateTime.Filters.Last(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                }
                            }
                        }
                        this.MergeExport(new int[] { }, null, code, null, status, null, warehouseName, beginRequestedArrivalDate, endRequestedArrivalDate, null, null, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, shippingCompanyCode, shippingCompanyName, null, null, null, null, logisticCompanyName, null, null, closedLoopStatus);
                    }
                    break;
            }
        }

        private void MergeExport(int[] ids, int? settlementCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginConfirmedReceptionTime, DateTime? endConfirmedReceptionTime, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus) {
            this.excelServiceClient.ExportPartsShippingOrderAsync(ids, settlementCompanyId, BaseApp.Current.CurrentUserData.EnterpriseId, code, type, status, warehouseId, warehouseName, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, logisticCompanyId, logisticCompanyCode, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, null);
            this.excelServiceClient.ExportPartsShippingOrderCompleted -= excelServiceClient_ExportPartsShippingOrderCompleted;
            this.excelServiceClient.ExportPartsShippingOrderCompleted += excelServiceClient_ExportPartsShippingOrderCompleted;
        }

        private void excelServiceClient_ExportPartsShippingOrderCompleted(object sender, ExportPartsShippingOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "GPSLocationQuery":
                    return true;
                case "ReceivingConfirm":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    return entities[0].Status == (int)DcsPartsShippingOrderStatus.已发货;
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    return selectItems.Length == 1;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            //TODO:查询时间范围需要保留时间 23:59:59
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                var timeFilters = compositeFilterItem.Filters.Where(filter => filter is CompositeFilterItem && ((CompositeFilterItem)filter).Filters.Any(item => (new[] {
                    "CreateTime", "ConfirmedReceptionTime", "RequestedArrivalDate", "ShippingDate"
                }).Contains(item.MemberName))).Cast<CompositeFilterItem>();

                if(timeFilters != null && timeFilters.Any()) {
                    foreach(var timeFilter in timeFilters) {
                        DateTime endTime;
                        if(timeFilter.Filters.ElementAt(1).Value is DateTime && DateTime.TryParse(timeFilter.Filters.ElementAt(1).Value.ToString(), out endTime)) {
                            timeFilter.Filters.ElementAt(1).Value = new DateTime(endTime.Year, endTime.Month, endTime.Day, 23, 59, 59);
                        }
                    }
                }
            } else
                compositeFilterItem.Filters.Add(filterItem);
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "ReceivingCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}