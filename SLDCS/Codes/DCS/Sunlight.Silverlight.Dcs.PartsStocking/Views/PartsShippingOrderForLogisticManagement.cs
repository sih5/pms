﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsLogisticsRelated", "PartsShippingOrderForLogistic", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT_MERGEEXPORT_PRINT_FDPrint,"PartsShippingOrderForLogistic"
    })]
    public class PartsShippingOrderForLogisticManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATAEDITVIEWFOREDIT = "_dataEditViewForEdit_";
        private const string DATA_EDIT_VIEW_SENDCONFIRM_IMPORT = "_DataEditViewSendConfirmImport_";
        private const string DATA_EDIT_VIEW_SENDINGCONFIRM_IMPORT = "_DataEditViewSendingConfirmImport_";
        private const string DATA_EDIT_VIEW_FOR_SENDINGCONFIRM = "_dataEditViewForSendingConfirm_";
        private const string DATA_EDIT_VIEW_FOR_SENDCONFIRM = "_dataEditViewForSendConfirm_";
        private const string DATA_EDIT_VIEW_FOR_EXTENDEDBACKCONFIRM = "_dataEditViewForExtendedBackConfirm_";
        private const string DATA_EDIT_VIEW_FOR_GPS = "_dataEditViewForGPS_";

        public PartsShippingOrderForLogisticManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsShippingOrderForLogistic;
        }

        //private DataEditViewBase dataEditViewForDeliveryConfirm;
        //private DataEditViewBase DataEditViewForDeliveryConfirm {
        //    get {
        //        if(dataEditViewForDeliveryConfirm == null) {
        //            dataEditViewForDeliveryConfirm = DI.GetDataEditView("PartsShippingOrderForDeliveryConfirm");
        //            dataEditViewForDeliveryConfirm.EditSubmitted += this.DataEditView_EditSubmitted;
        //            dataEditViewForDeliveryConfirm.EditCancelled += this.DataEditView_EditCancelled;
        //        }
        //        return dataEditViewForDeliveryConfirm;
        //    }
        //}
        private DataEditViewBase dataEditViewForGPS;
        private DataEditViewBase DataEditViewForGPS {
            get {
                if(this.dataEditViewForGPS == null) {
                    this.dataEditViewForGPS = DI.GetDataEditView("PartsShippingOrderForGPS");
                    this.dataEditViewForGPS.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditViewForGPS.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditViewForGPS;
            }
        }

        private DataEditViewBase dataSendingConfirmView;
        private DataEditViewBase DataSendingConfirmView {
            get {
                if(this.dataSendingConfirmView == null) {
                    this.dataSendingConfirmView = DI.GetDataEditView("PartsShippingOrderForSendingConfirm");
                    this.dataSendingConfirmView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataSendingConfirmView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataSendingConfirmView;
            }
        }

        private DataEditViewBase dataSendConfirmView;
        private DataEditViewBase DataSendConfirmView {
            get {
                if(this.dataSendConfirmView == null) {
                    this.dataSendConfirmView = DI.GetDataEditView("PartsShippingOrderForSendConfirm");
                    this.dataSendConfirmView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataSendConfirmView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataSendConfirmView;
            }
        }

        private DataEditViewBase dataExtendedBackConfirmView;
        private DataEditViewBase DataExtendedBackConfirmView {
            get {
                if(this.dataExtendedBackConfirmView == null) {
                    this.dataExtendedBackConfirmView = DI.GetDataEditView("PartsShippingOrderForExtendedBackConfirm");
                    this.dataExtendedBackConfirmView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataExtendedBackConfirmView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataExtendedBackConfirmView;
            }
        }

        private DataEditViewBase dataeditviewsendconfirmimport;//送达确认（批量）
        private DataEditViewBase DataEditViewSendConfirmImport {
            get {
                if(this.dataeditviewsendconfirmimport == null) {
                    this.dataeditviewsendconfirmimport = DI.GetDataEditView("PartsShippingOrderForSendConfirmImport");
                    this.dataeditviewsendconfirmimport.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataeditviewsendconfirmimport.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataeditviewsendconfirmimport;
            }
        }

        private DataEditViewBase dataeditviewsendingconfirmimport;//发货确认（批量）
        private DataEditViewBase DataEditViewSendingConfirmImport {
            get {
                if(this.dataeditviewsendingconfirmimport == null) {
                    this.dataeditviewsendingconfirmimport = DI.GetDataEditView("PartsShippingOrderForSendingConfirmImport");
                    this.dataeditviewsendingconfirmimport.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataeditviewsendingconfirmimport.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataeditviewsendingconfirmimport;
            }
        }

        private void ResetEditView() {
            this.dataEditViewForGPS = null;
            this.dataSendingConfirmView= null;
            this.dataSendConfirmView= null;
            this.dataExtendedBackConfirmView= null;
            this.dataeditviewsendconfirmimport= null;
            this.dataeditviewsendingconfirmimport = null;

        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsShippingOrderforLogistic"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            //this.RegisterView(DATAEDITVIEWFOREDIT, () => this.DataEditViewForDeliveryConfirm);
            this.RegisterView(DATA_EDIT_VIEW_SENDINGCONFIRM_IMPORT, () => this.DataEditViewSendingConfirmImport);//发货确认（批量）
            this.RegisterView(DATA_EDIT_VIEW_SENDCONFIRM_IMPORT, () => this.DataEditViewSendConfirmImport);//送达确认（批量）
            this.RegisterView(DATA_EDIT_VIEW_FOR_SENDINGCONFIRM, () => this.DataSendingConfirmView);//发货确认
            this.RegisterView(DATA_EDIT_VIEW_FOR_SENDCONFIRM, () => this.DataSendConfirmView);//送达确认
            this.RegisterView(DATA_EDIT_VIEW_FOR_EXTENDEDBACKCONFIRM, () => this.DataExtendedBackConfirmView);//超期或电商运单流水反馈
            this.RegisterView(DATA_EDIT_VIEW_FOR_GPS, () => this.DataEditViewForGPS);//GPS定位器设备号登记
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsShippingOrderForLogistic"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                //case "DeliveryConfirm":
                //    if(this.DataGridView.SelectedEntities == null)
                //        return false;
                //    var entities = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                //    if(entities.Length != 1)
                //        return false;
                //    return entities[0].Status == (int)DcsSupplierShippingOrderStatus.新建;
                case CommonActionKeys.PRINT:
                case CommonActionKeys.FDPRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    return selectItems.Length == 1;
                case "PrintNoPrice":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItemNoPrices = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    return selectItemNoPrices.Length == 1;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "SendingConfirm"://发货确认
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    return entitie[0].Status == (int)DcsPartsShippingOrderStatus.新建;
                case "SendConfirm"://送达确认
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    var entitie2 = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    return entitie2[0].Status == (int)DcsPartsShippingOrderStatus.已发货;
                case "ExtendedBackConfirm"://超期或电商运单流水反馈
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    var entitie3 = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    return (entitie3[0].Status == (int)DcsPartsShippingOrderStatus.新建
                        || entitie3[0].Status == (int)DcsPartsShippingOrderStatus.已发货
                        || entitie3[0].Status == (int)DcsPartsShippingOrderStatus.待提货
                        || entitie3[0].Status == (int)DcsPartsShippingOrderStatus.待收货);
                //&& entitie3[0].RequestedArrivalDate < DateTime.Now;
                case "SendingConfirmBatch"://发货确认 批量
                case "SendConfirmBatch"://送达确认 批量
                case "GPSLocationQuery":
                    return true;
                case "GPSLocationRegister":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems1 = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    return selectItems1.Length == 1;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                //case "DeliveryConfirm":
                //    this.DataEditViewForDeliveryConfirm.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                //    this.SwitchViewTo(DATAEDITVIEWFOREDIT);
                //    break;
                case "GPSLocationQuery":
                    HtmlPage.Window.Navigate(new Uri("http://www.xmsyhy.com", UriKind.RelativeOrAbsolute), "_blank");
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().FirstOrDefault();
                    if (selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new PartsShippingOrderPrintWindow {
                        Header = "配件发运单打印",
                        PartsShippingOrder = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case CommonActionKeys.FDPRINT:
                    var selectedItemFD = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().FirstOrDefault();
                    if (selectedItemFD == null)
                        return;
                    BasePrintWindow printWindow1 = new PartsShippingOrderPrintWindowFD {
                        Header = "配件发运单打印(福戴)",
                        PartsShippingOrder = selectedItemFD
                    };
                    printWindow1.ShowDialog();
                    break;
                case "PrintNoPrice":
                    var selectedItemNoPrice = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().FirstOrDefault();
                    if(selectedItemNoPrice == null)
                        return;
                    BasePrintWindow printWindowNoPrice = new PartsShippingOrderPrintNoPriceWindow {
                        Header = "配件发运单打印（无价格）",
                        PartsShippingOrderNoPrice = selectedItemNoPrice
                    };
                    printWindowNoPrice.ShowDialog();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().Select(r => r.Id).ToArray();
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                            this.MergeExport(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }
                        if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                            this.Export(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        //var settlementCompanyId = filterItem.Filters.Single(e => e.MemberName == "SettlementCompanyId").Value as int?;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var closedLoopStatus = filterItem.Filters.Single(e => e.MemberName == "ClosedLoopStatus").Value as int?;
                        var receivingCompanyCode = filterItem.Filters.Single(e => e.MemberName == "ReceivingCompanyCode").Value as string;
                        var receivingCompanyName = filterItem.Filters.Single(e => e.MemberName == "ReceivingCompanyName").Value as string;
                       // var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                      //  var logisticCompanyName = filterItem.Filters.Single(e => e.MemberName == "LogisticCompanyName").Value as string;
                      //  var expressCompany = filterItem.Filters.Single(e => e.MemberName == "ExpressCompany").Value as string;
                       // var expressCompanyCode = filterItem.Filters.Single(e => e.MemberName == "ExpressCompanyCode").Value as string;
                        DateTime? beginCreateTime = null;
                        DateTime? endCreateTime = null;

                        DateTime? beginRequestedArrivalDate = null;
                        DateTime? endRequestedArrivalDate = null;

                        DateTime? beginShippingDate = null;
                        DateTime? endShippingDate = null;

                        DateTime? beginConfirmedReceptionTime = null;
                        DateTime? endConfirmedReceptionTime = null;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    beginCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    endCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "RequestedArrivalDate") {
                                    beginRequestedArrivalDate = dateTime.Filters.First(r => r.MemberName == "RequestedArrivalDate").Value as DateTime?;
                                    endRequestedArrivalDate = dateTime.Filters.Last(r => r.MemberName == "RequestedArrivalDate").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "ShippingDate") {
                                    beginShippingDate = dateTime.Filters.First(r => r.MemberName == "ShippingDate").Value as DateTime?;
                                    endShippingDate = dateTime.Filters.Last(r => r.MemberName == "ShippingDate").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "ConfirmedReceptionTime") {
                                    beginConfirmedReceptionTime = dateTime.Filters.First(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                    endConfirmedReceptionTime = dateTime.Filters.Last(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                }
                            }
                        }
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId))
                            this.MergeExport(new int[] { }, null, code, type, status, warehouseId, null, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, null, null, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, closedLoopStatus);
                        if(CommonActionKeys.EXPORT.Equals(uniqueId))
                            this.Export(new int[] { }, null, code, type, status, warehouseId, null, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, null, null, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, closedLoopStatus);
                    }

                    break;
                case "SendingConfirm"://发货确认
                    this.DataSendingConfirmView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_FOR_SENDINGCONFIRM);
                    break;
                case "SendConfirm"://送达确认
                    this.DataSendConfirmView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_FOR_SENDCONFIRM);
                    break;
                case "ExtendedBackConfirm"://超期或电商运单流水反馈
                    this.DataExtendedBackConfirmView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_FOR_EXTENDEDBACKCONFIRM);
                    break;
                case "SendingConfirmBatch"://发货确认 批量
                    this.SwitchViewTo(DATA_EDIT_VIEW_SENDINGCONFIRM_IMPORT);
                    break;
                case "SendConfirmBatch"://送达确认 批量

                    this.SwitchViewTo(DATA_EDIT_VIEW_SENDCONFIRM_IMPORT);
                    break;
                case "GPSLocationRegister":
                    this.DataEditViewForGPS.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_FOR_GPS);
                    break;
            }
        }

        private void MergeExport(int[] ids, int? settlementCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginConfirmedReceptionTime, DateTime? endConfirmedReceptionTime, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus) {
            this.excelServiceClient.ExportPartsShippingOrderAsync(ids, settlementCompanyId, null, code, type, status, warehouseId, warehouseName, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, logisticCompanyId, logisticCompanyCode, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, null);
            this.excelServiceClient.ExportPartsShippingOrderCompleted -= excelServiceClient_ExportPartsShippingOrderCompleted;
            this.excelServiceClient.ExportPartsShippingOrderCompleted += excelServiceClient_ExportPartsShippingOrderCompleted;
        }

        private void Export(int[] ids, int? settlementCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginConfirmedReceptionTime, DateTime? endConfirmedReceptionTime, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus) {
            this.excelServiceClient.ExportPartsShippingOrderNotWithDetailAsync(ids, settlementCompanyId, null, code, type, status, warehouseId, warehouseName, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, logisticCompanyId, logisticCompanyCode, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, null);
            this.excelServiceClient.ExportPartsShippingOrderNotWithDetailCompleted -= excelServiceClient_ExportPartsShippingOrderNotWithDetailCompleted;
            this.excelServiceClient.ExportPartsShippingOrderNotWithDetailCompleted += excelServiceClient_ExportPartsShippingOrderNotWithDetailCompleted;
        }

        private void excelServiceClient_ExportPartsShippingOrderCompleted(object sender, ExportPartsShippingOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void excelServiceClient_ExportPartsShippingOrderNotWithDetailCompleted(object sender, ExportPartsShippingOrderNotWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                var timeFilters = compositeFilterItem.Filters.Where(filter => filter is CompositeFilterItem && ((CompositeFilterItem)filter).Filters.Any(item => (new[] {
                        "CreateTime", "ConfirmedReceptionTime", "RequestedArrivalDate", "ShippingDate"
                    }).Contains(item.MemberName))).Cast<CompositeFilterItem>();
                if(timeFilters != null && timeFilters.Any()) {
                    foreach(var timeFilter in timeFilters) {
                        DateTime endTime;
                        if(timeFilter.Filters.ElementAt(1).Value is DateTime && DateTime.TryParse(timeFilter.Filters.ElementAt(1).Value.ToString(), out endTime)) {
                            timeFilter.Filters.ElementAt(1).Value = new DateTime(endTime.Year, endTime.Month, endTime.Day, 23, 59, 59);
                        }
                    }
                }
            } else
                compositeFilterItem.Filters.Add(filterItem);
            //compositeFilterItem.Filters.Add(new FilterItem {
            //    MemberName = "LogisticCompanyId",
            //    MemberType = typeof(int),
            //    Operator = FilterOperator.IsEqualTo,
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId
            //});
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
