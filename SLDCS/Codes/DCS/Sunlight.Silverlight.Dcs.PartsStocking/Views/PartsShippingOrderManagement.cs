﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsLogisticsRelated", "PartsShippingOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_MAINEDIT_ABANDON, "PartsShippingOrder", CommonActionKeys.EXPORT_MERGEEXPORT_PRINT_FDPrint_EXPORTCUSTOMERINFO
    })]
    public class PartsShippingOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewTransportLossesDispose;
        private DataEditViewBase forDataEditView;
        private DataEditViewBase mainEditDataEditView;
        private const string DATA_VIEW_MAIN_EDIT_VIEW = "_DataEditViewMainEditDataEditView_";
        private const string DATA_VIEW_FOR_EDIT_VIEW = "_DataEditViewForDataEditView_";
        private const string DATA_VIEW_TLD_EDIT_VIEW = "_DataEditViewTransportLossesDispose_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public PartsShippingOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsShippingOrder;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsShippingOrderWithDetails"));
            }
        }

        //修改
        private DataEditViewBase ForDataEditView {
            get {
                if(this.forDataEditView == null) {
                    this.forDataEditView = DI.GetDataEditView("PartsShippingOrderFor");
                    this.forDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.forDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.forDataEditView;
            }
        }

        //主单修改
        private DataEditViewBase MainEditDataEditView {
            get {
                if(this.mainEditDataEditView == null) {
                    this.mainEditDataEditView = DI.GetDataEditView("PartsShippingOrderMainEdit");
                    this.mainEditDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.mainEditDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.mainEditDataEditView;
            }
        }

        private DataEditViewBase DataEditViewTransportLossesDispose {
            get {
                if(this.dataEditViewTransportLossesDispose == null) {
                    this.dataEditViewTransportLossesDispose = DI.GetDataEditView("PartsShippingOrderForTransportLossesDispose");
                    this.dataEditViewTransportLossesDispose.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewTransportLossesDispose.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewTransportLossesDispose;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsShippingOrder");
                    //this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewTransportLossesDispose = null;
            this.forDataEditView = null;
            this.mainEditDataEditView = null;
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_VIEW_MAIN_EDIT_VIEW, () => this.MainEditDataEditView);
            this.RegisterView(DATA_VIEW_FOR_EDIT_VIEW, () => this.ForDataEditView);
            this.RegisterView(DATA_VIEW_TLD_EDIT_VIEW, () => this.DataEditViewTransportLossesDispose);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsShippingOrder"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var edit = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    if(edit.Length != 1)
                        return false;
                    return edit[0].Status == (int)DcsPartsShippingOrderStatus.新建;
                case CommonActionKeys.MAINEDIT:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entitiesEdit = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    if(entitiesEdit.Length != 1)
                        return false;
                    return entitiesEdit[0].Status == (int)DcsPartsShippingOrderStatus.已发货;
                case CommonActionKeys.ABANDON:
                case "ShippingConfirm":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var shipping = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    return shipping[0].Status == (int)DcsPartsShippingOrderStatus.新建;
                case "ReceiptConfirm":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    return entities.Length == 1;
                case "TransportLossesDispose":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var partsShippingOrder = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    if(partsShippingOrder.Length != 1)
                        return false;
                    if(partsShippingOrder[0].Status == (int)DcsPartsShippingOrderStatus.收货确认 && partsShippingOrder[0].IsTransportLosses == true && partsShippingOrder[0].TransportLossesDisposeStatus == (int)DcsPartsShippingOrderTransportLossesDisposeStatus.未处理 && partsShippingOrder[0].Type != (int)DcsPartsShippingOrderType.退货)
                        return true;
                    return false;
                //if(String.CompareOrdinal(uniqueId, "ReceiptConfirm") == 0)
                //    return entities[0].Status == (int)DcsPartsShippingOrderStatus.收货确认;
                //if(String.CompareOrdinal(uniqueId, CommonActionKeys.EDIT) == 0 || String.CompareOrdinal(uniqueId, CommonActionKeys.ABANDON) == 0)
                //    return entities[0].Status == (int)DcsPartsShippingOrderStatus.新建;
                //return false;
                case"A4Print":
                case CommonActionKeys.PRINT:
                case CommonActionKeys.FDPRINT:
                case "PrintNoPrice":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().ToArray();
                    return selectItems.Length == 1;
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.MERGEEXPORT:
                case CommonActionKeys.EXPORTCUSTOMERINFO:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "GPSLocationQuery":
                    return true;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsShippingOrder = this.DataEditView.CreateObjectToEdit<PartsShippingOrder>();
                    partsShippingOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsShippingOrder.TransportCostSettleStatus = false;
                    partsShippingOrder.Status = (int)DcsPartsShippingOrderStatus.新建;
                    partsShippingOrder.ShippingDate = DateTime.Now;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    var entityForEdit = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().SingleOrDefault();
                    if(entityForEdit == null)
                        return;
                    if(entityForEdit.Status != (int)DcsPartsShippingOrderStatus.新建) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotEdit);
                        return;
                    }
                    this.ForDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_VIEW_FOR_EDIT_VIEW);
                    break;
                case CommonActionKeys.MAINEDIT:
                    this.MainEditDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_VIEW_MAIN_EDIT_VIEW);
                    break;
                case "TransportLossesDispose":
                    var entityForTransportLossesDispose = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().SingleOrDefault();
                    if(entityForTransportLossesDispose == null)
                        return;
                    this.DataEditViewTransportLossesDispose.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_VIEW_TLD_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    var entityForAbandon = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().SingleOrDefault();
                    if(entityForAbandon == null)
                        return;
                    if(entityForAbandon.Status != (int)DcsPartsShippingOrderStatus.新建) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotAbandon);
                        return;
                    }
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        try {
                            if(entityForAbandon.Can作废配件发运单)
                                entityForAbandon.作废配件发运单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError)
                                    submitOp.MarkErrorAsHandled();
                                else
                                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "ShippingConfirm":
                    var shippingConfirm = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().SingleOrDefault();
                    if(shippingConfirm == null)
                        return;
                    if(shippingConfirm.Status != (int)DcsPartsShippingOrderStatus.新建) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_ShippingConfirmNotification);
                        return;
                    }
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Shipping, () => {
                        try {
                            ShellViewModel.Current.IsBusy = true;
                            if(shippingConfirm.Can发运确认)
                                shippingConfirm.发运确认();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                } else
                                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_ShippingConfirmSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                            ShellViewModel.Current.IsBusy = false;
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "ReceiptConfirm":
                    var entityForConfirm = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().SingleOrDefault();
                    if(entityForConfirm == null)
                        return;
                    if(entityForConfirm.Status != (int)DcsPartsShippingOrderStatus.收货确认) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_StatusIsNotReceivingConfirmCanNotReceiptConfirm);
                        return;
                    }
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_ReceiptConfirm, () => {
                        try {
                            if(entityForConfirm.Can回执确认配件发运单)
                                entityForConfirm.回执确认配件发运单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError)
                                    submitOp.MarkErrorAsHandled();
                                else
                                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_ReceiptConfirmSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    //BasePrintWindow printWindow = new PartsShippingOrderPrintWindow {
                    //    Header = "配件发运单打印",
                    //    PartsShippingOrder = selectedItem
                    //};
                    //printWindow.ShowDialog();
                    SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataEditView_Text_PartsShippingOrderPrint, "ReportPartsShippingOrders", null, true, new Tuple<string, string>("partsShippingOrderId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

                    break;
                case "A4Print":
                    var selectedItems = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().FirstOrDefault();
                    if(selectedItems == null)
                        return;
                    //BasePrintWindow printWindow = new PartsShippingOrderPrintWindow {
                    //    Header = "配件发运单打印",
                    //    PartsShippingOrder = selectedItem
                    //};
                    //printWindow.ShowDialog();
                    SunlightPrinter.ShowPrinter(PartsStockingUIStrings.DataEditView_Text_PartsShippingOrderPrint, "ReportPartsShippingOrdersA4", null, true, new Tuple<string, string>("partsShippingOrderId", selectedItems.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

                    break;
                case CommonActionKeys.FDPRINT:
                    var selectedItemFD = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().FirstOrDefault();
                    if(selectedItemFD == null)
                        return;
                    BasePrintWindow printWindow1 = new PartsShippingOrderPrintWindowFD {
                        Header = "配件发运单打印(福戴)",
                        PartsShippingOrder = selectedItemFD
                    };
                    printWindow1.ShowDialog();
                    break;
                case "PrintNoPrice":
                    var selectedItemNoPrice = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().FirstOrDefault();
                    if(selectedItemNoPrice == null)
                        return;
                    BasePrintWindow printWindowNoPrice = new PartsShippingOrderPrintNoPriceWindow {
                        Header = PartsStockingUIStrings.DataEditView_Text_PartsShippingOrderPrint,
                        PartsShippingOrderNoPrice = selectedItemNoPrice
                    };
                    printWindowNoPrice.ShowDialog();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.EXPORTCUSTOMERINFO:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsShippingOrder>().Select(r => r.Id).ToArray();
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId)) {
                            this.MergeExport(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }
                        if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                            this.Export(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }
                        if(CommonActionKeys.EXPORTCUSTOMERINFO.Equals(uniqueId)) {
                            this.ExportPartsShippingCustomerInfo(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }

                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var settlementCompanyId = filterItem.Filters.Single(e => e.MemberName == "SettlementCompanyId").Value as int?;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var warehouseId = filterItem.Filters.Single(e => e.MemberName == "WarehouseId").Value as int?;
                        var closedLoopStatus = filterItem.Filters.Single(e => e.MemberName == "ClosedLoopStatus").Value as int?;
                        //  var receivingCompanyCode = filterItem.Filters.Single(e => e.MemberName == "ReceivingCompanyCode").Value as string;
                        var receivingCompanyName = filterItem.Filters.Single(e => e.MemberName == "ReceivingCompanyName").Value as string;
                        // var receivingWarehouseName = filterItem.Filters.Single(e => e.MemberName == "ReceivingWarehouseName").Value as string;
                        //  var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        //  var logisticCompanyName = filterItem.Filters.Single(e => e.MemberName == "LogisticCompanyName").Value as string;
                        //  var expressCompany = filterItem.Filters.Single(e => e.MemberName == "ExpressCompany").Value as string;
                        //  var expressCompanyCode = filterItem.Filters.Single(e => e.MemberName == "ExpressCompanyCode").Value as string;
                        //   var erpSourceOrderCode = filterItem.Filters.Single(e => e.MemberName == "ERPSourceOrderCodeQuery").Value as string;
                        //var originalRequirementBillCode = filterItem.Filters.Single(e => e.MemberName == "OriginalRequirementBillCode").Value as string;
                        DateTime? beginCreateTime = null;
                        DateTime? endCreateTime = null;

                        DateTime? beginRequestedArrivalDate = null;
                        DateTime? endRequestedArrivalDate = null;

                        DateTime? beginConfirmedReceptionTime = null;
                        DateTime? endConfirmedReceptionTime = null;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    beginCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    endCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "RequestedArrivalDate") {
                                    beginRequestedArrivalDate = dateTime.Filters.First(r => r.MemberName == "RequestedArrivalDate").Value as DateTime?;
                                    endRequestedArrivalDate = dateTime.Filters.Last(r => r.MemberName == "RequestedArrivalDate").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "ConfirmedReceptionTime") {
                                    beginConfirmedReceptionTime = dateTime.Filters.First(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                    endConfirmedReceptionTime = dateTime.Filters.Last(r => r.MemberName == "ConfirmedReceptionTime").Value as DateTime?;
                                }
                            }
                        }
                        if(CommonActionKeys.MERGEEXPORT.Equals(uniqueId))
                            this.MergeExport(new int[] { }, settlementCompanyId, code, type, status, warehouseId, null, beginRequestedArrivalDate, endRequestedArrivalDate, null, null, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, null, receivingCompanyName, null, null, null, null, null, null, null, closedLoopStatus, null, null);
                        if(CommonActionKeys.EXPORT.Equals(uniqueId))
                            this.Export(new int[] { }, settlementCompanyId, code, type, status, warehouseId, null, beginRequestedArrivalDate, endRequestedArrivalDate, null, null, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, null, receivingCompanyName, null, null, null, null, null, null, null, closedLoopStatus, null, null);
                        if(CommonActionKeys.EXPORTCUSTOMERINFO.Equals(uniqueId)) {
                            this.ExportPartsShippingCustomerInfo(new int[] { }, settlementCompanyId, code, type, status, warehouseId, null, beginRequestedArrivalDate, endRequestedArrivalDate, null, null, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, null, receivingCompanyName, null, null, null, null, null, null, null, closedLoopStatus, null);
                        }
                    }
                    break;
                case "GPSLocationQuery":
                    HtmlPage.Window.Navigate(new Uri("http://www.xmsyhy.com", UriKind.RelativeOrAbsolute), "_blank");
                    break;
            }
        }

        private void MergeExport(int[] ids, int? settlementCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginConfirmedReceptionTime, DateTime? endConfirmedReceptionTime, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string erpSourceOrderCode, string originalRequirementBillCode) {
            this.excelServiceClient.ExportPartsShippingOrder1Async(ids, settlementCompanyId, null, code, type, status, warehouseId, warehouseName, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, logisticCompanyId, logisticCompanyCode, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, erpSourceOrderCode, originalRequirementBillCode);
            this.excelServiceClient.ExportPartsShippingOrder1Completed -= excelServiceClient_ExportPartsShippingOrder1Completed;
            this.excelServiceClient.ExportPartsShippingOrder1Completed += excelServiceClient_ExportPartsShippingOrder1Completed;
        }

        private void Export(int[] ids, int? settlementCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginConfirmedReceptionTime, DateTime? endConfirmedReceptionTime, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string erpSourceOrderCode, string originalRequirementBillCode) {
            this.excelServiceClient.ExportPartsShippingOrderNotWithDetail1Async(ids, settlementCompanyId, null, code, type, status, warehouseId, warehouseName, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, logisticCompanyId, logisticCompanyCode, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, erpSourceOrderCode, originalRequirementBillCode);
            this.excelServiceClient.ExportPartsShippingOrderNotWithDetail1Completed -= excelServiceClient_ExportPartsShippingOrderNotWithDetail1Completed;
            this.excelServiceClient.ExportPartsShippingOrderNotWithDetail1Completed += excelServiceClient_ExportPartsShippingOrderNotWithDetail1Completed;
        }

        private void ExportPartsShippingCustomerInfo(int[] ids, int? settlementCompanyId, string code, int? type, int? status, int? warehouseId, string warehouseName, DateTime? beginRequestedArrivalDate, DateTime? endRequestedArrivalDate, DateTime? beginShippingDate, DateTime? endShippingDate, DateTime? beginCreateTime, DateTime? endCreateTime, DateTime? beginConfirmedReceptionTime, DateTime? endConfirmedReceptionTime, string receivingCompanyCode, string receivingCompanyName, string receivingWarehouseName, int? partsSalesCategoryId, int? logisticCompanyId, string logisticCompanyCode, string logisticCompanyName, string expressCompany, string expressCompanyCode, int? closedLoopStatus, string erpSourceOrderCode) {
            this.excelServiceClient.ExportPartsShippingCustomerInfoAsync(ids, settlementCompanyId, null, code, type, status, warehouseId, warehouseName, beginRequestedArrivalDate, endRequestedArrivalDate, beginShippingDate, endShippingDate, beginCreateTime, endCreateTime, beginConfirmedReceptionTime, endConfirmedReceptionTime, receivingCompanyCode, receivingCompanyName, receivingWarehouseName, partsSalesCategoryId, logisticCompanyId, logisticCompanyCode, logisticCompanyName, expressCompany, expressCompanyCode, closedLoopStatus, erpSourceOrderCode);
            this.excelServiceClient.ExportPartsShippingCustomerInfoCompleted -= excelServiceClient_ExportPartsShippingCustomerInfoCompleted;
            this.excelServiceClient.ExportPartsShippingCustomerInfoCompleted += excelServiceClient_ExportPartsShippingCustomerInfoCompleted;
        }

        private void excelServiceClient_ExportPartsShippingOrder1Completed(object sender, ExportPartsShippingOrder1CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void excelServiceClient_ExportPartsShippingOrderNotWithDetail1Completed(object sender, ExportPartsShippingOrderNotWithDetail1CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void excelServiceClient_ExportPartsShippingCustomerInfoCompleted(object sender, ExportPartsShippingCustomerInfoCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                var timeFilters = compositeFilterItem.Filters.Where(filter => filter is CompositeFilterItem && ((CompositeFilterItem)filter).Filters.Any(item => (new[] {
                        "CreateTime", "ConfirmedReceptionTime", "RequestedArrivalDate", "ShippingDate"
                    }).Contains(item.MemberName))).Cast<CompositeFilterItem>();
                if(timeFilters != null && timeFilters.Any()) {
                    foreach(var timeFilter in timeFilters) {
                        DateTime endTime;
                        if(timeFilter.Filters.ElementAt(1).Value is DateTime && DateTime.TryParse(timeFilter.Filters.ElementAt(1).Value.ToString(), out endTime)) {
                            timeFilter.Filters.ElementAt(1).Value = new DateTime(endTime.Year, endTime.Month, endTime.Day, 23, 59, 59);
                        }
                    }
                }
            } else
                compositeFilterItem.Filters.Add(filterItem);
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SettlementCompanyId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}