﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsStore", "PartsStockDetailQuery", ActionPanelKeys = new[] { CommonActionKeys.SCHEDULEREEXPORT_EXPORT, "PartsStockDetailQuery" })]
    public class PartsStockDetailQueryManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsStockDetailQuery"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                //compositeFilterItem.Filters.Add(new FilterItem {
                //    MemberName = "StorageCompanyId",
                //    MemberType = typeof(int),
                //    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                //    Operator = FilterOperator.IsEqualTo
                //});
                var codes = compositeFilterItem.Filters.SingleOrDefault(s => s.MemberName == "SparePartCode");
                if(codes != null && codes.Value != null){
                    compositeFilterItem.Filters.Add(new CompositeFilterItem {
                        MemberName = "SparePartCode",
                        MemberType = typeof(string),
                        Value = string.Join(",",((IEnumerable<string>)codes.Value).ToArray()),
                        Operator = FilterOperator.Contains
                    });
                    compositeFilterItem.Filters.Remove(codes);
                }
                this.DataGridView.FilterItem = compositeFilterItem;
            }
            this.DataGridView.ExecuteQueryDelayed();
        }

        private DcsDomainContext dcsDomainContext;

        private DcsDomainContext DcsDomainContext {
            get {
                return this.dcsDomainContext ?? (this.dcsDomainContext = new DcsDomainContext());
            }
        }
        private RadWindow windowStorage;

        private RadWindow WindowStorage {
            get {
                return this.windowStorage ?? (this.windowStorage = new RadWindow {
                    Content = this.HistoricalStorage,
                    Header = PartsStockingUIStrings.DataManagementView_Text_HistoryOutInBoundQuery,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen
                });
            }
        }
        private DcsCustomExportQueryWindowBase historicalStorage;
        private DcsCustomExportQueryWindowBase HistoricalStorage {
            get {
                if(this.historicalStorage == null) {
                    this.historicalStorage = DI.GetQueryWindow("VirtualOutboundAndInboundBill") as DcsCustomExportQueryWindowBase;
                    this.historicalStorage.Loaded += historicalStorage_Loaded;
                }
                return this.historicalStorage;
            }
        }
        private void historicalStorage_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var warehousePartsStock = this.DataGridView.SelectedEntities.First() as VirtualPartsStock;
            if(warehousePartsStock == null)
                return;
            var queryWindow = sender as DcsCustomExportQueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "WarehouseName", warehousePartsStock.WarehouseName
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "WarehouseName", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SparePartCode", warehousePartsStock.SparePartCode
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "SparePartCode", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SparePartName", warehousePartsStock.SparePartName
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "SparePartName", false
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("WarehouseId", typeof(int), FilterOperator.IsEqualTo, warehousePartsStock.WarehouseId));
            compositeFilterItem.Filters.Add(new FilterItem("SparePartId", typeof(int), FilterOperator.IsEqualTo, warehousePartsStock.SparePartId));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "HistoricalStorage":
                    this.WindowStorage.ShowDialog();
                    break;
                case "Delete":
                    var stockIds = this.DataGridView.SelectedEntities.Cast<VirtualPartsStock>().Select(r => r.PartsStockId).ToArray();
                    this.DcsDomainContext.DeletePartsStockByIds(stockIds, loadOpv => {
                        if(loadOpv.HasError) {
                            if(!loadOpv.IsErrorHandled)
                                loadOpv.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                            return;
                        }
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_DeleteSuccess);
                        this.DataGridView.ExecuteQuery();
                    }, null);
                    break;
                case CommonActionKeys.SCHEDULEREEXPORT:
                case CommonActionKeys.EXPORT:
                case "ExactExport":
                case "Print":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var warehouseId = filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                    var areaCategory = filterItem.Filters.Single(e => e.MemberName == "WarehouseAreaCategory").Value as int?;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var sparePartName = filterItem.Filters.Single(e => e.MemberName == "SparePartName").Value as string;
                    var warehouseAreaCode = filterItem.Filters.Single(e => e.MemberName == "WarehouseAreaCode").Value as string;
                    var warehouseRegionCode = filterItem.Filters.Single(e => e.MemberName == "WarehouseRegionCode").Value as string;
                    var warehouseAreaCategoryId = filterItem.Filters.Single(r => r.MemberName == "WarehouseAreaCategory").Value as int?;
                    var priceType = (int)DcsPartsSalesPricePriceType.基准销售价;
                    var greaterThanZero = filterItem.Filters.Single(e => e.MemberName == "GreaterThanZero").Value as bool?;

                    if(uniqueId.Equals(CommonActionKeys.SCHEDULEREEXPORT)) {
                        var exportParams = new Dictionary<string, string> {
                        {
                            "WarehouseId", (warehouseId ?? (object)string.Empty).ToString()
                        }, {
                            "WarehouseAreaCategory", (areaCategory ?? (object)string.Empty).ToString()
                        }, {
                            "SparePartCode", (sparePartCode ?? string.Empty)
                        }, {
                            "SparePartName", (sparePartName ?? string.Empty)
                        }, {
                            "WarehouseAreaCode", (warehouseAreaCode ?? string.Empty)
                        },
                    };
                        var dcsDomainContext = new DcsDomainContext();
                        dcsDomainContext.AddExportJobToScheduler("ScheduleWarehouseAreaWithPartsStock", exportParams, "配件库存明细", invokeOp => {
                            if(invokeOp.Value)
                                UIHelper.ShowNotification("导出任务已成功加入队列，请明天下载。");
                            else
                                UIHelper.ShowAlertMessage("导出任务未能加入队列，请联系管理员。");
                        }, null);
                    }
                    if(uniqueId.Equals(CommonActionKeys.EXPORT)) {
                        var entities = this.DataGridView.SelectedEntities;
                        int[] ids = null;
                        if(entities != null && entities.Any())
                            ids = entities.Cast<VirtualPartsStock>().Select(r => r.PartsStockId).ToArray();
                        var exportDomainContext = this.DcsDomainContext;
                        exportDomainContext.ExportPartsStock(ids ?? new int[] { }, BaseApp.Current.CurrentUserData.EnterpriseId, warehouseId, warehouseAreaCategoryId, sparePartName, sparePartCode, warehouseAreaCode, priceType, greaterThanZero, warehouseRegionCode,loadOpV =>
                        {
                            if(loadOpV.HasError)
                                return;
                            if(loadOpV.Value == null || string.IsNullOrEmpty(loadOpV.Value)) {
                                UIHelper.ShowNotification(loadOpV.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }
                            if(loadOpV.Value != null && !string.IsNullOrEmpty(loadOpV.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOpV.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    }
                    if(uniqueId.Equals("ExactExport")) {
                        var entities = this.DataGridView.SelectedEntities;
                        int[] ids = null;
                        if(entities != null && entities.Any())
                            ids = entities.Cast<VirtualPartsStock>().Select(r => r.PartsStockId).ToArray();
                        var exportDomainContext = this.DcsDomainContext;
                        if (string.IsNullOrEmpty(sparePartCode)) {
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_PartsCodeIsNotNull);
                            return;
                        }
                        exportDomainContext.ExactExportPartsStock(ids ?? new int[] { }, BaseApp.Current.CurrentUserData.EnterpriseId, warehouseId, warehouseAreaCategoryId, sparePartName, sparePartCode, warehouseAreaCode, priceType, greaterThanZero,loadOpV =>
                        {
                            if(loadOpV.HasError)
                                return;
                            if(loadOpV.Value == null || string.IsNullOrEmpty(loadOpV.Value)) {
                                UIHelper.ShowNotification(loadOpV.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }
                            if(loadOpV.Value != null && !string.IsNullOrEmpty(loadOpV.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOpV.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    }
                    if(uniqueId.Equals("Print")) {
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                            var selected = this.DataGridView.SelectedEntities.Cast<VirtualPartsStock>().Select(r => r.PartsStockId);
                            var partsStockIds = selected as int[] ?? selected.ToArray();
                            if(selected != null && partsStockIds.Count() != 0) {
                                var aa = "";
                                foreach(var partsStockId in partsStockIds) {
                                    if(aa == "") {
                                        aa = partsStockId.ToString();
                                        continue;
                                    }
                                    aa = aa + "," + partsStockId;
                                }
                                BasePrintWindow lablePrintWindow = new VirtualPartsStockForLablePrintWindow {
                                    Header = "标签打印",
                                    VirtualPartsStock = aa
                                };
                                lablePrintWindow.ShowDialog();
                            }
                        } else {
                            WarehouseArea.Clear();
                            var domainContext = this.DcsDomainContext;
                            domainContext.Load(domainContext.查询配件库位库存打印Query(warehouseAreaCategoryId, warehouseId, sparePartCode, sparePartName, warehouseAreaCode), LoadBehavior.RefreshCurrent, loadOp => {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                var selectedAll = loadOp.Entities.Select(r => r.PartsStockId).ToArray();
                                if(selectedAll == null || selectedAll.Length == 0)
                                    return;
                                if(selectedAll.Count() > 2000) {
                                    UIHelper.ShowAlertMessage("打印条数超出2000条，请返回编辑后重新打印");
                                    return;
                                }
                                var aa = "";
                                foreach(var partsStockId in selectedAll) {
                                    if(aa == "") {
                                        aa = partsStockId.ToString();
                                        continue;
                                    }
                                    aa = aa + "," + partsStockId;
                                }
                                BasePrintWindow lablePrintWindow = new VirtualPartsStockForLablePrintWindow {
                                    Header = "标签打印",
                                    VirtualPartsStock = aa
                                };
                                lablePrintWindow.ShowDialog();
                            }, null);
                        }
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.SCHEDULEREEXPORT:
                    return true;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "Print":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "Delete":
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any() && !this.DataGridView.SelectedEntities.Cast<VirtualPartsStock>().Any(r => r.Quantity != 0);
                case "HistoricalStorage":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualPartsStock>().ToArray();
                    if(entities.Length == 1)
                        return true;
                    return false;
                case "ExactExport":
                     var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return false;
                    var sparePartCode = filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any() && !string.IsNullOrEmpty(sparePartCode);
                default:
                    return false;
            }
        }

        private ObservableCollection<WarehouseArea> warehouseArea;

        public ObservableCollection<WarehouseArea> WarehouseArea {
            get {
                return warehouseArea ?? (this.warehouseArea = new ObservableCollection<WarehouseArea>());
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] { 
                    "PartsStockDetailQuery"
                };
            }
        }

        public PartsStockDetailQueryManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsStockDetailQuery;
        }
    }
}
