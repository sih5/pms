﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsStore", "PartsStockHistory", ActionPanelKeys = new[]{
        CommonActionKeys.MERGEEXPORT
    })]
    public class PartsStockHistoryManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public PartsStockHistoryManagement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.QueryPanel_Title_PartsStockChangeLog;
        }

        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsStockHistory"));
            }
        }


        protected override IEnumerable<string> QueryPanelKeys
        {
            get{
                return new[] {
                    "PartsStockHistory"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch(uniqueId){
                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 合并导出参数为 ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualPartsStockHistory>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null);
                    }
                        //否则 合并导出参数为 品牌，配件编号，配件名称，数量，成本价，成本总额，创建时间，修改时间
                    else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var sparePartCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        var warehouseAreaCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseAreaCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseAreaCode").Value as string;
                        var warehouseAreaCategory = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseAreaCategory") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseAreaCategory").Value as int?;
                       
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        foreach(var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                            }
                        }
                        this.ExecuteMergeExport(null, warehouseId, sparePartCode, sparePartName, warehouseAreaCode, warehouseAreaCategory,createTimeBegin, createTimeEnd);
                    }
                    break;

            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExecuteMergeExport(int[] ids,int? warehouseId, string sparePartCode, string sparePartName,string warehouseAreaCode,int? warehouseAreaCategory,DateTime? createTimeBegin, DateTime? createTimeEnd)
        {
            ShellViewModel.Current.IsBusy = false;
            this.excelServiceClient.ExportPartsStockHistoryAsync(ids, warehouseId, sparePartCode, sparePartName, warehouseAreaCode,warehouseAreaCategory, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportPartsStockHistoryCompleted -= this.ExcelServiceClient_ExportPartsStockHistoryCompleted;
            this.excelServiceClient.ExportPartsStockHistoryCompleted += this.ExcelServiceClient_ExportPartsStockHistoryCompleted;
        }

        private void ExcelServiceClient_ExportPartsStockHistoryCompleted(object sender, ExportPartsStockHistoryCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

    }
}
