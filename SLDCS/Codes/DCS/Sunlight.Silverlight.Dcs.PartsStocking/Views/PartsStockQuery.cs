﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using System.Windows.Controls;
using System.Windows;
using Sunlight.Silverlight.Dcs.Controls;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Telerik.Windows.Controls.RibbonView;
using Sunlight.Silverlight.Controls;
using System;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Input;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsStore", "PartsStockQuery", ActionPanelKeys = new[] {
        "PartsStockQuery"
    })]
    public class PartsStockQuery : DcsDataManagementViewBase {
        private Grid filterGrid0;
        private DataGridViewBase dataGridView;
        private IList<RadRibbonTab> customTabs;
        private ObservableCollection<KeyValuePair> kvTaskType;
        private readonly ObservableCollection<KeyValuePair> kvWarehouses = new ObservableCollection<KeyValuePair>();
        private ActionPanelBase actionPanel1;
        private static readonly RoutedUICommand QueryCommand0 = new RoutedUICommand("Query0", "QueryCommand0", typeof(PartsStockQuery));
     //   private readonly UIStringsForXaml uiStrings = new UIStringsForXaml();
        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        public PartsStockQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.QueryPanel_Title_PartsStockQuery;
        }
        protected override IEnumerable<RadRibbonTab> CustomRibbonTabs
        {
            get
            {
                if (this.customTabs == null)
                {
                    this.customTabs = new List<RadRibbonTab>();
                    this.customTabs.Add(new RadRibbonTab
                    {
                        Header = BusinessViewUtils.MakeTabHeader(new Uri("/Sunlight.Silverlight.Shell;component/Images/tab-query.png", UriKind.Relative), "数据查询")
                    });
                    var addTab = new RadRibbonTab
                    {
                        Header = BusinessViewUtils.MakeTabHeader(new Uri("/Sunlight.Silverlight.Shell;component/Images/tab-actions.png", UriKind.Relative), "业务操作")
                    };
                    addTab.MouseLeftButtonUp += addTab_MouseLeftButtonUp;
                    addTab.MouseLeftButtonDown += addTab_MouseLeftButtonUp;
                    this.customTabs.Add(addTab);
                }
                return this.customTabs;
            }
        }
        private void addTab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.actionPanel1.ExchangeData(this, "SetIsEnabled", "HistoricalStorage", this.OnRequestActionCanExecute(this.actionPanel1, "HistoricalStorage"));
            this.actionPanel1.ExchangeData(this, "SetIsEnabled", "LocketUnit", this.OnRequestActionCanExecute(this.actionPanel1, "LocketUnit"));
            this.actionPanel1.ExchangeData(this, "SetIsEnabled", CommonActionKeys.EXPORT, this.OnRequestActionCanExecute(this.actionPanel1, CommonActionKeys.EXPORT));//查询完成手动添加权限
            this.actionPanel1.ExchangeData(this, "SetIsEnabled", "ExactExport", this.OnRequestActionCanExecute(this.actionPanel1, "ExactExport"));//查询完成手动添加权限
            this.actionPanel1.ExchangeData(this, "SetIsEnabled", "PcPrint", this.OnRequestActionCanExecute(this.actionPanel1, "PcPrint"));
            this.actionPanel1.ExchangeData(this, "SetIsEnabled", "ChPrint", this.OnRequestActionCanExecute(this.actionPanel1, "ChPrint"));
            this.CheckActionsCanExecute();
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            DcsUtils.GetKeyValuePairs(new Dictionary<string, ObservableCollection<KeyValuePair>> {
                {
                    "Storage_Center", this.kvTaskType = new ObservableCollection<KeyValuePair>()
                }
            });
          domainContext.Load(domainContext.GetWarehousesOrderByNameWithBranchIdQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.Type != (int)DcsWarehouseType.虚拟库), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                //设置combobox下拉框的值
                foreach(var warehouse in loadOp.Entities) {
                    this.kvWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name
                    });
                }
          }, null);
          //初始化组件            
          this.InitControl();
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsStockQuery"));
            }
        }

        //protected override IEnumerable<string> QueryPanelKeys {
        //    get {
        //        return new[] {
        //            "PartsStockQuery"
        //        };
        //    }
        //}
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return null;
            }
        }

        protected override IEnumerable<string> ActionPanelKeys
        {
            get
            {
                return null;
            }
        }

        private void cusomerEdit_KeyDown(object sender,KeyEventArgs e)
        {
            if(e.Key==Key.Enter){
                //this.DataGridView.ExecuteQueryDelayed();
                var filitem = this.GetFilterValues(this.filterGrid0);
                var warehouseId = filitem["WarehouseId"];
                var storageCenter = filitem["StorageCenter"];
                var spareCode = filitem["SparePartCode"];
                var spareName = filitem["SparePartName"];
                var referenceCode = filitem["ReferenceCode"];
                var newCompositeFilterItem = new CompositeFilterItem();
                newCompositeFilterItem.Filters.Add(new FilterItem
                {
                    MemberName = "WarehouseId",
                    MemberType = typeof(int),
                    Value = warehouseId,
                    Operator = FilterOperator.IsEqualTo
                });
                newCompositeFilterItem.Filters.Add(new FilterItem
                {
                    MemberName = "StorageCenter",
                    MemberType = typeof(int),
                    Value = storageCenter,
                    Operator = FilterOperator.IsEqualTo
                });
                newCompositeFilterItem.Filters.Add(new CompositeFilterItem
                {
                    MemberName = "SparePartCode",
                    MemberType = typeof(IList<string>),
                    Value = spareCode,
                    Operator = FilterOperator.Contains
                });
                newCompositeFilterItem.Filters.Add(new FilterItem
                {
                    MemberName = "SparePartName",
                    MemberType = typeof(string),
                    Value = spareName,
                    Operator = FilterOperator.Contains
                });
                newCompositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "ReferenceCode",
                    MemberType = typeof(string),
                    Value = referenceCode,
                    Operator = FilterOperator.Contains
                });
                this.OnExecutingQuery(null, newCompositeFilterItem);
            }
        }
        private void InitControl()
        {
            if (this.customTabs[0].Items.Count == 0)
            {
                var spareCodeLabel = new TextBlock
                {
                    Text = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartCode,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Margin = new Thickness(3, 5, 4, 5)
                };
                var spareCode = new  Controls.MultipleTextQueryControl(typeof(IList<string>), "SparePartCode")
                {
                    Tag = "SparePartCode",
                    Height=22,
                    Width=120,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Margin = new Thickness(2, 2, 0, 2),
                };
                spareCode.KeyDown += this.cusomerEdit_KeyDown;
                var spareNameLable = new TextBlock
                {
                    Text = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_SparePartName,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Margin = new Thickness(3, 5, 4, 5)
                };
                var spareName = new TextBox
                {
                    Tag = "SparePartName",
                    Height = 22,
                    Width = 120,
                    Margin = new Thickness(2, 2, 0, 2)
                };
                spareName.KeyDown += this.cusomerEdit_KeyDown;
                var warehouseLable = new TextBlock
                {
                    Text = PartsStockingUIStrings.DataEditView_Text_PartsShiftOrder_VirtualPartsStock_WarehouseName,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Margin = new Thickness(8, 5, 4, 5)
                };
                var warehouseCombobox = new DcsComboBox
                {
                    Tag = "WarehouseId",
                    Height = 22,
                    Width = 120,
                    Margin = new Thickness(2, 2, 0, 2),
                    ItemsSource = this.kvWarehouses,
                    CanKeyboardNavigationSelectItems = true,
                    ClearSelectionButtonVisibility = Visibility.Visible,
                    ClearSelectionButtonContent = PartsStockingUIStrings.Action_Title_Remove,
                    IsEditable = false,
                    IsMouseWheelEnabled = true,
                    DisplayMemberPath = "Value",
                    SelectedValuePath = "Key"
                };
                var storageCenterLable = new TextBlock
                {
                    Text = PartsStockingUIStrings.DataGridView_ColumnItem_Title_ExpressToLogistic_FocufingCoreName,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Margin = new Thickness(8, 5, 4, 5)
                };
                var storageCenterCombobox = new DcsComboBox
                {
                    Tag = "StorageCenter",
                    Height = 22,
                    Width = 120,
                    Margin = new Thickness(2, 2, 0, 2),
                    ItemsSource = this.kvTaskType,
                    CanKeyboardNavigationSelectItems = true,
                    ClearSelectionButtonVisibility = Visibility.Visible,
                    ClearSelectionButtonContent = PartsStockingUIStrings.Action_Title_Remove,
                    IsEditable = false,
                    IsMouseWheelEnabled = true,
                    DisplayMemberPath = "Value",
                    SelectedValuePath = "Key"
                };  
                //var planTimeDateTimeBox = new DateTimeRangeBox
                //{
                //    Tag = "TaskTime",
                //    Height = 22,
                //    Width = 300,
                //    Margin = new Thickness(2, 10, 0, 2),
                //    SelectedValueFrom = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-01")).AddMonths(1),
                //    SelectedValueTo = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-01")).AddMonths(2).AddDays(-1)
                //};
                var referenceCode = new TextBox {
                    Tag = "ReferenceCode",
                    Height = 22,
                    Width = 120,
                    Margin = new Thickness(2, 2, 0, 2)
                };
                referenceCode.KeyDown += this.cusomerEdit_KeyDown;
                var referenceCodeLable = new TextBlock {
                    Text = PartsStockingUIStrings.DataGridView_ColumnItem_SihCode,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Margin = new Thickness(8, 5, 4, 5)
                };
                this.filterGrid0 = new Grid();
                this.filterGrid0.RowDefinitions.Add(new RowDefinition
                {
                    Height = new GridLength(0, GridUnitType.Auto)
                });
                this.filterGrid0.RowDefinitions.Add(new RowDefinition
                {
                    Height = new GridLength(0, GridUnitType.Auto)
                });
                this.filterGrid0.ColumnDefinitions.Add(new ColumnDefinition
                {
                    Width = new GridLength(0, GridUnitType.Auto)
                });
                this.filterGrid0.ColumnDefinitions.Add(new ColumnDefinition
                {
                    Width = new GridLength(0, GridUnitType.Auto)
                });
                this.filterGrid0.ColumnDefinitions.Add(new ColumnDefinition
                {
                    Width = new GridLength(0, GridUnitType.Auto)
                });
                this.filterGrid0.ColumnDefinitions.Add(new ColumnDefinition
                {
                    Width = new GridLength(0, GridUnitType.Auto)
                });
                this.filterGrid0.ColumnDefinitions.Add(new ColumnDefinition {
                    Width = new GridLength(0, GridUnitType.Auto)
                });
                this.filterGrid0.ColumnDefinitions.Add(new ColumnDefinition {
                    Width = new GridLength(0, GridUnitType.Auto)
                });
                Grid.SetColumn(spareCodeLabel, 0);
                Grid.SetRow(spareCodeLabel, 0);
                Grid.SetColumn(spareCode, 1);
                Grid.SetRow(spareCode, 0);
                Grid.SetColumn(spareNameLable, 0);
                Grid.SetRow(spareNameLable, 1);
                Grid.SetColumn(spareName, 1);
                Grid.SetRow(spareName, 1);
                Grid.SetColumn(warehouseLable, 2);
                Grid.SetRow(warehouseLable, 0);
                Grid.SetColumn(warehouseCombobox, 3);
                Grid.SetRow(warehouseCombobox, 0);
                Grid.SetColumn(storageCenterLable, 2);
                Grid.SetRow(storageCenterLable, 2);
                Grid.SetColumn(storageCenterCombobox, 3);
                Grid.SetRow(storageCenterCombobox, 2);
                Grid.SetColumn(referenceCodeLable, 4);
                Grid.SetRow(referenceCodeLable, 0);
                Grid.SetColumn(referenceCode, 5);
                Grid.SetRow(referenceCode, 0);

                this.filterGrid0.Children.Add(spareCodeLabel);
                this.filterGrid0.Children.Add(spareCode);
                this.filterGrid0.Children.Add(spareNameLable);
                this.filterGrid0.Children.Add(spareName);
                this.filterGrid0.Children.Add(warehouseLable);
                this.filterGrid0.Children.Add(warehouseCombobox);
                this.filterGrid0.Children.Add(storageCenterLable);
                this.filterGrid0.Children.Add(storageCenterCombobox);
                this.filterGrid0.Children.Add(referenceCodeLable);
                this.filterGrid0.Children.Add(referenceCode);

                var queryButton = new RadRibbonButton
                {
                    Text = "执行查询",
                    Size = ButtonSize.Large,
                    LargeImage = new BitmapImage(new Uri("/Sunlight.Silverlight.Shell;component/Images/query.png", UriKind.Relative)),
                    Command = QueryCommand0,
                    IsTabStop = true
                };
                CommandManager.AddExecutedHandler(queryButton, this.QueryButtonExecute);
                CommandManager.AddCanExecuteHandler(queryButton, this.QueryButtonCanExecute);
                var queryGroup = new RadRibbonGroup
                {
                    FontSize = 14,
                    Header = PartsStockingUIStrings.QueryPanel_Title_PartsStockQuery,
                };
                var queryGroup1 = new RadRibbonGroup
                {
                    FontSize = 14,
                    Header = PartsStockingUIStrings.DataManagementView_Text_StockExplain,
                };
                var remark = new TextBlock
                {
                    Text = PartsStockingUIStrings.DataManagementView_Text_StockExplainDetail + "\n" + PartsStockingUIStrings.DataManagementView_Text_StockExplainDetail2 + "\n" + PartsStockingUIStrings.DataManagementView_Text_StockExplainDetail3,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Right,
                    Foreground = new SolidColorBrush(Colors.Red),
                    Margin = new Thickness(0, 5, 4, 5)
                };
               
             
                queryGroup.Items.Add(this.filterGrid0);
                queryGroup.Items.Add(GetnewRectangle());
                queryGroup.Items.Add(queryButton);
                queryGroup1.Items.Add(remark);
                this.customTabs[0].Items.Add(queryGroup);
                this.customTabs[0].Items.Add(queryGroup1);
            }
            this.actionPanel1 = DI.GetActionPanel("PartsStockQuery");
            this.actionPanel1.ExecutingAction += this.ActionPanelExecutingAction;
            this.customTabs[1].Items.Add(this.actionPanel1);
            this.DataGridView.SelectionChanged += (sender1, e1) =>
            {
                this.actionPanel1.ExchangeData(this, "SetIsEnabled", "HistoricalStorage", this.OnRequestActionCanExecute(this.actionPanel1, "HistoricalStorage"));
                this.actionPanel1.ExchangeData(this, "SetIsEnabled", "LocketUnit", this.OnRequestActionCanExecute(this.actionPanel1, "LocketUnit")); 
                this.actionPanel1.ExchangeData(this, "SetIsEnabled", CommonActionKeys.EXPORT, this.OnRequestActionCanExecute(this.actionPanel1, CommonActionKeys.EXPORT));
                this.actionPanel1.ExchangeData(this, "SetIsEnabled", "ExactExport", this.OnRequestActionCanExecute(this.actionPanel1, "ExactExport"));
                this.actionPanel1.ExchangeData(this, "SetIsEnabled", "PcPrint", this.OnRequestActionCanExecute(this.actionPanel1, "PcPrint"));
                this.actionPanel1.ExchangeData(this, "SetIsEnabled", "ChPrint", this.OnRequestActionCanExecute(this.actionPanel1, "ChPrint"));
                this.CheckActionsCanExecute();
            };
        }
        private void ActionPanelExecutingAction(object sender, ActionPanelRibbonGroup.ExecutingActionEventArgs e)
        {
            if (e.UniqueId == CommonActionKeys.EXPORT)
                this.OnExecutingAction((ActionPanelBase)sender, CommonActionKeys.EXPORT);
            if (e.UniqueId == "HistoricalStorage")
                this.OnExecutingAction((ActionPanelBase)sender, "HistoricalStorage");
            if (e.UniqueId == "LocketUnit")
                this.OnExecutingAction((ActionPanelBase)sender, "LocketUnit");
             if (e.UniqueId == "ExactExport")
                this.OnExecutingAction((ActionPanelBase)sender, "ExactExport");
             if(e.UniqueId == "PcPrint")
                 this.OnExecutingAction((ActionPanelBase)sender, "PcPrint");
             if(e.UniqueId == "ChPrint")
                 this.OnExecutingAction((ActionPanelBase)sender, "ChPrint");
        }
        private static Rectangle GetnewRectangle()
        {
            return new Rectangle
            {
                Width = 2,
                Margin = new Thickness(2),
                HorizontalAlignment = HorizontalAlignment.Center,
                Fill = new LinearGradientBrush
                {
                    StartPoint = new Point(0.5, 0),
                    EndPoint = new Point(0.5, 1),
                    GradientStops = new GradientStopCollection {
                        new GradientStop {
                            Offset = 0,
                            Color = Color.FromArgb(0x10, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 0.25,
                            Color = Color.FromArgb(0x40, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 0.75,
                            Color = Color.FromArgb(0x40, 0x0, 0x0, 0x0)
                        },
                        new GradientStop {
                            Offset = 1,
                            Color = Color.FromArgb(0x10, 0x0, 0x0, 0x0)
                        },
                         new GradientStop {
                            Offset = 1.25,
                            Color = Color.FromArgb(0x10, 0x0, 0x0, 0x0)
                        },
                    },
                }
            };
        }
        private void QueryButtonCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void QueryButtonExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var filitem = this.GetFilterValues(this.filterGrid0);
            var warehouseId = filitem["WarehouseId"];
            var storageCenter = filitem["StorageCenter"];
            var spareCode = filitem["SparePartCode"];
            var spareName = filitem["SparePartName"];
            var referenceCode = filitem["ReferenceCode"];
            var newCompositeFilterItem = new CompositeFilterItem();
            newCompositeFilterItem.Filters.Add(new FilterItem
            {
                MemberName = "WarehouseId",
                MemberType = typeof(int),
                Value = warehouseId,
                Operator = FilterOperator.IsEqualTo
            });
            newCompositeFilterItem.Filters.Add(new FilterItem
            {
                MemberName = "StorageCenter",
                MemberType = typeof(int),
                Value = storageCenter,
                Operator = FilterOperator.IsEqualTo
            });
            newCompositeFilterItem.Filters.Add(new CompositeFilterItem
            {
                MemberName = "SparePartCode",
                MemberType = typeof(IList<string>),
                Value = spareCode,
                Operator = FilterOperator.IsEqualTo
            });
            newCompositeFilterItem.Filters.Add(new FilterItem
            {
                MemberName = "SparePartName",
                MemberType = typeof(string),
                Value = spareName,
                Operator = FilterOperator.Contains
            });
            newCompositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "ReferenceCode",
                MemberType = typeof(string),
                Value = referenceCode,
                Operator = FilterOperator.Contains
            });
            this.SwitchViewTo(DATA_GRID_VIEW);
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
            this.DataGridView.DataLoaded += DataGridView_DataLoaded;
        }
        private void DataGridView_DataLoaded(object sender, EventArgs e)
        {
            this.actionPanel1.ExchangeData(this, "SetIsEnabled", CommonActionKeys.EXPORT, this.OnRequestActionCanExecute(this.actionPanel1, CommonActionKeys.EXPORT));//查询完成手动添加权限
            this.CheckActionsCanExecute();
        }


        public IDictionary<string, object> GetFilterValues(Grid grid)
        {
            var result = new Dictionary<string, object>();
            foreach (var control in grid.Children.ToArray())
            {
                var frameworkElement = control as FrameworkElement;
                if (frameworkElement != null)
                {
                    var itemName = frameworkElement.Tag as string;
                    if (itemName == null)
                        continue;

                    object value = null;
                    if (control is PopupTextBox)
                        value = ((PopupTextBox)control).Text;
                    else if (control is DateTimeRangeBox)
                        value = new[] {
                            ((DateTimeRangeBox)control).SelectedValueFrom, ((DateTimeRangeBox)control).SelectedValueTo
                        };
                    else if (control is RadMaskedTextInput)
                        value = ((RadMaskedTextInput)control).Value;
                    else if (control is CheckBox)
                    {
                        var checkBox = (CheckBox)control;
                        if (checkBox.IsChecked.HasValue)
                            if ((bool)checkBox.IsChecked)
                                value = true;
                            else
                                value = false;
                    }
                    else if (control is DcsComboBox)
                    {
                        var comboBox = (DcsComboBox)control;
                        value = comboBox.SelectedValue;
                    }
                    else if (control is TextBox)
                    {
                        value = ((TextBox)control).Text;
                    }
                    else if (control is MultipleTextQueryControl)
                    {
                        value = ((MultipleTextQueryControl)control).GetFilterValue();
                    }
                    result.Add(itemName, value);
                }
            }
            return result;
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "ExactExport":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return false;
                    var partCodeFilters = filterItem.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "SparePartCode")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any() && (partCodeFilters != null && partCodeFilters.Value != null) ;
                case "HistoricalStorage":
                case "PcPrint":
                case "ChPrint":
                case "LocketUnit":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<WarehousePartsStock>().ToArray();
                    if(entities.Length == 1)
                        return true;
                    return false;
                case "PartsStockSync":
                    return true;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<WarehousePartsStock>().Select(r => r.SparePartId).ToArray();
                        var warehouseIds = this.DataGridView.SelectedEntities.Cast<WarehousePartsStock>().Select(r => r.WarehouseId).ToArray();
                        this.ExportWarehousePartsStock(ids, warehouseIds, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null,null,null);
                    } else {

                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var storageCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var partsName = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SparePartName") == null ? null : filterItem.Filters.Single(e => e.MemberName == "SparePartName").Value as string;
                        //var partsCode = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                        var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var storageCenter = filterItem.Filters.SingleOrDefault(r => r.MemberName == "StorageCenter") == null ? null : filterItem.Filters.Single(r => r.MemberName == "StorageCenter").Value as int?;
                        var referenceCode = filterItem.Filters.SingleOrDefault(e => e.MemberName == "ReferenceCode") == null ? null : filterItem.Filters.Single(e => e.MemberName == "ReferenceCode").Value as string;

                        var partCodeFilters = filterItem.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "SparePartCode")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        if (partCodeFilters != null && partCodeFilters.Value != null) {
                            var codesAll = ((IEnumerable<string>)partCodeFilters.Value).ToArray();
                            ShellViewModel.Current.IsBusy = true;
                            this.ExportWarehousePartsStock(null, null, storageCompanyId, warehouseId, partsName, codesAll, partsSalesCategoryId, storageCenter, referenceCode,false);
                        } else {
                            ShellViewModel.Current.IsBusy = true;
                            this.ExportWarehousePartsStock(null, null, storageCompanyId, warehouseId, partsName, null, partsSalesCategoryId, storageCenter, referenceCode,false);
                        }
                    }
                    break;
                case "ExactExport":
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<WarehousePartsStock>().Select(r => r.SparePartId).ToArray();
                        var warehouseIds = this.DataGridView.SelectedEntities.Cast<WarehousePartsStock>().Select(r => r.WarehouseId).ToArray();
                        this.ExportWarehousePartsStock(ids, warehouseIds, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null, null,null,null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var storageCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var partsName = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SparePartName") == null ? null : filterItem.Filters.Single(e => e.MemberName == "SparePartName").Value as string;
                        //var partsCode = filterItem.Filters.SingleOrDefault(e => e.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                        var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                        var storageCenter = filterItem.Filters.SingleOrDefault(r => r.MemberName == "StorageCenter") == null ? null : filterItem.Filters.Single(r => r.MemberName == "StorageCenter").Value as int?;
                        var referenceCode = filterItem.Filters.SingleOrDefault(e => e.MemberName == "ReferenceCode") == null ? null : filterItem.Filters.Single(e => e.MemberName == "ReferenceCode").Value as string;

                        var partCodeFilters = filterItem.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "SparePartCode")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        if (partCodeFilters != null && partCodeFilters.Value != null) {
                            var codesAll = ((IEnumerable<string>)partCodeFilters.Value).ToArray();
                            ShellViewModel.Current.IsBusy = true;
                            this.ExportWarehousePartsStock(null, null, storageCompanyId, warehouseId, partsName, codesAll, partsSalesCategoryId, storageCenter, referenceCode,true);
                        } else {
                            ShellViewModel.Current.IsBusy = true;
                            this.ExportWarehousePartsStock(null, null, storageCompanyId, warehouseId, partsName, null, partsSalesCategoryId, storageCenter, referenceCode,true);
                        }
                    }
                    break;
                case "HistoricalStorage":
                    this.WindowStorage.ShowDialog();
                    break;
                case "LocketUnit":
                    this.WindowLocketUnit.ShowDialog();
                    break;
                case "PartsStockSync":
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Text_IsManualSet, () => {
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext != null) {
                            domainContext.全量库存生成(invokeOp => {
                                if(invokeOp.HasError) {
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Text_ManualSetSuccess);
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        }
                    });
                    break;
                case "PcPrint":
                    var sparePartIds = this.DataGridView.SelectedEntities.Cast<WarehousePartsStock>().First();
                    var code = sparePartIds.SparePartId + ";" + sparePartIds.WarehouseCode.Substring(sparePartIds.WarehouseCode.Length - 2, 2);
                    this.PcPrintDataEditView.SetObjectToEditById(code);
                    this.PcPrintRadWindow.ShowDialog();
                    break;
                case "ChPrint":
                    var sparePartIds2 = this.DataGridView.SelectedEntities.Cast<WarehousePartsStock>().First();
                    var codes = sparePartIds2.SparePartId + ";" + sparePartIds2.WarehouseCode.Substring(sparePartIds2.WarehouseCode.Length - 2, 2);
                    this.ChPrintDataEditView.SetObjectToEditById(codes);
                    this.ChPrintRadWindow.ShowDialog();
                    break;
            }
        }


        private RadWindow windowStorage;

        private RadWindow WindowStorage {
            get {
                return this.windowStorage ?? (this.windowStorage = new RadWindow {
                    Content = this.HistoricalStorage,
                    Header = PartsStockingUIStrings.DataManagementView_Text_HistoryOutInBoundQuery,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }
        private RadWindow windowLocketUnitQuery;

        private RadWindow WindowLocketUnit {
            get {
                return this.windowLocketUnitQuery ?? (this.windowLocketUnitQuery = new RadWindow {
                    Content = this.HistoricalLocketUnit,
                    Header = PartsStockingUIStrings.Action_Title_LocketUnit,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }
        private QueryWindowBase historicalLocketUnit;

        private QueryWindowBase HistoricalLocketUnit {
            get {
                if(this.historicalLocketUnit == null) {
                    this.historicalLocketUnit = DI.GetQueryWindow("LocketUnitQuery");
                    this.historicalLocketUnit.Loaded += historicalLocketUnit_Loaded;
                }
                return this.historicalLocketUnit;
            }
        }

        private void historicalLocketUnit_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var warehousePartsStock = this.DataGridView.SelectedEntities.First() as WarehousePartsStock;
            if(warehousePartsStock == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "WarehouseName", warehousePartsStock.WarehouseName
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "WarehouseName", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SparePartCode", warehousePartsStock.SparePartCode
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "SparePartCode", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SparePartName", warehousePartsStock.SparePartName
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "SparePartName", false
            });
        }

        private DcsCustomExportQueryWindowBase historicalStorage;
        private DcsCustomExportQueryWindowBase HistoricalStorage {
            get {
                if(this.historicalStorage == null) {
                    this.historicalStorage = DI.GetQueryWindow("VirtualOutboundAndInboundBill") as DcsCustomExportQueryWindowBase;
                    this.historicalStorage.Loaded += historicalStorage_Loaded;
                }
                return this.historicalStorage;
            }
        }

        private void historicalStorage_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var warehousePartsStock = this.DataGridView.SelectedEntities.First() as WarehousePartsStock;
            if(warehousePartsStock == null)
                return;
            var queryWindow = sender as DcsCustomExportQueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "WarehouseName", warehousePartsStock.WarehouseName
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "WarehouseName", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SparePartCode", warehousePartsStock.SparePartCode
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "SparePartCode", false
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "SparePartName", warehousePartsStock.SparePartName
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "SparePartName", false
            });
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("WarehouseId", typeof(int), FilterOperator.IsEqualTo, warehousePartsStock.WarehouseId));
            compositeFilterItem.Filters.Add(new FilterItem("SparePartId", typeof(int), FilterOperator.IsEqualTo, warehousePartsStock.SparePartId));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }


        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        private void ExportWarehousePartsStock(int[] ids, int[] warehouseIds, int storageCompanyId, int? warehouseId, string partsName, string[] partsCodes, int? partsSalesCategoryId, int? storageCenter, string referenceCode,bool? isExactExport) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportWarehousePartsStockAsync(ids, warehouseIds, storageCompanyId, warehouseId, partsName, partsCodes, partsSalesCategoryId, storageCenter, referenceCode,isExactExport);
            this.excelServiceClient.ExportWarehousePartsStockCompleted -= excelServiceClient_ExportWarehousePartsStockCompleted;
            this.excelServiceClient.ExportWarehousePartsStockCompleted += excelServiceClient_ExportWarehousePartsStockCompleted;

        }

        private void excelServiceClient_ExportWarehousePartsStockCompleted(object sender, ExportWarehousePartsStockCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            //compositeFilterItem.Filters.Add(new FilterItem {
            //    MemberName = "StorageCompanyId",
            //    MemberType = typeof(int),
            //    Value = BaseApp.Current.CurrentUserData.EnterpriseId,
            //    Operator = FilterOperator.IsEqualTo
            //});
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataEditViewBase pcPrintDataEditView;
        private DataEditViewBase PcPrintDataEditView {
            get {
                if(this.pcPrintDataEditView == null) {
                    this.pcPrintDataEditView = DI.GetDataEditView("PartsStockQueryForPcPrint");
                    this.pcPrintDataEditView.EditCancelled += PcPrintDataEditView_EditCancelled;
                    this.pcPrintDataEditView.EditSubmitted += PcPrintDataEditView_EditSubmitted;
                }
                return this.pcPrintDataEditView;
            }
        }
        private DataEditViewBase chPrintDataEditView;
        private DataEditViewBase ChPrintDataEditView {
            get {
                if(this.chPrintDataEditView == null) {
                    this.chPrintDataEditView = DI.GetDataEditView("PartsStockQueryForChPrint");
                    this.chPrintDataEditView.EditCancelled += PcPrintDataEditView_EditCancelled;
                    this.chPrintDataEditView.EditSubmitted += PcPrintDataEditView_EditSubmitted;
                }
                return this.chPrintDataEditView;
            }
        }
        private void ResetEditView() {
            this.pcPrintDataEditView = null;
            this.pcPrintRadWindow = null;
        }
        private void PcPrintDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.PcPrintRadWindow.Close();
            //if(this.DataGridView.FilterItem != null)
            //    this.DataGridView.ExecuteQueryDelayed();
            this.ResetEditView();
        }

        private void PcPrintDataEditView_EditCancelled(object sender, EventArgs e) {
            this.PcPrintRadWindow.Close();
            this.ResetEditView();
        }
        //打印弹出
        private RadWindow pcPrintRadWindow;

        private RadWindow PcPrintRadWindow {
            get {
                if(this.pcPrintRadWindow == null) {
                    this.pcPrintRadWindow = new RadWindow();
                    this.pcPrintRadWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                    this.pcPrintRadWindow.Content = this.PcPrintDataEditView;
                    this.pcPrintRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.pcPrintRadWindow.Height = 200;
                    this.pcPrintRadWindow.Width = 400;
                    this.pcPrintRadWindow.Header = "";
                }
                return this.pcPrintRadWindow;
            }
        }

        //打印弹出
        private RadWindow chPrintRadWindow;

        private RadWindow ChPrintRadWindow {
            get {
                if(this.chPrintRadWindow == null) {
                    this.chPrintRadWindow = new RadWindow();
                    this.chPrintRadWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen;
                    this.chPrintRadWindow.Content = this.ChPrintDataEditView;
                    this.chPrintRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.chPrintRadWindow.Height = 200;
                    this.chPrintRadWindow.Width = 400;
                    this.chPrintRadWindow.Header = "";
                }
                return this.chPrintRadWindow;
            }
        }

    }
}
