﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsPurchasing", "Accessories", "PartsTransferOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_AUDIT_EXPORT_PRINT_MERGEEXPORT, "PartsTransferOrder"
    })]
    public class PartsTransferOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataKanBanView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataApproveView;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataInitialApproveView;
        private const string DATA_APPROVE_VIEW = "_DataApproveView_";
        private const string DATA_INITIALAPPROVE_VIEW = "_DataInitialApproveView_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_KANBAN_VIEW = "_DataKanbanView_";
        private const string DATA_DETAIL_VIEW = "_DataDetailView_";

        public PartsTransferOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PartsTransferOrder;
        }

        private DataGridViewBase DataGridView {
              get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("PartsTransferOrderWithDetails");
                    this.dataGridView.RowDoubleClick += DataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }
        private void DataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                this.SwitchViewTo(DATA_DETAIL_VIEW);
            }
        }
        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsTransferOrderWithDetails");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }

        private DataEditViewBase DataKanBanView {
            get {
                if(this.dataKanBanView == null) {
                    this.dataKanBanView = DI.GetDataEditView("PartsTransferOrderForKanBan");
                    this.dataKanBanView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataKanBanView;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsTransferOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataApproveView {
            get {
                if(this.dataApproveView == null) {
                    this.dataApproveView = DI.GetDataEditView("PartsTransferOrderForApprove");
                    ((PartsTransferOrderForApproveDataEditView)this.dataApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataApproveView;
            }
        }

        private DataEditViewBase DataInitialApproveView {
            get {
                if(this.dataInitialApproveView == null) {
                    this.dataInitialApproveView = DI.GetDataEditView("PartsTransferOrderForInitialApprove");
                    ((PartsTransferOrderForInitialApproveDataEditView)this.dataInitialApproveView).EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataInitialApproveView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataInitialApproveView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataKanBanView = null;
            this.dataApproveView = null;
            this.dataInitialApproveView = null;
            this.dataDetailView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_APPROVE_VIEW, () => this.DataApproveView);
            this.RegisterView(DATA_KANBAN_VIEW, () => this.DataKanBanView);
            this.RegisterView(DATA_INITIALAPPROVE_VIEW, () => this.DataInitialApproveView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataEditViewDetail);
        }

        private bool CheckRecordState(string uniqueId) {
            if(this.DataGridView.SelectedEntities == null)
                return false;
            var entities = this.DataGridView.SelectedEntities.Cast<PartsTransferOrder>().ToArray();
            if(entities.Length != 1)
                return false;
            return entities[0].Status == (int)DcsPartsTransferOrderStatus.新建;
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsTransferOrder"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsTransferOrder = this.DataEditView.CreateObjectToEdit<PartsTransferOrder>();
                    partsTransferOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsTransferOrder.Status = (int)DcsPartsTransferOrderStatus.新建;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    if(!CheckRecordState(uniqueId)) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotEdit);
                        return;
                    }
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<PartsTransferOrder>().First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    return;
                case CommonActionKeys.ABANDON:
                    if(!CheckRecordState(uniqueId)) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotAbandon);
                        return;
                    }
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        this.AbandonDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<PartsTransferOrder>().First().Id);
                        this.AbandonRadWindow.ShowDialog();
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    //if(!CheckRecordState(uniqueId)) {
                    //    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotApprove);
                    //    return;
                    //}
                    this.DataApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<PartsTransferOrder>().First().GetIdentity());
                    this.SwitchViewTo(DATA_APPROVE_VIEW);
                    return;
                case CommonActionKeys.AUDIT:
                    this.DataInitialApproveView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<PartsTransferOrder>().First().GetIdentity());
                    this.SwitchViewTo(DATA_INITIALAPPROVE_VIEW);
                    return;
                case "TransferKanBan":
                    this.DataKanBanView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<PartsTransferOrder>().First().GetIdentity());
                    this.SwitchViewTo(DATA_KANBAN_VIEW);
                    return;
                case CommonActionKeys.MERGEEXPORT: {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        //var originalBillCode = filterItem.Filters.Single(r => r.MemberName == "OriginalBillCode").Value as string;
                        var originalWarehouseId = filterItem.Filters.Single(e => e.MemberName == "OriginalWarehouseId").Value as int?;
                        var destWarehouseId = filterItem.Filters.Single(e => e.MemberName == "DestWarehouseId").Value as int?;
                        var type = filterItem.Filters.Single(e => e.MemberName == "Type").Value as int?;
                        //var sapPurchasePlanCode = filterItem.Filters.Single(r => r.MemberName == "SAPPurchasePlanCode").Value as string;
                        //var eRPSourceOrderCode = filterItem.Filters.Single(r => r.MemberName == "ERPSourceOrderCode").Value as string;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? approveTimeBegin = null;
                        DateTime? approveTimeEnd = null;
                        var dateTimeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem));
                        foreach(var dateTimeFilterItem in dateTimeFilterItems) {
                            var dateTime = dateTimeFilterItem as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.Any(r => r.MemberName == "ApproveTime")) {
                                    approveTimeBegin = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                    approveTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                }
                            }
                        }
                        var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                        var entities = this.DataGridView.SelectedEntities;
                        int? id = null;
                        if(entities != null && entities.Any())
                            id = entities.Cast<PartsTransferOrder>().First().Id;
                        this.ExportpartsTransferOrderWithDetail(id, BaseApp.Current.CurrentUserData.EnterpriseId, code, null, originalWarehouseId, destWarehouseId, type, status, createTimeBegin, createTimeEnd, approveTimeBegin, approveTimeEnd, null, null);
                        break;
                    }
            }
        }

        private void ExportpartsTransferOrderWithDetail(int? id, int storageCompanyId, string partsTransferOrderCode, string originalBillCode, int? originalWarehouseId, int? destWarehouseId, int? type, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd, string SAPPurchasePlanCode, string eRPSourceOrderCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportpartsTransferOrderWithDetailAsync(id, storageCompanyId, partsTransferOrderCode, originalBillCode, originalWarehouseId, destWarehouseId, type, status, createTimeBegin, createTimeEnd, approveTimeBegin, approveTimeEnd, SAPPurchasePlanCode, eRPSourceOrderCode);
            this.excelServiceClient.ExportpartsTransferOrderWithDetailCompleted -= excelServiceClient_ExportpartsTransferOrderWithDetailCompleted;
            this.excelServiceClient.ExportpartsTransferOrderWithDetailCompleted += excelServiceClient_ExportpartsTransferOrderWithDetailCompleted;
        }

        protected void excelServiceClient_ExportpartsTransferOrderWithDetailCompleted(object sender, ExportpartsTransferOrderWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "StorageCompanyId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsTransferOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsPartsTransferOrderStatus.初审通过;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.AUDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = this.DataGridView.SelectedEntities.Cast<PartsTransferOrder>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    return entities1[0].Status == (int)DcsPartsTransferOrderStatus.新建;
                case "TransferKanBan":
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Cast<PartsTransferOrder>().Count() == 1;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        //作废弹出
        private RadWindow abandonRadWindow;
        private RadWindow AbandonRadWindow {
            get {
                if(this.abandonRadWindow == null) {
                    this.abandonRadWindow = new RadWindow();
                    this.abandonRadWindow.CanClose = false;
                    this.abandonRadWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    this.abandonRadWindow.Content = this.AbandonDataEditView;
                    this.abandonRadWindow.ResizeMode = ResizeMode.NoResize;
                    this.abandonRadWindow.Height = 200;
                    this.abandonRadWindow.Width = 400;
                    this.abandonRadWindow.Header = "";
                }
                return this.abandonRadWindow;
            }
        }

        private DataEditViewBase abandonDataEditView;
        private DataEditViewBase AbandonDataEditView {
            get {
                if(this.abandonDataEditView == null) {
                    this.abandonDataEditView = DI.GetDataEditView("PartsTransferOrderForAbandon");
                    this.abandonDataEditView.EditCancelled += AbandonDataEditView_EditCancelled;
                    this.abandonDataEditView.EditSubmitted += AbandonDataEditView_EditSubmitted;
                }
                return this.abandonDataEditView;
            }
        }

        private void AbandonDataEditView_EditSubmitted(object sender, EventArgs e) {
            this.AbandonRadWindow.Close();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
        }

        private void AbandonDataEditView_EditCancelled(object sender, EventArgs e) {
            this.AbandonRadWindow.Close();
        }
    }
}
