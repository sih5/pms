﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Views
{
    [PageMeta("PartsStockingAndLogistics", "PartsOutStore", "PickingTask", ActionPanelKeys = new[] {
        "PickingTask", "AbandonMergeExport"})]
    public class PickingTaskManagement : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        public PickingTaskManagement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.QueryPanel_Title_PickingTask;
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
              this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private DataEditViewBase DataEditView
        {
            get
            {
                if (this.dataEditView == null)
                {
                    this.dataEditView = DI.GetDataEditView("PickingTaskForPick");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e)
        {
            this.ResetEditView();
            if (this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e)
        {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PickingTask"));
            }
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PickingTask"
                };
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case "Picking":                  
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () =>
                    {
                        ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<PickingTaskQuery>().SingleOrDefault();
                        if (entity == null)
                            return;
                        ((IEditableObject)entity).EndEdit();
                        try
                        {
                            if (entity.CanabandonPickingTask)
                            {
                                entity.abandonPickingTask();
                            }
                            ShellViewModel.Current.IsBusy = false;

                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if (domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp =>
                            {
                                if (submitOp.HasError)
                                {
                                    if (!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                                this.DataGridView.ExecuteQuery();
                            }, null);
                        }
                        catch (Exception ex)
                        {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<PickingTaskQuery>().Select(r => r.Id).ToArray();
                        this.ExportPickingTask(ids, null, null, null, null, null, null, null, null, null, null, null, null,null,null,null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                     //   var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        //var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        var partsOutboundPlanCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsOutboundPlanCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsOutboundPlanCode").Value as string;
                       // var orderTypeName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OrderTypeName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OrderTypeName").Value as string;
                        var counterpartCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyName").Value as string;
                        var sparePartCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SparePartName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        var orderTypeId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OrderTypeId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OrderTypeId").Value as int?;

                        DateTime? bCreateTime = null;
                        DateTime? eCreateTime = null;
                        DateTime? bPickingfinishtime = null;
                        DateTime? ePickingfinishtime = null;
                        string status = string.Empty;
                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                        {
                            var dateTime = filter as CompositeFilterItem;
                            if (dateTime != null)
                            {

                                 if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "Pickingfinishtime"))
                                {
                                    bPickingfinishtime = dateTime.Filters.First(r => r.MemberName == "Pickingfinishtime").Value as DateTime?;
                                    ePickingfinishtime = dateTime.Filters.Last(r => r.MemberName == "Pickingfinishtime").Value as DateTime?;
                                }
                                if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "CreateTime")) {
                                    bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(((CompositeFilterItem)dateTime).Filters.Any(r => r.MemberName == "Status"))
                                    status = string.Join(",", dateTime.Filters.Where(r => r.MemberName == "Status").Select(r => r.Value));
                            }
                        }

                        this.ExportPickingTask(null, code, warehouseId, null, status, sourceCode, partsOutboundPlanCode, null, counterpartCompanyName, bCreateTime, eCreateTime, bPickingfinishtime, ePickingfinishtime, sparePartCode, sparePartName, orderTypeId);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<PickingTaskQuery>().FirstOrDefault();
                    if (selectedItem == null)
                        return;
                    ////BasePrintWindow printWindow = new PickingTaskPrintWindow
                    ////{
                    ////    Header = "拣货任务单打印",
                    ////    PickingTaskQuery = selectedItem
                    ////};
                    ////printWindow.ShowDialog();
                    //SunlightPrinter.ShowPrinter("拣货任务单打印", "ReportPickingTask", null, true, new Tuple<string, string>("pickingTaskId", selectedItem.Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

                    this.WindowDataBase.ShowDialog();
                    break;

            }
        }
        private RadWindow windowDataBase;

        private RadWindow WindowDataBase {
            get {
                return this.windowDataBase ?? (this.windowDataBase = new RadWindow {
                    Content = this.PartsPurchaseRtnSettleBillCostSearchQueryWindow,
                    Header = "拣货单打印",
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.Manual
                });
            }
        }

        private DcsMultiPopForPrintQueryWindowBase partsPurchaseRtnSettleBillCostSearchQueryWindow;

        private DcsMultiPopForPrintQueryWindowBase PartsPurchaseRtnSettleBillCostSearchQueryWindow {
            get {
                if(this.partsPurchaseRtnSettleBillCostSearchQueryWindow == null) {
                    this.partsPurchaseRtnSettleBillCostSearchQueryWindow = DI.GetQueryWindow("PickingTaskForPrint") as DcsMultiPopForPrintQueryWindowBase;
                    this.partsPurchaseRtnSettleBillCostSearchQueryWindow.SelectionDecided += this.SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided;
                    this.partsPurchaseRtnSettleBillCostSearchQueryWindow.Loaded += settlementCostSearchQueryWindow_Loaded;
                }
                return this.partsPurchaseRtnSettleBillCostSearchQueryWindow;
            }
        }
        private void SparePartForPartsPurchaseOrderDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopForPrintQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var spareParts = queryWindow.SelectedEntities.Cast<PickingTaskForPrint>().ToArray();
            if(spareParts.Count() == 0)
                return;
            var warehouseAreaIds = spareParts.Select(r => r.WarehouseArea).ToArray();
            var billIds = "";
            for(var i = 0; i < warehouseAreaIds.Count(); i++) {
                billIds += warehouseAreaIds[i] + ",";
            }

            SunlightPrinter.ShowPrinter("拣货任务单打印", "ReportPickingTaskNew", null, true, new Tuple<string, string>("pickingTaskId", spareParts.FirstOrDefault().Id.ToString()), new Tuple<string, string>("warehouseAreaIds", billIds.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));
            //SunlightPrinter.ShowPrinter("拣货任务单打印", "ReportPickingTask", null, true, new Tuple<string, string>("pickingTaskId", spareParts.FirstOrDefault().Id.ToString()), new Tuple<string, string>("UserName", BaseApp.Current.CurrentUserData.UserName));

        }
        private void settlementCostSearchQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var partsPurchaseRtnSettleBill = this.DataGridView.SelectedEntities.First() as PickingTaskQuery;
            if(partsPurchaseRtnSettleBill == null)
                return;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "Code", partsPurchaseRtnSettleBill.Code
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "Code", false
            });         
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("PickingTaskId", typeof(int), FilterOperator.IsEqualTo, partsPurchaseRtnSettleBill.Id));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
            queryWindow.ExchangeData(null, "ExecuteQuery", null);
        }
       
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public void ExportPickingTask(int[] ids, string code, int? warehouseId, int? partsSalesCategoryId, string status, string sourceCode, string partsOutboundPlanCode,
            string orderTypeName, string counterpartCompanyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPickingfinishtime, DateTime? ePickingfinishtime, string sparePartCode, string sparePartName, int? orderTypeId)
        {

            this.excelServiceClient.ExportPickingTaskAsync(ids, code, warehouseId, partsSalesCategoryId, status, sourceCode, partsOutboundPlanCode, orderTypeName, counterpartCompanyName, bCreateTime, eCreateTime, bPickingfinishtime, ePickingfinishtime, sparePartCode, sparePartName, orderTypeId);
            this.excelServiceClient.ExportPickingTaskCompleted -= ExcelServiceClient_ExportPickingTaskCompleted;
            this.excelServiceClient.ExportPickingTaskCompleted += ExcelServiceClient_ExportPickingTaskCompleted;
        }
        private void ExcelServiceClient_ExportPickingTaskCompleted(object sender, ExportPickingTaskCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
               
                case "Picking":
                    if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PickingTaskQuery>().ToArray();
                    if (entities.Length != 1)
                        return false;
                    if (entities[0].Status != (int)DcsPickingTaskStatus.新建 && entities[0].Status != (int)DcsPickingTaskStatus.部分拣货)
                    {
                        return false;
                    }
                    return true;
                case CommonActionKeys.ABANDON:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1)
                    {
                        var boxupTask = DataGridView.SelectedEntities.Cast<PickingTaskQuery>().FirstOrDefault();
                        if (boxupTask.Status == (int)DcsPickingTaskStatus.新建)
                        {
                            return true;
                        }
                    }
                    return false;
                case CommonActionKeys.MERGEEXPORT:
                    return true;
                case CommonActionKeys.PRINT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                default:
                    return false;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
