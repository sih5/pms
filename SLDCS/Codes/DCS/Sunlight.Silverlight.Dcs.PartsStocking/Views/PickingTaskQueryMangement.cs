﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using System.Windows;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Views
{
    [PageMeta("PartsStockingAndLogistics", "PartsOutStore", "PickingTaskQuery", ActionPanelKeys = new[] {
        "PickingTaskQuery", CommonActionKeys.MERGEEXPORT})]
    public class PickingTaskQueryMangement : DcsDataManagementViewBase
    {
        private DataGridViewBase dataGridView;
        public PickingTaskQueryMangement()
        {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PickingTaskQuery;
        }
        private void Initialize()
        {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }
        private DataGridViewBase DataGridView
        {
            get
            {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PickingTaskQuery"));
            }
        }
        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] {
                    "PickingTask"
                };
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId)
        {
            switch (uniqueId)
            {
                case "Allot":
                    this.WindowDataBase.ShowDialog();
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any())
                    {
                        var ids = this.DataGridView.SelectedEntities.Cast<PickingTaskQuery>().Select(r => r.Id).ToArray();
                        this.ExportPickingTask(ids, null, null, null, null, null, null, null, null, null, null, null, null);
                    }
                    else
                    {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if (filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var warehouseId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var partsSalesCategoryId = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsSalesCategoryId") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var status = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Status") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        var partsOutboundPlanCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "PartsOutboundPlanCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "PartsOutboundPlanCode").Value as string;
                        var orderTypeName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OrderTypeName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OrderTypeName").Value as string;
                        var counterpartCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "CounterpartCompanyName").Value as string;

                        DateTime? bCreateTime = null;
                        DateTime? eCreateTime = null;
                        DateTime? bPickingfinishtime = null;
                        DateTime? ePickingfinishtime = null;

                        foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                        {
                            var dateTime = filter as CompositeFilterItem;
                            if (dateTime != null)
                            {
                                if (dateTime.Filters.First().MemberName == "CreateTime")
                                {
                                    bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if (dateTime.Filters.First().MemberName == "Pickingfinishtime")
                                {
                                    bPickingfinishtime = dateTime.Filters.First(r => r.MemberName == "Pickingfinishtime").Value as DateTime?;
                                    ePickingfinishtime = dateTime.Filters.Last(r => r.MemberName == "Pickingfinishtime").Value as DateTime?;
                                }
                            }
                        }

                        this.ExportPickingTask(null, code, warehouseId, partsSalesCategoryId, status, sourceCode, partsOutboundPlanCode, orderTypeName, counterpartCompanyName, bCreateTime, eCreateTime, bPickingfinishtime, ePickingfinishtime);
                    }
                    break;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public void ExportPickingTask(int[] ids, string code, int? warehouseId, int? partsSalesCategoryId, int? status, string sourceCode, string partsOutboundPlanCode,
            string orderTypeName, string counterpartCompanyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bPickingfinishtime, DateTime? ePickingfinishtime)
        {

            this.excelServiceClient.ExportPickingTaskAllAsync(ids, code, warehouseId, partsSalesCategoryId, status, sourceCode, partsOutboundPlanCode, orderTypeName, counterpartCompanyName, bCreateTime, eCreateTime, bPickingfinishtime, ePickingfinishtime);
            this.excelServiceClient.ExportPickingTaskAllCompleted -= ExcelServiceClient_ExportPickingTaskAllCompleted;
            this.excelServiceClient.ExportPickingTaskAllCompleted += ExcelServiceClient_ExportPickingTaskAllCompleted;
        }
        private void ExcelServiceClient_ExportPickingTaskAllCompleted(object sender, ExportPickingTaskAllCompletedEventArgs e)
        {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.filename));
        }
        private RadWindow windowDataBase;

        private RadWindow WindowDataBase
        {
             get {
                if(windowDataBase == null) {
                    windowDataBase = new RadWindow();
                    windowDataBase.Header = PartsStockingUIStrings.DataManagementView_Text_ChoosePersonel;
                    windowDataBase.Content = this.PersonnelDropDownQueryWindow;
                    windowDataBase.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.Manual;
                }
                return this.windowDataBase;
            }
        }
        private QueryWindowBase personnelDropDownQueryWindow;

        private QueryWindowBase PersonnelDropDownQueryWindow
        {
            get
            {
                if (this.personnelDropDownQueryWindow == null)
                {
                    this.personnelDropDownQueryWindow = DI.GetQueryWindow("PersonnelForCompany") as QueryWindowBase;
                    this.personnelDropDownQueryWindow.SelectionDecided += this.PersonnelDropDownQueryWindow_SelectionDecided;
                }
                return this.personnelDropDownQueryWindow;
            }
        }
        private void PersonnelDropDownQueryWindow_SelectionDecided(object sender, EventArgs e)
        {
            var queryWindow = sender as DcsQueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var personnel = queryWindow.SelectedEntities.Cast<Personnel>().SingleOrDefault();
            if (personnel == null)
                return;
            var pickList = this.DataGridView.SelectedEntities.Cast<PickingTaskQuery>().ToArray();
            var pickIds = pickList.Select(r => r.Id).ToArray();
            var dcsDomainContext = new DcsDomainContext();
           
            DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_SetManager, () =>
            {
                dcsDomainContext.设置责任人(pickIds, personnel.Id, loadOpv =>
                {
                    if (loadOpv.HasError)
                    {
                        if (!loadOpv.IsErrorHandled)
                            loadOpv.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOpv);
                        return;
                    }
                    var parent = queryWindow.ParentOfType<RadWindow>();
                    if (parent != null)
                        parent.Close();
                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Confirm_SetSuccess);
                    this.DataGridView.ExecuteQuery();
                }, null);   
            }
                );
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId)
        {
            if (this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch (uniqueId)
            {
                case "Allot":
                   if (this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PickingTaskQuery>().ToArray();
                    foreach(var item in entities){
                        if (item.Status != (int)DcsPickingTaskStatus.新建)
                        {
                            return false;
                        }
                    }
                    return true;
                case CommonActionKeys.MERGEEXPORT:
                    return true;
                default:
                    return false;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem)
        {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if (compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
