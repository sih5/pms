﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsSales", "PartsOuterPurchase", "PostSaleClaimsTmp", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class PostSaleClaimsTmpManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public PostSaleClaimsTmpManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_PostSaleClaimsTmp;
        }


        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        public DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null)
                    this.dataGridView = DI.GetDataGridView("PostSaleClaimsTmp");
                return this.dataGridView;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] { 
                    "PostSaleClaimsTmp"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            switch(uniqueId) {
                case "Export":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if (filterItem == null)
                        return;
                    var claimBillCode = filterItem.Filters.Single(e => e.MemberName == "ClaimBillCode").Value as string;
                    var sparePartCode = filterItem.Filters.Single(e => e.MemberName == "SparePartCode").Value as string;
                    var sparePartName = filterItem.Filters.Single(e => e.MemberName == "SparePartName").Value as string;
                    var dealerCode = filterItem.Filters.Single(e => e.MemberName == "DealerCode").Value as string;
                    var dealerName = filterItem.Filters.Single(r => r.MemberName == "DealerName").Value as string;
                    var type = filterItem.Filters.Single(r => r.MemberName == "Type").Value as int?;                    
                    DateTime? bCreateTime = null;
                    DateTime? eCreateTime = null;
                    foreach (var filter in filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)))
                    {
                        var dateTime = filter as CompositeFilterItem;
                        if (dateTime != null)
                        {
                            if (dateTime.Filters.First().MemberName == "CreateTime")
                            {
                                bCreateTime = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                eCreateTime = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                        }
                    }
                    ShellViewModel.Current.IsBusy = true;
                    if (this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<VirtualPostSaleClaimsTmp>().Select(r => r.Id).ToArray();
                        dcsDomainContext.exportPostSaleClaimsTmps(ids, claimBillCode, sparePartCode, sparePartName, dealerCode, dealerName,type,bCreateTime,eCreateTime, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    } else {
                        dcsDomainContext.exportPostSaleClaimsTmps(null, claimBillCode, sparePartCode, sparePartName, dealerCode, dealerName,type,bCreateTime,eCreateTime, loadOp => {
                            if (loadOp.HasError)
                                return;
                            if (loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if (loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                            ShellViewModel.Current.IsBusy = false;
                        }, null);
                    }
                    break;
                default:
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "Export":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilterItem = filterItem as CompositeFilterItem;
            if(newCompositeFilterItem == null)
                return;
            ClientVar.ConvertTime(newCompositeFilterItem);
            this.DataGridView.FilterItem = newCompositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
