﻿using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    public class AccurateTraceQueryWindow : DcsDropDownQueryWindowBase {
        public AccurateTraceQueryWindow() {          
        }

        public override string Title {
            get {
                return "可下架标签查询";
            }
        }

        public override string DataGridViewKey {
            get {
                return "AccurateTraceForDown";
            }
        }

        public override string QueryPanelKey {
            get {
                return "AccurateTraceForDown";
            }
        }
    }
}
