﻿

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    public class AgencyDifferenceBackBillQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "AgencyDifferenceBackBillForSparepart";
            }
        }

        public override string QueryPanelKey {
            get {
                return "AgencyDifferenceBackBillForSparepart";
            }
        }
    }
}