﻿using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 查询企业销售中心库存
    /// </summary>
    public class CompanyStockBySalesDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "CompanyStockBySales";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CompanyStockBySales";
            }
        }

        public override string Title {
            get {
                return PartsStockingUIStrings.QueryPanel_Title_CompanyStockBySales;
            }
        }
    }
}