﻿
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 查询企业销售中心库存
    /// </summary>
    public class CompanyStockBySalesQueryWindow : DcsMultiPopupsQueryWindowBase {
        public CompanyStockBySalesQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("CompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }

        public override string DataGridViewKey {
            get {
                return "CompanyStockBySales";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CompanyStockBySales";
            }
        }
    }
}
