﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 查询企业库存
    /// </summary>
    public class CompanyStockDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public CompanyStockDropDownQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("CompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }
        public override string DataGridViewKey {
            get {
                return "CompanyStockWithDetails";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CompanyStock";
            }
        }

        public override string Title {
            get {
                return PartsStockingUIStrings.QueryPanel_Title_CompanyPartsStock;
            }
        }
    }
}
