﻿
using Sunlight.Silverlight.Core.Model;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    public class CompanyStockForSelectQueryWindow : DcsMultiPopupsQueryWindowBase {
        public CompanyStockForSelectQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("CompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }

        public override string DataGridViewKey {
            get {
                return "CompanyStockWithDetails";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CompanyStock";

            }
        }
    }
}
