﻿using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 查询企业库存
    /// </summary>
    public class CompanyStockQueryWindow : DcsQueryWindowBase {
        public CompanyStockQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("CompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }

        public override string DataGridViewKey {
            get {
                return "CompanyStockWithDetails";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CompanyStock";
            }
        }
    }
}
