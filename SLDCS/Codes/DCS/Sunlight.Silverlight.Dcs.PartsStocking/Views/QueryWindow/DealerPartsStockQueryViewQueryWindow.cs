﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    public class DealerPartsStockQueryViewQueryWindow : DcsQueryWindowBase {

        /// <summary>
        /// 查询经销商保内外调拨配件库存
        /// </summary>
        public override string DataGridViewKey {
            get {
                return "DealerPartsStockQueryViewForWindow";
            }
        }

        public override string QueryPanelKey {
            get {
                return "DealerPartsStockQueryViewForWindow";
            }
        }
    }
}
