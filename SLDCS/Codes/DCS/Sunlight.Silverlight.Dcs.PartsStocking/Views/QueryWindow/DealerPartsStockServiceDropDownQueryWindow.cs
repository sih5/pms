﻿using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 调用窗体传入企业Id，品牌
    /// 服务站配件库存查询
    /// </summary>
    public class DealerPartsStockServiceDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "DealerPartsStockService";
            }
        }

        public override string QueryPanelKey {
            get {
                return "DealerPartsStockService";
            }
        }

        public override string Title {
            get {
                return PartsStockingUIStrings.QueryPanel_Title_DealerPartsStock;
            }
        }
    }
}