﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 调用窗体传入参数企业ID,品牌Id
    /// 服务站配件库存查询
    /// </summary>
    public class DealerPartsStockServiceQueryWindow : DcsMultiPopupsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "DealerPartsStockService";
            }
        }

        public override string QueryPanelKey {
            get {
                return "DealerPartsStockService";
            }
        }
    }
}