﻿

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 查询锁定单位（视图）
    /// </summary>
    public class LocketUnitQueryQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "LocketUnitQuery";
            }
        }

        public override string QueryPanelKey {
            get {
                return "LocketUnitQuery";
            }
        }
    }
}
