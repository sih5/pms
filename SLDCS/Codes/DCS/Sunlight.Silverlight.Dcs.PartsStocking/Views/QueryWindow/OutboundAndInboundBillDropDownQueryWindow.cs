﻿
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 必须传入参数 客户企业Id,客户账户Id
    /// 传入参数名:  CompanyId，CustomerAccountId
    /// 选择待结算出入单
    /// </summary>
    public class OutboundAndInboundBillDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return PartsStockingUIStrings.QueryPanel_Title_OutboundAndInboundBill;
            }
        }

        public override string DataGridViewKey {
            get {
                return "OutboundAndInboundBill";
            }
        }

        public override string QueryPanelKey {
            get {
                return "OutboundAndInboundBill";
            }
        }
    }
}
