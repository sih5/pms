﻿using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 必须传入参数 企业ID
    /// 传入参数名:CompanyId
    /// 选择积压件信息
    /// </summary>
    public class OverstockPartsInformationDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return PartsStockingUIStrings.QueryPanel_Title_OverstockPartsStock;
            }
        }

        public override string DataGridViewKey {
            get {
                return "OverstockPartsInformationForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "OverstockPartsInformationForSelect";
            }
        }
    }
}
