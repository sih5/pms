﻿namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 必须传入参数 企业ID
    /// 传入参数名:CompanyId
    /// 选择积压件信息
    /// </summary>
    public class OverstockPartsInformationQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "OverstockPartsInformationForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "OverstockPartsInformationForSelect";
            }
        }
    }
}
