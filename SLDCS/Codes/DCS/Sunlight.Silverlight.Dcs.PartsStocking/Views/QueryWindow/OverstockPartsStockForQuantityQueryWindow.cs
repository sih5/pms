﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 必须传入参数 企业ID
    /// 传入参数名:  CompanyId
    /// 查询积压件库存
    /// </summary>
    public class OverstockPartsStockForQuantityQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "OverstockPartsStockForQuantity";
            }
        }

        public override string QueryPanelKey {
            get {
                return "OverstockPartsStockForQuantity";
            }
        }
    }
}
