﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    public class PartsDifferenceBackBillSourceCodeQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "PartsDifferenceBackBillSourceCode";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsDifferenceBackBillSourceCode";
            }
        }
    }
}
