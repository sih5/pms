﻿using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    public class PurchaseWarehousePartsStockDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return PartsStockingUIStrings.QueryPanel_Title_PurchaseWarehousePartsStock;
            }
        }

        public override string DataGridViewKey {
            get {
                return "PurchaseWarehousePartsStock";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PurchaseWarehousePartsStock";
            }
        }
    }
}
