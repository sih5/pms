﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 采购退货查询仓库库存
    /// 调用窗体传入仓库Id(WarehouseId) 供应商Id(SupplierId)配件销售类型Id(PartsSaleCategoryId)
    /// </summary>
    public class PurchaseWarehousePartsStockQueryWindow : DcsMultiPopupsQueryWindowBase {

        public PurchaseWarehousePartsStockQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
            this.SetDefaultQueryItemValue("Common", "StorageCompanyId", BaseApp.Current.CurrentUserData.EnterpriseId);
        }

        public override string QueryPanelKey {
            get {
                return "PurchaseWarehousePartsStock";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PurchaseWarehousePartsStock";
            }
        }
    }
}
