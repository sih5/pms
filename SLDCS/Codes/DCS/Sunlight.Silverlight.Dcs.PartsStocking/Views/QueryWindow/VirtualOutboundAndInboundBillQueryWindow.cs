﻿
using System;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.Custom;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    public class VirtualOutboundAndInboundBillQueryWindow : DcsCustomExportQueryWindowBase {
        private FilterItem additionalFilterItem;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public VirtualOutboundAndInboundBillQueryWindow() {
            this.Export -= VirtualOutboundAndInboundBillQueryWindow_Export;
            this.Export += VirtualOutboundAndInboundBillQueryWindow_Export;
        }

        private void VirtualOutboundAndInboundBillQueryWindow_Export(object sender, EventArgs e) {
            var multiSelectView = this.CustomerView as VirtualOutboundAndInboundBillCustomView;
            if(multiSelectView != null) {
                var filterItem = multiSelectView.VirtualOutboundAndInboundBillHistoricalDataBaseDataGridView.FilterItem as CompositeFilterItem;
                var fileitem = filterItem.Filters.LastOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                var fileitem2 = filterItem.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                var sparePartId = fileitem.Filters.SingleOrDefault(r => r.MemberName == "SparePartId").Value;
                var warehouseId = fileitem.Filters.SingleOrDefault(r => r.MemberName == "WarehouseId").Value;
                string warehouseAreaCode = fileitem2.Filters.SingleOrDefault(r => r.MemberName == "WarehouseAreaCode").Value == null ? null : fileitem2.Filters.SingleOrDefault(r => r.MemberName == "WarehouseAreaCode").Value.ToString();
                var createTime = filterItem.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                DateTime? createTimeBegin = null;
                DateTime? createTimeEnd = null;
                if(createTime != null) {
                    if(createTime.Filters.FirstOrDefault(r => r.MemberName == "CreateTime") != null) 
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                    if(createTime.Filters.LastOrDefault(r => r.MemberName == "CreateTime") != null)
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                }
                ShellViewModel.Current.IsBusy = true;
                this.ExportOutboundAndInboundBill((int)sparePartId, (int)warehouseId, createTimeBegin, createTimeEnd, warehouseAreaCode);
            }
        }


        private void ExportOutboundAndInboundBill(int sparePartId, int warehouseId, DateTime? createTimeBegin, DateTime? createTimeEnd, string warehouseAreaCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportOutboundAndInboundBillAsync(sparePartId, warehouseId, createTimeBegin, createTimeEnd, warehouseAreaCode);
            this.excelServiceClient.ExportOutboundAndInboundBillCompleted -= excelServiceClient_ExportOutboundAndInboundBillCompleted;
            this.excelServiceClient.ExportOutboundAndInboundBillCompleted += excelServiceClient_ExportOutboundAndInboundBillCompleted;

        }

        private void excelServiceClient_ExportOutboundAndInboundBillCompleted(object sender, ExportOutboundAndInboundBillCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


        public override object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            switch(subject) {
                case "SetQueryItemValue": {
                        if(contents.Length == 3) {
                            this.QueryPanel.SetValue(contents[0] as string, contents[1] as string, contents[2]);
                            this.RefreshQueryResult();
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetQueryItemValue", 3), "contents");
                    }
                case "SetQueryItemEnabled": {
                        if(contents.Length == 3) {
                            this.QueryPanel.SetEnabled(contents[0] as string, contents[1] as string, (bool)contents[2]);
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetQueryItemEnabled", 3), "contents");
                    }
                case "SetAdditionalFilterItem": {
                        if(contents.Length == 1) {
                            if(this.additionalFilterItem == null && contents[0] == null)
                                return false;
                            if(this.additionalFilterItem != null && this.additionalFilterItem.Equals(contents[0] as FilterItem))
                                return false;
                            this.additionalFilterItem = contents[0] as FilterItem;
                            this.RefreshQueryResult();
                            return true;
                        }
                        throw new ArgumentException(String.Format(DcsUIStrings.QueryWindow_ExchangeData_ExceptionMessage_Contents, "SetAdditionalFilterItem", 1), "contents");
                    }
                case "ExecuteQuery": {
                        this.QueryPanel.ExecuteQuery();
                        return true;
                    }
                case "RefreshQueryResult": {
                        this.RefreshQueryResult();
                        return true;
                    }
                default:
                    throw new ArgumentException(DcsUIStrings.QueryWindow_ExchangeData_Exception_Subject, "subject");
            }
        }

        public override string[] DataGridViewKeies {
            get {
                return new[] {
                    ""
                };
            }
        }

        protected override UserControlBase CustomerView {
            get {
                return this.customerView ?? (this.customerView = new VirtualOutboundAndInboundBillCustomView());
            }
        }

        public override string[] GridViewTitles {
            get {
                return new[] {
                    ""
                };
            }
        }

        protected override void QueryPanel_ExecutingQuery(object sender, FilterPanelRibbonGroup.ExecutingQueryEventArgs e) {
            if(CustomerView != null) {
                var multiSelectView = this.CustomerView as VirtualOutboundAndInboundBillCustomView;
                if(multiSelectView != null) {
                    var compositeFilterItem = new CompositeFilterItem();
                    var eCompositeFilterItem = e.Filter as CompositeFilterItem;
                    var fileitem = eCompositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    var item = new CompositeFilterItem();
                    if(fileitem != null) {
                        item.Filters.Add(new FilterItem {
                            MemberName = "CreateTime",
                            MemberType = typeof(DateTime),
                            Operator = FilterOperator.IsGreaterThanOrEqualTo,
                            Value = fileitem.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?
                        });
                        item.Filters.Add(new FilterItem {
                            MemberName = "CreateTime",
                            MemberType = typeof(DateTime),
                            Operator = FilterOperator.IsLessThanOrEqualTo,
                            Value = fileitem.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?
                        });
                    }
                    item.Filters.Add(new FilterItem {
                        MemberName = "SalesCategoryId",
                        MemberType = typeof(int),
                        Operator = FilterOperator.IsEqualTo,
                        Value = eCompositeFilterItem.Filters.Single(r => r.MemberName == "SalesCategoryId").Value as int?
                    });
                    item.Filters.Add(new FilterItem {
                        MemberName = "PartsSalesOrderTypeId",
                        MemberType = typeof(int),
                        Operator = FilterOperator.IsEqualTo,
                        Value = eCompositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderTypeId").Value as int?
                    });
                    item.Filters.Add(new FilterItem {
                        MemberName = "WarehouseAreaCode",
                        MemberType = typeof(string),
                        Operator = FilterOperator.IsEqualTo,
                        Value = eCompositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseAreaCode").Value as string
                    });
                    compositeFilterItem.Filters.Add(item);
                    if(fileitem != null)
                        ClientVar.ConvertTime(compositeFilterItem);
                    compositeFilterItem.Filters.Add(additionalFilterItem);
                    multiSelectView.ExecuteQuery(compositeFilterItem);
                }
            }
        }

        private void RefreshQueryResult() {
            if(customerView != null) {
                var multiSelectView = this.CustomerView as VirtualOutboundAndInboundBillCustomView;
                if(multiSelectView != null) {
                    if(multiSelectView.VirtualOutboundAndInboundBillHistoricalDataBaseDataGridView.Entities != null && multiSelectView.VirtualPartsStockHistoricalStorageDataGridView.Entities != null)
                        this.QueryPanel.ExecuteQuery();
                }
            }
        }

        public override string QueryPanelKey {
            get {
                return "VirtualOutboundAndInboundBillHistoricalDataBase";
            }
        }

        public override System.Collections.Generic.IEnumerable<System.ServiceModel.DomainServices.Client.Entity> SelectedEntities {
            get {
                return null;
            }
        }
    }
}
