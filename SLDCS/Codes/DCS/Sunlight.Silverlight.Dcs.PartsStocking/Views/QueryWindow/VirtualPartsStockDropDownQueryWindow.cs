﻿
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 查询库位库存
    /// </summary>
    public class VirtualPartsStockDropDownQueryWindow : DcsMultiPopupsQueryWindowBase {

        public override string QueryPanelKey {
            get {
                return "VirtualPartsStockForSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "VirtualPartsStock";
            }
        }
    }
}