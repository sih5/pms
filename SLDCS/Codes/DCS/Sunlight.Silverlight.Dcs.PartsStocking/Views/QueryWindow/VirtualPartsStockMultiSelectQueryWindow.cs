﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    public class VirtualPartsStockMultiSelectQueryWindow : DcsMultiPopupsQueryWindowBase {
        /// <summary>
        /// 库位库区查询(多选)
        /// </summary>

        public override string QueryPanelKey {
            get {
                return "VirtualPartsStockForSelect";
            }
        }
 
        public override string DataGridViewKey {
            get {
                return "VirtualPartsStock";
            }
        }
    }
}
