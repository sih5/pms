﻿namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 查询库位库存
    /// </summary>
    public class VirtualPartsStockQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "VirtualPartsStock";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VirtualPartsStockForSelect";
            }
        }
    }
}
