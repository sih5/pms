﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 调拨查询仓库库存
    /// </summary>
    public class VirtualWarehousePartsStockQueryWindow : DcsMultiPopupsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "VirtualWarehousePartsStock";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VirtualWarehousePartsStock";
            }
        }
    }
}
