﻿using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 库区库位选择
    /// </summary>
    public class WarehouseAreaDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "WarehouseArea";
            }
        }

        public override string QueryPanelKey {
            get {
                return "WarehouseArea";
            }
        }

        public override string Title {
            get {
                return PartsStockingUIStrings.QueryPanel_Title_WarehouseArea;
            }
        }
    }
}
