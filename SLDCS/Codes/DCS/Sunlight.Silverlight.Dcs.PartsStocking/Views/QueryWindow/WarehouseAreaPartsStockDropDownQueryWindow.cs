﻿using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 查询仓库库位库存
    /// </summary>
    public class WarehouseAreaPartsStockDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "PartsStock";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsStock";
            }
        }

        public override string Title {
            get {
                return PartsStockingUIStrings.QueryPanel_Title_PartsStock;
            }
        }
    }
}
