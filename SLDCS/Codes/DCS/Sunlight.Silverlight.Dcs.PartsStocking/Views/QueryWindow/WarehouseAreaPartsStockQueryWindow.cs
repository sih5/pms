﻿namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 查询仓库库位库存
    /// </summary>
    public class WarehouseAreaPartsStockQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "PartsStock";
            }
        }

        public override string QueryPanelKey {
            get {
                return "PartsStock";
            }
        }
    }
}
