﻿namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {

    /// <summary>
    /// 库区库位选择
    /// </summary>
    public class WarehouseAreaQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "WarehouseArea";
            }
        }

        public override string QueryPanelKey {
            get {
                return "WarehouseArea";
            }
        }
    }
}
