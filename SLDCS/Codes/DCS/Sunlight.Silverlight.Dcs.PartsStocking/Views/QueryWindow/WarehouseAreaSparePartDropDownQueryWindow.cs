﻿using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 库区库位选择
    /// </summary>
    public class WarehouseAreaSparePartDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "WarehouseAreaSparePart";
            }
        }

        public override string QueryPanelKey {
            get {
                return "WarehouseArea";
            }
        }

        public override string Title {
            get {
                return PartsStockingUIStrings.QueryPanel_Title_WarehouseArea;
            }
        }
    }
}

