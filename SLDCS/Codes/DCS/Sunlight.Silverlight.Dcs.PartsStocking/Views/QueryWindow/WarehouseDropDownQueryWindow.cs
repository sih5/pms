﻿using Sunlight.Silverlight.Dcs.PartsStocking.Resources;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 仓库选择
    /// </summary>
    public class WarehouseDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "WarehouseForDropDown";
            }
        }

        public override string QueryPanelKey {
            get {
                return "Warehouse";
            }
        }

        public override string Title {
            get {
                return PartsStockingUIStrings.QueryPanel_Title_Warehouse;
            }
        }
    }
}
