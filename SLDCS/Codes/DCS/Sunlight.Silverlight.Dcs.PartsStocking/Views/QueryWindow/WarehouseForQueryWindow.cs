﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {

    /// <summary>
    /// 仓库选择(DropDownQueryWindow更改)
    /// </summary>
    public class WarehouseForQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "WarehouseForDropDown";
            }
        }

        public override string QueryPanelKey {
            get {
                return "Warehouse";
            }
        }
    }
}
