﻿
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 调用窗体传入参数企业ID(StorageCompanyId)
    /// 查询代理库仓库库存
    /// </summary>
    public class WarehousePartsStockLibraryDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "WarehousePartsStockLibrary";
            }
        }

        public override string QueryPanelKey {
            get {
                return "WarehousePartsStockLibrary";
            }
        }

        public override string Title {
            get {
                return PartsStockingUIStrings.QueryPanel_Title_WarehousePartsStock;
            }
        }
    }
}