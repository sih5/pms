﻿
namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 调用窗体传入参数企业ID(StorageCompanyId)
    /// 查询代理库仓库库存(多选弹框)
    /// </summary>
    public class WarehousePartsStockLibraryMulitQueryWindow : DcsMultiPopupsQueryWindowBase {

        public override string QueryPanelKey {
            get {
                return "WarehousePartsStockLibrary";
            }
        }

        public override string DataGridViewKey {
            get {
                return "WarehousePartsStockLibrary";
            }
        }
    }
}
