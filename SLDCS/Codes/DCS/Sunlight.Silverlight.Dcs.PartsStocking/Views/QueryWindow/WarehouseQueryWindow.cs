﻿namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    /// <summary>
    /// 仓库选择
    /// </summary>
    public class WarehouseQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "Warehouse";
            }
        }

        public override string QueryPanelKey {
            get {
                return "Warehouse";
            }
        }
    }
}
