﻿

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views.QueryWindow {
    public class PickingTaskForPickQueryWindow : DcsMultiPopupsQueryWindowBase {
        public PickingTaskForPickQueryWindow() {
            //this.SetDefaultQueryItemEnable("Common", "Status", false);
        }
        public override string DataGridViewKey {
            get {
                return "PickingTaskforPickWarehouseArea";
            }
        }
        public override string QueryPanelKey {
            get {
                return "PickingTaskForPrint";

            }
        }
    }
}
