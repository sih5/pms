﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Web;
using System.ServiceModel.DomainServices.Client;


namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsInStore", "ReceiptManagement", ActionPanelKeys = new[] {
        "ReceiptManagement", CommonActionKeys.MERGEEXPORT })]
    public class ReceiptManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase receiptInboundDataEdtiView;
        private const string DATA_DETAIL_VIEW = "_dataDetailView_";
        private DataEditViewBase dataDetailView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        public ReceiptManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_ReceiptManagement;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.ReceiptInboundDataEdtiView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailView);
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsInboundCheckBillReceipt"
                };
            }
        }
        private DataEditViewBase ReceiptInboundDataEdtiView {
            get {
                if(this.receiptInboundDataEdtiView == null) {
                    this.receiptInboundDataEdtiView = DI.GetDataEditView("PartsInboundReceipt");
                    this.receiptInboundDataEdtiView.EditSubmitted += this.ReceiptInboundDataEdtiView_EditSubmitted;
                    this.receiptInboundDataEdtiView.EditCancelled += this.DataEdtiView_EditCancelled;
                }
                return this.receiptInboundDataEdtiView;
            }
        }
        private void ReceiptInboundDataEdtiView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsInboundPlanForReceipt"));
            }
        }
        private DataEditViewBase DataDetailView {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsInboundPlanDetail");
                    this.dataDetailView.EditCancelled += this.DataEdtiView_EditCancelled;
                }
                return dataDetailView;
            }
        }
        private void ResetEditView() {
            this.receiptInboundDataEdtiView = null;
            this.dataDetailView = null;
        }        private void DataEdtiView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "Receipt":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var receipt = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundPlan>().ToArray();
                    return receipt.Length == 1;
                case "BatchReceipt":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var batchReceipt = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundPlan>().ToArray();
                    return batchReceipt.Where(o => o.InboundType != (int)DcsPartsInboundType.销售退货).ToArray().Length == 0 && batchReceipt.Length > 0;
                case "Mandatory":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var mandatory = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundPlan>().ToArray();
                    return mandatory.Length == 1;
                case CommonActionKeys.MERGEEXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "Print":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var print = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundPlan>().ToArray();
                    return print.Length == 1;
                default:
                    return true;

            }
        }
        protected override void OnExecutingAction(Silverlight.View.ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Receipt":
                    this.ReceiptInboundDataEdtiView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "BatchReceipt":
                    var partsInboundPlanIds = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundPlan>().Select(r => r.Id).ToArray();
                    var domainContextReceipt = this.DataGridView.DomainContext as DcsDomainContext;
                    if(domainContextReceipt != null) {
                        ShellViewModel.Current.IsBusy = true;
                        domainContextReceipt.批量收货(partsInboundPlanIds, invokeOp => {
                            if(invokeOp.HasError) {
                                if(!invokeOp.IsErrorHandled)
                                    invokeOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                ShellViewModel.Current.IsBusy = false;
                                domainContextReceipt.RejectChanges();
                                return;
                            }
                            ShellViewModel.Current.IsBusy = false;
                            UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Text_BatchReceiptSuccess);
                            this.DataGridView.ExecuteQueryDelayed();
                        }, null);
                    }
                    break;
                case "Mandatory":
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Terminate, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundPlan>().SingleOrDefault();
                        if (entity == null)
                            return;

                        var context = this.DataGridView.DomainContext as DcsDomainContext;
                        context.Load(context.GetPartsInboundPlansQuery().Where(e => e.Id == entity.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            if (loadOp.HasError)
                                return;
                            var inboundplan = loadOp.Entities.FirstOrDefault();
                            try {
                                if (inboundplan.Can终止配件入库计划)
                                    inboundplan.终止配件入库计划();
                                var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                                if (domainContext == null)
                                    return;
                                domainContext.SubmitChanges(submitOp => {
                                    if (submitOp.HasError) {
                                        if (!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        domainContext.RejectChanges();
                                        return;
                                    }
                                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_TerminateSuccess);
                                    this.CheckActionsCanExecute();
                                }, null);
                            } catch (Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }

                        }, null);
                    });
                    break;
                case CommonActionKeys.MERGEEXPORT:
                    //如果选中一条数据 合并导出参数为 出库计划ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsInboundPlan>().Select(r => r.Id).ToArray();
                        this.ExecuteMergeExport(ids, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    }
                        //否则 合并导出参数为 出库计划编号 源单据编号 仓库名称	出库类型	创建时间	对方单位编号	对方单位名称
                    else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "Code").Value as string;
                        var sourceCode = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        var warehouseId = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "WarehouseId") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                        var inboundType = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "InboundType") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "InboundType").Value as int?;
                        var counterpartCompanyCode = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyCode") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "CounterpartCompanyCode").Value as string;
                        var counterpartCompanyName = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "CounterpartCompanyName") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "CounterpartCompanyName").Value as string;
                        var sparePartCode = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "SparePartCode") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                        var sparePartName = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "SparePartName") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                        var originalRequirementBillCode = (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.SingleOrDefault(r => r.MemberName == "OriginalRequirementBillCode") == null ? null : (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Single(r => r.MemberName == "OriginalRequirementBillCode").Value as string;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? planDeliveryTimeBegin = null;
                        DateTime? planDeliveryTimeEnd = null;
                        foreach(var filter in (filterItem.Filters.FirstOrDefault() as CompositeFilterItem).Filters.Where(r => r.GetType() == typeof(CompositeFilterItem))) {
                            var dateTime = filter as CompositeFilterItem;
                            {
                                if(dateTime.Filters.First().MemberName == "CreateTime") {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.First().MemberName == "PlanDeliveryTime") {
                                    planDeliveryTimeBegin = dateTime.Filters.First(r => r.MemberName == "PlanDeliveryTime").Value as DateTime?;
                                    planDeliveryTimeEnd = dateTime.Filters.Last(r => r.MemberName == "PlanDeliveryTime").Value as DateTime?;
                                }
                            }
                        }
                        this.ExecuteMergeExport(null, code, sourceCode, warehouseId, inboundType, createTimeBegin, createTimeEnd, planDeliveryTimeBegin, planDeliveryTimeEnd, counterpartCompanyCode, counterpartCompanyName, sparePartCode, sparePartName, originalRequirementBillCode);
                    }
                    break;
                case "Print":
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<VirtualPartsInboundPlan>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    var dcsdomainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    dcsdomainContext.Load(dcsdomainContext.GetPartsInboundPlansQuery().Where(e => e.Id == selectedItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                        if (loadOp.HasError)
                            return;
                        var entity = loadOp.Entities.FirstOrDefault();
                        BasePrintWindow printWindow = new PartsInboundPlanPrintWindow {
                            Header = PartsStockingUIStrings.DataManagementView_PrintWindow_Title_PartsInboundPlan,
                            PartsInboundPlan = entity
                        };
                        printWindow.ShowDialog();
                    }, null);
                    break;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExecuteMergeExport(int[] ids, string code, string sourceCode, int? warehouseId, int? inboundType, DateTime? begainDate, DateTime? endDate, DateTime? planDeliveryTimeBegin, DateTime? planDeliveryTimeEnd, string counterpartCompanyCode, string counterpartCompanyName, string sparePartCode, string sparePartName, string originalRequirementBillCode) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportPartsInboundPlanWithDetailAsync(ids, null, BaseApp.Current.CurrentUserData.UserId, null, BaseApp.Current.CurrentUserData.EnterpriseId, code, sourceCode, warehouseId, inboundType, begainDate, endDate, planDeliveryTimeBegin, planDeliveryTimeEnd, counterpartCompanyCode, counterpartCompanyName, sparePartCode, sparePartName, originalRequirementBillCode);
            this.excelServiceClient.ExportPartsInboundPlanWithDetailCompleted -= this.ExcelServiceClient_ExportPartsInboundPlanWithDetailCompleted;
            this.excelServiceClient.ExportPartsInboundPlanWithDetailCompleted += this.ExcelServiceClient_ExportPartsInboundPlanWithDetailCompleted;
        }
        private void ExcelServiceClient_ExportPartsInboundPlanWithDetailCompleted(object sender, ExportPartsInboundPlanWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            //TODO:查询时间范围需要保留时间 23:59:59
            var newCompositeFilter = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                var createTimeFilters = compositeFilter.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilters != null) {
                    var dateTime = createTimeFilters.Filters.ElementAt(1).Value as DateTime?;
                    if(dateTime.HasValue)
                        createTimeFilters.Filters.ElementAt(1).Value = new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 23, 59, 59);
                }
                newCompositeFilter.Filters.Add(compositeFilter);
            } else
                newCompositeFilter.Filters.Add(filterItem);
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = newCompositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
