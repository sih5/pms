﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsStocking", "PartsLocationChanges", ActionPanelKeys = new[] {
       CommonActionKeys.EXPORT
    })]
    public class WarehouseAreaHistoryQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public WarehouseAreaHistoryQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.QueryPanel_Title_PartsLocationChanges;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("WarehouseAreaHistory"));
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "WarehouseAreaHistory"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    ShellViewModel.Current.IsBusy = true;
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<WarehouseAreaHistory>().Select(r => r.Id).ToArray();
                        this.ExportWarehouseAreaHistory(ids, null, null, null, null, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var warehouseId = compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseId").Value as int?;
                            var warehouseAreaCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseAreaCategoryId").Value as int?;
                            var partCode = compositeFilterItem.Filters.Single(r => r.MemberName == "SparePart.Code").Value as string;
                            var partName = compositeFilterItem.Filters.Single(r => r.MemberName == "SparePart.Name").Value as string;
                            var wareHouseAreaCode = compositeFilterItem.Filters.Single(r => r.MemberName == "WarehouseArea.Code").Value as string;
                            this.ExportWarehouseAreaHistory(null, warehouseId, warehouseAreaCategoryId, partCode, partName, wareHouseAreaCode);
                        }
                    }
                    break;
            }
        }
        private void ExportWarehouseAreaHistory(int[] ids, int? warehouseId, int? warehouseAreaCategoryId, string partCodes, string partName, string WarehouseAreaCode) {
            this.excelServiceClient.ExportWarehouseAreaHistoryAsync(ids, warehouseId, warehouseAreaCategoryId, partCodes, partName, WarehouseAreaCode);
            this.excelServiceClient.ExportWarehouseAreaHistoryCompleted -= excelServiceClient_ExportWarehouseAreaHistoryCompleted;
            this.excelServiceClient.ExportWarehouseAreaHistoryCompleted += excelServiceClient_ExportWarehouseAreaHistoryCompleted;
        }

        void excelServiceClient_ExportWarehouseAreaHistoryCompleted(object sender, ExportWarehouseAreaHistoryCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(compositeFilterItem);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            ClientVar.ConvertTime(compositeFilterItem);
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "StorageCompanyId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
