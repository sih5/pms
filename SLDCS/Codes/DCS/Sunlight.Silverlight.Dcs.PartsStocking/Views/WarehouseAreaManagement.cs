﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.Custom;
using Sunlight.Silverlight.Dcs.PartsStocking.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsStocking", "WarehouseArea", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON, "WarehouseArea"
    })]
    public class WarehouseAreaManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private WarehouseAreaView warehouseAreaView;
        private const string DATA_VIEW_WAREHOUSEAREAVIEW = "_DataViewWarehouseAreaView_";
        private DcsDetailPanelBase warehouseAreaDetailPanel;
        private RadWindow radWindow;

        private RadWindow RadWindow {
            get {
                return this.radWindow ?? (this.radWindow = new RadWindow {
                    Content = this.WindowEditView,
                    Width = 720,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    CanClose = false,
                    CanMove = true,
                    Header = ""
                });
            }
        }

        private DataEditViewBase windowEditView;

        private DataEditViewBase WindowEditView {
            get {
                if(this.windowEditView == null) {
                    this.windowEditView = DI.GetDataEditView("WarehouseAreaForStorage");
                    this.windowEditView.EditCancelled += windowEditView_EditCancelled;
                    this.windowEditView.EditSubmitted += windowEditView_EditSubmitted;
                }
                return this.windowEditView;
            }
        }

        void windowEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.RadWindow.Close();
        }

        private void windowEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.RadWindow.Close();
            this.ExecuteQueryDelayed();
        }

        /// <summary>
        /// method = 0 调用导入库区方法，method = 1 调用导入库位方法
        /// </summary>
        private int method = 0;

        public WarehouseAreaManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.DataManagementView_Title_WarehouseArea;
            this.Loaded += this.WarehouseAreaManagement_Loaded;
        }

        private DcsDetailPanelBase WarehouseAreaDetailPanel {
            get {
                return this.warehouseAreaDetailPanel ?? (this.warehouseAreaDetailPanel = DI.GetDetailPanel("WarehouseArea") as DcsDetailPanelBase);
            }
        }

        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("WarehouseAreaFor");
                    this.dataGridView.RowDoubleClick += dataGridView_RowDoubleClick;
                }
                return this.dataGridView;
            }
        }

        private void dataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            var virtualWarehouseArea = e.Row.DataContext as VirtualWarehouseArea;
            if(virtualWarehouseArea == null)
                return;
            this.WindowEditView.SetObjectToEditById(virtualWarehouseArea.Id);
            this.RadWindow.ShowDialog();
        }

        private void WarehouseAreaManagement_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.WarehouseAreaView.RefreshTreeView();
        }

        private WarehouseAreaView WarehouseAreaView {
            get {
                if(this.warehouseAreaView == null) {
                    this.warehouseAreaView = new WarehouseAreaView();
                    this.warehouseAreaView.PropertyChanged += this.CurrentSelectedItem_PropertyChanged;
                    //构建树菜单
                    this.warehouseAreaView.RadMenuItems.Add(new DcsMenuItem {
                        Header = PartsStockingUIStrings.MenuItem_Text_NewStockArea,
                        Command = new DelegateCommand(o => this.AddNewWarehouseArea()),
                        CheckMenuVisiable = () => this.WarehouseAreaView.CurrentSelectedItem != null && this.WarehouseAreaView.CurrentSelectedItem.Status == (int)DcsBaseDataStatus.有效 && this.WarehouseAreaView.CurrentSelectedItem.AreaKind != (int)DcsAreaKind.库位
                    });
                    this.warehouseAreaView.RadMenuItems.Add(new DcsMenuItem {
                        Header = PartsStockingUIStrings.MenuItem_Text_NewStockPosition,
                        Command = new DelegateCommand(o => this.AddNewWarehouseRegion()),
                        CheckMenuVisiable = () => this.WarehouseAreaView.CurrentSelectedItem != null && this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.库区 && this.WarehouseAreaView.CurrentSelectedItem.Status == (int)DcsBaseDataStatus.有效
                             && !(this.WarehouseAreaView.CurrentSelectedItem.WarehouseAreaCategory.Category == (int)DcsAreaType.待发区 && (this.WarehouseAreaView.CurrentSelectedItem.Code.Equals("X1") || this.WarehouseAreaView.CurrentSelectedItem.Code.Equals("X1-01-01-01")))
                    });
                    this.warehouseAreaView.RadMenuItems.Add(new DcsMenuItem {
                        Header = PartsStockingUIStrings.MenuItem_Text_Edit,
                        Command = new DelegateCommand(o => {
                            if(this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.仓库) {
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_WarehouseArea_WarehouseCanNotEdit);
                                return;
                            }
                            this.DataEditView.SetObjectToEditById(this.WarehouseAreaView.CurrentSelectedItem.Id);
                            this.WarehouseAreaView.TreeIsEnabled = false;
                            this.WarehouseAreaView.RadTransitionControl.Content = this.DataEditView;
                            this.CheckActionsCanExecute();
                        }),
                        CheckMenuVisiable = () => this.WarehouseAreaView.CurrentSelectedItem != null && this.WarehouseAreaView.CurrentSelectedItem.Status == (int)DcsBaseDataStatus.有效 && this.WarehouseAreaView.CurrentSelectedItem.AreaKind != (int)DcsAreaKind.仓库
                            && !(this.WarehouseAreaView.CurrentSelectedItem.WarehouseAreaCategory.Category == (int)DcsAreaType.待发区 && (this.WarehouseAreaView.CurrentSelectedItem.Code.Equals("X1") || this.WarehouseAreaView.CurrentSelectedItem.Code.Equals("X1-01-01-01")))
                    });
                    this.warehouseAreaView.RadMenuItems.Add(new DcsMenuItem {
                        Header = PartsStockingUIStrings.MenuItem_Text_Abandon,
                        Command = new DelegateCommand(o => this.AbandonWarehouseArea()),
                        CheckMenuVisiable = () => this.WarehouseAreaView.CurrentSelectedItem != null && this.WarehouseAreaView.CurrentSelectedItem.Status == (int)DcsBaseDataStatus.有效 && this.WarehouseAreaView.CurrentSelectedItem.AreaKind != (int)DcsAreaKind.仓库
                             && !(this.WarehouseAreaView.CurrentSelectedItem.WarehouseAreaCategory.Category == (int)DcsAreaType.待发区 && (this.WarehouseAreaView.CurrentSelectedItem.Code.Equals("X1") || this.WarehouseAreaView.CurrentSelectedItem.Code.Equals("X1-01-01-01")))
                    });
                    this.warehouseAreaView.RadTransitionControl.Content = this.WarehouseAreaView.Grid;
                }
                return this.warehouseAreaView;
            }
        }

        private void CurrentSelectedItem_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            switch(e.PropertyName) {
                case "CurrentSelectedItem":
                    this.ExecuteQueryDelayed();
                    this.CheckActionsCanExecute();
                    break;
            }
        }

        private void ExecuteQueryDelayed() {
            var composite = new CompositeFilterItem();
            var selectItem = this.WarehouseAreaView.SelectItem; //获取当前选中的节点
            if(selectItem == null)
                return;
            composite.Filters.Add(new FilterItem {
                MemberName = "ParentId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = this.WarehouseAreaView.CurrentSelectedItem.Id
            });
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_VIEW_WAREHOUSEAREAVIEW, () => this.WarehouseAreaView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.SwitchViewTo(DATA_VIEW_WAREHOUSEAREAVIEW);
            this.WarehouseAreaView.Grid.Children.Add(this.WarehouseAreaDetailPanel);
            var detail = new DcsDetailDataEditView();
            detail.UnregisterButton(detail.InsertButton);
            detail.UnregisterButton(detail.DeleteButton);
            detail.Register("库位", null, this.DataGridView);
            detail.SetValue(Grid.ColumnProperty, 2);
            this.WarehouseAreaView.Grid.Children.Add(detail);
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("WarehouseArea");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    var item = (WarehouseAreaDataEditView)this.dataEditView;
                    item.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.windowEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.WarehouseAreaView.RefreshTreeView();
            this.WarehouseAreaView.TreeIsEnabled = true;
            this.WarehouseAreaView.RadTransitionControl.Content = this.WarehouseAreaView.Grid;
            this.ExecuteQueryDelayed();
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.WarehouseAreaView.TreeIsEnabled = true;
            this.WarehouseAreaView.RadTransitionControl.Content = this.WarehouseAreaView.Grid;
        }

        private void AddNewWarehouseArea() {
            if(this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.库位) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_WarehouseArea_StorePositionCanNotNewChild);
                return;
            }
            var warehouseArea = this.DataEditView.CreateObjectToEdit<WarehouseArea>();
            var selectItem = this.WarehouseAreaView.SelectItem; //获取当前选中的节点
            warehouseArea.ParentId = this.WarehouseAreaView.CurrentSelectedItem.Id;
            //根据父节点Id获取节点
            var domcontext = new DcsDomainContext();
            domcontext.Load(domcontext.GetWarehouseAreaWithWarehouseAreaCategoryQuery((int)warehouseArea.ParentId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                warehouseArea.Status = (int)DcsBaseDataStatus.有效;
                if(this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.仓库)
                    warehouseArea.AreaKind = (int)DcsAreaKind.库区;
                else {
                    warehouseArea.AreaKind = this.WarehouseAreaView.CurrentSelectedItem.AreaKind;
                    warehouseArea.AreaCategoryId = ((WarehouseArea)selectItem.Tag).AreaCategoryId;
                    if(this.WarehouseAreaView.CurrentSelectedItem.TopLevelWarehouseAreaId.HasValue)
                        warehouseArea.TopLevelWarehouseAreaId = this.WarehouseAreaView.CurrentSelectedItem.TopLevelWarehouseAreaId.Value;
                    else
                        warehouseArea.TopLevelWarehouseAreaId = this.WarehouseAreaView.CurrentSelectedItem.Id;
                }
                this.WarehouseAreaView.TreeIsEnabled = false;
                if(selectItem == null)
                    return;
                warehouseArea.StorageCompanyId = ((WarehouseArea)selectItem.Tag).Warehouse.StorageCompanyId;
                warehouseArea.WarehouseId = ((WarehouseArea)selectItem.Tag).Warehouse.Id;
                warehouseArea.WarehouseName = ((WarehouseArea)selectItem.Tag).Warehouse.Name;
                warehouseArea.HasChildrenNode = ((WarehouseArea)selectItem.Tag).HasChildrenNode; //该节点是否有子节点 有库区库位类型可以编辑
                this.WarehouseAreaView.RadTransitionControl.Content = this.DataEditView;
                this.CheckActionsCanExecute();
            }, null);
        }

        private void AddNewWarehouseRegion() {
            if(this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.库位) {
                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_WarehouseArea_StorePositionCanNotNewChild);
                return;
            }
            var warehouseArea = this.DataEditView.CreateObjectToEdit<WarehouseArea>();
            var selectItem = this.WarehouseAreaView.SelectItem; //获取当前选中的节点
            warehouseArea.ParentId = this.WarehouseAreaView.CurrentSelectedItem.Id;
            //根据父节点Id获取节点
            var domcontext = new DcsDomainContext();
            domcontext.Load(domcontext.GetWarehouseAreaWithWarehouseAreaCategoryQuery((int)warehouseArea.ParentId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                warehouseArea.Status = (int)DcsBaseDataStatus.有效;
                warehouseArea.AreaKind = (int)DcsAreaKind.库位;
                warehouseArea.AreaCategoryId = ((WarehouseArea)selectItem.Tag).AreaCategoryId;
                if(this.WarehouseAreaView.CurrentSelectedItem.TopLevelWarehouseAreaId.HasValue)
                    warehouseArea.TopLevelWarehouseAreaId = this.WarehouseAreaView.CurrentSelectedItem.TopLevelWarehouseAreaId.Value;
                else
                    warehouseArea.TopLevelWarehouseAreaId = this.WarehouseAreaView.CurrentSelectedItem.Id;
                this.WarehouseAreaView.TreeIsEnabled = false;
                if(selectItem == null)
                    return;
                warehouseArea.StorageCompanyId = ((WarehouseArea)selectItem.Tag).Warehouse.StorageCompanyId;
                warehouseArea.WarehouseId = ((WarehouseArea)selectItem.Tag).Warehouse.Id;
                warehouseArea.WarehouseName = ((WarehouseArea)selectItem.Tag).Warehouse.Name;
                warehouseArea.HasChildrenNode = ((WarehouseArea)selectItem.Tag).HasChildrenNode; //该节点是否有子节点 有库区库位类型可以编辑
                this.WarehouseAreaView.RadTransitionControl.Content = this.DataEditView;
                this.CheckActionsCanExecute();
            }, null);
        }

        private void AbandonWarehouseArea() {
            DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                var entity = this.WarehouseAreaView.CurrentSelectedItem;
                if(entity == null)
                    return;
                try {
                    if(entity.Can作废库区库位)
                        entity.作废库区库位();
                    var domainContext = this.WarehouseAreaView.DomainContext;
                    if(domainContext == null)
                        return;
                    domainContext.SubmitChanges(submitOp => {
                        if(submitOp.HasError) {
                            if(!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                            domainContext.RejectChanges();
                            return;
                        }
                        this.WarehouseAreaView.RefreshTreeView();
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }, null);
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            });
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    //if(this.DataGridView.SelectedEntities == null) {
                    //    return true;
                    //} else {
                    //    var entities = this.DataGridView.SelectedEntities.Cast<VirtualWarehouseArea>().ToArray();
                    //    if(entities.Length > 1)
                    //        return false;
                    //}
                    return true;
                case "Print":
                    if(this.DataGridView.SelectedEntities == null) {
                        return false;
                    } else {
                        return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any();
                    }

                case "ImportWarehouse":
                    if(this.WarehouseAreaView.CurrentSelectedItem == null)
                        return false;
                    if(this.WarehouseAreaView.RadTransitionControl.Content == this.DataEditView)
                        return false;
                    return this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.仓库 || this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.库区;
                case "ImportArea":
                    if(this.WarehouseAreaView.CurrentSelectedItem == null)
                        return false;
                    if(this.WarehouseAreaView.RadTransitionControl.Content == this.DataEditView)
                        return false;
                    if (this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.库区 && this.WarehouseAreaView.CurrentSelectedItem.Code.Equals("X1"))
                        return false;
                    return this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.库区;
                default:
                    return this.WarehouseAreaView.CurrentSelectedItem != null && this.WarehouseAreaView.RadTransitionControl.Content != this.DataEditView;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    if(this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.库位) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_WarehouseArea_StorePositionCanNotNewChild);
                        return;
                    }
                    var warehouseArea = this.DataEditView.CreateObjectToEdit<WarehouseArea>();
                    var selectItem = this.WarehouseAreaView.SelectItem; //获取当前选中的节点
                    warehouseArea.ParentId = this.WarehouseAreaView.CurrentSelectedItem.Id;
                    //根据父节点Id获取节点
                    var domcontext = new DcsDomainContext();
                    domcontext.Load(domcontext.GetWarehouseAreaWithWarehouseAreaCategoryQuery((int)warehouseArea.ParentId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        warehouseArea.Status = (int)DcsBaseDataStatus.有效;
                        if(this.WarehouseAreaView.CurrentSelectedItem.AreaKind != (int)DcsAreaKind.仓库) {
                            warehouseArea.AreaCategoryId = ((WarehouseArea)selectItem.Tag).AreaCategoryId;
                            if(this.WarehouseAreaView.CurrentSelectedItem.TopLevelWarehouseAreaId.HasValue)
                                warehouseArea.TopLevelWarehouseAreaId = this.WarehouseAreaView.CurrentSelectedItem.TopLevelWarehouseAreaId.Value;
                            else
                                warehouseArea.TopLevelWarehouseAreaId = this.WarehouseAreaView.CurrentSelectedItem.Id;
                        }
                        this.WarehouseAreaView.TreeIsEnabled = false;
                        if(selectItem == null)
                            return;
                        warehouseArea.StorageCompanyId = ((WarehouseArea)selectItem.Tag).Warehouse.StorageCompanyId;
                        warehouseArea.WarehouseId = ((WarehouseArea)selectItem.Tag).Warehouse.Id;
                        warehouseArea.WarehouseName = ((WarehouseArea)selectItem.Tag).Warehouse.Name;
                        warehouseArea.HasChildrenNode = ((WarehouseArea)selectItem.Tag).HasChildrenNode; //该节点是否有子节点 有库区库位类型可以编辑
                        this.WarehouseAreaView.RadTransitionControl.Content = this.DataEditView;
                        this.CheckActionsCanExecute();
                    }, null);
                    break;
                case CommonActionKeys.EDIT:
                    if(this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.仓库) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_WarehouseArea_WarehouseCanNotEdit);
                        return;
                    }
                    if(this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.库区 &&this.WarehouseAreaView.CurrentSelectedItem.Code.Equals("X1") ) {
                        UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_NotAllowedUpdate);
                        return;
                    }
                    this.DataEditView.SetObjectToEditById(this.WarehouseAreaView.CurrentSelectedItem.Id);
                    this.WarehouseAreaView.TreeIsEnabled = false;
                    this.WarehouseAreaView.RadTransitionControl.Content = this.DataEditView;
                    //this.SwitchViewTo(DATA_EDIT_VIEW);
                    this.CheckActionsCanExecute();
                    break;
                case CommonActionKeys.ABANDON:
                    if(this.WarehouseAreaView.CurrentSelectedItem.AreaKind == (int)DcsAreaKind.库区 &&this.WarehouseAreaView.CurrentSelectedItem.Code.Equals("X1")) {
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_NotAllowedAbandon);
                                return;
                       }
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                            var entity = this.DataGridView.SelectedEntities.Cast<VirtualWarehouseArea>().ToArray();

                            if(entity.Count() == 0)
                                return;
                            foreach(var item in entity) {
                                if(item.AreaKind == (int)DcsAreaKind.仓库) {
                                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_NotAllowedAbandonWarehouse);
                                    return;
                                }
                            }
                            var domainContext = new DcsDomainContext();
                           var ids =entity.Select(r=>r.Id).ToArray();
                            domainContext.作废库位(ids, invokeOp => {
                                if(invokeOp.HasError) {
                                    if(!invokeOp.IsErrorHandled)
                                        invokeOp.MarkErrorAsHandled();
                                    var error = invokeOp.ValidationErrors.First();
                                    if(error != null)
                                        UIHelper.ShowNotification(invokeOp.ValidationErrors.First().ErrorMessage, 5);
                                    else
                                        DcsUtils.ShowDomainServiceOperationWindow(invokeOp);
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.ExecuteQueryDelayed();
                            }, null);
                        } else {
                            var entity = this.WarehouseAreaView.CurrentSelectedItem;
                            if(entity == null)
                                return;
                            if(entity.AreaKind == (int)DcsAreaKind.仓库) {
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_NotAllowedAbandonWarehouse);
                                return;
                            }
                            try {
                                if(entity.Can作废库区库位)
                                    entity.作废库区库位();
                                var domainContext = this.WarehouseAreaView.DomainContext;
                                if(domainContext == null)
                                    return;
                                domainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        domainContext.RejectChanges();
                                        return;
                                    }
                                    this.WarehouseAreaView.RefreshTreeView();
                                    UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                    this.CheckActionsCanExecute();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }
                    });
                    break;
                case "ImportWarehouse":
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_ImportWarehouseConfirm, () => {
                        method = 0;
                        this.Uploader.ShowFileDialog();
                    });
                    break;
                case "ImportArea":
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_ImportWarehouseAreaConfirm, () => {
                        method = 1;
                        this.Uploader.ShowFileDialog();
                    });
                    break;
                case "Print":
                    var selected = this.DataGridView.SelectedEntities.Cast<VirtualWarehouseArea>();
                    if(selected == null)
                        return;
                    WarehouseArea.Clear();
                    var aa = "";
                    foreach(var warehouse in selected) {
                        if(aa == "") {
                            aa = warehouse.Id.ToString();
                            continue;
                        }
                        aa = aa + "," + warehouse.Id;
                    }
                    BasePrintWindow lablePrintWindow = new WarehouseAreaForLablePrintWindow {
                        Header = "库位打印",
                        WarehouseArea = aa
                    };
                    lablePrintWindow.ShowDialog();
                    break;
            }
        }

        private ObservableCollection<WarehouseArea> warehouseArea;
        public ObservableCollection<WarehouseArea> WarehouseArea {
            get {
                return warehouseArea ?? (this.warehouseArea = new ObservableCollection<WarehouseArea>());
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
        }

        private string strFileName;
        private ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += uploader_FileUploaded;
                }
                return this.uploader;
            }
        }

        void uploader_FileUploaded(object sender, FileUploadedEventArgs e) {
            if(method == 0) {
                this.excelServiceClient.ImportWarehouseAreaAsync(this.WarehouseAreaView.CurrentSelectedItem.Id, e.HandlerData.CustomData["Path"].ToString());
                ShellViewModel.Current.IsBusy = true;
                this.excelServiceClient.ImportWarehouseAreaCompleted -= this.ExcelServiceClient_ImportWarehouseAreaCompleted;
                this.excelServiceClient.ImportWarehouseAreaCompleted += this.ExcelServiceClient_ImportWarehouseAreaCompleted;
            }
            if(method == 1) {
                ShellViewModel.Current.IsBusy = true;
                this.excelServiceClient.ImportWarehouseRegionAsync(this.WarehouseAreaView.CurrentSelectedItem.Id, e.HandlerData.CustomData["Path"].ToString());
                this.excelServiceClient.ImportWarehouseRegionCompleted -= this.ExcelServiceClient_ImportWarehouseRegionCompleted;
                this.excelServiceClient.ImportWarehouseRegionCompleted += this.ExcelServiceClient_ImportWarehouseRegionCompleted;
            }
        }

        private void ExcelServiceClient_ImportWarehouseRegionCompleted(object sender, ImportWarehouseRegionCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrWhiteSpace(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrWhiteSpace(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
            else
                this.WarehouseAreaView.RefreshTreeView();
        }

        private void ExcelServiceClient_ImportWarehouseAreaCompleted(object sender, ImportWarehouseAreaCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrWhiteSpace(e.errorMessage))
                UIHelper.ShowNotification(e.errorMessage);
            if(!string.IsNullOrWhiteSpace(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
            else
                this.WarehouseAreaView.RefreshTreeView();
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage(PartsStockingUIStrings.Upload_Error3);
                    e.Handled = true;
                }
            }
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        protected Action<string> UploadFileSuccessedProcessing;

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

    }
}