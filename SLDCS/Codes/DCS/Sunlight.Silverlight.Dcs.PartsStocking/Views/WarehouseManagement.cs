﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.PartsStocking.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.PartsStocking.Views {
    [PageMeta("PartsStockingAndLogistics", "PartsStocking", "Warehouse", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT
    })]
    public class WarehouseManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        public WarehouseManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = PartsStockingUIStrings.WarehouseManagement_Title_Warehouse;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("WarehouseWithOperators"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("Warehouse");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            //this.DataGridView.FilterItem = filterItem;
            //this.DataGridView.ExecuteQueryDelayed();
            var compositeFilterItem = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                compositeFilterItem = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(compositeFilterItem);
            } else
                compositeFilterItem.Filters.Add(filterItem);
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "StorageCompanyId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();

        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "Warehouse"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var warehouse = this.DataEditView.CreateObjectToEdit<Warehouse>();
                    warehouse.Status = (int)DcsBaseDataStatus.有效;
                    warehouse.StorageStrategy = (int)DcsStorageStrategy.定位存储;
                    warehouse.StorageCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    warehouse.Type = (int)DcsWarehouseType.总库;
                    warehouse.IsQualityWarehouse = false;
                    warehouse.IsCentralizedPurchase = false;
                    var DomainContext = new DcsDomainContext();
                    DomainContext.Load(DomainContext.GetCompaniesQuery().Where(e => e.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entity = loadOp.Entities.SingleOrDefault();
                        if(entity != null)
                            warehouse.StorageCompanyType = entity.Type;
                    }, null);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(PartsStockingUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<Warehouse>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废仓库)
                                entity.作废仓库();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(PartsStockingUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                       if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                          var ids = this.DataGridView.SelectedEntities.Cast<Warehouse>().Select(r => r.Id).ToArray();
                         this.ExportWarehouseQueryView(ids, BaseApp.Current.CurrentUserData.EnterpriseId, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, null);
                     } else {
                         var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                         if(filterItem == null)
                             return;
                         var Code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                         var Name = filterItem.Filters.Single(r => r.MemberName == "Name").Value as string;
                         var StorageCenter = filterItem.Filters.Single(r => r.MemberName == "StorageCenter").Value as int?;
                         var Status = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                         this.ExportWarehouseQueryView(new int[] { }, BaseApp.Current.CurrentUserData.EnterpriseId, BaseApp.Current.CurrentUserData.EnterpriseId, Code, Name, StorageCenter, Status);
                     }
                     break;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExportWarehouseQueryView(int[] warehouseIds, int StorageCompanyId, int branchId, string Code, string Name, int? StorageCenter, int? Status) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportWarehouseQueryViewAsync(warehouseIds, StorageCompanyId, branchId, Code, Name, StorageCenter, Status);
            this.excelServiceClient.ExportWarehouseQueryViewCompleted -= excelServiceClient_ExportWarehouseQueryViewCompleted;
            this.excelServiceClient.ExportWarehouseQueryViewCompleted += excelServiceClient_ExportWarehouseQueryViewCompleted;
        }

        private void excelServiceClient_ExportWarehouseQueryViewCompleted(object sender, ExportWarehouseQueryViewCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<Warehouse>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
        //protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
        //    this.SwitchViewTo(DATA_GRID_VIEW);
        //    var compositeFilterItem = new CompositeFilterItem();
        //    if(filterItem is CompositeFilterItem) {
        //        compositeFilterItem = filterItem as CompositeFilterItem;
        //        ClientVar.ConvertTime(compositeFilterItem);
        //    } else
        //        compositeFilterItem.Filters.Add(filterItem);
        //    compositeFilterItem.Filters.Add(new FilterItem {
        //        MemberName = "StorageCompanyId",
        //        Operator = FilterOperator.IsEqualTo,
        //        MemberType = typeof(int),
        //        Value = BaseApp.Current.CurrentUserData.EnterpriseId
        //    });
        //    this.DataGridView.FilterItem = compositeFilterItem;
        //    this.DataGridView.ExecuteQueryDelayed();
        //}
    }
}
