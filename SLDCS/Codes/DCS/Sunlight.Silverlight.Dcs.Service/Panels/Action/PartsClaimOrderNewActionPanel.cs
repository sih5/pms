﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Action {
    public class PartsClaimOrderNewActionPanel : DcsActionPanelBase {
        public PartsClaimOrderNewActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = ServiceUIStrings.ActionPanel_Title_RepairClaimApplication,
                UniqueId = "PartsClaimOrderNew",
                ActionItems = new[] {
                    new ActionItem {
                        Title = ServiceUIStrings.Action_Title_InitialApprove,
                        UniqueId = "InitialApprove",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/Audit.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Reject,
                        UniqueId = "Reject",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/Reject.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Audit,
                        UniqueId = "Audit",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/InitialApprove.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Approval,
                        UniqueId = "Approve",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Service/FinalApprove.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
