﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Action {
    public class PartsClaimOrderNewForAgencyActionPanel : DcsActionPanelBase {
        public PartsClaimOrderNewForAgencyActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsClaimOrderNewForAgency",
                Title = ServiceUIStrings.ActionPanel_Title_PartsClaimOrderNewForAgency,
                ActionItems = new[] {
                    //new ActionItem {
                    //    Title = "出库",
                    //    UniqueId = "DeliverParts",
                    //    ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/DeliverParts.png"),
                    //    CanExecute = false
                    //}
                    new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Submit,
                        UniqueId = "Submit",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Submit.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Receive,
                        UniqueId = "Receive",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/WarehouseParts.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Shipping,
                        UniqueId = "Shipping",
                        ImageUri = Utils.MakeServerUri("Client/DCS/Images/Menu/PartsStocking/PartsShippingOrderManagement.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
