﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Action {
    public class PartsClaimOrderNewForDealerActionPanel : DcsActionPanelBase {
        public PartsClaimOrderNewForDealerActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "PartsClaimOrderNewForDealer",
                Title = ServiceUIStrings.Action_Title_PartsClaimOrderNew,
                ActionItems = new[] {
                   new ActionItem{
                       Title= ServiceUIStrings.Action_Title_Shipping,
                       UniqueId="Shipping",
                       ImageUri =Utils.MakeServerUri("Client/Dcs/Images/Operations/shipping.png"),
                       CanExecute=false 
                    },   new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Terminate,
                        UniqueId = "Terminate",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute = false
                    },new ActionItem {
                         Title = ServiceUIStrings.Action_Title_LabelPrint,
                        UniqueId = "LabelPrint",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
