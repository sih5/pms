﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Action {
    public class SsUsedPartsStorageActionPanel : DcsActionPanelBase {
        public SsUsedPartsStorageActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = ServiceUIStrings.ActionPanel_Title_General,
                UniqueId = "SsUsedPartsStorage",
                ActionItems = new[] {
                       new ActionItem {
                        Title = ServiceUIStrings.Action_Title_PrintBarCode,
                        UniqueId = "PrintBarCode",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                    }
            };
        }
    }
}
