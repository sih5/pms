﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Action {
    public class UsedPartsInboundOrderActionPanel : DcsActionPanelBase {

        public UsedPartsInboundOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "UsedPartsInboundOrder",
                Title = ServiceUIStrings.ActionPanel_Title_UsedPartsInBoundOrder,
                ActionItems = new[] {
                    new ActionItem {
                        Title = ServiceUIStrings.Action_Title_WarehouseParts,
                        UniqueId = "WarehouseParts",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/WarehouseParts.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = ServiceUIStrings.Action_Title_ViewDetail,
                        UniqueId = "ViewDetail",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Terminate,
                        UniqueId = "Terminate",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Terminate.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
