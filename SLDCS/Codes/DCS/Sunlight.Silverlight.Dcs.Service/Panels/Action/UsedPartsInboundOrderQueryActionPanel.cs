﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Action {
    public class UsedPartsInboundOrderQueryActionPanel : DcsActionPanelBase {
        public UsedPartsInboundOrderQueryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = ServiceUIStrings.ActionPanel_Title_General,
                UniqueId = "Common",
                ActionItems = new[] {
                 new ActionItem {
                     Title = ServiceUIStrings.Action_Title_PrintForFinance,
                      UniqueId = "PrintForFinance",
                     ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                     CanExecute = false
                 },
                  new ActionItem {
                        Title = ServiceUIStrings.Action_Title_PrintGCC,
                        UniqueId = "PrintForGC",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
               
            };
        }
    }
}
