﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Action {
    public class UsedPartsOutboundOrderActionPanel : DcsActionPanelBase {
        public UsedPartsOutboundOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "UsedPartsOutboundOrder",
                Title = ServiceUIStrings.ActionPanel_Title_UsedPartsOutboundOrder,
                ActionItems = new[] {
                    new ActionItem {
                        Title = ServiceUIStrings.Action_Title_DeliverParts,
                        UniqueId = "DeliverParts",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/DeliverParts.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = ServiceUIStrings.Action_Title_ViewDetail,
                        UniqueId = "ViewDetail",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
