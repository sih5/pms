﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Action {
    public class UsedPartsOutboundOrderQueryActionPanel : DcsActionPanelBase {
        public UsedPartsOutboundOrderQueryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "UsedPartsOutboundOrderQuery",
                Title = ServiceUIStrings.ActionPanel_Title_UsedPartsOutboundOrder,
                ActionItems = new[] {
                    new ActionItem {
                        Title = ServiceUIStrings.Action_Title_NotPartCodePrint,
                        UniqueId = "NotPartCodePrint",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
