﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Action {
    public class UsedPartsShippingOrderActionPanel : DcsActionPanelBase {
        public UsedPartsShippingOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = ServiceUIStrings.ActionPanel_Title_General,
                UniqueId = "UsedPartsShippingOrder",
                ActionItems = new[] {
                    new ActionItem {
                        Title = ServiceUIStrings.Action_Title_ExportDetail,
                        UniqueId = "ExportDetail",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ExportDetail.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                        Title = ServiceUIStrings.Action_Title_PrintBarCode,
                        UniqueId = "PrintBarCode",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Abandon,
                        UniqueId ="Abandon",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title = ServiceUIStrings.Action_Title_PrintChooseBarCode,
                        UniqueId ="PrintChooseBarCode",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
