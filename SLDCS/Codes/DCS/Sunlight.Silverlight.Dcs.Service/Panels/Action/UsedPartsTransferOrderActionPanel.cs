﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Action {
    public class UsedPartsTransferOrderActionPanel : DcsActionPanelBase {
        public UsedPartsTransferOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = ServiceUIStrings.ActionPanel_Title_General,
                UniqueId = "Common",
                ActionItems = new[] {
                    new ActionItem {
                        Title = ServiceUIStrings.Action_Title_ExportDetail,
                        UniqueId = "ExportDetail",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Export.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title = ServiceUIStrings.Action_Title_PartsTransferOrderPrint,
                        UniqueId = "PartsTransferOrderPrint",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    },  new ActionItem {
                        Title = ServiceUIStrings.Action_Title_CodePrint,
                        UniqueId = "CodePrint",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
