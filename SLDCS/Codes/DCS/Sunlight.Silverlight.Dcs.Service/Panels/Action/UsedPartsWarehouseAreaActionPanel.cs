﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Action {
    public class UsedPartsWarehouseAreaActionPanel : DcsActionPanelBase {
        public UsedPartsWarehouseAreaActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = ServiceUIStrings.ActionPanel_Title_General,
                ActionItems = new[] {
                    new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Add,
                        UniqueId = CommonActionKeys.ADD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png")
                    }, new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Edit,
                        UniqueId = CommonActionKeys.EDIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = ServiceUIStrings.Action_Title_Abandon,
                        UniqueId = CommonActionKeys.ABANDON,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title= ServiceUIStrings.Action_Title_DefaultWarehouseArea,
                        UniqueId="DefaultWarehouseArea",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Setup.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
