﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using System.Linq;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.View;
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class PartsClaimOrderNewForAuditDataEditPanel {

        private DcsDomainContext domainContext = new DcsDomainContext();
        //private ObservableCollection<KeyValuePair> kvIsPasseds;
        //public ObservableCollection<KeyValuePair> KvIsPasseds
        //{
        //    get
        //    {
        //        return this.kvIsPasseds ?? (this.kvIsPasseds = new ObservableCollection<KeyValuePair>());
        //    }
        //}

        public PartsClaimOrderNewForAuditDataEditPanel()
        {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI()
        {
            //this.kvIsPasseds.Add(new KeyValuePair
            //{
            //    Key = 1,
            //    Value = "审核通过"
            //});
            //this.kvIsPasseds.Add(new KeyValuePair
            //{
            //    Key = 0,
            //    Value = "驳回"
            //});
            
            //this.CbIsRadio.SelectedValue = 1;

            //选择中心库销售订单
            var salesOrder = DI.GetQueryWindow("PartsSalesOrderForSelect");
            popupTextBoxCenterSaleOrderCode.PopupContent = salesOrder;
            salesOrder.Loaded += salesOrder_Loaded;
            salesOrder.SelectionDecided += salesOrder_SelectionDecided;
        }

        private void salesOrder_Loaded(object sender, RoutedEventArgs e)
        {
            var queryWindow = sender as QueryWindowBase;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if (queryWindow == null || partsClaimOrderNew == null)
                return;
            if (partsClaimOrderNew.HasValidationErrors)
                partsClaimOrderNew.ValidationErrors.Clear();
            if (partsClaimOrderNew.FaultyPartsId == default(int))
            {
                UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Validation_FaultyPartsIdIsNotNull);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if (parent != null)
                    parent.Close();
            }
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("SparePartId", typeof(int), FilterOperator.IsEqualTo, partsClaimOrderNew.FaultyPartsId));
        }


        private void salesOrder_SelectionDecided(object sender, EventArgs e)
        {
            var queryWindow = sender as DcsQueryWindowBase;
            if (queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsSalesOrder = queryWindow.SelectedEntities.Cast<PartsSalesOrder>().FirstOrDefault();
            if (partsSalesOrder == null)
                return;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if (partsClaimOrderNew == null)
                return;
            if (partsClaimOrderNew.HasValidationErrors)
                partsClaimOrderNew.ValidationErrors.Clear();
            //材料费
            var partsSalesOrderDetail = partsSalesOrder.PartsSalesOrderDetails.Where(r => r.SparePartId == partsClaimOrderNew.FaultyPartsId).FirstOrDefault();
            partsClaimOrderNew.MaterialManagementCost = partsSalesOrderDetail.OrderPrice;
            //中心库销售订单号
            partsClaimOrderNew.CenterPartsSalesOrderId = partsSalesOrder.Id;
            partsClaimOrderNew.CenterPartsSalesOrderCode = partsSalesOrder.Code;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if (parent != null)
                parent.Close();
        }
    }
}
