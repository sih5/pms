﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Windows;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class PartsClaimOrderNewRejectDataEditPanel {
        public PartsClaimOrderNewRejectDataEditPanel() {
            InitializeComponent();
            this.DataContextChanged += PartsClaimOrderNewRejectDataEditPanel_DataContextChanged;
        }

        void PartsClaimOrderNewRejectDataEditPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            partsClaimOrderNew.RejectReason = string.Empty;

        }
        private void Cancel_Click(object sender, RoutedEventArgs e) {
            var parent = this.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.DialogResult = false;
                parent.Close();
            }
        }
        private void Submit_Click(object sender, RoutedEventArgs e) {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null) {
                return;
            }
            partsClaimOrderNew.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(partsClaimOrderNew.RejectReason))
                partsClaimOrderNew.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_ServiceTripClaimApplication_DataEditView_Validation_ServiceTripClaimApplication_RejectReasonNull, new[] {
                    "RejectReason"
                }));
            if(partsClaimOrderNew.HasValidationErrors)
                return;
            ((IEditableObject)partsClaimOrderNew).EndEdit();
            var parent = this.ParentOfType<RadWindow>();
            if(parent != null) {
                parent.DialogResult = true;
                parent.Close();
            }
        }

    }
}
