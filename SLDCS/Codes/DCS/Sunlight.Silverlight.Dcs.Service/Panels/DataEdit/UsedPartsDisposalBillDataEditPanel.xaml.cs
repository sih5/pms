﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class UsedPartsDisposalBillDataEditPanel : IBaseView {
        private readonly string[] kvNames = {
            "UsedPartsDisposalBill_DisposalMethod", "UsedPartsDisposalBill_Status"
        };
        private ObservableCollection<KeyValuePair> kvOriginWarehouseName;
        public UsedPartsDisposalBillDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += UsedPartsDisposalBillDataEditPanel_DataContextChanged;
        }

        private void UsedPartsDisposalBillDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            usedPartsDisposalBill.PropertyChanged -= usedPartsDisposalBill_PropertyChanged;
            usedPartsDisposalBill.PropertyChanged += usedPartsDisposalBill_PropertyChanged;
        }

        private void usedPartsDisposalBill_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            switch(e.PropertyName) {
                case "UsedPartsWarehouseId":
                    ClearUsedPartsDisposalBillDetails();
                    break;
            }
        }

        private void ClearUsedPartsDisposalBillDetails() {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetUsedPartsWarehousesQuery().Where(entity => entity.Id == usedPartsDisposalBill.UsedPartsWarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                usedPartsDisposalBill.UsedPartsWarehouseName = entity.Name;
                usedPartsDisposalBill.UsedPartsWarehouseCode = entity.Code;
            }, null);
            if(usedPartsDisposalBill.UsedPartsDisposalDetails == null || !usedPartsDisposalBill.UsedPartsDisposalDetails.Any())
                return;
            DcsUtils.Confirm(ServiceUIStrings.DataPanel_Confirm_UsedPartsWarehouseHasChangedWillClearDetails, () => {
                foreach(var usedPartsDisposalDetail in usedPartsDisposalBill.UsedPartsDisposalDetails) {
                    usedPartsDisposalBill.ValidationErrors.Clear();
                    usedPartsDisposalBill.UsedPartsDisposalDetails.Remove(usedPartsDisposalDetail);
                }
            });
        }

        public ObservableCollection<KeyValuePair> KvOriginWarehouseName {
            get {
                return this.kvOriginWarehouseName ?? (this.kvOriginWarehouseName = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetUsedPartsWarehouseStaffByPersonnelIdQuery().Where(e => e.PersonnelId == BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var usedWarehouse in loadOp.Entities)
                    this.KvOriginWarehouseName.Add(new KeyValuePair {
                        Key = usedWarehouse.UsedPartsWarehouse.Id,
                        Value = usedWarehouse.UsedPartsWarehouse.Name
                    });
            }, null);
        }

        private bool IsApproveBill {
            get;
            set;
        }

        public bool UsedPartsDisposalMethodCanUse {
            get {
                return !IsApproveBill;
            }
        }

        public bool UsedPartsWarehouseCanUse {
            get {
                return !IsApproveBill;
            }
        }


        public object UsedPartsDisposalMethods {
            get {

                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            //使 审批 和 新增修改 操作可以使用1个页面
            switch(subject) {
                case "IsApproveBill":
                    this.IsApproveBill = contents[0] is bool && (bool)contents[0];
                    return true;
            }
            return true;
        }
    }
}
