﻿
namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class UsedPartsInboundOrderDataEditPanel {
        private readonly string[] kvNames = {
            "UsedPartsInboundOrder_InboundType"
        };

        public UsedPartsInboundOrderDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
