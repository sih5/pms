﻿
namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class UsedPartsOutboundOrderDataEditPanel {
        private readonly string[] kvNames = {
            "UsedPartsOutboundOrder_OutboundType"
        };

        public UsedPartsOutboundOrderDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
