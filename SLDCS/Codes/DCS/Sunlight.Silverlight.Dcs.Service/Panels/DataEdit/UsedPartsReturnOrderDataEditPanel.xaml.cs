﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Input;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class UsedPartsReturnOrderDataEditPanel : INotifyPropertyChanged {
        private ObservableCollection<KeyValuePair> kvDepartmentInformations;
        private ObservableCollection<KeyValuePair> kvOriginWarehouseName;
        private int? departmentInformationId;
        private string responsibleUnitName;
        private string partsSupplierName;

        public event PropertyChangedEventHandler PropertyChanged;


        public UsedPartsReturnOrderDataEditPanel() {
            this.InitializeComponent();
            this.DataContextChanged += this.UsedPartsReturnOrderDataEditPanel_DataContextChanged;
            this.Initializer.Register(this.CreateUI);
        }

        public int? DepartmentInformationId {
            get {
                return this.departmentInformationId;
            }
            set {
                this.departmentInformationId = value;
                this.OnPropertyChanged("DepartmentInformationId");
            }
        }

        public string ResponsibleUnitName {
            get {
                return this.responsibleUnitName;
            }
            set {
                this.responsibleUnitName = value;
                this.OnPropertyChanged("ResponsibleUnitName");
            }
        }

        public string PartsSupplierName {
            get {
                return this.partsSupplierName;
            }
            set {
                this.partsSupplierName = value;
                this.OnPropertyChanged("PartsSupplierName");
            }
        }

        public ObservableCollection<KeyValuePair> KvDepartmentInformations {
            get {
                return this.kvDepartmentInformations ?? (this.kvDepartmentInformations = new ObservableCollection<KeyValuePair>());
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private void UsedPartsReturnOrderDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            this.ResponsibleUnitName = string.Empty;
            this.PartsSupplierName = string.Empty;
            this.DepartmentInformationId = null;
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            this.OutboundWarehouseId = usedPartsReturnOrder.OutboundWarehouseId;
            usedPartsReturnOrder.PropertyChanged -= usedPartsReturnOrder_PropertyChanged;
            usedPartsReturnOrder.PropertyChanged += usedPartsReturnOrder_PropertyChanged;
            ReturnTypeChecked(usedPartsReturnOrder.ReturnOfficeId, usedPartsReturnOrder.ReturnOfficeName);
        }

        private int OutboundWarehouseId = 0;
        private void usedPartsReturnOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            switch(e.PropertyName) {
                case "OutboundWarehouseId":
                    if(usedPartsReturnOrder.UsedPartsReturnDetails.Any()) {
                        if(usedPartsReturnOrder.OutboundWarehouseId != this.OutboundWarehouseId) {
                            DcsUtils.Confirm(ServiceUIStrings.DataPanel_Confirm_OutboundWarehouseHasChangedWillClearDetails, () => {
                                var dcsDomainContext = new DcsDomainContext();
                                dcsDomainContext.Load(dcsDomainContext.GetUsedPartsWarehousesQuery().Where(entity => entity.Id == usedPartsReturnOrder.OutboundWarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                                    if(loadOp.HasError)
                                        return;
                                    var entity = loadOp.Entities.SingleOrDefault();
                                    if(entity == null)
                                        return;
                                    usedPartsReturnOrder.OutboundWarehouseCode = entity.Code;
                                    usedPartsReturnOrder.OutboundWarehouseName = entity.Name;
                                }, null);
                                foreach(var usedPartsReturnDetail in usedPartsReturnOrder.UsedPartsReturnDetails) {
                                    usedPartsReturnOrder.ValidationErrors.Clear();
                                    usedPartsReturnOrder.UsedPartsReturnDetails.Remove(usedPartsReturnDetail);
                                }
                            }, () => {
                                usedPartsReturnOrder.OutboundWarehouseId = this.OutboundWarehouseId;
                            });
                        }
                    } else {
                        this.OutboundWarehouseId = usedPartsReturnOrder.OutboundWarehouseId;
                        var dcsDomainContext = new DcsDomainContext();
                        dcsDomainContext.Load(dcsDomainContext.GetUsedPartsWarehousesQuery().Where(entity => entity.Id == usedPartsReturnOrder.OutboundWarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError)
                                return;
                            var entity = loadOp.Entities.SingleOrDefault();
                            if(entity == null)
                                return;
                            usedPartsReturnOrder.OutboundWarehouseCode = entity.Code;
                            usedPartsReturnOrder.OutboundWarehouseName = entity.Name;
                        }, null);
                    }
                    // ClearUsedPartsReturnOrderDetails();
                    break;
            }
        }

        private void ClearUsedPartsReturnOrderDetails() {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetUsedPartsWarehousesQuery().Where(entity => entity.Id == usedPartsReturnOrder.OutboundWarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                usedPartsReturnOrder.OutboundWarehouseCode = entity.Code;
                usedPartsReturnOrder.OutboundWarehouseName = entity.Name;
            }, null);
            if(usedPartsReturnOrder.UsedPartsReturnDetails == null || !usedPartsReturnOrder.UsedPartsReturnDetails.Any())
                return;
            //DcsUtils.Confirm(ServiceUIStrings.DataPanel_Confirm_OutboundWarehouseHasChangedWillClearDetails, () => {
            foreach(var usedPartsReturnDetail in usedPartsReturnOrder.UsedPartsReturnDetails) {
                usedPartsReturnOrder.ValidationErrors.Clear();
                usedPartsReturnOrder.UsedPartsReturnDetails.Remove(usedPartsReturnDetail);
            }
            //});
        }

        public ObservableCollection<KeyValuePair> KvOriginWarehouseName {
            get {
                return this.kvOriginWarehouseName ?? (this.kvOriginWarehouseName = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetDepartmentInformationsQuery().Where(v => v.Status == (int)DcsMasterDataStatus.有效 && v.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvDepartmentInformations.Clear();
                foreach(var departmentInformation in loadOp.Entities) {
                    this.KvDepartmentInformations.Add(new KeyValuePair {
                        Key = departmentInformation.Id,
                        Value = departmentInformation.Name,
                        UserObject = departmentInformation
                    });
                }
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetUsedPartsWarehouseStaffByPersonnelIdQuery().Where(e => e.PersonnelId == BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var usedWarehouse in loadOp.Entities)
                    this.KvOriginWarehouseName.Add(new KeyValuePair {
                        Key = usedWarehouse.UsedPartsWarehouse.Id,
                        Value = usedWarehouse.UsedPartsWarehouse.Name
                    });
            }, null);
            var responsibleUnit = DI.GetQueryWindow("ResponsibleUnit");
            responsibleUnit.SelectionDecided -= this.UsedPartsReturnOrderResponsibleUnit_SelectionDecided;
            responsibleUnit.SelectionDecided += this.UsedPartsReturnOrderResponsibleUnit_SelectionDecided;
            this.ptbUsedPartsResponsibleUnitDesti.PopupContent = responsibleUnit;

            var partsSupplier = DI.GetQueryWindow("PartsSupplier");
            partsSupplier.SelectionDecided -= this.UsedPartsReturnOrderPartsSupplier_SelectionDecided;
            partsSupplier.SelectionDecided += this.UsedPartsReturnOrderPartsSupplier_SelectionDecided;
            this.ptbUsedPartsSupplierDesti.PopupContent = partsSupplier;
        }

        private void ReturnTypeChecked(int id, string name) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            switch(usedPartsReturnOrder.ReturnType) {
                case (int)DcsUsedPartsReturnOrderReturnType.清退责任单位:
                    this.ptbUsedPartsSupplierDesti.IsEnabled = false;
                    this.cbDepartmentInformation.IsEnabled = false;
                    this.ResponsibleUnitName = string.IsNullOrEmpty(name) ? string.Empty : name;
                    this.DepartmentInformationId = null;
                    this.PartsSupplierName = string.Empty;
                    break;
                case (int)DcsUsedPartsReturnOrderReturnType.清退配件供应商:
                    this.ptbUsedPartsResponsibleUnitDesti.IsEnabled = false;
                    this.cbDepartmentInformation.IsEnabled = false;
                    this.PartsSupplierName = string.IsNullOrEmpty(name) ? string.Empty : name;
                    this.DepartmentInformationId = null;
                    this.ResponsibleUnitName = string.Empty;
                    break;
                case (int)DcsUsedPartsReturnOrderReturnType.内部部门:
                    this.ptbUsedPartsResponsibleUnitDesti.IsEnabled = false;
                    this.ptbUsedPartsSupplierDesti.IsEnabled = false;
                    this.DepartmentInformationId = string.IsNullOrEmpty(name) ? default(int) : id;
                    this.ResponsibleUnitName = string.Empty;
                    this.PartsSupplierName = string.Empty;
                    break;
                default:
                    this.cbDepartmentInformation.IsEnabled = true;
                    this.ptbUsedPartsResponsibleUnitDesti.IsEnabled = true;
                    this.ptbUsedPartsSupplierDesti.IsEnabled = true;
                    break;
            }
        }

        private void DepartmentInformationDcsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            var temp = sender as RadComboBox;
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(temp != null && usedPartsReturnOrder != null) {
                var departmentInformation = temp.SelectedItem as KeyValuePair;
                if(departmentInformation != null) {
                    usedPartsReturnOrder.ReturnOfficeId = ((DepartmentInformation)departmentInformation.UserObject).Id;
                    usedPartsReturnOrder.ReturnOfficeCode = ((DepartmentInformation)departmentInformation.UserObject).Code;
                    usedPartsReturnOrder.ReturnOfficeName = ((DepartmentInformation)departmentInformation.UserObject).Name;
                    usedPartsReturnOrder.ReturnType = (int)DcsUsedPartsReturnOrderReturnType.内部部门;
                    this.DepartmentInformationId = usedPartsReturnOrder.ReturnOfficeId;
                }
            }
            if(usedPartsReturnOrder != null)
                this.ReturnTypeChecked(usedPartsReturnOrder.ReturnOfficeId, usedPartsReturnOrder.ReturnOfficeName);
        }

        private void UsedPartsReturnOrderPartsSupplier_SelectionDecided(object sender, EventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;

            var usedpartsreturnorder = this.DataContext as UsedPartsReturnOrder;
            if(usedpartsreturnorder == null)
                return;
            usedpartsreturnorder.ReturnOfficeId = partsSupplier.Id;
            usedpartsreturnorder.ReturnOfficeCode = partsSupplier.Code;
            usedpartsreturnorder.ReturnOfficeName = partsSupplier.Name;
            usedpartsreturnorder.ReturnType = (int)DcsUsedPartsReturnOrderReturnType.清退配件供应商;
            this.PartsSupplierName = partsSupplier.Name;
            ReturnTypeChecked(usedPartsReturnOrder.ReturnOfficeId, usedPartsReturnOrder.ReturnOfficeName);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void UsedPartsReturnOrderResponsibleUnit_SelectionDecided(object sender, EventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var responsibleUnit = queryWindow.SelectedEntities.Cast<ResponsibleUnit>().FirstOrDefault();
            if(responsibleUnit == null)
                return;

            var usedpartsreturnorder = this.DataContext as UsedPartsReturnOrder;
            if(usedpartsreturnorder == null)
                return;
            usedpartsreturnorder.ReturnOfficeId = responsibleUnit.Id;
            usedpartsreturnorder.ReturnOfficeCode = responsibleUnit.Code;
            usedpartsreturnorder.ReturnOfficeName = responsibleUnit.Name;
            usedpartsreturnorder.ReturnType = (int)DcsUsedPartsReturnOrderReturnType.清退责任单位;
            this.ResponsibleUnitName = responsibleUnit.Name;
            ReturnTypeChecked(usedPartsReturnOrder.ReturnOfficeId, usedPartsReturnOrder.ReturnOfficeName);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        //清空选择的数据后弹框恢复可用状态
        private void PtbUsedPartsResponsibleUnitDesti_OnKeyDown(object sender, KeyEventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            if(this.ResponsibleUnitName == "") {
                usedPartsReturnOrder.ReturnOfficeId = 0;
                usedPartsReturnOrder.ReturnOfficeCode = null;
                usedPartsReturnOrder.ReturnOfficeName = null;
                this.cbDepartmentInformation.IsEnabled = true;
                this.ptbUsedPartsSupplierDesti.IsEnabled = true;
            }
        }

        private void PtbUsedPartsSupplierDesti_OnKeyDown(object sender, KeyEventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            if(this.PartsSupplierName == "") {
                usedPartsReturnOrder.ReturnOfficeId = 0;
                usedPartsReturnOrder.ReturnOfficeCode = null;
                usedPartsReturnOrder.ReturnOfficeName = null;
                this.cbDepartmentInformation.IsEnabled = true;
                this.ptbUsedPartsResponsibleUnitDesti.IsEnabled = true;
            }
        }

        private void CbDepartmentInformation_OnKeyDown(object sender, KeyEventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            if(this.PartsSupplierName == "") {
                usedPartsReturnOrder.ReturnOfficeId = 0;
                usedPartsReturnOrder.ReturnOfficeCode = null;
                usedPartsReturnOrder.ReturnOfficeName = null;
                this.ptbUsedPartsSupplierDesti.IsEnabled = true;
                this.ptbUsedPartsResponsibleUnitDesti.IsEnabled = true;
            }
        }
    }
}
