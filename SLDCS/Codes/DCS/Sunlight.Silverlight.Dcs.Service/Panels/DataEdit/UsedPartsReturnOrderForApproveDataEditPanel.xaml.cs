﻿
using System.ComponentModel;
using System.Windows;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class UsedPartsReturnOrderForApproveDataEditPanel : INotifyPropertyChanged {

        public event PropertyChangedEventHandler PropertyChanged;
        private string departmentInformationName;
        private string responsibleUnitName;
        private string partsSupplierName;


        public UsedPartsReturnOrderForApproveDataEditPanel() {
            this.InitializeComponent();
            this.DataContextChanged += this.UsedPartsReturnOrderDataEditPanel_DataContextChanged;
        }

        public string DepartmentInformationName {
            get {
                return this.departmentInformationName;
            }
            set {
                this.departmentInformationName = value;
                this.OnPropertyChanged("DepartmentInformationName");
            }
        }

        public string ResponsibleUnitName {
            get {
                return this.responsibleUnitName;
            }
            set {
                this.responsibleUnitName = value;
                this.OnPropertyChanged("ResponsibleUnitName");
            }
        }

        public string PartsSupplierName {
            get {
                return this.partsSupplierName;
            }
            set {
                this.partsSupplierName = value;
                this.OnPropertyChanged("PartsSupplierName");
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private void UsedPartsReturnOrderDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            ReturnTypeChecked(usedPartsReturnOrder.ReturnType, usedPartsReturnOrder.ReturnOfficeName);
        }

        private void ReturnTypeChecked(int returnType, string value) {
            switch(returnType) {
                case (int)DcsUsedPartsReturnOrderReturnType.清退责任单位:
                    this.ResponsibleUnitName = value;
                    this.PartsSupplierName = string.Empty;
                    this.DepartmentInformationName = string.Empty;
                    break;
                case (int)DcsUsedPartsReturnOrderReturnType.清退配件供应商:
                    this.PartsSupplierName = value;
                    this.ResponsibleUnitName = string.Empty;
                    this.DepartmentInformationName = string.Empty;
                    break;
                case (int)DcsUsedPartsReturnOrderReturnType.内部部门:
                    this.DepartmentInformationName = value;
                    this.ResponsibleUnitName = string.Empty;
                    this.PartsSupplierName = string.Empty;
                    break;
            }
        }
    }
}