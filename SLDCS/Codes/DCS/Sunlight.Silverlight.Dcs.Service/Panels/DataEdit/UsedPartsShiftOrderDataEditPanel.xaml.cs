﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class UsedPartsShiftOrderDataEditPanel {
        private ObservableCollection<KeyValuePair> kvUsedPartsWarehouseName;

        public ObservableCollection<KeyValuePair> KvUsedPartsWarehouseName {
            get {
                return this.kvUsedPartsWarehouseName ?? (this.kvUsedPartsWarehouseName = new ObservableCollection<KeyValuePair>());
            }
        }

        public UsedPartsShiftOrderDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += UsedPartsShiftOrderDataEditPanel_DataContextChanged;
        }

        private void UsedPartsShiftOrderDataEditPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var usedPartsShiftOrder = this.DataContext as UsedPartsShiftOrder;
            if(usedPartsShiftOrder == null)
                return;
            this.usedPartsWarehouseId = usedPartsShiftOrder.UsedPartsWarehouseId;
            usedPartsShiftOrder.PropertyChanged -= usedPartsShiftOrder_PropertyChanged;
            usedPartsShiftOrder.PropertyChanged += usedPartsShiftOrder_PropertyChanged;
        }

        private int usedPartsWarehouseId = 0;
        private void usedPartsShiftOrder_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var usedPartsShiftOrder = this.DataContext as UsedPartsShiftOrder;
            if(usedPartsShiftOrder == null)
                return;
            switch(e.PropertyName) {
                case "UsedPartsWarehouseId":
                    if(usedPartsShiftOrder.UsedPartsShiftDetails.Any()) {
                        if(usedPartsShiftOrder.UsedPartsWarehouseId != this.usedPartsWarehouseId) {
                            DcsUtils.Confirm(ServiceUIStrings.DataEditPanel_Confirm_Clear, () => {
                                var domainContext = new DcsDomainContext();
                                this.usedPartsWarehouseId = usedPartsShiftOrder.UsedPartsWarehouseId;
                                domainContext.Load(domainContext.GetUsedPartsWarehousesQuery().Where(ex => ex.Id == usedPartsShiftOrder.UsedPartsWarehouseId), loadOp => {
                                    if(loadOp.HasError)
                                        return;
                                    var entity = loadOp.Entities.FirstOrDefault();
                                    if(entity == null)
                                        return;
                                    usedPartsShiftOrder.UsedPartsWarehouseId = entity.Id;
                                    usedPartsShiftOrder.UsedPartsWarehouseCode = entity.Code;
                                    usedPartsShiftOrder.UsedPartsWarehouseName = entity.Name;
                                }, null);
                                foreach(var detail in usedPartsShiftOrder.UsedPartsShiftDetails) {
                                    usedPartsShiftOrder.ValidationErrors.Clear();
                                    usedPartsShiftOrder.UsedPartsShiftDetails.Remove(detail);
                                }
                            }, () => {
                                usedPartsShiftOrder.UsedPartsWarehouseId = this.usedPartsWarehouseId;
                            });
                        }
                    } else {
                        this.usedPartsWarehouseId = usedPartsShiftOrder.UsedPartsWarehouseId;
                        var domainContext = new DcsDomainContext();
                        domainContext.Load(domainContext.GetUsedPartsWarehousesQuery().Where(ex => ex.Id == usedPartsShiftOrder.UsedPartsWarehouseId), loadOp => {
                            if(loadOp.HasError)
                                return;
                            var entity = loadOp.Entities.FirstOrDefault();
                            if(entity == null)
                                return;
                            usedPartsShiftOrder.UsedPartsWarehouseId = entity.Id;
                            usedPartsShiftOrder.UsedPartsWarehouseCode = entity.Code;
                            usedPartsShiftOrder.UsedPartsWarehouseName = entity.Name;
                        }, null);
                    }
                    //ClearUsedPartsShiftDetails();
                    break;
            }
        }

        private void ClearUsedPartsShiftDetails() {
            var usedPartsShiftOrder = this.DataContext as UsedPartsShiftOrder;
            if(usedPartsShiftOrder == null)
                return;
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetUsedPartsWarehousesQuery().Where(ex => ex.Id == usedPartsShiftOrder.UsedPartsWarehouseId), loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                usedPartsShiftOrder.UsedPartsWarehouseId = entity.Id;
                usedPartsShiftOrder.UsedPartsWarehouseCode = entity.Code;
                usedPartsShiftOrder.UsedPartsWarehouseName = entity.Name;
            }, null);
            if(usedPartsShiftOrder.UsedPartsShiftDetails == null || usedPartsShiftOrder.UsedPartsShiftDetails.Any()) {
                DcsUtils.Confirm(ServiceUIStrings.DataEditPanel_Confirm_Clear, () => {
                    foreach(var detail in usedPartsShiftOrder.UsedPartsShiftDetails) {
                        usedPartsShiftOrder.ValidationErrors.Clear();
                        usedPartsShiftOrder.UsedPartsShiftDetails.Remove(detail);
                    }
                });
            }
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetUsedPartsWarehouseStaffByPersonnelIdQuery().Where(e => e.PersonnelId == BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var usedWarehouse in loadOp.Entities)
                    this.KvUsedPartsWarehouseName.Add(new KeyValuePair {
                        Key = usedWarehouse.UsedPartsWarehouse.Id,
                        Value = usedWarehouse.UsedPartsWarehouse.Name
                    });
            }, null);
        }
    }
}
