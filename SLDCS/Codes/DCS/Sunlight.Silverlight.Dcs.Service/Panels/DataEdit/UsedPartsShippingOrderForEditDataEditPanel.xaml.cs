﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class UsedPartsShippingOrderForEditDataEditPanel {
        private ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvUsedPartsWarehouses = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvPartsSalesCategory = new ObservableCollection<KeyValuePair>();
        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        private readonly string[] kvNames = {
            "PartsShipping_Method", "UsedPartsShippingOrder_Dispatcher"
        };
        public UsedPartsShippingOrderForEditDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.UsedPartsShippingOrderDataEditPanel_DataContextChanged;
        }

        private void UsedPartsShippingOrderDataEditPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            usedPartsShippingOrder.PropertyChanged += this.UsedPartsShippingOrder_PropertyChanged;
        }

        private void UsedPartsShippingOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            switch(e.PropertyName) {
                case "BranchId":
                    var branchInfo = this.KvBranches.SingleOrDefault(entity => entity.Key == usedPartsShippingOrder.BranchId);
                    if(branchInfo == null || !(branchInfo.UserObject is Branch))
                        return;
                    var branch = branchInfo.UserObject as Branch;
                    usedPartsShippingOrder.BranchCode = branch.Code;
                    usedPartsShippingOrder.BranchName = branch.Name;
                    usedPartsShippingOrder.PartsSalesCategoryId = 0;
                    domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.BranchId == branch.Id), loadOp1 => {
                        if(loadOp1.HasError)
                            return;
                        this.kvPartsSalesCategory.Clear();
                        this.KvUsedPartsWarehouses.Clear();
                        foreach(var partsSalesCategory in loadOp1.Entities)
                            this.kvPartsSalesCategory.Add(new KeyValuePair {
                                Key = partsSalesCategory.Id,
                                Value = partsSalesCategory.Name,
                                UserObject = partsSalesCategory
                            });
                        this.cbxPartsSalesCategory.ItemsSource = loadOp1.Entities;
                    }, null);
                    break;
                case "PartsSalesCategoryId":
                    usedPartsShippingOrder.DestinationWarehouseId = 0;
                    domainContext.Load(domainContext.旧件发运查询旧件仓库Query(usedPartsShippingOrder.PartsSalesCategoryId), loadOp => {
                        if(loadOp.HasError)
                            return;
                        this.KvUsedPartsWarehouses.Clear();
                        foreach(var usedPartsWarehouse in loadOp.Entities)
                            this.KvUsedPartsWarehouses.Add(new KeyValuePair {
                                Key = usedPartsWarehouse.Id,
                                Value = usedPartsWarehouse.Name,
                                UserObject = usedPartsWarehouse
                            });
                    }, null);
                    break;
            }
        }
        private void CreateUI() {
            domainContext.Load(domainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvBranches.Clear();
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                        UserObject = branch
                    });
            }, null);
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError)
                    return;
                this.kvPartsSalesCategory.Clear();
                foreach(var partsSalesCategory in loadOp1.Entities)
                    this.kvPartsSalesCategory.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name,
                        UserObject = partsSalesCategory
                    });
                this.cbxPartsSalesCategory.ItemsSource = loadOp1.Entities;
            }, null);
            domainContext.Load(domainContext.GetUsedPartsWarehousesQuery().Where(ex => ex.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp2 => {
                if(loadOp2.HasError)
                    return;
                this.KvUsedPartsWarehouses.Clear();
                foreach(var usedPartsWarehouse in loadOp2.Entities)
                    this.KvUsedPartsWarehouses.Add(new KeyValuePair {
                        Key = usedPartsWarehouse.Id,
                        Value = usedPartsWarehouse.Name,
                        UserObject = usedPartsWarehouse
                    });
            }, null);
            var queryWindowCompany = DI.GetQueryWindow("LogisticCompany");
            queryWindowCompany.Loaded += this.QueryWindowCompany_Loaded;
            queryWindowCompany.SelectionDecided += this.QueryWindowCompany_SelectionDecided;
            this.ptbLogisticCompanyCode.PopupContent = queryWindowCompany;
        }

        private void QueryWindowCompany_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(queryWindow == null || usedPartsShippingOrder == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = usedPartsShippingOrder.BranchId
            });
        }

        private void QueryWindowCompany_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var logisticCompanyServiceRange = queryWindow.SelectedEntities.Cast<LogisticCompanyServiceRange>().FirstOrDefault();
            if(logisticCompanyServiceRange == null || logisticCompanyServiceRange.LogisticCompany == null)
                return;

            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;

            usedPartsShippingOrder.LogisticCompanyId = logisticCompanyServiceRange.LogisticCompany.Id;
            usedPartsShippingOrder.LogisticCompanyCode = logisticCompanyServiceRange.LogisticCompany.Code;
            usedPartsShippingOrder.LogisticCompanyName = logisticCompanyServiceRange.LogisticCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches;
            }
        }

        public ObservableCollection<KeyValuePair> KvUsedPartsWarehouses {
            get {
                return this.kvUsedPartsWarehouses;
            }
        }
        public ObservableCollection<KeyValuePair> KvPartsSalesCategory {
            get {
                return this.kvPartsSalesCategory;
            }
        }

        public object KvShippingMethods {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvDispatchers {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }
    }
}
