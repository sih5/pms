﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class UsedPartsTransferOrderDataEditPanel {
        private ObservableCollection<KeyValuePair> kvOriginWarehouseName;

        public UsedPartsTransferOrderDataEditPanel() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += UsedPartsTransferOrderDataEditPanel_DataContextChanged;
        }

        private void UsedPartsTransferOrderDataEditPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var usedPartsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            if(usedPartsTransferOrder == null)
                return;
            usedPartsTransferOrder.PropertyChanged -= usedPartsTransferOrder_PropertyChanged;
            usedPartsTransferOrder.PropertyChanged += usedPartsTransferOrder_PropertyChanged;
        }

        private void usedPartsTransferOrder_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var usedPartsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            if(usedPartsTransferOrder == null)
                return;
            switch(e.PropertyName) {
                case "OriginWarehouseId":
                    ClearUsedPartsTransferDetails();
                    break;
            }
        }

        private void ClearUsedPartsTransferDetails() {
            var usedPartsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            if(usedPartsTransferOrder == null)
                return;
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetUsedPartsWarehousesQuery().Where(ex => ex.Id == usedPartsTransferOrder.OriginWarehouseId), loadOp => {
                if(loadOp.HasError)
                    return;
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                usedPartsTransferOrder.OriginWarehouseId = entity.Id;
                usedPartsTransferOrder.OriginWarehouseCode = entity.Code;
                usedPartsTransferOrder.OriginWarehouseName = entity.Name;
            }, null);
            if(usedPartsTransferOrder.UsedPartsTransferDetails == null || !usedPartsTransferOrder.UsedPartsTransferDetails.Any())
                return;
            DcsUtils.Confirm(ServiceUIStrings.DataEditPanel_Confirm_Clear, () => {
                foreach(var usedPartsTransferDetail in usedPartsTransferOrder.UsedPartsTransferDetails) {
                    usedPartsTransferOrder.ValidationErrors.Clear();
                    usedPartsTransferOrder.UsedPartsTransferDetails.Remove(usedPartsTransferDetail);
                }
            });
        }

        public ObservableCollection<KeyValuePair> KvOriginWarehouseName {
            get {
                return this.kvOriginWarehouseName ?? (this.kvOriginWarehouseName = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetUsedPartsWarehouseStaffByPersonnelIdQuery().Where(e => e.PersonnelId == BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var usedWarehouse in loadOp.Entities)
                    this.KvOriginWarehouseName.Add(new KeyValuePair {
                        Key = usedWarehouse.UsedPartsWarehouse.Id,
                        Value = usedWarehouse.UsedPartsWarehouse.Name
                    });
            }, null);
            //var queryWindowUsedPartsWarehouse = DI.GetQueryWindow("UsedPartsWarehouse");
            //queryWindowUsedPartsWarehouse.SelectionDecided -= this.UsedPartsTransferOrderOriginWarehouseCode_SelectionDecided;
            //queryWindowUsedPartsWarehouse.SelectionDecided += this.UsedPartsTransferOrderOriginWarehouseCode_SelectionDecided;
            ////this.ptbUsedPartsWarehouseOrigin.PopupContent = queryWindowUsedPartsWarehouse;

            var usedPartsWarehouse = DI.GetQueryWindow("UsedPartsWarehouse");
            usedPartsWarehouse.SelectionDecided -= this.UsedPartsTransferOrderDestinationWarehouseName_SelectionDecided;
            usedPartsWarehouse.SelectionDecided += this.UsedPartsTransferOrderDestinationWarehouseName_SelectionDecided;
            this.ptbUsedPartsWarehouseDesti.PopupContent = usedPartsWarehouse;
        }

        private void UsedPartsTransferOrderOriginWarehouseCode_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var usedPartsWarehouse = queryWindow.SelectedEntities.Cast<UsedPartsWarehouse>().FirstOrDefault();
            if(usedPartsWarehouse == null)
                return;

            var usedPartsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            if(usedPartsTransferOrder == null)
                return;
            if(usedPartsTransferOrder.OriginWarehouseId != default(int) && usedPartsTransferOrder.UsedPartsTransferDetails.Any() && usedPartsTransferOrder.OriginWarehouseId != usedPartsWarehouse.Id) {
                DcsUtils.Confirm(ServiceUIStrings.DataPanel_Confirm_UsedPartsWarehouseHasChangedWillClearDetails, () => {
                    usedPartsTransferOrder.OriginWarehouseId = usedPartsWarehouse.Id;
                    usedPartsTransferOrder.OriginWarehouseCode = usedPartsWarehouse.Code;
                    usedPartsTransferOrder.OriginWarehouseName = usedPartsWarehouse.Name;
                    foreach(var usedPartsTransferDetail in usedPartsTransferOrder.UsedPartsTransferDetails)
                        usedPartsTransferOrder.UsedPartsTransferDetails.Remove(usedPartsTransferDetail);
                });
            } else {
                usedPartsTransferOrder.OriginWarehouseId = usedPartsWarehouse.Id;
                usedPartsTransferOrder.OriginWarehouseCode = usedPartsWarehouse.Code;
                usedPartsTransferOrder.OriginWarehouseName = usedPartsWarehouse.Name;
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void UsedPartsTransferOrderDestinationWarehouseName_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var usedPartsWarehouse = queryWindow.SelectedEntities.Cast<UsedPartsWarehouse>().FirstOrDefault();
            if(usedPartsWarehouse == null)
                return;

            var usedPartsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            if(usedPartsTransferOrder == null)
                return;

            if(usedPartsTransferOrder.DestinationWarehouseId != default(int) && usedPartsTransferOrder.UsedPartsTransferDetails.Any() && usedPartsTransferOrder.DestinationWarehouseId != usedPartsWarehouse.Id) {
                DcsUtils.Confirm(ServiceUIStrings.DataPanel_Confirm_UsedPartsWarehouseHasChangedWillClearDetails, () => {
                    usedPartsTransferOrder.DestinationWarehouseId = usedPartsWarehouse.Id;
                    usedPartsTransferOrder.DestinationWarehouseCode = usedPartsWarehouse.Code;
                    usedPartsTransferOrder.DestinationWarehouseName = usedPartsWarehouse.Name;
                    foreach(var usedPartsTransferDetail in usedPartsTransferOrder.UsedPartsTransferDetails)
                        usedPartsTransferOrder.UsedPartsTransferDetails.Remove(usedPartsTransferDetail);
                });
            } else {
                usedPartsTransferOrder.DestinationWarehouseId = usedPartsWarehouse.Id;
                usedPartsTransferOrder.DestinationWarehouseCode = usedPartsWarehouse.Code;
                usedPartsTransferOrder.DestinationWarehouseName = usedPartsWarehouse.Name;
            }

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
