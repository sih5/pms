﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Dcs.View.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows;
using Telerik.Windows.Controls.DragDrop;

namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class UsedPartsWarehouseAreaDataEditPanel {
        private DataItemCollection usedPartsWarehouseAreaIdTreeItems;
        public event TreeViewDoubleClick OnTreeViewDoubleClick;
        public event TreeViewOnChecked OnTreeViewChecked;
        public event TreeViewOnItemClick OnTreeViewItemClick;
        public event TreeViewDataLoaded OnTreeViewDataLoaded;
        private int selectedDataItemId;

        public DependencyObject TreeViewDataExchangeDestination {
            get;
            set;
        }

        public delegate void TreeViewDropQuery(object sender, DragDropQueryEventArgs e);

        public delegate void TreeViewDropInfo(object sender, DragDropEventArgs e);

        public delegate void TreeViewDoubleClick(object sender, RadRoutedEventArgs e, DependencyObject destination);

        public delegate void TreeViewOnChecked(object sender, RadRoutedEventArgs e);

        public delegate void TreeViewOnItemClick(object sender, RadRoutedEventArgs e);

        public delegate void TreeViewOnUnchecked(object sender, RadRoutedEventArgs e);

        public delegate void TreeViewDataLoaded();

        public void ResfrashDataTree() {
            this.Initialize();
        }

        public bool EnableDragDrop {
            get;
            set;
        }

        private Visibility isVisibility = Visibility.Collapsed;

        /// <summary>
        /// 查询数据库用的DomainContext 外部传入
        /// </summary>
        public DcsDomainContext DomainContext {
            private get;
            set;
        }

        /// <summary>
        /// 是否有复选框
        /// </summary>
        public bool IsOptionElements {
            get;
            set;
        }

        /// <summary>
        ///     是否显示 过滤查询框
        /// </summary>
        public Visibility IsVisibility {
            get {
                return this.isVisibility;
            }
            private set {
                this.isVisibility = value;
            }
        }

        /// <summary>
        /// 启用过滤查询框
        /// </summary>
        public void EnableSearchTextBox() {
            this.IsVisibility = Visibility.Visible;
        }

        public int SelectedDataItemId {
            get {
                return this.selectedDataItemId;
            }
            set {
                this.selectedDataItemId = value;
                if(value != default(int) && this.usedPartsWarehouseAreaIdTreeItems != null && this.usedPartsWarehouseAreaIdTreeItems.Any()) {
                    var selectedItem = this.usedPartsWarehouseAreaIdTreeItems.FirstOrDefault(item => item.Id == value);
                    if(selectedItem != null) {
                        this.UsedPartsWarehouseAreaTreeView.SelectedItem = selectedItem;
                        if(!this.IsOptionElements) {
                            this.UsedPartsWarehouseAreaTreeView.ExpandAll();
                        }
                    }
                }
            }
        }

        public DataItem SelectedDataItem {
            get {
                return this.UsedPartsWarehouseAreaTreeView.SelectedItem as DataItem;
            }
        }

        public UsedPartsWarehouseAreaDataEditPanel() {
            this.InitializeComponent();
            this.Initializer.Register(this.Initialize);
            this.Loaded += this.TreeViewCollapseNode;
        }

        public DataItem GetUsedPartsWarehouseAreaInfoById(int usedPartsWarehouseAreaId) {
            if(usedPartsWarehouseAreaId != default(int) && this.usedPartsWarehouseAreaIdTreeItems != null && this.usedPartsWarehouseAreaIdTreeItems.Any())
                return this.usedPartsWarehouseAreaIdTreeItems.FirstOrDefault(item => item.Id == usedPartsWarehouseAreaId);
            return null;
        }

        /// <summary>
        /// 界面加载完成后，判断是否启用了Option框,没有启用的话，收起所有节点
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="routedEventArgs"> </param>
        private void TreeViewCollapseNode(object sender, RoutedEventArgs routedEventArgs) {
            if(!this.IsOptionElements)
                this.UsedPartsWarehouseAreaTreeView.CollapseAll();
        }

        /// <summary>
        /// 初始化填充车型树
        /// </summary>
        private void Initialize() {
            //get Include服务端关系
            this.DomainContext.Load(this.DomainContext.GetUsedPartsWarehouseAreasWithDetailsQuery().Where(area => area.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent
                , loadOp => {
                    this.usedPartsWarehouseAreaIdTreeItems = new DataItemCollection();
                    foreach(var usedPartsWarehouseArea in loadOp.Entities)
                        this.usedPartsWarehouseAreaIdTreeItems.Add(new DataItem {
                            Id = usedPartsWarehouseArea.Id,
                            ParentId = usedPartsWarehouseArea.ParentId.HasValue ? usedPartsWarehouseArea.ParentId.Value : 0,
                            Text = usedPartsWarehouseArea.Code,
                            Entity = usedPartsWarehouseArea
                        });
                    this.usedPartsWarehouseAreaIdTreeItems.Sort(item => item.Id, ListSortDirection.Ascending);
                    this.UsedPartsWarehouseAreaTreeView.ItemsSource = this.usedPartsWarehouseAreaIdTreeItems.Where(item => item.ParentId == 0);
                    if(this.OnTreeViewDataLoaded != null)
                        this.OnTreeViewDataLoaded();
                }, null);
            if(this.EnableDragDrop && this.TreeViewDataExchangeDestination != null) {
                RadDragAndDropManager.SetAllowDrop(this.TreeViewDataExchangeDestination, this.EnableDragDrop);
            }
            this.UsedPartsWarehouseAreaTreeView.ItemDoubleClick += this.UsedPartsWarehouseManagerDataGridView_DoubleClick;
            this.UsedPartsWarehouseAreaTreeView.Checked += this.UsedPartsWarehouseManagerDataGridView_Checked;
            this.UsedPartsWarehouseAreaTreeView.ItemClick += this.UsedPartsWarehouseManagerDataGridView_ItemClick;
        }

        private void UsedPartsWarehouseManagerDataGridView_Checked(object sender, RadRoutedEventArgs e) {
            if(this.OnTreeViewChecked != null)
                this.OnTreeViewChecked(sender, e);
        }

        private void UsedPartsWarehouseAreaTreeFilterSearchTextChanged(object sender, EventArgs e) {
            var searchText = this.txtUsedPartsWarehouseAreaTreeViewFilter.SearchText;
            var usedPartsWarehouseAreaCollection = this.usedPartsWarehouseAreaIdTreeItems.Search(searchText);
            usedPartsWarehouseAreaCollection.Sort(item => item.Id, ListSortDirection.Ascending);
            this.UsedPartsWarehouseAreaTreeView.ItemsSource = usedPartsWarehouseAreaCollection.Where(item => item.ParentId == 0);
        }

        private void UsedPartsWarehouseManagerDataGridView_DoubleClick(object sender, RadRoutedEventArgs e) {
            if(this.OnTreeViewDoubleClick != null)
                this.OnTreeViewDoubleClick(sender, e, this.TreeViewDataExchangeDestination);
        }

        private void UsedPartsWarehouseManagerDataGridView_ItemClick(object sender, RadRoutedEventArgs e) {
            if(this.OnTreeViewItemClick != null)
                this.OnTreeViewItemClick(sender, e);
        }
    }
}
