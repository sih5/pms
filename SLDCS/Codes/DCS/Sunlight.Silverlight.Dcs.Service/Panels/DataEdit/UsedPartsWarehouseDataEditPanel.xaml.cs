﻿
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.DataEdit {
    public partial class UsedPartsWarehouseDataEditPanel {
        private readonly string[] kvNames = {
             "Storage_Strategy"
        };

        public UsedPartsWarehouseDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && BaseApp.Current.CurrentUserData.EnterpriseId == e.BranchId), loadOp => {
                if(loadOp.HasError)
                    return;
                this.cbxPartsSalesCategory.ItemsSource = loadOp.Entities;
            }, null);
        }
        public object KvStoragePolicies {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
