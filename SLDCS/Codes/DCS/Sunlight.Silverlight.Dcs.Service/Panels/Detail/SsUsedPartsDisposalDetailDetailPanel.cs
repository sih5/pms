﻿using System;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Service.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Detail {
    public class SsUsedPartsDisposalDetailDetailPanel : SsUsedPartsDisposalDetailDataGridView, IDetailPanel {
        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return ServiceUIStrings.SsUsedPartsDisposalBill_SsUsedPartsDisposalDetails;
            }
        }
    }
}
