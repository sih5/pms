﻿using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Detail {
    public partial class SsUsedPartsStorageForBranchDetailPanel {
        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy"
        };

        public SsUsedPartsStorageForBranchDetailPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return ServiceUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
