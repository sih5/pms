﻿using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Service.Views.DataGrid;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Detail {
    public class UsedPartsInboundDetailDetailPanel : UsedPartsInboundDetailDataGridView, IDetailPanel {

        public System.Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return ServiceUIStrings.DetailPanel_Title_UsedPartsInboundDetail;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "N2";
        }
    }
}
