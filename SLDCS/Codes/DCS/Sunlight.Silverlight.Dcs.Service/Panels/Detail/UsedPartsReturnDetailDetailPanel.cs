﻿using System;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Service.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Detail {
    public class UsedPartsReturnDetailDetailPanel : UsedPartsReturnDetailDataGridView, IDetailPanel {

        public string Title {
            get {
                return Utils.GetEntityLocalizedName(typeof(UsedPartsReturnOrder), "UsedPartsReturnDetails");
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
