﻿using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Detail {
    public partial class UsedPartsReturnPolicyHistoryDetailPanel {
        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy"
        };

        public UsedPartsReturnPolicyHistoryDetailPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return ServiceUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
