﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Service.Views.DataGrid;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Detail {
    public class UsedPartsShippingDetailDetailPanel : UsedPartsShippingDetailDataGridView, IDetailPanel {

        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return ServiceUIStrings.DetailPanel_Title_UsedPartsShippingDetail;
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["UnitPrice"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["UsedPartsEncourageAmount"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["PartsManagementCost"]).DataFormatString = "N2";
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
    }
}
