﻿using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Detail {
    public partial class UsedPartsShippingOrderForConfirmDetailPanel {
        private readonly string[] kvNames = {
            "PartsShipping_Method", "UsedPartsShippingOrder_Dispatcher", "UsedPartsShippingOrder_Status"
        };

        public UsedPartsShippingOrderForConfirmDetailPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return ServiceUIStrings.DetailPanel_Title_Common;
            }
        }
    }
}
