﻿using System;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Service.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Detail {
    public class UsedPartsTransferDetailDetailPanel : UsedPartsTransferDetailDataGridView, IDetailPanel {

        public string Title {
            get {
                return Utils.GetEntityLocalizedName(typeof(UsedPartsTransferOrder), "UsedPartsTransferDetails");
            }
        }

        public Uri Icon {
            get {
                return null;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
        }
    }
}
