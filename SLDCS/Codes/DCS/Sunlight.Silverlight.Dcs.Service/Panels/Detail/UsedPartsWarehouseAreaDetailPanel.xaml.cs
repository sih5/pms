﻿namespace Sunlight.Silverlight.Dcs.Service.Panels.Detail {
    public partial class UsedPartsWarehouseAreaDetailPanel {
        private readonly string[] kvNames = {
            "Area_Kind", "Area_Category"
        };

        public UsedPartsWarehouseAreaDetailPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
