﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class DealerClaimSparePartsQueryPanel  : DcsQueryPanelBase {
        public DealerClaimSparePartsQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = "服务站索赔领料单查询",
                    EntityType = typeof(DealerClaimSparePart),
                    QueryItems = new QueryItem[]{
                         new CustomQueryItem {
                                ColumnName = "DealerCode",
                                DataType=typeof(string),
                                Title = "服务站编号",
                         },new CustomQueryItem {
                                ColumnName = "DealerName",
                                DataType=typeof(string),
                                Title = "服务站名称",
                         },new CustomQueryItem {
                                ColumnName = "SparePartCode",
                                Title = "配件编号",
                                DataType=typeof(string),
                         },new CustomQueryItem {
                                ColumnName = "SparePartName",
                                Title = "配件名称",
                                DataType=typeof(string),
                         },new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNewWithOtherInfo_CreateTime
                         },new DateTimeRangeQueryItem {
                                ColumnName = "SettleDate",
                                Title = "索赔单结算时间"
                         },new CustomQueryItem {
                                ColumnName = "ClaimCode",
                                DataType=typeof(string),
                                Title = "索赔单号",
                         }
                    }
                }
            };
        }
    }
}
