﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.ViewModel;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class PartsClaimOrderNewForDealerQueryPanel : DcsQueryPanelBase {
        private readonly PartsClaimOrderNewForDealerQueryPanelViewModel viewModel = new PartsClaimOrderNewForDealerQueryPanelViewModel();
        private readonly string[] kvNames = {
            "PartsClaimBill_StatusNew"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_PartsClaimOrderNew,
                    EntityType = typeof(PartsClaimOrderNewWithOtherInfo),
                    QueryItems = new[] {
                       //new ComboQueryItem {
                       //     ColumnName = "PartsSalesCategoryId",
                       //     Title = "品牌",
                       //     SelectedValuePath="Id",
                       //     DisplayMemberPath="Name",
                       //     ItemsSource=this.viewModel.PartsSalesCategories,
                       //     SelectedItemBinding = new Binding("PartsSalesCategory") {
                       //             Source = this.viewModel,
                       //             Mode = BindingMode.TwoWay
                       //         }
                       // },
                        new QueryItem {
                            ColumnName = "Code",
                            Title= ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNew_Code
                        }, new QueryItem {
                            ColumnName = "PartsSalesOrderCode",
                            Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsSalesOrderCode")
                        }, new QueryItem {
                            ColumnName = "PartsRetailOrderCode",
                            Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsRetailOrderCode")
                        }, new QueryItem {
                            ColumnName = "FaultyPartsCode",
                            Title=Utils.GetEntityLocalizedName(typeof(SparePart),"Code")
                        }, new QueryItem {
                            ColumnName = "FaultyPartsName",
                              Title=Utils.GetEntityLocalizedName(typeof(SparePart),"Name")
                        }
                        //, new QueryItem {
                        //    ColumnName = "FaultyPartsSupplierCode",
                        //      Title=Utils.GetEntityLocalizedName(typeof(PartsSupplier),"Code")
                        //}, new QueryItem {
                        //    ColumnName = "FaultyPartsSupplierName",
                        //     Title=Utils.GetEntityLocalizedName(typeof(PartsSupplier),"Name")
                        //}
                        ,new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsServiceTripClaimApplicationStatus.新增,
                            Title= ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNewWithOtherInfo_Status
                        }, new QueryItem {
                            ColumnName = "ShippingCode",
                             Title= ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNewWithOtherInfo_ShippingCode
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                    DateTime.Now.Date.AddDays(-7), DateTime.Now.Date
                                },
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNewWithOtherInfo_CreateTime
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                            //DefaultValue = new[] {
                            //        DateTime.Now.AddDays(-7), DateTime.Now
                            //    },
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNewWithOtherInfo_ApproveTime
                        } 
                    }
                }
            };
        }

        public PartsClaimOrderNewForDealerQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.viewModel.Initialize);
        }
    }

    public class PartsClaimOrderNewForDealerQueryPanelViewModel : ViewModelBase {
        private readonly ObservableCollection<PartsSalesCategory> partsSalesCategories = new ObservableCollection<PartsSalesCategory>();
        private readonly ObservableCollection<VirtualWarehouse> allserviceProductLines = new ObservableCollection<VirtualWarehouse>();
        private PagedCollectionView virtualWarehouse;
        private PartsSalesCategory selectedPartsSalesCategory;


        public ObservableCollection<PartsSalesCategory> PartsSalesCategories {
            get {
                return this.partsSalesCategories;
            }
        }
        public PartsSalesCategory PartsSalesCategory {
            get {
                return this.selectedPartsSalesCategory;
            }
            set {
                if(this.selectedPartsSalesCategory == value)
                    return;
                this.selectedPartsSalesCategory = value;
                this.NotifyOfPropertyChange("PartsSalesCategory");
                this.VirtualWarehouses.Refresh();
            }
        }
       
        public PagedCollectionView VirtualWarehouses {
            get {
                if(this.virtualWarehouse == null) {
                    this.virtualWarehouse = new PagedCollectionView(this.allserviceProductLines);
                    this.virtualWarehouse.Filter = o => ((VirtualWarehouse)o).PartsSalesCategoryId == (this.PartsSalesCategory != null ? this.PartsSalesCategory.Id : 221);
                }
                return this.virtualWarehouse;
            }
        }

        public override void Validate() {
            //
        }

        public void Initialize() {

            var domainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError)
            //        return;
            //    this.partsSalesCategories.Clear();
            //    foreach(var entity in loadOp.Entities) {
            //        this.partsSalesCategories.Add(entity);
            //    }
            //}, null);
            domainContext.Load(domainContext.GetWarehousesForQueryQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.allserviceProductLines.Clear();
                foreach(var serviceProductLine in loadOp.Entities)
                    this.allserviceProductLines.Add(serviceProductLine);
            }, null);
        }
    }
}
