﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class SsUsedPartsDisposalBillForDealerQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "SsUsedPartsDisposalBill_DisposalMethod", "SsUsedPartsDisposalBill_Status"
        };
        private ObservableCollection<KeyValuePair> KvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public SsUsedPartsDisposalBillForDealerQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && BaseApp.Current.CurrentUserData.EnterpriseId == e.BranchId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var item in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_SsUsedPartsDisposalBill,
                    EntityType = typeof(SsUsedPartsDisposalBill),
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            ColumnName = "UsedPartsDisposalMethod",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsSsUsedPartsDisposalBillDisposalMethod.自行处理
                        },new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = KvPartsSalesCategorys,
                            Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsSsUsedPartsDisposalBillStatus.新增
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date.AddDays(1)
                            }
                        }
                    }
                }
            };
        }
    }
}