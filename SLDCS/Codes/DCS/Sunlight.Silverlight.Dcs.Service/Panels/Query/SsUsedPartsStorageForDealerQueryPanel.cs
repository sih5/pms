﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class SsUsedPartsStorageForDealerQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvBranches;
        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy"
        };

        public SsUsedPartsStorageForDealerQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private ObservableCollection<KeyValuePair> KvBranches {
            get {
                return this.kvBranches ?? (this.kvBranches = new ObservableCollection<KeyValuePair>());
            }
        }
        private ObservableCollection<KeyValuePair> KvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        private void Initialize() {

            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranches.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvBranches.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
                domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp2 => {
                    if(loadOp.HasError)
                        return;
                    foreach(var item in loadOp2.Entities)
                        this.KvPartsSalesCategorys.Add(new KeyValuePair {
                            Key = item.Id,
                            Value = item.Name
                        });
                }, null);

                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = ServiceUIStrings.QueryPanel_Title_SsUsedPartsStorage,
                        EntityType = typeof(virtualSsUsedPartsStorageWithClaimBillStatus),
                        QueryItems = new [] {
                           new KeyValuesQueryItem {
                                Title = Utils.GetEntityLocalizedName(typeof(Branch), "Name"),
                                ColumnName = "BranchId",
                                KeyValueItems = KvBranches
                            } ,new KeyValuesQueryItem {
                                ColumnName = "PartsSalesCategoryId",
                                KeyValueItems = KvPartsSalesCategorys,
                                Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory
                            }, new QueryItem {
                                ColumnName = "ClaimBillCode",
                               Title = ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNew_Code
                            }, new KeyValuesQueryItem {
                                ColumnName = "UsedPartsReturnPolicy",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                                DefaultValue = (int)DcsPartsWarrantyTermReturnPolicy.返回本部,
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_SsUsedPartsStorageForBranch_UsedPartsReturnPolicy
                            },new QueryItem {
                                ColumnName = "UsedPartsBarCode",
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_SsUsedPartsStorageForBranch_UsedPartsBarCode
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                                DefaultValue = new [] {
                                    new DateTime(DateTime.Now.Year,DateTime.Now.Month,1),DateTime.Now.Date.AddDays(1)
                                },
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNewWithOtherInfo_CreateTime
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
