﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class SsUsedPartsStorageQueryPanel : DcsQueryPanelBase {

        private ObservableCollection<KeyValuePair> KvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy","RepairClaimBill_RepairClaimStatus"
        };

        public SsUsedPartsStorageQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var item in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title = ServiceUIStrings.QueryPanel_Title_SsUsedPartsStorage,
                        EntityType = typeof(SsUsedPartsStorage),
                        QueryItems = new[] {
                            new QueryItem {
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_SsUsedPartsStorage_UsedPartsCode,
                                ColumnName = "UsedPartsCode"
                            }, new QueryItem {
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_SsUsedPartsStorage_UsedPartsName,
                                ColumnName = "UsedPartsName"
                            }, new KeyValuesQueryItem {
                               Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory,
                                ColumnName = "PartsSalesCategoryId",
                                KeyValueItems = this.KvPartsSalesCategorys,
                            }, new QueryItem {
                                ColumnName = "ClaimBillCode"
                            }, new KeyValuesQueryItem {
                                ColumnName = "UsedPartsReturnPolicy",
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            }, new QueryItem {
                                ColumnName = "UsedPartsBarCode"
                            }, new QueryItem {
                                ColumnName = "ResponsibleUnitName"
                            } ,new KeyValuesQueryItem {
                                ColumnName = "RepairClaimBill.Status",
                                KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_RepairClaimBill_Status,
                                DefaultValue = (int)DcsRepairClaimBillRepairClaimStatus.初审通过
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "CreateTime",
                          
                            }, new DateTimeRangeQueryItem {
                                ColumnName = "ClaimBillCreateTime",
                             
                            }
                        }
                    }
                };
        }
    }
}
