﻿
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class SupplierExpenseAdjustBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "ExpenseAdjustmentBill_Status", "ClaimBill_SettlementStatus", "ExpenseAdjustmentBill_TransactionCategory", "ExpenseAdjustmentBill_SourceType"
        };

        private readonly ObservableCollection<KeyValuePair> KvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> KvResponsibleUnits = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        public SupplierExpenseAdjustBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var entity in loadOp.Entities) {
                    this.kvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);
            domainContext.Load(domainContext.GetBranchesQuery().Where(entity => entity.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.KvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                    });
            }, null);
            domainContext.Load(domainContext.GetResponsibleUnitsQuery().Where(entity => entity.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.KvResponsibleUnits.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                    });
            }, null);


            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(SupplierExpenseAdjustBill),
                    Title = ServiceUIStrings.QueryPanel_Title_ExpenseAdjustmentBill,
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "SettlementStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "TransactionCategory",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "SourceType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                        }, new QueryItem {
                            ColumnName = "Code"
                        }, new QueryItem {
                            ColumnName = "SourceCode"
                        }, new QueryItem {
                            ColumnName = "DealerCode"
                        }, new QueryItem {
                            ColumnName = "DealerName"
                        }, new KeyValuesQueryItem {
                            ColumnName = "BranchId",
                            KeyValueItems = this.KvBranches,
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_Common_Branch
                        }, new KeyValuesQueryItem {
                            ColumnName = "ResponsibleUnitId",
                            KeyValueItems = this.KvResponsibleUnits,
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_ResponsibleUnit
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        },new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = this.kvPartsSalesCategorys,
                            Title =ServiceUIStrings.QueryPanel_QueryItem_Title_ExpenseAdjustmentBill_PartsSalesCategory    
                        }
                    }
                }
            };
        }
    }
}