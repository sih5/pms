﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class TransactionInterfaceLogQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "SynchronousState"
        };
        public TransactionInterfaceLogQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup{
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_TransactionInterfaceLog,
                    EntityType = typeof(TransactionInterfaceLog),
                    QueryItems = new[]{
                         new CustomQueryItem {
                                ColumnName = "RepairOrder.Code",
                                DataType=typeof(string),
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_RepairOrder_Code,
                                IsExact = true
                         },new QueryItem {
                                ColumnName = "MemberPhone",
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_TransactionInterfaceLog_MemberPhone,
                                IsExact = true
                         },new QueryItem {
                                ColumnName = "MemberName",
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_TransactionInterfaceLog_MemberName,
                                IsExact = true
                         },new KeyValuesQueryItem {
                                ColumnName = "SyncStatus",
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNewWithOtherInfo_Status,
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                         },new DateTimeRangeQueryItem {
                                ColumnName = "SyncTime",
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNewWithOtherInfo_CreateTime
                         }
                    }
                }
            };
        }
    }
}
