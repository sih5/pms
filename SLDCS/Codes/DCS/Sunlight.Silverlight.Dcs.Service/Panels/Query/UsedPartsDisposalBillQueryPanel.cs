﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsDisposalBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "UsedPartsDisposalBill_Status", "UsedParts_OutboundStatus", "UsedPartsDisposalBill_DisposalMethod"
        };

        public UsedPartsDisposalBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsDisposalBill,
                    EntityType = typeof(UsedPartsDisposalBill),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "UsedPartsWarehouseCode"
                        }, new QueryItem {
                            ColumnName = "UsedPartsWarehouseName"
                        }, new KeyValuesQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsDisposalBill_Status,
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsUsedPartsDisposalBillStatus.生效
                        }, new KeyValuesQueryItem {
                            ColumnName = "OutboundStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new QueryItem {
                            ColumnName = "RelatedCompanyName"
                        }, new KeyValuesQueryItem {
                            ColumnName = "UsedPartsDisposalMethod",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                        }, new QueryItem {
                            ColumnName = "Code"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime"
                        }
                    }
                }
            };
        }
    }
}
