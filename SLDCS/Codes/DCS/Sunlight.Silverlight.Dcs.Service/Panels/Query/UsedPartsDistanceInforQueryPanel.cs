﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsDistanceInforQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "UsedParts_DistanceType","BaseData_Status"
        };

        public UsedPartsDistanceInforQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.QueryItemGroups = new[] {
                     new QueryItemGroup {
                         UniqueId = "Common",
                         EntityType = typeof(UsedPartsDistanceInfor),
                         Title = ServiceUIStrings.QueryPanel_Title_UsedPartsDistanceInfor,
                         QueryItems = new QueryItem[] {
                             new KeyValuesQueryItem {
                                 ColumnName = "UsedPartsDistanceType",
                                 KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                             }, new KeyValuesQueryItem {
                                 ColumnName = "Status",
                                 DefaultValue=(int)DcsBaseDataStatus.有效,
                                 KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                             }, new CustomQueryItem {
                                 ColumnName = "UsedPartsCompany.Code",
                                 DataType = typeof(string),
                                 Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsDistanceInfor_UsedPartsCompanyCode
                             }, new CustomQueryItem {
                                 ColumnName = "UsedPartsWarehouse.Code",
                                 DataType = typeof(string),
                                 Title=ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsDistanceInfor_UsedPartsWarehouseCode
                             }, new CustomQueryItem {
                                ColumnName = "UsedPartsCompany.Name",
                                DataType = typeof(string),
                                Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsDistanceInfor_UsedPartsCompanyName
                             }, new CustomQueryItem {
                                 ColumnName = "UsedPartsWarehouse.Name",
                                 DataType = typeof(string),
                                 Title=ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsDistanceInfor_UsedPartsWarehouseName
                             }, new DateTimeRangeQueryItem{
                                 ColumnName="CreateTime",
                                 DefaultValue = new[] {
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                         }
                     }
                 }};
        }
    }
}
