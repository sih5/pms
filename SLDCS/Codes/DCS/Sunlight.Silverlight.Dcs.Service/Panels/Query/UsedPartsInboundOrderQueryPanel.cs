﻿using System;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsInboundOrderQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "UsedPartsInboundOrder_InboundType","ClaimBill_SettlementStatus"
        };

        public UsedPartsInboundOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VirtualUsedPartsInboundOrder),
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsInboundOrder,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder),"Code")
                        }, new KeyValuesQueryItem {
                            ColumnName = "InboundType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsInboundOrder_InboundType
                        }, new QueryItem {
                            ColumnName = "UsedPartsWarehouseCode",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsInboundOrder_UsedPartsWarehouseCode
                        }, new QueryItem {
                            ColumnName = "UsedPartsWarehouseName",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsInboundOrder_UsedPartsWarehouseName
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                            },
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsInboundOrder_CreateTime
                        }, new QueryItem {
                            ColumnName = "SourceCode",
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder),"SourceCode")
                        }, new QueryItem {
                            ColumnName = "RelatedCompanyCode",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsInboundOrder_RelatedCompanyCode
                        }, new QueryItem {
                            ColumnName = "RelatedCompanyName",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsInboundOrder_RelatedCompanyName
                        },new KeyValuesQueryItem {
                            ColumnName = "SettlementStatus", 
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder),"SettlementStatus")

                        },new QueryItem {
                            ColumnName = "Businesscode",
                            Title = ServiceUIStrings.DataEditPanel_Text_UsedPartsShippingOrder_BusinessCode
                        }
                    }
                }
            };
        }
    }
}
