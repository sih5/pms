﻿using System;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsLogisticLossBillQueryPanel : DcsQueryPanelBase {

        public UsedPartsLogisticLossBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsLogisticLossBill,
                    EntityType = typeof(UsedPartsLogisticLossBill),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "DealerCode"
                        }, new QueryItem {
                            ColumnName = "DealerName"
                        }, new CustomQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "Code"),
                            ColumnName = "UsedPartsShippingOrder.Code",
                            DataType = typeof(string)
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date.AddDays(1)
                            }
                        }
                    }
                }
            };
        }
    }
}
