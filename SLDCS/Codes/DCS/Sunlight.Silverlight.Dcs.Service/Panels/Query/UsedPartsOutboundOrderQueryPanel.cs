﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.Service.Panels.Query
{
    public class UsedPartsOutboundOrderQueryPanel : DcsQueryPanelBase
    {
        private readonly string[] kvNames = {
            "UsedPartsOutboundOrder_OutboundType"
        };

        public UsedPartsOutboundOrderQueryPanel()
        {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize()
        {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsOutboundOrder,
                    EntityType = typeof(UsedPartsOutboundOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsOutboundOrder_OutboundType,
                            ColumnName = "OutboundType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new QueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsOutboundOrder_UsedPartsWarehouseCode,
                            ColumnName = "UsedPartsWarehouseCode"
                        }, new QueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsOutboundOrder_UsedPartsWarehouseName,
                            ColumnName = "UsedPartsWarehouseName"
                        }, new DateTimeRangeQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsOutboundOrder_CreateTime,
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                DateTime.Now.Date.AddDays(1 - DateTime.Now.Day), DateTime.Now.Date
                            }
                        }, new QueryItem {
                            ColumnName = "SourceCode"
                        }, new QueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsOutboundOrder_RelatedCompanyCode,
                            ColumnName = "RelatedCompanyCode"
                        }, new QueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsOutboundOrder_RelatedCompanyName,
                            ColumnName = "RelatedCompanyName"
                        }, new CustomQueryItem{
                            Title=ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsOutboundOrder_ReturnOfficeCode,
                            ColumnName="BusinessCode",
                            DataType=typeof(string)
                        }, new CustomQueryItem{
                            Title= ServiceUIStrings.QueryPanel_QueryItem_Title_SsUsedPartsStorageForBranch_UsedPartsBarCode,
                            ColumnName="UsedPartsBarCode",
                            DataType=typeof(string)
                        } ,  new CustomQueryItem{
                            Title= ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNew_Code,
                            ColumnName="ClaimBillCode",
                            DataType=typeof(string)
                        }  , new CustomQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsOutboundOrder_IfBillable,
                            ColumnName = "IfBillable",
                            DataType = typeof(bool)
                        }, new CustomQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsOutboundOrder_IfAlreadySettled,
                            ColumnName = "IfAlreadySettled",
                            DataType = typeof(bool)
                        }
                    }
                }
            };
        }
    }
}
