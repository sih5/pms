﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsReturnOrderQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "UsedPartsReturnOrder_ReturnType", "UsedPartsReturnOrder_Status"
        };

        public UsedPartsReturnOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsReturnOrder,
                    EntityType = typeof(UsedPartsReturnOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "OutboundWarehouseCode",
                            IsExact = false
                        },new QueryItem {
                            ColumnName = "OutboundWarehouseName",
                            IsExact = false
                        },new QueryItem {
                            ColumnName = "ReturnOfficeCode",
                            IsExact = false
                        },new QueryItem {
                            ColumnName = "ReturnOfficeName",
                            IsExact = false
                        },new QueryItem {
                            ColumnName = "ReturnOfficeCode",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsReturnOrder_ReturnOfficeCode
                        },new QueryItem {
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem {
                            ColumnName = "ReturnType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}
