﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsReturnPolicyHistoryQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy"
        };

        public UsedPartsReturnPolicyHistoryQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
        private ObservableCollection<KeyValuePair> KvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && BaseApp.Current.CurrentUserData.EnterpriseId == e.BranchId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var item in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsReturnPolicyHistory,
                    EntityType = typeof(UsedPartsReturnPolicyHistory),
                    QueryItems = new[] {
                        new CustomQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_Dealer_Code,
                            ColumnName = "Dealer.Code",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_Dealer_Name,
                            ColumnName = "Dealer.Name",
                            DataType = typeof(string)
                        }, new QueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsReturnPolicyHistory_UsedPartsCode,
                            ColumnName = "UsedPartsCode"
                        }, new QueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsReturnPolicyHistory_UsedPartsName,
                            ColumnName = "UsedPartsName"
                        },new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = KvPartsSalesCategorys,
                            Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory
                        }, new QueryItem {
                            ColumnName = "ClaimBillCode"
                        }, new QueryItem {
                            ColumnName = "UsedPartsBarCode"
                        }, new KeyValuesQueryItem {
                            ColumnName = "UsedPartsReturnPolicy",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue = (int)DcsPartsWarrantyTermReturnPolicy.返回本部
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date.AddDays(1)
                            }
                        }
                    }
                }
            };
        }
    }
}
