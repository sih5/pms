﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsShiftOrderQueryPanel : DcsQueryPanelBase {

        public UsedPartsShiftOrderQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsShiftOrder,
                    EntityType = typeof(UsedPartsShiftOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        }, new QueryItem {
                            ColumnName = "UsedPartsWarehouseCode"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59)
                            }
                        }, new QueryItem {
                            ColumnName = "UsedPartsWarehouseName"
                        }
                    }
                }
            };
        }
    }
}
