﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsShippingOrderForConfirmQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBranches = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvUsedPartsWarehouses = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> KvPartsSalesCategorys = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "PartsShipping_Method","UsedPartsShippingOrder_Status"
        };

        public UsedPartsShippingOrderForConfirmQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetBranchesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var branch in loadOp.Entities)
                    this.kvBranches.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetPartsSalesCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && BaseApp.Current.CurrentUserData.EnterpriseId == e.BranchId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var item in loadOp.Entities)
                    this.KvPartsSalesCategorys.Add(new KeyValuePair {
                        Key = item.Id,
                        Value = item.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetUsedPartsWarehousesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var usedPartsWarehouses in loadOp.Entities)
                    this.kvUsedPartsWarehouses.Add(new KeyValuePair {
                        Key = usedPartsWarehouses.Id,
                        Value = usedPartsWarehouses.Name
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsShippingOrder,
                    EntityType = typeof(UsedPartsShippingOrderWithOtherInfo),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(Branch), "Name"),
                            ColumnName = "BranchId",
                            DefaultValue=BaseApp.Current.CurrentUserData.EnterpriseId,
                            IsEnabled=false,
                            KeyValueItems = this.kvBranches
                        },new KeyValuesQueryItem {
                            ColumnName = "PartsSalesCategoryId",
                            KeyValueItems = KvPartsSalesCategorys,
                            Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory
                        }, new QueryItem {
                            ColumnName = "DealerCode",
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "DealerCode")
                        }, new QueryItem {
                            ColumnName = "DealerName",
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "DealerName")
                        },  new QueryItem {
                            ColumnName = "UsedPartsShippingOrderCode",
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "Code")
                        },new KeyValuesQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsWarehouse_Name,
                            ColumnName = "UsedPartsWarehouseId",
                            KeyValueItems = this.kvUsedPartsWarehouses
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            DefaultValue = (int)DcsUsedPartsShippingOrderStatus.新增,
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "Status")
                        }, new KeyValuesQueryItem {
                            ColumnName = "ShippingMethod",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "ShippingMethod")
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue =new []{
                                new DateTime(DateTime.Now.Year,DateTime.Now.Month,1), DateTime.Now.Date.AddDays(1)
                            },
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "CreateTime")
                        }
                    }
                }
            };
        }
    }
}
