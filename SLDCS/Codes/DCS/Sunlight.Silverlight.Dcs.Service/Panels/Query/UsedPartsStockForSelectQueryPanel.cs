﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsStockForSelectQueryPanel : DcsQueryPanelBase {

        public UsedPartsStockForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsStock,
                    EntityType = typeof(UsedPartsStock),
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "UsedPartsWarehouse.Code",
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsWarehouse), "Code"),
                            DataType = typeof(string)
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsStock_CreateTime
                        }, new CustomQueryItem {
                            ColumnName = "UsedPartsWarehouse.Name",
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsWarehouse), "Name"),
                            DataType = typeof(string)
                        }, new QueryItem {
                            ColumnName = "FaultyPartsSupplierCode"
                        }, new QueryItem {
                            ColumnName = "UsedPartsSupplierCode"
                        }, new QueryItem {
                            ColumnName = "FaultyPartsSupplierName"
                        }, new QueryItem {
                            ColumnName = "UsedPartsSupplierName"
                        }, new QueryItem {
                            ColumnName = "ResponsibleUnitName"
                        }, new QueryItem {
                            ColumnName = "ResponsibleUnitCode"
                        }, new CustomQueryItem {
                            ColumnName = "IfFaultyParts",
                            DataType = typeof(bool)
                        }
                    }
                }
            };
        }
    }
}
