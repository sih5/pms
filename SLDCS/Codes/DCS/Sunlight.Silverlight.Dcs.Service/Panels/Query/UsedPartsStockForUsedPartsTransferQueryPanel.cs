﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsStockForUsedPartsTransferQueryPanel : DcsQueryPanelBase {

        public UsedPartsStockForUsedPartsTransferQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsStock,
                    EntityType = typeof(UsedPartsStock),
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "UsedPartsWarehouse.Name",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsStock_UsedPartsWarehouse,
                            DataType = typeof(string),
                            IsEnabled = false
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsStock_CreateTime
                        }, new QueryItem {
                            ColumnName = "UsedPartsCode"
                        }, new QueryItem {
                            ColumnName = "UsedPartsName"
                        }, new QueryItem {
                            ColumnName = "UsedPartsSupplierCode"
                        }, new QueryItem {
                            ColumnName = "FaultyPartsSupplierCode"
                        }, new QueryItem {
                            ColumnName = "UsedPartsSupplierName"
                        }, new QueryItem {
                            ColumnName = "FaultyPartsSupplierName"
                        }, new QueryItem{
                            ColumnName = "UsedPartsBarCode"
                        }, new CustomQueryItem {
                            ColumnName = "IfFaultyParts",
                            DataType = typeof(bool)
                        }, new QueryItem {
                            ColumnName = "ResponsibleUnitCode"
                        }, new QueryItem {
                            ColumnName = "ResponsibleUnitName"
                        }
                    }
                }
            };
        }
    }
}