﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsStockQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "ClaimBill_Type","UsedPartsShippingDetail_ReceptionStatus"
        };
        private readonly ObservableCollection<KeyValuePair> kvUsedPartsWarehouse = new ObservableCollection<KeyValuePair>();
        public UsedPartsStockQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetUsedPartsWarehousesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities != null) {
                    foreach(var entity in loadOp.Entities) {
                        this.kvUsedPartsWarehouse.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Name
                        });
                    }
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsStock,
                    EntityType = typeof(UsedPartsStock),
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            ColumnName = "UsedPartsWarehouseId",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsStock_UsedPartsWarehouse,
                            KeyValueItems = this.kvUsedPartsWarehouse
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsStock_CreateTime
                        }, new CustomQueryItem {
                            ColumnName = "UsedPartsWarehouseArea.Code",
                            Title = Service.Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsStock_WarehouseAreaCode,
                            DataType = typeof(string)
                        }, new QueryItem {
                            ColumnName = "UsedPartsBarCode"
                        }, new KeyValuesQueryItem {
                            ColumnName = "ClaimBillType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new QueryItem {
                            ColumnName = "ClaimBillCode"
                        }, new QueryItem {
                            ColumnName = "UsedPartsCode",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Tilte_UsedPartsStock_UsedPartsCode
                        }, new QueryItem {
                            ColumnName = "UsedPartsName",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsStock_UsedPartsName
                        }, new QueryItem {
                            ColumnName = "UsedPartsSupplierCode"
                        }, new QueryItem {
                            ColumnName = "FaultyPartsSupplierCode"
                        }, new QueryItem {
                            ColumnName = "UsedPartsSupplierName"
                        }, new QueryItem {
                            ColumnName = "FaultyPartsSupplierName"
                        }, new QueryItem {
                            ColumnName = "ResponsibleUnitCode"
                        }, new QueryItem {
                            ColumnName = "ResponsibleUnitName"
                        },new DateTimeRangeQueryItem {
                            ColumnName = "ClaimBillCreateTime",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Tilte_UsedPartsStock_ClaimBillCreateTime
                        }, new CustomQueryItem {
                            ColumnName = "IfFaultyParts",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Tilte_UsedPartsStock_IfFaultyParts,
                            DataType = typeof(bool)
                        }, new KeyValuesQueryItem {
                            ColumnName = "ReceptionStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title= ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsStock_ReceptionStatus
                        }
                    }
                }
            };
        }

    }
}
