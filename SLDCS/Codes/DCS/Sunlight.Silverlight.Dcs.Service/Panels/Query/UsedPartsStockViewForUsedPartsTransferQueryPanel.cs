﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsStockViewForUsedPartsTransferQueryPanel : DcsQueryPanelBase {

        public UsedPartsStockViewForUsedPartsTransferQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsStock,
                    EntityType = typeof(UsedPartsStockView),
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "Name",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsStock_UsedPartsWarehouse,
                            DataType = typeof(string),
                            IsEnabled = false
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsStock_CreateTime
                        }, new QueryItem {
                            ColumnName = "UsedPartsCode",
                            Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"UsedPartsCode")
                        }, new QueryItem {
                            ColumnName = "UsedPartsName",
                            Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"UsedPartsName")
                        }, new QueryItem {
                            ColumnName = "UsedPartsSupplierCode",
                            Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"UsedPartsSupplierCode")
                        }, new QueryItem {
                            ColumnName = "FaultyPartsSupplierCode",
                            Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"FaultyPartsSupplierCode")
                        }, new QueryItem {
                            ColumnName = "UsedPartsSupplierName",
                            Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"UsedPartsSupplierName")
                        }, new QueryItem {
                            ColumnName = "FaultyPartsSupplierName",
                            Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"FaultyPartsSupplierName")
                        }, new QueryItem{
                            ColumnName = "UsedPartsBarCode",
                            Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"UsedPartsBarCode")
                        }, new QueryItem {
                            ColumnName = "IfFaultyParts",
                            Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"IfFaultyParts")
                        }, new QueryItem {
                            ColumnName = "ResponsibleUnitCode",
                            Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"ResponsibleUnitCode")
                        }, new QueryItem {
                            ColumnName = "ResponsibleUnitName",
                            Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"ResponsibleUnitName")
                        },new QueryItem {
                            ColumnName = "IsTransfers",
                            Title= ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsStockView_IsTransfers,
                            DefaultValue=false
                        }
                    }
                }
            };
        }
    }
}
