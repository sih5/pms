﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsTransferOrderQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "UsedPartsTransferOrder_Status", "UsedParts_OutboundStatus", "UsedParts_InboundStatus"
        };

        public UsedPartsTransferOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsTransferOrder,
                    EntityType = typeof(UsedPartsTransferOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsTransferOrder_Status,
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new QueryItem {
                            ColumnName = "OriginWarehouseCode"
                        }, new QueryItem {
                            ColumnName = "DestinationWarehouseCode"
                        }, new QueryItem {
                            ColumnName = "OriginWarehouseName"
                        }, new QueryItem {
                            ColumnName = "DestinationWarehouseName"
                        }, new KeyValuesQueryItem {
                            ColumnName = "OutboundStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "InboundStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now.Date
                            }
                        }
                    }
                }
            };
        }
    }
}
