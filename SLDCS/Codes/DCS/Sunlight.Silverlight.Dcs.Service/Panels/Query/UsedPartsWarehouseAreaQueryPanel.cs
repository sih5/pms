﻿using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class UsedPartsWarehouseAreaQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvStorageAreaType = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "Area_Category", "Area_Kind", "BaseData_Status"
        };

        private readonly int[] excludeStatus = new[] {
            (int)DcsAreaKind.仓库
        };

        public UsedPartsWarehouseAreaQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(() => {
                foreach(var status in this.KeyValueManager[this.kvNames[1]].Where(kv => !this.excludeStatus.Contains(kv.Key)))
                    this.kvStorageAreaType.Add(status);
            });
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsWarehouseArea,
                    EntityType = typeof(UsedPartsWarehouseArea),
                    QueryItems = new [] {
                        new CustomQueryItem {
                            Title = Utils.GetEntityLocalizedName(typeof(UsedPartsWarehouse), "Name"),
                            ColumnName = "UsedPartsWarehouse.Name",
                            DataType = typeof(string)
                        }, new QueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsWarehouseArea_Code,
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem {
                            ColumnName = "StorageCategory",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "StorageAreaType",
                            KeyValueItems = this.kvStorageAreaType
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                            DefaultValue = (int)DcsBaseDataStatus.有效
                        }
                    }
                }
            };
        }
    }
}
