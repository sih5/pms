﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class VirtualStructClaimPriceQueryPanel : DcsQueryPanelBase {
        public VirtualStructClaimPriceQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            //domainContext.Load(domainContext.GetPartsWarrantyCategoriesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError) {
            //        loadOp.MarkErrorAsHandled();
            //        return;
            //    }
            //    if(loadOp.Entities != null) {
            //        foreach(var entity in loadOp.Entities) {
            //            this.kvPartsWarrantyCategories.Add(new KeyValuePair {
            //                Key = entity.Id,
            //                Value = entity.Name
            //            });
            //        }
            //    }
            //}, null);
            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    //EntityType = typeof(VirtualStructClaimPrice),
                    Title = ServiceUIStrings.QueryPanel_Title_StructClaimPrice,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SparePartCode",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_SsUsedPartsStorage_UsedPartsCode,
                        },new QueryItem {
                            ColumnName = "SparePartName",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_SsUsedPartsStorage_UsedPartsName
                        },new KeyValuesQueryItem {
                            ColumnName = "PartsWarrantyCategoryId",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_StructClaimPrice_PartsWarrantyCategoryId,
                            KeyValueItems = this.kvPartsWarrantyCategories 
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrderNewWithOtherInfo_CreateTime
                        }
                    }
                }
            };
        }

        private ObservableCollection<KeyValuePair> kvPartsWarrantyCategories = new ObservableCollection<KeyValuePair>();
    }
}
