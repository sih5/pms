﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class VirtualSupplierClaimBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "SupplierClaimSettlementDetail_SourceType"
        };

        public VirtualSupplierClaimBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    //EntityType = typeof(""),//typeof(VirtualSupplierClaimsSettleSourceBill),
                    Title = ServiceUIStrings.QueryPanel_Title_VirtualSupplierClaimBill,
                    QueryItems = new[] {
                        new CustomQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_SupplierName,
                            ColumnName = "SupplierName",
                            DataType = typeof(string),
                            IsEnabled = false
                        },new CustomQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_Common_BranchName,
                            ColumnName = "BranchName",
                            IsEnabled = false,
                            DataType = typeof(string)
                        },new QueryItem {
                            IsExact = true,
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualClaimBill_SourceBillCode,
                            ColumnName = "SourceBillCode"
                        }, new KeyValuesQueryItem {
                            ColumnName = "SourceBillType",
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualClaimBill_SourceBillType,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new DateTimeRangeQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualClaimBill_CreateTime,
                            ColumnName = "CreateTime",
                            IsEnabled = false
                        }
                    }
                }
            };
        }
    }
}
