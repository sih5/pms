﻿using System;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class VirtualUsedPartsInboundPlanQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvInboundType = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "UsedPartsInboundOrder_InboundType", "UsedParts_InboundStatus"
        };

        public VirtualUsedPartsInboundPlanQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_UsedPartsInboundOrder,
                    EntityType = typeof(VirtualUsedPartsInboundPlan),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "SourceCode"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "SourceBillApprovalTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59)
                            }
                        }, new KeyValuesQueryItem {
                            ColumnName = "InboundType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "InboundStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new QueryItem {
                            ColumnName = "UsedPartsWarehouseCode"
                        }, new QueryItem {
                            ColumnName = "UsedPartsWarehouseName"
                        }
                    }
                }
            };
        }
    }
}
