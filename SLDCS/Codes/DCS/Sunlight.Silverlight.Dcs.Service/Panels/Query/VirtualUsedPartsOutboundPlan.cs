﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Panels.Query {
    public class VirtualUsedPartsOutboundPlan : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvOutboundStatus = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "UsedPartsOutboundOrder_OutboundType", "UsedParts_OutboundStatus"
        };

        public VirtualUsedPartsOutboundPlan() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(() => {
                foreach(var status in this.KeyValueManager[this.kvNames[1]].Where(kv => (kv.Key == (int)DcsUsedPartsOutboundStatus.部分出库 || kv.Key == (int)DcsUsedPartsOutboundStatus.待出库)))
                    this.kvOutboundStatus.Add(status);
            });
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = ServiceUIStrings.QueryPanel_Title_VirtualUsedPartsOutboundPlan,
                    EntityType = typeof(VirtualUsedPartsOutboundPlan),
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_SourceCode,
                            ColumnName = "SourceCode",
                            DataType = typeof(string)
                        }, new DateTimeRangeQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_SourceBillApprovalTime,
                            ColumnName = "SourceBillApprovalTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 00, 00, 00), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59)
                            }
                        }, new KeyValuesQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_OutboundType,
                            ColumnName = "OutboundType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_OutboundStatus,
                            ColumnName = "OutboundStatus",
                            KeyValueItems = this.kvOutboundStatus
                        }, new CustomQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_UsedPartsWarehouseCode,
                            ColumnName = "UsedPartsWarehouseCode",
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_UsedPartsWarehouseName,
                            ColumnName = "UsedPartsWarehouseName",
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = "对方单位名称",
                            ColumnName = "RelatedCompanyName",
                            DataType = typeof(string)
                        },new CustomQueryItem {
                            Title = "业务编码",
                            ColumnName = "BusinessCode",
                            DataType = typeof(string)
                        }
                    }
                }
            };
        }
    }
}
