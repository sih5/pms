﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Service {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }   

            public string QueryPanel_Title_UsedPartsWarehouse {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_UsedPartsWarehouse;
            }
        }

        public string CustomView_Text_UsedPartsWarehouseArea_Title_UsedPartsWarehouseManager {
            get {
                return Resources.ServiceUIStrings.CustomView_Text_UsedPartsWarehouseArea_Title_UsedPartsWarehouseManager;
            }
        }

        public string DataEditPanel_Text_UsedPartsWarehouseArea_Code {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsWarehouseArea_Code;
            }
        }

        public string DetailPanel_Text_SsUsedPartsStorage_UsedPartsCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SsUsedPartsStorage_UsedPartsCode;
            }
        }

        public string DetailPanel_Text_SsUsedPartsStorage_UsedPartsName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SsUsedPartsStorage_UsedPartsName;
            }
        }

        public string DetailPanel_Text_SsUsedPartsStorage_NewPartsCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SsUsedPartsStorage_NewPartsCode;
            }
        }

        public string DetailPanel_Text_SsUsedPartsStorage_NewPartsName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SsUsedPartsStorage_NewPartsName;
            }
        }

        public string DetailPanel_Text_SsUsedPartsStorage_NewPartsSecurityNumber {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SsUsedPartsStorage_NewPartsSecurityNumber;
            }
        }

        public string DetailPanel_Text_Dealer_Code {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_Dealer_Code;
            }
        }

        public string DetailPanel_Text_Dealer_Name {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_Dealer_Name;
            }
        }

        public string QueryPanel_Title_RepairItem {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_RepairItem;
            }
        }

        public string DetailPanel_Text_UsedPartsUsedPartsShippingOrder_TransportationVehicle {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_UsedPartsUsedPartsShippingOrder_TransportationVehicle;
            }
        }

        public string DetailPanel_Text_UsedPartsUsedPartsShippingOrder_Name {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_UsedPartsUsedPartsShippingOrder_Name;
            }
        }

        public string DataEditPanel_GroupTitle_UsedPartsShippingOrder_CarriersInformation {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_UsedPartsShippingOrder_CarriersInformation;
            }
        }

        public string DataEditPanel_GroupTitle_UsedPartsShippingOrder_DeliveryInformation {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_UsedPartsShippingOrder_DeliveryInformation;
            }
        }

        public string DataEditPanel_GroupTitle_UsedPartsShippingOrder_ShipInformation {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_UsedPartsShippingOrder_ShipInformation;
            }
        }

        public string DataEditPanel_Text_UsedPartsShippingOrder_TransportationVehicle {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsShippingOrder_TransportationVehicle;
            }
        }

        public string DetailPanel_Text_UsedPartsReturnPolicyHistory_NewPartsCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_UsedPartsReturnPolicyHistory_NewPartsCode;
            }
        }

        public string DetailPanel_Text_UsedPartsReturnPolicyHistory_NewPartsName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_UsedPartsReturnPolicyHistory_NewPartsName;
            }
        }

        public string DetailPanel_Text_UsedPartsReturnPolicyHistory_NewPartsSecurityNumber {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_UsedPartsReturnPolicyHistory_NewPartsSecurityNumber;
            }
        }

        public string DetailPanel_Text_UsedPartsReturnPolicyHistory_UsedPartsCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_UsedPartsReturnPolicyHistory_UsedPartsCode;
            }
        }

        public string DetailPanel_Text_UsedPartsReturnPolicyHistory_UsedPartsName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_UsedPartsReturnPolicyHistory_UsedPartsName;
            }
        }

        public string DataEditPanel_GroupTitle_LogisticCompany {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_LogisticCompany;
            }
        }

        public string DataEditPanel_GroupTitle_UsedPartsShippingOrder_CarrierInformation {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_UsedPartsShippingOrder_CarrierInformation;
            }
        }

        public string DataEditPanel_GroupTitle_UsedPartsShippingOrder_ReceivingInformation {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_UsedPartsShippingOrder_ReceivingInformation;
            }
        }

        public string DataEditPanel_Text_UsedPartsWarehouse_Name {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsWarehouse_Name;
            }
        }

        public string DetailPanel_Text_UsedPartsOutboundOrder_OutboundType {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_UsedPartsOutboundOrder_OutboundType;
            }
        }

        public string DetailPanel_Text_UsedPartsOutboundOrder_UsedPartsWarehouseName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_UsedPartsOutboundOrder_UsedPartsWarehouseName;
            }
        }

        public string DataEditPanel_Text_UsedPartsInboundOrder_UsedPartsWarehouseName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsInboundOrder_UsedPartsWarehouseName;
            }
        }

        public string DataEditPanel_Text_UsedPartsTransferOrder_DestinationWarehouseName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsTransferOrder_DestinationWarehouseName;
            }
        }

        public string DataEditPanel_Text_UsedPartsTransferOrder_OriginWarehouseName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsTransferOrder_OriginWarehouseName;
            }
        }

        public string DataEditPanel_Title_UsedPartsWarehouse {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Title_UsedPartsWarehouse;
            }
        }

        public string QueryPanel_Title_UsedPartsWarehouseName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_UsedPartsWarehouseName;
            }
        }

        public string DataEditPanel_Text_UsedPartsInboundOrder_InboundType {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsInboundOrder_InboundType;
            }
        }

        public string DataEditPanel_Text_Common_Span_To {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_Common_Span_To;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Currency {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_Common_Unit_Currency;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Km {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_Common_Unit_Km;
            }
        }

        public string DataEditPanel_Text_Common_Unit_Month {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_Common_Unit_Month;
            }
        }

        public string DataEditView_Title_SsUsedPartsStorageDataEditView {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_SsUsedPartsStorageDataEditView;
            }
        }

        public string QueryPanel_Title_ResponsibleUnit {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_ResponsibleUnit;
            }
        }

        public string QueryPanel_Title_RepairClaimBill {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_RepairClaimBill;
            }
        }

        public string QueryPanel_Title_RepairClaimApplication {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_RepairClaimApplication;
            }
        }

        public string QueryPanel_Title_PartsClaimApplication {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_PartsClaimApplication;
            }
        }

        public string QueryPanel_Title_ServiceTripClaimApplication {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_ServiceTripClaimApplication;
            }
        }

        public string QueryPanel_Title_Malfunction {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_Malfunction;
            }
        }

        public string DataEditPanel_GroupTitle_VehicleDetailInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_VehicleDetailInfo;
            }
        }

        public string DataEditPanel_GroupTitle_VehicleInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_VehicleInfo;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_FaultyParts {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_FaultyParts;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_FaultyPartsSupplier {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_FaultyPartsSupplier;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_Malfunction {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_Malfunction;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_ServiceProductLine {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_ServiceProductLine;
            }
        }

        public string DataEditPanel_GroupTitle_PartsClaimApplication_MalfunctionInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_PartsClaimApplication_MalfunctionInfo;
            }
        }

        public string DetailPanel_Text_Branch_Name {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_Branch_Name;
            }
        }

        public string DetailPanel_Text_PartsClaimApplication_FaultyPartsName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsClaimApplication_FaultyPartsName;
            }
        }

        public string DetailPanel_Text_PartsClaimApplication_MalfunctionDescription {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsClaimApplication_MalfunctionDescription;
            }
        }

        public string DataEditPanel_GroupTitle_MaintainInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_MaintainInfo;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_BranchId {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_BranchId;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_ClaimBillCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_ClaimBillCode;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_RepairRequestTime {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_RepairRequestTime;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_VehicleMaintenancePoilcyId {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_VehicleMaintenancePoilcyId;
            }
        }

        public string DataEditView_Text_RepairClaimItemDetail_IfObliged {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairClaimItemDetail_IfObliged;
            }
        }

        public string DataEditView_Text_RepairClaimItemDetail_IfSelfDefine {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairClaimItemDetail_IfSelfDefine;
            }
        }

        public string DataEditView_Text_RepairClaimItemDetail_RepairItemName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairClaimItemDetail_RepairItemName;
            }
        }

        public string DataEditView_Title_RepairClaimBill {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimBill;
            }
        }

        public string DataEditView_Title_RepairClaimItemDetail {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimItemDetail;
            }
        }

        public string DetailPanel_text_RepairClaimBill_DealerName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_RepairClaimBill_DealerName;
            }
        }

        public string DetailPanel_Text_RepairClaimBill_MaintainClaimBill_RepairClaimApplicationId {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimBill_MaintainClaimBill_RepairClaimApplicationId;
            }
        }

        public string DetailPanel_text_RepairClaimBill_MarketingDepartmentId {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_RepairClaimBill_MarketingDepartmentId;
            }
        }

        public string DetailPanel_text_RepairClaimBill_ResponsibleUnitId {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_RepairClaimBill_ResponsibleUnitId;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_RepairClaimApplication {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_RepairClaimApplication;
            }
        }

        public string DetailPanel_Text_PartsSupplier_Name {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsSupplier_Name;
            }
        }

        public string DetailPanel_Text_ServiceProductLine_Name {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceProductLine_Name;
            }
        }

        public string QueryPanel_QueryItem_Title_Common_Branch {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_Common_Branch;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_DealerName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_DealerName;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_VehicleInfomation {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_VehicleInfomation;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_MalfunctionInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_MalfunctionInfo;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_ApplicationCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_ApplicationCode;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_BasicInfomation {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_BasicInfomation;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_MalfunctionInfo {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_MalfunctionInfo;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_ServiceProductLine {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_ServiceProductLine;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_Branch {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_Branch;
            }
        }

        public string DataEditPanel_GroupTitle_RepairClaimApplication {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_RepairClaimApplication;
            }
        }

        public string DataEditPanel_Text_Common_BranchName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_Common_BranchName;
            }
        }

        public string DataEditPanel_Text_Common_ServiceProductLine {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_Common_ServiceProductLine;
            }
        }

        public string DataPanel_Text_RepairClaimBill_ClaimSupplierName {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_ClaimSupplierName;
            }
        }

        public string DataPanel_Text_RepairClaimBill_FaultyPartsName {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_FaultyPartsName;
            }
        }

        public string DataPanel_Text_RepairClaimBill_FaultyPartsSupplierName {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_FaultyPartsSupplierName;
            }
        }

        public string DataPanel_Text_RepairClaimBill_MalfunctionDescription {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_MalfunctionDescription;
            }
        }

        public string DataPanel_Text_RepairClaimBill_RepairClaimApplicationCode {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_RepairClaimApplicationCode;
            }
        }

        public string DataPanel_Text_RepairClaimBill_ServiceTripClaimAppCode {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_ServiceTripClaimAppCode;
            }
        }

        public string QueryPanel_Title_FaultyPartsSupplier {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_FaultyPartsSupplier;
            }
        }

        public string QueryPanel_Title_PartsClaimPrice {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_PartsClaimPrice;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_MaintenanceInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_MaintenanceInfo;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_RepairRequestTime {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_RepairRequestTime;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_VehicleMaintenancePoilcyName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_VehicleMaintenancePoilcyName;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_VehicleWarrantyCardCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_VehicleWarrantyCardCode;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_BranchName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_BranchName;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_ServiceProductLineName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_ServiceProductLineName;
            }
        }

        public string DataEditPanel_GroupTitle_ClaimInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_ClaimInfo;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_Title {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_Title;
            }
        }

        public string DataEditPanel_Text_PartsClaimRepairItemDetail_Title {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimRepairItemDetail_Title;
            }
        }

        public string DetailPanel_Text_PartsClaimApplication_BranchName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsClaimApplication_BranchName;
            }
        }

        public string DataEditPanel_Text_PartsClaimApplication_MalfunctionInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimApplication_MalfunctionInfo;
            }
        }

        public string DetailPanel_Text_PartsClaimApplication_Malfunction {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsClaimApplication_Malfunction;
            }
        }

        public string DetailPanel_Text_PartsClaimApplication_ServiceProductLine {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsClaimApplication_ServiceProductLine;
            }
        }

        public string DetailPanel_Text_PartsClaimApplication_FaultyPartsSupplierName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsClaimApplication_FaultyPartsSupplierName;
            }
        }

        public string DetailPanel_GroupTitle_PartsClaimApplication {
            get {
                return Resources.ServiceUIStrings.DetailPanel_GroupTitle_PartsClaimApplication;
            }
        }

        public string DetailPanel_Text_PartsClaimApplication_DealerName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsClaimApplication_DealerName;
            }
        }

        public string QueryPanel_Title_PartsSupplier {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_PartsSupplier;
            }
        }

        public string DataEditPanel_GroupTitle_BasicInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_BasicInfo;
            }
        }

        public string DataEditPanel_GroupTitle_OutInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_OutInfo;
            }
        }

        public string DataEditPanel_GroupTitle_ServiceTripClaimBill {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_ServiceTripClaimBill;
            }
        }

        public string DetailPanel_Text_MarketingDepartment_Name {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_MarketingDepartment_Name;
            }
        }

        public string DetailPanel_Text_ResponsibleUnit_Name {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ResponsibleUnit_Name;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_DealerName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_DealerName;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_IfClaimToSupplier {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_IfClaimToSupplier;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_IfUseOwnVehicle {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_IfUseOwnVehicle;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_IfUseTowTruck {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_IfUseTowTruck;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_ClaimSupplier {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_ClaimSupplier;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_FaultyParts {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_FaultyParts;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_FaultyPartsSupplier {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_FaultyPartsSupplier;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_Malfunction {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_Malfunction;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_PartsClaimApplication {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_PartsClaimApplication;
            }
        }

        public string DataEditPanel_GroupTitle_PartsClaimApplication_MaintenanceInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_PartsClaimApplication_MaintenanceInfo;
            }
        }

        public string DataEditView_GroupTitle_MaintainClaimApplicationForDealer {
            get {
                return Resources.ServiceUIStrings.DataEditView_GroupTitle_MaintainClaimApplicationForDealer;
            }
        }

        public string DetailPanel_Text_PartsClaimApplication_EstimatedMaintenanceTime {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsClaimApplication_EstimatedMaintenanceTime;
            }
        }

        public string DetailPanel_Text_PartsClaimApplication_VehicleMaintenancePoilcyName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsClaimApplication_VehicleMaintenancePoilcyName;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_Code {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_Code;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_EstimatedMaintenanceTime {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_EstimatedMaintenanceTime;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_VehicleMaintenancePoilcyName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_VehicleMaintenancePoilcyName;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_VehicleWarrantyCardCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_VehicleWarrantyCardCode;
            }
        }

        public string DataPanel_Text_RepairClaimBill_IfClaimToSupplier {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_IfClaimToSupplier;
            }
        }

        public string DataPanel_Text_RepairClaimBill_MarketingDepartment {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_MarketingDepartment;
            }
        }

        public string DataPanel_Text_RepairClaimBill_ResponsibleUnit {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_ResponsibleUnit;
            }
        }

        public string DataEditView_Text_PartsClaimRepairItemDetail_DefaultLaborHour {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_PartsClaimRepairItemDetail_DefaultLaborHour;
            }
        }

        public string DataEditView_Text_PartsClaimRepairItemDetail_LaborCost {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_PartsClaimRepairItemDetail_LaborCost;
            }
        }

        public string DataEditView_Text_PartsClaimRepairItemDetail_LaborUnitPrice {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_PartsClaimRepairItemDetail_LaborUnitPrice;
            }
        }

        public string DataEditView_Text_PartsClaimRepairItemDetail_Name {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_PartsClaimRepairItemDetail_Name;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_ClaimSupplierName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_ClaimSupplierName;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_ClaimSupplierName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_ClaimSupplierName;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_IfClaimToSupplier {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_IfClaimToSupplier;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_IfUseOwnVehicle {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_IfUseOwnVehicle;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_IfUseTowTruck {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_IfUseTowTruck;
            }
        }

        public string DetailPanel_GroupTitle_BasicInfo {
            get {
                return Resources.ServiceUIStrings.DetailPanel_GroupTitle_BasicInfo;
            }
        }

        public string DetailPanel_GroupTitle_OutInfo {
            get {
                return Resources.ServiceUIStrings.DetailPanel_GroupTitle_OutInfo;
            }
        }

        public string DetailPanel_GroupTitle_VehicleInfo {
            get {
                return Resources.ServiceUIStrings.DetailPanel_GroupTitle_VehicleInfo;
            }
        }

        public string DataEditView_GroupTitle_ServiceTripClaimBill {
            get {
                return Resources.ServiceUIStrings.DataEditView_GroupTitle_ServiceTripClaimBill;
            }
        }

        public string DataEditView_GroupTitle_VehicleDetailInfo {
            get {
                return Resources.ServiceUIStrings.DataEditView_GroupTitle_VehicleDetailInfo;
            }
        }

        public string DataEditPanel_GroupTitle_RepairClaimBill {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_RepairClaimBill;
            }
        }

        public string DataEditPanel_GroupTitle_RepairItem {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_RepairItem;
            }
        }

        public string QueryPanel_Title_ClaimSupplierName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_ClaimSupplierName;
            }
        }

        public string DetailPanel_Title_PartsClaimApplicationFinal {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Title_PartsClaimApplicationFinal;
            }
        }

        public string DetailPanel_Title_PartsClaimApplicationInitial {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Title_PartsClaimApplicationInitial;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_BranchName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_BranchName;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_Remark {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_Remark;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_RepairRequestTime {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_RepairRequestTime;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_ServiceActivityContent {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_ServiceActivityContent;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_ServiceActivityType {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_ServiceActivityType;
            }
        }

        public string DataEditPanel_Text_ExpenseAdjustmentBill_Dealer {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ExpenseAdjustmentBill_Dealer;
            }
        }

        public string DataEditPanel_Text_ExpenseAdjustmentBill_IfClaimToResponsible {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ExpenseAdjustmentBill_IfClaimToResponsible;
            }
        }

        public string DataEditPanel_Text_ExpenseAdjustmentBill_ResponsibleUnit {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ExpenseAdjustmentBill_ResponsibleUnit;
            }
        }

        public string DataEditPanel_Text_ExpenseAdjustmentBill_Source {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ExpenseAdjustmentBill_Source;
            }
        }

        public string QueryPanel_Title_Dealer {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_Dealer;
            }
        }

        public string DataEditPanel_GroupTitle_ExpenseAdjustmentBillinfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_ExpenseAdjustmentBillinfo;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_ResponsibleUnitId {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_ResponsibleUnitId;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimApplication_IfOwnVehicle {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimApplication_IfOwnVehicle;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimApplication_IfUseTowTruck {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimApplication_IfUseTowTruck;
            }
        }

        public string DataEditView_GroupTitle_ServiceTripClaimApplication {
            get {
                return Resources.ServiceUIStrings.DataEditView_GroupTitle_ServiceTripClaimApplication;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimApplication_DealerName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimApplication_DealerName;
            }
        }

        public string DetailPanel_text_PartsClaimOrder_ClaimSupplierName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_PartsClaimOrder_ClaimSupplierName;
            }
        }

        public string DetailPanel_text_PartsClaimOrder_DealerName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_PartsClaimOrder_DealerName;
            }
        }

        public string DetailPanel_text_PartsClaimOrder_FaultyPartsName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_PartsClaimOrder_FaultyPartsName;
            }
        }

        public string DetailPanel_text_PartsClaimOrder_IfClaimToSupplier {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_PartsClaimOrder_IfClaimToSupplier;
            }
        }

        public string DetailPanel_text_PartsClaimOrder_MalfunctionDescription {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_PartsClaimOrder_MalfunctionDescription;
            }
        }

        public string DetailPanel_text_PartsClaimOrder_MalfunctionReason {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_PartsClaimOrder_MalfunctionReason;
            }
        }

        public string DetailPanel_text_PartsClaimOrder_MarketingDepartmentName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_PartsClaimOrder_MarketingDepartmentName;
            }
        }

        public string DetailPanel_text_PartsClaimOrder_ResponsibleUnitName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_PartsClaimOrder_ResponsibleUnitName;
            }
        }

        public string DetailPanel_text_PartsClaimOrder_FaultyPartsSupplierName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_PartsClaimOrder_FaultyPartsSupplierName;
            }
        }

        public string DetailPanel_text_PartsClaimRepairItemDetail_RepairItemName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_PartsClaimRepairItemDetail_RepairItemName;
            }
        }

        public string DetailPanel_GroupTitle_ApplicationClaimlOrder {
            get {
                return Resources.ServiceUIStrings.DetailPanel_GroupTitle_ApplicationClaimlOrder;
            }
        }

        public string DetailPanel_Title_RepairClaimApplicationFinal {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Title_RepairClaimApplicationFinal;
            }
        }

        public string DetailPanel_Title_RepairClaimApplicationInitial {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Title_RepairClaimApplicationInitial;
            }
        }

        public string DataEditView_GroupTitle_PartsClaimOrder {
            get {
                return Resources.ServiceUIStrings.DataEditView_GroupTitle_PartsClaimOrder;
            }
        }

        public string DataEditView_Text_PartsClaimOrder_PartsClaimRepairItemDetails {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_PartsClaimOrder_PartsClaimRepairItemDetails;
            }
        }

        public string DataEditPanel_GroupTitle_ServiceActivity {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_ServiceActivity;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_RepairRequestTimes {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_RepairRequestTimes;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_ServiceActivityContent {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_ServiceActivityContent;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_ServiceActivityInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_ServiceActivityInfo;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimApplication_Code {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimApplication_Code;
            }
        }

        public string DetailPanel_Text_ExpenseAdjustmentBill_Branch {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ExpenseAdjustmentBill_Branch;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_RepairRequestTimes {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_RepairRequestTimes;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_ServiceActivityInfo {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_ServiceActivityInfo;
            }
        }

        public string ActionPanel_Title_RepairClaimBill {
            get {
                return Resources.ServiceUIStrings.ActionPanel_Title_RepairClaimBill;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_Dealer {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_Dealer;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_MarketingDepartmentName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_MarketingDepartmentName;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_RepairRequestTime {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_RepairRequestTime;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_ResponsibleUnitName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_ResponsibleUnitName;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_ServiceActivityCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_ServiceActivityCode;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_ServiceRepairClaimApplicationCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_ServiceRepairClaimApplicationCode;
            }
        }

        public string DataEditView_Text_RepairClaimItemDetail_RepairItem {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairClaimItemDetail_RepairItem;
            }
        }

        public string DataEditView_Validation_RepairClaimBill_RepairRequestTimeActivityIsNull {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_RepairClaimBill_RepairRequestTimeActivityIsNull;
            }
        }

        public string DataEditView_Validation_RepairClaimBill_ServiceActivityIdIsNull {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_RepairClaimBill_ServiceActivityIdIsNull;
            }
        }

        public string DetailPanel_text_PartsClaimOrder_PartsClaimApplicationCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_text_PartsClaimOrder_PartsClaimApplicationCode;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_ClaimBillCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_ClaimBillCode;
            }
        }

        public string DataEditView_Title_RepairClaimBillDetail {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimBillDetail;
            }
        }

        public string DataEditView_Text_RepairClaimBillForApprove {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairClaimBillForApprove;
            }
        }

        public string DataEditPanel_Title_RepairClaimBillForSupplierConfirm {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Title_RepairClaimBillForSupplierConfirm;
            }
        }

        public string DataEditPanel_GroupTitle_ActivityInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_ActivityInfo;
            }
        }

        public string DataEditView_Title_ServicesClaimBill {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_ServicesClaimBill;
            }
        }

        public string DataEditView_Title_RepairClaimMaterialDetail {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimMaterialDetail;
            }
        }

        public string QueryPanel_Title_ServiceTripRegion {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_ServiceTripRegion;
            }
        }

        public string DataEditView_Text_SsClaimSettlementBill_DealerName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_SsClaimSettlementBill_DealerName;
            }
        }

        public string DataDetailPanel_GroupTitle_SsClaimSettlementBillinfo {
            get {
                return Resources.ServiceUIStrings.DataDetailPanel_GroupTitle_SsClaimSettlementBillinfo;
            }
        }

        public string DetailPanel_Text_SsClaimSettlementBill_Branch {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SsClaimSettlementBill_Branch;
            }
        }

        public string DetailPanel_Text_SsClaimSettlementBill_Dealer {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SsClaimSettlementBill_Dealer;
            }
        }

        public string DataEditPanel_GroupTitle_SettleInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_SettleInfo;
            }
        }

        public string DataEditView_Title_SsClaimSettlementBill {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_SsClaimSettlementBill;
            }
        }

        public string QueryPanel_QueryItem_Title_SsClaimSettlementBill_BranchId {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_SsClaimSettlementBill_BranchId;
            }
        }

        public string DetailPanel_Text_SsClaimSettlementBill_BranchName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SsClaimSettlementBill_BranchName;
            }
        }

        public string DetailPanel_Text_SsClaimSettlementBill_DealerName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SsClaimSettlementBill_DealerName;
            }
        }

        public string DetailPanel_Text_SsClaimSettlementBill_IfInvoiced {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SsClaimSettlementBill_IfInvoiced;
            }
        }

        public string DataEditView_Text_SsClaimSettleInstruction_Attention {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_SsClaimSettleInstruction_Attention;
            }
        }

        public string DetailPanel_Text_PartsPurchaseSettleBill_Code {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsPurchaseSettleBill_Code;
            }
        }

        public string DataEditPanel_Text_UsedPartsLoanBill_IfReturned {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsLoanBill_IfReturned;
            }
        }

        public string QueryPanel_Title_DealerForBranch {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_DealerForBranch;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_ServiceProductLineId {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_ServiceProductLineId;
            }
        }

        public string DataEditPanel_GroupTitle_ServicesActivityInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_ServicesActivityInfo;
            }
        }

        public string DataEditView_Title_SsClaimSettlementBill_AccountGroupName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_SsClaimSettlementBill_AccountGroupName;
            }
        }

        public string DataEditView_Title_SsClaimSettlementBill_Amount {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_SsClaimSettlementBill_Amount;
            }
        }

        public string DataEditPanel_Text_UsedPartsReturnOrder_DepartmentName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsReturnOrder_DepartmentName;
            }
        }

        public string DataEditPanel_Text_UsedPartsReturnOrder_ResponsibleUnitName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsReturnOrder_ResponsibleUnitName;
            }
        }

        public string DataEditPanel_Text_UsedPartsReturnOrder_SupplierName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsReturnOrder_SupplierName;
            }
        }

        public string DataEditPanel_Text_UsedPartsReturnOrder_WarehouseName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsReturnOrder_WarehouseName;
            }
        }

        public string DataEditView_Title_MaintainClaimBill {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_MaintainClaimBill;
            }
        }

        public string DetailPanel_Text_MarketingDepartment_Names {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_MarketingDepartment_Names;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_PartsSalesCategoryName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_PartsSalesCategoryName;
            }
        }

        public string DetailPanel_Text_RepairObject_RepairObject {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairObject_RepairObject;
            }
        }

        public string DetailPanel_Text_SalesRegion_RegionName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SalesRegion_RegionName;
            }
        }

        public string DataEditPanel_Text_UsedPartsWarehouse_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsWarehouse_PartsSalesCategory;
            }
        }

        public string DataEditView_Title_SsUsedPartsStorage_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_SsUsedPartsStorage_PartsSalesCategory;
            }
        }

        public string QueryPanel_QueryItem_Title_Common_PartsSalesCategoryName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_Common_PartsSalesCategoryName;
            }
        }

        public string DataEditView_Text_ServiceTripClaimApplication_CountyName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_ServiceTripClaimApplication_CountyName;
            }
        }

        public string ServiceTripClaimApplication_GoOutTrouble {
            get {
                return Resources.ServiceUIStrings.ServiceTripClaimApplication_GoOutTrouble;
            }
        }

        public string ServiceTripClaimApplication_MarketDepartment {
            get {
                return Resources.ServiceUIStrings.ServiceTripClaimApplication_MarketDepartment;
            }
        }

        public string ServiceTripClaimApplication_SalesRegion {
            get {
                return Resources.ServiceUIStrings.ServiceTripClaimApplication_SalesRegion;
            }
        }

        public string DataEditView_Text_ServiceTripClaimApplication_PartsSalesCategoryName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_ServiceTripClaimApplication_PartsSalesCategoryName;
            }
        }

        public string DetailPanel_RadButton_SelectPhoto {
            get {
                return Resources.ServiceUIStrings.DetailPanel_RadButton_SelectPhoto;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_Countersign {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_Countersign;
            }
        }

        public string DataEditView_Title_RepairClaimApplicationCountersign {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplicationCountersign;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_FirstClassStationName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_FirstClassStationName;
            }
        }

        public string DetailPanel_Text_FirstClassStationName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_FirstClassStationName;
            }
        }

        public string DetailPanel_Text_MarketDepartmentName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_MarketDepartmentName;
            }
        }

        public string DetailPanel_Text_Region_City {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_Region_City;
            }
        }

        public string DetailPanel_Text_Region_District {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_Region_District;
            }
        }

        public string DetailPanel_Text_Region_Province {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_Region_Province;
            }
        }

        public string DetailPanel_Text_Region_Region {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_Region_Region;
            }
        }

        public string DetailPanel_Text_SalesRegion {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SalesRegion;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimApplication_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimApplication_PartsSalesCategory;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimApplication_RepairObject {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimApplication_RepairObject;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_Audit {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_Audit;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_ApproveComment {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_ApproveComment;
            }
        }

        public string DataEditPanel_Text_Button_Cancel {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_Button_Cancel;
            }
        }

        public string DataEditPanel_Text_Button_Submit {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_Button_Submit;
            }
        }

        public string DetailPanel_Text_PartsClaimApplication_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsClaimApplication_PartsSalesCategory;
            }
        }

        public string DetailPanel_Text_PartsClaimApplication_SalesRegion {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsClaimApplication_SalesRegion;
            }
        }

        public string DataEditView_Text_PartsClaimApplication_MarketDepartment {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_PartsClaimApplication_MarketDepartment;
            }
        }

        public string QueryPanel_QueryItem_Title_MarketingDepartment_Name {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_MarketingDepartment_Name;
            }
        }

        public string QueryPanel_QueryItem_Title_SalesRegion_RegionName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_SalesRegion_RegionName;
            }
        }

        public string DataEditView_Text_ServiceTripClaimApplication_ServicePartsSalesCategoryName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_ServiceTripClaimApplication_ServicePartsSalesCategoryName;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_FirstClassStation {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_FirstClassStation;
            }
        }

        public string DetailPanel_GroupTitle_Image {
            get {
                return Resources.ServiceUIStrings.DetailPanel_GroupTitle_Image;
            }
        }

        public string DetailPanel_GroupTitle_Preview_Image {
            get {
                return Resources.ServiceUIStrings.DetailPanel_GroupTitle_Preview_Image;
            }
        }

        public string DataGridView_ColumnItem_Title_RepairObject_RepairTarget {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_RepairObject_RepairTarget;
            }
        }

        public string DataEditView_Title_RepairClaimApplicationAudit {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplicationAudit;
            }
        }

        public string DataEditView_Title_RepairClaimApplicationSupplierApprove {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplicationSupplierApprove;
            }
        }

        public string DataEditView_Title_RepairClaimApplicationSupplierForApprove {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplicationSupplierForApprove;
            }
        }

        public string DataEditView_Title_RepairClaimApplicationSupplierInitialApprove {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplicationSupplierInitialApprove;
            }
        }

        public string DataEditPanel_Text_PartsClaimApplication_RepairRequestOrderCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimApplication_RepairRequestOrderCode;
            }
        }

        public string DataEditPanel_Text_PartsClaimApplication_SalesRegion {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimApplication_SalesRegion;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_ApproverName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_ApproverName;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_ApproveTime {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_ApproveTime;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_CheckerName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_CheckerName;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_CheckTime {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_CheckTime;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_SupApprover {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_SupApprover;
            }
        }

        public string DataEditPanel_Text_RepairClaimApplication_SupApproverTime {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimApplication_SupApproverTime;
            }
        }

        public string DataEditView_Text_ServicesClaimApplication_PartsSalesCategoryName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_ServicesClaimApplication_PartsSalesCategoryName;
            }
        }

        public string DataEditView_Title_MaintainClaimApplicationApprove {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_MaintainClaimApplicationApprove;
            }
        }

        public string DataEditView_Title_MaintainClaimApplicationAudit {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_MaintainClaimApplicationAudit;
            }
        }

        public string DataEditView_Title_MaintainClaimApplicationCountersign {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_MaintainClaimApplicationCountersign;
            }
        }

        public string DataEditView_Title_MaintainClaimApplicationForApprove {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_MaintainClaimApplicationForApprove;
            }
        }

        public string DataEditView_Title_ServicesClaimApplication {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_ServicesClaimApplication;
            }
        }

        public string DataEditView_Title_ServicesClaimApplicationAudit {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_ServicesClaimApplicationAudit;
            }
        }

        public string DataEditView_Title_ServicesClaimApplicationCountersign {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_ServicesClaimApplicationCountersign;
            }
        }

        public string DetailPanel_Text_ServicesClaimApplication_FirstClassStationName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServicesClaimApplication_FirstClassStationName;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_MarketingDepartment {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_MarketingDepartment;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_PartsSalesCategory;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_RepairObject {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_RepairObject;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_SalesRegion {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_SalesRegion;
            }
        }

        public string ServiceTripClaimBill_ServiceTripClaimAppCode {
            get {
                return Resources.ServiceUIStrings.ServiceTripClaimBill_ServiceTripClaimAppCode;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_Area {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_Area;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_OneLevelDealerName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_OneLevelDealerName;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_ServiceTripRegion {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_ServiceTripRegion;
            }
        }

        public string DataEditView_Validation_ServiceTripClaimBill_VINIsNull {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_ServiceTripClaimBill_VINIsNull;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_CountyName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_CountyName;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_ServiceTripClaimAppCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_ServiceTripClaimAppCode;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_ApprovalComment {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_ApprovalComment;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_ApproverName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_ApproverName;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_ApproveTime {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_ApproveTime;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_RepairClaimBill_ClaimBillCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_RepairClaimBill_ClaimBillCode;
            }
        }

        public string QueryPanel_QueryItem_Tilte_RepairClaimBill_MarketingDepartment {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Tilte_RepairClaimBill_MarketingDepartment;
            }
        }

        public string QueryPanel_QueryItem_Tilte_RepairClaimBill_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Tilte_RepairClaimBill_PartsSalesCategory;
            }
        }

        public string QueryPanel_QueryItem_Tilte_RepairClaimBill_SalesRegion {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Tilte_RepairClaimBill_SalesRegion;
            }
        }

        public string DataPanel_Text_RepairClaimBill_RepairObject {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_RepairObject;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_FirstClassStationCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_FirstClassStationCode;
            }
        }

        public string DetailPanel_Text_RepairClaimBill_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimBill_PartsSalesCategory;
            }
        }

        public string DataPanel_Text_RepairClaimBill_SalesRegion {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_SalesRegion;
            }
        }

        public string DataEditView_Text_ServiceTripClaimApplication_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_ServiceTripClaimApplication_PartsSalesCategory;
            }
        }

        public string DataGridView_ColumnItem_Title_Common_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_Common_PartsSalesCategory;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsClaimOrder_MarketingDepartmentName {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrder_MarketingDepartmentName;
            }
        }

        public string DataGridView_ColumnItem_Title_PartsClaimOrder_SalesRegionRegionName {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrder_SalesRegionRegionName;
            }
        }

        public string QueryPanel_QueryItem_Title_PartsClaimOrder_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_PartsClaimOrder_PartsSalesCategory;
            }
        }

        public string QueryPanel_QueryItem_Title_RepairClaimBill_PartsSalesCategoryName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_RepairClaimBill_PartsSalesCategoryName;
            }
        }

        public string QueryPanel_QueryItem_Title_RepairClaimBill_RepairObjectRepairTarget {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_RepairClaimBill_RepairObjectRepairTarget;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_PartsSalesCategoryId {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_PartsSalesCategoryId;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_SalesRegionId {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_MaintainClaimBill_SalesRegionId;
            }
        }

        public string DataEditPanel_Text_PartsClaimApplication_FirstClassStation {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimApplication_FirstClassStation;
            }
        }

        public string DataPanel_Text_RepairClaimBill_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_PartsSalesCategory;
            }
        }

        public string DataEditPanel_GroupTitle_MainInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_MainInfo;
            }
        }

        public string DataEditPanel_Text_RepairOrder_BranchName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_BranchName;
            }
        }

        public string DataEditPanel_Text_RepairOrder_ServiceProductLineName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_ServiceProductLineName;
            }
        }

        public string DataEditPanel_Text_RepairOrder_Title {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_Title;
            }
        }

        public string DataEditPanel_Text_RepairOrder_VIN {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_VIN;
            }
        }

        public string DataEditPanel_GroupTitle_FaultReason {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_FaultReason;
            }
        }

        public string QueryPanel_Title_RepairOrder_ServiceProductLineId {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_RepairOrder_ServiceProductLineId;
            }
        }

        public string DataEditPanel_GroupTitle_PrimaryInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_PrimaryInfo;
            }
        }

        public string DataEditView_GroupTitle_CustomerAccountInfo {
            get {
                return Resources.ServiceUIStrings.DataEditView_GroupTitle_CustomerAccountInfo;
            }
        }

        public string DataEditView_GroupTitle_OrderReceivingInfo {
            get {
                return Resources.ServiceUIStrings.DataEditView_GroupTitle_OrderReceivingInfo;
            }
        }

        public string DataEditView_Text_CustomerAccount_UseablePosition {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_CustomerAccount_UseablePosition;
            }
        }

        public string DetailPanel_Text_CustomerAccount_CustomerCredenceAmount {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_CustomerAccount_CustomerCredenceAmount;
            }
        }

        public string DetailPanel_Text_PartsSalesOrder_PartsSalesOrderTypeName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_PartsSalesOrder_PartsSalesOrderTypeName;
            }
        }

        public string DataEditView_Title_PartsSalesOrder_SecondLevelOrderType {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_PartsSalesOrder_SecondLevelOrderType;
            }
        }

        public string DataEditPanel_Text_RepairOrder_PartsSalesCategoryName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_PartsSalesCategoryName;
            }
        }

        public string DataEditPanel_Text_RepairOrder_IsRoadVehicl {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_IsRoadVehicl;
            }
        }

        public string DataEditPanel_Text_RepairOrder_RepairObjectId {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_RepairObjectId;
            }
        }

        public string DataEditPanel_Text_RepairOrder_VehicleContactPerson {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_VehicleContactPerson;
            }
        }

        public string DataEditPanel_Text_RepairOrder_VehicleMaintenancePoilcy {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_VehicleMaintenancePoilcy;
            }
        }

        public string DataEditPanel_GroupTitle_OutClaimInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_OutClaimInfo;
            }
        }

        public string DataEditPanel_Text_RepairOrder_CauseAnalyse {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_CauseAnalyse;
            }
        }

        public string DataEditPanel_Text_RepairOrder_CountyName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_CountyName;
            }
        }

        public string DataEditPanel_Text_RepairOrder_DealerName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_DealerName;
            }
        }

        public string DataEditPanel_Text_RepairOrder_FieldServiceCharge {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_FieldServiceCharge;
            }
        }

        public string DataEditPanel_Text_RepairOrder_IfUseOwnVehicle {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_IfUseOwnVehicle;
            }
        }

        public string DataEditPanel_Text_RepairOrder_IfUseTowTruck {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_IfUseTowTruck;
            }
        }

        public string DataEditPanel_Text_RepairOrder_OutClaimSupplier {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_OutClaimSupplier;
            }
        }

        public string DataEditPanel_Text_RepairOrder_Region {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_Region;
            }
        }

        public string DataEditPanel_Text_RepairOrder_ServiceTripClaimApp {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_ServiceTripClaimApp;
            }
        }

        public string DataEditPanel_Text_RepairOrder_TowCharge {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_TowCharge;
            }
        }

        public string DataEditPanel_Text_RepairOrder_TrimCost {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_TrimCost;
            }
        }

        public string DataEditPanel_Text_RepairOrderFaultReason_ClaimSupplier {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrderFaultReason_ClaimSupplier;
            }
        }

        public string DataEditPanel_Text_RepairOrderFaultReason_FaultyPartsApp {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrderFaultReason_FaultyPartsApp;
            }
        }

        public string DataEditPanel_Text_RepairOrderFaultReason_FaultyPartsSupplier {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrderFaultReason_FaultyPartsSupplier;
            }
        }

        public string DataEditPanel_Text_RepairOrderFaultReason_MalfunctionDescription {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrderFaultReason_MalfunctionDescription;
            }
        }

        public string DataEditPanel_Text_RepairOrderFaultReason_RepairClaimApplication {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrderFaultReason_RepairClaimApplication;
            }
        }

        public string DataEditPanel_Text_RepairOrderFaultReason_RepairTemplet {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrderFaultReason_RepairTemplet;
            }
        }

        public string DataEditPanel_Text_RepairOrderFaultReason_TempSupplier {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrderFaultReason_TempSupplier;
            }
        }

        public string DataEditPanel_Text_RepairOrderItemDetail_RepairItemName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrderItemDetail_RepairItemName;
            }
        }

        public string DataGridView_ColumnItem_Title_RepairOrderMaterialDetail_NewPartsSupplierName {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_RepairOrderMaterialDetail_NewPartsSupplierName;
            }
        }

        public string QueryPanel_Title_RepairClaimApplicationName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_RepairClaimApplicationName;
            }
        }

        public string QueryPanel_Title_ServiceTripClaimAppCode {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_ServiceTripClaimAppCode;
            }
        }

        public string QueryPanel_Title_MalfunctionDescription {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_MalfunctionDescription;
            }
        }

        public string QueryPanel_Title_FaultyPartsName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_FaultyPartsName;
            }
        }

        public string Button_Text_Common_Search {
            get {
                return Resources.ServiceUIStrings.Button_Text_Common_Search;
            }
        }

        public string DataEditView_Text_MalfunctionView_MalfunctionCategoryCode {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_MalfunctionView_MalfunctionCategoryCode;
            }
        }

        public string DataEditView_Text_MalfunctionView_MalfunctionCategoryName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_MalfunctionView_MalfunctionCategoryName;
            }
        }

        public string DataEditView_TabItemHeader_DataGridQuery {
            get {
                return Resources.ServiceUIStrings.DataEditView_TabItemHeader_DataGridQuery;
            }
        }

        public string DataEditView_TabItemHeader_TreeQuery {
            get {
                return Resources.ServiceUIStrings.DataEditView_TabItemHeader_TreeQuery;
            }
        }

        public string QueryPanel_Title_RepairTempletName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_RepairTempletName;
            }
        }

        public string QueryPanel_Title_TempSupplierName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_TempSupplierName;
            }
        }

        public string QueryPanel_Title_RepairItemName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_RepairItemName;
            }
        }

        public string DataEditView_Validation_PartsOuterPurchaseChange_OuterPurchasePrice {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_PartsOuterPurchaseChange_OuterPurchasePrice;
            }
        }

        public string DataEditView_Text_PartsSalesOrder_ReceiveSaleCateName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_PartsSalesOrder_ReceiveSaleCateName;
            }
        }

        public string DataEditView_Text_PartsSalesOrder_ReceivingWarehouseName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_PartsSalesOrder_ReceivingWarehouseName;
            }
        }

        public string DataEditPanel_Text_RepairOrder_FirstClassStationName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_FirstClassStationName;
            }
        }

        public string DataEditPanel_Text_RepairOrder_RepairWorkOrder {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_RepairWorkOrder;
            }
        }

        public string QueryPanel_Title_RepairOrderForHistory {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_RepairOrderForHistory;
            }
        }

        public string QueryPanel_Title_RepairOrderForInterfix {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_RepairOrderForInterfix;
            }
        }

        public string DataManagementView_Title_SalesBeyoundCheckForDealer {
            get {
                return Resources.ServiceUIStrings.DataManagementView_Title_SalesBeyoundCheckForDealer;
            }
        }

        public string Validation_PreSaleCheckOrder_VehicleBranchIsNotExist {
            get {
                return Resources.ServiceUIStrings.Validation_PreSaleCheckOrder_VehicleBranchIsNotExist;
            }
        }

        public string Validation_PreSaleCheckOrder_VehicleServiceProductLineIsNotExist {
            get {
                return Resources.ServiceUIStrings.Validation_PreSaleCheckOrder_VehicleServiceProductLineIsNotExist;
            }
        }

        public string DataGridView_ColumnItem_Title_PreSaleCheckOrder_DealerName {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_PreSaleCheckOrder_DealerName;
            }
        }

        public string QueryPanel_Title_PreSaleCheckOrder {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_PreSaleCheckOrder;
            }
        }

        public string DataEditPanel_Text_PreSaleCheckOrder_DealerCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PreSaleCheckOrder_DealerCode;
            }
        }

        public string DataEditPanel_Text_PreSaleCheckOrder_PartsSalesCategoryName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PreSaleCheckOrder_PartsSalesCategoryName;
            }
        }

        public string DataEditPanel_Text_PreSaleCheckOrder_ResponsibleUnit {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PreSaleCheckOrder_ResponsibleUnit;
            }
        }

        public string DataManagementView_Title_PreSaleItem {
            get {
                return Resources.ServiceUIStrings.DataManagementView_Title_PreSaleItem;
            }
        }

        public string RepairClaimBill_RejectQty {
            get {
                return Resources.ServiceUIStrings.RepairClaimBill_RejectQty;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_EngineSerialNumber {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_EngineSerialNumber;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_GearSerialNumber {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_GearSerialNumber;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_VehicleMaintenancePoilcy {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_VehicleMaintenancePoilcy;
            }
        }

        public string DataEditPanel_Text_RepairOrder_AreaId {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_AreaId;
            }
        }

        public string DataEditPanel_Text_RepairOrder_MarketingDepartmentId {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_MarketingDepartmentId;
            }
        }

        public string QueryPanel_QueryItem_Title_ExpenseAdjustmentBill_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_ExpenseAdjustmentBill_PartsSalesCategory;
            }
        }

        public string DataEditPanel_Text_ExpenseAdjustmentBill_AndGenerate {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ExpenseAdjustmentBill_AndGenerate;
            }
        }

        public string DataEditPanel_Text_Common_SupplierConfirmStatus {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_Common_SupplierConfirmStatus;
            }
        }

        public string DataPanel_Text_ExpenseAdjustmentBill_IfExpense {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_ExpenseAdjustmentBill_IfExpense;
            }
        }

        public string RepairClaimBill_FinalApproverComment {
            get {
                return Resources.ServiceUIStrings.RepairClaimBill_FinalApproverComment;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimBill_MarketingDepartments {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimBill_MarketingDepartments;
            }
        }

        public string DataEditView_GroupTitle_ServiceTripClaimBills {
            get {
                return Resources.ServiceUIStrings.DataEditView_GroupTitle_ServiceTripClaimBills;
            }
        }

        public string DetailPanel_ServiceTripClaimBill_FieldServiceCharges {
            get {
                return Resources.ServiceUIStrings.DetailPanel_ServiceTripClaimBill_FieldServiceCharges;
            }
        }

        public string DetailPanel_ServiceTripClaimBill_FinishingTimes {
            get {
                return Resources.ServiceUIStrings.DetailPanel_ServiceTripClaimBill_FinishingTimes;
            }
        }

        public string DetailPanel_ServiceTripClaimBill_Mileages {
            get {
                return Resources.ServiceUIStrings.DetailPanel_ServiceTripClaimBill_Mileages;
            }
        }

        public string DetailPanel_ServiceTripClaimBill_RejectReasons {
            get {
                return Resources.ServiceUIStrings.DetailPanel_ServiceTripClaimBill_RejectReasons;
            }
        }

        public string DetailPanel_ServiceTripClaimBill_SalesStatus {
            get {
                return Resources.ServiceUIStrings.DetailPanel_ServiceTripClaimBill_SalesStatus;
            }
        }

        public string DetailPanel_ServiceTripClaimBill_SupplierApproverName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_ServiceTripClaimBill_SupplierApproverName;
            }
        }

        public string DetailPanel_ServiceTripClaimBill_SupplierApproveTime {
            get {
                return Resources.ServiceUIStrings.DetailPanel_ServiceTripClaimBill_SupplierApproveTime;
            }
        }

        public string DetailPanel_ServiceTripClaimBill_TowCharges {
            get {
                return Resources.ServiceUIStrings.DetailPanel_ServiceTripClaimBill_TowCharges;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_Approve {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_Approve;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_ApproverNames {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_ApproverNames;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_ApproveTimes {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_ApproveTimes;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_ServiceTripClaimAppCodes {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_ServiceTripClaimAppCodes;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_ServiceDepartmentComment {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_ServiceDepartmentComment;
            }
        }

        public string DataEditPanel_Text_RepairClaimItemDetail_ApproveStatus {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimItemDetail_ApproveStatus;
            }
        }

        public string QueryPanel_Title_VehicleContactPerson {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_VehicleContactPerson;
            }
        }

        public string DataEditView_Title_RepairTemplet_Name {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairTemplet_Name;
            }
        }

        public string DataEditView_Title_RepairTemplet_TempletType {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairTemplet_TempletType;
            }
        }

        public string QueryPanel_QueryItem_Title_RepairClaimBill_VehicleInformationVIN {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_RepairClaimBill_VehicleInformationVIN;
            }
        }

        public string DetailPanel_GroupTitle_OutInfos {
            get {
                return Resources.ServiceUIStrings.DetailPanel_GroupTitle_OutInfos;
            }
        }

        public string DetailPanel_ServiceTripClaimBill_SupplierApproveComment {
            get {
                return Resources.ServiceUIStrings.DetailPanel_ServiceTripClaimBill_SupplierApproveComment;
            }
        }

        public string DetailPanel_ServiceTripClaimBill_SupplierApproverNames {
            get {
                return Resources.ServiceUIStrings.DetailPanel_ServiceTripClaimBill_SupplierApproverNames;
            }
        }

        public string DetailPanel_ServiceTripClaimBill_SupplierApproveTimes {
            get {
                return Resources.ServiceUIStrings.DetailPanel_ServiceTripClaimBill_SupplierApproveTimes;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_Codes {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_Codes;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_RepairClaimBill_ClaimBillCodes {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_RepairClaimBill_ClaimBillCodes;
            }
        }

        public string QueryPanel_Title_RepairWorkerName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_RepairWorkerName;
            }
        }

        public string DataEditView_Title_RepairTemplet_Code {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairTemplet_Code;
            }
        }

        public string DataManagementView_Title_RepairTemplet {
            get {
                return Resources.ServiceUIStrings.DataManagementView_Title_RepairTemplet;
            }
        }

        public string DataEditViewPanel_Text_ServiceTripClaimApplication_SubDealer {
            get {
                return Resources.ServiceUIStrings.DataEditViewPanel_Text_ServiceTripClaimApplication_SubDealer;
            }
        }

        public string DataEditPanel_Text_RepairOrderItemDetail_RepairWorkerName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrderItemDetail_RepairWorkerName;
            }
        }

        public string DataEditPanel_Text_RepairOrder_RepairWorkOrderCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_RepairWorkOrderCode;
            }
        }

        public string DetailPanel_GroupTitle_File {
            get {
                return Resources.ServiceUIStrings.DetailPanel_GroupTitle_File;
            }
        }

        public string DataEditPanel_Text_RepairOrder_RepairAddition {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_RepairAddition;
            }
        }

        public string DataEditPanel_Text_RepairOrder_DealerCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_DealerCode;
            }
        }

        public string QueryPanel_QueryItem_Title_VehicleInformation_VIN {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_VehicleInformation_VIN;
            }
        }

        public string DataEditPanel_Text_RepairOrder_ResponsibleUnit {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_ResponsibleUnit;
            }
        }

        public string DataEditPanel_Text_RepairOrder_Code {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_Code;
            }
        }

        public string DataEditPanel_Text_RepairOrder_OutPersonCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrder_OutPersonCode;
            }
        }

        public string DataEditPanel_Text_VehicleMainteTerm_RepairWorkerName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_VehicleMainteTerm_RepairWorkerName;
            }
        }

        public string DataEditPanel_Text_RepairOrderFaultReason_OutFaultReason {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairOrderFaultReason_OutFaultReason;
            }
        }

        public string DetailPanel_Text_ExpenseAdjustmentBill_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ExpenseAdjustmentBill_PartsSalesCategory;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimBill_SupplierCheckComment {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimBill_SupplierCheckComment;
            }
        }

        public string DataEditView_Text_DealerInvoiceInformation_BranchName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_DealerInvoiceInformation_BranchName;
            }
        }

        public string DataEditView_Text_DealerInvoiceInformation_DealerName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_DealerInvoiceInformation_DealerName;
            }
        }

        public string DataEditView_Text_DealerInvoiceInformation_Approver {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_DealerInvoiceInformation_Approver;
            }
        }

        public string DataEditView_Text_DealerInvoiceInformation_InitialApproverComment {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_DealerInvoiceInformation_InitialApproverComment;
            }
        }

        public string DataEditView_Text_DealerInvoiceInformation_ApproverComment {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_DealerInvoiceInformation_ApproverComment;
            }
        }

        public string DataEditView_Text_DealerInvoiceInformation_Comment {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_DealerInvoiceInformation_Comment;
            }
        }

        public string DataEditView_Text_UsedPartsCompany {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_UsedPartsCompany;
            }
        }

        public string DataEditView_Text_UsedPartsDistanceInfor_OriginCompanyName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_UsedPartsDistanceInfor_OriginCompanyName;
            }
        }

        public string DataEditView_Text_UsedPartsDistanceInfor_Remark {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_UsedPartsDistanceInfor_Remark;
            }
        }

        public string DataEditView_Text_UsedPartsDistanceInfor_ShippingDistance {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_UsedPartsDistanceInfor_ShippingDistance;
            }
        }

        public string DataEditView_Text_UsedPartsDistanceInfor_UsedPartsWarehouseName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_UsedPartsDistanceInfor_UsedPartsWarehouseName;
            }
        }

        public string DataEditPanel_Dealear_DealearInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Dealear_DealearInfo;
            }
        }

        public string DataEditPanel_Dealear_MalfunctionInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Dealear_MalfunctionInfo;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_MalfunctionDescription {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_MalfunctionDescription;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_RepairTempletCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_RepairTempletCode;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_RepairTempletName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_RepairTempletName;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_TempSupplierName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_TempSupplierName;
            }
        }

        public string DataPanel_Text_RepairClaimBill_ClaimType {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_ClaimType;
            }
        }

        public string DataPanel_Text_RepairClaimBill_MalfunctionCode {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_MalfunctionCode;
            }
        }

        public string DataPanel_Text_RepairClaimBill_RepairType {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_RepairType;
            }
        }

        public string DataPanel_Text_RepairClaimBill_VehicleInformation_FirstStoppageMileage {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_VehicleInformation_FirstStoppageMileage;
            }
        }

        public string DataPanel_Text_RepairClaimBill_VehicleInformation_SerialNumber {
            get {
                return Resources.ServiceUIStrings.DataPanel_Text_RepairClaimBill_VehicleInformation_SerialNumber;
            }
        }

        public string RepairClaimBill_SupplierCheckComment {
            get {
                return Resources.ServiceUIStrings.RepairClaimBill_SupplierCheckComment;
            }
        }

        public string RepairClaimBill_SupplierCheckerName {
            get {
                return Resources.ServiceUIStrings.RepairClaimBill_SupplierCheckerName;
            }
        }

        public string RepairClaimBill_SupplierCheckStatus {
            get {
                return Resources.ServiceUIStrings.RepairClaimBill_SupplierCheckStatus;
            }
        }

        public string RepairClaimBill_SupplierCheckTime {
            get {
                return Resources.ServiceUIStrings.RepairClaimBill_SupplierCheckTime;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_ServiceStation {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_ServiceStation;
            }
        }

        public string DetailPanel_Text_RepairClaimApplication_DealerCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairClaimApplication_DealerCode;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsDistanceInfor1_UsedPartsWarehouseCode {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor1_UsedPartsWarehouseCode;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsDistanceInfor1_UsedPartsWarehouseName {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor1_UsedPartsWarehouseName;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_Company1Code {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_Company1Code;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_Company1Name {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_Company1Name;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_CompanyName {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_CompanyName;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_UsedPartsWarehouseName {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_UsedPartsWarehouseName;
            }
        }

        public string DataEditPanel_Text_Common_FinalApproveCommentStatus {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_Common_FinalApproveCommentStatus;
            }
        }

        public string Countersignature_SourceCode {
            get {
                return Resources.ServiceUIStrings.Countersignature_SourceCode;
            }
        }

        public string Countersignature_SourceType {
            get {
                return Resources.ServiceUIStrings.Countersignature_SourceType;
            }
        }

        public string Countersignature_Type {
            get {
                return Resources.ServiceUIStrings.Countersignature_Type;
            }
        }

        public string DataEditPanel_GroupTitle_VehicleCustomerInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_VehicleCustomerInfo;
            }
        }

        public string DataEditPanel_GroupTitle_VehicleFault {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_VehicleFault;
            }
        }

        public string DataEditView_GroupTitle_RepairOrderDetail_Repair {
            get {
                return Resources.ServiceUIStrings.DataEditView_GroupTitle_RepairOrderDetail_Repair;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_BranchName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_BranchName;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_BridgeType {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_BridgeType;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_CustomOpinion {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_CustomOpinion;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_DealResult {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_DealResult;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_FaultDiscribe {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_FaultDiscribe;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_Person {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_Person;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_PersonTel {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_PersonTel;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_ProductCode {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_ProductCode;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_ProductFactory {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_ProductFactory;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_ProductType {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_ProductType;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_RepairType {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_RepairType;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_SalesStatus {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_SalesStatus;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_useIntroduction {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_useIntroduction;
            }
        }

        public string DataEditView_Text_RepairOrderDetail_VideoNumber {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairOrderDetail_VideoNumber;
            }
        }

        public string DetailPanel_GroupTitle_EngineInfo {
            get {
                return Resources.ServiceUIStrings.DetailPanel_GroupTitle_EngineInfo;
            }
        }

        public string DetailPanel_Text_RepairOrderDetail_EngineApplicationArea {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderDetail_EngineApplicationArea;
            }
        }

        public string DetailPanel_Text_RepairOrderDetail_EngineClaim {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderDetail_EngineClaim;
            }
        }

        public string DetailPanel_Text_RepairOrderDetail_EngineDamagedCondition {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderDetail_EngineDamagedCondition;
            }
        }

        public string DetailPanel_Text_RepairOrderDetail_EngineWarrantyProperties {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderDetail_EngineWarrantyProperties;
            }
        }

        public string DetailPanel_Text_RepairOrderInfo_CarCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderInfo_CarCode;
            }
        }

        public string DetailPanel_Text_RepairOrderInfo_IsOrNot {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderInfo_IsOrNot;
            }
        }

        public string DetailPanel_Text_RepairOrderInfo_OtherCost {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderInfo_OtherCost;
            }
        }

        public string DetailPanel_Text_RepairOrderInfo_OtherCostIntroduction {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderInfo_OtherCostIntroduction;
            }
        }

        public string DetailPanel_Text_RepairOrderInfo_OtherTransportation {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderInfo_OtherTransportation;
            }
        }

        public string DetailPanel_Text_RepairOrderInfo_OutMileage {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderInfo_OutMileage;
            }
        }

        public string DetailPanel_Text_RepairOrderInfo_OutTimeType {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderInfo_OutTimeType;
            }
        }

        public string DetailPanel_Text_RepairOrderInfo_PersonUnitPrice {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderInfo_PersonUnitPrice;
            }
        }

        public string DetailPanel_Text_RepairOrderInfo_TotalCost {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderInfo_TotalCost;
            }
        }

        public string DetailPanel_Text_RepairOrderInfo_UnitPrice {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderInfo_UnitPrice;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_FaultyPartsCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_FaultyPartsCode;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_FaultyPartsSerialNumber {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_FaultyPartsSerialNumber;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_OutCost {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_OutCost;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_SupplierCode {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_SupplierCode;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_SupplierName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_SupplierName;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_TotalFaultyPartsSerialNumber {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_TotalFaultyPartsSerialNumber;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_ActualLaborCost {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_ActualLaborCost;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_ActualMaterialCost {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_ActualMaterialCost;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_StandardCost {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_StandardCost;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_StandardMaterialCost {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_StandardMaterialCost;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_TotalActualCost {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_TotalActualCost;
            }
        }

        public string DetailPanel_Text_RepairOrderFaultReasonInfo_TotalStandardCost {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_RepairOrderFaultReasonInfo_TotalStandardCost;
            }
        }

        public string DataGridView_ColumnItem_Title_Common_ActuralMaterialCost {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_Common_ActuralMaterialCost;
            }
        }

        public string DataGridView_ColumnItem_Title_Common_NewPartsCode {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_Common_NewPartsCode;
            }
        }

        public string DataGridView_ColumnItem_Title_Common_NewPartsName {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_Common_NewPartsName;
            }
        }

        public string DataGridView_ColumnItem_Title_Common_UsedPartsCodes {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_Common_UsedPartsCodes;
            }
        }

        public string DataEditView_Text_RepairClaimApplication_ApplicationInfo {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairClaimApplication_ApplicationInfo;
            }
        }

        public string DataEditView_Text_RepairClaimApplication_Applicant {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairClaimApplication_Applicant;
            }
        }

        public string DataEditView_Title_RepairClaimApplication_BridgeType {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplication_BridgeType;
            }
        }

        public string DataEditView_Title_RepairClaimApplication_Code {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplication_Code;
            }
        }

        public string DataEditView_Title_RepairClaimApplication_EngineModel {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplication_EngineModel;
            }
        }

        public string DataEditView_Title_RepairClaimApplication_RepairObject {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplication_RepairObject;
            }
        }

        public string DataEditView_Title_RepairClaimApplication_ServiceProductLine {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplication_ServiceProductLine;
            }
        }

        public string DataEditView_Title_RepairClaimApplication_Status {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplication_Status;
            }
        }

        public string DataEditView_Title_RepairClaimApplication_VehicleCategoryName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplication_VehicleCategoryName;
            }
        }

        public string DataEditView_Title_RepairClaimApplication_VehicleSeries {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplication_VehicleSeries;
            }
        }

        public string DataGridView_ColumnItem_Title_RepairOrderMaterialDetail_NewPartsSupplierCode {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_RepairOrderMaterialDetail_NewPartsSupplierCode;
            }
        }

        public string DataGridView_ColumnItem_Title_RepairOrderMaterialDetail_UsedPartsSupplierCode {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_RepairOrderMaterialDetail_UsedPartsSupplierCode;
            }
        }

        public string DataEditPanel_Text_DealerInvoiceInformation_InvoiceCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_DealerInvoiceInformation_InvoiceCode;
            }
        }

        public string DataEditPanel_Text_DealerInvoiceInformation_SettlementTotalMoney {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_DealerInvoiceInformation_SettlementTotalMoney;
            }
        }

        public string DataEditPanel_Text_DealerInvoiceInformation_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_DealerInvoiceInformation_PartsSalesCategory;
            }
        }

        public string DataGridView_ColumnItem_Title_RepairClaimBill_VehicleType {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_RepairClaimBill_VehicleType;
            }
        }

        public string DetailPanel_Text_Dealer_BridgeType {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_Dealer_BridgeType;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimApplication_EstimatedFinishingTime {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimApplication_EstimatedFinishingTime;
            }
        }

        public string DetailPanel_Text_ServiceTripClaimApplication_RepairRequestTime {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_ServiceTripClaimApplication_RepairRequestTime;
            }
        }

        public string DataEditView_Title_RepairClaimApplication_RepairRequestTime {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_RepairClaimApplication_RepairRequestTime;
            }
        }

        public string DetailPanel_Title_PartsDetail {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Title_PartsDetail;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_ClaimBillCode {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_ClaimBillCode;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_ClaimBillType {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_ClaimBillType;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsCode {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsCode;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierCode {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierCode;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierName {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierName;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_IfFaultyParts {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_IfFaultyParts;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_ResponsibleUnitCode {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_ResponsibleUnitCode;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierCode {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierCode;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierName {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierName;
            }
        }

        public string QueryPanel_QueryItem_Title_Common_ServiceProductLineName {
            get {
                return Resources.ServiceUIStrings.QueryPanel_QueryItem_Title_Common_ServiceProductLineName;
            }
        }

        public string DataEditPanel_Text_UsedPartsReturnOrder_ObjectResponseCompany {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsReturnOrder_ObjectResponseCompany;
            }
        }

        public string DataGridView_ColumnItem_Title_StorageQuantity {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_StorageQuantity;
            }
        }

        public string DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_ShippingOrderCode {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_ShippingOrderCode;
            }
        }

        public string DataManagementView_Title_UsedPartsStockForEdit {
            get {
                return Resources.ServiceUIStrings.DataManagementView_Title_UsedPartsStockForEdit;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_Branch {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_Branch;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_Code {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_Code;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_Dealer {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_Dealer;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_MarketingDepartment {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_MarketingDepartment;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_PartsSalesCategory {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_PartsSalesCategory;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_SalesUnit {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_SalesUnit;
            }
        }

        public string DataEditPanel_Text_PartsClaimMaterialDetail_PartsSalesOrderCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimMaterialDetail_PartsSalesOrderCode;
            }
        }

        public string DataEditPanel_Text_SsClaimSettlementBill_ProductLineType {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_SsClaimSettlementBill_ProductLineType;
            }
        }

        public string DataEditView_Title_WorkOrderFeedback {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_WorkOrderFeedback;
            }
        }

        public string DataEditView_Text_RepairWorkOrder_RepairClassification {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_RepairWorkOrder_RepairClassification;
            }
        }

        public string DataEditView_Validation_RepairClaimApplication_FinalApproveComment {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_RepairClaimApplication_FinalApproveComment;
            }
        }

        public string DataEditView_Validation_PartsClaimOrder_InitialApproveComment {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_PartsClaimOrder_InitialApproveComment;
            }
        }

        public string DetailPanel_Text_SupplierClaimSettleBill_SupplierName {
            get {
                return Resources.ServiceUIStrings.DetailPanel_Text_SupplierClaimSettleBill_SupplierName;
            }
        }

        public string DataEditView_Title_SupplierClaimSettleBill {
            get {
                return Resources.ServiceUIStrings.DataEditView_Title_SupplierClaimSettleBill;
            }
        }

        public string DataEditView_Text_SupplierClaimSettleBill_SupplierName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_SupplierClaimSettleBill_SupplierName;
            }
        }

        public string DataEditView_Validation_SupplierExpenseAdjustBill_SelectSupplier {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_SupplierExpenseAdjustBill_SelectSupplier;
            }
        }

        public string DataEditView_Validation_ServiceTripClaimApplication_CarActualityOutLength {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_ServiceTripClaimApplication_CarActualityOutLength;
            }
        }

        public string DataEditView_Validation_ServiceTripClaimApplication_CustomOpinionOutLength {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_ServiceTripClaimApplication_CustomOpinionOutLength;
            }
        }

        public string DataEditView_Validation_ServiceTripClaimApplication_MalfunctionReasonOutLength {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_ServiceTripClaimApplication_MalfunctionReasonOutLength;
            }
        }

        public string DataEditView_Validation_ServiceTripClaimApplication_ServiceTripReasonOutLength {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_ServiceTripClaimApplication_ServiceTripReasonOutLength;
            }
        }

        public string DataEditView_Validation_RepairOrderFaultReasons_MalfunctionReasonOutLength {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_RepairOrderFaultReasons_MalfunctionReasonOutLength;
            }
        }

        public string DataEditView_Validation_ServiceTripClaimApplication_DetailedAddressOutLength {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_ServiceTripClaimApplication_DetailedAddressOutLength;
            }
        }

        public string DataEditView_Validation_ExpenseAdjustmentBill_SelectSourceType {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_ExpenseAdjustmentBill_SelectSourceType;
            }
        }

        public string DataEditView_Validation_SsClaimSettleInstruction_SettleMethodsIsNull {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_SsClaimSettleInstruction_SettleMethodsIsNull;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimApplication_Applicant {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimApplication_Applicant;
            }
        }

        public string DataEditPanel_Text_ServiceTripClaimApplication_ApplicationTel {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_ServiceTripClaimApplication_ApplicationTel;
            }
        }

        public string DataEditPanel_Text_RepairClaimBill_DealerContactAddress {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_RepairClaimBill_DealerContactAddress;
            }
        }

        public string DataEditView_Validation_ServiceTripClaimApplication_ServiceTripDurationIsZero {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_ServiceTripClaimApplication_ServiceTripDurationIsZero;
            }
        }

        public string DataEditView_GroupTitle_OilStandard {
            get {
                return Resources.ServiceUIStrings.DataEditView_GroupTitle_OilStandard;
            }
        }

        public string DataEditView_Text_VehicleMainteProductCategory_MaterialCost {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_VehicleMainteProductCategory_MaterialCost;
            }
        }

        public string DataEditView_Text_VehicleMainteProductCategory_Price {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_VehicleMainteProductCategory_Price;
            }
        }

        public string DataEditView_Text_VehicleMainteProductCategory_ProductCategoryCode {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_VehicleMainteProductCategory_ProductCategoryCode;
            }
        }

        public string DataEditView_Text_VehicleMainteProductCategory_ProductCategoryName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_VehicleMainteProductCategory_ProductCategoryName;
            }
        }

        public string DataEditView_Text_VehicleMainteProductCategory_Quantity {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_VehicleMainteProductCategory_Quantity;
            }
        }

        public string DataEditView_Text_VehicleMainteProductCategory_Type1 {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_VehicleMainteProductCategory_Type1;
            }
        }

        public string DataEditView_Text_VehicleMainteProductCategory_Type1Amount {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_VehicleMainteProductCategory_Type1Amount;
            }
        }

        public string DataEditView_Text_VehicleMainteProductCategory_Type2 {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_VehicleMainteProductCategory_Type2;
            }
        }

        public string DataEditView_Text_VehicleMainteProductCategory_Type2Amount {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_VehicleMainteProductCategory_Type2Amount;
            }
        }

        public string DataEditView_Text_VehicleMainteProductCategory_Quantity2 {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_VehicleMainteProductCategory_Quantity2;
            }
        }

        public string DataEditView_Text_VehicleMainteProductCategory_Price2 {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_VehicleMainteProductCategory_Price2;
            }
        }

        public string DataEditView_Validation_PartsClaimOrderNew_FaultyPartsCodeIsNull {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_FaultyPartsCodeIsNull;
            }
        }

        public string DataEditView_Validation_PartsClaimOrderNew_PartsSalesOrderCodeIsNull {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_PartsSalesOrderCodeIsNull;
            }
        }

        public string DataEditView_Validation_PartsClaimOrderNew_ReturnWarehouseIdIsNull {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_ReturnWarehouseIdIsNull;
            }
        }

        public string DataEditView_Text_Vehicleinformation_VehicleCategoryName {
            get {
                return Resources.ServiceUIStrings.DataEditView_Text_Vehicleinformation_VehicleCategoryName;
            }
        }

        public string DataEditView_Validation_PartsClaimOrderNew_AgencyOutWarehouseIdIsNull {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_AgencyOutWarehouseIdIsNull;
            }
        }

        public string DataEditView_Validation_PartsClaimOrderNew_LaborCostLessThanZero {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_LaborCostLessThanZero;
            }
        }

        public string DataEditView_Validation_PartsClaimOrderNew_OtherCostLessThanZero {
            get {
                return Resources.ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_OtherCostLessThanZero;
            }
        }

        public string DataGridView_ColumnItem_Title_RepairClaimBill_ClaimSupplierCode {
            get {
                return Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_RepairClaimBill_ClaimSupplierCode;
            }
        }

        public string ActionPanel_Title_RepairClaimBillForDealer {
            get {
                return Resources.ServiceUIStrings.ActionPanel_Title_RepairClaimBillForDealer;
            }
        }

        public string ActionPanel_Title_RepairOrderForWarrantyBranch {
            get {
                return Resources.ServiceUIStrings.ActionPanel_Title_RepairOrderForWarrantyBranch;
            }
        }

        public string DataEditPanel_GroupTitle_VehicleInfo_CustomerInfo {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_GroupTitle_VehicleInfo_CustomerInfo;
            }
        }

        public string QueryPanel_Title_IntegralClaimBillSettle {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_IntegralClaimBillSettle;
            }
        }

        public string DataManagementView_Title_OutofWarrantyPayment {
            get {
                return Resources.ServiceUIStrings.DataManagementView_Title_OutofWarrantyPayment;
            }
        }

        public string QueryPanel_Title_OutofWarrantyPayment {
            get {
                return Resources.ServiceUIStrings.QueryPanel_Title_OutofWarrantyPayment;
            }
        }

        public string Action_Title_OursFinished {
            get {
                return Resources.ServiceUIStrings.Action_Title_OursFinished;
            }
        }

        public string DataManagementView_Notification_OursFinishedSuccess {
            get {
                return Resources.ServiceUIStrings.DataManagementView_Notification_OursFinishedSuccess;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrderNew_CenterPartsSalesOrderCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrderNew_CenterPartsSalesOrderCode;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrderNew_Code {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrderNew_Code;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrderNew_FaultyPartsCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrderNew_FaultyPartsCode;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrderNew_MaterialManagementCost {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrderNew_MaterialManagementCost;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrderNew_ReturnCompanyName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrderNew_ReturnCompanyName;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrderNew_RWarehouseCompanyName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrderNew_RWarehouseCompanyName;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrderNew_RWarehouseCompanyType {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrderNew_RWarehouseCompanyType;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_AbandonerReason {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_AbandonerReason;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_AgencyOutWarehouseName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_AgencyOutWarehouseName;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_ApproveComment {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_ApproveComment;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_MalfunctionDescription {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_MalfunctionDescription;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_PartsRetailOrderCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_PartsRetailOrderCode;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_PartsSalesOrderCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_PartsSalesOrderCode;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_RDCPartsInboundPlanCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_RDCPartsInboundPlanCode;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_RDCPartsInboundPlanStauts {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_RDCPartsInboundPlanStauts;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_RDCReceiptSituation {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_RDCReceiptSituation;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_RepairOrderCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_RepairOrderCode;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_ReturnWarehouseName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_ReturnWarehouseName;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_SalesWarehouseName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_SalesWarehouseName;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_ShippingCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_ShippingCode;
            }
        }

        public string DataEditPanel_Text_sedPartsLogisticLossBillDetail_LogisticLossTotalAmount {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_sedPartsLogisticLossBillDetail_LogisticLossTotalAmount;
            }
        }

        public string DataEditPanel_Text_UsedPartsShippingOrder_BusinessCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsShippingOrder_BusinessCode;
            }
        }

        public string DataEditPanel_Text_UsedPartsShippingOrder_TotalAmount {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_UsedPartsShippingOrder_TotalAmount;
            }
        }

        public string DataEditPanel_Validation_PartsClaimOrderNew_CenterPartsSalesOrderCodeIsNotNull {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Validation_PartsClaimOrderNew_CenterPartsSalesOrderCodeIsNotNull;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_BranchCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_BranchCode;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_CustomerContactPerson {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_CustomerContactPerson;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_CustomerContactPhone {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_CustomerContactPhone;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_FaultyPartsName {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_FaultyPartsName;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_FaultyPartsReferenceCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_FaultyPartsReferenceCode;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_FaultyPartsSupplierCode {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_FaultyPartsSupplierCode;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_IsOutWarranty {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_IsOutWarranty;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_PartsSaleLong {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_PartsSaleLong;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_PartsSaleTime {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_PartsSaleTime;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_PartsWarrantyLong {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_PartsWarrantyLong;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_RepairRequestTime {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_RepairRequestTime;
            }
        }

        public string DataEditPanel_Validation_PartsClaimOrderNew_PartsRetailOrderCodeIsNull {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Validation_PartsClaimOrderNew_PartsRetailOrderCodeIsNull;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrderNew_StopReason {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrderNew_StopReason;
            }
        }

        public string DataEditPanel_Text_PartsClaimOrder_CDCReturnWarehouse {
            get {
                return Resources.ServiceUIStrings.DataEditPanel_Text_PartsClaimOrder_CDCReturnWarehouse;
            }
        }

}
}
