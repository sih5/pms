﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.Custom {
    public partial class UsedPartsWarehouseAreaDataView : IBaseView, INotifyPropertyChanged {
        private UsedPartsWarehouseArea currentSelectedItem;
        private DcsDomainContext domainContext;
        private UsedPartsWarehouseAreaDataTreeView usedPartsWarehouseAreaDataTreeView;
        private DcsDetailPanelBase usedPartsWarehouseAreaDetailPanel;
        private DcsDataGridViewBase usedPartsWarehouseManagerDataGridView;
        public event PropertyChangedEventHandler PropertyChanged;

        public UsedPartsWarehouseAreaDataView() {
            InitializeComponent();
            this.CreateUI();
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UsedPartsWarehouseAreaDataTreeView_OnTreeViewItemClick(object sender, RadRoutedEventArgs radRoutedEventArgs) {
            var selectedItem = ((RadTreeView)sender).SelectedItem as RadTreeViewItem;
            if(selectedItem == null)
                return;
            var usedPartsWarehouseArea = selectedItem.Tag as UsedPartsWarehouseArea;
            if(usedPartsWarehouseArea == null)
                return;
            this.CurrentSelectedItem = usedPartsWarehouseArea;
            this.UsedPartsWarehouseAreaDetailPanel.SetValue(DataContextProperty, this.CurrentSelectedItem);
            this.UsedPartsWarehouseManagerDataGridView.SetValue(DataContextProperty, this.CurrentSelectedItem);
        }

        private void CreateUI() {
            this.UsedPartsWarehouseManagerDataGridView.SetValue(VerticalAlignmentProperty, VerticalAlignment.Stretch);
            this.UsedPartsWarehouseManagerDataGridView.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Stretch);
            this.ManagerGridView.Content = this.UsedPartsWarehouseManagerDataGridView;
            this.UsedPartsWarehouseAreaDataTreeView.SetValue(Grid.RowSpanProperty, 2);
            this.Root.Children.Add(this.UsedPartsWarehouseAreaDataTreeView);
            this.UsedPartsWarehouseAreaDetailPanel.SetValue(Grid.ColumnProperty, 2);
            this.UsedPartsWarehouseAreaDetailPanel.SetValue(Grid.RowProperty, 0);
            this.Root.Children.Add(UsedPartsWarehouseAreaDetailPanel);
        }

        private void UsedPartsWarehouseAreaDataTreeView_OnTreeViewDataLoaded() {
            var firstNode = this.UsedPartsWarehouseAreaDataTreeView.GetFirstTreeViewItem();
            if(firstNode != null && firstNode.Tag is UsedPartsWarehouseArea) {
                firstNode.IsSelected = true;
                this.CurrentSelectedItem = (firstNode.Tag as UsedPartsWarehouseArea);
                this.UsedPartsWarehouseAreaDetailPanel.SetValue(DataContextProperty, this.CurrentSelectedItem);
                this.UsedPartsWarehouseManagerDataGridView.SetValue(DataContextProperty, this.CurrentSelectedItem);
            }
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            return null;
        }

        public UsedPartsWarehouseAreaDataTreeView UsedPartsWarehouseAreaDataTreeView {
            get {
                if(this.usedPartsWarehouseAreaDataTreeView == null) {
                    this.usedPartsWarehouseAreaDataTreeView = new UsedPartsWarehouseAreaDataTreeView();
                    this.usedPartsWarehouseAreaDataTreeView.DomainContext = this.DomainContext;
                    this.usedPartsWarehouseAreaDataTreeView.EntityQuery = this.usedPartsWarehouseAreaDataTreeView.DomainContext.GetUsedPartsWarehouseAreasWithUsedPartsWarehouseQuery(BaseApp.Current.CurrentUserData.EnterpriseId);
                    this.usedPartsWarehouseAreaDataTreeView.OnTreeViewDataLoaded += this.UsedPartsWarehouseAreaDataTreeView_OnTreeViewDataLoaded;
                    this.usedPartsWarehouseAreaDataTreeView.OnTreeViewItemClick += this.UsedPartsWarehouseAreaDataTreeView_OnTreeViewItemClick;
                    this.usedPartsWarehouseAreaDataTreeView.RefreshDataTree();
                }
                return this.usedPartsWarehouseAreaDataTreeView;
            }
        }

        public void CancelTreeItem(int id) {
            this.UsedPartsWarehouseAreaDataTreeView.CancelTreeItem(id);
            this.CurrentSelectedItem = null;
        }

        public ObservableCollection<DcsMenuItem> RadMenuItems {
            get {
                return this.UsedPartsWarehouseAreaDataTreeView.RadMenuItems;
            }
        }

        public DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        public DcsDataGridViewBase UsedPartsWarehouseManagerDataGridView {
            get {
                return this.usedPartsWarehouseManagerDataGridView ?? (this.usedPartsWarehouseManagerDataGridView = DI.GetDataGridView("UsedPartsWarehouseManager") as DcsDataGridViewBase);
            }
        }

        public DcsDetailPanelBase UsedPartsWarehouseAreaDetailPanel {
            get {
                return this.usedPartsWarehouseAreaDetailPanel ?? (this.usedPartsWarehouseAreaDetailPanel = DI.GetDetailPanel("UsedPartsWarehouseArea") as DcsDetailPanelBase);
            }
        }

        public UsedPartsWarehouseArea CurrentSelectedItem {
            get {
                return this.currentSelectedItem;
            }
            set {
                this.currentSelectedItem = value;
                this.OnPropertyChanged("CurrentSelectedItem");
            }
        }

        public void RefreshTreeView() {
            this.UsedPartsWarehouseAreaDataTreeView.RefreshDataTree();
        }
    }
}
