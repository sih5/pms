﻿
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class PartsClaimOrderNewDetailDataEditView {
        public PartsClaimOrderNewDetailDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }
        private readonly string[] kvNames = {
            "PartsInboundPlan_Status"
        };

        private KeyValueManager keyValueManager;

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_Detail_PartsClaimOrderNew;
            }
        }

        private FileUploadDataEditPanel productDataEditPanels;

        protected FileUploadDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload"));
            }
        }


        private void CreateUI() {
            this.DataEditPanels.SetValue(Grid.RowProperty, 6);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 6);
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.DataEditPanels.SetValue(Grid.RowSpanProperty, 3);
            this.DataEditPanels.isVisibility = true;
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Right;
            this.LayoutRoot.Children.Add(DataEditPanels);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsClaimOrderNewByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                DataEditPanels.FilePath = entity.Path;
                this.SetObjectToEdit(entity);
            }, null);
        }
    }
}
