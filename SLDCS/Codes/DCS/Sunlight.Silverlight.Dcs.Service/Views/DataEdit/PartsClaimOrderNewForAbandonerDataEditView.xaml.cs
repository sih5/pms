﻿
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class PartsClaimOrderNewForAbandonerDataEditView {
        public PartsClaimOrderNewForAbandonerDataEditView() {
            InitializeComponent();
        }

        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_Abandon_PartsClaimOrderNew;
            }
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        protected override void OnEditSubmitting() {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            partsClaimOrderNew.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(partsClaimOrderNew.AbandonerReason)) {
                partsClaimOrderNew.ValidationErrors.Add(new ValidationResult("作废原因不能为空", new[] {
                        "RejectComment"
                    }));
            }
            if(partsClaimOrderNew.HasValidationErrors)
                return;
            if(partsClaimOrderNew.Can作废配件索赔单新)
                partsClaimOrderNew.作废配件索赔单新();
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsClaimOrderNewsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
