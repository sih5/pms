﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Sunlight.Silverlight.Dcs.Service.Views.QueryWindow;


namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class PartsClaimOrderNewForAgencyDataEditView {
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        private ObservableCollection<KeyValuePair> kvReturnWarehouses;
        private ObservableCollection<KeyValuePair> kvAgencyOutWarehouses;
        private FileUploadDataEditPanel productDataEditPanels;

        private readonly string[] kvNames = {
            "Company_Type"
        };

        private KeyValueManager keyValueManager;

        private KeyValueManager KeyValueManager
        {
            get
            {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys {
            get {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvReturnWarehouses {
            get {
                return this.kvReturnWarehouses ?? (this.kvReturnWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvAgencyOutWarehouses {
            get {
                return this.kvAgencyOutWarehouses ?? (this.kvAgencyOutWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }
      
        protected FileUploadDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload"));
            }
        }

        protected override string BusinessName {
            get {
                return ServiceUIStrings.DataEditView_Title_PartsClaimOrderNewForAgency;
            }
        }

        private Company company;
        private void CreateUI()
        {
            this.gridAgencyControl.Visibility = Visibility.Collapsed;
            this.DataEditPanels.SetValue(Grid.RowProperty, 6);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 6);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Right;
            this.DataEditPanels.VerticalAlignment = VerticalAlignment.Bottom;
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 3);
            this.LayoutRoot.Children.Add(DataEditPanels);
            this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOption =>
            {
                if (loadOption.HasError)
                {
                    if (!loadOption.IsErrorHandled)
                        loadOption.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOption);
                    return;
                }
                if (loadOption.Entities != null && loadOption.Entities.Any())
                {
                    company = loadOption.Entities.First();

                    if (company.Type == (int)DcsCompanyType.服务站)
                    {
                        this.DomainContext.Load(DomainContext.GetPartsSalesCategoriesByDealerServiceInfoDealerIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp =>
                        {
                            if (loadOp.HasError)
                            {
                                if (!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            this.KvPartsSalesCategorys.Clear();
                            foreach (var partsSalesCategory in loadOp.Entities)
                                this.KvPartsSalesCategorys.Add(new KeyValuePair
                                {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name,
                                    UserObject = partsSalesCategory
                                });
                            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
                        }, null);
                    }
                    else
                    {
                        this.DomainContext.Load(DomainContext.GetPartsSalesCategoriesBySalesUnitOwnerCompanyIdQuery(BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOpSalesCategory =>
                        {
                            if (loadOpSalesCategory.HasError)
                            {
                                if (!loadOpSalesCategory.IsErrorHandled)
                                    loadOpSalesCategory.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOpSalesCategory);
                                return;
                            }
                            this.KvPartsSalesCategorys.Clear();
                            foreach (var partsSalesCategory in loadOpSalesCategory.Entities)
                                this.KvPartsSalesCategorys.Add(new KeyValuePair
                                {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name,
                                    UserObject = partsSalesCategory
                                });
                            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
                        }, null);
                    }
                }
            }, null);

            //选择配件
            var faultyPart = DI.GetQueryWindow("SparePart");
            popupTextBoxSparePart.PopupContent = faultyPart;
            faultyPart.Loaded += faultyPart_Loaded;
            faultyPart.SelectionDecided += faultyPart_SelectionDecided;
            //选择销售订单
            var salesOrder = DI.GetQueryWindow("PartsSalesOrderForSelect");
            popupTextBoxSaleOrderCode.PopupContent = salesOrder;
            salesOrder.Loaded += salesOrder_Loaded;
            salesOrder.SelectionDecided += salesOrder_SelectionDecided;
            //选择零售订单
            var retailOrder = DI.GetQueryWindow("PartsRetailOrderForSelect");
            popupTextBoxPartsRetailOrderCode.PopupContent = retailOrder;
            retailOrder.Loaded += partsRetailOrder_Loaded;
            retailOrder.SelectionDecided += partsRetailOrder_SelectionDecided;


        }

        //实体只读，重新分离附加实体
        private void AgainDetachAndAdd(PartsClaimOrderNew partsClaimOrderNew)
        {
            if (partsClaimOrderNew.IsReadOnly)
            {
                if (this.DomainContext.PartsClaimOrderNews.Contains(partsClaimOrderNew))
                {
                    this.DomainContext.PartsClaimOrderNews.Detach(partsClaimOrderNew);
                    this.DomainContext.PartsClaimOrderNews.Add(partsClaimOrderNew);
                }
            }

        }
        #region 界面选择事件
        private void faultyPart_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var sparePart = queryWindow.SelectedEntities.Cast<SparePart>().FirstOrDefault();
            if (sparePart == null)
                return;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            if (partsClaimOrderNew.IsReadOnly)
                this.AgainDetachAndAdd(partsClaimOrderNew);
            partsClaimOrderNew.FaultyPartsId = sparePart.Id;
            partsClaimOrderNew.FaultyPartsCode = sparePart.Code;
            partsClaimOrderNew.FaultyPartsName = sparePart.Name;
            partsClaimOrderNew.FaultyPartsReferenceCode = sparePart.ReferenceCode;

            this.popupTextBoxSparePart.Text = sparePart.Code;

            //查询配件保修时长
            int[] partIds = new int[] { sparePart.Id };
            this.DomainContext.Load(DomainContext.查询配件营销信息Query(partsClaimOrderNew.Branchid, partIds), LoadBehavior.RefreshCurrent, loadOp =>
            {
                if (loadOp.HasError)
                {
                    if (!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var branch = loadOp.Entities.FirstOrDefault();
                if (branch != null)
                {
                    partsClaimOrderNew.PartsWarrantyLong = branch.PartsWarrantyLong;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if (parent != null)
                    parent.Close();
            }, null);
        }

        private void faultyPart_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(queryWindow == null || partsClaimOrderNew == null)
                return;
            if(partsClaimOrderNew.Branchid == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesCategoryIdIsNull);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
        }

        private void repairOrder_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var repairOrder = queryWindow.SelectedEntities.Cast<RepairOrder>().FirstOrDefault();
            if(repairOrder == null)
                return;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void repairOrder_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(queryWindow == null || partsClaimOrderNew == null)
                return;
            if(partsClaimOrderNew.FaultyPartsId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_FaultyPartsCodeIsNull);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem("DealerId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            compositeFilterItem.Filters.Add(new FilterItem("SparePartId", typeof(int), FilterOperator.IsEqualTo, partsClaimOrderNew.FaultyPartsId));
            compositeFilterItem.Filters.Add(new FilterItem("WarrantyStatus", typeof(int), FilterOperator.IsEqualTo, (int)DcsWarrantyStatus.保外));
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "PartsSalesCategoryId", partsClaimOrderNew.PartsSalesCategoryId });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "PartsSalesCategoryId", false });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "BranchId", partsClaimOrderNew.Branchid });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "BranchId", false });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
            //清空查询条件：
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "VehicleInformation.VehicleLicensePlate", "" });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "VehicleInformation.VIN", "" });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "Code", "" });
        }

        private void partsRetailOrder_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsRetailOrder = queryWindow.SelectedEntities.Cast<PartsRetailOrder>().FirstOrDefault();
            if(partsRetailOrder == null)
                return;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            if (partsClaimOrderNew.IsReadOnly)
                this.AgainDetachAndAdd(partsClaimOrderNew);
            partsClaimOrderNew.PartsRetailOrderId = partsRetailOrder.Id;
            partsClaimOrderNew.PartsRetailOrderCode = partsRetailOrder.Code;
            partsClaimOrderNew.PartsSaleTime = partsRetailOrder.ApproveTime;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }


        private void retailOrder_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var dealerPartsRetailOrder = queryWindow.SelectedEntities.Cast<DealerPartsRetailOrder>().FirstOrDefault();
            if(dealerPartsRetailOrder == null)
                return;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            partsClaimOrderNew.PartsRetailOrderId = dealerPartsRetailOrder.Id;
            partsClaimOrderNew.PartsRetailOrderCode = dealerPartsRetailOrder.Code;
            partsClaimOrderNew.CustomerContactPerson = dealerPartsRetailOrder.Customer;
            partsClaimOrderNew.CustomerContactPhone = dealerPartsRetailOrder.CustomerCellPhone;
            //配件销售时间取值方式：如果有零售订单，取零售订单的审核时间。否则取维修单的维修完工时间。如果两者都没有，取销售订单的审核时间
            partsClaimOrderNew.PartsSaleTime = dealerPartsRetailOrder.ApproveTime;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        private void partsRetailOrder_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as PartsRetailOrderForSelectQueryWindow;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(queryWindow == null || partsClaimOrderNew == null)
                return;
            if(partsClaimOrderNew.FaultyPartsId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_FaultyPartsCodeIsNull);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
            var compositeFilterItem = new CompositeFilterItem();
            //queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("SparePartId", typeof(int), FilterOperator.IsEqualTo, partsClaimOrderNew.FaultyPartsId));
            //queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("SalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsClaimOrderNew.PartsSalesCategoryId));
            //queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("SalesUnitOwnerCompanyId", typeof(int), FilterOperator.IsEqualTo, partsClaimOrderNew.ReturnCompanyId));
            compositeFilterItem.LogicalOperator = LogicalOperator.And;
            compositeFilterItem.Filters.Add(new FilterItem("SalesCategoryId", typeof(int), FilterOperator.IsEqualTo, partsClaimOrderNew.PartsSalesCategoryId));
            compositeFilterItem.Filters.Add(new FilterItem("SparePartId", typeof(int), FilterOperator.IsEqualTo, partsClaimOrderNew.FaultyPartsId));
            compositeFilterItem.Filters.Add(new FilterItem("SalesUnitOwnerCompanyId", typeof(int), FilterOperator.IsEqualTo, partsClaimOrderNew.ReturnCompanyId));
           
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void salesOrder_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(queryWindow == null || partsClaimOrderNew == null)
                return;
            if(partsClaimOrderNew.HasValidationErrors)
                partsClaimOrderNew.ValidationErrors.Clear();
            if(partsClaimOrderNew.FaultyPartsId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_FaultyPartsCodeIsNull);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
            //queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "SalesCategoryId", partsClaimOrderNew.PartsSalesCategoryId });
            //queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "SalesCategoryId", false });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("SparePartId", typeof(int), FilterOperator.IsEqualTo, partsClaimOrderNew.FaultyPartsId));
        }

        private void salesOrder_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsSalesOrder = queryWindow.SelectedEntities.Cast<PartsSalesOrder>().FirstOrDefault();
            if(partsSalesOrder == null)
                return;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            if (partsClaimOrderNew.IsReadOnly)
                this.AgainDetachAndAdd(partsClaimOrderNew);
            partsClaimOrderNew.ShippingCode = null;
            partsClaimOrderNew.PartsSalesOrderId = partsSalesOrder.Id;
            partsClaimOrderNew.PartsSalesOrderCode = partsSalesOrder.Code;
            partsClaimOrderNew.SalesWarehouseId = partsSalesOrder.WarehouseId;
            partsClaimOrderNew.RWarehouseCompanyCode = partsSalesOrder.SalesUnitOwnerCompanyCode;
            partsClaimOrderNew.RWarehouseCompanyId = partsSalesOrder.SalesUnitOwnerCompanyId;
            partsClaimOrderNew.RWarehouseCompanyName = partsSalesOrder.SalesUnitOwnerCompanyName;

            //partsClaimOrderNew.CenterPartsSalesOrderId = partsSalesOrder.Id;
            //partsClaimOrderNew.CenterPartsSalesOrderCode = partsSalesOrder.Code;
            //材料费2
            var partsSalesOrderDetail = partsSalesOrder.PartsSalesOrderDetails.Where(r => r.SparePartId == partsClaimOrderNew.FaultyPartsId).FirstOrDefault();
            partsClaimOrderNew.MaterialManagementCost = partsSalesOrderDetail.OrderPrice;
            
            this.DomainContext.Load(this.DomainContext.GetCompanyByIdQuery(partsClaimOrderNew.RWarehouseCompanyId??0), LoadBehavior.RefreshCurrent, loadop =>
            {
                if (loadop.HasError)
                {
                    if (!loadop.IsErrorHandled)
                        loadop.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadop);
                }
                if (loadop.Entities != null && loadop.Entities.Any())
                {
                    partsClaimOrderNew.RWarehouseCompanyType = loadop.Entities.FirstOrDefault().Type;
                }
            }, null);

            //退货仓库
            this.DomainContext.Load(this.DomainContext.维修索赔获取退货仓库Query(partsSalesOrder.SalesUnitOwnerCompanyId), LoadBehavior.RefreshCurrent, loadWarehouse1 =>
            {
                if (loadWarehouse1.HasError)
                {
                    if (!loadWarehouse1.IsErrorHandled)
                        loadWarehouse1.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadWarehouse1);
                }
                this.kvReturnWarehouses.Clear();
                if (loadWarehouse1.Entities != null && loadWarehouse1.Entities.Any())
                {
                    foreach (var warehouse in loadWarehouse1.Entities)
                    {
                        this.kvReturnWarehouses.Add(new KeyValuePair
                        {
                            Key = warehouse.Id,
                            Value = warehouse.Name,
                            UserObject = warehouse
                        });
                    }
                }
            }, null);
            //销售出库仓库
            this.DomainContext.Load(DomainContext.GetWarehousesQuery().Where(r => r.Id == partsClaimOrderNew.SalesWarehouseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var warehouse = loadOp.Entities.FirstOrDefault();
                if(warehouse != null) {
                    partsClaimOrderNew.SalesWarehouseName = warehouse.Name;
                    partsClaimOrderNew.SalesWarehouseCode = warehouse.Code;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);

        }
        #endregion

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsClaimOrderNewByIdForReportQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if (entity == null)
                    return;
                if (entity.IsOutWarranty != null)
                {
                    if ((bool)entity.IsOutWarranty)
                    {
                        //超保
                        this.txtOverWarrantyDate.Text = string.Format(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNewForAgency_IsOutWarranty, entity.OverWarrantyDate);
                        this.txtOverWarrantyDate.Visibility = Visibility.Visible;
                    }
                }
                DataEditPanels.FilePath = entity.Path;
                //分公司编号
                var partsSalesCategory = this.KvPartsSalesCategorys.FirstOrDefault(kv => kv.Key == entity.PartsSalesCategoryId);
                if (partsSalesCategory != null)
                {
                    entity.BranchCode = ((PartsSalesCategory)partsSalesCategory.UserObject).BranchCode;
                }

                //退货仓库
                this.DomainContext.Load(this.DomainContext.维修索赔获取退货仓库Query(entity.RWarehouseCompanyId ?? 0), LoadBehavior.RefreshCurrent, loadWarehouse1 =>
                {
                    if (loadWarehouse1.HasError)
                    {
                        if (!loadWarehouse1.IsErrorHandled)
                            loadWarehouse1.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadWarehouse1);
                    }
                    this.kvReturnWarehouses.Clear();
                    if (loadWarehouse1.Entities != null && loadWarehouse1.Entities.Any())
                    {
                        foreach (var warehouse in loadWarehouse1.Entities)
                        {
                            this.kvReturnWarehouses.Add(new KeyValuePair
                            {
                                Key = warehouse.Id,
                                Value = warehouse.Name,
                                UserObject = warehouse
                            });
                        }
                    }
                    this.SetObjectToEdit(entity);
                }, null);

            }, null);
        }

        protected override void Reset() {
            this.txtOverWarrantyDate.Visibility = Visibility.Collapsed;
            this.DataEditPanels.FilePath = "";
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            if(this.DomainContext.PartsClaimOrderNews.Contains(partsClaimOrderNew))
                this.DomainContext.PartsClaimOrderNews.Detach(partsClaimOrderNew);
        }

        protected override bool OnRequestCanSubmit()
        {
            this.txtPartsSalesCategory.SelectedIndex = 0;//默认品牌
            return true;
        }

        protected override void OnEditSubmitting() {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;

            partsClaimOrderNew.Path = DataEditPanels.FilePath;
            partsClaimOrderNew.ValidationErrors.Clear();

            if(partsClaimOrderNew.PartsSalesOrderId == null || partsClaimOrderNew.PartsSalesOrderId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNewForAgency_PartsSalesOrderNotNull);
                return;
            }
            if(partsClaimOrderNew.PartsRetailOrderId == null || partsClaimOrderNew.PartsRetailOrderId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNewForAgency_PartsRetailOrderNotNull);
                return;
            }
            
            if (partsClaimOrderNew.ReturnWarehouseId == default(int))
                partsClaimOrderNew.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_ReturnWarehouseIdIsNull, new[] {
                    "ReturnWarehouseId"
                }));

            if(partsClaimOrderNew.PartsSalesCategoryId == default(int))
                partsClaimOrderNew.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesCategoryIdIsNull, new[] {
                    "PartsSalesCategoryId"
                }));
            if(partsClaimOrderNew.ReturnWarehouseId == default(int))
                partsClaimOrderNew.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_ReturnWarehouseIdIsNull, new[] {
                    "ReturnWarehouseId"
                }));
            
            if(string.IsNullOrEmpty(partsClaimOrderNew.FaultyPartsCode))
                partsClaimOrderNew.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_FaultyPartsCodeIsNull, new[] {
                    "FaultyPartsCode"
                }));
            if(partsClaimOrderNew.PartsWarrantyLong == null || partsClaimOrderNew.PartsWarrantyLong == default(int)){
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNewForAgency_PartsWarrantyLongNotNull);
                  return;
                }
            if (partsClaimOrderNew.RepairRequestTime == null || partsClaimOrderNew.RepairRequestTime.Value.Date > DateTime.Now.Date)
            {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNewForAgency_RepairRequestTimeNoMoreThan);
                return;
            }
            if (partsClaimOrderNew.RepairRequestTime == null || partsClaimOrderNew.RepairRequestTime.Value.Date < partsClaimOrderNew.PartsSaleTime.Value.Date)
            {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_RepairRequestTimeNotLessThan);
                return;
            }
            if(partsClaimOrderNew.HasValidationErrors)
                return;
            ((IEditableObject)partsClaimOrderNew).EndEdit();
            try {
                if (partsClaimOrderNew.Can校验配件是否允许提报配件索赔单)
                    partsClaimOrderNew.校验配件是否允许提报配件索赔单();
                if(EditState == DataEditState.Edit) {
                    try {
                        if(partsClaimOrderNew.Can修改配件索赔单新)
                            partsClaimOrderNew.修改配件索赔单新();
                    } catch(ValidationException ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                        return;
                    }
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public PartsClaimOrderNewForAgencyDataEditView() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsClaimOrderNewForDealerDataEditView_DataContextChanged;
        }

        private void PartsClaimOrderNewForDealerDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            partsClaimOrderNew.PropertyChanged -= partsClaimOrderNew_PropertyChanged;
            partsClaimOrderNew.PropertyChanged += partsClaimOrderNew_PropertyChanged;
        }

        private void partsClaimOrderNew_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            switch(e.PropertyName) {
                case "PartsSalesCategoryId":
                    if(partsClaimOrderNew.PartsSalesCategoryId == default(int))
                        return;
                    ClearChangeProperty(partsClaimOrderNew);
                    var partsSalesCategory = this.KvPartsSalesCategorys.FirstOrDefault(kv => kv.Key == partsClaimOrderNew.PartsSalesCategoryId);
                    if (partsSalesCategory != null) {
                        partsClaimOrderNew.Branchid = ((PartsSalesCategory)partsSalesCategory.UserObject).BranchId;
                        //填充 分公司编号=品牌所属分公司ID的编号
                        partsClaimOrderNew.BranchCode = ((PartsSalesCategory)partsSalesCategory.UserObject).BranchCode;
                    }
                    break;
                case "FaultyPartsId":
                    partsClaimOrderNew.PartsSaleTime = null;
                    partsClaimOrderNew.PartsRetailOrderId = null;
                    partsClaimOrderNew.PartsRetailOrderCode = null;
                    partsClaimOrderNew.PartsSalesOrderId = null;
                    partsClaimOrderNew.PartsSalesOrderCode = null;
                    break;
               
                case "RepairRequestTime":
                case "PartsSaleTime":
                    partsClaimOrderNew.PartsSaleLong = null;
                    if(partsClaimOrderNew.RepairRequestTime.HasValue && partsClaimOrderNew.PartsSaleTime.HasValue) {
                        var repairRequestDate = new DateTime(partsClaimOrderNew.RepairRequestTime.Value.Year, partsClaimOrderNew.RepairRequestTime.Value.Month, partsClaimOrderNew.RepairRequestTime.Value.Day, 00, 00, 00);
                        var partsSaleDate = new DateTime(partsClaimOrderNew.PartsSaleTime.Value.Year, partsClaimOrderNew.PartsSaleTime.Value.Month, partsClaimOrderNew.PartsSaleTime.Value.Day, 00, 00, 00);
                        partsClaimOrderNew.PartsSaleLong = (repairRequestDate - partsSaleDate).Days;
                    }
                    break;
                case "PartsSaleLong":
                case "PartsWarrantyLong":
                    partsClaimOrderNew.IsOutWarranty = null;
                    partsClaimOrderNew.OverWarrantyDate = null;
                    if(partsClaimOrderNew.PartsSaleLong.HasValue && partsClaimOrderNew.PartsWarrantyLong.HasValue) {
                        partsClaimOrderNew.IsOutWarranty = partsClaimOrderNew.PartsSaleLong > partsClaimOrderNew.PartsWarrantyLong;
                        if((bool)partsClaimOrderNew.IsOutWarranty) {
                            //超保
                            partsClaimOrderNew.OverWarrantyDate = partsClaimOrderNew.PartsSaleLong - partsClaimOrderNew.PartsWarrantyLong;
                            partsClaimOrderNew.OverWarrantyDate = partsClaimOrderNew.OverWarrantyDate < 0 ? 0 : partsClaimOrderNew.OverWarrantyDate;
                            this.txtOverWarrantyDate.Text = string.Format(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNewForAgency_IsOutWarranty, partsClaimOrderNew.OverWarrantyDate);
                            this.txtOverWarrantyDate.Visibility = Visibility.Visible;
                        } else {
                            this.txtOverWarrantyDate.Visibility = Visibility.Collapsed;
                        }
                    }
                    break;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        //如果选择仓库返回的仓库.仓储企业类型=分公司，则CDC退货仓库ID=选择仓库返回仓库.id，否则不填充
        private void DcsComboBox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            var warehouse = comboBox.SelectedItem as KeyValuePair;
            if(warehouse != null) {
                partsClaimOrderNew.ReturnWarehouseCode = ((Warehouse)warehouse.UserObject).Code;
                partsClaimOrderNew.ReturnWarehouseName = ((Warehouse)warehouse.UserObject).Name;
                partsClaimOrderNew.RWarehouseCompanyType = ((Warehouse)warehouse.UserObject).StorageCompanyType;
                partsClaimOrderNew.RWarehouseCompanyId = ((Warehouse)warehouse.UserObject).StorageCompanyId;
                this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == partsClaimOrderNew.RWarehouseCompanyId), LoadBehavior.RefreshCurrent, loadCompany => {
                    if(loadCompany.HasError) {
                        loadCompany.MarkErrorAsHandled();
                        return;
                    }
                    if(loadCompany.Entities != null && loadCompany.Entities.Any()) {
                        partsClaimOrderNew.RWarehouseCompanyCode = loadCompany.Entities.First().Code;
                    }
                }, null);
            }

        }

        //品牌变更清空字段值
        private static void ClearChangeProperty(PartsClaimOrderNew partsClaimOrderNew) {
            partsClaimOrderNew.TotalAmount = 0;
            partsClaimOrderNew.FaultyPartsSupplierId = null;
            partsClaimOrderNew.FaultyPartsSupplierCode = null;
            partsClaimOrderNew.FaultyPartsSupplierName = null;
            partsClaimOrderNew.Quantity = 1;
            partsClaimOrderNew.MaterialCost = null;
            partsClaimOrderNew.PartsWarrantyLong = null;
            partsClaimOrderNew.MaterialManagementCost = null;
            partsClaimOrderNew.RepairOrderId = null;
            partsClaimOrderNew.RepairOrderCode = null;
            partsClaimOrderNew.PartsSaleTime = null;
            partsClaimOrderNew.PartsRetailOrderId = null;
            partsClaimOrderNew.PartsRetailOrderCode = null;
            partsClaimOrderNew.PartsSalesOrderId = null;
            partsClaimOrderNew.PartsSalesOrderCode = null;
            partsClaimOrderNew.SalesWarehouseId = null;
            partsClaimOrderNew.SalesWarehouseName = null;
            partsClaimOrderNew.SalesWarehouseCode = null;
            partsClaimOrderNew.Branchid = 0;
            partsClaimOrderNew.OverWarrantyDate = null;

        }
    }
}
