﻿
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class PartsClaimOrderNewForDeliverParts {
        private ObservableCollection<KeyValuePair> kvCDCReturnWarehouse;
        public ObservableCollection<KeyValuePair> KvCDCReturnWarehouse {
            get {
                return this.kvCDCReturnWarehouse ?? (this.kvCDCReturnWarehouse = new ObservableCollection<KeyValuePair>());
            }
        }
        private readonly string[] kvNames = {
            "PartsInboundPlan_Status"
        };
        private KeyValueManager keyValueManager;

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public PartsClaimOrderNewForDeliverParts() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += PartsClaimOrderNewForDeliverParts_DataContextChanged;
        }

        void PartsClaimOrderNewForDeliverParts_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            partsClaimOrderNew.PropertyChanged -= partsClaimOrderNew_PropertyChanged;
            partsClaimOrderNew.PropertyChanged += partsClaimOrderNew_PropertyChanged;
        }

        private void partsClaimOrderNew_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            switch(e.PropertyName) {
                case "CDCReturnWarehouseId":
                    var warehouse = this.KvCDCReturnWarehouse.Single(r => r.Key == partsClaimOrderNew.ReturnWarehouseId).UserObject as Warehouse;
                    if(warehouse == null)
                        return;
                    partsClaimOrderNew.CDCReturnWarehouseCode = warehouse.Code;
                    partsClaimOrderNew.CDCReturnWarehouseName = warehouse.Name;
                    partsClaimOrderNew.CDCRWCompanyId = warehouse.StorageCompanyId;
                    partsClaimOrderNew.CDCRWCompanyType = warehouse.StorageCompanyType;
                    this.DomainContext.Load(this.DomainContext.GetCompaniesQuery().Where(r => r.Id == partsClaimOrderNew.CDCRWCompanyId), LoadBehavior.RefreshCurrent, loadCompany => {
                        if(loadCompany.HasError) {
                            loadCompany.MarkErrorAsHandled();
                            return;
                        }
                        var company = loadCompany.Entities.SingleOrDefault();
                        if(company != null) {
                            partsClaimOrderNew.CDCRWCompanyCode = company.Code;
                        }
                    }, null);
                    break;
            }
        }

        private FileUploadDataEditPanel productDataEditPanels;

        protected FileUploadDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload"));
            }
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData();
            this.DataEditPanels.SetValue(Grid.RowProperty, 6);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 6);
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.DataEditPanels.SetValue(Grid.RowSpanProperty, 4);
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Right;
            this.DataEditPanels.isVisibility = true;
            this.LayoutRoot.Children.Add(DataEditPanels);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_PartsClaimOrderNewForDeliverParts;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsClaimOrderNewByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                DataEditPanels.FilePath = entity.Path;
                this.SetObjectToEdit(entity);

                this.DomainContext.Load(this.DomainContext.GetWarehousesQuery().Where(u => u.Id == entity.AgencyOutWarehouseId), LoadBehavior.RefreshCurrent, loadWarehouse => {
                    if(loadWarehouse.HasError) {
                        loadWarehouse.MarkErrorAsHandled();
                        return;
                    }
                    var warehouse = loadWarehouse.Entities.SingleOrDefault();
                    if(warehouse != null)
                        entity.AgencyOutWarehouseName = warehouse.Name;
                }, null);

                this.DomainContext.Load(this.DomainContext.获取CDC退货仓库Query(entity.PartsSalesCategoryId, entity.Branchid), LoadBehavior.RefreshCurrent, loadWarehouse => {
                    if(loadWarehouse.HasError) {
                        loadWarehouse.MarkErrorAsHandled();
                        return;
                    }
                    this.KvCDCReturnWarehouse.Clear();
                    if(loadWarehouse.Entities != null && loadWarehouse.Entities.Any()) {
                        foreach(var warehouse in loadWarehouse.Entities) {
                            this.KvCDCReturnWarehouse.Add(new KeyValuePair {
                                Key = warehouse.Id,
                                Value = warehouse.Name,
                                UserObject = warehouse
                            });
                        }
                    }
                }, null);
            }, null);
        }
        protected override void OnEditSubmitting() {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            if(partsClaimOrderNew.HasValidationErrors)
                return;
            ((IEditableObject)partsClaimOrderNew).EndEdit();
            try {
                if(partsClaimOrderNew.Can出库配件索赔单新)
                    partsClaimOrderNew.出库配件索赔单新();

            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

    }
}
