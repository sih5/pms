﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Common.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class PartsClaimOrderNewForHeadAuditDataEditView {
        private ObservableCollection<KeyValuePair> kvPartsSalesCategorys;
        public PartsClaimOrderNewForHeadAuditDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        private readonly string[] kvNames = {
            "Company_Type"

        };
        private KeyValueManager keyValueManager;

        private KeyValueManager KeyValueManager
        {
            get
            {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public ObservableCollection<KeyValuePair> KvPartsSalesCategorys
        {
            get
            {
                return this.kvPartsSalesCategorys ?? (this.kvPartsSalesCategorys = new ObservableCollection<KeyValuePair>());
            }
        }


        private FileUploadDataEditPanel productDataEditPanels;

        protected FileUploadDataEditPanel DataEditPanels {
            get {
                return this.productDataEditPanels ?? (this.productDataEditPanels = (FileUploadDataEditPanel)DI.GetDataEditPanel("FileUpload"));
            }
        }

        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_PartsClaimOrderNewForAudit;
            }
        }

        private void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("PartsClaimOrderNewForHeadAudit");
            this.LayoutRoot.Children.Add(dataEditPanel);
            this.DataEditPanels.SetValue(Grid.RowProperty, 7);
            this.DataEditPanels.SetValue(Grid.ColumnProperty, 7);
            this.DataEditPanels.SetValue(Grid.ColumnSpanProperty, 2);
            this.DataEditPanels.SetValue(Grid.RowSpanProperty, 3);
            this.DataEditPanels.isVisibility = true;
            this.DataEditPanels.HorizontalAlignment = HorizontalAlignment.Right;
            this.Root.Children.Add(DataEditPanels);
            this.RegisterButton(new ButtonItem { 
                Command = new DelegateCommand(this.RejectCurrentData),
                Title = ServiceUIStrings.Action_Title_Reject, 
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/reject.png", UriKind.Relative) 
            },true); 
        }
        private void RejectCurrentData() {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            partsClaimOrderNew.ValidationErrors.Clear();
            partsClaimOrderNew.ApprovalId = 0;
            if (string.IsNullOrEmpty(partsClaimOrderNew.ApproveComment)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_ApproveCommentNotNull);
                return;
            }
            if (partsClaimOrderNew.HasValidationErrors)
                return;

            ((IEditableObject)partsClaimOrderNew).EndEdit();
            try {
                if(partsClaimOrderNew.Can总部审核配件索赔单新)
                    partsClaimOrderNew.总部审核配件索赔单新();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsClaimOrderNewByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                DataEditPanels.FilePath = entity.Path;
                entity.ApproveComment = null;
                entity.ApprovalId = 1;
                this.SetObjectToEdit(entity);
            }, null);
        }
        protected override void OnEditSubmitting() {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;

            partsClaimOrderNew.ValidationErrors.Clear();
            partsClaimOrderNew.ApprovalId = 1;
            if (partsClaimOrderNew.ApprovalId == 0 && string.IsNullOrEmpty(partsClaimOrderNew.ApproveComment))
            {
                partsClaimOrderNew.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_ApproveCommentNotNull, new[] {
                    "ApproveComment"
                }));
            }
            if (partsClaimOrderNew.HasValidationErrors)
                return;

            ((IEditableObject)partsClaimOrderNew).EndEdit();
            try {
                if(partsClaimOrderNew.Can总部审核配件索赔单新)
                    partsClaimOrderNew.总部审核配件索赔单新();

            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

    }
}
