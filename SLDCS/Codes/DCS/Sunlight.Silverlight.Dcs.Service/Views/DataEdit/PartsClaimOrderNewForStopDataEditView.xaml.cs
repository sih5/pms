﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class PartsClaimOrderNewForStopDataEditView {
        public PartsClaimOrderNewForStopDataEditView() {
            InitializeComponent();
        }

        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_PartsClaimOrderNewForStop;
            }
        }
        protected override bool OnRequestCanSubmit() {
            return true;
        }
        protected override void OnEditSubmitting() {
            var partsClaimOrderNew = this.DataContext as PartsClaimOrderNew;
            if(partsClaimOrderNew == null)
                return;
            partsClaimOrderNew.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(partsClaimOrderNew.StopReason)) {
                partsClaimOrderNew.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_PartsClaimOrderNew_StopReason, new[] {
                        "StopReason"
                    }));
            }
            if(partsClaimOrderNew.HasValidationErrors)
                return;
            if(partsClaimOrderNew.Can终止配件索赔单新)
                partsClaimOrderNew.终止配件索赔单新();
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetPartsClaimOrderNewsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
