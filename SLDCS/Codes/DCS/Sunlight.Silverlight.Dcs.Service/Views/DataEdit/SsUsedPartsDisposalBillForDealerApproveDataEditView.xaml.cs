﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class SsUsedPartsDisposalBillForDealerApproveDataEditView {

        private DataGridViewBase ssUsedPartsDisposalDetailForApproveDataGridView;

        public SsUsedPartsDisposalBillForDealerApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase SsUsedPartsDisposalDetailForApproveDataGridView {
            get {
                if(this.ssUsedPartsDisposalDetailForApproveDataGridView == null) {
                    this.ssUsedPartsDisposalDetailForApproveDataGridView = DI.GetDataGridView("SsUsedPartsDisposalDetailForApprove");
                    this.ssUsedPartsDisposalDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.ssUsedPartsDisposalDetailForApproveDataGridView;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSsUsedPartsDisposalBillsWithDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("SsUsedPartsDisposalBillForDealerApprove"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Content = this.SsUsedPartsDisposalDetailForApproveDataGridView,
                Header = Utils.GetEntityLocalizedName(typeof(SsUsedPartsDisposalBill), "SsUsedPartsDisposalDetails")
            });
            tabControl.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(tabControl);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override string Title {
            get {
                return Service.Resources.ServiceUIStrings.DataEditView_Title_ApproveSsUsedPartsDisposalBill;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.ssUsedPartsDisposalDetailForApproveDataGridView.CommitEdit())
                return;
            var ssUsedPartsDisposalBill = this.DataContext as SsUsedPartsDisposalBill;
            if(ssUsedPartsDisposalBill == null || ssUsedPartsDisposalBill.HasValidationErrors)
                return;
            ssUsedPartsDisposalBill.ValidationErrors.Clear();
            ((IEditableObject)ssUsedPartsDisposalBill).EndEdit();
            try {
                if(ssUsedPartsDisposalBill.Can审批服务站旧件处理单)
                    ssUsedPartsDisposalBill.审批服务站旧件处理单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
