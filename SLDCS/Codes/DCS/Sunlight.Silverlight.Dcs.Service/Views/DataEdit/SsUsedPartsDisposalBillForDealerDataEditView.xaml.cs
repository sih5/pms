﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class SsUsedPartsDisposalBillForDealerDataEditView {
        private DataGridViewBase ssUsedPartsDisposalDetailForEditDataGridView;

        public SsUsedPartsDisposalBillForDealerDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged -= this.SsUsedPartsDisposalBillForDealerDataEditView_DataContextChanged;
            this.DataContextChanged += this.SsUsedPartsDisposalBillForDealerDataEditView_DataContextChanged;
        }

        private DataGridViewBase SsUsedPartsDisposalDetailForEditDataGridView {
            get {
                if(this.ssUsedPartsDisposalDetailForEditDataGridView == null) {
                    this.ssUsedPartsDisposalDetailForEditDataGridView = DI.GetDataGridView("SsUsedPartsDisposalDetailForEdit");
                    this.ssUsedPartsDisposalDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.ssUsedPartsDisposalDetailForEditDataGridView;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSsUsedPartsDisposalBillsWithDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("SsUsedPartsDisposalBillForDealer"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(SsUsedPartsDisposalBill), "SsUsedPartsDisposalDetails"), null, () => this.SsUsedPartsDisposalDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        private void SsUsedPartsDisposalBillForDealerDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var ssUsedPartsDisposalBill = this.DataContext as SsUsedPartsDisposalBill;
            if(ssUsedPartsDisposalBill == null || !this.SsUsedPartsDisposalDetailForEditDataGridView.CommitEdit())
                return;
            ssUsedPartsDisposalBill.PropertyChanged -= this.SsUsedPartsDisposalBill_PropertyChanged;
            ssUsedPartsDisposalBill.PropertyChanged += this.SsUsedPartsDisposalBill_PropertyChanged;
        }

        private void SsUsedPartsDisposalBill_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName == "BranchName" || e.PropertyName == "UsedPartsDisposalMethod") {
                var ssUsedPartsDisposalBill = this.DataContext as SsUsedPartsDisposalBill;
                if(ssUsedPartsDisposalBill != null) {
                    //TODO:需要DataGridViewBase暴露CancelEdit方法，来解决数据表格中有校验异常数据，退出重新进入后报错问题。
                    var gridView = this.SsUsedPartsDisposalDetailForEditDataGridView.FindChildByType<RadGridView>();
                    gridView.CancelEdit();
                    foreach(var ssUsedPartsDisposalDetail in ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.ToList()) {
                        ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.Remove(ssUsedPartsDisposalDetail);
                    }
                    ssUsedPartsDisposalBill.TotalAmount = default(int);
                }
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_SsUsedPartsDisposalBill;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.ssUsedPartsDisposalDetailForEditDataGridView.CommitEdit())
                return;
            var ssUsedPartsDisposalBill = this.DataContext as SsUsedPartsDisposalBill;
            if(ssUsedPartsDisposalBill == null)
                return;
            ssUsedPartsDisposalBill.ValidationErrors.Clear();
            if(ssUsedPartsDisposalBill.BranchId == default(int))
                ssUsedPartsDisposalBill.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_SsUsedPartsDisposalBill_BranchIdIsNull, new[] {
                    "BranchId"
                }));
            if(!ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.Any()) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_SsUsedPartsDisposalBill_SsUsedPartsDisposalDetailsIsEmpty);
                return;
            }
            foreach(var ssUsedPartsDisposalDetail in ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails) {
                ssUsedPartsDisposalDetail.ValidationErrors.Clear();
                if(ssUsedPartsDisposalDetail.SerialNumber == default(int))
                    ssUsedPartsDisposalDetail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_SsUsedPartsDisposalDetail_UsedPartsBarCodeIsNull, new[] {
                        "UsedPartsBarCode"
                    }));
                if(string.IsNullOrWhiteSpace(ssUsedPartsDisposalDetail.ClaimBillCode))
                    ssUsedPartsDisposalDetail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_SsUsedPartsDisposalDetail_ClaimBillCodeIsNull, new[] {
                        "UsedPartsBarCode"
                    }));
                if(ssUsedPartsDisposalDetail.UsedPartsId == default(int))
                    ssUsedPartsDisposalDetail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_SsUsedPartsDisposalDetail_UsedPartsIdIsNull, new[] {
                        "UsedPartsBarCode"
                    }));
                if(ssUsedPartsDisposalDetail.UsedPartsReturnPolicy == default(int))
                    ssUsedPartsDisposalDetail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_SsUsedPartsDisposalDetail_UsedPartsReturnPolicyIsNull, new[] {
                        "UsedPartsBarCode"
                    }));
                if(string.IsNullOrWhiteSpace(ssUsedPartsDisposalDetail.UsedPartsBarCode))
                    ssUsedPartsDisposalDetail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_SsUsedPartsDisposalDetail_UsedPartsBarCodeIsNull, new[] {
                        "UsedPartsBarCode"
                    }));
                if(ssUsedPartsDisposalDetail.FaultyPartsId == default(int))
                    ssUsedPartsDisposalDetail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_SsUsedPartsDisposalDetail_FaultyPartsIdIsNull, new[] {
                        "UsedPartsBarCode"
                    }));
                if(ssUsedPartsDisposalDetail.FaultyPartsSupplierId == default(int))
                    ssUsedPartsDisposalDetail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_SsUsedPartsDisposalDetail_FaultyPartsSupplierIdIsNull, new[] {
                        "UsedPartsBarCode"
                    }));
            }
            if(ssUsedPartsDisposalBill.HasValidationErrors || ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.Any(detail => detail.HasValidationErrors))
                return;
            foreach(var entity in ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails)
                ((IEditableObject)entity).EndEdit();
            ((IEditableObject)ssUsedPartsDisposalBill).EndEdit();
            if(EditState == DataEditState.Edit) {
                try {
                    if(ssUsedPartsDisposalBill.Can修改服务站旧件处理单)
                        ssUsedPartsDisposalBill.修改服务站旧件处理单();
                } catch(ValidationException ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    return;
                }
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
