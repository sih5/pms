﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class SsUsedPartsStorageDataEditView {
        private ObservableCollection<SsUsedPartsStorage> ssUsedPartsStorages;
        private DataGridViewBase ssUsedPartsStorageForEditDataGridView;

        public SsUsedPartsStorageDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.SsUsedPartsStorageDataEditView_DataContextChanged;
        }

        private void SsUsedPartsStorageDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            this.SsUsedPartsStorages.Clear();
        }

        private void CreateUI() {
            this.Root.Children.Add(this.SsUsedPartsStorageForEditDataGridView);
        }

        private DataGridViewBase SsUsedPartsStorageForEditDataGridView {
            get {
                if(this.ssUsedPartsStorageForEditDataGridView == null) {
                    this.ssUsedPartsStorageForEditDataGridView = DI.GetDataGridView("SsUsedPartsStorageForEdit");
                    this.ssUsedPartsStorageForEditDataGridView.DataContext = this;
                    this.ssUsedPartsStorageForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.ssUsedPartsStorageForEditDataGridView;
            }
        }

        private void LoadEntityToEdit(int[] ids) {
            this.DomainContext.Load(this.DomainContext.GetSsUsedPartsStoragesWithDealerByIdsQuery(ids), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.SsUsedPartsStorages.Clear();
                var entity = loadOp.Entities.First();
                if(loadOp.Entities.Any()) {
                    this.SetObjectToEdit(entity);
                    foreach(var storage in loadOp.Entities) {
                        storage.Quantity = 1;
                        this.SsUsedPartsStorages.Add(storage);
                      
                    }
                }
            }, null);
        }

        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_SsUsedPartsStorageDataEditView;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.SsUsedPartsStorageForEditDataGridView.CommitEdit())
                return;
            try {
                foreach(var ssUsedPartsStorage in this.SsUsedPartsStorages.Where(entity => entity.EntityState != EntityState.Unmodified)) {
                    ssUsedPartsStorage.ValidationErrors.Clear();
                    ((IEditableObject)ssUsedPartsStorage).EndEdit();
                    if(ssUsedPartsStorage.Can修改服务站旧件库存)
                        ssUsedPartsStorage.修改服务站旧件库存();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            if(SsUsedPartsStorages != null)
                this.SsUsedPartsStorages.Clear();
            base.OnEditSubmitted();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int[])id));
            else
                this.LoadEntityToEdit((int[])id);
        }

        public ObservableCollection<SsUsedPartsStorage> SsUsedPartsStorages {
            get {
                return this.ssUsedPartsStorages ?? (this.ssUsedPartsStorages = new ObservableCollection<SsUsedPartsStorage>());
            }
        }
    }
}
