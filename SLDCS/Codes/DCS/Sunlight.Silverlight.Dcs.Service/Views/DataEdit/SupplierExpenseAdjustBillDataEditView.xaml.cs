﻿

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class SupplierExpenseAdjustBillDataEditView : INotifyPropertyChanged {
        private readonly string[] kvNames = {
          "ExpenseAdjustmentBill_TransactionCategory","ExpenseAdjustmentBill_DebitOrReplenish","ExpenseAdjustmentBill_SourceType","ServiceProductLineView_ProductLineType"
        };
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public object ProductLineTypes {
            get {
                return this.KeyValueManager[this.kvNames[3]];
            }
        }

        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        protected override string BusinessName {
            get {
                return "供应商扣补款单";
            }
        }
        private int? productLineType;
        private readonly DcsDomainContext domainContext = new DcsDomainContext();
        private Company companys;

        private string serviceProductName;
        public string ServiceProductName {
            get {
                return this.serviceProductName;
            }
            set {
                this.serviceProductName = value;
                this.OnPropertyChanged("ServiceProductName");
            }
        }

        private QueryWindowBase serviceTripClaimBillQueryWindow;
        private QueryWindowBase repairClaimBillQueryWindow;
        private QueryWindowBase maintainClaimBillQueryWindow;
        private QueryWindowBase servicesClaimBillQueryWindow;
        private ObservableCollection<KeyValuePair> kvBranchs;
        public ObservableCollection<KeyValuePair> KvBranchs {
            get {
                return this.kvBranchs ?? (this.kvBranchs = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<KeyValuePair> kvServiceProducts;
        public ObservableCollection<KeyValuePair> KvServiceProducts {
            get {
                return this.kvServiceProducts ?? (this.kvServiceProducts = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> kvSourceTypes;
        public ObservableCollection<KeyValuePair> KvSourceTypes {
            get {
                return this.kvSourceTypes ?? (this.kvSourceTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<KeyValuePair> kvPartsSalesCategory;
        public ObservableCollection<KeyValuePair> KvPartsSalesCategory {
            get {
                return this.kvPartsSalesCategory ?? (this.kvPartsSalesCategory = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<KeyValuePair> kvServiceProductLineView;
        public ObservableCollection<KeyValuePair> KvServiceProductLineView {
            get {
                return this.kvServiceProductLineView ?? (this.kvServiceProductLineView = new ObservableCollection<KeyValuePair>());
            }
        }

        public SupplierExpenseAdjustBillDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += SupplierExpenseAdjustBillDataEditView_DataContextChanged;

        }


        private void CreateUI() {
            this.KeyValueManager.LoadData(() => {
                this.KvSourceTypes.Clear();
                foreach(var kvPair in this.KeyValueManager[kvNames[2]].Where(e => e.Key != (int)DcsExpenseAdjustmentBillSourceType.配件索赔)) {
                    KvSourceTypes.Add(kvPair);
                }
            });
            var partsSupplier = DI.GetQueryWindow("PartsSupplier");//选择配件供应商
            partsSupplier.SelectionDecided += partsSupplier_SelectionDecided;
            this.PartsSupplierPopupTextBox.PopupContent = partsSupplier;
            var responsibleUnitPopupTextBox = DI.GetQueryWindow("ResponsibleUnit");
            responsibleUnitPopupTextBox.SelectionDecided += this.ResponsibleUnitPopupTextBox_SelectionDecided;
            this.ResponsibleUnitPopupTextBox.Title = ServiceUIStrings.QueryPanel_Title_ResponsibleUnit;
            this.ResponsibleUnitPopupTextBox.PopupContent = responsibleUnitPopupTextBox;

            this.DomainContext.Load(this.DomainContext.GetBranchesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvBranchs.Clear();
                //var i = 1;
                foreach(var branch in loadOp.Entities) {
                    this.KvBranchs.Add(new KeyValuePair {
                        Key = branch.Id,
                        Value = branch.Name,
                        UserObject = branch
                    });
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(ex => ex.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp1 => {
                if(loadOp1.HasError)
                    return;
                this.KvPartsSalesCategory.Clear();
                foreach(var partsSalesCategory in loadOp1.Entities) {
                    this.KvPartsSalesCategory.Add(new KeyValuePair {
                        Key = partsSalesCategory.Id,
                        Value = partsSalesCategory.Name,
                        UserObject = partsSalesCategory
                    });
                }
            }, null);
            var button = this.SourcePopupTextBox.ChildrenOfType<RadButton>().First();
            if(button != null)
                button.Click += button_Click;
        }

        //提示：请先选择源单据类型
        private void button_Click(object sender, RoutedEventArgs e) {
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;
            if(supplierExpenseAdjustBill == null)
                return;
            if(supplierExpenseAdjustBill.SourceType == null) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_ExpenseAdjustmentBill_SelectSourceType);
                this.SourcePopupTextBox.PopupContent = null;
                return;
            }
        }
        private void ResponsibleUnitPopupTextBox_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var responsibleUnit = queryWindow.SelectedEntities.Cast<ResponsibleUnit>().FirstOrDefault();
            if(responsibleUnit == null)
                return;
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;
            if(supplierExpenseAdjustBill == null)
                return;
            this.DomainContext.Load(this.DomainContext.GetResponsibleUnitsQuery().Where(entity => entity.Id == responsibleUnit.Id && entity.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                supplierExpenseAdjustBill.ResponsibleUnit = loadOp.Entities.SingleOrDefault();

                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void partsSupplier_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var partsSupplier = queryWindow.SelectedEntities.Cast<PartsSupplier>().FirstOrDefault();
            if(partsSupplier == null)
                return;
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;
            if(supplierExpenseAdjustBill == null)
                return;
            supplierExpenseAdjustBill.SupplierName = partsSupplier.Name;
            supplierExpenseAdjustBill.SupplierId = partsSupplier.Id;
            supplierExpenseAdjustBill.SupplierCode = partsSupplier.Code;
            var parent1 = queryWindow.ParentOfType<RadWindow>();
            if(parent1 != null)
                parent1.Close();
        }

        private void SupplierExpenseAdjustBillDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;
            if(supplierExpenseAdjustBill == null)
                return;
            BranchEnterprise();
            supplierExpenseAdjustBill.PropertyChanged -= supplierExpenseAdjustBill_PropertyChanged;
            supplierExpenseAdjustBill.PropertyChanged += supplierExpenseAdjustBill_PropertyChanged;
        }

        private void BranchEnterprise() {
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;
            if(supplierExpenseAdjustBill == null)
                return;
            if(supplierExpenseAdjustBill.EntityState == EntityState.Detached) {
                domainContext.Load(domainContext.GetCompaniesQuery().Where(ex => ex.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    var company = loadOp.Entities.FirstOrDefault();
                    if(company == null)
                        return;
                    this.companys = company;
                    if(companys.Type == (int)DcsCompanyType.分公司) {
                        supplierExpenseAdjustBill.BranchId = companys.Id;
                        supplierExpenseAdjustBill.BranchCode = companys.Code;
                        supplierExpenseAdjustBill.BranchName = companys.Name;
                        this.cbxBranch.IsEnabled = false;
                        this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && r.Status == (int)DcsBaseDataStatus.有效), loadOp1 => {
                            if(loadOp1.HasError)
                                return;
                            this.KvPartsSalesCategory.Clear();
                            var partsSalesCategories = loadOp1.Entities;
                            if(partsSalesCategories == null)
                                return;
                            foreach(var partsSalesCategory in loadOp1.Entities) {
                                this.KvPartsSalesCategory.Add(new KeyValuePair {
                                    Key = partsSalesCategory.Id,
                                    Value = partsSalesCategory.Name,
                                    UserObject = partsSalesCategory
                                });
                            }
                        }, null);
                    } else {
                        this.cbxBranch.IsEnabled = true;
                    }
                }, null);
            } else {
                return;
            }
        }

        private int keyId;
        public int KeyId {
            get {
                return this.keyId;
            }
            set {
                this.keyId = value;
                this.OnPropertyChanged("KeyId");
            }
        }

        private void supplierExpenseAdjustBill_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;
            if(supplierExpenseAdjustBill == null)
                return;
            switch(e.PropertyName) {
                case "SourceType":
                    supplierExpenseAdjustBill.SourceCode = "";
                    supplierExpenseAdjustBill.SourceId = 0;
                    switch(supplierExpenseAdjustBill.SourceType) {
                        case (int)DcsExpenseAdjustmentBillSourceType.保养索赔:
                            this.SourcePopupTextBox.PopupContent = this.MaintainClaimBillQueryWindow;
                            this.SourcePopupTextBox.Title = ServiceUIStrings.QueryPanel_Title_MaintainClaimBill;
                            break;
                        case (int)DcsExpenseAdjustmentBillSourceType.服务活动索赔:
                            this.SourcePopupTextBox.PopupContent = this.ServicesClaimBillQueryWindow;
                            this.SourcePopupTextBox.Title = ServiceUIStrings.QueryPanel_Title_ServiceActivity;
                            break;
                        case (int)DcsExpenseAdjustmentBillSourceType.外出服务索赔:
                            this.SourcePopupTextBox.PopupContent = this.ServiceTripClaimBillQueryWindow;
                            this.SourcePopupTextBox.Title = ServiceUIStrings.QueryPanel_Title_ServiceTripClaimBill;
                            break;
                        case (int)DcsExpenseAdjustmentBillSourceType.维修索赔:
                            this.SourcePopupTextBox.PopupContent = this.RepairClaimBillQueryWindow;
                            this.SourcePopupTextBox.Title = ServiceUIStrings.QueryPanel_Title_RepairClaimBill;
                            break;
                    }
                    break;
                case "BranchId":
                    this.KvServiceProductLineView.Clear();
                    supplierExpenseAdjustBill.ProductLineType = default(int);
                    domainContext.Load(domainContext.GetCompaniesQuery().Where(ex => ex.Id == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            return;
                        var company = loadOp.Entities.FirstOrDefault();
                        if(company == null)
                            return;
                        this.companys = company;
                        if(companys.Type != (int)DcsCompanyType.分公司) {
                            var singleOrDefault = this.KvBranchs.SingleOrDefault(r => r.Key == supplierExpenseAdjustBill.BranchId);
                            if(singleOrDefault != null) {
                                var branch = singleOrDefault.UserObject as Branch;
                                if(branch != null) {
                                    supplierExpenseAdjustBill.BranchCode = branch.Code;
                                    supplierExpenseAdjustBill.BranchName = branch.Name;
                                }
                            }
                            this.DomainContext.Load(this.DomainContext.GetPartsSalesCategoriesQuery().Where(r => r.BranchId == supplierExpenseAdjustBill.BranchId && r.Status == (int)DcsBaseDataStatus.有效), loadOp1 => {
                                if(loadOp1.HasError)
                                    return;
                                this.KvPartsSalesCategory.Clear();
                                var partsSalesCategories = loadOp1.Entities;
                                if(partsSalesCategories == null)
                                    return;
                                foreach(var partsSalesCategory in loadOp1.Entities) {
                                    this.KvPartsSalesCategory.Add(new KeyValuePair {
                                        Key = partsSalesCategory.Id,
                                        Value = partsSalesCategory.Name,
                                        UserObject = partsSalesCategory
                                    });
                                }
                            }, null);
                        }
                    }, null);
                    break;
                case "PartsSalesCategoryId":
                    this.KvServiceProductLineView.Clear();
                    supplierExpenseAdjustBill.ProductLineType = default(int);
                    this.DomainContext.Load(this.DomainContext.GetServiceProductLineViewsQuery().Where(ex => ex.PartsSalesCategoryId == supplierExpenseAdjustBill.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        if(loadOp.Entities.Any() && loadOp.Entities.Count() != default(int)) {
                            var i = 1;
                            foreach(var serviceProductLineView in loadOp.Entities) {
                                this.KvServiceProductLineView.Add(new KeyValuePair {
                                    Key = i++,
                                    Value = serviceProductLineView.ProductLineName,
                                    UserObject = serviceProductLineView
                                });
                            }
                        }
                    }, null);
                    break;
            }
        }

        private QueryWindowBase MaintainClaimBillQueryWindow {
            get {
                if(this.maintainClaimBillQueryWindow == null) {
                    this.maintainClaimBillQueryWindow = DI.GetQueryWindow("MaintainClaimBillForSupplierExpenseAdjust");
                    this.maintainClaimBillQueryWindow.SelectionDecided += this.MaintainClaimBillQueryWindow_SelectionDecided;
                    this.maintainClaimBillQueryWindow.Loaded += this.MaintainClaimBillQueryWindow_Loaded;
                }
                return this.maintainClaimBillQueryWindow;
            }
        }

        private void MaintainClaimBillQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;

            if(queryWindow == null || supplierExpenseAdjustBill == null)
                return;
            if(supplierExpenseAdjustBill.SupplierId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_SupplierExpenseAdjustBill_SelectSupplier);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
            if(supplierExpenseAdjustBill.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_ExpenseAdjustmentBill_SelectPartsSalesCategory);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SupplierId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = supplierExpenseAdjustBill.SupplierId
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesCategoryId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = supplierExpenseAdjustBill.PartsSalesCategoryId
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common","PartsSalesCategoryId",supplierExpenseAdjustBill.PartsSalesCategoryId
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common","PartsSalesCategoryId",false
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }
        private void MaintainClaimBillQueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            //var repairClaimBill = queryWindow.SelectedEntities.Cast<RepairClaimBill>().FirstOrDefault();
            //if(repairClaimBill == null)
            //    return;
            //var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;
            //if(supplierExpenseAdjustBill == null)
            //    return;
            //supplierExpenseAdjustBill.SourceCode = repairClaimBill.ClaimBillCode;
            //supplierExpenseAdjustBill.SourceId = repairClaimBill.Id;
            //this.DomainContext.Load(this.DomainContext.GetResponsibleUnitsQuery().Where(entity => entity.Id == repairClaimBill.ResponsibleUnitId && entity.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError) {
            //        if(loadOp.IsErrorHandled)
            //            loadOp.MarkErrorAsHandled();
            //        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
            //        return;
            //    }
            //    supplierExpenseAdjustBill.ResponsibleUnit = loadOp.Entities.SingleOrDefault();

            //    var parent = queryWindow.ParentOfType<RadWindow>();
            //    if(parent != null)
            //        parent.Close();
            //}, null);
        }

        private QueryWindowBase ServicesClaimBillQueryWindow {
            get {
                if(this.servicesClaimBillQueryWindow == null) {
                    this.servicesClaimBillQueryWindow = DI.GetQueryWindow("ServicesClaimBillForSupplierExpenseAdjust");
                    this.servicesClaimBillQueryWindow.SelectionDecided += this.MaintainClaimBillQueryWindow_SelectionDecided;
                    this.servicesClaimBillQueryWindow.Loaded += servicesClaimBillQueryWindow_Loaded;
                }
                return this.servicesClaimBillQueryWindow;
            }
        }

        private void servicesClaimBillQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;

            if(queryWindow == null || supplierExpenseAdjustBill == null)
                return;
            if(supplierExpenseAdjustBill.SupplierId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_SupplierExpenseAdjustBill_SelectSupplier);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
            if(supplierExpenseAdjustBill.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_ExpenseAdjustmentBill_SelectPartsSalesCategory);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SupplierId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = supplierExpenseAdjustBill.SupplierId
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesCategoryId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = supplierExpenseAdjustBill.PartsSalesCategoryId
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private QueryWindowBase ServiceTripClaimBillQueryWindow {
            get {
                if(this.serviceTripClaimBillQueryWindow == null) {
                    this.serviceTripClaimBillQueryWindow = DI.GetQueryWindow("ServiceTripClaimBill");
                    this.serviceTripClaimBillQueryWindow.SelectionDecided += this.ServiceTripClaimBillQueryWindow_SelectionDecided;
                    this.serviceTripClaimBillQueryWindow.Loaded += this.ServiceTripClaimBillQueryWindow_Loaded;
                }
                return this.serviceTripClaimBillQueryWindow;
            }
        }

        private void ServiceTripClaimBillQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;

            if(queryWindow == null || supplierExpenseAdjustBill == null)
                return;
            if(supplierExpenseAdjustBill.SupplierId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_SupplierExpenseAdjustBill_SelectSupplier);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
            if(supplierExpenseAdjustBill.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_ExpenseAdjustmentBill_SelectPartsSalesCategory);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.LogicalOperator = LogicalOperator.And;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "ClaimSupplierId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = supplierExpenseAdjustBill.SupplierId
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesCategoryId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = supplierExpenseAdjustBill.PartsSalesCategoryId
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }
        private void ServiceTripClaimBillQueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            //var serviceTripClaimBill = queryWindow.SelectedEntities.Cast<ServiceTripClaimBill>().FirstOrDefault();
            //if(serviceTripClaimBill == null)
            //    return;
            //var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;
            //if(supplierExpenseAdjustBill == null)
            //    return;
            //supplierExpenseAdjustBill.SourceCode = serviceTripClaimBill.Code;
            //supplierExpenseAdjustBill.SourceId = serviceTripClaimBill.Id;
            //this.DomainContext.Load(this.DomainContext.GetResponsibleUnitsQuery().Where(entity => entity.Id == serviceTripClaimBill.ResponsibleUnitId && entity.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError) {
            //        if(loadOp.IsErrorHandled)
            //            loadOp.MarkErrorAsHandled();
            //        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
            //        return;
            //    }
            //    supplierExpenseAdjustBill.ResponsibleUnit = loadOp.Entities.SingleOrDefault();

            //    var parent = queryWindow.ParentOfType<RadWindow>();
            //    if(parent != null)
            //        parent.Close();
            //}, null);
        }

        private QueryWindowBase RepairClaimBillQueryWindow {
            get {
                if(this.repairClaimBillQueryWindow == null) {
                    this.repairClaimBillQueryWindow = DI.GetQueryWindow("RepairClaimBillForSupplierExpenseAdjust");
                    this.repairClaimBillQueryWindow.SelectionDecided += this.RepairClaimBillQueryWindow_SelectionDecided;
                    this.repairClaimBillQueryWindow.Loaded += this.repairClaimBillQueryWindow_Loaded;
                    this.repairClaimBillQueryWindow.MaxWidth = 1200;
                }
                return this.repairClaimBillQueryWindow;
            }
        }

        private void repairClaimBillQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;

            if(queryWindow == null || supplierExpenseAdjustBill == null)
                return;
            if(supplierExpenseAdjustBill.SupplierId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_SupplierExpenseAdjustBill_SelectSupplier);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
            if(supplierExpenseAdjustBill.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_ExpenseAdjustmentBill_SelectPartsSalesCategory);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SupplierId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = supplierExpenseAdjustBill.SupplierId
            });
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "PartsSalesCategoryId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = supplierExpenseAdjustBill.PartsSalesCategoryId
            });
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }
        private void RepairClaimBillQueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            //var repairClaimBill = queryWindow.SelectedEntities.Cast<VirtualRepairClaimBillForBussiness>().FirstOrDefault();
            //if(repairClaimBill == null)
            //    return;
            //var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;
            //if(supplierExpenseAdjustBill == null)
            //    return;
            //supplierExpenseAdjustBill.SourceCode = repairClaimBill.ClaimBillCode;
            //supplierExpenseAdjustBill.SourceId = repairClaimBill.Id;
            //this.DomainContext.Load(this.DomainContext.GetResponsibleUnitsQuery().Where(entity => entity.Id == repairClaimBill.ResponsibleUnitId && entity.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
            //    if(loadOp.HasError) {
            //        if(loadOp.IsErrorHandled)
            //            loadOp.MarkErrorAsHandled();
            //        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
            //        return;
            //    }
            //    supplierExpenseAdjustBill.ResponsibleUnit = loadOp.Entities.SingleOrDefault();

            //    var parent = queryWindow.ParentOfType<RadWindow>();
            //    if(parent != null)
            //        parent.Close();
            //}, null);
        }



        protected override void OnEditSubmitting() {
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;
            if(supplierExpenseAdjustBill == null)
                return;
            supplierExpenseAdjustBill.ValidationErrors.Clear();
            if(supplierExpenseAdjustBill.TransactionCategory == default(int))
                supplierExpenseAdjustBill.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_ExpenseAdjustmentBill_TransactionCategoryIsNull, new[] {
                    "TransactionCategory"
                }));
            if(supplierExpenseAdjustBill.DebitOrReplenish == default(int))
                supplierExpenseAdjustBill.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_ExpenseAdjustmentBill_DebitOrReplenishIsNull, new[] {
                    "DebitOrReplenish"
                }));
            if(supplierExpenseAdjustBill.TransactionAmount <= 0)
                supplierExpenseAdjustBill.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_ExpenseAdjustmentBill_TransactionAmountLessThenZero, new[] {
                    "TransactionAmount"
                }));
            if(supplierExpenseAdjustBill.IfClaimToResponsible) {
                if(supplierExpenseAdjustBill.ResponsibleUnitId == default(int)) {
                    UIHelper.ShowAlertMessage(ServiceUIStrings.DataEditView_Validation_RepairOrder_ResponsibleUnitIsNull);
                    return;
                }
            }
            if(supplierExpenseAdjustBill.HasValidationErrors)
                return;
            ((IEditableObject)supplierExpenseAdjustBill).EndEdit();

            base.OnEditSubmitting();
        }


        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSupplierExpenseAdjustBillByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();

                if(entity != null) {

                    var domainContext = new DcsDomainContext();
                    if(entity.ServiceProductLineId != default(int) && entity.ServiceProductLineId != null) {
                        domainContext.Load(domainContext.GetServiceProductLineViewsQuery().Where(e => e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId && e.ProductLineId == entity.ServiceProductLineId), LoadBehavior.RefreshCurrent, loadUp => {
                            if(loadUp.HasError)
                                return;
                            KvServiceProducts.Clear();
                            foreach(var item in loadUp.Entities) {
                                this.KvServiceProducts.Add(new KeyValuePair {
                                    Key = item.ProductLineId,
                                    Value = item.ProductLineName
                                });
                            }
                            this.DomainContext.Load(this.DomainContext.GetServiceProductLineViewsQuery().Where(ex => ex.PartsSalesCategoryId == entity.PartsSalesCategoryId), LoadBehavior.RefreshCurrent, loadOp1 => {
                                if(loadOp1.HasError) {
                                    if(loadOp1.IsErrorHandled)
                                        loadOp1.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp1);
                                    return;
                                }
                                if(loadOp1.Entities.Any() && loadOp1.Entities.Count() != default(int)) {
                                    var i = 1;
                                    foreach(var serviceProductLineView in loadOp1.Entities) {
                                        this.KvServiceProductLineView.Add(new KeyValuePair {
                                            Key = i++,
                                            Value = serviceProductLineView.ProductLineName,
                                            UserObject = serviceProductLineView
                                        });
                                    }
                                }
                                ServiceProductName = this.KvServiceProducts.SingleOrDefault().Value;
                                keyId = this.KvServiceProductLineView.SingleOrDefault(ex => ex.Value == ServiceProductName).Key;
                            }, null);
                        }, null);
                    }
                    //productLineType1 = entity.ProductLineType;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public object KvTransactionCategorys {
            get {
                return this.keyValueManager[this.kvNames[0]];
            }
        }

        public object KvDebitOrReplenishes {
            get {
                return this.keyValueManager[this.kvNames[1]];
            }
        }


        private void DcsComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e) {
            var supplierExpenseAdjustBill = this.DataContext as SupplierExpenseAdjustBill;
            if(supplierExpenseAdjustBill == null)
                return;
            if(KeyId == 0)
                return;
            var serviceProductLineView = this.KvServiceProductLineView.SingleOrDefault(ex => ex.Key == KeyId).UserObject as ServiceProductLineView;

            supplierExpenseAdjustBill.ProductLineType = (int)serviceProductLineView.ProductLineType;
            productLineType = (int)serviceProductLineView.ProductLineType;
            ServiceProductName = serviceProductLineView.ProductLineName;
            supplierExpenseAdjustBill.ServiceProductLineId = serviceProductLineView.ProductLineId;
        }

        protected override void Reset() {
            KeyId = 0;
            this.KvServiceProductLineView.Clear();
        }

        protected override void OnEditCancelled() {
            this.DataContext = null;
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            this.DataContext = null;
            this.KvServiceProductLineView.Clear();
            base.OnEditSubmitted();
        }
    }
}
