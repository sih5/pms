﻿
using System.Linq;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class SupplierExpenseAdjustBillDetailDataEditView {
        public SupplierExpenseAdjustBillDetailDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
        }
        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        private readonly string[] kvNames = {
          "ExpenseAdjustmentBill_TransactionCategory","ExpenseAdjustmentBill_DebitOrReplenish","ExpenseAdjustmentBill_SourceType","ClaimBill_SettlementStatus","ExpenseAdjustmentBill_Status","ServiceProductLineView_ProductLineType"
        };
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetSupplierExpenseAdjustBillByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

    }
}
