﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class SupplierExpenseAdjustBillForImportDataEditView {
        private DataGridViewBase expenseAdjustmentBillForImportDataGridView;
        private const string EXPORT_DATA_FILE_NAME = "导出供应商扣补款单模版.xlsx";
        private ICommand exportFileCommand;
        private ICommand importCommand1;
        public new event EventHandler EditSubmitted;
        public SupplierExpenseAdjustBillForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return "导入扣补款信息";
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                if(this.expenseAdjustmentBillForImportDataGridView == null) {
                    this.expenseAdjustmentBillForImportDataGridView = DI.GetDataGridView("SupplierExpenseAdjustBillForImport");
                    this.expenseAdjustmentBillForImportDataGridView.DataContext = this.DataContext;
                }
                return this.expenseAdjustmentBillForImportDataGridView;
            }
        }

        private ObservableCollection<SupplierExpenseAdjustBill> supplierExpenseAdjustBills;

        public ObservableCollection<SupplierExpenseAdjustBill> SupplierExpenseAdjustBills {
            get {
                if(this.supplierExpenseAdjustBills == null) {
                    this.supplierExpenseAdjustBills = new ObservableCollection<SupplierExpenseAdjustBill>();
                } return supplierExpenseAdjustBills;
            }
        }

        private void CreateUI() {
            this.ShowSaveButton();
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = "供应商扣补款导入清单",
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportSupplierExpenseAdjustBillAsync(fileName, BaseApp.Current.CurrentUserData.EnterpriseId);
            this.ExcelServiceClient.ImportSupplierExpenseAdjustBillCompleted -= ExcelServiceClientOnImportSupplierExpenseAdjustBillCompleted;
            this.ExcelServiceClient.ImportSupplierExpenseAdjustBillCompleted += ExcelServiceClientOnImportSupplierExpenseAdjustBillCompleted;
        }

        private void ExcelServiceClientOnImportSupplierExpenseAdjustBillCompleted(object sender, ImportSupplierExpenseAdjustBillCompletedEventArgs e) {
            var dataEditView = this.DataContext as SupplierExpenseAdjustBillForImportDataEditView;
            if(dataEditView == null)
                return;
            dataEditView.SupplierExpenseAdjustBills.Clear();
            if(e.rightData != null && e.rightData.Length > 0) {
                //var maxSequeueNumber = 1;
                foreach(var data in e.rightData) {
                    dataEditView.SupplierExpenseAdjustBills.Add(new SupplierExpenseAdjustBill {
                        //SequeueNumber = maxSequeueNumber,
                        PartsSalesCategoryId = data.PartsSalesCategoryId,
                        ServiceProductLineId = data.ServiceProductLineId,
                        SourceId = data.SourceId,
                        SourceType = data.SourceType,
                        SourceCode = data.SourceCode,
                        SupplierName = data.SupplierName,
                        SupplierId = data.SupplierId,
                        BranchId = data.BranchId,
                        BranchName = data.BranchName,
                        BranchCode = data.BranchCode,
                        IfClaimToResponsible = data.IfClaimToResponsible,
                        DebitOrReplenish = data.DebitOrReplenish,
                        TransactionCategory = data.TransactionCategory,
                        TransactionAmount = data.TransactionAmount,
                        TransactionReason = data.TransactionReason,
                        SupplierAddress = data.SupplierAddress,
                        SupplierCode = data.SupplierCode,
                        SupplierContactPerson = data.SupplierContactPerson,
                        SupplierPhoneNumber = data.SupplierPhoneNumber,
                        Memo = data.Memo
                    });
                    //maxSequeueNumber++;
                }
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        public ICommand UploadFileCommand1 {
            get {
                return this.importCommand1 ?? (this.importCommand1 = new DelegateCommand(this.ShowFileDialog));
            }
        }

        private void ShowFileDialog(object obj) {
            this.uploader.ShowFileDialog();
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override void OnEditSubmitting() {
            foreach(var entity in this.SupplierExpenseAdjustBills) {
                entity.Code = GlobalVar.ASSIGNED_BY_SERVER;
                entity.Status = (int)DcsBaseDataStatus.有效;
                entity.SettlementStatus = (int)DcsClaimBillSettlementStatus.待结算;
                this.DomainContext.SupplierExpenseAdjustBills.Add(entity);
            }
            this.DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    this.DomainContext.RejectChanges();
                    return;
                }
                this.NotifyEditSubmitted();
                this.OnCustomEditSubmitted();
            }, null);
        }

        private void NotifyEditSubmitted() {
            var handler = this.EditSubmitted;
            if(handler != null)
                handler(this, EventArgs.Empty);
        }

        public void OnCustomEditSubmitted() {
            this.Reset();
        }

        protected override void Reset() {
            if(SupplierExpenseAdjustBills != null)
                this.SupplierExpenseAdjustBills.Clear();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }


        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                         new ImportTemplateColumn {
                                                Name = "扣补款类型",
                                                IsRequired = true
                                            }, new ImportTemplateColumn {
                                                Name = "分公司",
                                                IsRequired = true
                                            }, new ImportTemplateColumn {
                                                Name = "品牌",
                                                IsRequired = true
                                            },     new ImportTemplateColumn {
                                                Name = "产品线"
                                            },     new ImportTemplateColumn {
                                                Name = "产品线类型"
                                            },     new ImportTemplateColumn {
                                                Name = "供应商名称",
                                                IsRequired = true
                                            },     new ImportTemplateColumn {
                                                Name = "供应商联系人",
                                                //IsRequired = true
                                            },     new ImportTemplateColumn {
                                                Name = "供应商联系电话",
                                                //IsRequired = true
                                            },     new ImportTemplateColumn {
                                                Name = "供应商联系地址",
                                                //IsRequired = true
                                            }, new ImportTemplateColumn {
                                                Name = "扣补款方向",
                                                IsRequired = true
                                            }, new ImportTemplateColumn {
                                                Name = "源单据类型",
                                                //IsRequired = true
                                            }, new ImportTemplateColumn {
                                                Name = "源单据",
                                               //IsRequired = true
                                            },  new ImportTemplateColumn {
                                                Name = "扣补款金额",
                                                IsRequired = true
                                            },new ImportTemplateColumn {
                                                Name = "向责任单位索赔",
                                                IsRequired = true
                                            },   new ImportTemplateColumn {
                                                Name = "扣补款原因",
                                                IsRequired = true
                                            },  new ImportTemplateColumn {
                                                Name = "备注",
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
