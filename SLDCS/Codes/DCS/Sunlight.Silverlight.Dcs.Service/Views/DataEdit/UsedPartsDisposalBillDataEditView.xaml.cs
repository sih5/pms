﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsDisposalBillDataEditView {
        private DataGridViewBase usedPartsDisposalDetailForEditDataGridView;
        private FrameworkElement usedPartsDisposalBillDataEditPanel;
        private ICommand exportFileCommand;
        private const string EXPORT_DATA_FILE_NAME = "导出旧件残值处理管理清单模板.xlsx";

        private DataGridViewBase UsedPartsDisposalDetailForEditDataGridView {
            get {
                if(this.usedPartsDisposalDetailForEditDataGridView == null) {
                    this.usedPartsDisposalDetailForEditDataGridView = DI.GetDataGridView("UsedPartsDisposalDetailForEdit");
                    this.usedPartsDisposalDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.usedPartsDisposalDetailForEditDataGridView;
            }
        }

        private FrameworkElement UsedPartsDisposalBillDataEditPanel {
            get {
                if(this.usedPartsDisposalBillDataEditPanel == null)
                    this.usedPartsDisposalBillDataEditPanel = DI.GetDataEditPanel("UsedPartsDisposalBill");
                var partsDisposalBillDataEditPanel = this.usedPartsDisposalBillDataEditPanel as IBaseView;
                if(partsDisposalBillDataEditPanel != null)
                    partsDisposalBillDataEditPanel.ExchangeData(null, "IsApproveBill", false);
                return usedPartsDisposalBillDataEditPanel;
            }
        }

        public UsedPartsDisposalBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.UsedPartsDisposalBillDataEditView_DataContextChanged;
        }
        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
        private void InitializeCommand() {
            this.exportFileCommand = new DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = "旧件条码",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "旧件配件编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "旧件配件名称",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "计划量",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "价格",
                                            },
                                            new ImportTemplateColumn {
                                                Name = "索赔单编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "索赔单类型",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "是否祸首件",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "祸首件编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "祸首件供应商编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "祸首件供应商名称",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "责任单位编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "旧件供应商编号",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "旧件供应商名称",
                                                IsRequired = true
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }
        private void CreateUI() {
            this.Root.Children.Add(this.UsedPartsDisposalBillDataEditPanel);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = "导入",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(this.ImportDetail)
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = "导出模板",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = this.ExportTemplateCommand
            });
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(UsedPartsDisposalBill), "UsedPartsDisposalDetails"), null, () => this.UsedPartsDisposalDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        private void ImportDetail() {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            if(usedPartsDisposalBill.UsedPartsWarehouseId == default(int)) {
                UIHelper.ShowNotification("请先选择旧件仓库");
                return;
            }
            this.Uploader.ShowFileDialog();
        }

        private RadUpload uploader;
        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        protected Action<string> UploadFileSuccessedProcessing;

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    //this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
                        if(usedPartsDisposalBill == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ImportUsedPartsDisposalDetailAsync(usedPartsDisposalBill.UsedPartsWarehouseId, e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportUsedPartsDisposalDetailCompleted += this.ExcelServiceClient_ImportUsedPartsDisposalDetailCompleted;
                        this.excelServiceClient.ImportUsedPartsDisposalDetailCompleted += this.ExcelServiceClient_ImportUsedPartsDisposalDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        void ExcelServiceClient_ImportUsedPartsDisposalDetailCompleted(object sender, ImportUsedPartsDisposalDetailCompletedEventArgs e) {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            var detailList = usedPartsDisposalBill.UsedPartsDisposalDetails.ToList();
            foreach(var detail in detailList) {
                usedPartsDisposalBill.UsedPartsDisposalDetails.Remove(detail);
            }
            var serialNumber = 1;
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    usedPartsDisposalBill.UsedPartsDisposalDetails.Add(new UsedPartsDisposalDetail {
                        BranchId = data.BranchId,
                        ClaimBillCode = data.ClaimBillCode,
                        ClaimBillId = data.ClaimBillId,
                        ClaimBillType = data.ClaimBillType,
                        ConfirmedAmount = data.ConfirmedAmount,
                        FaultyPartsCode = data.FaultyPartsCode,
                        FaultyPartsId = data.FaultyPartsId,
                        FaultyPartsName = data.FaultyPartsName,
                        PlannedAmount = 1,
                        OutboundAmount = data.OutboundAmount,
                        FaultyPartsSupplierCode = data.FaultyPartsSupplierCode,
                        FaultyPartsSupplierId = data.FaultyPartsSupplierId,
                        FaultyPartsSupplierName = data.FaultyPartsSupplierName,
                        IfFaultyParts = data.IfFaultyParts,
                        Price = data.Price,
                        ResponsibleUnitCode = data.ResponsibleUnitCode,
                        ResponsibleUnitId = data.ResponsibleUnitId,
                        ResponsibleUnitName = data.ResponsibleUnitName,
                        SerialNumber = serialNumber,
                        UsedPartsBarCode = data.UsedPartsBarCode,
                        UsedPartsCode = data.UsedPartsCode,
                        UsedPartsId = data.UsedPartsId,
                        UsedPartsName = data.UsedPartsName,
                        UsedPartsSerialNumber = data.UsedPartsSerialNumber,
                        UsedPartsSupplierCode = data.UsedPartsSupplierCode,
                        UsedPartsSupplierId = data.UsedPartsSupplierId,
                        UsedPartsSupplierName = data.UsedPartsSupplierName
                    });
                    serialNumber++;
                }
                usedPartsDisposalBill.TotalAmount = usedPartsDisposalBill.UsedPartsDisposalDetails.Sum(entity => entity.Price * entity.PlannedAmount);
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }



        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage("请关闭选择的文件后再进行导入");
                    e.Handled = true;
                }
            }
        }

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                //取当前登录用户的HashCode来标记上传文件的唯一性
                //this.UploadFileSuccessedProcessing.Invoke(BaseApp.Current.CurrentUserData.GetHashCode().ToString(CultureInfo.InvariantCulture) + "_" + upload.CurrentSession.CurrentFile.Name);
                //暂不考虑并发
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }


        private void UsedPartsDisposalBillDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            usedPartsDisposalBill.UsedPartsDisposalDetails.EntityRemoved -= UsedPartsDisposalDetails_EntityRemoved;
            usedPartsDisposalBill.UsedPartsDisposalDetails.EntityRemoved += UsedPartsDisposalDetails_EntityRemoved;
        }

        private void UsedPartsDisposalDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<UsedPartsDisposalDetail> e) {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            usedPartsDisposalBill.TotalAmount = usedPartsDisposalBill.UsedPartsDisposalDetails.Sum(entity => entity.Price * entity.PlannedAmount);
            usedPartsDisposalBill.PropertyChanged -= this.UsedPartsDisposalDetails_PropertyChanged;
            usedPartsDisposalBill.PropertyChanged += this.UsedPartsDisposalDetails_PropertyChanged;
        }

        private void UsedPartsDisposalDetails_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            var usedPartsDisposalDetail = sender as UsedPartsDisposalDetail;
            if(usedPartsDisposalDetail == null)
                return;
            switch(e.PropertyName) {
                case "Price":
                    usedPartsDisposalBill.TotalAmount = usedPartsDisposalBill.UsedPartsDisposalDetails.Sum(entity => entity.Price * entity.PlannedAmount);
                    break;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetUsedPartsDisposalBillsWithDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                foreach(var detail in entity.UsedPartsDisposalDetails)
                    detail.SerialNumber = entity.UsedPartsDisposalDetails.Any() ? entity.UsedPartsDisposalDetails.Max(e => e.SerialNumber) + 1 : 1;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_UsedPartsDisposalBill;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.UsedPartsDisposalDetailForEditDataGridView.CommitEdit())
                return;

            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            usedPartsDisposalBill.ValidationErrors.Clear();
            foreach(var detail in usedPartsDisposalBill.UsedPartsDisposalDetails) {
                detail.ValidationErrors.Clear();
                if(!detail.Price.HasValue)
                    detail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_Common_PriceIsNull, new[] {
                        "Price"
                    }));
                //if(string.IsNullOrWhiteSpace(detail.UsedPartsBatchNumber))
                //    detail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_Common_UsedPartsBatchNumberIsNull, new[] {
                //        "UsedPartsBatchNumber"
                //    }));
                //if(string.IsNullOrWhiteSpace(detail.UsedPartsSerialNumber))
                //    detail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_Common_UsedPartsSerialNumberIsNull, new[] {
                //        "UsedPartsSerialNumber"
                //    }));
                if(!detail.ConfirmedAmount.HasValue)
                    detail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_Common_ConfirmedAmountIsNull, new[] {
                        "ConfirmedAmount"
                    }));
                if(!detail.OutboundAmount.HasValue)
                    detail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_Common_OutboundAmountIsNull, new[] {
                        "OutboundAmount"
                    }));
            }
            if(usedPartsDisposalBill.UsedPartsDisposalMethod == default(int))
                usedPartsDisposalBill.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsDisposalBill_UsedPartsDisposalMethodIsNull, new[] {
                    "UsedPartsDisposalMethod"
                }));

            if(usedPartsDisposalBill.HasValidationErrors || usedPartsDisposalBill.UsedPartsDisposalDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)usedPartsDisposalBill).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(usedPartsDisposalBill.Can生成旧件处理单)
                        usedPartsDisposalBill.生成旧件处理单();
                } else {
                    if(usedPartsDisposalBill.Can修改旧件处理单)
                        usedPartsDisposalBill.修改旧件处理单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
