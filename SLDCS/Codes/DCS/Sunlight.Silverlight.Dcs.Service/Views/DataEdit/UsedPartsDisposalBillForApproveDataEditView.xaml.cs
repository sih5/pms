﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsDisposalBillForApproveDataEditView {
        private DataGridViewBase usedPartsDisposalDetailForEditDataGridView;
        private FrameworkElement usedPartsDisposalBillDataEditPanel;

        private DataGridViewBase UsedPartsDisposalDetailForEditDataGridView {
            get {
                if(this.usedPartsDisposalDetailForEditDataGridView == null) {
                    this.usedPartsDisposalDetailForEditDataGridView = DI.GetDataGridView("UsedPartsDisposalDetailForApprove");
                    this.usedPartsDisposalDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.usedPartsDisposalDetailForEditDataGridView;
            }
        }

        private FrameworkElement UsedPartsDisposalBillDataEditPanel {
            get {
                if(this.usedPartsDisposalBillDataEditPanel == null)
                    this.usedPartsDisposalBillDataEditPanel = DI.GetDataEditPanel("UsedPartsDisposalBill");
                var partsDisposalBillDataEditPanel = this.usedPartsDisposalBillDataEditPanel as IBaseView;
                if(partsDisposalBillDataEditPanel != null)
                    partsDisposalBillDataEditPanel.ExchangeData(null, "IsApproveBill", true);
                return usedPartsDisposalBillDataEditPanel;
            }
        }

        public UsedPartsDisposalBillForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(this.UsedPartsDisposalBillDataEditPanel);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Header = Utils.GetEntityLocalizedName(typeof(UsedPartsDisposalBill), "UsedPartsDisposalDetails"),
                Content = this.UsedPartsDisposalDetailForEditDataGridView
            });
            tabControl.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(tabControl);
        }

        protected override void Reset() {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill != null && this.DomainContext.UsedPartsDisposalBills.Contains(usedPartsDisposalBill))
                this.DomainContext.UsedPartsDisposalBills.Detach(usedPartsDisposalBill);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetUsedPartsDisposalBillsWithDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    foreach(var detail in entity.UsedPartsDisposalDetails) {
                        detail.ConfirmedAmount = detail.PlannedAmount;
                        detail.SerialNumber = entity.UsedPartsDisposalDetails.Any() ? entity.UsedPartsDisposalDetails.Max(e => e.SerialNumber) + 1 : 1;
                    }
                    //根据设计要求，审批时赋默认值
                    entity.Status = (int)DcsUsedPartsDisposalBillStatus.生效;
                    entity.OutboundStatus = (int)DcsUsedPartsOutboundStatus.待出库;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_UsedPartsDisposalBillForApprove;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.UsedPartsDisposalDetailForEditDataGridView.CommitEdit())
                return;

            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            usedPartsDisposalBill.ValidationErrors.Clear();

            foreach(var detail in usedPartsDisposalBill.UsedPartsDisposalDetails)
                detail.ValidationErrors.Clear();
            if(usedPartsDisposalBill.HasValidationErrors || usedPartsDisposalBill.UsedPartsDisposalDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)usedPartsDisposalBill).EndEdit();
            try {
                if(usedPartsDisposalBill.Can审批旧件处理单)
                    usedPartsDisposalBill.审批旧件处理单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
