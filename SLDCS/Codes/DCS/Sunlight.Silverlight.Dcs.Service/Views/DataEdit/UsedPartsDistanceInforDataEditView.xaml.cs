﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsDistanceInforDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private string originCompanyName;
        private ObservableCollection<KeyValuePair> kvOriginUsedPartsWarehouse, kvUsedPartsWarehouses;
        private KeyValueManager keyValueManager;
        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }
        private readonly string[] kvName = new[] {
            "UsedParts_DistanceType"
        };

        public ObservableCollection<KeyValuePair> KvOriginUsedPartsWarehouse {
            get {
                return this.kvOriginUsedPartsWarehouse ?? (this.kvOriginUsedPartsWarehouse = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvUsedPartsWarehouses {
            get {
                return this.kvUsedPartsWarehouses ?? (this.kvUsedPartsWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        public object KvDistanceType {
            get {
                return this.KeyValueManager[this.kvName[0]];
            }
        }

        public string UsedPartsCompany {
            get {
                return BaseApp.Current.CurrentUserData.EnterpriseName;
            }
        }

        public UsedPartsDistanceInforDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvName);
            this.KeyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        public virtual void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetUsedPartsWarehousesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvOriginUsedPartsWarehouse.Clear();
                this.KvUsedPartsWarehouses.Clear();
                foreach(var warehouse in loadOp.Entities) {
                    this.kvOriginUsedPartsWarehouse.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
                    this.kvUsedPartsWarehouses.Add(new KeyValuePair {
                        Key = warehouse.Id,
                        Value = warehouse.Name,
                        UserObject = warehouse
                    });
                }
            }, null);
            this.OriginCompanyName = null;
            var queryWindowCompanyCode = DI.GetQueryWindow("Company");
            queryWindowCompanyCode.SelectionDecided += queryWindowCompanyCode_SelectionDecided;
            this.ptOriginCompanyName.PopupContent = queryWindowCompanyCode;
            this.comboBoxDistanceType.SelectionChanged += comboBoxDistanceType_SelectionChanged;
        }

        private void comboBoxDistanceType_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            var usedPartsDistanceInfor = this.DataContext as UsedPartsDistanceInfor;
            if(comboBox == null)
                return;
            if(comboBox.Text == "服务站-旧件库") {
                this.comboBoxOriginUsedPartsWarehouse.IsEnabled = false;
                this.ptOriginCompanyName.IsEnabled = true;
                usedPartsDistanceInfor.OriginUsedPartsWarehouseId = null;
            }
            if(comboBox.Text == "旧件库-旧件库") {
                this.ptOriginCompanyName.IsEnabled = false;
                this.comboBoxOriginUsedPartsWarehouse.IsEnabled = true;
                this.OriginCompanyName = null;
                usedPartsDistanceInfor.OriginCompanyId = null;
            }
        }

        private void queryWindowCompanyCode_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var company = queryWindow.SelectedEntities.Cast<Company>().FirstOrDefault();
            if(company == null)
                return;
            var usedPartsDistanceInfor = this.DataContext as UsedPartsDistanceInfor;
            if(usedPartsDistanceInfor == null)
                return;
            usedPartsDistanceInfor.OriginCompanyId = company.Id;
            this.OriginCompanyName = company.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public string OriginCompanyName {
            get {
                return this.originCompanyName;
            }
            set {
                this.originCompanyName = value;
                this.OnPropertyChanged("OriginCompanyName");
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var usedPartsDistanceInfor = this.DataContext as UsedPartsDistanceInfor;
            if(usedPartsDistanceInfor == null)
                return;
            usedPartsDistanceInfor.ValidationErrors.Clear();
            if(usedPartsDistanceInfor.UsedPartsWarehouseId == default(int)) {
                usedPartsDistanceInfor.ValidationErrors.Add(new ValidationResult("旧件仓库名称字段是必须的", new[] {
                    "UsedPartsWarehouseId"
                }));
                return;
            }
            if(this.comboBoxDistanceType.Text == "旧件库-旧件库" && usedPartsDistanceInfor.OriginUsedPartsWarehouseId == null) {
                usedPartsDistanceInfor.ValidationErrors.Add(new ValidationResult("源旧件库不能为空", new[] {
                    "OriginUsedPartsWarehouseId"
                }));
                return;
            }
            if(usedPartsDistanceInfor.ShippingDistance == default(int)) {
                usedPartsDistanceInfor.ValidationErrors.Add(new ValidationResult("旧件运距必须大于0", new[] {
                    "ShippingDistance"
                }));
                return;
            }
            if(usedPartsDistanceInfor.UsedPartsDistanceType == default(int)) {
                usedPartsDistanceInfor.ValidationErrors.Add(new ValidationResult("旧件运距类型不能为空", new[] {
                    "UsedPartsDistanceType"
                }));
                return;
            }
            if(usedPartsDistanceInfor.HasValidationErrors)
                return;
            if(this.comboBoxDistanceType.Text == "服务站-旧件库" && usedPartsDistanceInfor.OriginCompanyId == null) {
                UIHelper.ShowNotification("源服务站不能为空");
                return;
            }
            if(usedPartsDistanceInfor.OriginUsedPartsWarehouseId == usedPartsDistanceInfor.UsedPartsWarehouseId) {
                UIHelper.ShowNotification("旧件仓库名称与源旧件库不能选取相同的值");
                return;
            }

            if(usedPartsDistanceInfor.UsedPartsCompanyId == default(int)) {
                UIHelper.ShowNotification("旧件仓储企业不能为空");
                return;
            }
            ((IEditableObject)usedPartsDistanceInfor).EndEdit();
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetUsedPartsDistanceInforWithDetailsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    if(entity.Company1 != null)
                        if(!string.IsNullOrEmpty(entity.Company1.Name))
                            this.OriginCompanyName = entity.Company1.Name;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override string BusinessName {
            get {
                return ServiceUIStrings.DataManagementView_Title_UsedPartsDistanceInfor;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}