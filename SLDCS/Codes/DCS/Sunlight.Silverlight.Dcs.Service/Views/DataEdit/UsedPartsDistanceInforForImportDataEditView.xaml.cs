﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsDistanceInforForImportDataEditView {
        private DataGridViewBase usedPartsDistanceInforForImport;
        private const string EXPORT_DATA_FILE_NAME = "导出旧件仓储运距模板.xlsx";
        private ICommand exportFileCommand;
        public UsedPartsDistanceInforForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return "导入旧件仓储运距";
            }
        }

        protected DataGridViewBase ImportDataGridView {
            get {
                return this.usedPartsDistanceInforForImport ?? (this.usedPartsDistanceInforForImport = DI.GetDataGridView("UsedPartsDistanceInforForImport"));
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = "旧件仓储运距导入",
                Content = this.ImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            ShellViewModel.Current.IsBusy = true;
            this.ExcelServiceClient.ImportUsedPartsDistanceInforAsync(fileName);
            this.ExcelServiceClient.ImportUsedPartsDistanceInforCompleted -= ExcelServiceClient_ImportUsedPartsDistanceInforCompleted;
            this.ExcelServiceClient.ImportUsedPartsDistanceInforCompleted += ExcelServiceClient_ImportUsedPartsDistanceInforCompleted;
        }

        private void ExcelServiceClient_ImportUsedPartsDistanceInforCompleted(object sender, ImportUsedPartsDistanceInforCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Notification_CompletedImport, 5);
            } else {
                if(!string.IsNullOrEmpty(e.errorMessage))
                    UIHelper.ShowAlertMessage(e.errorMessage);
                if(!string.IsNullOrEmpty(e.errorDataFileName))
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Notification_CompletedImportWithError, 10);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => {
                var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = "旧件仓储企业名称",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "旧件仓库名称",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "旧件运距类型",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "源服务站编号",
                                            },
                                            new ImportTemplateColumn {
                                                Name = "源服务站名称",
                                            },
                                            new ImportTemplateColumn {
                                                Name = "源旧件库编号",
                                            },
                                            new ImportTemplateColumn {
                                                Name = "源旧件库名称",
                                            },
                                            new ImportTemplateColumn {
                                                Name = "旧件运距",
                                                IsRequired = true
                                            },
                                            new ImportTemplateColumn {
                                                Name = "备注",
                                            }
                                        };
                this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                }, null);
            });
        }
    }
}
