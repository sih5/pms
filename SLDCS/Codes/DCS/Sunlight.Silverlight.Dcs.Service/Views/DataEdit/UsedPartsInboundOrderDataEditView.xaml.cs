﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsInboundOrderDataEditView {
        private DataGridViewBase usedPartsInboundDetailForEditDataGridView;

        public UsedPartsInboundOrderDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CretaeUI);
        }

        private DataGridViewBase UsedPartsInboundDetailForEditDataGridView {
            get {
                if(this.usedPartsInboundDetailForEditDataGridView == null) {
                    this.usedPartsInboundDetailForEditDataGridView = DI.GetDataGridView("UsedPartsInboundDetailForEdit");
                    this.usedPartsInboundDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.usedPartsInboundDetailForEditDataGridView;
            }
        }

        private void CretaeUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("UsedPartsInboundOrder"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "UsedPartsInboundDetails"), null, this.UsedPartsInboundDetailForEditDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_WarehouseParts_UsedPartsInboundDetail;
            }
        }

        private void LoadEntitytoEdit(IList<object> parameter) {
            var sourceCode = parameter[0] == null ? null : (string)parameter[0];
            var approvalTime = parameter[1] == null ? null : (DateTime?)parameter[1];
            var inboundType = parameter[2] == null ? null : (int?)parameter[2];
            var inboundStatus = parameter[3] == null ? null : (int?)parameter[3];
            var warehouseCode = parameter[4] == null ? null : (string)parameter[4];
            var warehouseName = parameter[5] == null ? null : (string)parameter[5];
            this.DataContext = null;
            // 如果之前是通过查询按钮退出编辑界面，取消之前的修改
            if(this.DomainContext.HasChanges)
                this.DomainContext.RejectChanges();
            this.DomainContext.Load(this.DomainContext.查询虚拟旧件入库计划Query(sourceCode, approvalTime, approvalTime, inboundType, inboundStatus, warehouseCode, warehouseName, BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var virtualUsedPartsInboundPlan = loadOp.Entities.SingleOrDefault();
                if(virtualUsedPartsInboundPlan == null)
                    return;
                var usedPartsInboundOrder = new UsedPartsInboundOrder();
                this.DomainContext.UsedPartsInboundOrders.Add(usedPartsInboundOrder);
                usedPartsInboundOrder.UsedPartsWarehouseName = virtualUsedPartsInboundPlan.UsedPartsWarehouseName;
                usedPartsInboundOrder.InboundType = inboundType.HasValue ? inboundType.Value : default(int);
                usedPartsInboundOrder.UsedPartsWarehouseId = virtualUsedPartsInboundPlan.UsedPartsWarehouseId;
                usedPartsInboundOrder.UsedPartsWarehouseCode = virtualUsedPartsInboundPlan.UsedPartsWarehouseCode;
                usedPartsInboundOrder.SourceId = virtualUsedPartsInboundPlan.SourceId;
                usedPartsInboundOrder.SourceCode = virtualUsedPartsInboundPlan.SourceCode;
                usedPartsInboundOrder.RelatedCompanyId = virtualUsedPartsInboundPlan.RelatedCompanyId;
                usedPartsInboundOrder.RelatedCompanyCode = virtualUsedPartsInboundPlan.RelatedCompanyCode;
                usedPartsInboundOrder.RelatedCompanyName = virtualUsedPartsInboundPlan.RelatedCompanyName;
                usedPartsInboundOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                usedPartsInboundOrder.SettlementStatus = (int)DcsClaimBillSettlementStatus.待结算;
                usedPartsInboundOrder.IfBillable = false;
                usedPartsInboundOrder.IfAlreadySettled = false;

                this.DomainContext.Load(this.DomainContext.查询虚拟旧件入库清单Query(usedPartsInboundOrder.InboundType, usedPartsInboundOrder.SourceId, true), LoadBehavior.RefreshCurrent, loadOpDetail => {
                    if(loadOpDetail.HasError) {
                        if(!loadOpDetail.IsErrorHandled)
                            loadOpDetail.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOpDetail);
                        return;
                    }
                    if(loadOpDetail.Entities == null)
                        return;

                    if(loadOpDetail.Entities.GroupBy(e => e.UsedPartsBarCode).Any(repeat => repeat.Count() > 1)) {
                        UIHelper.ShowAlertMessage(ServiceUIStrings.Validation_Common_UsedPartsBarCodeCanNotRepeat);
                        return;
                    }
                    var serialNumber = 1;
                    foreach(var virtualUsedPartsInboundPlanDetail in loadOpDetail.Entities) {
                        var usedPartsInboundDetail = new UsedPartsInboundDetail();
                        usedPartsInboundDetail.SerialNumber = serialNumber++;
                        usedPartsInboundDetail.BranchId = virtualUsedPartsInboundPlanDetail.BranchId;
                        usedPartsInboundDetail.CostPrice = virtualUsedPartsInboundPlanDetail.CostPrice;
                        usedPartsInboundDetail.UsedPartsId = virtualUsedPartsInboundPlanDetail.UsedPartsId;
                        usedPartsInboundDetail.UsedPartsCode = virtualUsedPartsInboundPlanDetail.UsedPartsCode;
                        usedPartsInboundDetail.UsedPartsName = virtualUsedPartsInboundPlanDetail.UsedPartsName;
                        usedPartsInboundDetail.UsedPartsBarCode = virtualUsedPartsInboundPlanDetail.UsedPartsBarCode;
                        usedPartsInboundDetail.UsedPartsWarehouseAreaId = virtualUsedPartsInboundPlanDetail.UsedPartsWarehouseAreaId;
                        usedPartsInboundDetail.UsedPartsWarehouseAreaCode = virtualUsedPartsInboundPlanDetail.UsedPartsWarehouseAreaCode;
                        usedPartsInboundDetail.UsedPartsBatchNumber = virtualUsedPartsInboundPlanDetail.UsedPartsBatchNumber;
                        usedPartsInboundDetail.UsedPartsSerialNumber = virtualUsedPartsInboundPlanDetail.UsedPartsSerialNumber;
                        usedPartsInboundDetail.Remark = virtualUsedPartsInboundPlanDetail.Remark;
                        usedPartsInboundDetail.ClaimBillId = virtualUsedPartsInboundPlanDetail.ClaimBillId ?? 0;
                        usedPartsInboundDetail.ClaimBillCode = virtualUsedPartsInboundPlanDetail.ClaimBillCode ?? "空";
                        usedPartsInboundDetail.ClaimBillType = virtualUsedPartsInboundPlanDetail.ClaimBillType ?? 0;
                        usedPartsInboundDetail.UsedPartsSupplierId = virtualUsedPartsInboundPlanDetail.UsedPartsSupplierId ?? 0;
                        usedPartsInboundDetail.UsedPartsSupplierCode = virtualUsedPartsInboundPlanDetail.UsedPartsSupplierCode ?? "空";
                        usedPartsInboundDetail.UsedPartsSupplierName = virtualUsedPartsInboundPlanDetail.UsedPartsSupplierName ?? "空";
                        usedPartsInboundDetail.IfFaultyParts = virtualUsedPartsInboundPlanDetail.IfFaultyParts;
                        usedPartsInboundDetail.FaultyPartsCode = virtualUsedPartsInboundPlanDetail.FaultyPartsCode;
                        usedPartsInboundDetail.FaultyPartsName = virtualUsedPartsInboundPlanDetail.FaultyPartsName;
                        usedPartsInboundDetail.FaultyPartsSupplierId = virtualUsedPartsInboundPlanDetail.FaultyPartsSupplierId;
                        usedPartsInboundDetail.FaultyPartsSupplierCode = virtualUsedPartsInboundPlanDetail.FaultyPartsSupplierCode;
                        usedPartsInboundDetail.FaultyPartsSupplierName = virtualUsedPartsInboundPlanDetail.FaultyPartsSupplierName;
                        usedPartsInboundDetail.ResponsibleUnitId = virtualUsedPartsInboundPlanDetail.ResponsibleUnitId;
                        usedPartsInboundDetail.ResponsibleUnitCode = virtualUsedPartsInboundPlanDetail.ResponsibleUnitCode;
                        usedPartsInboundDetail.PlannedAmount = virtualUsedPartsInboundPlanDetail.PlannedAmount;
                        usedPartsInboundDetail.ReceptionStatus = virtualUsedPartsInboundPlanDetail.ReceptionStatus;
                        usedPartsInboundDetail.ReceptionRemark = virtualUsedPartsInboundPlanDetail.ReceptionRemark;
                        usedPartsInboundDetail.ConfirmedAmount = virtualUsedPartsInboundPlanDetail.ConfirmedAmount;
                        usedPartsInboundDetail.UsedPartsEncourageAmount = virtualUsedPartsInboundPlanDetail.UsedPartsEncourageAmount;
                        usedPartsInboundDetail.SettlementPrice = virtualUsedPartsInboundPlanDetail.SettlementPrice.HasValue ? virtualUsedPartsInboundPlanDetail.SettlementPrice.Value : default(decimal);
                        usedPartsInboundDetail.Quantity = virtualUsedPartsInboundPlanDetail.ConfirmedAmount.HasValue ? virtualUsedPartsInboundPlanDetail.ConfirmedAmount.Value : default(int);
                        usedPartsInboundOrder.UsedPartsInboundDetails.Add(usedPartsInboundDetail);
                    }
                    this.SetObjectToEdit(usedPartsInboundOrder);
                }, null);
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.usedPartsInboundDetailForEditDataGridView.CommitEdit())
                return;
            var usedPartsInboundOrder = this.DataContext as UsedPartsInboundOrder;
            if(usedPartsInboundOrder == null)
                return;
            usedPartsInboundOrder.ValidationErrors.Clear();
            if(usedPartsInboundOrder.UsedPartsInboundDetails.Any(r => r.Quantity > 1 || r.Quantity < 0)) {
                foreach(var detail in usedPartsInboundOrder.UsedPartsInboundDetails) {
                    detail.ValidationErrors.Clear();
                    if(detail.Quantity > 1 || detail.Quantity < 0)
                        detail.ValidationErrors.Add(new ValidationResult("入库量只能为0或1", new[] {
                            "Quantity"
                        }));
                }
            }

            if(usedPartsInboundOrder.InboundType == (int)DcsUsedPartsInboundOrderInboundType.服务站返件入库) {
                var isAllZero = usedPartsInboundOrder.UsedPartsInboundDetails.All(detail => detail.Quantity == 0);
                if(isAllZero) {
                    foreach(var detail in usedPartsInboundOrder.UsedPartsInboundDetails) {
                        detail.ValidationErrors.Clear();
                        if(detail.Quantity == 0)
                            detail.ValidationErrors.Add(new ValidationResult("入库量不可以全为0", new[] {
                            "Quantity"
                        }));
                    }
                }
            } else {
                foreach(var detail in usedPartsInboundOrder.UsedPartsInboundDetails) {
                    detail.ValidationErrors.Clear();
                    if(detail.Quantity <= 0)
                        detail.ValidationErrors.Add(new ValidationResult("入库量不可以为0", new[] {
                            "Quantity"
                        }));
                }
            }
            if(!usedPartsInboundOrder.UsedPartsInboundDetails.Any()) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_UsedPartsInboundDetailIsNull);
                return;
            }
            if(usedPartsInboundOrder.HasValidationErrors || usedPartsInboundOrder.UsedPartsInboundDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)usedPartsInboundOrder).EndEdit();
            try {
                if(this.EditState == DataEditState.Edit) {
                    if(usedPartsInboundOrder.Can旧件入库)
                        usedPartsInboundOrder.旧件入库(usedPartsInboundOrder.InboundType);
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            var usedPartsInboundOrder = this.DataContext as UsedPartsInboundOrder;
            if(usedPartsInboundOrder == null)
                return;
            DcsUtils.Confirm("是否打印旧件入库单?", () => this.DomainContext.Load(this.DomainContext.GetUsedPartsInboundOrdersQuery().Where(ex => ex.Id == usedPartsInboundOrder.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                BasePrintWindow printWindow = new UsedPartsInboundOrderPrintWindow {
                    Header = "旧件入库单打印",
                    UsedPartsInboundOrderId = entity.Id
                };
                printWindow.ShowDialog();
            }, null));
        }

        public override void SetObjectToEditById(object parameter) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntitytoEdit((object[])parameter));
            else
                this.LoadEntitytoEdit((object[])parameter);
        }
    }
}