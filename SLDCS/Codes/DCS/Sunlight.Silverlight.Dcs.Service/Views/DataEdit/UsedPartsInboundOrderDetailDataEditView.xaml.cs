﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsInboundOrderDetailDataEditView {
        private DataGridViewBase virtualUsedPartsInboundPlanDetailDataGridView;
        private ObservableCollection<VirtualUsedPartsInboundPlanDetail> virtualUsedPartsInboundPlanDetails = new ObservableCollection<VirtualUsedPartsInboundPlanDetail>();

        public UsedPartsInboundOrderDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase VirtualUsedPartsInboundPlanDetailDataGridView {
            get {
                if(this.virtualUsedPartsInboundPlanDetailDataGridView == null) {
                    this.virtualUsedPartsInboundPlanDetailDataGridView = DI.GetDataGridView("VirtualUsedPartsInboundPlanDetail");
                    this.virtualUsedPartsInboundPlanDetailDataGridView.DomainContext = this.DomainContext;
                    this.virtualUsedPartsInboundPlanDetailDataGridView.DataContext = this;
                }
                return this.virtualUsedPartsInboundPlanDetailDataGridView;
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Header = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "UsedPartsInboundDetails"),
                Content = this.VirtualUsedPartsInboundPlanDetailDataGridView
            });
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(tabControl);
        }

        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_ViewDetail_UsedPartsInboundDetail;
            }
        }

        private void LoadEntityToEdit(IList<int?> parameter) {
            var inboundType = parameter[0];
            var sourceId = parameter[1];
            if(inboundType == null || sourceId == null)
                return;
            this.DomainContext.Load(this.DomainContext.查询虚拟旧件入库清单Query((int)inboundType, (int)sourceId, false), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.VirtualUsedPartsInboundPlanDetails.Clear();
                var entities = loadOp.Entities;
                var serialNumber = 1;
                foreach(var entity in entities) {
                    entity.SerialNumber = serialNumber++;
                    this.VirtualUsedPartsInboundPlanDetails.Add(entity);
                }

            }, null);
        }

        public override void SetObjectToEditById(object parameter) {
            if(DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int?[])parameter));
            else
                this.LoadEntityToEdit((int?[])parameter);
        }

        public ObservableCollection<VirtualUsedPartsInboundPlanDetail> VirtualUsedPartsInboundPlanDetails {
            get {
                return this.virtualUsedPartsInboundPlanDetails ?? (this.virtualUsedPartsInboundPlanDetails = new ObservableCollection<VirtualUsedPartsInboundPlanDetail>());
            }
        }
    }
}
