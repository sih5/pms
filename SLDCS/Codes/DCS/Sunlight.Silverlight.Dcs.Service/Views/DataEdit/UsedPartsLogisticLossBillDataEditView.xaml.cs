﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsLogisticLossBillDataEditView {
        private DataGridViewBase usedPartsLogisticLossDetailForEditDataGridView;

        public UsedPartsLogisticLossBillDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase UsedPartsLogisticLossDetailForEditDataGridView {
            get {
                if(this.usedPartsLogisticLossDetailForEditDataGridView == null) {
                    this.usedPartsLogisticLossDetailForEditDataGridView = DI.GetDataGridView("UsedPartsLogisticLossDetailForEdit");
                    this.usedPartsLogisticLossDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.usedPartsLogisticLossDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDetailPanel("UsedPartsLogisticLossBill"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Content = this.UsedPartsLogisticLossDetailForEditDataGridView,
                Header = Utils.GetEntityLocalizedName(typeof(UsedPartsLogisticLossBill), "UsedPartsLogisticLossDetails")
            });
            tabControl.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(tabControl);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetUsedPartsLogisticLossBillWithDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.First();
                if(entity == null)
                    return;
                entity.TotalQuantity = entity.UsedPartsLogisticLossDetails.Sum(entityItem => entityItem.Quantity);
                entity.TotalAmount = entity.UsedPartsLogisticLossDetails.Sum(detail => (detail.Quantity * detail.Price) + detail.PartsManagementCost);
                entity.LogisticLossTotalAmount = entity.TotalAmount - entity.UsedPartsLogisticLossDetails.Sum(r => r.PartsManagementCost);
                var serialNumber = 0;
                foreach(var detail in entity.UsedPartsLogisticLossDetails) {
                    detail.SerialNumber = ++serialNumber;
                }
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_UsedPartsLogisticLossBill;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.UsedPartsLogisticLossDetailForEditDataGridView.CommitEdit())
                return;
            var usedPartsLogisticLossBill = this.DataContext as UsedPartsLogisticLossBill;
            if(usedPartsLogisticLossBill == null)
                return;
            usedPartsLogisticLossBill.ValidationErrors.Clear();
            if(!usedPartsLogisticLossBill.UsedPartsLogisticLossDetails.Any()) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_UsedPartsLogisticLossBill_UsedPartsLogisticLossDetailsIsEmpty);
                return;
            }
            foreach(var detail in usedPartsLogisticLossBill.UsedPartsLogisticLossDetails) {
                detail.ValidationErrors.Clear();
                ((IEditableObject)detail).EndEdit();
            }
            if(usedPartsLogisticLossBill.HasValidationErrors || usedPartsLogisticLossBill.UsedPartsLogisticLossDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)usedPartsLogisticLossBill).EndEdit();
            try {
                if(usedPartsLogisticLossBill.Can修改物流损失单)
                    usedPartsLogisticLossBill.修改物流损失单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitted() {
            var usedPartsLogisticLossBill = this.DataContext as UsedPartsLogisticLossBill;
            if(usedPartsLogisticLossBill == null)
                return;
            usedPartsLogisticLossBill.LogisticLossTotalAmount = 0;

        }

        protected override void OnEditCancelled() {
            var usedPartsLogisticLossBill = this.DataContext as UsedPartsLogisticLossBill;
            if(usedPartsLogisticLossBill == null)
                return;
            usedPartsLogisticLossBill.LogisticLossTotalAmount = 0;
        }
    }
}
