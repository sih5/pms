﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsOutboundOrderDataEditView {
        private DataGridViewBase usedPartsOutboundDetailForEditDataGridView;

        public UsedPartsOutboundOrderDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase UsedPartsOutboundDetailForEditDataGridView {
            get {
                if(this.usedPartsOutboundDetailForEditDataGridView == null) {
                    this.usedPartsOutboundDetailForEditDataGridView = DI.GetDataGridView("UsedPartsOutboundDetailForEdit");
                    this.usedPartsOutboundDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.usedPartsOutboundDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("UsedPartsOutboundOrder"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(UsedPartsOutboundOrder), "UsedPartsOutboundDetails"), null, () => this.UsedPartsOutboundDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            this.Root.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(IList<object> filterParam) {
            var beginTime = filterParam[1] == null ? null : (DateTime?)filterParam[1];
            var endTime = filterParam[2] == null ? null : (DateTime?)filterParam[2];
            this.DataContext = null;
            // 如果之前是通过查询按钮退出编辑界面，取消之前的修改
            if(this.DomainContext.HasChanges)
                this.DomainContext.RejectChanges();
            this.DomainContext.Load(this.DomainContext.查询虚拟旧件出库计划Query(filterParam[0] as string, beginTime, endTime, (int)filterParam[3], (int)filterParam[4], filterParam[5] as string, filterParam[6] as string, BaseApp.Current.CurrentUserData.UserId, null, null), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var virtualUsedPartsOutboundPlan = loadOp.Entities.SingleOrDefault();
                if(virtualUsedPartsOutboundPlan == null)
                    return;
                var usedPartsOutboundOrder = new UsedPartsOutboundOrder();
                this.DomainContext.UsedPartsOutboundOrders.Add(usedPartsOutboundOrder);
                usedPartsOutboundOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                usedPartsOutboundOrder.SourceId = virtualUsedPartsOutboundPlan.SourceId;
                usedPartsOutboundOrder.SourceCode = virtualUsedPartsOutboundPlan.SourceCode;
                usedPartsOutboundOrder.UsedPartsWarehouseCode = virtualUsedPartsOutboundPlan.UsedPartsWarehouseCode;
                usedPartsOutboundOrder.UsedPartsWarehouseName = virtualUsedPartsOutboundPlan.UsedPartsWarehouseName;
                usedPartsOutboundOrder.UsedPartsWarehouseId = virtualUsedPartsOutboundPlan.UsedPartsWarehouseId;
                usedPartsOutboundOrder.RelatedCompanyId = virtualUsedPartsOutboundPlan.RelatedCompanyId;
                usedPartsOutboundOrder.RelatedCompanyCode = virtualUsedPartsOutboundPlan.RelatedCompanyCode;
                usedPartsOutboundOrder.RelatedCompanyName = virtualUsedPartsOutboundPlan.RelatedCompanyName;
                usedPartsOutboundOrder.IfBillable = false;
                usedPartsOutboundOrder.IfAlreadySettled = false;
                usedPartsOutboundOrder.OutboundType = virtualUsedPartsOutboundPlan.OutboundType.HasValue ? virtualUsedPartsOutboundPlan.OutboundType.Value : default(int);
                usedPartsOutboundOrder.Remark = virtualUsedPartsOutboundPlan.Remark;
                this.DomainContext.Load(this.DomainContext.查询虚拟旧件出库清单Query(usedPartsOutboundOrder.OutboundType, usedPartsOutboundOrder.SourceId, true, BaseApp.Current.CurrentUserData.UserId, usedPartsOutboundOrder.UsedPartsWarehouseId), LoadBehavior.RefreshCurrent, loadOpDetail => {
                    if(loadOpDetail.HasError) {
                        if(!loadOpDetail.IsErrorHandled)
                            loadOpDetail.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOpDetail);
                        return;
                    }
                    if(loadOpDetail.Entities.GroupBy(e => e.UsedPartsBarCode).Any(repeat => repeat.Count() > 1)) {
                        UIHelper.ShowAlertMessage(ServiceUIStrings.Validation_Common_UsedPartsBarCodeCanNotRepeat);
                        return;
                    }
                    var serialNumber = 1;
                    foreach(var virtualUsedPartsOutboundPlanDetail in loadOpDetail.Entities) {
                        var usedPartsOutboundDetail = new UsedPartsOutboundDetail();
                        usedPartsOutboundDetail.SerialNumber = serialNumber++;
                        usedPartsOutboundOrder.UsedPartsOutboundDetails.Add(usedPartsOutboundDetail);
                        usedPartsOutboundDetail.UsedPartsBarCode = virtualUsedPartsOutboundPlanDetail.UsedPartsBarCode;
                        usedPartsOutboundDetail.UsedPartsId = virtualUsedPartsOutboundPlanDetail.UsedPartsId;
                        usedPartsOutboundDetail.UsedPartsCode = virtualUsedPartsOutboundPlanDetail.UsedPartsCode;
                        usedPartsOutboundDetail.UsedPartsName = virtualUsedPartsOutboundPlanDetail.UsedPartsName;
                        usedPartsOutboundDetail.UsedPartsWarehouseAreaId = virtualUsedPartsOutboundPlanDetail.UsedPartsWarehouseAreaId;
                        usedPartsOutboundDetail.UsedPartsWarehouseAreaCode = virtualUsedPartsOutboundPlanDetail.UsedPartsWarehouseAreaCode;
                        usedPartsOutboundDetail.UsedPartsBatchNumber = virtualUsedPartsOutboundPlanDetail.UsedPartsBatchNumber;
                        usedPartsOutboundDetail.UsedPartsSerialNumber = virtualUsedPartsOutboundPlanDetail.UsedPartsSerialNumber;
                        usedPartsOutboundDetail.Remark = virtualUsedPartsOutboundPlanDetail.Remark;
                        usedPartsOutboundDetail.CostPrice = virtualUsedPartsOutboundPlanDetail.CostPrice;
                        usedPartsOutboundDetail.BranchId = virtualUsedPartsOutboundPlanDetail.BranchId;
                        usedPartsOutboundDetail.PlannedAmount = virtualUsedPartsOutboundPlanDetail.PlannedAmount;
                        usedPartsOutboundDetail.ConfirmedAmount = virtualUsedPartsOutboundPlanDetail.ConfirmedAmount;
                        usedPartsOutboundDetail.SettlementPrice = virtualUsedPartsOutboundPlanDetail.SettlementPrice.HasValue ? virtualUsedPartsOutboundPlanDetail.SettlementPrice.Value : default(int);
                        usedPartsOutboundDetail.Quantity = virtualUsedPartsOutboundPlanDetail.ConfirmedAmount.HasValue ? virtualUsedPartsOutboundPlanDetail.ConfirmedAmount.Value : default(int);
                        usedPartsOutboundDetail.ClaimBillId = virtualUsedPartsOutboundPlanDetail.ClaimBillId;
                        usedPartsOutboundDetail.ClaimBillCode = virtualUsedPartsOutboundPlanDetail.ClaimBillCode;
                        usedPartsOutboundDetail.ClaimBillType = virtualUsedPartsOutboundPlanDetail.ClaimBillType;
                        usedPartsOutboundDetail.UsedPartsSupplierId = virtualUsedPartsOutboundPlanDetail.UsedPartsSupplierId;
                        usedPartsOutboundDetail.UsedPartsSupplierCode = virtualUsedPartsOutboundPlanDetail.UsedPartsSupplierCode;
                        usedPartsOutboundDetail.UsedPartsSupplierName = virtualUsedPartsOutboundPlanDetail.UsedPartsSupplierName;
                        usedPartsOutboundDetail.IfFaultyParts = virtualUsedPartsOutboundPlanDetail.IfFaultyParts;
                        usedPartsOutboundDetail.FaultyPartsCode = virtualUsedPartsOutboundPlanDetail.FaultyPartsCode;
                        usedPartsOutboundDetail.FaultyPartsId = virtualUsedPartsOutboundPlanDetail.FaultyPartsId;
                        usedPartsOutboundDetail.FaultyPartsName = virtualUsedPartsOutboundPlanDetail.FaultyPartsName;
                        usedPartsOutboundDetail.FaultyPartsSupplierId = virtualUsedPartsOutboundPlanDetail.FaultyPartsSupplierId;
                        usedPartsOutboundDetail.FaultyPartsSupplierName = virtualUsedPartsOutboundPlanDetail.FaultyPartsSupplierName;
                        usedPartsOutboundDetail.FaultyPartsSupplierCode = virtualUsedPartsOutboundPlanDetail.FaultyPartsSupplierCode;
                        usedPartsOutboundDetail.ResponsibleUnitId = virtualUsedPartsOutboundPlanDetail.ResponsibleUnitId;
                        usedPartsOutboundDetail.ResponsibleUnitCode = virtualUsedPartsOutboundPlanDetail.ResponsibleUnitCode;
                        usedPartsOutboundDetail.ResponsibleUnitName = virtualUsedPartsOutboundPlanDetail.ResponsibleUnitName;
                    }
                    this.SetObjectToEdit(usedPartsOutboundOrder);
                }, null);
            }, null);
        }

        protected override string Title {
            get {
                return Service.Resources.ServiceUIStrings.DataEditView_Title_UsedPartsOutboundOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.usedPartsOutboundDetailForEditDataGridView.CommitEdit())
                return;
            var usedPartsOutboundOrder = this.DataContext as UsedPartsOutboundOrder;
            if(usedPartsOutboundOrder == null)
                return;
            if(!usedPartsOutboundOrder.UsedPartsOutboundDetails.Any()) {
                UIHelper.ShowNotification("清单不能为空，请检查目的仓库是否已入库");
                return;
            }
            usedPartsOutboundOrder.ValidationErrors.Clear();
            //foreach(var detail in usedPartsOutboundOrder.UsedPartsOutboundDetails) {
            //    detail.ValidationErrors.Clear();
            //    if(string.IsNullOrWhiteSpace(detail.UsedPartsBatchNumber))
            //        detail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_Common_UsedPartsBatchNumberIsNull, new[] {
            //            "UsedPartsBatchNumber"
            //        }));
            //    if(string.IsNullOrWhiteSpace(detail.UsedPartsSerialNumber))
            //        detail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_Common_UsedPartsSerialNumberIsNull, new[] {
            //            "UsedPartsSerialNumber"
            //        }));
            //}
            if(usedPartsOutboundOrder.HasValidationErrors || usedPartsOutboundOrder.UsedPartsOutboundDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)usedPartsOutboundOrder).EndEdit();
            try {
                if(usedPartsOutboundOrder.Can旧件出库)
                    usedPartsOutboundOrder.旧件出库(usedPartsOutboundOrder.OutboundType);
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            var usedPartsOutboundOrder = this.DataContext as UsedPartsOutboundOrder;
            if(usedPartsOutboundOrder == null)
                return;
            DcsUtils.Confirm("是否打印旧件出库单?", () => this.DomainContext.Load(this.DomainContext.GetUsedPartsOutboundOrdersQuery().Where(ex => ex.Id == usedPartsOutboundOrder.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity == null)
                    return;
                BasePrintWindow printWindow = new UsedPartsOutboundOrderPrintWindow {
                    Header = "旧件出库单打印",
                    UsedPartsOutboundOrder = entity
                };
                printWindow.ShowDialog();
            }, null));
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((object[])id));
            else
                this.LoadEntityToEdit((object[])id);
        }
    }
}
