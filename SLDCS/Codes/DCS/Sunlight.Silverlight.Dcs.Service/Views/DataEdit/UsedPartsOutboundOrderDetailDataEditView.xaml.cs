﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsOutboundDetailDataEditView {
        private DataGridViewBase usedPartsOutboundDetailForVirtualDataGridView;
        private ObservableCollection<VirtualUsedPartsOutboundPlanDetail> virtualUsedPartsOutboundPlanDetails;

        public UsedPartsOutboundDetailDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Header = Utils.GetEntityLocalizedName(typeof(UsedPartsOutboundOrder), "UsedPartsOutboundDetails"),
                Content = this.UsedPartsOutboundDetailForVirtualDataGridView
            });
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            this.Root.Children.Add(tabControl);
        }

        private DataGridViewBase UsedPartsOutboundDetailForVirtualDataGridView {
            get {
                if(this.usedPartsOutboundDetailForVirtualDataGridView == null) {
                    this.usedPartsOutboundDetailForVirtualDataGridView = DI.GetDataGridView("VirtualUsedPartsOutboundPlanDetail");
                    this.usedPartsOutboundDetailForVirtualDataGridView.DomainContext = this.DomainContext;
                    this.usedPartsOutboundDetailForVirtualDataGridView.DataContext = this;
                }
                return this.usedPartsOutboundDetailForVirtualDataGridView;
            }
        }

        private void LoadEntityToEdit(IList<int?> filterParam) {
            if(!filterParam[0].HasValue || !filterParam[1].HasValue || !filterParam[2].HasValue)
                return;
            this.DomainContext.Load(this.DomainContext.查询虚拟旧件出库清单Query(filterParam[0].Value, filterParam[1].Value, false, BaseApp.Current.CurrentUserData.UserId, filterParam[2].Value), LoadBehavior.RefreshCurrent, loadOpDetail => {
                if(loadOpDetail.HasError) {
                    if(!loadOpDetail.IsErrorHandled)
                        loadOpDetail.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOpDetail);
                    return;
                }
                this.VirtualUsedPartsOutboundPlanDetails.Clear();
                var usedPartsOutboundPlanDetails = loadOpDetail.Entities;
                var serialNumber = 1;
                foreach(var detail in usedPartsOutboundPlanDetails) {
                    detail.SerialNumber = serialNumber++;
                    VirtualUsedPartsOutboundPlanDetails.Add(detail);
                }
            }, null);
        }

        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_ViewDetail_UsedPartsOutboundDetail;
            }
        }

        protected override void OnEditCancelled() {
            this.VirtualUsedPartsOutboundPlanDetails.Clear();
            base.OnEditCancelled();
        }

        public ObservableCollection<VirtualUsedPartsOutboundPlanDetail> VirtualUsedPartsOutboundPlanDetails {
            get {
                return this.virtualUsedPartsOutboundPlanDetails ?? (this.virtualUsedPartsOutboundPlanDetails = new ObservableCollection<VirtualUsedPartsOutboundPlanDetail>());
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int?[])id));
            else
                this.LoadEntityToEdit((int?[])id);
        }
    }
}
