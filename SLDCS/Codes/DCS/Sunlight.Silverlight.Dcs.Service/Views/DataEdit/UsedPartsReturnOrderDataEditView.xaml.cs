﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators.Models;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsReturnOrderDataEditView {
        private string strFileName;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private RadUpload uploader;
        private const string EXPORT_DATA_FILE_NAME = "旧件清退清单模板.xlsx";

        /// <summary>
        /// 文件上传成功后，调用该方法进行上传后的处理。不支持异步调用
        /// <param>上传文件名 <name>fileName</name> </param>
        /// </summary>
        protected Action<string> UploadFileSuccessedProcessing;
        private DataGridViewBase usedPartsReturnDetailForEditDataGridView;

        public UsedPartsReturnOrderDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private RadUpload Uploader {
            get {
                if(this.uploader == null) {
                    this.uploader = new RadUpload();
                    this.uploader.Filter = "Excel File|*.xlsx";
                    this.uploader.FilterIndex = 0;
                    this.uploader.IsAppendFilesEnabled = false;
                    this.uploader.IsAutomaticUpload = true;
                    this.uploader.MaxFileCount = 1;
                    this.uploader.MaxFileSize = 30000000;
                    this.uploader.MaxUploadSize = 10000000;
                    this.uploader.OverwriteExistingFiles = true;
                    this.uploader.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
                    this.uploader.FileUploadStarting -= this.Uploader_FileUploadStarting;
                    this.uploader.FileUploadStarting += this.Uploader_FileUploadStarting;
                    this.uploader.UploadFinished -= this.UpLoad_UploadFinished;
                    this.uploader.UploadFinished += this.UpLoad_UploadFinished;
                    this.uploader.FilesSelected -= this.Uploader_FilesSelected;
                    this.uploader.FilesSelected += this.Uploader_FilesSelected;
                    this.uploader.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_IMPORTFILE_DIR);
                    this.uploader.FileUploaded += (sender, e) => {
                        var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
                        if(usedPartsReturnOrder == null)
                            return;
                        if(usedPartsReturnOrder.OutboundWarehouseId == default(int)) {
                            UIHelper.ShowNotification(ServiceUIStrings.Validation_UsedPartsReturnDetail_WarehouseNameIsNull);
                            ShellViewModel.Current.IsBusy = false;
                            return;
                        }
                        ShellViewModel.Current.IsBusy = true;
                        if(e != null)
                            this.excelServiceClient.ImportUsedPartsReturnDetailAsync(usedPartsReturnOrder.OutboundWarehouseId, e.HandlerData.CustomData["Path"].ToString());
                        this.excelServiceClient.ImportUsedPartsReturnDetailCompleted -= ExcelServiceClient_ImportUsedPartsReturnDetailCompleted;
                        this.excelServiceClient.ImportUsedPartsReturnDetailCompleted += ExcelServiceClient_ImportUsedPartsReturnDetailCompleted;
                    };
                }
                return this.uploader;
            }
        }

        private DataGridViewBase UsedPartsReturnDetailForEditDataGridView {
            get {
                if(this.usedPartsReturnDetailForEditDataGridView == null) {
                    this.usedPartsReturnDetailForEditDataGridView = DI.GetDataGridView("UsedPartsReturnDetailForEdit");
                    this.usedPartsReturnDetailForEditDataGridView.DomainContext = this.DomainContext;
                    return this.usedPartsReturnDetailForEditDataGridView;
                }
                return this.usedPartsReturnDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("UsedPartsReturnOrder"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(UsedPartsReturnOrder), "UsedPartsReturnDetails"), null, this.UsedPartsReturnDetailForEditDataGridView);
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = "导入",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Import.png", UriKind.Relative),
                Command = new DelegateCommand(() => this.Uploader.ShowFileDialog())
            });
            detailDataEditView.RegisterButton(new ButtonItem {
                Title = "导出模板",
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/Export.png", UriKind.Relative),
                Command = new DelegateCommand(() => {
                    var columnItemsValues = new List<ImportTemplateColumn> {
                                            new ImportTemplateColumn {
                                                Name = "旧件条码",
                                                IsRequired = true
                                            }
                                        };
                    this.DomainContext.GetImportTemplateFileWithFormat(EXPORT_DATA_FILE_NAME, columnItemsValues, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                        }
                        if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                            this.ExportFile(loadOp.Value);
                    }, null);
                })
            });
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        private void Uploader_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            ShellViewModel.Current.IsBusy = true;
        }

        private void UpLoad_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload == null || upload.CurrentSession == null || upload.CurrentSession.CurrentFile == null)
                return;
            upload.CancelUpload();
            if(this.UploadFileSuccessedProcessing == null)
                return;
            Action importAction = () => {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Notification_StartingImport);
                this.UploadFileSuccessedProcessing.Invoke(this.strFileName);
            };
            importAction.Invoke();
        }

        private void Uploader_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any()) {
                this.strFileName = e.SelectedFiles[0].Name;
                try {
                    //尝试读取，出错说明文件已打开
                    using(e.SelectedFiles[0].File.OpenRead()) {
                    }
                } catch(Exception) {
                    UIHelper.ShowAlertMessage("请关闭选择的文件后再进行导入");
                    e.Handled = true;
                }
            }
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            this.ErrorFileName = errorFileName;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(ErrorFileName));
        }

        private void ExcelServiceClient_ImportUsedPartsReturnDetailCompleted(object sender, ImportUsedPartsReturnDetailCompletedEventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            foreach(var detail in usedPartsReturnOrder.UsedPartsReturnDetails) {
                usedPartsReturnOrder.UsedPartsReturnDetails.Remove(detail);
            }
            var serialNumber = 1;
            if(e.rightData != null && e.rightData.Length > 0) {
                foreach(var data in e.rightData) {
                    usedPartsReturnOrder.UsedPartsReturnDetails.Add(new UsedPartsReturnDetail {
                        SerialNumber = serialNumber,
                        UsedPartsBarCode = data.UsedPartsBarCode,
                        UsedPartsCode = data.UsedPartsCode,
                        UsedPartsName = data.UsedPartsName,
                        UnitPrice = data.UnitPrice,
                        ClaimBillCode = data.ClaimBillCode,
                        ClaimBillType = data.ClaimBillType,
                        IfFaultyParts = data.IfFaultyParts,
                        FaultyPartsCode = data.FaultyPartsCode,
                        FaultyPartsSupplierCode = data.FaultyPartsSupplierCode,
                        FaultyPartsSupplierName = data.FaultyPartsSupplierName,
                        ResponsibleUnitCode = data.ResponsibleUnitCode,
                        UsedPartsSupplierCode = data.UsedPartsSupplierCode,
                        UsedPartsSupplierName = data.UsedPartsSupplierName,
                        PlannedAmount = 1,
                        ConfirmedAmount = 0,
                        OutboundAmount = 0
                    });
                    serialNumber++;
                }
                usedPartsReturnOrder.TotalAmount = usedPartsReturnOrder.UsedPartsReturnDetails.Sum(entity => Convert.ToDecimal(entity.PlannedAmount) * entity.UnitPrice);
                usedPartsReturnOrder.TotalQuantity = usedPartsReturnOrder.UsedPartsReturnDetails.Sum(entity => entity.PlannedAmount);
            }
            ShellViewModel.Current.IsBusy = false;
            if(!string.IsNullOrEmpty(e.errorMessage))
                UIHelper.ShowAlertMessage(e.errorMessage);
            if(!string.IsNullOrEmpty(e.errorDataFileName))
                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.errorDataFileName));
        }

        protected string ErrorFileName {
            get;
            set;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetUsedPartsReturnOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    foreach(var item in entity.UsedPartsReturnDetails) {
                        item.SerialNumber = entity.UsedPartsReturnDetails.ToList().IndexOf(item) + 1;
                    }
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_UsedPartsReturnOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.UsedPartsReturnDetailForEditDataGridView.CommitEdit())
                return;

            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            usedPartsReturnOrder.ValidationErrors.Clear();
            foreach(var detail in usedPartsReturnOrder.UsedPartsReturnDetails)
                detail.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(usedPartsReturnOrder.OutboundWarehouseName)) {
                UIHelper.ShowAlertMessage(ServiceUIStrings.DataEditView_Validation_UsedPartsReturnOrder_OutboundWarehouseNameIsNull);
                return;
            }
            if(string.IsNullOrWhiteSpace(usedPartsReturnOrder.ReturnOfficeName)) {
                switch(usedPartsReturnOrder.ReturnType) {
                    case (int)DcsUsedPartsReturnOrderReturnType.清退责任单位:
                        UIHelper.ShowAlertMessage(ServiceUIStrings.DataEditView_Validation_UsedPartsReturnOrder_ResponsibleUnitNameIsNull);
                        return;
                    case (int)DcsUsedPartsReturnOrderReturnType.清退配件供应商:
                        UIHelper.ShowAlertMessage(ServiceUIStrings.DataEditView_Validation_UsedPartsReturnOrder_SupplierNameIsNull);
                        return;
                    case (int)DcsUsedPartsReturnOrderReturnType.内部部门:
                        UIHelper.ShowAlertMessage(ServiceUIStrings.DataEditView_Validation_UsedPartsReturnOrder_DepartmentNameIsNull);
                        return;
                }
            }

            var findBarCodeByGroup = from detail in usedPartsReturnOrder.UsedPartsReturnDetails
                                     group detail by detail.UsedPartsBarCode
                                         into details
                                         where details.Count() > 1
                                         select new {
                                             MaxCount = details.Count()
                                         };

            if(findBarCodeByGroup.Any()) {
                UIHelper.ShowAlertMessage(ServiceUIStrings.Validation_Common_UsedPartsBarCodeCanNotRepeat);
                return;
            }

            if(!usedPartsReturnOrder.UsedPartsReturnDetails.Any()) {
                UIHelper.ShowAlertMessage(ServiceUIStrings.DataEditView_Validation_UsedPartsReturnOrder_UsedPartsReturnDetailsIsEmpty);
                return;
            }

            if(usedPartsReturnOrder.HasValidationErrors || usedPartsReturnOrder.UsedPartsReturnDetails.Any(detail => detail.HasValidationErrors))
                return;

            if(usedPartsReturnOrder.EntityState == EntityState.Modified) {
                if(usedPartsReturnOrder.Can修改旧件清退单)
                    usedPartsReturnOrder.修改旧件清退单();
            }
            ((IEditableObject)usedPartsReturnOrder).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

    }
}
