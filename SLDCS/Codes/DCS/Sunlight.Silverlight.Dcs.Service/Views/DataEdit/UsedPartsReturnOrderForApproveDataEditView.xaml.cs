﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsReturnOrderForApproveDataEditView {
        private DataGridViewBase usedPartsReturnDetailForEditDataGridView;

        public UsedPartsReturnOrderForApproveDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase UsedPartsReturnDetailForEditDataGridView {
            get {
                if(this.usedPartsReturnDetailForEditDataGridView == null) {
                    this.usedPartsReturnDetailForEditDataGridView = DI.GetDataGridView("UsedPartsReturnDetailForApprove");
                    this.usedPartsReturnDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.usedPartsReturnDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("UsedPartsReturnOrderForApprove"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            this.UsedPartsReturnDetailForEditDataGridView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(this.UsedPartsReturnDetailForEditDataGridView);
        }

        protected override void Reset() {
            var usedpartsreturnorder = this.DataContext as UsedPartsReturnOrder;
            if(usedpartsreturnorder != null && this.DomainContext.UsedPartsReturnOrders.Contains(usedpartsreturnorder))
                this.DomainContext.UsedPartsReturnOrders.Detach(usedpartsreturnorder);
        }

        private void LoadEntityToEdit(int id) {
            this.DataContext = null;
            this.DomainContext.Load(this.DomainContext.GetUsedPartsReturnOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    foreach(var item in entity.UsedPartsReturnDetails) {
                        item.SerialNumber = entity.UsedPartsReturnDetails.ToList().IndexOf(item) + 1;
                    }
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_ApproveUsedPartsReturnOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.UsedPartsReturnDetailForEditDataGridView.CommitEdit())
                return;

            var usedpartsreturnorder = this.DataContext as UsedPartsReturnOrder;
            if(usedpartsreturnorder == null)
                return;
            usedpartsreturnorder.ValidationErrors.Clear();
            foreach(var detail in usedpartsreturnorder.UsedPartsReturnDetails)
                detail.ValidationErrors.Clear();
            if(usedpartsreturnorder.HasValidationErrors || usedpartsreturnorder.UsedPartsReturnDetails.Any(detail => detail.HasValidationErrors))
                return;
            ((IEditableObject)usedpartsreturnorder).EndEdit();
            try {
                if(usedpartsreturnorder.Can审批旧件清退单)
                    usedpartsreturnorder.审批旧件清退单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
