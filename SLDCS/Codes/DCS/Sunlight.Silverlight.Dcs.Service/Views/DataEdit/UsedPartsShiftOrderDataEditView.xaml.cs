﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsShiftOrderDataEditView {
        private DataGridViewBase usedPartsShiftDetailDataGridView;

        public UsedPartsShiftOrderDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase UsedPartsShiftDetailDataGridView {
            get {
                if(this.usedPartsShiftDetailDataGridView == null) {
                    this.usedPartsShiftDetailDataGridView = DI.GetDataGridView("UsedPartsShiftDetailForEdit");
                    this.usedPartsShiftDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.usedPartsShiftDetailDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("UsedPartsShiftOrder"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(UsedPartsShiftOrder), "UsedPartsShiftDetails"), null, () => this.UsedPartsShiftDetailDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_UsedPartsShiftOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.usedPartsShiftDetailDataGridView.CommitEdit())
                return;

            var usedPartsShiftOrder = this.DataContext as UsedPartsShiftOrder;
            if(usedPartsShiftOrder == null)
                return;
            usedPartsShiftOrder.ValidationErrors.Clear();
            if(!usedPartsShiftOrder.UsedPartsShiftDetails.Any()) {
                UIHelper.ShowAlertMessage(ServiceUIStrings.DataEditView_Validation_UsedPartsShiftDetail_DetailIsNull);
                return;
            }
            foreach(var usedPartsShiftDetail in usedPartsShiftOrder.UsedPartsShiftDetails)
                usedPartsShiftDetail.ValidationErrors.Clear();
            if(usedPartsShiftOrder.UsedPartsShiftDetails.GroupBy(e => e.UsedPartsBarCode).Any(group => group.Count() > 1)) {
                UIHelper.ShowAlertMessage(ServiceUIStrings.Validation_Common_UsedPartsBarCodeCanNotRepeat);
                return;
            }
            //if(usedPartsShiftOrder.UsedPartsShiftDetails.Any(e => string.IsNullOrWhiteSpace(e.UsedPartsSerialNumber)))
            //    foreach(var usedPartsShiftDetail in usedPartsShiftOrder.UsedPartsShiftDetails.Where(e => string.IsNullOrWhiteSpace(e.UsedPartsSerialNumber)))
            //        usedPartsShiftDetail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsShiftDetail_UsedPartsSerialNumberIsNull, new[] {
            //        "UsedPartsSerialNumber"
            //    }));
            //if(usedPartsShiftOrder.UsedPartsShiftDetails.Any(e => string.IsNullOrWhiteSpace(e.UsedPartsBatchNumber)))
            //    foreach(var usedPartsShiftDetail in usedPartsShiftOrder.UsedPartsShiftDetails.Where(e => string.IsNullOrWhiteSpace(e.UsedPartsBatchNumber)))
            //        usedPartsShiftDetail.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsShiftDetail_UsedPartsBatchNumberIsNull, new[] {
            //        "UsedPartsBatchNumber"
            //    }));

            if(usedPartsShiftOrder.HasValidationErrors || usedPartsShiftOrder.UsedPartsShiftDetails.Any((e => e.HasValidationErrors)))
                return;

            ((IEditableObject)usedPartsShiftOrder).EndEdit();
            try {
                if(usedPartsShiftOrder.Can生成旧件移库单)
                    usedPartsShiftOrder.生成旧件移库单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
    }
}
