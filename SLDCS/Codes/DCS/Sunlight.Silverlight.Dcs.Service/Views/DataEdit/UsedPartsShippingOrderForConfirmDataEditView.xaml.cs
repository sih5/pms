﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsShippingOrderForConfirmDataEditView {
        private DataGridViewBase usedPartsShippingDetailForConfirmDataGridView;

        public UsedPartsShippingOrderForConfirmDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase UsedPartsShippingDetailForConfirmDataGridView {
            get {
                if(this.usedPartsShippingDetailForConfirmDataGridView == null) {
                    this.usedPartsShippingDetailForConfirmDataGridView = DI.GetDataGridView("UsedPartsShippingDetailForConfirm");
                    this.usedPartsShippingDetailForConfirmDataGridView.DomainContext = this.DomainContext;
                    return this.usedPartsShippingDetailForConfirmDataGridView;
                }
                return this.usedPartsShippingDetailForConfirmDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("UsedPartsShippingOrderForConfirm"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "UsedPartsShippingDetails"),
                Content = this.UsedPartsShippingDetailForConfirmDataGridView
            });
            tabControl.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(tabControl);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetUsedPartsShippingOrderWithDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    // 实际到货日期 为 当前日期
                    entity.ActualArrivalDate = DateTime.Now.Date;
                    var serialNumber = 0;
                    foreach(var detail in entity.UsedPartsShippingDetails)
                        detail.SerialNumber = ++serialNumber;
                    // 查询 指定仓库下的 默认库位
                    this.DomainContext.Load(this.DomainContext.GetUsedPartsWarehouseAreasQuery().Where(e => e.UsedPartsWarehouseId == entity.DestinationWarehouseId && e.IfDefaultStoragePosition.HasValue && e.IfDefaultStoragePosition.Value), LoadBehavior.RefreshCurrent, loadAreaOp => {
                        if(loadAreaOp.HasError) {
                            if(!loadAreaOp.IsErrorHandled)
                                loadAreaOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var entityArea = loadAreaOp.Entities.SingleOrDefault();
                        if(entityArea != null)
                            foreach(var detail in entity.UsedPartsShippingDetails) {
                                detail.UsedPartsWarehouseAreaId = entityArea.Id;
                                detail.UsedPartsWarehouseAreaCode = entityArea.Code;
                                detail.ReceptionStatus = (int)DcsUsedPartsShippingDetailReceptionStatus.已接收;
                                detail.UsedPartsEncourageAmount = 0;
                                detail.PropertyChanged += detail_PropertyChanged;
                            }
                        this.SetObjectToEdit(entity);
                    }, null);
                }
            }, null);

        }

        private void detail_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            var usedPartsShippingDetail = sender as UsedPartsShippingDetail;
            if(usedPartsShippingDetail == null)
                return;
            switch(e.PropertyName) {
                case "UsedPartsEncourageAmount":
                case "ReceptionStatus":
                    if(e.PropertyName == "ReceptionStatus") {
                        if(usedPartsShippingDetail.ReceptionStatus == (int)DcsUsedPartsShippingDetailReceptionStatus.不合格) {
                            usedPartsShippingDetail.UsedPartsEncourageAmount = usedPartsShippingDetail.Quantity * usedPartsShippingDetail.UnitPrice * -1;
                        } else {
                            usedPartsShippingDetail.UsedPartsEncourageAmount = 0;
                        }
                    }
                    usedPartsShippingOrder.TotalAmount = usedPartsShippingOrder.UsedPartsShippingDetails.Sum(r => r.Quantity * r.UnitPrice + r.PartsManagementCost + (r.UsedPartsEncourageAmount ?? 0));
                    break;
            }
        }

        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_ConfirmUsedPartsShippingOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.UsedPartsShippingDetailForConfirmDataGridView.CommitEdit())
                return;

            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            usedPartsShippingOrder.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(usedPartsShippingOrder.Code)) {
                usedPartsShippingOrder.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsShippingOrder_BranchIdIsNull, new[] {
                    "Code"
                }));
            }
            if(usedPartsShippingOrder.HasValidationErrors || usedPartsShippingOrder.UsedPartsShippingDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)usedPartsShippingOrder).EndEdit();
            try {
                if(EditState == DataEditState.Edit)
                    if(usedPartsShippingOrder.Can确认旧件发运单)
                        usedPartsShippingOrder.确认旧件发运单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        protected override void Reset() {
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder != null && this.DomainContext.UsedPartsShippingOrders.Contains(usedPartsShippingOrder))
                this.DomainContext.UsedPartsShippingOrders.Detach(usedPartsShippingOrder);
        }

    }
}
