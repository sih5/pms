﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;


namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsShippingOrderForEditDataEditView {
        private DataGridViewBase usedPartsShippingDetailForEditDataGridView;
        public UsedPartsShippingOrderForEditDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.UsedPartsShippingOrderDataEditView_DataContextChanged;
        }

        private void UsedPartsShippingOrderDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            usedPartsShippingOrder.UsedPartsShippingDetails.EntityAdded -= this.UsedPartsShippingDetails_EntityAdded;
            usedPartsShippingOrder.UsedPartsShippingDetails.EntityAdded += this.UsedPartsShippingDetails_EntityAdded;
            usedPartsShippingOrder.UsedPartsShippingDetails.EntityRemoved -= this.UsedPartsShippingDetails_EntityRemoved;
            usedPartsShippingOrder.UsedPartsShippingDetails.EntityRemoved += this.UsedPartsShippingDetails_EntityRemoved;
            usedPartsShippingOrder.PropertyChanged -= this.UsedPartsShippingOrder_PropertyChanged;
            usedPartsShippingOrder.PropertyChanged += this.UsedPartsShippingOrder_PropertyChanged;
        }
        private void UsedPartsShippingDetails_EntityRemoved(object sender, EntityCollectionChangedEventArgs<UsedPartsShippingDetail> e) {
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder != null)
                usedPartsShippingOrder.TotalAmount = usedPartsShippingOrder.UsedPartsShippingDetails.Sum(entity => entity.Quantity * entity.UnitPrice + entity.PartsManagementCost);
        }

        private void UsedPartsShippingDetails_EntityAdded(object sender, EntityCollectionChangedEventArgs<UsedPartsShippingDetail> e) {
            if(e.Entity != null)
                e.Entity.PropertyChanged += this.Entity_PropertyChanged;
        }

        private void Entity_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            switch(e.PropertyName) {
                case "Quantity":
                case "UnitPrice":
                case "PartsManagementCost":
                    usedPartsShippingOrder.TotalAmount = usedPartsShippingOrder.UsedPartsShippingDetails.Sum(entity => entity.Quantity * entity.UnitPrice + entity.PartsManagementCost);
                    break;
            }
        }

        private DataGridViewBase UsedPartsShippingDetailForEditDataGridView {
            get {
                if(this.usedPartsShippingDetailForEditDataGridView == null) {
                    this.usedPartsShippingDetailForEditDataGridView = DI.GetDataGridView("UsedPartsShippingDetailForEdit");
                    this.usedPartsShippingDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.usedPartsShippingDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            var editPanel = DI.GetDataEditPanel("UsedPartsShippingOrderForEdit");
            var scrollViewer = new ScrollViewer();
            scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
            scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            scrollViewer.Content = editPanel;
            this.Root.Children.Add(scrollViewer);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "UsedPartsShippingDetails"), null, this.UsedPartsShippingDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetUsedPartsShippingOrdersWithDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }
        private void UsedPartsShippingOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            switch(e.PropertyName) {
                case "BranchId":
                    if(usedPartsShippingOrder.UsedPartsShippingDetails.Any()) {
                        DcsUtils.Confirm(ServiceUIStrings.DataEditView_Confirm_Change, () => {
                            foreach(var detail in usedPartsShippingOrder.UsedPartsShippingDetails.ToArray())
                                usedPartsShippingOrder.UsedPartsShippingDetails.Remove(detail);
                        });
                    }
                    break;
                case "PartsSalesCategoryId":
                    if(usedPartsShippingOrder.UsedPartsShippingDetails.Any()) {
                        DcsUtils.Confirm(ServiceUIStrings.DataEditView_Confirm_PartsSalesCategoryChange, () => {
                            foreach(var detail in usedPartsShippingOrder.UsedPartsShippingDetails.ToArray())
                                usedPartsShippingOrder.UsedPartsShippingDetails.Remove(detail);
                        });
                    }
                    break;
                default:
                    break;
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_UsedPartsShippingOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.UsedPartsShippingDetailForEditDataGridView.CommitEdit())
                return;

            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            usedPartsShippingOrder.ValidationErrors.Clear();
            if(usedPartsShippingOrder.PartsSalesCategoryId == default(int))
                usedPartsShippingOrder.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesCategoryIdIsNull, new[] {
                    "PartsSalesCategoryId"
                }));

            if(usedPartsShippingOrder.BranchId == default(int))
                usedPartsShippingOrder.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsShippingOrder_BranchIdIsNull, new[] {
                    "BranchId"
                }));
            if(usedPartsShippingOrder.DestinationWarehouseId == default(int))
                usedPartsShippingOrder.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsShippingOrder_DestinationWarehouseIdIsNull, new[] {
                    "DestinationWarehouseId"
                }));
            if(string.IsNullOrEmpty(usedPartsShippingOrder.Operator) || string.IsNullOrEmpty(usedPartsShippingOrder.OperatorPhone)) {
                UIHelper.ShowNotification("请填写经办人信息");
                return;
            }
            if(!usedPartsShippingOrder.UsedPartsShippingDetails.Any()) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_UsedPartsShippingOrder_UsedPartsShippingDetailsIsEmpty);
                return;
            }

            foreach(var detail in usedPartsShippingOrder.UsedPartsShippingDetails) {
                detail.ValidationErrors.Clear();
                ((IEditableObject)detail).EndEdit();
            }

            if(usedPartsShippingOrder.HasValidationErrors || usedPartsShippingOrder.UsedPartsShippingDetails.Any(entity => entity.HasValidationErrors))
                return;
            ((IEditableObject)usedPartsShippingOrder).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(usedPartsShippingOrder.Can生成旧件发运单)
                        usedPartsShippingOrder.生成旧件发运单();
                } else if(usedPartsShippingOrder.Can修改旧件发运单)
                    usedPartsShippingOrder.修改旧件发运单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

    }
}
