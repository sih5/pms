﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsTransferOrderDataEditView {
        private DataGridViewBase usedPartsTransferDetailForEditDataGridView;

        public UsedPartsTransferOrderDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }


        private DataGridViewBase UsedPartsTransferDetailForEditDataGridView {
            get {
                if(this.usedPartsTransferDetailForEditDataGridView == null) {
                    this.usedPartsTransferDetailForEditDataGridView = DI.GetDataGridView("UsedPartsTransferDetailForEdit");
                    this.usedPartsTransferDetailForEditDataGridView.DomainContext = this.DomainContext;
                    return this.usedPartsTransferDetailForEditDataGridView;
                }
                return this.usedPartsTransferDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("UsedPartsTransferOrder"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(UsedPartsTransferOrder), "UsedPartsTransferDetails"), null, () => {
                var view = this.UsedPartsTransferDetailForEditDataGridView;
                view.DomainContext = this.DomainContext;
                return view;
            });
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetUsedPartsTransferOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    foreach(var item in entity.UsedPartsTransferDetails) {
                        item.SerialNumber = entity.UsedPartsTransferDetails.ToList().IndexOf(item) + 1;
                    }
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_UsedPartsTransferOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.usedPartsTransferDetailForEditDataGridView.CommitEdit())
                return;

            var usedPartsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            if(usedPartsTransferOrder == null)
                return;

            usedPartsTransferOrder.ValidationErrors.Clear();
            if(usedPartsTransferOrder.OriginWarehouseId == usedPartsTransferOrder.DestinationWarehouseId)
                usedPartsTransferOrder.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsTransferOrder_DestinationWarehouseNameSameToOriginWarehouseName, new[] {
                    "DestinationWarehouseName"
                }));
            if(usedPartsTransferOrder.HasValidationErrors)
                return;
            if(!usedPartsTransferOrder.UsedPartsTransferDetails.Any()) {
                UIHelper.ShowNotification("旧件调拨单清单不能为空");
                return;
            }

            ((IEditableObject)usedPartsTransferOrder).EndEdit();
            try {
                if(this.EditState == DataEditState.New) {
                    if(usedPartsTransferOrder.Can生成旧件调拨单)
                        usedPartsTransferOrder.生成旧件调拨单();
                } else if(usedPartsTransferOrder.Can修改旧件调拨单)
                    usedPartsTransferOrder.修改旧件调拨单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
