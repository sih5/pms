﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsTransferOrderForApproveDataEditView {
        private DataGridViewBase usedPartsTransferDetailForEditDataGridView;

        public UsedPartsTransferOrderForApproveDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase UsedPartsTransferDetailForEditDataGridView {
            get {
                if(this.usedPartsTransferDetailForEditDataGridView == null) {
                    this.usedPartsTransferDetailForEditDataGridView = DI.GetDataGridView("UsedPartsTransferDetailForApprove");
                    this.usedPartsTransferDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.usedPartsTransferDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("UsedPartsTransferOrderForApprove"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            this.UsedPartsTransferDetailForEditDataGridView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(this.UsedPartsTransferDetailForEditDataGridView);
        }

        protected override void Reset() {
            var usedPartsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            if(usedPartsTransferOrder != null && this.DomainContext.UsedPartsTransferOrders.Contains(usedPartsTransferOrder))
                this.DomainContext.UsedPartsTransferOrders.Detach(usedPartsTransferOrder);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetUsedPartsTransferOrdersWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                var serialNumber = 0;
                foreach(var detail in entity.UsedPartsTransferDetails) {
                    detail.ConfirmedAmount = detail.PlannedAmount;
                    detail.SerialNumber = ++serialNumber;
                }
                entity.Status = (int)DcsUsedPartsTransferOrderStatus.生效;
                entity.OutboundStatus = (int)DcsUsedPartsOutboundStatus.待出库;
                entity.InboundStatus = (int)DcsUsedPartsInboundStatus.待入库;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string Title {
            get {
                return ServiceUIStrings.DataEditView_Title_ApproveUsedPartsTransferOrder;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.UsedPartsTransferDetailForEditDataGridView.CommitEdit())
                return;

            var usedPartsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            if(usedPartsTransferOrder == null)
                return;
            usedPartsTransferOrder.ValidationErrors.Clear();
            foreach(var detail in usedPartsTransferOrder.UsedPartsTransferDetails)
                detail.ValidationErrors.Clear();
            if(usedPartsTransferOrder.HasValidationErrors || usedPartsTransferOrder.UsedPartsTransferDetails.Any(detail => detail.HasValidationErrors))
                return;
            ((IEditableObject)usedPartsTransferOrder).EndEdit();
            try {
                if(usedPartsTransferOrder.Can审批旧件调拨单)
                    usedPartsTransferOrder.审批旧件调拨单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
