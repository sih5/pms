﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsWarehouseAreaDataEditView : INotifyPropertyChanged {
        private DataGridViewBase usedPartsWarehouseManagerForEditDataGridView;
        private UsedPartsWarehouseAreaDataTreeView usedPartsWarehouseAreaDataTreeView;
        private ObservableCollection<KeyValuePair> areaKinds;
        private bool canEditAreaCategory;
        private bool canEditAreaKind;
        private KeyValueManager keyValueManager;
        public event PropertyChangedEventHandler PropertyChanged;

        private readonly string[] kvNames = {
            "Area_Kind", "Area_Category"
        };

        public UsedPartsWarehouseAreaDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
            this.Loaded += this.UsedPartsWarehouseAreaDataEditView_Loaded;
            this.ckIfDefaultStoragePosition.IsEnabled = false;
        }


        private void UsedPartsWarehouseAreaDataEditView_Loaded(object sender, RoutedEventArgs e) {
            if(this.EditState == DataEditState.New)
                this.UsedPartsWarehouseAreaDataTreeView.RefreshDataTree();
        }

        private DataGridViewBase UsedPartsWarehouseManagerForEditDataGridView {
            get {
                if(this.usedPartsWarehouseManagerForEditDataGridView == null) {
                    this.usedPartsWarehouseManagerForEditDataGridView = DI.GetDataGridView("UsedPartsWarehouseManagerForEdit");
                    this.usedPartsWarehouseManagerForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.usedPartsWarehouseManagerForEditDataGridView;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private UsedPartsWarehouseAreaDataTreeView UsedPartsWarehouseAreaDataTreeView {
            get {
                if(this.usedPartsWarehouseAreaDataTreeView == null) {
                    this.usedPartsWarehouseAreaDataTreeView = new UsedPartsWarehouseAreaDataTreeView();
                    this.usedPartsWarehouseAreaDataTreeView.DomainContext = this.DomainContext;
                    this.usedPartsWarehouseAreaDataTreeView.EntityQuery = this.usedPartsWarehouseAreaDataTreeView.DomainContext.GetUsedPartsWarehouseAreasWithUsedPartsWarehouseQuery(BaseApp.Current.CurrentUserData.EnterpriseId);
                    this.usedPartsWarehouseAreaDataTreeView.OnTreeViewDataLoaded += usedPartsWarehouseAreaDataTreeView_OnTreeViewDataLoaded;
                }
                return this.usedPartsWarehouseAreaDataTreeView;
            }
        }

        void usedPartsWarehouseAreaDataTreeView_OnTreeViewDataLoaded() {
            var partsWarehouseArea = this.DataContext as UsedPartsWarehouseArea;
            if(partsWarehouseArea == null)
                return;
            //库区用途 只有顶层库区新增可编辑
            this.CanEditAreaCategory = this.EditState == DataEditState.New && !partsWarehouseArea.TopLevelUsedPartsWhseAreaId.HasValue;
            if(EditState == DataEditState.New)
                if(partsWarehouseArea.ParentId.HasValue)
                    this.UsedPartsWarehouseAreaDataTreeView.SelectedItemId = partsWarehouseArea.ParentId.Value;
                else
                    return;
            else
                this.UsedPartsWarehouseAreaDataTreeView.SelectedItemId = partsWarehouseArea.Id;
            var selectedItem = this.UsedPartsWarehouseAreaDataTreeView.SelectedItem as RadTreeViewItem;
            if(selectedItem == null || selectedItem.Tag == null || !(selectedItem.Tag is UsedPartsWarehouseArea))
                return;
            partsWarehouseArea.UsedPartsWarehouse = ((UsedPartsWarehouseArea)selectedItem.Tag).UsedPartsWarehouse;
            if(!partsWarehouseArea.ParentId.HasValue)
                return;
            var treeItem = this.UsedPartsWarehouseAreaDataTreeView.GetObjectById(partsWarehouseArea.ParentId.Value) as RadTreeViewItem;
            if(treeItem == null)
                return;
            var parentUsedPartsWarehouseArea = treeItem.Tag as UsedPartsWarehouseArea;
            if(parentUsedPartsWarehouseArea == null)
                return;
            if(this.EditState == DataEditState.New) {
                this.CanEditAreaKind = true;
                if(parentUsedPartsWarehouseArea.StorageAreaType == (int)DcsAreaKind.仓库) {
                    this.StorageAreaTypes = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key == (int)DcsAreaKind.库区));
                    this.CanEditAreaCategory = false;
                    partsWarehouseArea.StorageCategory = (int)DcsAreaType.保管区;
                } else {
                    this.StorageAreaTypes = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key != (int)DcsAreaKind.仓库));
                }
            } else {
                //库区库位类型 顶层库区不允许变更为库位（库区库位类型 不可编辑）
                //库区库位类型 如果是默认库位则 库区库位类型 不可编辑
                this.CanEditAreaKind = partsWarehouseArea.TopLevelUsedPartsWhseAreaId.HasValue && partsWarehouseArea.IfDefaultStoragePosition.HasValue && !partsWarehouseArea.IfDefaultStoragePosition.Value;
                this.StorageAreaTypes = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]].Where(item => item.Key != (int)DcsAreaKind.仓库));
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetUsedPartsWarehouseAreasWithAreaManagersAndDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    //if(entity.StorageAreaType == (int)DcsAreaKind.库位) {
                    //    this.ckIfDefaultStoragePosition.IsEnabled = true;
                    //} else {
                    this.ckIfDefaultStoragePosition.IsEnabled = false;
                    //}
                    this.SetObjectToEdit(entity);
                    this.UsedPartsWarehouseAreaDataTreeView.RefreshDataTree();
                }
            }, null);
        }

        private void CreateUI() {
            this.UsedPartsWarehouseAreaDataTreeView.SetValue(Grid.RowSpanProperty, 3);
            this.UsedPartsWarehouseAreaDataTreeView.SetValue(Grid.RowProperty, 0);
            this.UsedPartsWarehouseAreaDataTreeView.SetValue(IsEnabledProperty, false);
            this.Root.Children.Add(this.UsedPartsWarehouseAreaDataTreeView);
            var verticalLine = this.CreateVerticalLine(1);
            verticalLine.SetValue(Grid.ColumnProperty, 1);
            verticalLine.SetValue(Grid.RowSpanProperty, 2);
            this.Root.Children.Add(verticalLine);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(UsedPartsWarehouseArea), "UsedPartsWarehouseManagers"), null, () => this.UsedPartsWarehouseManagerForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.SetValue(VerticalAlignmentProperty, VerticalAlignment.Stretch);
            this.Root.Children.Add(detailDataEditView);
            this.KeyValueManager.LoadData();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_UsedPartsWarehouseArea;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.UsedPartsWarehouseManagerForEditDataGridView.CommitEdit())
                return;
            var usedpartsWarehouseArea = this.DataContext as UsedPartsWarehouseArea;
            if(usedpartsWarehouseArea == null)
                return;
            usedpartsWarehouseArea.ValidationErrors.Clear();
            if(usedpartsWarehouseArea.StorageAreaType == 0)
                usedpartsWarehouseArea.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsWarehouseArea_StorageAreaTypeIsNull, new[] {
                        "StorageAreaType"
                    }));
            if(usedpartsWarehouseArea.StorageCategory == 0) {
                usedpartsWarehouseArea.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsWarehouseArea_StorageCategoryIsNull, new[] {
                        "StorageCategory"
                    }));
            }
            if(usedpartsWarehouseArea.HasValidationErrors)
                return;
            ((IEditableObject)usedpartsWarehouseArea).EndEdit();
            try {
                if(EditState == DataEditState.Edit) {
                    if(usedpartsWarehouseArea.Can修改旧件库区库位)
                        usedpartsWarehouseArea.修改旧件库区库位();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public bool CanEditAreaCategory {
            get {
                return this.canEditAreaCategory;
            }
            set {
                this.canEditAreaCategory = value;
                this.OnPropertyChanged("CanEditAreaCategory");
            }
        }

        public bool CanEditAreaKind {
            get {
                return this.canEditAreaKind;
            }
            set {
                this.canEditAreaKind = value;
                this.OnPropertyChanged("CanEditAreaKind");
            }
        }

        public ObservableCollection<KeyValuePair> StorageAreaTypes {
            get {
                return this.areaKinds ?? (this.areaKinds = new ObservableCollection<KeyValuePair>());
            }
            set {
                this.areaKinds = value;
                this.OnPropertyChanged("StorageAreaTypes");
            }
        }

        public object StorageCategories {
            get {
                return this.KeyValueManager[kvNames[1]];
            }
        }

    }
}
