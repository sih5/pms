﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Text;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataEdit {
    public partial class UsedPartsWarehouseDataEditView {
        private DataGridViewBase usedPartsWarehouseStaffForEditDataGridView;

        private DataGridViewBase UsedPartsWarehouseStaffForEditDataGridView {
            get {
                if(this.usedPartsWarehouseStaffForEditDataGridView == null) {
                    this.usedPartsWarehouseStaffForEditDataGridView = DI.GetDataGridView("UsedPartsWarehouseStaffForEdit");
                    this.usedPartsWarehouseStaffForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.usedPartsWarehouseStaffForEditDataGridView;
            }
        }

        private bool CheckStringLengthTooLong(string value, int length) {
            return Encoding.UTF8.GetBytes(value).Length > length;
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("UsedPartsWarehouse"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(UsedPartsWarehouse), "UsedPartsWarehouseStaffs"), null, () => this.UsedPartsWarehouseStaffForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetUsedPartsWarehousesWithDetailsQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.UsedPartsWarehouseStaffForEditDataGridView.CommitEdit())
                return;
            var usedPartsWarehouse = this.DataContext as UsedPartsWarehouse;
            if(usedPartsWarehouse == null)
                return;
            usedPartsWarehouse.ValidationErrors.Clear();
            foreach(var usedPartsWarehouseStaff in usedPartsWarehouse.UsedPartsWarehouseStaffs)
                usedPartsWarehouseStaff.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(usedPartsWarehouse.Code))
                usedPartsWarehouse.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsWarehouse_CodeIsNull, new[] {
                    "Code"
                }));
            if(string.IsNullOrEmpty(usedPartsWarehouse.Name))
                usedPartsWarehouse.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsWarehouse_NameIsNull, new[] {
                    "Name"
                }));
            foreach(var usedPartsWarehouseStaff in usedPartsWarehouse.UsedPartsWarehouseStaffs.Where(e => e.PersonnelId == default(int)))
                usedPartsWarehouseStaff.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsWarehouseStaff_PersonnelIdIsNull, new[] {
                    "PersonnelId"
                }));

            if(usedPartsWarehouse.HasValidationErrors || usedPartsWarehouse.UsedPartsWarehouseStaffs.Any((e => e.HasValidationErrors))) {
                if(usedPartsWarehouse.UsedPartsWarehouseStaffs.Any((e => e.HasValidationErrors)))
                    UIHelper.ShowAlertMessage(ServiceUIStrings.DataEditView_Validation_UsedPartsWarehouseStaff_PersonnelIdIsNull);
                return;
            }
            if(EditState == DataEditState.New) {
                foreach(var area in this.DomainContext.UsedPartsWarehouseAreas.Where(entity => entity.EntityState == EntityState.New).ToList())
                    this.DomainContext.UsedPartsWarehouseAreas.Remove(area);
                var warehouseAreaWarehouse = new UsedPartsWarehouseArea {
                    Code = usedPartsWarehouse.Code,
                    Status = (int)DcsBaseDataStatus.有效,
                    StorageAreaType = (int)DcsAreaKind.仓库,
                    IfDefaultStoragePosition = false
                };
                ((IEditableObject)warehouseAreaWarehouse).EndEdit();
                usedPartsWarehouse.UsedPartsWarehouseAreas.Add(warehouseAreaWarehouse);
                //var usedPartsWarehouseArea = new UsedPartsWarehouseArea {
                //    UsedPartsWarehouseId = usedPartsWarehouse.Id,
                //    Code = usedPartsWarehouse.Code + " " + ServiceUIStrings.DataEditView_Text_UsedPartsWarehouseArea_Constant_DefaultArea,
                //    StorageCategory = (int)DcsAreaType.保管区,
                //    Status = (int)DcsBaseDataStatus.有效,
                //    StorageAreaType = (int)DcsAreaKind.库区,
                //    IfDefaultStoragePosition = false
                //};
                //if(CheckStringLengthTooLong(usedPartsWarehouseArea.Code, 50)) {
                //    usedPartsWarehouse.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsWarehouseArea_CodeIsTooLong, new[] {
                //            "Code"
                //        }));
                //    return;
                //}
                //((IEditableObject)usedPartsWarehouseArea).EndEdit();
                // warehouseAreaWarehouse.ChildUsedPartsWarehouseAreas.Add(usedPartsWarehouseArea);
                //var usedPartsWarehouseAreaStack = new UsedPartsWarehouseArea {
                //    UsedPartsWarehouseId = usedPartsWarehouse.Id,
                //    Code = usedPartsWarehouse.Code + " " + ServiceUIStrings.DataEditView_Text_UsedPartsWarehouseArea_Constant_DefaultPosition,
                //    StorageCategory = (int)DcsAreaType.保管区,
                //    Status = (int)DcsBaseDataStatus.有效,
                //    StorageAreaType = (int)DcsAreaKind.库位,
                //    RootUsedPartsWarehouseArea = usedPartsWarehouseArea,
                //    IfDefaultStoragePosition = true
                //};
                //if(CheckStringLengthTooLong(usedPartsWarehouseAreaStack.Code, 50)) {
                //    usedPartsWarehouse.ValidationErrors.Add(new ValidationResult(ServiceUIStrings.DataEditView_Validation_UsedPartsWarehouseArea_CodeIsTooLong, new[] {
                //            "Code"
                //        }));
                //    return;
                //}
                // ((IEditableObject)usedPartsWarehouseAreaStack).EndEdit();
                //usedPartsWarehouseArea.ChildUsedPartsWarehouseAreas.Add(usedPartsWarehouseAreaStack);
            }
            ((IEditableObject)usedPartsWarehouse).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_UsedPartsWarehouse;
            }
        }

        public UsedPartsWarehouseDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
