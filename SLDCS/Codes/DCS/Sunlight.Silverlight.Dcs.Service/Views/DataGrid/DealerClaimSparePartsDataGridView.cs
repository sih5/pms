﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class DealerClaimSparePartsDataGridView  : DcsDataGridViewBase {       
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                        Name = "ClaimCode",
                        Title = "索赔领料单号"
                    },new ColumnItem {
                        Name = "ClaimStatus",
                        Title="索赔单状态"
                    }, new ColumnItem {
                        Name = "DealerCode",
                        Title="服务站编号"
                    },new ColumnItem {
                        Name = "DealerName",
                        Title="服务站名称"
                    },new ColumnItem {
                        Name = "SparePartCode",
                        Title="配件图号"
                    },new ColumnItem {
                        Name = "SparePartName",
                        Title= "配件名称"
                    },new ColumnItem {
                        Name = "Price",
                        Title="配件基准价"
                    }, new ColumnItem {
                        Name = "SettleDate",
                        Title="索赔单结算时间"
                    }, new ColumnItem {
                        Name = "Quantity",
                        Title="数量"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title="创建时间"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerClaimSparePart);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerClaimSpareParts";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}