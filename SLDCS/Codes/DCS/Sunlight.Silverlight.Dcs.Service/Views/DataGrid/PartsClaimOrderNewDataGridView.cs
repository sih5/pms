﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class PartsClaimOrderNewDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsClaimBill_StatusNew", "Reject_Status","PartsInboundPlan_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"Status")
                    },new ColumnItem {
                        IsSortable = false,
                        Name = "IsUplodFile",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_IsUplodFile
                    },new ColumnItem {
                        Name = "Code",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"Code")
                    }, new ColumnItem {
                        Name = "ReturnCompanyName",
                        Title=Utils.GetEntityLocalizedName(typeof(Company),"Name")
                    },new ColumnItem {
                        Name = "PartsSalesOrderCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsSalesOrderCode")
                    },new ColumnItem {
                        Name = "PartsRetailOrderCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsRetailOrderCode")
                    },new ColumnItem {
                        Name = "CenterPartsSalesOrderCode",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_CenterPartsSalesOrderCode
                    },
                    new ColumnItem {
                        Name = "ReturnWarehouseName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"ReturnWarehouseName")
                    }, new ColumnItem {
                        Name = "ReturnWarehouseCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"ReturnWarehouseCode")
                    }, new ColumnItem {
                        Name = "RWarehouseCompanyCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"RWarehouseCompanyCode")
                    }, new ColumnItem {
                        Name = "SalesWarehouseCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"SalesWarehouseCode")
                    }, new ColumnItem {
                        Name = "SalesWarehouseName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"SalesWarehouseName")
                    }, new ColumnItem {
                        Name = "CustomerContactPerson",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"CustomerContactPerson")
                    },new ColumnItem {
                        Name = "CustomerContactPhone",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"CustomerContactPhone")
                    },new ColumnItem {
                        Name = "FaultyPartsName",
                        Title=Utils.GetEntityLocalizedName(typeof(SparePart),"Name")
                    },new ColumnItem {
                        Name = "FaultyPartsCode",
                        Title=Utils.GetEntityLocalizedName(typeof(SparePart),"Code")
                    },new ColumnItem {
                        Name = "FaultyPartsSupplierCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsSupplier),"Code")
                    },new ColumnItem {
                        Name = "PartsWarrantyLong",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsWarrantyLong")
                    },new ColumnItem {
                        Name = "RepairRequestTime",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"RepairRequestTime")
                    },new ColumnItem {
                        Name = "PartsSaleTime",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsSaleTime")
                    },new ColumnItem {
                        Name = "PartsSaleLong",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsSaleLong")
                    },new ColumnItem {
                        Name = "IsOutWarranty",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"IsOutWarranty")
                    },new ColumnItem {
                        Name = "OverWarrantyDate",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_OverWarrantyDate
                    }, new ColumnItem {
                        Name = "MaterialCost",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"MaterialCost")
                    },new ColumnItem {
                        Name = "MaterialManagementCost",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_MaterialManagementCost
                    },                   
                    new ColumnItem {
                        Name = "MalfunctionDescription",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"MalfunctionDescription")
                    }, new ColumnItem {
                        Name = "Remark",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"Remark")
                    }, new ColumnItem {
                        Name = "ApproveComment",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"ApproveComment")
                    }                    
                    , new KeyValuesColumnItem {
                        Name = "RejectStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"RejectStatus")
                    },new ColumnItem {
                        Name = "RejectReason",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_RejectReason
                    },new ColumnItem {
                        Name = "RejectQty",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"RejectQty")                
                    },new ColumnItem{
                        Name="AbandonerReason",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_AbandonerReason
                    },new ColumnItem{
                        Name="StopReason",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_StopReason
                    },new ColumnItem{
                        Name="ReturnReason",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_ReturnReason
                    }, new ColumnItem{
                        Name="CreatorName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"CreatorName")
                    },new ColumnItem{
                        Name="CreateTime",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"CreateTime")
                    },new ColumnItem{
                        Name="ModifierName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"ModifierName")
                    },new ColumnItem{
                        Name="ModifyTime",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"ModifyTime")
                    },new ColumnItem{
                        Name="ApproverName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"ApproverName")
                    },new ColumnItem{
                        Name="ApproveTime",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"ApproveTime")
                    },new ColumnItem{
                        Name="RejectName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"RejectName")
                    },new ColumnItem{
                        Name="RejectTime",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"RejectTime")
                    },new ColumnItem{
                        Name="CancelName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"CancelName")
                    },new ColumnItem{
                        Name="CancelTime",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"CancelTime")
                    },new ColumnItem{
                        Name="AbandonerName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"AbandonerName")
                    },new ColumnItem{
                        Name="AbandonTime",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"AbandonTime")
                    },new ColumnItem {
                        Title = ServiceUIStrings.QueryPanel_QueryItem_Title_Common_PartsSalesCategoryName,
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsClaimOrderNewWithOtherInfo);
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "bussinessCode":
                        //var filter = filters.Filters.SingleOrDefault(item => item.MemberName == "BusinessCode");
                        //if(filter == null)
                        //    break;
                        //return filter.Value;
                        return null;
                    case "bussinessName":
                        //var filterItem = filters.Filters.SingleOrDefault(item => item.MemberName == "BusinessName");
                        //if(filterItem == null)
                        //    break;
                        //return filterItem.Value;
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return "查询配件索赔单含业务编码";
        }

        public PartsClaimOrderNewDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}