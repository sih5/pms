﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class PartsClaimOrderNewForDealerDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsClaimBill_StatusNew", "Reject_Status","PartsInboundPlan_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_SsClaimSettleInstruction_Status
                    },new ColumnItem {
                        Name = "Code",
                         Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"Code")
                    }, new ColumnItem {
                        Name = "ReturnCompanyName",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_ReturnCompanyName
                    },new ColumnItem {
                        Name = "PartsSalesOrderCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsSalesOrderCode")
                    },new ColumnItem {
                        Name = "PartsRetailOrderCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsRetailOrderCode")
                    }, new ColumnItem {
                        Name = "ReturnWarehouseName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"ReturnWarehouseName")
                    }, new ColumnItem {
                        Name = "ReturnWarehouseCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"ReturnWarehouseCode")
                    }, new ColumnItem {
                        Name = "RWarehouseCompanyCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"RWarehouseCompanyCode")
                    }, new ColumnItem {
                        Name = "SalesWarehouseCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"SalesWarehouseCode")
                    }, new ColumnItem {
                        Name = "SalesWarehouseName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"SalesWarehouseName")
                    }, new ColumnItem {
                        Name = "CustomerContactPerson",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"CustomerContactPerson")
                    },new ColumnItem {
                        Name = "CustomerContactPhone",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"CustomerContactPhone")
                    },new ColumnItem {
                        Name = "FaultyPartsName",
                        Title=Utils.GetEntityLocalizedName(typeof(SparePart),"Name")
                    },new ColumnItem {
                        Name = "FaultyPartsCode",
                        Title=Utils.GetEntityLocalizedName(typeof(SparePart),"Code")
                    },new ColumnItem {
                        Name = "FaultyPartsSupplierCode",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsSupplier),"Code")
                    },new ColumnItem {
                        Name = "PartsWarrantyLong",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsWarrantyLong")
                    },new ColumnItem {
                        Name = "RepairRequestTime",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"RepairRequestTime")
                    },new ColumnItem {
                        Name = "PartsSaleTime",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsSaleTime")
                    },new ColumnItem {
                        Name = "PartsSaleLong",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"PartsSaleLong")
                    },new ColumnItem {
                        Name = "IsOutWarranty",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"IsOutWarranty")
                    },new ColumnItem {
                        Name = "OverWarrantyDate",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_OverWarrantyDate
                    },new ColumnItem {
                        Name = "MaterialCost",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"MaterialCost")
                    },new ColumnItem {
                        Name = "MaterialManagementCost",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_MaterialManagementCost
                        //Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"MaterialManagementCost")
                    }
                    //, new ColumnItem {
                    //    Name = "TotalAmount",
                    //    Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"TotalAmount")
                    //}
                    , new ColumnItem {
                        Name = "MalfunctionDescription",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"MalfunctionDescription")
                    }, new ColumnItem {
                        Name = "Remark",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_SsClaimSettleInstruction_Remark
                    }, new ColumnItem {
                        Name = "ApproveComment",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"ApproveComment")
                    },new ColumnItem {
                        Name = "ShippingCode",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_ShippingCode
                    }, new KeyValuesColumnItem {
                        Name = "RejectStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"RejectStatus")
                    }, new ColumnItem{
                        Name="ReturnReason",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_ReturnReason
                    }, new ColumnItem{
                        Name="AbandonerReason",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_AbandonerReason
                    }, new ColumnItem{
                        Name="StopReason",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_StopReason
                    }, new ColumnItem{
                        Name="RejectReason",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_RejectReason
                    },new ColumnItem {
                        Name = "RejectQty",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"RejectQty")
                    }, new ColumnItem{
                        Name="CreatorName",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_SsClaimSettleInstruction_CreatorName
                    },new ColumnItem{
                        Name="CreateTime",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_SsClaimSettleInstruction_CreateTime
                    },new ColumnItem{
                        Name="ModifierName",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_ModifierName
                    },new ColumnItem{
                        Name="ModifyTime",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_ModifyTime
                    },new ColumnItem{
                        Name="ApproverName",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_ApproverName
                    },new ColumnItem{
                        Name="ApproveTime",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_ApproveTime
                    },new ColumnItem{
                        Name="RejectName",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_RejectName
                    },new ColumnItem{
                        Name="RejectTime",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_PartsClaimOrderNew_RejectTime
                    },new ColumnItem{
                        Name="CancelName",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"CancelName")
                    },new ColumnItem{
                        Name="CancelTime",
                        Title=Utils.GetEntityLocalizedName(typeof(PartsClaimOrderNew),"CancelTime")
                    },new ColumnItem{
                        Name="AbandonerName",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_SsClaimSettleInstruction_AbandonerName
                    },new ColumnItem{
                        Name="AbandonTime",
                        Title= ServiceUIStrings.DataGridView_ColumnItem_Title_SsClaimSettleInstruction_AbandonTime
                    },new ColumnItem {
                        Title = ServiceUIStrings.QueryPanel_QueryItem_Title_Common_PartsSalesCategoryName,
                        Name = "PartsSalesCategoryName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(PartsClaimOrderNewWithOtherInfo);
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return base.OnRequestFilterDescriptor(queryName);
            var newCompositeFilterItem = new CompositeFilterItem();
            foreach(var filter in compositeFilterItem.Filters.Where(item => (item.MemberName != "BusinessCode" && item.MemberName != "BusinessName")))
                newCompositeFilterItem.Filters.Add(filter);
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "bussinessCode":
                        var bussinessCode = filters.Filters.SingleOrDefault(item => item.MemberName == "BusinessCode");
                        return bussinessCode == null ? null : bussinessCode.Value;
                    case "bussinessName":
                        var bussinessName = filters.Filters.SingleOrDefault(item => item.MemberName == "BusinessName");
                        return bussinessName == null ? null : bussinessName.Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override string OnRequestQueryName() {
            return "服务站查询配件索赔单含业务编码";
        }

        public PartsClaimOrderNewForDealerDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
