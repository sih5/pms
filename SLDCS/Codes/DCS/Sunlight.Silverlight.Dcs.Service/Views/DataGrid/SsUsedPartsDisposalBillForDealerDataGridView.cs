﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class SsUsedPartsDisposalBillForDealerDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SsUsedPartsDisposalBill_DisposalMethod", "SsUsedPartsDisposalBill_Status"
        };

        public SsUsedPartsDisposalBillForDealerDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "Code"
                    }, new KeyValuesColumnItem {
                        Name = "UsedPartsDisposalMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "ProcessTime"
                    }, new ColumnItem {
                        Name = "TotalAmount"
                    }, new ColumnItem{
                        Name = "ProcessAmount",
                        Title =ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsDisposalBill_ProcessAmount
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title =ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SsUsedPartsDisposalBill);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[]{
                        "SsUsedPartsDisposalDetail"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSsUsedPartsDisposalBillsWithPartsSalesCategory";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
