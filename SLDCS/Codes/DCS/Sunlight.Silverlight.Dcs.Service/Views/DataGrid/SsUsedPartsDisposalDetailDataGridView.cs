﻿
using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class SsUsedPartsDisposalDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy","SsUsedPartsDisposalBill_DisposalMethod"
        };

        public SsUsedPartsDisposalDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += SsUsedPartsDisposalDetailDataGridView_DataContextChanged;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ClaimBillCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = false
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsStorage_UsedPartsCode,
                        IsReadOnly = true,
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsStorage_UsedPartsName,
                        IsReadOnly = true,
                        Name = "UsedPartsName"
                    }, new KeyValuesColumnItem {
                        Name = "UsedPartsReturnPolicy",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        IsReadOnly = true,
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsDisposalDetail_UnitPrice
                    }, new ColumnItem {
                        Name = "PartsManagementCost",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "IfFaultyParts",
                    }, new ColumnItem {
                        Name = "FaultyPartsCode"
                    }, new ColumnItem {
                        Name = "FaultyPartsName"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierName"
                    }, new ColumnItem {
                        Name = "UsedPartsBatchNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsSerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)this.GridView.Columns["UnitPrice"]).DataFormatString = "c2";
        }

        private void SsUsedPartsDisposalDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var ssUsedPartsDisposalBill = e.NewValue as SsUsedPartsDisposalBill;
            if(ssUsedPartsDisposalBill == null || ssUsedPartsDisposalBill.Id == default(int))
                return;
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "SsUsedPartsDisposalBillId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = ssUsedPartsDisposalBill.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SsUsedPartsDisposalDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSsUsedPartsDisposalDetails";
        }
    }
}
