﻿
namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class SsUsedPartsDisposalDetailForApproveDataGridView : SsUsedPartsDisposalDetailForEditDataGridView {
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
        }
    }
}
