﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class SsUsedPartsDisposalDetailForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase usedPartsStockDropDownQueryWindow;
        private DcsMultiPopupsQueryWindowBase usedPartsStockMultiSelectQueryWindow;
        private RadWindow radQueryWindow;

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.UsedPartsStockMultiSelectQueryWindow,
                    Header = ServiceUIStrings.QueryPanel_Title_SsUsedPartsStorage,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy","SsUsedPartsDisposalBill_DisposalMethod"
        };

        public SsUsedPartsDisposalDetailForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        private DcsMultiPopupsQueryWindowBase UsedPartsStockMultiSelectQueryWindow {
            get {
                if(this.usedPartsStockMultiSelectQueryWindow == null) {
                    this.usedPartsStockMultiSelectQueryWindow = DI.GetQueryWindow("SsUsedPartsStorageMulit") as DcsMultiPopupsQueryWindowBase;
                    this.usedPartsStockMultiSelectQueryWindow.SelectionDecided += this.usedPartsStockMultiSelectQueryWindow_SelectionDecided;
                    this.usedPartsStockMultiSelectQueryWindow.Loaded += usedPartsStockMultiSelectQueryWindow_Loaded;

                }
                return this.usedPartsStockMultiSelectQueryWindow;
            }
        }

        private void usedPartsStockMultiSelectQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var ssUsedPartsDisposalBill = this.DataContext as SsUsedPartsDisposalBill;
            if(ssUsedPartsDisposalBill == null)
                return;
            var userdPartsDisposalMethodValue = this.KeyValueManager[kvNames[1]].SingleOrDefault(ex => ex.Key == ssUsedPartsDisposalBill.UsedPartsDisposalMethod).Value;
            var usedPartsDisposalMethodKey = this.KeyValueManager[kvNames[0]].SingleOrDefault(ex => ex.Value == userdPartsDisposalMethodValue).Key;
            if(ssUsedPartsDisposalBill.BranchId != default(int)) {
                this.usedPartsStockMultiSelectQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "BranchId", ssUsedPartsDisposalBill.BranchId });
                this.usedPartsStockMultiSelectQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "BranchId", false });
            }
            if(ssUsedPartsDisposalBill.UsedPartsDisposalMethod != default(int)) {
                this.usedPartsStockMultiSelectQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "UsedPartsReturnPolicy", usedPartsDisposalMethodKey });
                this.usedPartsStockMultiSelectQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "UsedPartsReturnPolicy", false });
            }
            if(ssUsedPartsDisposalBill.PartsSalesCategoryId != default(int)) {
                this.usedPartsStockMultiSelectQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "PartsSalesCategoryId", ssUsedPartsDisposalBill.PartsSalesCategoryId });
                this.usedPartsStockMultiSelectQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "PartsSalesCategoryId", false });
            }
            this.usedPartsStockMultiSelectQueryWindow.ExchangeData(null, "ExecuteQuery", null);
        }

        private void usedPartsStockMultiSelectQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var ssUsedPartsStorageEntities = queryWindow.SelectedEntities.Cast<SsUsedPartsStorage>();
            if(ssUsedPartsStorageEntities == null)
                return;
            var ssUsedPartsDisposalBill = this.DataContext as SsUsedPartsDisposalBill;
            if(ssUsedPartsDisposalBill == null) {
                return;
            }
            if(ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.Any(r => ssUsedPartsStorageEntities.Any(ex => ex.UsedPartsBarCode == r.UsedPartsBarCode))) {
                UIHelper.ShowNotification("旧件条码不允许重复");
                return;
            }
            foreach(var ssUsedPartsStorage in ssUsedPartsStorageEntities) {
                ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.Add(new SsUsedPartsDisposalDetail {
                    SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<SsUsedPartsDisposalDetail>().Max(entity => entity.SerialNumber) + 1 : 1,
                    PartsSalesCategoryId = ssUsedPartsDisposalBill.PartsSalesCategoryId,
                    UsedPartsBarCode = ssUsedPartsStorage.UsedPartsBarCode,
                    ClaimBillCode = ssUsedPartsStorage.ClaimBillCode,
                    UsedPartsId = ssUsedPartsStorage.UsedPartsId,
                    UsedPartsCode = ssUsedPartsStorage.UsedPartsCode,
                    UsedPartsName = ssUsedPartsStorage.UsedPartsName,
                    UsedPartsSerialNumber = ssUsedPartsStorage.UsedPartsSerialNumber,
                    UsedPartsBatchNumber = ssUsedPartsStorage.UsedPartsBatchNumber,
                    UsedPartsSupplierId = ssUsedPartsStorage.UsedPartsSupplierId,
                    UsedPartsSupplierCode = ssUsedPartsStorage.UsedPartsSupplierCode,
                    UsedPartsSupplierName = ssUsedPartsStorage.UsedPartsSupplierName,
                    UsedPartsReturnPolicy = ssUsedPartsStorage.UsedPartsReturnPolicy,
                    Quantity = ssUsedPartsStorage.Quantity,
                    UnitPrice = ssUsedPartsStorage.UnitPrice,
                    PartsManagementCost = ssUsedPartsStorage.PartsManagementCost,
                    FaultyPartsId = ssUsedPartsStorage.FaultyPartsId,
                    FaultyPartsCode = ssUsedPartsStorage.FaultyPartsCode,
                    FaultyPartsName = ssUsedPartsStorage.FaultyPartsName,
                    FaultyPartsSupplierId = ssUsedPartsStorage.FaultyPartsSupplierId,
                    FaultyPartsSupplierCode = ssUsedPartsStorage.FaultyPartsSupplierCode,
                    FaultyPartsSupplierName = ssUsedPartsStorage.FaultyPartsSupplierName,
                    IfFaultyParts = ssUsedPartsStorage.IfFaultyParts,
                    Shipped = ssUsedPartsStorage.Shipped,
                    NewPartsId = ssUsedPartsStorage.NewPartsId,
                    NewPartsCode = ssUsedPartsStorage.NewPartsCode,
                    NewPartsName = ssUsedPartsStorage.NewPartsName,
                    NewPartsSupplierId = ssUsedPartsStorage.NewPartsSupplierId,
                    NewPartsSupplierCode = ssUsedPartsStorage.NewPartsSupplierCode,
                    NewPartsSupplierName = ssUsedPartsStorage.NewPartsSupplierName,
                    NewPartsSerialNumber = ssUsedPartsStorage.NewPartsSerialNumber,
                    NewPartsBatchNumber = ssUsedPartsStorage.NewPartsBatchNumber,
                    NewPartsSecurityNumber = ssUsedPartsStorage.NewPartsSecurityNumber,
                    BranchId = ssUsedPartsStorage.BranchId,
                    BranchName = ssUsedPartsStorage.BranchName,
                    ResponsibleUnitId = ssUsedPartsStorage.ResponsibleUnitId,
                    ResponsibleUnitName = ssUsedPartsStorage.ResponsibleUnitName,
                    ResponsibleUnitCode = ssUsedPartsStorage.ResponsibleUnitCode,
                    ClaimBillCreateTime = ssUsedPartsStorage.ClaimBillCreateTime
                });
            }
            ssUsedPartsDisposalBill.TotalAmount = ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.Sum(entity => entity.UnitPrice);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }


        private QueryWindowBase UsedPartsStockDropDownQueryWindow {
            get {
                if(this.usedPartsStockDropDownQueryWindow == null) {
                    this.usedPartsStockDropDownQueryWindow = DI.GetQueryWindow("SsUsedPartsStorageDropDown");
                    this.usedPartsStockDropDownQueryWindow.SelectionDecided += this.UsedPartsStockDropDownQueryWindow_SelectionDecided;
                    this.usedPartsStockDropDownQueryWindow.Loaded += this.UsedPartsStockDropDownQueryWindow_Loaded;
                }
                return this.usedPartsStockDropDownQueryWindow;
            }
        }

        private void UsedPartsStockDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var ssUsedPartsDisposalBill = this.DataContext as SsUsedPartsDisposalBill;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || ssUsedPartsDisposalBill == null)
                return;
            if(ssUsedPartsDisposalBill.BranchId != default(int)) {
                this.UsedPartsStockDropDownQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("BranchId", typeof(int), FilterOperator.IsEqualTo, ssUsedPartsDisposalBill.BranchId));
            }
            if(ssUsedPartsDisposalBill.UsedPartsDisposalMethod != default(int)) {
                this.UsedPartsStockDropDownQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                    "Common", "UsedPartsReturnPolicy", ssUsedPartsDisposalBill.UsedPartsDisposalMethod
                });
                this.usedPartsStockDropDownQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                    "Common", "UsedPartsReturnPolicy", false
                });
            }
            if(ssUsedPartsDisposalBill.PartsSalesCategoryId != default(int)) {
                this.UsedPartsStockDropDownQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "PartsSalesCategoryId", ssUsedPartsDisposalBill.PartsSalesCategoryId });
                this.UsedPartsStockDropDownQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "PartsSalesCategoryId", false });
            }

        }

        private void UsedPartsStockDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var ssUsedPartsStorage = queryWindow.SelectedEntities.Cast<SsUsedPartsStorage>().FirstOrDefault();
            var ssUsedPartsDisposalDetail = queryWindow.DataContext as SsUsedPartsDisposalDetail;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(ssUsedPartsDisposalDetail == null || ssUsedPartsStorage == null || domainContext == null)
                return;
            ssUsedPartsDisposalDetail.UsedPartsBarCode = ssUsedPartsStorage.UsedPartsBarCode;
            ssUsedPartsDisposalDetail.ClaimBillCode = ssUsedPartsStorage.ClaimBillCode;
            ssUsedPartsDisposalDetail.UsedPartsId = ssUsedPartsStorage.UsedPartsId;
            ssUsedPartsDisposalDetail.UsedPartsCode = ssUsedPartsStorage.UsedPartsCode;
            ssUsedPartsDisposalDetail.UsedPartsName = ssUsedPartsStorage.UsedPartsName;
            ssUsedPartsDisposalDetail.UsedPartsSerialNumber = ssUsedPartsStorage.UsedPartsSerialNumber;
            ssUsedPartsDisposalDetail.UsedPartsBatchNumber = ssUsedPartsStorage.UsedPartsBatchNumber;
            ssUsedPartsDisposalDetail.UsedPartsSupplierId = ssUsedPartsStorage.UsedPartsSupplierId;
            ssUsedPartsDisposalDetail.UsedPartsSupplierCode = ssUsedPartsStorage.UsedPartsSupplierCode;
            ssUsedPartsDisposalDetail.UsedPartsSupplierName = ssUsedPartsStorage.UsedPartsSupplierName;
            ssUsedPartsDisposalDetail.UsedPartsReturnPolicy = ssUsedPartsStorage.UsedPartsReturnPolicy;
            ssUsedPartsDisposalDetail.Quantity = ssUsedPartsStorage.Quantity;
            ssUsedPartsDisposalDetail.UnitPrice = ssUsedPartsStorage.UnitPrice;
            ssUsedPartsDisposalDetail.PartsManagementCost = ssUsedPartsStorage.PartsManagementCost;
            ssUsedPartsDisposalDetail.FaultyPartsId = ssUsedPartsStorage.FaultyPartsId;
            ssUsedPartsDisposalDetail.FaultyPartsCode = ssUsedPartsStorage.FaultyPartsCode;
            ssUsedPartsDisposalDetail.FaultyPartsName = ssUsedPartsStorage.FaultyPartsName;
            ssUsedPartsDisposalDetail.FaultyPartsSupplierId = ssUsedPartsStorage.FaultyPartsSupplierId;
            ssUsedPartsDisposalDetail.FaultyPartsSupplierCode = ssUsedPartsStorage.FaultyPartsSupplierCode;
            ssUsedPartsDisposalDetail.FaultyPartsSupplierName = ssUsedPartsStorage.FaultyPartsSupplierName;
            ssUsedPartsDisposalDetail.IfFaultyParts = ssUsedPartsStorage.IfFaultyParts;
            ssUsedPartsDisposalDetail.Shipped = ssUsedPartsStorage.Shipped;
            ssUsedPartsDisposalDetail.NewPartsId = ssUsedPartsStorage.NewPartsId;
            ssUsedPartsDisposalDetail.NewPartsCode = ssUsedPartsStorage.NewPartsCode;
            ssUsedPartsDisposalDetail.NewPartsName = ssUsedPartsStorage.NewPartsName;
            ssUsedPartsDisposalDetail.NewPartsSupplierId = ssUsedPartsStorage.NewPartsSupplierId;
            ssUsedPartsDisposalDetail.NewPartsSupplierCode = ssUsedPartsStorage.NewPartsSupplierCode;
            ssUsedPartsDisposalDetail.NewPartsSupplierName = ssUsedPartsStorage.NewPartsSupplierName;
            ssUsedPartsDisposalDetail.NewPartsSerialNumber = ssUsedPartsStorage.NewPartsSerialNumber;
            ssUsedPartsDisposalDetail.NewPartsBatchNumber = ssUsedPartsStorage.NewPartsBatchNumber;
            ssUsedPartsDisposalDetail.NewPartsSecurityNumber = ssUsedPartsStorage.NewPartsSecurityNumber;
            ssUsedPartsDisposalDetail.BranchId = ssUsedPartsStorage.BranchId;
            ssUsedPartsDisposalDetail.BranchName = ssUsedPartsStorage.BranchName;
            ssUsedPartsDisposalDetail.ResponsibleUnitId = ssUsedPartsStorage.ResponsibleUnitId;
            ssUsedPartsDisposalDetail.ResponsibleUnitName = ssUsedPartsStorage.ResponsibleUnitName;
            ssUsedPartsDisposalDetail.ResponsibleUnitCode = ssUsedPartsStorage.ResponsibleUnitCode;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var ssUsedPartsDisposalBill = this.DataContext as SsUsedPartsDisposalBill;
            if(ssUsedPartsDisposalBill == null)
                return;
            var serialNumber = 1;
            foreach(var ssUsedPartsDisposalDetail in ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.Where(entity => !e.Items.Contains(entity))) {
                ssUsedPartsDisposalDetail.SerialNumber = serialNumber;
                serialNumber++;
            }
            ssUsedPartsDisposalBill.TotalAmount = ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.Sum(entity => entity.UnitPrice);
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var ssUsedPartsDisposalBill = this.DataContext as SsUsedPartsDisposalBill;
            if(ssUsedPartsDisposalBill == null)
                return;
            ssUsedPartsDisposalBill.TotalAmount = ssUsedPartsDisposalBill.SsUsedPartsDisposalDetails.Sum(entity => entity.UnitPrice);
        }
        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var ssUsedPartsDisposalBill = this.DataContext as SsUsedPartsDisposalBill;
            if(ssUsedPartsDisposalBill == null)
                return;
            if(ssUsedPartsDisposalBill.BranchId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_UsedPartsShippingOrder_BranchIdIsNull);
                return;
            }
            if(ssUsedPartsDisposalBill.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_PartsSalesOrder_PartsSalesCategoryIdIsNull);
                return;
            }
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            this.RadQueryWindow.ShowDialog();
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SsUsedPartsDisposalDetails");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
            this.GridView.CellEditEnded -= GridView_CellEditEnded;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ClaimBillCode",
                        IsReadOnly = true
                    }, new DropDownTextBoxColumnItem {
                        Name = "UsedPartsBarCode",
                        DropDownContent = this.UsedPartsStockDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsStorage_UsedPartsCode,
                        IsReadOnly = true,
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsStorage_UsedPartsName,
                        IsReadOnly = true,
                        Name = "UsedPartsName"
                    }, new KeyValuesColumnItem {
                        Name = "UsedPartsReturnPolicy",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        IsReadOnly = true,
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsDisposalDetail_UnitPrice
                    }, new ColumnItem {
                        Name = "PartsManagementCost",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "IfFaultyParts",
                    }, new ColumnItem {
                        Name = "FaultyPartsCode"
                    }, new ColumnItem {
                        Name = "FaultyPartsName"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierName"
                    }, new ColumnItem {
                        Name = "UsedPartsBatchNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsSerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SsUsedPartsDisposalDetail);
            }
        }
    }
}
