﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class SsUsedPartsStorageDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy"
        };

        public SsUsedPartsStorageDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(SsUsedPartsStorage);
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            return compositeFilterItem.ToFilterDescriptor();
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ClaimBillCode"
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new KeyValuesColumnItem {
                        Name = "UsedPartsReturnPolicy",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsStorage_UsedPartsCode,
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsStorage_UsedPartsName,
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Name = "UnitPrice"
                    }, new ColumnItem {
                        Name = "PartsManagementCost"
                    }, new ColumnItem {
                        Name = "ResponsibleUnitCode"
                    }, new ColumnItem {
                        Name = "ResponsibleUnitName"
                    }, new ColumnItem {
                        Name = "IfFaultyParts"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierName"
                    }, new ColumnItem {
                        Name = "FaultyPartsCode"
                    }, new ColumnItem {
                        Name = "FaultyPartsName"
                    },new ColumnItem {
                        Name = "ClaimBillCreateTime"
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title =ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSsUsedPartsStoragesWithPartsSalesCategory"; // GetSsUsedPartsStorages
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.DataPager.PageSize = 100;
        }
    }
}
