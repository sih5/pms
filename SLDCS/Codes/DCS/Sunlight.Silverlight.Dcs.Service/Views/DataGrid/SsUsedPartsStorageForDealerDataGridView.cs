﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class SsUsedPartsStorageForDealerDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy","RepairClaimBill_RepairClaimStatus","PartsClaimBill_Status"
        };

        public SsUsedPartsStorageForDealerDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(virtualSsUsedPartsStorageWithClaimBillStatus);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "RepairClaimStatus",
                        Title = "维修索赔单状态",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new KeyValuesColumnItem {
                        Name = "PartsClaimStatus",
                        Title = "配件索赔单状态",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    },new ColumnItem {
                        Name = "ClaimBillCode",
                        Title = "索赔单编号"
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        Title = "旧件条码"
                    }, new KeyValuesColumnItem {
                        Name = "UsedPartsReturnPolicy",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = "旧件返回政策"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsStorage_UsedPartsCode,
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsStorage_UsedPartsName,
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Name = "Shipped",
                        Title = "已发运"
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        Title = "单价"
                    //}, new ColumnItem {
                    //    Name = "PartsManagementCost"
                    }, new ColumnItem {
                        Name = "IfFaultyParts",
                        Title = "是否祸首件"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierCode",
                        Title = "旧件供应商编号"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierName",
                        Title = "旧件供应商名称"
                    }, new ColumnItem {
                        Name = "FaultyPartsCode",
                        Title = "祸首件编号"
                    }, new ColumnItem {
                        Name = "FaultyPartsName",
                        Title = "祸首件名称"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierCode",
                        Title = "祸首件供应商编号"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierName",
                        Title = "祸首件供应商名称"
                    }, new ColumnItem {
                        Name = "ClaimBillCreateTime",
                        Title = "索赔单生成时间"
                    },new ColumnItem{
                        Name="CreatorName",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsClaimSettleInstruction_CreatorName
                    },new ColumnItem{
                        Name="CreateTime",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsClaimSettleInstruction_CreateTime
                    }, new ColumnItem {
                        Name = "ModifierName",
                        Title = "修改人"
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        Title = "修改时间"
                    },new ColumnItem {
                      Name = "BranchName",
                      Title = "分公司名称"
                    },new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title =ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory
                    }
                };
            }
        }


        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "SsUsedPartsStorageForDealer"
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.DataPager.PageSize = 1000;
        }

        protected override string OnRequestQueryName() {
            return "查询旧件库存对应索赔单状态";
        }
    }
}
