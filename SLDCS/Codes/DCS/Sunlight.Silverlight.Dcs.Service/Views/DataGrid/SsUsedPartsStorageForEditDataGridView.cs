﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class SsUsedPartsStorageForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy"
        };

        public SsUsedPartsStorageForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var ssUsedPartsStorage = e.Row.DataContext as SsUsedPartsStorage;
            if(ssUsedPartsStorage == null)
                return;
            if(this.GridView.SelectedItems.Contains(ssUsedPartsStorage))
                return;
            switch(e.Cell.Column.UniqueName) {
                case "UsedPartsReturnPolicy":
                    e.Cancel = true;
                    break;
            }
        }

        protected void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            switch(e.Cell.Column.UniqueName) {
                case "UsedPartsReturnPolicy":
                    var ssUsedPartsStorage = (SsUsedPartsStorage)e.Cell.DataContext;
                    foreach(var ssUsedPartsStorages in this.GridView.SelectedItems.Cast<SsUsedPartsStorage>())
                        ssUsedPartsStorages.UsedPartsReturnPolicy = ssUsedPartsStorage.UsedPartsReturnPolicy;
                    break;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.SelectionChanged += this.GridView_SelectionChanged;
            this.GridView.RowLoaded += GridView_RowLoaded;
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            e.Row.IsSelected = true;
        }

        private void GridView_SelectionChanged(object sender, SelectionChangeEventArgs e) {
            this.GridView.CommitEdit();
            foreach(var entity in e.RemovedItems.Cast<SsUsedPartsStorage>())
                ((IRevertibleChangeTracking)entity).RejectChanges();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_Common_DealerCode,
                        Name = "Dealer.Code",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_Common_DealerName,
                        Name = "Dealer.Name",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ClaimBillCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "UsedPartsReturnPolicy",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsStorage_UsedPartsCode,
                        Name = "UsedPartsCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsUsedPartsStorage_UsedPartsName,
                        Name = "UsedPartsName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Shipped",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PartsManagementCost",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "IfFaultyParts",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        IsReadOnly = true,
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SsUsedPartsStorage);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SsUsedPartsStorages");
        }
    }
}
