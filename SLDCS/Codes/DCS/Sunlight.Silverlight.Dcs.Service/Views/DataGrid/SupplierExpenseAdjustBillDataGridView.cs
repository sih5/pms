﻿
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class SupplierExpenseAdjustBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "ExpenseAdjustmentBill_Status", "ClaimBill_SettlementStatus", "ExpenseAdjustmentBill_TransactionCategory", "ExpenseAdjustmentBill_SourceType","ExpenseAdjustmentBill_DebitOrReplenish","ServiceProductLineView_ProductLineType"
        };

        public SupplierExpenseAdjustBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Code"
                    },  new KeyValuesColumnItem {
                        Name = "SettlementStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new KeyValuesColumnItem {
                        Name = "TransactionCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    },new ColumnItem {
                        Name = "ServiceProductLine.Name",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_RepairClaimApplication_ServiceProductLine
                    },new KeyValuesColumnItem  {
                       Name="ProductLineType",
                        KeyValueItems=this.KeyValueManager[this.kvNames[5]],
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsClaimSettlementBill_ProductLineType
                    },new ColumnItem {
                        Name = "SupplierCode"
                    },new ColumnItem {
                        Name = "SupplierName"
                    },new KeyValuesColumnItem {
                        Name = "SourceType",
                        KeyValueItems = this.KeyValueManager[kvNames[3]]
                    },new ColumnItem {
                        Name = "SourceCode"
                    },new KeyValuesColumnItem {
                        Name = "DebitOrReplenish",
                        KeyValueItems = this.KeyValueManager[this.kvNames[4]]
                    }, new ColumnItem {
                        Name = "TransactionAmount"
                    },new ColumnItem {
                        Name = "TransactionReason"
                    },new ColumnItem {
                        Name = "SupplierContactPerson"
                    },new ColumnItem {
                        Name = "SupplierPhoneNumber"
                    },new ColumnItem {
                        Name = "SupplierAddress"
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_Common_BranchName
                    },new ColumnItem{
                        Name="PartsSalesCategory.Name",
                        Title=ServiceUIStrings.DataGridView_ColumnItem_Title_ServiceTripClaimApplication_PartsSalesCategory
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierExpenseAdjustBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierExpenseAdjustBillWithPartsSalesCategory";
        }
    }
}
