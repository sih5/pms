﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class SupplierExpenseAdjustBillForImportDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "ExpenseAdjustmentBill_Status", "ClaimBill_SettlementStatus", "ExpenseAdjustmentBill_TransactionCategory", "ExpenseAdjustmentBill_SourceType","ExpenseAdjustmentBill_DebitOrReplenish","ServiceProductLineView_ProductLineType"
        };
        private readonly ObservableCollection<KeyValuePair> kvPartsSalesCategories = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvServiceProductLines = new ObservableCollection<KeyValuePair>();

        public SupplierExpenseAdjustBillForImportDataGridView() {
            this.KeyValueManager.Register(kvNames);
            var domainContext = this.DomainContext as DcsDomainContext ?? new DcsDomainContext();
            domainContext.Load(domainContext.GetPartsSalesCategoriesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities != null) {
                    this.kvPartsSalesCategories.Clear();
                    foreach(var partsSalesCategory in loadOp.Entities) {
                        this.kvPartsSalesCategories.Add(new KeyValuePair {
                            Key = partsSalesCategory.Id,
                            Value = partsSalesCategory.Name
                        });
                    }
                }
            }, null);
            domainContext.Load(domainContext.GetServiceProductLineViewsQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                if(loadOp.Entities != null) {
                    this.kvServiceProductLines.Clear();
                    foreach(var serviceProductLine in loadOp.Entities) {
                        this.kvServiceProductLines.Add(new KeyValuePair {
                            Key = serviceProductLine.ProductLineId,
                            Value = serviceProductLine.ProductLineName
                        });
                    }
                }
            }, null);
        }

        protected override void OnControlsCreated() {
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            base.OnControlsCreated();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new KeyValuesColumnItem {
                        Name = "TransactionCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    },  new KeyValuesColumnItem{
                        Name = "ServiceProductLineId",
                          Title = ServiceUIStrings.DataGridView_ColumnItem_Title_RepairClaimApplication_ServiceProductLine,
                        KeyValueItems = this.kvServiceProductLines
                    },new KeyValuesColumnItem  {
                       Name="ProductLineType",
                        KeyValueItems=this.KeyValueManager[this.kvNames[5]],
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsClaimSettlementBill_ProductLineType
                    },new ColumnItem {
                        Name = "SupplierName"
                    },new ColumnItem {
                        Name = "SupplierContactPerson"
                    },new ColumnItem {
                        Name = "SupplierPhoneNumber"
                    },new ColumnItem {
                        Name = "SupplierAddress"
                    },new KeyValuesColumnItem {
                        Name = "DebitOrReplenish",
                        KeyValueItems = this.KeyValueManager[this.kvNames[4]]
                    },new KeyValuesColumnItem {
                        Name = "SourceType",
                        KeyValueItems = this.KeyValueManager[kvNames[3]]
                    },new ColumnItem {
                        Name = "SourceCode"
                    }, new ColumnItem {
                        Name = "TransactionAmount"
                    },new ColumnItem {
                        Name = "IfClaimToResponsible",
                        Title = ServiceUIStrings.DataEditPanel_Text_ExpenseAdjustmentBill_IfClaimToResponsible
                    },new ColumnItem {
                        Name = "TransactionReason"
                    },new ColumnItem {
                        Name = "Memo",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_SsClaimSettleInstruction_Remark
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_Common_BranchName
                    } ,  new KeyValuesColumnItem {
                        Name = "PartsSalesCategoryId",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_Common_PartsSalesCategory,
                        KeyValueItems = this.kvPartsSalesCategories
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SupplierExpenseAdjustBill);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetSupplierExpenseAdjustBillWithPartsSalesCategory";
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SupplierExpenseAdjustBills");
        }
    }
}
