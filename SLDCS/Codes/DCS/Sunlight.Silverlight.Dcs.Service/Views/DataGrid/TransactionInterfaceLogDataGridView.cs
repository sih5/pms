﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class TransactionInterfaceLogDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "SynchronousState"
        };

        protected override Type EntityType {
            get {
                return typeof(TransactionInterfaceLog);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        public TransactionInterfaceLogDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "SyncStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = "状态"
                    },new ColumnItem {
                        Title = "手机号码",
                        Name = "MemberPhone"
                    },new ColumnItem {
                        Title = "客户姓名",
                        Name = "MemberName"
                    },new ColumnItem {
                        Title = "VIN",
                        Name = "VIN"
                    },new ColumnItem {
                        Title = "服务站编号",
                        Name = "MinServiceCode"
                    },new ColumnItem {
                        Title = "维修单号",
                        Name = "RepairOrder.Code"
                    },new ColumnItem {
                       Name = "SyncTime",
                       Title="创建日期"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetTransactionInterfaceLogWithRepairOrder";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }
    }
}
