﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsDisposalBillWithDetailsDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "UsedPartsDisposalBill_DisposalMethod", "UsedPartsDisposalBill_Status", "UsedParts_OutboundStatus"
        };

        public UsedPartsDisposalBillWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouseCode"
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouseName"
                    }, new KeyValuesColumnItem {
                        Name = "UsedPartsDisposalMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Name = "OutboundStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ResidualValue",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "RelatedCompanyName"
                    }, new ColumnItem {
                        Name = "Supervisor"
                    }, new ColumnItem {
                        Name = "Operator"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsDisposalBill);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "UsedPartsDisposalDetail"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "旧件仓库人员查询旧件处理单";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
