﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsDisposalDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "ClaimBill_Type"
        };
        public UsedPartsDisposalDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.UsedPartsDisposalDetailDataGridView_DataContextChanged;
        }

        private void UsedPartsDisposalDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsDisposalBill = e.NewValue as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null || usedPartsDisposalBill.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "UsedPartsDisposalBillId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = usedPartsDisposalBill.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_Common_UsedPartsCode,
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_Common_UsedPartsName,
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "OutboundAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Price",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                         Name = "ClaimBillCode",
                    },new KeyValuesColumnItem {
                         Name = "ClaimBillType",
                           KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    },new ColumnItem {
                         Name = "IfFaultyParts",
                    },new ColumnItem {
                         Name = "FaultyPartsCode",
                    },new ColumnItem {
                         Name = "FaultyPartsSupplierCode",
                    },new ColumnItem {
                         Name = "FaultyPartsSupplierName",
                    },new ColumnItem {
                         Name = "ResponsibleUnitCode",
                    },new ColumnItem {
                         Name = "UsedPartsSupplierCode",
                    },new ColumnItem {
                         Name = "UsedPartsSupplierName",
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsDisposalDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsDisposalDetails";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
