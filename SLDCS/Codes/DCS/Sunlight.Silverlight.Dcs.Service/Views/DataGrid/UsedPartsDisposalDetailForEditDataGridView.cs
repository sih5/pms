﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsDisposalDetailForEditDataGridView : DcsDataGridViewBase {//: UsedPartsDisposalDetailForApproveDataGridView
        private readonly string[] kvNames = {
            "ClaimBill_Type"
        };
        public UsedPartsDisposalDetailForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        private DcsMultiPopupsQueryWindowBase usedPartsStockQueryWindow;
        private RadWindow radWindow;

        private DcsMultiPopupsQueryWindowBase UsedPartsStockQueryWindow {
            get {
                if(this.usedPartsStockQueryWindow == null) {
                    this.usedPartsStockQueryWindow = DI.GetQueryWindow("UsedPartsStockForUsedPartsTransferMulit") as DcsMultiPopupsQueryWindowBase;
                    this.usedPartsStockQueryWindow.SelectionDecided += this.UsedPartsStockQueryWindow_SelectionDecided;
                    this.usedPartsStockQueryWindow.Loaded += this.UsedPartsStockQueryWindow_Loaded;
                }
                return this.usedPartsStockQueryWindow;
            }
        }

        private void UsedPartsStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || usedPartsDisposalBill == null)
                return;
            if(usedPartsDisposalBill.UsedPartsWarehouseId != default(int))
                this.usedPartsStockQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("UsedPartsWarehouseId", typeof(int), FilterOperator.IsEqualTo, usedPartsDisposalBill.UsedPartsWarehouseId));
            this.usedPartsStockQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                    "Common", "UsedPartsWarehouse.Name", usedPartsDisposalBill.UsedPartsWarehouseName
                });
        }

        private void UsedPartsStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var usedPartsStocks = queryWindow.SelectedEntities.Cast<UsedPartsStock>();
            if(usedPartsStocks == null && usedPartsStocks.Count() == 0)
                return;
            var partsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(partsDisposalBill == null)
                return;
            //var parent = queryWindow.ParentOfType<RadWindow>();
            if(partsDisposalBill.UsedPartsDisposalDetails.Any(entity => usedPartsStocks.Any(stock => stock.UsedPartsBarCode == entity.UsedPartsBarCode))) {
                UIHelper.ShowNotification(ServiceUIStrings.Validation_Common_UsedPartsBarCodeCanNotRepeat);
                //if(parent != null)
                //    parent.Close();
                return;
            }
            foreach(var usedPartsStock in usedPartsStocks) {
                partsDisposalBill.UsedPartsDisposalDetails.Add(new UsedPartsDisposalDetail {
                    UsedPartsId = usedPartsStock.UsedPartsId,
                    UsedPartsBarCode = usedPartsStock.UsedPartsBarCode,
                    UsedPartsName = usedPartsStock.UsedPartsName,
                    UsedPartsCode = usedPartsStock.UsedPartsCode,
                    //TODO:需要设计确认，与设计中的数量校验有冲突
                    Price = usedPartsStock.SettlementPrice.HasValue ? usedPartsStock.SettlementPrice.Value : 0,
                    UsedPartsBatchNumber = usedPartsStock.UsedPartsBatchNumber,
                    UsedPartsSerialNumber = usedPartsStock.UsedPartsSerialNumber,
                    PlannedAmount = 1,
                    ConfirmedAmount = 0,
                    OutboundAmount = 0,
                    ClaimBillId = (int)usedPartsStock.ClaimBillId,
                    ClaimBillCode = usedPartsStock.ClaimBillCode,
                    ClaimBillType = (int)usedPartsStock.ClaimBillType,
                    IfFaultyParts = usedPartsStock.IfFaultyParts,
                    FaultyPartsId = usedPartsStock.FaultyPartsId,
                    FaultyPartsCode = usedPartsStock.FaultyPartsCode,
                    FaultyPartsName = usedPartsStock.FaultyPartsName,
                    FaultyPartsSupplierId = usedPartsStock.FaultyPartsSupplierId,
                    FaultyPartsSupplierCode = usedPartsStock.FaultyPartsSupplierCode,
                    FaultyPartsSupplierName = usedPartsStock.FaultyPartsSupplierName,
                    ResponsibleUnitId = usedPartsStock.ResponsibleUnitId,
                    ResponsibleUnitCode = usedPartsStock.ResponsibleUnitCode,
                    ResponsibleUnitName = usedPartsStock.ResponsibleUnitName,
                    UsedPartsSupplierId = (int)usedPartsStock.UsedPartsSupplierId,
                    UsedPartsSupplierCode = usedPartsStock.UsedPartsSupplierCode,
                    UsedPartsSupplierName = usedPartsStock.UsedPartsSupplierName,
                    BranchId = usedPartsStock.BranchId,
                    SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<UsedPartsDisposalDetail>().Max(entity => entity.SerialNumber) + 1 : 1
                });
            }
            partsDisposalBill.TotalAmount = partsDisposalBill.UsedPartsDisposalDetails.Sum(entity => entity.Price * entity.PlannedAmount);
            //if(parent != null)
            //    parent.Close();
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            var serialNumber = 1;
            foreach(var usedPartsDisposalDetail in usedPartsDisposalBill.UsedPartsDisposalDetails.Where(entity => !e.Items.Contains(entity))) {
                usedPartsDisposalDetail.SerialNumber = serialNumber++;
            }
            usedPartsDisposalBill.TotalAmount = usedPartsDisposalBill.UsedPartsDisposalDetails.Sum(entity => entity.Price * entity.PlannedAmount);
        }
        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            if(usedPartsDisposalBill == null)
                return;
            usedPartsDisposalBill.TotalAmount = usedPartsDisposalBill.UsedPartsDisposalDetails.Sum(entity => entity.Price * entity.PlannedAmount);
        }
        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var usedPartsDisposalBill = this.DataContext as UsedPartsDisposalBill;
            e.Cancel = true;
            if(usedPartsDisposalBill == null)
                return;
            if(string.IsNullOrEmpty(usedPartsDisposalBill.UsedPartsWarehouseName)) {
                UIHelper.ShowNotification(ServiceUIStrings.Notification_WarehouseNameIsNull);
            } else {
                SelectedWindow.ShowDialog();
            }
        }

        private RadWindow SelectedWindow {
            get {
                if(radWindow == null) {
                    radWindow = new RadWindow();
                    radWindow.Header = ServiceUIStrings.QueryPanel_Title_UsedPartsStock;
                    radWindow.Content = this.UsedPartsStockQueryWindow;
                    radWindow.WindowState = WindowState.Normal;
                    radWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;
                }
                return radWindow;
            }
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Price",
                        IsReadOnly = true
                    },new ColumnItem {
                         Name = "ClaimBillCode",
                         IsReadOnly = true
                    },new KeyValuesColumnItem {
                         Name = "ClaimBillType",
                          KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                         IsReadOnly = true
                    },new ColumnItem {
                         Name = "IfFaultyParts",
                         IsReadOnly = true
                    },new ColumnItem {
                         Name = "FaultyPartsCode",
                         IsReadOnly = true
                    },new ColumnItem {
                         Name = "FaultyPartsSupplierCode",
                         IsReadOnly = true
                    },new ColumnItem {
                         Name = "FaultyPartsSupplierName",
                         IsReadOnly = true
                    },new ColumnItem {
                         Name = "ResponsibleUnitCode",
                         IsReadOnly = true
                    },new ColumnItem {
                         Name = "UsedPartsSupplierCode",
                         IsReadOnly = true
                    },new ColumnItem {
                         Name = "UsedPartsSupplierName",
                         IsReadOnly = true
                    }
                };
            }
        }
        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsDisposalDetails");
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsDisposalDetail);
            }
        }
        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }
        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.Deleting -= this.GridView_Deleting;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.CellEditEnded -= GridView_CellEditEnded;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
        }
    }
}
