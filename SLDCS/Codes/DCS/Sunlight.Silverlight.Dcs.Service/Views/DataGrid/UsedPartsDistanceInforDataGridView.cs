﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsDistanceInforDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "UsedParts_DistanceType","BaseData_Status"
        };

        public UsedPartsDistanceInforDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "Company.Name",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_CompanyName
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouse.Name",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_UsedPartsWarehouseName
                    },new KeyValuesColumnItem{
                        Name = "UsedPartsDistanceType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Company1.Code",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_Company1Code
                    }, new ColumnItem {
                        Name = "Company1.Name",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_Company1Name
                    },new ColumnItem {
                        Name = "UsedPartsWarehouse1.Code",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor1_UsedPartsWarehouseCode
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouse1.Name",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor1_UsedPartsWarehouseName
                    }, new ColumnItem {
                        Name = "ShippingDistance"
                    }, new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsDistanceInfor);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsDistanceInforWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
        }
    }
}
