﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsDistanceInforForImportDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    },new ColumnItem {
                        Name = "Company.Name",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_CompanyName
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouse.Name",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_UsedPartsWarehouseName
                    },new ColumnItem{
                        Name = "UsedPartsDistanceType"
                    },new ColumnItem {
                        Name = "Company1.Code",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_Company1Code
                    }, new ColumnItem {
                        Name = "Company1.Name",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor_Company1Name
                    },new ColumnItem {
                        Name = "UsedPartsWarehouse1.Code",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor1_UsedPartsWarehouseCode
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouse1.Name",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsDistanceInfor1_UsedPartsWarehouseName
                    }, new ColumnItem {
                        Name = "ShippingDistance"
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsDistanceInfor);
            }
        }
    }
}