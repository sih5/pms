﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsInboundDetailDataGridView : DcsDataGridViewBase {

        public UsedPartsInboundDetailDataGridView() {
            this.DataContextChanged += this.UsedPartsInboundDetailDataGridView_DataContextChanged;
        }

        private void UsedPartsInboundDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsInboundOrder = e.NewValue as VirtualUsedPartsInboundOrder;
            if(usedPartsInboundOrder == null || usedPartsInboundOrder.Id == default(int))
                return;
            if(this.FilterItem == null) {
                this.FilterItem = new FilterItem {
                    MemberName = "UsedPartsInboundOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
                this.FilterItem.Value = usedPartsInboundOrder.Id;
                this.ExecuteQueryDelayed();
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Name = "ClaimBillCode",
                        Title = "索赔单编号"
                    }, new ColumnItem {
                        Name = "UsedPartsCode",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_UsedPartsCode
                    }, new ColumnItem {
                        Name = "UsedPartsName",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_UsedPartsName
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouseAreaCode",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_UsedPartsWarehouseAreaCode
                    }, new ColumnItem {
                        Name = "Quantity",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "CostPrice",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "UsedPartsEncourageAmount",
                        Title = "旧件激励金额",
                          TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "UsedPartsBatchNumber"
                    }, new ColumnItem {
                        Name = "UsedPartsSerialNumber"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsInboundDetails";
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsInboundDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["SettlementPrice"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["UsedPartsEncourageAmount"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["CostPrice"]).DataFormatString = "N2";
        }
    }
}
