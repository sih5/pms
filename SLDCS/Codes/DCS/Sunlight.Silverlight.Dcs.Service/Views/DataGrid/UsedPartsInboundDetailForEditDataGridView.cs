﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsInboundDetailForEditDataGridView : DcsDataGridViewBase {

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var usedPartsInboundOrder = this.DataContext as UsedPartsInboundOrder;
            if(usedPartsInboundOrder == null)
                return;
            var serialNumber = 1;
            foreach(var ssUsedPartsDisposalDetail in usedPartsInboundOrder.UsedPartsInboundDetails.Where(entity => !e.Items.Contains(entity)))
                ssUsedPartsDisposalDetail.SerialNumber = serialNumber++;
        }

        private QueryWindowBase usedPartsWarehouseAreaQueryWindow;
        private QueryWindowBase UsedPartsWarehouseAreaQueryWindow {
            get {
                if(this.usedPartsWarehouseAreaQueryWindow == null) {
                    this.usedPartsWarehouseAreaQueryWindow = DI.GetQueryWindow("UsedPartsWarehouseArea2DropDown");
                    this.usedPartsWarehouseAreaQueryWindow.Loaded += usedPartsWarehouseAreaQueryWindow_Loaded;
                    this.usedPartsWarehouseAreaQueryWindow.SelectionDecided += this.usedPartsWarehouseAreaDropDownQueryWindow_SelectionDecided;
                }
                return this.usedPartsWarehouseAreaQueryWindow;
            }
        }

        private void usedPartsWarehouseAreaQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null)
                return;
            var usedPartsInboundOrder = this.DataContext as UsedPartsInboundOrder;
            if(usedPartsInboundOrder == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "UsedPartsWarehouse.Name", usedPartsInboundOrder.UsedPartsWarehouseName
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common","UsedPartsWarehouse.Name",false
            });
        }

        private void usedPartsWarehouseAreaDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var usedPartsWarehouseArea = queryWindow.SelectedEntities.Cast<UsedPartsWarehouseArea>().FirstOrDefault();
            if(usedPartsWarehouseArea == null)
                return;
            var usedPartsInboundOrder = this.DataContext as UsedPartsInboundOrder;
            if(usedPartsInboundOrder == null)
                return;
            foreach(var detail in usedPartsInboundOrder.UsedPartsInboundDetails) {
                detail.UsedPartsWarehouseAreaId = usedPartsWarehouseArea.Id;
                detail.UsedPartsWarehouseAreaCode = usedPartsWarehouseArea.Code;
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }


        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsInboundDetails");
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsCode",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_UsedPartsCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsName",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_UsedPartsName,
                        IsReadOnly = true
                    }, new DropDownTextBoxColumnItem {
                        Name = "UsedPartsWarehouseAreaCode",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_UsedPartsWarehouseAreaCode,
                        DropDownContent = this.UsedPartsWarehouseAreaQueryWindow
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Quantity",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_Quantity,
                          IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    },  new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_IfFaultyParts,
                         Name = "IfFaultyParts",
                          IsReadOnly = true
                     },  new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierCode,
                         Name = "UsedPartsSupplierCode",
                          IsReadOnly = true
                     },  new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierName,
                         Name = "UsedPartsSupplierName",
                          IsReadOnly = true
                     }, new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsCode,
                         Name = "FaultyPartsCode",
                          IsReadOnly = true
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsName,
                         Name = "FaultyPartsName",
                          IsReadOnly = true
                     },   new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierCode,
                         Name = "FaultyPartsSupplierCode",
                          IsReadOnly = true
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierName,
                         Name = "FaultyPartsSupplierName",
                          IsReadOnly = true
                     } 
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsInboundDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.Deleting -= this.GridView_Deleting;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.DataLoaded += GridView_DataLoaded;
        }

        private void GridView_DataLoaded(object sender, EventArgs e) {
            var usedPartsInboundOrder = this.DataContext as UsedPartsInboundOrder;
            if(usedPartsInboundOrder == null)
                return;
            this.GridView.Columns["Quantity"].IsReadOnly = usedPartsInboundOrder.InboundType != (int)DcsUsedPartsInboundOrderInboundType.服务站返件入库;
        }
    }
}