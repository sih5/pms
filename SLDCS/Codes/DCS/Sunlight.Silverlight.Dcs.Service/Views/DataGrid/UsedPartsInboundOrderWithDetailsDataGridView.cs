﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsInboundOrderWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "UsedPartsInboundOrder_InboundType","ClaimBill_SettlementStatus"
        };

        public UsedPartsInboundOrderWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "Code")
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouseCode",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundOrder_UsedPartsWarehouseCode
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouseName",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundOrder_UsedPartsWarehouseName
                    }, new KeyValuesColumnItem {
                        Name = "InboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "InboundType")
                    }, new ColumnItem {
                        Name = "SourceCode",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "SourceCode")
                    }, new ColumnItem {
                        Name = "RelatedCompanyCode",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundOrder_RelatedCompanyCode
                    }, new ColumnItem {
                        Name = "RelatedCompanyName",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundOrder_RelatedCompanyName
                    },new ColumnItem {
                        Name = "Businesscode",
                        Title = "业务编码"
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        TextAlignment=TextAlignment.Right,
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "TotalAmount")
                    }, new ColumnItem {
                        Name = "TotalQuantity",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "TotalQuantity")
                    },new ColumnItem {
                        Name = "UsedPartsEncourageAmount",
                        Title = "旧件激励金额",
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Operator",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "Operator")
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "Remark")
                    },new KeyValuesColumnItem {
                        Name = "SettlementStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "SettlementStatus")
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "CreatorName")
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsInboundOrder), "CreateTime")
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "查询旧件入库单";
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualUsedPartsInboundOrder);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "UsedPartsInboundDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 500;  //每页改成显示500条
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["TotalAmount"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["UsedPartsEncourageAmount"]).DataFormatString = "N2";

        }
    }
}
