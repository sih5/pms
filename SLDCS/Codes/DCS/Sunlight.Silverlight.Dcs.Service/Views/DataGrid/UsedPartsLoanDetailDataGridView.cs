﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsLoanDetailDataGridView : DcsDataGridViewBase {

        public UsedPartsLoanDetailDataGridView() {
            this.DataContextChanged += this.UsedPartsLoanDetailDataGridView_DataContextChanged;
        }

        private void UsedPartsLoanDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsLoanBill = e.NewValue as UsedPartsLoanBill;
            if(usedPartsLoanBill == null || usedPartsLoanBill.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "UsedPartsLoanBillId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = usedPartsLoanBill.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_Common_UsedPartsCode,
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_Common_UsedPartsName,
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "OutboundAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "InboundAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Price",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ClaimBillCode"
                    }, new ColumnItem {
                        Name = "ClaimBillType"
                    }, new ColumnItem {
                        Name = "IfFaultyParts"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierName"
                    }, new ColumnItem {
                        Name = "ResponsibleUnitCode"
                    },new ColumnItem{
                        Name="FaultyPartsCode"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsLoanDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsLoanDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
