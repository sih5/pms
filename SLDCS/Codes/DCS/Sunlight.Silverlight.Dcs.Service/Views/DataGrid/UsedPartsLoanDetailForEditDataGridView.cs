﻿using System;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsLoanDetailForEditDataGridView : UsedPartsLoanDetailForEditForQueryDataGridView {
        private DcsMultiPopupsQueryWindowBase usedPartsStockQueryWindow;
        private RadWindow radWindow;

        private DcsMultiPopupsQueryWindowBase UsedPartsStockQueryWindow {
            get {
                if(this.usedPartsStockQueryWindow == null) {
                    this.usedPartsStockQueryWindow = DI.GetQueryWindow("UsedPartsStockForUsedPartsTransferMulit") as DcsMultiPopupsQueryWindowBase;
                    this.usedPartsStockQueryWindow.SelectionDecided += this.UsedPartsStockQueryWindow_SelectionDecided;
                    this.usedPartsStockQueryWindow.Loaded += this.UsedPartsStockQueryWindow_Loaded;
                }
                return this.usedPartsStockQueryWindow;
            }
        }

        private void UsedPartsStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var usedPartsLoanBill = this.DataContext as UsedPartsLoanBill;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || usedPartsLoanBill == null)
                return;
            if(usedPartsLoanBill.UsedPartsWarehouseId != 0)
                this.usedPartsStockQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("UsedPartsWarehouseId", typeof(int), FilterOperator.IsEqualTo, usedPartsLoanBill.UsedPartsWarehouseId));
            this.usedPartsStockQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                    "Common", "UsedPartsWarehouse.Name", usedPartsLoanBill.UsedPartsWarehouseName
                });
        }

        private void UsedPartsStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var usedPartsStocks = queryWindow.SelectedEntities.Cast<UsedPartsStock>();
            if(usedPartsStocks == null && usedPartsStocks.Count() == 0)
                return;
            var usedPartsLoanBill = this.DataContext as UsedPartsLoanBill;
            if(usedPartsLoanBill == null)
                return;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(usedPartsLoanBill.UsedPartsLoanDetails.Any(entity => usedPartsStocks.Any(stock => stock.UsedPartsBarCode == entity.UsedPartsBarCode))) {
                UIHelper.ShowNotification(ServiceUIStrings.Validation_Common_UsedPartsBarCodeCanNotRepeat);
                if(parent != null)
                    parent.Close();
                return;
            }
            foreach(var usedPartsStock in usedPartsStocks) {
                usedPartsLoanBill.UsedPartsLoanDetails.Add(new UsedPartsLoanDetail {
                    UsedPartsId = usedPartsStock.UsedPartsId,
                    UsedPartsBarCode = usedPartsStock.UsedPartsBarCode,
                    UsedPartsName = usedPartsStock.UsedPartsName,
                    UsedPartsCode = usedPartsStock.UsedPartsCode,
                    //TODO:需要设计确认，与设计中的数量校验有冲突
                    Price = usedPartsStock.SettlementPrice,
                    UsedPartsBatchNumber = usedPartsStock.UsedPartsBatchNumber,
                    UsedPartsSerialNumber = usedPartsStock.UsedPartsSerialNumber,
                    PlannedAmount = 1,
                    ConfirmedAmount = 0,
                    OutboundAmount = 0,
                    InboundAmount = 0,
                    BranchId = usedPartsStock.BranchId,
                    ClaimBillCode = usedPartsStock.ClaimBillCode,
                    ClaimBillType = (int)usedPartsStock.ClaimBillType,
                    IfFaultyParts = usedPartsStock.IfFaultyParts,
                    FaultyPartsSupplierCode = usedPartsStock.FaultyPartsSupplierCode,
                    FaultyPartsSupplierName = usedPartsStock.FaultyPartsSupplierName,
                    FaultyPartsId = usedPartsStock.FaultyPartsId,
                    FaultyPartsCode = usedPartsStock.FaultyPartsCode,
                    FaultyPartsName = usedPartsStock.FaultyPartsName,
                    ResponsibleUnitCode = usedPartsStock.ResponsibleUnitCode,
                    UsedPartsSupplierCode = usedPartsStock.UsedPartsSupplierCode,
                    UsedPartsSupplierName = usedPartsStock.UsedPartsSupplierName,
                    SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<UsedPartsLoanDetail>().Max(entity => entity.SerialNumber) + 1 : 1
                });
            }
            if(parent != null)
                parent.Close();
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var usedPartsLoanBill = this.DataContext as UsedPartsLoanBill;
            if(usedPartsLoanBill == null)
                return;
            var serialNumber = 1;
            foreach(var usedPartsLoanDetails in usedPartsLoanBill.UsedPartsLoanDetails.Where(entity => !e.Items.Contains(entity)))
                usedPartsLoanDetails.SerialNumber = serialNumber++;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var usedPartsLoanBill = this.DataContext as UsedPartsLoanBill;
            if(usedPartsLoanBill == null)
                return;
            e.Cancel = true;
            if(string.IsNullOrEmpty(usedPartsLoanBill.UsedPartsWarehouseName)) {
                UIHelper.ShowNotification(ServiceUIStrings.Notification_WarehouseNameIsNull);
            } else {
                SelectedWindow.ShowDialog();
            }
        }

        private RadWindow SelectedWindow {
            get {
                if(radWindow == null) {
                    radWindow = new RadWindow();
                    radWindow.Header = ServiceUIStrings.QueryPanel_Title_UsedPartsStock;
                    radWindow.Content = this.UsedPartsStockQueryWindow;
                    radWindow.WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterOwner;
                }
                return radWindow;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            //this.GridView.Columns["ConfirmedAmount"].IsReadOnly = true;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.Deleting -= this.GridView_Deleting;
            this.GridView.Deleting += this.GridView_Deleting;
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }
    }
}
