﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsLoanDetailForEditForQueryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "ClaimBill_Type"
        };

        public UsedPartsLoanDetailForEditForQueryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Price",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ClaimBillCode",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name="ClaimBillType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="IfFaultyParts",
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="FaultyPartsCode"
                    },new ColumnItem{
                        Name="FaultyPartsSupplierCode",
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="FaultyPartsSupplierName",
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="ResponsibleUnitCode",
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="UsedPartsSupplierCode",
                        IsReadOnly=true
                    },new ColumnItem{
                        Name="UsedPartsSupplierName",
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsLoanDetails");
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsLoanDetail);
            }
        }
    }
}

