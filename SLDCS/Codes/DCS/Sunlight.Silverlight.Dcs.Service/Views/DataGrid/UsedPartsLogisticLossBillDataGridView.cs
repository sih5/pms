﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsLogisticLossBillDataGridView : DcsDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(UsedPartsLogisticLossBill);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "DealerCode",
                        IsDefaultGroup = true,
                        ShowColumnWhenGrouped =true
                    }, new ColumnItem {
                        Name = "DealerName"
                    }, new ColumnItem {
                        Name = "UsedPartsShippingOrder.Code"
                    }, new ColumnItem {
                        Name = "LogisticCompanyCode"
                    }, new ColumnItem {
                        Name = "LogisticCompanyName"
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        FormatString = "c2",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "旧件仓库人员查询物流损失单";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
