﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsLogisticLossDetailForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "UsedPartsLogisticLossDetail_ProcessStatus"
        };

        public UsedPartsLogisticLossDetailForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var usedPartsLogisticLossDetail = (UsedPartsLogisticLossDetail)e.Cell.DataContext;
            if(e.Cell.Column.UniqueName == "ProcessStatus") {
                var st = usedPartsLogisticLossDetails.First(v => v.Key == usedPartsLogisticLossDetail.Id).Value;
                if(st == (int)DcsUsedPartsLogisticLossDetailProcessStatus.未返回) {
                    if(usedPartsLogisticLossDetail.ProcessStatus == (int)DcsUsedPartsLogisticLossDetailProcessStatus.丢失 &&
                                (int)e.OldData == (int)DcsUsedPartsLogisticLossDetailProcessStatus.未返回 &&
                                (int)e.NewData == (int)DcsUsedPartsLogisticLossDetailProcessStatus.丢失) {
                        usedPartsLogisticLossDetail.ProcessStatus = (int)e.OldData;// (int)DcsUsedPartsLogisticLossDetailProcessStatus.未返回;
                        UIHelper.ShowNotification(ServiceUIStrings.DataEditView_Validation_UsedPartsLogisticLossDetail_ProcessStatusNotAllowedToModify);
                        return;
                    }
                }
                if(!this.GridView.SelectedItems.Contains(usedPartsLogisticLossDetail))
                    return;
                foreach(var selectItem in this.GridView.SelectedItems.Cast<UsedPartsLogisticLossDetail>())
                    selectItem.ProcessStatus = usedPartsLogisticLossDetail.ProcessStatus;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.DataLoaded += GridView_DataLoaded;
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            ((GridViewDataColumn)this.GridView.Columns["PartsManagementCost"]).DataFormatString = "c2";

        }
        private Dictionary<int, int> usedPartsLogisticLossDetails = new Dictionary<int, int>();
        void GridView_DataLoaded(object sender, EventArgs e) {
            if(this.GridView.Items.Count > 0) {
                usedPartsLogisticLossDetails.Clear();
                foreach(var entity in this.GridView.Items.Cast<UsedPartsLogisticLossDetail>())
                    usedPartsLogisticLossDetails.Add(entity.Id, entity.ProcessStatus);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsLogisticLossDetails");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "ProcessStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },  new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ClaimBillCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsLogisticLossDetail_UsedPartsCode,
                        Name = "UsedPartsCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsLogisticLossDetail_UsedPartsName,
                        Name = "UsedPartsName",
                        IsReadOnly = true  
                    },new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsLogisticLossDetail_Price,
                        Name = "Price",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PartsManagementCost",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsLogisticLossDetail);
            }
        }
    }
}
