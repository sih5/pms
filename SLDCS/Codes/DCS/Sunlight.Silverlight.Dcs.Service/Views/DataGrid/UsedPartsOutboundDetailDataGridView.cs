﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsOutboundDetailDataGridView : DcsDataGridViewBase {

        private readonly string[] kvNames = { "ClaimBillType" };
        public UsedPartsOutboundDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.UsedPartsOutboundDetailDataGridView_DataContextChanged;
        }

        private void UsedPartsOutboundDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsOutboundOrder = e.NewValue as UsedPartsOutboundOrder;
            if(usedPartsOutboundOrder == null || usedPartsOutboundOrder.Id == default(int))
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "UsedPartsOutboundOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = usedPartsOutboundOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsOutboundDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsCode,
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsName,
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsWarehouseAreaCode,
                        Name = "UsedPartsWarehouseAreaCode"
                    }, new ColumnItem {
                        Name = "Quantity",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "CostPrice",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_ClaimBillCode,
                        Name ="ClaimBillCode"
                    },new KeyValuesColumnItem {                         
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_ClaimBillType,
                         Name="ClaimBillType",
                         KeyValueItems =this.KeyValueManager[kvNames[0]]    
                    },  new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_IfFaultyParts,
                         Name = "IfFaultyParts"
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsCode,
                         Name = "FaultyPartsCode"
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierCode,
                         Name = "FaultyPartsSupplierCode"
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierName,
                         Name = "FaultyPartsSupplierName"
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_ResponsibleUnitCode,
                         Name = "ResponsibleUnitCode"
                     }, new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierCode,
                         Name = "UsedPartsSupplierCode"
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierName,
                         Name = "UsedPartsSupplierName"
                     }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsOutboundDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
