﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsOutboundDetailForEditDataGridView : DcsDataGridViewBase {

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var usedPartsOutboundOrder = this.DataContext as UsedPartsOutboundOrder;
            if(usedPartsOutboundOrder == null)
                return;
            var serialNumber = 1;
            foreach(var usedPartsOutboundDetail in usedPartsOutboundOrder.UsedPartsOutboundDetails.Where(entity => !e.Items.Contains(entity)))
                usedPartsOutboundDetail.SerialNumber = serialNumber++;
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.CanUserInsertRows = false;
            this.GridView.Deleting -= this.GridView_Deleting;
            this.GridView.Deleting += this.GridView_Deleting;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsCode,
                        Name = "UsedPartsCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsName,
                        Name = "UsedPartsName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsWarehouseAreaCode,
                        Name = "UsedPartsWarehouseAreaCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PlannedAmount",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "ConfirmedAmount",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_OutboundAmount,
                        Name = "Quantity",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_IfFaultyParts,
                         Name = "IfFaultyParts",
                          IsReadOnly = true
                     },  new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierCode,
                         Name = "UsedPartsSupplierCode",
                          IsReadOnly = true
                     },  new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierName,
                         Name = "UsedPartsSupplierName",
                          IsReadOnly = true
                     }, new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsCode,
                         Name = "FaultyPartsCode",
                          IsReadOnly = true
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsName,
                         Name = "FaultyPartsName",
                          IsReadOnly = true
                     },   new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierCode,
                         Name = "FaultyPartsSupplierCode",
                          IsReadOnly = true
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierName,
                         Name = "FaultyPartsSupplierName",
                          IsReadOnly = true
                     } 
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsOutboundDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsOutboundDetails");
        }
    }
}
