﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView; 
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsOutboundOrderWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "UsedPartsOutboundOrder_OutboundType"
        };

        public UsedPartsOutboundOrderWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsOutboundOrder);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundOrder_UsedPartsWarehouseCode,
                        Name = "UsedPartsWarehouseCode"
                    }, new ColumnItem {
                        Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundOrder_UsedPartsWarehouseName,
                        Name = "UsedPartsWarehouseName"
                    }, new KeyValuesColumnItem {
                        Name = "OutboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                       Name = "SourceCode"
                    }, new ColumnItem {
                        Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundOrder_RelatedCompanyCode,
                        Name = "RelatedCompanyCode"
                    }, new ColumnItem {
                        Title =  "业务编号",
                        Name = "BusinessCode"
                    }, new ColumnItem {
                        Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundOrder_RelatedCompanyName,
                        Name = "RelatedCompanyName"
                    },  new ColumnItem {
                        Name = "TotalAmount",
                        Title="总金额"
                    }, new ColumnItem {
                        Name = "TotalQuantity",
                        Title="旧件总数量"
                    }, new ColumnItem {
                        Name = "Operator"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "IfBillable"
                    }, new ColumnItem {
                        Name = "IfAlreadySettled"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "旧件仓库人员查询旧件出库单";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "UsedPartsOutboundDetail"
                    }
                };
            }
        }


        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            var composites = filters.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).Cast<CompositeFilterItem>().ToArray();
            if(filters != null) {
                switch(parameterName) {
                    case "usedPartsBarCode":
                        return filters.Filters.Single(item => item.MemberName == "UsedPartsBarCode").Value;
                    case "claimBillCode":
                        return filters.Filters.Single(item => item.MemberName == "ClaimBillCode").Value;
                    case "businessCode":
                        return filters.Filters.Single(item => item.MemberName == "BusinessCode").Value;
                    case "relatedCompanyCode":
                        return filters.Filters.Single(item => item.MemberName == "RelatedCompanyCode").Value;
                    case "relatedCompanyName":
                        return filters.Filters.Single(item => item.MemberName == "RelatedCompanyName").Value;
                    case "beginCreateTime":
                        var beginCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return beginCreateTime == null ? null : beginCreateTime.Filters.First(r => r.MemberName == "CreateTime").Value;
                    case "endCreateTime":
                        var endCreateTime = composites.FirstOrDefault(p => p.Filters.First().MemberName == "CreateTime");
                        return endCreateTime == null ? null : endCreateTime.Filters.Last(r => r.MemberName == "CreateTime").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "RelatedCompanyName" && filter.MemberName != "RelatedCompanyCode" && filter.MemberName != "UsedPartsBarCode" && filter.MemberName != "ClaimBillCode" && filter.MemberName != "BusinessCode"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 500;  //旧件出库单查询每页改成显示500条
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
