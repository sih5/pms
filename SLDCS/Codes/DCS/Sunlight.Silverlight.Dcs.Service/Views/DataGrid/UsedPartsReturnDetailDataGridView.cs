﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsReturnDetailDataGridView : DcsDataGridViewBase {

        public UsedPartsReturnDetailDataGridView() {
            this.DataContextChanged += this.UsedPartsReturnDetailDataGridViewDataContextChanged;
            this.KeyValueManager.Register(kvNames);
        }

        private readonly string[] kvNames = { "ClaimBill_Type" };

        private void UsedPartsReturnDetailDataGridViewDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedpartsreturnorder = e.NewValue as UsedPartsReturnOrder;
            if(usedpartsreturnorder == null || usedpartsreturnorder.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "UsedPartsReturnOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = usedpartsreturnorder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ClaimBillCode"
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Name = "UsedPartsCode",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsReturnDetail_UsedPartsCode
                    }, new ColumnItem {
                        Name = "UsedPartsName",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsReturnDetail_UsedPartsName
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "OutboundAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsReturnDetail_UnitPrice,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ClaimBillCode"
                    }, new KeyValuesColumnItem {
                        Name = "ClaimBillType",
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                    }, new ColumnItem {
                        Name = "IfFaultyParts",
                    }, new ColumnItem {
                        Name = "FaultyPartsCode" 
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierName"
                    }, new ColumnItem {
                        Name = "ResponsibleUnitCode"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsReturnDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsReturnDetails";
        }
    }
}