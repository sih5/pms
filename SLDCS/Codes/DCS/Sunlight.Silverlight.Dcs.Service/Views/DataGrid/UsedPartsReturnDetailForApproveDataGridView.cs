﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsReturnDetailForApproveDataGridView : DcsDataGridViewBase {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        public UsedPartsReturnDetailForApproveDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        private readonly string[] kvNames = { "ClaimBill_Type" };

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsReturnDetails");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsCode",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsReturnDetail_UsedPartsCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsName",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsReturnDetail_UsedPartsName,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right,
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsReturnDetail_UnitPrice
                    }, new ColumnItem {
                        Name = "ClaimBillCode",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "ClaimBillType",
                        IsReadOnly = true,
                        KeyValueItems = this.KeyValueManager[kvNames[0]],
                    }, new ColumnItem {
                        Name = "IfFaultyParts",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "FaultyPartsCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ResponsibleUnitCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierName",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsReturnDetail);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.DataPager.PageSize = 50;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }
    }
}
