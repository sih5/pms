﻿using System;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsReturnDetailForEditDataGridView : UsedPartsReturnDetailForApproveDataGridView {
        private DcsMultiPopupsQueryWindowBase usedPartsStockQueryWindow;
        private RadWindow radWindow;

        private DcsMultiPopupsQueryWindowBase UsedPartsStockQueryWindow {
            get {
                if(this.usedPartsStockQueryWindow == null) {
                    this.usedPartsStockQueryWindow = DI.GetQueryWindow("UsedPartsStockForUsedPartsTransferMulit") as DcsMultiPopupsQueryWindowBase;
                    this.usedPartsStockQueryWindow.SelectionDecided += this.UsedPartsStockQueryWindowSelectionDecided;
                    this.usedPartsStockQueryWindow.Loaded += this.usedPartsStockQueryWindow_Loaded;
                }
                return this.usedPartsStockQueryWindow;
            }
        }

        private void usedPartsStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || usedPartsReturnOrder == null)
                return;
            var compositeFilterItem = new CompositeFilterItem();
            if(usedPartsReturnOrder.OutboundWarehouseId != 0) {
                this.usedPartsStockQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "UsedPartsWarehouse.Code", usedPartsReturnOrder.OutboundWarehouseId });
                this.usedPartsStockQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "UsedPartsWarehouse.Name", usedPartsReturnOrder.OutboundWarehouseName });
                this.usedPartsStockQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "UsedPartsWarehouse.Code", false });
                this.usedPartsStockQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "UsedPartsWarehouse.Name", false });
                compositeFilterItem.Filters.Add(new FilterItem("UsedPartsWarehouseId", typeof(int), FilterOperator.IsEqualTo, usedPartsReturnOrder.OutboundWarehouseId));
            }
            compositeFilterItem.Filters.Add(new FilterItem("ReceptionStatus", typeof(int), FilterOperator.IsNotEqualTo, (int)DcsUsedPartsShippingDetailReceptionStatus.不合格));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", compositeFilterItem);
        }

        private void UsedPartsStockQueryWindowSelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var usedPartsStocks = queryWindow.SelectedEntities.Cast<UsedPartsStock>();
            if(usedPartsStocks == null && !usedPartsStocks.Any())
                return;
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            if(usedPartsReturnOrder.UsedPartsReturnDetails.Any(entity => usedPartsStocks.Any(stock => stock.UsedPartsBarCode == entity.UsedPartsBarCode))) {
                UIHelper.ShowNotification(ServiceUIStrings.Validation_Common_UsedPartsBarCodeCanNotRepeat);
                return;
            }
            foreach(var usedPartsStock in usedPartsStocks) {
                UsedPartsReturnDetail UsedPartsReturnDetail = new UsedPartsReturnDetail();
                //usedPartsReturnOrder.UsedPartsReturnDetails.Add(new UsedPartsReturnDetail {
                UsedPartsReturnDetail.UsedPartsId = usedPartsStock.UsedPartsId;
                UsedPartsReturnDetail.UsedPartsBarCode = usedPartsStock.UsedPartsBarCode;
                UsedPartsReturnDetail.UsedPartsCode = usedPartsStock.UsedPartsCode;
                UsedPartsReturnDetail.UsedPartsName = usedPartsStock.UsedPartsName;
                UsedPartsReturnDetail.UnitPrice = usedPartsStock.SettlementPrice.HasValue ? usedPartsStock.SettlementPrice.Value : default(decimal);
                UsedPartsReturnDetail.UsedPartsBatchNumber = usedPartsStock.UsedPartsBatchNumber;
                UsedPartsReturnDetail.UsedPartsSerialNumber = usedPartsStock.UsedPartsSerialNumber;
                UsedPartsReturnDetail.UsedPartsSupplierId = usedPartsStock.UsedPartsSupplierId.HasValue ? usedPartsStock.UsedPartsSupplierId.Value : default(int);
                UsedPartsReturnDetail.UsedPartsSupplierCode = usedPartsStock.UsedPartsSupplierCode;
                UsedPartsReturnDetail.UsedPartsSupplierName = usedPartsStock.UsedPartsSupplierName;
                UsedPartsReturnDetail.ClaimBillType = usedPartsStock.ClaimBillType ?? default(int);
                UsedPartsReturnDetail.ClaimBillId = usedPartsStock.ClaimBillId.HasValue ? usedPartsStock.ClaimBillId.Value : default(int);
                UsedPartsReturnDetail.ClaimBillCode = usedPartsStock.ClaimBillCode;
                UsedPartsReturnDetail.IfFaultyParts = usedPartsStock.IfFaultyParts;
                UsedPartsReturnDetail.FaultyPartsId = usedPartsStock.FaultyPartsId;
                UsedPartsReturnDetail.FaultyPartsCode = usedPartsStock.FaultyPartsCode;
                UsedPartsReturnDetail.FaultyPartsName = usedPartsStock.FaultyPartsName;
                UsedPartsReturnDetail.FaultyPartsSupplierId = usedPartsStock.FaultyPartsSupplierId;
                UsedPartsReturnDetail.FaultyPartsSupplierCode = usedPartsStock.FaultyPartsSupplierCode;
                UsedPartsReturnDetail.FaultyPartsSupplierName = usedPartsStock.FaultyPartsSupplierName;
                UsedPartsReturnDetail.ResponsibleUnitId = usedPartsStock.ResponsibleUnitId;
                UsedPartsReturnDetail.ResponsibleUnitCode = usedPartsStock.ResponsibleUnitCode;
                UsedPartsReturnDetail.ResponsibleUnitName = usedPartsStock.ResponsibleUnitName;
                UsedPartsReturnDetail.PlannedAmount = 1;
                UsedPartsReturnDetail.ConfirmedAmount = 0;
                UsedPartsReturnDetail.OutboundAmount = 0;
                UsedPartsReturnDetail.BranchId = usedPartsStock.BranchId.HasValue ? usedPartsStock.BranchId.Value : default(int);
                UsedPartsReturnDetail.SerialNumber = usedPartsReturnOrder.UsedPartsReturnDetails.Count() + 1;// this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<UsedPartsReturnDetail>().Max(entity => entity.SerialNumber) + 1 : 1;
                usedPartsReturnOrder.UsedPartsReturnDetails.Add(UsedPartsReturnDetail);
                //});
            }
            this.CalcTotalAmount();
            //var parent = queryWindow.ParentOfType<RadWindow>();
            //if(parent != null)
            //    parent.Close();
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            var serialNumber = 0;
            foreach(var usedPartsReturnDetail in usedPartsReturnOrder.UsedPartsReturnDetails.Where(entity => !e.Items.Contains(entity)))
                usedPartsReturnDetail.SerialNumber = ++serialNumber;
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            this.CalcTotalAmount();
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            e.Cancel = true;
            if(usedPartsReturnOrder == null)
                return;
            if(usedPartsReturnOrder.OutboundWarehouseId == default(int))
                UIHelper.ShowNotification(ServiceUIStrings.Validation_UsedPartsReturnDetail_WarehouseNameIsNull);
            else
                SelectedWindow.ShowDialog();
        }

        private RadWindow SelectedWindow {
            get {
                return this.radWindow ?? (this.radWindow = new RadWindow {
                    Header = ServiceUIStrings.QueryPanel_Title_UsedPartsStock,
                    Content = this.UsedPartsStockQueryWindow,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                });
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.DataPager.PageSize = 500;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.Deleting -= this.GridView_Deleting;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.Deleted -= GridView_Deleted;
            this.GridView.Deleted += GridView_Deleted;
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        private void CalcTotalAmount() {
            var usedPartsReturnOrder = this.DataContext as UsedPartsReturnOrder;
            if(usedPartsReturnOrder == null)
                return;
            usedPartsReturnOrder.TotalAmount = usedPartsReturnOrder.UsedPartsReturnDetails.Sum(entity => Convert.ToDecimal(entity.PlannedAmount) * entity.UnitPrice);
            usedPartsReturnOrder.TotalQuantity = usedPartsReturnOrder.UsedPartsReturnDetails.Sum(entity => entity.PlannedAmount);
        }

    }
}
