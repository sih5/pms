﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsReturnOrderDetailForExportDataGridView : DcsDataGridViewBase {

        protected override Type EntityType {
            get {
                return typeof(UsedPartsReturnDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }
                };
            }
        }
    }
}
