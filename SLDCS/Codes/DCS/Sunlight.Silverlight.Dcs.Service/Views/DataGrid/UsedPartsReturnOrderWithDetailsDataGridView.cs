﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsReturnOrderWithDetailsDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "UsedPartsReturnOrder_ReturnType", "UsedPartsReturnOrder_Status"};

        public UsedPartsReturnOrderWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsReturnOrder);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "OutboundWarehouseCode"
                    }, new ColumnItem {
                        Name = "OutboundWarehouseName"
                    }, new ColumnItem {
                        Name = "ReturnOfficeCode"
                    }, new ColumnItem {
                        Name = "ReturnOfficeName"
                    }, new KeyValuesColumnItem {
                        Name = "ReturnType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "TotalAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "TotalQuantity",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "Operator"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "UsedPartsReturnDetail"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "旧件仓库人员查询旧件清退单";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ApproveTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["AbandonTime"]).DataFormatString = "d";
        }
    }
}
