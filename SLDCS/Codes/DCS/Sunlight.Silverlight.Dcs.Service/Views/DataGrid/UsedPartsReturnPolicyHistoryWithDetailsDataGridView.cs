﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsReturnPolicyHistoryWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy"
        };

        public UsedPartsReturnPolicyHistoryWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsReturnPolicyHistory);
            }
        }
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.DataPager.PageSize = 1000;
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_Common_DealerCode,
                        Name = "Dealer.Code",
                        IsDefaultGroup = true,
                        ShowColumnWhenGrouped = false
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_Common_DealerName,
                        Name = "Dealer.Name"
                    }, new ColumnItem {
                        Name = "ClaimBillCode"
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new KeyValuesColumnItem {
                        Name = "UsedPartsReturnPolicy",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsReturnPolicyHistory_UsedPartsCode,
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsReturnPolicyHistory_UsedPartsName,
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Name = "Shipped"
                    }, new ColumnItem {
                        Name = "UnitPrice"
                    }, new ColumnItem {
                        Name = "PartsManagementCost"
                    }, new ColumnItem {
                        Name = "IfFaultyParts"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierName"
                    }, new ColumnItem {
                        Name = "FaultyPartsCode"
                    }, new ColumnItem {
                        Name = "FaultyPartsName"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierName"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem{
                        Name = "PartsSalesCategory.Name",
                        Title =ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "UsedPartsReturnPolicyHistory"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsReturnPolicyHistoriesWithDealer";
        }
    }
}
