﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsShiftDetailDataGridView : DcsDataGridViewBase {
        public UsedPartsShiftDetailDataGridView() {
            this.DataContextChanged += this.UsedPartsShiftDetailDataGridView_DataContextChanged;
        }

        private void UsedPartsShiftDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsShiftOrder = e.NewValue as UsedPartsShiftOrder;
            if(usedPartsShiftOrder == null || usedPartsShiftOrder.Id == default(int))
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "UsedPartsShiftOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = usedPartsShiftOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsShiftDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShiftDetail_UsedPartsCode,
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShiftDetail_UsedPartsName,
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShiftDetail_OriginStoragePositionCode,
                        Name = "OriginStoragePositionCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShiftDetail_DestiStoragePositionCode,
                        Name = "DestiStoragePositionCode"
                    }, new ColumnItem {
                        Name = "Quantity"
                    }
                    //, new ColumnItem {
                    //    Name = "UsedPartsSerialNumber"
                    //}, new ColumnItem {
                    //    Name = "UsedPartsBatchNumber"
                    //}
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsShiftDetails";
        }
    }
}
