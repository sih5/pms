﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsShiftDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase usedPartsStockQueryWindow;
        private QueryWindowBase usedPartsWarehouseAreaDropDownQueryWindow;
        private RadWindow selectedRadWindow;

        private DcsMultiPopupsQueryWindowBase UsedPartsStockQueryWindow {
            get {
                if(this.usedPartsStockQueryWindow == null) {
                    this.usedPartsStockQueryWindow = DI.GetQueryWindow("UsedPartsStockForUsedPartsTransferMulit") as DcsMultiPopupsQueryWindowBase;
                    this.usedPartsStockQueryWindow.SelectionDecided += this.UsedPartsStockQueryWindow_SelectionDecided;
                    this.usedPartsStockQueryWindow.Loaded += this.UsedPartsStockQueryWindow_Loaded;
                }
                return this.usedPartsStockQueryWindow;
            }
        }

        private QueryWindowBase UsedPartsWarehouseAreaDropDownDropDownQueryWindow {
            get {
                if(this.usedPartsWarehouseAreaDropDownQueryWindow == null) {
                    this.usedPartsWarehouseAreaDropDownQueryWindow = DI.GetQueryWindow("UsedPartsWarehouseAreaDropDown");
                    this.usedPartsWarehouseAreaDropDownQueryWindow.SelectionDecided += this.UsedPartsWarehouseAreaDropDownQueryWindow_SelectionDecided;
                    this.usedPartsWarehouseAreaDropDownQueryWindow.Loaded += this.UsedPartsWarehouseAreaDropDownQueryWindow_Loaded;
                    this.usedPartsWarehouseAreaDropDownQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                        "Common", "StorageAreaType", (int)DcsAreaKind.库位
                    });
                    this.usedPartsWarehouseAreaDropDownQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                        "Common", "StorageAreaType", false
                    });
                }
                return this.usedPartsWarehouseAreaDropDownQueryWindow;
            }
        }

        private void UsedPartsStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var usedPartsShiftOrder = this.DataContext as UsedPartsShiftOrder;
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || usedPartsShiftOrder == null)
                return;
            this.UsedPartsStockQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("UsedPartsWarehouseId", typeof(int), FilterOperator.IsEqualTo, usedPartsShiftOrder.UsedPartsWarehouseId));
        }

        private void UsedPartsStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var usedPartsStocks = queryWindow.SelectedEntities.Cast<UsedPartsStock>();
            if(usedPartsStocks == null && usedPartsStocks.Count() == 0)
                return;
            var partsShiftOrder = this.DataContext as UsedPartsShiftOrder;
            if(partsShiftOrder == null)
                return;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(partsShiftOrder.UsedPartsShiftDetails.Any(entity => usedPartsStocks.Any(stock => stock.UsedPartsBarCode == entity.UsedPartsBarCode))) {
                UIHelper.ShowNotification(ServiceUIStrings.Validation_Common_UsedPartsBarCodeCanNotRepeat);
                if(parent != null)
                    parent.Close();
                return;
            }
            foreach(var usedPartsStock in usedPartsStocks) {
                var detail = new UsedPartsShiftDetail();
                detail.OriginStoragePositionId = usedPartsStock.UsedPartsWarehouseAreaId;
                detail.BranchId = usedPartsStock.BranchId;
                detail.UsedPartsId = usedPartsStock.UsedPartsId;
                detail.UsedPartsBarCode = usedPartsStock.UsedPartsBarCode;
                detail.UsedPartsCode = usedPartsStock.UsedPartsCode;
                detail.UsedPartsName = usedPartsStock.UsedPartsName;
                detail.UsedPartsSerialNumber = usedPartsStock.UsedPartsSerialNumber;
                detail.UsedPartsBatchNumber = usedPartsStock.UsedPartsBatchNumber;
                detail.Quantity = usedPartsStock.StorageQuantity;
                detail.OriginStoragePositionCode = usedPartsStock.UsedPartsWarehouseArea.Code;
                detail.SerialNumber = this.GridView.Items.Cast<UsedPartsShiftDetail>().Any() ? this.GridView.Items.Cast<UsedPartsShiftDetail>().Max(entity => entity.SerialNumber) + 1 : 1;
                partsShiftOrder.UsedPartsShiftDetails.Add(detail);
            }
            if(parent != null)
                parent.Close();
        }

        private void UsedPartsWarehouseAreaDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var usedPartsShiftOrder = this.DataContext as UsedPartsShiftOrder;
            if(usedPartsShiftOrder != null)
                this.UsedPartsWarehouseAreaDropDownDropDownQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("UsedPartsWarehouseId", typeof(int), FilterOperator.IsEqualTo, usedPartsShiftOrder.UsedPartsWarehouseId));
        }

        private void UsedPartsWarehouseAreaDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var usedPartsWarehouseArea = queryWindow.SelectedEntities.Cast<UsedPartsWarehouseArea>().FirstOrDefault();
            var usedPartsShiftDetail = queryWindow.DataContext as UsedPartsShiftDetail;
            if(usedPartsShiftDetail == null || usedPartsWarehouseArea == null)
                return;
            if(usedPartsShiftDetail.OriginStoragePositionId == usedPartsWarehouseArea.Id) {
                UIHelper.ShowNotification(ServiceUIStrings.Notification_UsedPartsShiftDetail_OriginStoragePositionIdAndDestiStoragePositionIdCanNotSame);
            } else {
                usedPartsShiftDetail.DestiStoragePositionId = usedPartsWarehouseArea.Id;
                usedPartsShiftDetail.DestiStoragePositionCode = usedPartsWarehouseArea.Code;
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            switch(e.Cell.Name) {
                case "DestiStoragePositionId":
                    var usedPartsShiftOrder = this.DataContext as UsedPartsShiftOrder;
                    if(usedPartsShiftOrder == null || usedPartsShiftOrder.UsedPartsWarehouseId == default(int)) {
                        UIHelper.ShowNotification(ServiceUIStrings.Notification_UsedPartsWarehouseName);
                        e.Cancel = true;
                    }
                    break;
            }
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var usedPartsShiftOrder = this.DataContext as UsedPartsShiftOrder;
            if(usedPartsShiftOrder == null)
                return;
            var serialNumber = 1;
            foreach(var ssUsedPartsDisposalDetail in usedPartsShiftOrder.UsedPartsShiftDetails.Where(entity => !e.Items.Contains(entity)))
                ssUsedPartsDisposalDetail.SerialNumber = serialNumber++;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var usedPartsShiftOrder = this.DataContext as UsedPartsShiftOrder;
            e.Cancel = true;
            if(usedPartsShiftOrder == null)
                return;
            if(usedPartsShiftOrder.UsedPartsWarehouseId == default(int))
                UIHelper.ShowNotification(ServiceUIStrings.Notification_UsedPartsWarehouseName);
            else
                this.SelectedRadWindow.ShowDialog();
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(!e.Cell.IsSelected || e.Cell.Column.UniqueName != "DestiStoragePositionCode")
                return;
            var usedPartsShiftDetail = e.Cell.DataContext as UsedPartsShiftDetail;
            if(usedPartsShiftDetail == null)
                return;
            foreach(var usedPartsShiftDetails in this.GridView.SelectedItems.Cast<UsedPartsShiftDetail>().Where(entity => entity != usedPartsShiftDetail)) {
                if(usedPartsShiftDetails.DestiStoragePositionId == usedPartsShiftDetail.DestiStoragePositionId) {
                    UIHelper.ShowNotification(ServiceUIStrings.Notification_UsedPartsShiftDetail_OriginStoragePositionIdAndDestiStoragePositionIdCanNotSame);
                } else {
                    usedPartsShiftDetails.DestiStoragePositionId = usedPartsShiftDetail.DestiStoragePositionId;
                    usedPartsShiftDetails.DestiStoragePositionCode = usedPartsShiftDetail.DestiStoragePositionCode;
                }
            }
        }

        private RadWindow SelectedRadWindow {
            get {
                if(this.selectedRadWindow == null) {
                    this.selectedRadWindow = new RadWindow();
                    this.selectedRadWindow.Header = ServiceUIStrings.QueryPanel_Title_UsedPartsStock;
                    this.selectedRadWindow.Content = this.UsedPartsStockQueryWindow;
                    this.selectedRadWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                }
                return this.selectedRadWindow;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsShiftDetails");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShiftDetail_UsedPartsCode,
                        Name = "UsedPartsCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShiftDetail_UsedPartsName,
                        Name = "UsedPartsName",
                        IsReadOnly = true
                    }
                    //, new ColumnItem {
                    //    Name = "UsedPartsSerialNumber",
                    //    IsReadOnly = true
                    //}, new ColumnItem {
                    //    Name = "UsedPartsBatchNumber",
                    //    IsReadOnly = true
                    //}
                    , new ColumnItem {
                        Name = "Quantity",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShiftDetail_OriginStoragePositionCode1,
                        Name = "OriginStoragePositionCode",
                        IsReadOnly = true
                    }, new DropDownTextBoxColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShiftDetail_DestiStoragePositionCode1,
                        DropDownContent = this.UsedPartsWarehouseAreaDropDownDropDownQueryWindow,
                        Name = "DestiStoragePositionCode",
                        IsEditable = false
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsShiftDetail);
            }
        }
    }
}
