﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsShippingDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "UsedPartsShippingDetail_ReceptionStatus"
        };
        public UsedPartsShippingDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += UsedPartsShippingDetailDataGridView_DataContextChanged;
        }
        private void UsedPartsShippingDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsShippingOrder = e.NewValue as UsedPartsShippingOrderWithOtherInfo;
            var usedPartsShipping = e.NewValue as UsedPartsShippingOrder;
            if((usedPartsShippingOrder == null || usedPartsShippingOrder.UsedPartsShippingOrderId == default(int)) && (usedPartsShipping == null || usedPartsShipping.Id == default(int)))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "UsedPartsShippingOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = usedPartsShippingOrder == null ? usedPartsShipping.Id : usedPartsShippingOrder.UsedPartsShippingOrderId;
            this.ExecuteQueryDelayed();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["UsedPartsEncourageAmount"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["UnitPrice"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["PartsManagementCost"]).DataFormatString = "N2";
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            if(parameterName == "id") {
                return this.FilterItem.Value;
            }
            return null;
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {                                      
                     new KeyValuesColumnItem {
                        Name = "ReceptionStatus",
                       KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    } ,new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShippingDetail_UsedPartsCode,
                        Name = "UsedPartsCode",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShippingDetail_UsedPartsName,
                        Name = "UsedPartsName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = "数量"
                    },
                    new ColumnItem {
                        Title = "出厂编号",
                        Name = "Number"
                    },
                     new ColumnItem {
                        Name = "ClaimBillCode",
                         
                    },   new ColumnItem {
                        Name = "FaultyPartsSupplierName",
                        Title = "责任单位名称"
                    },new ColumnItem {
                        Name = "FaultyPartsSupplierCode",
                        Title = "责任单位编号"
                    },   new ColumnItem {
                        Name = "IfFaultyParts",
                    },
                    new ColumnItem {
                        Name = "ReceptionRemark"
                    },
                    
                    new ColumnItem {
                        Name = "UsedPartsWarehouseAreaCode",
                    },
                    new ColumnItem {
                        Name = "FaultyPartsCode",
                         
                    },
                    new ColumnItem {
                        Name = "FaultyPartsName",
                         
                    },
                    new ColumnItem {
                        Name = "ShippingRemark",
                         
                    },new ColumnItem {
                      Name  = "PartsManagementCost",
                       TextAlignment = TextAlignment.Right,
                        Title = "配件管理费",
                    },
                    new ColumnItem {
                        Name = "SubDealerCode",
                         
                    },
                    new ColumnItem {
                        Name = "SubDealerName",
                         
                    },
                    new ColumnItem {
                        Name = "UsedPartsEncourageAmount",
                        TextAlignment = TextAlignment.Right,
                        Title = "旧件激励金额",
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsShippingDetail);
            }
        }
        protected override string OnRequestQueryName() {
            return "GetUsedPartsShippingDetailsOrderByUsedPartsBarCodeById";
        }
    }
}
