﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsShippingDetailForConfirmDataGridView : DcsDataGridViewBase {
        private QueryWindowBase usedPartsWarehouseAreaDropDownQueryWindow;
        private readonly ObservableCollection<KeyValuePair> kvTypes = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = {
            "UsedPartsShippingDetail_ReceptionStatus"
        };

        public UsedPartsShippingDetailForConfirmDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(Initializ);
        }

        private void Initializ() {
            this.KeyValueManager.LoadData(() => {
                this.kvTypes.Clear();
                var domainContext = this.DomainContext as DcsDomainContext;
                domainContext.Load(domainContext.GetBranchstrategiesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.BranchId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    var entity = loadOp.Entities.Single();
                    if(entity == null) {
                        UIHelper.ShowNotification("不存在分公司策略");
                        return;
                    }
                    if(entity.ReceptionStatus == null) {
                        UIHelper.ShowNotification("公司策略的验收状态策略为空,请维护");
                        return;
                    }
                    if((bool)entity.ReceptionStatus) {
                        foreach(var kvPair in this.KeyValueManager[kvNames[0]].Where(ex => ex.Key == (int)DcsUsedPartsShippingDetailReceptionStatus.不合格 || ex.Key == (int)DcsUsedPartsShippingDetailReceptionStatus.未接收 || ex.Key == (int)DcsUsedPartsShippingDetailReceptionStatus.已接收)) {
                            kvTypes.Add(kvPair);
                        }
                    } else {
                        foreach(var kvPair in this.KeyValueManager[kvNames[0]].Where(ex => ex.Key == (int)DcsUsedPartsShippingDetailReceptionStatus.罚没 || ex.Key == (int)DcsUsedPartsShippingDetailReceptionStatus.未接收 || ex.Key == (int)DcsUsedPartsShippingDetailReceptionStatus.已接收)) {
                            kvTypes.Add(kvPair);
                        }
                    }
                }, null);

            });
        }


        private QueryWindowBase UsedPartsWarehouseAreaDropDownQueryWindow {
            get {
                if(this.usedPartsWarehouseAreaDropDownQueryWindow == null) {
                    this.usedPartsWarehouseAreaDropDownQueryWindow = DI.GetQueryWindow("UsedPartsWarehouseAreaDropDown");
                    this.usedPartsWarehouseAreaDropDownQueryWindow.Loaded += this.usedPartsWarehouseAreaDropDownQueryWindow_Loaded;
                    this.usedPartsWarehouseAreaDropDownQueryWindow.SelectionDecided += this.usedPartsWarehouseAreaDropDownQueryWindow_SelectionDecided;
                }
                return this.usedPartsWarehouseAreaDropDownQueryWindow;
            }
        }

        private void usedPartsWarehouseAreaDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null)
                return;
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            var usedPartsShippingDetail = queryWindow.DataContext as UsedPartsShippingDetail;
            if(usedPartsShippingDetail == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "UsedPartsWarehouse.Name", usedPartsShippingOrder.UsedPartsWarehouse.Name
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "UsedPartsWarehouse.Name", false
            });
        }

        private void usedPartsWarehouseAreaDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var usedPartsWarehouseArea = queryWindow.SelectedEntities.Cast<UsedPartsWarehouseArea>().FirstOrDefault();
            if(usedPartsWarehouseArea == null)
                return;
            var usedPartsShippingDetail = queryWindow.DataContext as UsedPartsShippingDetail;
            if(usedPartsShippingDetail == null)
                return;
            usedPartsShippingDetail.UsedPartsWarehouseAreaId = usedPartsWarehouseArea.Id;
            usedPartsShippingDetail.UsedPartsWarehouseAreaCode = usedPartsWarehouseArea.Code;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var usedPartsShippingDetail = (UsedPartsShippingDetail)e.Cell.DataContext;
            if(!this.GridView.SelectedItems.Contains(usedPartsShippingDetail))
                return;
            if(e.Cell.Column.UniqueName == "UsedPartsWarehouseAreaCode")
                foreach(var selectItem in this.GridView.SelectedItems.Cast<UsedPartsShippingDetail>()) {
                    selectItem.UsedPartsWarehouseAreaId = usedPartsShippingDetail.UsedPartsWarehouseAreaId;
                    selectItem.UsedPartsWarehouseAreaCode = usedPartsShippingDetail.UsedPartsWarehouseAreaCode;
                }
            if(e.Cell.Column.UniqueName == "ReceptionStatus")
                foreach(var selectItem in this.GridView.SelectedItems.Cast<UsedPartsShippingDetail>())
                    selectItem.ReceptionStatus = usedPartsShippingDetail.ReceptionStatus;

        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            var serialNumber = 1;
            foreach(var usedPartsShippingDetail in usedPartsShippingOrder.UsedPartsShippingDetails.Where(entity => !e.Items.Contains(entity))) {
                usedPartsShippingDetail.SerialNumber = serialNumber;
                serialNumber++;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var maxSerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<UsedPartsShippingDetail>().Max(entity => entity.SerialNumber) + 1 : 1;
            e.NewObject = new UsedPartsShippingDetail {
                SerialNumber = maxSerialNumber,
                UsedPartsEncourageAmount = 0
            };
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            ((GridViewDataColumn)this.GridView.Columns["UnitPrice"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["UsedPartsEncourageAmount"]).DataFormatString = "N2";
            ((GridViewDataColumn)this.GridView.Columns["PartsManagementCost"]).DataFormatString = "N2";
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsShippingDetails");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    },
                   
                    new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShippingDetail_UsedPartsCode,
                        Name = "UsedPartsCode",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShippingDetail_UsedPartsName,
                        Name = "UsedPartsName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = "数量",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = "出厂编号",
                        IsReadOnly = true,
                        Name = "Number"
                    },
                     new ColumnItem {
                        Name = "ClaimBillCode",
                        IsReadOnly = true
                    },   new ColumnItem {
                        Name = "FaultyPartsSupplierName",
                        IsReadOnly = true,
                        Title = "责任单位名称"
                    },  new ColumnItem {
                        Name = "IfFaultyParts",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "ShippingRemark",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "FaultyPartsCode",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "FaultyPartsName",
                        IsReadOnly = true
                    },
                     new KeyValuesColumnItem {
                        Name = "ReceptionStatus",
                        KeyValueItems = this.kvTypes
                    },
                    new ColumnItem {
                        Name = "ReceptionRemark"
                    },
                    new DropDownTextBoxColumnItem {
                        Name = "UsedPartsWarehouseAreaCode",
                        DropDownContent = this.UsedPartsWarehouseAreaDropDownQueryWindow,
                        IsEditable = false
                    },
                    new ColumnItem {
                        Name = "UsedPartsEncourageAmount",
                        TextAlignment = TextAlignment.Right,
                        Title = "旧件激励金额",
                    },new ColumnItem {
                      Name  = "PartsManagementCost",
                       TextAlignment = TextAlignment.Right,
                        Title = "配件管理费",
                    },
                    new ColumnItem {
                        Name = "SubDealerCode",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Name = "SubDealerName",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsShippingDetail);
            }
        }
    }
}
