﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsShippingDetailForEditDataGridView : DcsDataGridViewBase {
        private DcsMultiPopupsQueryWindowBase usedPartsStockDropDownQueryWindow;
        private RadWindow radQueryWindow;

        private RadWindow RadQueryWindow {
            get {
                return this.radQueryWindow ?? (this.radQueryWindow = new RadWindow {
                    Content = this.UsedPartsStockDropDownQueryWindow,
                    Header = ServiceUIStrings.QueryPanel_Title_SsUsedPartsStorage,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen
                });
            }
        }

        private DcsMultiPopupsQueryWindowBase UsedPartsStockDropDownQueryWindow {
            get {
                if(this.usedPartsStockDropDownQueryWindow == null) {
                    this.usedPartsStockDropDownQueryWindow = DI.GetQueryWindow("SsUsedPartsStorageMulit") as DcsMultiPopupsQueryWindowBase;
                    this.usedPartsStockDropDownQueryWindow.SelectionDecided += this.usedPartsStockDropDownQueryWindow_SelectionDecided;
                    this.usedPartsStockDropDownQueryWindow.Loaded += usedPartsStockDropDownQueryWindow_Loaded;

                }
                return this.usedPartsStockDropDownQueryWindow;
            }
        }

        private void usedPartsStockDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            this.usedPartsStockDropDownQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "UsedPartsReturnPolicy", (int)DcsPartsWarrantyTermReturnPolicy.返回本部 });
            this.usedPartsStockDropDownQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "UsedPartsReturnPolicy", false });
            this.usedPartsStockDropDownQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "DealerId", BaseApp.Current.CurrentUserData.EnterpriseId });
            this.usedPartsStockDropDownQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "DealerId", false });
            this.usedPartsStockDropDownQueryWindow.ExchangeData(null, "SetQueryItemValue", new object[] { "Common", "PartsSalesCategoryId", usedPartsShippingOrder.PartsSalesCategoryId });
            this.usedPartsStockDropDownQueryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] { "Common", "PartsSalesCategoryId", false });
        }

        private void usedPartsStockDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var ssUsedPartsStorageEntities = queryWindow.SelectedEntities.Cast<SsUsedPartsStorage>();
            if(ssUsedPartsStorageEntities == null)
                return;
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            var serialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<UsedPartsShippingDetail>().Max(entity => entity.SerialNumber) + 1 : 1;
            foreach(var ssUsedPartsStorage in ssUsedPartsStorageEntities) {
                if(usedPartsShippingOrder.UsedPartsShippingDetails.Any(r => ssUsedPartsStorage.UsedPartsBarCode == r.UsedPartsBarCode))
                    continue;
                usedPartsShippingOrder.UsedPartsShippingDetails.Add(new UsedPartsShippingDetail {
                    SerialNumber = serialNumber++,
                    ClaimBillId = ssUsedPartsStorage.ClaimBillId,
                    ClaimBillCode = ssUsedPartsStorage.ClaimBillCode,
                    ClaimBillType = ssUsedPartsStorage.ClaimBillType,
                    BranchId = ssUsedPartsStorage.BranchId,
                    BranchName = ssUsedPartsStorage.BranchName,
                    UsedPartsId = ssUsedPartsStorage.UsedPartsId,
                    UsedPartsCode = ssUsedPartsStorage.UsedPartsCode,
                    UsedPartsName = ssUsedPartsStorage.UsedPartsName,
                    UsedPartsSerialNumber = ssUsedPartsStorage.UsedPartsSerialNumber,
                    UsedPartsBatchNumber = ssUsedPartsStorage.UsedPartsBatchNumber,
                    UsedPartsSupplierId = ssUsedPartsStorage.UsedPartsSupplierId,
                    UsedPartsSupplierCode = ssUsedPartsStorage.UsedPartsSupplierCode,
                    UsedPartsSupplierName = ssUsedPartsStorage.UsedPartsSupplierName,
                    UsedPartsReturnPolicy = ssUsedPartsStorage.UsedPartsReturnPolicy,
                    Quantity = ssUsedPartsStorage.Quantity,
                    UnitPrice = ssUsedPartsStorage.UnitPrice,
                    PartsManagementCost = ssUsedPartsStorage.PartsManagementCost,
                    UsedPartsBarCode = ssUsedPartsStorage.UsedPartsBarCode,
                    FaultyPartsId = ssUsedPartsStorage.FaultyPartsId,
                    FaultyPartsCode = ssUsedPartsStorage.FaultyPartsCode,
                    FaultyPartsName = ssUsedPartsStorage.FaultyPartsName,
                    FaultyPartsSupplierId = ssUsedPartsStorage.FaultyPartsSupplierId,
                    FaultyPartsSupplierCode = ssUsedPartsStorage.FaultyPartsSupplierCode,
                    FaultyPartsSupplierName = ssUsedPartsStorage.FaultyPartsSupplierName,
                    IfFaultyParts = ssUsedPartsStorage.IfFaultyParts,
                    ResponsibleUnitId = ssUsedPartsStorage.ResponsibleUnitId,
                    ResponsibleUnitCode = ssUsedPartsStorage.ResponsibleUnitCode,
                    ResponsibleUnitName = ssUsedPartsStorage.ResponsibleUnitName,
                    Shipped = ssUsedPartsStorage.Shipped,
                    NewPartsId = ssUsedPartsStorage.NewPartsId,
                    NewPartsCode = ssUsedPartsStorage.NewPartsCode,
                    NewPartsName = ssUsedPartsStorage.NewPartsName,
                    NewPartsSupplierId = ssUsedPartsStorage.NewPartsSupplierId,
                    NewPartsSupplierCode = ssUsedPartsStorage.NewPartsSupplierCode,
                    NewPartsSupplierName = ssUsedPartsStorage.NewPartsSupplierName,
                    NewPartsSerialNumber = ssUsedPartsStorage.NewPartsSerialNumber,
                    NewPartsBatchNumber = ssUsedPartsStorage.NewPartsBatchNumber,
                    NewPartsSecurityNumber = ssUsedPartsStorage.NewPartsSecurityNumber,
                    ReceptionStatus = (int)DcsUsedPartsShippingDetailReceptionStatus.在途,
                    PartsSalesCategoryId = ssUsedPartsStorage.PartsSalesCategoryId,
                    InboundAmount = 0,
                    ClaimBillCreateTime = ssUsedPartsStorage.ClaimBillCreateTime
                });
            }
            usedPartsShippingOrder.TotalAmount = usedPartsShippingOrder.UsedPartsShippingDetails.Sum(entity => entity.Quantity * entity.UnitPrice + entity.PartsManagementCost);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            var serialNumber = 1;
            foreach(var usedPartsShippingDetail in usedPartsShippingOrder.UsedPartsShippingDetails.Where(entity => !e.Items.Contains(entity))) {
                usedPartsShippingDetail.SerialNumber = serialNumber;
                serialNumber++;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.Cancel = true;
            var usedPartsShippingOrder = this.DataContext as UsedPartsShippingOrder;
            if(usedPartsShippingOrder == null)
                return;
            if(usedPartsShippingOrder.PartsSalesCategoryId == default(int)) {
                UIHelper.ShowNotification("请先选择品牌");
                return;
            }
            this.RadQueryWindow.ShowDialog();

        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Deleting += this.GridView_Deleting;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            var sortDesc = new SortDescriptor {
                Member = "UsedPartsBarCode",
                SortDirection = ListSortDirection.Ascending
            };
            this.GridView.SortDescriptors.Add(sortDesc);

        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsShippingDetails");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ClaimBillCode",
                        IsReadOnly = true
                    //}, new DropDownTextBoxColumnItem {
                    //    Name = "UsedPartsBarCode",
                    //    DropDownContent = this.UsedPartsStockDropDownQueryWindow,
                    //    IsEditable = false
                      }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShippingDetail_UsedPartsCode,
                        Name = "UsedPartsCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsShippingDetail_UsedPartsName,
                        Name = "UsedPartsName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "IfFaultyParts",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "FaultyPartsCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "FaultyPartsName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ShippingRemark"
                    }, new ColumnItem {
                        Name = "UnitPrice",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PartsManagementCost",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SubDealerCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "SubDealerName",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsShippingDetail);
            }
        }
    }
}
