﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsShippingOrderForConfirmDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "PartsShipping_Method", "UsedPartsShippingOrder_Status", "UsedPartsShippingOrder_Dispatcher"
        };

        public UsedPartsShippingOrderForConfirmDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "Status")
                    },new ColumnItem {
                        Name = "DealerCode",
                        IsGroupable = false,
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "DealerCode")
                    }, new ColumnItem {
                        Name = "DealerName",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "DealerName")
                    },new ColumnItem {
                        Name = "BussinessCode",
                        Title = "业务编码"
                    }, new ColumnItem {
                        Name = "UsedPartsShippingOrderCode",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "Code")
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_Name,
                        Name = "UsedPartsWarehouseName"
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "ShippingMethod")
                    }, new KeyValuesColumnItem {
                        Name = "Dispatcher",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "Dispatcher")
                    }, new ColumnItem {
                        Name = "WarehouseContact",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "WarehouseContact")
                    }, new ColumnItem {
                        Name = "WarehousePhone",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "WarehousePhone")
                    }, new ColumnItem {
                        Name = "DestinationAddress",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "DestinationAddress")
                    }, new ColumnItem {
                        Name = "TransportDriver",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "TransportDriver")
                    }, new ColumnItem {
                        Name = "DriverPhone",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "DriverPhone")
                    }, new ColumnItem {
                        Name = "Supervisor",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "Supervisor")
                    }, new ColumnItem {
                        Name = "SupervisorPhone",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "SupervisorPhone")
                    }, new ColumnItem {
                        Name = "SumUnitPrice",
                        TextAlignment = TextAlignment.Right,
                        Title =  Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "TotalAmount"),
                    },new ColumnItem {
                        Name = "DetailCount",
                        Title = "品种数量"
                    }, new ColumnItem {
                        Name = "Remark",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "Remark")
                    }, new ColumnItem {
                        Name = "ReceptionRemark",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "ReceptionRemark")
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "CreatorName")
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "CreateTime")
                    }, new ColumnItem {
                        Name = "BranchName",
                        Title = Utils.GetEntityLocalizedName(typeof(UsedPartsShippingOrder), "BranchName")
                    }, new ColumnItem{
                        Name = "PartsSalesCategoryName",
                        Title =ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "UsedPartsShippingOrderForConfirm","UsedPartsShippingDetail"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsShippingOrderWithOtherInfo);
            }
        }

        protected override string OnRequestQueryName() {
            return "旧件仓库人员查询发运单";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 500;    //每页改成显示500条
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["SumUnitPrice"]).DataFormatString = "N2";
        }
    }
}
