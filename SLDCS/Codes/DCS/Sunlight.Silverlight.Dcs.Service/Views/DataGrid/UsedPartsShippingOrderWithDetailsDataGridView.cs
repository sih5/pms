﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsShippingOrderWithDetailsDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "PartsShipping_Method", "UsedPartsShippingOrder_Status", "UsedPartsShippingOrder_Dispatcher"
        };

        public UsedPartsShippingOrderWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_Name,
                        Name = "UsedPartsWarehouse.Name"
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Name = "Dispatcher",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Name = "WarehouseContact"
                    }, new ColumnItem {
                        Name = "WarehousePhone"
                    }, new ColumnItem {
                        Name = "DestinationAddress"
                    }, new ColumnItem {
                        Name = "TransportDriver"
                    }, new ColumnItem {
                        Name = "DriverPhone"
                    }, new ColumnItem {
                        Name = "Supervisor"
                    }, new ColumnItem {
                        Name = "SupervisorPhone"
                    }, new ColumnItem {
                        Name = "SumUnitPrice",
                        Title = "总金额"
                    },  new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "ReceptionRemark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "BranchName"
                    },new ColumnItem {
                        Name = "PartsSalesCategory.Name",
                        Title =ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouse_PartsSalesCategory
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "UsedPartsShippingOrder","UsedPartsShippingDetail"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsShippingOrder);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsShippingOrdersWithUsedPartsWarehouse";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
