﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsStockDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "ClaimBill_Type","UsedPartsShippingDetail_ReceptionStatus"
        };

        public UsedPartsStockDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "UsedPartsWarehouse.Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouse.Name"
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouseArea.Code",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_WarehouseAreaCode
                    }, new ColumnItem {
                        Name = "StorageQuantity"
                    }, new ColumnItem {
                        Name = "LockedQuantity"
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_SettlementPrice
                    }, new ColumnItem {
                        Name = "ClaimBillCode"
                    }, new KeyValuesColumnItem {
                        Name = "ClaimBillType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "IfFaultyParts"
                    },new ColumnItem {
                        Name = "FaultyPartsCode"
                    } , new ColumnItem {
                        Name = "UsedPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierName"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierName"
                    }, new ColumnItem {
                        Name = "ResponsibleUnitCode"
                    }, new ColumnItem {
                        Name = "ResponsibleUnitName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_CreateTime
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_CreatorName
                    },new KeyValuesColumnItem {
                        Name = "ReceptionStatus",
                        Title = "验收状态",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsStocksWithDetailsByclaimBillCreateTime";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            DateTime? createTimeBegin = null;
            DateTime? createTimeEnd = null;
            var compositeFilterItems = filter.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
            foreach(var dateTimeFilterItem in compositeFilterItems) {
                var dateTime = dateTimeFilterItem as CompositeFilterItem;
                if(dateTime != null) {
                    if(dateTime.Filters.Any(r => r.MemberName == "ClaimBillCreateTime")) {
                        createTimeBegin = dateTime.Filters.First(r => r.MemberName == "ClaimBillCreateTime").Value as DateTime?;
                        createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ClaimBillCreateTime").Value as DateTime?;
                    }
                }
            }
            switch(parameterName) {
                case "claimBillCreateTimeBegin":
                    return createTimeBegin;
                case "claimBillCreateTimeEnd":
                    return createTimeEnd;
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                var compositeFilterItem1 = compositeFilterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                foreach(var item in compositeFilterItem.Filters.Where(r => r.GetType() != typeof(CompositeFilterItem)))
                    newCompositeFilterItem.Filters.Add(item);
                foreach(var item in compositeFilterItem1.Where(r => r.MemberName != "ClaimBillCreateTime"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.DataPager.PageSize = 1000;
        }
    }
}
