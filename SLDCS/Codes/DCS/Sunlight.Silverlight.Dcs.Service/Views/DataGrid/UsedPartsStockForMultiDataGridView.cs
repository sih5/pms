﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsStockForMultiDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "ClaimBill_Type"
        };

        public UsedPartsStockForMultiDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "UsedPartsWarehouse.Code",
                        IsSortDescending = false
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouse.Name"
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouseArea.Code",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_WarehouseAreaCode
                    }, new ColumnItem {
                        Name = "StorageQuantity"
                    }, new ColumnItem {
                        Name = "LockedQuantity"
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_SettlementPrice
                    }, new ColumnItem {
                        Name = "ClaimBillCode"
                    }, new KeyValuesColumnItem {
                        Name = "ClaimBillType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "IfFaultyParts"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "UsedPartsSupplierName"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierName"
                    }, new ColumnItem {
                        Name = "ResponsibleUnitCode"
                    }, new ColumnItem {
                        Name = "ResponsibleUnitName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_CreateTime
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_CreatorName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsStock);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsStocksWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.DataPager.PageSize = 1000;
        }
    }
}
