﻿using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsStockForUsedPartsTansferDataGridView : UsedPartsStockDataGridView {

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.DataPager.PageSize = 500;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            return compositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsStocksWithDetailsByStatus";
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "UsedPartsWarehouse.Code",
                        Title = "旧件仓库编号",
                    },new ColumnItem {
                        Name = "UsedPartsWarehouse.Name",
                        Title = "旧件仓库名称"
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouseArea.Code",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_WarehouseAreaCode
                    }, new ColumnItem {
                        Name = "StorageQuantity"
                    }, new ColumnItem {
                        Name = "LockedQuantity"
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_SettlementPrice
                    }, new ColumnItem {
                        Name = "ClaimBillCode"
                    }, new ColumnItem {
                        Name = "IfFaultyParts"
                    },new ColumnItem {
                        Name = "FaultyPartsCode"
                    } , new ColumnItem {
                        Name = "UsedPartsSupplierCode"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierCode"
                    },  new ColumnItem {
                        Name = "ResponsibleUnitCode"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_CreateTime
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_CreatorName
                    }
                };
            }
        }

    }
}
