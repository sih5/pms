﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsStockViewForUsedPartsTansferDataGridView : UsedPartsStockDataGridView {

        private readonly string[] kvNames = {
            "ClaimBill_Type","UsedPartsShippingDetail_ReceptionStatus"
        };

        public UsedPartsStockViewForUsedPartsTansferDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.DataPager.PageSize = 500;
        }


        protected override Type EntityType {
            get {
                return typeof(UsedPartsStockView);
            }
        }
        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            return compositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsStockViews";
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = "旧件仓库编号",
                    },new ColumnItem {
                        Name = "Name",
                        Title = "旧件仓库名称"
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"UsedPartsBarCode")
                    }, new ColumnItem {
                        Name = "UsedPartsCode",
                        Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"UsedPartsCode")
                    }, new ColumnItem {
                        Name = "UsedPartsName",
                        Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"UsedPartsName")
                    }, new ColumnItem {
                        Name = "AreaCode",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_WarehouseAreaCode
                    }, new ColumnItem {
                        Name = "StorageQuantity",
                        Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"StorageQuantity")
                    }, new ColumnItem {
                        Name = "LockedQuantity",
                        Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"LockedQuantity")
                    }, new ColumnItem {
                        Name = "SettlementPrice",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_SettlementPrice
                    }, new ColumnItem {
                        Name = "ClaimBillCode",
                        Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"ClaimBillCode")
                    }, new ColumnItem {
                        Name = "IfFaultyParts",
                        Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"IfFaultyParts")
                    },new ColumnItem {
                        Name = "FaultyPartsCode",
                        Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"FaultyPartsCode")
                    } , new ColumnItem {
                        Name = "UsedPartsSupplierCode",
                        Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"UsedPartsSupplierCode")
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierCode",
                        Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"FaultyPartsSupplierCode")
                    },  new ColumnItem {
                        Name = "ResponsibleUnitCode",
                        Title=Utils.GetEntityLocalizedName(typeof(UsedPartsStock),"ResponsibleUnitCode")
                    }, new ColumnItem {
                        Name = "CreateTime",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_CreateTime
                    }, new ColumnItem {
                        Name = "CreatorName",
                        Title = Service.Resources.ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsStock_CreatorName
                    }
                };
            }
        }
    }
}
