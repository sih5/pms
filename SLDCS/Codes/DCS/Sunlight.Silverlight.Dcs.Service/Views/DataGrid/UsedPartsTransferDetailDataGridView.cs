﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsTransferDetailDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "ClaimBill_Type"
        };

        public UsedPartsTransferDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.UsedPartsTransferDetailDataGridView_DataContextChanged;
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsTransferDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Name = "Price",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        TextAlignment = TextAlignment.Right
                    }, 
                    //new ColumnItem {
                    //    Name = "UsedPartsBatchNumber"
                    //}, 
                    //new ColumnItem {
                    //    Name = "UsedPartsSerialNumber"
                    //}, 
                    new ColumnItem {
                        Name = "ClaimBillCode"
                    }, new KeyValuesColumnItem {
                        Name = "ClaimBillType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "IfFaultyParts"
                    }, new ColumnItem {
                        Name = "FaultyPartsCode"
                    },
                    new ColumnItem {
                        Name = "UsedPartsSupplierName"
                    }, new ColumnItem {
                        Name = "FaultyPartsSupplierName"
                    }, new ColumnItem {
                        Name = "ResponsibleUnitName"
                    }
                };
            }
        }

        private void UsedPartsTransferDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsTransferOrder = e.NewValue as UsedPartsTransferOrder;
            if(usedPartsTransferOrder == null || usedPartsTransferOrder.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "UsedPartsTransferOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = usedPartsTransferOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsTransferDetails";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
