﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsTransferDetailForApproveDataGridView : DcsDataGridViewBase {
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsTransferDetails");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsTransferDetail_UsedPartsCode,
                        Name = "UsedPartsCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsTransferDetail_UsedPartsName,
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Price",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        IsReadOnly = false,
                        TextAlignment = TextAlignment.Right,
                    }
                    //, new ColumnItem {
                    //    Name = "UsedPartsSerialNumber",
                    //    IsReadOnly = true
                    //}, new ColumnItem {
                    //    Name = "UsedPartsBatchNumber",
                    //    IsReadOnly = true
                    //}
                    , new ColumnItem {
                        Name = "ClaimBillCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "IfFaultyParts",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "FaultyPartsCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsTransferDetail_UsedPartsSupplierName,
                        Name = "UsedPartsSupplierName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsTransferDetail_FaultyPartsSupplierName,
                        Name = "FaultyPartsSupplierName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsTransferDetail_ResponsibleUnitName,
                        Name = "ResponsibleUnitName",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsTransferDetail);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }
    }
}