﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsTransferDetailForEditDataGridView : UsedPartsTransferDetailForApproveDataGridView {
        private DcsMultiPopupsQueryWindowBase usedPartsStockQueryWindow;
        private RadWindow radWindow;

        private DcsMultiPopupsQueryWindowBase UsedPartsStockQueryWindow {
            get {
                if(this.usedPartsStockQueryWindow == null) {
                    this.usedPartsStockQueryWindow = DI.GetQueryWindow("UsedPartsStockViewForUsedPartsTransferMulit") as DcsMultiPopupsQueryWindowBase;
                    var dcsMultiPopupsQueryWindowBase = this.usedPartsStockQueryWindow;
                    if(dcsMultiPopupsQueryWindowBase != null) {
                        dcsMultiPopupsQueryWindowBase.SelectionDecided += this.UsedPartsStockQueryWindow_SelectionDecided;
                        dcsMultiPopupsQueryWindowBase.Loaded += this.usedPartsStockQueryWindow_Loaded;
                    }
                }
                return this.usedPartsStockQueryWindow;
            }
        }

        private void usedPartsStockQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var usedPartsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || usedPartsTransferOrder == null)
                return;

            if(usedPartsTransferOrder.OriginWarehouseId != 0) {
                var filterItem = new FilterItem();
                filterItem.MemberName = "WarehouseId";
                filterItem.MemberType = typeof(int);
                filterItem.Operator = FilterOperator.IsEqualTo;
                filterItem.Value = usedPartsTransferOrder.OriginWarehouseId;
                queryWindow.ExchangeData(null, "SetAdditionalFilterItem", filterItem);
                queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                    "Common", "Name", usedPartsTransferOrder.OriginWarehouseName
                });
                queryWindow.ExchangeData(null, "RefreshQueryResult", null);
            }
        }

        private void UsedPartsStockQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsMultiPopupsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var usedPartsStock = queryWindow.SelectedEntities.Cast<UsedPartsStockView>().ToArray();
            if(usedPartsStock == null)
                return;
            var partsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            if(partsTransferOrder == null)
                return;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(partsTransferOrder.UsedPartsTransferDetails.Any(entity => usedPartsStock.Any(ex => ex.UsedPartsBarCode == entity.UsedPartsBarCode))) {
                UIHelper.ShowNotification(ServiceUIStrings.Validation_Common_UsedPartsBarCodeCanNotRepeat);
                return;
            }
            foreach(var usedPartsStocks in usedPartsStock) {
                var usedPartsTransferDetail = new UsedPartsTransferDetail {
                    UsedPartsId = usedPartsStocks.UsedPartsId,
                    UsedPartsBarCode = usedPartsStocks.UsedPartsBarCode,
                    UsedPartsCode = usedPartsStocks.UsedPartsCode,
                    UsedPartsName = usedPartsStocks.UsedPartsName,
                    Price = usedPartsStocks.SettlementPrice.HasValue ? usedPartsStocks.SettlementPrice.Value : default(decimal),
                    UsedPartsBatchNumber = usedPartsStocks.UsedPartsBatchNumber,
                    UsedPartsSerialNumber = usedPartsStocks.UsedPartsSerialNumber,
                    ClaimBillType = usedPartsStocks.ClaimBillType,
                    ClaimBillId = usedPartsStocks.ClaimBillId,
                    ClaimBillCode = usedPartsStocks.ClaimBillCode,
                    IfFaultyParts = usedPartsStocks.IfFaultyParts,
                    FaultyPartsId = usedPartsStocks.FaultyPartsId,
                    FaultyPartsCode = usedPartsStocks.FaultyPartsCode,
                    FaultyPartsName = usedPartsStocks.FaultyPartsName,
                    UsedPartsSupplierId = usedPartsStocks.UsedPartsSupplierId,
                    UsedPartsSupplierCode = usedPartsStocks.UsedPartsSupplierCode,
                    UsedPartsSupplierName = usedPartsStocks.UsedPartsSupplierName,
                    FaultyPartsSupplierId = usedPartsStocks.FaultyPartsSupplierId,
                    FaultyPartsSupplierCode = usedPartsStocks.FaultyPartsSupplierCode,
                    FaultyPartsSupplierName = usedPartsStocks.FaultyPartsSupplierName,
                    ResponsibleUnitId = usedPartsStocks.ResponsibleUnitId,
                    ResponsibleUnitCode = usedPartsStocks.ResponsibleUnitCode,
                    ResponsibleUnitName = usedPartsStocks.ResponsibleUnitName,
                    InboundAmount = 0,
                    OutboundAmount = 0,
                    PlannedAmount = 1,
                    ConfirmedAmount = 0,
                    BranchId = usedPartsStocks.BranchId,
                    SerialNumber = this.GridView.Items.Count > 0 ? this.GridView.Items.Cast<UsedPartsTransferDetail>().Max(entity => entity.SerialNumber) + 1 : 1
                };
                partsTransferOrder.UsedPartsTransferDetails.Add(usedPartsTransferDetail);
            }
            if(parent != null)
                parent.Close();
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var usedPartsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            if(usedPartsTransferOrder == null)
                return;
            var serialNumber = 0;

            foreach(var usedPartsTransferDetail in usedPartsTransferOrder.UsedPartsTransferDetails.Where(entity => !e.Items.Contains(entity))) {
                usedPartsTransferDetail.SerialNumber = ++serialNumber;
            }
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var usedPartsTransferOrder = this.DataContext as UsedPartsTransferOrder;
            e.Cancel = true;
            if(usedPartsTransferOrder == null)
                return;
            if(usedPartsTransferOrder.OriginWarehouseId == default(int))
                UIHelper.ShowNotification(ServiceUIStrings.Notification_WarehouseNameIsNull);
            else
                SelectedWindow.ShowDialog();
        }

        private RadWindow SelectedWindow {
            get {
                return this.radWindow ?? (this.radWindow = new RadWindow {
                    Header = ServiceUIStrings.QueryPanel_Title_UsedPartsStock,
                    Content = this.UsedPartsStockQueryWindow,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                });
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Multiple;
            if(this.GridView.Columns["ConfirmedAmount"] != null)
                this.GridView.Columns["ConfirmedAmount"].IsReadOnly = true;
            this.GridView.AddingNewDataItem -= this.GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
            this.GridView.Deleting -= this.GridView_Deleting;
            this.GridView.Deleting += this.GridView_Deleting;
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsTransferDetail_UsedPartsCode,
                        Name = "UsedPartsCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsTransferDetail_UsedPartsName,
                        Name = "UsedPartsName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Price",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "PlannedAmount",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "ConfirmedAmount",
                        IsReadOnly = false,
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "ClaimBillCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "IfFaultyParts",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "FaultyPartsCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsTransferDetail_UsedPartsSupplierName,
                        Name = "UsedPartsSupplierName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsTransferDetail_FaultyPartsSupplierName,
                        Name = "FaultyPartsSupplierName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsTransferDetail_ResponsibleUnitName,
                        Name = "ResponsibleUnitName",
                        IsReadOnly = true
                    }
                };
            }
        }
    }
}