﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsTransferOrderDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "UsedPartsTransferOrder_Status", "UsedParts_OutboundStatus", "UsedParts_InboundStatus"
        };

        public UsedPartsTransferOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsTransferOrder);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsTransferOrder_Status,
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "OriginWarehouseName"
                    }, new ColumnItem {
                        Name = "DestinationWarehouseName"
                    },new ColumnItem {
                        Name = "TotalAmount",
                        Title = "总金额"
                    },new ColumnItem {
                        Name = "TotalQuantity",
                        Title = "总数量"
                    }, new KeyValuesColumnItem {
                        Name = "OutboundStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new KeyValuesColumnItem {
                        Name = "InboundStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ApproverName"
                    }, new ColumnItem {
                        Name = "ApproveTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "AbandonerName"
                    }, new ColumnItem {
                        Name = "AbandonTime"
                    }
                };
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "UsedPartsTransferDetail"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "根据旧件仓库人员查询旧件调拨单";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = true;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
