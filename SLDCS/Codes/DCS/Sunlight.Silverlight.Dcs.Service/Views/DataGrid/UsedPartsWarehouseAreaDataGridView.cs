﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsWarehouseAreaDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Area_Kind", "Area_Category"
        };

        public UsedPartsWarehouseAreaDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "UsedPartsWarehouse.Name"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouseArea_ParentUsedPartsWarehouseAreaCode,
                        Name = "ParentUsedPartsWarehouseArea.Code"
                    }, new KeyValuesColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouseArea_ParentUsedPartsWarehouseAreaStorageAreaType,
                        Name = "ParentUsedPartsWarehouseArea.StorageAreaType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsWarehouseArea_Code,
                        Name = "Code"
                    }, new KeyValuesColumnItem {
                        Name = "StorageAreaType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Name = "StorageCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "IfDefaultStoragePosition"
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsWarehouseArea);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsWarehouseAreasWithDetailsAndParentArea";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
