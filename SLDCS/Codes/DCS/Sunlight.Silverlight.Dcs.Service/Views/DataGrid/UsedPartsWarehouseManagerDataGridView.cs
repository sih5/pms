﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsWarehouseManagerDataGridView : DcsDataGridViewBase {
        public UsedPartsWarehouseManagerDataGridView() {
            this.DataContextChanged += this.UsedPartsWarehouseManagerDataGridView_DataContextChanged;
        }

        private void UsedPartsWarehouseManagerDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsWarehouseArea = e.NewValue as UsedPartsWarehouseArea;
            if(usedPartsWarehouseArea == null || usedPartsWarehouseArea.Id == default(int))
                return;
            this.FilterItem = new FilterItem {
                MemberName = "UsedPartsWarehouseAreaId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = usedPartsWarehouseArea.Id
            };
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Personnel.LoginId"
                    }, new ColumnItem {
                        Name = "Personnel.Name"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsWarehouseManager);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsWarehouseManagersWithPersonnel";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }
    }
}
