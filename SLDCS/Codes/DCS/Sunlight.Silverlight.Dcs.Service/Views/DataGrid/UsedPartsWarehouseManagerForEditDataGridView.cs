﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsWarehouseManagerForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase personnelDropDownQueryWindow;

        private QueryWindowBase PersonnelDropDownQueryWindow {
            get {
                if(this.personnelDropDownQueryWindow == null) {
                    this.personnelDropDownQueryWindow = DI.GetQueryWindow("PersonnelDropDown");
                    this.personnelDropDownQueryWindow.SelectionDecided += this.PersonnelDropDownQueryWindow_SelectionDecided;
                    this.personnelDropDownQueryWindow.Loaded += this.UsedPartsWarehouseManagerQueryWindow_Loaded;
                }
                return this.personnelDropDownQueryWindow;
            }
        }

        private void UsedPartsWarehouseManagerQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var usedPartsWarehouseArea = this.DataContext as UsedPartsWarehouseArea;
            if(usedPartsWarehouseArea != null)
                this.PersonnelDropDownQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("CorporationId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs eventArgs) {
            var usedPartsWarehouseManager = eventArgs.Row.DataContext as UsedPartsWarehouseManager;
            if(usedPartsWarehouseManager == null)
                return;
            if(usedPartsWarehouseManager.EntityState == EntityState.New || usedPartsWarehouseManager.EntityState == EntityState.Detached)
                return;
            if(eventArgs.Cell.Column.UniqueName == "Personnel.LoginId")
                eventArgs.Cancel = true;
        }

        private void PersonnelDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var personnel = queryWindow.SelectedEntities.Cast<Personnel>().FirstOrDefault();
            var usedPartsWarehouseManager = queryWindow.DataContext as UsedPartsWarehouseManager;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(usedPartsWarehouseManager == null || personnel == null || domainContext == null)
                return;
            domainContext.Load(domainContext.GetPersonnelsQuery().Where(d => d.Id == personnel.Id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                usedPartsWarehouseManager.Personnel = loadOp.Entities.SingleOrDefault();
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var usedPartsWarehouseArea = this.DataContext as UsedPartsWarehouseArea;
            if(usedPartsWarehouseArea == null || !usedPartsWarehouseArea.TopLevelUsedPartsWhseAreaId.HasValue)
                return;
            UIHelper.ShowNotification(ServiceUIStrings.DataGridView_Notification_Common_OnlyTopWarehouseAreaCanAddPerson);
            e.Cancel = true;
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            foreach(var item in e.Items.Cast<UsedPartsWarehouseManager>().Where(item => domainContext.UsedPartsWarehouseManagers.Contains(item)))
                domainContext.UsedPartsWarehouseManagers.Remove(item);
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsWarehouseManagers");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "Personnel.LoginId",
                        DropDownContent = this.PersonnelDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "Personnel.Name",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
            this.GridView.Deleted += this.GridView_Deleted;
            this.GridView.AddingNewDataItem += this.GridView_AddingNewDataItem;
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsWarehouseManager);
            }
        }
    }
}
