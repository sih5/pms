﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsWarehouseStaffDataGridView : DcsDataGridViewBase {
        public UsedPartsWarehouseStaffDataGridView() {
            this.DataContextChanged += this.UsedPartsWarehouseStaffDataGridView_DataContextChanged;
        }

        private void UsedPartsWarehouseStaffDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var usedPartsWarehouse = e.NewValue as UsedPartsWarehouse;
            if(usedPartsWarehouse == null || usedPartsWarehouse.Id == default(int))
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "UsedPartsWarehouseId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = usedPartsWarehouse.Id;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsWarehouseStaff);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Personnel.LoginId"
                    }, new ColumnItem {
                        Name = "Personnel.Name"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetUsedPartsWarehouseStaffsWithPersonnel";
        }
    }
}
