﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsWarehouseStaffForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase personnelDropDownQueryWindow;

        private QueryWindowBase PersonnelDropDownQueryWindow {
            get {
                if(this.personnelDropDownQueryWindow == null) {
                    this.personnelDropDownQueryWindow = DI.GetQueryWindow("PersonnelDropDown");
                    this.personnelDropDownQueryWindow.Loaded += this.PersonnelDropDownQueryWindow_Loaded;
                    this.personnelDropDownQueryWindow.SelectionDecided += this.PersonnelDropDownQueryWindow_SelectionDecided;
                }
                return this.personnelDropDownQueryWindow;
            }
        }

        private void PersonnelDropDownQueryWindow_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var personnelQueryWindow = sender as QueryWindowBase;
            if(personnelQueryWindow != null) {
                personnelQueryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem {
                    MemberName = "CorporationId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            }
        }

        private void PersonnelDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var personnel = queryWindow.SelectedEntities.Cast<Personnel>().FirstOrDefault();
            var usedPartsWarehouseStaff = queryWindow.DataContext as UsedPartsWarehouseStaff;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(usedPartsWarehouseStaff == null || personnel == null || domainContext == null)
                return;
            domainContext.Load(domainContext.GetPersonnelsQuery().Where(d => d.Id == personnel.Id), loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                usedPartsWarehouseStaff.Personnel = loadOp.Entities.SingleOrDefault();
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var usedPartsWarehouseStaff = e.Row.DataContext as UsedPartsWarehouseStaff;
            if(usedPartsWarehouseStaff == null)
                return;
            if(usedPartsWarehouseStaff.EntityState == EntityState.New || usedPartsWarehouseStaff.EntityState == EntityState.Detached)
                return;
            if(e.Cell.Column.UniqueName == "Personnel.LoginId")
                e.Cancel = true;
        }

        private void GridView_Deleted(object sender, GridViewDeletedEventArgs e) {
            var usedPartsWarehouse = this.DataContext as UsedPartsWarehouse;
            var domainContext = this.DomainContext as DcsDomainContext;
            if(usedPartsWarehouse == null || domainContext == null)
                return;
            foreach(var item in e.Items.Cast<UsedPartsWarehouseStaff>().Where(item => domainContext.UsedPartsWarehouseStaffs.Contains(item)))
                domainContext.UsedPartsWarehouseStaffs.Remove(item);
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Deleted += this.GridView_Deleted;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("UsedPartsWarehouseStaffs");
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new DropDownTextBoxColumnItem {
                        Name = "Personnel.LoginId",
                        DropDownContent = this.PersonnelDropDownQueryWindow,
                        IsEditable = false
                    }, new ColumnItem {
                        Name = "Personnel.Name",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(UsedPartsWarehouseStaff);
            }
        }
    }
}
