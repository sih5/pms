﻿using Sunlight.Silverlight.Core;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class UsedPartsWarehouseWithStaffsDataGridView : UsedPartsWarehouseDataGridView {
        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "UsedPartsWarehouseStaff"
                    }
                };
            }
        }
    }
}
