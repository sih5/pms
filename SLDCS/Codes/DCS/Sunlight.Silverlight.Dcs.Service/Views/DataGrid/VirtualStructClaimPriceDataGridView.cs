﻿using System;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class VirtualStructClaimPriceDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "PartsWarrantyTerm_ReturnPolicy"
        };

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {                  
                    new ColumnItem {
                        Name = "SparePartCode",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_UsedPartsCode
                    }, new ColumnItem {
                        Name = "SparePartName",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_UsedPartsName
                    }, new ColumnItem {
                        Name = "PartsWarrantyCategoryName",
                        Title = ServiceUIStrings.QueryPanel_QueryItem_Title_StructClaimPrice_PartsWarrantyCategoryId
                    }, new KeyValuesColumnItem {
                        Name = "PartsReturnPolicy",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = ServiceUIStrings.QueryPanel_QueryItem_Title_SsUsedPartsStorageForBranch_UsedPartsReturnPolicy
                    } ,new ColumnItem {
                        Name = "PartsSalesCategoryName",
                        Title = ServiceUIStrings.DataEditView_Text_ServicesClaimApplication_PartsSalesCategoryName
                    }                                                       
                };
            }
        }

        protected override Telerik.Windows.Data.IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var param = new[] {
                    "BranchId", "PartsRetailOrderId","SalesUnitId","DealerId","PartsSalesCategoryId"
                };
                foreach(var filterItem in compositeFilterItem.Filters) {
                    if(filterItem is CompositeFilterItem) {
                        var cp2 = filterItem as CompositeFilterItem;
                        foreach(var s in cp2.Filters.Where(filter2 => !param.Contains(filter2.MemberName))) {
                            newCompositeFilterItem.Filters.Add(s);
                        }
                    } else {
                        if(!param.Contains(filterItem.MemberName))
                            newCompositeFilterItem.Filters.Add(filterItem);
                    }
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filterItem = this.FilterItem as CompositeFilterItem;
            if(filterItem == null)
                return null;
            var compositeFilterItems = filterItem.Filters.Where(e => e is CompositeFilterItem);
            if(compositeFilterItems == null)
                return null;
            var compositeFilterItem = compositeFilterItems.FirstOrDefault(r => ((CompositeFilterItem)r).Filters.Any(v => v.MemberName != "CreateTime")) as CompositeFilterItem;
            if(compositeFilterItem == null)
                return null;
            switch(parameterName) {
                //营销分公司ID
                case "branchId":
                    var branchFilterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "BranchId");
                    return branchFilterItem == null ? null : branchFilterItem.Value;
                //零售订单ID
                case "dealerPartsRetailOrderId":
                    var partsretailorderFilterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsRetailOrderId");
                    return partsretailorderFilterItem == null ? null : partsretailorderFilterItem.Value;
                //销售组织ID
                case "salesUnitId":
                    var salesUnitFilterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "SalesUnitId");
                    return salesUnitFilterItem == null ? null : salesUnitFilterItem.Value;
                //经销商ID
                case "dealerId":
                    var dealerFilterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "DealerId");
                    return dealerFilterItem == null ? null : dealerFilterItem.Value;
                case "partsSalesCategoryId":
                    var partsSalesCategoryIdFilterItem = compositeFilterItem.Filters.SingleOrDefault(e => e.MemberName == "PartsSalesCategoryId");
                    return partsSalesCategoryIdFilterItem == null ? null : partsSalesCategoryIdFilterItem.Value;
                default:
                    return null;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualStructClaimPrice);
            }
        }

        protected override string OnRequestQueryName() {
            return "根据零售订单返回索赔价";
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            base.OnControlsCreated();
        }
    }
}