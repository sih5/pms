﻿
namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class VirtualStructClaimPriceSecondDataGridView : VirtualStructClaimPriceDataGridView {
        protected override string OnRequestQueryName() {
            return "根据销售组织和经销商返回索赔价";
        }
    }
}
