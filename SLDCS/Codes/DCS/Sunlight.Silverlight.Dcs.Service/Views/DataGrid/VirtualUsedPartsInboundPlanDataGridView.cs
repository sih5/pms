﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class VirtualUsedPartsInboundPlanDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "UsedPartsInboundOrder_InboundType", "UsedParts_InboundStatus"
        };

        public VirtualUsedPartsInboundPlanDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                     new KeyValuesColumnItem {
                        Name = "InboundStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "SourceCode"
                    }, new ColumnItem {
                        Name = "CompanyCode",
                        Title = "企业编号"
                    }, new ColumnItem {
                        Name = "CompanyName",
                        Title = "企业名称"
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouseCode"
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouseName"
                    }, new KeyValuesColumnItem {
                        Name = "InboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "SourceBillApprovalTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualUsedPartsInboundPlan);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询虚拟旧件入库计划";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                CompositeFilterItem dateRange;
                switch(parameterName) {
                    case "sourceCode":
                        return filters.Filters.Single(item => item.MemberName == "SourceCode").Value;
                    case "usedPartsWarehouseCode":
                        return filters.Filters.Single(item => item.MemberName == "UsedPartsWarehouseCode").Value;
                    case "usedPartsWarehouseName":
                        return filters.Filters.Single(item => item.MemberName == "UsedPartsWarehouseName").Value;
                    case "inboundType":
                        var inboundTypeFilter = filters.Filters.SingleOrDefault(item => item.MemberName == "InboundType");
                        return inboundTypeFilter == null ? null : inboundTypeFilter.Value;
                    case "inboundStatus":
                        var inboundStatusFilter = filters.Filters.SingleOrDefault(item => item.MemberName == "InboundStatus");
                        return inboundStatusFilter == null ? null : inboundStatusFilter.Value;
                    case "personnelId":
                        return BaseApp.Current.CurrentUserData.UserId;
                    case "sourceBillTimeBegin":
                        dateRange = filters.Filters.SingleOrDefault(item => (item is CompositeFilterItem) && ((CompositeFilterItem)item).Filters.All(e => e.MemberName == "SourceBillApprovalTime")) as CompositeFilterItem;
                        if(dateRange != null && dateRange.Filters.ElementAt(0) != null)
                            return dateRange.Filters.ElementAt(0).Value;
                        return null;
                    case "sourceBillTimeEnd":
                        dateRange = filters.Filters.SingleOrDefault(item => (item is CompositeFilterItem) && ((CompositeFilterItem)item).Filters.All(e => e.MemberName == "SourceBillApprovalTime")) as CompositeFilterItem;
                        if(dateRange != null && dateRange.Filters.ElementAt(1) != null)
                            return dateRange.Filters.ElementAt(1).Value;
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                if(this.FilterItem.MemberName != "SourceCode" && this.FilterItem.MemberName != "UsedPartsWarehouseCode" && this.FilterItem.MemberName != "UsedPartsWarehouseName" && this.FilterItem.MemberName != "InboundType" && this.FilterItem.MemberName != "InboundStatus")
                    newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "SourceCode" && filter.MemberName != "UsedPartsWarehouseCode" && filter.MemberName != "UsedPartsWarehouseName" && filter.MemberName != "InboundType" && filter.MemberName != "InboundStatus" && ((filter is CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.All(e => e.MemberName != "SourceBillApprovalTime"))))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 100;  //每页改成显示100条
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
