﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class VirtualUsedPartsInboundPlanDetailDataGridView : DcsDataGridViewBase {

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    }, new ColumnItem {
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Name = "UsedPartsCode",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_UsedPartsCode
                    }, new ColumnItem {
                        Name = "UsedPartsName",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_UsedPartsName
                    }, new ColumnItem {
                        Name = "UsedPartsWarehouseAreaCode",
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsInboundDetail_UsedPartsWarehouseAreaCode
                    }, new ColumnItem {
                        Name = "PlannedAmount"
                    }, new ColumnItem {
                        Name = "ConfirmedAmount"
                    //}, new ColumnItem {
                    //    Name = "InboundAmount"
                    }, new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_IfFaultyParts,
                         Name = "IfFaultyParts"
                     },  new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierCode,
                         Name = "UsedPartsSupplierCode"
                     },  new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierName,
                         Name = "UsedPartsSupplierName"
                     }, new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsCode,
                         Name = "FaultyPartsCode"
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsName,
                         Name = "FaultyPartsName"
                     },   new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierCode,
                         Name = "FaultyPartsSupplierCode"
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierName,
                         Name = "FaultyPartsSupplierName"
                     } 
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VirtualUsedPartsInboundPlanDetails");
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualUsedPartsInboundPlanDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
