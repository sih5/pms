﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class VirtualUsedPartsOutboundPlanDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "UsedPartsOutboundOrder_OutboundType", "UsedParts_OutboundStatus"
        };

        public VirtualUsedPartsOutboundPlanDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_OutboundStatus,
                        Name = "OutboundStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_SourceCode,
                        Name = "SourceCode"
                    },new ColumnItem {
                        Name = "RelatedCompanyCode",
                        Title = "对方单位编号"
                    },new ColumnItem {
                        Name = "RelatedCompanyName",
                        Title = "对方单位名称"
                    },new ColumnItem {
                        Name = "BusinessCode",
                        Title = "业务编码"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_UsedPartsWarehouseCode,
                        Name = "UsedPartsWarehouseCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_UsedPartsWarehouseName,
                        Name = "UsedPartsWarehouseName"
                    }, new KeyValuesColumnItem {
                        Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_OutboundType,
                        Name = "OutboundType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = ServiceUIStrings.QueryPanel_QueryItem_Title_VirtualUsedPartsOutboundPlan_SourceBillApprovalTime,
                        Name = "SourceBillApprovalTime"
                    }, new ColumnItem {
                        Title = "备注",
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualUsedPartsOutboundPlan);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询虚拟旧件出库计划";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                CompositeFilterItem dateRangeFilter;
                switch(parameterName) {
                    case "sourceCode":
                        return filters.Filters.Single(item => item.MemberName == "SourceCode").Value;
                    case "sourceBillTimeBegin":
                        dateRangeFilter = filters.Filters.SingleOrDefault(item => (item is CompositeFilterItem) && ((CompositeFilterItem)item).Filters.All(e => e.MemberName == "SourceBillApprovalTime")) as CompositeFilterItem;
                        if(dateRangeFilter != null && dateRangeFilter.Filters.ElementAt(0) != null)
                            return dateRangeFilter.Filters.ElementAt(0).Value;
                        return null;
                    case "sourceBillTimeEnd":
                        dateRangeFilter = filters.Filters.SingleOrDefault(item => (item is CompositeFilterItem) && ((CompositeFilterItem)item).Filters.All(e => e.MemberName == "SourceBillApprovalTime")) as CompositeFilterItem;
                        if(dateRangeFilter != null && dateRangeFilter.Filters.ElementAt(1) != null)
                            return dateRangeFilter.Filters.ElementAt(1).Value;
                        return null;
                    case "outboundType":
                        return filters.Filters.Single(item => item.MemberName == "OutboundType").Value;
                    case "outboundStatus":
                        return filters.Filters.Single(item => item.MemberName == "OutboundStatus").Value;
                    case "usedPartsWarehouseCode":
                        return filters.Filters.Single(item => item.MemberName == "UsedPartsWarehouseCode").Value;
                    case "usedPartsWarehouseName":
                        return filters.Filters.Single(item => item.MemberName == "UsedPartsWarehouseName").Value;
                    case "businessCode":
                        return filters.Filters.Single(item => item.MemberName == "BusinessCode").Value;
                    case "relatedCompanyName":
                        return filters.Filters.Single(item => item.MemberName == "RelatedCompanyName").Value;
                    case "personnelId":
                        return BaseApp.Current.CurrentUserData.UserId;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                if(this.FilterItem.MemberName != "SourceCode" && this.FilterItem.MemberName != "OutboundStatus" && this.FilterItem.MemberName != "OutboundType" && this.FilterItem.MemberName != "UsedPartsWarehouseCode" && this.FilterItem.MemberName != "UsedPartsWarehouseName")
                    newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "SourceCode" && filter.MemberName != "OutboundStatus" && filter.MemberName != "OutboundType" && filter.MemberName != "UsedPartsWarehouseCode" && filter.MemberName != "UsedPartsWarehouseName" && (filter is CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.All(e => e.MemberName != "SourceBillApprovalTime")))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.DataPager.PageSize = 100;   //旧件出库每页改成显示100条
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
        }
    }
}
