﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.Service.Views.DataGrid {
    public class VirtualUsedPartsOutboundPlanDetailDataGridView : DcsDataGridViewBase {

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SerialNumber"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_VirtualUsedPartsOutboundPlanDetail_UsedPartsBarCode,
                        Name = "UsedPartsBarCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_VirtualUsedPartsOutboundPlanDetail_UsedPartsCode,
                        Name = "UsedPartsCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_VirtualUsedPartsOutboundPlanDetail_UsedPartsName,
                        Name = "UsedPartsName"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_VirtualUsedPartsOutboundPlanDetail_UsedPartsWarehouseAreaCode,
                        Name = "UsedPartsWarehouseAreaCode"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_VirtualUsedPartsOutboundPlanDetail_PlannedAmount,
                        Name = "PlannedAmount"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_VirtualUsedPartsOutboundPlanDetail_ConfirmedAmount,
                        Name = "ConfirmedAmount"
                    }, new ColumnItem {
                        Title = ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_OutboundAmount,
                        Name = "OutboundAmount"
                    },  new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_IfFaultyParts,
                         Name = "IfFaultyParts"
                     },  new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierCode,
                         Name = "UsedPartsSupplierCode"
                     },  new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_UsedPartsSupplierName,
                         Name = "UsedPartsSupplierName"
                     }, new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsCode,
                         Name = "FaultyPartsCode"
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsName,
                         Name = "FaultyPartsName"
                     },   new ColumnItem {
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierCode,
                         Name = "FaultyPartsSupplierCode"
                     },  new ColumnItem{
                         Title =  ServiceUIStrings.DataGridView_ColumnItem_Title_UsedPartsOutboundDetail_FaultyPartsSupplierName,
                         Name = "FaultyPartsSupplierName"
                     } 
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VirtualUsedPartsOutboundPlanDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VirtualUsedPartsOutboundPlanDetails");
        }
    }
}
