﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
     [PageMeta("Service", "ClaimsBusinessForServiceStation", "DealerClaimSpareParts", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class DealerClaimSparePartsManagement: DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DcsDomainContext domainContext;

        private DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        public DealerClaimSparePartsManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = "服务站索赔领料单";
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerClaimSpareParts"));
            }
        }

      
        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                if(BaseApp.Current.CurrentUserData.EnterpriseId!=12730) {
                    var compositeFilterItem = filterItem as CompositeFilterItem;
                    compositeFilterItem.Filters.Add(new FilterItem {
                        MemberName = "DealerId",
                        Operator = FilterOperator.IsEqualTo,
                        MemberType = typeof(int),
                        Value = BaseApp.Current.CurrentUserData.EnterpriseId
                    });
                    ClientVar.ConvertTime(compositeFilterItem);
                }                              
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerClaimSpareParts"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {                
                case CommonActionKeys.EXPORT:                  
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var claimCode = compositeFilterItem.Filters.Single(r => r.MemberName == "ClaimCode").Value as string;
                            var dealerCode = compositeFilterItem.Filters.Single(r => r.MemberName == "DealerCode").Value as string;
                            var dealerName = compositeFilterItem.Filters.Single(r => r.MemberName == "DealerName").Value as string;
                            var sparePartCode = compositeFilterItem.Filters.Single(r => r.MemberName == "SparePartCode").Value as string;
                            var sparePartName = compositeFilterItem.Filters.Single(r => r.MemberName == "SparePartName").Value as string;
                            var createTime = compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).ToArray();
                            DateTime? createTimeStart = null;
                            DateTime? createTimeEnd = null;
                            DateTime? approveTimeStart = null;
                            DateTime? approveTimeEnd = null;
                            if(createTime != null) {
                                createTimeStart = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "CreateTime").Value : null);
                                createTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.LastOrDefault(r => r.MemberName == "CreateTime").Value : null);
                                approveTimeStart = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "SettleDate") != null ? createTime.FirstOrDefault(r => r.MemberName == "SettleDate").Value : null);
                                approveTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "SettleDate") != null ? createTime.LastOrDefault(r => r.MemberName == "SettleDate").Value : null);
                            }
                            DomainContext.导出服务站索赔领料单(claimCode, dealerCode, dealerName, sparePartCode, sparePartName, createTimeStart, createTimeEnd, approveTimeStart, approveTimeEnd, loadOp =>
                            {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
              
                default:
                    return false;
            }
        }
    }
}