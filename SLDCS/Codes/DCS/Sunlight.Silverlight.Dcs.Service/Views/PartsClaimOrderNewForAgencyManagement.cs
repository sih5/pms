﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("Service", "ClaimsBusinessForHeadquarters", "PartsClaimOrderNewForAgency", ActionPanelKeys = new[] {
       CommonActionKeys.ADD,"PartsClaimOrderNewForAgency",CommonActionKeys.AUDIT,CommonActionKeys.EXPORT
    })]
    public class PartsClaimOrderNewForAgencyManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataAuditView;
        private DataEditViewBase dataAddView;
        private const string DATA_AUDIT_VIEW = "_dataEditView_";
        private const string DATA_ADD_VIEW = "_dataAddView_";

        public PartsClaimOrderNewForAgencyManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_PartsClaimOrderNewForAgency;
        }
        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] { "PartsClaimOrderNewForAgency" };
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsClaimOrderNewForDeliverParts");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataAuditView = null;
            this.dataAddView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsClaimOrderNew"));
            }
        }

        private DataEditViewBase DataAuditView
        {
            get
            {
                if (this.dataAuditView == null)
                {
                    this.dataAuditView = DI.GetDataEditView("PartsClaimOrderNewForAudit");
                    this.dataAuditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataAuditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataAuditView;
            }

        }

        private DataEditViewBase DataAddView {
            get {
                if(this.dataAddView == null) {
                    this.dataAddView = DI.GetDataEditView("PartsClaimOrderNewForAgency");
                    this.dataAddView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataAddView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataAddView;
            }
        }


        private DcsDomainContext domainContext;
        private DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_AUDIT_VIEW, () => this.DataAuditView);
            this.RegisterView(DATA_ADD_VIEW, () => this.DataAddView);
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsClaimOrderNew = this.DataAddView.CreateObjectToEdit<PartsClaimOrderNew>();
                    partsClaimOrderNew.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.新增;
                    partsClaimOrderNew.RepairRequestTime = DateTime.Now;
                    partsClaimOrderNew.ReturnCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsClaimOrderNew.ReturnCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsClaimOrderNew.FaultyPartsName = null;
                    partsClaimOrderNew.FaultyPartsCode = null;
                    partsClaimOrderNew.FaultyPartsReferenceCode = null;
                    partsClaimOrderNew.Quantity = 1;
                    partsClaimOrderNew.LaborCost = 0;
                    partsClaimOrderNew.OtherCost = 0;
                    this.SwitchViewTo(DATA_ADD_VIEW);
                    break;
                case "DeliverParts":
                    var entitys = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                    if(entitys == null)
                        return;
                    this.DataEditView.SetObjectToEditById(entitys.Id);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().Select(r => r.Id).ToArray();
                        DomainContext.ExportPartsClaimOrderNew(ids,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var partsSalesOrderCode = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderCode").Value as string;
                            //var repairOrderCode = compositeFilterItem.Filters.Single(r => r.MemberName == "RepairOrderCode").Value as string;
                            //var businessCode = compositeFilterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                            //var businessName = compositeFilterItem.Filters.Single(r => r.MemberName == "BusinessName").Value as string;
                            var status = compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            //var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            //var cDcReturnWarehouseId = compositeFilterItem.Filters.Single(r => r.MemberName == "CDCReturnWarehouseId").Value as int?;
                            var code = compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var faultyPartsCode = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsCode").Value as string;
                            var faultyPartsName = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsName").Value as string;
                            var returnCompanyName = compositeFilterItem.Filters.Single(r => r.MemberName == "ReturnCompanyName").Value as string;
                            var partsRetailOrderCode = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsRetailOrderCode").Value as string;
                            var centerPartsSalesOrderCode = compositeFilterItem.Filters.Single(r => r.MemberName == "CenterPartsSalesOrderCode").Value as string;
                            //var faultyPartsSupplierCode = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsSupplierCode").Value as string;
                            //var faultyPartsSupplierName = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsSupplierName").Value as string;
                            var createTime = compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).ToArray();
                            DateTime? createTimeStart = null;
                            DateTime? createTimeEnd = null;
                            DateTime? approveTimeStart = null;
                            DateTime? approveTimeEnd = null;
                            if(createTime != null) {
                                createTimeStart = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "CreateTime").Value : null);
                                createTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.LastOrDefault(r => r.MemberName == "CreateTime").Value : null);
                                approveTimeStart = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "ApproveTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "ApproveTime").Value : null);
                                approveTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "ApproveTime") != null ? createTime.LastOrDefault(r => r.MemberName == "ApproveTime").Value : null);
                            }
                            DomainContext.ExportPartsClaimOrderNew(new int[] { }, null, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, partsSalesOrderCode, null, null, null, null, null, faultyPartsCode, faultyPartsName, null, null, status, createTimeStart, createTimeEnd, approveTimeStart, approveTimeEnd, returnCompanyName, code, partsRetailOrderCode,centerPartsSalesOrderCode, loadOp =>
                            {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
                case "Submit":
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_PartsClaimOrderNew_Submit, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        DomainContext.Load(DomainContext.GetPartsClaimOrderNewsQuery().Where(r => r.Id == entity.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            try {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                var submitEntity = loadOp.Entities.SingleOrDefault();
                                if(submitEntity == null)
                                    return;
                                submitEntity.ValidationErrors.Clear();
                                if(submitEntity.Can提交配件索赔单新)
                                    submitEntity.提交配件索赔单新();
                                DomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        DomainContext.RejectChanges();
                                        return;
                                    }
                                    ShellViewModel.Current.IsBusy = false;
                                    UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_PartsClaimOrderNew_SubmitSuccess);
                                    this.CheckActionsCanExecute();
                                    DataGridView.ExecuteQueryDelayed();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case "Receive":
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Receive, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        DomainContext.Load(DomainContext.GetPartsClaimOrderNewsQuery().Where(r => r.Id == entity.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            try {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                var shippingEntity = loadOp.Entities.SingleOrDefault();
                                if(shippingEntity == null)
                                    return;
                                shippingEntity.ValidationErrors.Clear();
                                if(shippingEntity.Can中心库确认收货配件索赔单新)
                                    shippingEntity.中心库确认收货配件索赔单新();
                                DomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        DomainContext.RejectChanges();
                                        return;
                                    }
                                    ShellViewModel.Current.IsBusy = false;
                                    UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_ReceiveSuccess);
                                    this.CheckActionsCanExecute();
                                    DataGridView.ExecuteQueryDelayed();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case "Shipping":
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_PartsClaimOrderNew_Shipping, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        DomainContext.Load(DomainContext.GetPartsClaimOrderNewsQuery().Where(r => r.Id == entity.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            try {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                var shippingEntity = loadOp.Entities.SingleOrDefault();
                                if(shippingEntity == null)
                                    return;
                                shippingEntity.ValidationErrors.Clear();
                                if(shippingEntity.Can中心库发运配件索赔单新)
                                    shippingEntity.中心库发运配件索赔单新();
                                DomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        DomainContext.RejectChanges();
                                        return;
                                    }
                                    ShellViewModel.Current.IsBusy = false;
                                    UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_PartsClaimOrderNew_ShippingSuccess);
                                    this.CheckActionsCanExecute();
                                    DataGridView.ExecuteQueryDelayed();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case CommonActionKeys.AUDIT:
                    var id = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().FirstOrDefault().Id;
                    this.DataAuditView.SetObjectToEditById(id);
                    this.SwitchViewTo(DATA_AUDIT_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "DeliverParts":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First().Status == (int)DcsPartsClaimBillStatusNew.服务站已发运;
                case CommonActionKeys.AUDIT:
                    if (this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First().Status == (int)DcsPartsClaimBillStatusNew.待中心库审核;
                case "Receive":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First().Status == (int)DcsPartsClaimBillStatusNew.服务站已发运;
                case "Shipping":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First().Status == (int)DcsPartsClaimBillStatusNew.区域已收货;
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    var entity = DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First();
                    return entity.Status == (int)DcsPartsClaimBillStatusNew.新增 && entity.ReturnCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId;
                default:
                    return false;
            }
        }
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                //compositeFilterItem.Filters.Add(new FilterItem {
                //    MemberName = "RWarehouseCompanyId",
                //    Operator = FilterOperator.IsEqualTo,
                //    MemberType = typeof(int),
                //    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                //});
                ClientVar.ConvertTime(compositeFilterItem);
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
