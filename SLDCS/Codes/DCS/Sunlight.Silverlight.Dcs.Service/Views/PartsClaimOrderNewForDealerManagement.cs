﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("Service", "ClaimsBusinessForServiceStation", "PartsClaimOrderNewForDealer", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_SUBMIT_ABANDON_EXPORT, "PartsClaimOrderNewForDealer"
    })]
    public class PartsClaimOrderNewForDealerManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DcsDomainContext domainContext;

        private DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        public PartsClaimOrderNewForDealerManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_PartsClaimOrderNewForDealer;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsClaimOrderNewForDealer"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("PartsClaimOrderNewForDealer");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "ReturnCompanyId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                ClientVar.ConvertTime(compositeFilterItem);
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsClaimOrderNewForDealer"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var partsClaimOrderNew = this.DataEditView.CreateObjectToEdit<PartsClaimOrderNew>();
                    partsClaimOrderNew.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    partsClaimOrderNew.Status = (int)DcsPartsClaimBillStatusNew.新增;
                    partsClaimOrderNew.RepairRequestTime = DateTime.Now;
                    partsClaimOrderNew.ReturnCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    partsClaimOrderNew.ReturnCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    partsClaimOrderNew.FaultyPartsName = null;
                    partsClaimOrderNew.FaultyPartsCode = null;
                    partsClaimOrderNew.FaultyPartsReferenceCode = null;
                    partsClaimOrderNew.Quantity = 1;
                    //控件bug,第二次新增时此文本框不能输入，所以赋默认值为0
                    partsClaimOrderNew.LaborCost = 0;
                    partsClaimOrderNew.OtherCost = 0;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    var entitys = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                    if(entitys == null)
                        return;
                    this.DataEditView.SetObjectToEditById(entitys.Id);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "LabelPrint":
                    var printIds = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().Select(r => r.Id).ToArray();
                    BasePrintWindow printWindowNew = new PartsClaimOrderNewLabelPrintWindow {
                        Header = ServiceUIStrings.Action_Title_LabelPrint,
                        PartsClaimOrderNewId = string.Join(",", printIds)
                    };
                    printWindowNew.ShowDialog();
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Submit, () => {
                         ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        DomainContext.Load(DomainContext.GetPartsClaimOrderNewsQuery().Where(r => r.Id == entity.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            try {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                var submitEntity = loadOp.Entities.SingleOrDefault();
                                if(submitEntity == null)
                                    return;
                                if(submitEntity.Can提交配件索赔单新)
                                    submitEntity.提交配件索赔单新();
                                DomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        DomainContext.RejectChanges();
                                        return;
                                    }
                                    ShellViewModel.Current.IsBusy = false;
                                    UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_SubmitSuccess);
                                    this.CheckActionsCanExecute();
                                    DataGridView.ExecuteQueryDelayed();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        DomainContext.Load(DomainContext.GetPartsClaimOrderNewsQuery().Where(r => r.Id == entity.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            try {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                var abandonEntity = loadOp.Entities.SingleOrDefault();
                                if(abandonEntity == null)
                                    return;
                                if(abandonEntity.Can作废配件索赔单新)
                                    abandonEntity.作废配件索赔单新();
                                DomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        DomainContext.RejectChanges();
                                        return;
                                    }
                                    UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_AbandonSuccess);
                                    this.CheckActionsCanExecute();
                                    DataGridView.ExecuteQueryDelayed();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case "Shipping":
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Shipping, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        DomainContext.Load(DomainContext.GetPartsClaimOrderNewsQuery().Where(r => r.Id == entity.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            try {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                var shippingEntity = loadOp.Entities.SingleOrDefault();
                                if(shippingEntity == null)
                                    return;
                                shippingEntity.ValidationErrors.Clear();
                                if(shippingEntity.Can服务站发运配件索赔单新)
                                    shippingEntity.服务站发运配件索赔单新();
                                 DomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        DomainContext.RejectChanges();
                                        return;
                                    }
                                    ShellViewModel.Current.IsBusy = false;
                                    UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_ShippingSuccess);
                                    this.CheckActionsCanExecute();
                                    DataGridView.ExecuteQueryDelayed();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case "Terminate":
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Terminate, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        DomainContext.Load(DomainContext.GetPartsClaimOrderNewsQuery().Where(r => r.Id == entity.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            try {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                var submitEntity = loadOp.Entities.SingleOrDefault();
                                if(submitEntity == null)
                                    return;
                                submitEntity.ValidationErrors.Clear();
                                if(submitEntity.Can终止配件索赔单新)
                                    submitEntity.终止配件索赔单新();
                                DomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        DomainContext.RejectChanges();
                                        return;
                                    }
                                    UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_TerminateSuccess);
                                    this.CheckActionsCanExecute();
                                    this.DataGridView.ExecuteQueryDelayed();
                                }, null);
                                ShellViewModel.Current.IsBusy = false;
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().Select(r => r.Id).ToArray();
                        DomainContext.ExportPartsClaimOrderNew(ids, null,null, null, null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var partsSalesOrderCode = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderCode").Value as string;
                            var partsRetailOrderCode = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsRetailOrderCode").Value as string;
                            var code = compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            //var repairOrderCode = compositeFilterItem.Filters.Single(r => r.MemberName == "RepairOrderCode").Value as string;
                            //var businessCode = compositeFilterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                            //var businessName = compositeFilterItem.Filters.Single(r => r.MemberName == "BusinessName").Value as string;
                            var status = compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            //var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            //var cDcReturnWarehouseId = compositeFilterItem.Filters.Single(r => r.MemberName == "CDCReturnWarehouseId").Value as int?;
                            var faultyPartsCode = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsCode").Value as string;
                            var faultyPartsName = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsName").Value as string;
                            //var faultyPartsSupplierCode = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsSupplierCode").Value as string;
                            //var faultyPartsSupplierName = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsSupplierName").Value as string;
                            var createTime = compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).ToArray();
                            DateTime? createTimeStart = null;
                            DateTime? createTimeEnd = null;
                            DateTime? approveTimeStart = null;
                            DateTime? approveTimeEnd = null;
                            if(createTime != null) {
                                createTimeStart = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "CreateTime").Value : null);
                                createTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.LastOrDefault(r => r.MemberName == "CreateTime").Value : null);
                                approveTimeStart = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "ApproveTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "ApproveTime").Value : null);
                                approveTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "ApproveTime") != null ? createTime.LastOrDefault(r => r.MemberName == "ApproveTime").Value : null);
                            }
                            DomainContext.ExportPartsClaimOrderNew(new int[] {
                            }, BaseApp.Current.CurrentUserData.EnterpriseId, null, null, null, partsSalesOrderCode, null, null, null, null, null, faultyPartsCode, faultyPartsName, null, null, status, createTimeStart, createTimeEnd, approveTimeStart, approveTimeEnd, null, code, partsRetailOrderCode,null, loadOp =>
                            {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.ABANDON:
                case "Shipping":
                case "Terminate":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId.Equals("Terminate")){
                        var entity = DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First();
                        return entity.Status != (int)DcsPartsClaimBillStatusNew.终止 && entity.Status != (int)DcsPartsClaimBillStatusNew.作废;
                    }
                    if(CommonActionKeys.EDIT.Equals(uniqueId) || CommonActionKeys.SUBMIT.Equals(uniqueId)) {
                        var e1 = DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First();
                       return e1.Status == (int)DcsPartsClaimBillStatusNew.新增 && e1.ReturnCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId;
                    }

                    if(CommonActionKeys.ABANDON.Equals(uniqueId)) {
                        var abandonEntity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First();
                        return abandonEntity.Status == (int)DcsPartsClaimBillStatusNew.新增 && abandonEntity.ReturnCompanyId == BaseApp.Current.CurrentUserData.EnterpriseId;
                    }
                    if(uniqueId.Equals("Shipping"))
                        return DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First().Status == (int)DcsPartsClaimBillStatusNew.终审通过;
                    return false;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "LabelPrint":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    return this.DataGridView.SelectedEntities.Any();
                default:
                    return false;
            }
        }
    }
}
