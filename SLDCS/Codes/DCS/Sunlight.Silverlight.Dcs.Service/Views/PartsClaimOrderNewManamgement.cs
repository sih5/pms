﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Service.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;


namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("Service", "ClaimsBusinessForHeadquarters", "PartsClaimOrderNew", ActionPanelKeys = new[] {
        CommonActionKeys.ABANDON_TERMINATE_EXPORT_DETAIL, "PartsClaimOrderNew"
    })]
    public class PartsClaimOrderNewManamgement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private RadWindow editWindow;
        private DataEditViewBase dataDetailView;
        private DataEditViewBase dataInitialView;
        private DataEditViewBase dataFinalView;
        private const string DATA_DETAIL_VIEW = "_dataDetailView";
        private const string DATA_INITIAL_VIEW = "_dataInitialView";
        private const string DATA_FINAL_VIEW = "_dataFinalView";


        public PartsClaimOrderNewManamgement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_TitlePartsClaimOrderNew;
        }

        #region 作废配件索赔单
        private RadWindow radAbandonerWindow;

        private RadWindow RadAbandonerWindow {
            get {
                if(this.radAbandonerWindow == null) {
                    this.radAbandonerWindow = new RadWindow();
                    this.radAbandonerWindow.CanClose = false;
                    this.radAbandonerWindow.Header = "";
                    this.radAbandonerWindow.ResizeMode = ResizeMode.NoResize;
                    this.radAbandonerWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    this.radAbandonerWindow.Content = this.AbandonerDataEditView;
                }
                return this.radAbandonerWindow;
            }
        }

        private DataEditViewBase abandonerDataEditView;

        private DataEditViewBase AbandonerDataEditView {
            get {
                if(this.abandonerDataEditView == null) {
                    this.abandonerDataEditView = DI.GetDataEditView("PartsClaimOrderNewForAbandoner");
                    this.abandonerDataEditView.EditSubmitted += abandonerDataEditView_EditSubmitted;
                    this.abandonerDataEditView.EditCancelled += abandonerDataEditView_EditCancelled;
                }
                return this.abandonerDataEditView;
            }
        }

        private void abandonerDataEditView_EditCancelled(object sender, EventArgs e) {
            var view = sender as PartsClaimOrderNewForAbandonerDataEditView;
            if(view == null)
                return;
            var parent = view.Parent as RadWindow;
            if(parent != null)
                parent.Close();
        }

        private void abandonerDataEditView_EditSubmitted(object sender, EventArgs e) {
            var view = sender as PartsClaimOrderNewForAbandonerDataEditView;
            if(view == null)
                return;
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            var parent = view.Parent as RadWindow;
            if(parent != null)
                parent.Close();
        }
        #endregion

        #region 终止配件索赔单
        private RadWindow radStopWindow;

        private RadWindow RadStopWindow {
            get {
                if(this.radStopWindow == null) {
                    this.radStopWindow = new RadWindow();
                    this.radStopWindow.CanClose = false;
                    this.radStopWindow.Header = "";
                    this.radStopWindow.ResizeMode = ResizeMode.NoResize;
                    this.radStopWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    this.radStopWindow.Content = this.StopDataEditView;
                }
                return this.radStopWindow;
            }
        }

        private DataEditViewBase stopDataEditView;

        private DataEditViewBase StopDataEditView {
            get {
                if(this.stopDataEditView == null) {
                    this.stopDataEditView = DI.GetDataEditView("PartsClaimOrderNewForStop");
                    this.stopDataEditView.EditSubmitted += stopDataEditView_EditSubmitted;
                    this.stopDataEditView.EditCancelled += stopDataEditView_EditCancelled;
                }
                return this.stopDataEditView;
            }
        }

        private void stopDataEditView_EditCancelled(object sender, EventArgs e) {
            var view = sender as PartsClaimOrderNewForStopDataEditView;
            if(view == null)
                return;
            var parent = view.Parent as RadWindow;
            if(parent != null)
                parent.Close();
        }

        private void stopDataEditView_EditSubmitted(object sender, EventArgs e) {
            var view = sender as PartsClaimOrderNewForStopDataEditView;
            if(view == null)
                return;
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            var parent = view.Parent as RadWindow;
            if(parent != null)
                parent.Close();
        }
        #endregion


        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "PartsClaimOrderNew"
                };
            }
        }

        private DataEditViewBase DataDetailView {
            get {
                if(this.dataDetailView == null) {
                    this.dataDetailView = DI.GetDataEditView("PartsClaimOrderNewDetail");
                    this.dataDetailView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataDetailView;
            }
        }


        private DataEditViewBase DataEditView
        {
            get
            {
                if (this.dataEditView == null)
                {
                    this.dataEditView = DI.GetDataEditView("PartsClaimOrderNewForHeadAudit");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }


        private DataEditViewBase DataInitialView
        {
            get
            {
                if (this.dataInitialView == null)
                {
                    this.dataInitialView = DI.GetDataEditView("PartsClaimOrderNewForInitialApprove");
                    this.dataInitialView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataInitialView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataInitialView;
            }
        }

        private DataEditViewBase DataFinalView
        {
            get
            {
                if (this.dataFinalView == null)
                {
                    this.dataFinalView = DI.GetDataEditView("PartsClaimOrderNewForFinalApprove");
                    this.dataFinalView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataFinalView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataFinalView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataDetailView = null;
            this.dataInitialView = null;
            this.dataFinalView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("PartsClaimOrderNew"));
            }
        }

        private DcsDomainContext domainContext;

        private DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_DETAIL_VIEW, () => this.DataDetailView);
            this.RegisterView(DATA_INITIAL_VIEW, () => this.DataInitialView);
            this.RegisterView(DATA_FINAL_VIEW, () => this.DataFinalView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ABANDON:
                    var selectEntity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                    if(selectEntity == null)
                        return;
                    this.AbandonerDataEditView.SetObjectToEditById(selectEntity.Id);
                    this.RadAbandonerWindow.ShowDialog();
                    //DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Abandon, () => {
                    //    var entity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                    //    if(entity == null)
                    //        return;
                    //    DomainContext.Load(DomainContext.GetPartsClaimOrderNewsQuery().Where(r => r.Id == entity.Id), LoadBehavior.RefreshCurrent, loadOp => {
                    //        try {
                    //            if(loadOp.HasError) {
                    //                if(!loadOp.IsErrorHandled)
                    //                    loadOp.MarkErrorAsHandled();
                    //                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    //                return;
                    //            }
                    //            var submitEntity = loadOp.Entities.SingleOrDefault();
                    //            if(submitEntity == null)
                    //                return;
                    //            if(submitEntity.Can作废配件索赔单新)
                    //                submitEntity.作废配件索赔单新();
                    //            DomainContext.SubmitChanges(submitOp => {
                    //                if(submitOp.HasError) {
                    //                    if(!submitOp.IsErrorHandled)
                    //                        submitOp.MarkErrorAsHandled();
                    //                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    //                    DomainContext.RejectChanges();
                    //                    return;
                    //                }
                    //                UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_AbandonSuccess);
                    //                this.CheckActionsCanExecute();
                    //                this.DataGridView.ExecuteQueryDelayed();
                    //            }, null);
                    //        } catch(Exception ex) {
                    //            UIHelper.ShowAlertMessage(ex.Message);
                    //        }
                    //    }, null);
                    //});
                    break;
                case CommonActionKeys.DETAIL:
                    var entityDetail = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                    if(entityDetail == null)
                        return;
                    this.DataDetailView.SetObjectToEditById(entityDetail.Id);
                    this.SwitchViewTo(DATA_DETAIL_VIEW);
                    break;
                case CommonActionKeys.TERMINATE:
                    var stopEntity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                    if(stopEntity == null)
                        return;
                    this.StopDataEditView.SetObjectToEditById(stopEntity.Id);
                    //this.RadStopWindow.ShowDialog();
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Terminate, () => {
                        ShellViewModel.Current.IsBusy = true;
                        var entity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        DomainContext.Load(DomainContext.GetPartsClaimOrderNewsQuery().Where(r => r.Id == entity.Id), LoadBehavior.RefreshCurrent, loadOp => {
                            try {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                    return;
                                }
                                var submitEntity = loadOp.Entities.SingleOrDefault();
                                if(submitEntity == null)
                                    return;
                                if(submitEntity.Can终止配件索赔单新)
                                    submitEntity.终止配件索赔单新();
                                DomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        DomainContext.RejectChanges();
                                        return;
                                    }
                                    ShellViewModel.Current.IsBusy = false;
                                    UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_TerminateSuccess);
                                    this.CheckActionsCanExecute();
                                    this.DataGridView.ExecuteQueryDelayed();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });

                    break;
                case "InitialApprove":
                    var entitys = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                    if(entitys == null)
                        return;
                    this.DataEditView.SetObjectToEditById(entitys.Id);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "Audit":
                     var e1 = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                     if (e1 == null)
                        return;
                     this.DataInitialView.SetObjectToEditById(e1.Id);
                    this.SwitchViewTo(DATA_INITIAL_VIEW);
                    break;
                case "Approve":
                     var e2 = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                     if (e2 == null)
                        return;
                     this.DataFinalView.SetObjectToEditById(e2.Id);
                    this.SwitchViewTo(DATA_FINAL_VIEW);
                    break;
                case "Reject":
                    var entity2 = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().SingleOrDefault();
                    if(entity2 == null)
                        return;
                    var domainContext2 = this.DataGridView.DomainContext as DcsDomainContext;
                    if(domainContext2 == null)
                        return;
                    domainContext2.Load(domainContext2.GetPartsClaimOrderNewsQuery().Where(r => r.Id == entity2.Id), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(loadOp.IsErrorHandled) {
                                loadOp.MarkErrorAsHandled();
                                return;
                            }
                        }
                        this.EditWindow.ShowDialog();
                        this.RejectForQueryWindow.SetValue(DataContextProperty, loadOp.Entities.First());
                    }, null);
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().Select(r => r.Id).ToArray();
                        DomainContext.ExportPartsClaimOrderNew(ids, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var partsSalesOrderCode = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesOrderCode").Value as string;
                            //var repairOrderCode = compositeFilterItem.Filters.Single(r => r.MemberName == "RepairOrderCode").Value as string;
                            //var businessCode = compositeFilterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;
                            //var businessName = compositeFilterItem.Filters.Single(r => r.MemberName == "BusinessName").Value as string;
                            var status = compositeFilterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                            //var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            //var cDcReturnWarehouseId = compositeFilterItem.Filters.Single(r => r.MemberName == "CDCReturnWarehouseId").Value as int?;
                            var faultyPartsCode = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsCode").Value as string;
                            var faultyPartsName = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsName").Value as string;
                            var faultyPartsSupplierCode = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsSupplierCode").Value as string;
                            var faultyPartsSupplierName = compositeFilterItem.Filters.Single(r => r.MemberName == "FaultyPartsSupplierName").Value as string;

                            var returnCompanyName = compositeFilterItem.Filters.Single(r => r.MemberName == "ReturnCompanyName").Value as string;
                            var code = compositeFilterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                            var partsRetailOrderCode = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsRetailOrderCode").Value as string;

                            var createTime = compositeFilterItem.Filters.OfType<CompositeFilterItem>().SelectMany(r => r.Filters).ToArray();
                            
                            DateTime? createTimeStart = null;
                            DateTime? createTimeEnd = null;
                            DateTime? approveTimeStart = null;
                            DateTime? approveTimeEnd = null;
                            if(createTime != null) {
                                createTimeStart = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "CreateTime").Value : null);
                                createTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "CreateTime") != null ? createTime.LastOrDefault(r => r.MemberName == "CreateTime").Value : null);
                                approveTimeStart = (DateTime?)(createTime.FirstOrDefault(r => r.MemberName == "ApproveTime") != null ? createTime.FirstOrDefault(r => r.MemberName == "ApproveTime").Value : null);
                                approveTimeEnd = (DateTime?)(createTime.LastOrDefault(r => r.MemberName == "ApproveTime") != null ? createTime.LastOrDefault(r => r.MemberName == "ApproveTime").Value : null);
                            }
                            DomainContext.ExportPartsClaimOrderNew(new int[] {
                            }, null, null, BaseApp.Current.CurrentUserData.EnterpriseId, null, partsSalesOrderCode, null, null, null, null, null, faultyPartsCode, faultyPartsName, faultyPartsSupplierCode, faultyPartsSupplierName, status, createTimeStart, createTimeEnd, approveTimeStart, approveTimeEnd,returnCompanyName, code, partsRetailOrderCode,null, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }

                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }
                    }
                    break;
            }
        }

        private RadWindow EditWindow {
            get {
                if(this.editWindow == null) {
                    this.editWindow = new RadWindow {
                        Content = this.RejectForQueryWindow,
                        Header = ServiceUIStrings.DataEditView_Title_Add_ServiceTripClaimApplication_Reject,
                        WindowState = WindowState.Normal,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen,
                        AllowDrop = false,
                        ResizeMode = ResizeMode.NoResize,
                        CanClose = false
                    };
                    this.editWindow.Closed += this.EditWindow_Closed;
                }
                return this.editWindow;
            }
        }

        private void EditWindow_Closed(object sender, WindowClosedEventArgs e) {
            var radWindow = sender as RadWindow;
            if(radWindow == null || radWindow.Content == null)
                return;
            var dataEditPanel = radWindow.Content as FrameworkElement;
            if(dataEditPanel == null || dataEditPanel.DataContext == null || !(dataEditPanel.DataContext is PartsClaimOrderNew))
                return;
            var repairClaimBill = dataEditPanel.DataContext as PartsClaimOrderNew;
            if(!string.IsNullOrWhiteSpace(repairClaimBill.RejectReason) && radWindow.DialogResult != null && (bool)radWindow.DialogResult) {
                try {
                    ShellViewModel.Current.IsBusy = true;
                    if(repairClaimBill.Can驳回配件索赔单新)
                        repairClaimBill.驳回配件索赔单新(repairClaimBill.RejectReason);
                    var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    if(domainContext == null)
                        return;
                    domainContext.SubmitChanges(submitOp => {
                        if(submitOp.HasError) {
                            if(!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                            domainContext.RejectChanges();
                            this.CheckActionsCanExecute();
                            return;
                        }
                        ShellViewModel.Current.IsBusy = false;
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_RejectSuccess);
                        this.CheckActionsCanExecute();
                        this.DataGridView.ExecuteQueryDelayed();
                    }, null);
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            }
        }

        private FrameworkElement rejectForQueryWindow;

        private FrameworkElement RejectForQueryWindow {
            get {
                return this.rejectForQueryWindow ?? (this.rejectForQueryWindow = DI.GetDataEditPanel("PartsClaimOrderNewReject"));
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            if(this.DataGridView.Entities == null)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.TERMINATE:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var erminateEntity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First();
                    return erminateEntity.Status != (int)DcsPartsClaimBillStatusNew.作废;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    var abandonEntity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First();
                    return abandonEntity.Status == (int)DcsPartsClaimBillStatusNew.新增 || abandonEntity.Status == (int)DcsPartsClaimBillStatusNew.待配件公司审核;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null || this.DataGridView.Entities.Any();
                case "InitialApprove":
                    if (this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First();
                    return entity.Status == (int)DcsPartsClaimBillStatusNew.中心库审核通过 || entity.Status == (int)DcsPartsClaimBillStatusNew.待配件公司审核;
                case "Reject":
                    if (this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First().Status == (int)DcsPartsClaimBillStatusNew.待配件公司审核
                        || DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First().Status == (int)DcsPartsClaimBillStatusNew.中心库审核通过;
                case "Audit":
                    if (this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First().Status == (int)DcsPartsClaimBillStatusNew.审核通过;
                case "Approve":
                    if (this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return DataGridView.SelectedEntities.Cast<PartsClaimOrderNewWithOtherInfo>().First().Status == (int)DcsPartsClaimBillStatusNew.初审通过;
                case CommonActionKeys.DETAIL:
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    return DataGridView.SelectedEntities.Count() == 1;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "BranchId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                ClientVar.ConvertTime(compositeFilterItem);
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}