﻿
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    public class DealerPartsRetailOrderForSelectQueryWindow : DcsQueryWindowBase
    {
        public DealerPartsRetailOrderForSelectQueryWindow()
        {
            this.SetDefaultQueryItemValue("Common", "Status", (int)DcsWorkflowOfSimpleApprovalStatus.已审核);
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }
        public override string QueryPanelKey {
            get {
                return "DealerPartsRetailOrderForSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "DealerPartsRetailOrderForSelect";
            }
        }

    }
}
