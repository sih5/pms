﻿
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    public class PartsRetailOrderForSelectQueryWindow : DcsQueryWindowBase
    {
        public override string QueryPanelKey {
            get {
                return "PartsRetailOrderForSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsRetailOrderForSelect";
            }
        }

    }
}
