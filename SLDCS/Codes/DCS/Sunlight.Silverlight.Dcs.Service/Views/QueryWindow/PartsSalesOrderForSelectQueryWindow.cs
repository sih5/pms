﻿

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    public class PartsSalesOrderForSelectQueryWindow : DcsQueryWindowBase
    {

        public override string QueryPanelKey {
            get {
                return "PartsSalesOrderForSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "PartsSalesOrderForSelect";
            }
        }

    }
}
