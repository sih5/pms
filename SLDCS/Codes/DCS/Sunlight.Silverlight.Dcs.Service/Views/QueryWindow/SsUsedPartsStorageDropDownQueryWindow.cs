﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    /// <summary>
    /// 选择服务站旧件库存
    /// </summary>
    public class SsUsedPartsStorageDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public SsUsedPartsStorageDropDownQueryWindow() {
            var filterItem = new CompositeFilterItem();
            filterItem.Filters.Add(new FilterItem("DealerId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            filterItem.Filters.Add(new FilterItem("Shipped", typeof(bool), FilterOperator.IsEqualTo, false));
            this.SetDefaultFilterItem(filterItem);
        }

        public override string Title {
            get {
                return ServiceUIStrings.QueryPanel_Title_SsUsedPartsStorage;
            }
        }

        public override string DataGridViewKey {
            get {
                return "SsUsedPartsStorage";
            }
        }

        public override string QueryPanelKey {
            get {
                return "SsUsedPartsStorage";
            }
        }
    }
}
