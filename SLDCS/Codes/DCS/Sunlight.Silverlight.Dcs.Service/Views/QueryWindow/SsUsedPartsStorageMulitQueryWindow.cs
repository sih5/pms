﻿using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    public class SsUsedPartsStorageMulitQueryWindow : DcsMultiPopupsQueryWindowBase {
        /// <summary>
        /// 选择服务站旧件库存
        /// </summary>

        public SsUsedPartsStorageMulitQueryWindow() {
            var filterItem = new CompositeFilterItem();
            filterItem.Filters.Add(new FilterItem("DealerId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            filterItem.Filters.Add(new FilterItem("Shipped", typeof(bool), FilterOperator.IsEqualTo, false));
            this.SetDefaultFilterItem(filterItem);
        }


        public override string QueryPanelKey {
            get {
                return "SsUsedPartsStorage";
            }
        }

        public override string DataGridViewKey {
            get {
                return "SsUsedPartsStorage";
            }
        }
    }
}
