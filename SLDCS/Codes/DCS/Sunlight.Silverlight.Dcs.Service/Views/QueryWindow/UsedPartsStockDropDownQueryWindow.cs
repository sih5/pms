﻿using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    /// <summary>
    /// 旧件库存选择
    /// </summary>
    public class UsedPartsStockDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return ServiceUIStrings.QueryPanel_Title_UsedPartsStock;
            }
        }

        public override string DataGridViewKey {
            get {
                return "UsedPartsStock";
            }
        }

        public override string QueryPanelKey {
            get {
                return "UsedPartsStockForSelect";
            }
        }
    }
}
