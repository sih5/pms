﻿

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    public class UsedPartsStockForUsedPartsTransferMulitQueryWindow : UsedPartsStockMulitQueryWindow {

        public override string QueryPanelKey {
            get {
                return "UsedPartsStockForUsedPartsTransfer";
            }
        }

        public override string DataGridViewKey {
            get {
                return "UsedPartsStockForUsedPartsTansfer";
            }
        }

    }
}
