﻿
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    public class UsedPartsStockMulitQueryWindow : DcsMultiPopupsQueryWindowBase {

        public UsedPartsStockMulitQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem {
                MemberName = "LockedQuantity",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = 0
            });
        }

        public override string QueryPanelKey {
            get {
                return "UsedPartsStockForSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "UsedPartsStockForMulti";
            }
        }
    }
}
