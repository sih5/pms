﻿namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    /// <summary>
    /// 旧件库存选择
    /// </summary>
    public class UsedPartsStockQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "UsedPartsStock";
            }
        }

        public override string QueryPanelKey {
            get {
                return "UsedPartsStockForSelect";
            }
        }
    }
}
