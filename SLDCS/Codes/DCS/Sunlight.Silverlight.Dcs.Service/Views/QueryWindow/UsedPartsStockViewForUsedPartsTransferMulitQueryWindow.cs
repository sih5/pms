﻿
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    public class UsedPartsStockViewForUsedPartsTransferMulitQueryWindow : DcsMultiPopupsQueryWindowBase {

        public UsedPartsStockViewForUsedPartsTransferMulitQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem {
                MemberName = "LockedQuantity",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = 0
            });
        }

        public override string QueryPanelKey {
            get {
                return "UsedPartsStockViewForUsedPartsTransfer";
            }
        }

        public override string DataGridViewKey {
            get {
                return "UsedPartsStockViewForUsedPartsTansfer";
            }
        }
    }
}
