﻿using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    /// <summary>
    /// 旧件库位选择
    /// </summary>
    public class UsedPartsWarehouseArea2DropDownQueryWindow : DcsDropDownQueryWindowBase {

        public UsedPartsWarehouseArea2DropDownQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
            this.SetDefaultQueryItemEnable("Common", "StorageAreaType", false);
            this.SetDefaultQueryItemValue("Common", "StorageAreaType", (int)Web.DcsAreaKind.库位);
        }

        public override string DataGridViewKey {
            get {
                return "UsedPartsWarehouseArea";
            }
        }

        public override string QueryPanelKey {
            get {
                return "UsedPartsWarehouseArea";
            }
        }

        public override string Title {
            get {
                return "旧件库位查询";
            }
        }
    }
}
