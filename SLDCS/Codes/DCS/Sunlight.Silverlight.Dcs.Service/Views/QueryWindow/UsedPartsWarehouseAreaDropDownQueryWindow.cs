﻿using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    /// <summary>
    /// 旧件库区库位选择
    /// </summary>
    public class UsedPartsWarehouseAreaDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public UsedPartsWarehouseAreaDropDownQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "UsedPartsWarehouseArea";
            }
        }

        public override string QueryPanelKey {
            get {
                return "UsedPartsWarehouseArea";
            }
        }

        public override string Title {
            get {
                return ServiceUIStrings.QueryPanel_Title_UsedPartsWarehouseArea;
            }
        }
    }
}
