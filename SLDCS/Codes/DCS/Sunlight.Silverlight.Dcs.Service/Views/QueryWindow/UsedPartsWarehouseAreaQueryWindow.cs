﻿namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {

    /// <summary>
    /// 旧件库区库位选择
    /// </summary>
    public class UsedPartsWarehouseAreaQueryWindow : DcsQueryWindowBase {
        public UsedPartsWarehouseAreaQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "UsedPartsWarehouseArea";
            }
        }

        public override string QueryPanelKey {
            get {
                return "UsedPartsWarehouseArea";
            }
        }
    }
}
