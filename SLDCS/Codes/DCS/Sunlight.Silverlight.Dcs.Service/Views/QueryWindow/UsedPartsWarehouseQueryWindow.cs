﻿namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    /// <summary>
    /// 旧件仓库选择
    /// </summary>
    public class UsedPartsWarehouseQueryWindow : DcsQueryWindowBase {
        public UsedPartsWarehouseQueryWindow() {
            this.SetDefaultQueryItemEnable("Common", "Status", false);
        }

        public override string DataGridViewKey {
            get {
                return "UsedPartsWarehouse";
            }
        }

        public override string QueryPanelKey {
            get {
                return "UsedPartsWarehouse";
            }
        }
    }
}
