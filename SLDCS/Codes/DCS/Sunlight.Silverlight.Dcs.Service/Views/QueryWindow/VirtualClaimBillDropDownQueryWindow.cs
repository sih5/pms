﻿using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    /// <summary>
    /// 结算源单据选择
    /// </summary>
    public class VirtualClaimBillDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "VirtualClaimBill";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VirtualClaimBill";
            }
        }

        public override string Title {
            get {
                return ServiceUIStrings.QueryPanel_Title_VirtualClaimBill;
            }
        }
    }
}
