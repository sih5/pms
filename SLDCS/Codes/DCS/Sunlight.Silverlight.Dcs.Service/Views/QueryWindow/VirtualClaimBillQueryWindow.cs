﻿
namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    /// <summary>
    /// 结算源单据选择
    /// </summary>
    public class VirtualClaimBillQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "VirtualClaimBill";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VirtualClaimBill";
            }
        }
    }
}
