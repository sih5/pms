﻿
namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    public class VirtualStructClaimPriceQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "VirtualStructClaimPrice";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VirtualStructClaimPrice";
            }
        }
    }
}
