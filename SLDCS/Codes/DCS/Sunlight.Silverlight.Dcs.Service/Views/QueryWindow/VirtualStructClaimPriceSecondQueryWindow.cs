﻿
namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    public class VirtualStructClaimPriceSecondQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "VirtualStructClaimPriceSecond";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VirtualStructClaimPrice";
            }
        }
    }
}
