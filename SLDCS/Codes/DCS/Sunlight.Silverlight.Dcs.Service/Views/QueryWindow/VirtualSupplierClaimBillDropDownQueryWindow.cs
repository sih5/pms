﻿using Sunlight.Silverlight.Dcs.Service.Resources;

namespace Sunlight.Silverlight.Dcs.Service.Views.QueryWindow {
    /// <summary>
    /// 供应商结算源单据选择
    /// </summary>
    public class VirtualSupplierClaimBillDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "VirtualSupplierClaimBill";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VirtualSupplierClaimBill";
            }
        }

        public override string Title {
            get {
                return ServiceUIStrings.QueryPanel_Title_VirtualSupplierClaimBill;
            }
        }
    }
}
