﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedPartsTreatmentForBranch", "SsUsedPartsStorage", ActionPanelKeys = new[] {
        CommonActionKeys.EDIT_EXPORT,"SsUsedPartsStorage"
    })]
    public class SsUsedPartsStorageForBranchManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        public SsUsedPartsStorageForBranchManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_SsUsedPartsStorageForBranch;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SsUsedPartsStorageForBranch"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SsUsedPartsStorage");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "BranchId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            }
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SsUsedPartsStorageForBranch"
                };
            }
        }
        private ObservableCollection<SsUsedPartsStorage> ssUsedPartsStorage;
        public ObservableCollection<SsUsedPartsStorage> SsUsedPartsStorage {
            get {
                return ssUsedPartsStorage ?? (this.ssUsedPartsStorage = new ObservableCollection<SsUsedPartsStorage>());
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Select(e => e.GetIdentity()).Cast<int>().ToArray());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case "PrintBarCode":
                    var selected = this.DataGridView.SelectedEntities.Cast<virtualSsUsedPartsStorageWithClaimBillStatus>();
                    if(selected == null)
                        return;
                    SsUsedPartsStorage.Clear();
                    var aa = "";
                    foreach(var usedPartsStorage in selected) {
                        if(aa == "") {
                            aa = usedPartsStorage.Id.ToString();
                            continue;
                        }
                        aa = aa + "," + usedPartsStorage.Id;
                    }
                    BasePrintWindow lablePrintWindow = new SsUsedPartsStorageForLablePrintWindow {
                        Header = "旧件条码标签打印",
                        SsUsedPartsStorage = aa
                    };
                    lablePrintWindow.ShowDialog();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<virtualSsUsedPartsStorageWithClaimBillStatus>().ToArray();
                    if(entities.Length <= 0)
                        return false;
                    return !entities.Any(entity => entity.Shipped);
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "PrintBarCode":
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any();
                default:
                    return false;
            }
        }
    }
}