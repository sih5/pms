﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedPartsTreatmentForServiceStation", "SsUsedPartsStorageQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT,"SsUsedPartsStorage"
    })]
    public class SsUsedPartsStorageQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public SsUsedPartsStorageQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_SsUsedPartsStorage;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SsUsedPartsStorageForDealer"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            CompositeFilterItem compositeFilter;
            if(!(filterItem is CompositeFilterItem)) {
                compositeFilter = new CompositeFilterItem();
                compositeFilter.Filters.Add(filterItem);
            } else
                compositeFilter = filterItem as CompositeFilterItem;
            compositeFilter.Filters.Add(new FilterItem {
                MemberName = "DealerId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = compositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SsUsedPartsStorageForDealer"
                };
            }
        }
        private ObservableCollection<SsUsedPartsStorage> ssUsedPartsStorage;
        public ObservableCollection<SsUsedPartsStorage> SsUsedPartsStorage {
            get {
                return ssUsedPartsStorage ?? (this.ssUsedPartsStorage = new ObservableCollection<SsUsedPartsStorage>());
            }
        }
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var branchId = filterItem.Filters.Single(e => e.MemberName == "BranchId").Value as int?;
                    var usedPartsReturnPolicy = filterItem.Filters.Single(e => e.MemberName == "UsedPartsReturnPolicy").Value as int?;
                    var partsSalesCategoryId = filterItem.Filters.Single(e => e.MemberName == "PartsSalesCategoryId").Value as int?;
                    var claimBillCode = filterItem.Filters.Single(e => e.MemberName == "ClaimBillCode").Value as string;
                    var usedPartsBarCode = filterItem.Filters.Single(e => e.MemberName == "UsedPartsBarCode").Value as string;
                    var compositeFilterItem = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(compositeFilterItem != null) {
                        createTimeBegin = compositeFilterItem.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = compositeFilterItem.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    this.ExportSsUsedPartsStorage(null, BaseApp.Current.CurrentUserData.EnterpriseId, branchId, null, null, claimBillCode, usedPartsBarCode, null, usedPartsReturnPolicy, partsSalesCategoryId, createTimeBegin, createTimeEnd);
                    break;
                case "PrintBarCode":
                    var selected = this.DataGridView.SelectedEntities.Cast<virtualSsUsedPartsStorageWithClaimBillStatus>();
                    if(selected == null)
                        return;
                    SsUsedPartsStorage.Clear();
                    var aa = "";
                    foreach(var usedPartsStorage in selected) {
                        if(aa == "") {
                            aa = usedPartsStorage.Id.ToString();
                            continue;
                        }
                        aa = aa + "," + usedPartsStorage.Id;
                    }
                    BasePrintWindow lablePrintWindow = new SsUsedPartsStorageForLablePrintWindow {
                        Header = "旧件条码标签打印",
                        SsUsedPartsStorage = aa
                    };
                    lablePrintWindow.ShowDialog();
                    break;
            }
        }

        private void ExportSsUsedPartsStorage(int? billId, int? dealerId, int? branchId, string dealerCode, string dealerName, string claimBillCode, string usedPartsBarCode, string usedPartsCode, int? usedPartsReturnPolicy, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            excelServiceClient.ExportSsUsedPartsStorageAsync(billId, dealerId, branchId, dealerCode, dealerName, claimBillCode, usedPartsBarCode, usedPartsCode, usedPartsReturnPolicy, partsSalesCategoryId, createTimeBegin, createTimeEnd);
            excelServiceClient.ExportSsUsedPartsStorageCompleted -= excelServiceClient_ExportSsUsedPartsStorageCompleted;
            excelServiceClient.ExportSsUsedPartsStorageCompleted += excelServiceClient_ExportSsUsedPartsStorageCompleted;
        }

        void excelServiceClient_ExportSsUsedPartsStorageCompleted(object sender, ExportSsUsedPartsStorageCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "PrintBarCode":
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any();
                default:
                    return false;
            }
        }
    }
}
