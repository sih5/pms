﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Service.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("Service", "SupplierClaimSettlement", "SupplierExpenseAdjustBill", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_APPROVE_BATCHAPPROVAL_ABANDON_DETAIL_IMPORT
    })]
    public class SupplierExpenseAdjustBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewDetail;
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_DETAIL = "_DataEditViewDetail_";
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SupplierExpenseAdjustBill"));
            }
        }

        private DataEditViewBase DataEditViewDetail {
            get {
                if(this.dataEditViewDetail == null) {
                    this.dataEditViewDetail = DI.GetDataEditView("SupplierExpenseAdjustBillDetail");
                    this.dataEditViewDetail.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditViewDetail;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("SupplierExpenseAdjustBillForImport");
                    ((SupplierExpenseAdjustBillForImportDataEditView)this.dataEditViewImport).EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditViewImport.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }

        public SupplierExpenseAdjustBillManagement() {
            this.Initializer.Register(this.InitializeData);
            this.Title = ServiceUIStrings.DataManagementView_Title_SupplierExpenseAdjustBill;
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SupplierExpenseAdjustBill");
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewDetail = null;
            this.dataEditViewImport = null;
        }
        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void InitializeData() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_DETAIL, () => this.DataEditViewDetail);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "SupplierExpenseAdjustBill"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<SupplierExpenseAdjustBill>().Any(e => e.Status == (int)DcsExpenseAdjustmentBillStatus.新增);
                case CommonActionKeys.DETAIL:
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1;
                case CommonActionKeys.BATCHAPPROVAL:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entitie = this.DataGridView.SelectedEntities.Cast<SupplierExpenseAdjustBill>().ToArray();
                    if(!entitie.Any())
                        return false;
                    return entitie.All(e => e.Status == (int)DcsExpenseAdjustmentBillStatus.新增);
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var supplierExpenseAdjustBill = this.DataEditView.CreateObjectToEdit<SupplierExpenseAdjustBill>();
                    supplierExpenseAdjustBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    supplierExpenseAdjustBill.Status = (int)DcsExpenseAdjustmentBillStatus.新增;
                    supplierExpenseAdjustBill.SettlementStatus = (int)DcsClaimBillSettlementStatus.待结算;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SupplierExpenseAdjustBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        try {
                            if(entity.Can审批供应商扣补款单)
                                entity.审批供应商扣补款单();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_Approve);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<SupplierExpenseAdjustBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        try {
                            if(entity.Can作废供应商扣补款单)
                                entity.作废供应商扣补款单();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.BATCHAPPROVAL:
                    DcsUtils.Confirm("是否确定审批通过", () => {
                        var ids = this.dataGridView.SelectedEntities.Cast<SupplierExpenseAdjustBill>().Select(r => r.Id).ToArray();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext != null) {
                            domainContext.批量审批供应商扣补款单(true, ids, invokeOp => {
                                if(invokeOp.HasError)
                                    return;
                                UIHelper.ShowNotification("批量审批通过完成");
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        }
                    }, () => {
                        var ids = this.dataGridView.SelectedEntities.Cast<SupplierExpenseAdjustBill>().Select(r => r.Id).ToArray();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext != null) {
                            domainContext.批量审批供应商扣补款单(false, ids, invokeOp => {
                                if(invokeOp.HasError)
                                    return;
                                UIHelper.ShowNotification("批量审批不通过完成");
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        }
                    }, "是", "否");
                    break;
                case CommonActionKeys.DETAIL:
                    this.DataEditViewDetail.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_DETAIL);
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

    }
}
