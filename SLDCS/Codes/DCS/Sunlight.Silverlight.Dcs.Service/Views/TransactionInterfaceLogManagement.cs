﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
     [PageMeta("Service", "RepairServiceForBranch", "TransactionInterfaceLog", ActionPanelKeys = new[]{
         "TransactionInterfaceLog"
    })]
    public class TransactionInterfaceLogManagement : DcsDataManagementViewBase {
         public TransactionInterfaceLogManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_TransactionInterfaceLog;
            
        }

         protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
             this.SwitchViewTo(DATA_GRID_VIEW);
             this.DataGridView.FilterItem = filterItem;
             this.DataGridView.ExecuteQueryDelayed();
         }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                   "TransactionInterfaceLog"
               };
            }
        }

        private DataGridViewBase dataGridView;

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "ManualProcessing":
                     DcsUtils.Confirm("是否手动设置所选中的会员单据?", () => {
                        var id = this.DataGridView.SelectedEntities.Cast<TransactionInterfaceLog>().Select(r => r.Id).FirstOrDefault();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext != null) {
                            domainContext.手动设置会员单据(id, invokeOp => {
                                if(invokeOp.HasError) {
                                    ShellViewModel.Current.IsBusy = false;
                                    return;
                                }
                                UIHelper.ShowNotification("手动设置完成");
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        }
                    });
                    break;
                case "Export":
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<TransactionInterfaceLog>().Select(r => r.Id).ToArray();
                        this.导出保外会员交易接口日志(ids, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var repairCode = filterItem.Filters.Single(r => r.MemberName == "RepairOrder.Code").Value as string;
                        var memberPhone = filterItem.Filters.Single(r => r.MemberName == "MemberPhone").Value as string;
                        var memberName = filterItem.Filters.Single(r => r.MemberName == "MemberName").Value as string;
                        var syncStatus = filterItem.Filters.Single(r => r.MemberName == "SyncStatus").Value as int?;
                        var createTime = filterItem.Filters.FirstOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "SyncTime")) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "SyncTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "SyncTime").Value as DateTime?;
                        }
                        this.导出保外会员交易接口日志(new int[] { }, repairCode, memberPhone, memberName, syncStatus, createTimeBegin, createTimeEnd);
                    }
                    break;
            }
        }

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void 导出保外会员交易接口日志(int[] ids, string repairCode, string memberPhone, string memberName, int? syncStatus, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.导出保外会员交易接口日志Async(ids, repairCode, memberPhone, memberName, syncStatus, createTimeBegin, createTimeEnd);
            this.excelServiceClient.导出保外会员交易接口日志Completed -= excelServiceClient_导出保外会员交易接口日志Completed;
            this.excelServiceClient.导出保外会员交易接口日志Completed += excelServiceClient_导出保外会员交易接口日志Completed;
        }

        private void excelServiceClient_导出保外会员交易接口日志Completed(object sender, 导出保外会员交易接口日志CompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case "ManualProcessing":
                    if(this.DataGridView.SelectedEntities == null || !this.DataGridView.SelectedEntities.Any())
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<TransactionInterfaceLog>().All(entity => entity.SyncStatus == (int)DcsSynchronousState.处理失败 || entity.SyncStatus == (int)DcsSynchronousState.未处理);
                case "Export":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("TransactionInterfaceLog"));
            }
        }
     }
}
