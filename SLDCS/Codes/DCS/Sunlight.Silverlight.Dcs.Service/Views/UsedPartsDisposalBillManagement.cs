﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedWarehouseExecution", "UsedPartsDisposalBill", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_TERMINATE_EXPORT_PRINT_EXPORTDETAIL
    })]
    public class UsedPartsDisposalBillManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewApprove;
        private ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private const string DATA_EDIT_VIEW_APPROVE = "_DataEditViewApprove_";

        public UsedPartsDisposalBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsDisposalBill;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("UsedPartsDisposalBillWithDetails"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("UsedPartsDisposalBill");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewApprove {
            get {
                if(this.dataEditViewApprove == null) {
                    this.dataEditViewApprove = DI.GetDataEditView("UsedPartsDisposalBillForApprove");
                    this.dataEditViewApprove.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewApprove.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewApprove;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_APPROVE, () => this.DataEditViewApprove);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewApprove = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private bool CheckRecordState(string uniqueId) {
            if(this.DataGridView.SelectedEntities == null)
                return false;
            var entities = this.DataGridView.SelectedEntities.Cast<UsedPartsDisposalBill>().ToArray();
            if(entities.Length != 1)
                return false;
            if(uniqueId == CommonActionKeys.TERMINATE)
                return entities[0].Status == (int)DcsUsedPartsDisposalBillStatus.生效 || entities[0].Status == (int)DcsUsedPartsDisposalBillStatus.新建;
            return entities[0].Status == (int)DcsUsedPartsDisposalBillStatus.新建;
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "UsedPartsDisposalBill"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var usedPartsDisposalBill = this.DataEditView.CreateObjectToEdit<UsedPartsDisposalBill>();
                    usedPartsDisposalBill.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    usedPartsDisposalBill.Status = (int)DcsUsedPartsDisposalBillStatus.新建;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    if(!CheckRecordState(uniqueId)) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotEdit);
                        return;
                    }
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    if(!CheckRecordState(uniqueId)) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotAbandon);
                        return;
                    }
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<UsedPartsDisposalBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废旧件处理单)
                                entity.作废旧件处理单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.TERMINATE:
                    if(!CheckRecordState(uniqueId)) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_StatusIsNotAvailableCanNotTerminate);
                        return;
                    }
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Terminate, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<UsedPartsDisposalBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can终止旧件处理单)
                                entity.终止旧件处理单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_TerminateSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    if(!CheckRecordState(uniqueId)) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotApprove);
                        return;
                    }
                    this.DataEditViewApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_APPROVE);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case CommonActionKeys.EXPORTDETAIL:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1) {
                        this.ExportUsedPartsDisposalDetail(this.DataGridView.SelectedEntities.Cast<UsedPartsDisposalBill>().First().Id, BaseApp.Current.CurrentUserData.UserId, null, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem1 = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem1 == null)
                            return;
                        var usedPartsWarehouseCode = filterItem1.Filters.Single(r => r.MemberName == "UsedPartsWarehouseCode").Value as string;
                        var usedPartsWarehouseName = filterItem1.Filters.Single(r => r.MemberName == "UsedPartsWarehouseName").Value as string;
                        var status = filterItem1.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var outboundStatus = filterItem1.Filters.Single(r => r.MemberName == "OutboundStatus").Value as int?;
                        var relatedCompanyName = filterItem1.Filters.Single(r => r.MemberName == "RelatedCompanyName").Value as string;
                        var usedPartsDisposalMethod = filterItem1.Filters.Single(r => r.MemberName == "UsedPartsDisposalMethod").Value as int?;
                        var code = filterItem1.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var compositeFilterItems = filterItem1.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        DateTime? approveTimeBegin = null;
                        DateTime? approveTimeEnd = null;
                        foreach(var dateTimeFilterItem in compositeFilterItems) {
                            var dateTime = dateTimeFilterItem as CompositeFilterItem;
                            if(dateTime != null) {
                                if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                    createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                    createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                                }
                                if(dateTime.Filters.Any(r => r.MemberName == "ApproveTime")) {
                                    approveTimeBegin = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                    approveTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                }
                            }
                        }

                        this.ExportUsedPartsDisposalDetail(null, BaseApp.Current.CurrentUserData.UserId, usedPartsWarehouseCode, usedPartsWarehouseName, status, outboundStatus, relatedCompanyName, usedPartsDisposalMethod, code, createTimeBegin, createTimeEnd, approveTimeBegin, approveTimeEnd);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<UsedPartsDisposalBill>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow;
                    printWindow = new UsedPartsDisposalBillPrintWindow {
                        Header = "旧件残值处理单打印",
                        UsedPartsDisposalBill = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.TERMINATE:
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any();
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.EXPORTDETAIL:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<UsedPartsDisposalBill>().ToArray();
                    return selectItems.Length == 1;
                default:
                    return false;
            }
        }
        //导出清单
        private void ExportUsedPartsDisposalDetail(int? billId, int personnelId, string usedPartsWarehouseCode, string usedPartsWarehouseName, int? status, int? outboundStatus, string relatedCompanyName, int? usedPartsDisposalMethod, string code, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? approveTimeBegin, DateTime? approveTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportUsedPartsDisposalDetailAsync(billId, personnelId, usedPartsWarehouseCode, usedPartsWarehouseName, status, outboundStatus, relatedCompanyName, usedPartsDisposalMethod, code, createTimeBegin, createTimeEnd, approveTimeBegin, approveTimeEnd);
            this.excelServiceClient.ExportUsedPartsDisposalDetailCompleted -= excelServiceClient_ExportUsedPartsDisposalDetailCompleted;
            this.excelServiceClient.ExportUsedPartsDisposalDetailCompleted += excelServiceClient_ExportUsedPartsDisposalDetailCompleted;
        }
        private void excelServiceClient_ExportUsedPartsDisposalDetailCompleted(object sender, ExportUsedPartsDisposalDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }


    }
}
