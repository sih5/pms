﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using System.Windows.Browser;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedPartsTransFee", "UsedPartsDistanceInfor", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT_IMPORT
    })]
    public class UsedPartsDistanceInforManagement : DcsDataManagementViewBase {
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewImport;
        private ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public UsedPartsDistanceInforManagement() {
            this.Initializer.Register(this.InitializeData);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsDistanceInfor;
        }

        private void InitializeData() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("UsedPartsDistanceInfor"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("UsedPartsDistanceInfor");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewImport {
            get {
                if(this.dataEditViewImport == null) {
                    this.dataEditViewImport = DI.GetDataEditView("UsedPartsDistanceInforForImport");
                    this.dataEditViewImport.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditViewImport.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditViewImport;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewImport = null;
        }
        private void DataEditView_EditSubmitted(object sender, System.EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, System.EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var expenseAdjustmentBill = this.DataEditView.CreateObjectToEdit<UsedPartsDistanceInfor>();
                    expenseAdjustmentBill.UsedPartsCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    expenseAdjustmentBill.Status = (int)DcsBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<UsedPartsDistanceInfor>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        try {
                            if(entity.Can作废旧件运距信息)
                                entity.作废旧件运距信息();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var companyCode = filterItem.Filters.Single(e => e.MemberName == "UsedPartsCompany.Code").Value as string;
                    var companyName = filterItem.Filters.Single(r => r.MemberName == "UsedPartsCompany.Name").Value as string;
                    var usedPartsWarehouseCode = filterItem.Filters.Single(e => e.MemberName == "UsedPartsWarehouse.Code").Value as string;
                    var usedPartsWarehouseName = filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouse.Name").Value as string;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.ExportUsedPartsDistanceInfor(companyCode, companyName, null, null,usedPartsWarehouseCode,usedPartsWarehouseName,createTimeBegin, createTimeEnd,status);
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<UsedPartsDistanceInfor>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status==(int)DcsBaseDataStatus.有效;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.Entities == null)
                        return false;
                    return true;
                default:
                    return false;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "UsedPartsDistanceInfor"
                };
            }
        }

        private void ExportUsedPartsDistanceInfor(string companyCode, string companyName, string storageCompanyCode, string storageCompanyName, string warehouseCode, string warehouseName, DateTime? createTimeBegin, DateTime? createTimeEnd, int? status) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportUsedPartsDistanceInforAsync( companyCode,  companyName,  storageCompanyCode,  storageCompanyName,  warehouseCode,  warehouseName, createTimeBegin,  createTimeEnd,status);
            this.excelServiceClient.ExportUsedPartsDistanceInforCompleted -= excelServiceClient_ExportUsedPartsDistanceInforCompleted;
            this.excelServiceClient.ExportUsedPartsDistanceInforCompleted += excelServiceClient_ExportUsedPartsDistanceInforCompleted;
        }

        private void excelServiceClient_ExportUsedPartsDistanceInforCompleted(object sender, ExportUsedPartsDistanceInforCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
