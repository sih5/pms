﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedPartsInbound", "UsedPartsInboundOrder", ActionPanelKeys = new[] {
        "UsedPartsInboundOrder",CommonActionKeys.EXPORTDETAIL
    })]
    public class UsedPartsInboundOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditViewViewDetail;
        private DataEditViewBase dataEditView;
        private const string DATA_EDIT_VIEW_VIEWDETAIL = "_DataEditViewViewDetail_";
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public UsedPartsInboundOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsInboundOrder;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_VIEWDETAIL, () => this.DataEditViewViewDetail);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VirtualUsedPartsInboundPlan"));
            }
        }

        private DataEditViewBase DataEditViewViewDetail {
            get {
                if(this.dataEditViewViewDetail == null) {
                    this.dataEditViewViewDetail = DI.GetDataEditView("UsedPartsInboundOrderDetail");
                    this.dataEditViewViewDetail.EditCancelled += this.DataEditViewViewDetail_EditCancelled;
                    this.dataEditViewViewDetail.EditSubmitted += this.DataEditViewViewDetail_EditSubmitted;
                }
                return this.dataEditViewViewDetail;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("UsedPartsInboundOrder");
                    this.dataEditView.EditCancelled += this.DataEditViewViewDetail_EditCancelled;
                    this.dataEditView.EditSubmitted += this.DataEditViewViewDetail_EditSubmitted;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewViewDetail = null;
        }
        private void DataEditViewViewDetail_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditViewViewDetail_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VirtualUsedPartsInboundPlan"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "ViewDetail":
                    if(DataGridView.SelectedEntities == null)
                        return false;
                    var entities1 = DataGridView.SelectedEntities.Cast<VirtualUsedPartsInboundPlan>().ToArray();
                    if(entities1.Length != 1)
                        return false;
                    return true;
                case "WarehouseParts":
                    if(DataGridView.SelectedEntities == null)
                        return false;
                    var entities = DataGridView.SelectedEntities.Cast<VirtualUsedPartsInboundPlan>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(entities.FirstOrDefault().InboundStatus != (int)DcsUsedPartsInboundStatus.待入库 && entities.FirstOrDefault().InboundStatus != (int)DcsUsedPartsInboundStatus.部分入库)
                        return false;
                    return true;
                case "Terminate":
                    if(DataGridView.SelectedEntities == null)
                        return false;
                    var entity = DataGridView.SelectedEntities.Cast<VirtualUsedPartsInboundPlan>().ToArray();
                    if(entity.Length == 1 && (entity.FirstOrDefault().InboundStatus == (int)DcsUsedPartsInboundStatus.待入库 || entity.FirstOrDefault().InboundStatus == (int)DcsUsedPartsInboundStatus.部分入库) && entity.FirstOrDefault().InboundType == (int)DcsUsedPartsInboundOrderInboundType.服务站返件入库)
                        return true;
                    return false;
                case CommonActionKeys.EXPORTDETAIL:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "ViewDetail":
                    var detailParam = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsInboundPlan>().Select(entity => new[] {
                        entity.InboundType, entity.SourceId
                    }).First();
                    this.DataEditViewViewDetail.SetObjectToEditById(detailParam);
                    SwitchViewTo(DATA_EDIT_VIEW_VIEWDETAIL);
                    break;
                case "WarehouseParts":
                    var warehousePartsParam = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsInboundPlan>().Select(entity => new Object[] {
                        entity.SourceCode, entity.SourceBillApprovalTime, entity.InboundType, entity.InboundStatus, entity.UsedPartsWarehouseCode, entity.UsedPartsWarehouseName
                    }).First();
                    this.DataEditView.SetObjectToEditById(warehousePartsParam);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "Terminate":
                    DcsUtils.Confirm("是否终止所选中的旧件入库单?", () => {
                        var id = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsInboundPlan>().Select(r => r.SourceId).FirstOrDefault();
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext != null) {
                            ShellViewModel.Current.IsBusy = true;
                            domainContext.更新旧件发运单(id, invokeOp => {
                                ShellViewModel.Current.IsBusy = false;
                                if(invokeOp.HasError)
                                    return;
                                UIHelper.ShowNotification("终止成功");
                                this.DataGridView.ExecuteQueryDelayed();
                            }, null);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORTDETAIL:
                    //如果选中一条数据 合并导出参数为 虚拟旧件入库计划ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var id = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsInboundPlan>().Select(r => r.SourceId).First() as int?;
                        this.dcsDomainContext.ExportVirtualUsedPartsInboundPlanDetail(id, BaseApp.Current.CurrentUserData.UserId, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                        }, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var inboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InboundType").Value as int?;
                        var inboundStatus = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundStatus") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InboundStatus").Value as int?;
                        var usedPartsWarehouseCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsWarehouseCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouseCode").Value as string;
                        var usedPartsWarehouseName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsWarehouseName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouseName").Value as string;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        var sourceBillApprovalTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? sourceBillApprovalTimeStart = null;
                        DateTime? sourceBillApprovalTimeEnd = null;
                        if(sourceBillApprovalTime != null) {
                            sourceBillApprovalTimeStart = sourceBillApprovalTime.Filters.First(r => r.MemberName == "SourceBillApprovalTime").Value as DateTime?;
                            sourceBillApprovalTimeEnd = sourceBillApprovalTime.Filters.Last(r => r.MemberName == "SourceBillApprovalTime").Value as DateTime?;
                        }
                        this.dcsDomainContext.ExportVirtualUsedPartsInboundPlanDetail(null, BaseApp.Current.CurrentUserData.UserId, sourceCode, inboundType, inboundStatus, usedPartsWarehouseCode, usedPartsWarehouseName, sourceBillApprovalTimeStart, sourceBillApprovalTimeEnd, loadOp1 => {
                            if(loadOp1.HasError)
                                return;
                            if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                                UIHelper.ShowNotification(loadOp1.Value);
                            }
                            if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                        }, null);
                    }
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}