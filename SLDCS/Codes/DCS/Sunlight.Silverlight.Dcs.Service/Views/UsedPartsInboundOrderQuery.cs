﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedPartsInbound", "UsedPartsInboundOrderQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT_EXPORTDETAIL_PRINT,"UsedPartsInboundOrderQuery"
    })]
    public class UsedPartsInboundOrderQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        public UsedPartsInboundOrderQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsInboundOrderForQuery;
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private DataGridViewBase DataGridView {
            get {
                return dataGridView ?? (this.dataGridView = DI.GetDataGridView("UsedPartsInboundOrderWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "UsedPartsInboundOrder"
                };
            }
        }
        /*
         , new CustomQueryItem {
                            ColumnName = "IfBillable",
                            DataType = typeof(bool),
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsInboundOrder_IfBillable
                        }, new CustomQueryItem {
                            ColumnName = "IfAlreadySettled",
                            DataType = typeof(bool),
                            Title = ServiceUIStrings.QueryPanel_QueryItem_Title_UsedPartsInboundOrder_IfAlreadySettled
                        }
         */
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                case CommonActionKeys.EXPORTDETAIL:
                    //如果选中一条数据 合并导出参数为 旧件入库单ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        if(uniqueId == CommonActionKeys.EXPORT) {
                            var ids = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsInboundOrder>().Select(r => r.Id).ToArray();
                            this.dcsDomainContext.ExportUsedPartsInboundOrder(ids, null, null, null, null, null, null, null, null, null, null, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value))
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }, null);
                        } else {
                            var ids = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsInboundOrder>().Select(r => r.Id).ToArray();
                            this.ExportUsedPartsInboundDetailComplete(ids, null, null, null, null, null, null, null, null, null);
                        }
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var inboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "InboundType").Value as int?;
                        var usedPartsWarehouseCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsWarehouseCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouseCode").Value as string;
                        var usedPartsWarehouseName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsWarehouseName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouseName").Value as string;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        var relatedCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "RelatedCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "RelatedCompanyCode").Value as string;
                        var relatedCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "RelatedCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "RelatedCompanyName").Value as string;
                        var settlementStatus = filterItem.Filters.SingleOrDefault(r => r.MemberName == "InboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SettlementStatus").Value as int?;
                        var outboundTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? inboundTimeStart = null;
                        DateTime? inboundTimeEnd = null;
                        if(outboundTime != null) {
                            inboundTimeStart = outboundTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            inboundTimeEnd = outboundTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        if(uniqueId == CommonActionKeys.EXPORT) {
                            this.dcsDomainContext.ExportUsedPartsInboundOrder(null, code, inboundType, usedPartsWarehouseCode, usedPartsWarehouseName, inboundTimeStart, inboundTimeEnd, sourceCode, relatedCompanyCode, relatedCompanyName, settlementStatus, loadOp1 => {
                                if(loadOp1.HasError)
                                    return;
                                if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                                    UIHelper.ShowNotification(loadOp1.Value);
                                }
                                if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value))
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                            }, null);
                        } else {
                            this.ExportUsedPartsInboundDetailComplete(new int[] { }, code, inboundType, usedPartsWarehouseCode, usedPartsWarehouseName, inboundTimeStart, inboundTimeEnd, sourceCode, relatedCompanyCode, relatedCompanyName);
                        }
                    }
                    break;
                case CommonActionKeys.PRINT:
                case "PrintForGC":
                case "PrintForFinance":
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsInboundOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    if(uniqueId == CommonActionKeys.PRINT) {
                        BasePrintWindow printWindow = new UsedPartsInboundOrderPrintWindow {
                            Header = ServiceUIStrings.PrintWindow_Title_UsedPartsInboundOrder,
                            UsedPartsInboundOrderId = selectedItem.Id
                        };
                        printWindow.ShowDialog();
                    } else if(uniqueId == "PrintForGC") {
                        BasePrintWindow printWindow = new UsedPartsInboundOrderGCPrintWindow {
                            Header = "工程车打印",
                            UsedPartsInboundOrder = selectedItem
                        };
                        printWindow.ShowDialog();
                    } else {
                        BasePrintWindow printWindow = new UsedPartsInboundOrderPrintForFinanceWindow {
                            Header = "入库单打印（财务）",
                            UsedPartsInboundOrderId = selectedItem.Id

                        };
                        printWindow.ShowDialog();
                    }
                    break;
                case "":
                    break;
            }
        }

        private void ExportUsedPartsInboundDetailComplete(int[] usedPartsInboundOrderId, string code, int? inboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? inboundTimeStart, DateTime? inboundTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportUsedPartsInboundDetailAsync(usedPartsInboundOrderId, code, inboundType, usedPartsWarehouseCode, usedPartsWarehouseName, inboundTimeStart, inboundTimeEnd, sourceCode, relatedCompanyCode, relatedCompanyName);
            this.excelServiceClient.ExportUsedPartsInboundDetailCompleted -= excelServiceClient_ExportUsedPartsInboundDetailCompleted;
            this.excelServiceClient.ExportUsedPartsInboundDetailCompleted += excelServiceClient_ExportUsedPartsInboundDetailCompleted;
        }

        private void excelServiceClient_ExportUsedPartsInboundDetailCompleted(object sender, ExportUsedPartsInboundDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) { 
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.EXPORTDETAIL:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                case "PrintForGC":
                case "PrintForFinance":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsInboundOrder>().ToArray();
                    return selectItems.Length == 1;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
