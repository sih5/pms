﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedPartsOutbound", "UsedPartsOutboundOrder", ActionPanelKeys = new[] {
        "UsedPartsOutboundOrder",CommonActionKeys.EXPORTDETAIL
    })]
    public class UsedPartsOutboundOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditViewDeliverParts;
        private DataEditViewBase dataEditViewViewDetail;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        private const string DATA_EDIT_VIEW_VIEWDETAIL = "_DataEditViewViewDetail_";
        private const string DATA_EDIT_VIEW_DELIVERPARTS = "_DataEditViewDeliverParts_";

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VirtualUsedPartsOutboundPlan"));
            }
        }

        private DataEditViewBase DataEditViewDeliverParts {
            get {
                if(this.dataEditViewDeliverParts == null) {
                    this.dataEditViewDeliverParts = DI.GetDataEditView("UsedPartsOutboundOrder");
                    this.dataEditViewDeliverParts.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewDeliverParts.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewDeliverParts;
            }
        }

        private DataEditViewBase DataEditViewViewDetail {
            get {
                if(this.dataEditViewViewDetail == null) {
                    this.dataEditViewViewDetail = DI.GetDataEditView("UsedPartsOutboundDetail");
                    this.dataEditViewViewDetail.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewViewDetail.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewViewDetail;
            }
        }

        public UsedPartsOutboundOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsOutboundOrder;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW_DELIVERPARTS, () => this.DataEditViewDeliverParts);
            this.RegisterView(DATA_EDIT_VIEW_VIEWDETAIL, () => this.DataEditViewViewDetail);
        }
        private void ResetEditView() {
            this.dataEditViewDeliverParts = null;
            this.dataEditViewViewDetail = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VirtualUsedPartsOutboundPlan"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilter = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilter);
            this.DataGridView.FilterItem = compositeFilter;
            //this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "DeliverParts":
                    var filter = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsOutboundPlan>().Select(entity => new object[] {
                        entity.SourceCode, entity.SourceBillApprovalTime, entity.SourceBillApprovalTime, entity.OutboundType, entity.OutboundStatus, entity.UsedPartsWarehouseCode, entity.UsedPartsWarehouseName
                    }).First();
                    this.DataEditViewDeliverParts.SetObjectToEditById(filter);
                    this.SwitchViewTo(DATA_EDIT_VIEW_DELIVERPARTS);
                    break;
                case "ViewDetail":
                    var filterParam = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsOutboundPlan>().Select(entity => new[] {
                        entity.OutboundType, entity.SourceId,entity.UsedPartsWarehouseId
                    }).First();
                    this.DataEditViewViewDetail.SetObjectToEditById(filterParam);
                    this.SwitchViewTo(DATA_EDIT_VIEW_VIEWDETAIL);
                    break;
                case CommonActionKeys.EXPORTDETAIL:
                    //如果选中一条数据 合并导出参数为 虚拟旧件出库计划ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var id = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsOutboundPlan>().Select(r => r.SourceId).First() as int?;
                        this.dcsDomainContext.ExportVirtualUsedPartsOutboundPlanDetail(id, BaseApp.Current.CurrentUserData.UserId, null, null, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                            }

                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                            }
                        }, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var outboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OutboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OutboundType").Value as int?;
                        var outboundStatus = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OutboundStatus") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OutboundStatus").Value as int?;
                        var usedPartsWarehouseCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsWarehouseCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouseCode").Value as string;
                        var usedPartsWarehouseName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsWarehouseName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouseName").Value as string;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        var relatedCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "RelatedCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "RelatedCompanyName").Value as string;
                        var businessCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "BusinessCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "BusinessCode").Value as string;

                        var sourceBillApprovalTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? sourceBillApprovalTimeStart = null;
                        DateTime? sourceBillApprovalTimeEnd = null;
                        if(sourceBillApprovalTime != null) {
                            sourceBillApprovalTimeStart = sourceBillApprovalTime.Filters.First(r => r.MemberName == "SourceBillApprovalTime").Value as DateTime?;
                            sourceBillApprovalTimeEnd = sourceBillApprovalTime.Filters.Last(r => r.MemberName == "SourceBillApprovalTime").Value as DateTime?;
                        }
                        this.dcsDomainContext.ExportVirtualUsedPartsOutboundPlanDetail(null, BaseApp.Current.CurrentUserData.UserId, sourceCode, outboundType, outboundStatus, usedPartsWarehouseCode, usedPartsWarehouseName, sourceBillApprovalTimeStart, sourceBillApprovalTimeEnd, relatedCompanyName, businessCode, loadOp1 => {
                            if(loadOp1.HasError)
                                return;
                            if(loadOp1.Value == null || string.IsNullOrEmpty(loadOp1.Value)) {
                                UIHelper.ShowNotification(loadOp1.Value);
                            }
                            if(loadOp1.Value != null && !string.IsNullOrEmpty(loadOp1.Value))
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp1.Value));
                        }, null);
                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case "DeliverParts":
                case "ViewDetail":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VirtualUsedPartsOutboundPlan>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0] != null;
                case CommonActionKeys.EXPORTDETAIL:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
