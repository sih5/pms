﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedPartsOutbound", "UsedPartsOutboundOrderQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT_EXPORTDETAIL_PRINT,"UsedPartsOutboundOrderQuery"
    })]
    public class UsedPartsOutboundOrderQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView; 

        public UsedPartsOutboundOrderQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsOutboundOrderQuery;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("UsedPartsOutboundOrderWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var composite = filterItem as CompositeFilterItem;
            if(composite == null)
                return;
            ClientVar.ConvertTime(composite);
            this.DataGridView.FilterItem = composite;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "UsedPartsOutboundOrder"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "NotPartCodePrint":
                    var selectedItem1 = this.DataGridView.SelectedEntities.Cast<UsedPartsOutboundOrder>().FirstOrDefault();
                    if(selectedItem1 == null)
                        return;
                    BasePrintWindow printWindow1 = new VirtualUsedPartsOutboundPlanPrintWindow {
                        Header = "无图号打印",
                        UsedPartsOutboundOrder = selectedItem1
                    };
                    printWindow1.ShowDialog();
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<UsedPartsOutboundOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new UsedPartsOutboundOrderPrintWindow {
                        Header = "旧件出库单打印",
                        UsedPartsOutboundOrder = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case CommonActionKeys.EXPORTDETAIL:
                    //如果选中一条数据 合并导出参数为 旧件出库单ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<UsedPartsOutboundOrder>().Select(r => r.Id).ToArray();
                        this.ExportUsedPartsOutboundDetail(ids, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var outboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OutboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OutboundType").Value as int?;
                        var usedPartsWarehouseCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsWarehouseCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouseCode").Value as string;
                        var usedPartsWarehouseName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsWarehouseName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouseName").Value as string;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        var relatedCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "RelatedCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "RelatedCompanyCode").Value as string;
                        var relatedCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "RelatedCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "RelatedCompanyName").Value as string;
                        var usedPartsBarCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsBarCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsBarCode").Value as string;
                        var claimBillCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "ClaimBillCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "ClaimBillCode").Value as string;
                        var ifBillable = filterItem.Filters.SingleOrDefault(r => r.MemberName == "IfBillable") == null ? null : filterItem.Filters.Single(r => r.MemberName == "IfBillable").Value as bool?;
                        var ifAlreadySettled = filterItem.Filters.SingleOrDefault(r => r.MemberName == "IfAlreadySettled") == null ? null : filterItem.Filters.Single(r => r.MemberName == "IfAlreadySettled").Value as bool?;
                        var outboundTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? outboundTimeStart = null;
                        DateTime? outboundTimeEnd = null;
                        if(outboundTime != null) {
                            outboundTimeStart = outboundTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            outboundTimeEnd = outboundTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportUsedPartsOutboundDetail(new int[] { }, code, outboundType, usedPartsWarehouseCode, usedPartsWarehouseName, outboundTimeStart, outboundTimeEnd, sourceCode, relatedCompanyCode, relatedCompanyName, usedPartsBarCode, claimBillCode, ifBillable, ifAlreadySettled);
                    }
                    break;
                case CommonActionKeys.EXPORT:
                    //如果选中一条数据 合并导出参数为 旧件出库单ID 
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<UsedPartsOutboundOrder>().Select(r => r.Id).ToArray();
                        this.ExportUsedPartsOutboundOrder(ids, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    }
                        //否则 导出参数为
                    else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var code = filterItem.Filters.SingleOrDefault(r => r.MemberName == "Code") == null ? null : filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var outboundType = filterItem.Filters.SingleOrDefault(r => r.MemberName == "OutboundType") == null ? null : filterItem.Filters.Single(r => r.MemberName == "OutboundType").Value as int?;
                        var usedPartsWarehouseCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsWarehouseCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouseCode").Value as string;
                        var usedPartsWarehouseName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsWarehouseName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouseName").Value as string;
                        var sourceCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "SourceCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "SourceCode").Value as string;
                        var relatedCompanyCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "RelatedCompanyCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "RelatedCompanyCode").Value as string;
                        var relatedCompanyName = filterItem.Filters.SingleOrDefault(r => r.MemberName == "RelatedCompanyName") == null ? null : filterItem.Filters.Single(r => r.MemberName == "RelatedCompanyName").Value as string;
                        var usedPartsBarCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "UsedPartsBarCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "UsedPartsBarCode").Value as string;
                        var claimBillCode = filterItem.Filters.SingleOrDefault(r => r.MemberName == "ClaimBillCode") == null ? null : filterItem.Filters.Single(r => r.MemberName == "ClaimBillCode").Value as string;
                        var ifBillable = filterItem.Filters.SingleOrDefault(r => r.MemberName == "IfBillable") == null ? null : filterItem.Filters.Single(r => r.MemberName == "IfBillable").Value as bool?;
                        var ifAlreadySettled = filterItem.Filters.SingleOrDefault(r => r.MemberName == "IfAlreadySettled") == null ? null : filterItem.Filters.Single(r => r.MemberName == "IfAlreadySettled").Value as bool?;
                        var outboundTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? outboundTimeStart = null;
                        DateTime? outboundTimeEnd = null;
                        if(outboundTime != null) {
                            outboundTimeStart = outboundTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            outboundTimeEnd = outboundTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }

                        this.ExportUsedPartsOutboundOrder(new int[] { }, code, outboundType, usedPartsWarehouseCode, usedPartsWarehouseName, outboundTimeStart, outboundTimeEnd, sourceCode, relatedCompanyCode, relatedCompanyName, usedPartsBarCode, claimBillCode, ifBillable, ifAlreadySettled);

                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.EXPORTDETAIL:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<UsedPartsOutboundOrder>().ToArray();
                    return selectItems.Length == 1;
                case "NotPartCodePrint":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems1 = this.DataGridView.SelectedEntities.Cast<UsedPartsOutboundOrder>().ToArray();
                    return selectItems1.Length == 1;
                default:
                    return false;
            }
        }


        //导出清单
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        private void ExportUsedPartsOutboundDetail(int[] usedPartsOutboundId, string code, int? outboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? outboundTimeStart, DateTime? outboundTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, string usedPartsBarCode, string claimBillCode, bool? ifBillable, bool? ifAlreadySettled) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportUsedPartsOutboundDetailAsync(usedPartsOutboundId, code, outboundType, usedPartsWarehouseCode, usedPartsWarehouseName, outboundTimeStart, outboundTimeEnd, sourceCode, relatedCompanyCode, relatedCompanyName, usedPartsBarCode, claimBillCode, ifBillable, ifAlreadySettled);
            this.excelServiceClient.ExportUsedPartsOutboundDetailCompleted -= ExcelServiceClientOnExportUsedPartsOutboundDetailCompleted;
            this.excelServiceClient.ExportUsedPartsOutboundDetailCompleted += ExcelServiceClientOnExportUsedPartsOutboundDetailCompleted;
        }

        private void ExcelServiceClientOnExportUsedPartsOutboundDetailCompleted(object sender, ExportUsedPartsOutboundDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
        //导出
        private void ExportUsedPartsOutboundOrder(int[] id, string code, int? outboundType, string usedPartsWarehouseCode, string usedPartsWarehouseName, DateTime? outboundTimeStart, DateTime? outboundTimeEnd, string sourceCode, string relatedCompanyCode, string relatedCompanyName, string usedPartsBarCode, string claimBillCode, bool? ifBillable, bool? ifAlreadySettled) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportUsedPartsOutboundOrderAsync(id, code, outboundType, usedPartsWarehouseCode, usedPartsWarehouseName, outboundTimeStart, outboundTimeEnd, sourceCode, relatedCompanyCode, relatedCompanyName, usedPartsBarCode, claimBillCode, ifBillable, ifAlreadySettled);
            this.excelServiceClient.ExportUsedPartsOutboundOrderCompleted -= ExcelServiceClientOnExportUsedPartsOutboundOrderCompleted;
            this.excelServiceClient.ExportUsedPartsOutboundOrderCompleted += ExcelServiceClientOnExportUsedPartsOutboundOrderCompleted;
        }

        private void ExcelServiceClientOnExportUsedPartsOutboundOrderCompleted(object sender, ExportUsedPartsOutboundOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

    }
}
