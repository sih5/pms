﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedWarehouseExecution", "UsedPartsReturnOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_EXPORT_EXPORTDETAIL,CommonActionKeys.PRINT
    })]
    public class UsedPartsReturnOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewApprove;
        private const string DATA_EDIT_VIEW_APPROVE = "_DataEditViewApprove_";

        public UsedPartsReturnOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsReturnOrder;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("UsedPartsReturnOrderWithDetails"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("UsedPartsReturnOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewApprove {
            get {
                if(this.dataEditViewApprove == null) {
                    this.dataEditViewApprove = DI.GetDataEditView("UsedPartsReturnOrderForApprove");
                    this.dataEditViewApprove.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewApprove.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewApprove;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_APPROVE, () => this.DataEditViewApprove);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewApprove = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private bool CheckRecordState() {
            if(this.DataGridView.SelectedEntities == null)
                return false;
            var entities = this.DataGridView.SelectedEntities.Cast<UsedPartsReturnOrder>().ToArray();
            if(entities.Length != 1)
                return false;
            return entities[0].Status == (int)DcsUsedPartsReturnOrderStatus.新建;
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "UsedPartsReturnOrder"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var usedPartsTransferOrder = this.DataEditView.CreateObjectToEdit<UsedPartsReturnOrder>();
                    usedPartsTransferOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    usedPartsTransferOrder.Status = (int)DcsUsedPartsReturnOrderStatus.新建;
                    usedPartsTransferOrder.OutboundStatus = (int)DcsUsedPartsOutboundStatus.待出库;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    if(!CheckRecordState()) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotEdit);
                        return;
                    }
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    if(!CheckRecordState()) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotApprove);
                        return;
                    }
                    this.DataEditViewApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_APPROVE);
                    break;
                case CommonActionKeys.ABANDON:
                    if(!CheckRecordState()) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotAbandon);
                        return;
                    }
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<UsedPartsReturnOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废旧件清退单)
                                entity.作废旧件清退单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORTDETAIL:
                    if(this.DataGridView.SelectedEntities.Any()) {
                        var selectItem = this.DataGridView.SelectedEntities.Cast<UsedPartsReturnOrder>().Select(r => r.Id).ToArray();
                        this.ExportUsedPartsReturnDetail(selectItem.ToArray());
                    } else {
                        UIHelper.ShowNotification(ServiceUIStrings.Validation_UsedPartsReturnOrder_DetailIsNull);

                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<UsedPartsReturnOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow;
                    printWindow = new UsedPartsReturnOrderPrintWindow {
                        Header = "旧件清退单打印",
                        UsedPartsReturnOrder = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var outboundWarehouseCode = filterItem.Filters.Single(r => r.MemberName == "OutboundWarehouseCode").Value as string;
                    var outboundWarehouseName = filterItem.Filters.Single(r => r.MemberName == "OutboundWarehouseName").Value as string;
                    var returnOfficeCode = filterItem.Filters.FirstOrDefault(r => r.MemberName == "ReturnOfficeCode").Value as string;
                    var returnOfficeName = filterItem.Filters.Single(r => r.MemberName == "ReturnOfficeName").Value as string;
                    var code = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                    var returnType = filterItem.Filters.Single(e => e.MemberName == "ReturnType").Value as int?;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<UsedPartsReturnOrder>().Select(e => e.Id).ToArray();
                        this.ExportUsedPartsReturnOrder(ids, BaseApp.Current.CurrentUserData.UserId, outboundWarehouseCode, outboundWarehouseName, returnOfficeCode, returnOfficeName, code, returnType, status, createTimeBegin, createTimeEnd);
                    } else {
                        this.ExportUsedPartsReturnOrder(null, BaseApp.Current.CurrentUserData.UserId, outboundWarehouseCode, outboundWarehouseName, returnOfficeCode, returnOfficeName, code, returnType, status, createTimeBegin, createTimeEnd);
                    }
                    break;
            }
        }
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        //导出
        private void ExportUsedPartsReturnOrder(int[] usedPartsReturnOrderIds, int personnelId, string outboundWarehouseCode, string outboundWarehouseName, string returnOfficeCode, string returnOfficeName, string code, int? returnType, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportUsedPartsReturnOrderAsync(usedPartsReturnOrderIds, personnelId, outboundWarehouseCode, outboundWarehouseName, returnOfficeCode, returnOfficeName, code, returnType, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportUsedPartsReturnOrderCompleted -= excelServiceClient_ExportUsedPartsReturnOrderCompleted;
            this.excelServiceClient.ExportUsedPartsReturnOrderCompleted += excelServiceClient_ExportUsedPartsReturnOrderCompleted;
        }

        private void excelServiceClient_ExportUsedPartsReturnOrderCompleted(object sender, ExportUsedPartsReturnOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        //导出清单
        private void ExportUsedPartsReturnDetail(int[] ids) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportUsedPartsReturnDetailAsync(ids);
            this.excelServiceClient.ExportUsedPartsReturnDetailCompleted -= excelServiceClient_ExportUsedPartsReturnDetailCompleted;
            this.excelServiceClient.ExportUsedPartsReturnDetailCompleted += excelServiceClient_ExportUsedPartsReturnDetailCompleted;
        }

        private void excelServiceClient_ExportUsedPartsReturnDetailCompleted(object sender, ExportUsedPartsReturnDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any();
                case CommonActionKeys.EXPORTDETAIL:
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1;
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<UsedPartsReturnOrder>().ToArray();
                    return selectItems.Length == 1;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
