﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedPartsTreatmentForBranch", "UsedPartsReturnPolicyHistoryQuery", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class UsedPartsReturnPolicyHistoryQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        public UsedPartsReturnPolicyHistoryQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsReturnPolicyHistory;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("UsedPartsReturnPolicyHistoryWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {

            var compositeFilterItem = filterItem as CompositeFilterItem ?? new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "BranchId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            ClientVar.ConvertTime(compositeFilterItem);
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "UsedPartsReturnPolicyHistory"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any()) {
                        var ids = this.DataGridView.SelectedEntities.Cast<UsedPartsReturnPolicyHistory>().Select(r => r.Id).ToArray();
                        ShellViewModel.Current.IsBusy = true;
                        this.dcsDomainContext.ExportUsedPartsReturnPolicyHistory(ids, null, null, null, null, null, null, null, null, null, null, loadOp => {
                            if(loadOp.HasError)
                                return;
                            if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                UIHelper.ShowNotification(loadOp.Value);
                                ShellViewModel.Current.IsBusy = false;
                            }
                            if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                ShellViewModel.Current.IsBusy = false;
                            }
                        }, null);
                    } else {
                        var compositeFilterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(compositeFilterItem != null) {
                            var dealerCode = compositeFilterItem.Filters.Single(r => r.MemberName == "Dealer.Code").Value as string;
                            var dealerName = compositeFilterItem.Filters.Single(r => r.MemberName == "Dealer.Name").Value as string;
                            var usedPartsCode = compositeFilterItem.Filters.Single(r => r.MemberName == "UsedPartsCode").Value as string;
                            var usedPartsName = compositeFilterItem.Filters.Single(r => r.MemberName == "UsedPartsName").Value as string;
                            var partsSalesCategoryId = compositeFilterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                            var usedPartsReturnPolicy = compositeFilterItem.Filters.Single(r => r.MemberName == "UsedPartsReturnPolicy").Value as int?;
                            var claimBillCode = compositeFilterItem.Filters.Single(r => r.MemberName == "ClaimBillCode").Value as string;
                            var usedPartsBarCode = compositeFilterItem.Filters.Single(r => r.MemberName == "UsedPartsBarCode").Value as string;
                            var createTime = compositeFilterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            DateTime? createtimeBegin = null;
                            DateTime? createtimeEnd = null;
                            if(createTime != null) {
                                createtimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createtimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            ShellViewModel.Current.IsBusy = true;
                            this.dcsDomainContext.ExportUsedPartsReturnPolicyHistory(null, dealerCode, dealerName, usedPartsCode, usedPartsName, partsSalesCategoryId, claimBillCode, usedPartsBarCode, usedPartsReturnPolicy, createtimeBegin, createtimeEnd, loadOp => {
                                if(loadOp.HasError)
                                    return;
                                if(loadOp.Value == null || string.IsNullOrEmpty(loadOp.Value)) {
                                    UIHelper.ShowNotification(loadOp.Value);
                                    ShellViewModel.Current.IsBusy = false;
                                }
                                if(loadOp.Value != null && !string.IsNullOrEmpty(loadOp.Value)) {
                                    HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(loadOp.Value));
                                    ShellViewModel.Current.IsBusy = false;
                                }
                            }, null);
                        }

                    }
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
