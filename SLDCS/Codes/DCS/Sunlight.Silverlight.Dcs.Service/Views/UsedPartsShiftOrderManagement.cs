﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedWarehouseExecution", "UsedPartsShiftOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EXPORT
    })]
    public class UsedPartsShiftOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        public UsedPartsShiftOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsShiftOrder;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("UsedPartsShiftOrderWithDetails"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("UsedPartsShiftOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilter = filterItem as CompositeFilterItem;
                var createTimeFilter = compositeFilter.Filters.SingleOrDefault(filter => (filter is CompositeFilterItem) && ((CompositeFilterItem)filter).Filters.Any(e => e.MemberName == "CreateTime")) as CompositeFilterItem;
                if(createTimeFilter != null && createTimeFilter.Filters.Count == 2) {
                    DateTime endTime;
                    if(createTimeFilter.Filters.ElementAt(1).Value is DateTime && DateTime.TryParse(createTimeFilter.Filters.ElementAt(1).Value.ToString(), out endTime)) {
                        createTimeFilter.Filters.ElementAt(1).Value = new DateTime(endTime.Year, endTime.Month, endTime.Day, 23, 59, 59);
                    }
                }
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "UsedPartsShiftOrder"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var usedPartsShiftOrder = this.DataEditView.CreateObjectToEdit<UsedPartsShiftOrder>();
                    usedPartsShiftOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    usedPartsShiftOrder.Status = (int)DcsUsedPartsShiftOrderStatus.生效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
