﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views {

    [PageMeta("OldParts", "UsedPartsTreatmentForBranch", "UsedPartsShippingOrderForConfirm", ActionPanelKeys = new[] {
        CommonActionKeys. CONFIRM_EXPORT_PRINT,"UsedPartsShippingOrder"
    })]
    public class UsedPartsShippingOrderForConfirmManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public UsedPartsShippingOrderForConfirmManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsShippingOrderForConfirm;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("UsedPartsShippingOrderForConfirm"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("UsedPartsShippingOrderForConfirm");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "UsedPartsShippingOrderForConfirm"
                };
            }
        }
        private RadWindow labelPrintWindow;
        private DataEditViewBase printLabelForUsedPartsShippingOrderDataEditView;

        private DataEditViewBase PrintLabelForUsedPartsShippingOrderDataEditView {
            get {
                return this.printLabelForUsedPartsShippingOrderDataEditView ?? (this.printLabelForUsedPartsShippingOrderDataEditView = DI.GetDataEditView("PrintLabelForUsedPartsShippingOrder"));
            }
        }

        private RadWindow LabelPrintWindow {
            get {
                return this.labelPrintWindow ?? (this.labelPrintWindow = new RadWindow {
                    Content = this.PrintLabelForUsedPartsShippingOrderDataEditView,
                    Header = "旧件发运清单",
                    WindowState = WindowState.Normal,
                    WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation.CenterScreen,
                    AllowDrop = false,
                    ResizeMode = ResizeMode.NoResize,
                    Width = 800,
                    CanClose = true
                });
            }
        }

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Confirm":
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrderWithOtherInfo>().FirstOrDefault().UsedPartsShippingOrderId);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;

                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1) {
                        this.ExportUsedPartsShippingOrderForConfirm(this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrderWithOtherInfo>().First().UsedPartsShippingOrderId, BaseApp.Current.CurrentUserData.UserId, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem1 = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem1 == null)
                            return;
                        var branchId = filterItem1.Filters.SingleOrDefault(e => e.MemberName == "BranchId").Value == null ? null : filterItem1.Filters.SingleOrDefault(e => e.MemberName == "BranchId").Value as int?;
                        var dealerCode = filterItem1.Filters.Single(r => r.MemberName == "DealerCode").Value as string;
                        var dealerName = filterItem1.Filters.Single(r => r.MemberName == "DealerName").Value as string;
                        var code = filterItem1.Filters.Single(r => r.MemberName == "UsedPartsShippingOrderCode").Value as string;
                        var partsSalesCategoryId = filterItem1.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var destinationWarehouseId = filterItem1.Filters.Single(r => r.MemberName == "UsedPartsWarehouseId").Value as int?;
                        var shippingMethod = filterItem1.Filters.Single(r => r.MemberName == "ShippingMethod").Value as int?;
                        var status = filterItem1.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTime = filterItem1.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportUsedPartsShippingOrderForConfirm(null, BaseApp.Current.CurrentUserData.UserId, branchId, partsSalesCategoryId, dealerCode, dealerName, code, shippingMethod, destinationWarehouseId, status, createTimeBegin, createTimeEnd);
                    }
                    break;

                case "ExportDetail":
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1) {
                        this.ExportUsedPartsShippingOrderForConfirmWithDetail(this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrderWithOtherInfo>().First().UsedPartsShippingOrderId, BaseApp.Current.CurrentUserData.UserId, null, null, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem1 = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem1 == null)
                            return;
                        var branchId = filterItem1.Filters.SingleOrDefault(e => e.MemberName == "BranchId").Value == null ? null : filterItem1.Filters.SingleOrDefault(e => e.MemberName == "BranchId").Value as int?;
                        var dealerCode = filterItem1.Filters.Single(r => r.MemberName == "DealerCode").Value as string;
                        var dealerName = filterItem1.Filters.Single(r => r.MemberName == "DealerName").Value as string;
                        var code = filterItem1.Filters.Single(r => r.MemberName == "UsedPartsShippingOrderCode").Value as string;
                        var partsSalesCategoryId = filterItem1.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var destinationWarehouseId = filterItem1.Filters.Single(r => r.MemberName == "UsedPartsWarehouseId").Value as int?;
                        var shippingMethod = filterItem1.Filters.Single(r => r.MemberName == "ShippingMethod").Value as int?;
                        var status = filterItem1.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTime = filterItem1.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportUsedPartsShippingOrderForConfirmWithDetail(null, BaseApp.Current.CurrentUserData.UserId, branchId, partsSalesCategoryId, dealerCode, dealerName, code, shippingMethod, destinationWarehouseId, status, createTimeBegin, createTimeEnd);
                    }
                    break;

                case "PrintBarCode":
                    var selected = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrderWithOtherInfo>().FirstOrDefault();
                    if(selected == null)
                        return;
                    BasePrintWindow lablePrintWindow = new UsedPartsShippingOrderForLablePrintWindow {
                        Header = "旧件条码标签打印",
                        UsedPartsShippingOrderId = selected.UsedPartsShippingOrderId
                    };
                    lablePrintWindow.ShowDialog();
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrderWithOtherInfo>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new UsedPartsShippingOrderPrintWindow {
                        Header = "服务站旧件发运单打印",
                        UsedPartsShippingOrderId = selectedItem.UsedPartsShippingOrderId
                    };
                    printWindow.ShowDialog();
                    break;
                case "PrintChooseBarCode":
                    var usedPartsShippingOrder = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrderWithOtherInfo>().FirstOrDefault();
                    this.PrintLabelForUsedPartsShippingOrderDataEditView.SetObjectToEditById(usedPartsShippingOrder.UsedPartsShippingOrderId);
                    LabelPrintWindow.ShowDialog();
                    break;
                case "Abandon":
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrderWithOtherInfo>().SingleOrDefault();
                        if(entity == null)
                            return;
                        ShellViewModel.Current.IsBusy = true;
                        dcsDomainContext.Load(dcsDomainContext.GetUsedPartsShippingOrdersQuery().Where(ex => ex.Id == entity.UsedPartsShippingOrderId), LoadBehavior.RefreshCurrent, loadOp => {
                            if(loadOp.HasError) {
                                if(!loadOp.IsErrorHandled)
                                    loadOp.MarkErrorAsHandled();
                                DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                                return;
                            }
                            var items = loadOp.Entities.SingleOrDefault();
                            if(items == null)
                                return;
                            try {
                                if(items.Can作废旧件发运单)
                                    items.作废旧件发运单();
                                dcsDomainContext.SubmitChanges(submitOp => {
                                    if(submitOp.HasError) {
                                        if(!submitOp.IsErrorHandled)
                                            submitOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                        dcsDomainContext.RejectChanges();
                                        return;
                                    }
                                    UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_AbandonSuccess);
                                    this.CheckActionsCanExecute();
                                    ShellViewModel.Current.IsBusy = false;
                                    this.DataGridView.ExecuteQuery();
                                }, null);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        }, null);
                    });
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case "Confirm":
                case "Abandon":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrderWithOtherInfo>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsUsedPartsShippingOrderStatus.新增;

                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrderWithOtherInfo>().ToArray();
                    return selectItems.Length == 1;
                case "ExportDetail":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();

                case "PrintBarCode":
                case "PrintChooseBarCode":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var select = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrderWithOtherInfo>().ToArray();
                    return select.Length == 1;
                default:
                    return false;
            }
        }

        //导出
        private void ExportUsedPartsShippingOrderForConfirm(int? billId, int personnelId, int? branchId, int? partsSalesCategoryId, string dealerCode, string dealerName, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportUsedPartsShippingOrderForConfirmAsync(billId, personnelId, branchId, partsSalesCategoryId, dealerCode, dealerName, code, shippingMethod, destinationwarehouseid, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportUsedPartsShippingOrderForConfirmCompleted -= excelServiceClient_ExportUsedPartsShippingOrderForConfirmCompleted;
            this.excelServiceClient.ExportUsedPartsShippingOrderForConfirmCompleted += excelServiceClient_ExportUsedPartsShippingOrderForConfirmCompleted;
        }

        private void excelServiceClient_ExportUsedPartsShippingOrderForConfirmCompleted(object sender, ExportUsedPartsShippingOrderForConfirmCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        //导出清单
        private void ExportUsedPartsShippingOrderForConfirmWithDetail(int? billId, int personnelId, int? branchId, int? partsSalesCategoryId, string dealerCode, string dealerName, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportUsedPartsShippingOrderForConfirmWithDetailAsync(billId, personnelId, branchId, partsSalesCategoryId, dealerCode, dealerName, code, shippingMethod, destinationwarehouseid, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportUsedPartsShippingOrderForConfirmWithDetailCompleted -= excelServiceClient_ExportUsedPartsShippingOrderForConfirmWithDetailCompleted;
            this.excelServiceClient.ExportUsedPartsShippingOrderForConfirmWithDetailCompleted += excelServiceClient_ExportUsedPartsShippingOrderForConfirmWithDetailCompleted;
        }

        private void excelServiceClient_ExportUsedPartsShippingOrderForConfirmWithDetailCompleted(object sender, ExportUsedPartsShippingOrderForConfirmWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}