﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedPartsTreatmentForServiceStation", "UsedPartsShippingOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT_PRINT,"UsedPartsShippingOrder"
    })]
    public class UsedPartsShippingOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewForEdit;
        private const string DATA_FOREDIT_VIEW = "_dataForEditView_";

        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public UsedPartsShippingOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsShippingOrder;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("UsedPartsShippingOrderWithDetails"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("UsedPartsShippingOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewForEdit {
            get {
                if(this.dataEditViewForEdit == null) {
                    this.dataEditViewForEdit = DI.GetDataEditView("UsedPartsShippingOrderForEdit");
                    this.dataEditViewForEdit.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewForEdit.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewForEdit;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_FOREDIT_VIEW, () => this.DataEditViewForEdit);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewForEdit = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "UsedPartsShippingOrder"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilter = new CompositeFilterItem();
            if(filterItem is CompositeFilterItem) {
                newCompositeFilter = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(newCompositeFilter);
            } else
                newCompositeFilter.Filters.Add(filterItem);
            newCompositeFilter.Filters.Add(new FilterItem {
                MemberName = "DealerId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            this.DataGridView.FilterItem = newCompositeFilter;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var usedPartsShippingOrder = this.DataEditView.CreateObjectToEdit<UsedPartsShippingOrder>();
                    usedPartsShippingOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    usedPartsShippingOrder.DealerId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    usedPartsShippingOrder.DealerCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    usedPartsShippingOrder.DealerName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    usedPartsShippingOrder.Dispatcher = (int)DcsUsedPartsShippingOrderDispatcher.经销商发运;
                    usedPartsShippingOrder.RequestedArrivalDate = DateTime.Now;
                    usedPartsShippingOrder.ShippingDate = DateTime.Now;
                    usedPartsShippingOrder.Status = (int)DcsUsedPartsShippingOrderStatus.新增;
                    usedPartsShippingOrder.InboundStatus = (int)DcsUsedPartsInboundStatus.待入库;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditViewForEdit.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_FOREDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废旧件发运单)
                                entity.作废旧件发运单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1) {
                        this.ExportUsedPartsShippingOrder(this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrder>().First().Id, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem1 = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem1 == null)
                            return;
                        var branchId = filterItem1.Filters.Single(e => e.MemberName == "BranchId").Value as int?;
                        var code = filterItem1.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var partsSalesCategoryId = filterItem1.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var destinationWarehouseId = filterItem1.Filters.Single(r => r.MemberName == "DestinationWarehouseId").Value as int?;
                        var shippingMethod = filterItem1.Filters.Single(r => r.MemberName == "ShippingMethod").Value as int?;
                        var status = filterItem1.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTime = filterItem1.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeBegin = null;
                        DateTime? createTimeEnd = null;
                        if(createTime != null) {
                            createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportUsedPartsShippingOrder(null, branchId, partsSalesCategoryId, code, shippingMethod, destinationWarehouseId, status, createTimeBegin, createTimeEnd);
                    }
                    break;
                case CommonActionKeys.PRINT:
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new UsedPartsShippingOrderPrintWindow {
                        Header = "服务站旧件发运单打印",
                        UsedPartsShippingOrderId = selectedItem.Id
                    };
                    printWindow.ShowDialog();
                    break;
                case "ExportDetail":
                    //List<string> codes = new List<string>();
                    //var entityC = this.DataGridView.Entities.Cast<UsedPartsShippingOrder>().ToArray();
                    //foreach(var item in entityC) {
                    //    codes.Add(item.Code);
                    //}
                    if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1) {
                        this.ExportUsedPartsShippingOrderWithDetail(this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrder>().First().Id, null, null, null, null, null, null, null, null);
                    } else {
                        var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                        if(filterItem == null)
                            return;
                        var branchId1 = filterItem.Filters.Single(e => e.MemberName == "BranchId").Value as int?;
                        var code1 = filterItem.Filters.Single(r => r.MemberName == "Code").Value as string;
                        var partsSalesCategoryId1 = filterItem.Filters.Single(r => r.MemberName == "PartsSalesCategoryId").Value as int?;
                        var destinationWarehouseId1 = filterItem.Filters.Single(r => r.MemberName == "DestinationWarehouseId").Value as int?;
                        var shippingMethod1 = filterItem.Filters.Single(r => r.MemberName == "ShippingMethod").Value as int?;
                        var status1 = filterItem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                        var createTime1 = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? createTimeB = null;
                        DateTime? createTimeE = null;
                        if(createTime1 != null) {
                            createTimeB = createTime1.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            createTimeE = createTime1.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }
                        this.ExportUsedPartsShippingOrderWithDetail(null, branchId1, partsSalesCategoryId1, code1, shippingMethod1, destinationWarehouseId1, status1, createTimeB, createTimeE);
                    }
                    break;
                case "PrintBarCode":
                    var selected = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrder>().FirstOrDefault();
                    if(selected == null)
                        return;
                    BasePrintWindow lablePrintWindow = new UsedPartsShippingOrderForLablePrintWindow {
                        Header = "旧件条码标签打印",
                        UsedPartsShippingOrderId = selected.Id
                    };
                    lablePrintWindow.ShowDialog();
                    break;

            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsUsedPartsShippingOrderStatus.新增;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var selectItems = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrder>().ToArray();
                    return selectItems.Length == 1;
                case "ExportDetail":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "PrintBarCode":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var select = this.DataGridView.SelectedEntities.Cast<UsedPartsShippingOrder>().ToArray();
                    return select.Length == 1;
                default:
                    return false;
            }
        }

        //导出
        private void ExportUsedPartsShippingOrder(int? billId, int? branchId, int? partsSalesCategoryId, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportUsedPartsShippingOrderAsync(billId, branchId, partsSalesCategoryId, code, shippingMethod, destinationwarehouseid, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportUsedPartsShippingOrderCompleted -= excelServiceClient_ExportUsedPartsShippingOrderCompleted;
            this.excelServiceClient.ExportUsedPartsShippingOrderCompleted += excelServiceClient_ExportUsedPartsShippingOrderCompleted;
        }

        private void excelServiceClient_ExportUsedPartsShippingOrderCompleted(object sender, ExportUsedPartsShippingOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        //导出清单
        private void ExportUsedPartsShippingOrderWithDetail(int? billId, int? branchId, int? partsSalesCategoryId, string code, int? shippingMethod, int? destinationwarehouseid, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd) {
            ShellViewModel.Current.IsBusy = true;
            this.excelServiceClient.ExportUsedPartsShippingOrderWithDetailAsync(billId, branchId, partsSalesCategoryId, code, shippingMethod, destinationwarehouseid, status, createTimeBegin, createTimeEnd);
            this.excelServiceClient.ExportUsedPartsShippingOrderWithDetailCompleted -= excelServiceClient_ExportUsedPartsShippingOrderWithDetailCompleted;
            this.excelServiceClient.ExportUsedPartsShippingOrderWithDetailCompleted += excelServiceClient_ExportUsedPartsShippingOrderWithDetailCompleted;
        }

        private void excelServiceClient_ExportUsedPartsShippingOrderWithDetailCompleted(object sender, ExportUsedPartsShippingOrderWithDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }
    }
}
