﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "OldPartsStock", "UsedPartsStock", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT,"UsedPartsStock"
    })]
    public class UsedPartsStockQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();
        public UsedPartsStockQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsStock;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("UsedPartsStock"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "UsedPartsStock"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    ShellViewModel.Current.IsBusy = true;
                    var usedPartsWarehouseId = filterItem.Filters.Single(r => r.MemberName == "UsedPartsWarehouseId").Value as int?;
                    var usedPartsBarCode = filterItem.Filters.Single(r => r.MemberName == "UsedPartsBarCode").Value as string;
                    var claimBillCode = filterItem.Filters.Single(e => e.MemberName == "ClaimBillCode").Value as string;
                    var claimBillType = filterItem.Filters.Single(e => e.MemberName == "ClaimBillType").Value as int?;
                    var code = filterItem.Filters.Single(e => e.MemberName == "UsedPartsWarehouseArea.Code").Value as string;
                    var usedPartsCode = filterItem.Filters.Single(e => e.MemberName == "UsedPartsCode").Value as string;
                    var usedPartsName = filterItem.Filters.Single(e => e.MemberName == "UsedPartsName").Value as string;
                    var usedPartsSupplierCode = filterItem.Filters.Single(e => e.MemberName == "UsedPartsSupplierCode").Value as string;
                    var usedPartsSupplierName = filterItem.Filters.Single(e => e.MemberName == "UsedPartsSupplierName").Value as string;
                    var faultyPartsSupplierName = filterItem.Filters.Single(e => e.MemberName == "FaultyPartsSupplierName").Value as string;
                    var faultyPartsSupplierCode = filterItem.Filters.Single(e => e.MemberName == "FaultyPartsSupplierCode").Value as string;
                    var responsibleUnitCode = filterItem.Filters.Single(e => e.MemberName == "ResponsibleUnitCode").Value as string;
                    var responsibleUnitName = filterItem.Filters.Single(e => e.MemberName == "ResponsibleUnitName").Value as string;
                    var ifFaultyParts = filterItem.Filters.Single(e => e.MemberName == "IfFaultyParts").Value as bool?;
                    var createTime = filterItem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    if(createTime != null) {
                        createTimeBegin = createTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        createTimeEnd = createTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                    }
                    ShellViewModel.Current.IsBusy = true;
                    this.excelServiceClient.ExportUsedPartsStockAsync(usedPartsWarehouseId, usedPartsBarCode, claimBillCode, claimBillType, code, usedPartsCode, usedPartsName, usedPartsSupplierCode, usedPartsSupplierName, faultyPartsSupplierCode, faultyPartsSupplierName, responsibleUnitCode, responsibleUnitName, createTimeBegin, createTimeEnd, ifFaultyParts);
                    this.excelServiceClient.ExportUsedPartsStockCompleted -= this.ExcelServiceClient_ExportUsedPartsStockCompleted;
                    this.excelServiceClient.ExportUsedPartsStockCompleted += this.ExcelServiceClient_ExportUsedPartsStockCompleted;
                    break;
                case "PrintBarCode":
                    var selected = this.DataGridView.SelectedEntities.Cast<UsedPartsStock>();
                    if(selected == null)
                        return;
                    UsedPartsStock.Clear();
                    var aa = "";
                    foreach(var usedPartsStock in selected) {
                        if(aa == "") {
                            aa = usedPartsStock.Id.ToString();
                            continue;
                        }
                        aa = aa + "," + usedPartsStock.Id;
                    }
                    BasePrintWindow lablePrintWindow = new UsedPartsStockForLablePrintWindow {
                        Header = "旧件条码标签打印",
                        UsedPartsStock = aa
                    };
                    lablePrintWindow.ShowDialog();
                    break;
            }

        }

        private void ExcelServiceClient_ExportUsedPartsStockCompleted(object sender, ExportUsedPartsStockCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "PrintBarCode":
                    return this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "UsedPartsWarehouse.BranchId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                ClientVar.ConvertTime(compositeFilterItem);
            }

            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private ObservableCollection<UsedPartsStock> usedPartsStock;
        public ObservableCollection<UsedPartsStock> UsedPartsStock {
            get {
                return usedPartsStock ?? (this.usedPartsStock = new ObservableCollection<UsedPartsStock>());
            }
        }
    }
}