﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Print;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Sunlight.Silverlight.ViewModel;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedWarehouseExecution", "UsedPartsTransferOrder", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_TERMINATE_EXPORT,"UsedPartsTransferOrder"
    })]
    public class UsedPartsTransferOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewApprove;
        private const string DATA_EDIT_VIEW_APPROVE = "_DataEditViewApprove_";
        private readonly ExcelServiceClient excelServiceClient = new ExcelServiceClient();

        public UsedPartsTransferOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsTransferOrder;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("UsedPartsTransferOrder"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("UsedPartsTransferOrder");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataEditViewApprove {
            get {
                if(this.dataEditViewApprove == null) {
                    this.dataEditViewApprove = DI.GetDataEditView("UsedPartsTransferOrderForApprove");
                    this.dataEditViewApprove.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewApprove.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewApprove;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_APPROVE, () => this.DataEditViewApprove);
        }
        private void ResetEditView() {
            this.dataEditView = null;
            this.dataEditViewApprove = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private bool CheckRecordState(string uniqueId) {
            if(this.DataGridView.SelectedEntities == null)
                return false;
            var entities = this.DataGridView.SelectedEntities.Cast<UsedPartsTransferOrder>().ToArray();
            if(entities.Length != 1)
                return false;
            if(uniqueId == CommonActionKeys.TERMINATE)
                return (entities[0].Status == (int)DcsUsedPartsTransferOrderStatus.生效 || entities[0].Status == (int)DcsUsedPartsTransferOrderStatus.新建);
            return entities[0].Status == (int)DcsUsedPartsTransferOrderStatus.新建;
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "UsedPartsTransferOrder"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            ClientVar.ConvertTime(compositeFilterItem);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var usedPartsTransferOrder = this.DataEditView.CreateObjectToEdit<UsedPartsTransferOrder>();
                    usedPartsTransferOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    usedPartsTransferOrder.Status = (int)DcsUsedPartsTransferOrderStatus.新建;
                    usedPartsTransferOrder.OutboundStatus = (int)DcsUsedPartsOutboundStatus.待出库;
                    usedPartsTransferOrder.InboundStatus = (int)DcsUsedPartsInboundStatus.待入库;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    if(!CheckRecordState(uniqueId)) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotEdit);
                        return;
                    }
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.APPROVE:
                    if(!CheckRecordState(uniqueId)) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotApprove);
                        return;
                    }
                    this.DataEditViewApprove.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_APPROVE);
                    break;
                case CommonActionKeys.ABANDON:
                    if(!CheckRecordState(uniqueId)) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_StatusIsNotNewCanNotAbandon);
                        return;
                    }
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<UsedPartsTransferOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废旧件调拨单)
                                entity.作废旧件调拨单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.TERMINATE:
                    if(!CheckRecordState(uniqueId)) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_StatusIsNotAvailableCanNotTerminate);
                        return;
                    }
                    DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Terminate, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<UsedPartsTransferOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can终止旧件调拨单)
                                entity.终止旧件调拨单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_TerminateSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                case "ExportDetail":
                    var filterItem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterItem == null)
                        return;
                    var code = filterItem.Filters.Single(e => e.MemberName == "Code").Value as string;
                    var originWarehouseCode = filterItem.Filters.Single(e => e.MemberName == "OriginWarehouseCode").Value as string;
                    var destinationWarehouseCode = filterItem.Filters.Single(e => e.MemberName == "DestinationWarehouseCode").Value as string;
                    var originWarehouseName = filterItem.Filters.Single(e => e.MemberName == "OriginWarehouseName").Value as string;
                    var destinationWarehouseName = filterItem.Filters.Single(e => e.MemberName == "DestinationWarehouseName").Value as string;
                    var outboundStatus = filterItem.Filters.Single(e => e.MemberName == "OutboundStatus").Value as int?;
                    var inboundStatus = filterItem.Filters.Single(e => e.MemberName == "InboundStatus").Value as int?;
                    var status = filterItem.Filters.Single(e => e.MemberName == "Status").Value as int?;
                    var compositeFilterItems = filterItem.Filters.Where(r => r.GetType() == typeof(CompositeFilterItem)).ToArray();
                    DateTime? createTimeBegin = null;
                    DateTime? createTimeEnd = null;
                    DateTime? approveTimeBegin = null;
                    DateTime? approveTimeEnd = null;
                    foreach(var dateTimeFilterItem in compositeFilterItems) {
                        var dateTime = dateTimeFilterItem as CompositeFilterItem;
                        if(dateTime != null) {
                            if(dateTime.Filters.Any(r => r.MemberName == "CreateTime")) {
                                createTimeBegin = dateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                                createTimeEnd = dateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                            }
                            if(dateTime.Filters.Any(r => r.MemberName == "ApproveTime")) {
                                approveTimeBegin = dateTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                                approveTimeEnd = dateTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                            }
                        }
                    }
                    if(CommonActionKeys.EXPORT.Equals(uniqueId)) {
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1)
                            this.excelServiceClient.ExportUsedPartsTransferOrderAsync(this.DataGridView.SelectedEntities.Cast<UsedPartsTransferOrder>().First().Id, BaseApp.Current.CurrentUserData.UserId, code, status, originWarehouseCode, destinationWarehouseCode, originWarehouseName, destinationWarehouseName, outboundStatus, inboundStatus, createTimeBegin, createTimeEnd, approveTimeBegin, approveTimeEnd);
                        else
                            this.excelServiceClient.ExportUsedPartsTransferOrderAsync(null, BaseApp.Current.CurrentUserData.UserId, code, status, originWarehouseCode, destinationWarehouseCode, originWarehouseName, destinationWarehouseName, outboundStatus, inboundStatus, createTimeBegin, createTimeEnd, approveTimeBegin, approveTimeEnd);
                        ShellViewModel.Current.IsBusy = true;
                        this.excelServiceClient.ExportUsedPartsTransferOrderCompleted -= excelServiceClient_ExportUsedPartsTransferOrderCompleted;
                        this.excelServiceClient.ExportUsedPartsTransferOrderCompleted += excelServiceClient_ExportUsedPartsTransferOrderCompleted;

                    }
                    if("ExportDetail".Equals(uniqueId)) {
                        if(this.DataGridView.SelectedEntities != null && this.DataGridView.SelectedEntities.Count() == 1)
                            this.excelServiceClient.ExportUsedPartsTransferDetailAsync(this.DataGridView.SelectedEntities.Cast<UsedPartsTransferOrder>().Select(r => r.Id).ToArray(), BaseApp.Current.CurrentUserData.UserId, null, null, null, null, null, null, null, null, null, null, null, null);
                        else
                            this.excelServiceClient.ExportUsedPartsTransferDetailAsync(new int[] {
                            }, BaseApp.Current.CurrentUserData.UserId, code, status, originWarehouseCode, destinationWarehouseCode, originWarehouseName, destinationWarehouseName, outboundStatus, inboundStatus, createTimeBegin, createTimeEnd, approveTimeBegin, approveTimeEnd);
                        this.excelServiceClient.ExportUsedPartsTransferDetailCompleted -= excelServiceClient_ExportUsedPartsTransferDetailCompleted;
                        this.excelServiceClient.ExportUsedPartsTransferDetailCompleted += excelServiceClient_ExportUsedPartsTransferDetailCompleted;

                    }
                    break;
                case "PartsTransferOrderPrint":
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<UsedPartsTransferOrder>().FirstOrDefault();
                    if(selectedItem == null)
                        return;
                    BasePrintWindow printWindow = new UsedPartsTransferOrderPrintWindow {
                        Header = "旧件调拨单打印",
                        UsedPartsTransferOrder = selectedItem
                    };
                    printWindow.ShowDialog();
                    break;
                case "CodePrint":
                    var selected = this.DataGridView.SelectedEntities.Cast<UsedPartsTransferOrder>().FirstOrDefault();
                    if(selected == null)
                        return;
                    BasePrintWindow lablePrintWindow = new UsedPartsTransferOrderForLablePrintWindow {
                        Header = "旧件条码标签打印",
                        UsedPartsTransferOrder = selected
                    };
                    lablePrintWindow.ShowDialog();
                    break;
            }
        }

        void excelServiceClient_ExportUsedPartsTransferDetailCompleted(object sender, ExportUsedPartsTransferDetailCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        private void excelServiceClient_ExportUsedPartsTransferOrderCompleted(object sender, ExportUsedPartsTransferOrderCompletedEventArgs e) {
            ShellViewModel.Current.IsBusy = false;
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(e.fileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                case CommonActionKeys.TERMINATE:
                case "PartsTransferOrderPrint":
                case "CodePrint":
                case "ExportDetail":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
