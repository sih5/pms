﻿using System;
using System.ComponentModel;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Service.Resources;
using Sunlight.Silverlight.Dcs.Service.Views.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.Service.Views {
    [PageMeta("OldParts", "UsedWarehouseStructure", "UsedPartsWarehouseArea", ActionPanelKeys = new[] {
            "UsedPartsWarehouseArea"
    })]
    public class UsedPartsWarehouseAreaManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private UsedPartsWarehouseAreaDataView usedPartsWarehouseAreaDataView;
        private const string DATA_VIEW_WAREHOUSEAREAVIEW = "_DataViewWarehouseAreaBaseView_";

        public UsedPartsWarehouseAreaManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = ServiceUIStrings.DataManagementView_Title_UsedPartsWarehouseArea;
        }

        private UsedPartsWarehouseAreaDataView UsedPartsWarehouseAreaDataView {
            get {
                if(this.usedPartsWarehouseAreaDataView == null) {
                    this.usedPartsWarehouseAreaDataView = new UsedPartsWarehouseAreaDataView();
                    this.usedPartsWarehouseAreaDataView.PropertyChanged += this.CurrentSelectedItem_PropertyChanged;
                    this.usedPartsWarehouseAreaDataView.RadMenuItems.Add(new DcsMenuItem {
                        Header = ServiceUIStrings.MenuItem_Text_NewStockArea,
                        Command = new DelegateCommand(o => this.AddNewUsedPartsWarehouseArea()),
                        CheckMenuVisiable = () => this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem != null && this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.StorageAreaType != (int)DcsAreaKind.仓库 && this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.Status == (int)DcsBaseDataStatus.有效 && this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.StorageAreaType != (int)DcsAreaKind.库位
                    });
                    this.usedPartsWarehouseAreaDataView.RadMenuItems.Add(new DcsMenuItem {
                        Header = ServiceUIStrings.MenuItem_Text_NewStockPosition,
                        Command = new DelegateCommand(o => {
                            var warehouseArea = this.DataEditView.CreateObjectToEdit<UsedPartsWarehouseArea>();
                            warehouseArea.ParentId = this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.Id;
                            warehouseArea.Status = (int)DcsBaseDataStatus.有效;
                            warehouseArea.StorageAreaType = (int)DcsAreaKind.库位;
                            warehouseArea.StorageCategory = this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.StorageCategory;
                            warehouseArea.TopLevelUsedPartsWhseAreaId = this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.Id;
                            this.SwitchViewTo(DATA_EDIT_VIEW);
                        }),
                        CheckMenuVisiable = () => this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem != null && this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.StorageAreaType == (int)DcsAreaKind.库区 && this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.Status == (int)DcsBaseDataStatus.有效
                    });
                    this.usedPartsWarehouseAreaDataView.RadMenuItems.Add(new DcsMenuItem {
                        Header = ServiceUIStrings.MenuItem_Text_Edit,
                        Command = new DelegateCommand(o => {
                            this.DataEditView.SetObjectToEditById(this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.Id);
                            this.SwitchViewTo(DATA_EDIT_VIEW);
                        }),
                        CheckMenuVisiable = () => this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem != null && this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.Status == (int)DcsBaseDataStatus.有效 && this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.StorageAreaType != (int)DcsAreaKind.仓库
                    });
                    this.usedPartsWarehouseAreaDataView.RadMenuItems.Add(new DcsMenuItem {
                        Header = ServiceUIStrings.MenuItem_Text_Abandon,
                        Command = new DelegateCommand(o => this.AbandonUsedPartsWarehouseArea()),
                        CheckMenuVisiable = () => this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem != null && this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.Status == (int)DcsBaseDataStatus.有效 && this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.StorageAreaType != (int)DcsAreaKind.仓库
                    });
                }
                return this.usedPartsWarehouseAreaDataView;
            }
        }

        private void CurrentSelectedItem_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            switch(e.PropertyName) {
                case "CurrentSelectedItem":
                    this.CheckActionsCanExecute();
                    break;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_VIEW_WAREHOUSEAREAVIEW, () => this.UsedPartsWarehouseAreaDataView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.SwitchViewTo(DATA_VIEW_WAREHOUSEAREAVIEW);
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("UsedPartsWarehouseArea");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }
        private void ResetEditView() {
            this.dataEditView = null;
        }
        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            this.ResetEditView();
            this.UsedPartsWarehouseAreaDataView.RefreshTreeView();
            this.SwitchViewTo(DATA_VIEW_WAREHOUSEAREAVIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.ResetEditView();
            this.SwitchViewTo(DATA_VIEW_WAREHOUSEAREAVIEW);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "DefaultWarehouseArea":
                    if(this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem == null)
                        return false;
                    return this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.StorageAreaType == (int)DcsAreaKind.库位 && this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.IfDefaultStoragePosition == false;
                default:
                    return this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem != null && this.CurrentViewKey == DATA_VIEW_WAREHOUSEAREAVIEW;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    if(this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.StorageAreaType == (int)DcsAreaKind.库位) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_UsedPartsWarehouseArea_StorePositionCanNotNewChild);
                        return;
                    }
                    this.AddNewUsedPartsWarehouseArea();
                    break;
                case CommonActionKeys.EDIT:
                    if(this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.StorageAreaType == (int)DcsAreaKind.仓库) {
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_UsedPartsWarehouseArea_WarehouseCanNotEdit);
                        return;
                    }
                    this.DataEditView.SetObjectToEditById(this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.Id);
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    AbandonUsedPartsWarehouseArea();
                    break;
                case "DefaultWarehouseArea":
                    DcsUtils.Confirm("确定要修改所选数据为默认库位吗？", () => {
                        var entity = this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem;
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can指定为默认库位)
                                entity.指定为默认库位();
                            var domainContext = this.UsedPartsWarehouseAreaDataView.DomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification("所选数据修改成功");
                                this.UsedPartsWarehouseAreaDataView.RefreshTreeView();
                                this.SwitchViewTo(DATA_VIEW_WAREHOUSEAREAVIEW);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        private void AddNewUsedPartsWarehouseArea() {
            var usedPartsWarehouseArea = this.DataEditView.CreateObjectToEdit<UsedPartsWarehouseArea>();
            usedPartsWarehouseArea.ParentId = this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.Id;
            usedPartsWarehouseArea.IfDefaultStoragePosition = false;
            usedPartsWarehouseArea.Status = (int)DcsBaseDataStatus.有效;
            if(this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.StorageAreaType == (int)DcsAreaKind.仓库)
                usedPartsWarehouseArea.StorageAreaType = (int)DcsAreaKind.库区;
            else {
                usedPartsWarehouseArea.StorageCategory = this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.StorageCategory;
                usedPartsWarehouseArea.TopLevelUsedPartsWhseAreaId = this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.TopLevelUsedPartsWhseAreaId.HasValue ? this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.TopLevelUsedPartsWhseAreaId.Value : this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem.Id;
            }
            this.SwitchViewTo(DATA_EDIT_VIEW);
        }

        private void AbandonUsedPartsWarehouseArea() {
            DcsUtils.Confirm(ServiceUIStrings.DataManagementView_Confirm_Abandon, () => {
                var entity = this.UsedPartsWarehouseAreaDataView.CurrentSelectedItem;
                if(entity == null)
                    return;
                try {
                    if(entity.Can作废旧件库区库位)
                        entity.作废旧件库区库位();
                    var domainContext = this.UsedPartsWarehouseAreaDataView.DomainContext;
                    if(domainContext == null)
                        return;
                    domainContext.SubmitChanges(submitOp => {
                        if(submitOp.HasError) {
                            if(!submitOp.IsErrorHandled)
                                submitOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                            domainContext.RejectChanges();
                            return;
                        }
                        this.UsedPartsWarehouseAreaDataView.CancelTreeItem(entity.Id);
                        UIHelper.ShowNotification(ServiceUIStrings.DataManagementView_Notification_AbandonSuccess);
                        this.CheckActionsCanExecute();
                    }, null);
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            });
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
        }
    }
}
