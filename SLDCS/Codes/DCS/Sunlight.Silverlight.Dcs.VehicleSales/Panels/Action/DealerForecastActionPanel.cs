﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;


namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class DealerForecastActionPanel : DcsActionPanelBase {
        public DealerForecastActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "DealerForecast",
                Title = VehicleSalesUIStrings.ActionPanel_Title_DealerForecast,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_RegionManagerApprove,
                        UniqueId = "RegionManagerApprove",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
