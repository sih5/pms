﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class DealerTransactionBillActionPanel : DcsActionPanelBase {
        public DealerTransactionBillActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "DealerTransactionBill",
                ActionItems = new[] {
                    new ActionItem {
                        Title =VehicleSalesUIStrings.Action_Title_OTDConfirm,
                        UniqueId ="OTDConfirm",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/OTDConfirm.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
