﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class ProductActionPanel : DcsActionPanelBase {
        public ProductActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Product",
                Title = VehicleSalesUIStrings.ActionPanel_Title_Product,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_AddProduct,
                        UniqueId = "AddProduct",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AddProduct.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_EditProduct,
                        UniqueId = "EditProduct",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/EditProduct.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_AbandonProduct,
                        UniqueId = "AbandonProduct",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AbandonProduct.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_ResumeProduct,
                        UniqueId = "ResumeProduct",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ResumeProduct.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
