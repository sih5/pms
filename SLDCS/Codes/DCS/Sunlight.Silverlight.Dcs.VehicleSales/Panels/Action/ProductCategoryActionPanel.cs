﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class ProductCategoryActionPanel : DcsActionPanelBase {
        public ProductCategoryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "ProductCategory",
                Title = VehicleSalesUIStrings.ActionPanel_Title_ProductCategory,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_AddRootProductCategory,
                        UniqueId = "AddRootProductCategory",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AddRootProductCategory.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_AddProductCategory,
                        UniqueId = "AddProductCategory",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AddProductCategory.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_EditProductCategory,
                        UniqueId = "EditProductCategory",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/EditProductCategory.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_AbandonProductCategory,
                        UniqueId = "AbandonProductCategory",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/AbandonProductCategory.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
