﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class SpecialVehicleInformationActionPanel : DcsActionPanelBase {
        public SpecialVehicleInformationActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "SpecialVehicleInformation",
                ActionItems = new[] {
                    new ActionItem {
                        Title =VehicleSalesUIStrings.Action_Title_Trade,
                        UniqueId ="Trade",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Trade.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
