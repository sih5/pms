﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VINChangeRecordActionPanel : DcsActionPanelBase {
        public VINChangeRecordActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehicleInformation,
                UniqueId = "VINChangeRecord",
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_VINEdit,
                        UniqueId = "VINEdit",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/VINEdit.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
