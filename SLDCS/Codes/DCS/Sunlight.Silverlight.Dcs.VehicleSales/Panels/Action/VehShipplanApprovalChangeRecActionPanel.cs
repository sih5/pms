﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehShipplanApprovalChangeRecActionPanel : DcsActionPanelBase {
        public VehShipplanApprovalChangeRecActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehShipplanApprovalChangeRec,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_Adjustment,
                        UniqueId = "Edit",
                        CanExecute = false,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ApproveEdit.png")
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_ApprovalImport,
                        UniqueId = "Import",
                        CanExecute = false,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ApproveImport.png")
                    }
                }
            };
        }
    }
}
