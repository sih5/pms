﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehicleAccountReceivableBillActionPanel : DcsActionPanelBase {
        public VehicleAccountReceivableBillActionPanel() {
            var a = new VehicleCustomerInformation();
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "VehicleAccountReceivableBill",
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehicleAccountReceivableBill,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_Add,
                        UniqueId = CommonActionKeys.ADD,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                         Title = VehicleSalesUIStrings.Action_Title_Edit,
                        UniqueId = CommonActionKeys.EDIT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                         Title = VehicleSalesUIStrings.Action_Title_Review,
                        UniqueId = "Review",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Review.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                         Title = VehicleSalesUIStrings.Action_Title_BankQuery,
                        UniqueId = "BankQuery",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/BankQuery.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                         Title = VehicleSalesUIStrings.Action_Title_VehicleAccountReceivableBill_Approve,
                        UniqueId = CommonActionKeys.APPROVE,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Approve.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                         Title = VehicleSalesUIStrings.Action_Title_Recorded,
                        UniqueId = "Recorded",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Recorded.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                         Title = VehicleSalesUIStrings.Action_Title_RollOut,
                        UniqueId = "RollOut",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/RollOut.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                         Title = VehicleSalesUIStrings.Action_Title_RollOutApprove,
                        UniqueId = "RollOutApprove",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/RollOutApprove.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                         Title = VehicleSalesUIStrings.Action_Title_Cashier,
                        UniqueId = "Cashier",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Cashier.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                         Title = VehicleSalesUIStrings.Action_Title_ApproveNoPass,
                        UniqueId = "ApproveNoPass",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/ApproveNoPass.png"),
                        CanExecute = false
                    },
                    new ActionItem {
                         Title = VehicleSalesUIStrings.Action_Title_Abandon,
                        UniqueId = CommonActionKeys.ABANDON,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
