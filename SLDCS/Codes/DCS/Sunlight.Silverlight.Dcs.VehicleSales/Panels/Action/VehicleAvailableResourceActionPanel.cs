﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehicleAvailableResourceActionPanel : DcsActionPanelBase {
        public VehicleAvailableResourceActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "VehicleAvailableResource",
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehicleAvailableResource,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_ImportForVehicleAvailableResource,
                        UniqueId = CommonActionKeys.IMPORT,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_ImportForVehicleMCO,
                        UniqueId = "MCOImport",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
