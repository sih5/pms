﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehicleOrderForDealerActionPanel : DcsActionPanelBase {
        public VehicleOrderForDealerActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehicleOrder,
                UniqueId = "VehicleOrder",
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_InsteadAdd,
                        UniqueId = "InsteadAdd",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/InsteadAdd.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_InsteadEdit,
                        UniqueId = "InsteadEdit",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/InsteadEdit.png"),
                        CanExecute = false
                    },new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_InsteadSubmit,
                        UniqueId = "InsteadSubmit",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/InsteadSubmit.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
