﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehicleOrderForExportActionPanel : DcsActionPanelBase {
        public VehicleOrderForExportActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "VehicleOrder",
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehicleOrderForExport,
                ActionItems = new[] {new ActionItem {
                        Title =VehicleSalesUIStrings.Action_Title_DealerExport,
                        UniqueId = "DealerExport",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/DealerExport.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_CollectExport,
                        UniqueId = "CollectExport",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/CollectExport.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}





