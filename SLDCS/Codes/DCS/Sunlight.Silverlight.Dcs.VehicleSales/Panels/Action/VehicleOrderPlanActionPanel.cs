﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehicleOrderPlanActionPanel : DcsActionPanelBase {
        public VehicleOrderPlanActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "Common",
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehicleOrderPlan,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_Detail,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Detail.png"),
                        UniqueId = "Detail",
                        CanExecute = false
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_Collect,
                        UniqueId = "Collect",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Collect.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
