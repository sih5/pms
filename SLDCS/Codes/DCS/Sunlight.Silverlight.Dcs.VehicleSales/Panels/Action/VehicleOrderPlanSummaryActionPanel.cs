﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehicleOrderPlanSummaryActionPanel : DcsActionPanelBase {
        public VehicleOrderPlanSummaryActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "VehicleOrderPlanSummary",
                Title = VehicleSalesUIStrings.ActionPanel_Title_MonthlyOrder,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_AddImport,
                        UniqueId = "AddImport",
                        CanExecute = false,
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Add.png")
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_DelImport,
                        UniqueId = "DelImport",
                        CanExecute = false,
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Delete.png")
                    }
                }
            };
        }
    }
}
