﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehiclePreAllocationRefActionPanel : DcsActionPanelBase {
        public VehiclePreAllocationRefActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehiclePreAllocationRef,
                UniqueId = "Common",
                ActionItems = new[] {
                    new ActionItem {
                        Title =VehicleSalesUIStrings.Action_Title_PreCarMatchExport,
                        UniqueId = CommonActionKeys.EXPORT,
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/PreCarMatchExport.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title =VehicleSalesUIStrings.Action_Title_AdjustImport ,
                        UniqueId = CommonActionKeys.IMPORT,
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/AdjustImport.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
