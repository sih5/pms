﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehicleReturnOrderActionPanel : DcsActionPanelBase {
        public VehicleReturnOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehicleOrder,
                UniqueId = "VehicleReturnOrder",
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_Handle,
                        UniqueId = "Handle",
                        ImageUri =  Utils.MakeServerUri("Client/Dcs/Images/Operations/Handle.png"),
                        CanExecute = false
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_Abandon,
                        UniqueId = "Abandon",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Abandon.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
