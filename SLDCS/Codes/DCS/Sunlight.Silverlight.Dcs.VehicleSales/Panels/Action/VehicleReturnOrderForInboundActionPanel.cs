﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehicleReturnOrderForInboundActionPanel : DcsActionPanelBase {
        public VehicleReturnOrderForInboundActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "VehicleReturnOrder",
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehicleReturnOrderForInbound,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_Inbound,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Inbound.png"),
                        UniqueId = "Inbound",
                        CanExecute = false
                    }
                }
            };
        }
    }
}
