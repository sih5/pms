﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehicleShippingOrderActionPanel : DcsActionPanelBase {
        public VehicleShippingOrderActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehicleShippingOrder,
                UniqueId = "VehicleShippingOrder",
                ActionItems = new[] {
                    new ActionItem {
                        Title =VehicleSalesUIStrings.Action_Title_OutboundConfrim,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/OutboundConfrim.png"),
                        UniqueId = "OutboundConfrim",
                        CanExecute = false
                    }, new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_ImportForVehicleMCO,
                        UniqueId = "MCOImport",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Import.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
