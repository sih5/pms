﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehicleShippingOrderConfirmActionPanel : DcsActionPanelBase {
        public VehicleShippingOrderConfirmActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "VehicleShippingOrderConfirm",
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehicleShippingOrderConfirm,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_PickConfirm,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/PickConfirm.png"),
                        UniqueId = "PickConfirm",
                        CanExecute = false
                    }
                }
            };
        }
    }
}
