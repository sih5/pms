﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class VehicleShipplanApprovalActionPanel : DcsActionPanelBase {
        public VehicleShipplanApprovalActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "VehicleShipplanApproval",
                Title = VehicleSalesUIStrings.ActionPanel_Title_VehicleShipplanApproval,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_Revocation,
                        UniqueId = "Revocation",
                        CanExecute = false,
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/Revocation.png")
                    }
                }
            };
        }
    }
}
