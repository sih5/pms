﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class WholesaleApprovalActionPanel : DcsActionPanelBase {
        public WholesaleApprovalActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "WholesaleApproval",
                Title = VehicleSalesUIStrings.ActionPanel_Title_WholesaleApproval,
                ActionItems = new[]{
                     new ActionItem{
                        UniqueId="WholesaleResourcePrint",
                        Title=VehicleSalesUIStrings.Action_Title_WholesaleResourcePrint,
                        ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}
