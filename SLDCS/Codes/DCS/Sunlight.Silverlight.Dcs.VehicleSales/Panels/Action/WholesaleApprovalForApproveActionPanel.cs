﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class WholesaleApprovalForApproveActionPanel : DcsActionPanelBase{
        public WholesaleApprovalForApproveActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "WholesaleApprovalForApprove",
                Title=VehicleSalesUIStrings.ActionPanel_Title_WholesaleApprovalForApprova,
                ActionItems=new []{
                     new ActionItem{
                        UniqueId="AllowModify",
                        Title=VehicleSalesUIStrings.Action_Title_AllowModify,
                        ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Edit.png"),
                        CanExecute=false
                    }, new ActionItem{
                        UniqueId="WholesaleResourcePrint",
                        Title=VehicleSalesUIStrings.Action_Title_WholesaleResourcePrint,
                        ImageUri=Utils.MakeServerUri("Client/Dcs/Images/Operations/Print.png"),
                        CanExecute=false
                    }
                }
            };
        }
    }
}
