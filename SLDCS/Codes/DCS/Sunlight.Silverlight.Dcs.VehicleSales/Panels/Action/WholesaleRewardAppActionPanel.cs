﻿using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Action {
    public class WholesaleRewardAppActionPanel : DcsActionPanelBase {
        //根据设计人员要求，暂时去掉允许修改，如果有需要，可通过还原此文件的上一版本恢复允许修改
        public WholesaleRewardAppActionPanel() {
            this.ActionItemGroup = new ActionItemGroup {
                UniqueId = "WholesaleRewardAppForApprove",
                Title = VehicleSalesUIStrings.ActionPanel_Title_WholesaleRewardApp,
                ActionItems = new[] {
                    new ActionItem {
                        Title = VehicleSalesUIStrings.Action_Title_RewardConfirm,
                        UniqueId = "RewardConfirm",
                        ImageUri = Utils.MakeServerUri("Client/Dcs/Images/Operations/RewardConfirm.png"),
                        CanExecute = false
                    }
                }
            };
        }
    }
}
