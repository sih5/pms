﻿using System;
using System.ComponentModel;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class DealerRegionManagerAffiDataEditPanel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public DealerRegionManagerAffiDataEditPanel() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("Dealer");
            queryWindow.SelectionDecided += this.queryWindow_SelectionDecided;
            this.ptbDealerCode.PopupContent = queryWindow;
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var dealer = queryWindow.SelectedEntities.Cast<Dealer>().FirstOrDefault();
            if(dealer == null)
                return;

            var dealerRegionManagerAffi = this.DataContext as DealerRegionManagerAffi;
            if(dealerRegionManagerAffi == null)
                return;
            dealerRegionManagerAffi.DealerId = dealer.Id;
            dealerRegionManagerAffi.DealerCode = dealer.Code;
            dealerRegionManagerAffi.DealerName = dealer.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
