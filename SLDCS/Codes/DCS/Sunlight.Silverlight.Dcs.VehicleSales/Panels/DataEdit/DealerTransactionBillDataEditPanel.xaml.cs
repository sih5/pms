﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class DealerTransactionBillDataEditPanel : INotifyPropertyChanged {
        private ObservableCollection<KeyValuePair> kvDealerBuyerWarehouseCode;
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public ObservableCollection<KeyValuePair> KvDealerBuyerWarehouseCodes {
            get {
                return this.kvDealerBuyerWarehouseCode ?? (this.kvDealerBuyerWarehouseCode = new ObservableCollection<KeyValuePair>());
            }
        }

        public DealerTransactionBillDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("Dealer");
            queryWindow.SelectionDecided += this.queryWindow_SelectionDecided;
            this.ptbDealerSellerName.PopupContent = queryWindow;
            var domainContext = new DcsDomainContext();
            //TODO:预入仓库数据源指经销商仓库而不是经销商库存；
            domainContext.Load(domainContext.GetDealerWarehousesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效 && v.DealerId == BaseApp.Current.CurrentUserData.EnterpriseId), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var dealerWarehouse in loadOp.Entities)
                    this.KvDealerBuyerWarehouseCodes.Add(new KeyValuePair {
                        Key = dealerWarehouse.Id,
                        Value = dealerWarehouse.Name
                    });
            }, null);
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var dealer = queryWindow.SelectedEntities.Cast<Dealer>().FirstOrDefault();
            if(dealer == null)
                return;

            var dealerTransactionBill = this.DataContext as DealerTransactionBill;
            if(dealerTransactionBill == null)
                return;
            dealerTransactionBill.DealerSellerName = dealer.Name;
            dealerTransactionBill.DealerSellerId = dealer.Id;
            dealerTransactionBill.DealerSellerCode = dealer.Code;
            dealerTransactionBill.DealerBuyerName = BaseApp.Current.CurrentUserData.EnterpriseName;
            dealerTransactionBill.DealerBuyerId = BaseApp.Current.CurrentUserData.EnterpriseId;
            dealerTransactionBill.DealerBuyerCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
