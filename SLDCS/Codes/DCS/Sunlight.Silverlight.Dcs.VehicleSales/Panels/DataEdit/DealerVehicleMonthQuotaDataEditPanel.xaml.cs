﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class DealerVehicleMonthQuotaDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvVehicleModelCategories = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvYearYearOfPlans = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvMonthOfPlans = new ObservableCollection<KeyValuePair>();

        public DealerVehicleMonthQuotaDataEditPanel() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleModelCategoriesQuery().Where(e => e.Status == (int)DcsBaseDataStatus.有效 && e.Type == (int)DcsVehicleModelCategoryType.经销商月目标车型), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var vehicleModelCategories in loadOp.Entities)
                    this.kvVehicleModelCategories.Add(new KeyValuePair {
                        Key = vehicleModelCategories.Id,
                        Value = vehicleModelCategories.Name,
                    });
            }, null);

            var currentTime = DateTime.Now;
            var year = currentTime.Year;
            var date = year + 10;
            for(var i = year; i < date; i++) {
                this.kvYearYearOfPlans.Add(new KeyValuePair {
                    Key = i,
                    Value = i.ToString(CultureInfo.InvariantCulture)
                });
            }

            for(var i = 1; i < 13; i++) {
                this.kvMonthOfPlans.Add(new KeyValuePair {
                    Key = i,
                    Value = i.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0')
                });
            }

            var dealer = DI.GetQueryWindow("Dealer");
            dealer.SelectionDecided += this.DealerCode_SelectionDecided;
            this.ptbDealer.PopupContent = dealer;
        }

        private void DealerCode_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var dealer = queryWindow.SelectedEntities.Cast<Dealer>().FirstOrDefault();
            if(dealer == null)
                return;

            var dealerVehicleMonthQuota = this.DataContext as DealerVehicleMonthQuota;
            if(dealerVehicleMonthQuota == null)
                return;
            dealerVehicleMonthQuota.DealerId = dealer.Id;
            dealerVehicleMonthQuota.DealerCode = dealer.Code;
            dealerVehicleMonthQuota.DealerName = dealer.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public ObservableCollection<KeyValuePair> KvVehicleModelCategories {
            get {
                return this.kvVehicleModelCategories;
            }
        }

        public ObservableCollection<KeyValuePair> KvYearYearOfPlans {
            get {
                return this.kvYearYearOfPlans ?? (this.kvYearYearOfPlans = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvMonthOfPlans {
            get {
                return this.kvMonthOfPlans ?? (this.kvMonthOfPlans = new ObservableCollection<KeyValuePair>());
            }
        }
    }
}
