﻿using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class MultiLevelAuditConfigDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvInitialStatus = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "MultiLevelAuditConfig_InitialStatus"
        };

        private ObservableCollection<KeyValuePair> kvBusinessObjectNames;
        public ObservableCollection<KeyValuePair> KvBusinessObjectNames {
            get {
                return this.kvBusinessObjectNames ?? (this.kvBusinessObjectNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public object KvInitialStatus {
            get {
                return this.kvInitialStatus;
            }
        }

        public MultiLevelAuditConfigDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.Initialize);
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void CreateUI() {
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValue in this.KeyValueManager[this.kvNames[0]].Where(entity => entity.Key != (int)DcsMultiLevelAuditConfigInitialStatus.作废))
                    kvInitialStatus.Add(keyValue);
            });
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetBusinessObjectsQuery().Where(r => r.BusinessDomain == (int)DcsBusinessDomain.整车), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                }
                var entities = loadOp.Entities;
                if(entities == null || !entities.Any())
                    return;
                foreach(var entity in entities) {
                    this.KvBusinessObjectNames.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);
        }
    }
}
