﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class OEMVehicleMonthQuotaDataEditPanel {
        private ObservableCollection<KeyValuePair> kvVehicleModelCategoryNames;
        private ObservableCollection<KeyValuePair> kvYears;
        private ObservableCollection<KeyValuePair> kvMonths;
        public OEMVehicleMonthQuotaDataEditPanel() {
            InitializeComponent();
            this.Loaded += OEMVehicleMonthQuotaDataEditPanel_Loaded;
        }

        public ObservableCollection<KeyValuePair> KvVehicleModelCategoryNames {
            get {
                return this.kvVehicleModelCategoryNames ?? (this.kvVehicleModelCategoryNames = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvYears {
            get {
                return this.kvYears ?? (this.kvYears = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvMonths {
            get {
                return this.kvMonths ?? (this.kvMonths = new ObservableCollection<KeyValuePair>());
            }
        }

        private void OEMVehicleMonthQuotaDataEditPanel_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.CreateUI();
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleModelCategoriesQuery().Where(item => item.Status == (int)DcsBaseDataStatus.有效 && item.Type == (int)DcsVehicleModelCategoryType.总部月目标车型), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvVehicleModelCategoryNames.Clear();
                foreach(var vehicleModelCategoryNames in loadOp.Entities)
                    this.KvVehicleModelCategoryNames.Add(new KeyValuePair {
                        Key = vehicleModelCategoryNames.Id,
                        Value = vehicleModelCategoryNames.Name,
                    });
            }, null);

            var currentTime = DateTime.Now;
            var year = currentTime.Year;
            var date = year + 10;
            this.kvYears.Clear();
            this.KvMonths.Clear();
            for(var i = year; i < date; i++) {
                this.kvYears.Add(new KeyValuePair {
                    Key = i,
                    Value = i.ToString(CultureInfo.InvariantCulture),
                });
            }

            for(var i = 1; i < 13; i++) {
                this.KvMonths.Add(new KeyValuePair {
                    Key = i,
                    Value = i.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0'),
                });
            }
        }
    }
}
