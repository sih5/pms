﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class ProductDataEditPanel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private DateTime lastTime = DateTime.Now;
        private string strFileName;
        private ValidationResult vr;

        private BitmapImage tempBitmapImage;
        public BitmapImage TempBitmapImage {
            get {
                return this.tempBitmapImage;
            }
            set {
                this.tempBitmapImage = value;
                this.OnPropertyChanged("TempBitmapImage");
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private readonly string[] kvNames = {
            "PartsBranch_ProductLifeCycle","Prodcut_Color"
        };

        public ProductDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            var radMenuItem = new RadMenuItem();
            radMenuItem.Click += RadMenuItem_Click;
            radMenuItem.Header = VehicleSalesUIStrings.MenuItem_Text_DeletePicture;
            ImageContextMenu.Items.Add(radMenuItem);
            this.DataContextChanged += ProductDataEditPanel_DataContextChanged;
            this.RadUpload.UploadServiceUrl = DcsUtils.GetUploadHandlerUrl().ToString();
            this.RadUpload.FileUploadStarting -= RadUpload_FileUploadStarting;
            this.RadUpload.FileUploadStarting += RadUpload_FileUploadStarting;
            this.RadUpload.UploadFinished -= this.RadUpload_UploadFinished;
            this.RadUpload.UploadFinished += this.RadUpload_UploadFinished;
            this.RadUpload.FilesSelected -= this.RadUpload_FilesSelected;
            this.RadUpload.FilesSelected += this.RadUpload_FilesSelected;
            this.RadUpload.FileUploaded += (sender, e) => {
                var product = this.DataContext as Product;
                if(product != null)
                    product.Picture = e.HandlerData.CustomData["Path"].ToString();
            };
            this.RadUpload.AdditionalPostFields.Add("Category", GlobalVar.UPLOAD_PRODUCT_DIR);
        }

        private void RadUpload_FileUploadStarting(object sender, FileUploadStartingEventArgs e) {
            //取当前登录用户的HashCode来标记上传文件的唯一性
            e.UploadData.FileName = strFileName;
            UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditPanel_Notification_UploadingImg);//图片正在上传
            //添加ValidationErrors旨在控制上传图片过程中禁止提交动作
            var p = this.DataContext as Product;
            vr = new ValidationResult(VehicleSalesUIStrings.DataEditPanel_Validation_Product_UploadimgImg, new[]{//图片正在上传
                "Picture"
            });
            if(p != null)
                p.ValidationErrors.Add(this.vr);
        }

        private void RadUpload_UploadFinished(object sender, RoutedEventArgs e) {
            var upload = sender as RadUpload;
            if(upload != null && upload.CurrentSession != null && upload.CurrentSession.CurrentFile != null) {
                upload.CancelUpload();
            }
            //上传成功后 移除ValidationErrors
            var p = this.DataContext as Product;
            if(p != null)
                p.ValidationErrors.Remove(this.vr);
            UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditPanel_Notification_UploadImgSuccess);
        }

        void ProductDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var product = e.NewValue as Product;
            if(product != null && !string.IsNullOrWhiteSpace(product.Picture)) {
                var imgConverter = new ImageConverter();
                TempBitmapImage = (BitmapImage)imgConverter.Convert(product.Picture, null, null, null);
            }
        }

        public object KvProductLifeCycles {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvColors {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        private void Border_OnMouseRightButtonDown(object sender, MouseButtonEventArgs e) {
            var product = this.DataContext as Product;
            if(product != null && !string.IsNullOrWhiteSpace(product.Picture))
                ImageContextMenu.Visibility = Visibility.Visible;
            else
                ImageContextMenu.Visibility = Visibility.Collapsed;
        }

        void RadMenuItem_Click(object sender, Telerik.Windows.RadRoutedEventArgs e) {
            var product = this.DataContext as Product;
            if(product != null) {
                product.Picture = "";
                this.TempBitmapImage = null;
            }
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            if((DateTime.Now - lastTime).TotalMilliseconds < 300) {
                this.RadUpload.ShowFileDialog();
            }
            lastTime = DateTime.Now;
        }

        private void RadUpload_FilesSelected(object sender, FilesSelectedEventArgs e) {
            if(e.SelectedFiles.Any())
                try {
                    var bitmapImage = new BitmapImage();
                    bitmapImage.SetSource(e.SelectedFiles[0].File.OpenRead());
                    TempBitmapImage = bitmapImage;
                    var product = this.DataContext as Product;
                    if(product == null)
                        return;
                    strFileName = string.Format("{0}_{1}{2}", Path.GetFileNameWithoutExtension(e.SelectedFiles[0].Name), Guid.NewGuid(), Path.GetExtension(e.SelectedFiles[0].Name));
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                    e.Handled = true;
                }
        }
    }
}
