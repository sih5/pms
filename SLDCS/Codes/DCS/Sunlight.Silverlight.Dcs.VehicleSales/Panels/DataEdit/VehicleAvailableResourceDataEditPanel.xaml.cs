﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class VehicleAvailableResourceDataEditPanel {
        private ObservableCollection<KeyValuePair> kvVehicleWarehouses;

        public VehicleAvailableResourceDataEditPanel() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleWarehousesQuery().Where(e => e.Status == (int)DcsMasterDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvVehicleWarehouses.Clear();
                foreach(var vehicleWarehouse in loadOp.Entities)
                    this.KvVehicleWarehouses.Add(new KeyValuePair {
                        Key = vehicleWarehouse.Id,
                        Value = vehicleWarehouse.Name,
                        UserObject = vehicleWarehouse
                    });
            }, null);
        }

        public ObservableCollection<KeyValuePair> KvVehicleWarehouses {
            get {
                return this.kvVehicleWarehouses ?? (this.kvVehicleWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e) {
            var vehicleAvailableResource = this.DataContext as VehicleAvailableResource;
            if(vehicleAvailableResource == null)
                return;
            vehicleAvailableResource.IsAvailable = this.radioButtonFirst.IsChecked.HasValue && this.radioButtonFirst.IsChecked.Value;
        }

        private void DcsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var vehicleAvailableResource = this.DataContext as VehicleAvailableResource;
            if(vehicleAvailableResource == null)
                return;
            if(e.AddedItems == null || e.AddedItems.Count < 1)
                return;
            var keyValuePair = e.AddedItems[0] as KeyValuePair;
            if(keyValuePair == null)
                return;
            var vehicleWarehouse = keyValuePair.UserObject as VehicleWarehouse;
            if(vehicleWarehouse == null)
                return;

            vehicleAvailableResource.VehicleWarehouseId = vehicleWarehouse.Id;
            vehicleAvailableResource.VehicleWarehouseCode = vehicleWarehouse.Code;
        }
    }
}
