﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class VehicleDLRAccountFreezeBillDataEditPanel {
        private readonly string[] kvNames = {
            "VehicleCustomerAccount_Availability"
        };
        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;

        public VehicleDLRAccountFreezeBillDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleFundsTypesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                }
                var entities = loadOp.Entities;
                if(entities == null || !entities.Any())
                    return;
                foreach(var entity in entities) {
                    this.KvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);
        }

        private void CreateUI() {
            var queryWindowVehicleCustomerInformation = DI.GetQueryWindow("VehicleCustomerInformation");
            queryWindowVehicleCustomerInformation.SelectionDecided += this.VehicleCustomerInformation_SelectionDecided;
            this.ptbCustomerCompanyCode.PopupContent = queryWindowVehicleCustomerInformation;
        }

        private void VehicleCustomerInformation_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var customer = queryWindow.SelectedEntities.Cast<VehicleCustomerInformation>().FirstOrDefault();
            if(customer == null)
                return;

            var vehicleDLRAccountFreezeBill = this.DataContext as VehicleDLRAccountFreezeBill;
            if(vehicleDLRAccountFreezeBill == null)
                return;
            vehicleDLRAccountFreezeBill.CustomerCompanyId = customer.CustomerCompany.Id;
            vehicleDLRAccountFreezeBill.CustomerCompanyCode = customer.CustomerCompany.Code;
            vehicleDLRAccountFreezeBill.CustomerCompanyName = customer.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public object KvVehicleCustomerAccount {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }
    }
}
