﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class VehicleDealerCreditLimitAppDataEditPanel {
        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;

        public ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public VehicleDealerCreditLimitAppDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("VehicleCustomerInformation");
            queryWindow.SelectionDecided += this.queryWindow_SelectionDecided;
            this.ptbDealerSellerName.PopupContent = queryWindow;
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleFundsTypesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var vehicleFundsType in loadOp.Entities)
                    this.KvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = vehicleFundsType.Id,
                        Value = vehicleFundsType.Name
                    });
            }, null);
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var vehicleCustomerInformation = queryWindow.SelectedEntities.Cast<VehicleCustomerInformation>().FirstOrDefault();
            if(vehicleCustomerInformation == null)
                return;

            var vehicleDealerCreditLimitApp = this.DataContext as VehicleDealerCreditLimitApp;
            if(vehicleDealerCreditLimitApp == null)
                return;
            vehicleDealerCreditLimitApp.CustomerCompanyId = vehicleCustomerInformation.CustomerCompanyId;
            vehicleDealerCreditLimitApp.CustomerCompanyCode = vehicleCustomerInformation.CustomerCompany.Code;
            vehicleDealerCreditLimitApp.CustomerCompanyName = vehicleCustomerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}

