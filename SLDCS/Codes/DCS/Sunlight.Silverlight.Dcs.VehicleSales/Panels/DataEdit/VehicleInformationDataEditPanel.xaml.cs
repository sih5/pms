﻿using System;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using System.Collections.ObjectModel;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class VehicleInformationDataEditPanel {
        private readonly ObservableCollection<KeyValuePair> kvVehicleTypes = new ObservableCollection<KeyValuePair>();
        public object KvVehicleTypes {
            get {
                return this.kvVehicleTypes;
            }
        }
        private readonly string[] kvNames = { 
            "VehicleInformation_VehicleType" 
        };

        public VehicleInformationDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void CreateUI() {
            var queryWindowSparePart = DI.GetQueryWindow("Product");
            queryWindowSparePart.SelectionDecided += this.Product_SelectionDecided;
            queryWindowSparePart.Loaded += queryWindowSparePart_Loaded;
            this.ptbProduct.PopupContent = queryWindowSparePart;
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValue in this.KeyValueManager[this.kvNames[0]].Where(entity => entity.Key == (int)DcsVehicleInformationVehicleType.公务 || entity.Key == (int)DcsVehicleInformationVehicleType.特殊 || entity.Key == (int)DcsVehicleInformationVehicleType.一汽))
                    kvVehicleTypes.Add(keyValue);
            });
        }

        private void queryWindowSparePart_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void Product_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var product = queryWindow.SelectedEntities.Cast<Product>().FirstOrDefault();
            if(product == null)
                return;

            var vehicleInformation = this.DataContext as VehicleInformation;
            if(vehicleInformation == null)
                return;
            vehicleInformation.ProductId = product.Id;
            vehicleInformation.ProductCode = product.Code;
            vehicleInformation.ProductName = product.Name;
            vehicleInformation.ProductCategoryCode = product.Code;
            vehicleInformation.ProductCategoryName = product.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }
    }
}
