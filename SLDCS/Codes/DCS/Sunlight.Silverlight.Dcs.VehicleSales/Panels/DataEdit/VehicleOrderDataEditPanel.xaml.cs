﻿
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class VehicleOrderDataEditPanel {
        private ObservableCollection<VehicleFundsType> vehicleFundsTypes;
        private DcsDomainContext domainContext;

        private DcsDomainContext DomainContext {
            get {
                return this.domainContext ?? (this.domainContext = new DcsDomainContext());
            }
        }

        public ObservableCollection<VehicleFundsType> VehicleFundsTypes {
            get {
                return this.vehicleFundsTypes ?? (this.vehicleFundsTypes = new ObservableCollection<VehicleFundsType>());
            }
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("Dealer");
            queryWindow.SelectionDecided += queryWindow_SelectionDecided;
            this.ptbDealerCode.PopupContent = queryWindow;
            this.DomainContext.Load(this.DomainContext.GetVehicleFundsTypesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                foreach(var vehicleFundsType in loadOp.Entities) {
                    this.VehicleFundsTypes.Add(vehicleFundsType);
                }
            }, null);
        }

        private void queryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var dealer = queryWindow.SelectedEntities.Cast<Dealer>().FirstOrDefault();
            if(dealer == null)
                return;
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            vehicleOrder.DealerId = dealer.Id;
            vehicleOrder.DealerCode = dealer.Code;
            vehicleOrder.DealerName = dealer.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public VehicleOrderDataEditPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
    }
}
