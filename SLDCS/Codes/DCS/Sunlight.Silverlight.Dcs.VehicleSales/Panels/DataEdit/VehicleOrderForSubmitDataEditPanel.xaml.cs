﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class VehicleOrderForSubmitDataEditPanel {
        private readonly string[] kvNames = {
            "VehicleOrder_OrderType"
        };

        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;
        public VehicleOrderForSubmitDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleFundsTypesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                }
                var entities = loadOp.Entities;
                if(entities != null)
                    foreach(var entity in entities) {
                        this.KvVehicleFundsTypes.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Name
                        });
                    }
            }, null);
        }

        public ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }
    }
}
