﻿
namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class VehicleOrderForSubmitDetailDataEditPanel {
        private readonly string[] kvNames = {
            "VehicleOrder_OrderType","VehicleOrder_Status"
        };

        public VehicleOrderForSubmitDetailDataEditPanel() {
            this.KeyValueManager.Register(this.kvNames);
            InitializeComponent();
        }
    }
}
