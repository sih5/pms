﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Input;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class WholesaleApprovalDataEditPanel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<KeyValuePair> kvSpecialConfigs = new ObservableCollection<KeyValuePair>();
        private string tenderingOrCentralPurchase;
        private ICommand scopeGroupCommand;
        public ICommand ScopeGroupCommand {
            get {
                return this.scopeGroupCommand ?? (this.scopeGroupCommand = new Core.Command.DelegateCommand(this.ScopeGroupChange));
            }
        }

        public string TenderingOrCentralPurchase {
            get {
                return this.tenderingOrCentralPurchase;
            }
            set {
                this.tenderingOrCentralPurchase = value;
                this.OnPropertyChanged("TenderingOrCentralPurchase");
            }
        }

        public ObservableCollection<KeyValuePair> KvSpecialConfigs {
            get {
                return this.kvSpecialConfigs ?? (this.kvSpecialConfigs = new ObservableCollection<KeyValuePair>());
            }
        }

        private void ScopeGroupChange() {
            var wholesaleApproval = this.DataContext as WholesaleApproval;
            if(wholesaleApproval == null)
                return;
            if(Convert.ToInt32(this.Radio.SelectedValue) == 1) {
                wholesaleApproval.IfSpecialConfig = true;
                this.txtSpecialConfigRequirement.IsReadOnly = false;
                wholesaleApproval.SpecialConfigRequirement = null;
            } else {
                wholesaleApproval.IfSpecialConfig = false;
                this.txtSpecialConfigRequirement.IsReadOnly = true;
            }
        }

        private void Load() {
            this.KvSpecialConfigs.Add(new KeyValuePair {
                Key = 1,
                Value = "是"
            });
            this.KvSpecialConfigs.Add(new KeyValuePair {
                Key = 2,
                Value = "否"
            });
            this.Radio.SelectedValue = 2;
        }

        private readonly string[] kvNames = {
            "WholesaleApproval_EmployeePurchaseDocType"
        };

        public object KvEmployeePurchaseDocType {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("KeyAccount");
            queryWindow.SelectionDecided += this.queryWindow_SelectionDecided;
            this.ptbKeyAccountCode.PopupContent = queryWindow;
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var selectedWholesaleApproval = queryWindow.SelectedEntities.Cast<KeyAccount>().FirstOrDefault();
            var wholesaleApproval = this.DataContext as WholesaleApproval;
            if(selectedWholesaleApproval == null || wholesaleApproval == null)
                return;
            wholesaleApproval.KeyAccountId = selectedWholesaleApproval.Id;
            wholesaleApproval.KeyAccountCode = selectedWholesaleApproval.KeyAccountCode;
            wholesaleApproval.ContactPerson = selectedWholesaleApproval.ContactPerson;
            wholesaleApproval.CustomerCode = selectedWholesaleApproval.Customer.Code;
            wholesaleApproval.ContactJobPosition = selectedWholesaleApproval.ContactJobPosition;
            wholesaleApproval.CustomerName = selectedWholesaleApproval.Customer.Name;
            wholesaleApproval.ContactPhone = selectedWholesaleApproval.ContactPhone;
            wholesaleApproval.CustomerType = selectedWholesaleApproval.Customer.CustomerType;
            wholesaleApproval.ContactCellPhone = selectedWholesaleApproval.ContactCellPhone;
            wholesaleApproval.CustomerAddress = selectedWholesaleApproval.Customer.Address;
            wholesaleApproval.ContactFax = selectedWholesaleApproval.ContactFax;
            wholesaleApproval.CustomerPostCode = selectedWholesaleApproval.Customer.PostCode;
            wholesaleApproval.CompanyName = selectedWholesaleApproval.Customer.CompanyName;
            wholesaleApproval.CustomerProperty = selectedWholesaleApproval.CustomerProperty;
            wholesaleApproval.IfTenderingOrCentralPurchase = selectedWholesaleApproval.IfTenderingOrCentralPurchase;
            this.TenderingOrCentralPurchase = wholesaleApproval.IfTenderingOrCentralPurchase != false ? (wholesaleApproval.IfTenderingOrCentralPurchase == true ? "是" : null) : "否";
            this.txtEmployeePurchaseDocType.IsEnabled = false;
            this.txtEmployeePurchaseDocOtherType.IsReadOnly = true;
            if(wholesaleApproval.EntityState == EntityState.New) {
                switch(wholesaleApproval.CustomerProperty) {
                    case (int)DcsKeyAccountCustomerProperty.车改或组织内员工购车:
                    case (int)DcsKeyAccountCustomerProperty.其它:
                        this.txtEmployeePurchaseDocType.IsEnabled = true;
                        break;
                    default:
                        this.txtEmployeePurchaseDocType.IsEnabled = false;
                        break;
                }
            }
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
            if(wholesaleApproval.HasValidationErrors)
                wholesaleApproval.ValidationErrors.Clear();
        }

        private void txtEmployeePurchaseDocType_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var wholesaleApproval = this.DataContext as WholesaleApproval;
            if(wholesaleApproval == null)
                return;
            if(wholesaleApproval.EmployeePurchaseDocType == (int)(int)DcsWholesaleApprovalEmployeePurchaseDocType.其它)
                this.txtEmployeePurchaseDocOtherType.IsReadOnly = false;
            else
                this.txtEmployeePurchaseDocOtherType.IsReadOnly = true;
        }

        public WholesaleApprovalDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.Initializer.Register(this.Load);
            // this.DataContextChanged += WholesaleApprovalDataEditPanel_DataContextChanged;
        }

        //void WholesaleApprovalDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
        //    var wholesaleApproval = this.DataContext as WholesaleApproval;
        //    if(wholesaleApproval == null)
        //        return;
        //    wholesaleApproval.PropertyChanged += wholesaleApproval_PropertyChanged;
        //}

        //void wholesaleApproval_PropertyChanged(object sender, PropertyChangedEventArgs e) {
        //    var wholesaleApproval = this.DataContext as WholesaleApproval;
        //    if(wholesaleApproval == null)
        //        return;
        //    switch(e.PropertyName) {
        //        case "IfSpecialConfig":
        //            if(Convert.ToInt32(this.Radio.SelectedValue) == 1) {
        //                wholesaleApproval.IsSpecialOperation = true;
        //            } else
        //                wholesaleApproval.IsSpecialOperation = false;
        //            if(wholesaleApproval.IfSpecialConfig) {
        //                this.txtSpecialConfigRequirement.IsReadOnly = false;
        //                wholesaleApproval.SpecialConfigRequirement = null;
        //            } else {
        //                this.txtSpecialConfigRequirement.IsReadOnly = true;
        //            }
        //            break;
        //    }
        //}

    }
}