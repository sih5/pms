﻿using System;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class WholesaleApprovalForApproveDataEditPanel {
        private readonly string[] kvNames = { 
            "KeyAccount_CustomerProperty"
        };

        public object CustomerPropertys {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("KeyAccountForSelect");
            queryWindow.SelectionDecided += this.queryWindow_SelectionDecided;
            this.ptbKeyAccountCode.PopupContent = queryWindow;
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var selectedKeyAccount = queryWindow.SelectedEntities.Cast<KeyAccount>().FirstOrDefault();
            var wholesaleApproval = this.DataContext as WholesaleApproval;
            if(selectedKeyAccount == null || wholesaleApproval == null || wholesaleApproval.KeyAccountId == default(int))
                return;
            wholesaleApproval.KeyAccount.KeyAccountCode = selectedKeyAccount.KeyAccountCode;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void WholesaleApprovalForApproveDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var wholesaleApproval = this.DataContext as WholesaleApproval;
            if(wholesaleApproval != null && wholesaleApproval.KeyAccount != null && wholesaleApproval.KeyAccountCode != null) {
                wholesaleApproval.ContactPerson = wholesaleApproval.KeyAccount.ContactPerson;
                wholesaleApproval.ContactJobPosition = wholesaleApproval.KeyAccount.ContactJobPosition;
                wholesaleApproval.ContactPhone = wholesaleApproval.KeyAccount.ContactPhone;
                wholesaleApproval.ContactCellPhone = wholesaleApproval.KeyAccount.ContactCellPhone;
                wholesaleApproval.ContactFax = wholesaleApproval.KeyAccount.ContactFax;
                if(wholesaleApproval.KeyAccount.Customer != null) {
                    wholesaleApproval.CustomerCode = wholesaleApproval.KeyAccount.Customer.Code;
                    wholesaleApproval.CustomerName = wholesaleApproval.KeyAccount.Customer.Name;
                    wholesaleApproval.CustomerAddress = wholesaleApproval.KeyAccount.Customer.Address;
                    wholesaleApproval.CustomerPostCode = wholesaleApproval.KeyAccount.Customer.PostCode;
                    wholesaleApproval.CompanyName = wholesaleApproval.KeyAccount.Customer.CompanyName;
                }
            }
        }

        public WholesaleApprovalForApproveDataEditPanel() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.WholesaleApprovalForApproveDataEditPanel_DataContextChanged;
        }
    }
}
