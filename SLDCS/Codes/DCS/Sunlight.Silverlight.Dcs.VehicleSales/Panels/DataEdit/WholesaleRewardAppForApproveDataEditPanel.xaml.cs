﻿using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {

    public partial class WholesaleRewardAppForApproveDataEditPanel {
        private readonly string[] kvNames = {
            "WholesaleRewardApp_CustomerDetailCategory"
        };

        public WholesaleRewardAppForApproveDataEditPanel() {
            this.InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
        }

        private void RadRadioButtonApprovePass_OnActivate(object sender, Telerik.Windows.RadRoutedEventArgs e) {
            var wholesaleRewardApp = this.DataContext as WholesaleRewardApp;
            if(wholesaleRewardApp != null)
                wholesaleRewardApp.Status = (int)DcsWholesaleRewardAppStatus.有效;
        }

        private void RadRadioButtondiApproveNoPass_OnActivate(object sender, Telerik.Windows.RadRoutedEventArgs e) {
            var wholesaleRewardApp = this.DataContext as WholesaleRewardApp;
            if(wholesaleRewardApp != null)
                wholesaleRewardApp.Status = (int)DcsWholesaleRewardAppStatus.新增;
        }
    }
}
