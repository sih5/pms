﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit {
    public partial class WholesaleRewardAppForReportDataEditPanel : INotifyPropertyChanged {
        private readonly string[] kvNames = {
            "KeyAccount_CustomerProperty", "WholesaleRewardApp_CustomerDetailCategory"
        };
        public event PropertyChangedEventHandler PropertyChanged;
        private KeyAccount keyAccount;
        private WholesaleApproval wholesaleApproval;

        public KeyAccount KeyAccount {
            get {
                return keyAccount;
            }
            set {
                keyAccount = value;
                this.OnPropertyChanged("KeyAccount");
            }
        }

        public WholesaleApproval WholesaleApproval {
            get {
                return wholesaleApproval;
            }
            set {
                wholesaleApproval = value;
                this.OnPropertyChanged("WholesaleApproval");
            }
        }

        public DcsDomainContext DomainContext;

        public object KvCustomerDetailCategory {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        public WholesaleRewardAppForReportDataEditPanel() {
            InitializeComponent();
            this.DataContextChanged += WholesaleRewardAppForReportDataEditPanel_DataContextChanged;
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        private void WholesaleRewardAppForReportDataEditPanel_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            this.KeyAccount = null;
            this.WholesaleApproval = null;
            var wholesaleRewardApp = e.NewValue as WholesaleRewardApp;
            if(wholesaleRewardApp == null)
                return;
            this.KeyAccount = wholesaleRewardApp.KeyAccount;
            this.WholesaleApproval = wholesaleRewardApp.WholesaleApproval;
        }

        private void CreateUI() {
            var wholesaleApproval = DI.GetQueryWindow("WholesaleApproval");
            wholesaleApproval.SelectionDecided += wholesaleApproval_SelectionDecided;
            wholesaleApproval.ExchangeData(null, "SetQueryItemValue", new object[] {
                    "Common", "ProductCode", BaseApp.Current.CurrentUserData.EnterpriseId
                });
            this.popupTextBoxWholesaleApprovalCode.PopupContent = wholesaleApproval;
        }

        private void wholesaleApproval_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var wholesaleApproval = queryWindow.SelectedEntities.Cast<WholesaleApproval>().FirstOrDefault();
            if(wholesaleApproval == null)
                return;
            var wholesaleRewardApp = this.DataContext as WholesaleRewardApp;
            if(wholesaleRewardApp == null)
                return;
            wholesaleRewardApp.KeyAccountId = wholesaleApproval.KeyAccountId;
            wholesaleRewardApp.WholesaleApprovalId = wholesaleApproval.Id;
            wholesaleRewardApp.WholesaleApprovalCode = wholesaleApproval.Code;
            foreach(var item in wholesaleRewardApp.WholesaleRewardAppDetails)
                wholesaleRewardApp.WholesaleRewardAppDetails.Remove(item);
            foreach(var detail in wholesaleApproval.WholesaleApprovalDetails) {
                wholesaleRewardApp.WholesaleRewardAppDetails.Add(new WholesaleRewardAppDetail {
                    Id = GlobalVar.ASSIGNED_BY_SERVER_INT,
                    WholesaleRewardAppId = wholesaleRewardApp.Id,
                    ProductCategoryCode = detail.ProductCategoryCode,
                    ProductCategoryName = detail.ProductCategoryName,
                    VehicleDeliveryTime = DateTime.Now
                });
            }

            this.DomainContext.Load(this.DomainContext.GetVehicleInformationsQuery().Where(w => w.WholesaleApprovalId == wholesaleApproval.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var detail in wholesaleRewardApp.WholesaleRewardAppDetails) {
                    var vehicleInfomation = loadOp.Entities.Where(obj => obj.ProductCategoryCode == detail.ProductCategoryCode).FirstOrDefault();
                    if(vehicleInfomation != null) {
                        detail.VIN = vehicleInfomation.VIN;
                        detail.InvoiceCode = vehicleInfomation.SalesInvoiceNumber;
                        detail.TransactionPrice = vehicleInfomation.SellPrice ?? 0;
                    }
                }
            }, null);
            
            var productCategoryCodes = wholesaleApproval.WholesaleApprovalDetails.Select(d => d.ProductCategoryCode).ToArray();

            this.DomainContext.Load(this.DomainContext.GetVehModelRetailSuggestedPricesByCategoryCodesQuery(productCategoryCodes), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var detail in wholesaleRewardApp.WholesaleRewardAppDetails) {
                    var suggestedPrice = loadOp.Entities.Where(obj => obj.ProductCategoryCode == detail.ProductCategoryCode).FirstOrDefault();
                    if(suggestedPrice != null)
                        detail.MarketGuidePrice = suggestedPrice.Price;
                }
            }, null);


            this.KeyAccount = wholesaleApproval.KeyAccount;
            this.WholesaleApproval = wholesaleApproval;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
