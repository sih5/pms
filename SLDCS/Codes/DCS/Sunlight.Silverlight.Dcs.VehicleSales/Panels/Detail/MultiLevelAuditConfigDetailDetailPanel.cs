﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Detail {
    public class MultiLevelAuditConfigDetailDetailPanel : MultiLevelAuditConfigDetailDataGridView, IDetailPanel {
        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return "多级审核配置清单";//VehicleSalesUIStrings.DetailPanel_Title_MultiLevelAuditConfigDetail;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.DomainDataSource.PageSize = 10;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowIndicatorVisibility = Visibility.Collapsed;
        }
    }
}
