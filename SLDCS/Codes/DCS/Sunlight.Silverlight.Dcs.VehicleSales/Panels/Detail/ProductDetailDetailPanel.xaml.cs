﻿
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Detail {
    public partial class ProductDetailDetailPanel {
        private readonly string[] kvNames = {
            "PartsBranch_ProductLifeCycle","Prodcut_Color"
        };

        public ProductDetailDetailPanel() {
            this.KeyValueManager.Register(this.kvNames);
            InitializeComponent();
        }
        public override string Title {
            get {
                return VehicleSalesUIStrings.DetailPanel_Title_ProductDetail;
            }
        }
    }
}
