﻿
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Detail {
    public partial class VINChangeRecordDetailPanel {
        public VINChangeRecordDetailPanel() {
            InitializeComponent();
            this.DataContextChanged += VINChangeRecordDetailPanel_DataContextChanged;
        }

        public override string Title {
            get {
                return VehicleSalesUIStrings.DetailPanel_Title_VINChangeRecord;
            }
        }

        private void VINChangeRecordDetailPanel_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var vehicleInformation = this.DataContext as VehicleInformation;
            if(vehicleInformation == null)
                return;

            var domainContext = new DcsDomainContext();
            domainContext.Load((EntityQuery)domainContext.GetVINChangeRecordsQuery().Where(r => r.VehicleId == vehicleInformation.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any())
                    return;
                var entity = loadOp.Entities.Cast<VINChangeRecord>().First();
                this.DataContext = entity;
            }, null);
        }
    }
}
