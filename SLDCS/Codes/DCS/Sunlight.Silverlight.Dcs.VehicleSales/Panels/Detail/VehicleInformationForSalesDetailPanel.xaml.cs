﻿using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Detail {
    public partial class VehicleInformationForSalesDetailPanel {
        private readonly string[] kvNames = {
            "Vehicle_Status","VehicleInformation_VehicleType"
        };
        public VehicleInformationForSalesDetailPanel() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
        }

        public override string Title {
            get {
                return VehicleSalesUIStrings.DetailPanel_Title_VehicleInformation;
            }
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            var vehicleInformation = this.DataContext as VehicleInformation;
            if(vehicleInformation == null)
                return;

            domainContext.Load((EntityQuery)domainContext.GetTiledRegionsQuery().Where(r => r.Id == vehicleInformation.LicensePlateRegionId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any())
                    return;
                var tiledRegion = loadOp.Entities.Cast<TiledRegion>().SingleOrDefault();
                vehicleInformation.ProvinceName = tiledRegion.ProvinceName;
                vehicleInformation.CityName = tiledRegion.CityName;
            }, null);
        }
    }
}
