﻿using System;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Detail {
    public class VehicleOrderExportDetailDetailPanel : VehicleOrderExportDetailDataGridView, IDetailPanel {
        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return VehicleSalesUIStrings.DetailPanel_Title_VehicleOrderExportDetailCollects;
            }
        }
    }
}
