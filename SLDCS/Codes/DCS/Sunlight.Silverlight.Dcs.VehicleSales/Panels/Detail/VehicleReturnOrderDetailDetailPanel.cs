﻿using System;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Detail {
    public class VehicleReturnOrderDetailDetailPanel : VehicleReturnOrderDetailDataGridView, IDetailPanel {

        public Uri Icon {
            get {
                return null;
            }
        }

        public string Title {
            get {
                return "整车退货清单";//VehicleSalesUIStrings.DetailPanel_Title_VehicleReturnOrderDetail
            }
        }
    }
}
