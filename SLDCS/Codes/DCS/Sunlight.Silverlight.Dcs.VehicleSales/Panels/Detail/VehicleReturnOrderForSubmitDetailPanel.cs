﻿
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Detail {
    public class VehicleReturnOrderForSubmitDetailPanel : VehicleReturnOrderDetailForSubmitDataGridView, IDetailPanel {

        public System.Uri Icon {
            get {
                return null;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        public string Title {
            get {
                return Utils.GetEntityLocalizedName(typeof(VehicleReturnOrder), VehicleSalesUIStrings.DetailPanel_Title_VehicleReturnOrderDetail);
            }
        }
    }
}
