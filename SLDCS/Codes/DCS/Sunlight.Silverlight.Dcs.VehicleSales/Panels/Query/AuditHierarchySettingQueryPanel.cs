﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class AuditHierarchySettingQueryPanel :DcsQueryPanelBase{
        public AuditHierarchySettingQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =VehicleSalesUIStrings.QueryPanel_Title_AuditHierarchySetting,
                    EntityType = typeof(AuditHierarchySetting),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "PositionCode"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false
                        },new QueryItem {
                            ColumnName = "PositionName"
                        }
                    }
                }
            };
        }
    }
}
