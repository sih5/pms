﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class DealerForecastQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "DealerForecast_Status"
        };

        private void Initialize() {
            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(DealerForecast),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_DealerForecastSubmit,
                    QueryItems = new QueryItem[] {
                        new DateTimeRangeQueryItem {
                            ColumnName = "ForecastDate"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }

        public DealerForecastQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
