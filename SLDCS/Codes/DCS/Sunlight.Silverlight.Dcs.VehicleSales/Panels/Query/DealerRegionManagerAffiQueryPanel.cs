﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class DealerRegionManagerAffiQueryPanel : DcsQueryPanelBase {

        public DealerRegionManagerAffiQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =VehicleSalesUIStrings.QueryPanel_Title_DealerRegionManagerAffi,
                    EntityType = typeof(DealerRegionManagerAffi),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "RegionManager"
                        }, new QueryItem {
                            ColumnName = "DealerName"
                        }
                    }
                }
            };
        }
    }
}
