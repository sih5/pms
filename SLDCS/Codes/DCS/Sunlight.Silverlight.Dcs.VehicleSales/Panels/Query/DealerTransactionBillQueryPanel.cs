﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class DealerTransactionBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "DealerTransactionBill_Status"
        };

        private void Initialize() {
            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(DealerTransactionBill),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_DealerTransactionBill,
                    QueryItems = new [] {
                        new QueryItem {
                            ColumnName = "DealerBuyerName"
                        }, new QueryItem {
                            ColumnName = "DealerSellerName"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }

        public DealerTransactionBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
