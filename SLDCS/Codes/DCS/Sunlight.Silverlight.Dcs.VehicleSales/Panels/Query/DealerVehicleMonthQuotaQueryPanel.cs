﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class DealerVehicleMonthQuotaQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvVehicleModelCategories = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetVehicleModelCategoriesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效 && r.Type == (int)DcsVehicleModelCategoryType.经销商月目标车型), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var vehicleModelCategories in loadOp.Entities)
                    this.kvVehicleModelCategories.Add(new KeyValuePair {
                        Key = vehicleModelCategories.Id,
                        Value = vehicleModelCategories.Name
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(DealerVehicleMonthQuota),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_DealerVehicleMonthQuota,
                    QueryItems = new[] {
                        new KeyValuesQueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleModelCategoryName,
                            ColumnName = "VehicleModelCategoryId",
                            KeyValueItems = kvVehicleModelCategories
                        }, new QueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_YearOfPlan,
                            ColumnName = "YearOfPlan"
                        }, new QueryItem {
                            ColumnName = "DealerName"
                        }, new QueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_MonthOfPlan,
                            ColumnName = "MonthOfPlan"
                        }
                    }
                }
            };
        }

        public DealerVehicleMonthQuotaQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
    }
}
