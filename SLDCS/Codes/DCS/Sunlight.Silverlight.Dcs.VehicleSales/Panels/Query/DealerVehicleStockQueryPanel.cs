﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {

    public class DealerVehicleStockQueryPanel : DcsQueryPanelBase {

        public DealerVehicleStockQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =VehicleSalesUIStrings.QueryPanel_Title_DealerVehicleStock,
                    EntityType = typeof(DealerVehicleStock),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "ProductCode",
                            IsExact = false
                        }, new QueryItem {
                            ColumnName = "VIN",
                            IsExact = false
                        }
                    }
                }
            };
        }
    }
}
