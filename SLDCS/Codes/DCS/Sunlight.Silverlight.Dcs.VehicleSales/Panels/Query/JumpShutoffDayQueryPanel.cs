﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class JumpShutoffDayQueryPanel : DcsQueryPanelBase {
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(JumpShutoffDay),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_JumpShutoffDay,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "YearOfOrder"
                        }, new QueryItem {
                            ColumnName = "MonthOfOrder"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "JumpShutoffDate",
                            IsExact = false
                        }
                    }
                }
            };
        }

        public JumpShutoffDayQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
    }
}