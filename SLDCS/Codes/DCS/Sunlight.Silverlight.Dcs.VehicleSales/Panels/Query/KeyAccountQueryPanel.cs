﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class KeyAccountQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "KeyAccount_Status","Customer_CustomerType","KeyAccount_CompanyScale"
        };

        private void Initialize() {
            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(KeyAccount),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_KeyAccount,
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "Customer.Name",
                            DataType=typeof(string),
                            IsExact = false,
                            Title =VehicleSalesUIStrings.QueryPanel_QueryItem_Title_Customer_Name
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        },new QueryItem {
                            ColumnName = "DealerName",
                            IsExact = false,
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_KeyAccount_DealerName
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "CompanyScale",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                        }, new KeyValuesQueryItem {
                            ColumnName = "Customer.CustomerType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_KeyAccount_CustomerType
                        }
                    }
                }
            };
        }

        public KeyAccountQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
