﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class MultiLevelAuditConfigQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvBusinessObjectNames = new ObservableCollection<KeyValuePair>();
        public MultiLevelAuditConfigQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetBusinessObjectsQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvBusinessObjectNames.Clear();
                foreach(var vehicleFundsType in loadOp.Entities)
                    this.kvBusinessObjectNames.Add(new KeyValuePair {
                        Key = vehicleFundsType.Id,
                        Value = vehicleFundsType.Name,
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title ="多级审核配置查询",//VehicleSalesUIStrings.QueryPanel_Title_MultiLevelAuditConfig,
                    EntityType = typeof(MultiLevelAuditConfig),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            IsExact = true
                        }, new QueryItem {
                            ColumnName = "Name",
                            IsExact = true
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false
                        }, new KeyValuesQueryItem {
                             Title = "BO类型名称",// VehicleSalesUIStrings.QueryPanel_QueryItem_Title_MultiLevelAuditConfig_BusinessObjectId,
                            ColumnName = "BusinessObjectId",
                            KeyValueItems = kvBusinessObjectNames,
                            IsExact = false
                        }
                    }
                }
            };
        }
    }
}
