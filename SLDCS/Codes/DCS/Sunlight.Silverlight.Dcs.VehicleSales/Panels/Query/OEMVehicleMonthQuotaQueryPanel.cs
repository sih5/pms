﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class OEMVehicleMonthQuotaQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvVehicleModelCategoryNames = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleModelCategoriesQuery().Where(item => item.Status == (int)DcsBaseDataStatus.有效 && item.Type == (int)DcsVehicleModelCategoryType.总部月目标车型), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvVehicleModelCategoryNames.Clear();
                foreach(var vehicleModelCategoryNames in loadOp.Entities)
                    this.kvVehicleModelCategoryNames.Add(new KeyValuePair {
                        Key = vehicleModelCategoryNames.Id,
                        Value = vehicleModelCategoryNames.Name,
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(OEMVehicleMonthQuota),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_OEMVehicleMonthQuota,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "YearOfPlan"
                        }, new QueryItem {
                            ColumnName = "MonthOfPlan"
                        }, new KeyValuesQueryItem {
                            ColumnName = "VehicleModelCategoryId",
                            Title = VehicleSalesUIStrings.DataEditPanel_Text_OEMVehicleMonthQuota_VehicleModelCategoryName,
                            KeyValueItems = kvVehicleModelCategoryNames,
                        }
                    }
                }
            };
        }

        public OEMVehicleMonthQuotaQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
    }
}

