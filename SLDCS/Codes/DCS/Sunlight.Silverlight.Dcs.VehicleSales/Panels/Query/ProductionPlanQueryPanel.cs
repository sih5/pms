﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class ProductionPlanQueryPanel : DcsQueryPanelBase {
        public ProductionPlanQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =VehicleSalesUIStrings.QueryPanel_Title_ProductionPlan,
                    EntityType = typeof(ProductionPlan),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "ProductCode",
                            IsExact = true
                        }, new QueryItem {
                            ColumnName = "ProductCategoryCode",
                            IsExact = true
                        },new DateTimeRangeQueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_Title_ProductionPlan_RolloutDate,
                            ColumnName = "RolloutDate"
                        },new QueryItem {
                            ColumnName = "YearOfOrder"
                        },new QueryItem {
                            ColumnName = "MonthOfOrder"
                        }
                    }
                }
            };
        }
    }
}
