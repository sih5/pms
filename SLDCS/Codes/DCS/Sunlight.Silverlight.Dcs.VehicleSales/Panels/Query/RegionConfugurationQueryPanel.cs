﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class RegionConfugurationQueryPanel : DcsQueryPanelBase {
        public RegionConfugurationQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    EntityType = typeof(RegionConfuguration),
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_RegionConfuguration,
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "DealerName",
                            DataType = typeof(string),
                            Title =VehicleSalesUIStrings.QueryPanel_QueryItem_Title_RegionConfuguration_Dealer
                        }, new QueryItem {
                            ColumnName = "Name",
                            Title =VehicleSalesUIStrings.QueryPanel_QueryItem_Title_RegionConfuguration_Name
                        }
                    }
                }
            };
        }
    }
}
