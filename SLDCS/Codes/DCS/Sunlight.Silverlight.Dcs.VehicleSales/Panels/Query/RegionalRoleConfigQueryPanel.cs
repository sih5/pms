﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class RegionalRoleConfigQueryPanel : DcsQueryPanelBase {
        public RegionalRoleConfigQueryPanel() {
            this.Initializer.Register(this.Initilaze);
        }

        private void Initilaze() {
            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId="Common",
                    Title=VehicleSalesUIStrings.QueryPanel_Title_RegionalRoleConfig,
                    EntityType=typeof(RegionalRoleConfig),
                    QueryItems=new QueryItem[]{
                        new CustomQueryItem{
                            ColumnName="AuditHierarchySetting.PositionName",
                            DataType=typeof(string),
                            Title=VehicleSalesUIStrings.QueryPanel_Title_RegionalRoleConfigResource_PositionName
                        }
                    }
                }
            };
        }
    }
}
