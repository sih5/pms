﻿using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class SpecialVehicleInformationAfterSaleQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvVehicleTypes;
        private readonly string[] kvNames = {
            "VehicleInformation_VehicleType"
        };

        private ObservableCollection<KeyValuePair> KvVehicleTypes {
            get {
                return this.kvVehicleTypes ?? (this.kvVehicleTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public SpecialVehicleInformationAfterSaleQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValue in this.KeyValueManager[this.kvNames[0]].Where(entity => entity.Key != (int)DcsVehicleInformationVehicleType.普通 && entity.Key != (int)DcsVehicleInformationVehicleType.进口)) {
                    KvVehicleTypes.Add(keyValue);
                }
            });

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(RetainedCustomerVehicleList),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_SpecialVehicleInformationAfterSale,
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "RetainedCustomer.Customer.Name",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_Customer_Name,
                            DataType = typeof(string),
                        }, new CustomQueryItem {
                            ColumnName = "VehicleInformation.SalesDealerName",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleInformation_SalesDealerName,
                            DataType = typeof(string)
                        }, new KeyValuesQueryItem {
                            ColumnName = "VehicleInformation.VehicleType",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleInformation_VehicleType,
                            KeyValueItems = this.KvVehicleTypes,
                            IsExact = false
                        }, new DateTimeRangeQueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleInformation_SalesDate,
                            ColumnName = "VehicleInformation.SalesDate",
                            IsExact = false
                        }, new CustomQueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleInformation_VIN,
                            ColumnName = "VehicleInformation.VIN",
                            DataType = typeof(string)
                        }
                    }
                }
            };
        }
    }
}

