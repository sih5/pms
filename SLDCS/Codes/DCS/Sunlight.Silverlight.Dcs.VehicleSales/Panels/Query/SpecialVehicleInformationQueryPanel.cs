﻿using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class SpecialVehicleInformationQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvVehicleTypes;

        private ObservableCollection<KeyValuePair> KvVehicleTypes {
            get {
                return this.kvVehicleTypes ?? (this.kvVehicleTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        private readonly string[] kvNames = { 
            "VehicleInformation_VehicleType" 
        };

        public SpecialVehicleInformationQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.KeyValueManager.LoadData(() => {
                foreach(var keyValue in this.KeyValueManager[this.kvNames[0]].Where(entity => entity.Key == (int)DcsVehicleInformationVehicleType.公务 || entity.Key == (int)DcsVehicleInformationVehicleType.特殊 || entity.Key == (int)DcsVehicleInformationVehicleType.一汽)) {
                    KvVehicleTypes.Add(keyValue);
                }
            });
            this.QueryItemGroups = new[]{
              new QueryItemGroup{
                UniqueId = "VehicleInformation",
                EntityType = typeof(VehicleInformation),
                Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleInformation,
                QueryItems = new[]{
                  new QueryItem{
                   ColumnName = "VIN"
                  },new KeyValuesQueryItem{
                   ColumnName = "VehicleType",
                   KeyValueItems = this.KvVehicleTypes
                  }
                }
              }
           };
        }
    }
}

