﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehPifaPriceChangeAppQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "VehiclePriceChangeApp_Status"
        };

        public VehPifaPriceChangeAppQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title =VehicleSalesUIStrings.QueryPanel_Title_VehPifaPriceChangeApp,
                        EntityType = typeof(VehPifaPriceChangeApp),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "Code",
                            },new DateTimeRangeQueryItem {
                                ColumnName = "ExecutionDate",
                                DefaultValue = new[] {
                                    DateTime.Now,DateTime.Now.AddDays(1)
                                }
                            },new KeyValuesQueryItem {
                                ColumnName = "Status",
                                IsExact = false,
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            }
                        }
                    }
                };
        }
    }
}
