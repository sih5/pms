﻿
using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehRetailPriceChangeAppQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "VehiclePriceChangeApp_Status"
        };

        public VehRetailPriceChangeAppQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId="Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehRetailPriceChangeApp,
                    EntityType = typeof(VehRetailPriceChangeApp),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            IsExact = false
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ExecutionDate",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),  DateTime.Now.AddDays(1)
                            }
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }
    }
}
