﻿

using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehShipplanApprovalChangeRecQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "VehShipplanApprovalChangeRec_AdjustmentType"
        };

        public VehShipplanApprovalChangeRecQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehShipplanApprovalChangeRec),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehShipplanApprovalChangeRec,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "OriginalShipplanApprovalCode",
                            IsExact = false,
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehShipplanApprovalChangeRec_OriginalShipplanApprovalCode
                        }, new QueryItem {
                            ColumnName = "OriginalVIN",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehShipplanApprovalChangeRec_OriginalVIN
                        }, new KeyValuesQueryItem {
                            ColumnName = "AdjustmentType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehShipplanApprovalChangeRec_AdjustmentType
                        }
                    }
                }
            };
        }
    }
}