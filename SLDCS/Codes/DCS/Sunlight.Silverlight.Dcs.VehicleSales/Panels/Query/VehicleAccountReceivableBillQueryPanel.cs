﻿
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleAccountReceivableBillQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "VehicleAccountReceivableBill_Status"
        };

        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;

        private ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public VehicleAccountReceivableBillQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleFundsTypesQuery().Where(entity => entity.Status != (int)DcsBaseDataStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                var entities = loadOp.Entities;
                if(loadOp.Entities == null || !loadOp.Entities.Any())
                    return;
                foreach(var entity in entities) {
                    this.KvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehicleAccountReceivableBill),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleAccountReceivableBill,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "MoneyOrderNumber"
                        }, new QueryItem {
                            ColumnName = "CustomerCompanyCode"
                        }, new KeyValuesQueryItem {
                            ColumnName = "VehicleFundsTypeId",
                            KeyValueItems = this.KvVehicleFundsTypes,
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleAccountReceivableBill_VehicleFundsType
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "IssueDate"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }

    }
}