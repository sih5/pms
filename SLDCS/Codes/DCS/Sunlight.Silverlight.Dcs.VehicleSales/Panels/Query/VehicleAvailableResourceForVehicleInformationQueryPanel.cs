﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleAvailableResourceForVehicleInformationQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvVehicleWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvProductCategories = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "VehicleAvailableResource_LockStatus"
        };

        public VehicleAvailableResourceForVehicleInformationQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetVehicleWarehousesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var vehicleWarehouse in loadOp.Entities)
                    this.kvVehicleWarehouses.Add(new KeyValuePair {
                        Key = vehicleWarehouse.Id,
                        Value = vehicleWarehouse.Name
                    });
            }, null);
            dcsDomainContext.Load(dcsDomainContext.GetProductCategoriesQuery().Where(r => r.Status == (int)DcsMasterDataStatus.有效 && r.ProductCategoryLevel == 2), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var productCategory in loadOp.Entities)
                    this.kvProductCategories.Add(new KeyValuePair {
                        Key = productCategory.Id,
                        Value = productCategory.Code
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleAvailableResource,
                    EntityType = typeof(VehicleAvailableResource),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "VIN"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "VehicleInformation.RolloutDate",
                            Title = Utils.GetEntityLocalizedName(typeof(VehicleInformation), "RolloutDate")
                        }, new CustomQueryItem {
                            ColumnName = "VehicleInformation.EngineSerialNumber",
                            DataType = typeof(string),
                            Title = Utils.GetEntityLocalizedName(typeof(VehicleInformation), "EngineSerialNumber")
                        }, new KeyValuesQueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleAvailableResource_ProductCategory,
                            ColumnName = "ProductCategoryId",
                            KeyValueItems = this.kvProductCategories
                        }, new KeyValuesQueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleAvailableResource_VehicleWarehouseName,
                            ColumnName = "VehicleWarehouseId",
                            KeyValueItems = this.kvVehicleWarehouses
                        }, new QueryItem {
                            ColumnName = "IsAvailable"
                        }, new KeyValuesQueryItem {
                            ColumnName = "LockStatus",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }
    }
}
