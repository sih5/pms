﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleAvailableResourceForVehicleOrderQueryPanel : DcsQueryPanelBase {
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleAvailableResource,
                    EntityType = typeof(VehicleAvailableResource),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "ProductCode"
                        },new QueryItem {
                            ColumnName = "ProductName"
                        },new QueryItem {
                            ColumnName = "ProductCategoryCode",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleAvailableResource_ProductCategoryCode
                        },new QueryItem {
                            ColumnName = "ProductCategoryName",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleAvailableResource_ProductCategoryName
                        }
                    }
                }
            };
        }

        public VehicleAvailableResourceForVehicleOrderQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
    }
}
