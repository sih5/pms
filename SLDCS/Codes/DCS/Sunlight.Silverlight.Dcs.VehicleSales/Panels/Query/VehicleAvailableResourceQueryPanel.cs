﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleAvailableResourceQueryPanel : DcsQueryPanelBase {
        public VehicleAvailableResourceQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleAvailableResource,
                    EntityType = typeof(VehicleAvailableResource),
                    QueryItems = new[] {
                         new QueryItem {
                            ColumnName = "ProductCode"
                       }
                    }
                }
            };
        }

    }
}
