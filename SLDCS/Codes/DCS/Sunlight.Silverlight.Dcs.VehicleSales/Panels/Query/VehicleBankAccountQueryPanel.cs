﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleBankAccountQueryPanel : DcsQueryPanelBase {

        public VehicleBankAccountQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleBankAccount,
                    EntityType = typeof(VehicleBankAccount),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "BankName",
                            IsExact = false
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new QueryItem {
                            ColumnName = "BankAccountNumber",
                            IsExact = false
                        }
                    }
                }
            };
        }
    }
}
