﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleCustomerAccountQueryPanel : DcsQueryPanelBase {

        public VehicleCustomerAccountQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehicleCustomerAccount),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleCustomerAccount,
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "Company.Code",
                            Title =VehicleSalesUIStrings.QueryPanel_CustomQueryItem_Company_Code,
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "Company.Name",
                            Title =VehicleSalesUIStrings.QueryPanel_CustomQueryItem_Company_Name,
                            DataType = typeof(string)
                        }
                    }
                }
            };
        }
    }
}
