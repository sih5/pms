﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleCustomerInformationForSelectQueryPanel : DcsQueryPanelBase {
        public VehicleCustomerInformationForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    EntityType = typeof(VehicleCustomerInformation),
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleCustomerInformation,
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "CustomerCompany.Code",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleCustomerInformation_CustomerCompany_Code,
                            DataType=typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "CustomerCompany.Name",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleCustomerInformation_CustomerCompany_Name,
                            DataType=typeof(string)
                        }
                    }
                }
            };
        }
    }
}