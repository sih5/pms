﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleDLRAccountFreezeBillQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>();

        private readonly string[] kvNames = new[] {
            "VehicleDLRAccountFreezeBill_Status"
        };

        public VehicleDLRAccountFreezeBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleFundsTypesQuery().Where(item => item.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.kvVehicleFundsTypes.Clear();
                foreach(var vehicleFundsType in loadOp.Entities)
                    this.kvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = vehicleFundsType.Id,
                        Value = vehicleFundsType.Name,
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleDLRAccountFreezeBill,
                    EntityType = typeof(VehicleDLRAccountFreezeBill),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new DateTimeRangeQueryItem {
                            Title =  VehicleSalesUIStrings.QueryPanel_QueryItem_Title_ApproveTime,
                            ColumnName = "ApproveTime"
                        }, new KeyValuesQueryItem {
                             Title =  VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleDLRAccountFreezeBill_VehicleFundsTypeId,
                            ColumnName = "VehicleFundsTypeId",
                            KeyValueItems = kvVehicleFundsTypes
                        }, new QueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleDLRAccountFreezeBill_CustomerCompanyName,
                            ColumnName = "CustomerCompanyName"
                        }
                    }
                }
            };
        }
    }
}
