﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleDealerCreditLimitAppQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = new[] {
            "VehicleDealerCreditLimitApp_Status"
        };
        private void Initialize() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleFundsTypesQuery().Where(v => v.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var vehicleFundsType in loadOp.Entities)
                    this.kvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = vehicleFundsType.Id,
                        Value = vehicleFundsType.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehicleDealerCreditLimitApp),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleDealerCreditLimitApp,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        },new QueryItem {
                            ColumnName = "CustomerCompanyName",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleDealerCreditLimitApp_CustomerCompanyName
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                            Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleDealerCreditLimitApp_ApproveTime,
                            IsExact = false
                        },new KeyValuesQueryItem {
                            ColumnName = "VehicleFundsTypeId",
                            KeyValueItems = kvVehicleFundsTypes,
                            Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleDealerCreditLimitApp_VehicleFundsType
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }

        public VehicleDealerCreditLimitAppQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}