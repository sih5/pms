﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleFundsTypeQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "BaseData_Status"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleFundsType,
                    EntityType = typeof(VehicleFundsType),
                    QueryItems = new QueryItem[] {
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }

        public VehicleFundsTypeQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
