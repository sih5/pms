﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleInformationForSelectQueryPanel : DcsQueryPanelBase {
        public VehicleInformationForSelectQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    EntityType = typeof(VehicleInformation),
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleInformation,
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "VIN",
                            DataType=typeof(string)
                        }
                    }
                }
            };
        }
    }
}