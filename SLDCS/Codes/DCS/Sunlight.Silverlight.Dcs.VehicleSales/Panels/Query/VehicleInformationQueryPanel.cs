﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleInformationQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "Vehicle_Status"
        };
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehicleInformation),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleInformation,
                    QueryItems = new [] {
                        new QueryItem {
                            ColumnName = "VIN",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleInformation_VIN
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleInformation_Status
                        }, new QueryItem {
                            ColumnName = "EngineSerialNumber",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleInformation_EngineSerialNumber
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "RolloutDate",
                            IsExact = false,
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleInformation_RolloutDate
                        }, new QueryItem {
                            ColumnName = "ProductCode",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleInformation_ProductCode
                        }
                    }
                }
            };
        }

        public VehicleInformationQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}