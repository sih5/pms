﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleModelAffiProductQueryPanel : DcsQueryPanelBase {
        public VehicleModelAffiProductQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleModelAffiProduct,
                    EntityType = typeof(VehicleModelAffiProduct),
                    QueryItems = new[] {
                         new QueryItem {
                            ColumnName = "ProductCode"
                         }, new QueryItem {
                            ColumnName = "ProductName"
                         }, new QueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleModelAffiProduct_ProductCategoryCode,
                            ColumnName = "ProductCategoryCode"
                         },new QueryItem {
                            Title =VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleModelAffiProduct_ProductCategoryName,
                            ColumnName = "ProductCategoryName"
                         },
                    }
                }
            };
        }
    }
}
