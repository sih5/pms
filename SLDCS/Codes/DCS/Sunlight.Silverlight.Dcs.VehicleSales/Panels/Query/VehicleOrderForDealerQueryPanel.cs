﻿
using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleOrderForDealerQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "VehicleOrder_OrderType", "VehicleOrder_Status"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleOrderForDealer  ,
                    EntityType = typeof(VehicleOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrder_Code,
                        },new KeyValuesQueryItem {
                            ColumnName = "OrderType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            IsExact = false
                        },new QueryItem {
                            ColumnName = "YearOfPlan",
                        } ,new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                            IsExact = false
                        },new QueryItem {
                            ColumnName = "MonthOfPlan"
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false,
                            DefaultValue=new[]{
                                DateTime.Now,DateTime.Now
                            }
                        },new QueryItem {
                            ColumnName = "DealerName"
                        }
                    }
                }
            };
        }

        public VehicleOrderForDealerQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
