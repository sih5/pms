﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleOrderForExportQueryPanel : DcsQueryPanelBase {

        private readonly string[] kvNames = new[] {
            "VehicleOrder_OrderType","VehicleOrder_Status","VehicleOrderDetail_Status"
        };

        private void Initialize() {

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehicleOrder),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_DealerVehicleMonthQuota,
                    QueryItems = new[] {
                        new QueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrder_Code,
                            ColumnName = "Code"
                        },new KeyValuesQueryItem {
                            ColumnName = "OrderType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new QueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrder_YearOfPlan,
                            ColumnName = "YearOfPlan"
                        }, new QueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrder_MonthOfPlan,
                            ColumnName = "MonthOfPlan"
                        },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        },new KeyValuesQueryItem {
                            Title =VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrder_Status,
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        },new KeyValuesQueryItem {
                            Title =VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrderDetail_Status,
                            ColumnName = "VehicleOrderDetail.Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                        },new QueryItem {
                             Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrder_DealerName,
                            ColumnName = "DealerName"
                        }
                    }
                }
            };
        }

        public VehicleOrderForExportQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
