﻿
using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleOrderForSubmitQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "VehicleOrder_OrderType", "VehicleOrder_Status"
        };

        public VehicleOrderForSubmitQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehicleOrder),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleOrder,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrder_Code
                        }, new KeyValuesQueryItem {
                            ColumnName = "OrderType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new QueryItem {
                            ColumnName = "YearOfPlan"
                        }, new KeyValuesQueryItem {
                           ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new QueryItem {
                            ColumnName = "MonthOfPlan"
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue=new[]{
                                DateTime.Now,DateTime.Now
                            }
                        }
                    }
                }
            };
        }
    }
}
