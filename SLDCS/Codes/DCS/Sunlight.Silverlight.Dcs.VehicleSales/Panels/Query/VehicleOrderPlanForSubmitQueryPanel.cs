﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleOrderPlanForSubmitQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "VehicleOrderPlan_Status"
        };

        public VehicleOrderPlanForSubmitQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehicleOrderPlan),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleOrderPlan,
                    QueryItems = new[] {
                        new CustomQueryItem {
                            DataType = typeof(int),
                            ColumnName = "YearOfPlan"
                        }, new QueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Tilte_VehicleOrderPlan_Code,
                            ColumnName = "Code"
                        }, new CustomQueryItem {
                            DataType = typeof(int),
                            ColumnName = "MonthOfPlan",
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrderPlanStatus
                        }
                    }
                }
            };
        }
    }
}
