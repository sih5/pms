﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleOrderPlanQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "VehicleOrderPlan_Status"
        };

        public VehicleOrderPlanQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =VehicleSalesUIStrings.QueryPanel_Title_VehicleOrderPlan,
                    EntityType = typeof(VehicleOrderPlan),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "YearOfPlan"
                        }, new QueryItem {
                            ColumnName = "Code",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Tilte_VehicleOrderPlan_Code
                        },new QueryItem {
                            ColumnName = "MonthOfPlan"
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime",
                            DefaultValue=new[]{
                                DateTime.Now,DateTime.Now
                            }
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrderPlanStatus
                        },new QueryItem {
                            IsExact = false,
                            ColumnName = "DealerName"
                        }
                    }
                }
            };
        }
    }
}
