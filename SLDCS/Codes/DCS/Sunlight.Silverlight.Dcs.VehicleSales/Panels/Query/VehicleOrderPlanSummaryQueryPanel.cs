﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleOrderPlanSummaryQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "VehicleOrderPlanSummary_UploadType"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleOrderPlanSummary,
                    EntityType = typeof(VehicleOrderPlanSummary),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "YearOfOrder",
                            IsExact = false
                        },new KeyValuesQueryItem {
                            ColumnName = "UploadType",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new QueryItem {
                            ColumnName = "MonthOfOrder",
                            IsExact = false
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false
                        },new QueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrderPlanSummary_DealerName,
                            ColumnName = "DealerName"
                        },new QueryItem {
                            ColumnName = "ProductCode"
                        }
                    }
                }
            };
        }

        public VehicleOrderPlanSummaryQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
