﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleOrderScheduleQueryPanel : DcsQueryPanelBase {
        public VehicleOrderScheduleQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =VehicleSalesUIStrings.QueryPanel_Title_VehicleOrderSchedule,
                    EntityType = typeof(VehicleOrderSchedule),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "YearOfOrder",
                            IsExact = true
                        }, new QueryItem {
                            ColumnName = "MonthOfOrder",
                            IsExact = true
                        },new DateTimeRangeQueryItem {
                            ColumnName = "OrderPlanToPODate"
                        },new DateTimeRangeQueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleOrderSchedule_POToProductionPlanDate,
                            ColumnName = "POToProductionPlanDate"
                        },new DateTimeRangeQueryItem {
                            ColumnName = "OrderSubmitDeadline"
                        }
                    }
                }
            };
        }
    }
}
