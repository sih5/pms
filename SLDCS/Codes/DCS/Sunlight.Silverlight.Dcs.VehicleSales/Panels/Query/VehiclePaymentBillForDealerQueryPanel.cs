﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;


namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehiclePaymentBillForDealerQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvVehiclePaymentMethods = new ObservableCollection<KeyValuePair>();
        private readonly ObservableCollection<KeyValuePair> kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>();
        private readonly DcsDomainContext domainContext = new DcsDomainContext();

        private readonly string[] kvNames = {
            "VehiclePaymentBill_Status"
       };

        public VehiclePaymentBillForDealerQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.domainContext.Load(this.domainContext.GetVehiclePaymentMethodsQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var vehiclePaymentMethod in loadOp.Entities)
                    this.kvVehiclePaymentMethods.Add(new KeyValuePair {
                        Key = vehiclePaymentMethod.Id,
                        Value = vehiclePaymentMethod.Name
                    });
            }, null);
            this.domainContext.Load(this.domainContext.GetVehicleFundsTypesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var vehicleFundsType in loadOp.Entities)
                    this.kvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = vehicleFundsType.Id,
                        Value = vehicleFundsType.Name
                    });
            }, null);

            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "VehiclePaymentBill",
                    EntityType = typeof(VehiclePaymentBill),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehiclePaymentBill,
                    QueryItems = new QueryItem[] {
                        new KeyValuesQueryItem {
                            Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePaymentMethod_Name,
                            KeyValueItems = this.kvVehiclePaymentMethods,
                            ColumnName = "VehiclePaymentMethodId",
                            IsExact = false,
                        }, new KeyValuesQueryItem {
                            Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleFundsType_Name,
                            KeyValueItems = this.kvVehicleFundsTypes,
                            ColumnName = "VehicleFundsTypeId",
                            IsExact = false
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "TimeOfIncomingPayment",
                            IsExact = false
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            IsExact = false
                        }
                    }
                }
            };
        }
    }
}
