﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehiclePaymentBillQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;
        private ObservableCollection<KeyValuePair> kvVehiclePaymentMethods;

        private readonly string[] kvNames = {
            "VehiclePaymentBill_Status"
        };

        public VehiclePaymentBillQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<KeyValuePair> KvVehiclePaymentMethods {
            get {
                return this.kvVehiclePaymentMethods ?? (this.kvVehiclePaymentMethods = new ObservableCollection<KeyValuePair>());
            }
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetVehicleFundsTypesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOption => {
                if(loadOption.HasError)
                    return;
                //该方法只执行一次，所以没有清空操作
                foreach(var vehicleFundsType in loadOption.Entities)
                    this.KvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = vehicleFundsType.Id,
                        Value = vehicleFundsType.Name
                    });

                dcsDomainContext.Load(dcsDomainContext.GetVehiclePaymentMethodsQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError)
                        return;
                    //该方法只执行一次，所以没有清空操作
                    foreach(var vehiclePaymentMethod in loadOp.Entities)
                        this.KvVehiclePaymentMethods.Add(new KeyValuePair {
                            Key = vehiclePaymentMethod.Id,
                            Value = vehiclePaymentMethod.Name
                        });

                    this.QueryItemGroups = new[] {
                        new QueryItemGroup {
                            UniqueId = "Common",
                            Title =VehicleSalesUIStrings.QueryPanel_Title_VehiclePaymentBill,
                            EntityType = typeof(VehiclePaymentBill),
                            QueryItems = new[] {
                                new QueryItem {
                                    Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehiclePaymentBill_CustomerCompanyName,
                                    ColumnName = "CustomerCompanyName",
                                    IsExact=false,
                                }, new KeyValuesQueryItem {
                                    ColumnName = "Status",
                                    IsExact=false,
                                    KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                                },new KeyValuesQueryItem {
                                    Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehiclePaymentBill_VehiclePaymentMethod,
                                    ColumnName = "VehiclePaymentMethodId",
                                    KeyValueItems = this.KvVehiclePaymentMethods,
                                    IsExact = false
                               },new KeyValuesQueryItem {
                                   Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehiclePaymentBill_VehicleFundsType,
                                    ColumnName = "VehicleFundsTypeId",
                                    KeyValueItems = this.KvVehicleFundsTypes,
                                    IsExact=false,
                                },new DateTimeRangeQueryItem {
                                    ColumnName = "CreateTime",
                                    IsExact=false,
                                }, new DateTimeRangeQueryItem{
                                    ColumnName = "TimeOfIncomingPayment",
                                    IsExact=false,
                                }
                            }
                        }
                    };
                }, null);
            }, null);
        }
    }
}
