﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehiclePaymentMethodQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] { 
            "BaseData_Status"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId="Common",
                    Title=VehicleSalesUIStrings.QueryPanel_Title_VehiclePaymentMethod,
                    EntityType=typeof(VehiclePaymentMethod),
                    QueryItems = new QueryItem[] {
                        new DateTimeRangeQueryItem{
                            ColumnName="CreateTime"
                        }, new KeyValuesQueryItem{
                            ColumnName="Status",
                            KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }

        public VehiclePaymentMethodQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
