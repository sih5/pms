﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehiclePreAllocationRefForManagerQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "VehicleOrder_OrderType"
        };

        public VehiclePreAllocationRefForManagerQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehiclePreAllocationRef,
                    EntityType = typeof(VehiclePreAllocationRef),
                    QueryItems = new[] {
                       new QueryItem {
                            ColumnName = "YearOfOrder"
                       },new KeyValuesQueryItem {
                           ColumnName="OrderType",
                           IsExact = false,
                           KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                       }, new QueryItem {
                            ColumnName = "MonthOfOrder"
                       },new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false,
                        }, new QueryItem {
                            ColumnName = "DealerName"
                       },new QueryItem {
                            ColumnName = "ProductCode"
                       }
                    }
                }
            };
        }
    }
}
