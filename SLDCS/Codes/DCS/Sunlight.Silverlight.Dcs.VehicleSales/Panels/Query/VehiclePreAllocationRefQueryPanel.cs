﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehiclePreAllocationRefQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "VehicleOrder_OrderType"
        };

        public VehiclePreAllocationRefQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehiclePreAllocationRef,
                    EntityType = typeof(VehiclePreAllocationRef),
                    QueryItems = new[] {
                       new QueryItem {
                            ColumnName = "DealerCode"
                       },new KeyValuesQueryItem {
                           ColumnName="OrderType",
                           IsExact = false,
                           KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                       }
                    }
                }
            };
        }

    }
}
