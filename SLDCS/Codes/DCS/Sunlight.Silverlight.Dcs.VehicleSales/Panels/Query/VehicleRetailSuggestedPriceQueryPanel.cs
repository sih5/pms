﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleRetailSuggestedPriceQueryPanel : DcsQueryPanelBase {

        public VehicleRetailSuggestedPriceQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] { 
                new QueryItemGroup{
                    UniqueId="Common",
                    Title=VehicleSalesUIStrings.QueryPanel_Title_VehicleRetailSuggestedPrice,
                    EntityType=typeof(VehicleRetailSuggestedPrice),
                    QueryItems=new []{
                        new QueryItem{
                            ColumnName="ProductCode",
                        }, new DateTimeRangeQueryItem{
                            ColumnName="ExecutionTime",
                            DefaultValue=new[]{
                                DateTime.Now,DateTime.Now.AddDays(1)
                            },IsExact=false
                        }, new QueryItem{
                            ColumnName="ProductName",
                        }
                    }
                }
            };
        }
    }
}
