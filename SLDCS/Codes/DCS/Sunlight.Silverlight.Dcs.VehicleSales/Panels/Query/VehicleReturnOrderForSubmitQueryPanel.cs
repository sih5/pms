﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleReturnOrderForSubmitQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "VehicleReturnOrder_Status"
        };
        public VehicleReturnOrderForSubmitQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =VehicleSalesUIStrings.QueryPanel_Title_VehicleReturnOrder,
                    EntityType = typeof(VehicleReturnOrder),
                    QueryItems = new QueryItem[] {
                        new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new KeyValuesQueryItem {
                            ColumnName= "Status",
                             KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        }
                    }
                }
            };
        }
    }
}
