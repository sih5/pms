﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleReturnOrderQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "VehicleReturnOrder_Status"
        };

        public VehicleReturnOrderQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =VehicleSalesUIStrings.QueryPanel_Title_VehicleReturnOrder,
                    EntityType = typeof(VehicleReturnOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code"
                        }, new KeyValuesQueryItem{
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        },new QueryItem {
                            ColumnName = "DealerName"
                        }
                    }
                }
            };
        }
    }
}
