﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleSeriesQueryPanel : DcsQueryPanelBase {
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehicleSery),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleSeries,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "ProductCode"
                        }, new QueryItem {
                            ColumnName = "ProductCategoryCode",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_ProductCategoryCode,
                        }, new QueryItem {
                            ColumnName = "VehicleSeriesCode"
                        }, new QueryItem {
                            ColumnName = "RegionCode"
                        }
                    }
                }
            };
        }

        public VehicleSeriesQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
    }
}