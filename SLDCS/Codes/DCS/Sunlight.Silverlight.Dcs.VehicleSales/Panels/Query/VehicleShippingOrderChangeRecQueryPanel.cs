﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleShippingOrderChangeRecQueryPanel : DcsQueryPanelBase {

        private void Initialize() {
            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehicleShippingOrderChangeRec),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrderChangeRec,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "OriginalVIN"
                        }, new QueryItem {
                            ColumnName = "NewVIN"
                        }
                    }
                }
            };
        }

        public VehicleShippingOrderChangeRecQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
    }
}
