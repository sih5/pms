﻿using System;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleShippingOrderConfirmQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "VehicleShippingOrder_Status","VehicleShipping_Method"
        };
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehicleShippingOrder),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder,
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title =VehicleSalesUIStrings.QueryPanel_QueryItem_VehicleShippingOrder_Code,
                        }, new KeyValuesQueryItem {
                            ColumnName = "ShippingMethod",
                            KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                        }, new CustomQueryItem {
                            ColumnName = "VehicleShippingDetail.ShippingDate",
                            Title =VehicleSalesUIStrings.QueryPanel_QueryItem_VehicleShippingOrder_ShippingDate,
                            DataType = typeof(DateTime),
                            IsExact = false
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            IsExact = false
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new CustomQueryItem  {
                             Title = Utils.GetEntityLocalizedName(typeof(VehicleShippingDetail), "VIN"),
                             ColumnName = "VehicleShippingDetail.VIN",
                             DataType = typeof(string)
                        }
                    }
                }
            };
        }

        public VehicleShippingOrderConfirmQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}