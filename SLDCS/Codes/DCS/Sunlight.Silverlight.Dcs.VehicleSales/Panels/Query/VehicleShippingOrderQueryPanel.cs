﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleShippingOrderQueryPanel : DcsQueryPanelBase {
        private readonly ObservableCollection<KeyValuePair> kvVehicleWarehouses = new ObservableCollection<KeyValuePair>();
        private readonly string[] kvNames = {
            "VehicleShippingOrder_Status", "VehicleShipping_Method", "VehicleShippingDetail_VehicleReceptionStatus"
        };

        public VehicleShippingOrderQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetVehicleWarehousesQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var vehiclewarehouse in loadOp.Entities)
                    this.kvVehicleWarehouses.Add(new KeyValuePair {
                        Key = vehiclewarehouse.Id,
                        Value = vehiclewarehouse.Name
                    });
            }, null);
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder,
                    EntityType = typeof(VehicleShippingOrder),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "Code",
                            Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_Code,
                            IsExact = false
                        }, new KeyValuesQueryItem {
                            ColumnName = "DispatchWarehouseId",
                            Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_DispatchWarehouseId,
                            KeyValueItems = this.kvVehicleWarehouses,
                        }, new QueryItem {
                            ColumnName = "DealerName",
                            Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_DealerName,
                            IsExact = false
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new CustomQueryItem {
                            DataType = typeof(string),
                            ColumnName = "VIN",
                            Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_VIN,
                            IsExact = false
                        }, new CustomQueryItem {
                            DataType = typeof(DateTime),
                            ColumnName = "ShippingDate",
                            Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_ShippingDate
                        }
                    }
                }
            };
        }
    }
}
