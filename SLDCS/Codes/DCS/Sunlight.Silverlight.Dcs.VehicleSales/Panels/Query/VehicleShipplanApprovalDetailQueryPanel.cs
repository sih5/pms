﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleShipplanApprovalDetailQueryPanel : DcsQueryPanelBase {
        public VehicleShipplanApprovalDetailQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private readonly string[] kvNames = new[] {
            "VehicleShipplanApprovalDetail_Status"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title =VehicleSalesUIStrings.QueryPanel_Title_VehicleShipplanApprovalDetail,
                    EntityType = typeof(VehicleShipplanApprovalDetail),
                    QueryItems = new QueryItem[]{
                        new CustomQueryItem {
                            ColumnName="DealerName",
                            DataType = typeof(string),
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_VehicleShipplanApproval_DealerName
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "ApproveTime",
                             Title = VehicleSalesUIStrings.QueryPanel_QueryItem_VehicleShipplanApproval_ApproveTime
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }
    }
}
