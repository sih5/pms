﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleShipplanApprovalForSecondQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;
        private ObservableCollection<KeyValuePair> keyProductCategories;

        private readonly string[] kvNames = {
            "VehicleShipplanApproval_Status"
        };

        public VehicleShipplanApprovalForSecondQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private ObservableCollection<KeyValuePair> KeyProductCategories {
            get {
                return this.keyProductCategories ?? (this.keyProductCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetVehicleFundsTypesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                //该方法只执行一次，所以没有清空操作
                foreach(var vehicleFundsType in loadOp.Entities)
                    this.KvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = vehicleFundsType.Id,
                        Value = vehicleFundsType.Name
                    });

                dcsDomainContext.Load(dcsDomainContext.GetProductCategoriesQuery().Where(r => r.ProductCategoryLevel == 2 && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOption => {
                    if(loadOption.HasError) {
                        loadOption.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOption.Error);
                        return;
                    }
                    this.KeyProductCategories.Clear();
                    foreach(var entity in loadOption.Entities) {
                        this.KeyProductCategories.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Code
                        });
                    }
                }, null);
                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title =VehicleSalesUIStrings.QueryPanel_Title_VehicleShipplanApproval,
                        EntityType = typeof(VehicleShipplanApproval),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "Code",
                                IsExact=false,
                                Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleShipplanApproval_Code
                            }, new KeyValuesQueryItem {
                                ColumnName = "VehicleFundsTypeId",
                                IsExact=false,
                                KeyValueItems=this.KvVehicleFundsTypes,
                                Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleFundsType_Name
                            },new QueryItem {
                                ColumnName = "DealerName",
                           }, new DateTimeRangeQueryItem{
                                ColumnName = "CreateTime",
                                IsExact=false,
                                DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1),  new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)
                                }
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                IsExact=false,
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            },new KeyValuesQueryItem {
                                ColumnName = "ProductCategoryId",
                                IsExact=false,
                                KeyValueItems = this.KeyProductCategories,
                                Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleShipplanApproval_ProductCategoryCode
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
