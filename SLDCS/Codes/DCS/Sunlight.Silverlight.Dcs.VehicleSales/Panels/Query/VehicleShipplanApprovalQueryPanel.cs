﻿using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleShipplanApprovalQueryPanel : DcsQueryPanelBase {
        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;

        private readonly string[] kvNames = {
            "VehicleShipplanApproval_Status"
        };

        public VehicleShipplanApprovalQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        private void Initialize() {
            var dcsDomainContext = new DcsDomainContext();
            dcsDomainContext.Load(dcsDomainContext.GetVehicleFundsTypesQuery().Where(r=>r.Status==(int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                //该方法只执行一次，所以没有清空操作
                foreach(var vehicleFundsType in loadOp.Entities)
                    this.KvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = vehicleFundsType.Id,
                        Value = vehicleFundsType.Name
                    });

                this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title =VehicleSalesUIStrings.QueryPanel_Title_VehicleShipplanApproval,
                        EntityType = typeof(VehicleShipplanApproval),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "Code",
                                IsExact=false,
                                Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleShipplanApproval_Code
                            }, new KeyValuesQueryItem {
                                ColumnName = "VehicleFundsTypeId",
                                IsExact=false,
                                KeyValueItems=this.KvVehicleFundsTypes,
                                Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleFundsType_Name
                            },new QueryItem {
                                ColumnName = "DealerName",
                           }, new DateTimeRangeQueryItem{
                                ColumnName = "ApproveTime",
                                IsExact=false,
                                Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleShipplanApproval_ApproveTime
                            }, new KeyValuesQueryItem {
                                ColumnName = "Status",
                                IsExact=false,
                                KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                            },new QueryItem {
                                ColumnName = "SourceCode",
                                IsExact=false,
                                Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleShipplanApproval_SourceCode
                            }
                        }
                    }
                };
            }, null);
        }
    }
}
