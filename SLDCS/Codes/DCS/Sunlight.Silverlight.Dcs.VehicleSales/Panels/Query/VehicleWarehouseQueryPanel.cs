﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleWarehouseQueryPanel : DcsQueryPanelBase {
        public VehicleWarehouseQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                    new QueryItemGroup {
                        UniqueId = "Common",
                        Title =VehicleSalesUIStrings.QueryPanel_Title_VehicleWarehouse,
                        EntityType = typeof(VehicleWarehouse),
                        QueryItems = new[] {
                            new QueryItem {
                                ColumnName = "Code",
                            },new QueryItem {
                                ColumnName = "Name",
                           }
                        }
                    }
                };
        }
    }
}
