﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleWholesalePactPriceQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "VehiclePrice_Status"
        };
        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleWholesalePactPrice,
                    EntityType = typeof(VehicleWholesalePactPrice),
                    QueryItems = new[] {
                        new QueryItem {
                            ColumnName = "DealerCode"
                        },new QueryItem {
                            ColumnName = "DealerName"
                        },new QueryItem {
                            ColumnName = "ProductCode"
                        },new QueryItem {
                            ColumnName = "ProductName"
                        },new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new QueryItem {
                            ColumnName = "VehicleSalesTypeName"
                        }
                    }
                }
            };
        }
        public VehicleWholesalePactPriceQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
