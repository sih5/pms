﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class VehicleWholesalePriceQueryPanel : DcsQueryPanelBase {

        private void Initialize() {
            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(VehicleWholesalePrice),
                    Title =VehicleSalesUIStrings.QueryPanel_Title_VehicleWholesalePrice,
                    QueryItems = new [] {
                        new QueryItem {
                            ColumnName = "ProductCode"
                        }, new QueryItem {
                            ColumnName = "ProductName"
                        }, new DateTimeRangeQueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Tilte_VehicleWholesalePrice_ExecutionDate,
                            ColumnName = "ExecutionDate",
                             IsExact = true,
                            DefaultValue = new [] {
                              new DateTime(DateTime.Now.Year,DateTime.Now.Month,1),DateTime.Now.Date.AddDays(1)
                            }
                        }
                    }
                }
            };
        }

        public VehicleWholesalePriceQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
    }
}
