﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class WholesaleApprovalForApproveQueryPanel : DcsQueryPanelBase {
        private void Initialize() {
            this.QueryItemGroups = new[] { 
                new QueryItemGroup {
                    UniqueId="Common",
                    EntityType=typeof(WholesaleApproval),
                    Title=VehicleSalesUIStrings.QueryPanel_Title_WholesaleApprovalForAprrove,
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem { 
                            ColumnName = "KeyAccount.KeyAccountCode",
                            DataType = typeof(string),
                            IsExact = true,
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_KeyAccount_KeyAccountCode
                        }, new CustomQueryItem {
                           ColumnName = "KeyAccount.Customer.Name",
                            DataType = typeof(string),
                            IsExact = true,
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_Customer_CustomerName
                        }, new QueryItem {
                            ColumnName = "DealerName",
                            IsExact = true
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[]{
                                DateTime.Now,DateTime.Now.AddDays(1)
                            },
                            IsExact = false
                        }
                    }
                }
            };
        }

        public WholesaleApprovalForApproveQueryPanel() {
            this.Initializer.Register(this.Initialize);
        }
    }
}
