﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class WholesaleApprovalForSelectQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "MultiLevelAuditConfig_InitialStatus"
        };

        private void Initialize() {
            QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(WholesaleApproval),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_WholesaleApproval,
                    QueryItems = new[] {
                        new CustomQueryItem {
                            ColumnName = "KeyAccount.KeyAccountCode",
                            Title =  VehicleSalesUIStrings.QueryPanel_QueryItem_Title_WholesaleApproval_KeyAccountCode,
                            DataType = typeof(string)
                        }, new CustomQueryItem {
                            ColumnName = "KeyAccount.Customer.Name",
                            DataType = typeof(string),
                            Title =  VehicleSalesUIStrings.QueryPanel_QueryItem_Title_WholesaleApproval_CustomerName
                        }, new QueryItem {
                            ColumnName = "DealerName",
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_WholesaleApproval_Dealer
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue = new[] {
                                new DateTime(DateTime.Now.Year, 1, 1), DateTime.Now
                            }
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                            DefaultValue=(int)DcsMultiLevelAuditConfigInitialStatus.审核通过,
                            IsEnabled = false
                        }
                    }
                }
            };
        }

        public WholesaleApprovalForSelectQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
