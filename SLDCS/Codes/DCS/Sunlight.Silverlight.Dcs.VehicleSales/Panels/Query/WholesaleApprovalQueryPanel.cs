﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class WholesaleApprovalQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = {
            "MultiLevelAuditConfig_InitialStatus"
        };

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    EntityType = typeof(WholesaleApproval),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_WholesaleApproval,
                    QueryItems = new QueryItem[] {
                        new CustomQueryItem {
                            ColumnName = "KeyAccount.Customer.Code",
                            DataType = typeof(string),
                            IsExact = true,
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_WholesaleApproval_Code
                        },new CustomQueryItem {
                            ColumnName = "KeyAccount.Customer.Name",
                            DataType = typeof(string),
                            IsExact = true,
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_WholesaleApproval_Name
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime",
                            DefaultValue=new[]{
                                DateTime.Now,DateTime.Now.AddDays(1)
                            },
                            IsExact = false
                        },new CustomQueryItem {
                            ColumnName = "KeyAccount.KeyAccountCode",
                            DataType = typeof(string),
                            Title = VehicleSalesUIStrings.QueryPanel_Title_WholesaleApproval_KeyAccountCode
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            IsExact = false,
                            DefaultValue =  (int)DcsMultiLevelAuditConfigInitialStatus.新建,
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }

        public WholesaleApprovalQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}