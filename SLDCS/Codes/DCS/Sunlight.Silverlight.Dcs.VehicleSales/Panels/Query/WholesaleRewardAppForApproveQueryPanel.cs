﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class WholesaleRewardAppForApproveQueryPanel : DcsQueryPanelBase {
        private readonly string[] kvNames = new[] {
            "WholesaleRewardApp_Status"
        };

        public WholesaleRewardAppForApproveQueryPanel() {
            this.Initializer.Register(this.Initialize);
            this.KeyValueManager.Register(this.kvNames);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup {
                    UniqueId = "Common",
                    Title = VehicleSalesUIStrings.QueryPanel_Title_WholesaleRewardAppForApprove,
                    EntityType = typeof(WholesaleRewardApp),
                    QueryItems = new[] {
                        new CustomQueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_KeyAccount_KeyAccountCode,
                            ColumnName = "KeyAccount.KeyAccountCode",
                            DataType = typeof(string),
                            IsExact = true
                        }, new CustomQueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_Customer_Name,
                            ColumnName = "KeyAccount.Customer.Name",
                            DataType = typeof(string),
                            IsExact = true
                        }, new QueryItem {
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_WholesaleRewardApp_DealerName,
                            ColumnName = "DealerName",
                        }, new DateTimeRangeQueryItem {
                            ColumnName = "CreateTime"
                        }, new KeyValuesQueryItem {
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }
                    }
                }
            };
        }
    }
}
