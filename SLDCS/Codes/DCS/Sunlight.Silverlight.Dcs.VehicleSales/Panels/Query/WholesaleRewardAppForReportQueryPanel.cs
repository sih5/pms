﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Panels.Query {
    public class WholesaleRewardAppForReportQueryPanel : DcsQueryPanelBase {
        private string[] kvNames = { 
            "WholesaleRewardApp_Status" 
        };

        public WholesaleRewardAppForReportQueryPanel() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.QueryItemGroups = new[] {
                new QueryItemGroup{
                    UniqueId = "Common",
                    EntityType = typeof(WholesaleRewardApp),
                    Title = VehicleSalesUIStrings.QueryPanel_Title_WholesaleRewardApp,
                    QueryItems = new QueryItem[]{
                        new CustomQueryItem{
                            ColumnName = "KeyAccountCode",
                            DataType = typeof(string),
                            IsExact = true,
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_KeyAccount_KeyAccountCode
                        }, new CustomQueryItem{
                            ColumnName = "CustomerName",
                            DataType = typeof(string),
                            IsExact = true,
                            Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_WholesaleRewardApp_CustomerName
                        }, new KeyValuesQueryItem{
                            ColumnName = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        }, new DateTimeRangeQueryItem{
                            ColumnName = "CreateTime"
                        }
                    }
                }
            };
        }
    }
}
