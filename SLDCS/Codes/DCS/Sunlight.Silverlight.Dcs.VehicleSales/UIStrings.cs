﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.VehicleSales {
    public class UIStrings : ModelBase {
        public void Refresh(params string[] propertyNames) {
            this.NotifyOfPropertiesChange(propertyNames.Length == 0 ? this.GetType().GetProperties().Select(p => p.Name).ToArray() : propertyNames);
        }   

            public string DataPanel_GroupTitle_DetailInfo {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_GroupTitle_DetailInfo;
            }
        }

        public string DataPanel_Text_Common_Dealer {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_Dealer;
            }
        }

        public string DataPanel_Text_Common_Span_To {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_Span_To;
            }
        }

        public string DataPanel_Text_Common_Unit_Currency {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_Unit_Currency;
            }
        }

        public string DataPanel_Text_Common_Unit_Km {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_Unit_Km;
            }
        }

        public string DataPanel_Text_Common_Unit_Month {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_Unit_Month;
            }
        }

        public string DataPanel_Text_Common_ServiceProductLineName {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_ServiceProductLineName;
            }
        }

        public string DataPanel_GroupTitle_VehicleInfo {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_GroupTitle_VehicleInfo;
            }
        }

        public string DataPanel_Text_Common_BranchShort {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_BranchShort;
            }
        }

        public string DataPanel_Text_Common_Branch {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_Branch;
            }
        }

        public string DataPanel_Text_Common_ServiceProductLine {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_ServiceProductLine;
            }
        }

        public string DataPanel_Text_Common_Quantity {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_Quantity;
            }
        }

        public string DataPanel_Text_Common_BranchName {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_BranchName;
            }
        }

        public string DataPanel_Text_Common_DealerName {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_DealerName;
            }
        }

        public string DataPanel_GroupTitle_BasicInfo {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_GroupTitle_BasicInfo;
            }
        }

        public string DataPanel_Text_Common_BranchCode {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_BranchCode;
            }
        }

        public string DataPanel_Text_Common_DealerCode {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_DealerCode;
            }
        }

        public string Button_Text_Common_NewBill {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_NewBill;
            }
        }

        public string DataPanel_Text_Common_ServiceProductLineCode {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_ServiceProductLineCode;
            }
        }

        public string DataPanel_Text_Common_Span_During {
            get {
                return Resources.VehicleSalesUIStrings.DataPanel_Text_Common_Span_During;
            }
        }

        public string DataEditPanel_Text_OEMVehicleMonthQuota_MonthOfPlan {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_OEMVehicleMonthQuota_MonthOfPlan;
            }
        }

        public string DataEditPanel_Text_OEMVehicleMonthQuota_RetailQuotaQuantity {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_OEMVehicleMonthQuota_RetailQuotaQuantity;
            }
        }

        public string DataEditPanel_Text_OEMVehicleMonthQuota_VehicleModelCategoryName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_OEMVehicleMonthQuota_VehicleModelCategoryName;
            }
        }

        public string DataEditPanel_Text_OEMVehicleMonthQuota_WholesaleQuotaQuantity {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_OEMVehicleMonthQuota_WholesaleQuotaQuantity;
            }
        }

        public string DataEditPanel_Text_OEMVehicleMonthQuota_YearOfPlan {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_OEMVehicleMonthQuota_YearOfPlan;
            }
        }

        public string QueryPanel_Title_Dealer {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_Dealer;
            }
        }

        public string DataEditPanel_Text_DealerVehicleMonthQuota_MonthOfPlan {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerVehicleMonthQuota_MonthOfPlan;
            }
        }

        public string DataEditPanel_Text_DealerVehicleMonthQuota_RetailQuotaQuantity {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerVehicleMonthQuota_RetailQuotaQuantity;
            }
        }

        public string DataEditPanel_Text_DealerVehicleMonthQuota_VehicleModelCategoryName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerVehicleMonthQuota_VehicleModelCategoryName;
            }
        }

        public string DataEditPanel_Text_DealerVehicleMonthQuota_WholesaleQuotaQuantity {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerVehicleMonthQuota_WholesaleQuotaQuantity;
            }
        }

        public string DataEditPanel_Text_DealerVehicleMonthQuota_YearOfPlan {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerVehicleMonthQuota_YearOfPlan;
            }
        }

        public string DataEditPanel_Text_VehicleOrder_Balance {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleOrder_Balance;
            }
        }

        public string DataEditPanel_Text_VehicleOrder_Code {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleOrder_Code;
            }
        }

        public string DataEditPanel_Text_VehicleOrder_TotalAmount {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleOrder_TotalAmount;
            }
        }

        public string DataEditPanel_Text_VehicleOrder_VehicleFundsType {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleOrder_VehicleFundsType;
            }
        }

        public string DataEditPanel_Title_VehicleOrderSchedule_POToProductionPlanDate {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Title_VehicleOrderSchedule_POToProductionPlanDate;
            }
        }

        public string Button_Text_Common_Search {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_Search;
            }
        }

        public string DataEditView_Text_VehicleOrderPlan_Code {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleOrderPlan_Code;
            }
        }

        public string Button_Text_Common_Prompt {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_Prompt;
            }
        }

        public string DataEditView_Text_Title_Prompt {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_Title_Prompt;
            }
        }

        public string DataEditView_Text_VehicleShipplanApproval_SourceCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleShipplanApproval_SourceCode;
            }
        }

        public string DataEditView_Text_VehicleShipplanApproval_ApproveAmount {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleShipplanApproval_ApproveAmount;
            }
        }

        public string DataEditView_Text_VehicleShipplanApproval_AvailableAmount {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleShipplanApproval_AvailableAmount;
            }
        }

        public string DataEditView_Text_VehicleShipplanApproval_WarehouseName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleShipplanApproval_WarehouseName;
            }
        }

        public string DataEditView_Text_VehicleShipplanApproval_VehicleFundsTypeName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleShipplanApproval_VehicleFundsTypeName;
            }
        }

        public string DataEditView_ButtonText_Search {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_ButtonText_Search;
            }
        }

        public string QueryPanel_Title_VehicleAvailableResource {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_VehicleAvailableResource;
            }
        }

        public string QueryPanel_Title_VehiclePreAllocationRef {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_VehiclePreAllocationRef;
            }
        }

        public string DataEditView_Text_AdjustmentSource {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_AdjustmentSource;
            }
        }

        public string DataEditView_Text_VehicleShipplanApprovalDetail {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleShipplanApprovalDetail;
            }
        }

        public string DataEditView_Text_VehicleShipplanApprovalDetail_AdjustmentType {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleShipplanApprovalDetail_AdjustmentType;
            }
        }

        public string DataEditView_Text_VehicleShipplanApprovalDetail_NewVin {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleShipplanApprovalDetail_NewVin;
            }
        }

        public string DataEditView_Text_VehicleShipplanApprovalDetail_VIN {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleShipplanApprovalDetail_VIN;
            }
        }

        public string DataEditView_Text_VehicleShipplanApproval_Code {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleShipplanApproval_Code;
            }
        }

        public string QueryPanel_Title_Product {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_Product;
            }
        }

        public string DataEditPanel_Text_ProductCategory_ParentCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_ProductCategory_ParentCode;
            }
        }

        public string DataEditPanel_Text_ProductCategory_ParentName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_ProductCategory_ParentName;
            }
        }

        public string DataEditPanel_Text_ProductCategory_RootCategoryCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_ProductCategory_RootCategoryCode;
            }
        }

        public string DataEditPanel_Text_ProductCategory_RootCategoryName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_ProductCategory_RootCategoryName;
            }
        }

        public string DataEditPanel_Common_Price_RetailForecastTotal {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_Price_RetailForecastTotal;
            }
        }

        public string DataEditPanel_Common_Price_Total {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_Price_Total;
            }
        }

        public string DataEditPanel_Common_Price_WholesaleForecastTotal {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_Price_WholesaleForecastTotal;
            }
        }

        public string DataEditPanel_Text_DealerForecastDetail_RetailForecastAmount {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerForecastDetail_RetailForecastAmount;
            }
        }

        public string DataEditPanel_Text_DealerForecastDetail_WholesaleForecastAmount {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerForecastDetail_WholesaleForecastAmount;
            }
        }

        public string DataEditPanel_Text_DealerForecast_Code {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerForecast_Code;
            }
        }

        public string DataEditPanel_Text_DealerForecast_ForecastDateMonths {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerForecast_ForecastDateMonths;
            }
        }

        public string DataEditPanel_Text_DealerForecast_ForecastDateYears {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerForecast_ForecastDateYears;
            }
        }

        public string DetailPanel_Text_Product_PurchaseCycle {
            get {
                return Resources.VehicleSalesUIStrings.DetailPanel_Text_Product_PurchaseCycle;
            }
        }

        public string DataEditPanel_Text_Product_ParentCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Product_ParentCode;
            }
        }

        public string DataEditPanel_Text_Product_ParentName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Product_ParentName;
            }
        }

        public string DataEditPanel_Text_Product_PurchaseCycle {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Product_PurchaseCycle;
            }
        }

        public string DataEditPanel_Common_EngineCylinder_OnePointFive {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_EngineCylinder_OnePointFive;
            }
        }

        public string DataEditPanel_Common_EngineCylinder_OnePointSix {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_EngineCylinder_OnePointSix;
            }
        }

        public string DataEditPanel_Common_EngineCylinder_OnePointThree {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_EngineCylinder_OnePointThree;
            }
        }

        public string DataEditPanel_Common_EngineCylinder_TwoPointZero {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_EngineCylinder_TwoPointZero;
            }
        }

        public string DataEditPanel_Common_Price_Subtotal {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_Price_Subtotal;
            }
        }

        public string DataEditPanel_Common_Sedan_Hatchback {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_Sedan_Hatchback;
            }
        }

        public string DataEditPanel_Common_Sedan_JinXiangHatchback {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_Sedan_JinXiangHatchback;
            }
        }

        public string DataEditPanel_Common_VehicleModel_Mazda {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_VehicleModel_Mazda;
            }
        }

        public string DataEditPanel_Common_VehicleModel_Mazda2 {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_VehicleModel_Mazda2;
            }
        }

        public string DataEditPanel_Common_VehicleModel_Mazda3 {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_VehicleModel_Mazda3;
            }
        }

        public string DataEditPanel_Common_VehicleModel_Mazda35HB {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_VehicleModel_Mazda35HB;
            }
        }

        public string DataEditPanel_Common_VehicleModel_Mazda3BU {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_VehicleModel_Mazda3BU;
            }
        }

        public string DataEditPanel_Common_VehicleModel_Mazda3Classic2012 {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_VehicleModel_Mazda3Classic2012;
            }
        }

        public string DataEditPanel_Common_VehicleModel_Mazda3SDN {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_VehicleModel_Mazda3SDN;
            }
        }

        public string DataEditPanel_Common_VehicleModel_MazdaChinaMadeCars {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_VehicleModel_MazdaChinaMadeCars;
            }
        }

        public string DataEditPanel_Common_WagonType_TheThreeCar {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Common_WagonType_TheThreeCar;
            }
        }

        public string DataEditPanel_RadButton_SelectPhoto {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_RadButton_SelectPhoto;
            }
        }

        public string DataEditPanel_Text_Region_City {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Region_City;
            }
        }

        public string DataEditPanel_Text_Region_District {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Region_District;
            }
        }

        public string DataEditPanel_Text_Region_Province {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Region_Province;
            }
        }

        public string DataEditPanel_Text_Region_Region {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Region_Region;
            }
        }

        public string QueryPanel_Title_TiledRegion {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_TiledRegion;
            }
        }

        public string DataEditPanel_Text_VehicleReturnOrder_DealerWarehouseName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleReturnOrder_DealerWarehouseName;
            }
        }

        public string Button_Text_Common_ApproveNoPass {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_ApproveNoPass;
            }
        }

        public string Button_Text_Common_ApprovePass {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_ApprovePass;
            }
        }

        public string DetailPanel_Title_VehicleReturnOrder_Code {
            get {
                return Resources.VehicleSalesUIStrings.DetailPanel_Title_VehicleReturnOrder_Code;
            }
        }

        public string DataEditPanel_Text_VehicleReturnOrder_VehicleFundsTypeId {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleReturnOrder_VehicleFundsTypeId;
            }
        }

        public string DataEditPanel_Text_DealerTransactionBill_DealerBuyerWarehouseCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerTransactionBill_DealerBuyerWarehouseCode;
            }
        }

        public string DataEditPanel_Text_DealerTransactionBill_DealerSellerName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerTransactionBill_DealerSellerName;
            }
        }

        public string Button_Text_Common_RegionManagerApproveNoPass {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_RegionManagerApproveNoPass;
            }
        }

        public string Button_Text_Common_RegionManagerApprovePass {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_RegionManagerApprovePass;
            }
        }

        public string DataEditView_Title_Approve_DealerForecast {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Title_Approve_DealerForecast;
            }
        }

        public string DataEditPanel_Text_VehRetailPriceChangeApp_ExecutionDate {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehRetailPriceChangeApp_ExecutionDate;
            }
        }

        public string DataEditPanel_Text_VehicleWholesalePactPrice_VehicleSalesTypeName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleWholesalePactPrice_VehicleSalesTypeName;
            }
        }

        public string DataEditPanel_Text_VehicleWholesalePactPrice_ExecutionDate {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleWholesalePactPrice_ExecutionDate;
            }
        }

        public string DataEditPanel_Text_VehicleWholesalePactPrice_ExpireDate {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleWholesalePactPrice_ExpireDate;
            }
        }

        public string QueryPanel_Title_VehicleRetailSuggestedPrice {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_VehicleRetailSuggestedPrice;
            }
        }

        public string DataEditPanel_Text_Region_CityName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Region_CityName;
            }
        }

        public string DataEditPanel_Text_Region_ProvinceName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Region_ProvinceName;
            }
        }

        public string DetailPanel_Text_VINChangeRecord_EngineNumberAfterChange {
            get {
                return Resources.VehicleSalesUIStrings.DetailPanel_Text_VINChangeRecord_EngineNumberAfterChange;
            }
        }

        public string DetailPanel_Text_VINChangeRecord_EngineNumberBeforeChange {
            get {
                return Resources.VehicleSalesUIStrings.DetailPanel_Text_VINChangeRecord_EngineNumberBeforeChange;
            }
        }

        public string DataGridView_ColumnItem_Title_VehicleRetailSuggestedPrice_Price {
            get {
                return Resources.VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleRetailSuggestedPrice_Price;
            }
        }

        public string DataEditPanel_Text_VehicleAvailableResource_No {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleAvailableResource_No;
            }
        }

        public string DataEditPanel_Text_VehicleAvailableResource_ProductCategoryCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleAvailableResource_ProductCategoryCode;
            }
        }

        public string DataEditPanel_Text_VehicleAvailableResource_VehicleWarehouseName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleAvailableResource_VehicleWarehouseName;
            }
        }

        public string DataEditPanel_Text_VehicleAvailableResource_Yes {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleAvailableResource_Yes;
            }
        }

        public string DataEditPanel_Text_VehicleInformation_ProductionPlanSON {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleInformation_ProductionPlanSON;
            }
        }

        public string DataEditPanel_Text_VehicleInformation_RolloutDate {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleInformation_RolloutDate;
            }
        }

        public string DataEditPanel_Text_VehPifaPriceChangeApp_ExecutedImmediatelyIsFalse {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehPifaPriceChangeApp_ExecutedImmediatelyIsFalse;
            }
        }

        public string DataEditPanel_Text_VehPifaPriceChangeApp_ExecutedImmediatelyIsTrue {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehPifaPriceChangeApp_ExecutedImmediatelyIsTrue;
            }
        }

        public string DataEditPanel_Text_VehPifaPriceChangeApp_ExecutionDate {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehPifaPriceChangeApp_ExecutionDate;
            }
        }

        public string DataEditPanel_Text_VehicleWholesalePactPrice_ProductPrice {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleWholesalePactPrice_ProductPrice;
            }
        }

        public string DataEditPanel_Text_VehicleInformation_ProductCategoryCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleInformation_ProductCategoryCode;
            }
        }

        public string DataEditPanel_Text_VehicleShippingOrder_Code {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleShippingOrder_Code;
            }
        }

        public string DataEditView_Text_VehicleAvailableResource {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleAvailableResource;
            }
        }

        public string DataEditView_Text_VehicleShippingOrder {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleShippingOrder;
            }
        }

        public string QueryPanel_Title_VehicleCustomerInformation {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_VehicleCustomerInformation;
            }
        }

        public string DataEditPanel_Text_VehicleDealerCreditLimitApp_CustomerCompanyCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleDealerCreditLimitApp_CustomerCompanyCode;
            }
        }

        public string DataEditPanel_Text_VehicleDealerCreditLimitApp_CustomerCompanyName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleDealerCreditLimitApp_CustomerCompanyName;
            }
        }

        public string DataEditPanel_Text_VehicleDealerCreditLimitApp_VehicleFundsType {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleDealerCreditLimitApp_VehicleFundsType;
            }
        }

        public string DataGridView_ColumnItem_Title_VehiclePaymentMethod_ApproveTime {
            get {
                return Resources.VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePaymentMethod_ApproveTime;
            }
        }

        public string QueryPanel_Title_VehicleCustomer {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_VehicleCustomer;
            }
        }

        public string DataEditPanel_Text_VehicleDLRAccountFreezeBill_CustomerCompanyCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleDLRAccountFreezeBill_CustomerCompanyCode;
            }
        }

        public string DataEditPanel_Text_VehicleDLRAccountFreezeBill_CustomerCompanyName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleDLRAccountFreezeBill_CustomerCompanyName;
            }
        }

        public string DataEditPanel_Text_VehicleDLRAccountFreezeBill_Status {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleDLRAccountFreezeBill_Status;
            }
        }

        public string QueryPanel_Title_VehicleCustomerInformation2 {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_VehicleCustomerInformation2;
            }
        }

        public string QueryPanel_Title_VehicleFundsType_Name {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_VehicleFundsType_Name;
            }
        }

        public string DataEditPanel_Text_VehicleAccountReceivableBill_CustomerCompanyCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleAccountReceivableBill_CustomerCompanyCode;
            }
        }

        public string DataEditPanel_Text_VehicleAccountReceivableBill_CustomerCompanyName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleAccountReceivableBill_CustomerCompanyName;
            }
        }

        public string DataEditPanel_Text_VehicleAccountReceivableBill_IssueBank {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleAccountReceivableBill_IssueBank;
            }
        }

        public string DataEditPanel_Text_VehicleAccountReceivableBill_Remark {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleAccountReceivableBill_Remark;
            }
        }

        public string DataEditPanel_Text_VehicleAccountReceivableBill_TransferOutTime {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleAccountReceivableBill_TransferOutTime;
            }
        }

        public string DataEditView_Text_VehicleAccountReceivableBill_VehicleFundsType {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleAccountReceivableBill_VehicleFundsType;
            }
        }

        public string DataEditView_Title_Import_VehicleDLRStartSecurityDeposit {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Title_Import_VehicleDLRStartSecurityDeposit;
            }
        }

        public string Button_Text_Common_Export {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_Export;
            }
        }

        public string Button_Text_Common_Import {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_Import;
            }
        }

        public string DataEditView_Text_VehicleBankAccount_BankAccountNumber {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleBankAccount_BankAccountNumber;
            }
        }

        public string DataEditView_Text_VehicleBankAccount_BankName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleBankAccount_BankName;
            }
        }

        public string DataEditView_Text_VehiclePaymentBill_CertificateSummary {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehiclePaymentBill_CertificateSummary;
            }
        }

        public string DataEditView_Text_VehiclePaymentBill_Code {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehiclePaymentBill_Code;
            }
        }

        public string DataEditView_Text_VehiclePaymentBill_CustomerCompanyCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehiclePaymentBill_CustomerCompanyCode;
            }
        }

        public string DataEditView_Text_VehiclePaymentBill_CustomerCompanyName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehiclePaymentBill_CustomerCompanyName;
            }
        }

        public string DataEditView_Text_VehiclePaymentBill_TimeOfIncomingPayment {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehiclePaymentBill_TimeOfIncomingPayment;
            }
        }

        public string DataEditView_Text_VehiclePaymentBill_VehicleFundsType {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehiclePaymentBill_VehicleFundsType;
            }
        }

        public string DataEditView_Text_VehiclePaymentBill_VehiclePaymentMethod {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehiclePaymentBill_VehiclePaymentMethod;
            }
        }

        public string DetailPanel_Content_btnApprovalNotPass {
            get {
                return Resources.VehicleSalesUIStrings.DetailPanel_Content_btnApprovalNotPass;
            }
        }

        public string DetailPanel_Content_btnApprovalPass {
            get {
                return Resources.VehicleSalesUIStrings.DetailPanel_Content_btnApprovalPass;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_ContactJobPosition {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_ContactJobPosition;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_CustomerAddress {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_CustomerAddress;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_CustomerCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_CustomerCode;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_CustomerCustomerProperty {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_CustomerCustomerProperty;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_CustomerName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_CustomerName;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_CustomerPostCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_CustomerPostCode;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_KeyAccountCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_KeyAccountCode;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_KeyAccountContactCellPhone {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_KeyAccountContactCellPhone;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_KeyAccountContactFax {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_KeyAccountContactFax;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_KeyAccountContactPerson {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_KeyAccountContactPerson;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_KeyAccountContactPhone {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_KeyAccountContactPhone;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_WholesaleApprovalId {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_WholesaleApprovalId;
            }
        }

        public string QueryPanel_Title_WholesaleApprovalForReport {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_WholesaleApprovalForReport;
            }
        }

        public string QueryPanel_Title_KeyAccount {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_KeyAccount;
            }
        }

        public string DataEditPanel_Text_Customer_Address {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Customer_Address;
            }
        }

        public string DataEditPanel_Text_Customer_Code {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Customer_Code;
            }
        }

        public string DataEditPanel_Text_Customer_Name {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Customer_Name;
            }
        }

        public string DataEditPanel_Text_KeyAccount_ContactJobPosition {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_KeyAccount_ContactJobPosition;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_WholesaleApprovalCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_WholesaleApprovalCode;
            }
        }

        public string DataEditPanel_Text_WholesaleApproval_CustomerCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleApproval_CustomerCode;
            }
        }

        public string DataEditPanel_Text_WholesaleApproval_CustomerName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleApproval_CustomerName;
            }
        }

        public string Button_Text_Common_InvertSelect {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_InvertSelect;
            }
        }

        public string Button_Text_Common_SelectAll {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_SelectAll;
            }
        }

        public string DataEditPanel_Text_Customer_PostCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Customer_PostCode;
            }
        }

        public string QueryPanel_Title_Customer {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_Customer;
            }
        }

        public string QueryPanel_Title_RegionalRoleConfig {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_RegionalRoleConfig;
            }
        }

        public string DataGridView_ColumnItem_Title_RegionalRoleConfigResource_Name {
            get {
                return Resources.VehicleSalesUIStrings.DataGridView_ColumnItem_Title_RegionalRoleConfigResource_Name;
            }
        }

        public string QueryPanel_Title_RegionalRoleConfigResource_PositionName {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_RegionalRoleConfigResource_PositionName;
            }
        }

        public string DataEditView_Text_VehicleWarehouse_Fax {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_VehicleWarehouse_Fax;
            }
        }

        public string DataEditPanel_Text_VehicleReturnOrder_ReturnedWarehouseName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleReturnOrder_ReturnedWarehouseName;
            }
        }

        public string DataEditPanel_Text_Customer_CityName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_Customer_CityName;
            }
        }

        public string DataEditPanel_Text_VehicleInformation_ProvinceName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleInformation_ProvinceName;
            }
        }

        public string DataEditPanel_Text_VehicleInformation_SalesDealerName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleInformation_SalesDealerName;
            }
        }

        public string DataEditPanel_Text_VehicleInformation_SalesInvoiceNumber {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleInformation_SalesInvoiceNumber;
            }
        }

        public string DataEditPanel_Text_VehicleInformation_VehicleLicensePlate {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleInformation_VehicleLicensePlate;
            }
        }

        public string DataEditView_Validation_RegionConfuguration_AuditHierarchySettingIdIsNotNull {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Validation_RegionConfuguration_AuditHierarchySettingIdIsNotNull;
            }
        }

        public string QueryPanel_Title_SpecialVehicleInformation {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_SpecialVehicleInformation;
            }
        }

        public string QueryPanel_Title_TileRegion {
            get {
                return Resources.VehicleSalesUIStrings.QueryPanel_Title_TileRegion;
            }
        }

        public string Button_Text_Common_ExportFile {
            get {
                return Resources.VehicleSalesUIStrings.Button_Text_Common_ExportFile;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_ApprovalComment {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_ApprovalComment;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_IfTenderingOrCentralPurchase {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_IfTenderingOrCentralPurchase;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp_DealerKeyAccountResponsible {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp_DealerKeyAccountResponsible;
            }
        }

        public string DataEditPanel_Text_WholesaleRewardApp__CustomerDetailCategory {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleRewardApp__CustomerDetailCategory;
            }
        }

        public string DataManagementView_PrintWindow_Title_WholesaleApproval {
            get {
                return Resources.VehicleSalesUIStrings.DataManagementView_PrintWindow_Title_WholesaleApproval;
            }
        }

        public string DataEditView_Text_CustomVehicleShipplanApproval_ApprovepAmount {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_CustomVehicleShipplanApproval_ApprovepAmount;
            }
        }

        public string DataEditView_Text_CustomVehicleShipplanApproval_AvailableAmount {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_CustomVehicleShipplanApproval_AvailableAmount;
            }
        }

        public string DataEditView_Text_CustomVehicleShipplanApproval_Code {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_CustomVehicleShipplanApproval_Code;
            }
        }

        public string DataEditView_Text_CustomVehicleShipplanApproval_DealerName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_CustomVehicleShipplanApproval_DealerName;
            }
        }

        public string DataEditView_Text_CustomVehicleShipplanApproval_VehicleCategory {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_CustomVehicleShipplanApproval_VehicleCategory;
            }
        }

        public string DataEditView_Text_CustomVehicleShipplanApproval_VehicleFundsType {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_CustomVehicleShipplanApproval_VehicleFundsType;
            }
        }

        public string DataEditView_Text_CustomVehicleShipplanApproval_VehicleOrderCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_CustomVehicleShipplanApproval_VehicleOrderCode;
            }
        }

        public string DataEditView_Text_CustomVehicleShipplanApproval_VehicleOrderType {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_CustomVehicleShipplanApproval_VehicleOrderType;
            }
        }

        public string DataEditView_Text_CustomVehicleShipplanApproval_VehicleWarehouseName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditView_Text_CustomVehicleShipplanApproval_VehicleWarehouseName;
            }
        }

        public string DataEditPanel_Text_WholesaleApproval_ApprovalComment {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleApproval_ApprovalComment;
            }
        }

        public string DataEditPanel_Text_WholesaleApproval_EmployeePurchaseDocOtherType {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleApproval_EmployeePurchaseDocOtherType;
            }
        }

        public string DataEditPanel_Text_WholesaleApproval_EmployeePurchaseDocType {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_WholesaleApproval_EmployeePurchaseDocType;
            }
        }

        public string DataEditPanel_Text_VehicleBankAccount_CustomerCompanyCode {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleBankAccount_CustomerCompanyCode;
            }
        }

        public string DataEditPanel_Text_VehicleBankAccount_CustomerCompanyName {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_VehicleBankAccount_CustomerCompanyName;
            }
        }

        public string DataGridView_ColumnItem_Title_VehModelRetailSuggestedPrice_ProductCategoryCode {
            get {
                return Resources.VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehModelRetailSuggestedPrice_ProductCategoryCode;
            }
        }

        public string DataEditPanel_Text_DealerForecast_ApprovalBy {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerForecast_ApprovalBy;
            }
        }

        public string DataEditPanel_Text_DealerForecast_ApprovalIsNotPassed {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerForecast_ApprovalIsNotPassed;
            }
        }

        public string DataEditPanel_Text_DealerForecast_NoBy {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerForecast_NoBy;
            }
        }

        public string DataEditPanel_Text_DealerForecast_PendingApproval {
            get {
                return Resources.VehicleSalesUIStrings.DataEditPanel_Text_DealerForecast_PendingApproval;
            }
        }

        public string DetailPanel_Title_VehicleAvailableResource {
            get {
                return Resources.VehicleSalesUIStrings.DetailPanel_Title_VehicleAvailableResource;
            }
        }

}
}
