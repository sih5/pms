﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.Custom {
    public partial class ProductCategoryAndProductView : IBaseView, INotifyPropertyChanged {
        private ProductCategoryForProductDataTreeView productCategoryForProductDataTreeView;
        private DataGridViewBase productDataGridView;
        public IEnumerable<Entity> productDataGridViewSelectedEntities;
        public IEnumerable<Entity> ProductDataGridViewSelectedEntities {
            get {
                return this.productDataGridViewSelectedEntities;
            }
            set {
                productDataGridViewSelectedEntities = value;
                this.OnPropertyChanged("ProductDataGridViewSelectedEntities");
            }
        }

        public DomainContext TreeViewDomainContext {
            get {
                return this.productCategoryForProductDataTreeView.DomainContext;
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public RadTreeViewItem selectedTreeViewItem;
        public RadTreeViewItem SelectedTreeViewItem {
            get {
                return this.selectedTreeViewItem;
            }
            set {
                this.selectedTreeViewItem = value;
                this.OnPropertyChanged("SelectedTreeViewItem");
            }
        }

        private ProductCategoryForProductDataTreeView ProductCategoryForProductDataTreeView {
            get {
                if(this.productCategoryForProductDataTreeView == null) {
                    this.productCategoryForProductDataTreeView = new ProductCategoryForProductDataTreeView();
                    this.productCategoryForProductDataTreeView.DomainContext = new DcsDomainContext();
                    this.productCategoryForProductDataTreeView.EntityQuery = this.productCategoryForProductDataTreeView.DomainContext.GetProductCategoriesQuery().Where(p => p.Status == (int)DcsBaseDataStatus.有效);
                    this.productCategoryForProductDataTreeView.OnTreeViewItemClick -= ProductCategoryDataTreeView_OnTreeViewItemClick;
                    this.productCategoryForProductDataTreeView.OnTreeViewItemClick += ProductCategoryDataTreeView_OnTreeViewItemClick;
                    this.productCategoryForProductDataTreeView.RefreshDataTree();
                }
                return this.productCategoryForProductDataTreeView;
            }
        }

        public DataGridViewBase ProductDataGridView {
            get {
                if(this.productDataGridView == null) {
                    this.productDataGridView = DI.GetDataGridView("Product");
                    this.productDataGridView.SelectionChanged += ProductDataGridView_SelectionChanged;
                }
                return this.productDataGridView;
            }
        }

        void ProductDataGridView_SelectionChanged(object sender, EventArgs e) {
            ProductDataGridViewSelectedEntities = this.ProductDataGridView.SelectedEntities;
        }

        //返回查询界面时ProductDataGridView重新查询
        public void SetProductDataGridViewFilterItem(FilterItem fi) {
            this.ProductDataGridView.FilterItem = fi;
        }

        public FilterItem GetProductDataGridViewFilterItem() {
            return this.ProductDataGridView.FilterItem;
        }

        public void ExecuteQuery() {
            this.ProductDataGridView.ExecuteQueryDelayed();
        }

        private void ProductCategoryDataTreeView_OnTreeViewItemClick(object sender, RadRoutedEventArgs radRoutedEventArgs) {
            var selectedItem = ((RadTreeView)sender).SelectedItem as RadTreeViewItem;
            SelectedTreeViewItem = selectedItem;
            if(selectedItem == null || !(selectedItem.Tag is ProductCategory))
                return;
            if((selectedItem.Tag as ProductCategory).ProductCategoryLevel == GlobalVar.PRODUCTCATEGORY_MAX_LEVEL) {
                ((ProductDataGridView)ProductDataGridView).ProductCategoryId = (selectedItem.Tag as ProductCategory).Id;
                ProductDataGridView.ExecuteQueryDelayed();
                ((ProductDataGridView)ProductDataGridView).ProductCategoryId = 0;
            }
        }

        public ProductCategoryAndProductView() {
            InitializeComponent();
            this.CreateUI();
        }

        private void CreateUI() {
            ProductCategoryForProductDataTreeView.SetValue(Grid.ColumnProperty, 0);
            this.Root.Children.Add(ProductCategoryForProductDataTreeView);
            ProductDataGridView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(ProductDataGridView);
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new NotImplementedException();
        }

        public void RefreshTreeView() {
            this.ProductCategoryForProductDataTreeView.RefreshDataTree();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
