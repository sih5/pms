﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Input;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.TreeView;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.Custom {
    public partial class RegionAndDealerView : IBaseView {

        private ObservableCollection<Dealer> dealers;

        public ObservableCollection<Dealer> Dealers {
            get {
                return this.dealers ?? (this.dealers = new ObservableCollection<Dealer>());
            }
        }

        private RegionDataTreeView regionDataTreeView;

        public RegionDataTreeView RegionDataTreeView {
            get {
                if(this.regionDataTreeView == null) {
                    this.regionDataTreeView = new RegionDataTreeView();
                    this.regionDataTreeView.DomainContext = new DcsDomainContext();
                    this.regionDataTreeView.OnTreeViewItemClick += this.RegionDataTreeView_OnTreeViewItemClick;
                }
                return this.regionDataTreeView;
            }
            set {
                this.regionDataTreeView = value;
            }
        }

        private void RegionDataTreeView_OnTreeViewItemClick(object sender, Telerik.Windows.RadRoutedEventArgs e) {
            this.Dealers.Clear();
            var selectedItem = ((RadTreeView)sender).SelectedItem as RadTreeViewItem;
            if(selectedItem == null || !(selectedItem.Tag is Region) || selectedItem.Items.Count > 0)
                return;
            var regjion = selectedItem.Tag as Region;
            this.RegionDataTreeView.DomainContext.Load(this.RegionDataTreeView.DomainContext.GetDealerByRegionNameQuery(regjion.Name), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                if(loadOp.Entities == null || !loadOp.Entities.Any())
                    return;
                var entities = loadOp.Entities.ToArray();
                foreach(var entity in entities) {
                    this.Dealers.Add(entity);
                }
            }, null);
        }

        private void CreateUI() {
            this.Root.Children.Add(this.RegionDataTreeView);
            RadDragAndDropManager.AddDragInfoHandler(this.DealerForRegionGridView, this.OnGridViewDragInfo);
            RadDragAndDropManager.AddDragQueryHandler(this.DealerForRegionGridView, this.OnGridViewDragQuery);
        }

        private void OnGridViewDragInfo(object sender, DragDropEventArgs e) {
            var draggedItems = e.Options.Payload as IEnumerable;
            if(e.Options.Status == DragStatus.DragInProgress) {
                var cue = new TreeViewDragCue();
                cue.ItemTemplate = this.Resources["DragCueTemplate"] as DataTemplate;
                cue.ItemsSource = draggedItems;
                e.Options.DragCue = cue;
            }
        }

        private void OnGridViewDragQuery(object sender, DragDropQueryEventArgs e) {
            var gridView = sender as RadGridView;
            if(gridView != null) {
                if(!gridView.SelectedItems.Any())
                    return;
                var selectedItems = gridView.SelectedItems.ToList(); ;
                e.QueryResult = selectedItems.Count > 0;
                e.Options.Payload = selectedItems;
            }
            e.QueryResult = true;
            e.Handled = true;
        }

        public RegionAndDealerView() {
            InitializeComponent();
            this.CreateUI();
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new System.NotImplementedException();
        }

        public void RefreshTreeViewAndClearGridView() {
            this.RegionDataTreeView.EntityQuery = this.RegionDataTreeView.DomainContext.GetRegionsQuery().Where(entity => entity.Type == 1);
            this.RegionDataTreeView.RefreshDataTree();
            this.Dealers.Clear();
        }

        private void DealerForRegion_DataLoaded(object sender, System.EventArgs e) {
            this.DealerForRegionGridView.SelectAll();
        }

        private ICommand selectAllCommand;

        private ICommand invertSelectCommand;

        private void InitializeCommand() {
            this.selectAllCommand = new DelegateCommand(() => this.DealerForRegionGridView.SelectAll());
            this.invertSelectCommand = new DelegateCommand(() => {
                var oldSelectItems = this.DealerForRegionGridView.SelectedItems;
                var items = this.DealerForRegionGridView.Items;
                var sourceItem = items.SourceCollection as ObservableCollection<Dealer>;
                if(sourceItem == null)
                    return;
                var newSelectedItems = new ObservableCollection<Dealer>();
                foreach(var dealer in sourceItem) {
                    if(!oldSelectItems.Contains(dealer))
                        newSelectedItems.Add(dealer);
                    else
                        this.DealerForRegionGridView.SelectedItems.Remove(dealer);
                }
                this.DealerForRegionGridView.SelectedItems.Clear();
                foreach(var item in newSelectedItems) {
                    this.DealerForRegionGridView.SelectedItems.Add(item);
                }
            });
        }

        public ICommand SelectAllCommand {
            get {
                if(this.selectAllCommand == null)
                    this.InitializeCommand();
                return this.selectAllCommand;
            }
        }

        public ICommand InvertSelectCommand {
            get {
                if(this.invertSelectCommand == null)
                    this.InitializeCommand();
                return this.invertSelectCommand;
            }
        }
    }
}