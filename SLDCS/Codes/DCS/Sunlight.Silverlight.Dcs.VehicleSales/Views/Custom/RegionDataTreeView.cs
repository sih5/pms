﻿

using System;
using System.Linq;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.Custom {
    public class RegionDataTreeView : DcsDataTreeView {

        protected override void PopulateItemsHandler(System.Collections.Generic.IEnumerable<object> entities, int? id, System.Collections.Generic.ICollection<object> treeItems, Action<RadTreeViewItem> additionalHandler) {
            foreach(var region in entities.Cast<Region>()) {
                var item = new RadTreeViewItem {
                    Header = region.Name,
                    Tag = region
                };
                treeItems.Add(item);
                if(additionalHandler != null)
                    additionalHandler(item);
            }
        }

        protected override int? TreeViewRootItemIdentity() {
            return null;
        }
    }
}
