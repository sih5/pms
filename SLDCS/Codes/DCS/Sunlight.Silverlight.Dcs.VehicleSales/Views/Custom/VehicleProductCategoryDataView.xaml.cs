﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.View;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.Custom {
    public partial class VehicleProductCategoryDataView : IBaseView {
        private ProductCategoryDataTreeView productCategoryDataTreeView;
        private DataGridViewBase vehicleModelAffiProductForEditDataGridView;
        public event GridViewRowDoubleClick OnDataGridRowDoubleClick;
        private object gridViewSelectedItem;
        public object GridViewSelectedItem {
            get {
                return this.gridViewSelectedItem;
            }
        }

        public VehicleProductCategoryDataView() {
            InitializeComponent();
            this.CreateUI();
            this.VehicleModelAffiProductForEditDataGridView.RowDoubleClick += this.DataGridRow_DoubleClick;
        }

        public delegate void GridViewRowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e);
        void DataGridRow_DoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            if(this.OnDataGridRowDoubleClick != null)
                this.OnDataGridRowDoubleClick(sender, e);
        }

        private ProductCategoryDataTreeView ProductCategoryDataTreeView {
            get {
                if(this.productCategoryDataTreeView == null) {
                    this.productCategoryDataTreeView = new ProductCategoryDataTreeView();
                    this.productCategoryDataTreeView.DomainContext = new DcsDomainContext();
                    this.productCategoryDataTreeView.OnTreeViewItemClick += ProductCategoryDataTreeView_OnTreeViewItemClick;
                }
                return this.productCategoryDataTreeView;
            }
        }

        private DataGridViewBase VehicleModelAffiProductForEditDataGridView {
            get {
                if(this.vehicleModelAffiProductForEditDataGridView == null) {
                    this.vehicleModelAffiProductForEditDataGridView = DI.GetDataGridView("VehicleModelAffiProductForEdit");
                    this.vehicleModelAffiProductForEditDataGridView.DataContext = this;
                    this.vehicleModelAffiProductForEditDataGridView.SelectionChanged += vehicleModelAffiProductForEditDataGridView_SelectionChanged;
                }
                return this.vehicleModelAffiProductForEditDataGridView;
            }
        }

        void vehicleModelAffiProductForEditDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            this.gridViewSelectedItem = this.VehicleModelAffiProductForEditDataGridView.SelectedEntities.FirstOrDefault();
        }

        private void ProductCategoryDataTreeView_OnTreeViewItemClick(object sender, RadRoutedEventArgs radRoutedEventArgs) {
            this.VehicleModelAffiProducts.Clear();
            var selectedItem = ((RadTreeView)sender).SelectedItem as RadTreeViewItem;
            if(selectedItem == null || !(selectedItem.Tag is ProductCategory) || selectedItem.Level == 0)
                return;
            ExpandAllRadTreeViewItems(selectedItem);
        }

        private void ExpandAllRadTreeViewItems(RadTreeViewItem radTreeViewItem) {
            var productCategory = (ProductCategory)radTreeViewItem.Tag;
            var productAffiProductCategories = productCategory.ProductAffiProductCategories;
            foreach(ProductAffiProductCategory productAffiProductCategory in productAffiProductCategories) {
                if(productAffiProductCategory.Product == null)
                    continue;
                var wholesalePrice = productAffiProductCategory.Product.VehicleWholesalePrices == null ? new decimal(0) : productAffiProductCategory.Product.VehicleWholesalePrices.First().Price;
                //产品可以采购才可新增
                if(productAffiProductCategory.Product.IfPurchasable) {
                    var vehicleModelAffiProduct = new VehicleModelAffiProduct {
                        ProductCategoryId = productCategory.Id,
                        ProductCategoryCode = productCategory.Code,
                        ProductCategoryName = productCategory.Name,
                        ProductId = productAffiProductCategory.ProductId,
                        ProductCode = productAffiProductCategory.Product.Code,
                        ProductName = productAffiProductCategory.Product.Name,
                        CanBeOrdered = productAffiProductCategory.Product.CanBeOrdered,
                        IfPurchasable = productAffiProductCategory.Product.IfPurchasable,
                        Color = productAffiProductCategory.Product.Color,
                        OrderCycle = productAffiProductCategory.Product.OrderCycle,
                        PurchaseCycle = productAffiProductCategory.Product.PurchaseCycle,
                        ParameterDescription = productAffiProductCategory.Product.ParameterDescription,
                        Version = productAffiProductCategory.Product.Version,
                        Configuration = productAffiProductCategory.Product.Configuration,
                        IfOption = productAffiProductCategory.Product.IfOption,
                        EngineCylinder = productAffiProductCategory.Product.EngineCylinder,
                        EmissionStandard = productAffiProductCategory.Product.EmissionStandard,
                        ManualAutomatic = productAffiProductCategory.Product.ManualAutomatic,
                        ColorCode = productAffiProductCategory.Product.ColorCode,
                        WholesalePrice = wholesalePrice
                    };
                    VehicleModelAffiProducts.Add(vehicleModelAffiProduct);
                }
            }
            foreach(var treeViewItem in radTreeViewItem.Items.Cast<RadTreeViewItem>())
                ExpandAllRadTreeViewItems(treeViewItem);
        }

        private void CreateUI() {
            ProductCategoryDataTreeView.SetValue(Grid.ColumnProperty, 0);
            this.Root.Children.Add(ProductCategoryDataTreeView);
            VehicleModelAffiProductForEditDataGridView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(VehicleModelAffiProductForEditDataGridView);
        }

        private ObservableCollection<VehicleModelAffiProduct> vehicleModelAffiProducts;

        public ObservableCollection<VehicleModelAffiProduct> VehicleModelAffiProducts {
            get {
                return vehicleModelAffiProducts ?? (this.vehicleModelAffiProducts = new ObservableCollection<VehicleModelAffiProduct>());
            }
            set {
                this.vehicleModelAffiProducts = value;
            }
        }

        public object ExchangeData(IBaseView sender, string subject, params object[] contents) {
            throw new System.NotImplementedException();
        }

        public void RefreshTreeViewAndClearGridView(int vehicleSalesOrgId) {
            this.ProductCategoryDataTreeView.EntityQuery = this.productCategoryDataTreeView.DomainContext.根据销售组织查询产品分类Query(vehicleSalesOrgId);
            this.ProductCategoryDataTreeView.RefreshDataTree();
            this.VehicleModelAffiProducts.Clear();
        }
    }
}
