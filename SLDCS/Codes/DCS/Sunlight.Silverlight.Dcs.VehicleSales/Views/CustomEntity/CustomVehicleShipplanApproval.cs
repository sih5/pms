﻿using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.CustomEntity {
    /// <summary>
    /// 继承自Entity，原因是报错信息用红框方式显示，能捕捉到ValidationErrors
    /// 完全没把发车审批单管理2的查询条件完全抽离，便于维护
    /// </summary>
    public class CustomVehicleShipplanApproval : Entity {
        private string code;
        private int vehicleWarehouseId;
        private string dealerName;
        private string remark;
        private int vehicleOrderType;
        private int vehicleFundsType;
        private string vehicleCategoryCode;
        private string vehicleCategoryName;
        private decimal availableAmount;
        private decimal approvepAmount;

        /// <summary>
        /// 审批单编号
        /// </summary>
        public string Code {
            get {
                return this.code;
            }
            set {
                this.code = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Code"));
            }
        }

        /// <summary>
        /// 发车仓库Id
        /// </summary>
        public int VehicleWarehouseId {
            get {
                return this.vehicleWarehouseId;
            }
            set {
                this.vehicleWarehouseId = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("VehicleWarehouseId"));
            }
        }

        /// <summary>
        /// 经销商名称
        /// </summary>
        public string DealerName {
            get {
                return dealerName;
            }
            set {
                this.dealerName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("DealerName"));
            }
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get {
                return this.remark;
            }
            set {
                this.remark = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("Remark"));
            }
        }

        /// <summary>
        /// 订单类型
        /// </summary>
        public int VehicleOrderType {
            get {
                return this.vehicleOrderType;
            }
            set {
                this.vehicleOrderType = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("VehicleOrderType"));
            }
        }

        /// <summary>
        /// 资金类型
        /// </summary>
        public int VehicleFundsType {
            get {
                return this.vehicleFundsType;
            }
            set {
                this.vehicleFundsType = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("VehicleFundsType"));
            }
        }

        /// <summary>
        /// 车型大类
        /// </summary>
        public string VehicleCategoryCode {
            get {
                return this.vehicleCategoryCode;
            }
            set {
                this.vehicleCategoryCode = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("VehicleCategoryCode"));
            }
        }

        /// <summary>
        /// 车型名称
        /// </summary>
        public string VehicleCategoryName {
            get {
                return this.vehicleCategoryName;
            }
            set {
                this.vehicleCategoryName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("VehicleCategoryName"));
            }
        }

        /// <summary>
        /// 可用余额
        /// </summary>
        public decimal AvailableAmount {
            get {
                return this.availableAmount;
            }
            set {
                this.availableAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("AvailableAmount"));
            }
        }

        /// <summary>
        /// 审批金额
        /// </summary>
        public decimal ApprovepAmount {
            get {
                return this.approvepAmount;
            }
            set {
                this.approvepAmount = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs("ApprovepAmount"));
            }
        }
    }
}
