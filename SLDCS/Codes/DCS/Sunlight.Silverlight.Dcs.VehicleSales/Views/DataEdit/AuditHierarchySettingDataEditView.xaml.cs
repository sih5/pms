﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class AuditHierarchySettingDataEditView {
        private DataGridViewBase personnelDataGridView;
        private DataGridViewBase auditRoleAffiPerconnelDataGridView;
        private CompositeFilterItem compositeFilterItem = new CompositeFilterItem();
        public AuditHierarchySettingDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_AuditHierarchySetting;
            }
        }

        private DataGridViewBase PersonnelDataGridView {
            get {
                if(this.personnelDataGridView == null) {
                    this.personnelDataGridView = DI.GetDataGridView("PersonnelForSelect");
                    this.personnelDataGridView.Loaded += this.PersonnelDataGridView_Loaded;
                    this.personnelDataGridView.RowDoubleClick += personnelDataGridView_RowDoubleClick;
                }
                return this.personnelDataGridView;
            }
        }

        private void PersonnelDataGridView_Loaded(object sender, RoutedEventArgs e) {
            //TODO:DataGridView默认过滤查询条件（2个过滤条件以上）必须放在DataGridView_Loaded事件处理
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                IsExact = false,
                Value = (int)DcsMasterDataStatus.有效
            });

            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "CorporationId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                IsExact = false,
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            compositeFilterItem.LogicalOperator = LogicalOperator.And;
            this.personnelDataGridView.FilterItem = compositeFilterItem;
            this.PersonnelDataGridView.ExecuteQueryDelayed();
        }

        private void personnelDataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            var auditHierarchySetting = this.DataContext as AuditHierarchySetting;
            if(auditHierarchySetting == null)
                return;
            if(this.PersonnelDataGridView.SelectedEntities == null || !this.PersonnelDataGridView.SelectedEntities.Any())
                return;
            var person = this.PersonnelDataGridView.SelectedEntities.Cast<Personnel>().First();
            //去除重复
            if(auditHierarchySetting.AuditRoleAffiPerconnels.Any(r => r.PersonnelId == person.Id))
                return;
            this.DomainContext.Load(this.DomainContext.GetPersonnelsQuery().Where(r => r.Id == person.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                var personnel = loadOp.Entities.SingleOrDefault();
                if(personnel == null)
                    return;
                auditHierarchySetting.AuditRoleAffiPerconnels.Add(new AuditRoleAffiPerconnel {
                    Personnel = personnel,
                    PersonnelId = personnel.Id,
                    AuditHierarchySetting = auditHierarchySetting
                });
            }, null);
        }

        private DataGridViewBase AuditRoleAffiPerconnelDataGridView {
            get {
                if(this.auditRoleAffiPerconnelDataGridView == null) {
                    this.auditRoleAffiPerconnelDataGridView = DI.GetDataGridView("AuditRoleAffiPerconnelForEdit");
                    this.auditRoleAffiPerconnelDataGridView.DomainContext = this.DomainContext;
                }
                return this.auditRoleAffiPerconnelDataGridView;
            }
        }

        protected override void OnEditSubmitting() {
            var auditHierarchySetting = this.DataContext as AuditHierarchySetting;
            if(auditHierarchySetting == null || !this.PersonnelDataGridView.CommitEdit())
                return;
            auditHierarchySetting.ValidationErrors.Clear();
            foreach(var auditRoleAffiPerconnel in auditHierarchySetting.AuditRoleAffiPerconnels.Where(r => r.HasValidationErrors))
                auditRoleAffiPerconnel.ValidationErrors.Clear();

            if(auditHierarchySetting.EntityState == EntityState.New) {
                auditHierarchySetting.PostAuditStatus.Add(new PostAuditStatu {
                    AuditHierarchySettingId = auditHierarchySetting.Id,
                    Name = auditHierarchySetting.PositionName + VehicleSalesUIStrings.AppoverIsPass,
                });
                auditHierarchySetting.PostAuditStatus.Add(new PostAuditStatu {
                    AuditHierarchySettingId = auditHierarchySetting.Id,
                    Name = auditHierarchySetting.PositionName + VehicleSalesUIStrings.AppoverIsNotPass,
                });
            }

            if(string.IsNullOrWhiteSpace(auditHierarchySetting.PositionName)) {
                auditHierarchySetting.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_AuditHierarchySetting_PositionNameIsNotNull, new[] {
                    "PositionName"
                }));
            }

            if(auditHierarchySetting.EntityState == EntityState.Modified)
                foreach(var entity in auditHierarchySetting.PostAuditStatus)
                    entity.Name = auditHierarchySetting.PositionName + entity.Name;

            if(auditHierarchySetting.AuditRoleAffiPerconnels == null || auditHierarchySetting.AuditRoleAffiPerconnels.Count == 0) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_AuditHierarchySetting_AuditRoleAffiPerconnelIsNotNull);
                return;
            }
            ((IEditableObject)auditHierarchySetting).EndEdit();
            if(auditHierarchySetting.ValidationErrors.Any())
                return;
            base.OnEditSubmitting();
        }

        protected override void OnEditCancelled() {
            this.SearchTextBoxEmployeeName.SearchText = string.Empty;
            compositeFilterItem.Filters.Clear();
        }

        private void CreateUI() {
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(VehicleSalesUIStrings.DetailDataEditView_HeadText_Personnel, null, () => this.PersonnelDataGridView);
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.UnregisterButton(detailEditView.DeleteButton);

            detailEditView.SetValue(Grid.RowProperty, 1);
            this.GridPerson.Children.Add(detailEditView);

            detailEditView = new DcsDetailDataEditView();//重新实例化
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(AuditHierarchySetting), "AuditRoleAffiPerconnels"), null, () => this.AuditRoleAffiPerconnelDataGridView);
            detailEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailEditView);
        }

        private void TxtPsersonName_SearchTextChanged(object sender, System.EventArgs e) {
            var filterText = this.SearchTextBoxEmployeeName.SearchText;
            foreach(var item in compositeFilterItem.Filters.Where(v => v.MemberName == "Name").ToList()) {
                compositeFilterItem.Filters.Remove(item);
            }
            if(!string.IsNullOrEmpty(filterText) && this.compositeFilterItem.Filters.All(v => v.MemberName != "Name")) {
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "Name",
                    MemberType = typeof(string),
                    Operator = FilterOperator.IsEqualTo,
                    IsExact = false,
                    Value = filterText
                });
                compositeFilterItem.LogicalOperator = LogicalOperator.And;
            }
            this.personnelDataGridView.FilterItem = compositeFilterItem;
            this.PersonnelDataGridView.ExecuteQueryDelayed();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetAuditHierarchySettingsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    foreach(var postAuditStatu in entity.PostAuditStatus)
                        postAuditStatu.Name = postAuditStatu.Name.Substring(entity.PositionName.Count());
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
