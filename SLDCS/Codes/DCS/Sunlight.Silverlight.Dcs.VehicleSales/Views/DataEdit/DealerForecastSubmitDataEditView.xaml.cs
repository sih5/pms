﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class DealerForecastSubmitDataEditView : INotifyPropertyChanged {
        private FrameworkElement dataEditPanel;
        private ObservableCollection<DealerForecastDetail> dealerForecastDetails;
        private ObservableCollection<KeyValuePair> kvYears;
        private ObservableCollection<KeyValuePair> kvMonthes;
        private int year;
        private int month;
        private bool isIniting;

        private FrameworkElement DataEditPanel {
            get {
                if(this.dataEditPanel == null) {
                    this.dataEditPanel = DI.GetDataEditPanel("DealerForecastSubmit");
                    this.dataEditPanel.SetValue(DataContextProperty, this);
                }
                return this.dataEditPanel;
            }
        }

        public ObservableCollection<DealerForecastDetail> DealerForecastDetails {
            get {
                return this.dealerForecastDetails ?? (this.dealerForecastDetails = new ObservableCollection<DealerForecastDetail>());
            }
        }

        public ObservableCollection<KeyValuePair> KvYears {
            get {
                return this.kvYears ?? (this.kvYears = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvMonthes {
            get {
                return this.kvMonthes ?? (this.kvMonthes = new ObservableCollection<KeyValuePair>());
            }
        }

        public int Year {
            get {
                return this.year;
            }
            set {
                this.year = value;
                this.OnPropertyChanged("Year");
            }
        }

        public int Month {
            get {
                return this.month;
            }
            set {
                this.month = value;
                this.OnPropertyChanged("Month");
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DealerForecast;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(this.CreateHorizontalLine(0, 1));
            this.DataEditPanel.SetValue(Grid.RowProperty, 2);
            this.Root.Children.Add(this.DataEditPanel);
            this.DataContextChanged += DealerForecastSubmitDataEditView_DataContextChanged;
            this.PropertyChanged += DealerForecastSubmitDataEditView_PropertyChanged;
            for(var i = 1; i <= 12; i++) {
                this.KvMonthes.Add(new KeyValuePair {
                    Key = i,
                    Value = i.ToString(CultureInfo.InvariantCulture)
                });
            }
        }

        private void DealerForecastSubmitDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealerForecast = this.DataContext as DealerForecast;
            if(dealerForecast == null)
                return;
            this.Year = 0;
            this.Month = 0;
            if(dealerForecast.Id == 0)
                return;
            this.Year = dealerForecast.ForecastDate.Year;
            this.Month = dealerForecast.ForecastDate.Month;
        }

        private void DealerForecastSubmitDataEditView_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(this.isIniting)
                return;
            switch(e.PropertyName) {
                case "ResaleM2TwoRoom1":
                case "ResaleM2TwoRoom2":
                    if(e.PropertyName.Equals("ResaleM2TwoRoom1"))
                        this.SetRetailForecastDetail(this.ResaleM2TwoRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L3);
                    else
                        this.SetRetailForecastDetail(this.ResaleM2TwoRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L5);
                    this.ResaleM2TwoRoomSub = this.ResaleM2TwoRoom1 + this.ResaleM2TwoRoom2;
                    break;
                case "ResaleM2ThreeRoom1":
                case "ResaleM2ThreeRoom2":
                    if(e.PropertyName.Equals("ResaleM2ThreeRoom1"))
                        this.SetRetailForecastDetail(this.ResaleM2ThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.劲翔三厢, (int)DcsDealerForecastDetailEngineCylinder._1L3);
                    else
                        this.SetRetailForecastDetail(this.ResaleM2ThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.劲翔三厢, (int)DcsDealerForecastDetailEngineCylinder._1L5);
                    this.ResaleM2ThreeRoomSub = this.ResaleM2ThreeRoom1 + this.ResaleM2ThreeRoom2;
                    break;
                case "ResaleM2TwoRoomSub":
                case "ResaleM2ThreeRoomSub":
                    this.ResaleM2Sub = this.ResaleM2TwoRoomSub + this.ResaleM2ThreeRoomSub;
                    break;
                case "ResaleM32012ThreeRoom1":
                case "ResaleM32012ThreeRoom2":
                    if(e.PropertyName.Equals("ResaleM32012ThreeRoom1"))
                        this.SetRetailForecastDetail(this.ResaleM32012ThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3经典款2012年型, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
                    else
                        this.SetRetailForecastDetail(this.ResaleM32012ThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3经典款2012年型, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
                    this.ResaleM32012ThreeRoomSub = this.ResaleM32012ThreeRoom1 + this.ResaleM32012ThreeRoom2;
                    break;
                case "ResaleM3SDNThreeRoom1":
                case "ResaleM3SDNThreeRoom2":
                    if(e.PropertyName.Equals("ResaleM3SDNThreeRoom1"))
                        this.SetRetailForecastDetail(this.ResaleM3SDNThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋SDN, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
                    else
                        this.SetRetailForecastDetail(this.ResaleM3SDNThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋SDN, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
                    this.ResaleM3SDNThreeRoomSub = this.ResaleM3SDNThreeRoom1 + this.ResaleM3SDNThreeRoom2;
                    break;
                case "ResaleM35HBThreeRoom1":
                case "ResaleM35HBThreeRoom2":
                    if(e.PropertyName.Equals("ResaleM35HBThreeRoom1"))
                        this.SetRetailForecastDetail(this.ResaleM35HBThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋5HB, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
                    else
                        this.SetRetailForecastDetail(this.ResaleM35HBThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋5HB, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
                    this.ResaleM35HBThreeRoomSub = this.ResaleM35HBThreeRoom1 + this.ResaleM35HBThreeRoom2;
                    break;
                case "ResaleM3BUTwoRoom1":
                case "ResaleM3BUTwoRoom2":
                    if(e.PropertyName.Equals("ResaleM3BUTwoRoom1"))
                        this.SetRetailForecastDetail(this.ResaleM3BUTwoRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3_BU, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
                    else
                        this.SetRetailForecastDetail(this.ResaleM3BUTwoRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3_BU, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
                    this.ResaleM3BUTwoRoomSub = this.ResaleM3BUTwoRoom1 + this.ResaleM3BUTwoRoom2;
                    break;
                case "ResaleM32012ThreeRoomSub":
                case "ResaleM3SDNThreeRoomSub":
                case "ResaleM35HBThreeRoomSub":
                case "ResaleM3BUTwoRoomSub":
                    this.ResaleM3Sub = this.ResaleM32012ThreeRoomSub + this.ResaleM3SDNThreeRoomSub + this.ResaleM35HBThreeRoomSub + this.ResaleM3BUTwoRoomSub;
                    break;
                case "ResaleM2Sub":
                case "ResaleM3Sub":
                    this.ResaleMazdaSub = this.ResaleM2Sub + this.ResaleM3Sub;
                    break;
                case "ResaleMazdaSub":
                    this.ResaleDomesticMazdaSub = this.ResaleMazdaSub - this.ResaleM3BUTwoRoomSub;
                    break;
                case "WholesaleM2TwoRoom1":
                case "WholesaleM2TwoRoom2":
                    if(e.PropertyName.Equals("WholesaleM2TwoRoom1"))
                        this.SetWholesaleForecastDetail(this.WholesaleM2TwoRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L3);
                    else
                        this.SetWholesaleForecastDetail(this.WholesaleM2TwoRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L5);
                    this.WholesaleM2TwoRoomSub = this.WholesaleM2TwoRoom1 + this.WholesaleM2TwoRoom2;
                    break;
                case "WholesaleM2ThreeRoom1":
                case "WholesaleM2ThreeRoom2":
                    if(e.PropertyName.Equals("WholesaleM2ThreeRoom1"))
                        this.SetWholesaleForecastDetail(this.WholesaleM2ThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.劲翔三厢, (int)DcsDealerForecastDetailEngineCylinder._1L3);
                    else
                        this.SetWholesaleForecastDetail(this.WholesaleM2ThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.劲翔三厢, (int)DcsDealerForecastDetailEngineCylinder._1L5);
                    this.WholesaleM2ThreeRoomSub = this.WholesaleM2ThreeRoom1 + this.WholesaleM2ThreeRoom2;
                    break;
                case "WholesaleM2TwoRoomSub":
                case "WholesaleM2ThreeRoomSub":
                    this.WholesaleM2Sub = this.WholesaleM2TwoRoomSub + this.WholesaleM2ThreeRoomSub;
                    break;
                case "WholesaleM32012ThreeRoom1":
                case "WholesaleM32012ThreeRoom2":
                    if(e.PropertyName.Equals("WholesaleM32012ThreeRoom1"))
                        this.SetWholesaleForecastDetail(this.WholesaleM32012ThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3经典款2012年型, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
                    else
                        this.SetWholesaleForecastDetail(this.WholesaleM32012ThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3经典款2012年型, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
                    this.WholesaleM32012ThreeRoomSub = this.WholesaleM32012ThreeRoom1 + this.WholesaleM32012ThreeRoom2;
                    break;
                case "WholesaleM3SDNThreeRoom1":
                case "WholesaleM3SDNThreeRoom2":
                    if(e.PropertyName.Equals("WholesaleM3SDNThreeRoom1"))
                        this.SetWholesaleForecastDetail(this.WholesaleM3SDNThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋SDN, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
                    else
                        this.SetWholesaleForecastDetail(this.WholesaleM3SDNThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋SDN, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
                    this.WholesaleM3SDNThreeRoomSub = this.WholesaleM3SDNThreeRoom1 + this.WholesaleM3SDNThreeRoom2;
                    break;
                case "WholesaleM35HBThreeRoom1":
                case "WholesaleM35HBThreeRoom2":
                    if(e.PropertyName.Equals("WholesaleM35HBThreeRoom1"))
                        this.SetWholesaleForecastDetail(this.WholesaleM35HBThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋5HB, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
                    else
                        this.SetWholesaleForecastDetail(this.WholesaleM35HBThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋5HB, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
                    this.WholesaleM35HBThreeRoomSub = this.WholesaleM35HBThreeRoom1 + this.WholesaleM35HBThreeRoom2;
                    break;
                case "WholesaleM3BUTwoRoom1":
                case "WholesaleM3BUTwoRoom2":
                    if(e.PropertyName.Equals("WholesaleM3BUTwoRoom1"))
                        this.SetWholesaleForecastDetail(this.WholesaleM3BUTwoRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3_BU, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
                    else
                        this.SetWholesaleForecastDetail(this.WholesaleM3BUTwoRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3_BU, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
                    this.WholesaleM3BUTwoRoomSub = this.WholesaleM3BUTwoRoom1 + this.WholesaleM3BUTwoRoom2;
                    break;
                case "WholesaleM32012ThreeRoomSub":
                case "WholesaleM3SDNThreeRoomSub":
                case "WholesaleM35HBThreeRoomSub":
                case "WholesaleM3BUTwoRoomSub":
                    this.WholesaleM3Sub = this.WholesaleM32012ThreeRoomSub + this.WholesaleM3SDNThreeRoomSub + this.WholesaleM35HBThreeRoomSub + this.WholesaleM3BUTwoRoomSub;
                    break;
                case "WholesaleM2Sub":
                case "WholesaleM3Sub":
                    this.WholesaleMazdaSub = this.WholesaleM2Sub + this.WholesaleM3Sub;
                    break;
                case "WholesaleMazdaSub":
                    this.WholesaleDomesticMazdaSub = this.WholesaleMazdaSub - this.WholesaleM3BUTwoRoomSub;
                    break;
                case "Year":
                case "Month":
                    this.SetForecastDate();
                    break;
            }
        }

        private void DealerForecastSubmitDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.SetForecastDetails();
            this.KvYears.Clear();
            var intYear = DateTime.Now.Year;
            for(var i = 0; i < 10; i++) {
                this.KvYears.Add(new KeyValuePair {
                    Key = intYear + i,
                    Value = (intYear + i).ToString(CultureInfo.InvariantCulture)
                });
            }
        }

        private void SetForecastDate() {
            if(this.Year == 0 || this.Month == 0)
                return;
            var dealerForecast = this.DataContext as DealerForecast;
            if(dealerForecast == null)
                return;
            dealerForecast.ForecastDate = new DateTime(this.Year, this.Month, 1);
        }

        private void SetForecastDetails() {
            this.SetRetailForecastDetail(this.ResaleM2TwoRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L3);
            this.SetRetailForecastDetail(this.ResaleM2TwoRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L5);
            this.SetRetailForecastDetail(this.ResaleM2ThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.劲翔三厢, (int)DcsDealerForecastDetailEngineCylinder._1L3);
            this.SetRetailForecastDetail(this.ResaleM2ThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.劲翔三厢, (int)DcsDealerForecastDetailEngineCylinder._1L5);
            this.SetRetailForecastDetail(this.ResaleM32012ThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3经典款2012年型, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
            this.SetRetailForecastDetail(this.ResaleM32012ThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3经典款2012年型, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
            this.SetRetailForecastDetail(this.ResaleM3SDNThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋SDN, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
            this.SetRetailForecastDetail(this.ResaleM3SDNThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋SDN, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
            this.SetRetailForecastDetail(this.ResaleM35HBThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋5HB, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
            this.SetRetailForecastDetail(this.ResaleM35HBThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋5HB, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
            this.SetRetailForecastDetail(this.ResaleM3BUTwoRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3_BU, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
            this.SetRetailForecastDetail(this.ResaleM3BUTwoRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3_BU, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
            this.SetWholesaleForecastDetail(this.WholesaleM2TwoRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L3);
            this.SetWholesaleForecastDetail(this.WholesaleM2TwoRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L5);
            this.SetWholesaleForecastDetail(this.WholesaleM2ThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.劲翔三厢, (int)DcsDealerForecastDetailEngineCylinder._1L3);
            this.SetWholesaleForecastDetail(this.WholesaleM2ThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda2, (int)DcsDealerForecastDetailWagonType.劲翔三厢, (int)DcsDealerForecastDetailEngineCylinder._1L5);
            this.SetWholesaleForecastDetail(this.WholesaleM32012ThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3经典款2012年型, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
            this.SetWholesaleForecastDetail(this.WholesaleM32012ThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3经典款2012年型, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
            this.SetWholesaleForecastDetail(this.WholesaleM3SDNThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋SDN, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
            this.SetWholesaleForecastDetail(this.WholesaleM3SDNThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋SDN, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
            this.SetWholesaleForecastDetail(this.WholesaleM35HBThreeRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋5HB, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
            this.SetWholesaleForecastDetail(this.WholesaleM35HBThreeRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋5HB, (int)DcsDealerForecastDetailWagonType.三厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
            this.SetWholesaleForecastDetail(this.WholesaleM3BUTwoRoom1, (int)DcsDealerForecastDetailVehicleModel.Mazda3_BU, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._1L6);
            this.SetWholesaleForecastDetail(this.WholesaleM3BUTwoRoom2, (int)DcsDealerForecastDetailVehicleModel.Mazda3_BU, (int)DcsDealerForecastDetailWagonType.两厢, (int)DcsDealerForecastDetailEngineCylinder._2L0);
        }

        private void SetRetailForecastDetail(int retailForecastAmount, int vehicleModel, int wagonType, int engineCylinder) {
            var dealerForecast = this.DataContext as DealerForecast;
            if(dealerForecast == null)
                return;
            var detail = this.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == vehicleModel && r.WagonType == wagonType && r.EngineCylinder == engineCylinder);
            if(detail == null)
                this.DealerForecastDetails.Add(new DealerForecastDetail {
                    DealerForecastId = dealerForecast.Id,
                    RetailForecastAmount = retailForecastAmount,
                    VehicleModel = vehicleModel,
                    WagonType = wagonType,
                    EngineCylinder = engineCylinder
                });
            else
                detail.RetailForecastAmount = retailForecastAmount;
        }

        private void SetWholesaleForecastDetail(int wholesaleForecastAmount, int vehicleModel, int wagonType, int engineCylinder) {
            var dealerForecast = this.DataContext as DealerForecast;
            if(dealerForecast == null)
                return;
            var detail = this.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == vehicleModel && r.WagonType == wagonType && r.EngineCylinder == engineCylinder);
            if(detail == null)
                this.DealerForecastDetails.Add(new DealerForecastDetail {
                    DealerForecastId = dealerForecast.Id,
                    WholesaleForecastAmount = wholesaleForecastAmount,
                    VehicleModel = vehicleModel,
                    WagonType = wagonType,
                    EngineCylinder = engineCylinder
                });
            else
                detail.WholesaleForecastAmount = wholesaleForecastAmount;
        }

        private void SetVehicleModelAmount(DealerForecast dealerForecast) {
            //Mazda2两厢1.3L
            var m2TwoRoom1Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda2 && r.WagonType == (int)DcsDealerForecastDetailWagonType.两厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._1L3);
            if(m2TwoRoom1Detail != null) {
                this.ResaleM2TwoRoom1 = m2TwoRoom1Detail.RetailForecastAmount;
                this.WholesaleM2TwoRoom1 = m2TwoRoom1Detail.WholesaleForecastAmount;
            }
            //Mazda2两厢1.5L
            var m2TwoRoom2Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda2 && r.WagonType == (int)DcsDealerForecastDetailWagonType.两厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._1L5);
            if(m2TwoRoom2Detail != null) {
                this.ResaleM2TwoRoom2 = m2TwoRoom2Detail.RetailForecastAmount;
                this.WholesaleM2TwoRoom2 = m2TwoRoom2Detail.WholesaleForecastAmount;
            }
            //Mazda2劲翔三厢1.3L
            var m2ThreeRoom1Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda2 && r.WagonType == (int)DcsDealerForecastDetailWagonType.劲翔三厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._1L3);
            if(m2ThreeRoom1Detail != null) {
                this.ResaleM2ThreeRoom1 = m2ThreeRoom1Detail.RetailForecastAmount;
                this.WholesaleM2ThreeRoom1 = m2ThreeRoom1Detail.WholesaleForecastAmount;
            }
            //Mazda2劲翔三厢1.5L
            var m2ThreeRoom2Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda2 && r.WagonType == (int)DcsDealerForecastDetailWagonType.劲翔三厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._1L5);
            if(m2ThreeRoom2Detail != null) {
                this.ResaleM2ThreeRoom2 = m2ThreeRoom2Detail.RetailForecastAmount;
                this.WholesaleM2ThreeRoom2 = m2ThreeRoom2Detail.WholesaleForecastAmount;
            }
            //Mazda3经典款2012型三厢1.6L
            var m32012ThreeRoom1Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda3经典款2012年型 && r.WagonType == (int)DcsDealerForecastDetailWagonType.三厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._1L6);
            if(m32012ThreeRoom1Detail != null) {
                this.ResaleM32012ThreeRoom1 = m32012ThreeRoom1Detail.RetailForecastAmount;
                this.WholesaleM32012ThreeRoom1 = m32012ThreeRoom1Detail.WholesaleForecastAmount;
            }
            //Mazda3经典款2012型三厢2.0L
            var m32012ThreeRoom2Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda3经典款2012年型 && r.WagonType == (int)DcsDealerForecastDetailWagonType.三厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._2L0);
            if(m32012ThreeRoom2Detail != null) {
                this.ResaleM32012ThreeRoom2 = m32012ThreeRoom2Detail.RetailForecastAmount;
                this.WholesaleM32012ThreeRoom2 = m32012ThreeRoom2Detail.WholesaleForecastAmount;
            }
            //Mazda3星骋SDN三厢1.6L
            var m3SDNThreeRoom1Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋SDN && r.WagonType == (int)DcsDealerForecastDetailWagonType.三厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._1L6);
            if(m3SDNThreeRoom1Detail != null) {
                this.ResaleM3SDNThreeRoom1 = m3SDNThreeRoom1Detail.RetailForecastAmount;
                this.WholesaleM3SDNThreeRoom1 = m3SDNThreeRoom1Detail.WholesaleForecastAmount;
            }
            //Mazda3星骋SDN三厢2.0L
            var m3SDNThreeRoom2Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋SDN && r.WagonType == (int)DcsDealerForecastDetailWagonType.三厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._2L0);
            if(m3SDNThreeRoom2Detail != null) {
                this.ResaleM3SDNThreeRoom2 = m3SDNThreeRoom2Detail.RetailForecastAmount;
                this.WholesaleM3SDNThreeRoom2 = m3SDNThreeRoom2Detail.WholesaleForecastAmount;
            }
            //Mazda3星骋5HB三厢1.6L
            var m35HBThreeRoom1Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋5HB && r.WagonType == (int)DcsDealerForecastDetailWagonType.三厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._1L6);
            if(m35HBThreeRoom1Detail != null) {
                this.ResaleM35HBThreeRoom1 = m35HBThreeRoom1Detail.RetailForecastAmount;
                this.WholesaleM35HBThreeRoom1 = m35HBThreeRoom1Detail.WholesaleForecastAmount;
            }
            //Mazda3星骋5HB三厢2.0L
            var m35HBThreeRoom2Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda3星骋5HB && r.WagonType == (int)DcsDealerForecastDetailWagonType.三厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._2L0);
            if(m35HBThreeRoom2Detail != null) {
                this.ResaleM35HBThreeRoom2 = m35HBThreeRoom2Detail.RetailForecastAmount;
                this.WholesaleM35HBThreeRoom2 = m35HBThreeRoom2Detail.WholesaleForecastAmount;
            }
            //Mazda3 BU两厢1.6L
            var m3BUTwoRoom1Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda3_BU && r.WagonType == (int)DcsDealerForecastDetailWagonType.两厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._1L6);
            if(m3BUTwoRoom1Detail != null) {
                this.ResaleM3BUTwoRoom1 = m3BUTwoRoom1Detail.RetailForecastAmount;
                this.WholesaleM3BUTwoRoom1 = m3BUTwoRoom1Detail.WholesaleForecastAmount;
            }
            //Mazda3 BU两厢2.0L
            var m3BUTwoRoom2Detail = dealerForecast.DealerForecastDetails.FirstOrDefault(r => r.VehicleModel == (int)DcsDealerForecastDetailVehicleModel.Mazda3_BU && r.WagonType == (int)DcsDealerForecastDetailWagonType.两厢 && r.EngineCylinder == (int)DcsDealerForecastDetailEngineCylinder._2L0);
            if(m3BUTwoRoom2Detail != null) {
                this.ResaleM3BUTwoRoom2 = m3BUTwoRoom2Detail.RetailForecastAmount;
                this.WholesaleM3BUTwoRoom2 = m3BUTwoRoom2Detail.WholesaleForecastAmount;
            }
        }

        private void InitView() {
            this.isIniting = true;
            this.ResaleM2TwoRoom1 = 0;
            this.ResaleM2TwoRoom2 = 0;
            this.ResaleM2TwoRoomSub = 0;
            this.ResaleM2ThreeRoom1 = 0;
            this.ResaleM2ThreeRoom2 = 0;
            this.ResaleM2ThreeRoomSub = 0;
            this.ResaleM2Sub = 0;
            this.ResaleM32012ThreeRoom1 = 0;
            this.ResaleM32012ThreeRoom2 = 0;
            this.ResaleM32012ThreeRoomSub = 0;
            this.ResaleM3SDNThreeRoom1 = 0;
            this.ResaleM3SDNThreeRoom2 = 0;
            this.ResaleM3SDNThreeRoomSub = 0;
            this.ResaleM35HBThreeRoom1 = 0;
            this.ResaleM35HBThreeRoom2 = 0;
            this.ResaleM35HBThreeRoomSub = 0;
            this.ResaleM3BUTwoRoom1 = 0;
            this.ResaleM3BUTwoRoom2 = 0;
            this.ResaleM3BUTwoRoomSub = 0;
            this.ResaleM3Sub = 0;
            this.ResaleDomesticMazdaSub = 0;
            this.ResaleMazdaSub = 0;
            this.WholesaleM2TwoRoom1 = 0;
            this.WholesaleM2TwoRoom2 = 0;
            this.WholesaleM2TwoRoomSub = 0;
            this.WholesaleM2ThreeRoom1 = 0;
            this.WholesaleM2ThreeRoom2 = 0;
            this.WholesaleM2ThreeRoomSub = 0;
            this.WholesaleM2Sub = 0;
            this.WholesaleM32012ThreeRoom1 = 0;
            this.WholesaleM32012ThreeRoom2 = 0;
            this.WholesaleM32012ThreeRoomSub = 0;
            this.WholesaleM3SDNThreeRoom1 = 0;
            this.WholesaleM3SDNThreeRoom2 = 0;
            this.WholesaleM3SDNThreeRoomSub = 0;
            this.WholesaleM35HBThreeRoom1 = 0;
            this.WholesaleM35HBThreeRoom2 = 0;
            this.WholesaleM35HBThreeRoomSub = 0;
            this.WholesaleM3BUTwoRoom1 = 0;
            this.WholesaleM3BUTwoRoom2 = 0;
            this.WholesaleM3BUTwoRoomSub = 0;
            this.WholesaleM3Sub = 0;
            this.WholesaleDomesticMazdaSub = 0;
            this.WholesaleMazdaSub = 0;
            this.DealerForecastDetails.Clear();
            this.isIniting = false;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealerForecastWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
                this.SetVehicleModelAmount(entity);
            }, null);
        }

        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var dealerForecast = this.DataContext as DealerForecast;
            if(dealerForecast == null)
                return;
            dealerForecast.ValidationErrors.Clear();
            if(this.Year == 0) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_DealerForecast_ForecastYearIsNull);
                return;
            }
            if(this.Month == 0) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_DealerForecast_ForecastMonthIsNull);
                return;
            }
            // 获取当前年、月 有多少天
            var day = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            // 当前月的总天数减五天
            day = day - 5;
            if(DateTime.Now.Day > day) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_DealerForecast_DateTimeMonthLessThanFive);
                return;
            }
            if((dealerForecast.ForecastDate.Year - DateTime.Now.Year) * 12 + dealerForecast.ForecastDate.Month < DateTime.Now.Month) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_DealerForecast_DateTimeNow);
                return;
            }
            if(dealerForecast.HasValidationErrors)
                return;
            if(dealerForecast.Id != default(int)) {
                var details = dealerForecast.DealerForecastDetails.ToArray();
                foreach(var dealerForecastDetail in details) {
                    dealerForecast.DealerForecastDetails.Remove(dealerForecastDetail);
                }
            }
            foreach(var detail in this.DealerForecastDetails) {
                ((IEditableObject)detail).EndEdit();
                dealerForecast.DealerForecastDetails.Add(detail);
            }
            ((IEditableObject)dealerForecast).EndEdit();
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            this.InitView();
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            this.InitView();
            base.OnEditCancelled();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public DealerForecastSubmitDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += DealerForecastSubmitDataEditView_Loaded;
        }

        #region 零售预测每个车型的数量定义为属性

        private int resaleM2TwoRoom1;//Mazda2两厢1.3L
        private int resaleM2TwoRoom2;//Mazda2两厢1.5L
        private int resaleM2TwoRoomSub;//Mazda2两厢小计
        private int resaleM2ThreeRoom1;//Mazda2三厢1.3L
        private int resaleM2ThreeRoom2;//Mazda2三厢1.5L
        private int resaleM2ThreeRoomSub;//Mazda2三厢小计
        private int resaleM2Sub;//Mazda2小计
        private int resaleM32012ThreeRoom1;//Mazda3经典款2012型三厢1.6L
        private int resaleM32012ThreeRoom2;//Mazda3经典款2012型三厢2.0L
        private int resaleM32012ThreeRoomSub;//Mazda3经典款2012型三厢小计
        private int resaleM3SDNThreeRoom1;//Mazda3星骋SDN三厢1.6L
        private int resaleM3SDNThreeRoom2;//Mazda3星骋SDN三厢2.0L
        private int resaleM3SDNThreeRoomSub;//Mazda3星骋SDN三厢小计
        private int resaleM35HBThreeRoom1;//Mazda3星骋5HB三厢1.6L
        private int resaleM35HBThreeRoom2;//Mazda3星骋5HB三厢2.0L
        private int resaleM35HBThreeRoomSub;//Mazda3星骋5HB三厢小计
        private int resaleM3BUTwoRoom1;//Mazda3 BU两厢1.6L
        private int resaleM3BUTwoRoom2;//Mazda3 BU两厢2.0L
        private int resaleM3BUTwoRoomSub;//Mazda3 BU两厢小计
        private int resaleM3Sub;//Mazda3小计
        private int resaleDomesticMazdaSub;//Mazda国产车零售预测总计
        private int resaleMazdaSub;//Mazda零售预测总计

        public int ResaleM2TwoRoom1 {
            get {
                return this.resaleM2TwoRoom1;
            }
            set {
                this.resaleM2TwoRoom1 = value;
                this.OnPropertyChanged("ResaleM2TwoRoom1");
            }
        }

        public int ResaleM2TwoRoom2 {
            get {
                return this.resaleM2TwoRoom2;
            }
            set {
                this.resaleM2TwoRoom2 = value;
                this.OnPropertyChanged("ResaleM2TwoRoom2");
            }
        }

        public int ResaleM2TwoRoomSub {
            get {
                return this.resaleM2TwoRoomSub;
            }
            set {
                this.resaleM2TwoRoomSub = value;
                this.OnPropertyChanged("ResaleM2TwoRoomSub");
            }
        }

        public int ResaleM2ThreeRoom1 {
            get {
                return this.resaleM2ThreeRoom1;
            }
            set {
                this.resaleM2ThreeRoom1 = value;
                this.OnPropertyChanged("ResaleM2ThreeRoom1");
            }
        }

        public int ResaleM2ThreeRoom2 {
            get {
                return this.resaleM2ThreeRoom2;
            }
            set {
                this.resaleM2ThreeRoom2 = value;
                this.OnPropertyChanged("ResaleM2ThreeRoom2");
            }
        }

        public int ResaleM2ThreeRoomSub {
            get {
                return this.resaleM2ThreeRoomSub;
            }
            set {
                this.resaleM2ThreeRoomSub = value;
                this.OnPropertyChanged("ResaleM2ThreeRoomSub");
            }
        }

        public int ResaleM2Sub {
            get {
                return this.resaleM2Sub;
            }
            set {
                this.resaleM2Sub = value;
                this.OnPropertyChanged("ResaleM2Sub");
            }
        }

        public int ResaleM32012ThreeRoom1 {
            get {
                return this.resaleM32012ThreeRoom1;
            }
            set {
                this.resaleM32012ThreeRoom1 = value;
                this.OnPropertyChanged("ResaleM32012ThreeRoom1");
            }
        }

        public int ResaleM32012ThreeRoom2 {
            get {
                return this.resaleM32012ThreeRoom2;
            }
            set {
                this.resaleM32012ThreeRoom2 = value;
                this.OnPropertyChanged("ResaleM32012ThreeRoom2");
            }
        }

        public int ResaleM32012ThreeRoomSub {
            get {
                return this.resaleM32012ThreeRoomSub;
            }
            set {
                this.resaleM32012ThreeRoomSub = value;
                this.OnPropertyChanged("ResaleM32012ThreeRoomSub");
            }
        }

        public int ResaleM3SDNThreeRoom1 {
            get {
                return this.resaleM3SDNThreeRoom1;
            }
            set {
                this.resaleM3SDNThreeRoom1 = value;
                this.OnPropertyChanged("ResaleM3SDNThreeRoom1");
            }
        }

        public int ResaleM3SDNThreeRoom2 {
            get {
                return this.resaleM3SDNThreeRoom2;
            }
            set {
                this.resaleM3SDNThreeRoom2 = value;
                this.OnPropertyChanged("ResaleM3SDNThreeRoom2");
            }
        }

        public int ResaleM3SDNThreeRoomSub {
            get {
                return this.resaleM3SDNThreeRoomSub;
            }
            set {
                this.resaleM3SDNThreeRoomSub = value;
                this.OnPropertyChanged("ResaleM3SDNThreeRoomSub");
            }
        }

        public int ResaleM35HBThreeRoom1 {
            get {
                return this.resaleM35HBThreeRoom1;
            }
            set {
                this.resaleM35HBThreeRoom1 = value;
                this.OnPropertyChanged("ResaleM35HBThreeRoom1");
            }
        }

        public int ResaleM35HBThreeRoom2 {
            get {
                return this.resaleM35HBThreeRoom2;
            }
            set {
                this.resaleM35HBThreeRoom2 = value;
                this.OnPropertyChanged("ResaleM35HBThreeRoom2");
            }
        }

        public int ResaleM35HBThreeRoomSub {
            get {
                return this.resaleM35HBThreeRoomSub;
            }
            set {
                this.resaleM35HBThreeRoomSub = value;
                this.OnPropertyChanged("ResaleM35HBThreeRoomSub");
            }
        }

        public int ResaleM3BUTwoRoom1 {
            get {
                return this.resaleM3BUTwoRoom1;
            }
            set {
                this.resaleM3BUTwoRoom1 = value;
                this.OnPropertyChanged("ResaleM3BUTwoRoom1");
            }
        }

        public int ResaleM3BUTwoRoom2 {
            get {
                return this.resaleM3BUTwoRoom2;
            }
            set {
                this.resaleM3BUTwoRoom2 = value;
                this.OnPropertyChanged("ResaleM3BUTwoRoom2");
            }
        }

        public int ResaleM3BUTwoRoomSub {
            get {
                return this.resaleM3BUTwoRoomSub;
            }
            set {
                this.resaleM3BUTwoRoomSub = value;
                this.OnPropertyChanged("ResaleM3BUTwoRoomSub");
            }
        }

        public int ResaleM3Sub {
            get {
                return this.resaleM3Sub;
            }
            set {
                this.resaleM3Sub = value;
                this.OnPropertyChanged("ResaleM3Sub");
            }
        }

        public int ResaleDomesticMazdaSub {
            get {
                return this.resaleDomesticMazdaSub;
            }
            set {
                this.resaleDomesticMazdaSub = value;
                this.OnPropertyChanged("ResaleDomesticMazdaSub");
            }
        }

        public int ResaleMazdaSub {
            get {
                return this.resaleMazdaSub;
            }
            set {
                this.resaleMazdaSub = value;
                this.OnPropertyChanged("ResaleMazdaSub");
            }
        }

        #endregion

        #region 批发预测每个车型的数量定义为属性

        private int wholesaleM2TwoRoom1;//Mazda2两厢1.3L
        private int wholesaleM2TwoRoom2;//Mazda2两厢1.5L
        private int wholesaleM2TwoRoomSub;//Mazda2两厢小计
        private int wholesaleM2ThreeRoom1;//Mazda2三厢1.3L
        private int wholesaleM2ThreeRoom2;//Mazda2三厢1.5L
        private int wholesaleM2ThreeRoomSub;//Mazda2三厢小计
        private int wholesaleM2Sub;//Mazda2小计
        private int wholesaleM32012ThreeRoom1;//Mazda3经典款2012型三厢1.6L
        private int wholesaleM32012ThreeRoom2;//Mazda3经典款2012型三厢2.0L
        private int wholesaleM32012ThreeRoomSub;//Mazda3经典款2012型三厢小计
        private int wholesaleM3SDNThreeRoom1;//Mazda3星骋SDN三厢1.6L
        private int wholesaleM3SDNThreeRoom2;//Mazda3星骋SDN三厢2.0L
        private int wholesaleM3SDNThreeRoomSub;//Mazda3星骋SDN三厢小计
        private int wholesaleM35HBThreeRoom1;//Mazda3星骋5HB三厢1.6L
        private int wholesaleM35HBThreeRoom2;//Mazda3星骋5HB三厢2.0L
        private int wholesaleM35HBThreeRoomSub;//Mazda3星骋5HB三厢小计
        private int wholesaleM3BUTwoRoom1;//Mazda3 BU两厢1.6L
        private int wholesaleM3BUTwoRoom2;//Mazda3 BU两厢2.0L
        private int wholesaleM3BUTwoRoomSub;//Mazda3 BU两厢小计
        private int wholesaleM3Sub;//Mazda3小计
        private int wholesaleDomesticMazdaSub;//Mazda国产车批发预测总计
        private int wholesaleMazdaSub;//Mazda批发预测总计

        public int WholesaleM2TwoRoom1 {
            get {
                return this.wholesaleM2TwoRoom1;
            }
            set {
                this.wholesaleM2TwoRoom1 = value;
                this.OnPropertyChanged("WholesaleM2TwoRoom1");
            }
        }

        public int WholesaleM2TwoRoom2 {
            get {
                return this.wholesaleM2TwoRoom2;
            }
            set {
                this.wholesaleM2TwoRoom2 = value;
                this.OnPropertyChanged("WholesaleM2TwoRoom2");
            }
        }

        public int WholesaleM2TwoRoomSub {
            get {
                return this.wholesaleM2TwoRoomSub;
            }
            set {
                this.wholesaleM2TwoRoomSub = value;
                this.OnPropertyChanged("WholesaleM2TwoRoomSub");
            }
        }

        public int WholesaleM2ThreeRoom1 {
            get {
                return this.wholesaleM2ThreeRoom1;
            }
            set {
                this.wholesaleM2ThreeRoom1 = value;
                this.OnPropertyChanged("WholesaleM2ThreeRoom1");
            }
        }

        public int WholesaleM2ThreeRoom2 {
            get {
                return this.wholesaleM2ThreeRoom2;
            }
            set {
                this.wholesaleM2ThreeRoom2 = value;
                this.OnPropertyChanged("WholesaleM2ThreeRoom2");
            }
        }

        public int WholesaleM2ThreeRoomSub {
            get {
                return this.wholesaleM2ThreeRoomSub;
            }
            set {
                this.wholesaleM2ThreeRoomSub = value;
                this.OnPropertyChanged("WholesaleM2ThreeRoomSub");
            }
        }

        public int WholesaleM2Sub {
            get {
                return this.wholesaleM2Sub;
            }
            set {
                this.wholesaleM2Sub = value;
                this.OnPropertyChanged("WholesaleM2Sub");
            }
        }

        public int WholesaleM32012ThreeRoom1 {
            get {
                return this.wholesaleM32012ThreeRoom1;
            }
            set {
                this.wholesaleM32012ThreeRoom1 = value;
                this.OnPropertyChanged("WholesaleM32012ThreeRoom1");
            }
        }

        public int WholesaleM32012ThreeRoom2 {
            get {
                return this.wholesaleM32012ThreeRoom2;
            }
            set {
                this.wholesaleM32012ThreeRoom2 = value;
                this.OnPropertyChanged("WholesaleM32012ThreeRoom2");
            }
        }

        public int WholesaleM32012ThreeRoomSub {
            get {
                return this.wholesaleM32012ThreeRoomSub;
            }
            set {
                this.wholesaleM32012ThreeRoomSub = value;
                this.OnPropertyChanged("WholesaleM32012ThreeRoomSub");
            }
        }

        public int WholesaleM3SDNThreeRoom1 {
            get {
                return this.wholesaleM3SDNThreeRoom1;
            }
            set {
                this.wholesaleM3SDNThreeRoom1 = value;
                this.OnPropertyChanged("WholesaleM3SDNThreeRoom1");
            }
        }

        public int WholesaleM3SDNThreeRoom2 {
            get {
                return this.wholesaleM3SDNThreeRoom2;
            }
            set {
                this.wholesaleM3SDNThreeRoom2 = value;
                this.OnPropertyChanged("WholesaleM3SDNThreeRoom2");
            }
        }

        public int WholesaleM3SDNThreeRoomSub {
            get {
                return this.wholesaleM3SDNThreeRoomSub;
            }
            set {
                this.wholesaleM3SDNThreeRoomSub = value;
                this.OnPropertyChanged("WholesaleM3SDNThreeRoomSub");
            }
        }

        public int WholesaleM35HBThreeRoom1 {
            get {
                return this.wholesaleM35HBThreeRoom1;
            }
            set {
                this.wholesaleM35HBThreeRoom1 = value;
                this.OnPropertyChanged("WholesaleM35HBThreeRoom1");
            }
        }

        public int WholesaleM35HBThreeRoom2 {
            get {
                return this.wholesaleM35HBThreeRoom2;
            }
            set {
                this.wholesaleM35HBThreeRoom2 = value;
                this.OnPropertyChanged("WholesaleM35HBThreeRoom2");
            }
        }

        public int WholesaleM35HBThreeRoomSub {
            get {
                return this.wholesaleM35HBThreeRoomSub;
            }
            set {
                this.wholesaleM35HBThreeRoomSub = value;
                this.OnPropertyChanged("WholesaleM35HBThreeRoomSub");
            }
        }

        public int WholesaleM3BUTwoRoom1 {
            get {
                return this.wholesaleM3BUTwoRoom1;
            }
            set {
                this.wholesaleM3BUTwoRoom1 = value;
                this.OnPropertyChanged("WholesaleM3BUTwoRoom1");
            }
        }

        public int WholesaleM3BUTwoRoom2 {
            get {
                return this.wholesaleM3BUTwoRoom2;
            }
            set {
                this.wholesaleM3BUTwoRoom2 = value;
                this.OnPropertyChanged("WholesaleM3BUTwoRoom2");
            }
        }

        public int WholesaleM3BUTwoRoomSub {
            get {
                return this.wholesaleM3BUTwoRoomSub;
            }
            set {
                this.wholesaleM3BUTwoRoomSub = value;
                this.OnPropertyChanged("WholesaleM3BUTwoRoomSub");
            }
        }

        public int WholesaleM3Sub {
            get {
                return this.wholesaleM3Sub;
            }
            set {
                this.wholesaleM3Sub = value;
                this.OnPropertyChanged("WholesaleM3Sub");
            }
        }

        public int WholesaleDomesticMazdaSub {
            get {
                return this.wholesaleDomesticMazdaSub;
            }
            set {
                this.wholesaleDomesticMazdaSub = value;
                this.OnPropertyChanged("WholesaleDomesticMazdaSub");
            }
        }

        public int WholesaleMazdaSub {
            get {
                return this.wholesaleMazdaSub;
            }
            set {
                this.wholesaleMazdaSub = value;
                this.OnPropertyChanged("WholesaleMazdaSub");
            }
        }

        #endregion

        private void YearComboBox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            var yearComboBox = sender as DcsComboBox;
            if(yearComboBox == null)
                return;
            int selectedYear = 1;
            if(yearComboBox.SelectedValue != null) {
                var selectYear = Int32.TryParse(yearComboBox.SelectedValue.ToString(), out selectedYear);
            }
            selectedYear = selectedYear != DateTime.Now.Year ? DateTime.Now.Month : 1;
            this.AddKvMonthes(selectedYear);
        }

        private void AddKvMonthes(int monthes) {
            this.KvMonthes.Clear();
            for(var i = 1; i <= 12; i++) {
                this.KvMonthes.Add(new KeyValuePair {
                    Key = i,
                    Value = i.ToString(CultureInfo.InvariantCulture)
                });
            }
        }
    }
}
