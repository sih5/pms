﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class DealerRegionManagerAffiDataEditView {
        public DealerRegionManagerAffiDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("DealerRegionManagerAffi"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealerRegionManagerAffisQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var dealerRegionManagerAffi = this.DataContext as DealerRegionManagerAffi;
            if(dealerRegionManagerAffi == null)
                return;
            dealerRegionManagerAffi.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(dealerRegionManagerAffi.DealerCode))
                dealerRegionManagerAffi.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_DealerRegionManagerAffi_DealerCodeIsNull, new[] {
                    "DealerCode"
                }));
            if(string.IsNullOrEmpty(dealerRegionManagerAffi.DealerName))
                dealerRegionManagerAffi.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_DealerRegionManagerAffi_DealerNameIsNull, new[] {
                    "DealerName"
                }));
            if(string.IsNullOrEmpty(dealerRegionManagerAffi.RegionManager))
                dealerRegionManagerAffi.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_DealerRegionManagerAffi_RegionManagerIsNull, new[] {
                    "RegionManager"
                }));
            if(dealerRegionManagerAffi.HasValidationErrors)
                return;
            ((IEditableObject)dealerRegionManagerAffi).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DealerRegionManagerAffi;
            }
        }
    }
}
