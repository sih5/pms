﻿
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class DealerRegionManagerAffiForImportDataEditView {
        private ICommand exportFileCommand;

        private DataGridViewBase importDataGrid;
        private DataGridViewBase ImportDataGrid {
            get {
                return this.importDataGrid ?? (this.importDataGrid = DI.GetDataGridView("DealerRegionManagerAffiForImport"));
            }
        }

        private void CreateUI() {
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            detailDataEditView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_DealerRegionManagerAffi, null, () => this.ImportDataGrid);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        public DealerRegionManagerAffiForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => ((DcsDataGridViewBase)this.ImportDataGrid).ExportData());
        }

        private void UploadFileProcessing(string fileName) {
            this.DomainContext.导入区域经理(fileName, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                }

                this.ImportComplete();
                this.DomainContext.IsInvoking = false;
                if(string.IsNullOrEmpty(loadOp.Value) || string.IsNullOrWhiteSpace(loadOp.Value)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImport);
                } else {
                    this.ExportFile(loadOp.Value);
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImportWithError);
                }
            }, null);

        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_Import_DealerRegionManagerAffi;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
    }
}
