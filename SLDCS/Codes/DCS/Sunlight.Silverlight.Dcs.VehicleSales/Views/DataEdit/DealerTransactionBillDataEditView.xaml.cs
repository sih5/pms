﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class DealerTransactionBillDataEditView {
        private DataGridViewBase dealerTransactionDetailForEditDataGridView;
        private DataGridViewBase DealerTransactionDetailForEditDataGridView {
            get {
                if(this.dealerTransactionDetailForEditDataGridView == null) {
                    this.dealerTransactionDetailForEditDataGridView = DI.GetDataGridView("DealerTransactionDetailForEdit");
                    this.dealerTransactionDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.dealerTransactionDetailForEditDataGridView;
            }
        }

        public DealerTransactionBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DealerTransactionBill;
            }
        }

        private void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("DealerTransactionBill");
            this.Root.Children.Add(dataEditPanel);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(VehicleSalesUIStrings.DetailPanel_Title_DealerTransactionDetail, null, () => this.DealerTransactionDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override void OnEditSubmitting() {
            var dealerTransactionBill = this.DataContext as DealerTransactionBill;
            if(dealerTransactionBill == null)
                return;
            dealerTransactionBill.ValidationErrors.Clear();
            foreach(var partsRetailOrderDetail in dealerTransactionBill.DealerTransactionDetails) {
                partsRetailOrderDetail.ValidationErrors.Clear();
            }
            if(string.IsNullOrEmpty(dealerTransactionBill.DealerSellerName))
                dealerTransactionBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_DealerTransactionBill_DealerSellerName, new[] {
                    "DealerSellerName"
                }));
            if(dealerTransactionBill.DealerBuyerWarehouseId == default(int))
                dealerTransactionBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_DealerTransactionBill_DealerBuyerWarehouseCode, new[] {
                    "DealerBuyerWarehouseId"
                }));
            if(dealerTransactionBill.DealerSellerId == dealerTransactionBill.DealerBuyerId) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_dealerTransactionBill_DealerSellerIdEqualDealerBuyerId);
                return;
            }
            if(dealerTransactionBill.DealerTransactionDetails == null || !dealerTransactionBill.DealerTransactionDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_DealerTransactionBill_DealerTransactionDetail_IsNotNull);
                return;
            }
            foreach(var partsRetailOrderDetail in dealerTransactionBill.DealerTransactionDetails) {
                ((IEditableObject)partsRetailOrderDetail).EndEdit();
            }
            if(dealerTransactionBill.DealerTransactionDetails.Any(entity => string.IsNullOrEmpty(entity.ProductCode))) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_DealerTransactionBill_DealerTransactionDetail_ProductCodeIsNotNull);
                return;
            }
            if(dealerTransactionBill.HasValidationErrors || dealerTransactionBill.DealerTransactionDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)dealerTransactionBill).EndEdit();
            base.OnEditSubmitting();
        }
    }
}
