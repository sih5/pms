﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class DealerTransactionBillForApproveDataEditView {
        private DataGridViewBase dealerTransactionDetailForApproveDataGridView;

        private DataGridViewBase DealerTransactionDetailForApproveDataGridView {
            get {
                if(this.dealerTransactionDetailForApproveDataGridView == null) {
                    this.dealerTransactionDetailForApproveDataGridView = DI.GetDataGridView("DealerTransactionDetailForApprove");
                    this.dealerTransactionDetailForApproveDataGridView.DomainContext = this.DomainContext;
                }
                return this.dealerTransactionDetailForApproveDataGridView;
            }
        }

        public DealerTransactionBillForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DealerTransactionBill;
            }
        }

        private void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("DealerTransactionBillForApprove");
            this.Root.Children.Add(dataEditPanel);
            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Header = VehicleSalesUIStrings.DetailPanel_Title_DealerTransactionDetail,
                Content = this.DealerTransactionDetailForApproveDataGridView
            });
            tabControl.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(tabControl);

            this.Root.Children.Add(this.CreateVerticalLine(1));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load((EntityQuery)this.DomainContext.GetDealerTransactionBillWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.Cast<DealerTransactionBill>().SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    if(entity.DealerTransactionDetails != null) {
                        foreach(var detail in entity.DealerTransactionDetails) {
                            for(int i = 1; i < detail.Quantity; i++) {
                                entity.DealerTransactionDetails.Add(new DealerTransactionDetail {
                                    DealerTransactionBillId = detail.DealerTransactionBillId,
                                    ProductCategoryCode = detail.ProductCategoryCode,
                                    ProductCategoryName = detail.ProductCategoryName,
                                    ProductId = detail.ProductId,
                                    ProductCode = detail.ProductCode,
                                    ProductName = detail.ProductName
                                });
                            }
                        }
                    }
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var dealerTransactionBill = this.DataContext as DealerTransactionBill;
            if(dealerTransactionBill == null)
                return;
            foreach(var partsRetailOrderDetail in dealerTransactionBill.DealerTransactionDetails) {
                partsRetailOrderDetail.ValidationErrors.Clear();
            }
            if(dealerTransactionBill.DealerTransactionDetails == null || !dealerTransactionBill.DealerTransactionDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_DealerTransactionBill_DealerTransactionDetail_IsNotNull);
                return;
            }
            foreach(var partsRetailOrderDetail in dealerTransactionBill.DealerTransactionDetails) {
                ((IEditableObject)partsRetailOrderDetail).EndEdit();
            }
            if(dealerTransactionBill.DealerTransactionDetails.Any(entity => string.IsNullOrEmpty(entity.VIN))) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_DealerTransactionBill_DealerTransactionDetail_VINIsNotNull);
                return;
            }
            if(dealerTransactionBill.HasValidationErrors || dealerTransactionBill.DealerTransactionDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)dealerTransactionBill).EndEdit();
            try {
                if(dealerTransactionBill.Can审核经销商买卖单)
                    dealerTransactionBill.审核经销商买卖单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }
    }
}
