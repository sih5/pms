﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class DealerVehicleMonthQuotaDataEditView {
        public DealerVehicleMonthQuotaDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DealerVehicleMonthQuota;
            }
        }

        protected override void OnEditSubmitting() {
            var dealerVehicleMonthQuota = this.DataContext as DealerVehicleMonthQuota;
            if(dealerVehicleMonthQuota == null)
                return;
            dealerVehicleMonthQuota.ValidationErrors.Clear();
            if(dealerVehicleMonthQuota.YearOfPlan == default(int))
                dealerVehicleMonthQuota.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_DealerVehicleMonthQuota_YearOfPlanIsNull, new[]{
                    "YearOfPlan"
                }));
            if(dealerVehicleMonthQuota.MonthOfPlan == default(int))
                dealerVehicleMonthQuota.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_DealerVehicleMonthQuota_MonthOfPlanIsNull, new[]{
                    "MonthOfPlan"
                }));
            if(string.IsNullOrWhiteSpace(dealerVehicleMonthQuota.VehicleModelCategoryName))
                dealerVehicleMonthQuota.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_DealerVehicleMonthQuota_VehicleModelCategoryNameIsNull, new[] {
                    "VehicleModelCategoryName"
                }));
            if(string.IsNullOrWhiteSpace(dealerVehicleMonthQuota.DealerCode))
                dealerVehicleMonthQuota.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_DealerVehicleMonthQuota_DealerCodeNameIsNull, new[] {
                    "DealerCode"
                }));
            if(dealerVehicleMonthQuota.HasValidationErrors)
                return;
            ((IEditableObject)dealerVehicleMonthQuota).EndEdit();
            base.OnEditSubmitting();
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("DealerVehicleMonthQuota"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetDealerVehicleMonthQuotasQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }
    }
}
