﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class DealerVehicleMonthQuotaForImportDataEditView {
        private ICommand exportFileCommand;
        private DataGridViewBase dealerVehicleMonthQuotaTemplateDataGridView;

        public DealerVehicleMonthQuotaForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = DcsUIStrings.BusinessName_DealerVehicleMonthQuota,
                Content = this.DealerVehicleMonthQuotaTemplateDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_DealerVehicleMonthQuota;
            }
        }

        private DataGridViewBase DealerVehicleMonthQuotaTemplateDataGridView {
            get {
                return this.dealerVehicleMonthQuotaTemplateDataGridView ?? (dealerVehicleMonthQuotaTemplateDataGridView = DI.GetDataGridView("DealerVehicleMonthQuotaForImport"));
            }
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            this.DomainContext.导入整车经销商月目标(fileName, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                }

                this.ImportComplete();
                this.DomainContext.IsInvoking = false;
                if(string.IsNullOrEmpty(loadOp.Value) || string.IsNullOrWhiteSpace(loadOp.Value)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImport);
                } else {
                    this.ExportFile(loadOp.Value);
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImportWithError);
                }
            }, null);
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    InitialExportCommand();
                return this.exportFileCommand;
            }
        }

        private void InitialExportCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => ((DcsDataGridViewBase)this.DealerVehicleMonthQuotaTemplateDataGridView).ExportData());
        }
    }
}
