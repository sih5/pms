﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class JumpShutoffDayDataEditView {

        public JumpShutoffDayDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_JumpShutoffDay;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("JumpShutoffDay"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetJumpShutoffDaysQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var jumpShutoffDay = this.DataContext as JumpShutoffDay;
            if(jumpShutoffDay == null)
                return;
            jumpShutoffDay.ValidationErrors.Clear();
            if(jumpShutoffDay.YearOfOrder.Equals(default(int)))
                jumpShutoffDay.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_JumpShutoffDay_YearOfOrderIsNull, new[] {
                    "YearOfOrder"
                }));
            if(jumpShutoffDay.MonthOfOrder.Equals(default(int)))
                jumpShutoffDay.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_JumpShutoffDay_MonthOfOrderIsNull, new[] {
                    "MonthOfOrder"
                }));
            if(string.IsNullOrEmpty(jumpShutoffDay.JumpShutoffDate.ToString(CultureInfo.InvariantCulture)))
                jumpShutoffDay.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_JumpShutoffDay_JumpShutoffDateIsNull, new[] {
                    "JumpShutoffDate"
                }));

            if(jumpShutoffDay.ValidationErrors.Any())
                return;
            ((IEditableObject)jumpShutoffDay).EndEdit();
            base.OnEditSubmitting();
        }
    }
}
