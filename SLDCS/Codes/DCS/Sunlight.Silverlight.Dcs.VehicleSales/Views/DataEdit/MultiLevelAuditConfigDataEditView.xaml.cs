﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class MultiLevelAuditConfigDataEditView {
        public MultiLevelAuditConfigDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetMultiLevelAuditConfigWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        private DataGridViewBase multiLevelAuditConfigDetailForEditDataGridView;
        private DataGridViewBase MultiLevelAuditConfigDetailForEditDataGridView {
            get {
                if(multiLevelAuditConfigDetailForEditDataGridView == null) {
                    this.multiLevelAuditConfigDetailForEditDataGridView = DI.GetDataGridView("MultiLevelAuditConfigDetailForEdit");
                    this.multiLevelAuditConfigDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.multiLevelAuditConfigDetailForEditDataGridView;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("MultiLevelAuditConfig"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(VehicleSalesUIStrings.DetailPanel_Title_MultiLevelAuditConfigDetail, null, () => this.MultiLevelAuditConfigDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(detailDataEditView);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_MultiLevelAuditConfig;
            }
        }

        protected override void OnEditSubmitting() {
            var multiLevelAuditConfig = this.DataContext as MultiLevelAuditConfig;
            if(multiLevelAuditConfig == null)
                return;
            this.multiLevelAuditConfigDetailForEditDataGridView.CommitEdit();
            multiLevelAuditConfig.ValidationErrors.Clear();
            foreach(var detail in multiLevelAuditConfig.MultiLevelAuditConfigDetails.Where(r => r.HasValidationErrors))
                detail.ValidationErrors.Clear();
            if(multiLevelAuditConfig.BusinessObjectId == default(int))
                multiLevelAuditConfig.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_MultiLevelAuditConfig_BusinessObjectIdIsNotNull, new[] {
                    "BusinessObjectId"
                }));
            if(multiLevelAuditConfig.InitialStatus == default(int))
                multiLevelAuditConfig.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_MultiLevelAuditConfig_InitialStatusIsNotNull, new[] {
                    "InitialStatus"
                }));
            foreach(var multiLevelAuditConfigDetails in multiLevelAuditConfig.MultiLevelAuditConfigDetails) {
                multiLevelAuditConfigDetails.InitialPostAuditStatusId = multiLevelAuditConfigDetails.InitialPostAuditStatusId ?? 0;
                ((IEditableObject)multiLevelAuditConfigDetails).EndEdit();
            }
            if(multiLevelAuditConfig.MultiLevelAuditConfigDetails == null) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_MultiLevelAuditConfig_MultiLevelAuditConfigDetailsIsNotNull, 5);
                return;
            }
            if(multiLevelAuditConfig.MultiLevelAuditConfigDetails.Any(entity => entity.AuditHierarchySettingId == default(int))) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_MultiLevelAuditConfig_AuditHierarchySettingIdIsNotNull, 5);
                return;
            }
            if(multiLevelAuditConfig.MultiLevelAuditConfigDetails.Any(entity => entity.ApprovedPostAuditStatusId == default(int))) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_MultiLevelAuditConfig_ApprovedPostAuditStatusIdIsNotNull, 5);
                return;
            }
            if(multiLevelAuditConfig.MultiLevelAuditConfigDetails.Any(entity => entity.DisapprovedPostAuditStatusId == default(int))) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_MultiLevelAuditConfig_DisapprovedPostAuditStatusIdIsNotNull, 5);
                return;
            }
            var details = multiLevelAuditConfig.MultiLevelAuditConfigDetails.ToArray();
            for(int rowId = 0; rowId < details.Length; rowId++) {
                if(rowId != details.Length - 1 && details[rowId].InitialPostAuditStatusId != null && details[rowId + 1].InitialPostAuditStatusId == null) {
                    UIHelper.ShowNotification("清单中的其它初始状态如果上面有一条是不为空的，下面的初始状态都不能为空");
                    return;
                }
                if(details[rowId].InitialPostAuditStatusId != null && details[rowId].InitialPostAuditStatusId > 0) {
                    if(details.Take(rowId).All(e => e.ApprovedPostAuditStatusId != details[rowId].InitialPostAuditStatusId)) {
                        UIHelper.ShowNotification("从第二条记录开始，记录中的审核初始状态必须等于上面某条记录的审核通过状态");
                        return;
                    }
                }
            }

            if(multiLevelAuditConfig.HasValidationErrors || multiLevelAuditConfig.MultiLevelAuditConfigDetails.Any(e => e.HasValidationErrors))
                return;
            ((IEditableObject)multiLevelAuditConfig).EndEdit();
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

    }
}
