﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class OEMVehicleMonthQuotaDataEditView {

        public OEMVehicleMonthQuotaDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_OEMVehicleMonthQuota;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("OEMVehicleMonthQuota"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetOEMVehicleMonthQuotasQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var oEMVehicleMonthQuota = this.DataContext as OEMVehicleMonthQuota;
            if(oEMVehicleMonthQuota == null)
                return;
            oEMVehicleMonthQuota.ValidationErrors.Clear();
            if(oEMVehicleMonthQuota.YearOfPlan.Equals(default(int)))
                oEMVehicleMonthQuota.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_OEMVehicleMonthQuota_YearOfPlanIsNull, new[] {
                    "YearOfPlan"
                }));
            if(oEMVehicleMonthQuota.MonthOfPlan.Equals(default(int)))
                oEMVehicleMonthQuota.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_OEMVehicleMonthQuota_MonthOfPlanIsNull, new[] {
                    "MonthOfPlan"
                }));
            if(string.IsNullOrEmpty(oEMVehicleMonthQuota.VehicleModelCategoryName))
                oEMVehicleMonthQuota.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_OEMVehicleMonthQuota_VehicleModelCategoryNameIsNull, new[] {
                    "VehicleModelCategoryName"
                }));
            if(oEMVehicleMonthQuota.ValidationErrors.Any())
                return;
            ((IEditableObject)oEMVehicleMonthQuota).EndEdit();
            base.OnEditSubmitting();
        }
    }
}