﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class ProductCategoryDataEditView {
        private ProductCategoryForProductDataTreeView productCategoryForProductDataTreeView;
        public ProductCategory tempProductCategory;
        private ProductCategoryForProductDataTreeView ProductCategoryForProductDataTreeView {
            get {
                if(this.productCategoryForProductDataTreeView == null) {
                    this.productCategoryForProductDataTreeView = new ProductCategoryForProductDataTreeView();
                    this.productCategoryForProductDataTreeView.DomainContext = new DcsDomainContext();
                    this.productCategoryForProductDataTreeView.EntityQuery = this.productCategoryForProductDataTreeView.DomainContext.GetProductCategoriesQuery().Where(p => p.Status == (int)DcsBaseDataStatus.有效);
                    this.productCategoryForProductDataTreeView.OnTreeViewItemClick += ProductCategoryDataTreeView_OnTreeViewItemClick;
                }
                return this.productCategoryForProductDataTreeView;
            }
        }

        public ProductCategoryDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += ProductCategoryDataEditView_Loaded;
        }

        void ProductCategoryDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            ProductCategoryForProductDataTreeView.RefreshDataTree();
            this.tempProductCategory = this.Tag as ProductCategory;
            var productCategory = this.DataContext as ProductCategory;
            if(productCategory != null)
                if(EditState == DataEditState.New)
                    if(productCategory.ParentId.HasValue)
                        this.ProductCategoryForProductDataTreeView.SelectedItemId = productCategory.ParentId.Value;
                    else
                        return;
                else
                    this.ProductCategoryForProductDataTreeView.SelectedItemId = productCategory.Id;
        }

        private void ProductCategoryDataTreeView_OnTreeViewItemClick(object sender, RadRoutedEventArgs radRoutedEventArgs) {
            var selectItem = ((RadTreeView)sender).SelectedItem as RadTreeViewItem;
            var productCategory = this.DataContext as ProductCategory;
            if(selectItem == null || !(selectItem.Tag is ProductCategory) || productCategory == null)
                return;
            tempProductCategory = (ProductCategory)selectItem.Tag;
            if(tempProductCategory.RootProductCategory == null)
                return;
            productCategory.RootCategoryId = tempProductCategory.RootProductCategory.Id;
            productCategory.RootCategoryName = tempProductCategory.RootProductCategory.Name;
            productCategory.RootCategoryCode = tempProductCategory.RootProductCategory.Code;
            productCategory.ProductCategoryLevel = tempProductCategory.ProductCategoryLevel + 1;
            productCategory.ParentId = tempProductCategory.Id;
            productCategory.ParentCode = tempProductCategory.Code;
            productCategory.ParentName = tempProductCategory.Name;
            if(productCategory.ParentCode != default(string) && productCategory.ValidationErrors.Any(r => r.MemberNames.Contains("ParentCode")))
                productCategory.ValidationErrors.Remove(productCategory.ValidationErrors.First(r => r.MemberNames.Contains("ParentCode")));
            if(productCategory.ParentName != default(string) && productCategory.ValidationErrors.Any(r => r.MemberNames.Contains("ParentName")))
                productCategory.ValidationErrors.Remove(productCategory.ValidationErrors.First(r => r.MemberNames.Contains("ParentName")));
            if(productCategory.ParentCode != default(string) && productCategory.ValidationErrors.Any(r => r.MemberNames.Contains("RootCategoryCode")))
                productCategory.ValidationErrors.Remove(productCategory.ValidationErrors.First(r => r.MemberNames.Contains("RootCategoryCode")));
            if(productCategory.ParentName != default(string) && productCategory.ValidationErrors.Any(r => r.MemberNames.Contains("RootCategoryName")))
                productCategory.ValidationErrors.Remove(productCategory.ValidationErrors.First(r => r.MemberNames.Contains("RootCategoryName")));
        }

        private void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("ProductCategory");
            dataEditPanel.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(dataEditPanel);
            ProductCategoryForProductDataTreeView.SetValue(Grid.ColumnProperty, 0);
            this.Root.Children.Add(ProductCategoryForProductDataTreeView);
            if(EditState == DataEditState.New)
                this.RegisterButton(new ButtonItem {
                    Title = VehicleSalesUIStrings.Action_Title_SaveNoExit,
                    Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/SaveNoExit.png", UriKind.Relative),
                    Command = new Core.Command.DelegateCommand(this.SaveNoExit)
                });
        }

        private void SaveNoExit() {
            if(CheckProductCategory()) {
                this.DomainContext.SubmitChanges(submitOp => {
                    if(submitOp.HasError) {
                        if(!submitOp.IsErrorHandled)
                            submitOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                        return;
                    }
                    if(tempProductCategory != null) {
                        var newProductCategory = new ProductCategory();
                        newProductCategory.RootCategoryId = tempProductCategory.RootProductCategory.Id;
                        newProductCategory.RootCategoryName = tempProductCategory.RootProductCategory.Name;
                        newProductCategory.RootCategoryCode = tempProductCategory.RootProductCategory.Code;
                        newProductCategory.ProductCategoryLevel = tempProductCategory.ProductCategoryLevel + 1;
                        newProductCategory.ParentId = tempProductCategory.Id;
                        newProductCategory.ParentCode = tempProductCategory.Code;
                        newProductCategory.ParentName = tempProductCategory.Name;
                        newProductCategory.Status = (int)DcsBaseDataStatus.有效;
                        this.DomainContext.ProductCategories.Add(newProductCategory);
                        this.DataContext = null;
                        this.SetValue(DataContextProperty, newProductCategory);
                    }
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Confirm_SaveSuccess);
                    ProductCategoryForProductDataTreeView.RefreshDataTree();
                }, null);
            }
        }

        private bool CheckProductCategory() {
            var productCategory = this.DataContext as ProductCategory;
            if(productCategory == null)
                return false;
            productCategory.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(productCategory.RootCategoryCode))
                productCategory.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_ProductCategory_RootCategoryCodeIsNull, new[]{
                    "RootCategoryCode"
                }));
            if(string.IsNullOrEmpty(productCategory.RootCategoryName))
                productCategory.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_ProductCategory_RootCategoryNameIsNull, new[]{
                    "RootCategoryName"
                }));
            if(string.IsNullOrEmpty(productCategory.ParentCode))
                productCategory.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_ProductCategory_ParentCodeIsNull, new[]{
                    "ParentCode"
                }));
            if(string.IsNullOrEmpty(productCategory.ParentName))
                productCategory.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_ProductCategory_ParentNameIsNull, new[]{
                    "ParentName"
                }));
            if(productCategory.ProductCategoryLevel > GlobalVar.PRODUCTCATEGORY_MAX_LEVEL) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_ProductCategory_ProductCategoryLevelIsMax);
                return false;
            }
            if(productCategory.HasValidationErrors)
                return false;
            ((IEditableObject)productCategory).EndEdit();
            return true;

        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetProductCategoriyWithRootAndParentQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    if(entity.RootProductCategory != null) {
                        entity.RootCategoryName = entity.RootProductCategory.Name;
                        entity.RootCategoryCode = entity.RootProductCategory.Code;
                    }
                    if(entity.ParentProductCategory != null) {
                        entity.ParentCode = entity.ParentProductCategory.Code;
                        entity.ParentName = entity.ParentProductCategory.Name;
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(CheckProductCategory())
                base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_ProductCategory;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
