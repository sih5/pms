﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class ProductDataEditView {
        private ProductCategoryForProductDataTreeView productCategoryForProductDataTreeView;
        public ProductCategory tempProductCategory;
        private ProductDataEditPanel productDataEditPanel;
        private ProductAffiProductCategory tempProductAffiProductCategory;

        public ProductAffiProductCategory TempProductAffiProductCategory {
            get {
                return this.tempProductAffiProductCategory ?? new ProductAffiProductCategory();
            }
            set {
                this.tempProductAffiProductCategory = value;
            }
        }

        private ProductCategoryForProductDataTreeView ProductCategoryForProductDataTreeView {
            get {
                if(this.productCategoryForProductDataTreeView == null) {
                    this.productCategoryForProductDataTreeView = new ProductCategoryForProductDataTreeView();
                    this.productCategoryForProductDataTreeView.DomainContext = new DcsDomainContext();
                    this.productCategoryForProductDataTreeView.EntityQuery = this.productCategoryForProductDataTreeView.DomainContext.GetProductCategoriesQuery().Where(p => p.Status == (int)DcsBaseDataStatus.有效);
                    this.productCategoryForProductDataTreeView.OnTreeViewItemClick += ProductCategoryDataTreeView_OnTreeViewItemClick;
                    this.productCategoryForProductDataTreeView.RefreshDataTree();
                }
                return this.productCategoryForProductDataTreeView;
            }
        }

        public ProductDataEditPanel ProductDataEditPanel {
            get {
                return this.productDataEditPanel ?? (this.productDataEditPanel = (ProductDataEditPanel)DI.GetDataEditPanel("Product"));
            }
        }

        public ProductDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += ProductDataEditView_Loaded;
        }

        private void ProductDataEditView_Loaded(object sender, RoutedEventArgs e) {
            if(TempProductAffiProductCategory != null)
                this.ProductCategoryForProductDataTreeView.SelectedItemId = TempProductAffiProductCategory.ProductCategoryId;//第三级节点只有展开时才会被选中
        }

        private void ProductCategoryDataTreeView_OnTreeViewItemClick(object sender, RadRoutedEventArgs radRoutedEventArgs) {
            var selectItem = ((RadTreeView)sender).SelectedItem as RadTreeViewItem;
            var product = this.DataContext as Product;
            if(selectItem == null || !(selectItem.Tag is ProductCategory) || product == null)
                return;
            tempProductCategory = (ProductCategory)selectItem.Tag;
            product.ParentCode = tempProductCategory.Code;
            product.ParentName = tempProductCategory.Name;
            TempProductAffiProductCategory.ProductCategoryId = tempProductCategory.Id;
            TempProductAffiProductCategory.ProductId = product.Id;
            TempProductAffiProductCategory.RootCategoryId = tempProductCategory.RootCategoryId;
        }

        private void CreateUI() {
            this.RegisterButton(new ButtonItem {
                Title = VehicleSalesUIStrings.Action_Title_SaveNoExit,
                Command = new DelegateCommand(this.BatchAddproductInternal),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/SaveNoExit.png", UriKind.Relative)
            });
            ProductDataEditPanel.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(ProductDataEditPanel);
            ProductCategoryForProductDataTreeView.SetValue(Grid.ColumnProperty, 0);
            this.Root.Children.Add(ProductCategoryForProductDataTreeView);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void CurrentEntityVerification(Product product) {
            product.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(product.ParentCode))
                product.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_Product_ParentCodeIsNull, new[]{
                    "ParentCode"
                }));
            if(string.IsNullOrEmpty(product.ParentName))
                product.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_Product_ParentNameIsNull, new[]{
                    "ParentName"
                }));
            if(string.IsNullOrEmpty(product.Code))
                product.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_Product_ParentCodeIsNull, new[]{
                    "Code"
                }));
            if(string.IsNullOrEmpty(product.EmissionStandard))
                product.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_Product_EmissionStandardIsNull, new[]{
                    "EmissionStandard"
                }));
            if(string.IsNullOrEmpty(product.Name))
                product.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_Product_NameIsNull, new[]{
                    "Name"
                }));
            if(string.IsNullOrEmpty(product.ColorCode))
                product.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_Product_ColorIsNull, new[]{
                    "Color"
                }));
            if(string.IsNullOrEmpty(product.Version))
                product.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_Product_VersionIsNull, new[]{
                    "Version"
                }));
            if(string.IsNullOrEmpty(product.EngineCylinder))
                product.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_Product_EngineCylinderIsNull, new[]{
                    "EngineCylinder"
                }));
            if(string.IsNullOrEmpty(product.Configuration))
                product.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_Product_ConfigurationIsNull, new[]{
                    "Configuration"
                }));
            if(string.IsNullOrEmpty(product.ManualAutomatic))
                product.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_Product_ManualAutomaticIsNull, new[]{
                    "ManualAutomatic"
                }));
            if(product.ProductLifeCycle == default(int))
                product.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_Product_ProductLifeCycleIsNull, new[]{
                    "ProductLifeCycle"
                }));
        }

        private void BatchAddproductInternal() {
            var product = this.DataContext as Product;
            if(product == null)
                return;
            if(tempProductCategory != null) {
                if(tempProductCategory.ProductCategoryLevel != GlobalVar.PRODUCTCATEGORY_MAX_LEVEL) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_Product_ProductCategoryLevelBanAdd);
                    return;
                }
            }
            CurrentEntityVerification(product);
            if(product.HasValidationErrors)
                return;
            //产品与产品分类关系不论新增修改界面都为新增状态
            product.ProductAffiProductCategories.Add(TempProductAffiProductCategory);
            ((IEditableObject)product).EndEdit();
            this.DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    DomainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_SubmitSuccess);
                var newProduct = this.CreateObjectToEdit<Product>();
                if(tempProductCategory != null) {
                    newProduct.ParentCode = tempProductCategory.Code;
                    newProduct.ParentName = tempProductCategory.Name;
                    this.TempProductAffiProductCategory = new ProductAffiProductCategory {
                        ProductCategoryId = tempProductCategory.Id,
                        ProductId = newProduct.Id,
                        RootCategoryId = tempProductCategory.RootCategoryId
                    };
                }
                newProduct.Status = (int)DcsMasterDataStatus.有效;
                newProduct.ProductLifeCycle = (int)DcsPartsBranchProductLifeCycle.新产品;
                newProduct.CanBeOrdered = true;
                newProduct.IfPurchasable = true;
                this.productCategoryForProductDataTreeView.RefreshDataTree();
                ProductDataEditPanel.TempBitmapImage = null;
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetProductWithProductAffiProductCategoryQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    var temp = entity.ProductAffiProductCategories.SingleOrDefault();
                    if(temp != null) {
                        entity.ParentName = temp.ProductCategory.Name;
                        entity.ParentCode = temp.ProductCategory.Code;
                        TempProductAffiProductCategory = temp;
                        this.ProductCategoryForProductDataTreeView.SelectedItemId = temp.ProductCategoryId;
                        entity.ProductAffiProductCategories.Remove(temp);
                    }
                    this.DataContext = null;//解决新增产品后立即修改 不触发DataEditPanel的DataContextChange问题
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var product = this.DataContext as Product;
            if(product == null)
                return;

            if(tempProductCategory != null) {
                if(tempProductCategory.ProductCategoryLevel != GlobalVar.PRODUCTCATEGORY_MAX_LEVEL) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_Product_ProductCategoryLevelBanAdd);
                    return;
                }
            }
            this.CurrentEntityVerification(product);
            if(product.HasValidationErrors)
                return;
            //产品与产品分类关系不论新增修改界面都为新增状态
            product.ProductAffiProductCategories.Add(TempProductAffiProductCategory);
            ((IEditableObject)product).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_Product;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditCancelled() {
            ProductDataEditPanel.TempBitmapImage = null;
            base.OnEditCancelled();
        }

        protected override void OnEditSubmitted() {
            ProductDataEditPanel.TempBitmapImage = null;
            base.OnEditSubmitted();
        }
    }
}
