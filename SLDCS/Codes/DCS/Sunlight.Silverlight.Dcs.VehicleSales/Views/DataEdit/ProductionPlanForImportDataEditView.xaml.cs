﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class ProductionPlanForImportDataEditView {
        private ICommand exportFileCommand;
        private DataGridViewBase productionPlanTemplateDataGridView;

        public ProductionPlanForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected DataGridViewBase ProductionPlanForImportDataGridView {
            get {
                return this.productionPlanTemplateDataGridView ?? (this.productionPlanTemplateDataGridView = DI.GetDataGridView("ProductionPlanForImport"));
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = DcsUIStrings.BusinessName_ProductionPlan,
                Content = this.ProductionPlanForImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }


        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_ProductionPlan;
            }
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            this.ExcelServiceClient.ImportProductionPlanAsync(fileName);
            this.ExcelServiceClient.ImportProductionPlanCompleted -= ExcelServiceClient_ImportProductionPlanCompleted;
            this.ExcelServiceClient.ImportProductionPlanCompleted += ExcelServiceClient_ImportProductionPlanCompleted;
        }

        private void ExcelServiceClient_ImportProductionPlanCompleted(object sender, ImportProductionPlanCompletedEventArgs e) {
            this.ImportComplete();
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImport);
            } else {
                if(this.HasImportingError)
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImportWithError);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    InitialExportCommand();
                return this.exportFileCommand;
            }
        }

        private void InitialExportCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => ((DcsDataGridViewBase)this.ProductionPlanForImportDataGridView).ExportData());
        }
    }
}
