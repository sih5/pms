﻿
using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.TreeView;
using ValidationResult = System.ComponentModel.DataAnnotations.ValidationResult;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class RegionConfugurationDataEditView {
        public RegionConfugurationDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("RegionDealerListForEdit");
                    this.dataGridView.SetValue(AllowDropProperty, true);
                    this.dataGridView.DomainContext = this.DomainContext;
                }
                return this.dataGridView;
            }
        }

        private void OnGridViewDropInfo(object sender, DragDropEventArgs e) {
            var draggedItems = e.Options.Payload as ICollection;
            if(draggedItems == null)
                return;
            var cue = e.Options.DragCue as TreeViewDragCue;
            if(e.Options.Status == DragStatus.DropPossible)
                cue.IsDropPossible = true;
            else if(e.Options.Status == DragStatus.DropImpossible)
                cue.IsDropPossible = false;
            else if(e.Options.Status == DragStatus.DropComplete) {
                var regionConfuguration = this.DataContext as RegionConfuguration;
                if(regionConfuguration == null)
                    return;
                foreach(var item in draggedItems) {
                    var dealer = item as Dealer;
                    if(dealer != null) {
                        if(regionConfuguration.RegionDealerLists.Any(entity => entity.DealerId == dealer.Id))
                            continue;
                        regionConfuguration.RegionDealerLists.Add(new RegionDealerList {
                            DealerId = dealer.Id
                        });
                    }
                }
            }
        }

        private void OnGridViewDropQuery(object sender, DragDropQueryEventArgs e) {
            var draggedItems = e.Options.Payload as IEnumerable;
            bool result = draggedItems.Cast<Dealer>().All(item => item != null);
            e.QueryResult = result;
            e.Handled = true;
        }

        private RegionAndDealerView regionAndDealerView;

        private RegionAndDealerView RegionAndDealerView {
            get {
                if(this.regionAndDealerView == null) {
                    this.regionAndDealerView = new RegionAndDealerView();
                    this.regionAndDealerView.RegionDataTreeView.DomainContext = this.DomainContext;
                }
                return this.regionAndDealerView;
            }
        }

        private void CreateUI() {
            RadDragAndDropManager.SetAllowDrop(this.DataGridView, true);
            RadDragAndDropManager.AddDropQueryHandler(this.DataGridView, this.OnGridViewDropQuery);
            RadDragAndDropManager.AddDropInfoHandler(this.DataGridView, this.OnGridViewDropInfo);

            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("RegionConfuguration"));
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            detailDataEditView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_RegionDealerList, null, this.DataGridView);
            this.LayoutRoot.Children.Add(detailDataEditView);

            var tabControl = new RadTabControl();
            tabControl.HorizontalAlignment = HorizontalAlignment.Left;
            tabControl.SetValue(Grid.ColumnProperty, 2);

            var tabItem = new RadTabItem();
            tabItem.Content = this.RegionAndDealerView;
            tabItem.Header = VehicleSalesUIStrings.DataEditView_GroupTitle_RegionAndDealerTree;

            tabControl.Items.Add(tabItem);
            this.LayoutRoot.Children.Add(tabControl);
            this.RegionAndDealerView.RefreshTreeViewAndClearGridView();
        }

        protected override void OnEditSubmitting() {
            var regionConfuguration = this.DataContext as RegionConfuguration;
            if(regionConfuguration == null)
                return;
            regionConfuguration.ValidationErrors.Clear();
            if(String.IsNullOrWhiteSpace(regionConfuguration.Code))
                regionConfuguration.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_RegionConfuguration_CodeIsNull, new[] {
                    "Code"
                }));
            if(String.IsNullOrWhiteSpace(regionConfuguration.Name))
                regionConfuguration.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_RegionConfuguration_NameIsNull, new[] {
                    "Name"
                }));
            if(!regionConfuguration.RegionDealerLists.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_RegionConfuguration_RegionDealerListIsNull);
                return;
            }
            if(regionConfuguration.RegionDealerLists.GroupBy(e => e.DealerId).Any(e => e.Count() > 1)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_RegionConfuguration_RegionDealerListIsUnique);
                return;
            }
            foreach(var regionDealerLists in regionConfuguration.RegionDealerLists)
                ((IEditableObject)regionDealerLists).EndEdit();
            if(regionConfuguration.HasValidationErrors)
                return;
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_RegionConfuguration;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetRegionConfugurationWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}