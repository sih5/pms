﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class RegionalRoleConfigDataEditView : INotifyPropertyChanged {
        private DataGridViewBase dataGridView;
        private ObservableCollection<RegionalRoleConfig> regionalRoleConfigs;
        private ObservableCollection<KeyValuePair> kvAuditHierarchySettings;
        private int auditHierarchySettingId;
        private string auditHierarchySettingName;

        public RegionalRoleConfigDataEditView() {
            InitializeComponent();
            this.Loaded += RegionalRoleConfigDataEditView_Loaded;
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_RegionalRoleConfig;
            }
        }

        public DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("RegionConfugurationForEdit");
                    this.dataGridView.DataContext = this;
                    this.dataGridView.DomainContext = this.DomainContext;
                }
                return this.dataGridView;
            }
        }

        public string AuditHierarchySettingName {
            get {
                return this.auditHierarchySettingName;
            }
            set {
                this.auditHierarchySettingName = value;
                this.OnPropertyChanged("AuditHierarchySettingName");
            }
        }

        public int AuditHierarchySettingId {
            get {
                return this.auditHierarchySettingId;
            }
            set {
                this.auditHierarchySettingId = value;
                this.OnPropertyChanged("AuditHierarchySettingId");
            }
        }

        public ObservableCollection<RegionalRoleConfig> RegionalRoleConfigs {
            get {
                return this.regionalRoleConfigs ?? (this.regionalRoleConfigs = new ObservableCollection<RegionalRoleConfig>());
            }
        }

        public ObservableCollection<KeyValuePair> KvAuditHierarchySettings {
            get {
                return this.kvAuditHierarchySettings ?? (this.kvAuditHierarchySettings = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            detailDataEditView.Register(VehicleSalesUIStrings.DetailDataEditView_HeadText_RegionConfuguration, null, this.DataGridView);
            this.Root.Children.Add(detailDataEditView);
        }

        private void RegionalRoleConfigDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            IsEnableCbb = true;
            this.RegionalRoleConfigs.Clear();
            this.DomainContext.Load(this.DomainContext.GetAuditHierarchySettingsQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                this.KvAuditHierarchySettings.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvAuditHierarchySettings.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.PositionName,
                        UserObject = entity
                    });
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!DataGridView.CommitEdit())
                return;

            foreach(var regionalRoleConfig in RegionalRoleConfigs)
                regionalRoleConfig.ValidationErrors.Clear();

            if(this.RegionalRoleConfigs.GroupBy(r => r.RegionConfugurationId).Any(r => r.Count() > 1)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_RegionConfuguration_RegionConfugurationIdUnique);
                return;
            }

            if(this.AuditHierarchySettingId == default(int)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_RegionConfuguration_AuditHierarchySettingIdIsNotNull);
                return;
            }

            foreach(var regional in this.RegionalRoleConfigs) {
                regional.AuditHierarchySettingId = this.AuditHierarchySettingId;
                ((IEditableObject)regional).EndEdit();
            }

            foreach(var item in this.DomainContext.RegionalRoleConfigs.Where(e => e.EntityState == EntityState.New).ToList()) {
                if(!RegionalRoleConfigs.Contains(item))
                    this.DomainContext.RegionalRoleConfigs.Remove(item);
            }

            if(this.RegionalRoleConfigs.Any(r => r.ValidationErrors.Any()))
                return;
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetRegionalRoleConfigWithDetailsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.RegionalRoleConfigs.Add(entity);
                    IsEnableCbb = false;
                    AuditHierarchySettingName = entity.AuditHierarchySetting.PositionName;
                    AuditHierarchySettingId = entity.AuditHierarchySetting.Id;
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        //控制ComBox可不可编辑
        private bool isEnableCbb = true;

        public bool IsEnableCbb {
            get {
                return this.isEnableCbb;
            }
            set {
                this.isEnableCbb = value;
                this.OnPropertyChanged("IsEnableCbb");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if(IsEnableCbb)
                this.RegionalRoleConfigs.Clear();
        }
    }
}