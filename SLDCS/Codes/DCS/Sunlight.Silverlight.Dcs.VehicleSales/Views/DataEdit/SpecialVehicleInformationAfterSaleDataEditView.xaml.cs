﻿using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class SpecialVehicleInformationAfterSaleDataEditView {
        public SpecialVehicleInformationAfterSaleDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return VehicleSalesUIStrings.BusinessName_RetainedCustomerVehicleList;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetRetainedCustomerVehicleListsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    entity.CityName = entity.VehicleInformation.TiledRegion.CityName;
                    entity.ProvinceName = entity.VehicleInformation.TiledRegion.ProvinceName;
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var retainedCustomerVehicleList = this.DataContext as RetainedCustomerVehicleList;
            if(retainedCustomerVehicleList == null)
                return;
            retainedCustomerVehicleList.ValidationErrors.Clear();
            if(retainedCustomerVehicleList.VehicleInformation == null || retainedCustomerVehicleList.VehicleInformation.SalesDealerName == default(string)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_SalesDealerNameIsNotNull);
                return;
            }

            if(retainedCustomerVehicleList.VehicleInformation == null || retainedCustomerVehicleList.VehicleInformation.VIN == default(string)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_VINIsNotNull);
                return;
            }

            if(retainedCustomerVehicleList.RetainedCustomer.Customer == null || retainedCustomerVehicleList.RetainedCustomer.Customer.Name == default(string)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_Customer_NameIsNotNull);
                return;
            }

            if(!retainedCustomerVehicleList.VehicleInformation.SellPrice.HasValue || retainedCustomerVehicleList.VehicleInformation.SellPrice.Value == default(int)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_SellPriceIsNotNull);
                return;
            }
            if(string.IsNullOrEmpty(retainedCustomerVehicleList.VehicleInformation.SalesInvoiceNumber)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_SalesInvoiceNumberIsNotNull);
                return;
            }

            if(retainedCustomerVehicleList.VehicleInformation.VehicleType == default(int)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_VehicleTypeIsNotNull);
                return;
            }

            var invoiceDate = retainedCustomerVehicleList.VehicleInformation.InvoiceDate.HasValue ? retainedCustomerVehicleList.VehicleInformation.InvoiceDate.Value.ToString("yyyy-MM-dd") : null;
            if(invoiceDate == null || Convert.ToDateTime(invoiceDate) < Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"))) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_InvoiceDateIsNotNull);
                return;
            }

            if(retainedCustomerVehicleList.VehicleInformation.ValidationErrors.Any())
                return;

            ((IEditableObject)retainedCustomerVehicleList).EndEdit();
            if(this.EditState == DataEditState.New) {
                if(retainedCustomerVehicleList.RetainedCustomer.Can生成特殊车辆保有客户信息) {
                    if(retainedCustomerVehicleList.VehicleInformation.SalesDealerId != null)
                        retainedCustomerVehicleList.RetainedCustomer.生成特殊车辆保有客户信息(retainedCustomerVehicleList.VehicleInformation.SalesDealerId.Value, retainedCustomerVehicleList.VehicleInformation.SalesDealerCode, retainedCustomerVehicleList.VehicleInformation.SalesDealerName, retainedCustomerVehicleList.VehicleInformation.LicensePlateRegionId.HasValue ? retainedCustomerVehicleList.VehicleInformation.LicensePlateRegionId.Value : 0, retainedCustomerVehicleList.VehicleId, retainedCustomerVehicleList.VehicleInformation.KeyCode, retainedCustomerVehicleList.VehicleInformation.InvoiceDate.Value, retainedCustomerVehicleList.VehicleInformation.SellPrice.Value, retainedCustomerVehicleList.VehicleInformation.VehicleLicensePlate, retainedCustomerVehicleList.VehicleInformation.Mileage.HasValue ? retainedCustomerVehicleList.VehicleInformation.Mileage.Value : 0, retainedCustomerVehicleList.VehicleInformation.SalesInvoiceNumber);
                }
            } else if(this.EditState == DataEditState.Edit) {
                if(retainedCustomerVehicleList.RetainedCustomer.Can更新特殊车辆保有客户信息) {
                    if(retainedCustomerVehicleList.VehicleInformation.SalesDealerId != null)
                        retainedCustomerVehicleList.RetainedCustomer.更新特殊车辆保有客户信息(retainedCustomerVehicleList.VehicleInformation.SalesDealerId.Value, retainedCustomerVehicleList.VehicleInformation.SalesDealerCode, retainedCustomerVehicleList.VehicleInformation.SalesDealerName, retainedCustomerVehicleList.VehicleInformation.LicensePlateRegionId.HasValue ? retainedCustomerVehicleList.VehicleInformation.LicensePlateRegionId.Value : 0, retainedCustomerVehicleList.VehicleInformation.Id, retainedCustomerVehicleList.VehicleInformation.KeyCode, retainedCustomerVehicleList.VehicleInformation.SellPrice.Value, retainedCustomerVehicleList.VehicleInformation.VehicleLicensePlate, retainedCustomerVehicleList.VehicleInformation.Mileage.HasValue ? retainedCustomerVehicleList.VehicleInformation.Mileage.Value : default(int), retainedCustomerVehicleList.VehicleInformation.SalesInvoiceNumber);
                }
            }
            base.OnEditSubmitting();
        }

        private void CreateUI() {
            var queryWindowyDealer = DI.GetQueryWindow("DealerForSpecialVehicle");
            queryWindowyDealer.Loaded += queryWindowyDealer_Loaded;
            queryWindowyDealer.SelectionDecided += this.QueryWindowyDealer_SelectionDecided;
            this.popupTextBoxSalesDealerName.PopupContent = queryWindowyDealer;

            var queryWindowerTiledRegion = DI.GetQueryWindow("TiledRegion");
            queryWindowerTiledRegion.Loaded += queryWindowerTiledRegion_Loaded;
            queryWindowerTiledRegion.SelectionDecided += this.QueryWindowerTiledRegion_SelectionDecided;
            this.popupTextBoxCityName.PopupContent = queryWindowerTiledRegion;

            var queryWindowCustomer = DI.GetQueryWindow("CustomerForSpecialVehicle");
            queryWindowCustomer.SelectionDecided += this.QueryWindowCustomer_SelectionDecided;
            this.popupTextBoxCustomerName.PopupContent = queryWindowCustomer;

            var queryWindowVehicleInformation = DI.GetQueryWindow("VehicleInformationForSelect");
            queryWindowVehicleInformation.SelectionDecided += this.QueryWindowVehicleInformation_SelectionDecided;
            this.popupTextBoxVIN.PopupContent = queryWindowVehicleInformation;
        }

        private void queryWindowyDealer_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var retainedCustomerVehicleList = this.DataContext as RetainedCustomerVehicleList;
            if(retainedCustomerVehicleList == null)
                return;
            if(retainedCustomerVehicleList.VehicleInformation == null) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notfiction_RetainedCustomerVehicleList);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
        }

        private void queryWindowerTiledRegion_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            var retainedCustomerVehicleList = this.DataContext as RetainedCustomerVehicleList;
            if(retainedCustomerVehicleList == null)
                return;
            if(retainedCustomerVehicleList.VehicleInformation == null) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notfiction_RetainedCustomerVehicleList);
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }
        }

        private void QueryWindowyDealer_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var selectItem = queryWindow.SelectedEntities.Cast<Dealer>().FirstOrDefault();
            if(selectItem == null)
                return;
            var retainedCustomerVehicleList = this.DataContext as RetainedCustomerVehicleList;
            if(retainedCustomerVehicleList == null)
                return;
            if(retainedCustomerVehicleList.VehicleInformation == null)
                return;
            retainedCustomerVehicleList.VehicleInformation.SalesDealerId = selectItem.Id;
            retainedCustomerVehicleList.VehicleInformation.SalesDealerCode = selectItem.Code;
            retainedCustomerVehicleList.VehicleInformation.SalesDealerName = selectItem.Name;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void QueryWindowerTiledRegion_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var selectItem = queryWindow.SelectedEntities.Cast<TiledRegion>().FirstOrDefault();
            if(selectItem == null)
                return;
            var retainedCustomerVehicleList = this.DataContext as RetainedCustomerVehicleList;
            if(retainedCustomerVehicleList == null)
                return;
            if(retainedCustomerVehicleList.VehicleInformation == null)
                return;
            retainedCustomerVehicleList.VehicleInformation.LicensePlateRegionId = selectItem.Id;
            retainedCustomerVehicleList.CityName = selectItem.CityName;
            retainedCustomerVehicleList.ProvinceName = selectItem.ProvinceName;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void QueryWindowCustomer_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var selectItem = queryWindow.SelectedEntities.Cast<Customer>().FirstOrDefault();
            if(selectItem == null)
                return;
            var retainedCustomerVehicleList = this.DataContext as RetainedCustomerVehicleList;
            this.DomainContext.Load(this.DomainContext.GetCustomersQuery().Where(c => c.Id == selectItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    if(retainedCustomerVehicleList == null)
                        return;
                    if(retainedCustomerVehicleList.RetainedCustomer == null)
                        return;
                    retainedCustomerVehicleList.RetainedCustomer.Customer = entity;
                    retainedCustomerVehicleList.RetainedCustomer.CustomerId = selectItem.Id;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }

        private void QueryWindowVehicleInformation_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var selectItem = queryWindow.SelectedEntities.Cast<VehicleInformation>().FirstOrDefault();
            if(selectItem == null)
                return;
            var retainedCustomerVehicleList = this.DataContext as RetainedCustomerVehicleList;
            if(retainedCustomerVehicleList == null)
                return;

            this.DomainContext.Load(this.DomainContext.GetVehicleInformationsQuery().Where(c => c.Id == selectItem.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    retainedCustomerVehicleList.VehicleInformation = entity;
                    retainedCustomerVehicleList.VehicleInformation.SellPrice = default(int);
                    retainedCustomerVehicleList.VehicleId = entity.Id;
                    //默认值是当前系统时间，如果为空，就赋值为当前时间
                    retainedCustomerVehicleList.VehicleInformation.InvoiceDate = retainedCustomerVehicleList.VehicleInformation.InvoiceDate.HasValue ? retainedCustomerVehicleList.VehicleInformation.InvoiceDate.Value : DateTime.Now;
                }
                var parent = queryWindow.ParentOfType<RadWindow>();
                if(parent != null)
                    parent.Close();
            }, null);
        }
    }
}
