﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;


namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class SpecialVehicleInformationForImportDataEditView {
        public SpecialVehicleInformationForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private ICommand exportFileCommand;
        private DataGridViewBase specialVehicleInformationTemplateDataGridView;

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = VehicleSalesUIStrings.DataGridView_Title_SpecialVehicleInformation,//DcsUIStrings.BusinessName_VehicleInformation,
                Content = this.SpecialVehicleInformationTemplateDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehiclePaymentBill;
            }
        }

        private DataGridViewBase SpecialVehicleInformationTemplateDataGridView {
            get {
                return this.specialVehicleInformationTemplateDataGridView ?? (specialVehicleInformationTemplateDataGridView = DI.GetDataGridView("SpecialVehicleInformationTemplate"));
            }
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            this.DomainContext.导入特殊车辆信息(fileName, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                }

                this.ImportComplete();
                this.DomainContext.IsInvoking = false;
                if(string.IsNullOrEmpty(loadOp.Value) || string.IsNullOrWhiteSpace(loadOp.Value)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImport);
                } else {
                    this.ExportFile(loadOp.Value);
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImportWithError);
                }
            }, null);
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    InitialExportCommand();
                return this.exportFileCommand;
            }
        }

        private void InitialExportCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => ((DcsDataGridViewBase)this.SpecialVehicleInformationTemplateDataGridView).ExportData());
        }
    }
}
