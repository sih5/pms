﻿﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
﻿using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
﻿using Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid;
﻿using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
﻿using Telerik.Windows.Controls;
﻿using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class SpecialVehicleInformationForTradeDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private ICommand searchVehicleInformationCommand;
        private string originalVin, newVin;

        private ObservableCollection<VehicleInformation> vehicleInformations;
        public ObservableCollection<VehicleInformation> VehicleInformations {
            get {
                return this.vehicleInformations ?? (this.vehicleInformations = new ObservableCollection<VehicleInformation>());
            }
        }

        private ObservableCollection<VehicleAvailableResource> vehicleAvailableResources;
        public ObservableCollection<VehicleAvailableResource> VehicleAvailableResources {
            get {
                return this.vehicleAvailableResources ?? (this.vehicleAvailableResources = new ObservableCollection<VehicleAvailableResource>());
            }
        }

        private ObservableCollection<SpecialVehicleChangeRecord> specialVehicleChangeRecord;
        public ObservableCollection<SpecialVehicleChangeRecord> SpecialVehicleChangeRecords {
            get {
                return this.specialVehicleChangeRecord ?? (this.specialVehicleChangeRecord = new ObservableCollection<SpecialVehicleChangeRecord>());
            }
        }

        private SpecialVehicleInformationForAdjustmentDataGridView dataGridView;

        private SpecialVehicleInformationForAdjustmentDataGridView DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = new SpecialVehicleInformationForAdjustmentDataGridView();
                    this.dataGridView.DomainContext = this.DomainContext;
                    this.dataGridView.SetValue(DataContextProperty, this);
                }
                return this.dataGridView;
            }
        }

        private DataGridViewBase specialVehicleInformationForTrade;
        private DataGridViewBase SpecialVehicleInformationForTradeDataGridView {
            get {
                if(this.specialVehicleInformationForTrade == null) {
                    this.specialVehicleInformationForTrade = DI.GetDataGridView("SpecialVehicleInformationForTrade");
                    this.specialVehicleInformationForTrade.DomainContext = this.DomainContext;
                    this.specialVehicleInformationForTrade.SetValue(DataContextProperty, this);
                    this.specialVehicleInformationForTrade.SelectionChanged += this.SpecialVehicleInformationForTradeDataGridView_SelectionChanged;
                }
                return this.specialVehicleInformationForTrade;
            }
        }

        private void SpecialVehicleInformationForTradeDataGridView_SelectionChanged(object sender, EventArgs e) {
            var vehicleInformation = this.SpecialVehicleInformationForTradeDataGridView.SelectedEntities.Cast<VehicleInformation>().FirstOrDefault();
            if(vehicleInformation == null)
                return;
            this.VehicleAvailableResources.Clear();
            this.DataGridClear();
            this.DataGridView.SpecialVehicleChangeRecord.OriginalVIN = this.OriginalVIN;
            this.DataGridView.SpecialVehicleChangeRecord.ProductCode = vehicleInformation.ProductCode;
            this.DataGridView.SpecialVehicleChangeRecord.ProductCategoryCode = vehicleInformation.ProductCategoryCode;
            this.DataGridView.SpecialVehicleChangeRecord.OriginalVehicleId = vehicleInformation.Id;
            if(string.IsNullOrWhiteSpace(this.OriginalVIN)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_OriginalVinIsNull);
                return;
            }
            if(string.IsNullOrWhiteSpace(vehicleInformation.ProductCode)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_ProductCodeIsNull);
                return;
            }
            this.DomainContext.Load(this.DomainContext.查询可用资源调整数据源Query(this.NewVIN, vehicleInformation.ProductCode, this.OriginalVIN), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                var entities = loadOp.Entities;
                if(entities != null) {
                    foreach(var entity in entities) {
                        this.VehicleAvailableResources.Add(entity);
                    }
                }
            }, null);
        }

        private DataGridViewBase vehicleAvailableResourceForEditDataGridView;

        private DataGridViewBase VehicleAvailableResourceForEditDataGridView {
            get {
                if(this.vehicleAvailableResourceForEditDataGridView == null) {
                    this.vehicleAvailableResourceForEditDataGridView = DI.GetDataGridView("VehicleAvailableResourceForEdit");
                    this.vehicleAvailableResourceForEditDataGridView.DomainContext = this.DomainContext;
                    this.vehicleAvailableResourceForEditDataGridView.SetValue(DataContextProperty, this);
                    this.vehicleAvailableResourceForEditDataGridView.SelectionChanged += this.VehicleAvailableResourceForEditDataGridView_SelectionChanged;
                }
                return this.vehicleAvailableResourceForEditDataGridView;
            }
        }

        private void VehicleAvailableResourceForEditDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            this.DataGridClear();
            var vehicleAvailableResource = this.VehicleAvailableResourceForEditDataGridView.SelectedEntities.Cast<VehicleAvailableResource>().FirstOrDefault();
            if(vehicleAvailableResource == null)
                return;
            this.DataGridView.SpecialVehicleChangeRecord.NewVIN = vehicleAvailableResource.VIN;
            this.DataGridView.SpecialVehicleChangeRecord.NewVehicleId = vehicleAvailableResource.VehicleId;
        }

        private void CreateUI() {
            var specialVehicleInformationForTradeDataGridView = new DcsDetailDataEditView();
            specialVehicleInformationForTradeDataGridView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleInformation, null, () => this.SpecialVehicleInformationForTradeDataGridView);
            specialVehicleInformationForTradeDataGridView.SetValue(Grid.RowProperty, 1);
            specialVehicleInformationForTradeDataGridView.UnregisterButton(specialVehicleInformationForTradeDataGridView.InsertButton);
            specialVehicleInformationForTradeDataGridView.UnregisterButton(specialVehicleInformationForTradeDataGridView.DeleteButton);

            var specialVehicleInformationForAdjustmentDataGridView = new DcsDetailDataEditView();
            specialVehicleInformationForAdjustmentDataGridView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_SpecialVehicleChangeRecord, null, () => this.DataGridView);
            specialVehicleInformationForAdjustmentDataGridView.SetValue(Grid.RowProperty, 2);

            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Header = VehicleSalesUIStrings.DetailPanel_Title_VehicleAvailableResource,  
                Content = this.VehicleAvailableResourceForEditDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            tabControl.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(tabControl);
            this.LayoutRoot.Children.Add(specialVehicleInformationForTradeDataGridView);
            this.LayoutRoot.Children.Add(specialVehicleInformationForAdjustmentDataGridView);
        }

        private void InitializeCommand() {
            this.searchVehicleInformationCommand = new DelegateCommand(() => {
                if(string.IsNullOrWhiteSpace(this.OriginalVIN)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_OriginalVinIsNull);
                    return;
                }
                this.Clear();
                this.DomainContext.Load(this.DomainContext.根据VIN查询特殊车辆信息Query(this.OriginalVIN), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    if(loadOp.Entities == null)
                        return;
                    var entity = loadOp.Entities.ToArray();
                    foreach(var item in entity) {
                        this.VehicleInformations.Add(item);
                    }
                }, null);
            });
        }

        protected override void OnEditSubmitting() {
            var specialVehicle = new SpecialVehicleChangeRecord();
            this.DomainContext.SpecialVehicleChangeRecords.Add(specialVehicle);
            specialVehicle.OriginalVIN = this.DataGridView.SpecialVehicleChangeRecord.OriginalVIN;
            specialVehicle.OriginalVehicleId = this.DataGridView.SpecialVehicleChangeRecord.OriginalVehicleId;
            specialVehicle.NewVIN = this.DataGridView.SpecialVehicleChangeRecord.NewVIN;
            specialVehicle.NewVehicleId = this.DataGridView.SpecialVehicleChangeRecord.NewVehicleId;
            specialVehicle.ValidationErrors.Clear();
            if(!this.SpecialVehicleInformationForTradeDataGridView.CommitEdit())
                return;
            if(!this.VehicleAvailableResourceForEditDataGridView.CommitEdit())
                return;
            if(!this.DataGridView.CommitEdit())
                return;
            ((IEditableObject)specialVehicle).EndEdit();
            if(this.SpecialVehicleChangeRecords.Count > 0) {
                try {
                    if(specialVehicle.Can特殊车辆换车)
                        specialVehicle.特殊车辆换车();
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            } else {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_SpecialVehicleChangeRecord_SpecialVehicleChangeRecordIsNull);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditCancelled() {
            this.Clear();
            this.DataGridClear();
            base.OnEditCancelled();
        }

        //清除特殊车辆调整结果信息
        private void DataGridClear() {
            this.SpecialVehicleChangeRecords.Clear();
        }

        private void Clear() {
            this.VehicleInformations.Clear();
            this.VehicleAvailableResources.Clear();
            this.DataGridClear();
        }

        public ICommand SearchVehicleInformationCommand {
            get {
                if(this.searchVehicleInformationCommand == null)
                    InitializeCommand();
                return this.searchVehicleInformationCommand;
            }
        }

        public string OriginalVIN {
            get {
                return this.originalVin;
            }
            set {
                this.originalVin = value;
                this.OnPropertyChanged("OriginalVIN");
            }
        }

        public string NewVIN {
            get {
                return this.newVin;
            }
            set {
                this.newVin = value;
                this.OnPropertyChanged("NewVIN");
            }
        }

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public SpecialVehicleInformationForTradeDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
    }
}