﻿
using System;
using System.ComponentModel;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VINChangeRecordDataEditView {
        public VINChangeRecordDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_Edit_ChangeRecord;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("VINChangeRecord"));
        }

        protected override void OnEditSubmitting() {
            var vInChangeRecord = this.DataContext as VINChangeRecord;
            if(vInChangeRecord == null)
                return;
            vInChangeRecord.ValidationErrors.Clear();

            if(vInChangeRecord.ValidationErrors.Any())
                return;
            ((IEditableObject)vInChangeRecord).EndEdit();
            try {
                if(vInChangeRecord.Can调整发动机号或者下线日期)
                    vInChangeRecord.调整发动机号或者下线日期();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }

            base.OnEditSubmitting();
        }
    }
}
