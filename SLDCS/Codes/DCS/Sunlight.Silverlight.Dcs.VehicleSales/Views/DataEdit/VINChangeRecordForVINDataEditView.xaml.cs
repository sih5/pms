﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VINChangeRecordForVINDataEditView {
        public VINChangeRecordForVINDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_Edit_VINChangeRecord;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("VINChangeRecordForVIN"));
        }

        protected override void OnEditSubmitting() {
            var vInChangeRecord = this.DataContext as VINChangeRecord;
            if(vInChangeRecord == null)
                return;
            vInChangeRecord.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(vInChangeRecord.NewVIN))
                vInChangeRecord.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VINChangeRecord_NewVINIsNull, new[] {
                    "NewVIN"
                }));
            if(vInChangeRecord.ValidationErrors.Any())
                return;
            ((IEditableObject)vInChangeRecord).EndEdit();
            try {
                if(vInChangeRecord.Can调整VIN码)
                    vInChangeRecord.调整VIN码();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }

            base.OnEditSubmitting();
        }
    }
}
