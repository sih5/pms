﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows;
using Telerik.Windows.Controls;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehPifaPriceChangeAppDataEditView {
        private DataGridViewBase vehPifaPriceChangeAppDetailForEditDataGridView;
        private ProductCategoryDataTreeView productCategoryDataTreeView;
        private ObservableCollection<Product> products;
        private DataGridViewBase productForVehicleWholesalePactPriceDataGridView;
        private ObservableCollection<KeyValuePair> kvVehicleSalesTypes;

        public VehPifaPriceChangeAppDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehPifaPriceChangeApp;
            }
        }

        private DataGridViewBase VehPifaPriceChangeAppDetailForEditDataGridView {
            get {
                if(vehPifaPriceChangeAppDetailForEditDataGridView == null) {
                    this.vehPifaPriceChangeAppDetailForEditDataGridView = DI.GetDataGridView("VehPifaPriceChangeAppDetailForEdit");
                    this.vehPifaPriceChangeAppDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehPifaPriceChangeAppDetailForEditDataGridView;
            }
        }

        public ObservableCollection<KeyValuePair> KvVehicleSalesTypes {
            get {
                return this.kvVehicleSalesTypes ?? (this.kvVehicleSalesTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<Product> Products {
            get {
                return products ?? (this.products = new ObservableCollection<Product>());
            }
            set {
                this.products = value;
            }
        }

        public DataGridViewBase ProductForVehicleWholesalePactPriceDataGridView {
            get {
                if(this.productForVehicleWholesalePactPriceDataGridView == null) {
                    this.productForVehicleWholesalePactPriceDataGridView = DI.GetDataGridView("ProductForVehicleWholesalePactPrice");
                    this.productForVehicleWholesalePactPriceDataGridView.DataContext = this;
                    this.productForVehicleWholesalePactPriceDataGridView.RowDoubleClick += productForVehicleWholesalePactPriceDataGridView_RowDoubleClick;
                }
                return this.productForVehicleWholesalePactPriceDataGridView;
            }
        }

        private void productForVehicleWholesalePactPriceDataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            var vehPifaPriceChangeApp = this.DataContext as VehPifaPriceChangeApp;
            var selectedEntities = this.ProductForVehicleWholesalePactPriceDataGridView.SelectedEntities;
            if(vehPifaPriceChangeApp == null || selectedEntities == null || !selectedEntities.Any())
                return;

            var product = this.ProductForVehicleWholesalePactPriceDataGridView.SelectedEntities.Cast<Product>().First();
            if(product == null)
                return;
            if(vehPifaPriceChangeApp.VehPifaPriceChangeAppDetails.Any(r => r.ProductId == product.Id))
                return;
            vehPifaPriceChangeApp.VehPifaPriceChangeAppDetails.Add(new VehPifaPriceChangeAppDetail {
                VehPifaPriceChangeApp = vehPifaPriceChangeApp,
                PriceBeforeChange = product.CurrentPrice,
                ProductCode = product.Code,
                ProductId = product.Id,
                ProductName = product.Name
            });
        }

        private ProductCategoryDataTreeView ProductCategoryDataTreeView {
            get {
                if(this.productCategoryDataTreeView == null) {
                    this.productCategoryDataTreeView = new ProductCategoryDataTreeView();
                    this.productCategoryDataTreeView.DomainContext = new DcsDomainContext();
                    this.productCategoryDataTreeView.OnTreeViewItemClick += ProductCategoryDataTreeView_OnTreeViewItemClick;
                }
                return this.productCategoryDataTreeView;
            }
        }

        private void ProductCategoryDataTreeView_OnTreeViewItemClick(object sender, RadRoutedEventArgs radRoutedEventArgs) {
            this.Products.Clear();
            var selectedItem = ((RadTreeView)sender).SelectedItem as RadTreeViewItem;
            if(selectedItem == null || !(selectedItem.Tag is ProductCategory) || selectedItem.Items.Count > 0)
                return;
            var productCategory = selectedItem.Tag as ProductCategory;
            var productAffiProductCategories = productCategory.ProductAffiProductCategories;

            foreach(ProductAffiProductCategory productAffiProductCategory in productAffiProductCategories)
                Products.Add(productAffiProductCategory.Product);
        }

        private void CreateUI() {
            var detailEdtiView = new DcsDetailDataEditView();
            detailEdtiView.Register(Utils.GetEntityLocalizedName(typeof(VehPifaPriceChangeApp), "VehPifaPriceChangeAppDetails"), null, () => this.VehPifaPriceChangeAppDetailForEditDataGridView);
            detailEdtiView.SetValue(Grid.RowProperty, 1);
            this.SubRoot.Children.Add(detailEdtiView);

            this.ProductCategoryDataTreeView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(this.ProductCategoryDataTreeView);

            this.ProductForVehicleWholesalePactPriceDataGridView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(this.ProductForVehicleWholesalePactPriceDataGridView);

            this.Loaded += VehPifaPriceChangeAppDataEditView_Loaded;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e) {
            var vehPifaPriceChangeApp = this.DataContext as VehPifaPriceChangeApp;
            if(vehPifaPriceChangeApp == null)
                return;
            vehPifaPriceChangeApp.ExecutionDate = this.radioButtonFirst.IsChecked.HasValue && this.radioButtonFirst.IsChecked.Value ? DateTime.Now : vehPifaPriceChangeApp.ExecutionDate;
            if(this.radioButtonFirst.IsChecked == true)
                vehPifaPriceChangeApp.ExecutedImmediately = true;
            else {
                vehPifaPriceChangeApp.ExecutedImmediately = false;
            }
        }

        private void VehPifaPriceChangeAppDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetVehicleSalesTypesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvVehicleSalesTypes.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvVehicleSalesTypes.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.VehicleSalesTypeName,
                        UserObject = entity
                    });
                }
                this.Products.Clear();
                //此代码不能写在Loaded方法的首行，否则会出现，编辑时，下拉列表销售类型集合无数据
                var vehPifaPriceChangeApp = this.DataContext as VehPifaPriceChangeApp;
                if(vehPifaPriceChangeApp == null)
                    return;

                if(loadOp.Entities == null || !loadOp.Entities.Any() || vehPifaPriceChangeApp.EntityState != EntityState.New)
                    return;
                vehPifaPriceChangeApp.VehicleSalesTypeId = loadOp.Entities.First().Id;
                vehPifaPriceChangeApp.VehicleSalesTypeName = loadOp.Entities.First().VehicleSalesTypeName;
                vehPifaPriceChangeApp.VehicleSalesTypeCode = loadOp.Entities.First().VehicleSalesTypeCode;
            }, null);
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            if(comboBox == null)
                return;
            var vehPifaPriceChangeApp = this.DataContext as VehPifaPriceChangeApp;
            if(vehPifaPriceChangeApp == null)
                return;

            if(e.AddedItems == null || e.AddedItems.Count < 1)
                return;
            var keyValuePair = e.AddedItems[0] as KeyValuePair;
            if(keyValuePair == null || (keyValuePair.UserObject as VehicleSalesType) == null)
                return;
            var vehicleSalesType = (VehicleSalesType)keyValuePair.UserObject;
            vehPifaPriceChangeApp.VehicleSalesTypeCode = vehicleSalesType.VehicleSalesTypeCode;

            this.ProductCategoryDataTreeView.EntityQuery = this.productCategoryDataTreeView.DomainContext.查询产品分类以及批发价格Query(vehPifaPriceChangeApp.VehicleSalesTypeId);
            this.productCategoryDataTreeView.RefreshDataTree();
            this.Products.Clear();
            if(!vehPifaPriceChangeApp.VehPifaPriceChangeAppDetails.Any())
                return;
            if(e.RemovedItems != null && e.RemovedItems.Count == 1) {
                DcsUtils.Confirm(VehicleSalesUIStrings.DataEditView_Confirm_ClearHaveAlreadySelectedPrices, () => {
                    foreach(var detailEntity in vehPifaPriceChangeApp.VehPifaPriceChangeAppDetails.Where(r => r.EntityState == EntityState.New))
                        vehPifaPriceChangeApp.VehPifaPriceChangeAppDetails.Remove(detailEntity);
                }, () => {
                    comboBox.SelectionChanged -= this.ComboBox_SelectionChanged;
                    comboBox.SelectedItem = e.RemovedItems[0];
                    this.ProductCategoryDataTreeView.EntityQuery = this.productCategoryDataTreeView.DomainContext.查询产品分类以及批发价格Query(((VehicleSalesType)(((KeyValuePair)e.RemovedItems[0]).UserObject)).Id);
                    this.productCategoryDataTreeView.RefreshDataTree();
                    comboBox.SelectionChanged += this.ComboBox_SelectionChanged;
                });
            }

        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehPifaPriceChangeAppWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var vehPifaPriceChangeApp = this.DataContext as VehPifaPriceChangeApp;
            if(vehPifaPriceChangeApp == null || !this.VehPifaPriceChangeAppDetailForEditDataGridView.CommitEdit())
                return;
            vehPifaPriceChangeApp.ValidationErrors.Clear();
            foreach(var detail in vehPifaPriceChangeApp.VehPifaPriceChangeAppDetails.Where(r => r.HasValidationErrors))
                detail.ValidationErrors.Clear();

            if(vehPifaPriceChangeApp.VehPifaPriceChangeAppDetails.Any(r => r.PriceAfterChange <= 0)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehPifaPriceChangeAppDetail_PriceAfterChangeLessThanZero);
                return;
            }
            ((IEditableObject)(vehPifaPriceChangeApp)).EndEdit();
            if(vehPifaPriceChangeApp.ValidationErrors.Any())
                return;
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}