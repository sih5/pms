﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehRetailPriceChangeAppDataEditView {
        private ObservableCollection<Product> products;
        private DataGridViewBase dataGridView;
        private DataGridViewBase productForVehicleWholesalePactPriceDataGridView;
        private ProductCategoryDataTreeView productCategoryDataTreeView;

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("VehRetailPriceChangeAppDetailForEdit");
                    this.dataGridView.DomainContext = this.DomainContext;
                }
                return this.dataGridView;
            }
        }

        public DataGridViewBase ProductForVehicleWholesalePactPriceDataGridView {
            get {
                if(this.productForVehicleWholesalePactPriceDataGridView == null) {
                    this.productForVehicleWholesalePactPriceDataGridView = DI.GetDataGridView("ProductForVehicleWholesalePactPrice");
                    this.productForVehicleWholesalePactPriceDataGridView.DataContext = this;
                    this.productForVehicleWholesalePactPriceDataGridView.RowDoubleClick += productForVehicleWholesalePactPriceDataGridView_RowDoubleClick;
                }
                return this.productForVehicleWholesalePactPriceDataGridView;
            }
        }

        private void productForVehicleWholesalePactPriceDataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            var vehRetailPriceChangeApp = this.DataContext as VehRetailPriceChangeApp;
            var selectedEntities = this.ProductForVehicleWholesalePactPriceDataGridView.SelectedEntities;
            if(vehRetailPriceChangeApp == null || selectedEntities == null || !selectedEntities.Any())
                return;

            var product = this.ProductForVehicleWholesalePactPriceDataGridView.SelectedEntities.Cast<Product>().First();
            if(product == null || vehRetailPriceChangeApp.VehRetailPriceChangeAppDetails.Any(r => r.ProductCode == product.Code))
                return;

            //ProductPrice是服务端虚拟的扩展属性，返回的是整车零售价格.价格
            var vehRetailPriceChangeAppDetail = new VehRetailPriceChangeAppDetail {
                PriceBeforeChange = product.ProductPrice,
                ProductCode = product.Code,
                ProductId = product.Id,
                ProductName = product.Name
            };
            vehRetailPriceChangeAppDetail.PropertyChanged += this.VehRetailPriceChangeAppDetail_PropertyChanged;
            vehRetailPriceChangeApp.VehRetailPriceChangeAppDetails.Add(vehRetailPriceChangeAppDetail);
        }

        private ProductCategoryDataTreeView ProductCategoryDataTreeView {
            get {
                if(this.productCategoryDataTreeView == null) {
                    this.productCategoryDataTreeView = new ProductCategoryDataTreeView();
                    this.productCategoryDataTreeView.DomainContext = new DcsDomainContext();
                    this.productCategoryDataTreeView.EntityQuery = this.productCategoryDataTreeView.DomainContext.查询产品分类以及零售价格Query();
                    this.productCategoryDataTreeView.OnTreeViewItemClick += this.ProductCategoryDataTreeView_OnTreeViewItemClick;
                }
                return this.productCategoryDataTreeView;
            }
        }

        private void ProductCategoryDataTreeView_OnTreeViewItemClick(object sender, RadRoutedEventArgs radRoutedEventArgs) {
            this.Products.Clear();
            var selectedItem = ((RadTreeView)sender).SelectedItem as RadTreeViewItem;
            if(selectedItem == null || !(selectedItem.Tag is ProductCategory) || selectedItem.Items.Count > 0)
                return;
            var productCategory = selectedItem.Tag as ProductCategory;
            var productAffiProductCategories = productCategory.ProductAffiProductCategories;

            foreach(ProductAffiProductCategory productAffiProductCategory in productAffiProductCategories)
                Products.Add(productAffiProductCategory.Product);
        }

        public VehRetailPriceChangeAppDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.VehRetailPriceChangeAppDataEditView_DataContextChanged;
        }

        private void VehRetailPriceChangeAppDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehRetailPriceChangeApp = this.DataContext as VehRetailPriceChangeApp;
            if(vehRetailPriceChangeApp == null)
                return;
            vehRetailPriceChangeApp.PropertyChanged += this.VehRetailPriceChangeApp_PropertyChanged;
            if(vehRetailPriceChangeApp.VehRetailPriceChangeAppDetails.Any()) {
                foreach(var vehRetailPriceChangeAppDetail in vehRetailPriceChangeApp.VehRetailPriceChangeAppDetails) {
                    vehRetailPriceChangeAppDetail.PropertyChanged += this.VehRetailPriceChangeAppDetail_PropertyChanged;
                }
            }
        }

        private void VehRetailPriceChangeApp_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var vehRetailPriceChangeApp = sender as VehRetailPriceChangeApp;
            if(vehRetailPriceChangeApp == null)
                return;
            switch(e.PropertyName) {
                case "ExecutedImmediately":
                    if(vehRetailPriceChangeApp.ExecutedImmediately)
                        vehRetailPriceChangeApp.ExecutionDate = DateTime.Now;
                    break;
            }
        }

        private void VehRetailPriceChangeAppDetail_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            var vehRetailPriceChangeAppDetail = sender as VehRetailPriceChangeAppDetail;
            if(vehRetailPriceChangeAppDetail == null)
                return;
            switch(e.PropertyName) {
                case "PriceAfterChange":
                    if(vehRetailPriceChangeAppDetail.PriceBeforeChange.HasValue)
                        vehRetailPriceChangeAppDetail.Difference = (decimal)(vehRetailPriceChangeAppDetail.PriceBeforeChange - vehRetailPriceChangeAppDetail.PriceAfterChange);
                    else
                        vehRetailPriceChangeAppDetail.Difference = vehRetailPriceChangeAppDetail.PriceAfterChange;
                    break;
            }
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_VehRetailPriceChangeAppDetail, null, this.DataGridView);
            detailDataEditView.SetValue(Grid.RowProperty, 1);
            this.ProductCategoryDataTreeView.SetValue(Grid.ColumnProperty, 1);
            this.ProductCategoryDataTreeView.SetValue(Grid.RowSpanProperty, 2);

            this.ProductForVehicleWholesalePactPriceDataGridView.SetValue(Grid.ColumnProperty, 2);
            this.ProductForVehicleWholesalePactPriceDataGridView.SetValue(Grid.RowSpanProperty, 2);

            this.LayoutRoot.Children.Add(this.ProductForVehicleWholesalePactPriceDataGridView);
            this.LayoutRoot.Children.Add(this.ProductCategoryDataTreeView);
            this.LayoutRoot.Children.Add(detailDataEditView);
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("VehRetailPriceChangeApp"));
            this.Loaded += VehRetailPriceChangeAppDataEditView_Loaded;
        }

        private void VehRetailPriceChangeAppDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.ProductCategoryDataTreeView.RefreshDataTree();
            this.Products.Clear();
        }

        protected override void OnEditSubmitting() {
            var vehRetailPriceChangeApp = this.DataContext as VehRetailPriceChangeApp;
            if(vehRetailPriceChangeApp == null || !this.DataGridView.CommitEdit())
                return;

            if(!vehRetailPriceChangeApp.VehRetailPriceChangeAppDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehRetailPriceChangeApp_VehRetailPriceChangeAppIsNull);
                return;
            }
            if(vehRetailPriceChangeApp.VehRetailPriceChangeAppDetails.Any(entity => entity.PriceAfterChange <= 0)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehRetailPriceChangeAppDetail_PriceAfterChangeLessThenZero);
                return;
            }
            if(vehRetailPriceChangeApp.ExecutedImmediately)
                vehRetailPriceChangeApp.ExecutionDate = DateTime.Now;
            this.Products.Clear();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehRetailPriceChangeAppWidthDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    if(entity.VehRetailPriceChangeAppDetails.Any()) {
                        foreach(var vehRetailPriceChangeAppDetail in entity.VehRetailPriceChangeAppDetails) {
                            if(vehRetailPriceChangeAppDetail.PriceBeforeChange.HasValue)
                                vehRetailPriceChangeAppDetail.Difference = (decimal)vehRetailPriceChangeAppDetail.PriceBeforeChange - vehRetailPriceChangeAppDetail.PriceAfterChange;
                            else {
                                vehRetailPriceChangeAppDetail.Difference = vehRetailPriceChangeAppDetail.PriceAfterChange;
                            }
                            vehRetailPriceChangeAppDetail.PropertyChanged -= this.VehRetailPriceChangeAppDetail_PropertyChanged;
                            vehRetailPriceChangeAppDetail.PropertyChanged += this.VehRetailPriceChangeAppDetail_PropertyChanged;
                        }
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehRetailPriceChangeApp;
            }
        }

        public ObservableCollection<Product> Products {
            get {
                return products ?? (this.products = new ObservableCollection<Product>());
            }
            set {
                this.products = value;
            }
        }
    }
}
