﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Command;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehShipplanApprovalChangeRecDataEditView : INotifyPropertyChanged {
        private readonly string[] kvNames = {
            "VehShipplanApprovalChangeRec_AdjustmentType"
        };

        private string code, originalVin, newVin;
        private int adjustmentType;
        private ICommand searchVehicleShipplanApprovalDetailCommand;
        private KeyValueManager keyValueManager = new KeyValueManager();
        private VehShipplanApprovalChangeRecForChangeRecEditDataGridView dataGridView;

        private VehShipplanApprovalChangeRecForChangeRecEditDataGridView DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = new VehShipplanApprovalChangeRecForChangeRecEditDataGridView();
                    this.dataGridView.DomainContext = this.DomainContext;
                    this.dataGridView.SetValue(DataContextProperty, this);
                }
                return this.dataGridView;
            }
        }

        private ObservableCollection<VehicleShipplanApprovalDetail> vehicleShipplanApprovalDetails;
        private ObservableCollection<VehicleShipplanApprovalDetail> vehicleShipplanApprovalDetailsForAdjustment;
        private ObservableCollection<VehShipplanApprovalChangeRec> vehShipplanApprovalChangeRecs;
        private ObservableCollection<VehicleAvailableResource> vehicleAvailableResources;

        public ObservableCollection<VehicleShipplanApprovalDetail> VehicleShipplanApprovalDetails {
            get {
                return this.vehicleShipplanApprovalDetails ?? (this.vehicleShipplanApprovalDetails = new ObservableCollection<VehicleShipplanApprovalDetail>());
            }
        }

        public ObservableCollection<VehicleShipplanApprovalDetail> VehicleShipplanApprovalDetailsForAdjustment {
            get {
                return this.vehicleShipplanApprovalDetailsForAdjustment ?? (this.vehicleShipplanApprovalDetailsForAdjustment = new ObservableCollection<VehicleShipplanApprovalDetail>());
            }
        }

        public ObservableCollection<VehShipplanApprovalChangeRec> VehShipplanApprovalChangeRecs {
            get {
                return this.vehShipplanApprovalChangeRecs ?? (this.vehShipplanApprovalChangeRecs = new ObservableCollection<VehShipplanApprovalChangeRec>());
            }
        }

        public ObservableCollection<VehicleAvailableResource> VehicleAvailableResources {
            get {
                return this.vehicleAvailableResources ?? (this.vehicleAvailableResources = new ObservableCollection<VehicleAvailableResource>());
            }
        }

        private DataGridViewBase vehicleShipplanApprovalDetailForChangeRecDataGridView;

        private DataGridViewBase VehicleShipplanApprovalDetailForChangeRecDataGridView {
            get {
                if(this.vehicleShipplanApprovalDetailForChangeRecDataGridView == null) {
                    this.vehicleShipplanApprovalDetailForChangeRecDataGridView = DI.GetDataGridView("VehicleShipplanApprovalDetailForChangeRec");
                    this.vehicleShipplanApprovalDetailForChangeRecDataGridView.DomainContext = this.DomainContext;
                    this.vehicleShipplanApprovalDetailForChangeRecDataGridView.SetValue(DataContextProperty, this);
                    this.vehicleShipplanApprovalDetailForChangeRecDataGridView.SelectionChanged += this.VehicleShipplanApprovalDetailForChangeRecDataGridView_SelectionChanged;
                }
                return this.vehicleShipplanApprovalDetailForChangeRecDataGridView;
            }
        }

        private void VehicleShipplanApprovalDetailForChangeRecDataGridView_SelectionChanged(object sender, System.EventArgs e) {

            this.NewShipplanApprovalClear();
            var vehicleShipplanApprovalDetail = this.VehicleShipplanApprovalDetailForChangeRecDataGridView.SelectedEntities.Cast<VehicleShipplanApprovalDetail>().FirstOrDefault();
            if(vehicleShipplanApprovalDetail == null)
                return;
            this.DataGridView.VehShipplanApprovalChangeRec.OriginalShipplanApprovalCode = vehicleShipplanApprovalDetail.VehicleShipplanApproval.Code;
            this.DataGridView.VehShipplanApprovalChangeRec.OriginalVIN = vehicleShipplanApprovalDetail.VIN;
            this.DataGridView.VehShipplanApprovalChangeRec.ProductCode = vehicleShipplanApprovalDetail.ProductCode;
            this.DataGridView.VehShipplanApprovalChangeRec.ProductCategoryName = vehicleShipplanApprovalDetail.ProductCategoryName;
            switch(this.AdjustmentType) {
                case (int)DcsVehShipplanApprovalChangeRecAdjustmentType.可用资源调整:
                    this.DomainContext.Load(this.DomainContext.查询可用资源调整数据源Query(this.NewVin, vehicleShipplanApprovalDetail.ProductCode, vehicleShipplanApprovalDetail.VIN), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                        var entities = loadOp.Entities;
                        if(entities != null) {
                            foreach(var entity in entities) {
                                entity.AdjustmentType = this.AdjustmentType;
                                this.VehicleAvailableResources.Add(entity);
                            }
                        }
                    }, null);
                    break;
                case (int)DcsVehShipplanApprovalChangeRecAdjustmentType.审批单调整:
                    this.DomainContext.Load(this.DomainContext.查询审批单调整数据源Query(this.NewVin, vehicleShipplanApprovalDetail.ProductCode, vehicleShipplanApprovalDetail.VIN), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                        if(loadOp.Entities != null) {
                            var entities = loadOp.Entities.ToArray();
                            foreach(var entity in entities) {
                                if(entity.VehicleShipplanApprovalDetails != null) {
                                    var details = entity.VehicleShipplanApprovalDetails.ToArray();
                                    foreach(var detail in details) {
                                        detail.AdjustmentType = this.AdjustmentType;
                                        this.VehicleShipplanApprovalDetailsForAdjustment.Add(detail);
                                    }
                                }
                            }
                        }
                    }, null);
                    break;
            }
        }

        private DataGridViewBase vehicleShipplanApprovalDetailForAdjustmentDataGridView;

        private DataGridViewBase VehicleShipplanApprovalDetailForAdjustmentDataGridView {
            get {
                if(this.vehicleShipplanApprovalDetailForAdjustmentDataGridView == null) {
                    this.vehicleShipplanApprovalDetailForAdjustmentDataGridView = DI.GetDataGridView("VehicleShipplanApprovalDetailForAdjustment");
                    this.vehicleShipplanApprovalDetailForAdjustmentDataGridView.DomainContext = this.DomainContext;
                    this.vehicleShipplanApprovalDetailForAdjustmentDataGridView.SetValue(DataContextProperty, this);
                    this.vehicleShipplanApprovalDetailForAdjustmentDataGridView.SelectionChanged += this.VehicleShipplanApprovalDetailForAdjustmentDataGridView_SelectionChanged;
                }
                return this.vehicleShipplanApprovalDetailForAdjustmentDataGridView;
            }
        }

        private void VehicleShipplanApprovalDetailForAdjustmentDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            var vehicleShipplanApprovalDetail = this.VehicleShipplanApprovalDetailForAdjustmentDataGridView.SelectedEntities.Cast<VehicleShipplanApprovalDetail>().FirstOrDefault();
            if(vehicleShipplanApprovalDetail == null)
                return;
            this.DataGridView.VehShipplanApprovalChangeRec.NewVIN = vehicleShipplanApprovalDetail.VIN;
            this.DataGridView.VehShipplanApprovalChangeRec.NewShipplanApprovalCode = vehicleShipplanApprovalDetail.VehicleShipplanApproval.Code;
        }

        private DataGridViewBase vehicleAvailableResourceForAdjustmentDataGridView;

        private DataGridViewBase VehicleAvailableResourceForAdjustmentDataGridView {
            get {
                if(this.vehicleAvailableResourceForAdjustmentDataGridView == null) {
                    this.vehicleAvailableResourceForAdjustmentDataGridView = DI.GetDataGridView("VehicleAvailableResourceForAdjustment");
                    this.vehicleAvailableResourceForAdjustmentDataGridView.DomainContext = this.DomainContext;
                    this.vehicleAvailableResourceForAdjustmentDataGridView.SetValue(DataContextProperty, this);
                    this.vehicleAvailableResourceForAdjustmentDataGridView.SelectionChanged += this.VehicleAvailableResourceForAdjustmentDataGridView_SelectionChanged;
                }
                return this.vehicleAvailableResourceForAdjustmentDataGridView;
            }
        }

        private void VehicleAvailableResourceForAdjustmentDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            if(this.VehicleAvailableResourceForAdjustmentDataGridView.SelectedEntities == null)
                return;
            var vehicleAvailableResource = this.VehicleAvailableResourceForAdjustmentDataGridView.SelectedEntities.Cast<VehicleAvailableResource>().FirstOrDefault();
            if(vehicleAvailableResource == null)
                return;
            this.DataGridView.VehShipplanApprovalChangeRec.NewVIN = vehicleAvailableResource.VIN;
            this.DataGridView.VehShipplanApprovalChangeRec.NewShipplanApprovalCode = vehicleAvailableResource.VehicleShipplanApprovalCode;
        }

        public VehShipplanApprovalChangeRecDataEditView() {
            InitializeComponent();
            this.keyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.AdjustmentType = (int)DcsVehShipplanApprovalChangeRecAdjustmentType.可用资源调整;
            this.DataGridView.VehShipplanApprovalChangeRec.AdjustmentType = (int)DcsVehShipplanApprovalChangeRecAdjustmentType.可用资源调整;
            var vehicleShipplanApprovalDetailForChangeRecDetailDataEditView = new DcsDetailDataEditView();
            vehicleShipplanApprovalDetailForChangeRecDetailDataEditView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleShipplanApprovalDetail, null, () => this.VehicleShipplanApprovalDetailForChangeRecDataGridView);
            vehicleShipplanApprovalDetailForChangeRecDetailDataEditView.SetValue(Grid.RowProperty, 1);
            vehicleShipplanApprovalDetailForChangeRecDetailDataEditView.UnregisterButton(vehicleShipplanApprovalDetailForChangeRecDetailDataEditView.InsertButton);
            vehicleShipplanApprovalDetailForChangeRecDetailDataEditView.UnregisterButton(vehicleShipplanApprovalDetailForChangeRecDetailDataEditView.DeleteButton);
            var vehShipplanApprovalChangeRecForChangeRecEditDataGridViewDetailDataEditView = new DcsDetailDataEditView();
            vehShipplanApprovalChangeRecForChangeRecEditDataGridViewDetailDataEditView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_VehShipplanApprovalChangeRec, null, () => this.DataGridView);
            vehShipplanApprovalChangeRecForChangeRecEditDataGridViewDetailDataEditView.SetValue(Grid.RowProperty, 2);

            this.VehicleShipplanApprovalDetailForAdjustmentDataGridView.SetValue(Grid.RowProperty, 3);
            this.VehicleShipplanApprovalDetailForAdjustmentDataGridView.SetValue(Grid.ColumnSpanProperty, 3);
            this.VehicleShipplanApprovalDetailForAdjustmentDataGridView.SetValue(MarginProperty, new Thickness(4, 4, 4, 4));

            this.VehicleAvailableResourceForAdjustmentDataGridView.SetValue(Grid.RowProperty, 3);
            this.VehicleAvailableResourceForAdjustmentDataGridView.SetValue(Grid.ColumnSpanProperty, 3);
            this.VehicleAvailableResourceForAdjustmentDataGridView.SetValue(MarginProperty, new Thickness(4, 4, 4, 4));
            this.AdjustmentTypeRoot.Children.Add(this.VehicleShipplanApprovalDetailForAdjustmentDataGridView);
            this.AdjustmentTypeRoot.Children.Add(this.VehicleAvailableResourceForAdjustmentDataGridView);
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1, 0, 0, 3));
            this.LayoutRoot.Children.Add(vehicleShipplanApprovalDetailForChangeRecDetailDataEditView);
            this.LayoutRoot.Children.Add(vehShipplanApprovalChangeRecForChangeRecEditDataGridViewDetailDataEditView);
            this.VehicleShipplanApprovalDetailForAdjustmentDataGridView.Visibility = Visibility.Collapsed;
            this.VehicleAvailableResourceForAdjustmentDataGridView.Visibility = Visibility.Visible;
        }

        private void InitializeCommand() {
            this.searchVehicleShipplanApprovalDetailCommand = new DelegateCommand(() => {
                if(string.IsNullOrWhiteSpace(this.Code) && string.IsNullOrWhiteSpace(this.OriginalVin)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehShipplanApprovalChangeRec_CodeAndOriginalVinIsNull);
                    return;
                }
                this.Clear();
                this.OriginalShipplanApprovalClear();
                this.DomainContext.Load(this.DomainContext.查询整车发车审批单Query(this.Code, this.OriginalVin), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    if(loadOp.Entities == null)
                        return;
                    var vehicleShipplanApprovals = loadOp.Entities.ToArray();
                    foreach(var vehicleShipplanApproval in vehicleShipplanApprovals) {
                        if(vehicleShipplanApproval.VehicleShipplanApprovalDetails != null) {
                            var details = vehicleShipplanApproval.VehicleShipplanApprovalDetails.ToArray();
                            foreach(var detail in details) {
                                this.VehicleShipplanApprovalDetails.Add(detail);
                            }
                        }
                    }
                }, null);
            });
        }

        private void AdjustmentType_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            if(this.VehicleShipplanApprovalDetailForChangeRecDataGridView.SelectedEntities == null)
                return;
            var vehicleShipplanApprovalDetail = this.VehicleShipplanApprovalDetailForChangeRecDataGridView.SelectedEntities.Cast<VehicleShipplanApprovalDetail>().FirstOrDefault();
            if(vehicleShipplanApprovalDetail == null)
                return;
            switch(this.AdjustmentType) {
                case (int)DcsVehShipplanApprovalChangeRecAdjustmentType.可用资源调整:
                    this.NewShipplanApprovalClear();
                    this.DataGridView.VehShipplanApprovalChangeRec.AdjustmentType = (int)DcsVehShipplanApprovalChangeRecAdjustmentType.可用资源调整;
                    this.VehicleShipplanApprovalDetailForAdjustmentDataGridView.Visibility = Visibility.Collapsed;
                    this.VehicleAvailableResourceForAdjustmentDataGridView.Visibility = Visibility.Visible;

                    this.DomainContext.Load(this.DomainContext.查询可用资源调整数据源Query(this.NewVin, vehicleShipplanApprovalDetail.ProductCode, vehicleShipplanApprovalDetail.VIN), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                        var entities = loadOp.Entities;
                        if(entities != null) {
                            foreach(var entity in entities) {
                                entity.AdjustmentType = this.AdjustmentType;
                                this.VehicleAvailableResources.Add(entity);
                            }
                        }
                    }, null);

                    break;
                case (int)DcsVehShipplanApprovalChangeRecAdjustmentType.审批单调整:
                    this.NewShipplanApprovalClear();
                    this.DataGridView.VehShipplanApprovalChangeRec.AdjustmentType = (int)DcsVehShipplanApprovalChangeRecAdjustmentType.审批单调整;
                    this.VehicleShipplanApprovalDetailForAdjustmentDataGridView.Visibility = Visibility.Visible;
                    this.VehicleAvailableResourceForAdjustmentDataGridView.Visibility = Visibility.Collapsed;

                    this.DomainContext.Load(this.DomainContext.查询审批单调整数据源Query(this.NewVin, vehicleShipplanApprovalDetail.ProductCode, vehicleShipplanApprovalDetail.VIN), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError)
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                        if(loadOp.Entities != null) {
                            var entities = loadOp.Entities.ToArray();
                            foreach(var entity in entities) {
                                if(entity.VehicleShipplanApprovalDetails != null) {
                                    var details = entity.VehicleShipplanApprovalDetails.ToArray();
                                    foreach(var detail in details) {
                                        detail.AdjustmentType = this.AdjustmentType;
                                        this.VehicleShipplanApprovalDetailsForAdjustment.Add(detail);
                                    }
                                }
                            }
                        }
                    }, null);
                    break;
            }
        }

        private void Clear() {
            this.VehicleShipplanApprovalDetails.Clear();
            this.VehicleShipplanApprovalDetailsForAdjustment.Clear();
            this.VehicleAvailableResources.Clear();
        }

        private void VehShipplanApprovalChangeRecsClear() {
            this.VehShipplanApprovalChangeRecs.Clear();
        }

        //清除源单据信息
        private void OriginalShipplanApprovalClear() {
            this.DataGridView.VehShipplanApprovalChangeRec.OriginalShipplanApprovalCode = string.Empty;
            this.DataGridView.VehShipplanApprovalChangeRec.OriginalVIN = string.Empty;
            this.DataGridView.VehShipplanApprovalChangeRec.ProductCode = string.Empty;
            this.DataGridView.VehShipplanApprovalChangeRec.ProductCategoryName = string.Empty;
        }

        //清除目标单据信息
        private void NewShipplanApprovalClear() {
            this.VehicleShipplanApprovalDetailsForAdjustment.Clear();
            this.VehicleAvailableResources.Clear();
            this.DataGridView.VehShipplanApprovalChangeRec.NewVIN = string.Empty;
            this.DataGridView.VehShipplanApprovalChangeRec.NewShipplanApprovalCode = string.Empty;
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            if(this.VehShipplanApprovalChangeRecs.Count > 0) {
                foreach(var vehShipplanApprovalChangeRec in this.VehShipplanApprovalChangeRecs) {
                    ((IEditableObject)vehShipplanApprovalChangeRec).EndEdit();
                    try {
                        this.DomainContext.VehShipplanApprovalChangeRecs.Add(vehShipplanApprovalChangeRec);
                        if(vehShipplanApprovalChangeRec.Can生成审批单调整) {
                            vehShipplanApprovalChangeRec.生成审批单调整();
                        }
                    } catch(Exception ex) {
                        UIHelper.ShowAlertMessage(ex.Message);
                    }
                }
            } else {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehShipplanApprovalChangeRec_VehShipplanApprovalChangeRecIsNull);
                return;
            }
            base.OnEditSubmitting();
            this.Clear();
            this.VehShipplanApprovalChangeRecsClear();
        }

        protected override void OnEditCancelled() {
            this.Clear();
            this.VehShipplanApprovalChangeRecsClear();
            base.OnEditCancelled();
        }

        public string Code {
            get {
                return this.code;
            }
            set {
                this.code = value;
                this.OnPropertyChanged("Code");
            }
        }

        public string OriginalVin {
            get {
                return this.originalVin;
            }
            set {
                this.originalVin = value;
                this.OnPropertyChanged("OriginalVin");
            }
        }

        public string NewVin {
            get {
                return this.newVin;
            }
            set {
                this.newVin = value;
                this.OnPropertyChanged("NewVin");
            }
        }

        public int AdjustmentType {
            get {
                return this.adjustmentType;
            }
            set {
                this.adjustmentType = value;
                this.OnPropertyChanged("AdjustmentType");
            }
        }

        public object KvAdjustmentTypes {
            get {
                return this.keyValueManager[this.kvNames[0]];
            }
        }

        public ICommand SearchVehicleShipplanApprovalDetailCommand {
            get {
                if(this.searchVehicleShipplanApprovalDetailCommand == null)
                    InitializeCommand();
                return this.searchVehicleShipplanApprovalDetailCommand;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
