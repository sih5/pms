﻿
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleAccountReceivableBillDataEditView {
        private readonly string[] kvNames = {
            "VehicleAccountReceivableBill_Type"
        };

        private ObservableCollection<VehicleBankAccount> kvBanks;
        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;

        private KeyValueManager keyValueManager;

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleFundsTypesQuery().Where(entity => entity.Status != (int)DcsBaseDataStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                var entities = loadOp.Entities;
                if(entities != null)
                    foreach(var entity in entities) {
                        this.KvVehicleFundsTypes.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Name
                        });
                    }
            }, null);
            domainContext.Load(domainContext.GetVehicleBankAccountsQuery().Where(entity => entity.Status != (int)DcsBaseDataStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                var entities = loadOp.Entities;
                if(entities != null)
                    foreach(var entity in entities) {
                        this.KvBanks.Add(entity);
                    }
            }, null);


            var queryWindow = DI.GetQueryWindow("VehicleCustomerInformation");
            queryWindow.SelectionDecided += this.QueryWindow_SelectionDecided;
            this.popupTextBoxCustomerInformation.PopupContent = queryWindow;
        }

        private void QueryWindow_SelectionDecided(object sender, System.EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var vehicleCustomerInformation = queryWindow.SelectedEntities.Cast<VehicleCustomerInformation>().FirstOrDefault();
            if(vehicleCustomerInformation == null)
                return;
            var vehicleAccountReceivableBill = this.DataContext as VehicleAccountReceivableBill;
            if(vehicleAccountReceivableBill == null)
                return;
            vehicleAccountReceivableBill.CustomerCompanyCode = vehicleCustomerInformation.CustomerCompany.Code;
            vehicleAccountReceivableBill.CustomerCompanyId = vehicleCustomerInformation.CustomerCompany.Id;
            vehicleAccountReceivableBill.CustomerCompanyName = vehicleCustomerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void VehicleAccountReceivableBillDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var vehicleAccountReceivableBill = this.DataContext as VehicleAccountReceivableBill;
            if(vehicleAccountReceivableBill == null)
                return;
            vehicleAccountReceivableBill.PropertyChanged -= this.VehicleAccountReceivableBill_PropertyChanged;
            vehicleAccountReceivableBill.PropertyChanged += this.VehicleAccountReceivableBill_PropertyChanged;
        }

        private void VehicleAccountReceivableBill_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            if(!e.PropertyName.Equals("VehicleBankAccountId"))
                return;
            var vehicleAccountReceivableBill = sender as VehicleAccountReceivableBill;
            if(vehicleAccountReceivableBill == null)
                return;
            var bank = this.KvBanks.SingleOrDefault(entity => entity.Id == vehicleAccountReceivableBill.VehicleBankAccountId);
            if(bank == null)
                return;
            vehicleAccountReceivableBill.PaymentReceptionBankAccount = bank.BankAccountNumber;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleAccountReceivableBillByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var vehicleAccountReceivableBill = this.DataContext as VehicleAccountReceivableBill;
            if(vehicleAccountReceivableBill == null)
                return;
            vehicleAccountReceivableBill.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(vehicleAccountReceivableBill.MoneyOrderNumber))
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_MoneyOrderNumberIsNull, new[] {
                    "MoneyOrderNumber"
                }));
            if(vehicleAccountReceivableBill.CustomerCompanyId == default(int))
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_CustomerCompanyIsNull, new[] {
                    "CustomerCompanyCode"
                }));
            if(vehicleAccountReceivableBill.NoteDenomination <= default(decimal))
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_NoteDenominationIsLessThenZero, new[] {
                    "NoteDenomination"
                }));
            if(vehicleAccountReceivableBill.Type == default(int))
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_TypeIsNull, new[] {
                    "Type"
                }));
            if(string.IsNullOrWhiteSpace(vehicleAccountReceivableBill.IssueCompany))
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_IssueCompanyIsNull, new[] {
                    "IssueCompany"
                }));

            if(!vehicleAccountReceivableBill.IssueDate.HasValue)
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_IssueDateIsNull, new[] {
                    "IssueDate"
                }));
            if(!vehicleAccountReceivableBill.DueDate.HasValue)
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_DueDateIsNull, new[] {
                    "DueDate"
                }));
            if(!vehicleAccountReceivableBill.BillReceptionDate.HasValue)
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_BillReceptionDateIsNull, new[] {
                    "BillReceptionDate"
                }));
            if(vehicleAccountReceivableBill.VehicleFundsTypeId == default(int))
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_VehicleFundsTypeIdIsNull, new[] {
                    "VehicleFundsTypeId"
                }));

            if(vehicleAccountReceivableBill.HasValidationErrors)
                return;
            ((IEditableObject)vehicleAccountReceivableBill).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public VehicleAccountReceivableBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.KeyValueManager.LoadData();
            this.DataContextChanged += this.VehicleAccountReceivableBillDataEditView_DataContextChanged;
        }

        protected override string Title {
            get {
                return DcsUIStrings.BusinessName_VehicleAccountReceivableBill;
            }
        }

        public object KvTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public ObservableCollection<VehicleBankAccount> KvBanks {
            get {
                return this.kvBanks ?? (this.kvBanks = new ObservableCollection<VehicleBankAccount>());
            }
        }

        public ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }
    }
}