﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleAccountReceivableBillForTransferOutDataEditView : INotifyPropertyChanged {
        private readonly string[] kvNames = {
            "VehicleAccountReceivableBill_Type","VehicleAccountReceivableBill_TransferOutType"
        };

        private KeyValueManager keyValueManager;

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private string vehicleFundsTypeName;

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleAccountReceivableBillByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                entity.TransferOutTime = DateTime.Now;
                entity.Status = (int)DcsVehicleAccountReceivableBillStatus.转出申请;
                entity.TransferOutOperatorrName = BaseApp.Current.CurrentUserData.UserName;
                entity.TransferOutOperatorId = BaseApp.Current.CurrentUserData.UserId;
                entity.SalesCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                entity.InterestRefundAmount = 0;
                this.DomainContext.Load(this.DomainContext.GetVehicleFundsTypesQuery().Where(r => r.Id == entity.VehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp2 => {
                    if(loadOp2.HasError) {
                        if(!loadOp2.IsErrorHandled)
                            loadOp2.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp2);
                        return;
                    }
                    if(loadOp2.Entities != null) {
                        var vehicleFundsType = loadOp2.Entities.SingleOrDefault();
                        if(vehicleFundsType != null)
                            this.VehicleFundsTypeName = vehicleFundsType.Name;
                    }
                }, null);
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var vehicleAccountReceivableBill = this.DataContext as VehicleAccountReceivableBill;
            if(vehicleAccountReceivableBill == null)
                return;
            vehicleAccountReceivableBill.ValidationErrors.Clear();
            if(!vehicleAccountReceivableBill.TransferOutTime.HasValue)
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_TransferOutTimeIsNull, new[] {
                    "TransferOutTime"
                }));
            if(!vehicleAccountReceivableBill.TransferOutType.HasValue || vehicleAccountReceivableBill.TransferOutType == default(int))
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_TransferOutTypeIsNull, new[] {
                    "TransferOutType"
                }));
            if(!vehicleAccountReceivableBill.InterestRefundAmount.HasValue || vehicleAccountReceivableBill.InterestRefundAmount <= default(decimal))
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_InterestRefundAmountIsLessThenZero, new[] {
                    "InterestRefundAmount"
                }));
            if(string.IsNullOrWhiteSpace(vehicleAccountReceivableBill.TransferOutCompany))
                vehicleAccountReceivableBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleAccountReceivableBill_TransferOutCompanyIsNull, new[] {
                    "TransferOutCompany"
                }));
            if(vehicleAccountReceivableBill.HasValidationErrors)
                return;
            ((IEditableObject)vehicleAccountReceivableBill).EndEdit();
            base.OnEditSubmitting();
        }

        protected virtual void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public VehicleAccountReceivableBillForTransferOutDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.keyValueManager.LoadData();
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_VehicleAccountReceivableBillForTransferOut;
            }
        }

        public string VehicleFundsTypeName {
            get {
                return this.vehicleFundsTypeName;
            }
            set {
                this.vehicleFundsTypeName = value;
                this.OnPropertyChanged("SelectedFileName");
            }
        }

        public Object KvTransferOutTypes {
            get {
                return this.keyValueManager[this.kvNames[1]];
            }
        }
    }
}
