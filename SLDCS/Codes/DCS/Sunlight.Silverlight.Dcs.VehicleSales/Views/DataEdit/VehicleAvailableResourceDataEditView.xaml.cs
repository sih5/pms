﻿using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleAvailableResourceDataEditView {

        public VehicleAvailableResourceDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("VehicleAvailableResource"));
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleAvailableResource;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleAvailableResourceWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var vehicleAvailableResource = this.DataContext as VehicleAvailableResource;
            if(vehicleAvailableResource == null)
                return;
            vehicleAvailableResource.ValidationErrors.Clear();
            if(vehicleAvailableResource.HasValidationErrors)
                return;
            ((IEditableObject)vehicleAvailableResource).EndEdit();
            base.OnEditSubmitting();
        }
    }
}
