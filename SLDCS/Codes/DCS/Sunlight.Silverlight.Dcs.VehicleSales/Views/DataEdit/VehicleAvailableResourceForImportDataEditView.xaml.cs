﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleAvailableResourceForImportDataEditView {
        private ICommand exportFileCommand;
        private DataGridViewBase vehicleAvailableResourceTemplateDataGridView;

        public VehicleAvailableResourceForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = DcsUIStrings.BusinessName_VehicleAvailableResource,
                Content = this.VehicleAvailableResourceTemplateDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_Import_VehicleAvailableResource;
            }
        }

        private DataGridViewBase VehicleAvailableResourceTemplateDataGridView {
            get {
                return this.vehicleAvailableResourceTemplateDataGridView ?? (vehicleAvailableResourceTemplateDataGridView = DI.GetDataGridView("VehicleAvailableResourceForImport"));
            }
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            this.DomainContext.国产车导入(fileName, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                }

                this.ImportComplete();
                this.DomainContext.IsInvoking = false;
                if(string.IsNullOrEmpty(loadOp.Value) || string.IsNullOrWhiteSpace(loadOp.Value)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImport);
                } else {
                    this.ExportFile(loadOp.Value);
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImportWithError);
                }
            }, null);
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    InitialExportCommand();
                return this.exportFileCommand;
            }
        }

        private void InitialExportCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => ((DcsDataGridViewBase)this.VehicleAvailableResourceTemplateDataGridView).ExportData());
        }
    }
}
