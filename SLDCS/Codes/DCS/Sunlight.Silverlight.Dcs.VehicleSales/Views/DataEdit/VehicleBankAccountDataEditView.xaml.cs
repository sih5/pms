﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleBankAccountDataEditView {
        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleBankAccount;
            }
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("VehicleCustomerInformation");
            queryWindow.SelectionDecided += this.VehicleCustomerInformationQueryWindow_SelectionDecided;
            this.popupTextBoxCustomerCompany.PopupContent = queryWindow;
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleBankAccountByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        private void VehicleCustomerInformationQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var vehiclebankaccount = this.DataContext as VehicleBankAccount;
            if(vehiclebankaccount == null)
                return;
            var vehicleCustomerInformation = queryWindow.SelectedEntities.Cast<VehicleCustomerInformation>().FirstOrDefault();
            if(vehicleCustomerInformation == null)
                return;
            vehiclebankaccount.CustomerCompanyId = vehicleCustomerInformation.CustomerCompany.Id;
            vehiclebankaccount.CustomerCompanyCode = vehicleCustomerInformation.CustomerCompany.Code;
            vehiclebankaccount.CustomerCompanyName = vehicleCustomerInformation.CustomerCompany.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void OnEditSubmitting() {
            var vehiclebankaccount = this.DataContext as VehicleBankAccount;
            if(vehiclebankaccount == null)
                return;
            vehiclebankaccount.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(vehiclebankaccount.BankName))
                vehiclebankaccount.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleBankAccount_BankNameIsNull, new[] { "BankName" }));
            if(string.IsNullOrWhiteSpace(vehiclebankaccount.BankAccountNumber))
                vehiclebankaccount.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleBankAccount_BankAccountNumberIsNull, new[] { "BankAccountNumber" }));
            if(string.IsNullOrWhiteSpace(vehiclebankaccount.CustomerCompanyCode))
                vehiclebankaccount.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleBankAccount_CompanyCodeIsNull, new[] { "CustomerCompanyCode" }));
            if(vehiclebankaccount.HasValidationErrors)
                return;
            ((IEditableObject)vehiclebankaccount).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        public VehicleBankAccountDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
    }
}
