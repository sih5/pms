﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleDLRAccountFreezeBillDataEditView {
        public VehicleDLRAccountFreezeBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleDLRAccountFreezeBill;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("VehicleDLRAccountFreezeBill"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleDLRAccountFreezeBillWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var vehicleDLRAccountFreezeBill = this.DataContext as VehicleDLRAccountFreezeBill;
            if(vehicleDLRAccountFreezeBill == null)
                return;
            vehicleDLRAccountFreezeBill.ValidationErrors.Clear();
            if(vehicleDLRAccountFreezeBill.VehicleFundsTypeId == default(int))
                vehicleDLRAccountFreezeBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleDLRAccountFreezeBill_VehicleFundsTypeIdIsNull, new[] { "VehicleFundsTypeId" }));
            if(vehicleDLRAccountFreezeBill.AccountAvailability == default(int))
                vehicleDLRAccountFreezeBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleDLRAccountFreezeBill_AccountAvailabilityIsNull, new[] { "AccountAvailability" }));
            if(vehicleDLRAccountFreezeBill.HasValidationErrors)
                return;
            ((IEditableObject)vehicleDLRAccountFreezeBill).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
