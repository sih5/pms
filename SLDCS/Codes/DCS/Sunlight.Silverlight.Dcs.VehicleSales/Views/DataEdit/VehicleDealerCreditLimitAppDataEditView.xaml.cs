﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleDealerCreditLimitAppDataEditView {
        public VehicleDealerCreditLimitAppDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += VehicleDealerCreditLimitAppDataEditView_DataContextChanged;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleDealerCreditLimitApp;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("VehicleDealerCreditLimitApp"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleDealerCreditLimitAppsQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void VehicleDealerCreditLimitAppDataEditView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var vehicleDealerCreditLimitApp = this.DataContext as VehicleDealerCreditLimitApp;
            if(vehicleDealerCreditLimitApp == null)
                return;
            vehicleDealerCreditLimitApp.PropertyChanged -= vehicleDealerCreditLimitApp_PropertyChanged;
            vehicleDealerCreditLimitApp.PropertyChanged += vehicleDealerCreditLimitApp_PropertyChanged;
        }

        private void vehicleDealerCreditLimitApp_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(!e.PropertyName.Equals("ExpireDate"))
                return;
            var vehicleDealerCreditLimitApp = this.DataContext as VehicleDealerCreditLimitApp;
            if(vehicleDealerCreditLimitApp == null)
                return;
            var expireDateError = vehicleDealerCreditLimitApp.ValidationErrors.SingleOrDefault(r => r.MemberNames.Equals("ExpireDate"));
            if(expireDateError != null)
                vehicleDealerCreditLimitApp.ValidationErrors.Remove(expireDateError);
            if(vehicleDealerCreditLimitApp.ExpireDate >= DateTime.Now.Date.AddDays(1))
                return;
            vehicleDealerCreditLimitApp.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleDealerCreditLimitApp_ExpireDateIsError, new[] { "ExpireDate" }));
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var vehicleDealerCreditLimitApp = this.DataContext as VehicleDealerCreditLimitApp;
            if(vehicleDealerCreditLimitApp == null)
                return;
            vehicleDealerCreditLimitApp.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(vehicleDealerCreditLimitApp.CustomerCompanyCode))
                vehicleDealerCreditLimitApp.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleDealerCreditLimitApp_CustomerCompanyCodeIsNull, new[] {
                    "CustomerCompanyCode"
                }));
            if(string.IsNullOrEmpty(vehicleDealerCreditLimitApp.CustomerCompanyName))
                vehicleDealerCreditLimitApp.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleDealerCreditLimitApp_CustomerCompanyNameIsNull, new[] {
                    "CustomerCompanyName"
                }));
            if(vehicleDealerCreditLimitApp.VehicleFundsTypeId.Equals(default(int)))
                vehicleDealerCreditLimitApp.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_OEMVehicleMonthQuota_VehicleFundsTypeIdIsNull, new[] {
                    "VehicleFundsTypeId"
                }));
            if(vehicleDealerCreditLimitApp.ValidationErrors.Any())
                return;
            ((IEditableObject)vehicleDealerCreditLimitApp).EndEdit();
            base.OnEditSubmitting();
        }
    }
}