﻿
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleDLRStartSecurityDepositForImportDataEditView {
        private DataGridViewBase vehicleDlrStartSecurityDepositForImportDataGridView;
        private ICommand exportFileCommand;

        public VehicleDLRStartSecurityDepositForImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected DataGridViewBase VehicleDlrStartSecurityDepositForImportDataGridView {
            get {
                return this.vehicleDlrStartSecurityDepositForImportDataGridView ?? (this.vehicleDlrStartSecurityDepositForImportDataGridView = DI.GetDataGridView("VehicleDLRStartSecurityDepositForImport"));
            }
        }

        private void CreateUI() {
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleDLRStartSecurityDeposits,
                Content = this.VehicleDlrStartSecurityDepositForImportDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
            this.HideCancelButton();
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        private void UploadFileProcessing(string fileName) {
            this.ExcelServiceClient.ImportVehicleDLRStartSecurityDepositAsync(fileName);
            this.ExcelServiceClient.ImportVehicleDLRStartSecurityDepositCompleted -= ExcelServiceClient_ImportVehicleDLRStartSecurityDepositCompleted;
            this.ExcelServiceClient.ImportVehicleDLRStartSecurityDepositCompleted += ExcelServiceClient_ImportVehicleDLRStartSecurityDepositCompleted;
        }

        private void ExcelServiceClient_ImportVehicleDLRStartSecurityDepositCompleted(object sender, ImportVehicleDLRStartSecurityDepositCompletedEventArgs e) {
            //调用父类方法标记导入完成
            this.ImportComplete();
            //只要返回错误数据 或者 错误信息 或者 错误信息的文件名。都标记为导入存在错误
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImport);
            } else {
                if(this.HasImportingError)
                    //直接导出错误数据文件，直接调用导出方法。传入文件名
                    this.ExportFile(e.errorDataFileName);
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImportWithError);
            }
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_Import_VehicleDLRStartSecurityDeposit;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    InitialExportCommand();
                return this.exportFileCommand;
            }
        }

        private void InitialExportCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => ((DcsDataGridViewBase)this.VehicleDlrStartSecurityDepositForImportDataGridView).ExportData());
        }
    }
}
