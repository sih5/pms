﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleInformationDataEditView {
        private void CreateUI() {
            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("VehicleInformation"));
        }

        public VehicleInformationDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleInformationsQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleInformation;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            var vehicleInformation = this.DataContext as VehicleInformation;
            if(vehicleInformation == null)
                return;
            vehicleInformation.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(vehicleInformation.VIN))
                vehicleInformation.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_VINIsNotNull, new[] {
                    "VIN"
                }));
            if(string.IsNullOrEmpty(vehicleInformation.ProductCategoryCode))
                vehicleInformation.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_ProductCategoryCodeIsNotNull, new[] {
                    "ProductCategoryCode"
                }));
            if(string.IsNullOrEmpty(vehicleInformation.ProductCode))
                vehicleInformation.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_ProductCodeIsNotNull, new[] {
                    "ProductCode"
                }));
            if(string.IsNullOrEmpty(vehicleInformation.EngineSerialNumber))
                vehicleInformation.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_EngineSerialNumberIsNotNull, new[] {
                    "EngineSerialNumber"
                }));
            if(!vehicleInformation.RolloutDate.HasValue)
                vehicleInformation.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_RolloutDateIsNotNull, new[] {
                    "RolloutDate"
                }));
            if(vehicleInformation.VehicleType == 0)
                vehicleInformation.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_VehicleTypeIsNotNull, new[] {
                    "VehicleType"
                }));
            if(string.IsNullOrEmpty(vehicleInformation.ProductionPlanSON))
                vehicleInformation.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleInformation_SONIsNotNull, new[] {
                    "ProductionPlanSON"
                }));
            if(vehicleInformation.EntityState == EntityState.New) {
                var vehicleInformationLocal = vehicleInformation;
                this.DomainContext.Load(this.DomainContext.GetVehicleInformationsQuery().Where(v => v.VIN.ToLower().Equals(vehicleInformationLocal.VIN)), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    }
                    if(loadOp.Entities != null && loadOp.Entities.Any())
                        DcsUtils.Confirm(VehicleSalesUIStrings.DataEditView_Confirm_ProductCode, () => {
                            var entity = loadOp.Entities.First();
                            entity.VIN = vehicleInformation.VIN;
                            entity.ProductCategoryCode = vehicleInformation.ProductCategoryCode;
                            entity.ProductCode = vehicleInformation.ProductCode;
                            entity.EngineSerialNumber = vehicleInformation.EngineSerialNumber;
                            entity.RolloutDate = vehicleInformation.RolloutDate;
                            entity.VehicleType = vehicleInformation.VehicleType;
                            entity.SON = vehicleInformation.ProductionPlanSON;

                            if(this.DomainContext.VehicleInformations.Any(r => r.EntityState == EntityState.New))
                                this.DomainContext.VehicleInformations.Detach(vehicleInformation);
                            ((IEditableObject)vehicleInformation).EndEdit();
                            base.OnEditSubmitting();
                        }, () => {
                            if(this.DomainContext.VehicleInformations.All(r => r.EntityState != EntityState.New))
                                this.DomainContext.VehicleInformations.Add(vehicleInformation);
                        });
                    else {
                        ((IEditableObject)vehicleInformation).EndEdit();
                        base.OnEditSubmitting();
                    }
                }, null);
            } else {
                ((IEditableObject)vehicleInformation).EndEdit();
                base.OnEditSubmitting();
            }
        }
    }
}
