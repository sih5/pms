﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using Sunlight.Silverlight.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleOrderDataEditView {
        private DataGridViewBase vehicleOrderDetailForEditDataGridView;
        private DataGridViewBase vehicleOrderDetailCollectForEditDataGridView;
        private VehicleProductCategoryDataView vehicleProductCategoryDataView;
        private QueryWindowBase vehicleAvailableResourceQueryWindow;
        private RadWindow queryWindow;
        private bool isCanChangeDetails = true;
        private bool isCanSubmit = true;

        private DataGridViewBase VehicleOrderDetailForEditDataGridView {
            get {
                if(this.vehicleOrderDetailForEditDataGridView == null) {
                    this.vehicleOrderDetailForEditDataGridView = DI.GetDataGridView("VehicleOrderDetailForEdit");
                    this.vehicleOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleOrderDetailForEditDataGridView;
            }
        }

        private DataGridViewBase VehicleOrderDetailCollectForEditDataGridView {
            get {
                if(this.vehicleOrderDetailCollectForEditDataGridView == null) {
                    this.vehicleOrderDetailCollectForEditDataGridView = DI.GetDataGridView("VehicleOrderDetailCollectForEdit");
                    this.vehicleOrderDetailCollectForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleOrderDetailCollectForEditDataGridView;
            }
        }

        public VehicleProductCategoryDataView VehicleProductCategoryDataView {
            get {
                if(this.vehicleProductCategoryDataView == null) {
                    this.vehicleProductCategoryDataView = new VehicleProductCategoryDataView();
                    this.vehicleProductCategoryDataView.OnDataGridRowDoubleClick += vehicleProductCategoryDataView_OnDataGridRowDoubleClick;
                }
                return this.vehicleProductCategoryDataView;
            }
        }

        public QueryWindowBase VehicleAvailableResourceQueryWindow {
            get {
                return this.vehicleAvailableResourceQueryWindow ?? (this.vehicleAvailableResourceQueryWindow = DI.GetQueryWindow("VehicleAvailableResourceForVehicleOrder"));
            }
        }

        private RadWindow QueryWindow {
            get {
                return this.queryWindow ?? (this.queryWindow = new RadWindow {
                    CanMove = true,
                    CanClose = true,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    Header = VehicleSalesUIStrings.QueryPanel_Title_VehicleAvailableResource,
                    Content = this.VehicleAvailableResourceQueryWindow
                });
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleOrder;
            }
        }

        private void vehicleOrderDetailCollects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if(!this.isCanChangeDetails) {
                this.isCanChangeDetails = true;
                return;
            }
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            IEnumerable<VehicleOrderDetailCollect> items;
            if(e.Action == NotifyCollectionChangedAction.Add) {
                items = e.NewItems.Cast<VehicleOrderDetailCollect>();
                foreach(var orderDetailCollect in items) {
                    orderDetailCollect.PropertyChanged -= orderDetailCollect_PropertyChanged;
                    orderDetailCollect.PropertyChanged += orderDetailCollect_PropertyChanged;
                    for(var i = 0; i < orderDetailCollect.Quantity; i++) {
                        vehicleOrder.VehicleOrderDetails.Add(this.CreateDefaultVehicleOrderDetail(vehicleOrder, orderDetailCollect));
                    }
                }
            } else {
                items = e.OldItems.Cast<VehicleOrderDetailCollect>();
                foreach(var orderDetailCollect in items)
                    this.ClearVehicleOrderDetailsByDetailCollect(vehicleOrder, orderDetailCollect);
            }
            this.StatTotalAmount(vehicleOrder);
        }

        private void orderDetailCollect_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(!e.PropertyName.Equals("Quantity"))
                return;
            var vehicleOrderDetailCollect = sender as VehicleOrderDetailCollect;
            if(vehicleOrderDetailCollect == null)
                return;
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            if(vehicleOrder.VehicleOrderDetailCollects.Sum(r => r.Quantity) > 5000) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderDetailCollect_QuantityGreaterThanFiveThousand);
                vehicleOrderDetailCollect.Quantity = vehicleOrder.VehicleOrderDetails.Where(r => r.ProductId == vehicleOrderDetailCollect.ProductId && r.ProductCategoryCode == vehicleOrderDetailCollect.ProductCategoryCode).ToArray().Count();
                return;
            }
            var oldCount = vehicleOrder.VehicleOrderDetails.Count(r => r.ProductCategoryCode == vehicleOrderDetailCollect.ProductCategoryCode && r.ProductId == vehicleOrderDetailCollect.ProductId);
            if(vehicleOrderDetailCollect.Quantity > oldCount) {
                for(var i = 0; i < vehicleOrderDetailCollect.Quantity - oldCount; i++)
                    vehicleOrder.VehicleOrderDetails.Add(this.CreateDefaultVehicleOrderDetail(vehicleOrder, vehicleOrderDetailCollect));
            } else {
                for(var i = 0; i < oldCount - vehicleOrderDetailCollect.Quantity; i++)
                    vehicleOrder.VehicleOrderDetails.Remove(vehicleOrder.VehicleOrderDetails.OrderBy(r => r.SON).Last(r => r.ProductCategoryCode == vehicleOrderDetailCollect.ProductCategoryCode && r.ProductId == vehicleOrderDetailCollect.ProductId));
            }
            this.StatTotalAmount(vehicleOrder);
        }

        private void VehicleOrderForDealerDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            vehicleOrder.PropertyChanged -= vehicleOrder_PropertyChanged;
            vehicleOrder.PropertyChanged += vehicleOrder_PropertyChanged;
            vehicleOrder.VehicleOrderDetailCollects.CollectionChanged += vehicleOrderDetailCollects_CollectionChanged;
        }

        private void vehicleOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            switch(e.PropertyName) {
                case "VehicleFundsTypeId":
                case "DealerId":
                    if(vehicleOrder.VehicleFundsTypeId != default(int) && vehicleOrder.DealerId != default(int))
                        if(this.DomainContext == null)
                            this.Initializer.Register(() => this.GetVehicleCustomerAccountInfo(vehicleOrder));
                        else
                            this.GetVehicleCustomerAccountInfo(vehicleOrder);
                    break;
            }
        }

        private void vehicleProductCategoryDataView_OnDataGridRowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            var vehicleModelAffiProduct = VehicleProductCategoryDataView.GridViewSelectedItem as VehicleModelAffiProduct;
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null || vehicleModelAffiProduct == null)
                return;

            if(vehicleOrder.VehicleOrderDetailCollects.Any(r => r.ProductId == vehicleModelAffiProduct.ProductId)) {
                if(vehicleOrder.VehicleOrderDetailCollects.Sum(r => r.Quantity) > 4999) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderDetailCollect_QuantityGreaterThanFiveThousand);
                    return;
                }
                DcsUtils.Confirm(string.Format(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderDetailCollect_ProductIsAlreadyExists, vehicleModelAffiProduct.ProductCode), () => vehicleOrder.VehicleOrderDetailCollects.First(r => r.ProductId == vehicleModelAffiProduct.ProductId).Quantity += 1);
            } else {

                vehicleOrder.VehicleOrderDetailCollects.Add(this.CreateDefaultVehicleOrderDetailCollect(vehicleModelAffiProduct));
            }
        }

        private VehicleOrderDetail CreateDefaultVehicleOrderDetail(VehicleOrder vehicleOrder, VehicleOrderDetailCollect vehicleOrderDetailCollect) {
            return new VehicleOrderDetail {
                VehicleOrderId = vehicleOrder.Id,
                ProductCategoryCode = vehicleOrderDetailCollect.ProductCategoryCode,
                ProductCategoryName = vehicleOrderDetailCollect.ProductCategoryName,
                ProductId = vehicleOrderDetailCollect.ProductId,
                ProductCode = vehicleOrderDetailCollect.ProductCode,
                ProductName = vehicleOrderDetailCollect.ProductName,
                SON = GlobalVar.ASSIGNED_BY_SERVER,
                Price = vehicleOrderDetailCollect.Price,
                Status = (int)DcsVehicleOrderDetailStatus.新建
            };
        }

        private VehicleOrderDetailCollect CreateDefaultVehicleOrderDetailCollect(VehicleModelAffiProduct vehicleModelAffiProduct) {
            return new VehicleOrderDetailCollect {
                ProductId = vehicleModelAffiProduct.ProductId,
                ProductCode = vehicleModelAffiProduct.ProductCode,
                ProductName = vehicleModelAffiProduct.ProductName,
                ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode,
                ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName,
                Quantity = 1,
                Price = vehicleModelAffiProduct.WholesalePrice
            };
        }

        private void ClearVehicleOrderDetailsByDetailCollect(VehicleOrder vehicleOrder, VehicleOrderDetailCollect vehicleOrderDetailCollect) {
            var details = vehicleOrder.VehicleOrderDetails.Where(r => r.ProductCategoryCode == vehicleOrderDetailCollect.ProductCategoryCode && r.ProductId == vehicleOrderDetailCollect.ProductId);
            if(details == null)
                return;
            foreach(var vehicleOrderDetail in details) {
                vehicleOrder.VehicleOrderDetails.Remove(vehicleOrderDetail);
            }
        }

        private void CreateVehicleOrderDetailCollects(VehicleOrder vehicleOrder) {
            var vehicleOrderDetailCollects = (from orderDetail in vehicleOrder.VehicleOrderDetails
                                              group orderDetail by new {
                                                  orderDetail.ProductCode,
                                                  orderDetail.ProductCategoryCode,
                                                  orderDetail.ProductName,
                                                  orderDetail.ProductId,
                                                  orderDetail.ProductCategoryName,
                                                  orderDetail.Price
                                              }
                                                  into collect
                                                  select new {
                                                      collect.Key.ProductCategoryCode,
                                                      collect.Key.ProductCode,
                                                      collect.Key.ProductId,
                                                      collect.Key.ProductName,
                                                      collect.Key.ProductCategoryName,
                                                      Quantity = collect.Count(),
                                                      collect.Key.Price
                                                  });
            foreach(var vehicleOrderDetailCollect in vehicleOrderDetailCollects) {
                this.isCanChangeDetails = false;
                var detailCollect = new VehicleOrderDetailCollect {
                    ProductId = vehicleOrderDetailCollect.ProductId,
                    ProductName = vehicleOrderDetailCollect.ProductName,
                    ProductCode = vehicleOrderDetailCollect.ProductCode,
                    ProductCategoryCode = vehicleOrderDetailCollect.ProductCategoryCode,
                    ProductCategoryName = vehicleOrderDetailCollect.ProductCategoryName,
                    Quantity = vehicleOrderDetailCollect.Quantity,
                    Price = vehicleOrderDetailCollect.Price
                };
                detailCollect.PropertyChanged -= orderDetailCollect_PropertyChanged;
                detailCollect.PropertyChanged += orderDetailCollect_PropertyChanged;
                vehicleOrder.VehicleOrderDetailCollects.Add(detailCollect);

            }
        }

        private void GetVehicleCustomerAccountInfo(VehicleOrder vehicleOrder) {
            if(vehicleOrder.DealerId == default(int) || vehicleOrder.VehicleFundsTypeId == default(int))
                return;
            this.DomainContext.Load(this.DomainContext.GetVehicleCustomerAccountsQuery().Where(r => r.CustomerCompanyId == vehicleOrder.DealerId && r.VehicleFundsTypeId == vehicleOrder.VehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                if(!loadOp.Entities.Any()) {
                    this.SetViewStatus(false);
                    UIHelper.ShowAlertMessage(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrder_VehicleCustomerAccountIsNull);
                    return;
                }
                this.SetViewStatus(true);
                var customerAccount = loadOp.Entities.SingleOrDefault();
                if(customerAccount == null)
                    return;
                vehicleOrder.VehicleCustomerAccountId = customerAccount.Id;
                vehicleOrder.Balance = customerAccount.AccountBalance;
            }, null);
        }

        private void SetViewStatus(bool isEnabled) {
            this.isCanSubmit = isEnabled;
            foreach(var children in this.Root.Children.Where(children => !(children is Rectangle))) {
                var vehicleOrderForDealerDataEditPanel = children as VehicleOrderDataEditPanel;
                if(vehicleOrderForDealerDataEditPanel != null) {
                    var grid = vehicleOrderForDealerDataEditPanel.Root;
                    foreach(var control in grid.Children.Where(control => !(control is DcsComboBox) && !(control is PopupTextBox) && !(control is TextBlock))) {
                        control.SetValue(IsEnabledProperty, isEnabled);
                    }
                    continue;
                }
                children.SetValue(IsEnabledProperty, isEnabled);
            }
        }

        private void StatTotalAmount(VehicleOrder vehicleOrder) {
            vehicleOrder.TotalAmount = (from collect in vehicleOrder.VehicleOrderDetailCollects
                                        select collect.Price * collect.Quantity).Sum();
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("VehicleOrder"));
            this.Root.Children.Add(this.CreateVerticalLine(1, 0, 0, 2));
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleOrderDetailCollects, null, () => this.VehicleOrderDetailCollectForEditDataGridView);
            detailEditView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleOrderDetails, null, () => this.VehicleOrderDetailForEditDataGridView);
            detailEditView.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(detailEditView);
            this.VehicleProductCategoryDataView.SetValue(Grid.ColumnProperty, 2);
            this.VehicleProductCategoryDataView.SetValue(Grid.RowSpanProperty, 2);
            this.VehicleProductCategoryDataView.SetValue(MarginProperty, new Thickness(0, 0, 0, 3));
            this.Root.Children.Add(this.VehicleProductCategoryDataView);
            this.RegisterButton(new ButtonItem {
                Title = VehicleSalesUIStrings.DataEditView_Button_Common_DealerStockQuery,
                Command = new DelegateCommand(() => this.QueryWindow.ShowDialog()),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/query.png", UriKind.Relative)
            });
            this.Loaded += VehicleOrderDataEditView_Loaded;
        }

        private void VehicleOrderDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.VehicleProductCategoryDataView.RefreshTreeViewAndClearGridView(GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID);

        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    isCanChangeDetails = false;
                    entity.VehicleOrderDetailCollects.Clear();
                    isCanChangeDetails = true;
                    this.SetObjectToEdit(entity);
                    this.GetVehicleCustomerAccountInfo(entity);
                    this.CreateVehicleOrderDetailCollects(entity);
                }
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override void OnEditSubmitting() {
            if(!this.isCanSubmit) {
                UIHelper.ShowAlertMessage(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrder_VehicleCustomerAccountIsNull);
                return;
            }
            if(!this.VehicleOrderDetailCollectForEditDataGridView.CommitEdit() || !this.VehicleOrderDetailForEditDataGridView.CommitEdit())
                return;
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            if(!vehicleOrder.VehicleOrderDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrder_VehicleOrderDetailIsNull);
                return;
            }
            if(vehicleOrder.VehicleOrderDetailCollects.Any(r => r.Quantity < 1)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrder_VehicleOrderDetailCollect_QuantityLessThenOne);
                return;
            }
            if(vehicleOrder.VehicleOrderDetails.All(entity => entity.Price <= 0)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderDetail_PriceGreaterThanZero);
                return;
            }
            vehicleOrder.ValidationErrors.Clear();
            ((IEditableObject)vehicleOrder).EndEdit();
            try {
                if(vehicleOrder.EntityState == EntityState.Modified) {
                    if(vehicleOrder.Can调整整车订单)
                        vehicleOrder.调整整车订单();
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            this.isCanChangeDetails = true;
            this.SetViewStatus(true);
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            this.isCanChangeDetails = true;
            this.SetViewStatus(true);
            base.OnEditCancelled();
        }

        public VehicleOrderDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged -= VehicleOrderForDealerDataEditView_DataContextChanged;
            this.DataContextChanged += VehicleOrderForDealerDataEditView_DataContextChanged;
        }
    }
}
