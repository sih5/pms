﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleOrderForConfirmDataEditView {
        private DataGridViewBase vehicleOrderDetailDataGridView;
        private DataGridViewBase vehicleOrderDetailCollectDataGridView;
        private ObservableCollection<VehicleOrderDetailCollect> vehicleOrderDetailCollects;
        private QueryWindowBase vehicleAvailableResourceQueryWindow;
        private ObservableCollection<VehicleOrderDetail> oldVehicleOrderDetails;

        private ObservableCollection<VehicleOrderDetail> OldVehicleOrderDetails {
            get {
                return this.oldVehicleOrderDetails ?? (this.oldVehicleOrderDetails = new ObservableCollection<VehicleOrderDetail>());
            }
        }

        private RadWindow queryWindow;

        public DataGridViewBase VehicleOrderDetailDataGridView {
            get {
                if(this.vehicleOrderDetailDataGridView == null) {
                    this.vehicleOrderDetailDataGridView = DI.GetDataGridView("VehicleOrderDetailForConfirm");
                    this.vehicleOrderDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleOrderDetailDataGridView;
            }
        }

        private DataGridViewBase VehicleOrderDetailCollectDataGridView {
            get {
                if(this.vehicleOrderDetailCollectDataGridView == null) {
                    this.vehicleOrderDetailCollectDataGridView = DI.GetDataGridView("VehicleOrderDetailCollect");
                    this.vehicleOrderDetailCollectDataGridView.DomainContext = this.DomainContext;
                    this.vehicleOrderDetailCollectDataGridView.SetValue(DataContextProperty, this);
                }
                return this.vehicleOrderDetailCollectDataGridView;
            }
        }

        public ObservableCollection<VehicleOrderDetailCollect> VehicleOrderDetailCollects {
            get {
                return this.vehicleOrderDetailCollects ?? (this.vehicleOrderDetailCollects = new ObservableCollection<VehicleOrderDetailCollect>());
            }
        }

        public QueryWindowBase VehicleAvailableResourceQueryWindow {
            get {
                return this.vehicleAvailableResourceQueryWindow ?? (this.vehicleAvailableResourceQueryWindow = DI.GetQueryWindow("VehicleAvailableResourceForVehicleOrder"));
            }
        }

        private RadWindow QueryWindow {
            get {
                return this.queryWindow ?? (this.queryWindow = new RadWindow {
                    CanMove = true,
                    CanClose = true,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    Header = VehicleSalesUIStrings.QueryPanel_Title_VehicleAvailableResource,
                    Content = this.VehicleAvailableResourceQueryWindow
                });
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("VehicleOrderForConfirm"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Header = VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleOrderDetails,
                Content = this.VehicleOrderDetailDataGridView
            });
            tabControl.Items.Add(new RadTabItem {
                Header = VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleOrderDetailCollects,
                Content = this.VehicleOrderDetailCollectDataGridView
            });
            tabControl.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(tabControl);
            this.RegisterButton(new ButtonItem {
                Title = VehicleSalesUIStrings.DataEditView_Button_Common_DealerStockQuery,
                Command = new DelegateCommand(() => this.QueryWindow.ShowDialog()),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/query.png", UriKind.Relative)
            });
        }

        private void GetVehicleCustomerAccountInfo(VehicleOrder vehicleOrder) {
            this.DomainContext.Load(this.DomainContext.GetVehicleCustomerAccountsQuery().Where(r => r.CustomerCompanyId == vehicleOrder.DealerId && r.VehicleFundsTypeId == vehicleOrder.VehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                var customerAccount = loadOp.Entities.SingleOrDefault();
                if(customerAccount == null)
                    return;
                vehicleOrder.Balance = customerAccount.AccountBalance;
            }, null);
        }

        private void CreateVehicleOrderDetailCollects(VehicleOrder vehicleOrder) {
            this.VehicleOrderDetailCollects.Clear();
            var vehicleOrderDetailCollects = (from orderDetail in vehicleOrder.VehicleOrderDetails
                                              group orderDetail by new {
                                                  ProductCode = orderDetail.ProductCode,
                                                  ProductCategoryCode = orderDetail.ProductCategoryCode,
                                                  ProductName = orderDetail.ProductName,
                                                  ProductId = orderDetail.ProductId,
                                                  ProductCategoryName = orderDetail.ProductCategoryName
                                              }
                                                  into collect
                                                  select new {
                                                      collect.Key.ProductCategoryCode,
                                                      collect.Key.ProductCode,
                                                      collect.Key.ProductId,
                                                      collect.Key.ProductName,
                                                      collect.Key.ProductCategoryName,
                                                      Quantity = collect.Count()
                                                  });
            foreach(var vehicleOrderDetailCollect in vehicleOrderDetailCollects) {
                this.VehicleOrderDetailCollects.Add(new VehicleOrderDetailCollect {
                    ProductId = vehicleOrderDetailCollect.ProductId,
                    ProductName = vehicleOrderDetailCollect.ProductName,
                    ProductCode = vehicleOrderDetailCollect.ProductCode,
                    ProductCategoryCode = vehicleOrderDetailCollect.ProductCategoryCode,
                    ProductCategoryName = vehicleOrderDetailCollect.ProductCategoryName,
                    Quantity = vehicleOrderDetailCollect.Quantity
                });
            }
        }

        private void GetVehicleFundsType(VehicleOrder vehicleOrder) {
            this.DomainContext.Load(this.DomainContext.GetVehicleFundsTypesQuery().Where(r => r.Id == vehicleOrder.VehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null)
                    vehicleOrder.VehicleFundsTypeName = entity.Name;
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleOrdersQuery().Where(r => r.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null)
                    return;
                this.SetObjectToEdit(entity);
                this.GetVehicleCustomerAccountInfo(entity);
                this.GetVehicleFundsType(entity);
                this.DomainContext.Load(this.DomainContext.GetVehicleOrderDetailsQuery().Where(r => r.VehicleOrderId == id), LoadBehavior.KeepCurrent, loadOp2 => {
                    if(loadOp2.HasError) {
                        if(loadOp2.IsErrorHandled)
                            loadOp2.MarkErrorAsHandled();
                        DcsUtils.ShowLoadError(loadOp2);
                        return;
                    }
                    var entities = loadOp2.Entities;
                    if(entities != null) {
                        this.OldVehicleOrderDetails.Clear();
                        foreach(var detail in entities) {
                            this.OldVehicleOrderDetails.Add(new VehicleOrderDetail {
                                SON = detail.SON,
                                EstimatedShippingDate = detail.EstimatedShippingDate,
                                Status = detail.Status
                            });
                        }
                    }
                    this.CreateVehicleOrderDetailCollects(entity);
                }, null);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            vehicleOrder.ValidationErrors.Clear();
            if(vehicleOrder.HasValidationErrors)
                return;
            this.VehicleOrderDetailDataGridView.CommitEdit();
            this.VehicleOrderDetailCollectDataGridView.CommitEdit();
            ((IEditableObject)vehicleOrder).EndEdit();
            foreach(var detail in vehicleOrder.VehicleOrderDetails) {
                detail.ValidationErrors.Clear();
            }
            bool canUser = false;
            if(vehicleOrder.VehicleOrderDetails != null && vehicleOrder.VehicleOrderDetails.Count != 0) {
                foreach(var detail in vehicleOrder.VehicleOrderDetails) {
                    detail.ValidationErrors.Clear();
                    if(detail.Status == (int)DcsVehicleOrderDetailStatus.已确认 || detail.Status == (int)DcsVehicleOrderDetailStatus.拒绝)
                        canUser = true;
                    ((IEditableObject)detail).EndEdit();
                }
                if(canUser == false) {
                    UIHelper.ShowAlertMessage("清单中至少有一条状态为已确认或为拒绝");
                    return;
                }
            }
            //根据原始数据还原状态
            //if(this.VehicleOrderDetailDataGridView.SelectedEntities == null || !this.VehicleOrderDetailDataGridView.SelectedEntities.Any()) {
            //    if(vehicleOrder.VehicleOrderDetails != null)
            //        foreach(var details in vehicleOrder.VehicleOrderDetails) {
            //            this.SetVehicleOrderDetails(details);
            //        }
            //} else {
            //    var sons = this.VehicleOrderDetailDataGridView.SelectedEntities.Cast<VehicleOrderDetail>().Select(e => e.SON);
            //    foreach(var details in vehicleOrder.VehicleOrderDetails.Where(e => !sons.Contains(e.SON))) {
            //        this.SetVehicleOrderDetails(details);
            //    }
            //}
            //确认销售订单状态
            SetVehicleOrderStatus(vehicleOrder);
            try {
                if(vehicleOrder.Can确认整车订单)
                    vehicleOrder.确认整车订单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private static void SetVehicleOrderStatus(VehicleOrder vehicleOrder) {
            if(vehicleOrder.VehicleOrderDetails.All(r => r.Status == (int)DcsVehicleOrderDetailStatus.新建))
                vehicleOrder.Status = (int)DcsVehicleOrderStatus.提交;
            else if(vehicleOrder.VehicleOrderDetails.All(r => r.Status == (int)DcsVehicleOrderDetailStatus.拒绝))
                vehicleOrder.Status = (int)DcsVehicleOrderStatus.拒绝;
            else if(vehicleOrder.VehicleOrderDetails.Any(r => r.Status == (int)DcsVehicleOrderDetailStatus.完成))
                vehicleOrder.Status = (int)DcsVehicleOrderStatus.部分审批;
            else if(vehicleOrder.VehicleOrderDetails.Any(r => r.Status == (int)DcsVehicleOrderDetailStatus.新建))
                vehicleOrder.Status = (int)DcsVehicleOrderStatus.部分确认;
            else
                vehicleOrder.Status = (int)DcsVehicleOrderStatus.已确认;
        }

        //private void SetVehicleOrderDetails(VehicleOrderDetail details) {
        //    var oldDetails = this.oldVehicleOrderDetails.FirstOrDefault(e => e.SON == details.SON);
        //    if(oldDetails == null)
        //        return;
        //    details.EstimatedShippingDate = oldDetails.EstimatedShippingDate;
        //    details.Status = oldDetails.Status;
        //}

        protected override void OnEditSubmitted() {
            this.VehicleOrderDetailCollects.Clear();
            this.OldVehicleOrderDetails.Clear();
            this.DataContext = null;
            base.OnEditSubmitted();
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_VehicleOrderForConfirm;
            }
        }

        public VehicleOrderForConfirmDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
    }
}