﻿
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleOrderForInsteadSubmitDataEditView {
        private DataGridViewBase vehicleOrderDetailDataGridView;
        private DataGridViewBase vehicleOrderDetailCollectDataGridView;
        private ObservableCollection<VehicleOrderDetailCollect> vehicleOrderDetailCollects;

        public DataGridViewBase VehicleOrderDetailDataGridView {
            get {
                if(this.vehicleOrderDetailDataGridView == null) {
                    this.vehicleOrderDetailDataGridView = DI.GetDataGridView("VehicleOrderDetailForSubmit");
                    this.vehicleOrderDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleOrderDetailDataGridView;
            }
        }

        private DataGridViewBase VehicleOrderDetailCollectDataGridView {
            get {
                if(this.vehicleOrderDetailCollectDataGridView == null) {
                    this.vehicleOrderDetailCollectDataGridView = DI.GetDataGridView("VehicleOrderDetailCollect");
                    this.vehicleOrderDetailCollectDataGridView.DomainContext = this.DomainContext;
                    this.vehicleOrderDetailCollectDataGridView.SetValue(DataContextProperty, this);
                }
                return this.vehicleOrderDetailCollectDataGridView;
            }
        }

        public ObservableCollection<VehicleOrderDetailCollect> VehicleOrderDetailCollects {
            get {
                return this.vehicleOrderDetailCollects ?? (this.vehicleOrderDetailCollects = new ObservableCollection<VehicleOrderDetailCollect>());
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("VehicleOrderForInsteadSubmit"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Header = VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleOrderDetailCollects,
                Content = this.VehicleOrderDetailCollectDataGridView
            });
            tabControl.Items.Add(new RadTabItem {
                Header = VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleOrderDetails,
                Content = this.VehicleOrderDetailDataGridView
            });
            tabControl.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(tabControl);
        }

        private void GetVehicleCustomerAccountInfo(VehicleOrder vehicleOrder) {
            this.DomainContext.Load(this.DomainContext.GetVehicleCustomerAccountsQuery().Where(r => r.CustomerCompanyId == vehicleOrder.DealerId && r.VehicleFundsTypeId == vehicleOrder.VehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                var customerAccount = loadOp.Entities.SingleOrDefault();
                if(customerAccount == null)
                    return;
                vehicleOrder.Balance = customerAccount.AccountBalance;
            }, null);
        }

        private void CreateVehicleOrderDetailCollects(VehicleOrder vehicleOrder) {
            var vehicleOrderDetailCollects = (from orderDetail in vehicleOrder.VehicleOrderDetails
                                              group orderDetail by new {
                                                  ProductCode = orderDetail.ProductCode,
                                                  ProductCategoryCode = orderDetail.ProductCategoryCode,
                                                  ProductName = orderDetail.ProductName,
                                                  ProductId = orderDetail.ProductId,
                                                  ProductCategoryName = orderDetail.ProductCategoryName
                                              }
                                                  into collect
                                                  select new {
                                                      collect.Key.ProductCategoryCode,
                                                      collect.Key.ProductCode,
                                                      collect.Key.ProductId,
                                                      collect.Key.ProductName,
                                                      collect.Key.ProductCategoryName,
                                                      Quantity = collect.Count()
                                                  });
            foreach(var vehicleOrderDetailCollect in vehicleOrderDetailCollects) {
                this.VehicleOrderDetailCollects.Add(new VehicleOrderDetailCollect {
                    ProductId = vehicleOrderDetailCollect.ProductId,
                    ProductName = vehicleOrderDetailCollect.ProductName,
                    ProductCode = vehicleOrderDetailCollect.ProductCode,
                    ProductCategoryCode = vehicleOrderDetailCollect.ProductCategoryCode,
                    ProductCategoryName = vehicleOrderDetailCollect.ProductCategoryName,
                    Quantity = vehicleOrderDetailCollect.Quantity
                });
            }
        }

        private void GetVehicleFundsType(VehicleOrder vehicleOrder) {
            this.DomainContext.Load(this.DomainContext.GetVehicleFundsTypesQuery().Where(r => r.Id == vehicleOrder.VehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                var entity = loadOp.Entities.FirstOrDefault();
                if(entity != null)
                    vehicleOrder.VehicleFundsTypeName = entity.Name;
            }, null);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowLoadError(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
                this.GetVehicleCustomerAccountInfo(entity);
                this.GetVehicleFundsType(entity);
                this.CreateVehicleOrderDetailCollects(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            if(vehicleOrder.HasValidationErrors)
                return;
            ((IEditableObject)vehicleOrder).EndEdit();
            if(vehicleOrder.VehicleOrderDetails != null)
                foreach(var detail in vehicleOrder.VehicleOrderDetails) {
                    ((IEditableObject)detail).EndEdit();
                }

            try {
                if(vehicleOrder.Can提交整车订单)
                    vehicleOrder.提交整车订单();
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            this.VehicleOrderDetailCollects.Clear();
            base.OnEditSubmitted();
        }

        protected override void OnEditCancelled() {
            this.VehicleOrderDetailCollects.Clear();
            base.OnEditCancelled();
        }

        public VehicleOrderForInsteadSubmitDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
    }
}
