﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleOrderForSubmitDataEditView {
        private DataGridViewBase vehicleOrderDetailForEditDataGridView; //单车清单
        private DataGridViewBase vehicleOrderDetailCollectDataGridView; //订单清单
        private VehicleProductCategoryDataView vehicleProductCategoryDataView;
        private bool checkVehicleCustomerAccount = true;
        private bool isCanSubmit = true;
        private QueryWindowBase vehicleAvailableResourceQueryWindow;
        private RadWindow queryWindow;

        public QueryWindowBase VehicleAvailableResourceQueryWindow {
            get {
                return this.vehicleAvailableResourceQueryWindow ?? (this.vehicleAvailableResourceQueryWindow = DI.GetQueryWindow("VehicleAvailableResourceForVehicleOrder"));
            }
        }

        private RadWindow QueryWindow {
            get {
                return this.queryWindow ?? (this.queryWindow = new RadWindow {
                    CanMove = true,
                    CanClose = true,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    Header = VehicleSalesUIStrings.QueryPanel_Title_VehicleAvailableResource,
                    Content = this.VehicleAvailableResourceQueryWindow
                });
            }
        }

        private DataGridViewBase VehicleOrderDetailForEditDataGridView {
            get {
                if(this.vehicleOrderDetailForEditDataGridView == null) {
                    this.vehicleOrderDetailForEditDataGridView = DI.GetDataGridView("VehicleOrderDetailForEdit");
                    this.vehicleOrderDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleOrderDetailForEditDataGridView;
            }
        }

        private DataGridViewBase VehicleOrderDetailCollectsDataGridView {
            get {
                if(this.vehicleOrderDetailCollectDataGridView == null) {
                    this.vehicleOrderDetailCollectDataGridView = DI.GetDataGridView("VehicleOrderDetailCollectForEdit");
                    this.vehicleOrderDetailCollectDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleOrderDetailCollectDataGridView;
            }
        }

        public VehicleProductCategoryDataView VehicleProductCategoryDataView {
            get {
                if(this.vehicleProductCategoryDataView == null) {
                    this.vehicleProductCategoryDataView = new VehicleProductCategoryDataView();
                    this.vehicleProductCategoryDataView.OnDataGridRowDoubleClick += VehicleProductCategoryDataView_OnDataGridRowDoubleClick;
                }
                return this.vehicleProductCategoryDataView;
            }
        }

        private void VehicleProductCategoryDataView_OnDataGridRowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            var vehicleModelAffiProduct = VehicleProductCategoryDataView.GridViewSelectedItem as VehicleModelAffiProduct;
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null || vehicleModelAffiProduct == null)
                return;
            if(vehicleOrder.VehicleOrderDetailCollects.Sum(r => r.Quantity) > 4999) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderDetailCollect_QuantityGreaterThanFiveThousand);
                return;
            }
            if(vehicleOrder.VehicleOrderDetailCollects.Any(r => r.ProductId == vehicleModelAffiProduct.ProductId))
                DcsUtils.Confirm(string.Format(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderDetailCollect_ProductIsAlreadyExists, vehicleModelAffiProduct.ProductCode), () => vehicleOrder.VehicleOrderDetailCollects.First(r => r.ProductId == vehicleModelAffiProduct.ProductId).Quantity += 1);
            else {
                var vehicleOrderDetailCollect = this.CreateDefaultVehicleOrderDetailCollect(vehicleModelAffiProduct);
                vehicleOrderDetailCollect.PropertyChanged += this.VehicleOrderDetailCollect_PropertyChanged;
                vehicleOrderDetailCollect.Quantity = 1;
                vehicleOrder.VehicleOrderDetailCollects.Add(vehicleOrderDetailCollect);
                SetTotalAmount(vehicleOrder);
            }
        }

        private VehicleOrderDetailCollect CreateDefaultVehicleOrderDetailCollect(VehicleModelAffiProduct vehicleModelAffiProduct) {
            return new VehicleOrderDetailCollect {
                ProductId = vehicleModelAffiProduct.ProductId,
                ProductCode = vehicleModelAffiProduct.ProductCode,
                ProductName = vehicleModelAffiProduct.ProductName,
                ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode,
                ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName,
                Price = vehicleModelAffiProduct.WholesalePrice
            };
        }

        public VehicleOrderForSubmitDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.DataContextChanged += this.VehicleOrderForSubmitDataEditView_DataContextChanged;
        }

        private void VehicleOrderForSubmitDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            vehicleOrder.PropertyChanged += this.VehicleOrder_PropertyChanged;
            vehicleOrder.VehicleOrderDetailCollects.CollectionChanged += VehicleOrderDetailCollects_CollectionChanged;
        }

        private void VehicleOrder_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var vehicleOrder = sender as VehicleOrder;
            if(vehicleOrder == null)
                return;
            if(checkVehicleCustomerAccount)
                switch(e.PropertyName) {
                    case "VehicleFundsTypeId":
                    case "DealerId":
                        if(vehicleOrder.VehicleFundsTypeId != default(int) && vehicleOrder.DealerId != default(int))
                            if(this.DomainContext == null)
                                this.Initializer.Register(() => this.LoadAccountBalance(vehicleOrder));
                            else
                                this.LoadAccountBalance(vehicleOrder);
                        break;
                }
        }

        private void LoadAccountBalance(VehicleOrder vehicleOrder) {
            this.DomainContext.Load(this.DomainContext.GetVehicleCustomerAccountsQuery().Where(entity => entity.CustomerCompanyId == vehicleOrder.DealerId && entity.VehicleFundsTypeId == vehicleOrder.VehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity == null) {
                    vehicleOrder.Balance = default(decimal);
                    UIHelper.ShowAlertMessage(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrder_VehicleCustomerAccountIsNull);
                    this.SetViewStatus(false);
                } else {
                    vehicleOrder.Balance = entity.AccountBalance;
                    this.SetViewStatus(true);
                }
            }, null);
        }

        private void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("VehicleOrderForSubmit");
            this.LayoutRoot.Children.Add(dataEditPanel);
            this.LayoutRoot.Children.Add(this.CreateVerticalLine(1, 0, 0, 2));
            this.VehicleProductCategoryDataView.SetValue(Grid.ColumnProperty, 2);
            this.VehicleProductCategoryDataView.SetValue(Grid.RowSpanProperty, 2);
            this.VehicleProductCategoryDataView.SetValue(MarginProperty, new Thickness(0, 0, 0, 3));
            this.LayoutRoot.Children.Add(this.VehicleProductCategoryDataView);
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.Register(VehicleSalesUIStrings.DetailPanel_Title_VehicleOrderDetailCollects, null, () => this.VehicleOrderDetailCollectsDataGridView);
            detailEditView.Register(VehicleSalesUIStrings.DetailPanel_Title_VehicleOrderDetail, null, () => this.VehicleOrderDetailForEditDataGridView);
            detailEditView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(detailEditView);
            this.RegisterButton(new ButtonItem {
                Title = VehicleSalesUIStrings.DataEditView_Button_Common_DealerStockQuery,
                Command = new DelegateCommand(() => this.QueryWindow.ShowDialog()),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/query.png", UriKind.Relative)
            });
            this.Loaded += VehicleOrderForSubmitDataEditView_Loaded;
        }

        private void VehicleOrderForSubmitDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.VehicleProductCategoryDataView.RefreshTreeViewAndClearGridView(GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID);
        }

        private void VehicleOrderDetailCollects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if(e.Action == NotifyCollectionChangedAction.Remove) {
                var removeVehicleOrderDetailCollects = e.OldItems.Cast<VehicleOrderDetailCollect>().ToArray();
                var vehicleOrder = this.DataContext as VehicleOrder;
                if(vehicleOrder == null)
                    return;
                var vehicleOrderDetail = vehicleOrder.VehicleOrderDetails;
                if(vehicleOrderDetail == null)
                    return;
                foreach(var removeOrderDetailCollect in removeVehicleOrderDetailCollects) {
                    var productCode = removeOrderDetailCollect.ProductCode;
                    var details = vehicleOrderDetail.Where(entity => entity.ProductCode == productCode);
                    foreach(var orderDetail in details) {
                        vehicleOrderDetail.Remove(orderDetail);
                    }
                }
                SetTotalAmount(vehicleOrder);
            }
        }

        private void SetTotalAmount(VehicleOrder vehicleOrder) {
            vehicleOrder.TotalAmount = (from collect in vehicleOrder.VehicleOrderDetailCollects
                                        select collect.Price * collect.Quantity).Sum();
        }

        private void SetViewStatus(bool isEnabled) {
            foreach(var children in this.LayoutRoot.Children.Where(children => !(children is Rectangle))) {
                var vehicleOrderForDealerDataEditPanel = children as VehicleOrderForSubmitDataEditPanel;
                if(vehicleOrderForDealerDataEditPanel != null) {
                    var grid = vehicleOrderForDealerDataEditPanel.LayoutRoot;
                    foreach(var control in grid.Children.Where(control => !(control is DcsComboBox) && !(control is TextBlock))) {
                        control.SetValue(IsEnabledProperty, isEnabled);
                    }
                    continue;
                }
                isCanSubmit = isEnabled;
                children.SetValue(IsEnabledProperty, isEnabled);
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            if(!isCanSubmit) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrder_VehicleCustomerAccountIsNull);
                return;
            }
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            if(vehicleOrder.VehicleOrderDetailCollects.Any(r => r.Quantity < 1)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrder_VehicleOrderDetailCollect_QuantityLessThenOne);
                return;
            }
            if(vehicleOrder.VehicleOrderDetails == null || !vehicleOrder.VehicleOrderDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrder_VehicleOrderDetailIsNull);
                return;
            }
            if(vehicleOrder.VehicleOrderDetails.All(entity => entity.Price <= 0)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderDetail_PriceGreaterThanZero);
                return;
            }
            ((IEditableObject)vehicleOrder).EndEdit();
            if(vehicleOrder.VehicleOrderDetails != null)
                foreach(var detail in vehicleOrder.VehicleOrderDetails) {
                    ((IEditableObject)detail).EndEdit();
                }

            if(vehicleOrder.EntityState != EntityState.New) {
                try {
                    if(vehicleOrder.Can调整整车订单)
                        vehicleOrder.调整整车订单();
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            }
            this.SetViewStatus(true);
            base.OnEditSubmitting();
        }

        protected override void OnEditCancelled() {
            checkVehicleCustomerAccount = false;
            this.SetViewStatus(true);
            base.OnEditCancelled();
            checkVehicleCustomerAccount = true;
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleOrder;
            }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.LoadAccountBalance(entity);
                    var vehicleOrderDetails = entity.VehicleOrderDetails;
                    if(vehicleOrderDetails != null) {
                        entity.TotalAmount = vehicleOrderDetails.Sum(v => v.Price);
                        var orderDetailCollects = (from orderDetail in vehicleOrderDetails
                                                   group orderDetail by new {
                                                       orderDetail.ProductCode,
                                                       orderDetail.ProductCategoryCode,
                                                       orderDetail.ProductName,
                                                       orderDetail.ProductId,
                                                       orderDetail.ProductCategoryName,
                                                       orderDetail.Price
                                                   }
                                                       into collect
                                                       select new {
                                                           collect.Key.ProductCategoryCode,
                                                           collect.Key.ProductCode,
                                                           collect.Key.ProductId,
                                                           collect.Key.ProductName,
                                                           collect.Key.ProductCategoryName,
                                                           Quantity = collect.Count(),
                                                           collect.Key.Price
                                                       });
                        foreach(var vehicleOrderDetailCollect in orderDetailCollects.Select(orderDetailCollect => new VehicleOrderDetailCollect {
                            ProductId = orderDetailCollect.ProductId,
                            ProductName = orderDetailCollect.ProductName,
                            ProductCode = orderDetailCollect.ProductCode,
                            ProductCategoryCode = orderDetailCollect.ProductCategoryCode,
                            ProductCategoryName = orderDetailCollect.ProductCategoryName,
                            Price = orderDetailCollect.Price,
                            Quantity = orderDetailCollect.Quantity
                        })) {
                            vehicleOrderDetailCollect.PropertyChanged += this.VehicleOrderDetailCollect_PropertyChanged;
                            entity.VehicleOrderDetailCollects.Add(vehicleOrderDetailCollect);
                        }
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private void VehicleOrderDetailCollect_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(!e.PropertyName.Equals("Quantity"))
                return;
            var vehicleOrderDetailCollect = sender as VehicleOrderDetailCollect;
            if(vehicleOrderDetailCollect == null)
                return;
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            var vehicleOrderDetails = vehicleOrder.VehicleOrderDetails;
            if(vehicleOrderDetails == null)
                return;
            switch(e.PropertyName) {
                case "Quantity":
                    if(vehicleOrder.VehicleOrderDetailCollects.Sum(r => r.Quantity) > 5000) {
                        UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderDetailCollect_QuantityGreaterThanFiveThousand);
                        vehicleOrderDetailCollect.Quantity = vehicleOrder.VehicleOrderDetails.Where(r => r.ProductId == vehicleOrderDetailCollect.ProductId && r.ProductCategoryCode == vehicleOrderDetailCollect.ProductCategoryCode).ToArray().Count();
                        return;
                    }
                    var vehicleOrderDetailsCount = vehicleOrderDetails.Count(entity => entity.ProductId == vehicleOrderDetailCollect.ProductId);
                    if(vehicleOrderDetailsCount < vehicleOrderDetailCollect.Quantity) {
                        for(var i = 0; i < vehicleOrderDetailCollect.Quantity - vehicleOrderDetailsCount; i++) {
                            vehicleOrderDetails.Add(new VehicleOrderDetail {
                                ProductCode = vehicleOrderDetailCollect.ProductCode,
                                ProductId = vehicleOrderDetailCollect.ProductId,
                                ProductName = vehicleOrderDetailCollect.ProductName,
                                ProductCategoryCode = vehicleOrderDetailCollect.ProductCategoryCode,
                                ProductCategoryName = vehicleOrderDetailCollect.ProductCategoryName,
                                SON = GlobalVar.ASSIGNED_BY_SERVER,
                                Status = (int)DcsVehicleOrderDetailStatus.新建,
                                Price = vehicleOrderDetailCollect.Price
                            });
                        }
                    } else if(vehicleOrderDetailsCount > vehicleOrderDetailCollect.Quantity) {
                        for(var i = 0; i < (vehicleOrderDetailsCount - vehicleOrderDetailCollect.Quantity); i++) {
                            vehicleOrderDetails.Remove(vehicleOrderDetails.OrderBy(entity => entity.SON).Last(entity => entity.ProductId == vehicleOrderDetailCollect.ProductId));
                        }
                    }
                    SetTotalAmount(vehicleOrder);
                    break;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}