﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleOrderForSubmitDetailDataEditView {
        private ObservableCollection<VehicleOrderDetailCollect> vehicleOrderDetailCollects;
        private DataGridViewBase vehicleOrderDetailForSubmitDataGridView;//单车清单
        private DataGridViewBase vehicleOrderDetailCollectDataGridView;//订单清单

        private DataGridViewBase VehicleOrderDetailForSubmitDataGridView {
            get {
                if(this.vehicleOrderDetailForSubmitDataGridView == null) {
                    this.vehicleOrderDetailForSubmitDataGridView = DI.GetDataGridView("VehicleOrderDetailForSubmit");
                    this.vehicleOrderDetailForSubmitDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleOrderDetailForSubmitDataGridView;
            }
        }

        private DataGridViewBase VehicleOrderDetailCollectDataGridView {
            get {
                if(this.vehicleOrderDetailCollectDataGridView == null) {
                    this.vehicleOrderDetailCollectDataGridView = DI.GetDataGridView("VehicleOrderDetailCollect");
                    this.vehicleOrderDetailCollectDataGridView.DomainContext = this.DomainContext;
                    this.vehicleOrderDetailCollectDataGridView.SetValue(DataContextProperty, this);
                }
                return this.vehicleOrderDetailCollectDataGridView;
            }
        }

        public VehicleOrderForSubmitDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Title = VehicleSalesUIStrings.DataEditView_Title_VehicleOrderForSubmit;

            var dataEditPanel = DI.GetDataEditPanel("VehicleOrderForSubmitDetail");
            this.LayoutRoot.Children.Add(dataEditPanel);

            var tabControl = new RadTabControl();
            var tabItemVehicleOrderDetailCollects = new RadTabItem {
                Content = this.VehicleOrderDetailCollectDataGridView,
                Header = VehicleSalesUIStrings.DetailPanel_Title_VehicleOrderDetailCollects
            };
            tabControl.Items.Add(tabItemVehicleOrderDetailCollects);

            var tabItemVehicleOrderDetailForSubmit = new RadTabItem {
                Content = this.VehicleOrderDetailForSubmitDataGridView,
                Header = VehicleSalesUIStrings.DetailPanel_Title_VehicleOrderDetail
            };
            tabControl.Items.Add(tabItemVehicleOrderDetailForSubmit);
            tabControl.SetValue(Grid.ColumnProperty, 1);
            this.LayoutRoot.Children.Add(tabControl);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleOrderWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.VehicleOrderDetailCollects.Clear();
                    var vehicleOrderDetails = entity.VehicleOrderDetails;
                    if(vehicleOrderDetails != null) {
                        var orderDetailCollects = (from orderDetail in vehicleOrderDetails
                                                   group orderDetail by new {
                                                       orderDetail.ProductCode,
                                                       orderDetail.ProductCategoryCode
                                                   }
                                                       into collect
                                                       select new {
                                                           collect.Key.ProductCategoryCode,
                                                           collect.Key.ProductCode,
                                                           Quantity = collect.Count()
                                                       });
                        foreach(var orderDetailCollect in orderDetailCollects) {
                            this.VehicleOrderDetailCollects.Add(new VehicleOrderDetailCollect {
                                ProductCode = orderDetailCollect.ProductCode,
                                ProductCategoryCode = orderDetailCollect.ProductCategoryCode,
                                Quantity = orderDetailCollect.Quantity
                            });
                        }
                    }
                    this.DomainContext.Load(this.DomainContext.GetVehicleCustomerAccountsQuery().Where(v => v.CustomerCompanyId == entity.DealerId && v.VehicleFundsTypeId == entity.VehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var vehicleCustomerAccount = loadOp2.Entities.SingleOrDefault();
                        if(vehicleCustomerAccount != null)
                            entity.Balance = vehicleCustomerAccount.AccountBalance;
                    }, null);
                    this.DomainContext.Load(this.DomainContext.GetVehicleFundsTypesQuery().Where(v => v.Id == entity.VehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp2 => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var vehicleFundsType = loadOp2.Entities.SingleOrDefault();
                        if(vehicleFundsType != null)
                            entity.VehicleFundsTypeName = vehicleFundsType.Name;
                    }, null);
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            try {
                if(vehicleOrder.Can提交整车订单)
                    vehicleOrder.提交整车订单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            this.VehicleOrderDetailCollects.Clear();
            base.OnEditSubmitting();
        }

        protected override void OnEditCancelled() {
            this.VehicleOrderDetailCollects.Clear();
            base.OnEditCancelled();
        }

        public ObservableCollection<VehicleOrderDetailCollect> VehicleOrderDetailCollects {
            get {
                return this.vehicleOrderDetailCollects ?? (this.vehicleOrderDetailCollects = new ObservableCollection<VehicleOrderDetailCollect>());
            }
        }
    }
}
