﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleOrderPlanDataEditView {
        private DataGridViewBase vehicleOrderPlanDetailForEditDataGridView;
        private VehicleProductCategoryDataView vehicleProductCategoryDataView;
        private ObservableCollection<KeyValuePair> kvYearOfPlans;
        private ObservableCollection<KeyValuePair> kvMonthOfPlans;
        private Dictionary<int, List<int>> dictionary;

        public VehicleOrderPlanDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += VehicleOrderPlanDataEditView_Loaded;
            this.DataContextChanged += VehicleOrderPlanDataEditView_DataContextChanged;
        }

        private Dictionary<int, List<int>> Dictionary {
            get {
                return this.dictionary ?? (this.dictionary = new Dictionary<int, List<int>>());
            }
        }

        public ObservableCollection<KeyValuePair> KvYearOfPlans {
            get {
                return this.kvYearOfPlans ?? (this.kvYearOfPlans = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvMonthOfPlans {
            get {
                return this.kvMonthOfPlans ?? (this.kvMonthOfPlans = new ObservableCollection<KeyValuePair>());
            }
        }

        private void VehicleOrderPlanDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if(!(this.DataContext is VehicleOrderPlan))
                return;
            var vehicleOrderPlanDataEditView = this.DataContext as VehicleOrderPlan;
            vehicleOrderPlanDataEditView.PropertyChanged -= this.VehicleOrderPlanDataEditViewPropertyChanged;
            vehicleOrderPlanDataEditView.PropertyChanged += this.VehicleOrderPlanDataEditViewPropertyChanged;
        }

        private void VehicleOrderPlanDataEditViewPropertyChanged(object sender, PropertyChangedEventArgs e) {
            var vehicleOrderPlanDataContext = this.DataContext as VehicleOrderPlan;
            if(vehicleOrderPlanDataContext == null || vehicleOrderPlanDataContext.VehicleSalesOrgId == default(int))
                return;
            switch(e.PropertyName) {
                case "VehicleSalesOrgId":
                    this.VehicleProductCategoryDataView.RefreshTreeViewAndClearGridView(vehicleOrderPlanDataContext.VehicleSalesOrgId);
                    break;
                case "YearOfPlan":
                case "MonthOfPlan":
                    if(vehicleOrderPlanDataContext.YearOfPlan != default(int) && vehicleOrderPlanDataContext.MonthOfPlan != default(int)) {
                        vehicleOrderPlanDataContext.StartTime = new DateTime(vehicleOrderPlanDataContext.YearOfPlan, vehicleOrderPlanDataContext.MonthOfPlan, 1);
                        vehicleOrderPlanDataContext.EndTime = vehicleOrderPlanDataContext.StartTime.AddMonths(1).AddDays(-1);
                    }
                    break;
            }
        }

        private void VehicleOrderPlanDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.查询可生成订货计划的月份(OnGetYearAndMonth, null);
        }

        private void OnGetYearAndMonth(InvokeOperation<int[]> invOp) {
            if(invOp.HasError) {
                UIHelper.ShowAlertMessage(invOp.Error.Message);
                invOp.MarkErrorAsHandled();
            } else {
                int[] time = invOp.Value;
                KvYearOfPlans.Clear();
                KvMonthOfPlans.Clear();
                this.Dictionary.Clear();
                for(int yearIndex = time[1]; yearIndex <= time[3]; yearIndex++) {
                    if(yearIndex == default(int))
                        continue;
                    KvYearOfPlans.Add(new KeyValuePair {
                        Key = yearIndex,
                        Value = yearIndex.ToString(CultureInfo.InvariantCulture).PadLeft(4, '0')
                    });

                    List<int> monthList;
                    //处理最小年对应的月份,不跨年时，最大年和最小年相等，都在最小年之内
                    if(yearIndex == time[1] && time[1] != default(int)) {
                        monthList = new List<int>();
                        //time[1]==time[3]表示不跨年，此时最大月份就是循环结束的约束
                        var endIndex = time[1] == time[3] ? time[2] : 12;
                        for(int monthIndex = time[0]; monthIndex <= endIndex; monthIndex++) {
                            if(monthIndex == default(int))
                                continue;
                            monthList.Add(monthIndex);
                        }

                        this.Dictionary.Add(yearIndex, monthList);
                    }
                    //处理最小年月最大年之间对应年的月份
                    if(yearIndex < time[3] && yearIndex > time[1] && time[1] != default(int)) {
                        monthList = new List<int>();
                        for(int monthIndex = 1; monthIndex <= 12; monthIndex++) {
                            if(monthIndex == default(int))
                                continue;
                            monthList.Add(monthIndex);
                        }

                        this.Dictionary.Add(yearIndex, monthList);
                    }

                    //处理最大年对应的月份
                    if(yearIndex == time[3] && yearIndex > time[1] && time[1] != default(int)) {
                        monthList = new List<int>();
                        for(int monthIndex = 1; monthIndex <= time[2]; monthIndex++) {
                            if(monthIndex == default(int))
                                continue;
                            monthList.Add(monthIndex);
                        }
                        this.Dictionary.Add(yearIndex, monthList);
                    }
                }
            }
            this.DomainContext.IsInvoking = false;
            //此处代码只针对修改模块
            var vehicleOrderPlan = this.DataContext as VehicleOrderPlan;
            if(vehicleOrderPlan == null || vehicleOrderPlan.EntityState == EntityState.New)
                return;
            //在修改的时候动态设定月份集合的绑定
            this.SetMonthsValue(vehicleOrderPlan.YearOfPlan);
        }

        private DataGridViewBase VehicleOrderPlanDetailForEditDataGridView {
            get {
                if(this.vehicleOrderPlanDetailForEditDataGridView == null) {
                    this.vehicleOrderPlanDetailForEditDataGridView = DI.GetDataGridView("VehicleOrderPlanDetailForEdit");
                    this.vehicleOrderPlanDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleOrderPlanDetailForEditDataGridView;
            }
        }

        private VehicleProductCategoryDataView VehicleProductCategoryDataView {
            get {
                if(this.vehicleProductCategoryDataView == null) {
                    this.vehicleProductCategoryDataView = new VehicleProductCategoryDataView();
                    this.vehicleProductCategoryDataView.OnDataGridRowDoubleClick += vehicleProductCategoryDataView_OnDataGridRowDoubleClick;
                }
                return vehicleProductCategoryDataView;
            }
        }

        private void vehicleProductCategoryDataView_OnDataGridRowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            var vehicleModelAffiProduct = VehicleProductCategoryDataView.GridViewSelectedItem as VehicleModelAffiProduct;
            var vehicleOrderPlan = this.DataContext as VehicleOrderPlan;
            if(vehicleOrderPlan == null || vehicleModelAffiProduct == null)
                return;
            if(vehicleOrderPlan.VehicleOrderPlanDetails.Any(r => r.ProductId == vehicleModelAffiProduct.ProductId))
                return;
            vehicleOrderPlan.VehicleOrderPlanDetails.Add(new VehicleOrderPlanDetail {
                ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode,
                ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName,
                ProductId = vehicleModelAffiProduct.ProductId,
                ProductName = vehicleModelAffiProduct.ProductName,
                ProductCode = vehicleModelAffiProduct.ProductCode,
                PlannedQuantity = 1
            });
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(VehicleOrderPlan), "VehicleOrderPlanDetails"), null, this.VehicleOrderPlanDetailForEditDataGridView);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(detailDataEditView);
            var queryWindowDealer = DI.GetQueryWindow("Dealer");
            queryWindowDealer.SelectionDecided += this.dealerQueryWindowow_SelectionDecided;
            this.popupTextBoxDealerCode.PopupContent = queryWindowDealer;
            this.VehicleProductCategoryDataView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(VehicleProductCategoryDataView);
        }

        private void dealerQueryWindowow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var dealer = queryWindow.SelectedEntities.Cast<Dealer>().FirstOrDefault();
            var vehicleOrderPlan = this.DataContext as VehicleOrderPlan;
            if(dealer == null || vehicleOrderPlan == null)
                return;
            vehicleOrderPlan.DealerId = dealer.Id;
            vehicleOrderPlan.DealerCode = dealer.Code;
            vehicleOrderPlan.DealerName = dealer.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void SetMonthsValue(int yearIndex) {
            if(this.Dictionary.ContainsKey(yearIndex))
                foreach(var month in this.Dictionary[yearIndex]) {
                    this.KvMonthOfPlans.Add(new KeyValuePair {
                        Key = month,
                        Value = month.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0')
                    });
                }
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleOrderPlansWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    this.VehicleProductCategoryDataView.RefreshTreeViewAndClearGridView(entity.VehicleSalesOrgId);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            var vehicleOrderPlan = this.DataContext as VehicleOrderPlan;
            if(vehicleOrderPlan == null)
                return;
            if(!VehicleOrderPlanDetailForEditDataGridView.CommitEdit())
                return;
            vehicleOrderPlan.ValidationErrors.Clear();
            if(vehicleOrderPlan.YearOfPlan == default(int))
                vehicleOrderPlan.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderPlan_YearOfPlanIsNull, new[] {
                    "YearOfPlan"
                }));
            if(vehicleOrderPlan.MonthOfPlan == default(int))
                vehicleOrderPlan.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderPlan_MonthOfPlanIsNull, new[] {
                    "MonthOfPlan"
                }));
            if(string.IsNullOrEmpty(vehicleOrderPlan.DealerName))
                vehicleOrderPlan.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderPlan_DealerNameIsNull, new[] {
                    "DealerName"
                }));
            if(string.IsNullOrEmpty(vehicleOrderPlan.DealerCode))
                vehicleOrderPlan.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderPlan_DealerCodeIsNull, new[] {
                    "DealerCode"
                }));
            if(!vehicleOrderPlan.VehicleOrderPlanDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderPlan_VehicleOrderPlanDetailsIsNull);
                return;
            }
            if(vehicleOrderPlan.HasValidationErrors)
                return;
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleOrderPlan;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        private void ComboBox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            KvMonthOfPlans.Clear();
            if(e.AddedItems == null || e.AddedItems.Count < 1)
                return;
            var keyValuePair = e.AddedItems[0] as KeyValuePair;
            if(keyValuePair == null)
                return;
            this.SetMonthsValue(keyValuePair.Key);
        }
    }
}
