﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleOrderPlanDetailCollectDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private string yearOfPlan = null;
        private string monthOfPlan = null;
        private int year, month;
        private string dealerName;
        private ICommand searchCommand;
        private DataGridViewBase vehicleOrderPlanDetailForCollectDataGridView;
        private ObservableCollection<VehicleOrderPlanDetail> vehicleOrderPlanDetails;

        public VehicleOrderPlanDetailCollectDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private DataGridViewBase VehicleOrderPlanDetailForCollectDataGridView {
            get {
                if(this.vehicleOrderPlanDetailForCollectDataGridView == null) {
                    this.vehicleOrderPlanDetailForCollectDataGridView = DI.GetDataGridView("VehicleOrderPlanDetailForCollect");
                    this.vehicleOrderPlanDetailForCollectDataGridView.DomainContext = this.DomainContext;
                    this.vehicleOrderPlanDetailForCollectDataGridView.DataContext = this;
                }
                return this.vehicleOrderPlanDetailForCollectDataGridView;
            }
        }

        public ObservableCollection<VehicleOrderPlanDetail> VehicleOrderPlanDetails {
            get {
                if(this.vehicleOrderPlanDetails == null) {
                    this.vehicleOrderPlanDetails = new ObservableCollection<VehicleOrderPlanDetail>();
                }
                return vehicleOrderPlanDetails;
            }
            set {
                vehicleOrderPlanDetails = value;
                this.OnPropertyChanged("VehicleOrderPlanDetails");
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public ICommand SearchCommand {
            get {
                return this.searchCommand ?? (this.searchCommand = new Core.Command.DelegateCommand(this.Search));
            }
        }

        private void CreateUI() {
            this.HideSaveButton();
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = VehicleSalesUIStrings.DataGridView_Title_VehicleOrderPlanDetailCollect,
                Content = this.VehicleOrderPlanDetailForCollectDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
        }


        private void Search() {
            int.TryParse(yearOfPlan, out year);
            int.TryParse(monthOfPlan, out month);
            this.DomainContext.Load(this.DomainContext.查询整车订货计划清单Query(year, month, this.DealerName), LoadBehavior.RefreshCurrent, loadOp => {//this.CutoffTime
                if(loadOp.HasError || loadOp.Entities == null)
                    return;
                this.VehicleOrderPlanDetails.Clear();
                this.VehicleOrderPlanDetails = new ObservableCollection<VehicleOrderPlanDetail>(loadOp.Entities);
                this.Collect();
            }, null);
        }

        //汇总事件
        private void Collect() {
            var mergeDetail = from detail in VehicleOrderPlanDetails
                              group detail by new {
                                  detail.ProductCode,
                                  detail.VehicleOrderPlan.YearOfPlan,
                                  detail.VehicleOrderPlan.MonthOfPlan
                              } into g
                              select new {
                                  g.Key.ProductCode,
                                  g.Key.YearOfPlan,
                                  g.Key.MonthOfPlan
                              };
            var mergeCollection = new ObservableCollection<VehicleOrderPlanDetail>();
            foreach(var detailItem in mergeDetail) {
                var item = detailItem;
                var details = this.VehicleOrderPlanDetails.Where(e => e.ProductCode == item.ProductCode);
                var vehicleOrderPlanDetails = details as VehicleOrderPlanDetail[] ?? details.ToArray();
                if(vehicleOrderPlanDetails.Any()) {
                    mergeCollection.Add(new VehicleOrderPlanDetail {
                        ProductCode = vehicleOrderPlanDetails.First().ProductCode,
                        VehicleOrderPlan = new VehicleOrderPlan {
                            YearOfPlan = vehicleOrderPlanDetails.First().VehicleOrderPlan.YearOfPlan,
                            MonthOfPlan = vehicleOrderPlanDetails.First().VehicleOrderPlan.MonthOfPlan
                        },
                        ProductCategoryCode = vehicleOrderPlanDetails.First().ProductCategoryCode,
                        PlannedQuantity = vehicleOrderPlanDetails.Sum(e => e.PlannedQuantity),
                    });
                }
            }
            this.VehicleOrderPlanDetails = new ObservableCollection<VehicleOrderPlanDetail>(mergeCollection);
            foreach(var vehicleOrderPlan in this.DomainContext.VehicleOrderPlans.Where(entity => entity.EntityState == EntityState.New).ToList())
                this.DomainContext.VehicleOrderPlans.Remove(vehicleOrderPlan);
        }

        public string YearOfPlan {
            get {
                return this.yearOfPlan;
            }
            set {
                this.yearOfPlan = value;
                this.OnPropertyChanged("YearOfPlan");
            }
        }

        public string MonthOfPlan {
            get {
                return this.monthOfPlan;
            }
            set {
                this.monthOfPlan = value;
                this.OnPropertyChanged("MonthOfPlan");
            }
        }

        public string DealerName {
            get {
                return this.dealerName;
            }
            set {
                this.dealerName = value;
                this.OnPropertyChanged("DealerName");
            }
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_Collect_VehicleOrderPlanDetail;
            }
        }

        protected override void OnEditCancelled() {
            this.VehicleOrderPlanDetails.Clear();
            this.DealerName = "";
            this.MonthOfPlan = null;
            this.YearOfPlan = null;
            base.OnEditCancelled();
        }
    }
}
