﻿
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleOrderPlanDetailDataEditView : INotifyPropertyChanged {
        private ICommand searchCommand;
        public event PropertyChangedEventHandler PropertyChanged;
        private string yearOfPlan;
        private string monthOfPlan;
        private int year, month;
        private string dealerName;
        private DataGridViewBase vehicleOrderPlanDetailForDetailsDataGridView;
        private ObservableCollection<VehicleOrderPlanDetail> vehicleOrderPlanDetails;

        private DataGridViewBase VehicleOrderPlanDetailForDetailsDataGridView {
            get {
                if(this.vehicleOrderPlanDetailForDetailsDataGridView == null) {
                    this.vehicleOrderPlanDetailForDetailsDataGridView = DI.GetDataGridView("VehicleOrderPlanDetailForDetails");
                    this.vehicleOrderPlanDetailForDetailsDataGridView.DomainContext = this.DomainContext;
                    this.vehicleOrderPlanDetailForDetailsDataGridView.DataContext = this;
                }
                return this.vehicleOrderPlanDetailForDetailsDataGridView;
            }
        }

        public ObservableCollection<VehicleOrderPlanDetail> VehicleOrderPlanDetails {
            get {
                if(this.vehicleOrderPlanDetails == null) {
                    this.vehicleOrderPlanDetails = new ObservableCollection<VehicleOrderPlanDetail>();
                }
                return vehicleOrderPlanDetails;
            }
            set {
                vehicleOrderPlanDetails = value;
                this.OnPropertyChanged("VehicleOrderPlanDetails");
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public VehicleOrderPlanDetailDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
        public ICommand SearchCommand {
            get {
                return this.searchCommand ?? (this.searchCommand = new Core.Command.DelegateCommand(this.Search));
            }
        }

        private void CreateUI() {
            this.HideSaveButton();
            var tabControl = new RadTabControl();
            tabControl.BackgroundVisibility = Visibility.Collapsed;
            tabControl.Items.Add(new RadTabItem {
                Header = VehicleSalesUIStrings.DataGridView_Title_VehicleOrderPlanDetail,
                Content = this.VehicleOrderPlanDetailForDetailsDataGridView
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(tabControl);
        }

        private void Search() {
            int.TryParse(yearOfPlan, out year);
            int.TryParse(monthOfPlan, out month);
            this.DomainContext.Load(this.DomainContext.查询整车订货计划清单Query(year, month, this.DealerName), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError || loadOp.Entities == null)
                    return;
                this.VehicleOrderPlanDetails.Clear();
                this.VehicleOrderPlanDetails = new ObservableCollection<VehicleOrderPlanDetail>(loadOp.Entities);
            }, null);
        }


        public string YearOfPlan {
            get {
                return this.yearOfPlan;
            }
            set {
                this.yearOfPlan = value;
                this.OnPropertyChanged("YearOfPlan");
            }
        }

        public string MonthOfPlan {
            get {
                return this.monthOfPlan;
            }
            set {
                this.monthOfPlan = value;
                this.OnPropertyChanged("MonthOfPlan");
            }
        }

        public string DealerName {
            get {
                return this.dealerName;
            }
            set {
                this.dealerName = value;
                this.OnPropertyChanged("DealerName");
            }
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_Detail_VehicleOrderPlanDetail;
            }
        }

        protected override void OnEditCancelled() {
            this.VehicleOrderPlanDetails.Clear();
            this.DealerName = "";
            this.MonthOfPlan = null;
            this.YearOfPlan = null;
            base.OnEditCancelled();
        }
    }
}
