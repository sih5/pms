﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.Custom;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleOrderPlanForSubmitDataEditView {
        private DataGridViewBase vehicleOrderPlanDetailForEditDataGridView;
        private VehicleProductCategoryDataView vehicleProductCategoryDataView;
        private ObservableCollection<KeyValuePair> kvYearOfPlans;
        private ObservableCollection<KeyValuePair> kvMonthOfPlans;
        private Dictionary<int, List<int>> dictionary;

        public VehicleOrderPlanForSubmitDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += VehicleOrderPlanForSupplierDataEditView_Loaded;
            this.DataContextChanged += VehicleOrderPlanForSupplierDataEditView_DataContextChanged;
        }

        private Dictionary<int, List<int>> Dictionary {
            get {
                return this.dictionary ?? (this.dictionary = new Dictionary<int, List<int>>());
            }
        }

        private void VehicleOrderPlanForSupplierDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if(!(this.DataContext is VehicleOrderPlan))
                return;
            var vehicleOrderPlanForSupplierDataEditView = this.DataContext as VehicleOrderPlan;
            vehicleOrderPlanForSupplierDataEditView.PropertyChanged -= this.VehicleOrderPlanForSupplierDataEditViewPropertyChanged;
            vehicleOrderPlanForSupplierDataEditView.PropertyChanged += this.VehicleOrderPlanForSupplierDataEditViewPropertyChanged;
        }

        private void VehicleOrderPlanForSupplierDataEditViewPropertyChanged(object sender, PropertyChangedEventArgs e) {
            var vehicleOrderPlan = this.DataContext as VehicleOrderPlan;
            if(vehicleOrderPlan == null || vehicleOrderPlan.VehicleSalesOrgId == default(int))
                return;
            switch(e.PropertyName) {
                case "VehicleSalesOrgId":
                    this.VehicleProductCategoryDataView.RefreshTreeViewAndClearGridView(vehicleOrderPlan.VehicleSalesOrgId);
                    break;
                case "YearOfPlan":
                case "MonthOfPlan":
                    if(vehicleOrderPlan.YearOfPlan != default(int) && vehicleOrderPlan.MonthOfPlan != default(int)) {
                        vehicleOrderPlan.StartTime = new DateTime(vehicleOrderPlan.YearOfPlan, vehicleOrderPlan.MonthOfPlan, 1);
                        vehicleOrderPlan.EndTime = vehicleOrderPlan.StartTime.AddMonths(1).AddDays(-1);
                    }
                    break;
            }
        }

        public ObservableCollection<KeyValuePair> KvYearOfPlans {
            get {
                return this.kvYearOfPlans ?? (this.kvYearOfPlans = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvMonthOfPlans {
            get {
                return this.kvMonthOfPlans ?? (this.kvMonthOfPlans = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleOrderPlan;
            }
        }

        private DataGridViewBase VehicleOrderPlanDetailForEditDataGridView {
            get {
                if(this.vehicleOrderPlanDetailForEditDataGridView == null) {
                    this.vehicleOrderPlanDetailForEditDataGridView = DI.GetDataGridView("VehicleOrderPlanDetailForEdit");
                    this.vehicleOrderPlanDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleOrderPlanDetailForEditDataGridView;
            }
        }

        private VehicleProductCategoryDataView VehicleProductCategoryDataView {
            get {
                if(this.vehicleProductCategoryDataView == null) {
                    this.vehicleProductCategoryDataView = new VehicleProductCategoryDataView();
                    this.vehicleProductCategoryDataView.OnDataGridRowDoubleClick += vehicleProductCategoryDataView_OnDataGridRowDoubleClick;
                }
                return vehicleProductCategoryDataView;
            }
        }

        private void vehicleProductCategoryDataView_OnDataGridRowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            var vehicleModelAffiProduct = VehicleProductCategoryDataView.GridViewSelectedItem as VehicleModelAffiProduct;
            var vehicleOrderPlan = this.DataContext as VehicleOrderPlan;
            if(vehicleOrderPlan == null || vehicleModelAffiProduct == null)
                return;
            if(vehicleOrderPlan.VehicleOrderPlanDetails.Any(r=>r.ProductId==vehicleModelAffiProduct.ProductId))
                return;
            vehicleOrderPlan.VehicleOrderPlanDetails.Add(new VehicleOrderPlanDetail {
                ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode,
                ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName,
                ProductId = vehicleModelAffiProduct.ProductId,
                ProductName = vehicleModelAffiProduct.ProductName,
                ProductCode = vehicleModelAffiProduct.ProductCode,
                PlannedQuantity = 1
            });
        }

        private void CreateUI() {
            this.VehicleProductCategoryDataView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(VehicleProductCategoryDataView);
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(VehicleOrderPlan), "VehicleOrderPlanDetails"), null, () => this.VehicleOrderPlanDetailForEditDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 2);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            this.Root.Children.Add(detailDataEditView);
        }

        private void VehicleOrderPlanForSupplierDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.查询可生成订货计划的月份(OnGetYearAndMonth, null);
        }

        private void OnGetYearAndMonth(InvokeOperation<int[]> invOp) {
            if(invOp.HasError) {
                UIHelper.ShowAlertMessage(invOp.Error.Message);
                invOp.MarkErrorAsHandled();
            } else {
                int[] time = invOp.Value;
                KvYearOfPlans.Clear();
                KvMonthOfPlans.Clear();
                this.Dictionary.Clear();
                for(int yearIndex = time[1]; yearIndex <= time[3]; yearIndex++) {
                    if(yearIndex==default(int))
                        continue;
                    KvYearOfPlans.Add(new KeyValuePair {
                        Key = yearIndex,
                        Value = yearIndex.ToString(CultureInfo.InvariantCulture).PadLeft(4, '0')
                    });

                    List<int> monthList;
                    //处理最小年对应的月份
                    if(yearIndex == time[1] && time[1] != default(int)) {
                        monthList = new List<int>();
                        //time[1]==time[3]表示不跨年，此时最大约就是循环结束的约束
                            var endIndex = time[1] == time[3] ? time[2] : 12;
                        for(int monthIndex = time[0]; monthIndex <= endIndex; monthIndex++) {
                            if(monthIndex == default(int))
                                continue;
                            monthList.Add(monthIndex);
                        }

                        this.Dictionary.Add(yearIndex, monthList);
                    }
                    //处理最小年月最大年之间对应年的月份
                    if(yearIndex < time[3] && yearIndex > time[1] && time[1] != default(int)) {
                        monthList = new List<int>();
                        for(int monthIndex = 1; monthIndex <= 12; monthIndex++) {
                            if(monthIndex == default(int))
                                continue;
                            monthList.Add(monthIndex); 
                        }
                           
                        this.Dictionary.Add(yearIndex, monthList);
                    }

                    //处理最大年对应的月份
                    if(yearIndex == time[3] && yearIndex > time[1] && time[1] != default(int)) {
                        monthList = new List<int>();
                        for(int monthIndex = 1; monthIndex <= time[2]; monthIndex++) {
                            if(monthIndex == default(int))
                                continue;
                            monthList.Add(monthIndex);
                        }
                        this.Dictionary.Add(yearIndex, monthList);
                    }
                }

            }
            this.DomainContext.IsInvoking = false;
            //此处代码只针对修改模块
            var vehicleOrderPlan = this.DataContext as VehicleOrderPlan;
            if(vehicleOrderPlan == null || vehicleOrderPlan.EntityState == EntityState.New)
                return;
            //在修改的时候动态设定月份集合的绑定
            this.SetMonthsValue(vehicleOrderPlan.YearOfPlan);
        }

        private void SetMonthsValue(int yearIndex) {
            if(this.Dictionary.ContainsKey(yearIndex))
                foreach(var month in this.Dictionary[yearIndex]) {
                    this.KvMonthOfPlans.Add(new KeyValuePair {
                        Key = month,
                        Value = month.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0')
                    });
                }
        }

        protected override void OnEditSubmitting() {
            var vehicleOrderPlan = this.DataContext as VehicleOrderPlan;
            if(vehicleOrderPlan == null || !this.VehicleOrderPlanDetailForEditDataGridView.CommitEdit())
                return;
            vehicleOrderPlan.ValidationErrors.Clear();
            if(!vehicleOrderPlan.VehicleOrderPlanDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderPlan_VehicleOrderPlanDetailsIsNull);
                return;
            }
            if(vehicleOrderPlan.YearOfPlan == default(int))
                vehicleOrderPlan.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderPlan_YearOfPlanIsNull, new[]{
                    "YearOfPlan"
                }));
            if(vehicleOrderPlan.MonthOfPlan == default(int))
                vehicleOrderPlan.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderPlan_MonthOfPlanIsNull, new[]{
                    "MonthOfPlan"
                }));
            if(vehicleOrderPlan.ValidationErrors.Any())
                return;
            ((IEditableObject)vehicleOrderPlan).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleOrderPlansWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                    this.VehicleProductCategoryDataView.RefreshTreeViewAndClearGridView(entity.VehicleSalesOrgId);
                }
            }, null);
        }

        private void ComboBox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e) {
            KvMonthOfPlans.Clear();
            if(e.AddedItems == null || e.AddedItems.Count < 1)
                return;
            var keyValuePair = e.AddedItems[0] as KeyValuePair;
            if(keyValuePair == null)
                return;
            this.SetMonthsValue(keyValuePair.Key);
        }
    }
}
