﻿using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleOrderPlanSummaryForDelImportDataEditView {

        private DataGridViewBase vehicleOrderPlanSummaryForDelImportDataGridView;
        private ICommand exportFileCommand;

        public VehicleOrderPlanSummaryForDelImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected DataGridViewBase VehicleOrderPlanSummaryForDelImportDataGridView {
            get {
                return this.vehicleOrderPlanSummaryForDelImportDataGridView ?? (this.vehicleOrderPlanSummaryForDelImportDataGridView = DI.GetDataGridView("VehicleOrderPlanSummaryForDelImport"));
            }
        }

        private void CreateUI() {
            VehicleOrderPlanSummaryForDelImportDataGridView.SetValue(Grid.RowProperty, 1);
            this.Root.Children.Add(VehicleOrderPlanSummaryForDelImportDataGridView);
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }


        private void UploadFileProcessing(string fileName) {
            this.ExcelServiceClient.ImportVehicleOrderPlanSummaryDelAsync(fileName);
            this.ExcelServiceClient.ImportVehicleOrderPlanSummaryDelCompleted -= this.ExcelServiceClient_ImportVehicleOrderPlanSummaryDelCompleted;
            this.ExcelServiceClient.ImportVehicleOrderPlanSummaryDelCompleted += this.ExcelServiceClient_ImportVehicleOrderPlanSummaryDelCompleted;
        }

        private void ExcelServiceClient_ImportVehicleOrderPlanSummaryDelCompleted(object sender, ImportVehicleOrderPlanSummaryDelCompletedEventArgs e) {
            //调用父类方法标记导入完成
            this.ImportComplete();
            //只要返回错误数据 或者 错误信息 或者 错误信息的文件名。都标记为导入存在错误
            this.HasImportingError = this.CanExportUse || (!string.IsNullOrEmpty(e.errorDataFileName)) || (!string.IsNullOrWhiteSpace(e.errorMessage));
            if(!this.HasImportingError) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImport);
            } else {
                //if(e.errorData != null && e.errorData.Any())
                //    // 如果返回错误数据，绑定到数据表格中。
                //    this.ErrorData = e.errorData;
                if(this.HasImportingError)
                    // //1.如果直接导出错误数据文件，直接调用导出方法。传入文件名
                    this.ExportFile(e.errorDataFileName);
                // //2.如果需要通过导出按钮，导出错误信息文件。设置ErrorFileName为服务端返回的文件名称。再按照 CreateUI中导出按钮设置方法1 来设置即可。
                //this.ErrorFileName = e.errorDataFileName;
                // //3.如果只导出数据表格中的数据，就不需要设置ErrorFileName
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImportWithError);
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    InitialExportCommand();
                return this.exportFileCommand;
            }
        }

        private void InitialExportCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => ((DcsDataGridViewBase)this.VehicleOrderPlanSummaryForDelImportDataGridView).ExportData());
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_DelImport_VehicleOrderPlanSummary;
            }
        }
    }
}
