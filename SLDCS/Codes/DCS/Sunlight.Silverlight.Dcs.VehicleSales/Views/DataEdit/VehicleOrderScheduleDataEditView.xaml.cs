﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleOrderScheduleDataEditView {
        public VehicleOrderScheduleDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("VehicleOrderSchedule"));
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load((EntityQuery)this.DomainContext.GetVehicleOrderSchedulesQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var vehicleOrderSchedule = this.DataContext as VehicleOrderSchedule;
            if(vehicleOrderSchedule == null)
                return;
            vehicleOrderSchedule.ValidationErrors.Clear();
            if(vehicleOrderSchedule.OrderSubmitDeadline.Date <= vehicleOrderSchedule.OrderPlanToPODate.Date || vehicleOrderSchedule.OrderSubmitDeadline.Date >= vehicleOrderSchedule.POToProductionPlanDate.Date)
                vehicleOrderSchedule.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderSchedule_OrderSubmitDeadlineIsValidRange, new[] {
                    "OrderSubmitDeadline"
                }));
            if(vehicleOrderSchedule.YearOfOrder <= 0) {
                vehicleOrderSchedule.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderSchedule_YearOfOrderCanNotBeZero, new[] {
                    "YearOfOrder"
                }));
            }
            if(vehicleOrderSchedule.MonthOfOrder <= 0) {
                vehicleOrderSchedule.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleOrderSchedule_MonthOfOrderCanNotBeZero, new[] {
                    "MonthOfOrder"
                }));
            }
            if(vehicleOrderSchedule.HasValidationErrors)
                return;
            vehicleOrderSchedule.OrderSubmitDeadline = new DateTime(vehicleOrderSchedule.OrderSubmitDeadline.Year, vehicleOrderSchedule.OrderSubmitDeadline.Month, vehicleOrderSchedule.OrderSubmitDeadline.Day, 0, 0, 0);
            vehicleOrderSchedule.OrderPlanToPODate = new DateTime(vehicleOrderSchedule.OrderPlanToPODate.Year, vehicleOrderSchedule.OrderPlanToPODate.Month, vehicleOrderSchedule.OrderPlanToPODate.Day, 0, 0, 0);
            vehicleOrderSchedule.POToProductionPlanDate = new DateTime(vehicleOrderSchedule.POToProductionPlanDate.Year, vehicleOrderSchedule.POToProductionPlanDate.Month, vehicleOrderSchedule.POToProductionPlanDate.Day, 0, 0, 0);
            ((IEditableObject)vehicleOrderSchedule).EndEdit();
            base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleOrderSchedule;
            }
        }
    }
}
