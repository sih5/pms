﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehiclePaymentBillDataEditView {
        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;
        private ObservableCollection<KeyValuePair> kvVehicleBankAccounts;
        private ObservableCollection<KeyValuePair> kvVehiclePaymentMethods;
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = new[] {
            "VehiclePaymentBill_CertificateSummary"
        };

        public VehiclePaymentBillDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(this.kvNames);
            this.Loaded += VehiclePaymentBillDataEditView_Loaded;
        }

        private void VehiclePaymentBillDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetVehicleFundsTypesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                foreach(var entity in loadOp.Entities) {
                    this.KvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name,
                        UserObject = entity
                    });
                }
            }, null);

            this.DomainContext.Load(this.DomainContext.GetVehicleBankAccountsQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                this.KvVehicleBankAccounts.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvVehicleBankAccounts.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.BankName,
                        UserObject = entity
                    });
                }
            }, null);
        }


        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehiclePaymentBill;
            }
        }

        public object KvCertificateSummaries {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvVehicleBankAccounts {
            get {
                return this.kvVehicleBankAccounts ?? (this.kvVehicleBankAccounts = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvVehiclePaymentMethods {
            get {
                return this.kvVehiclePaymentMethods ?? (this.kvVehiclePaymentMethods = new ObservableCollection<KeyValuePair>());
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            var queryWindow = DI.GetQueryWindow("VehicleCustomerInformation");
            queryWindow.SelectionDecided += queryWindow_SelectionDecided;
            this.popupTextBoxCustomerCompanyCode.PopupContent = queryWindow;
            this.ComboBoxVehicleBankAccount.SelectionChanged += ComboBoxVehicleBankAccount_SelectionChanged;
            this.KeyValueManager.LoadData();

            this.DomainContext.Load(this.DomainContext.GetVehiclePaymentMethodsQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }

                foreach(var entity in loadOp.Entities) {
                    this.KvVehiclePaymentMethods.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);
        }

        private void ComboBoxVehicleBankAccount_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var vehiclePaymentBill = this.DataContext as VehiclePaymentBill;
            if(e.AddedItems == null || e.AddedItems.Count < 1 || vehiclePaymentBill == null)
                return;
            var keyValuePair = e.AddedItems[0] as KeyValuePair;
            if(keyValuePair == null)
                return;

            var vehicleBancAccount = keyValuePair.UserObject as VehicleBankAccount;
            if(vehicleBancAccount == null)
                return;

            vehiclePaymentBill.BankAccountNumber = vehicleBancAccount.BankAccountNumber;
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var vehiclePaymentBill = this.DataContext as VehiclePaymentBill;
            if(queryWindow == null || queryWindow.SelectedEntities == null || vehiclePaymentBill == null)
                return;

            var vehicleCustomerInformation = queryWindow.SelectedEntities.Cast<VehicleCustomerInformation>().FirstOrDefault();
            if(vehicleCustomerInformation == null)
                return;

            vehiclePaymentBill.CustomerCompanyCode = vehicleCustomerInformation.CustomerCompany == null ? default(string) : vehicleCustomerInformation.CustomerCompany.Code;
            vehiclePaymentBill.CustomerCompanyName = vehicleCustomerInformation.CustomerCompany == null ? default(string) : vehicleCustomerInformation.CustomerCompany.Name;
            vehiclePaymentBill.CustomerCompanyId = vehicleCustomerInformation.CustomerCompany == null ? default(int) : vehicleCustomerInformation.CustomerCompany.Id;

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void OnEditSubmitting() {
            var vehiclePaymentBill = this.DataContext as VehiclePaymentBill;
            if(vehiclePaymentBill == null)
                return;
            vehiclePaymentBill.ValidationErrors.Clear();
            if(vehiclePaymentBill.Amount == default(int))
                vehiclePaymentBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehiclePaymentBill_AmountIsRequired, new[] { "Amount" }));
            if(vehiclePaymentBill.VehiclePaymentMethodId == default(int))
                vehiclePaymentBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehiclePaymentBill_VehiclePaymentMethodIsRequired, new[] { "VehiclePaymentMethodId" }));
            if(vehiclePaymentBill.VehicleFundsTypeId == default(int))
                vehiclePaymentBill.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehiclePaymentBill_VehicleFundsTypeIsRequired, new[] { "VehicleFundsTypeId" }));
            if(vehiclePaymentBill.HasValidationErrors)
                return;
            ((IEditableObject)vehiclePaymentBill).EndEdit();
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehiclePaymentBillWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
