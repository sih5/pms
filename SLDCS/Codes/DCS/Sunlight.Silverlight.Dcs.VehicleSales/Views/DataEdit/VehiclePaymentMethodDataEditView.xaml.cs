﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehiclePaymentMethodDataEditView {
        public VehiclePaymentMethodDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("VehiclePaymentMethod"));
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehiclePaymentMethod;
            }
        }

        protected override void OnEditSubmitting() {
            var vehiclePaymentMethod = this.DataContext as VehiclePaymentMethod;
            if(vehiclePaymentMethod == null)
                return;
            vehiclePaymentMethod.ValidationErrors.Clear();
            if(string.IsNullOrWhiteSpace(vehiclePaymentMethod.Code))
                vehiclePaymentMethod.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehiclePaymentMethod_CodeIsNull, new[] { "Code" }));
            if(string.IsNullOrWhiteSpace(vehiclePaymentMethod.Name))
                vehiclePaymentMethod.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehiclePaymentMethod_NameIsNull, new[] { "Name" }));
            if(vehiclePaymentMethod.HasValidationErrors)
                return;
            ((IEditableObject)vehiclePaymentMethod).EndEdit();
            base.OnEditSubmitting();
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehiclePaymentMethodsQuery().Where(entity => entity.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
