﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehiclePreAllocationRefForMateExportDataEditView : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        private string productCode;
        private DateTime? beginCreateTime = DateTime.Now.Date;
        private DateTime? finishCreateTime = DateTime.Now.Date;
        private KeyValueManager keyValueManager = new KeyValueManager();

        private readonly string[] kvNames = {
            "VehicleOrder_OrderType"
        };

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_Export_VehiclePreAllocationRef;
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public string ProductCode {
            get {
                return this.productCode;
            }
            set {
                this.productCode = value;
                this.OnPropertyChanged("ProductCode");
            }
        }

        public Object KvOrderTypes {
            get {
                return this.keyValueManager[this.kvNames[0]];
            }
        }

        public DateTime? BeginCreateTime {
            get {
                return this.beginCreateTime;
            }
            set {
                this.beginCreateTime = value;
                this.OnPropertyChanged("BeginCreateTime");
            }
        }

        public DateTime? FinishCreateTime {
            get {
                return this.finishCreateTime;
            }
            set {
                this.finishCreateTime = value;
                this.OnPropertyChanged("FinishCreateTime");
            }
        }

        private void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public VehiclePreAllocationRefForMateExportDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
            this.keyValueManager.LoadData();
        }

        private void CreateUI() {
            var dealer = DI.GetQueryWindow("Dealer");
            dealer.SelectionDecided += this.DealerCode_SelectionDecided;
            this.ptbDealer.PopupContent = dealer;
            this.HideSaveButton();
        }

        private void DealerCode_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var dealer = queryWindow.SelectedEntities.Cast<Dealer>().FirstOrDefault();
            if(dealer == null)
                return;

            var vehiclePreAllocationRef = this.DataContext as VehiclePreAllocationRef;

            if(vehiclePreAllocationRef != null) {
                vehiclePreAllocationRef.VehiclePreAllocationRefDealerCode = dealer.Code;
            }

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void InitialExportCommand(object sender, Telerik.Windows.RadRoutedEventArgs e) {
            if(BeginCreateTime != null) {
                BeginCreateTime = new DateTime(this.BeginCreateTime.Value.Year, this.BeginCreateTime.Value.Month, this.BeginCreateTime.Value.Day);
            }
            if(FinishCreateTime != null) {
                FinishCreateTime = new DateTime(this.FinishCreateTime.Value.Year, this.FinishCreateTime.Value.Month, this.FinishCreateTime.Value.Day, 23, 59, 59);
            }
            var dataContext = this.DataContext as VehiclePreAllocationRef;
            if(dataContext != null) {
                this.DomainContext.导出已匹配的预分车关联单(dataContext.YearOfOrder, dataContext.MonthOfOrder, dataContext.OrderType, dataContext.VehiclePreAllocationRefDealerCode, this.ProductCode, this.BeginCreateTime, this.FinishCreateTime, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    }
                    this.DomainContext.IsInvoking = false;
                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                        this.ExportFile(loadOp.Value);
                    else
                        UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehiclePreAllocationRef_ResultIsNull);
                }, null);
            }
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }
    }
}