﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleReturnOrderForHandleDataEditView {
        private ObservableCollection<KeyValuePair> kvVehicleWarehouses;

        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;
        public VehicleReturnOrderForHandleDataEditView() {
            InitializeComponent();
            this.Loaded += this.VehicleReturnOrderForHandleDataEditView_Loaded;
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_VehicleReturnOrderForHandle;
            }
        }
        private DataGridViewBase vehicleReturnOrderDetailForDetailDataGridView;

        private DataGridViewBase VehicleReturnOrderDetailForDetailDataGridView {
            get {
                if(this.vehicleReturnOrderDetailForDetailDataGridView == null) {
                    this.vehicleReturnOrderDetailForDetailDataGridView = DI.GetDataGridView("VehicleReturnOrderDetailForDetail");
                    this.vehicleReturnOrderDetailForDetailDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleReturnOrderDetailForDetailDataGridView;
            }
        }

        public ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvVehicleWarehouses {
            get {
                return this.kvVehicleWarehouses ?? (this.kvVehicleWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(VehicleReturnOrderDetail), VehicleSalesUIStrings.DetailPanel_Title_VehicleReturnOrderDetail), null, () => this.VehicleReturnOrderDetailForDetailDataGridView);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            detailDataEditView.UnregisterButton(detailDataEditView.InsertButton);
            detailDataEditView.UnregisterButton(detailDataEditView.DeleteButton);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleReturnOrderDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    foreach(var item in entity.VehicleReturnOrderDetails)
                        item.ReturnPrice = item.OriginalSalesPrice;
                    this.SetObjectToEdit(entity);
                }
            }, null);

        }

        protected override void OnEditSubmitting() {
            if(!this.VehicleReturnOrderDetailForDetailDataGridView.CommitEdit())
                return;
            var vehicleReturnOrder = this.DataContext as VehicleReturnOrder;
            if(vehicleReturnOrder == null)
                return;
            vehicleReturnOrder.ValidationErrors.Clear();
            foreach(var item in vehicleReturnOrder.VehicleReturnOrderDetails)
                item.ValidationErrors.Clear();

            if(!vehicleReturnOrder.VehicleFundsTypeId.HasValue)
                vehicleReturnOrder.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleReturnOrder_VehicleFundsTypeIsNull, new[] {
                    "VehicleFundsTypeId"
                }));
            if(vehicleReturnOrder.HasValidationErrors)
                return;
            if(vehicleReturnOrder.VehicleReturnOrderDetails.All(entity => entity.ReturnPrice == null)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleReturnOrderDetail_ReturnPriceIsNull);
                return;
            }

            vehicleReturnOrder.Status = (int)DcsVehicleReturnOrderStatus.处理完成;
            ((IEditableObject)vehicleReturnOrder).EndEdit();
            try {
                if(vehicleReturnOrder.Can处理整车退货单)
                    vehicleReturnOrder.处理整车退货单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        private void VehicleReturnOrderForHandleDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetVehicleWarehousesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvVehicleWarehouses.Clear();
                foreach(var vehicleWarehouse in loadOp.Entities) {
                    this.KvVehicleWarehouses.Add(new KeyValuePair {
                        Key = vehicleWarehouse.Id,
                        Value = vehicleWarehouse.Name,
                        UserObject = vehicleWarehouse
                    });
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetVehicleFundsTypesQuery().Where(r => r.Status == (int)DcsVehicleBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvVehicleFundsTypes.Clear();
                foreach(var vehicleFundsType in loadOp.Entities) {
                    this.KvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = vehicleFundsType.Id,
                        Value = vehicleFundsType.Name,
                        UserObject = vehicleFundsType
                    });
                }
            }, null);

        }
    }
}
