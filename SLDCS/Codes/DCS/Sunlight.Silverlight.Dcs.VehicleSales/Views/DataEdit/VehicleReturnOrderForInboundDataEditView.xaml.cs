﻿
using System;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleReturnOrderForInboundDataEditView {
        private DataGridViewBase dataGridView;
        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("VehicleReturnOrderDetailForInboundEdit");
                    this.dataGridView.DomainContext = this.DomainContext;
                }
                return this.dataGridView;
            }
        }

        public VehicleReturnOrderForInboundDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.UnregisterButton(detailEditView.DeleteButton);
            detailEditView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleReturnOrderDetail, null, () => this.DataGridView);
            detailEditView.SetValue(Grid.ColumnProperty, 1);
            this.LayoutRoot.Children.Add(detailEditView);

            this.LayoutRoot.Children.Add(DI.GetDataEditPanel("VehicleReturnOrderForInbound"));
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditSubmitting() {
            var vehicleReturnOrder = this.DataContext as VehicleReturnOrder;
            if(vehicleReturnOrder == null)
                return;
            try {
                if(vehicleReturnOrder.Can退货入库) {
                    vehicleReturnOrder.退货入库();
                }
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override void OnEditSubmitted() {
            UIHelper.ShowNotification(VehicleSalesUIStrings.DataManagementView_Notification_InboundSuccess);
            base.OnEditSubmitted();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleReturnOrderDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_VehicleReturnOrderForInbound;
            }
            set {
                base.Title = value;
            }
        }
    }
}
