﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleReturnOrderForSubmitDataEditView {
        private ObservableCollection<KeyValuePair> kvDealerWarehouses;

        protected override string BusinessName {
            get {
                return "整车退货单";//DcsUIStrings.BusinessName_VehicleReturnOrderForSubmit;
            }
        }

        public VehicleReturnOrderForSubmitDataEditView() {
            InitializeComponent();
            this.Loaded += this.VehicleReturnOrderForSubmitDataEditView_Loaded;
            this.Initializer.Register(this.CreateUI);
        }

        public ObservableCollection<KeyValuePair> KvDealerWarehouses {
            get {
                return this.kvDealerWarehouses ?? (this.kvDealerWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        private void VehicleReturnOrderForSubmitDataEditView_Loaded(object sender, System.Windows.RoutedEventArgs e) {

            this.DomainContext.Load(this.DomainContext.GetDealerWarehousesQuery().Where(r => r.Status == (int)DcsVehicleBaseDataStatus.有效 && r.DealerId == BaseApp.Current.CurrentUserData.EnterpriseId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvDealerWarehouses.Clear();
                foreach(var dealerWarehouse in loadOp.Entities) {
                    this.KvDealerWarehouses.Add(new KeyValuePair {
                        Key = dealerWarehouse.Id,
                        Value = dealerWarehouse.Name,
                        UserObject = dealerWarehouse
                    });
                }
            }, null);

        }

        private DataGridViewBase dataGridViewVehicleReturnOrderDetailForEdit;

        private DataGridViewBase DataGridViewVehicleReturnOrderDetailForEdit {
            get {
                if(this.dataGridViewVehicleReturnOrderDetailForEdit == null) {
                    this.dataGridViewVehicleReturnOrderDetailForEdit = DI.GetDataGridView("VehicleReturnOrderDetailForEdit");
                    this.dataGridViewVehicleReturnOrderDetailForEdit.DomainContext = this.DomainContext;
                }
                return this.dataGridViewVehicleReturnOrderDetailForEdit;
            }
        }

        protected override void OnEditSubmitting() {
            if(!this.DataGridViewVehicleReturnOrderDetailForEdit.CommitEdit())
                return;
            var vehicleReturnOrder = this.DataContext as VehicleReturnOrder;
            if(vehicleReturnOrder == null)
                return;
            vehicleReturnOrder.ValidationErrors.Clear();
            foreach(var item in vehicleReturnOrder.VehicleReturnOrderDetails)
                item.ValidationErrors.Clear();
            if(!vehicleReturnOrder.VehicleReturnOrderDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleReturnOrder_DetailIsNull);
                return;
            }
            if(vehicleReturnOrder.EntityState == EntityState.New)
                vehicleReturnOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
            ((IEditableObject)vehicleReturnOrder).EndEdit();
            base.OnEditSubmitting();
        }
        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleReturnOrderDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        private void CreateUI() {
            var detailDataEditView = new DcsDetailDataEditView();
            detailDataEditView.Register(Utils.GetEntityLocalizedName(typeof(VehicleReturnOrderDetail), VehicleSalesUIStrings.DetailPanel_Title_VehicleReturnOrderDetail), null, () => this.DataGridViewVehicleReturnOrderDetailForEdit);
            detailDataEditView.SetValue(Grid.ColumnProperty, 1);
            this.LayoutRoot.Children.Add(detailDataEditView);
        }

    }
}
