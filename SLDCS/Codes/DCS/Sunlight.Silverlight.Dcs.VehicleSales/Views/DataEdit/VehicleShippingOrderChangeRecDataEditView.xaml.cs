﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleShippingOrderChangeRecDataEditView : INotifyPropertyChanged {
        private string originalVin, newVin;
        private ICommand searchvehicleShippingOrderCommand;

        private ObservableCollection<VehicleShippingDetail> vehicleShippingDetail;
        public ObservableCollection<VehicleShippingDetail> VehicleShippingDetails {
            get {
                return this.vehicleShippingDetail ?? (this.vehicleShippingDetail = new ObservableCollection<VehicleShippingDetail>());
            }
        }

        private ObservableCollection<VehicleAvailableResource> vehicleAvailableResources;
        public ObservableCollection<VehicleAvailableResource> VehicleAvailableResources {
            get {
                return this.vehicleAvailableResources ?? (this.vehicleAvailableResources = new ObservableCollection<VehicleAvailableResource>());
            }
        }

        private ObservableCollection<VehicleShippingOrderChangeRec> vehicleShippingOrderChangeRec;
        public ObservableCollection<VehicleShippingOrderChangeRec> VehicleShippingOrderChangeRecs {
            get {
                return this.vehicleShippingOrderChangeRec ?? (this.vehicleShippingOrderChangeRec = new ObservableCollection<VehicleShippingOrderChangeRec>());
            }
        }

        private VehicleShippingOrderChangeRecForEditDataGridView dataGridView;

        private VehicleShippingOrderChangeRecForEditDataGridView DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = new VehicleShippingOrderChangeRecForEditDataGridView();
                    this.dataGridView.DomainContext = this.DomainContext;
                    this.dataGridView.SetValue(DataContextProperty, this);
                }
                return this.dataGridView;
            }
        }

        private DataGridViewBase vehicleShippingDetailForEditDataGridViewDataGridView;
        private DataGridViewBase VehicleShippingDetailForEditDataGridViewDataGridView {
            get {
                if(this.vehicleShippingDetailForEditDataGridViewDataGridView == null) {
                    this.vehicleShippingDetailForEditDataGridViewDataGridView = DI.GetDataGridView("VehicleShippingDetailForAddEdit");
                    this.vehicleShippingDetailForEditDataGridViewDataGridView.DomainContext = this.DomainContext;
                    this.vehicleShippingDetailForEditDataGridViewDataGridView.SetValue(DataContextProperty, this);
                    this.vehicleShippingDetailForEditDataGridViewDataGridView.SelectionChanged += this.VehicleShippingDetailForEditDataGridViewDataGridView_SelectionChanged;
                }
                return this.vehicleShippingDetailForEditDataGridViewDataGridView;
            }
        }

        private void VehicleShippingDetailForEditDataGridViewDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            var vehicleShippingDetail = this.VehicleShippingDetailForEditDataGridViewDataGridView.SelectedEntities.Cast<VehicleShippingDetail>().FirstOrDefault();
            if(vehicleShippingDetail == null)
                return;
            this.VehicleAvailableResources.Clear();
            this.DataGridClear();
            this.DataGridView.VehicleShippingOrderChangeRec.OriginalVIN = this.OriginalVIN;
            this.DataGridView.VehicleShippingOrderChangeRec.ProductCode = vehicleShippingDetail.ProductCode;
            this.DataGridView.VehicleShippingOrderChangeRec.ProductCategoryName = vehicleShippingDetail.ProductCategoryName;
            this.DataGridView.VehicleShippingOrderChangeRec.VehicleShippingOrderCode = vehicleShippingDetail.VehicleShippingOrder.Code;
            this.DataGridView.VehicleShippingOrderChangeRec.OriginalVehicle = vehicleShippingDetail.VehicleId;
            this.DataGridView.VehicleShippingOrderChangeRec.VehicleShippingOrderId = vehicleShippingDetail.VehicleShippingOrder.Id;
            if(string.IsNullOrWhiteSpace(this.OriginalVIN)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShippingOrderChangeRec_OriginalVinIsNull);
                return;
            }
            if(string.IsNullOrWhiteSpace(vehicleShippingDetail.ProductCode)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShippingOrderChangeRec_ProductCodeIsNull);
                return;
            }
            this.DomainContext.Load(this.DomainContext.查询可用资源调整数据源Query(this.NewVIN, vehicleShippingDetail.ProductCode, this.OriginalVIN), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                var entities = loadOp.Entities;
                if(entities != null) {
                    foreach(var entity in entities) {
                        this.VehicleAvailableResources.Add(entity);
                    }
                }
            }, null);
        }

        private DataGridViewBase vehicleAvailableResourceForEditDataGridView;

        private DataGridViewBase VehicleAvailableResourceForEditDataGridView {
            get {
                if(this.vehicleAvailableResourceForEditDataGridView == null) {
                    this.vehicleAvailableResourceForEditDataGridView = DI.GetDataGridView("VehicleAvailableResourceForEdit");
                    this.vehicleAvailableResourceForEditDataGridView.DomainContext = this.DomainContext;
                    this.vehicleAvailableResourceForEditDataGridView.SetValue(DataContextProperty, this);
                    this.vehicleAvailableResourceForEditDataGridView.SelectionChanged += this.VehicleAvailableResourceForEditDataGridView_SelectionChanged;
                }
                return this.vehicleAvailableResourceForEditDataGridView;
            }
        }

        private void VehicleAvailableResourceForEditDataGridView_SelectionChanged(object sender, System.EventArgs e) {
            this.DataGridClear();
            var vehicleAvailableResource = this.VehicleAvailableResourceForEditDataGridView.SelectedEntities.Cast<VehicleAvailableResource>().FirstOrDefault();
            if(vehicleAvailableResource == null)
                return;
            this.DataGridView.VehicleShippingOrderChangeRec.NewVIN = vehicleAvailableResource.VIN;
            this.DataGridView.VehicleShippingOrderChangeRec.NewVehicleId = vehicleAvailableResource.VehicleId;
        }

        private void CreateUI() {
            var VehicleShippingOrderForDetailDataEditView = new DcsDetailDataEditView();
            VehicleShippingOrderForDetailDataEditView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleShippingOrder, null, () => this.VehicleShippingDetailForEditDataGridViewDataGridView);
            VehicleShippingOrderForDetailDataEditView.SetValue(Grid.RowProperty, 1);
            VehicleShippingOrderForDetailDataEditView.UnregisterButton(VehicleShippingOrderForDetailDataEditView.InsertButton);
            VehicleShippingOrderForDetailDataEditView.UnregisterButton(VehicleShippingOrderForDetailDataEditView.DeleteButton);

            var vehicleShippingOrderChangeRecForEditDataGridView = new DcsDetailDataEditView();
            vehicleShippingOrderChangeRecForEditDataGridView.Register(VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleShippingOrderChangeRec, null, () => this.DataGridView);
            vehicleShippingOrderChangeRecForEditDataGridView.SetValue(Grid.RowProperty, 2);

            var tabControl = new RadTabControl();
            tabControl.Items.Add(new RadTabItem {
                Content = this.VehicleAvailableResourceForEditDataGridView,
                Header = VehicleSalesUIStrings.DetailPanel_Title_VehicleAvailableResource,  
            });
            tabControl.SetValue(Grid.RowProperty, 1);
            tabControl.SetValue(Grid.ColumnProperty, 2);
            this.LayoutRoot.Children.Add(tabControl);
            this.LayoutRoot.Children.Add(VehicleShippingOrderForDetailDataEditView);
            this.LayoutRoot.Children.Add(vehicleShippingOrderChangeRecForEditDataGridView);
        }

        private void InitializeCommand() {
            this.searchvehicleShippingOrderCommand = new DelegateCommand(() => {
                if(string.IsNullOrWhiteSpace(this.OriginalVIN)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShippingOrderChangeRec_OriginalVinIsNull);
                    return;
                }
                this.Clear();
                this.DomainContext.Load(this.DomainContext.查询整车发运单清单Query(this.OriginalVIN), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        if(!loadOp.IsErrorHandled)
                            loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                        return;
                    }
                    if(loadOp.Entities == null)
                        return;
                    var vehicleShippingDetails = loadOp.Entities.ToArray();

                    foreach(var Item in vehicleShippingDetails) {
                        this.VehicleShippingDetails.Add(Item);
                    }
                }, null);
            });
        }

        protected override void OnEditSubmitting() {
            var vehicleShippingOrderChangeRec = this.DataContext as VehicleShippingOrderChangeRec;
            if(vehicleShippingOrderChangeRec == null)
                return;

            vehicleShippingOrderChangeRec.OriginalVIN = this.DataGridView.VehicleShippingOrderChangeRec.OriginalVIN;
            vehicleShippingOrderChangeRec.ProductCode = this.DataGridView.VehicleShippingOrderChangeRec.ProductCode;
            vehicleShippingOrderChangeRec.ProductCategoryName = this.DataGridView.VehicleShippingOrderChangeRec.ProductCategoryName;
            vehicleShippingOrderChangeRec.VehicleShippingOrderCode = this.DataGridView.VehicleShippingOrderChangeRec.VehicleShippingOrderCode;
            vehicleShippingOrderChangeRec.OriginalVehicle = this.DataGridView.VehicleShippingOrderChangeRec.OriginalVehicle;
            vehicleShippingOrderChangeRec.VehicleShippingOrderId = this.DataGridView.VehicleShippingOrderChangeRec.VehicleShippingOrderId;
            vehicleShippingOrderChangeRec.NewVIN = this.DataGridView.VehicleShippingOrderChangeRec.NewVIN;
            vehicleShippingOrderChangeRec.NewVehicleId = this.DataGridView.VehicleShippingOrderChangeRec.NewVehicleId;

            vehicleShippingOrderChangeRec.ValidationErrors.Clear();

            if(!this.VehicleAvailableResourceForEditDataGridView.CommitEdit())
                return;
            if(!this.VehicleShippingDetailForEditDataGridViewDataGridView.CommitEdit())
                return;
            if(!this.DataGridView.CommitEdit())
                return;
            ((IEditableObject)vehicleShippingOrderChangeRec).EndEdit();

            if(this.VehicleShippingOrderChangeRecs.Count > 0) {
                try {
                    if(vehicleShippingOrderChangeRec.Can生成发运单调整)
                        vehicleShippingOrderChangeRec.生成发运单调整();
                } catch(Exception ex) {
                    UIHelper.ShowAlertMessage(ex.Message);
                }
            } else {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehShipplanApprovalChangeRec_VehShipplanApprovalChangeRecIsNull);
                return;
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        protected override void OnEditCancelled() {
            this.Clear();
            this.DataGridClear();
            base.OnEditCancelled();
        }

        //清除发运单调整结果信息
        private void DataGridClear() {
            this.VehicleShippingOrderChangeRecs.Clear();
        }

        private void Clear() {
            this.VehicleShippingDetails.Clear();
            this.VehicleAvailableResources.Clear();
            this.DataGridClear();
        }

        public ICommand SearchvehicleShippingOrderCommand {
            get {
                if(this.searchvehicleShippingOrderCommand == null)
                    InitializeCommand();
                return this.searchvehicleShippingOrderCommand;
            }
        }

        public string OriginalVIN {
            get {
                return this.originalVin;
            }
            set {
                this.originalVin = value;
                this.OnPropertyChanged("OriginalVIN");
            }
        }

        public string NewVIN {
            get {
                return this.newVin;
            }
            set {
                this.newVin = value;
                this.OnPropertyChanged("NewVIN");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public VehicleShippingOrderChangeRecDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
    }
}
