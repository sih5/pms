﻿
using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleShippingOrderDataEditView {
        private DataGridViewBase vehicleShippingDetailForEditDataGridView;

        public VehicleShippingOrderDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_Edit_VehicleShippingOrder;
            }
        }

        private DataGridViewBase VehicleShippingDetailForEditDataGridView {
            get {
                if(this.vehicleShippingDetailForEditDataGridView == null) {
                    this.vehicleShippingDetailForEditDataGridView = DI.GetDataGridView("VehicleShippingDetailForEdit");
                    this.vehicleShippingDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.vehicleShippingDetailForEditDataGridView;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleShippingOrderWithDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected virtual void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("VehicleShippingOrder");
            this.Root.Children.Add(dataEditPanel);

            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(VehicleShippingOrder), "VehicleShippingDetails"), null, this.VehicleShippingDetailForEditDataGridView);
            detailEditView.SetValue(Grid.ColumnProperty, 2);

            this.Root.Children.Add(detailEditView);
        }


        protected override void OnEditSubmitting() {
            var vehicleShippingOrder = this.DataContext as VehicleShippingOrder;
            if(vehicleShippingOrder == null || !this.VehicleShippingDetailForEditDataGridView.CommitEdit())
                return;
            if(vehicleShippingOrder.VehicleShippingDetails.Any(e => e.VehicleReceptionStatus == (int)DcsVehicleShippingDetailVehicleReceptionStatus.已接车 && (e.ReceivingWarehouseId == null || e.ReceivingWarehouseId <= 0))) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShippingDetail_ReceivingWarehouseIdIsRequired);
                return;
            }

            if(vehicleShippingOrder.ValidationErrors.Any())
                return;
            ((IEditableObject)vehicleShippingOrder).EndEdit();
            try {
                if(vehicleShippingOrder.Can发运单接车确认)
                    vehicleShippingOrder.发运单接车确认();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            base.OnEditSubmitting();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
