﻿
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleShippingOrderImportDataEditView {
        private DataGridViewBase vehicleShippingOrderForImportErrorDataGridView;
        private ICommand exportFileCommand;

        private DataGridViewBase VehicleShippingOrderForImportErrorDataGridView {
            get {
                return this.vehicleShippingOrderForImportErrorDataGridView ?? (this.vehicleShippingOrderForImportErrorDataGridView = DI.GetDataGridView("VehicleShippingOrderForImport"));
            }
        }

        public VehicleShippingOrderImportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            this.UploadFileSuccessedProcessing = this.UploadFileProcessing;
            var tabControl = new RadTabControl();
            tabControl.SetValue(Grid.RowProperty, 1);
            tabControl.SetValue(MarginProperty, new Thickness(10));

            var tabItem = new RadTabItem();
            tabItem.Header = VehicleSalesUIStrings.DataEditView_GroupTitle_VehicleShippingOrder;
            tabItem.Content = this.VehicleShippingOrderForImportErrorDataGridView;

            tabControl.Items.Add(tabItem);

            this.VehicleShippingOrderForImportErrorDataGridView.SetValue(Grid.RowProperty, 1);
            this.LayoutRoot.Children.Add(tabControl);
        }

        private void InitializeCommand() {
            this.exportFileCommand = new Core.Command.DelegateCommand(() => ((DcsDataGridViewBase)this.VehicleShippingOrderForImportErrorDataGridView).ExportData());
        }

        private void UploadFileProcessing(string fileName) {
            this.DomainContext.导入发运单(fileName, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                }

                this.ImportComplete();
                this.DomainContext.IsInvoking = false;
                if(string.IsNullOrEmpty(loadOp.Value) || string.IsNullOrWhiteSpace(loadOp.Value)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImport);
                } else {
                    this.ExportFile(loadOp.Value);
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Notification_CompletedImportWithError);
                }
            }, null);

        }

        protected override bool IsAutomaticUploadFile {
            get {
                return true;
            }
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_Import_VehicleShippingOrder;
            }
        }

        public ICommand ExportTemplateCommand {
            get {
                if(this.exportFileCommand == null)
                    this.InitializeCommand();
                return this.exportFileCommand;
            }
        }
    }
}
