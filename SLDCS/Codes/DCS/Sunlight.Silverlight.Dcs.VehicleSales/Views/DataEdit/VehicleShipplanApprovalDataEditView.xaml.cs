﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleShipplanApprovalDataEditView : INotifyPropertyChanged {
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;
        private decimal availableAmount;
        private decimal approvepAmount;
        public ICommand searchCommand;
        private DataGridViewBase vehicleOrderDetailForSelectDataGridView;

        public VehicleShipplanApprovalDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleShipplanApproval;
            }
        }

        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        //审批金额
        public decimal AvailableAmount {
            get {
                return this.availableAmount;
            }
            set {
                this.availableAmount = value;
                this.OnPropertyChanged("AvailableAmount");
            }
        }

        //可用金额
        public decimal ApprovepAmount {
            get {
                return this.approvepAmount;
            }
            set {
                this.approvepAmount = value;
                this.OnPropertyChanged("ApprovepAmount");
            }
        }

        //不能把此GridView的DomainContext等于当前的GridView，因为当前的编辑是在原有的数据基础上做修改后转换为当前DomainContext的VehicleShipplanApprovalDetail，否则会导致修改后的数据影响到GridView的原有整车订单清单数据
        private DataGridViewBase VehicleOrderDetailForSelectDataGridView {
            get {
                if(vehicleOrderDetailForSelectDataGridView == null) {
                    this.vehicleOrderDetailForSelectDataGridView = DI.GetDataGridView("VehicleOrderDetailForSelect");
                    this.vehicleOrderDetailForSelectDataGridView.SelectionChanged += vehicleOrderDetailForSelectDataGridView_SelectionChanged;
                }
                return this.vehicleOrderDetailForSelectDataGridView;
            }
        }

        //进行汇总操作
        private void vehicleOrderDetailForSelectDataGridView_SelectionChanged(object sender, EventArgs e) {
            var selectedEntities = this.VehicleOrderDetailForSelectDataGridView.SelectedEntities;
            if(selectedEntities == null || !selectedEntities.Any())
                return;
            var vehicleOrderDetails = this.VehicleOrderDetailForSelectDataGridView.SelectedEntities.Cast<VehicleOrderDetail>();
            this.ApprovepAmount = vehicleOrderDetails.Sum(r => r.Price);//汇总清单价格合计
        }

        private void CreateUI() {
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(VehicleShipplanApproval), "VehicleShipplanApprovalDetails"), null, () => this.VehicleOrderDetailForSelectDataGridView);
            detailEditView.SetValue(Grid.RowProperty, 4);
            detailEditView.SetValue(Grid.ColumnSpanProperty, 7);
            this.Root.Children.Add(detailEditView);

            var queryWindow = DI.GetQueryWindow("VehiclePreAllocationRef");
            queryWindow.Loaded += queryWindow_Loaded;
            queryWindow.SelectionDecided += queryWindow_SelectionDecided;
            this.popupTextBoxSourceCode.PopupContent = queryWindow;
            this.Loaded += VehicleShipplanApprovalDataEditView_Loaded;
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var vehiclePreAllocationRef = queryWindow.SelectedEntities.Cast<VehiclePreAllocationRef>().FirstOrDefault();
            if(vehiclePreAllocationRef == null)
                return;

            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null)
                return;

            //要使用vehiclePreAllocationRef.VehicleOrder下的属性，必须判断vehiclePreAllocationRef.VehicleOrder是否为空
            vehicleShipplanApproval.SourceCode = vehiclePreAllocationRef.VehicleOrder == null ? default(string) : vehiclePreAllocationRef.VehicleOrder.Code;
            vehicleShipplanApproval.SourceId = vehiclePreAllocationRef.VehicleOrder == null ? default(int) : vehiclePreAllocationRef.VehicleOrder.Id;
            vehicleShipplanApproval.DealerId = vehiclePreAllocationRef.DealerId;
            vehicleShipplanApproval.DealerCode = vehiclePreAllocationRef.DealerCode;
            vehicleShipplanApproval.DealerName = vehiclePreAllocationRef.DealerName;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void queryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow != null)
                queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void VehicleShipplanApprovalDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetVehicleWarehousesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    return;
                }
                this.KvWarehouses.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetVehicleFundsTypesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    return;
                }
                this.KvVehicleFundsTypes.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name,
                        UserObject = entity
                    });
                }
            }, null);

            //当用户不是正常的退出，而是直接单击TabControl，直接在主界面退出已达到退出的时候，此时当前的变量是不会被重新初始化的，OnEditSubmitted，和OnEditCanceled不会被执行，可以考虑使用扩展属性以规避
            this.ApprovepAmount = default(decimal);
            this.AvailableAmount = default(decimal);
            //在加载时重新查询以达到清空数据表格的目的
            if(this.VehicleOrderDetailForSelectDataGridView.FilterItem != null) {
                var composite = this.VehicleOrderDetailForSelectDataGridView.FilterItem as CompositeFilterItem;
                if(composite != null) {
                    foreach(var filter in composite.Filters) {
                        switch(filter.MemberName) {
                            case "VehicleOrder.Code":
                                filter.Value = default(string);
                                break;
                            case "VehicleWarehouseId":
                                filter.Value = default(int);
                                break;
                        }
                    }
                }
                this.VehicleOrderDetailForSelectDataGridView.ExecuteQueryDelayed();
            }
        }

        private void InitialCommand() {
            this.searchCommand = new DelegateCommand(() => {
                var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
                if(vehicleShipplanApproval == null)
                    return;

                if(vehicleShipplanApproval.VehicleWarehouseId <= default(int)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApprovalDataEditView_WarehouseNameIsNull);
                    return;
                }
                if(string.IsNullOrEmpty(vehicleShipplanApproval.SourceCode)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_SourceCodeIsNull);
                    return;
                }

                var composite = new CompositeFilterItem();
                composite.Filters.Add(new FilterItem {
                    MemberName = "VehicleOrder.Code",
                    MemberType = typeof(string),
                    Operator = FilterOperator.IsEqualTo,
                    IsExact = false,
                    Value = vehicleShipplanApproval.SourceCode
                });
                composite.Filters.Add(new FilterItem {
                    MemberName = "VehicleWarehouseId",
                    MemberType = typeof(int),
                    IsExact = false,
                    Operator = FilterOperator.IsEqualTo,
                    Value = vehicleShipplanApproval.VehicleWarehouseId
                });
                //附加参数
                this.VehicleOrderDetailForSelectDataGridView.FilterItem = composite;
                //编辑后的GridView无法进行查询，必须把之前的编辑回滚
                this.VehicleOrderDetailForSelectDataGridView.DomainContext.RejectChanges();
                //清空之前已经添加的清单
                foreach(var detail in vehicleShipplanApproval.VehicleShipplanApprovalDetails) {
                    vehicleShipplanApproval.VehicleShipplanApprovalDetails.Remove(detail);
                }
                //执行查询
                this.VehicleOrderDetailForSelectDataGridView.ExecuteQueryDelayed();
            });
        }

        public ICommand SearchCommand {
            get {
                if(this.searchCommand == null)
                    InitialCommand();
                return this.searchCommand;
            }
        }

        protected override void OnEditSubmitting() {
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null || !this.VehicleOrderDetailForSelectDataGridView.CommitEdit())
                return;
            this.VehicleOrderDetailConverter();
            this.ValidateInternal();
            if(vehicleShipplanApproval.VehicleWarehouseId <= default(int)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApprovalDataEditView_WarehouseNameIsNull);
                return;
            }
            if(this.VehicleOrderDetailForSelectDataGridView.Entities==null||!this.VehicleOrderDetailForSelectDataGridView.Entities.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_VehicleShipplanApprovalDetailsIsNull);
                return;
            }
            //如果数据表格中有数据，而没有勾选时，给出提示
            if(!vehicleShipplanApproval.VehicleShipplanApprovalDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_PeleaseSelectDetails);
                return;
            }
            
            if(vehicleShipplanApproval.VehicleShipplanApprovalDetails.Any(r=>String.IsNullOrEmpty(r.VIN))) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApprovalDetail_VINlsIsRequired);
                return;
            }

            if(this.ApprovepAmount > this.AvailableAmount) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApprovalDataEditView_ApprovepAmountIsGreateThanAvailableAmount);
                return;
            }

            if(vehicleShipplanApproval.ValidationErrors.Any())
                return;
            ((IEditableObject)vehicleShipplanApproval).EndEdit();
            try {
                if(vehicleShipplanApproval.Can生成整车发车审批单)
                    vehicleShipplanApproval.生成整车发车审批单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }

            base.OnEditSubmitting();
        }

        private void ValidateInternal() {
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null || !this.VehicleOrderDetailForSelectDataGridView.CommitEdit())
                return;
            this.VehicleOrderDetailConverter();
            ((IEditableObject)vehicleShipplanApproval).EndEdit();
            vehicleShipplanApproval.ValidationErrors.Clear();
            if(vehicleShipplanApproval.VehicleShipplanApprovalDetails.Any(r => r.HasValidationErrors))
                foreach(var detail in vehicleShipplanApproval.VehicleShipplanApprovalDetails)
                    detail.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(vehicleShipplanApproval.SourceCode))
                vehicleShipplanApproval.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_SourceCodeIsNull, new[] { "SourceCode" }));
            if(vehicleShipplanApproval.VehicleFundsTypeId == default(int))
                vehicleShipplanApproval.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_VehicleFundsTypeIsNull, new[] { "VehicleFundsTypeId" }));
        }

        private void VehicleOrderDetailConverter() {
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null)
                return;

            if(this.VehicleOrderDetailForSelectDataGridView.SelectedEntities == null || !this.VehicleOrderDetailForSelectDataGridView.SelectedEntities.Any())
                return;
            //清空之前已经添加的清单
            foreach(var detail in vehicleShipplanApproval.VehicleShipplanApprovalDetails) {
                vehicleShipplanApproval.VehicleShipplanApprovalDetails.Remove(detail);
            }
            foreach(var entity in this.VehicleOrderDetailForSelectDataGridView.SelectedEntities.Cast<VehicleOrderDetail>()) {
                vehicleShipplanApproval.VehicleShipplanApprovalDetails.Add(new VehicleShipplanApprovalDetail {
                    VehicleShipplanApproval = vehicleShipplanApproval,
                    VehicleId = entity.VehicleId,
                    VIN = entity.VIN,
                    SON = entity.SON,
                    ProductId = entity.ProductId,
                    ProductName = entity.ProductName,
                    ProductCode = entity.ProductCode,
                    ProductCategoryCode = entity.ProductCategoryCode,
                    ProductCategoryName = entity.ProductCategoryName,
                    Price = entity.Price,
                    Status = (int)DcsVehicleShipplanApprovalDetailStatus.新增,
                    ActualShippingDate = entity.ShippingDate,//客户端虚拟属性
                    EstimatedShippingDate = entity.EstimatedShippingDate,
                    DispatchWarehouseName = entity.VehicleWarehouseName,
                    DispatchWarehouseId = entity.VehicleWarehouseId
                });
            }
        }

        //退出时重新初始化当前页面的变量
        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.ClearGridViewFilters();
        }

        //提交成功时重新初始化当前页面的变量，及清空数据表格
        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.ClearGridViewFilters();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        //清空Gridview的FilterItem 的Value，在页面加载时重新查询以达到清空数据表格数据的目的，和清空当前变量的赋值
        private void ClearGridViewFilters() {
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval==null)
                return;
            vehicleShipplanApproval.VehicleWarehouseId = default(int);
            this.ApprovepAmount = default(decimal);
            this.AvailableAmount = default(decimal);
            var composite = this.VehicleOrderDetailForSelectDataGridView.FilterItem as CompositeFilterItem;
            if(composite == null)
                return;

            var newComposite = new CompositeFilterItem();
            foreach(var filter in composite.Filters) {
                switch(filter.MemberName) {
                    case "VehicleOrder.Code":
                        filter.Value = default(string);
                        newComposite.Filters.Add(filter);
                        break;
                    case "VehicleWarehouseId":
                        filter.Value = default(int);
                        newComposite.Filters.Add(filter);
                        break;
                }
            }

            this.VehicleOrderDetailForSelectDataGridView.FilterItem = newComposite;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null)
                return;
            
            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            //由于查询可用余额的经销商编号是必填的，此处添加用户提醒
            if(vehicleShipplanApproval.DealerId == default(int)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_DealerCodeIsNull);
                return;
            }
            var keyValuePair = e.AddedItems[0] as KeyValuePair;
            if(keyValuePair == null || (keyValuePair.UserObject as VehicleFundsType) == null)
                return;

            var vehicleFundsTypeId = ((VehicleFundsType)(keyValuePair.UserObject)).Id;
            this.DomainContext.Load(this.DomainContext.查询可用余额Query(vehicleShipplanApproval.DealerId, vehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                var vehicleCustomerAccount = loadOp.Entities.FirstOrDefault();
                vehicleShipplanApproval.VehicleCustomerAccount = vehicleCustomerAccount;
                if(vehicleCustomerAccount == null)
                    return;
                this.AvailableAmount = vehicleCustomerAccount.CustomerCredenceAmount + vehicleCustomerAccount.AccountBalance - (vehicleCustomerAccount.PendingAmount.HasValue ? vehicleCustomerAccount.PendingAmount.Value : 0);
            }, null);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
