﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.CustomEntity;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using DelegateCommand = Sunlight.Silverlight.Core.Command.DelegateCommand;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    /// <summary>
    /// 本页面说明，根据资金类型查询可用金额，
    /// 1.当经销商没有被赋值，而先选择了资金类型，这个时间是不会去查询可用金额
    /// 2.当经销商赋值(2处赋值)，再选择资金类型
    /// 由于查询可用金额的服务端是必填字段，客户端需要同时维护3处地方(1.GridView里面的SelectionChanged事件里2.当前的queryWindow_SelectionDecided3.当前的资金类型下来事件中)
    /// </summary>
    public partial class VehicleShipplanApprovalForSecondDataEditView : INotifyPropertyChanged {
        private CustomVehicleShipplanApproval customVehicleShipplanApproval = new CustomVehicleShipplanApproval();
        private ObservableCollection<KeyValuePair> kvWarehouses;
        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;
        private ObservableCollection<KeyValuePair> kvVehicleCategories;
        private ObservableCollection<VehicleForShippingPlanApprove> vehicleForShippingPlanApproves;
        public ICommand searchCommand;
        private KeyValueManager keyValueManager;
        private DataGridViewBase vehicleForShippingPlanApproveForSelect;

        private readonly string[] kvNames = new[] {
            "VehicleOrder_OrderType"
        };

        public VehicleShipplanApprovalForSecondDataEditView() {
            InitializeComponent();
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.CreateUI);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleShipplanApproval;
            }
        }

        //之所以每次进入界面实例化，是为了清除界面中已经存在的查询条件
        public CustomVehicleShipplanApproval CustomVehicleShipplanApproval {
            get {
                return this.customVehicleShipplanApproval;
            }
            set {
                this.customVehicleShipplanApproval = value;
                this.OnPropertyChanged("CustomVehicleShipplanApproval");
            }
        }

        public KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        public Object KvVehicleOrderTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        //发车仓库
        public ObservableCollection<KeyValuePair> KvWarehouses {
            get {
                return this.kvWarehouses ?? (this.kvWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        //资金类型
        public ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        //车型大类
        public ObservableCollection<KeyValuePair> KvVehicleCategories {
            get {
                return this.kvVehicleCategories ?? (this.kvVehicleCategories = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<VehicleForShippingPlanApprove> VehicleForShippingPlanApproves {
            get {
                return this.vehicleForShippingPlanApproves ?? (vehicleForShippingPlanApproves = new ObservableCollection<VehicleForShippingPlanApprove>());
            }
        }

        private DataGridViewBase VehicleForShippingPlanApproveForSelectDataGridView {
            get {
                if(vehicleForShippingPlanApproveForSelect == null) {
                    this.vehicleForShippingPlanApproveForSelect = DI.GetDataGridView("VehicleForShippingPlanApproveForSelect");
                    this.vehicleForShippingPlanApproveForSelect.DomainContext = this.DomainContext;
                    this.vehicleForShippingPlanApproveForSelect.DataContext = this;
                }
                return this.vehicleForShippingPlanApproveForSelect;
            }
        }

        private void CreateUI() {
            this.RegisterButton(new ButtonItem {
                Title = VehicleSalesUIStrings.DataEditView_Button_BatchProcessEntity,
                Command = new DelegateCommand(this.ProcessVehicleShipplanApprovalAction),
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/new-bill.png", UriKind.Relative)
            });

            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(VehicleShipplanApproval), "VehicleShipplanApprovalDetails"), null, () => this.VehicleForShippingPlanApproveForSelectDataGridView);
            detailEditView.SetValue(Grid.RowProperty, 4);
            detailEditView.SetValue(Grid.ColumnSpanProperty, 7);
            detailEditView.SetValue(MaxWidthProperty, Convert.ToDouble(1100));
            this.Root.Children.Add(detailEditView);

            var queryWindow = DI.GetQueryWindow("Dealer");
            queryWindow.Loaded += queryWindow_Loaded;
            queryWindow.SelectionDecided += queryWindow_SelectionDecided;
            this.popupTextBoxDealerName.PopupContent = queryWindow;
            this.Loaded += VehicleShipplanApprovalDataEditView_Loaded;

            this.KeyValueManager.LoadData();
        }

        //此回调方法只针对新增不退出操作
        private void ProcessVehicleShipplanApprovalAction() {
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null || !this.VehicleForShippingPlanApproveForSelectDataGridView.CommitEdit())
                return;
            this.VehicleOrderDetailConverter();
            vehicleShipplanApproval.ValidationErrors.Clear();
            if(vehicleShipplanApproval.VehicleShipplanApprovalDetails.Any(r => r.HasValidationErrors))
                foreach(var detail in vehicleShipplanApproval.VehicleShipplanApprovalDetails)
                    detail.ValidationErrors.Clear();
            CustomVehicleShipplanApproval.ValidationErrors.Clear();//该扩展实体中，由于实现了红框效果
            if(vehicleShipplanApproval.VehicleFundsTypeId == default(int)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_VehicleFundsTypeIsNull);
                return;
            }

            if(vehicleShipplanApproval.SourceCode == default(string)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_SourceCodeIsNull);
                return;
            }

            if(this.VehicleForShippingPlanApproveForSelectDataGridView.SelectedEntities == null || !this.VehicleForShippingPlanApproveForSelectDataGridView.SelectedEntities.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_VehicleShipplanApprovalDetailsIsNull);
                return;
            }
            //如果数据表格中有数据，而没有勾选时，给出提示
            if(!vehicleShipplanApproval.VehicleShipplanApprovalDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_PeleaseSelectDetails);
                return;
            }

            if(vehicleShipplanApproval.VehicleShipplanApprovalDetails.Any(r => String.IsNullOrEmpty(r.VIN))) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApprovalDetail_VINlsIsRequired);
                return;
            }

            if(CustomVehicleShipplanApproval.ApprovepAmount > CustomVehicleShipplanApproval.AvailableAmount) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_CustomVehicleShipplanApproval_ApprovepAmountIsGreateThanAvailableAmount);
                return;
            }

            var isSameCode = this.VehicleForShippingPlanApproveForSelectDataGridView.SelectedEntities.Cast<VehicleForShippingPlanApprove>().GroupBy(r => r.Code).Count() == 1;
            if(!isSameCode) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApprovalDetails_VehicleOrderIsNotSame);
                return;
            }

            var dealerId = this.VehicleForShippingPlanApproveForSelectDataGridView.SelectedEntities.Cast<VehicleForShippingPlanApprove>().First().DealerId;
            if(vehicleShipplanApproval.DealerId != dealerId) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApprovalDetails_DealerIsNotSameAndMainOrder);
                return;
            }

            if(vehicleShipplanApproval.ValidationErrors.Any())
                return;
            try {
                if(vehicleShipplanApproval.Can生成整车发车审批单)
                    vehicleShipplanApproval.生成整车发车审批单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            ((IEditableObject)vehicleShipplanApproval).EndEdit();
            this.DomainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    return;
                }
                //提交成功后执行，因为页面尚未退出，可能会做第二次新增操作，事先把创建发车审批单
                var newVehicleShipplanApproval = this.CreateObjectToEdit<VehicleShipplanApproval>();
                newVehicleShipplanApproval.Code = GlobalVar.ASSIGNED_BY_SERVER;
                newVehicleShipplanApproval.Status = (int)DcsVehicleShipplanApprovalStatus.新增;
                newVehicleShipplanApproval.VehicleSalesOrgId = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID;
                newVehicleShipplanApproval.VehicleSalesOrgCode = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGCODE;
                newVehicleShipplanApproval.VehicleSalesOrgName = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGNAME;
                this.VehicleOrderDetailConverter();
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Confirm_SaveSuccess);
                //执行查询
                this.DomainContext.Load(this.DomainContext.查询预分车关联单以及相关信息Query(this.CustomVehicleShipplanApproval.VehicleWarehouseId, this.CustomVehicleShipplanApproval.VehicleOrderType, this.CustomVehicleShipplanApproval.VehicleCategoryCode, newVehicleShipplanApproval.DealerId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                        return;
                    }
                    this.VehicleForShippingPlanApproves.Clear();
                    foreach(var vehicleForShippingPlanApprove in loadOp.Entities) {
                        vehicleForShippingPlanApprove.ShippingDate = DateTime.Now;
                        this.VehicleForShippingPlanApproves.Add(vehicleForShippingPlanApprove);
                    }
                }, null);
            }, null);
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(queryWindow == null || queryWindow.SelectedEntities == null || vehicleShipplanApproval == null)
                return;
            
            var dealer = queryWindow.SelectedEntities.Cast<Dealer>().FirstOrDefault();
            if(dealer == null)
                return;

            CustomVehicleShipplanApproval.DealerName = dealer.Name;//如果选择后就无法清除，那么经销商Id就确定下来了
            vehicleShipplanApproval.DealerName = dealer.Name;
            vehicleShipplanApproval.DealerId = dealer.Id;
            vehicleShipplanApproval.DealerCode = dealer.Code;
            if(vehicleShipplanApproval.DealerId != default(int) && CustomVehicleShipplanApproval.VehicleOrderType != default(int))
                this.DomainContext.Load(this.DomainContext.查询可用余额Query(vehicleShipplanApproval.DealerId, CustomVehicleShipplanApproval.VehicleFundsType), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        loadOp.MarkErrorAsHandled();
                        return;
                    }
                    var vehicleCustomerAccount = loadOp.Entities.FirstOrDefault();
                    vehicleShipplanApproval.VehicleCustomerAccount = vehicleCustomerAccount;
                    if(vehicleCustomerAccount == null) {
                        CustomVehicleShipplanApproval.AvailableAmount = default(int);//清空数据
                        return;
                    }

                    CustomVehicleShipplanApproval.AvailableAmount = vehicleCustomerAccount.CustomerCredenceAmount + vehicleCustomerAccount.AccountBalance - (vehicleCustomerAccount.PendingAmount.HasValue ? vehicleCustomerAccount.PendingAmount.Value : 0);
                }, null);

            if(this.CustomVehicleShipplanApproval.VehicleWarehouseId != default(int) && this.CustomVehicleShipplanApproval.VehicleOrderType != default(int) && vehicleShipplanApproval.DealerId != default(int) && string.IsNullOrEmpty(this.CustomVehicleShipplanApproval.VehicleCategoryCode))
                //执行查询
                this.DomainContext.Load(this.DomainContext.查询预分车关联单以及相关信息Query(this.CustomVehicleShipplanApproval.VehicleWarehouseId, this.CustomVehicleShipplanApproval.VehicleOrderType, this.CustomVehicleShipplanApproval.VehicleCategoryCode, vehicleShipplanApproval.DealerId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                        return;
                    }
                    this.VehicleForShippingPlanApproves.Clear();
                    foreach(var vehicleForShippingPlanApprove in loadOp.Entities) {
                        vehicleForShippingPlanApprove.ShippingDate = DateTime.Now;//TODO:待确认,根据类型是DateTime类型而不是DateTime？，必须赋默认值
                        this.VehicleForShippingPlanApproves.Add(vehicleForShippingPlanApprove);
                    }
                }, null);
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        private void queryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow != null)
                queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void VehicleShipplanApprovalDataEditView_Loaded(object sender, RoutedEventArgs e) {
            //默认值赋值
            this.CustomVehicleShipplanApproval.Code = GlobalVar.ASSIGNED_BY_SERVER;
            this.CustomVehicleShipplanApproval.VehicleOrderType = (int)DcsVehicleOrderOrderType.紧急订单;
            this.CustomVehicleShipplanApproval.DealerName = default(string);
            this.CustomVehicleShipplanApproval.VehicleCategoryName = default(string);
            this.CustomVehicleShipplanApproval.VehicleCategoryCode = default(string);
            this.CustomVehicleShipplanApproval.VehicleWarehouseId = default(int);
            this.CustomVehicleShipplanApproval.Remark = default(string);
            this.CustomVehicleShipplanApproval.ApprovepAmount = default(int);
            this.CustomVehicleShipplanApproval.AvailableAmount = default(int);

            this.DomainContext.Load(this.DomainContext.GetVehicleWarehousesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    return;
                }
                this.KvWarehouses.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvWarehouses.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetVehicleFundsTypesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    return;
                }
                this.KvVehicleFundsTypes.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name,
                        UserObject = entity
                    });
                }
            }, null);
            this.DomainContext.Load(this.DomainContext.GetProductCategoriesQuery().Where(r => r.ProductCategoryLevel == 2 && r.Status == (int)DcsMasterDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                    return;
                }
                this.KvVehicleCategories.Clear();
                this.comboBoxVehicleCategoryCode.ItemsSource = loadOp.Entities;
            }, null);
            //防止单击TabControl退出界面，此处要清空数据源
            this.VehicleForShippingPlanApproves.Clear();
        }

        private void InitialCommand() {
            this.searchCommand = new DelegateCommand(() => {
                var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
                if(vehicleShipplanApproval == null)
                    return;
                if(this.CustomVehicleShipplanApproval.VehicleWarehouseId <= default(int)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_CustomVehicleShipplanApproval_WarehouseNameIsNotNull);
                    return;
                }

                if(string.IsNullOrEmpty(this.CustomVehicleShipplanApproval.VehicleCategoryCode)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_CustomVehicleShipplanApproval_VehicleCategoryIdIsNotNull);
                    return;
                }
                if(this.CustomVehicleShipplanApproval.VehicleOrderType <= default(int)) {
                    UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_CustomVehicleShipplanApproval_VehicleOrderTypeIsNotNull);
                    return;
                }

                //执行查询
                this.DomainContext.Load(this.DomainContext.查询预分车关联单以及相关信息Query(this.CustomVehicleShipplanApproval.VehicleWarehouseId, this.CustomVehicleShipplanApproval.VehicleOrderType, this.CustomVehicleShipplanApproval.VehicleCategoryCode, vehicleShipplanApproval.DealerId), LoadBehavior.RefreshCurrent, loadOp => {
                    if(loadOp.HasError) {
                        loadOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                        return;
                    }
                    this.VehicleForShippingPlanApproves.Clear();
                    foreach(var vehicleForShippingPlanApprove in loadOp.Entities) {
                        vehicleForShippingPlanApprove.ShippingDate = DateTime.Now;
                        this.VehicleForShippingPlanApproves.Add(vehicleForShippingPlanApprove);
                    }
                }, null);
            });
        }

        public ICommand SearchCommand {
            get {
                if(this.searchCommand == null)
                    InitialCommand();
                return this.searchCommand;
            }
        }

        protected override void OnEditSubmitting() {
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null || !this.VehicleForShippingPlanApproveForSelectDataGridView.CommitEdit())
                return;
            this.VehicleOrderDetailConverter();
            vehicleShipplanApproval.ValidationErrors.Clear();
            if(vehicleShipplanApproval.VehicleShipplanApprovalDetails.Any(r => r.HasValidationErrors))
                foreach(var detail in vehicleShipplanApproval.VehicleShipplanApprovalDetails)
                    detail.ValidationErrors.Clear();
            CustomVehicleShipplanApproval.ValidationErrors.Clear();//该扩展实体中，由于实现了红框效果
            if(vehicleShipplanApproval.VehicleFundsTypeId == default(int)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_VehicleFundsTypeIsNull);
                return;
            }

            if(vehicleShipplanApproval.SourceCode == default(string)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_SourceCodeIsNull);
                return;
            }

            if(this.VehicleForShippingPlanApproveForSelectDataGridView.SelectedEntities == null || !this.VehicleForShippingPlanApproveForSelectDataGridView.SelectedEntities.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_VehicleShipplanApprovalDetailsIsNull);
                return;
            }
            //如果数据表格中有数据，而没有勾选时，给出提示
            if(!vehicleShipplanApproval.VehicleShipplanApprovalDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_PeleaseSelectDetails);
                return;
            }

            if(vehicleShipplanApproval.VehicleShipplanApprovalDetails.Any(r => String.IsNullOrEmpty(r.VIN))) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApprovalDetail_VINlsIsRequired);
                return;
            }

            if(CustomVehicleShipplanApproval.ApprovepAmount > CustomVehicleShipplanApproval.AvailableAmount) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_CustomVehicleShipplanApproval_ApprovepAmountIsGreateThanAvailableAmount);
                return;
            }

            var isSameCode = this.VehicleForShippingPlanApproveForSelectDataGridView.SelectedEntities.Cast<VehicleForShippingPlanApprove>().GroupBy(r => r.Code).Count() == 1;
            if(!isSameCode) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApprovalDetails_VehicleOrderIsNotSame);
                return;
            }

            var dealerId = this.VehicleForShippingPlanApproveForSelectDataGridView.SelectedEntities.Cast<VehicleForShippingPlanApprove>().First().DealerId;
            if(vehicleShipplanApproval.DealerId != dealerId) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApprovalDetails_DealerIsNotSameAndMainOrder);
                return;
            }

            if(vehicleShipplanApproval.ValidationErrors.Any())
                return;

            try {
                if(vehicleShipplanApproval.Can生成整车发车审批单)
                    vehicleShipplanApproval.生成整车发车审批单();
            } catch(Exception ex) {
                UIHelper.ShowAlertMessage(ex.Message);
            }
            ((IEditableObject)vehicleShipplanApproval).EndEdit();
            base.OnEditSubmitting();
        }

        private void VehicleOrderDetailConverter() {
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null)
                return;

            if(this.VehicleForShippingPlanApproveForSelectDataGridView.SelectedEntities == null || !this.VehicleForShippingPlanApproveForSelectDataGridView.SelectedEntities.Any())
                return;

            //清空之前已经添加的清单
            foreach(var detail in vehicleShipplanApproval.VehicleShipplanApprovalDetails) {
                vehicleShipplanApproval.VehicleShipplanApprovalDetails.Remove(detail);
            }

            //查询条件转换为当前主单的属性,单独放到此处便于维护(源单据Id和Code在GridView组件里面赋的值)
            vehicleShipplanApproval.VehicleFundsTypeId = this.CustomVehicleShipplanApproval.VehicleFundsType;
            vehicleShipplanApproval.VehicleWarehouseId = this.CustomVehicleShipplanApproval.VehicleWarehouseId;
            vehicleShipplanApproval.Remark = this.CustomVehicleShipplanApproval.Remark;
            vehicleShipplanApproval.Amount = this.CustomVehicleShipplanApproval.ApprovepAmount;


            foreach(var entity in this.VehicleForShippingPlanApproveForSelectDataGridView.SelectedEntities.Cast<VehicleForShippingPlanApprove>()) {
                vehicleShipplanApproval.VehicleShipplanApprovalDetails.Add(new VehicleShipplanApprovalDetail {
                    VehicleShipplanApproval = vehicleShipplanApproval,
                    VehicleId = entity.VehicleId,
                    VIN = entity.Vin,
                    SON = entity.Son,
                    ProductId = entity.ProductId,
                    ProductName = entity.ProductName,
                    ProductCode = entity.ProductCode,
                    ProductCategoryCode = entity.ProductCategoryCode,
                    ProductCategoryName = entity.ProductCategoryName,
                    Price = entity.Price,
                    Status = (int)DcsVehicleShipplanApprovalDetailStatus.新增,
                    ActualShippingDate = entity.ShippingDate,//客户端虚拟属性
                    EstimatedShippingDate = entity.EstimatedShippingDate,
                    DispatchWarehouseName = entity.VehicleWarehouseName,
                    DispatchWarehouseId = entity.VehicleWarehouseId
                });
                vehicleShipplanApproval.DealerCode = entity.DealerCode;
                vehicleShipplanApproval.DealerId = entity.DealerId;
                vehicleShipplanApproval.DealerName = entity.DealerName;
            }
        }

        //退出时重新初始化当前页面的变量
        protected override void OnEditCancelled() {
            base.OnEditCancelled();
            this.ClearGridViewFilters();
        }

        //提交成功时重新初始化当前页面的变量，及清空数据表格
        protected override void OnEditSubmitted() {
            base.OnEditSubmitted();
            this.ClearGridViewFilters();
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        //清空Gridview的FilterItem 的Value，在页面加载时重新查询以达到清空数据表格数据的目的，和清空当前变量的赋值
        private void ClearGridViewFilters() {
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null)
                return;
            vehicleShipplanApproval.VehicleWarehouseId = default(int);
            var composite = this.VehicleForShippingPlanApproveForSelectDataGridView.FilterItem as CompositeFilterItem;
            if(composite == null)
                return;

            var newComposite = new CompositeFilterItem();
            foreach(var filter in composite.Filters) {
                switch(filter.MemberName) {
                    case "VehicleOrder.Code":
                        filter.Value = default(string);
                        newComposite.Filters.Add(filter);
                        break;
                    case "VehicleWarehouseId":
                        filter.Value = default(int);
                        newComposite.Filters.Add(filter);
                        break;
                }
            }

            this.VehicleForShippingPlanApproveForSelectDataGridView.FilterItem = newComposite;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null)
                return;

            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            //由于查询可用余额的经销商编号是必填的，此处添加用户提醒
            if(vehicleShipplanApproval.DealerId == default(int)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleShipplanApproval_DealerCodeIsNull);
                return;
            }
            var keyValuePair = e.AddedItems[0] as KeyValuePair;
            if(keyValuePair == null || (keyValuePair.UserObject as VehicleFundsType) == null)
                return;

            var vehicleFundsTypeId = ((VehicleFundsType)(keyValuePair.UserObject)).Id;
            this.DomainContext.Load(this.DomainContext.查询可用余额Query(vehicleShipplanApproval.DealerId, vehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                var vehicleCustomerAccount = loadOp.Entities.FirstOrDefault();
                vehicleShipplanApproval.VehicleCustomerAccount = vehicleCustomerAccount;
                if(vehicleCustomerAccount == null) {
                    CustomVehicleShipplanApproval.AvailableAmount = default(int);
                    return;
                }
                CustomVehicleShipplanApproval.AvailableAmount = vehicleCustomerAccount.CustomerCredenceAmount + vehicleCustomerAccount.AccountBalance - (vehicleCustomerAccount.PendingAmount.HasValue ? vehicleCustomerAccount.PendingAmount.Value : 0);
            }, null);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string property) {
            if(this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}

