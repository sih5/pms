﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ServiceModel.DomainServices.Client;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleWarehouseDataEditView {
        private KeyValueManager keyValueManager;
        private readonly string[] kvNames = new[] {
            "VehicleWarehouse_Type","Storage_Strategy"
        };

        public VehicleWarehouseDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.KeyValueManager.Register(kvNames);
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleWarehouse;
            }
        }

        public object KvTypes {
            get {
                return this.KeyValueManager[this.kvNames[0]];
            }
        }

        public object KvStoragePolicies {
            get {
                return this.KeyValueManager[this.kvNames[1]];
            }
        }

        private KeyValueManager KeyValueManager {
            get {
                return this.keyValueManager ?? (this.keyValueManager = new KeyValueManager());
            }
        }

        private void CreateUI() {
            var queryWindowTiledRegion = DI.GetQueryWindow("TiledRegion");
            queryWindowTiledRegion.SelectionDecided += this.QueryWindowTiledRegion_SelectionDecided;
            this.popupTextBoxTiledRegion.PopupContent = queryWindowTiledRegion;

            this.KeyValueManager.LoadData();
        }

        private void QueryWindowTiledRegion_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var tiledRegion = queryWindow.SelectedEntities.Cast<TiledRegion>().FirstOrDefault();
            if(tiledRegion == null)
                return;
            var vehicleWarehouse = this.DataContext as VehicleWarehouse;
            if(vehicleWarehouse == null)
                return;

            var localTiledRegion = tiledRegion;
            this.DomainContext.Load(this.DomainContext.GetTiledRegionsQuery().Where(r => r.Id == localTiledRegion.Id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                vehicleWarehouse.TiledRegion = loadOp.Entities.SingleOrDefault();
                vehicleWarehouse.RegionId = tiledRegion.Id;
            }, null);

            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override void OnEditSubmitting() {
            var vehicleWarehouse = this.DataContext as VehicleWarehouse;
            if(vehicleWarehouse == null)
                return;
            vehicleWarehouse.ValidationErrors.Clear();
            if(vehicleWarehouse.Type == default(int))
                vehicleWarehouse.ValidationErrors.Add(new ValidationResult(String.Format(VehicleSalesUIStrings.DataEditView_Validation_VehicleWarehouse_ThisPropertyIsNull1, new ValidationResult(Utils.GetEntityLocalizedName(typeof(VehicleWarehouse), "Type"))), new[] { "Type" }));
            if(vehicleWarehouse.StoragePolicy == default(int))
                vehicleWarehouse.ValidationErrors.Add(new ValidationResult(String.Format(VehicleSalesUIStrings.DataEditView_Validation_VehicleWarehouse_ThisPropertyIsNull1, new ValidationResult(Utils.GetEntityLocalizedName(typeof(VehicleWarehouse), "StoragePolicy"))), new[] { "StoragePolicy" }));

            //避免更新TileRegion
            foreach(var tileRegion in this.DomainContext.TiledRegions.Where(r => r.EntityState == EntityState.Modified))
                this.DomainContext.TiledRegions.Detach(tileRegion);

            if(vehicleWarehouse.ValidationErrors.Any())
                return;
            ((IEditableObject)vehicleWarehouse).EndEdit();
            base.OnEditSubmitting();
        }


        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleWarehouseWithDetailsByIdQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
