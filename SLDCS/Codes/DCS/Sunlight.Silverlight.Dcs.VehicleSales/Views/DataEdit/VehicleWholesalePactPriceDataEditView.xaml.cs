﻿
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Controls.Business;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows;
using Telerik.Windows.Controls;
using SelectionChangedEventArgs = Telerik.Windows.Controls.SelectionChangedEventArgs;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class VehicleWholesalePactPriceDataEditView {
        private ProductCategoryDataTreeView productCategoryDataTreeView;
        private DataGridViewBase productForVehicleWholesalePactPriceDataGridView;
        private ObservableCollection<Product> products;
        private ObservableCollection<KeyValuePair> kvVehicleSalesTypes;
        private ButtonItem btnSaveCancleReturn;
        private VehicleSalesType tempVehicleSalesType;

        public VehicleWholesalePactPriceDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
            this.Loaded += VehicleWholesalePactPriceDataEditView_Loaded;
            this.DataContextChanged += VehicleWholesalePactPriceDataEditView_DataContextChanged;
        }

        void VehicleWholesalePactPriceDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehicleWholesalePactPrice = this.DataContext as VehicleWholesalePactPrice;
            if(vehicleWholesalePactPrice == null)
                return;
            if(vehicleWholesalePactPrice.Id != default(int)) {
                ProductCategoryDataTreeView.Visibility = Visibility.Collapsed;
                ProductForVehicleWholesalePactPriceDataGridView.Visibility = Visibility.Collapsed;
                if(btnSaveCancleReturn != null)
                    this.UnregisterButton(btnSaveCancleReturn);
            } else {
                ProductCategoryDataTreeView.Visibility = Visibility.Visible;
                ProductForVehicleWholesalePactPriceDataGridView.Visibility = Visibility.Visible;
                if(btnSaveCancleReturn != null)
                    this.RegisterButton(btnSaveCancleReturn);
            }
        }

        void VehicleWholesalePactPriceDataEditView_Loaded(object sender, RoutedEventArgs e) {
            this.DomainContext.Load(this.DomainContext.GetVehicleSalesTypesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvVehicleSalesTypes.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KvVehicleSalesTypes.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.VehicleSalesTypeName,
                        UserObject = entity
                    });
                }
                this.Products.Clear();
                //此代码不能写在Loaded方法的首行，否则会出现，编辑时，下拉列表销售类型集合无数据
                var vehicleWholesalePactPrice = this.DataContext as VehicleWholesalePactPrice;
                if(vehicleWholesalePactPrice == null)
                    return;

                if(loadOp.Entities == null || !loadOp.Entities.Any() || vehicleWholesalePactPrice.EntityState != EntityState.New)
                    return;
                tempVehicleSalesType = loadOp.Entities.First();
                vehicleWholesalePactPrice.VehicleSalesTypeId = loadOp.Entities.First().Id;
                vehicleWholesalePactPrice.VehicleSalesTypeName = loadOp.Entities.First().VehicleSalesTypeName;
                vehicleWholesalePactPrice.VehicleSalesTypeCode = loadOp.Entities.First().VehicleSalesTypeCode;
            }, null);


        }

        public ObservableCollection<KeyValuePair> KvVehicleSalesTypes {
            get {
                return this.kvVehicleSalesTypes ?? (this.kvVehicleSalesTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        public ObservableCollection<Product> Products {
            get {
                return products ?? (this.products = new ObservableCollection<Product>());
            }
            set {
                this.products = value;
            }
        }

        private ProductCategoryDataTreeView ProductCategoryDataTreeView {
            get {
                if(this.productCategoryDataTreeView == null) {
                    this.productCategoryDataTreeView = new ProductCategoryDataTreeView();
                    this.productCategoryDataTreeView.DomainContext = new DcsDomainContext();
                    this.productCategoryDataTreeView.OnTreeViewItemClick += ProductCategoryDataTreeView_OnTreeViewItemClick;
                }
                return this.productCategoryDataTreeView;
            }
        }

        private void ProductCategoryDataTreeView_OnTreeViewItemClick(object sender, RadRoutedEventArgs radRoutedEventArgs) {
            this.Products.Clear();
            var selectedItem = ((RadTreeView)sender).SelectedItem as RadTreeViewItem;
            if(selectedItem == null || !(selectedItem.Tag is ProductCategory) || selectedItem.Items.Count > 0)
                return;
            var productCategory = selectedItem.Tag as ProductCategory;
            var productAffiProductCategories = productCategory.ProductAffiProductCategories;
            foreach(ProductAffiProductCategory productAffiProductCategory in productAffiProductCategories) {
                var vehicleWholesalePrice = productAffiProductCategory.Product.VehicleWholesalePrices.SingleOrDefault();
                productAffiProductCategory.Product.CurrentPrice = vehicleWholesalePrice != null ? vehicleWholesalePrice.Price : 0;
                Products.Add(productAffiProductCategory.Product);
            }
        }

        public DataGridViewBase ProductForVehicleWholesalePactPriceDataGridView {
            get {
                if(this.productForVehicleWholesalePactPriceDataGridView == null) {
                    this.productForVehicleWholesalePactPriceDataGridView = DI.GetDataGridView("ProductForVehicleWholesalePactPrice");
                    this.productForVehicleWholesalePactPriceDataGridView.DataContext = this;
                    this.productForVehicleWholesalePactPriceDataGridView.RowDoubleClick += productForVehicleWholesalePactPriceDataGridView_RowDoubleClick;
                }
                return this.productForVehicleWholesalePactPriceDataGridView;
            }
        }

        void productForVehicleWholesalePactPriceDataGridView_RowDoubleClick(object sender, DataGridViewRowDoubleClickEventArgs e) {
            var vehicleWholesalePactPrice = this.DataContext as VehicleWholesalePactPrice;
            var selectProduct = ProductForVehicleWholesalePactPriceDataGridView.SelectedEntities.Cast<Product>().FirstOrDefault();
            if(selectProduct == null || vehicleWholesalePactPrice == null)
                return;
            vehicleWholesalePactPrice.ProductId = selectProduct.Id;
            vehicleWholesalePactPrice.ProductName = selectProduct.Name;
            vehicleWholesalePactPrice.ProductCode = selectProduct.Code;
            vehicleWholesalePactPrice.ProductPrice = selectProduct.CurrentPrice;
        }


        private void CreateUI() {
            ProductCategoryDataTreeView.SetValue(Grid.ColumnProperty, 1);
            this.Root.Children.Add(ProductCategoryDataTreeView);
            ProductForVehicleWholesalePactPriceDataGridView.SetValue(Grid.ColumnProperty, 2);
            this.Root.Children.Add(ProductForVehicleWholesalePactPriceDataGridView);
            btnSaveCancleReturn = new ButtonItem {
                Title = VehicleSalesUIStrings.Action_Title_SaveNoExit,
                Icon = new Uri("/Sunlight.Silverlight.Dcs;component/Images/new-bill.png", UriKind.Relative),
                Command = new Core.Command.DelegateCommand(this.SaveNoExit)
            };
            this.RegisterButton(btnSaveCancleReturn);

            var queryWindow = DI.GetQueryWindow("Dealer");
            queryWindow.SelectionDecided += this.queryWindow_SelectionDecided;
            this.popupTextBoxDealer.PopupContent = queryWindow;
        }

        private void queryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;

            var dealer = queryWindow.SelectedEntities.Cast<Dealer>().FirstOrDefault();
            if(dealer == null)
                return;

            var vehicleWholesalePactPrice = this.DataContext as VehicleWholesalePactPrice;
            if(vehicleWholesalePactPrice == null)
                return;
            vehicleWholesalePactPrice.DealerId = dealer.Id;
            vehicleWholesalePactPrice.DealerCode = dealer.Code;
            vehicleWholesalePactPrice.DealerName = dealer.Name;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        //TODO：设计要求该下拉为只读 同时要求添加该事件 本段代码将成为垃圾代码
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var comboBox = sender as RadComboBox;
            var vehicleWholesalePactPrice = this.DataContext as VehicleWholesalePactPrice;
            if(vehicleWholesalePactPrice == null || comboBox == null)
                return;

            if(e.AddedItems == null || e.AddedItems.Count != 1)
                return;
            var keyValuePair = e.AddedItems[0] as KeyValuePair;
            if(keyValuePair == null || (keyValuePair.UserObject as VehicleSalesType) == null)
                return;
            var vehicleSalesType = (VehicleSalesType)keyValuePair.UserObject;
            vehicleWholesalePactPrice.VehicleSalesTypeCode = vehicleSalesType.VehicleSalesTypeCode;
            this.ProductCategoryDataTreeView.EntityQuery = this.productCategoryDataTreeView.DomainContext.查询产品分类以及批发价格Query(vehicleWholesalePactPrice.VehicleSalesTypeId);
            this.productCategoryDataTreeView.RefreshDataTree();
            this.Products.Clear();
            if(e.RemovedItems != null && e.RemovedItems.Count == 1) {
                DcsUtils.Confirm(VehicleSalesUIStrings.DataEditView_Confirm_ClearHaveAlreadySelectedPrices, () => {
                    vehicleWholesalePactPrice.ProductId = default(int);
                    vehicleWholesalePactPrice.ProductName = string.Empty;
                    vehicleWholesalePactPrice.ProductCode = string.Empty;
                }, () => {
                    comboBox.SelectionChanged -= this.ComboBox_SelectionChanged;
                    comboBox.SelectedItem = e.RemovedItems[0];
                    comboBox.SelectionChanged += this.ComboBox_SelectionChanged;
                });
            }
        }

        private void SaveNoExit() {
            if(CheckVehicleWholesalePactPrice())
                this.DomainContext.SubmitChanges(submitOp => {
                    if(submitOp.HasError) {
                        if(!submitOp.IsErrorHandled)
                            submitOp.MarkErrorAsHandled();
                        DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    } else {
                        var newVehicleWholesalePactPrice = new VehicleWholesalePactPrice();
                        newVehicleWholesalePactPrice.Status = (int)DcsVehiclePriceStatus.新建;
                        newVehicleWholesalePactPrice.BranchId = BaseApp.Current.CurrentUserData.EnterpriseId;
                        newVehicleWholesalePactPrice.BranchCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                        newVehicleWholesalePactPrice.BranchName = BaseApp.Current.CurrentUserData.EnterpriseName;
                        newVehicleWholesalePactPrice.Code = GlobalVar.ASSIGNED_BY_SERVER;
                        if(tempVehicleSalesType != null) {
                            newVehicleWholesalePactPrice.VehicleSalesTypeId = tempVehicleSalesType.Id;
                            newVehicleWholesalePactPrice.VehicleSalesTypeName = tempVehicleSalesType.VehicleSalesTypeName;
                            newVehicleWholesalePactPrice.VehicleSalesTypeCode = tempVehicleSalesType.VehicleSalesTypeCode;
                        }
                        this.DomainContext.VehicleWholesalePactPrices.Add(newVehicleWholesalePactPrice);
                        this.DataContext = null;
                        this.SetValue(DataContextProperty, newVehicleWholesalePactPrice);
                        UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Confirm_SaveSuccess);
                        this.Products.Clear();
                    }
                }, null);
        }


        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetVehicleWholesalePactPriceWithDetailsQuery().Where(v => v.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        private bool CheckVehicleWholesalePactPrice() {
            var vehicleWholesalePactPrice = this.DataContext as VehicleWholesalePactPrice;
            if(vehicleWholesalePactPrice == null)
                return false;
            vehicleWholesalePactPrice.ValidationErrors.Clear();
            if(DateTime.Compare(vehicleWholesalePactPrice.ExecutionDate, vehicleWholesalePactPrice.ExpireDate) < 0) {
                vehicleWholesalePactPrice.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleWholesalePactPrice_ExecutionDateUpperThanExpireDate, new[] {
                    "ExpireDate"
                }));
            }
            if(vehicleWholesalePactPrice.TreatyPrice == 0) {
                vehicleWholesalePactPrice.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_VehicleWholesalePactPrice_TreatyPriceIsZero, new[] {
                    "TreatyPrice"
                }));
            }
            if(vehicleWholesalePactPrice.HasValidationErrors)
                return false;
            ((IEditableObject)vehicleWholesalePactPrice).EndEdit();
            return true;
        }

        protected override void OnEditSubmitting() {
            if(CheckVehicleWholesalePactPrice())
                base.OnEditSubmitting();
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_VehicleWholesalePactPrice;
            }
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }
    }
}
