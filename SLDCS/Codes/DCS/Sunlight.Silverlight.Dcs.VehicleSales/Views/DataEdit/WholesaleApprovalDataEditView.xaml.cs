﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class WholesaleApprovalDataEditView {
        private DataGridViewBase wholesaleApprovalDetailForEditDataGridView;
        public WholesaleApprovalDataEditView() {
            InitializeComponent();
            this.DataContextChanged += WholesaleApprovalDataEditView_DataContextChanged;
            this.Initializer.Register(this.CreateUI);
        }

        private void WholesaleApprovalDataEditView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var wholesaleApproval = this.DataContext as WholesaleApproval;
            if(wholesaleApproval == null)
                return;
            wholesaleApproval.PropertyChanged += wholesaleApproval_PropertyChanged;
        }

        private void wholesaleApproval_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            var wholesaleApproval = this.DataContext as WholesaleApproval;
            if(wholesaleApproval == null)
                return;
            switch(e.PropertyName) {
                case "CustomerCode":
                    var customerCodeError = wholesaleApproval.ValidationErrors.FirstOrDefault(error => error.MemberNames.Contains("CustomerCode"));
                    if(customerCodeError != null)
                        wholesaleApproval.ValidationErrors.Remove(customerCodeError);
                    break;
                case "CustomerName":
                    var customerNameError = wholesaleApproval.ValidationErrors.FirstOrDefault(error => error.MemberNames.Contains("CustomerName"));
                    if(customerNameError != null)
                        wholesaleApproval.ValidationErrors.Remove(customerNameError);
                    break;
                case "CustomerType":
                    var customerTypeError = wholesaleApproval.ValidationErrors.FirstOrDefault(error => error.MemberNames.Contains("CustomerType"));
                    if(customerTypeError != null)
                        wholesaleApproval.ValidationErrors.Remove(customerTypeError);
                    break;
            }
        }

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_WholesaleApproval;
            }
        }

        private DataGridViewBase WholesaleApprovalDetailForEditDataGridView {
            get {
                if(this.wholesaleApprovalDetailForEditDataGridView == null) {
                    this.wholesaleApprovalDetailForEditDataGridView = DI.GetDataGridView("WholesaleApprovalDetailForEdit");
                    this.wholesaleApprovalDetailForEditDataGridView.DomainContext = this.DomainContext;
                }
                return this.wholesaleApprovalDetailForEditDataGridView;
            }
        }

        protected virtual void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("WholesaleApproval"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(WholesaleApproval), "WholesaleApprovalDetails"), null, this.WholesaleApprovalDetailForEditDataGridView);
            detailEditView.SetValue(Grid.ColumnProperty, 2);

            this.Root.Children.Add(detailEditView);
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetWholesaleApprovalDetailWithDetailsQuery(id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    var wholesaleApprovalEntity = entity;
                    var wholesaleApproval = this.DataContext as WholesaleApproval;
                    if(wholesaleApproval != null) {
                        wholesaleApproval.KeyAccountId = wholesaleApprovalEntity.KeyAccount.Id;
                        wholesaleApproval.KeyAccountCode = wholesaleApprovalEntity.KeyAccount.KeyAccountCode;
                        wholesaleApproval.ContactPerson = wholesaleApprovalEntity.KeyAccount.ContactPerson;
                        wholesaleApproval.CustomerCode = wholesaleApprovalEntity.KeyAccount.Customer.Code;
                        wholesaleApproval.ContactJobPosition = wholesaleApprovalEntity.ContactJobPosition;
                        wholesaleApproval.CustomerName = wholesaleApprovalEntity.KeyAccount.Customer.Name;
                        wholesaleApproval.ContactPhone = wholesaleApprovalEntity.KeyAccount.ContactPhone;
                        wholesaleApproval.CustomerType = wholesaleApprovalEntity.KeyAccount.Customer.CustomerType;
                        wholesaleApproval.ContactCellPhone = wholesaleApprovalEntity.KeyAccount.ContactCellPhone;
                        wholesaleApproval.CustomerAddress = wholesaleApprovalEntity.KeyAccount.Customer.Address;
                        wholesaleApproval.ContactFax = wholesaleApprovalEntity.KeyAccount.ContactFax;
                        wholesaleApproval.CustomerPostCode = wholesaleApprovalEntity.KeyAccount.Customer.PostCode;
                        wholesaleApproval.CompanyName = wholesaleApprovalEntity.KeyAccount.Customer.CompanyName;
                        wholesaleApproval.CustomerProperty = wholesaleApprovalEntity.CustomerProperty;
                    }
                }
                this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var wholesaleApproval = this.DataContext as WholesaleApproval;
            if(wholesaleApproval == null || !this.WholesaleApprovalDetailForEditDataGridView.CommitEdit())
                return;
            wholesaleApproval.PostAuditStatusId = 0;//TODO:新增WholesaleApproval实例时默认给PostAuditStatusId属性赋值为0
            wholesaleApproval.ValidationErrors.Clear();
            if(string.IsNullOrEmpty(wholesaleApproval.CustomerCode))
                wholesaleApproval.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_CustomerCodeIsNotNull, new[] {
                    "CustomerCode"
                }));
            if(string.IsNullOrEmpty(wholesaleApproval.CustomerName))
                wholesaleApproval.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_CustomerCodeIsNotNull, new[] {
                    "CustomerName"
                }));
            if(wholesaleApproval.CustomerProperty == null)
                wholesaleApproval.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_CustomerPropertyIsNotNull, new[] {
                    "CustomerProperty"
                }));
            if(string.IsNullOrEmpty(wholesaleApproval.CustomerAddress))
                wholesaleApproval.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_CustomerAddressIsNotNull, new[] {
                    "CustomerAddress"
                }));
            if(string.IsNullOrEmpty(wholesaleApproval.CustomerPostCode))
                wholesaleApproval.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_CustomerPostCodeIsNotNull, new[] {
                    "CustomerPostCode"
                }));
            if(wholesaleApproval.IfTenderingOrCentralPurchase == null) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_IfTenderingOrCentralPurchaseIsNotNull);
                return;
            }
            if(string.IsNullOrEmpty(wholesaleApproval.ContactPerson))
                wholesaleApproval.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_ContactPersonIsNotNull, new[] {
                    "ContactPerson"
                }));
            if(string.IsNullOrEmpty(wholesaleApproval.ContactPhone))
                wholesaleApproval.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_ContactPhoneIsNotNull, new[] {
                    "ContactPhone"
                }));
            if(string.IsNullOrEmpty(wholesaleApproval.ContactCellPhone))
                wholesaleApproval.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_ContactCellPhoneIsNotNull, new[] {
                    "ContactCellPhone"
                }));
            if(string.IsNullOrEmpty(wholesaleApproval.ContactJobPosition))
                wholesaleApproval.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_ContactJobPositionIsNotNull, new[] {
                    "ContactJobPosition"
                }));
            if(!wholesaleApproval.WholesaleApprovalDetails.Any()) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehRetailPriceChangeApp_VehRetailPriceChangeAppIsNull);
                return;
            }
            if(wholesaleApproval.ValidationErrors.Any())
                return;
            ((IEditableObject)wholesaleApproval).EndEdit();
            base.OnEditSubmitting();
        }
    }
}
