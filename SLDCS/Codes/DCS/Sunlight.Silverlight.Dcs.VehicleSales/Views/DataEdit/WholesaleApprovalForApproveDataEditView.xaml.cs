﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class WholesaleApprovalForApproveDataEditView {
        private DataGridViewBase wholesaleApprovalDetail;
        private bool isPass = false;

        private DataGridViewBase WholesaleApprovalDetail {
            get {
                return this.wholesaleApprovalDetail ?? (this.wholesaleApprovalDetail = DI.GetDataGridView("WholesaleApprovalDetailForApproveForEdit"));
            }
            set {
                this.wholesaleApprovalDetail = value;
            }
        }

        private void CreateUI() {
            this.Root.Children.Add(DI.GetDataEditPanel("WholesaleApprovalForApprove"));
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detailEditView = new DcsDetailDataEditView();
            detailEditView.Register(Utils.GetEntityLocalizedName(typeof(WholesaleApproval), "WholesaleApprovalDetails"), null, this.WholesaleApprovalDetail);
            detailEditView.SetValue(Grid.ColumnProperty, 2);
            detailEditView.UnregisterButton(detailEditView.InsertButton);
            detailEditView.UnregisterButton(detailEditView.DeleteButton);
            this.Root.Children.Add(detailEditView);
        }

        protected override void OnEditSubmitting() {
            if(!this.WholesaleApprovalDetail.CommitEdit())
                return;
            var wholesaleApproval = this.DataContext as WholesaleApproval;
            if(wholesaleApproval == null)
                return;
            if(wholesaleApproval.KeyAccount == null || wholesaleApproval.KeyAccountId == default(int)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_KeyAccountIsNull);
                return;
            }
            if(string.IsNullOrEmpty(wholesaleApproval.KeyAccount.KeyAccountCode)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_KeyAccountCodeIsNull);
                return;
            }
            wholesaleApproval.ValidationErrors.Clear();
            if(this.RadRadioButtonApprovePass.IsChecked == false && this.RadRadioButtondiApproveNoPass.IsChecked == false) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_WholesaleApproval_ApprovePassIsNull);
                return;
            }
            ((IEditableObject)wholesaleApproval).EndEdit();
            try {
                if(isPass) {
                    if(wholesaleApproval.Can批售审批单审核通过)
                        wholesaleApproval.批售审批单审核通过(wholesaleApproval.KeyAccountCode);
                } else
                    if(wholesaleApproval.Can批售审批单审核不通过)
                        wholesaleApproval.批售审批单审核不通过(wholesaleApproval.KeyAccountCode);

            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        private void RadRadioButtonApprovePass_Activate(object sender, RadRoutedEventArgs e) {
            this.isPass = true;
        }

        private void RadRadioButtondiApproveNoPass_Activate(object sender, Telerik.Windows.RadRoutedEventArgs e) {
            this.isPass = false;
        }

        private void DcsDataEditViewBase_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.RadRadioButtonApprovePass.IsChecked = false;
            this.RadRadioButtondiApproveNoPass.IsChecked = false;
        }

        private void LoadEntityToApproval(int id) {
            this.DomainContext.Load(DomainContext.根据多级审核状态查询批售审批单Query(BaseApp.Current.CurrentUserData.UserId), LoadBehavior.RefreshCurrent, loadOP => {
                if(loadOP.HasError) {
                    if(!loadOP.IsErrorHandled)
                        loadOP.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOP);
                    return;
                }
                var entity = loadOP.Entities.Where(w => w.Id == id).FirstOrDefault();
                if(entity != null) {
                    var keyAccount = entity.KeyAccount;
                    if(keyAccount != null) {
                        if(string.IsNullOrWhiteSpace(entity.KeyAccount.KeyAccountCode))
                            entity.KeyAccount.KeyAccountCode = GlobalVar.ASSIGNED_BY_SERVER;
                        var customer = keyAccount.Customer;
                        if(customer != null) {
                            entity.CustomerCode = customer.Code;
                            entity.CustomerName = customer.Name;
                            entity.CustomerType = customer.CustomerType;
                            entity.CustomerAddress = customer.Address;
                            entity.CustomerPostCode = customer.PostCode;
                            entity.CompanyName = customer.CompanyName;
                        }
                        entity.ContactPerson = keyAccount.ContactPerson;
                        entity.ContactJobPosition = keyAccount.ContactJobPosition;
                        entity.ContactPhone = keyAccount.ContactPhone;
                        entity.ContactCellPhone = keyAccount.ContactCellPhone;
                        entity.ContactFax = keyAccount.ContactFax;
                    }
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_Approval_WholesaleApprovalForApprove;
            }
        }

        protected override bool OnRequestCanSubmit() {
            return true;
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToApproval((int)id));
            else
                this.LoadEntityToApproval((int)id);
        }

        public WholesaleApprovalForApproveDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }
    }
}
