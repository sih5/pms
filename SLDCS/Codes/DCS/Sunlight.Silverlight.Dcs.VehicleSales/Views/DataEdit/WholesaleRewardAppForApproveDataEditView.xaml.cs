﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class WholesaleRewardAppForApproveDataEditView {
        private DataGridViewBase wholesaleRewardAppForApproveForApproveDataGridView;

        private DataGridViewBase WholesaleRewardAppForApproveForApproveDataGridView {
            get {
                if(this.wholesaleRewardAppForApproveForApproveDataGridView == null)
                    this.wholesaleRewardAppForApproveForApproveDataGridView = DI.GetDataGridView("WholesaleRewardAppDetailForApprove");
                return this.wholesaleRewardAppForApproveForApproveDataGridView;
            }
        }

        public WholesaleRewardAppForApproveDataEditView() {
            this.InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        protected override string Title {
            get {
                return VehicleSalesUIStrings.DataEditView_Title_WholesaleRewardAppForApprove;
            }
        }

        private void CreateUI() {
            var dataEditPanel = DI.GetDataEditPanel("WholesaleRewardAppForApprove");
            Grid.SetRow(dataEditPanel, 1);
            this.Root.Children.Add(dataEditPanel);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detail = new DcsDetailDataEditView();
            detail.Register(VehicleSalesUIStrings.DetailPanel_Title_WholesaleRewardAppDetail, null, () => this.WholesaleRewardAppForApproveForApproveDataGridView);
            Grid.SetColumn(detail, 2);
            this.Root.Children.Add(detail);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetWholesaleRewardAppWithKeyAccountAndCustomerQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null) {
                    this.SetObjectToEdit(entity);
                }
            }, null);
        }

        protected override void OnEditSubmitting() {
            if(!this.WholesaleRewardAppForApproveForApproveDataGridView.CommitEdit())
                return;
            var wholesaleRewardApp = this.DataContext as WholesaleRewardApp;
            if(wholesaleRewardApp == null)
                return;
            wholesaleRewardApp.ValidationErrors.Clear();
            ((IEditableObject)wholesaleRewardApp).EndEdit();
            try {
                switch(wholesaleRewardApp.Status) {
                    case (int)DcsWholesaleRewardAppStatus.有效:
                        if(wholesaleRewardApp.Can审核批售奖励申请单)
                            wholesaleRewardApp.审核批售奖励申请单();
                        break;
                    case (int)DcsWholesaleRewardAppStatus.新增:
                        if(wholesaleRewardApp.Can批售奖励申请单审核不通过)
                            wholesaleRewardApp.批售奖励申请单审核不通过();
                        break;
                }
            } catch(ValidationException ex) {
                UIHelper.ShowAlertMessage(ex.Message);
                return;
            }
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
