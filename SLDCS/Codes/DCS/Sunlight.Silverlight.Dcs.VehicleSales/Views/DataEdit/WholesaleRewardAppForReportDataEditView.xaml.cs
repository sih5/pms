﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Panels.DataEdit;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit {
    public partial class WholesaleRewardAppForReportDataEditView {
        private WholesaleRewardAppForReportDataEditPanel dataEditPanel;
        private DataGridViewBase wholesaleRewardAppDetailForEdit;

        protected override string BusinessName {
            get {
                return DcsUIStrings.BusinessName_WholesaleRewardApp;
            }
        }

        public WholesaleRewardAppForReportDataEditPanel DataEditPanel {
            get {
                if(this.dataEditPanel == null) {
                    this.dataEditPanel = DI.GetDataEditPanel("WholesaleRewardAppForReport") as WholesaleRewardAppForReportDataEditPanel;
                    this.dataEditPanel.DomainContext = this.DomainContext;
                }
                return dataEditPanel;
            }
        }

        public DataGridViewBase WholesaleRewardAppDetailForEdit {
            get {
                if(this.wholesaleRewardAppDetailForEdit == null) {
                    this.wholesaleRewardAppDetailForEdit = DI.GetDataGridView("WholesaleRewardAppDetailForReportForEdit");
                    this.wholesaleRewardAppDetailForEdit.DomainContext = this.DomainContext;
                }
                return this.wholesaleRewardAppDetailForEdit;
            }
        }

        public WholesaleRewardAppForReportDataEditView() {
            InitializeComponent();
            this.Initializer.Register(this.CreateUI);
        }

        private void CreateUI() {
            Grid.SetRow(this.DataEditPanel, 1);
            this.Root.Children.Add(this.DataEditPanel);
            this.Root.Children.Add(this.CreateVerticalLine(1));
            var detail = new DcsDetailDataEditView();
            detail.UnregisterButton(detail.DeleteButton);
            detail.UnregisterButton(detail.InsertButton);
            detail.Register(Utils.GetEntityLocalizedName(typeof(WholesaleRewardApp), "WholesaleRewardAppDetails"), null, this.WholesaleRewardAppDetailForEdit);
            Grid.SetColumn(detail, 2);
            this.Root.Children.Add(detail);
        }

        private void LoadEntityToEdit(int id) {
            this.DomainContext.Load(this.DomainContext.GetWholesaleRewardAppWithKeyAccountAndCustomerQuery().Where(e => e.Id == id), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(!loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                    return;
                }
                var entity = loadOp.Entities.SingleOrDefault();
                if(entity != null)
                    this.SetObjectToEdit(entity);
            }, null);
        }

        protected override void OnEditSubmitting() {
            var wholesaleRewardApp = this.DataContext as WholesaleRewardApp;
            if(wholesaleRewardApp == null || !this.WholesaleRewardAppDetailForEdit.CommitEdit())
                return;
            wholesaleRewardApp.ValidationErrors.Clear();
            if(wholesaleRewardApp.WholesaleApprovalId == default(int))
                wholesaleRewardApp.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleRewardApp_WholesaleApprovalIdIsRequired, new[] { "WholesaleApprovalId" }));
            if(wholesaleRewardApp.WholesaleRewardAppDetails.Any(e => string.IsNullOrEmpty(e.InvoiceTitle)))
                wholesaleRewardApp.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleRewardApp_InvoiceTitleIsRequired, new[] { "InvoiceTitle" }));
            if(wholesaleRewardApp.WholesaleRewardAppDetails.Any(e => string.IsNullOrEmpty(e.InvoiceCode)))
                wholesaleRewardApp.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleRewardApp_InvoiceCodeIsRequired, new[] { "InvoiceCode" }));
            if(wholesaleRewardApp.WholesaleRewardAppDetails.Any(e => e.VehicleDeliveryTime == default(DateTime)))
                wholesaleRewardApp.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleRewardApp_VehicleDeliveryTimeIsRequired, new[] { "VehicleDeliveryTime" }));
            if(wholesaleRewardApp.WholesaleRewardAppDetails.Any(e => e.AppliedRewardAmount == default(decimal)))
                wholesaleRewardApp.ValidationErrors.Add(new ValidationResult(VehicleSalesUIStrings.DataEditView_Validation_WholesaleRewardApp_AppliedRewardAmountIsRequired, new[] { "AppliedRewardAmount" }));
            if(wholesaleRewardApp.HasValidationErrors) {
                UIHelper.ShowNotification(string.Join(Environment.NewLine, wholesaleRewardApp.ValidationErrors));
                return;
            }
            ((IEditableObject)wholesaleRewardApp).EndEdit();
            base.OnEditSubmitting();
        }

        public override void SetObjectToEditById(object id) {
            if(this.DomainContext == null)
                this.Initializer.Register(() => this.LoadEntityToEdit((int)id));
            else
                this.LoadEntityToEdit((int)id);
        }
    }
}
