﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class AuditRoleAffiPerconnelForEditDataGridView : AuditRoleAffiPerconnelDataGridView{
        //覆盖父类查询
        protected override void AuditRoleAffiPerconnelDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("AuditRoleAffiPerconnels");
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        IsReadOnly = true,
                        Name = "Personnel.Name"
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = false;
        }
    }
}
