﻿
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class CustomerForSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] KvNames = new[] {
            "Sex_Type"
        };

        public CustomerForSelectDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "Code"
                    }, new KeyValuesColumnItem {
                        Name = "Gender",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }, new ColumnItem {
                        Name = "CellPhoneNumber",
                        Title =  VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_CellPhoneNumber,
                    }, new ColumnItem {
                        Name = "SalesDealerName",
                        Title =  VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_SalesDealerName,
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetCustomers";
        }

        protected override System.Type EntityType {
            get {
                return typeof(Customer);
            }
        }
    }
}
