﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class DealerForRegionDataGridView : DcsDataGridViewBase {
        public DealerForRegionDataGridView() {
            this.Initializer.Register(this.Intitialize);
        }

        private void Intitialize() {
            this.GridView.DataLoaded += this.GridView_DataLoaded;
        }

        private void GridView_DataLoaded(object sender, EventArgs e) {

            this.GridView.SelectAll();
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.IsReadOnly = true;
        }

        protected override Type EntityType {
            get {
                return typeof(Dealer);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("Dealers");
        }
    }
}
