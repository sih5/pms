﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class DealerForecastDataGridView : DcsDataGridViewBase {
        private readonly string[] KvNames = new[] {
            "DealerForecast_Status"
        };

        public DealerForecastDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_DealerForecast_Code
                    }, new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "DealerName"
                    }, new ColumnItem {
                        Name = "CreatorName",
                    }, new ColumnItem {
                        Name = "CreateTime",
                    }, new ColumnItem {
                        Name = "ForecastDate",
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }, new ColumnItem {
                        Name = "ApproverName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_DealerForecast_ApproverName
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_DealerForecast_ApproveTime
                    }, new ColumnItem {
                        Name = "ModifierName",
                    }, new ColumnItem {
                        Name = "ModifyTime",
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerForecasts";
        }

        protected override System.Type EntityType {
            get {
                return typeof(DealerForecast);
            }
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                      "DealerForecastDetail"  
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["ForecastDate"]).DataFormatString = "d";
        }
    }
}
