﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class DealerForecastDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "DealerForecastDetail_VehicleModel","DealerForecastDetail_WagonType","DealerForecastDetail_EngineCylinder"
        };
        public DealerForecastDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.DealerForecastDetailDataGridView_DataContextChanged;
        }

        private void DealerForecastDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealerForecast = e.NewValue as DealerForecast;
            if(dealerForecast == null || dealerForecast.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "DealerForecastId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = dealerForecast.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                       Name = "VehicleModel",
                       KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                       Name = "WagonType",
                       KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new KeyValuesColumnItem {
                       Name = "EngineCylinder",
                       KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem {
                       Name = "WholesaleForecastAmount"
                    }, new ColumnItem {
                       Name = "RetailForecastAmount"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerForecastDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerForecastDetails";
        }
    }
}
