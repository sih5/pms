﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class DealerRegionManagerAffiForImportDataGridView : DcsDataGridViewBase {


        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "RegionManager"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerRegionManagerAffi);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
