﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class DealerTransactionBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "DealerTransactionBill_Status"
        };

        public DealerTransactionBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "DealerBuyerName"
                    }, new ColumnItem {
                        Name = "DealerSellerName"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "ApproverName",
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ApproverName
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ApproveTime
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },   new ColumnItem {
                        Name = "DealerBuyerWarehouseCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_DealerTransactionBill_DealerBuyerWarehouseCode
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerTransactionBillWithDetails";
        }

        protected override System.Type EntityType {
            get {
                return typeof(DealerTransactionBill);
            }
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                      "DealerTransactionDetail"  
                    }
                };
            }
        }
    }
}
