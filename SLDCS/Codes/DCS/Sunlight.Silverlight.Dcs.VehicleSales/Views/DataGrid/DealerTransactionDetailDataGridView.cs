﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class DealerTransactionDetailDataGridView : DcsDataGridViewBase {
        public DealerTransactionDetailDataGridView() {
            this.DataContextChanged += this.DealerTransactionBillDetailDataGridViewDataContextChanged;
        }

        private void DealerTransactionBillDetailDataGridViewDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var dealerTransactionBill = e.NewValue as DealerTransactionBill;
            if(dealerTransactionBill == null || dealerTransactionBill.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "DealerTransactionBillId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = dealerTransactionBill.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "ProductCode"
                    }, new ColumnItem {
                       Name = "Quantity"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerTransactionDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetDealerTransactionDetails";
        }
    }
}
