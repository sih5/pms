﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class DealerTransactionDetailForApproveDataGridView : DcsDataGridViewBase {
        private QueryWindowBase dealerVehicleStockDropDownQueryWindow;

        private QueryWindowBase DealerVehicleStockDropDownQueryWindow {
            get {
                if(dealerVehicleStockDropDownQueryWindow == null) {
                    dealerVehicleStockDropDownQueryWindow = DI.GetQueryWindow("DealerVehicleStockDropDown");
                    dealerVehicleStockDropDownQueryWindow.Loaded += DealerVehicleStockDropDownQueryWindow_Loaded;
                    dealerVehicleStockDropDownQueryWindow.SelectionDecided += this.DealerVehicleStockDropDownQueryWindow_SelectionDecided;
                }
                return this.dealerVehicleStockDropDownQueryWindow;
            }
        }

        //由于在默认的弹出中已经增加了企业登录当前登录企业，此处不需要添加企业的过滤条件
        private void DealerVehicleStockDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;

            var dataContext = queryWindow.DataContext as DealerTransactionDetail;
            if(dataContext == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "ProductCode", dataContext.ProductCode
            });
            queryWindow.ExchangeData(null, "SetQueryItemEnabled", new object[] {
                "Common", "ProductCode", false
            });
        }

        private void DealerVehicleStockDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var dealerTransactionDetail = queryWindow.DataContext as DealerTransactionDetail;
            if(dealerTransactionDetail == null)
                return;
            var dealerVehicleStock = queryWindow.SelectedEntities.Cast<DealerVehicleStock>().FirstOrDefault();
            if(dealerVehicleStock == null)
                return;

            dealerTransactionDetail.VIN = dealerVehicleStock.VIN;
            dealerTransactionDetail.VehicleId = dealerVehicleStock.VehicleId;
            var parent = queryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new DropDownTextBoxColumnItem {
                        Name = "VIN",
                        DropDownContent = this.DealerVehicleStockDropDownQueryWindow
                    }, new ColumnItem {
                        Name = "ProductCode",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerTransactionDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerTransactionDetails");
        }
    }
}
