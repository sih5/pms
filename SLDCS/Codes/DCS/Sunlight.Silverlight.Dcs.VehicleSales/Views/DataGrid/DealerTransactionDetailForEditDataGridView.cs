﻿using System;
using System.Linq;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class DealerTransactionDetailForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase vehicleModelAffiProductDropDownQueryWindow;
        private QueryWindowBase VehicleModelAffiProductDropDownQueryWindow {
            get {
                if(vehicleModelAffiProductDropDownQueryWindow == null) {
                    vehicleModelAffiProductDropDownQueryWindow = DI.GetQueryWindow("VehicleModelAffiProductDropDown");
                    vehicleModelAffiProductDropDownQueryWindow.SelectionDecided += this.VehicleModelAffiProductDropDownQueryWindow_SelectionDecided;
                }
                return this.vehicleModelAffiProductDropDownQueryWindow;
            }
        }

        private void VehicleModelAffiProductDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var dropDownQueryWindowBase = sender as DcsDropDownQueryWindowBase;
            if(dropDownQueryWindowBase == null)
                return;
            var dealerTransactionDetail = dropDownQueryWindowBase.DataContext as DealerTransactionDetail;
            if(dealerTransactionDetail == null)
                return;
            var vehicleModelAffiProduct = dropDownQueryWindowBase.SelectedEntities.Cast<VehicleModelAffiProduct>().FirstOrDefault();
            if(vehicleModelAffiProduct == null)
                return;

            dealerTransactionDetail.ProductId = vehicleModelAffiProduct.ProductId;
            dealerTransactionDetail.ProductName = vehicleModelAffiProduct.ProductName;
            dealerTransactionDetail.ProductCode = vehicleModelAffiProduct.ProductCode;
            dealerTransactionDetail.ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode;
            dealerTransactionDetail.ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName;
            dealerTransactionDetail.Quantity = 1;
            var parent = dropDownQueryWindowBase.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new DropDownTextBoxColumnItem {
                        Name = "ProductCode",
                        DropDownContent = this.VehicleModelAffiProductDropDownQueryWindow,
                        IsReadOnly = false
                    }, new ColumnItem {
                        Name = "Quantity",
                        IsReadOnly = false
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerTransactionDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }

        void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            var dealerTransactionBill = this.DataContext as DealerTransactionBill;
            if(dealerTransactionBill == null)
                return;
            if(dealerTransactionBill.DealerSellerId == default(int) && dealerTransactionBill.DealerBuyerWarehouseId == default(int)) {
                UIHelper.ShowNotification("卖方经销商与预入仓库不能为空");
                e.Cancel = true;
                return;
            }
            if(dealerTransactionBill.DealerSellerId == default(int)) {
                UIHelper.ShowNotification("卖方经销商不能为空");
                e.Cancel = true;
                return;
            }
            if(dealerTransactionBill.DealerBuyerWarehouseId == default(int)) {
                UIHelper.ShowNotification("预入仓库不能为空");
                e.Cancel = true;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("DealerTransactionDetails");
        }
    }
}
