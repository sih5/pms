﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class DealerVehicleMonthQuotaForImportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                  new ColumnItem {
                      Name = "YearOfPlan"
                  },new ColumnItem {
                      Name = "MonthOfPlan"
                  },new ColumnItem {
                      Name = "DealerCode"
                  },new ColumnItem {
                      Name = "DealerName"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_DealerVehicleMonthQuota_VehicleModelCategoryName,
                      Name = "VehicleModelCategoryName"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_DealerVehicleMonthQuota_WholesaleQuotaQuantity,
                      Name = "WholesaleQuotaQuantity"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_DealerVehicleMonthQuota_RetailQuotaQuantity,
                      Name = "RetailQuotaQuantity"
                  }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerVehicleMonthQuota);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
