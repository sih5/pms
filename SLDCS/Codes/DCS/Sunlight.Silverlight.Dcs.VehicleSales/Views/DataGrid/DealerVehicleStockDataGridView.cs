﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class DealerVehicleStockDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode"
                    },new ColumnItem {
                        Name = "VIN"
                    },new ColumnItem {
                        Name = "VehicleWarehouseName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(DealerVehicleStock);
            }
        }


        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetDealerVehicleStocks";
        }
    }
}
