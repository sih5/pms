﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class JumpShutoffDayDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };
        public JumpShutoffDayDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "YearOfOrder"
                    }, new ColumnItem {
                        Name = "MonthOfOrder"
                    }, new ColumnItem {
                        Name = "JumpShutoffDate",
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreateTime",
                    }, new ColumnItem {
                        Name = "CreatorName",
                    }, new ColumnItem {
                        Name = "ModifierName",
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetJumpShutoffDays";
        }

        protected override Type EntityType {
            get {
                return typeof(JumpShutoffDay);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["JumpShutoffDate"]).DataFormatString = "d";
        }
    }
}