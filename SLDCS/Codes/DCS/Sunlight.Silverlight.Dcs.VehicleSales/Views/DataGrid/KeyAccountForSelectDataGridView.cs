﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class KeyAccountForSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "Customer_CustomerType"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="Customer.Code",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_Code
                    }, new ColumnItem{
                        Name="Customer.Name",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_Name
                    }, new ColumnItem{
                        Name="DealerCode"
                    }, new ColumnItem{
                        Name="DealerName"
                    }, new KeyValuesColumnItem{
                        Name="Customer.CustomerType",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]],
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_CustomerType
                    }, new ColumnItem{
                        Name="IsConfirmed"
                    }, new ColumnItem{
                        Name="KeyAccountCode"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(KeyAccount);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetKeyAccountsForCAM";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters == null)
                return null;

            var createTimeFilters = filters.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;

            switch(parameterName) {
                case "name":
                    var nameFilters = filters.Filters.SingleOrDefault(item => item.MemberName == "Customer.Name");
                    if(nameFilters != null)
                        return nameFilters.Value;
                    return null;
                case "status":
                    var statusFilters = filters.Filters.SingleOrDefault(item => item.MemberName == "Status");
                    if(statusFilters != null) {
                        return statusFilters.Value;
                    }
                    return null;
                case "customerType":
                    var customerTypeFilters = filters.Filters.SingleOrDefault(item => item.MemberName == "Customer.CustomerType");
                    if(customerTypeFilters != null) {
                        return customerTypeFilters.Value;
                    }
                    return null;
                case "companyScale":
                    var companyScaleFilters = filters.Filters.SingleOrDefault(item => item.MemberName == "CompanyScale");
                    if(companyScaleFilters != null) {
                        return companyScaleFilters.Value;
                    }
                    return null;
                case "startCreateTime":
                    if(createTimeFilters == null)
                        return null;
                    return createTimeFilters.Filters.ElementAt(0).Value;
                case "endCreateTime":
                    if(createTimeFilters == null)
                        return null;
                    return createTimeFilters.Filters.ElementAt(1).Value;
                case "dealerName":
                    var dealerNameFilters = filters.Filters.SingleOrDefault(item => item.MemberName == "DealerName");
                    if(dealerNameFilters != null)
                        return dealerNameFilters.Value;
                    return null;

            }
            return null;
        }

        public KeyAccountForSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
