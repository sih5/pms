﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class MultiLevelAuditConfigDetailDataGridView : DcsDataGridViewBase {
        public MultiLevelAuditConfigDetailDataGridView() {
            this.DataContextChanged += this.MultiLevelAuditConfigDetailDataGridViewDataContextChanged;
        }

        private void MultiLevelAuditConfigDetailDataGridViewDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var multiLevelAuditConfig = e.NewValue as MultiLevelAuditConfig;
            if(multiLevelAuditConfig == null || multiLevelAuditConfig.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "MultiLevelAuditConfigId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = multiLevelAuditConfig.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_MultiLevelAuditConfigDetail_AuditHierarchySettingId,
                       Name = "AuditHierarchySetting.PositionName"
                    }, new ColumnItem {
                         Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_MultiLevelAuditConfigDetail_InitialPostAuditStatusId,
                       Name = "InitialPostAuditStatus.Name"
                    }, new ColumnItem {
                         Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_MultiLevelAuditConfigDetail_ApprovedPostAuditStatusId,
                       Name = "ApprovedPostAuditStatus.Name"
                    }, new ColumnItem {
                         Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_MultiLevelAuditConfigDetail_DisapprovedPostAuditStatusId,
                       Name = "DisapprovedPostAuditStatus.Name"
                    },new ColumnItem {
                       Name = "IsSpecialOperation"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(MultiLevelAuditConfigDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetMultiLevelAuditConfigDetailsByPostAuditStatusId";
        }
    }
}
