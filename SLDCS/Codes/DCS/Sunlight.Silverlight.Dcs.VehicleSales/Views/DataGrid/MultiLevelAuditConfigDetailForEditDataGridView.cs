﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class MultiLevelAuditConfigDetailForEditDataGridView : DcsDataGridViewBase {
        private ObservableCollection<KeyValuePair> kvPositionNames = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<KeyValuePair> kvApprovedPostAuditStatus = new ObservableCollection<KeyValuePair>();
        private ObservableCollection<PostAuditStatu> approvedPostAuditStatus = new ObservableCollection<PostAuditStatu>();
        private ObservableCollection<PostAuditStatu> status = new ObservableCollection<PostAuditStatu>();
        private bool temp;

        private ObservableCollection<KeyValuePair> KvPositionNames {
            get {
                return this.kvPositionNames ?? (this.kvPositionNames = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<KeyValuePair> KvApprovedPostAuditStatus {
            get {
                return this.kvApprovedPostAuditStatus ?? (this.kvApprovedPostAuditStatus = new ObservableCollection<KeyValuePair>());
            }
        }

        private ObservableCollection<PostAuditStatu> ApprovedPostAuditStatus {
            get {
                return this.approvedPostAuditStatus ?? (this.approvedPostAuditStatus = new ObservableCollection<PostAuditStatu>());
            }
        }

        private ObservableCollection<PostAuditStatu> Status {
            get {
                return this.status ?? (this.status = new ObservableCollection<PostAuditStatu>());
            }
        }

        private void InitializeData() {
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.Load(domainContext.GetPostAuditStatusQuery(), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var auditStatus in loadOp.Entities) {
                    this.KvApprovedPostAuditStatus.Add(new KeyValuePair {
                        Key = auditStatus.Id,
                        Value = auditStatus.Name
                    });
                    this.ApprovedPostAuditStatus.Add(auditStatus);
                }
                var sortDesc = new SortDescriptor {
                    Member = "AuditHierarchySettingId",
                    SortDirection = ListSortDirection.Ascending
                };
                //todo:临时解决
                //通过排序 刷新下拉列表中不显示的值
                this.GridView.SortDescriptors.Add(sortDesc);
                this.GridView.SortDescriptors.Remove(sortDesc);
            }, null);

            domainContext.Load(domainContext.GetAuditHierarchySettingsQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }
                foreach(var entity in loadOp.Entities) {
                    if(this.KvPositionNames.Any(r => r.Key == entity.Id)) {
                        continue;
                    }
                    this.KvPositionNames.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.PositionName
                    });
                }
                var sortDesc = new SortDescriptor {
                    Member = "AuditHierarchySettingId",
                    SortDirection = ListSortDirection.Ascending
                };
                //todo:临时解决
                //通过排序 刷新下拉列表中不显示的值
                this.GridView.SortDescriptors.Add(sortDesc);
                this.GridView.SortDescriptors.Remove(sortDesc);
            }, null);
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            this.temp = false;
            var auditConfig = this.DataContext as MultiLevelAuditConfig;
            if(auditConfig == null)
                e.Cancel = true;
            if(auditConfig != null && auditConfig.MultiLevelAuditConfigDetails.Any(d => d.ApprovedPostAuditStatusId == 0 || d.DisapprovedPostAuditStatusId == 0)) {
                UIHelper.ShowNotification("审核通过状态或审核不通过状态不能为空");
                e.Cancel = true;
            }
            var sum = this.GridView.Items.Count;
            if(sum > 1) {
                var r = (MultiLevelAuditConfigDetail)this.GridView.Items[sum - 1];
                if(r.InitialPostAuditStatusId == null || r.InitialPostAuditStatusId == 0) {
                    if(this.GridView.Items.Cast<MultiLevelAuditConfigDetail>().Any(d => d.InitialPostAuditStatusId != null && d.InitialPostAuditStatusId != 0)) {
                        UIHelper.ShowNotification("清单中除了第一条，其它初始状态如果上面有一条是不为空的，下面的初始状态都不能为空");
                        e.Cancel = true;
                    }
                }
            }
        }

        private void GridView_PreparingCellForEdit(object sender, GridViewPreparingCellForEditEventArgs e) {
            var detail = e.Row.DataContext as MultiLevelAuditConfigDetail;
            var auditConfig = this.DataContext as MultiLevelAuditConfig;
            if(detail == null || auditConfig == null)
                return;
            var comboBox = e.EditingElement as RadComboBox;
            if(comboBox == null)
                return;
            if(this.GridView.SelectedItem == null)
                return;
            switch(e.Column.UniqueName) {
                case "InitialPostAuditStatusId":
                    var currentIndex = this.GridView.Items.IndexOf(this.GridView.SelectedItem);
                    this.Status.Clear();
                    for(int i = 0; i < currentIndex; i++) {
                        if(((MultiLevelAuditConfigDetail)this.GridView.Items[i]).ApprovedPostAuditStatusId == 0) {
                            UIHelper.ShowNotification("审核通过状态不能为空");
                            return;
                        }
                        var entity = ((MultiLevelAuditConfigDetail)this.GridView.Items[i]).ApprovedPostAuditStatus;
                        if(this.Status.Any(s => s.Id == entity.Id))
                            continue;
                        ;
                        this.Status.Add(entity);
                    }
                    comboBox.ItemsSource = this.Status.Select(entity => new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                    break;
                case "ApprovedPostAuditStatusId":
                    comboBox.ItemsSource = this.ApprovedPostAuditStatus.Where(entity => entity.AuditHierarchySettingId == detail.AuditHierarchySettingId).Select(entity => new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });

                    break;
                case "DisapprovedPostAuditStatusId":
                    comboBox.ItemsSource = this.ApprovedPostAuditStatus.Where(entity => entity.AuditHierarchySettingId == detail.AuditHierarchySettingId).Select(entity => new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                    break;
            }
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs eventArgs) {
            var auditConfig = this.DataContext as MultiLevelAuditConfig;
            if(auditConfig == null)
                return;
            var detail = eventArgs.Row.DataContext as MultiLevelAuditConfigDetail;
            if(detail == null)
                return;
            if(eventArgs.Cell.Column.UniqueName == "InitialPostAuditStatusId" && auditConfig.MultiLevelAuditConfigDetails.Count == 1)
                eventArgs.Cancel = true;

            if(detail.AuditHierarchySettingId == default(int)) {
                if(eventArgs.Cell.Column.UniqueName == "ApprovedPostAuditStatusId" || eventArgs.Cell.Column.UniqueName == "DisapprovedPostAuditStatusId") {
                    eventArgs.Cancel = true;
                }
                if((eventArgs.Cell.Column.UniqueName == "ApprovedPostAuditStatusId" || eventArgs.Cell.Column.UniqueName == "DisapprovedPostAuditStatusId") && temp) {
                    eventArgs.Cancel = true;
                    UIHelper.ShowNotification("必须先选择岗位名称之后，才能选择审核通过状态和审核不通过状态");
                }
            }
            temp = true;
            if(detail.EntityState != EntityState.New && detail.EntityState != EntityState.Detached) {
                if(eventArgs.Cell.Column.UniqueName == "AuditHierarchySettingId")
                    eventArgs.Cancel = true;
            }
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var detail = e.Cell.DataContext as MultiLevelAuditConfigDetail;
            if(detail == null)
                return;
            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "AuditHierarchySettingId":
                    var auditConfig = this.DataContext as MultiLevelAuditConfig;
                    if(auditConfig == null)
                        return;
                    if(e.NewValue == null)
                        return;
                    if(auditConfig.MultiLevelAuditConfigDetails.Where(d => !ReferenceEquals(d, e.Cell.DataContext)).Any(d => d.AuditHierarchySettingId == (int)e.NewValue)) {
                        e.IsValid = false;
                        e.ErrorMessage = string.Format("岗位名称不能重复" /*VehicleSalesUIStrings.DataEditView_Validation_MultiLevelAuditConfig_AuditHierarchySettingIdIsNotRepeat*/);
                    }
                    break;
                case "ApprovedPostAuditStatusId":
                    if(detail.ApprovedPostAuditStatusId != 0 || detail.DisapprovedPostAuditStatusId != 0) {
                        var comboBox = e.EditingElement as RadComboBox;
                        if(comboBox == null)
                            return;
                        var keyValuePair = comboBox.SelectedItem as KeyValuePair;
                        if(keyValuePair == null)
                            return;
                        if(detail.DisapprovedPostAuditStatusId == keyValuePair.Key) {
                            e.IsValid = false;
                            e.ErrorMessage = string.Format("审核通过状态不能等于审核不通过状态" /*VehicleSalesUIStrings.DataEditView_Validation_MultiLevelAuditConfig_DisapprovedPostAuditStatusIdIsNotEque*/);
                        }

                    }
                    break;
                case "DisapprovedPostAuditStatusId":
                    if(detail.ApprovedPostAuditStatusId != 0 || detail.DisapprovedPostAuditStatusId != 0) {
                        var comboBox = e.EditingElement as RadComboBox;
                        if(comboBox == null)
                            return;
                        var keyValuePair = comboBox.SelectedItem as KeyValuePair;
                        if(keyValuePair == null)
                            return;
                        if(detail.ApprovedPostAuditStatusId == keyValuePair.Key) {
                            e.IsValid = false;
                            e.ErrorMessage = string.Format("审核通过状态不能等于审核不通过状态" /*VehicleSalesUIStrings.DataEditView_Validation_MultiLevelAuditConfig_DisapprovedPostAuditStatusIdIsNotEque*/);
                        }
                    }
                    break;
            }
        }

        private void GridView_Deleting(object sender, GridViewDeletingEventArgs e) {
            var c = this.GridView.SelectedItems.Count;
            var r = (MultiLevelAuditConfigDetail)this.GridView.SelectedItems[c - 1];
            var detail = this.DataContext as MultiLevelAuditConfig;
            if(detail != null)
                foreach(var item in detail.MultiLevelAuditConfigDetails) {
                    if(item.InitialPostAuditStatusId == r.ApprovedPostAuditStatusId) {
                        item.InitialPostAuditStatusId = 0;
                    }
                }
        }

        private void GridView_CollectionChanged(object sender, NotifyCollectionChangedEventArgs eventArgs) {
            if(this.GridView.Items.Count > 0)
                this.GridView.SelectedItem = this.GridView.Items[0];
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new KeyValuesColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_MultiLevelAuditConfigDetail_AuditHierarchySettingId,
                        Name = "AuditHierarchySettingId",
                        KeyValueItems = this.KvPositionNames
                    }, new KeyValuesColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_MultiLevelAuditConfigDetail_InitialPostAuditStatusId,
                        Name = "InitialPostAuditStatusId",
                        KeyValueItems = this.KvApprovedPostAuditStatus
                    }, new KeyValuesColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_MultiLevelAuditConfigDetail_ApprovedPostAuditStatusId,
                        Name = "ApprovedPostAuditStatusId",
                        KeyValueItems = this.KvApprovedPostAuditStatus
                    }, new KeyValuesColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_MultiLevelAuditConfigDetail_DisapprovedPostAuditStatusId,
                        Name = "DisapprovedPostAuditStatusId",
                        KeyValueItems = this.KvApprovedPostAuditStatus
                    }, new ColumnItem {
                        Name = "IsSpecialOperation"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(MultiLevelAuditConfigDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("MultiLevelAuditConfigDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = (SelectionMode)Telerik.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            this.GridView.PreparingCellForEdit += GridView_PreparingCellForEdit;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.Deleting += GridView_Deleting;
            this.GridView.Items.CollectionChanged += this.GridView_CollectionChanged;
        }

        public MultiLevelAuditConfigDetailForEditDataGridView() {
            this.Initializer.Register(this.InitializeData);
        }
    }
}