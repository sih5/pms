﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class OEMVehicleMonthQuotaDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public OEMVehicleMonthQuotaDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "YearOfPlan"
                    }, new ColumnItem {
                        Name = "MonthOfPlan"
                    }, new ColumnItem {
                        Name = "VehicleModelCategoryName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_OEMVehicleMonthQuota_VehicleModelCategoryName
                    }, new ColumnItem {
                        Name = "RetailQuotaQuantity",
                        TextAlignment=System.Windows.TextAlignment.Right,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_OEMVehicleMonthQuota_RetailQuotaQuantity
                    }, new ColumnItem {
                        Name = "WholesaleQuotaQuantity",
                        TextAlignment=System.Windows.TextAlignment.Right,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_OEMVehicleMonthQuota_WholesaleQuotaQuantity
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetOEMVehicleMonthQuotas";
        }

        protected override Type EntityType {
            get {
                return typeof(OEMVehicleMonthQuota);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }
    }
}
