﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class OEMVehicleMonthQuotaForImportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "YearOfPlan"
                    }, new ColumnItem {
                        Name = "MonthOfPlan"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_OEMVehicleMonthQuota_VehicleModelCategoryName,
                        Name = "VehicleModelCategoryName"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_OEMVehicleMonthQuota_WholesaleQuotaQuantity,
                        Name = "WholesaleQuotaQuantity"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_OEMVehicleMonthQuota_RetailQuotaQuantity,
                        Name = "RetailQuotaQuantity"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        protected override Type EntityType {
            get {
                return typeof(OEMVehicleMonthQuota);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
