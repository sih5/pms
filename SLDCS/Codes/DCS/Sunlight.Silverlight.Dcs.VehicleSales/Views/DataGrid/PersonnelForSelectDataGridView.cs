﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    /// <summary>
    /// 修改界面，供选择数据使用，使用整行选中
    /// </summary>
    public class PersonnelForSelectDataGridView :DcsDataGridViewBase{
        private readonly string[] kvNames = new[] {
            "MasterData_Status"
        };

        public PersonnelForSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(Personnel);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return true;
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "CellNumber"
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode=SelectionMode.Single;
            this.GridView.ShowGroupPanel = false;

        }

        protected override string OnRequestQueryName() {
            return "GetPersonnels";
        }
    }
}
