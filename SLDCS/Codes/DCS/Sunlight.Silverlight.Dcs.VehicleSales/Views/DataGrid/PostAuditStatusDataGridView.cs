﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class PostAuditStatusDataGridView : DcsDataGridViewBase {
        public PostAuditStatusDataGridView() {
            this.DataContextChanged += PostAuditStatusDataGridView_DataContextChanged;
        }

        private void PostAuditStatusDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var auditHierarchySetting = e.NewValue as AuditHierarchySetting;
            if(auditHierarchySetting == null || auditHierarchySetting.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "AuditHierarchySettingId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = auditHierarchySetting.Id;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(PostAuditStatu);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Name"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetPostAuditStatus";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
