﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class ProductDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "Prodcut_Color"
        };
        public int ProductCategoryId;

        public ProductDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    }, new ColumnItem {
                        Name = "CanBeOrdered"
                    }, new ColumnItem {
                        Name = "IfPurchasable"
                    }, new ColumnItem {
                        Name = "OrderCycle"
                    }, new ColumnItem {
                        Name = "PurchaseCycle",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Product_PurchaseCycle
                    }, new ColumnItem {
                        Name = "IfOption"
                    }, new ColumnItem {
                        Name = "ParameterDescription"
                    }, new ColumnItem {
                        Name = "Version"
                    }, new KeyValuesColumnItem{
                        Name = "Color",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Configuration"
                    }, new ColumnItem {
                        Name = "EngineCylinder"
                    }, new ColumnItem {
                        Name = "EmissionStandard"
                    }, new ColumnItem {
                        Name = "ManualAutomatic"
                    }, new ColumnItem {
                        Name = "ColorCode"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            if(this.ProductCategoryId != 0) {
                return "根据产品分类ID查询产品信息";
            }
            return "GetProducts";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            if(queryName == "根据产品分类ID查询产品信息")
                switch(parameterName) {
                    case "productCategoryId":
                        return this.ProductCategoryId;
                }
            return null;
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "ProductDetail"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(Product);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }
    }
}
