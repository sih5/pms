﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class ProductionPlanDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "ProductionPlan_Status"
        };

        public ProductionPlanDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "YearOfOrder"
                    }, new ColumnItem {
                        Name = "MonthOfOrder"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ProductionPlan_ProductCategoryCode,
                        Name = "ProductCategoryCode"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ProductionPlan_ProductionPlanSON,
                        Name = "ProductionPlanSON"
                    }, new KeyValuesColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ProductionPlan_Status,
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ProductionPlan_RolloutDate,
                        Name = "RolloutDate"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(ProductionPlan);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetProductionPlans";
        }
    }
}
