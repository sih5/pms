﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class ProductionPlanForImportDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "ProductionPlan_Status"
        };

        public ProductionPlanForImportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "YearOfOrder"
                    }, new ColumnItem {
                        Name = "MonthOfOrder"
                    }, new ColumnItem {
                        Name = "ProductCategoryCode"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ProductionPlan_ProductionPlanSON,
                        Name = "ProductionPlanSON"
                    }, new KeyValuesColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ProductionPlan_Status,
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "RolloutDate"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["YearOfOrder"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["MonthOfOrder"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["RolloutDate"]).DataFormatString = "d";
        }

        protected override Type EntityType {
            get {
                return typeof(ProductionPlan);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
