﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class RegionConfugurationForEditDataGridView : DcsDataGridViewBase {
        private ObservableCollection<KeyValuePair> kvRegionConfugurations;

        public ObservableCollection<KeyValuePair> KvRegionConfugurations {
            get {
                return this.kvRegionConfugurations ?? (this.kvRegionConfugurations = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[]{
                    new KeyValuesColumnItem{
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_RegionConfuguration_Code,
                        Name = "RegionConfugurationId",
                        KeyValueItems=this.KvRegionConfugurations,
                    }, new ColumnItem{
                        Name="RegionConfuguration.Name",
                        IsReadOnly=true
                    }
                };
            }
        }

        private void GridView_Loaded(object sender, RoutedEventArgs e) {
            var domainContext = this.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.Load(domainContext.GetRegionConfugurationsQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    loadOp.MarkErrorAsHandled();
                    return;
                }

                foreach(var entity in loadOp.Entities) {
                    if(this.KvRegionConfugurations.Any(r => r.Key == entity.Id))
                        continue;

                    this.KvRegionConfugurations.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Code,
                        UserObject = entity
                    });
                }
            }, null);
        }

        protected override Type EntityType {
            get {
                return typeof(RegionalRoleConfig);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("RegionalRoleConfigs");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
            this.GridView.Loaded += GridView_Loaded;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            e.NewObject = new RegionalRoleConfig {
                Status = (int)DcsBaseDataStatus.有效
            };
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var regionalRoleConfig = e.Cell.DataContext as RegionalRoleConfig;
            if(regionalRoleConfig == null)
                return;
            if(e.Cell.DataColumn.DataMemberBinding.Path.Path == "RegionConfugurationId") {
                var radComboBox = e.EditingElement as RadComboBox;
                if(radComboBox == null)
                    return;
                var keyValuePair = radComboBox.SelectedItem as KeyValuePair;
                if(keyValuePair == null)
                    return;
                var regionConfuguration = (RegionConfuguration)(keyValuePair.UserObject);

                regionalRoleConfig.RegionConfuguration = regionConfuguration;
            }
        }
    }
}
