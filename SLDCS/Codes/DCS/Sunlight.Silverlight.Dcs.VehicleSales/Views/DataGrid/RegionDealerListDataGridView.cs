﻿using System;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class RegionDealerListDataGridView : DcsDataGridViewBase {

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Dealer.Code"
                    }, new ColumnItem {
                        Name = "Dealer.Name"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(RegionDealerList);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetRegionDealerListsWithDetals";
        }

        public RegionDealerListDataGridView() {
            this.DataContextChanged += RegionDealerListDataGridView_DataContextChanged;
        }

        private void RegionDealerListDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var regionConfuguration = e.NewValue as RegionConfuguration;
            if(regionConfuguration == null)
                return;
            if(this.FilterItem == null) {
                this.FilterItem = new FilterItem {
                    MemberName = "RegionConfugurationId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            }
            this.FilterItem.Value = regionConfuguration.Id;
            this.ExecuteQueryDelayed();
        }
    }
}
