﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class RegionDealerListForEditDataGridView : DcsDataGridViewBase {

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Dealer.Code",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Dealer.Name",
                        IsReadOnly =true
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.ShowGroupPanel = false;
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(RegionDealerList);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("RegionDealerLists");
        }
    }
}
