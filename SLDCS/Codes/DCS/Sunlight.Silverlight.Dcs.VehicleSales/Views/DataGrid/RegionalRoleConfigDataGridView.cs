﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class RegionalRoleConfigDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "BaseData_Status"
        };

        public RegionalRoleConfigDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[]{
                    new ColumnItem{
                        Name="AuditHierarchySetting.PositionName",
                        Title=VehicleSalesUIStrings.DataGridView_ColumnItem_Title_RegionalRoleConfigResource_PositionName
                    }, new ColumnItem{
                        Name="RegionConfuguration.Name",
                        Title=VehicleSalesUIStrings.DataGridView_ColumnItem_Title_RegionalRoleConfigResource_Name
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(RegionalRoleConfig);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetRegionalRoleConfigWithDetails";
        }
    }
}
