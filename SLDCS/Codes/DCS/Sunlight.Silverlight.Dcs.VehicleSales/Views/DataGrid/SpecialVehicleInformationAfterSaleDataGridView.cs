﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;


namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class SpecialVehicleInformationAfterSaleDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehicleInformation_VehicleType"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "RetainedCustomer.Customer.Name",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_Name
                    }, new ColumnItem {
                        Name = "VehicleInformation.VIN"
                    }, new ColumnItem {
                        Name = "VehicleInformation.SellPrice",
                        TextAlignment = TextAlignment.Right 
                    }, new ColumnItem {
                        Name = "VehicleInformation.SalesDate",
                        TextAlignment = TextAlignment.Right                 
                    }, new ColumnItem {
                        Name = "VehicleInformation.Mileage"
                    }, new ColumnItem {
                        Name = "VehicleInformation.VehicleLicensePlate"
                    }, new ColumnItem {
                        Name = "VehicleInformation.TiledRegion.ProvinceName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_TiledRegion_ProvinceName
                    }, new ColumnItem {
                        Name = "VehicleInformation.TiledRegion.CityName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_TiledRegion_CityName
                    }, new ColumnItem {
                        Name = "VehicleInformation.SalesInvoiceNumber",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleInformation_SalesInvoiceNumber
                    }, new ColumnItem {
                        Name = "VehicleInformation.FabricationDate",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "VehicleInformation.ProductCode"
                    }, new ColumnItem {
                        Name = "VehicleInformation.EngineSerialNumber",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleInformation_EngineSerialNumber
                    }, new ColumnItem {
                        Name = "RetainedCustomer.Customer.SalesAdvisorName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_SalesAdvisorName
                    }, new ColumnItem {
                        Name = "VehicleInformation.KeyCode",
                    }, new KeyValuesColumnItem {
                        Name = "VehicleInformation.VehicleType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(RetainedCustomerVehicleList);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询保有客户信息车辆信息";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters == null)
                return null;
            var createTimeFilters = filters.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "VehicleInformation.SalesDate")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
            switch(parameterName) {
                case "vin":
                    var vinFilters = filters.Filters.SingleOrDefault(item => item.MemberName == "VehicleInformation.VIN");
                    if(vinFilters != null)
                        return vinFilters.Value;
                    return null;
                case "beginsalesDate":
                    if(createTimeFilters == null)
                        return null;
                    return createTimeFilters.Filters.ElementAt(0).Value;
                case "finishsalesDate":
                    if(createTimeFilters == null)
                        return null;
                    return createTimeFilters.Filters.ElementAt(1).Value;
                case "branchName":
                    var branchNameFilters = filters.Filters.SingleOrDefault(item => item.MemberName == "VehicleInformation.SalesDealerName");
                    if(branchNameFilters != null)
                        return branchNameFilters.Value;
                    return null;
                case "vehicleType":
                    var inboundTypeFilter = filters.Filters.SingleOrDefault(item => item.MemberName == "VehicleInformation.VehicleType");
                    return inboundTypeFilter == null ? null : inboundTypeFilter.Value;
                case "name":
                    var nameFilters = filters.Filters.SingleOrDefault(item => item.MemberName == "RetainedCustomer.Customer.Name");
                    if(nameFilters != null)
                        return nameFilters.Value;
                    return null;
            }
            return null;
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)(this.GridView.Columns["VehicleInformation.FabricationDate"])).DataFormatString = "d";
            ((GridViewDataColumn)(this.GridView.Columns["VehicleInformation.SalesDate"])).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["VehicleInformation.SellPrice"]).DataFormatString = "c2";
        }

        public SpecialVehicleInformationAfterSaleDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}

