﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class SpecialVehicleInformationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
           "VehicleInformation_VehicleType","Vehicle_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem{
                    Name = "VIN"
                   },new ColumnItem{
                    Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleInformationDetail_ProductionPlanSON,
                    Name = "ProductionPlanSON"
                   },new ColumnItem{
                    Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleInformationDetail_ProductCategoryCode,
                    Name = "ProductCategoryCode"
                   },new ColumnItem{
                    Name = "ProductCode"
                   },new ColumnItem{
                    Name = "EngineSerialNumber"
                   },new KeyValuesColumnItem{
                    Name = "VehicleType",
                    KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                   },new ColumnItem{
                    Name = "RolloutDate"
                   },new ColumnItem{
                    Name = "OutOfFactoryDate"
                   },new ColumnItem{
                    Name = "SalesDate"
                   },new KeyValuesColumnItem{
                    Name = "Status",
                    KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                   }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleInformation);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleInformations";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            ((GridViewDataColumn)(this.GridView.Columns["RolloutDate"])).DataFormatString = "d";
            ((GridViewDataColumn)(this.GridView.Columns["OutOfFactoryDate"])).DataFormatString = "d";
            ((GridViewDataColumn)(this.GridView.Columns["SalesDate"])).DataFormatString = "d";
        }

        public SpecialVehicleInformationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
