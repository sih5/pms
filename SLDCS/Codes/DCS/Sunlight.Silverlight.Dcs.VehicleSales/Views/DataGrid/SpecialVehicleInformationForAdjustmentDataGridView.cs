﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class SpecialVehicleInformationForAdjustmentDataGridView : DcsDataGridViewBase {
        private SpecialVehicleChangeRecord specialVehicleChangeRecord;
        public SpecialVehicleInformationForAdjustmentDataGridView() {
            this.Initializer.Register(this.Initialize);
        }
        private void Initialize() {
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            if(this.SpecialVehicleChangeRecord.NewVehicleId == default(int)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehicleAvailableResourceIsNull);
                e.Cancel = true;
                return;
            }
            if(string.IsNullOrEmpty(this.SpecialVehicleChangeRecord.NewVIN)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_SpecialVehicleChangeRecord_NewVINIdIsNull);
                e.Cancel = true;
                return;
            }
            e.NewObject = new SpecialVehicleChangeRecord {
                OriginalVIN = this.SpecialVehicleChangeRecord.OriginalVIN,
                NewVIN = this.SpecialVehicleChangeRecord.NewVIN,
                ProductCode = this.SpecialVehicleChangeRecord.ProductCode,
                ProductCategoryCode = this.SpecialVehicleChangeRecord.ProductCategoryCode
            };

        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "OriginalVIN",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "NewVIN",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_SpecialVehicleChangeRecorde_ProductCode,
                        Name = "ProductCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_SpecialVehicleChangeRecorde_ProductCategoryCode,
                        Name = "ProductCategoryCode",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(SpecialVehicleChangeRecord);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("SpecialVehicleChangeRecords");
        }

        public SpecialVehicleChangeRecord SpecialVehicleChangeRecord {
            get {
                return this.specialVehicleChangeRecord ?? (this.specialVehicleChangeRecord = new SpecialVehicleChangeRecord());
            }
        }
    }
}
