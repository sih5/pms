﻿using System;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class SpecialVehicleInformationForTradeDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehicleInformation_VehicleType","Vehicle_Status"
        };

        public SpecialVehicleInformationForTradeDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem {
                        Name = "VIN"
                    }, new ColumnItem {
                        Name = "SON"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleInformation_ProductCategoryCode,
                        Name = "ProductCategoryCode"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "EngineSerialNumber"
                    }, new KeyValuesColumnItem {
                        Name = "VehicleType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "RolloutDate"
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleInformation);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            ((GridViewDataColumn)(this.GridView.Columns["RolloutDate"])).DataFormatString = "d";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleInformations");
        }
    }
}
