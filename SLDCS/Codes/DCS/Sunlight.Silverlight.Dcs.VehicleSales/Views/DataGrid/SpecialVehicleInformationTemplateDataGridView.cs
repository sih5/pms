﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class SpecialVehicleInformationTemplateDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem{
                    Name = "ProductCode"
                   },new ColumnItem{
                    Name = "VIN"
                   },new ColumnItem{
                    Name = "EngineSerialNumber"
                   },new ColumnItem{
                    Name = "RolloutDate"
                   },new ColumnItem{
                    Name = "SON"
                   },new ColumnItem{
                    Name = "VehicleType"
                   }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleInformation);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}
