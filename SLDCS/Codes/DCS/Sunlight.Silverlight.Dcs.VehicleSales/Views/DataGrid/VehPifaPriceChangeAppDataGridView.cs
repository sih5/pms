﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehPifaPriceChangeAppDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "VehiclePriceChangeApp_Status"
        };

        public VehPifaPriceChangeAppDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    },new ColumnItem {
                        Name = "ExecutionDate",
                        TextAlignment = TextAlignment.Right
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "BranchName"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "ApproverName"
                    },new ColumnItem {
                        Name = "ApproveTime"
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehPifaPriceChangeApp);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehPifaPriceChangeApps";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "VehPifaPriceChangeAppDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)(this.GridView.Columns["ExecutionDate"])).DataFormatString = "d";
            ((GridViewDataColumn)(this.GridView.Columns["CreateTime"])).DataFormatString = "d";
            ((GridViewDataColumn)(this.GridView.Columns["ModifyTime"])).DataFormatString = "d";
        }
    }
}
