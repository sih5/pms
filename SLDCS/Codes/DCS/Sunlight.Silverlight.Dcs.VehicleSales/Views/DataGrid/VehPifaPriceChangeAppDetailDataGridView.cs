﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehPifaPriceChangeAppDetailDataGridView : DcsDataGridViewBase {
        public VehPifaPriceChangeAppDetailDataGridView() {
            this.DataContextChanged += VehPifaPriceChangeAppDetailDataGridView_DataContextChanged;
        }

        protected virtual void VehPifaPriceChangeAppDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehPifaPriceChangeApp = e.NewValue as VehPifaPriceChangeApp;
            if(vehPifaPriceChangeApp == null || vehPifaPriceChangeApp.Id == default(int))
                return;
            if(this.FilterItem == null)
                FilterItem = new FilterItem {
                    MemberName = "VehRetailPriceChangeAppId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            FilterItem.Value = vehPifaPriceChangeApp.Id;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(VehPifaPriceChangeAppDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductName",
                    },new ColumnItem {
                        Name = "PriceBeforeChange",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Name = "PriceAfterChange",
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehPifaPriceChangeAppDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["PriceBeforeChange"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PriceAfterChange"]).DataFormatString = "c2";
        }
    }
}
