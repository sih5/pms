﻿using System.Collections.Generic;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehPifaPriceChangeAppDetailForEditDataGridView : VehPifaPriceChangeAppDetailDataGridView {
        //覆盖父类查询方法
        protected override void VehPifaPriceChangeAppDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ProductName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PriceBeforeChange",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "PriceAfterChange",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehPifaPriceChangeApp_AccountDifference,
                        Name = "AccountDifference",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehPifaPriceChangeAppDetails");
        }

        //此处修改变更后价格，批量修改
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.CellValidating += GridView_CellValidating;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.IsReadOnly = false;
            ((GridViewDataColumn)(this.GridView.Columns["PriceBeforeChange"])).DataFormatString = "c2";
            ((GridViewDataColumn)(this.GridView.Columns["PriceAfterChange"])).DataFormatString = "c2";
            ((GridViewDataColumn)(this.GridView.Columns["AccountDifference"])).DataFormatString = "c2";
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var detail = e.Cell.DataContext as PartsShiftOrderDetail;
            if(detail == null)
                return;
            switch(e.Cell.Column.UniqueName) {
                case "PriceAfterChange":
                    e.Cancel = detail.EntityState == EntityState.New;
                    break;
            }
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var vehPifaPriceChangeAppDetail = e.Cell.DataContext as VehPifaPriceChangeAppDetail;
            if(vehPifaPriceChangeAppDetail == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "PriceAfterChange":
                    if(!(e.NewValue is decimal) || (decimal)e.NewValue < 0) {
                        e.IsValid = false;
                        e.ErrorMessage = VehicleSalesUIStrings.DataEditView_Validation_VehPifaPriceChangeAppDetail_PriceAfterChangeIsNotValid;
                    }
                    break;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var vehPifaPriceChangeApp = this.DataContext as VehPifaPriceChangeApp;
            var vehPifaPriceChangeAppDetail = e.Cell.DataContext as VehPifaPriceChangeAppDetail;
            if(vehPifaPriceChangeAppDetail == null || vehPifaPriceChangeApp == null)
                return;
            switch(e.Cell.Column.UniqueName) {
                case "PriceAfterChange":
                    if(this.GridView.SelectedItems != null && this.GridView.SelectedItems.Count > 0) {
                        foreach(var item in this.GridView.SelectedItems) {
                            var detail = item as VehPifaPriceChangeAppDetail;
                            if(detail == null)
                                continue;
                            detail.PriceAfterChange = vehPifaPriceChangeAppDetail.PriceAfterChange;
                            detail.AccountDifference = (decimal)(detail.PriceBeforeChange - detail.PriceAfterChange);
                        }
                    } else {
                        vehPifaPriceChangeAppDetail.AccountDifference = (decimal)(vehPifaPriceChangeAppDetail.PriceBeforeChange - vehPifaPriceChangeAppDetail.PriceAfterChange);
                    }
                    break;
            }
        }

    }
}
