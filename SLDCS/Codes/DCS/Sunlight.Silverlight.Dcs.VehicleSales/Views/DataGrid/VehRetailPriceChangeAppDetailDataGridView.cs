﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehRetailPriceChangeAppDetailDataGridView : DcsDataGridViewBase {
        public VehRetailPriceChangeAppDetailDataGridView() {
            this.DataContextChanged += this.VehRetailPriceChangeAppDetailDataGridView_DataContextChanged;
            this.DataLoaded += this.VehRetailPriceChangeAppDetailDataGridView_DataLoaded;
        }

        private void VehRetailPriceChangeAppDetailDataGridView_DataLoaded(object sender, EventArgs e) {
            var vehRetailPriceChangeApp = this.DataContext as VehRetailPriceChangeApp;
            if(vehRetailPriceChangeApp == null)
                return;
            var vehRetailPriceChangeAppDetails = vehRetailPriceChangeApp.VehRetailPriceChangeAppDetails;
            if(vehRetailPriceChangeAppDetails == null)
                return;
            foreach(var vehRetailPriceChangeAppDetail in vehRetailPriceChangeAppDetails) {
                if(vehRetailPriceChangeAppDetail.PriceBeforeChange.HasValue) {
                    vehRetailPriceChangeAppDetail.Difference = (decimal)(vehRetailPriceChangeAppDetail.PriceBeforeChange - vehRetailPriceChangeAppDetail.PriceAfterChange);
                } else {
                    vehRetailPriceChangeAppDetail.Difference = vehRetailPriceChangeAppDetail.PriceAfterChange;
                }
            }
        }

        private void VehRetailPriceChangeAppDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehRetailPriceChangeApp = e.NewValue as VehRetailPriceChangeApp;
            if(vehRetailPriceChangeApp == null || vehRetailPriceChangeApp.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "VehRetailPriceChangeAppId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = vehRetailPriceChangeApp.Id;
            this.ExecuteQueryDelayed();
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductName"
                    }, new ColumnItem {
                        Name = "PriceBeforeChange",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                       Name = "PriceAfterChange",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Difference",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehRetailPriceChangeAppDetail_Difference
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehRetailPriceChangeAppDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.RowLoaded += GridView_RowLoaded;
            ((GridViewDataColumn)this.GridView.Columns["PriceBeforeChange"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PriceAfterChange"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Difference"]).DataFormatString = "c2";
        }

        //在加载的时候计算，避免在扩展属性返回时计算，编辑界面和查询界面同时用到
        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var vehRetailPriceChangeAppDetail = e.Row.Item as VehRetailPriceChangeAppDetail;
            if(vehRetailPriceChangeAppDetail == null)
                return;

            var priceBeforeChange = vehRetailPriceChangeAppDetail.PriceBeforeChange.HasValue ? vehRetailPriceChangeAppDetail.PriceBeforeChange.Value : 0;
            vehRetailPriceChangeAppDetail.Difference = priceBeforeChange - vehRetailPriceChangeAppDetail.PriceAfterChange;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehRetailPriceChangeAppDetails";
        }
    }
}
