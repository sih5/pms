﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using MaskType = Sunlight.Silverlight.Core.Model.MaskType;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehRetailPriceChangeAppDetailForEditDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ProductName",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PriceBeforeChange",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "PriceAfterChange",
                        MaskType = MaskType.Numeric,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Difference",
                        IsReadOnly = true,
                        MaskType = MaskType.Numeric,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehRetailPriceChangeAppDetail_Difference,
                        TextAlignment = TextAlignment.Right
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehRetailPriceChangeAppDetail);
            }
        }

        //多行选中，目的是可以一次删除多条数据，避免一次只能删除一条数据源
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["PriceBeforeChange"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PriceAfterChange"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["Difference"]).DataFormatString = "c2";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehRetailPriceChangeAppDetails");
        }
    }
}
