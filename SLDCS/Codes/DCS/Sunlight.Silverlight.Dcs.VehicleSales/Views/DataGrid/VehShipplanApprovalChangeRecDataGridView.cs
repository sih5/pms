﻿using System;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehShipplanApprovalChangeRecDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehShipplanApprovalChangeRec_AdjustmentType"
        };

        public VehShipplanApprovalChangeRecDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "OriginalShipplanApprovalCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_OriginalShipplanApprovalCode
                    }, new ColumnItem {
                        Name = "OriginalVIN",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_OriginalVIN
                    }, new ColumnItem {
                        Name = "NewVIN",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_NewVIN
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "AdjustmentType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_AdjustmentType
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehShipplanApprovalChangeRec);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehShipplanApprovalChangeRecs";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }
    }
}
