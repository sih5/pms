﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehShipplanApprovalChangeRecForChangeRecEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehShipplanApprovalChangeRec_AdjustmentType"
        };

        private VehShipplanApprovalChangeRec vehShipplanApprovalChangeRec;

        public VehShipplanApprovalChangeRecForChangeRecEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            if(string.IsNullOrWhiteSpace(this.VehShipplanApprovalChangeRec.OriginalVIN)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehShipplanApprovalChangeRec_VehShipplanApprovalIsNull);
                e.Cancel = true;
                return;
            }
            if(string.IsNullOrWhiteSpace(this.VehShipplanApprovalChangeRec.NewVIN)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehShipplanApprovalChangeRec_VehicleAvailableResourceIsNull);
                e.Cancel = true;
                return;

            }
            e.NewObject = new VehShipplanApprovalChangeRec {
                AdjustmentType = this.VehShipplanApprovalChangeRec.AdjustmentType,
                NewVIN = this.VehShipplanApprovalChangeRec.NewVIN,
                NewShipplanApprovalCode = this.VehShipplanApprovalChangeRec.NewShipplanApprovalCode,
                OriginalVIN = this.VehShipplanApprovalChangeRec.OriginalVIN,
                OriginalShipplanApprovalCode = this.VehShipplanApprovalChangeRec.OriginalShipplanApprovalCode,
                ProductCode = this.VehShipplanApprovalChangeRec.ProductCode,
                ProductCategoryName = this.VehShipplanApprovalChangeRec.ProductCategoryName,
                Status = (int)DcsVehicleBaseDataStatus.有效
            };
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "OriginalVIN",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_OriginalVIN
                    }, new ColumnItem {
                        Name = "NewVIN",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_NewVIN
                    }, new ColumnItem {
                        Name = "ProductCode",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_ProductCode
                    }, new ColumnItem {
                        Name = "ProductCategoryName",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_ProductCategoryName
                    }, new ColumnItem {
                        Name = "OriginalShipplanApprovalCode",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_OriginalShipplanApprovalCode
                    }, new KeyValuesColumnItem {
                        Name = "AdjustmentType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "NewShipplanApprovalCode",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_NewShipplanApprovalCode
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehShipplanApprovalChangeRec);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehShipplanApprovalChangeRecs");
        }

        /// <summary>
        /// 调整单据
        /// </summary>
        public VehShipplanApprovalChangeRec VehShipplanApprovalChangeRec {
            get {
                return this.vehShipplanApprovalChangeRec ?? (this.vehShipplanApprovalChangeRec = new VehShipplanApprovalChangeRec());
            }
        }
    }
}