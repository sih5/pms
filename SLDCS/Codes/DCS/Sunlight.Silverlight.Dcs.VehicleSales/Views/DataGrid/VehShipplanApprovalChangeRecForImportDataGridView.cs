﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehShipplanApprovalChangeRecForImportDataGridView : DcsDataGridViewBase {

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "OriginalVIN",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_OriginalVIN
                    },
                    new ColumnItem {
                        Name = "NewVIN",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehShipplanApprovalChangeRec_TargetVIN
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehShipplanApprovalChangeRec);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}
