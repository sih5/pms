﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleAccountReceivableBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehicleAccountReceivableBill_Type","VehicleAccountReceivableBill_TransferOutType","VehicleAccountReceivableBill_Status"
        };

        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes;

        private ObservableCollection<KeyValuePair> KvVehicleFundsTypes {
            get {
                return this.kvVehicleFundsTypes ?? (this.kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>());
            }
        }

        private void InitializeData() {
            this.KvVehicleFundsTypes.Clear();
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetVehicleFundsTypesQuery().Where(entity => entity.Status != (int)DcsBaseDataStatus.作废), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError) {
                    if(loadOp.IsErrorHandled)
                        loadOp.MarkErrorAsHandled();
                    return;
                }
                var entities = loadOp.Entities;
                if(entities != null) {
                    foreach(var entity in entities) {
                        this.KvVehicleFundsTypes.Add(new KeyValuePair {
                            Key = entity.Id,
                            Value = entity.Name
                        });
                    }
                }
            }, null);
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CustomerCompanyCode"
                    },
                    new ColumnItem {
                        Name = "CustomerCompanyName"
                    },
                    new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, 
                    new KeyValuesColumnItem {
                        Name = "VehicleFundsTypeId",
                        KeyValueItems = this.KvVehicleFundsTypes,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleAccountReceivableBill_VehicleFundsType
                    }, 
                    new ColumnItem {
                        Name = "IssueBank"
                    },
                    new ColumnItem {
                        Name = "IssueCompany"
                    },
                    new ColumnItem {
                        Name = "IssueDate"
                    },
                    new ColumnItem {
                        Name = "IssueCompanyAccount"
                    },
                    new ColumnItem {
                        Name = "MoneyOrderNumber"
                    },
                    new ColumnItem {
                        Name = "PaymentReceptionOperatorrName"
                    },
                    new ColumnItem {
                        Name = "PaymentReceptionDate"
                    },
                    new ColumnItem {
                        Name = "PaymentReceptionBank"
                    },
                    new ColumnItem {
                        Name = "PaymentReceptionBankAccount"
                    },
                    new ColumnItem {
                        Name = "DueDate"
                    },
                    new ColumnItem {
                        Name = "MaturityInterestRate"
                    },
                    new ColumnItem {
                        Name = "MaturityInterest"
                    },
                    new ColumnItem {
                        Name = "DaysOfInterest"
                    },
                    new ColumnItem {
                        Name = "InterestFreePeriod"
                    },
                    new ColumnItem {
                        Name = "NoteDenomination"
                    },
                    new ColumnItem {
                        Name = "InterestRefundAmount"
                    },
                    new ColumnItem {
                        Name = "BillReceptionDate"
                    },
                    new ColumnItem {
                        Name = "TransferOutOperatorrName"
                    },
                    new ColumnItem {
                        Name = "TransferOutTime"
                    },
                    new ColumnItem {
                        Name = "PaymentOperatorName"
                    },
                    new ColumnItem {
                        Name = "PaymentDate"
                    },
                    new ColumnItem {
                        Name = "TransferOutCompany"
                    },
                    new KeyValuesColumnItem {
                        Name = "TransferOutType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    },
                    new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, 
                    new ColumnItem {
                        Name = "Remark"
                    },
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleAccountReceivableBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleAccountReceivableBills";
        }

        public VehicleAccountReceivableBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.InitializeData);
        }
    }
}
