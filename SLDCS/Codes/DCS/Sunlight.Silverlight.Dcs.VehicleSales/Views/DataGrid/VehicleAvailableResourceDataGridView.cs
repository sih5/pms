﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleAvailableResourceDataGridView :DcsDataGridViewBase{
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleAvailableResource_ProductCategoryCode,
                        Name = "ProductCategoryCode"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    },new ColumnItem {
                        Name = "VIN"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleAvailableResource);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleAvailableResources";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
