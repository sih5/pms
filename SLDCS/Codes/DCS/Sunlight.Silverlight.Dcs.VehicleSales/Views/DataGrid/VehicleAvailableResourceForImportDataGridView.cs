﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    /// <summary>
    /// 仅在可用资源管理中，“国产车导入”操作中用作模板
    /// </summary>
    public class VehicleAvailableResourceForImportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode"
                    },new ColumnItem {
                        Name = "VIN"
                    }, new ColumnItem {
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleAvailableResource_EngineSerialNumber,
                        Name = "VehicleInformation.EngineSerialNumber"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleAvailableResource_RolloutDate,
                        Name = "VehicleInformation.RolloutDate"
                    }, new ColumnItem {
                        Name = "VehicleInformation.ProductionPlanSON",
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleInformation_ProductionPlanSON,
                    }, new ColumnItem {
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleInformationForImport_VehicleWarehouseName,
                        Name = "VehicleWarehouseName"
                    },   new ColumnItem {
                        Name = "IsAvailable"
                    }
                };
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(VehicleAvailableResource);
            }
        }
    }
}
