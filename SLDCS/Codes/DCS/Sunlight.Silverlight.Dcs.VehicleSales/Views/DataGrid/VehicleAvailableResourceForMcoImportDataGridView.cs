﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    /// <summary>
    /// 仅在可用资源管理中，“MCO导入”操作中用作模板
    /// </summary>
    public class VehicleAvailableResourceForMcoImportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode"
                    },new ColumnItem {
                        Name = "VIN"
                    }, new ColumnItem {
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleAvailableResource_EngineSerialNumber,
                        Name = "VehicleInformation.EngineSerialNumber"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleAvailableResource_RolloutDate,
                        Name = "VehicleInformation.RolloutDate"
                    }, new ColumnItem {
                        Name = "VehicleInformation.DealerCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleAvailableResource_DealerCode,
                    }
                };
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(VehicleAvailableResource);
            }
        }
    }
}
