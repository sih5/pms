﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleAvailableResourceForVehicleInformationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "VehicleAvailableResource_LockStatus"
        };

        public VehicleAvailableResourceForVehicleInformationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VIN"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleAvailableResource_ProductCategoryCode,
                        Name = "ProductCategoryCode"
                    }, new ColumnItem {
                        Name = "VehicleInformation.ProductionPlanSON",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleInformation_ProductionPlanSON,
                    }, new ColumnItem {
                        Name = "VehicleInformation.EngineSerialNumber"
                    }, new ColumnItem {
                        Name = "VehicleInformation.RolloutDate"
                    }, new KeyValuesColumnItem {
                        Name = "LockStatus",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },   new ColumnItem {
                        Name = "IsAvailable"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleInformation_VehicleWarehouseName,
                        Name = "VehicleWarehouseName"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleInformation_ModifierName,
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleInformation_ModifyTime,
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleAvailableResource_Remaik,
                        Name = "Remaik"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleAvailableResourceWithDetails";
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleAvailableResource);
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var queryParameters = this.FilterItem.ToQueryParameters();
            // 方法参数 parameterName 与 FilterItem.MemberName 的映射关系
            var mappings = new Dictionary<string, string> {
                {"vin", "VIN"},
                {"engineSerialNumber", "VehicleInformation.EngineSerialNumber"},
                {"productCategoryId", "ProductCategoryId"},
                {"isAvailable", "IsAvailable"},
                {"vehicleWarehouseId", "VehicleWarehouseId"},
                {"lockStatus", "LockStatus"},
            };

            // 根据映射关系获取参数值
            Func<string, object> getParameterValue = parameter => {
                object value;
                queryParameters.TryGetValue(mappings[parameter], out value);
                return value;
            };

            // 单独处理日期区间类型的查询条件取值
            object rolloutDateRange;
            DateTime? beginTime = null;
            DateTime? endTime = null;
            if(queryParameters.TryGetValue("VehicleInformation.RolloutDate", out rolloutDateRange)) {
                var range = rolloutDateRange as DateTime?[];
                if(range != null) {
                    beginTime = range[0];
                    endTime = range[1];
                }
            }

            switch(parameterName) {
                case "startRolloutDate":
                    return beginTime;
                case "endRolloutDate":
                    return endTime;
                default:
                    if(mappings.ContainsKey(parameterName))
                        return getParameterValue(parameterName);
                    break;
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var filterItem = new FilterItem();
            return filterItem.ToFilterDescriptor();
        }
    }
}
