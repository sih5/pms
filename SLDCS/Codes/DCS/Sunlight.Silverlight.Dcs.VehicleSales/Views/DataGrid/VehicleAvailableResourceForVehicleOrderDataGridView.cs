﻿
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleAvailableResourceForVehicleOrderDataGridView : DcsDataGridViewBase {

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCategoryCode",
                        Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleAvailableResource_ProductCategoryCode
                    },new ColumnItem {
                        Name = "ProductCategoryName",
                        Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Title_VehicleAvailableResource_ProductCategoryName
                    },new ColumnItem {
                        Name = "ProductCode"
                    },new ColumnItem {
                        Name = "ProductName"
                    },new ColumnItem {
                        Name = "Quantity",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleAvailableResource_Quantity
                    }
                };
            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(VehicleAvailableResource);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询整车可用资源";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            switch(parameterName) {
                case "productCode":
                    var productCodeFilterItem = filter.Filters.SingleOrDefault(r => r.MemberName == "ProductCode");
                    return productCodeFilterItem == null ? null : productCodeFilterItem.Value;
                case "productName":
                    var productNameFilterItem = filter.Filters.SingleOrDefault(r => r.MemberName == "ProductName");
                    return productNameFilterItem == null ? null : productNameFilterItem.Value;
                case "productCategoryCode":
                    var productCategoryCodeFilterItem = filter.Filters.SingleOrDefault(r => r.MemberName == "ProductCategoryCode");
                    return productCategoryCodeFilterItem == null ? null : productCategoryCodeFilterItem.Value;
                case "productCategoryName":
                    var productCategoryNameFilterItem = filter.Filters.SingleOrDefault(r => r.MemberName == "ProductCategoryName");
                    return productCategoryNameFilterItem == null ? null : productCategoryNameFilterItem.Value;
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }
    }
}
