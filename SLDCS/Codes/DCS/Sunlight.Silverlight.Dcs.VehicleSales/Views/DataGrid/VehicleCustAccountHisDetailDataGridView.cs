﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleCustAccountHisDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "VehicleCustAccountHisDetail_BusinessType"
        };
        public VehicleCustAccountHisDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.VehicleCustAccountHisDetailDataGridView_DataContextChanged;
        }

        private void VehicleCustAccountHisDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehicleCustomerAccount = e.NewValue as VehicleCustomerAccount;
            if(vehicleCustomerAccount == null || vehicleCustomerAccount.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "VehicleCustomerAccountId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = vehicleCustomerAccount.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "ChangeAmount",
                       TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                       Name = "ProcessDate"
                    },new ColumnItem {
                       Name = "SourceCode"
                    }, new KeyValuesColumnItem {
                       Name = "BusinessType",
                       KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleCustAccountHisDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleCustAccountHisDetailsByCustomerAccountId";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var vehicleCustomerAccountId = compositeFilterItem.Filters.FirstOrDefault(filter => filter.MemberName == "VehicleCustomerAccountId");
                if(vehicleCustomerAccountId != null) {
                    switch(parameterName) {
                        case "vehicleCustomerAccountId":
                            return vehicleCustomerAccountId.Value;
                    }
                }
            }
            return null;
        }

        protected override Telerik.Windows.Data.IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null)
                foreach(var filterItem in compositeFilterItem.Filters.Where(filter => filter.MemberName != "VehicleCustomerAccountId"))
                    newCompositeFilterItem.Filters.Add(filterItem);
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["ChangeAmount"]).DataFormatString = "c2";
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["ProcessDate"]).DataFormatString = "d";
        }
    }
}
