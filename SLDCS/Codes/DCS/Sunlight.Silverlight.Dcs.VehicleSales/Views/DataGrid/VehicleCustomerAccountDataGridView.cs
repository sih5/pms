﻿using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleCustomerAccountDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "MasterData_Status"
        };

        public VehicleCustomerAccountDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Company_Code,
                        Name = "Company.Code"
                    }, new ColumnItem {
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Company_Name,
                        Name = "Company.Name"
                    }, new ColumnItem {
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleCustomerAccount_AccountBalance,
                        Name = "AccountBalance",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "CustomerCredenceAmount",
                        TextAlignment = TextAlignment.Right,
                    },  new ColumnItem {
                        Name = "PendingAmount",
                        TextAlignment = TextAlignment.Right,
                    },  new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "VehicleFundsType.Name",
                        Title=VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleCustomerAccount_VehicleFundsTypeName,
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleCustomerAccountsWithCustomerCompany";
        }

        protected override System.Type EntityType {
            get {
                return typeof(VehicleCustomerAccount);
            }
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                      "VehicleCustAccountHisDetail"  
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["AccountBalance"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CustomerCredenceAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["PendingAmount"]).DataFormatString = "c2";
        }
    }
}
