﻿using System;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleCustomerInformationForSelectDataGridView : DcsDataGridViewBase {

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CustomerCompany.Code",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleCustomerInformation_CustomerCompany_Code
                    },
                    new ColumnItem {
                        Name = "CustomerCompany.Name",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleCustomerInformation_CustomerCompany_Name
                    },
                    new ColumnItem {
                        Name = "CustomerCompany.ContactAddress",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleCustomerInformation_CustomerCompany_ContactAddress
                    },
                    new ColumnItem {
                        Name = "CustomerCompany.ContactPhone",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleCustomerInformation_CustomerCompany_ContactPhone
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleCustomerInformation);
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "companyCode":
                        return filters.Filters.Single(item => item.MemberName == "CustomerCompany.Code").Value;
                    case "companyName":
                        return filters.Filters.Single(item => item.MemberName == "CustomerCompany.Name").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "查询整车客户信息";
        }
    }
}
