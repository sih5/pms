﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleDLRAccountFreezeBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "VehicleDLRAccountFreezeBill_Status","VehicleCustomerAccount_Availability"
        };

        public VehicleDLRAccountFreezeBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleDLRAccountFreezeBill_CustomerCompanyCode,
                        Name = "CustomerCompanyCode"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleDLRAccountFreezeBill_CustomerCompanyName,
                        Name = "CustomerCompanyName"
                    }, new ColumnItem {
                        Name = "Code"
                    }, new KeyValuesColumnItem {
                        Name = "AccountAvailability",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleFundsType_Name,
                        Name = "VehicleFundsType.Name"
                    },  new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Remark"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "ModifierName"
                    },new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ApproverName,
                        Name = "ApproverName"
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ApproveTime,
                        Name = "ApproveTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleDLRAccountFreezeBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleDLRAccountFreezeBillsWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ApproveTime"]).DataFormatString = "d";
        }
    }
}
