﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleDealerCreditLimitAppDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehicleDealerCreditLimitApp_Status"
        };

        public VehicleDealerCreditLimitAppDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "CustomerCompanyCode"
                    }, new ColumnItem {
                        Name = "CustomerCompanyName"
                    }, new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "VehicleFundsType.Name",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleDealerCreditLimitApp_VehicleFundsType
                    }, new ColumnItem {
                        Name = "CredenceLimit",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleDealerCreditLimitApp_CredenceLimit,
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ExpireDate",
                        TextAlignment=TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "Remark"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        TextAlignment=TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ApproverName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleDealerCreditLimitApp_ApproverName
                    }, new ColumnItem {
                        Name = "ApproveTime",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleDealerCreditLimitApp_ApproveTime,
                        TextAlignment=TextAlignment.Right
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleDealerCreditLimitAppsWithVehicleFundsType";
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleDealerCreditLimitApp);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["ExpireDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CredenceLimit"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ApproveTime"]).DataFormatString = "d";
        }
    }
}
