﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleForShippingPlanApproveForSelectDataGridView : DcsDataGridViewBase {
        private DcsDomainContext domainContext;

        private DcsDomainContext DcsDomainContext {
            get {
                return this.domainContext ?? (this.domainContext = this.DomainContext as DcsDomainContext);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleForShippingPlanApprove_Code,
                        Name = "Code",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleForShippingPlanApprove_DealerName,
                        Name = "DealerName",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleForShippingPlanApprove_Son,
                        Name = "Son",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleForShippingPlanApprove_Vin,
                        Name = "Vin",
                       IsReadOnly = true
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleForShippingPlanApprove_ProductCategoryCode,
                        Name = "ProductCategoryCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleForShippingPlanApprove_ProductCode,
                        Name = "ProductCode",
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleForShippingPlanApprove_ShippingDate,
                        Name = "ShippingDate",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleForShippingPlanApprove_Price,
                        Name = "Price",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleForShippingPlanApprove_EstimatedShippingDate,
                        Name = "EstimatedShippingDate",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleForShippingPlanApprove_VehicleWarehouseName,
                        Name = "VehicleWarehouseName",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleForShippingPlanApprove);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询预分车关联单以及相关信息";
        }

        //此处不整行选中，目的是便于通过修改其中一跳数据的实体属性达到批量修改的目的
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionChanged += GridView_SelectionChanged;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.CellValidating += GridView_CellValidating;
            ((GridViewDataColumn)this.GridView.Columns["ShippingDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["EstimatedShippingDate"]).DataFormatString = "d";
            this.GridView.RowLoaded += GridView_RowLoaded;
        }

        //此事件根据业务要求是只有相同的经销商才能执行此事件里面的方法
        private void GridView_SelectionChanged(object sender, SelectionChangeEventArgs e) {
            var dataEditView = this.DataContext as VehicleShipplanApprovalForSecondDataEditView;
            if(dataEditView == null)
                return;
            if(GridView.Items == null || GridView.Items.Count < 1)
                return;
            var vehicleShipplanApproval = dataEditView.DataContext as VehicleShipplanApproval;
            var customVehicleShipplanApproval = dataEditView.CustomVehicleShipplanApproval;
            if(vehicleShipplanApproval == null)
                return;
            var selectedItems = this.GridView.SelectedItems;
            //当没有任何选择的数据时，此时需要清除数据禁用状态，清楚橘黄色颜色背景
            if(selectedItems == null || !selectedItems.Any()) {
                foreach(VehicleForShippingPlanApprove vehicleForShip in this.GridView.Items.Cast<VehicleForShippingPlanApprove>()) {
                    GridViewRow gridViewRow = this.GridView.GetRowForItem(vehicleForShip);
                    if(gridViewRow != null) {
                        gridViewRow.Background = new SolidColorBrush(Colors.White);
                    }
                }
                return;
            }

            //if(this.GridView.CurrentItem != null) {
            //    //this.GridView.SelectedItems.Cast<VehicleForShippingPlanApprove>().Where(r => r.Son != ((VehicleForShippingPlanApprove)this.GridView.CurrentItem).Son);
            //    var oldSelectItems = this.GridView.SelectedItems.Cast<VehicleForShippingPlanApprove>().Where(r => r.Son == ((VehicleForShippingPlanApprove)this.GridView.CurrentItem).Son);

            //    if(oldSelectItems == null)
            //        return;

            //    var enumerable = oldSelectItems as VehicleForShippingPlanApprove[] ?? oldSelectItems.ToArray();
            //    foreach(var sourceItem in enumerable) {
            //        this.GridView.SelectedItems.Remove(sourceItem);
            //    }
            //}

            //只有第一次给经销商赋值，并且资金类型已经填充时查询
            if(selectedItems.Count == 1) {
                vehicleShipplanApproval.DealerId = selectedItems.Cast<VehicleForShippingPlanApprove>().First().DealerId;
                vehicleShipplanApproval.DealerCode = selectedItems.Cast<VehicleForShippingPlanApprove>().First().DealerCode;
                vehicleShipplanApproval.DealerName = selectedItems.Cast<VehicleForShippingPlanApprove>().First().DealerName;
                vehicleShipplanApproval.SourceId = selectedItems.Cast<VehicleForShippingPlanApprove>().First().Id;
                vehicleShipplanApproval.SourceCode = selectedItems.Cast<VehicleForShippingPlanApprove>().First().Code;
                if(vehicleShipplanApproval.DealerId != default(int) && customVehicleShipplanApproval.VehicleOrderType != default(int))
                    this.DcsDomainContext.Load(this.DcsDomainContext.查询可用余额Query(vehicleShipplanApproval.DealerId, customVehicleShipplanApproval.VehicleFundsType), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            loadOp.MarkErrorAsHandled();
                            return;
                        }
                        var vehicleCustomerAccount = loadOp.Entities.FirstOrDefault();
                        vehicleShipplanApproval.VehicleCustomerAccount = vehicleCustomerAccount;
                        if(vehicleCustomerAccount == null) {
                            customVehicleShipplanApproval.AvailableAmount = default(int);
                            return;
                        }

                        customVehicleShipplanApproval.AvailableAmount = vehicleCustomerAccount.CustomerCredenceAmount + vehicleCustomerAccount.AccountBalance - (vehicleCustomerAccount.PendingAmount.HasValue ? vehicleCustomerAccount.PendingAmount.Value : 0);
                    }, null);
            }

            var vehicleForShippingPlanApprove = this.GridView.SelectedItems.Cast<VehicleForShippingPlanApprove>();
            var forShippingPlanApproves = vehicleForShippingPlanApprove as VehicleForShippingPlanApprove[] ?? vehicleForShippingPlanApprove.ToArray();
            customVehicleShipplanApproval.ApprovepAmount = forShippingPlanApproves.Sum(r => r.Price);//汇总清单价格合计

            //此段代码会用到，当主单的经销商是根据数据表格填充时，做颜色切换，和行禁用启用，尚不实现
            foreach(VehicleForShippingPlanApprove vehicleForShip in this.GridView.Items.Cast<VehicleForShippingPlanApprove>()) {
                GridViewRow gridViewRow = this.GridView.GetRowForItem(vehicleForShip);
                if(gridViewRow != null) {
                    gridViewRow.Background = vehicleForShip.Id == vehicleShipplanApproval.SourceId ? new SolidColorBrush(Colors.Yellow) : new SolidColorBrush(Colors.Gray);
                }
            }
        }

        private void GridView_RowLoaded(object sender, RowLoadedEventArgs e) {
            var dataEditView = this.DataContext as VehicleShipplanApprovalForSecondDataEditView;
            if(dataEditView == null)
                return;
            if(this.GridView.SelectedItems == null || this.GridView.SelectedItems.Count < 1) {
                foreach(VehicleForShippingPlanApprove vehicleForShip in this.GridView.Items.Cast<VehicleForShippingPlanApprove>()) {
                    GridViewRow gridViewRow = this.GridView.GetRowForItem(vehicleForShip);
                    if(gridViewRow != null) {
                        gridViewRow.Background = new SolidColorBrush(Colors.White);
                    }
                }
                return;
            }
            if(GridView.Items == null || GridView.Items.Count < 1)
                return;
            var vehicleShipplanApproval = dataEditView.DataContext as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null)
                return;

            foreach(VehicleForShippingPlanApprove vehicleForShip in this.GridView.Items.Cast<VehicleForShippingPlanApprove>()) {
                GridViewRow gridViewRow = this.GridView.GetRowForItem(vehicleForShip);
                if(gridViewRow != null) {
                    gridViewRow.Background = vehicleForShip.Id == vehicleShipplanApproval.SourceId ? new SolidColorBrush(Colors.Yellow) : new SolidColorBrush(Colors.Gray);
                }
            }
        }
        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var vehicleForShippingPlanApprove = e.Cell.DataContext as VehicleForShippingPlanApprove;
            if(vehicleForShippingPlanApprove == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "Vin":
                    var vin = e.NewValue as string;
                    if(vin == null || string.IsNullOrWhiteSpace(vin)) {
                        e.IsValid = false;
                        e.ErrorMessage = VehicleSalesUIStrings.DataGridView_Validation_VehicleForShippingPlanApprove_VINIsNull;
                    }
                    break;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(!e.Cell.Column.UniqueName.Equals("ShippingDate"))
                return;
            var detail = e.Cell.DataContext as VehicleForShippingPlanApprove;
            if(detail == null)
                return;
            foreach(var item in this.GridView.SelectedItems.Cast<VehicleForShippingPlanApprove>())
                item.ShippingDate = detail.ShippingDate;
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleForShippingPlanApproves");
        }
    }
}
