﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleFundsTypeDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "BaseData_Status"
        };

        protected override Type EntityType {
            get {
                return typeof(VehicleFundsType);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                        new ColumnItem {
                            Name = "Code"
                        },new ColumnItem {
                            Name = "Name"
                        },new KeyValuesColumnItem {
                            Name = "Status",
                            KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                        },new ColumnItem {
                            Name = "CreatorName"
                        },new ColumnItem {
                            Name = "CreateTime",
                            TextAlignment = TextAlignment.Right
                        },new ColumnItem {
                            Name = "ModifierName"
                        },new ColumnItem {
                            Name = "ModifyTime",
                            TextAlignment = TextAlignment.Right
                        },new ColumnItem {
                            Name = "ApproverName"
                        },new ColumnItem {
                            Name = "ApproveTime",
                            TextAlignment = TextAlignment.Right
                        },new ColumnItem {
                            Name = "Remark"
                        }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleFundsTypes";
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        public VehicleFundsTypeDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ApproveTime"]).DataFormatString = "d";
        }
    }
}
