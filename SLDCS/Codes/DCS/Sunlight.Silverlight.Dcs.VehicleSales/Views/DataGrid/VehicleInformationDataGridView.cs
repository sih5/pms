﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleInformationDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Vehicle_Status","VehicleInformation_VehicleType"
        };
        public VehicleInformationDataGridView() {
            this.KeyValueManager.Register(this.kvNames);

        }
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VIN"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductName",
                    }, new ColumnItem {
                        Name = "ProductCategoryCode",
                    }, new ColumnItem {
                        Name = "ProductCategoryName",
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    }, new ColumnItem {
                        Name = "VehicleWarehouseName",
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleAvailableResource_VehicleWarehouseNameForPosition,
                    },new KeyValuesColumnItem {
                        Name = "VehicleType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                    }, new ColumnItem {
                        Name = "ResponsibleUnitName",
                    }, new ColumnItem {
                        Name = "SalesDealerCode",
                    }, new ColumnItem {
                        Name = "SalesDealerName",
                    }, new ColumnItem {
                        Name = "EngineSerialNumber",
                    }, new ColumnItem {
                        Name = "RolloutDate",
                    }, new ColumnItem {
                        Name = "FabricationDate",
                    }, new ColumnItem {
                        Name = "SalesDate"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetVehicleInfomationWithResouce";
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                      "VehicleInformationForSales" ,"VINChangeRecord" 
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleInformation);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.KeyValueManager.Register(this.kvNames);
            ((GridViewDataColumn)this.GridView.Columns["FabricationDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["SalesDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["RolloutDate"]).DataFormatString = "d";
        }
    }
}