﻿using System;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleInformationForSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "VehicleInformation_VehicleType"
        };

        public VehicleInformationForSelectDataGridView() {
            this.KeyValueManager.Register(kvNames);
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VIN"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new KeyValuesColumnItem {
                        Name = "VehicleType",
                        KeyValueItems = this.KeyValueManager[kvNames[0]]
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleInformation);
            }
        }

        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "vIN":
                        return filters.Filters.Single(item => item.MemberName == "VIN").Value;
                    case "vehicleCategoryId":
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }


        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                if(this.FilterItem.MemberName != "vIN" && this.FilterItem.MemberName != "vehicleCategoryId")
                    newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "vIN" && filter.MemberName != "vehicleCategoryId"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }


        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "查询特殊车辆信息";
        }
    }
}
