﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleModelAffiProductDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductName"
                    },new ColumnItem { 
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_ProductCategoryCode,
                        Name = "ProductCategoryCode"
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_ProductCategoryName,
                        Name = "ProductCategoryName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleModelAffiProduct);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleModelAffiProducts";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
