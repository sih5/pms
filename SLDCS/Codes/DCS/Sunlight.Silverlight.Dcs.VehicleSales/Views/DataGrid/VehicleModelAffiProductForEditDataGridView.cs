﻿using System;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleModelAffiProductForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] KvNames = new[] {
            "Prodcut_Color"
        };

        public VehicleModelAffiProductForEditDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_ProductCategoryCode,
                        Name = "ProductCategoryCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ProductCode",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_WholesalePrice,
                        Name = "WholesalePrice",
                        IsReadOnly = true
                    }
                    , new KeyValuesColumnItem {
                        Name = "Color",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanDetail_Color,
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_CanBeOrdered,
                        Name = "CanBeOrdered",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_IfPurchasable,
                        Name = "IfPurchasable",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_OrderCycle,
                        Name = "OrderCycle",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_PurchaseCycle,
                        Name = "PurchaseCycle",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_IfOption,
                        Name = "IfOption",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_ParameterDescription,
                        Name = "ParameterDescription",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                         Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_Version,
                      Name  = "Version",
                      IsReadOnly = true
                    },
                    new ColumnItem {
                         Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_Configuration,
                        Name = "Configuration",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_EngineCylinder,
                        Name = "EngineCylinder",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                         Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_EmissionStandard,
                        Name = "EmissionStandard",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                         Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_ManualAutomatic,
                        Name = "ManualAutomatic",
                        IsReadOnly = true
                    },
                    new ColumnItem {
                         Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleModelAffiProduct_ColorCode,
                        Name = "ColorCode",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleModelAffiProduct);
            }
        }

        protected override void OnControlsCreated() {
            var a = new VehicleModelAffiProduct();
            var colorCode = a.ColorCode;
            var manualAutomatic = a.ManualAutomatic;
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CanUserInsertRows = true;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.ShowGroupPanel = false;
            ((GridViewDataColumn)this.GridView.Columns["WholesalePrice"]).DataFormatString = "c2";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleModelAffiProducts");
        }
    }
}
