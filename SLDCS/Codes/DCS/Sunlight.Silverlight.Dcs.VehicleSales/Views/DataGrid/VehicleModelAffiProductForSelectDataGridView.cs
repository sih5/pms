﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleModelAffiProductForSelectDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem { 
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehModelRetailSuggestedPrice_ProductCategoryCode,
                        Name = "ProductCategoryCode"
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehModelRetailSuggestedPrice_ProductCategoryName,
                        Name = "ProductCategoryName"
                    },new ColumnItem {
                        Name = "Price",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehModelRetailSuggestedPrice_GuidePrice,
                    }
                };
            }
        }
        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            switch(parameterName) {
                case "productCategoryCode":
                    var productCategoryCode = filter.Filters.SingleOrDefault(e => e.MemberName == "productCategoryCode");
                    return productCategoryCode != null ? productCategoryCode.Value : null;
                case "productCategoryName":
                    var productCategoryName = filter.Filters.SingleOrDefault(e => e.MemberName == "productCategoryName");
                    return productCategoryName != null ? productCategoryName.Value : null;
            }
            return null;
        }

        protected override Type EntityType {
            get {
                return typeof(VehModelRetailSuggestedPrice);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询销售车辆产品以及价格";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}