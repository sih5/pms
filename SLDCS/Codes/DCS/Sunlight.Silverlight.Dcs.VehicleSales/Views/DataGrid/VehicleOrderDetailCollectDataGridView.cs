﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderDetailCollectDataGridView : DcsDataGridViewBase {

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCategoryCode",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetailCollect_ProductCategoryCode
                    }, new ColumnItem {
                        Name = "ProductCode",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetailCollect_ProductCode
                    }, new ColumnItem {
                        Name = "Quantity",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetailCollect_Quantity
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = false;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleOrderDetailCollect);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleOrderDetailCollects");
        }
    }
}
