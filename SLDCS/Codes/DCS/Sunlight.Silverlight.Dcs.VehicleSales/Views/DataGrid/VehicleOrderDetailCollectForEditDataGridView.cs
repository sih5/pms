﻿
using System.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderDetailCollectForEditDataGridView : VehicleOrderDetailCollectDataGridView {
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.Columns["Quantity"].IsReadOnly = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CanUserInsertRows = false;
            this.GridView.CanUserDeleteRows = true;
        }
    }
}
