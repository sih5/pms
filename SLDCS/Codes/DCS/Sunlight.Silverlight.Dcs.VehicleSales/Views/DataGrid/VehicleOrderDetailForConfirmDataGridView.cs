﻿using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderDetailForConfirmDataGridView : VehicleOrderDetailDataGridView {
        private ObservableCollection<KeyValuePair> keyValuePairs;

        private ObservableCollection<KeyValuePair> KeyValuePairs {
            get {
                return this.keyValuePairs ?? (this.keyValuePairs = new ObservableCollection<KeyValuePair>(this.KeyValueManager[this.kvNames[0]]));
            }
        }

        private ObservableCollection<VehicleOrderDetail> oldVehicleOrderDetails;

        private ObservableCollection<VehicleOrderDetail> OldVehicleOrderDetails {
            get {
                return this.oldVehicleOrderDetails ?? (this.oldVehicleOrderDetails = new ObservableCollection<VehicleOrderDetail>());
            }
        }

        private void FilterKeyValuePairs() {
            this.KeyValuePairs.Clear();
            foreach(var keyValuePair in this.KeyValueManager[this.kvNames[0]])
                this.KeyValuePairs.Add(keyValuePair);
        }

        public VehicleOrderDetailForConfirmDataGridView() {
            this.KeyValueManager.LoadData(this.FilterKeyValuePairs);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.CanUserDeleteRows = false;
            this.DataContextChanged += this.VehicleOrderDetailForConfirmDataGridView_DataContextChanged;
            this.DataLoaded += this.VehicleOrderDetailForConfirmDataGridView_DataLoaded;
            this.GridView.IsReadOnly = false;
            this.GridView.Columns["SON"].IsReadOnly = true;
            this.GridView.Columns["ProductCategoryCode"].IsReadOnly = true;
            this.GridView.Columns["ProductCode"].IsReadOnly = true;
            this.GridView.Columns["ExpectedShippingDate"].IsReadOnly = true;
            this.GridView.Columns["EstimatedShippingDate"].IsReadOnly = false;
            this.GridView.BeginningEdit += GridView_BeginningEdit;
            this.GridView.Columns["Status"].IsReadOnly = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            ((GridViewComboBoxColumn)(this.GridView.Columns["Status"])).ItemsSource = this.KeyValuePairs;
            this.GridView.PreparingCellForEdit += GridView_PreparingCellForEdit;

        }

        public void GridView_PreparingCellForEdit(object sender, GridViewPreparingCellForEditEventArgs e) {
            var comboBox = e.EditingElement as RadComboBox;
            if(comboBox == null)
                return;
            var sons = (this.OldVehicleOrderDetails.Where(r => r.Status == (int)DcsVehicleOrderDetailStatus.已确认)).Select(r => r.SON);
            if(sons.Any()) {
                switch(e.Column.UniqueName) {
                    case "Status":
                        comboBox.ItemsSource = this.keyValuePairs.Where(r => r.Key != (int)DcsVehicleOrderDetailStatus.新建 && r.Key != (int)DcsVehicleOrderDetailStatus.完成).ToArray();
                        break;
                }
            } else {
                switch(e.Column.UniqueName) {
                    case "Status":
                        comboBox.ItemsSource = this.keyValuePairs.Where(r => r.Key != (int)DcsVehicleOrderDetailStatus.完成).ToArray();
                        break;
                }
            }
        }

        private void VehicleOrderDetailForConfirmDataGridView_DataLoaded(object sender, System.EventArgs e) {
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            if(vehicleOrder.VehicleOrderDetails == null)
                return;
            this.OldVehicleOrderDetails.Clear();
            foreach(var detail in vehicleOrder.VehicleOrderDetails) {
                this.OldVehicleOrderDetails.Add(new VehicleOrderDetail {
                    SON = detail.SON,
                    Status = detail.Status
                });
            }
        }

        private void VehicleOrderDetailForConfirmDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            if(vehicleOrder.VehicleOrderDetails == null)
                return;
            this.OldVehicleOrderDetails.Clear();
            foreach(var detail in vehicleOrder.VehicleOrderDetails) {
                this.OldVehicleOrderDetails.Add(new VehicleOrderDetail {
                    SON = detail.SON,
                    Status = detail.Status
                });
            }
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var vehicleOrderDetail = e.Row.DataContext as VehicleOrderDetail;
            if(vehicleOrderDetail == null)
                return;
            var sons = (this.OldVehicleOrderDetails.Where(r => r.Status == (int)DcsVehicleOrderDetailStatus.拒绝)).Select(r => r.SON);
            switch(vehicleOrderDetail.Status) {
                case (int)DcsVehicleOrderDetailStatus.新建:
                    e.Cancel = false;
                    break;
                case (int)DcsVehicleOrderDetailStatus.拒绝:
                    e.Cancel = sons.Contains(vehicleOrderDetail.SON);
                    break;
                case (int)DcsVehicleOrderDetailStatus.已确认:
                    e.Cancel = false;
                    break;
                default:
                    e.Cancel = true;
                    break;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        private void SetVehicleOrderStatus(VehicleOrderDetail detail) {
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            var sons = (this.OldVehicleOrderDetails.Where(r => r.Status == (int)DcsVehicleOrderDetailStatus.拒绝 || r.Status == (int)DcsVehicleOrderDetailStatus.完成)).Select(r => r.SON).ToArray();
            //此处不能修改为，如果条件不满足，就返回，后面的If是在此处执行完成后才执行
            if(this.GridView.SelectedItems != null && this.GridView.SelectedItems.Any() && this.GridView.SelectedItems.Contains(detail))
                foreach(var selectiem in this.GridView.SelectedItems.Cast<VehicleOrderDetail>()) {
                    if(sons.Contains(selectiem.SON))
                        continue;
                    selectiem.Status = detail.Status;
                }
            if(vehicleOrder.VehicleOrderDetails.All(r => r.Status == (int)DcsVehicleOrderDetailStatus.新建))
                vehicleOrder.Status = (int)DcsVehicleOrderStatus.提交;
            else if(vehicleOrder.VehicleOrderDetails.All(r => r.Status == (int)DcsVehicleOrderDetailStatus.拒绝))
                vehicleOrder.Status = (int)DcsVehicleOrderStatus.拒绝;
            else if(vehicleOrder.VehicleOrderDetails.Any(r => r.Status == (int)DcsVehicleOrderDetailStatus.完成))
                vehicleOrder.Status = (int)DcsVehicleOrderStatus.部分审批;
            else {
                if(vehicleOrder.VehicleOrderDetails.Any(r => r.Status == (int)DcsVehicleOrderDetailStatus.新建))
                    vehicleOrder.Status = (int)DcsVehicleOrderStatus.部分确认;
                else
                    vehicleOrder.Status = (int)DcsVehicleOrderStatus.已确认;
            }
        }

        private void SetEstimatedShippingDateBatch(VehicleOrderDetail detail) {
            if(this.GridView.SelectedItems == null)
                return;
            var sons = (this.OldVehicleOrderDetails.Where(r => r.Status == (int)DcsVehicleOrderDetailStatus.完成 || r.Status == (int)DcsVehicleOrderDetailStatus.已确认 || r.Status == (int)DcsVehicleOrderDetailStatus.拒绝)).Select(r => r.SON).ToArray();
            var selectedDetails = this.GridView.SelectedItems.Cast<VehicleOrderDetail>().ToArray();
            foreach(var vehicleOrderDetail in selectedDetails) {
                if(sons.Contains(vehicleOrderDetail.SON))
                    continue;
                vehicleOrderDetail.EstimatedShippingDate = detail.EstimatedShippingDate;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var detail = e.Cell.DataContext as VehicleOrderDetail;
            if(detail == null)
                return;
            switch(e.Cell.Column.UniqueName) {
                case "EstimatedShippingDate":
                    this.SetEstimatedShippingDateBatch(detail);
                    break;
                case "Status":
                    this.SetVehicleOrderStatus(detail);
                    break;
            }
        }
    }
}