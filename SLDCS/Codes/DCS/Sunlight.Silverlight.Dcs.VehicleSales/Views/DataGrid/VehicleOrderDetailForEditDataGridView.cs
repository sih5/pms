﻿using System;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehicleOrderDetail_Status"
        };

        public VehicleOrderDetailForEditDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SON",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetail_SON
                    }, new ColumnItem {
                        Name = "ProductCategoryCode",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetail_ProductCategory
                    }, new ColumnItem {
                        Name = "ProductCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ExpectedShippingDate",
                        MaskType = MaskType.DateTime,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetail_ExpectedShippingDate
                    }, new ColumnItem {
                        Name = "EstimatedShippingDate",
                        MaskType = MaskType.DateTime,
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetail_EstimatedShippingDate
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "Price",
                        MaskType = MaskType.Numeric,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.CanUserDeleteRows = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.Deleted += GridView_Deleted;
        }

        private void GridView_Deleted(object sender, Telerik.Windows.Controls.GridViewDeletedEventArgs e) {
            var vehicleOrder = this.DataContext as VehicleOrder;
            if(vehicleOrder == null)
                return;
            foreach(var vehicleOrderDetailCollect in vehicleOrder.VehicleOrderDetailCollects) {
                vehicleOrderDetailCollect.Quantity = vehicleOrder.VehicleOrderDetails.Any(r => r.ProductId == vehicleOrderDetailCollect.ProductId && r.ProductCategoryCode == vehicleOrderDetailCollect.ProductCategoryCode) ? vehicleOrder.VehicleOrderDetails.Where(r => r.ProductId == vehicleOrderDetailCollect.ProductId && r.ProductCategoryCode == vehicleOrderDetailCollect.ProductCategoryCode).ToArray().Count() : 0;
            }
        }

        private void GridView_CellEditEnded(object sender, Telerik.Windows.Controls.GridViewCellEditEndedEventArgs e) {
            var detail = e.Cell.DataContext as VehicleOrderDetail;
            if(detail == null)
                return;
            switch(e.Cell.Column.UniqueName) {
                case "ExpectedShippingDate":
                    this.SetExpectedShippingDateBatch(detail);
                    break;
            }
        }

        private void SetExpectedShippingDateBatch(VehicleOrderDetail detail) {
            if(this.GridView.SelectedItems == null)
                return;
            var selectedDetails = this.GridView.SelectedItems.Cast<VehicleOrderDetail>().ToArray();
            foreach(var vehicleOrderDetail in selectedDetails)
                vehicleOrderDetail.ExpectedShippingDate = detail.ExpectedShippingDate;
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleOrderDetails");
        }
    }
}
