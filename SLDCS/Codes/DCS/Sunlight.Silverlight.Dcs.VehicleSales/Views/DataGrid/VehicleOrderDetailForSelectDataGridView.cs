﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;
using FilterOperator = Sunlight.Silverlight.Core.Model.FilterOperator;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    /// <summary>
    /// 发车审批管理节点用到此组件，用途是选择多条进行发运审批单清单的转换
    /// </summary>
    public class VehicleOrderDetailForSelectDataGridView : DcsDataGridViewBase {
        private QueryWindowBase vehicleAvailableResourceDropDownQueryWindow;

        private QueryWindowBase VehicleAvailableResourceDropDownQueryWindow {
            get {
                if(this.vehicleAvailableResourceDropDownQueryWindow == null) {
                    this.vehicleAvailableResourceDropDownQueryWindow = DI.GetQueryWindow("VehicleAvailableResourceDropDown");
                    this.vehicleAvailableResourceDropDownQueryWindow.SelectionDecided += vehicleAvailableResourceDropDownQueryWindow_SelectionDecided;
                    this.vehicleAvailableResourceDropDownQueryWindow.Loaded += vehicleAvailableResourceDropDownQueryWindow_Loaded;
                }
                return this.vehicleAvailableResourceDropDownQueryWindow;
            }
        }

        private void vehicleAvailableResourceDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var vehicleShipplanApproval = this.DataContext as VehicleShipplanApproval;
            var queryWindow = sender as QueryWindowBase;
            if(vehicleShipplanApproval == null || queryWindow==null)
                return;
            var vehicleOrderDetail=queryWindow.DataContext as VehicleOrderDetail;
            if(vehicleOrderDetail==null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("VehicleWarehouseId", typeof(int), FilterOperator.IsEqualTo, vehicleShipplanApproval.VehicleWarehouseId));
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("ProductCode", typeof(string), FilterOperator.IsEqualTo, vehicleOrderDetail.ProductCode));
            queryWindow.ExchangeData(null, "RefreshQueryResult", null);
        }

        private void vehicleAvailableResourceDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var dropDownQueryWindow = sender as DcsDropDownQueryWindowBase;
            if(dropDownQueryWindow == null)
                return;
            var vehicleAvailableResource = dropDownQueryWindow.SelectedEntities.Cast<VehicleAvailableResource>().FirstOrDefault();
            if(vehicleAvailableResource == null)
                return;
            var vehicleOrderDetail = dropDownQueryWindow.DataContext as VehicleOrderDetail;
            if(vehicleOrderDetail == null)
                return;
            vehicleOrderDetail.VIN = vehicleAvailableResource.VIN;
            vehicleOrderDetail.VehicleId = vehicleAvailableResource.VehicleId;
            vehicleOrderDetail.VehicleWarehouseId = vehicleAvailableResource.VehicleWarehouseId;
            vehicleOrderDetail.VehicleWarehouseName = vehicleAvailableResource.VehicleWarehouseName;

            var parent = dropDownQueryWindow.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SON",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetail_SON
                    },new DropDownTextBoxColumnItem {
                        Name = "VIN",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetail_VIN,
                        DropDownContent = this.VehicleAvailableResourceDropDownQueryWindow
                    }, new ColumnItem {
                        Name = "ProductCategoryCode",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetail_ProductCategory
                    }, new ColumnItem {
                        Name = "ProductCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ShippingDate",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetail_ShippingDate
                    }, new ColumnItem {
                        Name = "Price",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "EstimatedShippingDate",
                        IsReadOnly = true
                    },new ColumnItem {
                        IsReadOnly=true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetail_VehicleWarehouseName,
                        Name = "VehicleWarehouseName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleOrderDetail);
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "vehicleWarehouseId":
                        return filters.Filters.Single(item => item.MemberName == "VehicleWarehouseId").Value;
                    case "vehicleOrderCode":
                        return filters.Filters.Single(item => item.MemberName == "VehicleOrder.Code").Value;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "VehicleWarehouseId" || filter.MemberName == "VehicleOrder.Code"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "查询订单信息及可用资源信息";
        }

        //此处不整行选中，目的是便于通过修改其中一跳数据的实体属性达到批量修改的目的
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.CanUserDeleteRows = false;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
            this.GridView.CellValidating += GridView_CellValidating;
            ((GridViewDataColumn)this.GridView.Columns["ShippingDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["EstimatedShippingDate"]).DataFormatString = "d";
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var vehicleOrderDetail = e.Cell.DataContext as VehicleOrderDetail;
            if(vehicleOrderDetail == null)
                return;

            switch(e.Cell.DataColumn.DataMemberBinding.Path.Path) {
                case "VIN":
                    var vin = e.NewValue as string;
                    if(vin == null || string.IsNullOrWhiteSpace(vin)) {
                        e.IsValid = false;
                        e.ErrorMessage = VehicleSalesUIStrings.DataGridView_Validation_VehicleOrderDetail_VINIsNull;
                    }
                    break;
            }
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            if(!e.Cell.Column.UniqueName.Equals("ShippingDate"))
                return;
            var detail = e.Cell.DataContext as VehicleOrderDetail;
            if(detail == null)
                return;
            foreach(var item in this.GridView.Items.Cast<VehicleOrderDetail>())
                item.ShippingDate = detail.ShippingDate;
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }
    }
}