﻿
namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderDetailForSubmitDataGridView : VehicleOrderDetailForEditDataGridView {
        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.Columns["ExpectedShippingDate"].IsReadOnly = true;
        }
    }
}
