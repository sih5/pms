﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderExportDetailDataGridView : DcsDataGridViewBase {
        protected readonly string[] kvNames = {
            "VehicleOrderDetail_Status"
        };

        public VehicleOrderExportDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.VehicleOrderDetailDataGridView_DataContextChanged;
        }

        private void VehicleOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehicleOrder = e.NewValue as VehicleOrder;
            if(vehicleOrder == null)
                return;

            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "VehicleOrderId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = vehicleOrder.Id
            });
            this.FilterItem = compositeFilterItem;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "SON",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderExportDetail_SON
                    }, new ColumnItem {
                        Name = "ProductCategoryCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderExportDetail_ProductCategoryCode
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ExpectedShippingDate",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderExportDetail_ExpectedShippingDate
                    }, new ColumnItem {
                        Name = "EstimatedShippingDate",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderExportDetail_EstimatedShippingDate
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderExportDetailCollect_Status
                    }
                    /*, new ColumnItem {
                        Name = "数据来源",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderExportDetail_EstimatedShippingDate
                    }, new ColumnItem {
                        Name = "下线时间",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderExportDetail_EstimatedShippingDate
                    }*/
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.Cell;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleOrderDetails";
        }
    }
}
