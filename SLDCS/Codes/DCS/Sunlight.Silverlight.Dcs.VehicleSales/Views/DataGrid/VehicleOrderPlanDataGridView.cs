﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderPlanDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
             "VehicleOrderPlan_Status"
        };

        public VehicleOrderPlanDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleOrderPlan);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlan_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "YearOfPlan"
                    }, new ColumnItem {
                        Name = "MonthOfPlan"
                    }, new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "DealerName"
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleOrderPlans";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "VehicleOrderPlanDetail"
                    }
                };
            }
        }
    }
}
