﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderPlanDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] KvNames = new[] {
            "Prodcut_Color"
        };

        public VehicleOrderPlanDetailDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
            this.DataContextChanged += VehicleOrderPlanDetailDataGridView_DataContextChanged;
        }

        protected virtual void VehicleOrderPlanDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehicleOrderPlan = e.NewValue as VehicleOrderPlan;
            if(vehicleOrderPlan == null || vehicleOrderPlan.Id == default(int))
                return;
            if(this.FilterItem == null)
                FilterItem = new FilterItem {
                    MemberName = "VehicleOrderPlanId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            FilterItem.Value = vehicleOrderPlan.Id;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleOrderPlanDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCategoryCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanDetail_ProductCategoryCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ProductCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "PlannedQuantity",
                        MaskType = MaskType.Numeric,
                        MaskText = "d",
                        TextAlignment = TextAlignment.Right,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanDetail_PlannedQuantity,
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "Product.Color",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanDetail_Color,
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleOrderPlanDetailWithDetails";
        }
    }
}
