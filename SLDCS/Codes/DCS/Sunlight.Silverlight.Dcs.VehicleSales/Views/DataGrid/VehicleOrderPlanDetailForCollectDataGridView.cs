﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderPlanDetailForCollectDataGridView : VehicleOrderPlanDetailForDetailsDataGridView {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VehicleOrderPlan.YearOfPlan"
                    },new ColumnItem {
                        Name = "VehicleOrderPlan.MonthOfPlan"
                    }, new ColumnItem {
                        Name = "ProductCategoryCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanDetail_ProductCategoryCode
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "PlannedQuantity",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanDetail_PlannedQuantity
                    }
                };
            }
        }
    }
}
