﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderPlanDetailForDetailsDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VehicleOrderPlan.DealerCode"
                    }, new ColumnItem {
                        Name = "VehicleOrderPlan.DealerName"
                    }, new ColumnItem {
                        Name = "VehicleOrderPlan.YearOfPlan"
                    },new ColumnItem {
                        Name = "VehicleOrderPlan.MonthOfPlan"
                    }, new ColumnItem {
                        Name = "ProductCategoryCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanDetail_ProductCategoryCode
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "PlannedQuantity",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanDetail_PlannedQuantity
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleOrderPlanDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleOrderPlanDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
