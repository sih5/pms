﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderPlanDetailForEditDataGridView : DcsDataGridViewBase {
        //整车计划提交与整车计划管理共用此类
        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanDetail_ProductCategoryCode,
                        Name = "ProductCategoryCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ProductCode",
                        IsReadOnly = true,
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanDetail_PlannedQuantity,
                        Name = "PlannedQuantity"
                    }
                };
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = false;
            this.GridView.CanUserInsertRows = true;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleOrderPlanDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleOrderPlanDetails");
        }
    }
}
