﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderPlanSummaryDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "VehicleOrderPlanSummary_UploadType"
        };

        public VehicleOrderPlanSummaryDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "YearOfOrder"
                    }, new ColumnItem {
                        Name = "MonthOfOrder"
                    }, new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "DealerName"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductionPlanSON",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanSummary_ProductionPlanSON
                    }, new KeyValuesColumnItem{
                        Name = "UploadType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleOrderPlanSummaries";
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleOrderPlanSummary);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["YearOfOrder"]).DataFormatString = "d4";
            ((GridViewDataColumn)this.GridView.Columns["MonthOfOrder"]).DataFormatString = "d2";
        }
    }
}
