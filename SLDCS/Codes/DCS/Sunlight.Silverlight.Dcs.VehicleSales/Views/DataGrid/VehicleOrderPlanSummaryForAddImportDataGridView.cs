﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web.ExcelService;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderPlanSummaryForAddImportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ImportVehicleOrderPlanSummary_YearOfOrder,
                        Name = "YearOfOrder"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ImportVehicleOrderPlanSummary_MonthOfOrder,
                        Name = "MonthOfOrder"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ImportVehicleOrderPlanSummary_ProductCode,
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ImportVehicleOrderPlanSummary_Amount,
                        Name = "Amount"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
        }

        protected override Type EntityType {
            get {
                return typeof(ImportVehicleOrderPlanSummary);
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }
    }
}