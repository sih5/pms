﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderPlanSummaryForDelImportDataGridView : VehicleOrderPlanSummaryForAddImportDataGridView {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                   new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ImportVehicleOrderPlanSummary_YearOfOrder,
                        Name = "YearOfOrder"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ImportVehicleOrderPlanSummary_MonthOfOrder,
                        Name = "MonthOfOrder"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ImportVehicleOrderPlanSummary_ProductCode,
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ImportVehicleOrderPlanSummary_ProductionPlanSON,
                        Name = "ProductionPlanSON"
                    }
                };
            }
        }
    }
}
