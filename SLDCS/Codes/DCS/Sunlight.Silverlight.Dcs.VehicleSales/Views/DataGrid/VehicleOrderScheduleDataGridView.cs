﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderScheduleDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "BaseData_Status"
        };

        public VehicleOrderScheduleDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "YearOfOrder"
                    }, new ColumnItem {
                        Name = "MonthOfOrder"
                    }, new ColumnItem {
                        Name = "OrderSubmitDeadline"
                    }, new ColumnItem {
                        Name = "OrderPlanToPODate"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderSchedule_POToProductionPlanDate,
                        Name = "POToProductionPlanDate"
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleOrderSchedule);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["OrderSubmitDeadline"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["OrderPlanToPODate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["POToProductionPlanDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleOrderSchedules";
        }
    }
}
