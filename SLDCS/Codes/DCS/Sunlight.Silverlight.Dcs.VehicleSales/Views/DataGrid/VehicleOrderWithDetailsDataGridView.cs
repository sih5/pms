﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleOrderWithDetailsDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehicleOrder_OrderType", "VehicleOrder_Status"
        };

        public VehicleOrderWithDetailsDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title =  VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrder_Code
                    }, new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "DealerName"
                    }, new KeyValuesColumnItem {
                        Name = "OrderType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "YearOfPlan"
                    }, new ColumnItem {
                        Name = "MonthOfPlan"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleOrder);
            }
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "VehicleOrderDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleOrders";
        }
    }
}
