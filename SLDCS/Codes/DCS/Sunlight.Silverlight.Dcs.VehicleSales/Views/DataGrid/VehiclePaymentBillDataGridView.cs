﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = Telerik.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehiclePaymentBillDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
           "VehiclePaymentBill_CertificateSummary", "VehiclePaymentBill_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                  new ColumnItem {
                      Name = "Code"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePaymentBill_CustomerCompanyCode,
                      Name = "CustomerCompanyCode"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePaymentBill_CustomerCompanyName,
                      Name = "CustomerCompanyName"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleFundsType_Name,
                      Name = "VehicleFundsType.Name"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePaymentMethod_Name,
                      Name = "VehiclePaymentMethod.Name"
                  },new ColumnItem {
                      Name = "TimeOfIncomingPayment",
                      TextAlignment = TextAlignment.Right
                  },new ColumnItem {
                      Name = "Amount",
                      TextAlignment = TextAlignment.Right
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleBankAccount_BankAccountNumber,
                      Name = "VehicleBankAccount.BankAccountNumber"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleBankAccount_BankName,
                      Name = "VehicleBankAccount.BankName"
                  },new ColumnItem {
                      Name = "BillNumber"
                  },new ColumnItem {
                      Name = "PaymentCertificateNumber"
                  },new ColumnItem {
                      Name = "TimeOfRecord",
                      TextAlignment = TextAlignment.Right
                  },new KeyValuesColumnItem {
                      Name = "CertificateSummary",
                      KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                  },new KeyValuesColumnItem {
                      Name = "Status",
                      KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                  },new ColumnItem {
                      Name = "Remark"
                  },new ColumnItem {
                      Name = "CreatorName"
                  },new ColumnItem {
                      Name = "CreateTime",
                      TextAlignment = TextAlignment.Right
                  },new ColumnItem {
                      Name = "ModifierName"
                  },new ColumnItem {
                      Name = "ModifyTime",
                      TextAlignment = TextAlignment.Right
                  },new ColumnItem {
                      Name = "ApproverName"
                  },new ColumnItem {
                      Name = "ApproveTime",
                      TextAlignment = TextAlignment.Right
                  }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehiclePaymentBill);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehiclePaymentBillWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = (System.Windows.Controls.SelectionMode)SelectionMode.Single;
            ((GridViewDataColumn)(this.GridView.Columns["TimeOfIncomingPayment"])).DataFormatString = "d";
            ((GridViewDataColumn)(this.GridView.Columns["TimeOfRecord"])).DataFormatString = "d";
            ((GridViewDataColumn)(this.GridView.Columns["CreateTime"])).DataFormatString = "d";
            ((GridViewDataColumn)(this.GridView.Columns["ModifyTime"])).DataFormatString = "d";
            ((GridViewDataColumn)(this.GridView.Columns["ApproveTime"])).DataFormatString = "d";
            ((GridViewDataColumn)(this.GridView.Columns["Amount"])).DataFormatString = "c2";
        }

        public VehiclePaymentBillDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
