﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    /// <summary>
    /// 本组件只充当模板的作用，不显示数据
    /// </summary>
    public class VehiclePaymentBillForImportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                  new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePaymentBill_CustomerCompanyCode,
                      Name = "CustomerCompanyCode"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePaymentBill_CustomerCompanyName,
                      Name = "CustomerCompanyName"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePaymentMethod_Name,
                      Name = "VehiclePaymentMethod.Name"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleFundsType_Name,
                      Name = "VehicleFundsType.Name"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleBankAccount_TimeOfIncomingPayment,
                      Name = "TimeOfIncomingPayment"
                  },new ColumnItem {
                      Name = "Amount"
                  },new ColumnItem {
                      Name = "PaymentCertificateNumber"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleBankAccountForImport_BankName,
                      Name = "VehicleBankAccount.BankName"
                  },new ColumnItem {
                      Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleBankAccountForImport_BankAccountNumber,
                      Name = "VehicleBankAccount.BankAccountNumber"
                  },new ColumnItem {
                      Name = "Remark"
                  }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehiclePaymentBill);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
