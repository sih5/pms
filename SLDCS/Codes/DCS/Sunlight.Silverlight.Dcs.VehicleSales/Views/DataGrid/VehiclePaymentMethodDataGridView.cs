﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehiclePaymentMethodDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "BaseData_Status"  
        };

        public VehiclePaymentMethodDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="Code"
                    }, new ColumnItem{
                        Name="Name"
                    }, new KeyValuesColumnItem{
                        Name="Status",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name="CreatorName"
                    }, new ColumnItem{
                        Name="CreateTime",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name="ModifierName"
                    }, new ColumnItem{
                        Name="ModifyTime",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name="ApproverName",
                        Title=VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePaymentMethod_ApproverName
                    }, new ColumnItem{
                        Name="ApproveTime",
                        Title=VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehiclePaymentMethod_ApproveTime,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name="Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehiclePaymentMethod);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehiclePaymentMethods";
        }
    }
}
