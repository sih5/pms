﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehiclePreAllocationRefDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrder_Code,
                        Name = "VehicleOrder.Code"
                    }, new ColumnItem {
                        Name = "DealerCode"
                    },new ColumnItem {
                        Name = "DealerName"
                    },new ColumnItem {
                        Name = "ProductCode"
                    },new ColumnItem {
                        Name = "VehicleOrderSON"
                    },new ColumnItem {
                        Name = "ProductionPlanSON"
                    },new ColumnItem {
                        Name = "ExpectedShippingDate"
                    },new ColumnItem {
                        Name = "RolloutDate"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehiclePreAllocationRef);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehiclePreAllocationRefWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit=GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["ExpectedShippingDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["RolloutDate"]).DataFormatString = "d";
        }
    }
}
