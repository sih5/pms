﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    /// <summary>
    /// 本组件只充当模板的作用，不显示数据
    /// </summary>
    public class VehiclePreAllocationRefForAdjustImportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                  new ColumnItem {
                      Name = "YearOfOrder"
                  },new ColumnItem {
                      Name = "MonthOfOrder"
                  },new ColumnItem {
                      Name = "DealerCode"
                  },new ColumnItem {
                      Name = "DealerName"
                  },new ColumnItem {
                      Name = "VehicleOrderSON"
                  },new ColumnItem {
                      Name = "VIN"
                  },new ColumnItem {
                      Name = "ProductCode"
                  }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehiclePreAllocationRef);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
        }
    }
}
