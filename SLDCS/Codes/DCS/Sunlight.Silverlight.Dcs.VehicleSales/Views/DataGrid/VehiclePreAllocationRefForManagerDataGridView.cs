﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehiclePreAllocationRefForManagerDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "YearOfOrder"
                    }, new ColumnItem {
                        Name = "MonthOfOrder"
                    },new ColumnItem {
                        Name = "DealerCode"
                    },new ColumnItem {
                        Name = "DealerName"
                    },new ColumnItem {
                        Name = "VehicleOrderSON"
                    },new ColumnItem {
                        Name = "ProductionPlanSON"
                    },new ColumnItem {
                        Name = "VIN"
                    },new ColumnItem {
                        Name = "ProductCode"
                    },new ColumnItem {
                        Name = "ProductCategoryCode"
                    },new ColumnItem {
                        Name = "RolloutDate"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehiclePreAllocationRef);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehiclePreAllocationRefWithVehicleAvailableResources";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["RolloutDate"]).DataFormatString = "d";
        }
    }
}
