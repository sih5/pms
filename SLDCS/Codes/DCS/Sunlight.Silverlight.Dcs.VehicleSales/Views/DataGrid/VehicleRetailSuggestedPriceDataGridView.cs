﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleRetailSuggestedPriceDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "VehiclePrice_Status"
        };

        public VehicleRetailSuggestedPriceDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem{
                        Name="ProductCode"
                    }, new ColumnItem{
                        Name="ProductName"
                    }, new ColumnItem{
                        Name="Price",
                        TextAlignment = TextAlignment.Right,
                        Title=VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleRetailSuggestedPrice_Price
                    }, new ColumnItem{
                        Name="ExecutionTime",
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem{
                        Name="Status",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem{
                        Name="CreatorName"
                    }, new ColumnItem{
                        Name="CreateTime",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name="ModifierName"
                    }, new ColumnItem{
                        Name="ModifyTime",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name="Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleRetailSuggestedPrice);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleRetailSuggestedPrices";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ExecutionTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }
    }
}