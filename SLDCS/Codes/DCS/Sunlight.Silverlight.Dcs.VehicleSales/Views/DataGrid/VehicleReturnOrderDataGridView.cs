﻿
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleReturnOrderDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { "VehicleReturnOrder_Status" };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    },new ColumnItem {
                        Name = "DealerCode"
                    },new ColumnItem {
                        Name = "DealerName"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "VehicleReturnOrder"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "GetVehicleReturnOrders";
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleReturnOrder);
            }
        }

        public VehicleReturnOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
