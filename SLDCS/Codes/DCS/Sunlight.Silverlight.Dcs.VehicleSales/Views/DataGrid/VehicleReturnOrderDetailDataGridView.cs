﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleReturnOrderDetailDataGridView : DcsDataGridViewBase {

        public VehicleReturnOrderDetailDataGridView() {
            this.DataContextChanged += this.VehicleReturnOrderDetailDataGridView_DataContextChanged;
        }

        private void VehicleReturnOrderDetailDataGridView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e) {
            var vehicleReturnOrder = e.NewValue as VehicleReturnOrder;
            if(vehicleReturnOrder == null || vehicleReturnOrder.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "VehicleReturnOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = vehicleReturnOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VIN"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "OriginalSalesPrice"
                    }, new ColumnItem {
                        Name = "ReturnPrice"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleReturnOrderDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleReturnOrderDetails";
        }
    }
}
