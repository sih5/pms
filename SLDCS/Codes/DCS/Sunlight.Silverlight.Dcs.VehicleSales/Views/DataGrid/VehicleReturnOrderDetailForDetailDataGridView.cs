﻿
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleReturnOrderDetailForDetailDataGridView : DcsDataGridViewBase {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode",
                         IsReadOnly=true
                    },new ColumnItem {
                        Name = "VIN",
                         IsReadOnly=true
                    },new ColumnItem {
                        Name = "OriginalSalesPrice",
                         IsReadOnly=true
                    },new ColumnItem {
                        Name = "ReturnPrice",
                        TextAlignment = TextAlignment.Right,
                        MaskType = MaskType.Numeric,
                        IsReadOnly = false,
                    }
                };
            }
        }
        protected override void OnControlsCreated() {
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            base.OnControlsCreated();

        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleReturnOrderDetails");
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleReturnOrderDetail);
            }
        }
    }
}
