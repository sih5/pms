﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleReturnOrderDetailForEditDataGridView : DcsDataGridViewBase {
        private QueryWindowBase dealerVehicleStockDropDownQueryWindow;


        private QueryWindowBase DealerVehicleStockDropDownQueryWindow {
            get {
                if(this.dealerVehicleStockDropDownQueryWindow == null) {
                    this.dealerVehicleStockDropDownQueryWindow = DI.GetQueryWindow("DealerVehicleStockDropDown");
                    this.dealerVehicleStockDropDownQueryWindow.Loaded += dealerVehicleStockDropDownQueryWindow_Loaded;
                    this.dealerVehicleStockDropDownQueryWindow.SelectionDecided += this.dealerVehicleStockDropDownQueryWindow_SelectionDecided;
                }
                return this.dealerVehicleStockDropDownQueryWindow;
            }
        }

        private void dealerVehicleStockDropDownQueryWindow_Loaded(object sender, RoutedEventArgs e) {
            var queryWindow = sender as QueryWindowBase;
            if(queryWindow == null)
                return;
            queryWindow.ExchangeData(null, "SetAdditionalFilterItem", new FilterItem("CompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            var dataContext = queryWindow.DataContext as DealerTransactionDetail;
            if(dataContext == null)
                return;
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "InventoryStatus", DcsDealerVehicleStockInventoryStatus.在库
            });
            queryWindow.ExchangeData(null, "SetQueryItemValue", new object[] {
                "Common", "LockStatus", DcsDealerVehicleStockLockStatus.未锁定
            });
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleReturnOrderDetails");
        }



        private void dealerVehicleStockDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var queryWindow = sender as DcsDropDownQueryWindowBase;
            if(queryWindow == null || queryWindow.SelectedEntities == null)
                return;
            var dealerVehicleStock = queryWindow.SelectedEntities.Cast<DealerVehicleStock>().FirstOrDefault();
            var vehicleReturnOrderDetail = queryWindow.DataContext as VehicleReturnOrderDetail;
            if(dealerVehicleStock == null || vehicleReturnOrderDetail == null)
                return;
            vehicleReturnOrderDetail.VehicleId = dealerVehicleStock.VehicleId;
            vehicleReturnOrderDetail.VIN = dealerVehicleStock.VIN;
            vehicleReturnOrderDetail.ProductId = dealerVehicleStock.ProductId;
            vehicleReturnOrderDetail.ProductCode = dealerVehicleStock.ProductCode;
            vehicleReturnOrderDetail.ProductName = dealerVehicleStock.ProductName;
            vehicleReturnOrderDetail.ProductCategoryCode = dealerVehicleStock.ProductCategoryCode;
            vehicleReturnOrderDetail.ProductCategoryName = dealerVehicleStock.ProductCategoryName;
            vehicleReturnOrderDetail.OriginalSalesPrice = dealerVehicleStock.PurchasePrice;
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode",
                        IsReadOnly=true
                    },new DropDownTextBoxColumnItem {
                        Name = "VIN",
                        DropDownContent = this.DealerVehicleStockDropDownQueryWindow,
                        IsEditable = false
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleReturnOrderDetail_ReturnPrice,
                        Name = "OriginalSalesPrice",
                        IsReadOnly=true
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleReturnOrderDetail);
            }
        }
    }
}
