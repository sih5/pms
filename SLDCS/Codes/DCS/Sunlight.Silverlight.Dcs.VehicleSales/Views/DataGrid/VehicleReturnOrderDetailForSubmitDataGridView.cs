﻿
using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleReturnOrderDetailForSubmitDataGridView : DcsDataGridViewBase {
        public VehicleReturnOrderDetailForSubmitDataGridView() {
            this.DataContextChanged += this.VehicleReturnOrderDetailForSubmitDataGridView_DataContextChanged;
        }
        private void VehicleReturnOrderDetailForSubmitDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehicleReturnOrder = e.NewValue as VehicleReturnOrder;
            if(vehicleReturnOrder == null || vehicleReturnOrder.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "VehicleReturnOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = vehicleReturnOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCategoryCode"
                    },new ColumnItem {
                        Name = "VIN"
                    },new ColumnItem {
                        Name = "ProductCode"
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleReturnOrderDetail_ReturnPrice,
                        Name = "OriginalSalesPrice"
                    },new ColumnItem {
                        Name = "ReturnPrice"
                    }
                };
            }
        }
        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleReturnOrderDetails";
        }
        protected override Type EntityType {
            get {
                return typeof(VehicleReturnOrderDetail);
            }
        }
    }
}
