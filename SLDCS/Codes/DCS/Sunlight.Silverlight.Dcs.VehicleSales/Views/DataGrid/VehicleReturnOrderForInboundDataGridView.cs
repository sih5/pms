﻿using System;
using System.Windows.Controls;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleReturnOrderForInboundDataGridView : DcsDataGridViewBase {

        public VehicleReturnOrderForInboundDataGridView() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.GridView.Loaded += this.GridView_Loaded;
        }

        private void GridView_Loaded(object sender, System.Windows.RoutedEventArgs e) {
            this.ExecuteQueryDelayed();
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleReturnOrder_Code
                    }, new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "DealerName"
                    }, new ColumnItem {
                        Name = "ReturnedWarehouseName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleReturnOrder_ReturnedWarehouseName
                    }, new ColumnItem {
                        Name = "ProcessFinishTime",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleReturnOrder_ProcessFinishTime
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleReturnOrder);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.IsReadOnly = true;
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "VehicleReturnOrderDetail"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleReturnOrders";
        }

        protected override Telerik.Windows.Data.IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            if(this.FilterItem == null) {
                this.FilterItem = new FilterItem {
                    IsExact = true,
                    MemberName = "Status",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = (int)DcsVehicleReturnOrderStatus.经销商已出库
                };
                return this.FilterItem.ToFilterDescriptor();
            }
            return this.FilterItem.ToFilterDescriptor();
        }
    }
}
