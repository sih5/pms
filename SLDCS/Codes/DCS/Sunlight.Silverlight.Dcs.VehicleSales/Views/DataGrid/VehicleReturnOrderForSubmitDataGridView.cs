﻿using System.Collections.Generic;
using System.Windows.Controls;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleReturnOrderForSubmitDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehicleReturnOrder_Status"
        };

        public VehicleReturnOrderForSubmitDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    },new ColumnItem {
                        Name = "CreateTime"
                    },new ColumnItem {
                        Name = "CreatorName"
                    },new ColumnItem {
                        Name = "ProcessFinishTime",
                        Title = VehicleSalesUIStrings.QueryPanel_QueryItem_Tilte_VehicleReturnOrder_ProcessFinishTime
                    },new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };

            }
        }

        protected override System.Type EntityType {
            get {
                return typeof(VehicleReturnOrder);
            }
        }
        protected override string OnRequestQueryName() {
            return "GetVehicleReturnOrders";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "VehicleReturnOrderForSubmit"
                    }
                };
            }
        }
    }
}
