﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleSeriesDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductCategoryCode",
                        Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleSeries_ProductCategoryCode
                    }, new ColumnItem {
                        Name = "VehicleSeriesCode",
                    }, new ColumnItem {
                        Name = "RegionCode",
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleSeries";
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleSery);
            }
        }
    }
}