﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    /// <summary>
    /// 仅在车系代码维护，导入中用作模板
    /// </summary>
    public class VehicleSeriesForImportDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductCategoryCode"
                    }, new ColumnItem {
                        Name = "VehicleSeriesCode"
                    }, new ColumnItem {
                        Name = "RegionCode"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleSery);
            }
        }
    }
}
