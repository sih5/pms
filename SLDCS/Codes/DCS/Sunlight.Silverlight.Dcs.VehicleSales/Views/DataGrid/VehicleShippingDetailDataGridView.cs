﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShippingDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehicleShippingDetail_VehicleReceptionStatus"
        };
        public VehicleShippingDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += this.VehicleShippingDetailDataGridViewDataContextChanged;
        }

        private void VehicleShippingDetailDataGridViewDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehicleShippingOrder = e.NewValue as VehicleShippingOrder;
            if(vehicleShippingOrder == null || vehicleShippingOrder.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "VehicleShippingOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = vehicleShippingOrder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "VIN",
                       Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingDetail_VIN,
                    }, new ColumnItem {
                       Name = "ProductCode"
                    }, new ColumnItem {
                       Name = "ProductCategoryCode"
                    }, new ColumnItem {
                       Name = "ShippingDate",
                       TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                       Name = "VehicleReceptionStatus",
                       KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    }, new ColumnItem {
                       Name = "ReceivingWarehouseName",
                       Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingDetail_ReceivingWarehouseName
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShippingDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleShippingDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["ShippingDate"]).DataFormatString = "d";
        }
    }
}