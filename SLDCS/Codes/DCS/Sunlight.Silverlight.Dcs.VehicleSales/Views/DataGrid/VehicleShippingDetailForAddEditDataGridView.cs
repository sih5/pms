﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShippingDetailForAddEditDataGridView : DcsDataGridViewBase {
        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VehicleShippingOrder.Code",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingOrder_Codes
                    }, new ColumnItem {
                        Name = "VIN"
                    },new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductCategoryName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingDetail_ProductCategoryName
                    }, new ColumnItem {
                        Name = "ShippingDate"
                    }, new ColumnItem {
                        Name = "VehicleShippingOrder.DealerCode"
                    }, new ColumnItem {
                        Name = "VehicleShippingOrder.DealerName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShippingDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleShippingDetails");
        }
    }
}
