﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShippingDetailForEditDataGridView : DcsDataGridViewBase {
        private ObservableCollection<KeyValuePair> kvDealerWarehouses;
        private readonly string[] kvNames = {
            "VehicleShippingDetail_VehicleReceptionStatus"
        };

        public ObservableCollection<KeyValuePair> KvDealerWarehouses {
            get {
                return this.kvDealerWarehouses ?? (this.kvDealerWarehouses = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                       Name = "VIN",
                       Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingDetail_VIN,
                       IsReadOnly = true
                    }, new ColumnItem {
                       Name = "ProductCode",
                       IsReadOnly = true
                    }, new ColumnItem {
                       Name = "ProductCategoryCode",
                       IsReadOnly = true
                    }, new ColumnItem {
                       Name = "ShippingDate",
                       IsReadOnly = true
                    }, new KeyValuesColumnItem {
                       Name = "VehicleReceptionStatus",
                       KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new KeyValuesColumnItem {
                       Name = "ReceivingWarehouseId",
                       KeyValueItems = this.KvDealerWarehouses,
                       Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingDetail_ReceivingWarehouseName
                    }
                };
            }
        }

        protected override bool ShowCheckBox {
            get {
                return true;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShippingDetail);
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleShippingDetails");
        }


        public VehicleShippingDetailForEditDataGridView() {
            this.CreateUI();
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.IsSynchronizedWithCurrentItem = true;
            this.GridView.CanUserDeleteRows = false;
            this.GridView.CanUserInsertRows = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.CellEditEnded += GridView_CellEditEnded;
        }

        private void CreateUI() {
            var domainContext = new DcsDomainContext();
            domainContext.Load(domainContext.GetDealerWarehousesQuery().Where(e => e.DealerId == BaseApp.Current.CurrentUserData.EnterpriseId && e.Status == (int)DcsBaseDataStatus.有效), loadOp => {
                if(loadOp.HasError)
                    return;
                this.KvDealerWarehouses.Clear();
                foreach(var dealerwarehouse in loadOp.Entities)
                    this.KvDealerWarehouses.Add(new KeyValuePair {
                        Key = dealerwarehouse.Id,
                        Value = dealerwarehouse.Name,
                        UserObject = dealerwarehouse
                    });
            }, null);
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var detail = e.Cell.DataContext as VehicleShippingDetail;
            if(detail == null)
                return;
            switch(e.Cell.Column.UniqueName) {
                case "VehicleReceptionStatus":
                    this.SetVehicleShippingDetailStatus(detail);
                    break;
                case "ReceivingWarehouseId":
                    var temp = e.EditingElement as RadComboBox;
                    if(temp != null) {
                        var dealerwarehouse = temp.SelectedItem as KeyValuePair;
                        if(dealerwarehouse != null) {
                            detail.ReceivingWarehouseId = ((DealerWarehouse)dealerwarehouse.UserObject).Id;
                            detail.ReceivingWarehouseCode = ((DealerWarehouse)dealerwarehouse.UserObject).Code;
                            detail.ReceivingWarehouseName = ((DealerWarehouse)dealerwarehouse.UserObject).Name;
                        }
                    }
                    break;
            }
        }



        private void SetVehicleShippingDetailStatus(VehicleShippingDetail detail) {
            if(this.GridView.SelectedItems != null && this.GridView.SelectedItems.Any()) {
                foreach(var selectiem in this.GridView.SelectedItems.Cast<VehicleShippingDetail>()) {
                    selectiem.VehicleReceptionStatus = detail.VehicleReceptionStatus;
                }
            }
        }
    }
}
