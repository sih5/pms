﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShippingOrderChangeRecDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "OriginalVIN"
                    }, new ColumnItem {
                        Name = "NewVIN"
                    }, new ColumnItem {
                        Name = "ProductCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingOrderChangeRec_ProductCode,
                    },  new ColumnItem {
                        Name = "VehicleShippingOrder.DealerName"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingOrderChangeRec_VehicleShippingOrderCode,
                        Name = "VehicleShippingOrderCode"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShippingOrderChangeRec);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleShippingOrderChangeRecsWithDetail";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
        }
    }
}
