﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShippingOrderChangeRecForEditDataGridView : DcsDataGridViewBase {
        private VehicleShippingOrderChangeRec vehicleShippingOrderChangeRec;
        public VehicleShippingOrderChangeRecForEditDataGridView() {
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.GridView.AddingNewDataItem -= GridView_AddingNewDataItem;
            this.GridView.AddingNewDataItem += GridView_AddingNewDataItem;
        }

        private void GridView_AddingNewDataItem(object sender, GridViewAddingNewEventArgs e) {
            if(this.VehicleShippingOrderChangeRec.NewVehicleId == default(int)) {
                UIHelper.ShowNotification(VehicleSalesUIStrings.DataEditView_Validation_VehShipplanApprovalChangeRec_NewVehicleIdIsNull);
                e.Cancel = true;
                return;
            }
            //if(string.IsNullOrEmpty(this.VehicleShippingOrderChangeRec.NewVIN)) {
            //    UIHelper.ShowNotification("请填写替换VIN后并选择VIN"/*VehicleSalesUIStrings.DataEditView_Validation_VehShipplanApprovalChangeRec_NewVINIdIsNull*/);
            //    e.Cancel = true;
            //    return;
            //}
            e.NewObject = new VehicleShippingOrderChangeRec {
                OriginalVIN = this.VehicleShippingOrderChangeRec.OriginalVIN,
                NewVIN = this.VehicleShippingOrderChangeRec.NewVIN,
                VehicleShippingOrderCode = this.VehicleShippingOrderChangeRec.VehicleShippingOrderCode,
                ProductCode = this.VehicleShippingOrderChangeRec.ProductCode,
                ProductCategoryName = this.VehicleShippingOrderChangeRec.ProductCategoryName,
                Status = (int)DcsVehicleBaseDataStatus.有效,
                VehicleSalesOrgId = 1,
                VehicleSalesOrgCode = "CAM",
                VehicleSalesOrgName = "CAM"
            };
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "OriginalVIN",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "NewVIN",
                        IsReadOnly = true
                    },new ColumnItem {
                        Name = "ProductCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingOrderChangeRec_ProductCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ProductCategoryName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingOrderChangeRec_ProductCategoryName,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "VehicleShippingOrderCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingOrderChangeRec_VehicleShippingOrderCodes,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShippingOrderChangeRec);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            this.GridView.SelectionMode = SelectionMode.Multiple;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleShippingOrderChangeRecs");
        }

        public VehicleShippingOrderChangeRec VehicleShippingOrderChangeRec {
            get {
                return this.vehicleShippingOrderChangeRec ?? (this.vehicleShippingOrderChangeRec = new VehicleShippingOrderChangeRec());
            }
        }
    }
}
