﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShippingOrderConfirmDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehicleShippingOrder_Status","VehicleShipping_Method"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingOrder_Code,
                    }, new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "DealerName",
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                    }, new ColumnItem {
                        Name = "CreateTime",
                    }, new ColumnItem {
                        Name = "CreatorName",
                    }, new ColumnItem {
                        Name = "ModifyTime",
                    }, new ColumnItem {
                        Name = "ModifierName",
                    }
                };
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var filters = compositeFilterItem.Filters;
                foreach(var item in filters) {
                    if(new[] { "Code", "VehicleShippingDetail.ShippingDate", "Status", "VehicleShippingDetail.VIN" }.Contains(item.MemberName))
                        continue;
                    if(item.GetType() == typeof(CompositeFilterItem))
                        continue;
                    newCompositeFilterItem.Filters.Add(item);

                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override string OnRequestQueryName() {
            return "查询整车发运单";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var compositeCreateTime = compositeFilterItem.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                switch(parameterName) {
                    case "code":
                        return compositeFilterItem.Filters.Single(item => item.MemberName == "Code").Value as string;
                    case "shippingDate":
                        return compositeFilterItem.Filters.Single(item => item.MemberName == "VehicleShippingDetail.ShippingDate").Value as DateTime?;
                    case "createTimeStart":
                        if(compositeCreateTime != null)
                            return compositeCreateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                        return null;
                    case "createTimeEnd":
                        if(compositeCreateTime != null)
                            return compositeCreateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        return null;
                    case "status":
                        return compositeFilterItem.Filters.Single(item => item.MemberName == "Status").Value as int?;
                    //case "dealerName":
                    //    return compositeFilterItem.Filters.Single(item => item.MemberName == "DealerName").Value as string;
                    case "vin":
                        return compositeFilterItem.Filters.Single(item => item.MemberName == "VehicleShippingDetail.VIN").Value as string;
                }
            }
            return null;
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                      "VehicleShippingDetail" 
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShippingOrder);
            }
        }

        public VehicleShippingOrderConfirmDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }
    }
}