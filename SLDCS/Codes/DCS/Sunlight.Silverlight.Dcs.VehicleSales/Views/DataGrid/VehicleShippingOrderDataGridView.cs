﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShippingOrderDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehicleShippingOrder_Status", "VehicleShipping_Method", "VehicleShippingDetail_VehicleReceptionStatus"
        };

        public VehicleShippingOrderDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShippingOrder);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code",
                        Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_Code
                    }, new ColumnItem {
                        Name = "DealerCode",
                        Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_DealerCode
                    }, new ColumnItem {
                        Name = "DealerName",
                        Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_DealerName
                    }, new KeyValuesColumnItem {
                        Name = "ShippingMethod",
                        Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_ShippingMethod,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Multiple;
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.FullRow;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }

        protected override string OnRequestQueryName() {
            return "查询整车发运单";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            switch(parameterName) {
                case "code":
                    var codeFilters = filter.Filters.FirstOrDefault(item => item.MemberName != null && item.MemberName == "Code");
                    if(codeFilters != null) {
                        var codestring = codeFilters.Value as string;
                        return string.IsNullOrEmpty(codestring) ? null : codestring;
                    }
                    return null;
                case "dealerName":
                    var dealerNameFilters = filter.Filters.FirstOrDefault(item => item.MemberName != null && item.MemberName == "DealerName");
                    if(dealerNameFilters != null) {
                        var dealerNamestring = dealerNameFilters.Value as string;
                        return string.IsNullOrEmpty(dealerNamestring) ? null : dealerNamestring;
                    }
                    return null;
                case "status":
                    var statusFilters = filter.Filters.FirstOrDefault(item => item.MemberName != null && item.MemberName == "Status");
                    if(statusFilters != null) {
                        var statusint = statusFilters.Value as int?;
                        if(statusint.HasValue)
                            return statusint.Value;
                    }
                    return null;
                case "dispatchWarehouseId":
                    var dispatchwarehouseidFilters = filter.Filters.FirstOrDefault(item => item.MemberName != null && item.MemberName == "DispatchWarehouseId");
                    if(dispatchwarehouseidFilters!=null) {
                        var warehouseint = dispatchwarehouseidFilters.Value as int?;
                        if(warehouseint.HasValue)
                            return warehouseint.Value;
                    }
                    return null;
                case "vin":
                    var vinFilters = filter.Filters.FirstOrDefault(item => item.MemberName != null && item.MemberName == "VIN");
                    if(vinFilters != null) {
                        var vinString = vinFilters.Value as string;
                        return string.IsNullOrEmpty(vinString) ? null : vinString;
                    }
                    return null;
                case "shippingDate":
                    var endTimeFilters = filter.Filters.FirstOrDefault(item => item.MemberName != null && item.MemberName == "ShippingDate");
                    if(endTimeFilters != null) {
                        var dateTime = endTimeFilters.Value as DateTime?;
                        if(dateTime.HasValue)
                            return new DateTime(dateTime.Value.Year, dateTime.Value.Month, dateTime.Value.Day, 0, 0, 0);
                    }
                    return null;
            }
            return null;
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var newCompositeFilterItem = new CompositeFilterItem();
                var compositeFilters = compositeFilterItem.Filters.Where(item => (item.GetType() == typeof(CompositeFilterItem) && item.MemberName != "DispatchWarehouseId" && item.MemberName != "VIN" && item.MemberName != "ShippingDate"));
                if(compositeFilters.Any())
                    foreach(var item in compositeFilters)
                        newCompositeFilterItem.Filters.Add(item);
                return newCompositeFilterItem.ToFilterDescriptor();
            }
            return this.FilterItem.ToFilterDescriptor();
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "VehicleShippingOrder"
                    }
                };
            }
        }
    }
}
