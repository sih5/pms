﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShippingOrderDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] KvNames = new[] {
            "VehicleShippingDetail_VehicleReceptionStatus"
        };

        public VehicleShippingOrderDetailDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
            this.DataContextChanged += VehicleShippingOrderDetailDataGridView_DataContextChanged;
        }

        protected virtual void VehicleShippingOrderDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehicleshippingorder = e.NewValue as VehicleShippingOrder;
            if(vehicleshippingorder == null || vehicleshippingorder.Id == default(int))
                return;
            if(this.FilterItem == null)
                FilterItem = new FilterItem {
                    MemberName = "VehicleShippingOrderId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            FilterItem.Value = vehicleshippingorder.Id;
            this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShippingDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["ShippingDate"]).DataFormatString = "d";
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VIN",
                        Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_VIN,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ProductCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderDetailCollect_ProductCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ProductCategoryCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleOrderPlanDetail_ProductCategoryCode,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ShippingDate",
                        Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_ShippingDate
                    }, new KeyValuesColumnItem {
                        Name = "VehicleReceptionStatus",
                        Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_VehicleReceptionStatus,
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]],
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "DispatchWarehouseName",
                        Title = VehicleSalesUIStrings.QueryPanel_Title_VehicleShippingOrder_DispatchWarehouseId
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleShippingDetails";
        }
    }
}
