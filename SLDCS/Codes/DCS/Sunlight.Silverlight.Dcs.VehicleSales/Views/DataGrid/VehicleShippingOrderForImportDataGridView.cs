﻿using System;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShippingOrderForImportDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehicleShipping_Method"
        };

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VehicleShippingOrder.DealerCode"
                    }, new ColumnItem {
                        Name = "VehicleShippingOrder.DealerName"
                    }, new ColumnItem {
                        Name = "DispatchWarehouseCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingDetail_DispatchWarehouse
                    }, new ColumnItem {
                        Name = "VIN",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShippingDetail_VIN
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ShippingDate"
                    }, new KeyValuesColumnItem {
                        Name = "VehicleShippingOrder.ShippingMethod"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShippingDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        public VehicleShippingOrderForImportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
