﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShipplanApprovalDataGridView : DcsDataGridViewBase {
        private ObservableCollection<KeyValuePair> keyValuePairs;
        private readonly string[] kvNames = new[] {
            "VehicleShipplanApproval_Status"
        };

        public VehicleShipplanApprovalDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        private ObservableCollection<KeyValuePair> KeyValuePairs {
            get {
                return this.keyValuePairs ?? (this.keyValuePairs = new ObservableCollection<KeyValuePair>());
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShipplanApproval);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApproval_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "DealerName"
                    }, new KeyValuesColumnItem {
                        Name = "VehicleFundsTypeId",
                        KeyValueItems = this.KeyValuePairs,
                        Title =VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleFundsType_Name,
                    }, new ColumnItem{
                        Name = "Amount",
                        TextAlignment = TextAlignment.Right
                    },new ColumnItem {
                        Title=VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleCustomerAccount_AvailableBalance,
                        Name = "VehicleCustomerAccount.AvailableBalance",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title=VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApproval_ApproveTime,
                        Name = "ApproveTime"
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime"
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime"
                    },new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleShipplanApprovalsWithDetails";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "VehicleShipplanApprovalDetail"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.Loaded += GridView_Loaded;
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ApproveTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["Amount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["VehicleCustomerAccount.AvailableBalance"]).DataFormatString = "c2";
        }

        private void GridView_Loaded(object sender, RoutedEventArgs e) {
            var dcsDomainContext = this.DomainContext as DcsDomainContext;
            if(dcsDomainContext == null)
                return;
            dcsDomainContext.Load(dcsDomainContext.GetVehicleFundsTypesQuery().Where(r => r.Status == (int)DcsBaseDataStatus.有效), LoadBehavior.RefreshCurrent, loadOp => {
                if(loadOp.HasError)
                    loadOp.MarkErrorAsHandled();

                this.KeyValuePairs.Clear();
                foreach(var entity in loadOp.Entities) {
                    this.KeyValuePairs.Add(new KeyValuePair {
                        Key = entity.Id,
                        Value = entity.Name
                    });
                }
            }, null);
        }
    }
}
