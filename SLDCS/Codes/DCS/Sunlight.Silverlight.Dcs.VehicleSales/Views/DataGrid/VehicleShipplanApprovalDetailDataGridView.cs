﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShipplanApprovalDetailDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "VehicleShipplanApprovalDetail_Status"
        };

        public VehicleShipplanApprovalDetailDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.DataContextChanged += VehicleShipplanApprovalDetailDataGridView_DataContextChanged;
        }

        protected virtual void VehicleShipplanApprovalDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var vehicleShipplanApproval = e.NewValue as VehicleShipplanApproval;
            if(vehicleShipplanApproval == null || vehicleShipplanApproval.Id == default(int))
                return;
            vehicleShipplanApproval.PropertyChanged -= vehicleShipplanApproval_PropertyChanged;
            vehicleShipplanApproval.PropertyChanged += vehicleShipplanApproval_PropertyChanged;
            if(this.FilterItem == null)
                FilterItem = new FilterItem {
                    MemberName = "VehicleShipplanApprovalId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            FilterItem.Value = vehicleShipplanApproval.Id;
            this.ExecuteQueryDelayed();
        }

        protected void vehicleShipplanApproval_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
            if(e.PropertyName == "Status")
                this.ExecuteQueryDelayed();
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShipplanApprovalDetail);
            }
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VIN"
                    },new ColumnItem {
                        Name = "ProductCode",
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_ProductCategoryCode,
                        Name = "ProductCategoryCode",
                        IsReadOnly = true
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_Status,
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Name = "Price",
                    },new ColumnItem {
                        Name = "VehicleInformation.RolloutDate"
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_DispatchWarehouseName,
                        Name = "DispatchWarehouseName"
                    }
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleShipplanApprovalDetailWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["VehicleInformation.RolloutDate"]).DataFormatString = "d";
        }
    }
}
