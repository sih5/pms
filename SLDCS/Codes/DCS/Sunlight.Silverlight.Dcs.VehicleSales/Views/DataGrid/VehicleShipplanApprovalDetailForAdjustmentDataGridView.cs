﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShipplanApprovalDetailForAdjustmentDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "VehShipplanApprovalChangeRec_AdjustmentType"
        };

        public VehicleShipplanApprovalDetailForAdjustmentDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VIN",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_VIN
                    }, 
                    new ColumnItem {
                        Name = "ProductCode"
                    }, 
                    new ColumnItem {
                        Name = "ProductCategoryName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_ProductCategory
                    },
                    new KeyValuesColumnItem {
                        Name = "AdjustmentType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_AdjustmentType
                    }, 
                    new ColumnItem {
                        Name = "DispatchWarehouseName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_DispatchWarehouse
                    }, 
                    new ColumnItem {
                        Name = "VehicleShipplanApproval.Code",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_VehicleShipplanApproval_Code
                    } 
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShipplanApprovalDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.ShowGroupPanel = false;
            this.GridView.IsReadOnly = true;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleShipplanApprovalDetailsForAdjustment");
        }
    }
}
