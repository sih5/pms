﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShipplanApprovalDetailForChangeRecDataGridView : DcsDataGridViewBase {

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VehicleShipplanApproval.Code",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_VehicleShipplanApproval_Code2
                    }, new ColumnItem {
                        Name = "VIN"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductCategoryName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_ProductCategory
                    }, new ColumnItem {
                        Name = "DispatchWarehouseName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_DispatchWarehouse
                    }, new ColumnItem {
                        Name = "VehicleShipplanApproval.DealerCode"
                    }, new ColumnItem {
                        Name = "VehicleShipplanApproval.DealerName"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShipplanApprovalDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("VehicleShipplanApprovalDetails");
        }
    }
}
