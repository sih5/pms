﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShipplanApprovalDetailForSelectedDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[]{
            "VehicleShipping_Method","VehicleShipplanApprovalDetail_Status"
        };

        private readonly DcsDomainContext dcsDomainContext = new DcsDomainContext();

        private ObservableCollection<KeyValuePair> kvVehicleFundsTypes = new ObservableCollection<KeyValuePair>();

        private void Initialize() {
            dcsDomainContext.Load(dcsDomainContext.GetVehicleFundsTypesQuery(), loadOp => {
                if(loadOp.HasError)
                    return;
                foreach(var vehicleFundsType in loadOp.Entities)
                    this.kvVehicleFundsTypes.Add(new KeyValuePair {
                        Key = vehicleFundsType.Id,
                        Value = vehicleFundsType.Name
                    });
            }, null);
        }

        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "VehicleShipplanApproval.DealerCode"
                    }, new ColumnItem {
                        Name = "VehicleShipplanApproval.DealerName"
                    }, new ColumnItem {
                        Name = "DispatchWarehouseName",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApprovalDetail_DispatchWarehouseName
                    }, new KeyValuesColumnItem {
                        Name = "VehicleShipplanApproval.ShippingMethod",
                       KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                       Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApproval_ShippingMethod
                    }, new KeyValuesColumnItem {
                        Name = "VehicleShipplanApproval.VehicleFundsTypeId",
                        KeyValueItems = this.kvVehicleFundsTypes,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApproval_VehicleFundsType
                    }, new ColumnItem {
                        Name = "VIN"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductCategoryCode"
                    }, new ColumnItem {
                        Name = "SON"
                    }, new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]],
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleShipplanApproval_Status
                    }, new ColumnItem {
                        Name = "Price"
                    }
                };
            }
        }

        protected override Telerik.Windows.Data.IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();

            if(compositeFilterItem == null) {
                newCompositeFilterItem.Filters.Add(this.FilterItem);
            } else {
                foreach(var item in compositeFilterItem.Filters.Where(filter => filter.MemberName != "DealerName" && filter.GetType() != typeof(CompositeFilterItem)))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters != null) {
                switch(parameterName) {
                    case "dealerName":
                        return filters.Filters.Single(item => item.MemberName == "DealerName").Value;
                    case "status":
                        return filters.Filters.Single(item => item.MemberName == "Status").Value;
                    case "startApprovalTime":
                        if(filters.Filters.Any(filter => filter.GetType() == typeof(CompositeFilterItem))) {
                            var approvalTime = filters.Filters.Single(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            return approvalTime == null ? null : (DateTime?)approvalTime.Filters.ElementAt(0).Value == DateTime.MinValue ? null : approvalTime.Filters.ElementAt(0).Value as DateTime?;
                        }
                        return null;
                    case "endApprovalTime":
                        if(filters.Filters.Any(filter => filter.GetType() == typeof(CompositeFilterItem))) {
                            var approvalTime = filters.Filters.Single(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                            if(approvalTime == null)
                                return null;
                            var endApprovalTime = approvalTime.Filters.ElementAt(1).Value as DateTime?;
                            if(endApprovalTime.HasValue)
                                approvalTime.Filters.ElementAt(1).Value = new DateTime(endApprovalTime.Value.Year, endApprovalTime.Value.Month, endApprovalTime.Value.Day, 23, 59, 59);

                            return approvalTime.Filters.ElementAt(1).Value;
                        }
                        return null;
                }
            }
            return base.OnRequestQueryParameter(queryName, parameterName);
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleShipplanApprovalDetail);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询整车发车审批清单";
        }

        public VehicleShipplanApprovalDetailForSelectedDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
            this.Initializer.Register(this.Initialize);
        }
    }
}
