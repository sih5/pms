﻿using System.Linq;
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleShipplanApprovalForSecondDataGridView : VehicleShipplanApprovalDataGridView {
        protected override string OnRequestQueryName() {
            return "GetVehicleShipplanApprovalWithDetails";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            switch(parameterName) {
                case "vehicleShipplanApprovalCode":
                    return filter.Filters.Single(item => item.MemberName == "Code").Value as string;
                case "dealerName":
                    return filter.Filters.Single(item => item.MemberName == "DealerName").Value as string;
                case "vehicleFundsTypeId":
                    return filter.Filters.Single(item => item.MemberName == "VehicleFundsTypeId").Value as int?;
                case "status":
                    return filter.Filters.Single(item => item.MemberName == "Status").Value as int?;
                case "productCategoryId":
                    return filter.Filters.Single(item => item.MemberName == "ProductCategoryId").Value as int?;
            }
            return null;
        }

        protected override Telerik.Windows.Data.IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var filters = compositeFilterItem.Filters;
                foreach(var item in filters) {
                    if(new[]
                        {
                            "Code", "DealerName", "VehicleFundsTypeId", "Status", "ProductCategoryId"
                        }.Contains(item.MemberName))
                        continue;
                    newCompositeFilterItem.Filters.Add(item);
                }
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }
    }
}
