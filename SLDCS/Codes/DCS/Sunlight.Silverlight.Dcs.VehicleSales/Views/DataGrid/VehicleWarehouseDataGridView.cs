﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleWarehouseDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "VehicleWarehouse_Type"
        };

        public VehicleWarehouseDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "Name"
                    },new KeyValuesColumnItem {
                        Name = "Type",
                        KeyValueItems=this.KeyValueManager[this.kvNames[0]]
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleWarehouse_ContactPhone,
                        Name = "ContactPhone"
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleWarehouse_ProvinceName,
                        Name = "TiledRegion.ProvinceName"
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleWarehouse_CityName,
                        Name = "TiledRegion.CityName"
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleWarehouse_Fax,
                        Name = "Fax"
                    },new ColumnItem {
                        Name = "Region.PostCode"
                    },new ColumnItem {
                        Name = "ContactPerson"
                    },new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleWarehouse_Address,
                        Name = "Address"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleWarehouse);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleWarehouseWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
