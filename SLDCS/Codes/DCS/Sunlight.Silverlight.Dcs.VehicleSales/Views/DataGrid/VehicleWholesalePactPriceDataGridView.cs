﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleWholesalePactPriceDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "VehiclePrice_Status"
        };

        protected override Type EntityType {
            get {
                return typeof(VehicleWholesalePactPrice);
            }
        }

        public VehicleWholesalePactPriceDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "DealerName"
                    }, new ColumnItem {
                        Name = "VehicleSalesTypeName"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductName"
                    }, new ColumnItem {
                        Name = "ProductPrice",
                        TextAlignment = TextAlignment.Right,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleWholesalePactPrice_ProductPrice
                    }, new ColumnItem {
                        Name = "TreatyPrice",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "ExecutionDate",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleWholesalePactPrice_ExecutionDate,
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "ExpireDate",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleWholesalePactPrice_ExpireDate,
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["ProductPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TreatyPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ExecutionDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ExpireDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleWholesalePactPriceWithDetails";
        }
    }
}
