﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class VehicleWholesalePriceDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] {
            "VehiclePrice_Status"
        };

        public VehicleWholesalePriceDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "BranchName"
                    }, new ColumnItem {
                        Name = "VehicleSalesTypeName"
                    }, new ColumnItem {
                        Name = "ProductCode"
                    }, new ColumnItem {
                        Name = "ProductName"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleWholesalePrice_Price,
                        Name = "Price",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_VehicleWholesalePactPrice_TreatyPrice,
                        Name = "TreatyPrice",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_ExecutionDate,
                        Name = "ExecutionDate",
                        TextAlignment = TextAlignment.Right
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "CreatorName"
                    }, new ColumnItem {
                        Name = "CreateTime",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ModifierName"
                    }, new ColumnItem {
                        Name = "ModifyTime",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "Remark"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(VehicleWholesalePrice);
            }
        }

        protected override string OnRequestQueryName() {
            return "GetVehicleWholesalePriceWithDetails";
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            ((GridViewDataColumn)this.GridView.Columns["Price"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["TreatyPrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["ExecutionDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["CreateTime"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["ModifyTime"]).DataFormatString = "d";
        }
    }
}
