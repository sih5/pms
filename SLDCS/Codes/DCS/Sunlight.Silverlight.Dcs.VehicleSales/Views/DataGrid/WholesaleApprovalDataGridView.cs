﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class WholesaleApprovalDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "MultiLevelAuditConfig_InitialStatus", "KeyAccount_CustomerProperty", "KeyAccount_PurchasePurpose", "WholesaleApproval_EmployeePurchaseDocType"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "KeyAccount.KeyAccountCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_KeyAccountCode
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.Code",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_Code
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.Name",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_Name
                    }, new KeyValuesColumnItem {
                        Name = "KeyAccount.CustomerProperty",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_CustomerProperty,
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem{
                        Name = "PostAuditStatu.Name",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_PostAuditStatusName
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.Address",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_Address
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.PostCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_PostCode
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.Email",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_Email
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.Fax",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_Fax
                    }, new ColumnItem {
                        Name = "KeyAccount.ContactPerson",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_ContactPerson
                    }, new ColumnItem {
                        Name = "KeyAccount.ContactPhone",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_ContactPhone
                    }, new ColumnItem {
                        Name = "KeyAccount.ContactCellPhone",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_ContactCellPhone
                    }, new ColumnItem {
                        Name = "KeyAccount.ContactJobPosition",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_ContactJobPosition
                    }, new ColumnItem {
                        Name = "KeyAccount.ContactFax",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_ContactFax
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "KeyAccount.CurrentPurchaseScale",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_CurrentPurchaseScale
                    }, new ColumnItem {
                        Name = "KeyAccount.PurchaseMethod",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_PurchaseMethod
                    }, new KeyValuesColumnItem {
                        Name = "KeyAccount.PurchasePurpose",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]],
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_PurchasePurpose
                    }, new ColumnItem {
                        Name = "KeyAccount.ItemOtherDescription",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_ItemOtherDescription
                    }, new ColumnItem {
                        Name = "KeyAccount.IfTenderingOrCentralPurchase",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_IfTenderingOrCentralPurchase
                    }, new KeyValuesColumnItem{
                        Name = "EmployeePurchaseDocType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]],
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_EmployeePurchaseDocType
                    }, new ColumnItem{
                        Name = "DealerKeyAccountResponsible"
                    }, new ColumnItem{
                        Name = "DealerContactPhone"
                    }, new ColumnItem{
                        Name = "IfSpecialConfig"
                    }, new ColumnItem{
                        Name = "EmployeePurchaseDocOtherType",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_EmployeePurchaseDocOtherType
                    }, new ColumnItem{
                        Name = "SpecialConfigRequirement"
                    }, new ColumnItem{
                        Name = "ApprovalComment"
                    }
                };
            }
        }
        protected override string OnRequestQueryName() {
            return "查询批售审批单";
        }

        protected override Type EntityType {
            get {
                return typeof(WholesaleApproval);
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters == null)
                return null;

            var createTimeFilters = filters.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;

            switch(parameterName) {
                case "customerCode":
                    var code = filters.Filters.FirstOrDefault(item => item.MemberName == "KeyAccount.Customer.Code");
                    if(code == null)
                        return null;
                    return code.Value as string;
                case "customerName":
                    var customerName = filters.Filters.FirstOrDefault(item => item.MemberName == "KeyAccount.Customer.Name");
                    if(customerName == null)
                        return null;
                    return customerName.Value as string;
                case "begincreateTime":
                    return createTimeFilters.Filters.ElementAt(0).Value;
                case "finishcreateTime":
                    return createTimeFilters.Filters.ElementAt(1).Value;
                case "status":
                    return filters.Filters.Single(item => item.MemberName == "Status").Value;
                case "keyAccountCode":
                    return filters.Filters.Single(item => item.MemberName == "KeyAccount.KeyAccountCode").Value as string;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                var newCompositeFilterItem = new CompositeFilterItem();
                var compositeFilters = compositeFilterItem.Filters.Where(item => item.GetType() == typeof(CompositeFilterItem) && item.MemberName != "KeyAccount.Customer.Code" && item.MemberName != "KeyAccount.Customer.Name" && item.MemberName != "KeyAccount.KeyAccountCode" && item.MemberName != "Status" && item.MemberName != "CreateTime");
                if(compositeFilters.Any()) {
                    foreach(var item in compositeFilters)
                        newCompositeFilterItem.Filters.Add(item);
                    return newCompositeFilterItem.ToFilterDescriptor();
                }
            }
            return this.FilterItem.ToFilterDescriptor();
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                      "WholesaleApprovalDetail"  
                    }
                };
            }
        }

        public WholesaleApprovalDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}