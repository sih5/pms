﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class WholesaleApprovalDetailDataGridView : DcsDataGridViewBase {
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem{
                        Name="ProductCategoryCode"
                    }, new KeyValuesColumnItem{
                        Name="Color"
                    }, new ColumnItem{
                        Name="Quantity",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name="SupplyDate"
                    }, new ColumnItem{
                        Name="IfAgentDeliverDealerNeeded"
                    }, new ColumnItem{
                        Name="RetailGuidePrice"
                    }, new ColumnItem{
                        Name="WholesalePrice"
                    }, new ColumnItem{
                        Name="DiscountAmount"
                    }, new ColumnItem{
                        Name="AgentDeliveryDealerName"
                    }, new ColumnItem{
                        Name="AgentDeliveryDealerContact"
                    }, new ColumnItem{
                        Name="DeliveryAgentPhoneNumber"
                    }, new ColumnItem{
                        Name = "AgentVehicleDeliverAppDate"
                    }, new ColumnItem{
                        Name = "AgentVehicleDeliverAppCity"
                    }, new ColumnItem{
                        Name = "StandardWholesaleRebate",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApprovalDetail_StandardWholesaleRebate
                    }, new ColumnItem{
                        Name = "SpecialWholesaleRebate",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApprovalDetail_SpecialWholesaleRebate
                    }, new ColumnItem{
                        Name = "OtherWholesaleReward"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WholesaleApprovalDetail);
            }
        }

        private void WholesaleApprovalDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var wholesaleApproval = e.NewValue as WholesaleApproval;
            if(wholesaleApproval == null || wholesaleApproval.Id == default(int))
                return;
            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "WholesaleApprovalId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo
                };
            this.FilterItem.Value = wholesaleApproval.Id;
            this.ExecuteQueryDelayed();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            this.GridView.ShowGroupPanel = true;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.SelectionUnit = Telerik.Windows.Controls.GridView.GridViewSelectionUnit.Cell;
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WholesalePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SupplyDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["StandardWholesaleRebate"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SpecialWholesaleRebate"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OtherWholesaleReward"]).DataFormatString = "c2";
        }

        protected override string OnRequestQueryName() {
            return "GetWholesaleApprovalDetails";
        }

        public WholesaleApprovalDetailDataGridView() {
            this.DataContextChanged += WholesaleApprovalDetailDataGridView_DataContextChanged;
        }
    }
}
