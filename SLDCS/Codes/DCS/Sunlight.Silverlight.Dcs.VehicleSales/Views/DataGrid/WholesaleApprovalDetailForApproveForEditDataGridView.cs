﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class WholesaleApprovalDetailForApproveForEditDataGridView : WholesaleApprovalDetailDataGridView {
    
        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { new ColumnItem{
                                Name = "ProductCategoryCode",
                                IsReadOnly = true
                            }, new KeyValuesColumnItem{
                                Name = "Color",
                                IsReadOnly=true
                            }, new ColumnItem{
                                Name="Quantity",
                                TextAlignment = TextAlignment.Right,
                                IsReadOnly=true
                            }, new ColumnItem{
                                Name="SupplyDate",
                                IsReadOnly=true
                            }, new ColumnItem{
                                Name="IfAgentDeliverDealerNeeded",
                                IsReadOnly = true
                            }, new ColumnItem{
                                Name="RetailGuidePrice",
                                IsReadOnly = true
                            }, new ColumnItem{
                                Name = "WholesalePrice",
                                IsReadOnly = true
                            }, new ColumnItem{
                                Name = "DiscountAmount",
                                IsReadOnly = true
                            }, new ColumnItem{
                                Name = "DiscountRate",
                                IsReadOnly = true,
                            }, new ColumnItem{
                                Name = "AgentDeliveryDealerName",
                                IsReadOnly = true
                            }, new ColumnItem{
                                Name = "AgentDeliveryDealerContact",
                                IsReadOnly = true
                            }, new ColumnItem{
                                Name="DeliveryAgentPhoneNumber",
                                IsReadOnly = true
                            }, new ColumnItem{
                                Name = "AgentVehicleDeliverAppDate",
                                IsReadOnly = true
                            }, new ColumnItem{
                                Name = "AgentVehicleDeliverAppCity",
                                IsReadOnly=true
                            }, new ColumnItem{
                                Name = "StandardWholesaleRebate",
                                Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApprovalDetail_StandardWholesaleRebate,
                                IsReadOnly = true
                            }, new ColumnItem{
                                Name = "SpecialWholesaleRebate",
                                Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApprovalDetail_SpecialWholesaleRebate,
                                IsReadOnly = true
                            }, new ColumnItem{
                                Name = "OtherWholesaleReward",
                                IsReadOnly = true
                            }
                        };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WholesaleApprovalDetail);
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        private void WholesaleApprovalDetailForApproveForEditDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            if(this.GridView == null)
                return;
            var wholesaleApproval = e.NewValue as WholesaleApproval;
            if(wholesaleApproval == null || wholesaleApproval.WholesaleApprovalDetails == null)
                return;
            if(wholesaleApproval.IsSpecialOperation) {
                this.GridView.IsReadOnly = false;
                var detail = wholesaleApproval.WholesaleApprovalDetails.FirstOrDefault();
                if(detail.StandardWholesaleRebate == default(decimal) || detail.StandardWholesaleRebate == null) {
                    ((GridViewDataColumn)this.GridView.Columns["StandardWholesaleRebate"]).IsReadOnly = false;
                    ((GridViewDataColumn)this.GridView.Columns["SpecialWholesaleRebate"]).IsReadOnly = true;
                    ((GridViewDataColumn)this.GridView.Columns["OtherWholesaleReward"]).IsReadOnly = false;
                } else {
                    ((GridViewDataColumn)this.GridView.Columns["StandardWholesaleRebate"]).IsReadOnly = true;
                    ((GridViewDataColumn)this.GridView.Columns["SpecialWholesaleRebate"]).IsReadOnly = false;
                    ((GridViewDataColumn)this.GridView.Columns["OtherWholesaleReward"]).IsReadOnly = false;
                }
            } else {
                 ((GridViewDataColumn)this.GridView.Columns["StandardWholesaleRebate"]).IsReadOnly = true;
                 ((GridViewDataColumn)this.GridView.Columns["SpecialWholesaleRebate"]).IsReadOnly = true;
                 ((GridViewDataColumn)this.GridView.Columns["OtherWholesaleReward"]).IsReadOnly = true;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("WholesaleApprovalDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = System.Windows.Controls.SelectionMode.Single;
            this.GridView.ShowGroupPanel = false;

            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WholesalePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountRate"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["StandardWholesaleRebate"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SpecialWholesaleRebate"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["OtherWholesaleReward"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SupplyDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["AgentVehicleDeliverAppDate"]).DataFormatString = "d";
        }

        public WholesaleApprovalDetailForApproveForEditDataGridView() {
            this.DataContextChanged += WholesaleApprovalDetailForApproveForEditDataGridView_DataContextChanged;
        }
    }

}
