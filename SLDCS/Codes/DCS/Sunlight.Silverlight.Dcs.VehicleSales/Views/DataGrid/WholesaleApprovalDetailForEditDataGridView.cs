﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class WholesaleApprovalDetailForEditDataGridView : DcsDataGridViewBase {

        private QueryWindowBase wholesaleApprovalDetailDropDownQueryWindow;
        private QueryWindowBase WholesaleApprovalDetailDownQueryWindow {
            get {
                if(wholesaleApprovalDetailDropDownQueryWindow == null) {
                    wholesaleApprovalDetailDropDownQueryWindow = DI.GetQueryWindow("VehicleModelAffiProductForSelectDropDown");
                    wholesaleApprovalDetailDropDownQueryWindow.SelectionDecided += this.VehicleModelAffiProductDropDownQueryWindow_SelectionDecided;
                }
                return this.wholesaleApprovalDetailDropDownQueryWindow;
            }
        }


        private void VehicleModelAffiProductDropDownQueryWindow_SelectionDecided(object sender, EventArgs e) {
            var dropDownQueryWindowBase = sender as DcsDropDownQueryWindowBase;
            if(dropDownQueryWindowBase == null || dropDownQueryWindowBase.SelectedEntities == null)
                return;
            var wholesaleApprovalDetail = dropDownQueryWindowBase.DataContext as WholesaleApprovalDetail;
            var wholesaleApproval = this.DataContext as WholesaleApproval;
            if(wholesaleApproval == null)
                return;
            if(wholesaleApprovalDetail == null)
                return;
            var vehicleModelAffiProduct = dropDownQueryWindowBase.SelectedEntities.Cast<VehModelRetailSuggestedPrice>().FirstOrDefault();
            if(vehicleModelAffiProduct == null)
                return;
            wholesaleApprovalDetail.WholesaleApprovalId = wholesaleApproval.Id;
            wholesaleApprovalDetail.ProductCategoryCode = vehicleModelAffiProduct.ProductCategoryCode;
            wholesaleApprovalDetail.ProductCategoryName = vehicleModelAffiProduct.ProductCategoryName;
            //if(vehicleModelAffiProduct.Color != null)
            //    wholesaleApprovalDetail.Color = (int)vehicleModelAffiProduct.Color;
            wholesaleApprovalDetail.Quantity = 1;
            wholesaleApprovalDetail.RetailGuidePrice = vehicleModelAffiProduct.Price;
            wholesaleApprovalDetail.SupplyDate = DateTime.Now;
            wholesaleApprovalDetail.WholesalePrice = vehicleModelAffiProduct.Price;
            wholesaleApprovalDetail.DiscountAmount = wholesaleApprovalDetail.RetailGuidePrice - wholesaleApprovalDetail.WholesalePrice;
            wholesaleApprovalDetail.DiscountRate = wholesaleApprovalDetail.RetailGuidePrice == 0 ? 0 : Convert.ToDouble(wholesaleApprovalDetail.DiscountAmount / wholesaleApprovalDetail.RetailGuidePrice);

            var parent = dropDownQueryWindowBase.ParentOfType<RadWindow>();
            if(parent != null)
                parent.Close();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new DropDownTextBoxColumnItem{
                        Name="ProductCategoryCode",
                        DropDownContent=this.WholesaleApprovalDetailDownQueryWindow
                    }, 
                    new ColumnItem{
                        Name="Color"
                    }, new ColumnItem{
                        Name="Quantity",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem{
                        Name="SupplyDate"
                    }, new ColumnItem{
                        Name="IfAgentDeliverDealerNeeded"
                    }, new ColumnItem{
                        Name="RetailGuidePrice",
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name="WholesalePrice"
                    }, new ColumnItem{
                        Name="DiscountAmount",
                        IsReadOnly=true
                    }, new ColumnItem{
                        Name="DiscountRate",
                        IsReadOnly=true,
                    }, new ColumnItem{
                        Name="AgentDeliveryDealerName"
                    }, new ColumnItem{
                        Name="AgentDeliveryDealerContact"
                    }, new ColumnItem{
                        Name="DeliveryAgentPhoneNumber"
                    }, new ColumnItem{
                        Name = "AgentVehicleDeliverAppDate"
                    }, new ColumnItem{
                        Name = "AgentVehicleDeliverAppCity"
                    }, new ColumnItem{
                        Name = "StandardWholesaleRebate",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApprovalDetail_StandardWholesaleRebate
                    }, new ColumnItem{
                        Name = "SpecialWholesaleRebate",
                        IsReadOnly = true,
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApprovalDetail_SpecialWholesaleRebate
                    }, new ColumnItem{
                        Name = "OtherWholesaleReward",
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WholesaleApprovalDetail);
            }
        }

        private void GridView_CellValidating(object sender, GridViewCellValidatingEventArgs e) {
            var wholesaleApprovalDetail = e.Cell.DataContext as WholesaleApprovalDetail;
            if(wholesaleApprovalDetail == null)
                return;

            switch(e.Cell.Column.UniqueName) {
                case "Quantity":
                    var quantityError = wholesaleApprovalDetail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("Quantity"));
                    if(quantityError != null)
                        wholesaleApprovalDetail.ValidationErrors.Remove(quantityError);
                    if(!(e.NewValue is int) || (int)e.NewValue <= 0) {
                        e.IsValid = false;
                        e.ErrorMessage = string.Format(VehicleSalesUIStrings.DataGridView_Validation_WholesaleApprovalDetail_QuantityIsMustGreatThanZero, wholesaleApprovalDetail.Quantity);
                    }
                    break;
                case "ProductCode":
                    var productCodeError = wholesaleApprovalDetail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("ProductCode"));
                    if(productCodeError != null)
                        wholesaleApprovalDetail.ValidationErrors.Remove(productCodeError);
                    if(!(e.NewValue is string) || (string)e.NewValue == null) {
                        e.IsValid = false;
                        e.ErrorMessage = VehicleSalesUIStrings.DataGridView_Validation_WholesaleApprovalDetail_ProductCodeIsNull;
                    }
                    break;
                case "SupplyDate":
                    var supplyDateError = wholesaleApprovalDetail.ValidationErrors.FirstOrDefault(r => r.MemberNames.Contains("SupplyDate"));
                    if(supplyDateError != null)
                        wholesaleApprovalDetail.ValidationErrors.Remove(supplyDateError);
                    var supplyData = Convert.ToDateTime(e.NewValue);
                    if(supplyData == default(DateTime)) {
                        e.IsValid = false;
                        e.ErrorMessage = VehicleSalesUIStrings.DataGridView_Validation_WholesaleApprovalDetail_SupplyDateIsNull;
                    }
                    break;
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.CellValidating += this.GridView_CellValidating;
            this.GridView.BeginningEdit += this.GridView_BeginningEdit;
            this.GridView.CellEditEnded += this.GridView_CellEditEnded;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            ((GridViewDataColumn)this.GridView.Columns["RetailGuidePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["WholesalePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountRate"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["SupplyDate"]).DataFormatString = "d";
            ((GridViewDataColumn)this.GridView.Columns["WholesalePrice"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountAmount"]).DataFormatString = "c2";
            ((GridViewDataColumn)this.GridView.Columns["DiscountRate"]).DataFormatString = "c2";
        }

        private void GridView_CellEditEnded(object sender, GridViewCellEditEndedEventArgs e) {
            var currentData = e.Cell.DataContext as WholesaleApprovalDetail;
            if(currentData == null)
                return;
            if(!currentData.IfAgentDeliverDealerNeeded) {
                currentData.Color = null;
                currentData.AgentDeliveryDealerContact = null;
                currentData.AgentDeliveryDealerName = null;
                currentData.AgentVehicleDeliverAppCity = null;
                currentData.AgentVehicleDeliverAppDate = null;
                currentData.DeliveryAgentPhoneNumber = null;
            }
        }

        private void GridView_BeginningEdit(object sender, GridViewBeginningEditRoutedEventArgs e) {
            var wholesaleApproval = this.DataContext as WholesaleApproval;
            if(wholesaleApproval == null)
                return;
            var currentData = e.Row.DataContext as WholesaleApprovalDetail;
            if(currentData == null)
                return;
            var columnName = e.Cell.Column.UniqueName;
            switch(columnName) {
                case "Color":
                case "AgentDeliveryDealerName":
                case "AgentDeliveryDealerContact":
                case "DeliveryAgentPhoneNumber":
                case "AgentVehicleDeliverAppDate":
                case "AgentVehicleDeliverAppCity":
                    e.Cancel = !currentData.IfAgentDeliverDealerNeeded;
                    break;
                default:
                    break;
            }
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("WholesaleApprovalDetails");
        }
    }
}
