﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class WholesaleApprovalForApproveDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = { 
            "MultiLevelAuditConfig_InitialStatus","KeyAccount_CustomerProperty","KeyAccount_PurchasePurpose","WholesaleApproval_EmployeePurchaseDocType"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "DealerName"
                    }, new ColumnItem{
                        Name = "KeyAccount.KeyAccountCode"
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.Code",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_CustomerCode
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.Name",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_CustomerName
                    }, new KeyValuesColumnItem {
                        Name = "KeyAccount.CustomerProperty",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]
                    }, new ColumnItem {
                        Name = "PostAuditStatu.Name",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_PostAuditStatusName
                    } , new ColumnItem {
                        Name = "KeyAccount.Customer.Address"
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.PostCode"
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.Email",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_Email
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.Fax"
                    }, new ColumnItem {
                        Name = "KeyAccount.ContactPerson"
                    }, new ColumnItem {
                        Name = "KeyAccount.ContactPhone"
                    }, new ColumnItem {
                        Name = "KeyAccount.ContactCellPhone"
                    }, new ColumnItem {
                        Name = "KeyAccount.ContactJobPosition"
                    }, new ColumnItem {
                        Name = "KeyAccount.ContactFax"
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]]
                    }, new ColumnItem {
                        Name = "KeyAccount.CustomerSummary"
                    }, new ColumnItem {
                        Name = "KeyAccount.CurrentPurchaseScale"
                    }, new ColumnItem {
                        Name = "KeyAccount.PurchaseMethod"
                    }, new KeyValuesColumnItem {
                        Name = "KeyAccount.PurchasePurpose",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]] 
                    }, new ColumnItem {
                        Name = "KeyAccount.ItemOtherDescription"
                    }, new ColumnItem {
                        Name = "KeyAccount.IfTenderingOrCentralPurchase"
                    }, new KeyValuesColumnItem {
                        Name = "EmployeePurchaseDocType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[3]]
                    }, new ColumnItem {
                        Name = "DealerKeyAccountResponsible"
                    }, new ColumnItem {
                        Name = "DealerContactPhone"
                    }, new ColumnItem {
                        Name = "IfSpecialConfig"
                    }, new ColumnItem {
                        Name = "EmployeePurchaseDocOtherType"
                    }, new ColumnItem {
                        Name = "SpecialConfigRequirement"
                    }, new ColumnItem {
                        Name = "ApprovalComment"
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WholesaleApproval);
            }
        }

        protected override string OnRequestQueryName() {
            return "根据多级审核状态查询批售审批单";
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                        "WholesaleApprovalDetail"
                   }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters == null)
                return null;
            switch(parameterName) {
                case "id":
                    return BaseApp.Current.CurrentUserData.UserId;
            }
            return null;
        }

        public WholesaleApprovalForApproveDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }
    }
}
