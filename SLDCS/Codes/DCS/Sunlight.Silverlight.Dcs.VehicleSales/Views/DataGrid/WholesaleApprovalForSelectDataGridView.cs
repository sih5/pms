﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class WholesaleApprovalForSelectDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = {
            "Customer_CustomerType"
        };

        public WholesaleApprovalForSelectDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "KeyAccount.KeyAccountCode"
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.Code",
                        Title =  VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_CustomerCode
                    }, new ColumnItem {
                        Name = "KeyAccount.Customer.Name",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_CustomerName
                    }, new KeyValuesColumnItem {
                        Name = "KeyAccount.Customer.CustomerType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                    }
                };
            }
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filters = this.FilterItem as CompositeFilterItem;
            if(filters == null)
                return null;
            var createTimeFilters = filters.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
            switch(parameterName) {
                case "name":
                    var productName = filters.Filters.SingleOrDefault(e => e.MemberName == "KeyAccount.Customer.Name");
                    return productName != null ? productName.Value : null;
                case "begincreateTime":
                    if(createTimeFilters == null)
                        return null;
                    return createTimeFilters.Filters.ElementAt(0).Value;
                case "finishcreateTime":
                    if(createTimeFilters == null)
                        return null;
                    return createTimeFilters.Filters.ElementAt(1).Value;
                case "keyAccountCode":
                    var productCategoryName = filters.Filters.SingleOrDefault(e => e.MemberName == "KeyAccount.KeyAccountCode");
                    return productCategoryName != null ? productCategoryName.Value : null;
                case "status":
                    var status = filters.Filters.SingleOrDefault(e => e.MemberName == "Status");
                    return status != null ? status.Value : null;
                case "dealerName":
                    var dealerName = filters.Filters.SingleOrDefault(e => e.MemberName == "DealerName");
                    return dealerName != null ? dealerName.Value : null;
            }
            return null;
        }

        protected override Type EntityType {
            get {
                return typeof(WholesaleApproval);
            }
        }

        protected override string OnRequestQueryName() {
            return "查询批售审批单";
        }
    }
}
