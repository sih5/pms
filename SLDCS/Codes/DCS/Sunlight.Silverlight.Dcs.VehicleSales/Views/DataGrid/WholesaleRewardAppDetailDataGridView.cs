﻿using System;
using System.Collections.Generic;
using System.Windows;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class WholesaleRewardAppDetailDataGridView : DcsDataGridViewBase {
        public WholesaleRewardAppDetailDataGridView() {
            this.DataContextChanged += this.WholesaleRewardAppDetailDataGridView_DataContextChanged;
        }

        private void WholesaleRewardAppDetailDataGridView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
            var wholesaleRewardApp = e.NewValue as WholesaleRewardApp;
            if(wholesaleRewardApp == null || wholesaleRewardApp.Id == default(int))
                return;

            if(this.FilterItem == null)
                this.FilterItem = new FilterItem {
                    MemberName = "WholesaleRewardAppId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                };
            this.FilterItem.Value = wholesaleRewardApp.Id;
            this.ExecuteQueryDelayed();
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "InvoiceTitle"
                    }, new ColumnItem {
                        Name = "VIN"
                    }, new ColumnItem {
                        Name = "InvoiceCode"
                    }, new ColumnItem {
                        Name = "ProductCategoryCode"
                    }, new ColumnItem {
                        Name = "MarketGuidePrice",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "TransactionPrice",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "VehicleDeliveryTime"
                    }, new ColumnItem {
                        Name = "AppliedRewardAmount",
                        TextAlignment = TextAlignment.Right,
                    }, new ColumnItem {
                        Name = "ApprovedRewardAmount",
                        TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WholesaleRewardAppDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = true;
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["MarketGuidePrice"]).DataFormatString = "c2";
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["TransactionPrice"]).DataFormatString = "c2";
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["AppliedRewardAmount"]).DataFormatString = "c2";
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["ApprovedRewardAmount"]).DataFormatString = "c2";
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["VehicleDeliveryTime"]).DataFormatString = "d";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override string OnRequestQueryName() {
            return "GetWholesaleRewardAppDetails";
        }
    }
}
