﻿using System;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class WholesaleRewardAppDetailForApproveDataGridView : DcsDataGridViewBase {
        protected override System.Collections.Generic.IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] { 
                    new ColumnItem {
                        Name = "InvoiceTitle",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "VIN",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "InvoiceCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ProductCategoryCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MarketGuidePrice",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "TransactionPrice",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "VehicleDeliveryTime",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "AppliedRewardAmount",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "ApprovedRewardAmount",
                        TextAlignment = TextAlignment.Right,
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WholesaleRewardAppDetail);
            }
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
            this.GridView.SelectionMode = SelectionMode.Single;
            this.GridView.CanUserDeleteRows = true;
            this.GridView.CanUserInsertRows = true;
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["MarketGuidePrice"]).DataFormatString = "c2";
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["TransactionPrice"]).DataFormatString = "c2";
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["AppliedRewardAmount"]).DataFormatString = "c2";
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["ApprovedRewardAmount"]).DataFormatString = "c2";
            ((Telerik.Windows.Controls.GridViewDataColumn)this.GridView.Columns["VehicleDeliveryTime"]).DataFormatString = "d";
        }

        protected override bool ShowCheckBox {
            get {
                return false;
            }
        }

        protected override bool UsePaging {
            get {
                return false;
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("WholesaleRewardAppDetails");
        }
    }
}
