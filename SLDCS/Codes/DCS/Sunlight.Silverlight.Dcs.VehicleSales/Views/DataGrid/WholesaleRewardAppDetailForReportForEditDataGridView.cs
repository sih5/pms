﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class WholesaleRewardAppDetailForReportForEditDataGridView : WholesaleRewardAppDetailDataGridView {

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Name = "InvoiceTitle"
                    }, new ColumnItem {
                        Name = "VIN",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "InvoiceCode"
                    }, new ColumnItem {
                        Name = "ProductCategoryCode",
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "MarketGuidePrice",
                        IsReadOnly = true,
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "TransactionPrice",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }, new ColumnItem {
                        Name = "VehicleDeliveryTime"
                    }, new ColumnItem {
                        Name = "AppliedRewardAmount",
                        TextAlignment = TextAlignment.Right
                    }, new ColumnItem {
                        Name = "ApprovedRewardAmount",
                        TextAlignment = TextAlignment.Right,
                        IsReadOnly = true
                    }
                };
            }
        }

        protected override Binding OnRequestDataSourceBinding() {
            return new Binding("WholesaleRewardAppDetails");
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.IsReadOnly = false;
            this.GridView.ShowGroupPanel = false;
        }
    }
}
