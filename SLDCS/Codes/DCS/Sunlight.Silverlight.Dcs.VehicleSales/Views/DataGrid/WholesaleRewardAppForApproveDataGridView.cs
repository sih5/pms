﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class WholesaleRewardAppForApproveDataGridView : DcsDataGridViewBase {
        //TODO: 客户性质，尚未翻译
        private readonly string[] KvNames = new[] {
            "WholesaleRewardApp_CustomerDetailCategory","WholesaleRewardApp_Status"
        };

        public WholesaleRewardAppForApproveDataGridView() {
            this.KeyValueManager.Register(this.KvNames);
        }

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new[] {
                    new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_Code,
                        Name = "Code"
                    }, new ColumnItem {
                        Name = "WholesaleApprovalCode"
                    }, new ColumnItem {
                        Name = "DealerCode"
                    }, new ColumnItem {
                        Name = "DealerName"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_KeyAccount_KeyAccountCode,
                        Name = "KeyAccount.KeyAccountCode",
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_Code,
                        Name = "KeyAccount.Customer.Code",
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_Name,
                        Name = "KeyAccount.Customer.Name",
                    }, 
                    //TODO: 客户性质，尚未翻译
                    //new KeyValuesColumnItem {
                    //    Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_KeyAccount_CustomerProperty,
                    //    Name = "KeyAccount.CustomerProperty",
                    //    KeyValueItems = this.KeyValueManager[this.KvNames[2]]
                    //}, 
                    new KeyValuesColumnItem {
                        Name = "CustomerDetailCategory",
                        KeyValueItems = this.KeyValueManager[this.KvNames[0]]
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_KeyAccount_IfTenderingOrCentralPurchase,
                        Name = "KeyAccount.IfTenderingOrCentralPurchase"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_Address,
                        Name = "KeyAccount.Customer.Address"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_PostCode,
                        Name = "KeyAccount.Customer.PostCode"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_Email,
                        Name = "KeyAccount.Customer.Email"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_Customer_Fax,
                        Name = "KeyAccount.Customer.Fax",
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_KeyAccount_ContactPerson,
                        Name = "KeyAccount.ContactPerson",
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_KeyAccount_ContactPhone,
                        Name = "KeyAccount.ContactPhone",
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_KeyAccount_ContactCellPhone,
                        Name = "KeyAccount.ContactCellPhone"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_KeyAccount_ContactJobPosition,
                        Name = "KeyAccount.ContactJobPosition"
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_KeyAccount_ContactFax,
                        Name = "KeyAccount.ContactFax",
                    }, new KeyValuesColumnItem {
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.KvNames[1]]
                    }, new ColumnItem {
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleApproval_DealerKeyAccountResponsible,
                        Name = "WholesaleApproval.DealerKeyAccountResponsible",
                    } 
                };
            }
        }

        protected override string OnRequestQueryName() {
            return "GetWholesaleRewardAppWithKeyAccountAndCustomer";
        }

        protected override System.Type EntityType {
            get {
                return typeof(WholesaleRewardApp);
            }
        }

        protected override Core.IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[] {
                      "WholesaleRewardAppDetail"  
                    }
                };
            }
        }
    }
}
