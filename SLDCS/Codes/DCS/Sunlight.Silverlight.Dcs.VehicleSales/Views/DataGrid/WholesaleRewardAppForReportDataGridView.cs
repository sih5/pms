﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.DataGrid {
    public class WholesaleRewardAppForReportDataGridView : DcsDataGridViewBase {
        private readonly string[] kvNames = new[] { 
            "Customer_CustomerType", "WholesaleRewardApp_CustomerDetailCategory", "WholesaleRewardApp_Status"
        };

        protected override IEnumerable<ColumnItem> ColumnItems {
            get {
                return new ColumnItem[] { 
                    new ColumnItem{
                        Name = "Code",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_Code
                    }, new ColumnItem{
                        Name = "WholesaleApprovalCode"
                    }, new ColumnItem{
                        Name = "KeyAccount.KeyAccountCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_KeyAccountCode
                    }, new ColumnItem{
                        Name = "KeyAccount.Customer.Code",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_CustomerCode
                    }, new ColumnItem{
                        Name = "KeyAccount.Customer.Name",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_CustomerName
                    }, new KeyValuesColumnItem{
                        Name = "KeyAccount.Customer.CustomerType",
                        KeyValueItems = this.KeyValueManager[this.kvNames[0]],
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_CustomerType
                    }, new KeyValuesColumnItem{
                        Name = "CustomerDetailCategory",
                        KeyValueItems = this.KeyValueManager[this.kvNames[1]]//TODO:加EntityString
                    }, new ColumnItem{
                        Name = "KeyAccount.IfTenderingOrCentralPurchase",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_IfTenderingOrCentralPurchase
                    }, new ColumnItem{
                        Name = "KeyAccount.Customer.Address",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_CustomerAddress
                    }, new ColumnItem{
                        Name = "KeyAccount.Customer.PostCode",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_CustomerPostCode
                    }, new ColumnItem{
                        Name = "KeyAccount.Customer.Email",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_CustomerEMail
                    }, new ColumnItem{
                        Name = "KeyAccount.Customer.Fax",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_CustomerFax
                    }, new ColumnItem{
                        Name = "KeyAccount.ContactPerson",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_KeyAccountContactPerson
                    }, new ColumnItem{
                        Name = "KeyAccount.ContactPhone",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_KeyAccountContactPhone
                    }, new ColumnItem{
                        Name = "KeyAccount.ContactCellPhone",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_KeyAccountContactCellPhone
                    }, new ColumnItem{
                        Name = "KeyAccount.ContactJobPosition",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_KeyAccountContactJobPosition
                    }, new ColumnItem{
                        Name = "KeyAccount.ContactFax",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_KeyAccountContactFax
                    }, new KeyValuesColumnItem{
                        Name = "Status",
                        KeyValueItems = this.KeyValueManager[this.kvNames[2]]
                    }, new ColumnItem{
                        Name = "WholesaleApproval.DealerKeyAccountResponsible",
                        Title = VehicleSalesUIStrings.DataGridView_ColumnItem_Title_WholesaleRewardApp_DealerKeyAccountResponsible
                    }
                };
            }
        }

        protected override Type EntityType {
            get {
                return typeof(WholesaleRewardApp);
            }
        }

        protected override IRowDetailsTemplateProvider RowDetailsTemplateProvider {
            get {
                return new TabControlRowDetailsTemplateProvider {
                    DetailPanelNames = new[]{
                        "WholesaleRewardAppDetail"
                    }
                };
            }
        }

        public WholesaleRewardAppForReportDataGridView() {
            this.KeyValueManager.Register(this.kvNames);
        }

        protected override string OnRequestQueryName() {
            return "查询批售奖励申请单";
        }

        protected override object OnRequestQueryParameter(string queryName, string parameterName) {
            var filter = this.FilterItem as CompositeFilterItem;
            if(filter == null)
                return null;
            switch(parameterName) {
                case "keyAccountCode":
                    var keyAccountCode = filter.Filters.FirstOrDefault(item => item.MemberName == "KeyAccountCode");
                    if(keyAccountCode == null)
                        return null;
                    return keyAccountCode.Value as string;
                case "customerName":
                    var customerName = filter.Filters.FirstOrDefault(item => item.MemberName == "CustomerName");
                    if(customerName == null)
                        return null;
                    return customerName.Value as string;
                default:
                    return null;
            }
        }

        protected override IFilterDescriptor OnRequestFilterDescriptor(string queryName) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            var newCompositeFilterItem = new CompositeFilterItem();
            if(compositeFilterItem != null) {
                var filters = compositeFilterItem.Filters;
                foreach(var item in filters.Where(filter => filter.MemberName != "KeyAccountCode" && filter.MemberName != "CustomerName"))
                    newCompositeFilterItem.Filters.Add(item);
            }
            return newCompositeFilterItem.ToFilterDescriptor();
        }

        protected override void OnControlsCreated() {
            base.OnControlsCreated();
            this.GridView.SelectionUnit = GridViewSelectionUnit.FullRow;
        }
    }
}
