﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesOrder", "DealerTransactionBillConfirm", ActionPanelKeys = new[] {
        "DealerTransactionBill",CommonActionKeys.EXPORT
    })]
    public class DealerTransactionBillForConfirmManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public DealerTransactionBillForConfirmManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_DealerTransactionBillForConfirm;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("DealerTransactionBill"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "DealerTransactionBill"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case "OTDConfirm":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Confirm, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<DealerTransactionBill>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can确认经销商买卖单)
                                entity.确认经销商买卖单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(VehicleSalesUIStrings.DataManagementView_Notification_ConfirmSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "OTDConfirm":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<DealerTransactionBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsDealerTransactionBillStatus.新建;
                default:
                    return false;
            }
        }
    }
}
