﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.Print {
    public class BasePrintWindow : RadWindow, INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName) {
            var handler = this.PropertyChanged;
            if(handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public BasePrintWindow() {
            this.SetBinding(DataContextProperty, new Binding {
                RelativeSource = new RelativeSource(RelativeSourceMode.Self)
            });
            this.FontSize = 12;
            BasePrintWindow_Loaded();
            this.Closed += (sender, args) => ViewModel.ShellViewModel.Current.IsRibbonMinimized = false;
        }

        private void BasePrintWindow_Loaded() {
            var browserProperties = Application.Current.Host;
            var height = browserProperties.Content.ActualHeight;
            var width = browserProperties.Content.ActualWidth;
            this.Width = 920;
            this.Height = height;
            this.Left = (width / 2) - 455;
        }
    }
}