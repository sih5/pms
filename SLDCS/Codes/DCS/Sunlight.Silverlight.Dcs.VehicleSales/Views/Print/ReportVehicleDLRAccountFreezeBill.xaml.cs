﻿using System;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.Print {
    public partial class ReportVehicleDLRAccountFreezeBill {
        private void GetParameterValues(RenderBeginEventArgs args) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            foreach(var filter in compositeFilterItem.Filters.Where(filter => filter.Value != null || filter is CompositeFilterItem)) {
                if(filter is CompositeFilterItem) {
                    var dateTimeFilterItem = filter as CompositeFilterItem;
                    if(dateTimeFilterItem.Filters.Any(r => r.MemberName.Equals("CreateTime"))) {
                        args.ParameterValues["paraStartDateTime"] = (DateTime?)(dateTimeFilterItem.Filters.ElementAt(0).Value);
                        args.ParameterValues["ParaEndDateTime"] = (DateTime?)(dateTimeFilterItem.Filters.ElementAt(1).Value);
                    } else {
                        args.ParameterValues["paraStartApproveTime"] = (DateTime?)(dateTimeFilterItem.Filters.ElementAt(0).Value);
                        args.ParameterValues["ParaEndApproveTime"] = (DateTime?)(dateTimeFilterItem.Filters.ElementAt(1).Value);
                    }
                    continue;
                }
                switch(filter.MemberName) {
                    case "Code":
                        args.ParameterValues["paraCode"] = filter.Value.ToString();
                        break;
                    case "CustomerCompanyName":
                        args.ParameterValues["ParaCustomerCompanyName"] = filter.Value.ToString();
                        break;
                    case "VehicleFundsTypeId":
                        args.ParameterValues["paraVehicleFundsTypeId"] = (int?)filter.Value;
                        break;
                    case "Status":
                        args.ParameterValues["ParaStatus"] = (int?)filter.Value;
                        break;
                }
            }
        }

        private void ReportViewer_RenderBegin(object sender, RenderBeginEventArgs args) {
            this.GetParameterValues(args);
        }

        public FilterItem FilterItem {
            private get;
            set;
        }

        public ReportVehicleDLRAccountFreezeBill() {
            InitializeComponent();
        }
    }
}
