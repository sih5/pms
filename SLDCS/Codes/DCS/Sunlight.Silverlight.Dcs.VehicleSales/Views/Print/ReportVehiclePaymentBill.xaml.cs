﻿using System;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Telerik.ReportViewer.Silverlight;
namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.Print {
    public partial class ReportVehiclePaymentBill {
        private void GetParameterValues(RenderBeginEventArgs args) {
            var compositeFilterItem = this.FilterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            foreach(var filter in compositeFilterItem.Filters.Where(filter => filter.Value != null || filter is CompositeFilterItem)) {
                if(filter is CompositeFilterItem) {
                    var dateTimeFilterItem = filter as CompositeFilterItem;
                    if(dateTimeFilterItem.Filters.Any(r => r.MemberName.Equals("CreateTime"))) {
                        args.ParameterValues["parastartDateTime"] = (DateTime?)(dateTimeFilterItem.Filters.ElementAt(0).Value);
                        args.ParameterValues["paraendDateTime"] = (DateTime?)(dateTimeFilterItem.Filters.ElementAt(1).Value);
                    } else {
                        args.ParameterValues["parastatTimeOfIncomingPayment"] = (DateTime?)(dateTimeFilterItem.Filters.ElementAt(0).Value);
                        args.ParameterValues["paraendTimeOfIncomingPayment"] = (DateTime?)(dateTimeFilterItem.Filters.ElementAt(1).Value);
                    }
                    continue;
                }
                switch(filter.MemberName) {
                    case "CustomerCompanyName":
                        args.ParameterValues["paracustomerCompanyName"] = filter.Value.ToString();
                        break;
                    case "VehicleFundsTypeId":
                        args.ParameterValues["paravehicleFundsTypeId"] = (int?)filter.Value;
                        break;
                    case "VehiclePaymentMethodId":
                        args.ParameterValues["paravehiclePaymentMethodId"] = (int?)filter.Value;
                        break;
                    case "Status":
                        args.ParameterValues["parastatus"] = (int?)filter.Value;
                        break;
                }
            }
        }

        private void ReportViewerRenderBegin(object sender, RenderBeginEventArgs args) {
            this.GetParameterValues(args);
        }

        public FilterItem FilterItem {
            private get;
            set;
        }

        public ReportVehiclePaymentBill() {
            InitializeComponent();
        }
    }
}
