﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.Print {
    public partial class ReportWholesaleApproval {
        private WholesaleApproval wholesaleApproval;

        public WholesaleApproval WholesaleApproval {
            get {
                return this.wholesaleApproval;
            }
            set {
                this.wholesaleApproval = value;
                this.OnPropertyChanged("WholesaleApproval");
            }
        }

        private void GetParameterValues(RenderBeginEventArgs args) {
            if(this.WholesaleApproval != null) {
                args.ParameterValues["paraid"] = WholesaleApproval.Id;
                args.ParameterValues["WholesaleApprovalId"] = WholesaleApproval.Id;
                args.ParameterValues["paraPostAuditStatusId"] = WholesaleApproval.PostAuditStatusId;
            }
        }

        private void ReportViewerRenderBegin(object sender, RenderBeginEventArgs args) {
            this.GetParameterValues(args);
        }

        public ReportWholesaleApproval() {
            InitializeComponent();
        }
    }
}
