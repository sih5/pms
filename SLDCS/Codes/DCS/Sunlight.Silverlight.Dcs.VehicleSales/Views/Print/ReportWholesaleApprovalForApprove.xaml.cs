﻿using Sunlight.Silverlight.Dcs.Web;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.Print {
    public partial class ReportWholesaleApprovalForApprove {
        private WholesaleApproval wholesaleApproval;

        public WholesaleApproval WholesaleApproval {
            get {
                return this.wholesaleApproval;
            }
            set {
                this.wholesaleApproval = value;
                this.OnPropertyChanged("WholesaleApproval");
            }
        }

        private void GetParameterValues(RenderBeginEventArgs args) {
            if(this.WholesaleApproval != null) {
                args.ParameterValues["paraid"] = WholesaleApproval.Id;
                args.ParameterValues["WholesaleApprovalId"] = WholesaleApproval.Id;
                args.ParameterValues["paraPostAuditStatusId"] = WholesaleApproval.PostAuditStatusId;
            }
        }

        private void ReportViewerRenderBegin(object sender, RenderBeginEventArgs args) {
            this.GetParameterValues(args);
        }

        public ReportWholesaleApprovalForApprove() {
            InitializeComponent();
        }
    }
}
