﻿using System;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Telerik.ReportViewer.Silverlight;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.Print {
    public partial class ReportWholesaleRewardApp {
        public int WholesaleRewardAppId {
            private get;
            set;
        }

        public ReportWholesaleRewardApp() {
            InitializeComponent();
        }

        private void reportViewer_RenderBegin(object sender, RenderBeginEventArgs args) {
            args.ParameterValues["id"] = WholesaleRewardAppId;
        }
    }
}
