﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.Custom;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesProduct", "ProductSearch")]
    public class ProductForSearchManagement : DcsDataManagementViewBase {
        private ProductCategoryAndProductView productCategoryAndProductView;
        private const string VIEW_PRODUCTCATEGORYANDPRODUCT = "_ViewProductCategoryAndProductView_";

        public ProductForSearchManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_ProductSearch;
        }

        private void Initialize() {
            this.RegisterView(VIEW_PRODUCTCATEGORYANDPRODUCT, () => this.ProductCategoryAndProductView);
        }


        private ProductCategoryAndProductView ProductCategoryAndProductView {
            get {
                if(productCategoryAndProductView == null) {
                    this.productCategoryAndProductView = new ProductCategoryAndProductView();
                }
                return this.productCategoryAndProductView;
            }
        }


        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            throw new NotImplementedException();
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
            this.SwitchViewTo(VIEW_PRODUCTCATEGORYANDPRODUCT);
            this.ProductCategoryAndProductView.ProductDataGridView.FilterItem = filterItem;
            this.ProductCategoryAndProductView.ProductDataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]
                {
                    "Product"
                };
            }
        }
    }
}
