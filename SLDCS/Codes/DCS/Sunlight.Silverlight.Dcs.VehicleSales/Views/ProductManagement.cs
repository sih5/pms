﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.Custom;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.DataEdit;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesProduct", "Product", ActionPanelKeys = new[]{
        "ProductCategory","Product"
    })]
    public class ProductManagement : DcsDataManagementViewBase {
        private ProductCategoryAndProductView productCategoryAndProductView;
        private DataEditViewBase productCategoryDataEditView;
        private DataEditViewBase productDataEditView;
        private const string VIEW_PRODUCTCATEGORYANDPRODUCT = "_ViewProductCategoryAndProductView_";
        private const string DATA_EDIT_VIEW_PRODUCTCATEGORY = "_DataEditViewProductCategory_";
        private const string DATA_EDIT_VIEW_PRODUCT = "_DataEditViewProduct_";
        private ProductCategory tempProductCategory;
        public ProductManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_Product;
        }

        private void Initialize() {
            this.RegisterView(VIEW_PRODUCTCATEGORYANDPRODUCT, () => this.ProductCategoryAndProductView);
            this.RegisterView(DATA_EDIT_VIEW_PRODUCTCATEGORY, () => this.ProductCategoryDataEditView);
            this.RegisterView(DATA_EDIT_VIEW_PRODUCT, () => this.ProductDataEditView);
        }

        private ProductCategoryAndProductView ProductCategoryAndProductView {
            get {
                if(this.productCategoryAndProductView == null) {
                    this.productCategoryAndProductView = new ProductCategoryAndProductView();
                    this.productCategoryAndProductView.PropertyChanged += this.SelectedTreeViewItem_PropertyChanged;
                }
                return this.productCategoryAndProductView;
            }
        }

        private void SelectedTreeViewItem_PropertyChanged(object sender, PropertyChangedEventArgs e) {
            if(e.PropertyName == "SelectedTreeViewItem" || e.PropertyName == "ProductDataGridViewSelectedEntities")
                this.CheckActionsCanExecute();
        }

        private DataEditViewBase ProductCategoryDataEditView {
            get {
                if(this.productCategoryDataEditView == null) {
                    this.productCategoryDataEditView = DI.GetDataEditView("ProductCategory");
                    this.productCategoryDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.productCategoryDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.productCategoryDataEditView;
            }
        }

        private DataEditViewBase ProductDataEditView {
            get {
                if(this.productDataEditView == null) {
                    this.productDataEditView = DI.GetDataEditView("Product");
                    this.productDataEditView.EditSubmitted += this.ProductDataEditView_EditSubmitted;
                    this.productDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.productDataEditView;
            }
        }

        private void ProductDataEditView_EditSubmitted(object sender, EventArgs e) {
            if(ProductCategoryAndProductView.GetProductDataGridViewFilterItem() != null) {
                ProductCategoryAndProductView.ExecuteQuery();//是否刷新GridView仍然根据QueryPanel面板查询条件FilterItem进行判断
            }
            ProductCategoryAndProductView.RefreshTreeView();
            this.SwitchViewTo(VIEW_PRODUCTCATEGORYANDPRODUCT);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(ProductCategoryAndProductView.GetProductDataGridViewFilterItem() != null) {
                ProductCategoryAndProductView.ExecuteQuery();
            }
            ProductCategoryAndProductView.RefreshTreeView();
            this.SwitchViewTo(VIEW_PRODUCTCATEGORYANDPRODUCT);
        }

        //编辑产品与产品分类撤销操作共用
        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(VIEW_PRODUCTCATEGORYANDPRODUCT);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != VIEW_PRODUCTCATEGORYANDPRODUCT)
                return false;

            var tempTreeItem = this.ProductCategoryAndProductView.SelectedTreeViewItem;
            switch(uniqueId) {
                //case "AddRootProductCategory"://TODO:设计要求暂不实现
                case "AddProductCategory":
                case "EditProductCategory":
                    return true;
                case "AbandonProductCategory":
                case "AddProduct":
                    if(tempTreeItem == null)
                        return false;
                    if(uniqueId == "AddProduct")
                        return ((ProductCategory)tempTreeItem.Tag).ProductCategoryLevel == GlobalVar.PRODUCTCATEGORY_MAX_LEVEL;
                    return ((ProductCategory)tempTreeItem.Tag).Status != (int)DcsMasterDataStatus.作废;
                case "EditProduct":
                case "AbandonProduct":
                case "ResumeProduct":
                    if(this.ProductCategoryAndProductView.ProductDataGridViewSelectedEntities == null)
                        return false;
                    var entities = this.ProductCategoryAndProductView.ProductDataGridViewSelectedEntities.Cast<Product>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == "EditProduct" || uniqueId == "AbandonProduct")
                        return entities[0].Status != (int)DcsMasterDataStatus.作废;
                    return entities[0].Status != (int)DcsMasterDataStatus.有效;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            if(this.ProductCategoryAndProductView.SelectedTreeViewItem != null)
                tempProductCategory = (ProductCategory)this.ProductCategoryAndProductView.SelectedTreeViewItem.Tag;
            switch(uniqueId) {
                //case "AddRootProductCategory"://TODO:设计要求暂不实现
                case "AddProductCategory":
                    var productCategory = this.ProductCategoryDataEditView.CreateObjectToEdit<ProductCategory>();
                    if(tempProductCategory != null && tempProductCategory.RootProductCategory == null) {
                        productCategory.ProductCategoryLevel = tempProductCategory.ProductCategoryLevel + 1;
                        productCategory.RootCategoryId = tempProductCategory.Id;
                        productCategory.RootCategoryName = tempProductCategory.Name;
                        productCategory.RootCategoryCode = tempProductCategory.Code;
                        productCategory.ParentId = tempProductCategory.Id;
                        productCategory.ParentCode = tempProductCategory.Code;
                        productCategory.ParentName = tempProductCategory.Name;
                    }
                    if(tempProductCategory != null && tempProductCategory.RootProductCategory != null) {
                        this.ProductCategoryDataEditView.Tag = tempProductCategory;
                        productCategory.ProductCategoryLevel = tempProductCategory.ProductCategoryLevel + 1;
                        productCategory.ParentId = tempProductCategory.Id;
                        productCategory.ParentCode = tempProductCategory.Code;
                        productCategory.ParentName = tempProductCategory.Name;
                        productCategory.RootCategoryId = tempProductCategory.RootProductCategory.Id;
                        productCategory.RootCategoryName = tempProductCategory.RootProductCategory.Name;
                        productCategory.RootCategoryCode = tempProductCategory.RootProductCategory.Code;
                    }
                    productCategory.Status = (int)DcsBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW_PRODUCTCATEGORY);
                    break;
                case "EditProductCategory":
                    if(tempProductCategory != null) {
                        this.ProductCategoryDataEditView.SetObjectToEditById(tempProductCategory.Id);
                    }
                    this.SwitchViewTo(DATA_EDIT_VIEW_PRODUCTCATEGORY);
                    break;
                case "AbandonProductCategory":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        if(tempProductCategory == null)
                            return;
                        try {
                            if(tempProductCategory.Can作废产品分类)
                                tempProductCategory.作废产品分类();
                            var domainContext = this.ProductCategoryAndProductView.TreeViewDomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                                ProductCategoryAndProductView.RefreshTreeView();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "AddProduct":
                    var product = this.ProductDataEditView.CreateObjectToEdit<Product>();
                    if(tempProductCategory != null) {
                        product.ParentCode = tempProductCategory.Code;
                        product.ParentName = tempProductCategory.Name;
                        ((ProductDataEditView)ProductDataEditView).TempProductAffiProductCategory = new ProductAffiProductCategory {
                            ProductCategoryId = tempProductCategory.Id,
                            ProductId = product.Id,
                            RootCategoryId = tempProductCategory.RootCategoryId
                        };
                    }
                    ((ProductDataEditView)ProductDataEditView).tempProductCategory = this.tempProductCategory;
                    product.Status = (int)DcsMasterDataStatus.有效;
                    product.ProductLifeCycle = (int)DcsPartsBranchProductLifeCycle.新产品;
                    product.CanBeOrdered = true;
                    product.IfPurchasable = true;
                    this.SwitchViewTo(DATA_EDIT_VIEW_PRODUCT);
                    break;
                case "EditProduct":
                    this.ProductDataEditView.SetObjectToEditById(this.ProductCategoryAndProductView.ProductDataGridViewSelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW_PRODUCT);
                    break;
                case "AbandonProduct":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.ProductCategoryAndProductView.ProductDataGridViewSelectedEntities.Cast<Product>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废产品)
                                entity.作废产品();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "ResumeProduct":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Resume, () => {
                        var entity = this.ProductCategoryAndProductView.ProductDataGridViewSelectedEntities.Cast<Product>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can恢复产品)
                                entity.恢复产品();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_ResumeSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.ProductCategoryAndProductView.ProductDataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
            this.SwitchViewTo(VIEW_PRODUCTCATEGORYANDPRODUCT);
            this.ProductCategoryAndProductView.SetProductDataGridViewFilterItem(filterItem);
            this.ProductCategoryAndProductView.ExecuteQuery();
            ProductCategoryAndProductView.RefreshTreeView();//依据设计要求 点击查询面板时同样刷新产品分类树
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]
                {
                    "Product"
                };
            }
        }
    }
}
