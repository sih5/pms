﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    /// <summary>
    /// 基础客户查询
    /// </summary>
    public class CustomerDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string Title {
            get {
                return VehicleSalesUIStrings.QueryPanel_Title_Customer;
            }
        }

        public CustomerDropDownQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("SalesDealerId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }
        public override string DataGridViewKey {
            get {
                return "CustomerForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CustomerForSelect";
            }
        }
    }
}
