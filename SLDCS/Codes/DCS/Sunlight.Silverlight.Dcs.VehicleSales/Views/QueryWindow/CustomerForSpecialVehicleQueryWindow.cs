﻿
namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class CustomerForSpecialVehicleQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "CustomerForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CustomerForSelect";
            }
        }
    }
}
