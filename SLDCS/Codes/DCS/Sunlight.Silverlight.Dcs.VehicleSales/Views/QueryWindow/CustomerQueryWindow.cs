﻿
using Sunlight.Silverlight.Core.Model;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    /// <summary>
    /// 基础客户查询
    /// </summary>
    public class CustomerQueryWindow : DcsQueryWindowBase {
        public CustomerQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem("SalesDealerId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
        }
        public override string DataGridViewKey {
            get {
                return "CustomerForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "CustomerForSelect";
            }
        }
    }
}
