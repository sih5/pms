﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class DealerForSpecialVehicleQueryWindow : DcsQueryWindowBase {
        public DealerForSpecialVehicleQueryWindow() {
            this.SetDefaultFilterItem(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsMasterDataStatus.有效
            });
        }

        public override string QueryPanelKey {
            get {
                return "DealerForSelect";
            }
        }

        public override string DataGridViewKey {
            get {
                return "DealerForSpecialVehicle";
            }
        }
    }
}
