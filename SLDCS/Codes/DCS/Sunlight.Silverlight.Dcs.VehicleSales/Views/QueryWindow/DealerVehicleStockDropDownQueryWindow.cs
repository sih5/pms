﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class DealerVehicleStockDropDownQueryWindow : DcsDropDownQueryWindowBase {
        /// <summary>
        /// 经销商库存信息查询
        /// 注：企业ID=登录人员企业ID，库存状态=在库，锁定状态=未锁定
        ///     CompanyId         InventoryStatus  LockStatus
        /// </summary>
        public override string Title {
            get {
                return VehicleSalesUIStrings.QueryPanel_Title_DealerVehicleStock;
            }
        }

        public override string DataGridViewKey {
            get {
                return "DealerVehicleStock";
            }
        }

        public override string QueryPanelKey {
            get {
                return "DealerVehicleStock";
            }
        }

        public DealerVehicleStockDropDownQueryWindow() {
            var composite = new CompositeFilterItem();
            composite.Filters.Add(new FilterItem("CompanyId", typeof(int), FilterOperator.IsEqualTo, BaseApp.Current.CurrentUserData.EnterpriseId));
            composite.Filters.Add(new FilterItem("LockStatus", typeof(int), FilterOperator.IsEqualTo, (int)DcsDealerVehicleStockLockStatus.未锁定));
            composite.Filters.Add(new FilterItem("InventoryStatus", typeof(int), FilterOperator.IsEqualTo, (int)DcsDealerVehicleStockInventoryStatus.在库));
            this.SetDefaultFilterItem(composite);
        }
    }
}
