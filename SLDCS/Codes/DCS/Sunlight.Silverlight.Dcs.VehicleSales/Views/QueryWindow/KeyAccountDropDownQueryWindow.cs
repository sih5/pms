﻿using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class KeyAccountDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return VehicleSalesUIStrings.QueryPanel_Title_KeyAccount;
            }
        }

        public override string DataGridViewKey {
            get {
                return "KeyAccount";
            }
        }

        public override string QueryPanelKey {
            get {
                return "KeyAccount";
            }
        }
    }
}
