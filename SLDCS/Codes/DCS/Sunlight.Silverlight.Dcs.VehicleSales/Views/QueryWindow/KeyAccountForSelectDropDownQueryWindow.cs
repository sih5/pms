﻿using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class KeyAccountForSelectDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return VehicleSalesUIStrings.QueryPanel_Title_KeyAccount;
            }
        }

        public override string DataGridViewKey {
            get {
                return "KeyAccountForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "KeyAccount";
            }
        }

    }
}
