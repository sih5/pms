﻿
namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class KeyAccountForSelectQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "KeyAccountForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "KeyAccount";
            }
        }
    }
}
