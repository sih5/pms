﻿namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class KeyAccountQueryWindow : DcsQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "KeyAccount";
            }
        }

        public override string QueryPanelKey {
            get {
                return "KeyAccount";
            }
        }
    }
}
