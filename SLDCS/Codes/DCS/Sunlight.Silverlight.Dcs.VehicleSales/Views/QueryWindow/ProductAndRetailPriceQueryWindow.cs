﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class ProductAndRetailPriceQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return VehicleSalesUIStrings.QueryPanel_Title_ProductAndRetailPrice;
            }
        }

        public override string DataGridViewKey {
            get {
                return "VehicleModelAffiProduct";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VehicleModelAffiProduct";
            }
        }
    }
}
