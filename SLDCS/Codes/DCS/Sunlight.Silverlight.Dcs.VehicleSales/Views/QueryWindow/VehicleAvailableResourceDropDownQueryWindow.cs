﻿using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class VehicleAvailableResourceDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "VehicleAvailableResource";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VehicleAvailableResource";
            }
        }

        public override string Title {
            get {
                return VehicleSalesUIStrings.QueryPanel_Title_VehicleAvailableResource;
            }
        }
    }
}
