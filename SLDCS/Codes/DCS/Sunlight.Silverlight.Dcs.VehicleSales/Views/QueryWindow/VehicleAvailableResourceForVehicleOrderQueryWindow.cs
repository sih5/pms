﻿
namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class VehicleAvailableResourceForVehicleOrderQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "VehicleAvailableResourceForVehicleOrder";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VehicleAvailableResourceForVehicleOrder";
            }
        }
    }
}
