﻿using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class VehicleCustomerInformationDropDownQueryWindow : DcsDropDownQueryWindowBase {

        public override string Title {
            get {
                return VehicleSalesUIStrings.QueryPanel_Title_VehicleCustomerInformation;
            }
        }

        public override string DataGridViewKey {
            get {
                return "VehicleCustomerInformationForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VehicleCustomerInformationForSelect";
            }
        }
    }
}
