﻿
namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class VehicleCustomerInformationQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "VehicleCustomerInformationForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VehicleCustomerInformationForSelect";
            }
        }
    }
}
