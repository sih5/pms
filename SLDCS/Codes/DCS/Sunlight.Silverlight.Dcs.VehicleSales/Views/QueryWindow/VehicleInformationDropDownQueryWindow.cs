﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class VehicleInformationDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string Title {
            get {
                return VehicleSalesUIStrings.QueryPanel_Title_VehicleInformation;
            }
        }

        public override string DataGridViewKey {
            get {
                return "VehicleInformationForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VehicleInformationForSelect";
            }
        }

        public VehicleInformationDropDownQueryWindow() {
            var filterItem = new CompositeFilterItem();
            filterItem.Filters.Add(new FilterItem("VehicleType", typeof(int), FilterOperator.IsNotEqualTo, (int)DcsVehicleInformationVehicleType.普通));
            filterItem.Filters.Add(new FilterItem("VehicleType", typeof(int), FilterOperator.IsNotEqualTo, (int)DcsVehicleInformationVehicleType.进口));
            this.SetDefaultFilterItem(filterItem);
        }
    }
}
