﻿
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Web;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class VehicleInformationForSelectQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "VehicleInformationForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VehicleInformationForSelect";
            }
        }

        public VehicleInformationForSelectQueryWindow() {
            var filterItem = new CompositeFilterItem();
            filterItem.Filters.Add(new FilterItem("VehicleType", typeof(int), FilterOperator.IsNotEqualTo, (int)DcsVehicleInformationVehicleType.普通));
            filterItem.Filters.Add(new FilterItem("VehicleType", typeof(int), FilterOperator.IsNotEqualTo, (int)DcsVehicleInformationVehicleType.进口));
            this.SetDefaultFilterItem(filterItem);
        }
    }
}
