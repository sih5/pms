﻿
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class VehicleModelAffiProductDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "VehicleModelAffiProduct";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VehicleModelAffiProduct";
            }
        }

        public override string Title {
            get {
                return VehicleSalesUIStrings.QueryPanel_Title_VehicleModelAffiProduct;
            }
        }
    }
}
