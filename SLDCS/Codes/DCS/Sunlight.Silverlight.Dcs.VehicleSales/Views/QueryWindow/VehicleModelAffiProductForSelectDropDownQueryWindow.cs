﻿
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    /// <summary>
    /// 产品以及零售价格信息查询
    /// </summary>
    public class VehicleModelAffiProductForSelectDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "VehicleModelAffiProductForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VehicleModelAffiProductForSelect";
            }
        }

        public override string Title {
            get {
                return VehicleSalesUIStrings.QueryPanel_Title_VehicleModelAffiProductForSelect;
            }
        }
    }
}
