﻿namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    public class VehiclePreAllocationRefQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "VehiclePreAllocationRef";
            }
        }

        public override string QueryPanelKey {
            get {
                return "VehiclePreAllocationRef";
            }
        }
    }
}
