﻿
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    /// <summary>
    /// 批售审批单查询
    /// </summary>
    public class WholesaleApprovalDropDownQueryWindow : DcsDropDownQueryWindowBase {
        public override string DataGridViewKey {
            get {
                return "WholesaleApprovalForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "WholesaleApprovalForSelect";
            }
        }

        public override string Title {
            get {
                return VehicleSalesUIStrings.QueryPanel_Title_WholesaleApproval;
            }
        }
    }
}
