﻿
namespace Sunlight.Silverlight.Dcs.VehicleSales.Views.QueryWindow {
    /// <summary>
    /// 批售审批单查询
    /// </summary>
    public class WholesaleApprovalQueryWindow : DcsQueryWindowBase {

        public override string DataGridViewKey {
            get {
                return "WholesaleApprovalForSelect";
            }
        }

        public override string QueryPanelKey {
            get {
                return "WholesaleApprovalForSelect";
            }
        }
    }
}
