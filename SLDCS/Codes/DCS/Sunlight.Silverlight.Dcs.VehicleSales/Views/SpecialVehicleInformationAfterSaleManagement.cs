﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleInformation", "SpecialVehicleInformationAfterSale", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT
    })]
    public class SpecialVehicleInformationAfterSaleManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        public SpecialVehicleInformationAfterSaleManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_SpecialVehicleInformationAfterSale;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SpecialVehicleInformationAfterSale"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("SpecialVehicleInformationAfterSale");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return dataEditView;
            }
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null) {
                this.DataGridView.ExecuteQueryDelayed();
                this.SwitchViewTo(DATA_GRID_VIEW);
            }
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]{
                   "SpecialVehicleInformationAfterSale" 
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var retainedCustomerVehicleList = this.DataEditView.CreateObjectToEdit<RetainedCustomerVehicleList>();
                    retainedCustomerVehicleList.RetainedCustomer = new RetainedCustomer();
                    retainedCustomerVehicleList.RetainedCustomer.Status = (int)DcsRetainedCustomerStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW) {
                return false;
            }
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<RetainedCustomerVehicleList>().Any(r => r.RetainedCustomer != null && r.RetainedCustomer.Status == (int)DcsRetainedCustomerStatus.有效);//导航属性RetainedCustomer不能为空，否则RetainedCustomer.Status会报空引用
                default:
                    return false;
            }
        }
    }
}
