﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleInformation", "SpecialVehicleInformation", ActionPanelKeys = new[]{
         CommonActionKeys.ADD_EDIT_CONFIRM_IMPORT,"SpecialVehicleInformation"
    })]
    public class SpecialVehicleInformationManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataImportView;
        private DataEditViewBase dataTradeView;
        private const string DATA_IMPORT_VIEW = "_dataImportView_";
        private const string DATA_TRADE_VIEW = "_dataTradeView_";

        public SpecialVehicleInformationManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_SpecialVehicleInformation;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("SpecialVehicleInformation"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleInformation");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return dataEditView;
            }
        }

        private DataEditViewBase DataImportView {
            get {
                if(this.dataImportView == null) {
                    this.dataImportView = DI.GetDataEditView("SpecialVehicleInformationForImport");
                    this.dataImportView.EditCancelled += this.dataEditView_EditCancelled;
                }
                return this.dataImportView;
            }
        }

        private DataEditViewBase DataTradeView {
            get {
                if(this.dataTradeView == null) {
                    this.dataTradeView = DI.GetDataEditView("SpecialVehicleInformationForTrade");
                    this.dataTradeView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataTradeView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataTradeView;
            }
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null) {
                this.DataGridView.ExecuteQueryDelayed();
                this.SwitchViewTo(DATA_GRID_VIEW);
            }
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_IMPORT_VIEW, () => this.DataImportView);
            this.RegisterView(DATA_TRADE_VIEW, () => this.DataTradeView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]{
                   "SpecialVehicleInformation" 
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var vehicleInformation = this.DataEditView.CreateObjectToEdit<VehicleInformation>();
                    vehicleInformation.BranchId = 1;
                    vehicleInformation.BranchCode = "CAM";
                    vehicleInformation.BranchName = "CAM";
                    vehicleInformation.OldVIN = "VIN";
                    vehicleInformation.VehicleCategoryId = 1;
                    vehicleInformation.VehicleCategoryName = "CAM";
                    vehicleInformation.Status = (int)DcsVehicleStatus.本部仓库;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.CONFIRM:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Confirm, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleInformation>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can确认特殊车辆信息)
                                entity.确认特殊车辆信息();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(VehicleSalesUIStrings.DataManagementView_Notification_ConfirmSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_IMPORT_VIEW);
                    break;
                case "Trade":
                    this.SwitchViewTo(DATA_TRADE_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW) {
                return false;
            }
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case CommonActionKeys.IMPORT:
                case "Trade":
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.CONFIRM:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<VehicleInformation>().ToArray();
                    if(entity.Length != 1)
                        return false;
                    return entity[0].Status == (int)DcsVehicleStatus.本部仓库;
                default:
                    return false;
            }
        }
    }
}
