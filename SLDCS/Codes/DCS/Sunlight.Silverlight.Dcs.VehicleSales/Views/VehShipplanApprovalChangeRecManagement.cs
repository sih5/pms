﻿
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesExecution", "VehShipplanApprovalChangeRec", ActionPanelKeys = new[] {
        "VehShipplanApprovalChangeRec"
    })]
    public class VehShipplanApprovalChangeRecManagement : DcsDataManagementViewBase {
        private const string IMPORT_DATA_EDIT_VIEW = "_ImportDataEditView_";
        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehShipplanApprovalChangeRec"));
            }
        }

        private DataEditViewBase importDataEditView;

        private DataEditViewBase ImportDataEditView {
            get {
                if(this.importDataEditView == null) {
                    this.importDataEditView = DI.GetDataEditView("VehShipplanApprovalChangeRecImport");
                    this.importDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.importDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.importDataEditView;
            }
        }


        private DataEditViewBase dataEditView;

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehShipplanApprovalChangeRec");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }

        private void DataEditView_EditSubmitted(object sender, System.EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, System.EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public VehShipplanApprovalChangeRecManagement() {
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehShipplanApprovalChangeRec;
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(IMPORT_DATA_EDIT_VIEW, () => this.ImportDataEditView);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "Edit":
                case "Import":
                    return true;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Edit":
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "Import":
                    this.SwitchViewTo(IMPORT_DATA_EDIT_VIEW);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override System.Collections.Generic.IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehShipplanApprovalChangeRec"
                };
            }
        }
    }
}