﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleFinance", "VehicleAccountReceivableBill", ActionPanelKeys = new[] {
        "VehicleAccountReceivableBill"
    })]
    public class VehicleAccountReceivableBillManagement : DcsDataManagementViewBase {
        private const string TRANSFER_OUT_DATA_EDIT_VIEW = "_TransferOut_";
        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleAccountReceivableBill"));
            }
        }

        private DataEditViewBase dataEditView;

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleAccountReceivableBill");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase transferOutDataEditView;

        private DataEditViewBase TransferOutDataEditView {
            get {
                if(this.transferOutDataEditView == null) {
                    this.transferOutDataEditView = DI.GetDataEditView("VehicleAccountReceivableBillForTransferOut");
                    this.transferOutDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.transferOutDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.transferOutDataEditView;
            }
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(TRANSFER_OUT_DATA_EDIT_VIEW, () => this.TransferOutDataEditView);
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var vehicleAccountReceivableBill = this.DataEditView.CreateObjectToEdit<VehicleAccountReceivableBill>();
                    vehicleAccountReceivableBill.SalesCompanyId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    vehicleAccountReceivableBill.SalesCompanyCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    vehicleAccountReceivableBill.SalesCompanyName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    vehicleAccountReceivableBill.Status = (int)DcsVehicleAccountReceivableBillStatus.新增;
                    vehicleAccountReceivableBill.IssueDate = DateTime.Now;
                    vehicleAccountReceivableBill.DueDate = DateTime.Now;
                    vehicleAccountReceivableBill.BillReceptionDate = DateTime.Now;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "RollOut":
                    this.TransferOutDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(TRANSFER_OUT_DATA_EDIT_VIEW);
                    break;
                case "Review":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleAccountReceivableBill>().First();
                        if(entity == null)
                            return;
                        try {
                            entity.Status = (int)DcsVehicleAccountReceivableBillStatus.已接收;
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "BankQuery":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleAccountReceivableBill>().First();
                        if(entity == null)
                            return;
                        try {
                            entity.Status = (int)DcsVehicleAccountReceivableBillStatus.银行查询;
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleAccountReceivableBill>().First();
                        if(entity == null)
                            return;
                        try {
                            entity.Status = (int)DcsVehicleAccountReceivableBillStatus.审核通过;
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Recorded":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Recorded, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleAccountReceivableBill>().First();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can汇票入账)
                                entity.汇票入账();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_RecordedSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "RollOutApprove":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleAccountReceivableBill>().First();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can转出审核)
                                entity.转出审核();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Cashier":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Cashier, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleAccountReceivableBill>().First();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can出纳付票)
                                entity.出纳付票();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_CashierSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "ApproveNoPass":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleAccountReceivableBill>().First();
                        if(entity == null)
                            return;
                        try {
                            entity.Status = (int)DcsVehicleAccountReceivableBillStatus.新增;
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleAccountReceivableBill>().First();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废整车应收票据)
                                entity.作废整车应收票据();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case "RollOut":
                case "Review":
                case "BankQuery":
                case CommonActionKeys.APPROVE:
                case "Recorded":
                case "RollOutApprove":
                case "Cashier":
                case "ApproveNoPass":
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VehicleAccountReceivableBill>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId.Equals("RollOut"))
                        return entities[0].Status == (int)DcsVehicleAccountReceivableBillStatus.已入账;
                    if(uniqueId.Equals("BankQuery"))
                        return entities[0].Status == (int)DcsVehicleAccountReceivableBillStatus.已接收;
                    if(uniqueId.Equals(CommonActionKeys.APPROVE))
                        return entities[0].Status == (int)DcsVehicleAccountReceivableBillStatus.银行查询;
                    if(uniqueId.Equals("Recorded"))
                        return entities[0].Status == (int)DcsVehicleAccountReceivableBillStatus.审核通过;
                    if(uniqueId.Equals("RollOutApprove"))
                        return entities[0].Status == (int)DcsVehicleAccountReceivableBillStatus.转出申请;
                    if(uniqueId.Equals("Cashier"))
                        return entities[0].Status == (int)DcsVehicleAccountReceivableBillStatus.转出审批;
                    if(uniqueId.Equals("ApproveNoPass"))
                        return entities[0].Status == (int)DcsVehicleAccountReceivableBillStatus.新增 || entities[0].Status == (int)DcsVehicleAccountReceivableBillStatus.已接收 || entities[0].Status == (int)DcsVehicleAccountReceivableBillStatus.银行查询;
                    return entities[0].Status == (int)DcsVehicleAccountReceivableBillStatus.新增;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleAccountReceivableBill"
                };
            }
        }

        public VehicleAccountReceivableBillManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleAccountReceivableBill;
        }
    }
}