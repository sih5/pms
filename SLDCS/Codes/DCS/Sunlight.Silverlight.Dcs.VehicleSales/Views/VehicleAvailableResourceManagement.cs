﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleInformation", "VehicleAvailableResource", ActionPanelKeys = new[] {
        "VehicleAvailableResource",CommonActionKeys.EDIT_EXPORT
    })]
    public class VehicleAvailableResourceManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataImportView;
        private DataEditViewBase mcoDataImportView;
        private const string DATA_IMPORT_VIEW = "_dataImportView_";
        private const string DATA_IMPORT_VIEW_MCO = "_dataImportView_Mco";


        public VehicleAvailableResourceManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleAvailableResource;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleAvailableResourceForVehicleInformation"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleAvailableResource");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataImportView {
            get {
                if(this.dataImportView == null) {
                    this.dataImportView = DI.GetDataEditView("VehicleAvailableResourceForImport");
                    this.dataImportView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataImportView;
            }
        }

        private DataEditViewBase McoDataImportView {
            get {
                if(this.mcoDataImportView == null) {
                    this.mcoDataImportView = DI.GetDataEditView("VehicleAvailableResourceForMcoImport");
                    this.mcoDataImportView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.mcoDataImportView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_IMPORT_VIEW, () => this.DataImportView);
            this.RegisterView(DATA_IMPORT_VIEW_MCO, () => this.McoDataImportView);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleAvailableResourceForVehicleInformation"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_IMPORT_VIEW);
                    break;
                case "MCOImport":
                    this.SwitchViewTo(DATA_IMPORT_VIEW_MCO);
                    break;
                case CommonActionKeys.EXPORT:
                    var filterTtem = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(filterTtem != null) {
                        var vin = filterTtem.Filters.Single(r => r.MemberName == "VIN").Value as string;
                        var composite = filterTtem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? startRolloutDate = null;
                        DateTime? endRolloutDate = null;
                        if(composite != null) {
                            startRolloutDate = composite.Filters.First(r => r.MemberName == "VehicleInformation.RolloutDate").Value as DateTime?;
                            endRolloutDate = composite.Filters.Last(r => r.MemberName == "VehicleInformation.RolloutDate").Value as DateTime?;
                        }
                        var engineSerialNumber = filterTtem.Filters.Single(r => r.MemberName == "VehicleInformation.EngineSerialNumber").Value as string;
                        var productCategoryId = filterTtem.Filters.Single(r => r.MemberName == "ProductCategoryId").Value as int?;
                        var isAvailable = filterTtem.Filters.Single(r => r.MemberName == "IsAvailable").Value as bool?;
                        var vehicleWarehouseId = filterTtem.Filters.Single(r => r.MemberName == "VehicleWarehouseId").Value as int?;
                        var lockStatus = filterTtem.Filters.Single(r => r.MemberName == "LockStatus").Value as int?;

                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext != null)
                            domainContext.Load(domainContext.GetProductCategoriesQuery().Where(r => r.Id == (productCategoryId.HasValue ? productCategoryId.Value : default(int))), LoadBehavior.RefreshCurrent, loadOption => {
                                if(loadOption.HasError) {
                                    if(!loadOption.IsErrorHandled)
                                        loadOption.MarkErrorAsHandled();
                                }
                                domainContext.导出可用资源信息(startRolloutDate, endRolloutDate, vin, engineSerialNumber, loadOption.Entities.SingleOrDefault() == null ? default(string) : loadOption.Entities.Single().Code, isAvailable, vehicleWarehouseId, lockStatus, loadOp => {
                                    if(loadOp.HasError) {
                                        if(!loadOp.IsErrorHandled)
                                            loadOp.MarkErrorAsHandled();
                                        DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                                    }
                                    domainContext.IsInvoking = false;
                                    if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                                        this.ExportFile(loadOp.Value);
                                }, null);
                            }, null);
                    }
                    break;
            }
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VehicleAvailableResource>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].LockStatus == (int)DcsVehicleAvailableResourceLockStatus.未锁定;
                case CommonActionKeys.IMPORT:
                case "MCOImport":
                    return true;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
