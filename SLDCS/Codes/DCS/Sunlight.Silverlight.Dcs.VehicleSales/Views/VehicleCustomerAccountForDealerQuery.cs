﻿using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;
using System.Collections.Generic;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    /// <summary>
    /// 此节点，根据UI设计确认，没有查询面板，没有业务操作，进入界面直接查询
    /// </summary>
    [PageMeta("VehicleSales", "VehicleFinance", "VehicleCustomerAccountForDealerQuery")]
    public class VehicleCustomerAccountForDealerQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        private DataGridViewBase DataGridView {
            get {
                if(this.dataGridView == null) {
                    this.dataGridView = DI.GetDataGridView("VehicleCustomerAccount");
                }
                return this.dataGridView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override IEnumerable<string> QueryPanelKeys
        {
            get
            {
                return new[] { 
                    "VehicleCustomerAccountForDealer"
                };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "CustomerCompanyId",
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId,
                Operator = FilterOperator.IsEqualTo
            });
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {

        }

        public VehicleCustomerAccountForDealerQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleCustomerAccountForDealer;
        }
    }
}
