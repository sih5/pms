﻿using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleFinance", "VehicleDLRStartSecurityDeposit")]
    public class VehicleDLRStartSecurityDepositManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditViewImport;
        private const string DATA_EDIT_VIEW_IMPORT = "_DataEditViewImport_";

        public VehicleDLRStartSecurityDepositManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleDLRStartSecurityDeposit;
        }

        private void Initialize() {
            this.RegisterView(DATA_EDIT_VIEW_IMPORT, () => this.DataEditViewImport);
            this.SwitchViewTo(DATA_EDIT_VIEW_IMPORT);
        }

        private DataEditViewBase DataEditViewImport {
            get {
                return this.dataEditViewImport ?? (this.dataEditViewImport = DI.GetDataEditView("VehicleDLRStartSecurityDepositForImport"));
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {

        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {

        }
    }
}
