﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleFinance", "VehicleDealerCreditLimitApp", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_APPROVE_PRINT
    })]
    public class VehicleDealerCreditLimitAppManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        public VehicleDealerCreditLimitAppManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleDealerCreditLimitApp;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleDealerCreditLimitApp"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleDealerCreditLimitApp");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleDealerCreditLimitApp"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var vehicleDealerCreditLimitApp = this.DataEditView.CreateObjectToEdit<VehicleDealerCreditLimitApp>();
                    vehicleDealerCreditLimitApp.VehicleAccountGroupId = 1;
                    vehicleDealerCreditLimitApp.Status = (int)DcsVehicleDealerCreditLimitAppStatus.新建;
                    vehicleDealerCreditLimitApp.ExpireDate = DateTime.Now.AddDays(1);
                    vehicleDealerCreditLimitApp.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleDealerCreditLimitApp>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废整车经销商信用额度申请单)
                                entity.作废整车经销商信用额度申请单();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    var approveEntity = this.DataGridView.SelectedEntities.Cast<VehicleDealerCreditLimitApp>().SingleOrDefault();
                    if(approveEntity == null)
                        return;
                    var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                    if(domainContext == null)
                        return;
                    domainContext.Load(domainContext.查询可用余额Query(approveEntity.CustomerCompanyId, approveEntity.VehicleFundsTypeId), LoadBehavior.RefreshCurrent, loadOp => {
                        if(loadOp.HasError) {
                            if(!loadOp.IsErrorHandled)
                                loadOp.MarkErrorAsHandled();
                            DcsUtils.ShowDomainServiceOperationWindow(loadOp);
                            return;
                        }
                        var confirmMsg = loadOp.Entities.Any() ? VehicleSalesUIStrings.DataManagementView_Confirm_Approve : VehicleSalesUIStrings.DataManagementView_Confirm_VehicleDealerCreditLimitApp_VehicleCustomerAccountIsNull;
                        DcsUtils.Confirm(confirmMsg, () => {
                            try {
                                if(approveEntity.Can审核整车经销商信用额度申请单)
                                    approveEntity.审核整车经销商信用额度申请单();
                                this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                            } catch(Exception ex) {
                                UIHelper.ShowAlertMessage(ex.Message);
                            }
                        });
                    }, null);
                    break;
                case CommonActionKeys.PRINT:
                    var printWindow = new ReportVehicleDealerCreditLimitApp {
                        Header = VehicleSalesUIStrings.DataManagementView_PrintWindow_Title_VehicleDealerCreditLimitApp,
                        FilterItem = this.DataGridView.FilterItem
                    };
                    printWindow.ShowDialog();
                    break;

            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.PRINT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<VehicleDealerCreditLimitApp>().ToArray();
                    if(entity.Length != 1)
                        return false;
                    return entity[0].Status == (int)DcsVehicleDealerCreditLimitAppStatus.新建;
                default:
                    return false;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
    }
}