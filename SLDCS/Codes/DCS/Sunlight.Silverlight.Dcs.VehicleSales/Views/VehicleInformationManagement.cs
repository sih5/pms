﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleInformation", "VINChangeRecord", ActionPanelKeys = new[] {
        CommonActionKeys.EDIT_IMPORT,"VINChangeRecord"
    })]
    public class VehicleInformationManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewVin;
        private DataEditViewBase dataImportView;
        private const string DATA_EDIT_VIEW_VIN = "_DataEditViewVIN_";
        private const string DATA_IMPORT_VIEW = "_dataImportView_";

        public VehicleInformationManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleInformation;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleInformation"));
            }
        }

        private DataEditViewBase DataEditViewVin {
            get {
                if(this.dataEditViewVin == null) {
                    this.dataEditViewVin = DI.GetDataEditView("VINChangeRecordForVIN");
                    this.dataEditViewVin.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditViewVin.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewVin;
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VINChangeRecord");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataImportView {
            get {
                if(this.dataImportView == null) {
                    this.dataImportView = DI.GetDataEditView("VehicleInformationForImport");
                    this.dataImportView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataImportView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_VIN, () => this.DataEditViewVin);
            this.RegisterView(DATA_IMPORT_VIEW, () => this.DataImportView);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleInformation"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                    var vINChangeRecord = this.DataEditView.CreateObjectToEdit<VINChangeRecord>();
                    vINChangeRecord.Status = (int)DcsVehicleBaseDataStatus.有效;
                    vINChangeRecord.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    if(!this.DataGridView.SelectedEntities.Cast<VehicleInformation>().Any())
                        return;
                    var vehicleInformation = this.DataGridView.SelectedEntities.Cast<VehicleInformation>().First();
                    if(vehicleInformation != null) {
                        vINChangeRecord.RolloutDateBeforeChange = vehicleInformation.RolloutDate;
                        vINChangeRecord.EngineNumberBeforeChange = vehicleInformation.EngineSerialNumber;
                        vINChangeRecord.RolloutDateAfterChange = vehicleInformation.RolloutDate;
                        vINChangeRecord.EngineNumberAfterChange = vehicleInformation.EngineSerialNumber;
                        vINChangeRecord.VehicleId = vehicleInformation.Id;
                    }
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "VINEdit":
                    var vInChangeRecord = this.DataEditViewVin.CreateObjectToEdit<VINChangeRecord>();
                    vInChangeRecord.Status = (int)DcsVehicleBaseDataStatus.有效;
                    vInChangeRecord.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    if(!this.DataGridView.SelectedEntities.Cast<VehicleInformation>().Any())
                        return;
                    var vehicleInformationForVIN = this.DataGridView.SelectedEntities.Cast<VehicleInformation>().First();
                    if(vehicleInformationForVIN != null) {
                        vInChangeRecord.OriginalVIN = vehicleInformationForVIN.VIN;
                        vInChangeRecord.VehicleId = vehicleInformationForVIN.Id;
                    }
                    this.SwitchViewTo(DATA_EDIT_VIEW_VIN);
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_IMPORT_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EDIT:
                case "VINEdit":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<VehicleInformation>().ToArray();
                    if(entity.Length != 1)
                        return false;
                    if(uniqueId == CommonActionKeys.ABANDON)
                        return entity.First().Status == (int)DcsVehicleBaseDataStatus.有效;
                    return true;
                case CommonActionKeys.IMPORT:
                    return true;
                default:
                    return false;
            }
        }
    }
}