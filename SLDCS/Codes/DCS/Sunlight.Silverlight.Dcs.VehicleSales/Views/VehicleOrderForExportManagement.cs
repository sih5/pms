﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesOrder", "VehicleOrderExport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT_ALL,"VehicleOrderForExport"
    })]
    public class VehicleOrderForExportManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public VehicleOrderForExportManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleOrderExport;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleOrderExportWithDetails"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleOrderForExport"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            var filterTtem = this.DataGridView.FilterItem as CompositeFilterItem;
            if(filterTtem != null) {
                var code = filterTtem.Filters.Single(r => r.MemberName == "Code").Value as string;
                var orderType = filterTtem.Filters.Single(r => r.MemberName == "OrderType").Value as int?;
                var dealerName = filterTtem.Filters.Single(r => r.MemberName == "DealerName").Value as string;
                var vehicleOrderStatus = filterTtem.Filters.Single(r => r.MemberName == "Status").Value as int?;
                var yearOfPlan = filterTtem.Filters.Single(r => r.MemberName == "YearOfPlan").Value as int?;
                var monthOfPlan = filterTtem.Filters.Single(r => r.MemberName == "MonthOfPlan").Value as int?;
                var vehicleOrderDetailStatus = filterTtem.Filters.Single(r => r.MemberName == "VehicleOrderDetail.Status").Value as int?;
                var composite = filterTtem.Filters.SingleOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                DateTime? beginCreateTime = null;
                DateTime? finishCreateTime = null;
                if(composite != null) {
                    beginCreateTime = composite.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                    finishCreateTime = composite.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                }
                switch(uniqueId) {
                    case CommonActionKeys.EXPORT_ALL:
                        if(domainContext != null)
                            domainContext.导出整车订单(code, orderType, dealerName, vehicleOrderStatus, yearOfPlan, monthOfPlan, vehicleOrderDetailStatus, beginCreateTime, finishCreateTime, 1, loadOp => {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                                }
                                domainContext.IsInvoking = false;
                                if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                                    this.ExportFile(loadOp.Value);
                            }, null);
                        break;
                    case "DealerExport":
                        if(domainContext != null)
                            domainContext.导出整车订单(code, orderType, dealerName, vehicleOrderStatus, yearOfPlan, monthOfPlan, vehicleOrderDetailStatus, beginCreateTime, finishCreateTime, 2, loadOp => {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                                }
                                domainContext.IsInvoking = false;
                                if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                                    this.ExportFile(loadOp.Value);
                            }, null);
                        break;
                    case "CollectExport":
                        if(domainContext != null)
                            domainContext.导出整车订单(code, orderType, dealerName, vehicleOrderStatus, yearOfPlan, monthOfPlan, vehicleOrderDetailStatus, beginCreateTime, finishCreateTime, 3, loadOp => {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                                }
                                domainContext.IsInvoking = false;
                                if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                                    this.ExportFile(loadOp.Value);
                            }, null);
                        break;
                }
            }
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT_ALL:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case "DealerExport":
                case "CollectExport":
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }
    }
}
