﻿
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesOrder", "VehicleOrderSubmit", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_SUBMIT_EXPORT_ALL
    })]
    public class VehicleOrderForSubmitManagement : DcsDataManagementViewBase {
        private const string DETAIL_EDIT_VIEW = "_DetailDataEditView_";
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase detailDataEditView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleOrderWithDetails"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleOrderForSubmit");
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DetailDataEditView {
            get {
                if(this.detailDataEditView == null) {
                    this.detailDataEditView = DI.GetDataEditView("VehicleOrderForSubmitDetail");
                    this.detailDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                    this.detailDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                }
                return this.detailDataEditView;
            }
        }

        private void DataEditView_EditSubmitted(object sender, System.EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, System.EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        public VehicleOrderForSubmitManagement() {
            this.Title = DcsUIStrings.BusinessName_VehicleOrder;
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DETAIL_EDIT_VIEW, () => this.DetailDataEditView);
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleOrderForSubmit"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var vehicleOrder = this.DataEditView.CreateObjectToEdit<VehicleOrder>();
                    vehicleOrder.VehicleSalesOrgId = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID;
                    vehicleOrder.VehicleSalesOrgCode = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGCODE;
                    vehicleOrder.VehicleSalesOrgName = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGNAME;
                    vehicleOrder.VehicleFundsTypeId = 1;
                    vehicleOrder.OrderType = (int)DcsVehicleOrderOrderType.紧急订单;
                    vehicleOrder.Status = (int)DcsVehicleOrderStatus.新建;
                    vehicleOrder.DealerId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    vehicleOrder.DealerCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    vehicleOrder.DealerName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    vehicleOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.SUBMIT:
                    this.DetailDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DETAIL_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT_ALL:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EXPORT_ALL:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VehicleOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsVehicleOrderStatus.新建;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                ClientVar.ConvertTime(compositeFilterItem);
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "DealerId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}