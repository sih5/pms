﻿
using System;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesOrder", "VehicleOrder", ActionPanelKeys = new[] {
        "VehicleOrderForDealer",CommonActionKeys.ABANDON_CONFIRM_EXPORT_ALL
    })]
    public class VehicleOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase insteadSubmitDataEditView;
        private DataEditViewBase confirmDataEditView;
        private const string INSTEAD_SUBMIT_DATA_EDIT_VIEW = "_insteadSubmitDataEditView_";
        private const string CONFIRM_DATA_EDIT_VIEW = "_confirmDataEditView_";

        public DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleOrderWithDetails"));
            }
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleOrder");
                    this.dataEditView.EditSubmitted += DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        public DataEditViewBase InsteadSubmitDataEditView {
            get {
                if(this.insteadSubmitDataEditView == null) {
                    this.insteadSubmitDataEditView = DI.GetDataEditView("VehicleOrderForInsteadSubmit");
                    this.insteadSubmitDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.insteadSubmitDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.insteadSubmitDataEditView;
            }
        }

        public DataEditViewBase ConfirmDataEditView {
            get {
                if(this.confirmDataEditView == null) {
                    this.confirmDataEditView = DI.GetDataEditView("VehicleOrderForConfirm");
                    this.confirmDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.confirmDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.confirmDataEditView;
            }
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(INSTEAD_SUBMIT_DATA_EDIT_VIEW, () => this.InsteadSubmitDataEditView);
            this.RegisterView(CONFIRM_DATA_EDIT_VIEW, () => this.ConfirmDataEditView);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleOrder;
        }

        protected override System.Collections.Generic.IEnumerable<string> QueryPanelKeys {
            get {
                return new[] { "VehicleOrderForDealer" };
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterAll = filterItem as CompositeFilterItem;
            if(compositeFilterAll == null)
                return;
            ClientVar.ConvertTime(compositeFilterAll);
            this.DataGridView.FilterItem = compositeFilterAll;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "InsteadAdd":
                    return true;
                case "InsteadEdit":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<VehicleOrder>().Any(r => r.Status == (int)DcsVehicleOrderStatus.新建 || r.Status == (int)DcsVehicleOrderStatus.提交);
                case "InsteadSubmit":
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.CONFIRM:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VehicleOrder>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(!uniqueId.Equals(CommonActionKeys.CONFIRM))
                        return entities[0].Status == (int)DcsVehicleOrderStatus.新建;
                    return entities[0].Status == (int)DcsVehicleOrderStatus.提交 || entities[0].Status == (int)DcsVehicleOrderStatus.部分确认 || entities[0].Status == (int)DcsVehicleOrderStatus.已确认 || entities[0].Status == (int)DcsVehicleOrderStatus.部分审批;
                case CommonActionKeys.EXPORT_ALL:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        //UI中提到的如果是新增，那么状态是新建，指的是销售订单提交页面的新增
        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "InsteadAdd":
                    var vehicleOrder = this.DataEditView.CreateObjectToEdit<VehicleOrder>();
                    vehicleOrder.VehicleSalesOrgId = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID;
                    vehicleOrder.VehicleSalesOrgCode = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGCODE;
                    vehicleOrder.VehicleSalesOrgName = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGNAME;
                    vehicleOrder.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    vehicleOrder.VehicleFundsTypeId = 1;
                    vehicleOrder.OrderType = (int)DcsVehicleOrderOrderType.紧急订单;
                    vehicleOrder.Status = (int)DcsVehicleOrderStatus.提交;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "InsteadEdit":
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case "InsteadSubmit":
                    this.InsteadSubmitDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(INSTEAD_SUBMIT_DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.CONFIRM:
                    this.ConfirmDataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(CONFIRM_DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        try {
                            if(entity.Can作废整车订单)
                                entity.作废整车订单();
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    return;
                                }
                                UIHelper.ShowNotification(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT_ALL:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
            }
        }

        public VehicleOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleOrder;
        }
    }
}
