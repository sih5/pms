﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesPlan", "VehicleOrderPlanSubmit", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON_EXPORT
    })]
    public class VehicleOrderPlanForSubmitManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;

        public VehicleOrderPlanForSubmitManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleOrderPlanForSupplier;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleOrderPlan"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleOrderPlanForSubmit");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var vehicleOrderPlan = this.DataEditView.CreateObjectToEdit<VehicleOrderPlan>();
                    vehicleOrderPlan.VehicleSalesOrgId = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID;
                    vehicleOrderPlan.VehicleSalesOrgCode = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGCODE;
                    vehicleOrderPlan.VehicleSalesOrgName = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGNAME;
                    vehicleOrderPlan.Status = (int)DcsVehicleOrderPlanStatus.新建;
                    vehicleOrderPlan.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    vehicleOrderPlan.DealerId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    vehicleOrderPlan.DealerCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    vehicleOrderPlan.DealerName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleOrderPlan>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废整车订货计划单)
                                entity.作废整车订货计划单();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<VehicleOrderPlan>().First().Status == (int)DcsVehicleOrderPlanStatus.新建;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            if(filterItem is CompositeFilterItem) {
                var compositeFilterItem = filterItem as CompositeFilterItem;
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "DealerId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleOrderPlanForSubmit"
                };
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
    }
}
