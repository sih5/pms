﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesPlan", "VehicleOrderPlan", ActionPanelKeys = new[]{
        CommonActionKeys.ADD_EDIT_ABANDON,"VehicleOrderPlan"
    })]
    public class VehicleOrderPlanManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataEditViewVehicleOrderPlanDetail;
        private DataEditViewBase dataEditViewVehicleOrderPlanDetailCollect;
        private const string DATA_EDIT_VIEW_VEHICLEORDERPLANDETAIL = "_DataEditViewVehicleOrderPlanDetail_";
        private const string DATA_EDIT_VIEW_VEHICLEORDERPLANDETAILCOLLECT = "_DataEditViewVehicleOrderPlanDetailCollect_";

        public VehicleOrderPlanManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleOrderPlan;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_EDIT_VIEW_VEHICLEORDERPLANDETAILCOLLECT, () => this.DataEditViewVehicleOrderPlanDetailCollect);
            this.RegisterView(DATA_EDIT_VIEW_VEHICLEORDERPLANDETAIL, () => this.DataEditViewVehicleOrderPlanDetail);
        }

        private DataEditViewBase DataEditViewVehicleOrderPlanDetail {
            get {
                if(this.dataEditViewVehicleOrderPlanDetail == null) {
                    this.dataEditViewVehicleOrderPlanDetail = DI.GetDataEditView("VehicleOrderPlanDetail");
                    this.dataEditViewVehicleOrderPlanDetail.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewVehicleOrderPlanDetail;
            }
        }

        private DataEditViewBase DataEditViewVehicleOrderPlanDetailCollect {
            get {
                if(this.dataEditViewVehicleOrderPlanDetailCollect == null) {
                    this.dataEditViewVehicleOrderPlanDetailCollect = DI.GetDataEditView("VehicleOrderPlanDetailCollect");
                    this.dataEditViewVehicleOrderPlanDetailCollect.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditViewVehicleOrderPlanDetailCollect;
            }
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleOrderPlan"));
            }
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleOrderPlan");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[]
                {
                    "VehicleOrderPlan"
                };
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                case "Detail":
                case "Collect":
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VehicleOrderPlan>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    return entities[0].Status == (int)DcsVehicleOrderPlanStatus.新建;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var vehicleOrderPlan = this.DataEditView.CreateObjectToEdit<VehicleOrderPlan>();
                    vehicleOrderPlan.VehicleSalesOrgId = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID;
                    vehicleOrderPlan.VehicleSalesOrgCode = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGCODE;
                    vehicleOrderPlan.VehicleSalesOrgName = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGNAME;
                    vehicleOrderPlan.Status = (int)DcsVehicleOrderPlanStatus.新建;
                    vehicleOrderPlan.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleOrderPlan>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废整车订货计划单)
                                entity.作废整车订货计划单();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Detail":
                    this.SwitchViewTo(DATA_EDIT_VIEW_VEHICLEORDERPLANDETAIL);
                    break;
                case "Collect":
                    this.SwitchViewTo(DATA_EDIT_VIEW_VEHICLEORDERPLANDETAILCOLLECT);
                    break;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, Core.Model.FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }
    }
}
