﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "MonthlyOrder", "VehicleOrderPlanSummary", ActionPanelKeys = new[]{
        "VehicleOrderPlanSummary"
    })]
    public class VehicleOrderPlanSummaryManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase vehicleOrderPlanSummaryAddImportDataEditView;
        private DataEditViewBase vehicleOrderPlanSummaryDelImportDataEditView;
        private const string DATA_EDIT_VIEW_VEHICLEORDERPLANSUMMARYADDIMPORT = "_DataEditViewVehicleOrderPlanSummaryAddImport_";
        private const string DATA_EDIT_VIEW_VEHICLEORDERPLANSUMMARYDELIMPORT = "_DataEditViewVehicleOrderPlanSummaryDelImport_";

        public VehicleOrderPlanSummaryManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleOrderPlanSummary;
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW_VEHICLEORDERPLANSUMMARYADDIMPORT, () => this.VehicleOrderPlanSummaryAddImportDataEditView);
            this.RegisterView(DATA_EDIT_VIEW_VEHICLEORDERPLANSUMMARYDELIMPORT, () => this.VehicleOrderPlanSummaryDelImportDataEditView);
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleOrderPlanSummary"));
            }
        }

        private DataEditViewBase VehicleOrderPlanSummaryAddImportDataEditView {
            get {
                if(this.vehicleOrderPlanSummaryAddImportDataEditView == null) {
                    this.vehicleOrderPlanSummaryAddImportDataEditView = DI.GetDataEditView("VehicleOrderPlanSummaryForAddImport");
                    this.vehicleOrderPlanSummaryAddImportDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.vehicleOrderPlanSummaryAddImportDataEditView;
            }
        }

        private DataEditViewBase VehicleOrderPlanSummaryDelImportDataEditView {
            get {
                if(this.vehicleOrderPlanSummaryDelImportDataEditView == null) {
                    this.vehicleOrderPlanSummaryDelImportDataEditView = DI.GetDataEditView("VehicleOrderPlanSummaryForDelImport");
                    this.vehicleOrderPlanSummaryDelImportDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.vehicleOrderPlanSummaryDelImportDataEditView;
            }
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "AddImport":
                    this.SwitchViewTo(DATA_EDIT_VIEW_VEHICLEORDERPLANSUMMARYADDIMPORT);
                    break;
                case "DelImport":
                    this.SwitchViewTo(DATA_EDIT_VIEW_VEHICLEORDERPLANSUMMARYDELIMPORT);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "AddImport":
                case "DelImport":
                    return true;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleOrderPlanSummary"
                };
            }
        }
    }
}
