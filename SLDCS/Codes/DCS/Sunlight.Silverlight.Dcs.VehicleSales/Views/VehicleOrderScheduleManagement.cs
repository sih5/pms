﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesPlan", "VehicleOrderSchedule", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON
    })]
    public class VehicleOrderScheduleManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        public VehicleOrderScheduleManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleOrderSchedule;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleOrderSchedule"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleOrderSchedule");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleOrderSchedule"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var vehicleOrderSchedule = this.DataEditView.CreateObjectToEdit<VehicleOrderSchedule>();
                    vehicleOrderSchedule.VehicleSalesOrgId = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID;
                    vehicleOrderSchedule.VehicleSalesOrgCode = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGCODE;
                    vehicleOrderSchedule.VehicleSalesOrgName = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGNAME;
                    vehicleOrderSchedule.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    vehicleOrderSchedule.OrderPlanAdjustDeadline = DateTime.Now;
                    vehicleOrderSchedule.Status = (int)DcsBaseDataStatus.有效;
                    vehicleOrderSchedule.OrderPlanToPODate = DateTime.Now;
                    vehicleOrderSchedule.OrderSubmitDeadline = DateTime.Now;
                    vehicleOrderSchedule.POToProductionPlanDate = DateTime.Now;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleOrderSchedule>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废整车订单日历)
                                entity.作废整车订单日历();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;

            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<VehicleOrderSchedule>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == CommonActionKeys.EDIT)
                        return entities[0].Status == (int)DcsBaseDataStatus.有效;
                    return entities[0].Status != (int)DcsBaseDataStatus.作废;
                default:
                    return false;
            }
        }
    }
}
