﻿using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleFinance", "VehiclePaymentBillForDealerQuery")]
    public class VehiclePaymentBillForDealerQuery : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public VehiclePaymentBillForDealerQuery() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehiclePaymentBillForDealer;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehiclePaymentBill"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilter = filterItem as CompositeFilterItem;
            if(compositeFilter != null)
                compositeFilter.Filters.Add(new FilterItem {
                    MemberName = "CustomerCompanyId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {

        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                  "VehiclePaymentBillForDealer"
                };
            }
        }
    }
}
