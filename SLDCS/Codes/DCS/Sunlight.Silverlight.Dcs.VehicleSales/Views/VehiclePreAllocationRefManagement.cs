﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesExecution", "VehiclePreAllocationRef", ActionPanelKeys = new[] {
       "VehiclePreAllocationRef"
    })]
    public class VehiclePreAllocationRefManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase MateExportDataEditView;
        private const string DATA_EDIT_VIEW_MATEEXPORT = "_DataEditViewMateExport_";
        private DataEditViewBase AdjustImportDataEditView;
        private const string DATA_EDIT_VIEW_ADJUSTIMPORT = "_DataEditViewAdjustImport_";


        public VehiclePreAllocationRefManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehiclePreAllocationRef;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehiclePreAllocationRefForManager"));
            }
        }

        //匹配导出
        private DataEditViewBase DataEditViewMateExport {
            get {
                if(this.MateExportDataEditView == null) {
                    this.MateExportDataEditView = DI.GetDataEditView("VehiclePreAllocationRefForMateExport");
                    this.MateExportDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.MateExportDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.MateExportDataEditView;
            }
        }

        //调整导入
        private DataEditViewBase DataEditViewAdjustImport {
            get {
                if(this.AdjustImportDataEditView == null) {
                    this.AdjustImportDataEditView = DI.GetDataEditView("VehiclePreAllocationRefForAdjustImport");
                    this.AdjustImportDataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.AdjustImportDataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.AdjustImportDataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW_MATEEXPORT, () => this.DataEditViewMateExport);
            this.RegisterView(DATA_EDIT_VIEW_ADJUSTIMPORT, () => this.DataEditViewAdjustImport);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehiclePreAllocationRefForManager"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var vehiclePaymentMethod = this.DataEditViewMateExport.CreateObjectToEdit<VehiclePreAllocationRef>();
                    vehiclePaymentMethod.YearOfOrder = DateTime.Now.Year;
                    vehiclePaymentMethod.MonthOfOrder = DateTime.Now.Month;
                    this.SwitchViewTo(DATA_EDIT_VIEW_MATEEXPORT);
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_EDIT_VIEW_ADJUSTIMPORT);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return true;
                case CommonActionKeys.IMPORT:
                    return true;
                default:
                    return false;
            }
        }
    }
}
