﻿using System;
using System.Linq;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesStocking", "VehicleReturnOrderInbound", ActionPanelKeys = new[] {
        "VehicleReturnOrderForInbound"
    })]
    public class VehicleReturnOrderForInboundManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase vehicleReturnOrderForInbound;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleReturnOrderForInbound"));
            }
        }
        private DataEditViewBase VehicleReturnOrderForInbound {
            get {
                if(this.vehicleReturnOrderForInbound == null) {
                    this.vehicleReturnOrderForInbound = DI.GetDataEditView("VehicleReturnOrderForInbound");
                    this.vehicleReturnOrderForInbound.EditCancelled += this.VehicleReturnOrderForInbound_EditCancelled;
                    this.vehicleReturnOrderForInbound.EditSubmitted += this.VehicleReturnOrderForInbound_EditSubmitted;
                }
                return this.vehicleReturnOrderForInbound;
            }
        }

        void VehicleReturnOrderForInbound_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        void VehicleReturnOrderForInbound_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }
        public VehicleReturnOrderForInboundManagement() {
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleReturnOrderForInbound;
            this.Initializer.Register(this.Initialize);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.VehicleReturnOrderForInbound);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case "Inbound":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.ToArray();
                    if(entities.Length != 1)
                        return false;
                    return true;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case "Inbound":
                    this.VehicleReturnOrderForInbound.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {

        }
    }
}
