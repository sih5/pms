﻿using System;
using System.Collections.Generic;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleShipping", "VehicleShippingOrderChangeRec", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_IMPORT
    })]
    public class VehicleShippingOrderChangeRecManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;
        private DataEditViewBase dataImportView;
        private const string DATA_IMPORT_VIEW = "_dataImportView_";


        public VehicleShippingOrderChangeRecManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleShippingOrderChangeRec;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleShippingOrderChangeRec"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleShippingOrderChangeRec");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private DataEditViewBase DataImportView {
            get {
                if(this.dataImportView == null) {
                    this.dataImportView = DI.GetDataEditView("VehicleShippingOrderChangeRecForImport");
                    this.dataImportView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataImportView;
            }
        }


        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
            this.RegisterView(DATA_IMPORT_VIEW, () => this.DataImportView);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleShippingOrderChangeRec"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var vehicleShippingOrderChangeRec = this.DataEditView.CreateObjectToEdit<VehicleShippingOrderChangeRec>();
                    vehicleShippingOrderChangeRec.VehicleSalesOrgId = 1;
                    vehicleShippingOrderChangeRec.VehicleSalesOrgCode = "CAM";
                    vehicleShippingOrderChangeRec.VehicleSalesOrgName = "CAM";
                    vehicleShippingOrderChangeRec.Status = (int)DcsVehicleBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(DATA_IMPORT_VIEW);
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.IMPORT:
                case CommonActionKeys.ADD:
                    return true;
                default:
                    return false;
            }
        }
    }
}
