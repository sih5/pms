﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleShipping", "VehicleShippingOrder", ActionPanelKeys = new[] {
        CommonActionKeys.IMPORT_ABANDON, "VehicleShippingOrder"
    })]
    public class VehicleShippingOrderManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase importDataEditView;
        private DataEditViewBase mcoDataImportView;
        private const string IMPORT_DATA_EDIT_VIEW = "_ImportDataEditView_";
        private const string DATA_IMPORT_VIEW_MCO = "_dataImportView_";

        public VehicleShippingOrderManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleShippingOrder;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleShippingOrder"));
            }
        }

        private DataEditViewBase ImportDataEditView {
            get {
                if(this.importDataEditView == null) {
                    this.importDataEditView = DI.GetDataEditView("VehicleShippingOrderImport");
                    this.importDataEditView.EditCancelled += this.ImportDataEditView_EditCancelled;
                    this.importDataEditView.EditSubmitted += this.ImportDataEditView_EditSubmitted;
                }
                return this.importDataEditView;
            }
        }

        private DataEditViewBase McoDataImportView {
            get {
                if(this.mcoDataImportView == null) {
                    this.mcoDataImportView = DI.GetDataEditView("VehicleShippingOrderForMcoImport");
                    this.mcoDataImportView.EditCancelled += this.ImportDataEditView_EditCancelled;
                }
                return this.mcoDataImportView;
            }
        }

        void ImportDataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        void ImportDataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(IMPORT_DATA_EDIT_VIEW, () => this.ImportDataEditView);
            this.RegisterView(DATA_IMPORT_VIEW_MCO, () => this.McoDataImportView);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var newCompositeFilter = new CompositeFilterItem();
            var compositeFilter = filterItem as CompositeFilterItem;
            if(compositeFilter == null)
                return;
            if(compositeFilter.Filters.Any(filter => filter.GetType() == typeof(CompositeFilterItem))) {
                var createTimeFilter = compositeFilter.Filters.Single(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTimeFilter == null)
                    return;
                var endCreateTimeFilter = createTimeFilter.Filters.ElementAt(1).Value as DateTime?;
                if(endCreateTimeFilter.HasValue)
                    createTimeFilter.Filters.ElementAt(1).Value = new DateTime(endCreateTimeFilter.Value.Year, endCreateTimeFilter.Value.Month, endCreateTimeFilter.Value.Day, 23, 59, 59);
            }
            newCompositeFilter.Filters.Add(compositeFilter);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleShippingOrder"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.IMPORT:
                    this.SwitchViewTo(IMPORT_DATA_EDIT_VIEW);
                    break;
                case "MCOImport":
                    this.SwitchViewTo(DATA_IMPORT_VIEW_MCO);
                    break;
                case "OutboundConfrim":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_OutboundConfrim, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleShippingOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can发运单出库确认)
                                entity.发运单出库确认();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Confirm_OutboundConfrimSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleShippingOrder>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废发运单)
                                entity.作废发运单();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.IMPORT:
                case "MCOImport":
                    return true;
                case "OutboundConfrim":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<VehicleShippingOrder>().Any(entity => entity.Status == (int)DcsVehicleShippingOrderStatus.新增);
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<VehicleShippingOrder>().Any(entity => entity.Status == (int)DcsVehicleShippingOrderStatus.新增);
                default:
                    return false;
            }
        }
    }
}
