﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.Client;
using System.Windows.Browser;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesExecution", "VehicleShipplanApprovalExport", ActionPanelKeys = new[] {
        CommonActionKeys.EXPORT
    })]
    public class VehicleShipplanApprovalForExportManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;

        public VehicleShipplanApprovalForExportManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleShipplanApprovalForExport;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleShipplanApprovalForExport"));
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    var composite = this.DataGridView.FilterItem as CompositeFilterItem;
                    if(composite != null) {
                        var filterTtem = composite.Filters.Where(r => r.GetType() == typeof(FilterItem)).ToArray();
                        if(filterTtem == null)
                            return;
                        var vehicleFundsTypeId = filterTtem.First(r => r.MemberName == "VehicleFundsTypeId").Value as int?;
                        var dealerName = filterTtem.First(r => r.MemberName == "DealerName").Value as string;
                        var productCategoryId = filterTtem.First(r => r.MemberName == "ProductCategoryId").Value as int?;
                        var compositeCreateTime = composite.Filters.FirstOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        var compositeApproveTime = composite.Filters.LastOrDefault(r => r.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                        DateTime? beginCreateTime = null;
                        DateTime? endCreateTime = null;
                        if(compositeCreateTime != null && compositeCreateTime.Filters.Any() ) {
                            beginCreateTime = compositeCreateTime.Filters.First(r => r.MemberName == "CreateTime").Value as DateTime?;
                            endCreateTime = compositeCreateTime.Filters.Last(r => r.MemberName == "CreateTime").Value as DateTime?;
                        }

                        DateTime? beginApproveTime = null;
                        DateTime? endApproveTime = null;
                        if(compositeApproveTime != null && compositeApproveTime.Filters.Any()) {
                            beginApproveTime = compositeApproveTime.Filters.First(r => r.MemberName == "ApproveTime").Value as DateTime?;
                            endApproveTime = compositeApproveTime.Filters.Last(r => r.MemberName == "ApproveTime").Value as DateTime?;
                        }
                        var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                        if(domainContext == null)
                            return;
                        domainContext.Load(domainContext.GetProductCategoriesQuery().Where(r => r.Id == (productCategoryId.HasValue ? productCategoryId.Value : 0)), LoadBehavior.RefreshCurrent, loadOption => {
                            if(loadOption.HasError) {
                                if(!loadOption.IsErrorHandled)
                                    loadOption.MarkErrorAsHandled();
                            }
                            domainContext.按车型导出发车审批单(dealerName,null, loadOption.Entities.SingleOrDefault() == null ? null : loadOption.Entities.First().Code, vehicleFundsTypeId, beginApproveTime, endApproveTime, beginCreateTime,endCreateTime, loadOp => {
                                if(loadOp.HasError) {
                                    if(!loadOp.IsErrorHandled)
                                        loadOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(loadOp.Error);
                                }
                                domainContext.IsInvoking = false;
                                if(!string.IsNullOrEmpty(loadOp.Value) || !string.IsNullOrWhiteSpace(loadOp.Value))
                                    this.ExportFile(loadOp.Value);
                            }, null);
                        }, null);
                    }
                    break;
            }
        }

        protected void ExportFile(string errorFileName = null) {
            if(string.IsNullOrWhiteSpace(errorFileName)) {
                UIHelper.ShowNotification(DcsUIStrings.DataEditView_Validation_NeedOutputErrorFileName);
                return;
            }
            HtmlPage.Window.Navigate(DcsUtils.GetDownloadFileUrl(errorFileName));
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        //TODO:待基类作调整
        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem == null)
                return;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "VehicleSalesOrgId",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID
            });
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleShipplanApprovalForExport"
                };
            }
        }
    }
}
