﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesExecution", "VehicleShipplanApproval", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_ABANDON_APPROVE_EXPORT,"VehicleShipplanApproval"
    })]
    public class VehicleShipplanApprovalManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;

        public VehicleShipplanApprovalManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleShipplanApproval;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleShipplanApproval"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleShipplanApproval");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var vehicleShipplanApproval = this.DataEditView.CreateObjectToEdit<VehicleShipplanApproval>();
                    vehicleShipplanApproval.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    vehicleShipplanApproval.Status = (int)DcsVehicleShipplanApprovalStatus.新增;
                    vehicleShipplanApproval.VehicleSalesOrgId = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID;
                    vehicleShipplanApproval.VehicleSalesOrgCode = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGCODE;
                    vehicleShipplanApproval.VehicleSalesOrgName = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGNAME;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleShipplanApproval>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废整车发车审批单)
                                entity.作废整车发车审批单();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.APPROVE:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleShipplanApproval>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can审核整车发车审批单)
                                entity.审核整车发车审批单();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case "Revocation":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Revocation, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleShipplanApproval>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can撤销整车发车审批单)
                                entity.撤销整车发车审批单();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_RevocationSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case "Revocation":
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<VehicleShipplanApproval>().First().Status == (int)DcsVehicleShipplanApprovalStatus.有效;
                case CommonActionKeys.APPROVE:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<VehicleShipplanApproval>().First().Status == (int)DcsVehicleShipplanApprovalStatus.新增;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<VehicleShipplanApproval>().First().Status == (int)DcsVehicleShipplanApprovalStatus.新增;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem != null)
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "VehicleSalesOrgId",
                    MemberType = typeof(int),
                    Operator = FilterOperator.IsEqualTo,
                    Value = GlobalVar.DEFAULT_OEMVEHICLEMONTHQUOTA_VEHICLESALESORGID
                });
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleShipplanApproval"
                };
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
    }
}
