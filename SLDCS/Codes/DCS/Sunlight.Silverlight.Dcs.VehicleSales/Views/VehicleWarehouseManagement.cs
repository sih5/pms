﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleSalesStocking", "VehicleWarehouse", ActionPanelKeys = new[] {
        CommonActionKeys.ADD_EDIT_ABANDON
    })]
    public class VehicleWarehouseManagement : DcsDataManagementViewBase {
        private DataEditViewBase dataEditView;
        private DataGridViewBase dataGridView;

        public VehicleWarehouseManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_VehicleWarehouse;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("VehicleWarehouse"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("VehicleWarehouse");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var vehicleWarehouse = this.DataEditView.CreateObjectToEdit<VehicleWarehouse>();
                    vehicleWarehouse.Status = (int)DcsBaseDataStatus.有效;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<VehicleWarehouse>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废整车仓库信息)
                                entity.作废整车仓库信息();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
            }
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<VehicleWarehouse>().First().Status == (int)DcsBaseDataStatus.有效;
                case CommonActionKeys.ABANDON:
                    if(this.DataGridView.SelectedEntities == null || this.DataGridView.SelectedEntities.Count() != 1)
                        return false;
                    return this.DataGridView.SelectedEntities.Cast<VehicleWarehouse>().First().Status == (int)DcsBaseDataStatus.有效;
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            var compositeFilterItem = new CompositeFilterItem();
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "Status",
                MemberType = typeof(int),
                Operator = FilterOperator.IsEqualTo,
                Value = (int)DcsBaseDataStatus.有效
            });
            if(filterItem != null) {
                var originalFilterItem = filterItem as CompositeFilterItem;
                if(originalFilterItem != null)
                    foreach(var filter in originalFilterItem.Filters)
                        compositeFilterItem.Filters.Add(filter);
                else
                    compositeFilterItem.Filters.Add(filterItem);
            }

            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = compositeFilterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "VehicleWarehouse"
                };
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                this.DataGridView.ExecuteQueryDelayed();//因为数据表格中的默认条件是状态=有效，作废后查询一遍，以清除数据表格的作废数据
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
    }
}
