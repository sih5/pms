﻿using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleKeyAccount", "WholesaleApprovalForApprove", ActionPanelKeys = new[] {
        CommonActionKeys.APPROVE_EXPORT_PRINT , "WholesaleApprovalForApprove"
    })]
    public class WholesaleApprovalForApproveManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("WholesaleApprovalForApprove"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("WholesaleApprovalForApprove");
                    this.DataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.DataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void DataEditView_EditCancelled(object sender, System.EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditSubmitted(object sender, System.EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override void OnExecutingAction(Silverlight.View.ActionPanelBase actionPanel, string uniqueId) {
            var selectedItem = this.DataGridView.SelectedEntities.Cast<WholesaleApproval>().FirstOrDefault();
            switch(uniqueId) {
                case CommonActionKeys.APPROVE:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EXPORT:
                    ((DcsDataGridViewBase)this.DataGridView).ExportData();
                    break;
                case "AllowModify":
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_AllowModify, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<WholesaleApproval>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can批售审批单允许修改)
                                entity.批售审批单允许修改();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOP => {
                                if(submitOP.HasError) {
                                    if(!submitOP.IsErrorHandled)
                                        submitOP.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOP);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(VehicleSalesUIStrings.DataManagementView_Notfiction_ModifySuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(System.Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                            return;
                        }
                    });
                    break;
                case "WholesaleResourcePrint":
                    if(selectedItem == null)
                        return;
                    var reportWindow = new ReportWholesaleApprovalForApprove {
                        Header = VehicleSalesUIStrings.DataManagementView_PrintWindow_Title_WholesaleApproval,
                        WholesaleApproval = selectedItem
                    };
                    reportWindow.ShowDialog();
                    break;
                case CommonActionKeys.PRINT:
                    if(selectedItem == null)
                        return;
                    var reportWindowForWholesaleApproval = new ReportWholesaleApproval {
                        Header = VehicleSalesUIStrings.DataManagementView_PrintWindow_Title_WholesaleApproval,
                        WholesaleApproval = selectedItem
                    };
                    reportWindowForWholesaleApproval.ShowDialog();
                    break;
                default:
                    break;
            }

        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;
            switch(uniqueId) {
                case CommonActionKeys.APPROVE:
                case "AllowModify":
                case "WholesaleResourcePrint":
                case CommonActionKeys.PRINT:
                    var selectedEntities = this.DataGridView.SelectedEntities;
                    if(selectedEntities == null)
                        return false;
                    var entities = selectedEntities.Cast<WholesaleApproval>().ToArray();
                    if(entities.Length != 1)
                        return false;
                    if(uniqueId == "AllowModify")
                        return entities[0].Status != (int)DcsMultiLevelAuditConfigInitialStatus.作废 &&
                            entities[0].Status != (int)DcsMultiLevelAuditConfigInitialStatus.审核通过 &&
                            entities[0].Status != (int)DcsMultiLevelAuditConfigInitialStatus.审核不通过;
                    if(uniqueId == "WholesaleResourcePrint" || uniqueId == CommonActionKeys.PRINT) {
                        if(uniqueId == "WholesaleResourcePrint")
                            return entities[0].Status == (int)DcsMultiLevelAuditConfigInitialStatus.审核通过 && entities[0].WholesaleApprovalDetails.FirstOrDefault().IfAgentDeliverDealerNeeded == true;
                        return entities[0].Status == (int)DcsMultiLevelAuditConfigInitialStatus.审核通过;
                    }
                    if(uniqueId == CommonActionKeys.PRINT)
                        return true;
                    return entities[0].Status == (int)DcsMultiLevelAuditConfigInitialStatus.提交;
                case CommonActionKeys.EXPORT:
                    return this.DataGridView.Entities != null && this.DataGridView.Entities.Any();
                default:
                    return false;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "WholesaleApprovalForApprove"
                };
            }
        }

        public WholesaleApprovalForApproveManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_WholesaleApprovalForApprove;
        }
    }
}
