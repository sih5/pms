﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleKeyAccount", "WholesaleApproval", ActionPanelKeys = new[]{
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT_PRINT , "WholesaleApproval"
    })]
    public class WholesaleApprovalManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        public WholesaleApprovalManagement() {
            this.Initializer.Register(this.Initialize);
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_WholesaleApproval;
        }

        private DataGridViewBase DataGridView {
            get {
                return this.dataGridView ?? (this.dataGridView = DI.GetDataGridView("WholesaleApproval"));
            }
        }

        private DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("WholesaleApproval");
                    this.dataEditView.EditSubmitted += this.DataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += this.DataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        private void DataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void DataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            if(compositeFilterItem != null) {
                compositeFilterItem.Filters.Add(new FilterItem {
                    MemberName = "DealerId",
                    Operator = FilterOperator.IsEqualTo,
                    MemberType = typeof(int),
                    Value = BaseApp.Current.CurrentUserData.EnterpriseId
                });
                var createTime = compositeFilterItem.Filters.Where(filter => filter is CompositeFilterItem && (filter as CompositeFilterItem).Filters.All(item => item.MemberName == "CreateTime")).SingleOrDefault(filter => filter.GetType() == typeof(CompositeFilterItem)) as CompositeFilterItem;
                if(createTime == null) {
                    UIHelper.ShowNotification("创建日期必须输入！");
                    return;
                }
                if((DateTime?)createTime.Filters.ElementAt(0).Value == DateTime.MinValue) {
                    UIHelper.ShowNotification("开始时间必须输入！");
                    return;
                }
                if((DateTime?)createTime.Filters.ElementAt(1).Value == DateTime.MaxValue.Date) {
                    UIHelper.ShowNotification("结束时间必须输入！");
                    return;
                }
                var statusFilters = compositeFilterItem.Filters.SingleOrDefault(item => item.MemberName == "Status");
                if(statusFilters != null && statusFilters.Value == null) {
                    UIHelper.ShowNotification("状态必须输入！");
                    return;
                }
            }
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] {
                    "WholesaleApproval"
                };
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var wholesaleApproval = this.DataEditView.CreateObjectToEdit<WholesaleApproval>();
                    wholesaleApproval.Status = (int)DcsMultiLevelAuditConfigInitialStatus.新建;
                    wholesaleApproval.DealerId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    wholesaleApproval.DealerCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    wholesaleApproval.DealerName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    wholesaleApproval.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<WholesaleApproval>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废批售审批单)
                                entity.作废批售审批单();
                            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
                            if(domainContext == null)
                                return;
                            domainContext.SubmitChanges(submitOp => {
                                if(submitOp.HasError) {
                                    if(!submitOp.IsErrorHandled)
                                        submitOp.MarkErrorAsHandled();
                                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                                    domainContext.RejectChanges();
                                    return;
                                }
                                UIHelper.ShowNotification(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                                this.CheckActionsCanExecute();
                            }, null);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Approve, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<WholesaleApproval>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交批售审批单)
                                entity.提交批售审批单();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_ApproveSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PRINT:
                case "WholesaleResourcePrint":
                    var selectedItem = this.DataGridView.SelectedEntities.Cast<WholesaleApproval>().FirstOrDefault();
                    if(uniqueId.Equals(CommonActionKeys.PRINT)) {
                        if(selectedItem == null)
                            return;
                        var reportWindowForWholesaleApproval = new ReportWholesaleApproval {
                            Header = VehicleSalesUIStrings.DataManagementView_PrintWindow_Title_WholesaleApproval,
                            WholesaleApproval = selectedItem
                        };
                        reportWindowForWholesaleApproval.ShowDialog();
                    }
                    if(uniqueId.Equals("WholesaleResourcePrint")) {
                        if(selectedItem == null)
                            return;
                        var reportWindow = new ReportWholesaleApprovalForApprove {
                            Header = VehicleSalesUIStrings.DataManagementView_PrintWindow_Title_WholesaleApproval,
                            WholesaleApproval = selectedItem
                        };
                        reportWindow.ShowDialog();
                    }
                    break;
            }
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entities = this.DataGridView.SelectedEntities.Cast<WholesaleApproval>().ToArray();
                    if(entities.Length != 1 || entities[0].Status == (int)DcsMultiLevelAuditConfigInitialStatus.提交)
                        return false;
                    return entities[0].Status == (int)DcsMultiLevelAuditConfigInitialStatus.新建;
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.PRINT:
                case "WholesaleResourcePrint":
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<WholesaleApproval>().ToArray();
                    if(entity.Length != 1)
                        return false;
                    if(uniqueId == CommonActionKeys.ABANDON)
                        return entity[0].Status == (int)DcsMultiLevelAuditConfigInitialStatus.新建;
                    if(uniqueId == CommonActionKeys.PRINT || uniqueId == "WholesaleResourcePrint")
                        return entity[0].Status == (int)DcsMultiLevelAuditConfigInitialStatus.审核通过;
                    return true;
                default:
                    return false;
            }
        }
    }
}