﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sunlight.Silverlight.Core;
using Sunlight.Silverlight.Core.Model;
using Sunlight.Silverlight.Dcs.VehicleSales.Resources;
using Sunlight.Silverlight.Dcs.VehicleSales.Views.Print;
using Sunlight.Silverlight.Dcs.Web;
using Sunlight.Silverlight.View;

namespace Sunlight.Silverlight.Dcs.VehicleSales.Views {
    [PageMeta("VehicleSales", "VehicleKeyAccount", "WholesaleRewardAppForReport", ActionPanelKeys = new[] { 
        CommonActionKeys.ADD_EDIT_ABANDON_SUBMIT_PRINT
    })]
    public class WholesaleRewardAppForReportManagement : DcsDataManagementViewBase {
        private DataGridViewBase dataGridView;
        private DataEditViewBase dataEditView;

        protected override IEnumerable<string> QueryPanelKeys {
            get {
                return new[] { 
                    "WholesaleRewardAppForReport" 
                };
            }
        }

        public DataGridViewBase DataGridView {
            get {
                return dataGridView ?? (this.dataGridView = DI.GetDataGridView("WholesaleRewardAppForReport"));
            }
        }

        public DataEditViewBase DataEditView {
            get {
                if(this.dataEditView == null) {
                    this.dataEditView = DI.GetDataEditView("WholesaleRewardAppForReport");
                    this.dataEditView.EditSubmitted += dataEditView_EditSubmitted;
                    this.dataEditView.EditCancelled += dataEditView_EditCancelled;
                }
                return this.dataEditView;
            }
        }

        public WholesaleRewardAppForReportManagement() {
            this.Title = VehicleSalesUIStrings.DataManagementView_Title_WholesaleRewardAppForReport;
            this.Initializer.Register(this.Initialize);
        }

        private void dataEditView_EditSubmitted(object sender, EventArgs e) {
            if(this.DataGridView.FilterItem != null)
                this.DataGridView.ExecuteQueryDelayed();
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void dataEditView_EditCancelled(object sender, EventArgs e) {
            this.SwitchViewTo(DATA_GRID_VIEW);
        }

        private void Initialize() {
            this.RegisterView(DATA_GRID_VIEW, () => this.DataGridView);
            this.RegisterView(DATA_EDIT_VIEW, () => this.DataEditView);
        }

        protected override bool OnRequestActionCanExecute(ActionPanelBase actionPanel, string uniqueId) {
            if(this.CurrentViewKey != DATA_GRID_VIEW)
                return false;

            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    return true;
                case CommonActionKeys.EDIT:
                case CommonActionKeys.SUBMIT:
                case CommonActionKeys.ABANDON:
                case CommonActionKeys.PRINT:
                    if(this.DataGridView.SelectedEntities == null)
                        return false;
                    var entity = this.DataGridView.SelectedEntities.Cast<WholesaleRewardApp>();
                    if(entity.Count() != 1)
                        return false;
                    if(uniqueId == CommonActionKeys.PRINT)
                        return entity.First().Status == (int)DcsWholesaleRewardAppStatus.奖励发放确认;
                    return entity.First().Status == (int)DcsWholesaleRewardAppStatus.新增;
                default:
                    return false;
            }
        }

        protected override void OnExecutingAction(ActionPanelBase actionPanel, string uniqueId) {
            switch(uniqueId) {
                case CommonActionKeys.ADD:
                    var wholesaleRewardApp = this.DataEditView.CreateObjectToEdit<WholesaleRewardApp>();
                    wholesaleRewardApp.Code = GlobalVar.ASSIGNED_BY_SERVER;
                    wholesaleRewardApp.Status = (int)DcsWholesaleRewardAppStatus.新增;
                    wholesaleRewardApp.DealerId = BaseApp.Current.CurrentUserData.EnterpriseId;
                    wholesaleRewardApp.DealerCode = BaseApp.Current.CurrentUserData.EnterpriseCode;
                    wholesaleRewardApp.DealerName = BaseApp.Current.CurrentUserData.EnterpriseName;
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.EDIT:
                    this.DataEditView.SetObjectToEditById(this.DataGridView.SelectedEntities.Cast<WholesaleRewardApp>().First().GetIdentity());
                    this.SwitchViewTo(DATA_EDIT_VIEW);
                    break;
                case CommonActionKeys.SUBMIT:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Submit, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<WholesaleRewardApp>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can提交批售奖励申请单)
                                entity.提交批售奖励申请单();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_SubmitSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.ABANDON:
                    DcsUtils.Confirm(VehicleSalesUIStrings.DataManagementView_Confirm_Abandon, () => {
                        var entity = this.DataGridView.SelectedEntities.Cast<WholesaleRewardApp>().SingleOrDefault();
                        if(entity == null)
                            return;
                        try {
                            if(entity.Can作废批售奖励申请单)
                                entity.作废批售奖励申请单();
                            this.ExecuteSerivcesMethod(VehicleSalesUIStrings.DataManagementView_Notification_AbandonSuccess);
                        } catch(Exception ex) {
                            UIHelper.ShowAlertMessage(ex.Message);
                        }
                    });
                    break;
                case CommonActionKeys.PRINT:
                    var printView = new ReportWholesaleRewardApp {
                        Header = "打印长安马自达批售奖励申请单",
                        WholesaleRewardAppId = this.DataGridView.SelectedEntities.Cast<WholesaleRewardApp>().First().Id
                    };
                    printView.ShowDialog();
                    break;
                default:
                    break;
            }
        }

        protected override void OnExecutingQuery(QueryPanelBase queryPanel, FilterItem filterItem) {
            this.SwitchViewTo(DATA_GRID_VIEW);
            var compositeFilterItem = filterItem as CompositeFilterItem;
            compositeFilterItem.Filters.Add(new FilterItem {
                MemberName = "DealerId",
                Operator = FilterOperator.IsEqualTo,
                MemberType = typeof(int),
                Value = BaseApp.Current.CurrentUserData.EnterpriseId
            });
            this.DataGridView.FilterItem = filterItem;
            this.DataGridView.ExecuteQueryDelayed();
        }

        private void ExecuteSerivcesMethod(string notifyMessage) {
            var domainContext = this.DataGridView.DomainContext as DcsDomainContext;
            if(domainContext == null)
                return;
            domainContext.SubmitChanges(submitOp => {
                if(submitOp.HasError) {
                    if(!submitOp.IsErrorHandled)
                        submitOp.MarkErrorAsHandled();
                    DcsUtils.ShowDomainServiceOperationWindow(submitOp);
                    domainContext.RejectChanges();
                    return;
                }
                UIHelper.ShowNotification(notifyMessage);
                this.CheckActionsCanExecute();
            }, null);
        }
    }
}
