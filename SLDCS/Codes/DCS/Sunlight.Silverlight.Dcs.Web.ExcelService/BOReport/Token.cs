﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {

        /// <summary>
        /// 获取Token(登录凭证)
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="cmsName">IP或机器名:端口 pmsrf.foton.com.cn:6400</param>
        /// <param name="authentication">登录类型</param>
        /// <param name="validMinutes">失效时间（分钟）</param>
        /// <param name="validNumOfLogons">使用几次后失效</param>
        /// <returns></returns>
        public string GetToken(string userName, string password, string cmsName, string authentication, int validMinutes, int validNumOfLogons) {
            try {
                var boCredential = new EnterpriseCredential {
                    Login = userName,
                    Password = password,
                    Domain = cmsName,
                    AuthType = authentication
                };
                var boSession = new Session();
                var boSessionInfo = boSession.login(boCredential, "");
                var sessionToken = boSession.createLogonToken(boSessionInfo.SessionID, validMinutes, validNumOfLogons);
                //String sessionToken = boSessionInfo.DefaultToken;
                return sessionToken;
            } catch(Exception) {
                return "";
            }

        }
    }
}
