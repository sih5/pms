﻿

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class AccountGroupExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 品牌
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 销售企业Id
        /// </summary>
        public int SalesCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 账户组编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 账户组编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 账户组名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 账户组名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 账户用途
        /// </summary>
        public int AccountType {
            get;
            set;
        }
        /// <summary>
        /// 账户用途_Str
        /// </summary>
        public string AccountTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
    }
}
