﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
  public  class AccountPeriodExtend {
      /// <summary>
      /// 账期表Id
      /// </summary>
      public int Id {
          get;
          set;
      }
      /// <summary>
      /// 账期表Id_Str
      /// </summary>
      public string IdStr {
          get;
          set;
      }
      /// <summary>
      /// 账期年
      /// </summary>
      public string Year {
          get;
          set;
      }
      /// <summary>
      /// 账期月
      /// </summary>
      public string Month {
          get;
          set;
      }
      /// <summary>
      /// 状态
      /// </summary>
      public int Status {
          get;
          set;
      }
      /// <summary>
      /// 状态
      /// </summary>
      public string StatusStr {
          get;
          set;
      }
    }
}
