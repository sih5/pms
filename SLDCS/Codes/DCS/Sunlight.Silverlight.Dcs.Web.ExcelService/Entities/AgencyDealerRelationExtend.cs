﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class AgencyDealerRelationExtend {

        /// <summary>
        /// 分公司名称 
        /// </summary>
        public string BranchNameStr {
            get;
            set;
        }

        /// <summary>
        /// 品牌 
        /// </summary>
        public string PartsSalesOrderTypeNameStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesOrderTypeId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id_Str
        /// </summary>
        public string PartsSalesOrderTypeIdStr {
            get;
            set;
        }

        /// <summary>
        /// 代理库Id
        /// </summary>
        public int AgencyId {
            get;
            set;
        }
        /// <summary>
        /// 代理库Id_Str
        /// </summary>
        public string AgencyIdStr {
            get;
            set;
        }

        /// <summary>
        /// 代理库编号
        /// </summary>
        public string AgencyCode {
            get;
            set;
        }
        /// <summary>
        /// 代理库编号_Str
        /// </summary>
        public string AgencyCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 代理库名称
        /// </summary>
        public string AgencyName {
            get;
            set;
        }
        /// <summary>
        /// 代理库名称_Str
        /// </summary>
        public string AgencyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商Id
        /// </summary>
        public int DealerId {
            get;
            set;
        }
        /// <summary>
        /// 经销商Id_Str
        /// </summary>
        public string DealerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商编号
        /// </summary>
        public string DealerCode {
            get;
            set;
        }
        /// <summary>
        /// 经销商编号_Str
        /// </summary>
        public string DealerCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商名称
        /// </summary>
        public string DealerName {
            get;
            set;
        }
        /// <summary>
        /// 经销商名称_Str
        /// </summary>
        public string DealerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id_Str
        /// </summary>
        public string AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废人_Str
        /// </summary>
        public string AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// 作废时间_Str
        /// </summary>
        public string AbandonTimeStr {
            get;
            set;
        }
    }
}
