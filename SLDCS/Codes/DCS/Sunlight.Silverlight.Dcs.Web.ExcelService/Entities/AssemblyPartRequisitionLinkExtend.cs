﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class AssemblyPartRequisitionLinkExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        public int Id {
            get;
            set;
        }
        public string PartCode {
            get;
            set;
        }
        public int PartId {
            get;
            set;
        }
        public string PartName {
            get;
            set;
        }
        public int KeyCode {
            get;
            set;
        }
        public string KeyCodeStr {
            get;
            set;
        }
        public string QtyStr {
            get;
            set;
        }
        public int Qty {
            get;
            set;
        }
         public string Remark {
            get;
            set;
        }
    }
}
