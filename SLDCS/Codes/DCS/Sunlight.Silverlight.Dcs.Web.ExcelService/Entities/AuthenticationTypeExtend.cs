﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class AuthenticationTypeExtend {
        public string ErrorMsg {
            get;
            set;
        }

        public string AuthenticationTypeName {
            get;
            set;
        }

        public string AuthenticationTypeCode {
            get;
            set;
        }
        public string ValDateStr {
            get;
            set;
        }
        public int ValDate {
            get;
            set;
        }
    }
}