﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public class BorrowBillDetailExtend
    {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg
        {
            get;
            set;
        }
        /// <summary>
        /// ID
        /// </summary>
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        /// 借用单ID
        /// </summary>
        public int BorrowBillId
        {
            get;
            set;
        }
        /// <summary>
        /// 配件ID
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }

       /// <summary>
       /// 配件编号
       /// </summary>
        public string SparePartCode
        {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName
        {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr
        {
            get;
            set;
        }
        /// <summary>
        /// 红岩号
        /// </summary>
        public string SihCode
        {
            get;
            set;
        }
        /// <summary>
        /// 红岩号_Str
        /// </summary>
        public string SihCodeStr
        {
            get;
            set;
        }

        /// <summary>
        /// 单位
        /// </summary>
        public string MeasureUnit
        {
            get;
            set;
        }
        /// <summary>
        /// 单位_Str
        /// </summary>
        public string MeasureUnitStr
        {
            get;
            set;
        }
        /// <summary>
        /// 库位
        /// </summary>
        public string WarehouseAreaCode
        {
            get;
            set;
        }

        /// <summary>
        /// 库位ID
        /// </summary>
        public int WarehouseAreaId
        {
            get;
            set;
        }

        /// <summary>
        /// 借用量
        /// </summary>
        public int? BorrowQty
        {
            get;
            set;
        }
        /// <summary>
        /// 借用量_Str
        /// </summary>

        public string BorrowQtyStr
        {
            get;
            set;
        }
        /// <summary>
        /// 出库量
        /// </summary>
        public int?  OutboundAmount{
            get;
            set;
        }
        /// <summary>
        /// 待确认量
        /// </summary>
        public int? ConfirmingQty{
            get;
            set;
        }
        /// <summary>
        /// 已确认数量
        /// </summary>
        public int? ConfirmedQty
        {
            get;
            set;
        }
    }
}
