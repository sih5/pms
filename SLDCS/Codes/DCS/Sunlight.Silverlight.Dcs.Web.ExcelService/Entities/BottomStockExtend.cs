﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class BottomStockExtend
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌ID
        /// </summary>
        public int PartsSalesCategoryId
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌ID_Str
        /// </summary>
        public string PartsSalesCategoryIdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌
        /// </summary>
        public string PartsSalesCategoryName
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌_Str
        /// </summary>
        public string PartsSalesCategoryNameStr
        {
            get;
            set;
        }
        /// <summary>
        /// 企业ID
        /// </summary>
        public int CompanyID
        {
            get;
            set;
        }
        /// <summary>
        /// 企业ID_Str
        /// </summary>
        public string CompanyIDStr
        {
            get;
            set;
        }
        /// <summary>
        /// 企业编号
        /// </summary>
        public string CompanyCode
        {
            get;
            set;
        }
        /// <summary>
        /// 企业编号_Str
        /// </summary>
        public string CompanyCodeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 企业名称
        /// </summary>
        public string CompanyName
        {
            get;
            set;
        }
        /// <summary>
        /// 企业名称_Str
        /// </summary>
        public string CompanyNameStr
        {
            get;
            set;
        }
        /// <summary>
        /// 企业类型
        /// </summary>
        public int? CompanyType
        {
            get;
            set;
        }
        /// <summary>
        /// 企业类型_Str
        /// </summary>
        public string CompanyTypeStr
        {
            get;
            set;
        }

        /// <summary>
        /// 仓库ID
        /// </summary>
        public int? WarehouseID
        {
            get;
            set;
        }
        /// <summary>
        /// 仓库ID_Str
        /// </summary>
        public string WarehouseIDStr
        {
            get;
            set;
        }
        /// <summary>
        /// 仓库编号
        /// </summary>
        public string WarehouseCode
        {
            get;
            set;
        }
        /// <summary>
        /// 仓库编号_Str
        /// </summary>
        public string WarehouseCodeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string WarehouseName
        {
            get;
            set;
        }
        /// <summary>
        /// 仓库名称_Str
        /// </summary>
        public string WarehouseNameStr
        {
            get;
            set;
        }
        /// <summary>
        /// 配件ID
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }
        /// <summary>
        /// 配件ID_Str
        /// </summary>
        public string SparePartIdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode
        {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName
        {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr
        {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            get;
            set;
        }
        /// <summary>
        /// 保底库存
        /// </summary>
        public int StockQty
        {
            get;
            set;
        }
        /// <summary>
        /// 保底库存_Str
        /// </summary>
        public string StockQtyStr
        {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr
        {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId
        {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr
        {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId
        {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id_Str
        /// </summary>
        public string AbandonerIdStr
        {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName
        {
            get;
            set;
        }
        /// <summary>
        /// 作废人_Str
        /// </summary>
        public string AbandonerNameStr
        {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime
        {
            get;
            set;
        }
        /// <summary>
        /// 作废时间_Str
        /// </summary>
        public string AbandonTimeStr
        {
            get;
            set;
        }
    }
}
