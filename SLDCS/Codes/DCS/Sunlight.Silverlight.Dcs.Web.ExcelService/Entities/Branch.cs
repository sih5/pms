﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class Branch {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 分公司编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 分公司名称
        /// </summary>
        public string Name {
            get;
            set;
        }

        /// <summary>
        /// 简称
        /// </summary>
        public string Abbreviation {
            get;
            set;
        }

        /// <summary>
        /// 主营品牌
        /// </summary>
        public string MainBrand {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime RowVersion {
            get;
            set;
        }
    }
}
