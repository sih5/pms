﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class BranchSupplierRelationExtend {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 供应商Id
        /// </summary>
        public int SupplierId {
            get;
            set;
        }
        /// <summary>
        /// 采购周期（天）
        /// </summary>
        public int PurchasingCycle {
            get;
            set;
        }
        /// <summary>
        /// 物流周期（天）
        /// </summary>
        public int ArrivalCycle
        {
            get;
            set;
        }
        /// <summary>
        /// 配件管理费率等级Id
        /// </summary>
        public int? PartsManagementCostGradeId {
            get;
            set;
        }
        /// <summary>
        /// 配件索赔系数
        /// </summary>
        public Decimal? PartsClaimCoefficient {
            get;
            set;
        }
        /// <summary>
        /// 配件索赔价格类型
        /// </summary>
        public int ClaimPriceCategory {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id
        /// </summary>
        public int CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id
        /// </summary>
        public int ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id
        /// </summary>
        public int AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime RowVersion {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }


        /// <summary>
        /// 配件销售类型Id_Str
        /// </summary>
        public string PartsSalesCategoryIdStr {
            get;
            set;
        }
        /// <summary>
        /// 供应商Id_Str
        /// </summary>
        public string SupplierIdStr {
            get;
            set;
        }
        /// <summary>
        /// 物流周期（天）_Str
        /// </summary>
        public string ArrivalCycleStr
        {
            get;
            set;
        }
        /// <summary>
        /// 采购周期（天）_Str
        /// </summary>
        public string PurchasingCycleStr
        {
            get;
            set;
        }
        /// <summary>
        /// 配件管理费率等级Id_Str
        /// </summary>
        public string PartsManagementCostGradeIdStr {
            get;
            set;
        }
        /// <summary>
        /// 配件索赔系数_Str
        /// </summary>
        public string PartsClaimCoefficientStr {
            get;
            set;
        }
        /// <summary>
        /// 配件索赔价格类型_Str
        /// </summary>
        public string ClaimPriceCategoryStr {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id_Str
        /// </summary>
        public string AbandonerIdStr {
            get;
            set;
        }
        /// <summary>
        /// 作废人_Str
        /// </summary>
        public string AbandonerNameStr {
            get;
            set;
        }
        /// <summary>
        /// 作废时间_Str
        /// </summary>
        public string AbandonTimeStr {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }

        /// <summary>
        /// 分公司编号_Str
        /// </summary>
        public string BranchCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 分公司名称_Str
        /// </summary>
        public string BranchNameStr {
            get;
            set;
        }

        /// <summary>
        /// 品牌_Str
        /// </summary>
        public string PartsSalesCategoryNameStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商编号_Str
        /// </summary>
        public string SupplierCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商名称_Str
        /// </summary>
        public string SupplierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 业务编号
        /// </summary>
        public string BusinessCode {
            get;
            set;
        }

        /// <summary>
        /// 业务编号_Str
        /// </summary>
        public string BusinessCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 业务名称
        /// </summary>
        public string BusinessName {
            get;
            set;
        }

        /// <summary>
        /// 业务名称_Str
        /// </summary>
        public string BusinessNameStr {
            get;
            set;
        }

        /// <summary>
        /// 配件管理费率等级名称_Str
        /// </summary>
        public string PartsManagementCostGradeNameStr {
            get;
            set;
        }
        public bool IfSPByLabor {
            get;
            set;
        }
        public string IfSPByLaborStr {
            get;
            set;
        }
        public decimal? LaborUnitPrice {
            get;
            set;
        }
        public string LaborUnitPriceStr {
            get;
            set;
        }
        public double? LaborCoefficient {
            get;
            set;
        }
        public string LaborCoefficientStr {
            get;
            set;
        }
        public bool IfHaveOutFee {
            get;
            set;
        }
        public string IfHaveOutFeeStr {
            get;
            set;
        }
        public decimal? OutServiceCarUnitPrice {
            get;
            set;
        }
        public string OutServiceCarUnitPriceStr {
            get;
            set;
        }
        public decimal? OutSubsidyPrice {
            get;
            set;
        }
        public string OutSubsidyPriceStr {
            get;
            set;
        }
        public double? OldPartTransCoefficient {
            get;
            set;
        }
        public string OldPartTransCoefficientStr {
            get;
            set;
        }
    }
}
