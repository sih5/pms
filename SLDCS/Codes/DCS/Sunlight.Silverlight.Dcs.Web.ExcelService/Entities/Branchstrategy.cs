﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class Branchstrategy {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 配件采购定价策略
        /// </summary>
        public int PartsPurchasePricingStrategy {
            get;
            set;
        }

        /// <summary>
        /// 紧急销售处理策略
        /// </summary>
        public int EmergencySalesStrategy {
            get;
            set;
        }

        /// <summary>
        /// 旧件返件策略
        /// </summary>
        public int UsedPartsReturnStrategy {
            get;
            set;
        }

        /// <summary>
        /// 配件运费计算方式
        /// </summary>
        public int PartsShipping {
            get;
            set;
        }

        /// <summary>
        /// 是否GPS接口
        /// </summary>
        public bool IsGPS {
            get;
            set;
        }

        /// <summary>
        /// 直供生成策略
        /// </summary>
        public int DirectSupplyStrategy {
            get;
            set;
        }

        /// <summary>
        /// 采购价申请策略
        /// </summary>
        public bool PurchasePricingStrategy {
            get;
            set;
        }

        /// <summary>
        /// 索赔自动终止时间策略
        /// </summary>
        public int ClaimAutoStopStrategy {
            get;
            set;
        }

        /// <summary>
        /// 索赔单允许驳回次数策略
        /// </summary>
        public int ClaimRejectTimeStrategy {
            get;
            set;
        }

        /// <summary>
        /// 终止提报索赔起始时间策略
        /// </summary>
        public int StopClaimStartStrategy {
            get;
            set;
        }

        /// <summary>
        /// 终止提报索赔小时数策略
        /// </summary>
        public int StopClaimTimeStrategy {
            get;
            set;
        }

        /// <summary>
        /// 扫码开始时间
        /// </summary>
        public DateTime ScanCodeStartTime {
            get;
            set;
        }

        /// <summary>
        /// 是否全部提报外出申请
        /// </summary>
        public bool IsAllServiceTripApp {
            get;
            set;
        }

        /// <summary>
        /// 是否强保计算管理费
        /// </summary>
        public bool IsFirstMainteManage {
            get;
            set;
        }

        /// <summary>
        /// 是否外采计算管理费
        /// </summary>
        public bool IsOuterPurchaseManage {
            get;
            set;
        }

        /// <summary>
        /// 是否欠款计算管理费
        /// </summary>
        public bool IsDebtManage {
            get;
            set;
        }

        /// <summary>
        /// 外出半径是否强制
        /// </summary>
        public bool Outforce {
            get;
            set;
        }

        /// <summary>
        /// 外出人数
        /// </summary>
        public int OutNumberPeople {
            get;
            set;
        }

        /// <summary>
        /// 外出天数
        /// </summary>
        public int OutNumberDays {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int AbandonerId {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime AbandonTime {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime RowVersion {
            get;
            set;
        }
    }
}
