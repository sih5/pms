﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class BranchstrategyExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件采购定价策略
        /// </summary>
        public int PartsPurchasePricingStrategy {
            get;
            set;
        }
        /// <summary>
        /// 配件采购定价策略_Str
        /// </summary>
        public string PartsPurchasePricingStrategyStr {
            get;
            set;
        }

        /// <summary>
        /// 紧急销售处理策略
        /// </summary>
        public int? EmergencySalesStrategy {
            get;
            set;
        }
        /// <summary>
        /// 紧急销售处理策略_Str
        /// </summary>
        public string EmergencySalesStrategyStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件返件策略
        /// </summary>
        public int? UsedPartsReturnStrategy {
            get;
            set;
        }
        /// <summary>
        /// 旧件返件策略_Str
        /// </summary>
        public string UsedPartsReturnStrategyStr {
            get;
            set;
        }

        /// <summary>
        /// 配件运费计算方式
        /// </summary>
        public int? PartsShipping {
            get;
            set;
        }
        /// <summary>
        /// 配件运费计算方式_Str
        /// </summary>
        public string PartsShippingStr {
            get;
            set;
        }

        /// <summary>
        /// 是否GPS接口
        /// </summary>
        public bool? IsGPS {
            get;
            set;
        }

        /// <summary>
        /// 采购订单是否需要计划价
        /// </summary>
        public bool? PurchaseIsNeedStd {
            get;
            set;
        }
        /// <summary>
        /// 是否GPS接口_Str
        /// </summary>
        public string IsGPSStr {
            get;
            set;
        }

        /// <summary>
        /// 直供生成策略
        /// </summary>
        public int? DirectSupplyStrategy {
            get;
            set;
        }
        /// <summary>
        /// 直供生成策略_Str
        /// </summary>
        public string DirectSupplyStrategyStr {
            get;
            set;
        }

        /// <summary>
        /// 采购价申请策略
        /// </summary>
        public bool? PurchasePricingStrategy {
            get;
            set;
        }
        /// <summary>
        /// 采购价申请策略_Str
        /// </summary>
        public string PurchasePricingStrategyStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔自动终止时间策略
        /// </summary>
        public int? ClaimAutoStopStrategy {
            get;
            set;
        }
        /// <summary>
        /// 索赔自动终止时间策略_Str
        /// </summary>
        public string ClaimAutoStopStrategyStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔单允许驳回次数策略
        /// </summary>
        public int? ClaimRejectTimeStrategy {
            get;
            set;
        }
        /// <summary>
        /// 索赔单允许驳回次数策略_Str
        /// </summary>
        public string ClaimRejectTimeStrategyStr {
            get;
            set;
        }

        /// <summary>
        /// 终止提报索赔起始时间策略
        /// </summary>
        public int? StopClaimStartStrategy {
            get;
            set;
        }
        /// <summary>
        /// 终止提报索赔起始时间策略_Str
        /// </summary>
        public string StopClaimStartStrategyStr {
            get;
            set;
        }

        /// <summary>
        /// 终止提报索赔小时数策略
        /// </summary>
        public int? StopClaimTimeStrategy {
            get;
            set;
        }
        /// <summary>
        /// 终止提报索赔小时数策略_Str
        /// </summary>
        public string StopClaimTimeStrategyStr {
            get;
            set;
        }

        /// <summary>
        /// 扫码开始时间
        /// </summary>
        public DateTime? ScanCodeStartTime {
            get;
            set;
        }
        /// <summary>
        /// 扫码开始时间_Str
        /// </summary>
        public string ScanCodeStartTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 是否全部提报外出申请
        /// </summary>
        public bool? IsAllServiceTripApp {
            get;
            set;
        }
        /// <summary>
        /// 是否全部提报外出申请_Str
        /// </summary>
        public string IsAllServiceTripAppStr {
            get;
            set;
        }

        /// <summary>
        /// 是否强保计算管理费
        /// </summary>
        public bool? IsFirstMainteManage {
            get;
            set;
        }
        /// <summary>
        /// 是否强保计算管理费_Str
        /// </summary>
        public string IsFirstMainteManageStr {
            get;
            set;
        }

        /// <summary>
        /// 是否外采计算管理费
        /// </summary>
        public bool? IsOuterPurchaseManage {
            get;
            set;
        }
        /// <summary>
        /// 是否外采计算管理费_Str
        /// </summary>
        public string IsOuterPurchaseManageStr {
            get;
            set;
        }

        /// <summary>
        /// 是否欠款计算管理费
        /// </summary>
        public bool? IsDebtManage {
            get;
            set;
        }
        /// <summary>
        /// 是否欠款计算管理费_Str
        /// </summary>
        public string IsDebtManageStr {
            get;
            set;
        }

        /// <summary>
        /// 外出半径是否强制
        /// </summary>
        public bool? Outforce {
            get;
            set;
        }
        /// <summary>
        /// 外出半径是否强制_Str
        /// </summary>
        public string OutforceStr {
            get;
            set;
        }

        /// <summary>
        /// 外出人数
        /// </summary>
        public int? OutNumberPeople {
            get;
            set;
        }
        /// <summary>
        /// 外出人数_Str
        /// </summary>
        public string OutNumberPeopleStr {
            get;
            set;
        }

        /// <summary>
        /// 外出天数
        /// </summary>
        public int? OutNumberDays {
            get;
            set;
        }
        /// <summary>
        /// 外出天数_Str
        /// </summary>
        public string OutNumberDaysStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id_Str
        /// </summary>
        public string AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废人_Str
        /// </summary>
        public string AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// 作废时间_Str
        /// </summary>
        public string AbandonTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
    }
}
