﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class CAReconciliationExtend {

        public int CompanyType {
            get;
            set;
        }

        public string StatementCode {
            get;
            set;
        }

        public int PartsSalesCategoryId {
            get;
            set;
        }

        public string PartsSalesCategoryName {
            get;
            set;
        }

        public int AccountGroupId {
            get;
            set;
        }
        public string AccountGroupName {
            get;
            set;
        }
        public int CorporationId {
            get;
            set;
        }
        public string CorporationCode {
            get;
            set;
        }


        public string CorporationName {
            get;
            set;
        }

        public decimal YeBalance {
            get;
            set;
        }

        public string YeBalanceStr {
            get;
            set;
        }
        public DateTime ReconciliationTime {
            get;
            set;
        }

        public string ReconciliationTimeStr {
            get;
            set;
        }
        public int ReconciliationDirection {
            get;
            set;
        }
        public string ReconciliationDirectionStr {
            get;
            set;
        }
        public string TimeStr {
            get;
            set;
        }
        public DateTime Time {
            get;
            set;
        }

        public string Abstract {
            get;
            set;
        }
        public decimal Money {
            get;
            set;
        }

        public string MoneyStr {
            get;
            set;
        }

        public string Remark {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }
    }
}
