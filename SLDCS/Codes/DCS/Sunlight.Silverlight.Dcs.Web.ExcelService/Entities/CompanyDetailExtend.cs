﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class CompanyDetailExtend {

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        /// <summary>
        ///企业编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 企业编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 企业名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 企业类型
        /// </summary>
        public int Type {
            get;
            set;
        }

        /// <summary>
        /// 企业id
        /// </summary>
        public int CompanyId {
            get;
            set;
        }
    }
}
