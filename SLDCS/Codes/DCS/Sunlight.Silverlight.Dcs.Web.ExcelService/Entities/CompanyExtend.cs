﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class CompanyExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// 企业Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 企业Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 企业类型
        /// </summary>
        public int Type {
            get;
            set;
        }
        /// <summary>
        /// 企业类型_Str
        /// </summary>
        public string TypeStr {
            get;
            set;
        }

        /// <summary>
        /// 企业编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 企业编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 企业名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 客户编码
        /// </summary>
        public string CustomerCode {
            get;
            set;
        }
        /// <summary>
        /// 客户编码_Str
        /// </summary>
        public string CustomerCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商编码
        /// </summary>
        public string SupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 供应商编码_Str
        /// </summary>
        public string SupplierCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 简称
        /// </summary>
        public string ShortName {
            get;
            set;
        }
        /// <summary>
        /// 简称_Str
        /// </summary>
        public string ShortNameStr {
            get;
            set;
        }

        /// <summary>
        /// 区省市Id
        /// </summary>
        public int? RegionId {
            get;
            set;
        }
        /// <summary>
        /// 区省市Id_Str
        /// </summary>
        public string RegionIdStr {
            get;
            set;
        }

        /// <summary>
        /// 省
        /// </summary>
        public string ProvinceName {
            get;
            set;
        }
        /// <summary>
        /// 省_Str
        /// </summary>
        public string ProvinceNameStr {
            get;
            set;
        }

        /// <summary>
        /// 市
        /// </summary>
        public string CityName {
            get;
            set;
        }
        /// <summary>
        /// 市_Str
        /// </summary>
        public string CityNameStr {
            get;
            set;
        }

        /// <summary>
        /// 县
        /// </summary>
        public string CountyName {
            get;
            set;
        }
        /// <summary>
        /// 县_Str
        /// </summary>
        public string CountyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 城市级别
        /// </summary>
        public int? CityLevel {
            get;
            set;
        }
        /// <summary>
        /// 城市级别_Str
        /// </summary>
        public string CityLevelStr {
            get;
            set;
        }

        /// <summary>
        /// 成立日期
        /// </summary>
        public DateTime? FoundDate {
            get;
            set;
        }
        /// <summary>
        /// 成立日期_Str
        /// </summary>
        public string FoundDateStr {
            get;
            set;
        }

        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactPerson {
            get;
            set;
        }
        /// <summary>
        /// 联系人_Str
        /// </summary>
        public string ContactPersonStr {
            get;
            set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string ContactPhone {
            get;
            set;
        }
        /// <summary>
        /// 联系电话_Str
        /// </summary>
        public string ContactPhoneStr {
            get;
            set;
        }

        /// <summary>
        /// 联系人手机
        /// </summary>
        public string ContactMobile {
            get;
            set;
        }
        /// <summary>
        /// 联系人手机_Str
        /// </summary>
        public string ContactMobileStr {
            get;
            set;
        }

        /// <summary>
        /// 传真
        /// </summary>
        public string Fax {
            get;
            set;
        }
        /// <summary>
        /// 传真_Str
        /// </summary>
        public string FaxStr {
            get;
            set;
        }

        /// <summary>
        /// 联系地址
        /// </summary>
        public string ContactAddress {
            get;
            set;
        }
        /// <summary>
        /// 联系地址_Str
        /// </summary>
        public string ContactAddressStr {
            get;
            set;
        }

        /// <summary>
        /// 邮政编码
        /// </summary>
        public string ContactPostCode {
            get;
            set;
        }
        /// <summary>
        /// 邮政编码_Str
        /// </summary>
        public string ContactPostCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 电子邮箱
        /// </summary>
        public string ContactMail {
            get;
            set;
        }
        /// <summary>
        /// 电子邮箱_Str
        /// </summary>
        public string ContactMailStr {
            get;
            set;
        }

        /// <summary>
        /// 注册证号
        /// </summary>
        public string RegisterCode {
            get;
            set;
        }
        /// <summary>
        /// 注册证号_Str
        /// </summary>
        public string RegisterCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 注册名称
        /// </summary>
        public string RegisterName {
            get;
            set;
        }
        /// <summary>
        /// 注册名称_Str
        /// </summary>
        public string RegisterNameStr {
            get;
            set;
        }

        /// <summary>
        /// 企业性质
        /// </summary>
        public int? CorporateNature {
            get;
            set;
        }
        /// <summary>
        /// 企业性质_Str
        /// </summary>
        public string CorporateNatureStr {
            get;
            set;
        }

        /// <summary>
        /// 法定代表人
        /// </summary>
        public string LegalRepresentative {
            get;
            set;
        }
        /// <summary>
        /// 法定代表人_Str
        /// </summary>
        public string LegalRepresentativeStr {
            get;
            set;
        }

        /// <summary>
        /// 身份证件类型
        /// </summary>
        public int? IdDocumentType {
            get;
            set;
        }
        /// <summary>
        /// 身份证件类型_Str
        /// </summary>
        public string IdDocumentTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 身份证件号码
        /// </summary>
        public string IdDocumentNumber {
            get;
            set;
        }
        /// <summary>
        /// 身份证件号码_Str
        /// </summary>
        public string IdDocumentNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 注册资本
        /// </summary>
        public Decimal? RegisterCapital {
            get;
            set;
        }
        /// <summary>
        /// 注册资本_Str
        /// </summary>
        public string RegisterCapitalStr {
            get;
            set;
        }

        /// <summary>
        /// 注册日期
        /// </summary>
        public DateTime? RegisterDate {
            get;
            set;
        }
        /// <summary>
        /// 注册日期_Str
        /// </summary>
        public string RegisterDateStr {
            get;
            set;
        }

        /// <summary>
        /// 经营范围
        /// </summary>
        public string BusinessScope {
            get;
            set;
        }
        /// <summary>
        /// 经营范围_Str
        /// </summary>
        public string BusinessScopeStr {
            get;
            set;
        }

        /// <summary>
        /// 营业地址
        /// </summary>
        public string BusinessAddress {
            get;
            set;
        }
        /// <summary>
        /// 营业地址_Str
        /// </summary>
        public string BusinessAddressStr {
            get;
            set;
        }

        /// <summary>
        /// 注册地址
        /// </summary>
        public string RegisteredAddress {
            get;
            set;
        }
        /// <summary>
        /// 注册地址_Str
        /// </summary>
        public string RegisteredAddressStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int? Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }
    }
}
