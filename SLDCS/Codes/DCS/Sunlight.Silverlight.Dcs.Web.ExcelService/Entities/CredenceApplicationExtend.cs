﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class CredenceApplicationExtend {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 客户企业Id
        /// </summary>
        public int CustomerCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 客户编号_Str
        /// </summary>
        public string CustomerCodeStr {
            get;
            set;
        }
        /// <summary>
        /// 客户名称_Str
        /// </summary>
        public string CustomerNameStr {
            get;
            set;
        }
        /// <summary>
        /// 账户组Id
        /// </summary>
        public int AccountGroupId {
            get;
            set;
        }
        /// <summary>
        /// 账户组名称_Str
        /// </summary>
        public string AccountGroupNameStr {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id
        /// </summary>
        public int CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 失效日期
        /// </summary>
        public DateTime ExpireDate {
            get;
            set;
        }
        /// <summary>
        /// 失效日期_Str
        /// </summary>
        public string ExpireDateStr {
            get;
            set;
        }
        /// <summary>
        /// 信用额度
        /// </summary>
        public decimal CredenceLimit {
            get;
            set;
        }
        /// <summary>
        /// 信用额度_Str
        /// </summary>
        public string CredenceLimitStr {
            get;
            set;
        }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        public string CreditTypeStr {
            get;
            set;
        }
        public int CreditType {
            get;
            set;
        }
    }
}
