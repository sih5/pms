﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class CustomerAccountExtend {
        public int Id {
            get;
            set;
        }
        
        public int AccountGroupId {
            get;
            set;
        }

        public decimal? SIHCredit {
            get;
            set;
        }

        public int CustomerCompanyId {
            get;
            set;
        }

        public DateTime? ValidationTime {
            get;
            set;
        }

        public DateTime? ExpireTime {
            get;
            set;
        }
    }
}
