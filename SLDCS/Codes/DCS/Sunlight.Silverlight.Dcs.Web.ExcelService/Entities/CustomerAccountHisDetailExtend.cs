﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class CustomerAccountHisDetailExtend {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 发生后金额
        /// </summary>
        public decimal AfterChangeAmount {
            get;
            set;
        }

        /// <summary>
        /// 发生日期
        /// </summary>
        public DateTime? ProcessDate {
            get;
            set;
        }

        /// <summary>
        /// 客户id
        /// </summary>
        public int CustomerCompanyId {
            get;
            set;
        }

        public int AccountGroupId {
            get;
            set;
        }
        public int CompanyType {
            get;
            set;
        }
        public int SalesUnitId {
            get;
            set;
        }
    }
}