﻿
using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class CustomerDirectSpareListExtend {
        /// <summary>
        /// 品牌ID
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 品牌Id_Str
        /// </summary>
        public string PartsSalesCategoryIdStr
        {
            get;
            set;
        }
        ///<summary>
        ///  客户ID  
        ///</summary>
        public int CustomerId {
            get;
            set;
        }
        ///<summary>
        ///  客户ID_Str  
        ///</summary>
        public string CustomerIdStr
        {
            get;
            set;
        }
        ///<summary>
        ///  配件Id  
        ///</summary>
        public int SparePartId {
            get;
            set;
        }
        ///<summary>
        ///  配件Id_Str  
        ///</summary>
        public string SparePartIdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg
        {
            get;
            set;
        }
        /// <summary>
        /// ID
        /// </summary>
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        /// ID_Str
        /// </summary>
        public string IdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌名称
        /// </summary>
        public string PartsSalesCategoryName
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌名称_Str
        /// </summary>
        public string PartsSalesCategoryNameStr
        {
            get;
            set;
        }

        /// <summary>
        /// 客户编号
        /// </summary>
        public string CustomerCode
        {
            get;
            set;
        }
        /// <summary>
        /// 客户编号_Str
        /// </summary>
        public string CustomerCodeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 客户名称
        /// </summary>
        public string CustomerName
        {
            get;
            set;
        }
        /// <summary>
        /// 客户名称_Str
        /// </summary>
        public string CustomerNameStr
        {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode
        {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName
        {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr
        {
            get;
            set;
        }
        /// <summary>
        /// 是否直供
        /// </summary>
        public int? IfDirectProvision
        {
            get;
            set;
        }
        /// <summary>
        /// 是否直供_Str
        /// </summary>
        public string IfDirectProvisionStr
        {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId
        {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName
        {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime
        {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        public int? Status
        {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr
        {
            get;
            set;
        }
        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit
        {
            get;
            set;
        }
        /// <summary>
        /// 计量单位_Str
        /// </summary>
        public string MeasureUnitStr
        {
            get;
            set;
        }
    }
}
