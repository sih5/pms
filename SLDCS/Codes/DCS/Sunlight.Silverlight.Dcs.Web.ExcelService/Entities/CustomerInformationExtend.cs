﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class CustomerInformationExtend {
        /// <summary>
        /// 销售企业名称_Str
        /// </summary>
        public string SalesCompanyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 客户企业名称_Str
        /// </summary>
        public string CustomerCompanyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 销售企业Id
        /// </summary>
        public int SalesCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 销售企业Id_Str
        /// </summary>
        public string SalesCompanyIdStr {
            get;
            set;
        }

        /// <summary>
        /// 客户企业Id
        /// </summary>
        public int CustomerCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 客户企业Id_Str
        /// </summary>
        public string CustomerCompanyIdStr {
            get;
            set;
        }

        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactPerson {
            get;
            set;
        }
        /// <summary>
        /// 联系人_Str
        /// </summary>
        public string ContactPersonStr {
            get;
            set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string ContactPhone {
            get;
            set;
        }
        /// <summary>
        /// 联系电话_Str
        /// </summary>
        public string ContactPhoneStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }
    }
}
