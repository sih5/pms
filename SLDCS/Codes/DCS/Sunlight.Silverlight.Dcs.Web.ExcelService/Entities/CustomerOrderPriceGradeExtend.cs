﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class CustomerOrderPriceGradeExtend {

        /// <summary>
        /// 客户企业Id
        /// </summary>
        public int CustomerCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 客户企业Id_Str
        /// </summary>
        public string CustomerCompanyIdStr {
            get;
            set;
        }
        /// <summary>
        /// 客户企业编号_Str  、、品牌、订单类型名称、系数
        /// </summary>
        public string CustomerCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 客户企业名称_Str
        /// </summary>
        public string CustomerCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id_Str
        /// </summary>
        public string PartsSalesCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 品牌_Str
        /// </summary>
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 配件销售订单类型Id
        /// </summary>
        public int PartsSalesOrderTypeId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售订单类型Id
        /// </summary>
        public string PartsSalesOrderTypeIdStr {
            get;
            set;
        }
        /// <summary>
        /// 配件销售订单类型编号
        /// </summary>
        public string PartsSalesOrderTypeCode {
            get;
            set;
        }

        /// 订单类型名称_Str
        /// </summary>
        public string PartsSalesOrderTypeName {
            get;
            set;
        }

        /// <summary>
        /// 系数
        /// </summary>
        public double Coefficient {
            get;
            set;
        }
        /// <summary>
        /// 系数_str
        /// </summary>
        public string CoefficientStr {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }
    }
}
