﻿
using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class CustomerServiceEvaluateExtend {

        public int Id {
            get;
            set;
        }

        public int RepairClaimBillId {
            get;
            set;
        }

        public string RepairClaimBillCode {
            get;
            set;
        }


        public string EvaluateSource {
            get;
            set;
        }


        public int EvaluateGrade {
            get;
            set;
        }

        public string EvaluateGradeStr {
            get;
            set;
        }


        public string isContentFactor {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }

        public DateTime? EvaluateTime {
            get;
            set;
        }

        public string tempErrorMessage {
            get;
            set;
        }

        public string EvaluateTimeStr {
            get;
            set;
        }
        public int? CreatorId {
            get;
            set;
        }

        public string CreatorName {
            get;
            set;
        }

        public DateTime? CreateTime {
            get;
            set;
        }

        public string VIN {
            get;
            set;
        }


        public int PartsSalesCategoryId {
            get;
            set;
        }

        public string PartsSalesCategoryName {
            get;
            set;
        }



        public int MarketingDepartmentId {
            get;
            set;
        }


        public string MarketingDepartmentName {
            get;
            set;
        }


        public string DealerName {
            get;
            set;
        }


        public string DealerCode {
            get;
            set;
        }

        public int? RepairType {
            get;
            set;
        }

        public int? DisContentFactor {
            get;
            set;
        }


        public string DisContentFactorStr {
            get;
            set;
        }


        public string VehicleContactPerson {
            get;
            set;
        }


        public string ContactPhone {
            get;
            set;
        }

        public string VehicleLicensePlate {
            get;
            set;
        }

        public DateTime? RepairRequestTime {
            get;
            set;
        }

        public string EvaluationResults {
            get;
            set;
        }

        public string CustomOpinion {
            get;
            set;
        }
    }
}
