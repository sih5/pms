﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities
{
    public class CustomerSupplyInitialFeeSetExtend
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        /// ID_Str
        /// </summary>
        public string IdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌ID
        /// </summary>
        public int PartsSalesCategoryId
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌Id_Str
        /// </summary>
        public string PartsSalesCategoryIdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌名称
        /// </summary>
        public string PartsSalesCategoryName
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌名称_Str
        /// </summary>
        public string PartsSalesCategoryNameStr
        {
            get;
            set;
        }
        ///<summary>
        ///  客户ID  
        ///</summary>
        public int CustomerId
        {
            get;
            set;
        }
        ///<summary>
        ///  客户ID_Str  
        ///</summary>
        public string CustomerIdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 客户编号
        /// </summary>
        public string CustomerCode
        {
            get;
            set;
        }
        /// <summary>
        /// 客户编号_Str
        /// </summary>
        public string CustomerCodeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 客户名称
        /// </summary>
        public string CustomerName
        {
            get;
            set;
        }
        /// <summary>
        /// 客户名称_Str
        /// </summary>
        public string CustomerNameStr
        {
            get;
            set;
        }
        ///<summary>
        ///  供应商Id  
        ///</summary>
        public int SupplierId
        {
            get;
            set;
        }
        ///<summary>
        ///  供应商Id_Str  
        ///</summary>
        public string SupplierIdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierCode
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商编号_Str
        /// </summary>
        public string SupplierCodeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SupplierName
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称_Str
        /// </summary>
        public string SupplierNameStr
        {
            get;
            set;
        }
        /// <summary>
        /// 起订金额
        /// </summary>
        public decimal? InitialFee
        {
            get;
            set;
        }
        /// <summary>
        /// 起订金额_Str
        /// </summary>
        public string InitialFeeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        public int? Status
        {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr
        {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId
        {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName
        {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime
        {
            get;
            set;
        }
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg
        {
            get;
            set;
        }
        //约定发货时间
        public int AppointShippingDays {
            get;
            set;
        }
        //约定发货时间
        public string AppointShippingDaysStr {
            get;
            set;
        }
    }
}
