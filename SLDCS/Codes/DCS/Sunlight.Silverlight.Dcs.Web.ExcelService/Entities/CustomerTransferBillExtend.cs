﻿using System;
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class CustomerTransferBillExtend {
        /**
         * 业务类型
         * */
        public string BusinessTypeStr {
            get;
            set;
        }
        public int BusinessType {
            get;
            set;
        }
        public int OutboundAccountGroupId {
            get;
            set;
        }
        public int InboundAccountGroupId {
            get;
            set;
        }
        /**
        * 转出账户组
        * */
        public string OutboundAccountGroupName {
            get;
            set;
        }
        /**
         * 转出客户编号
         * */
        public string OutboundCustomerCompanyCode {
            get;
            set;
        }
        /**
         * 转出客户名称
         * */
        public string OutboundCustomerCompanyName {
            get;
            set;
        }
        /**
       * 转入账户组
       * */
        public string InboundAccountGroupName {
            get;
            set;
        }
        /**
         * 转入客户编号
         * */
        public string InboundCustomerCompanyCode {
            get;
            set;
        }
        /**
         * 转入客户名称
         * */
        public string InboundCustomerCompanyName {
            get;
            set;
        }
        /**
         * 金额
         * */
        public double Amount {
            get;
            set;
        }
        public string AmountStr {
            get;
            set;
        }
        /**
        * 摘要
        * */
        public string Summary {
            get;
            set;
        }
        /**
       * 过账日期
       * */
        public DateTime InvoiceDate {
            get;
            set;
        }
        public string InvoiceDateStr {
            get;
            set;
        }
        /**
         * 经办人
         * */
        public string Operator {
            get;
            set;
        }
        /**
         * 备注
         * */
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        public int OutboundCustomerCompanyId {
            get;
            set;
        }
        public int InboundCustomerCompanyId {
            get;
            set;
        }
    }
}