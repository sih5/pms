﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class Dealer {
        /// <summary>
        /// 主键Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 产品编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 产品名称
        /// </summary>
        public string Name {
            get;
            set;
        }
    }
}
