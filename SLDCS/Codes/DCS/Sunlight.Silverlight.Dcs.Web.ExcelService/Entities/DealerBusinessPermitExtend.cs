﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DealerBusinessPermitExtend {
        public string ErrorMsg {
            get;
            set;
        }
        public int? DealerId {
            get;
            set;
        }
        public string DealerCode {
            get;
            set;
        }
        public string DealerName {
            get;
            set;
        }
        public string PartsSalesCategoryName {
            get;
            set;
        }
        public int? ProductLineId {
            get;
            set;
        }
        public string ProductLineCode {
            get;
            set;
        }
        public string ProductLineName {
            get;
            set;
        }

        public string ProductLineTypeStr {
            get;
            set;
        }

        public int? PartsSalesCategoryId {
            get;
            set;
        }
        public int? BranchId {
            get;
            set;
        }

        public int? ProductLineType {
            get;
            set;
        }
        public int? LaborHourUnitPriceGradeId {
            get;
            set;
        }

        public string LaborHourUnitPriceGrade {
            get;
            set;
        }

        //发动机维修权限
        public string EngineRepairPowerStr {
            get;
            set;
        }

        public int? EngineRepairPower {
            get;
            set;
        }
    }
}
