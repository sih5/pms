﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DealerExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        public int InvoiceCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 对应的开票企业信息ID
        /// </summary>
        public int CompanyInvoiceInfoId {
            get;
            set;
        }

        /// <summary>
        /// 经销商编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 经销商编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 经销商名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 是否虚拟经销商
        /// </summary>
        public bool IsVirtualDealer {
            get;
            set;
        }
        /// <summary>
        /// 是否虚拟经销商_Str
        /// </summary>
        public string IsVirtualDealerStr {
            get;
            set;
        }

        /// <summary>
        /// 简称
        /// </summary>
        public string ShortName {
            get;
            set;
        }
        /// <summary>
        /// 简称_Str
        /// </summary>
        public string ShortNameStr {
            get;
            set;
        }

        /// <summary>
        /// 服务经理
        /// </summary>
        public string Manager {
            get;
            set;
        }
        /// <summary>
        /// 服务经理_Str
        /// </summary>
        public string ManagerStr {
            get;
            set;
        }

        /// <summary>
        /// MDM客户编码
        /// </summary>
        public string CustomerCode {
            get;
            set;
        }

        ///// <summary>
        ///// MDM客户编码_Str
        ///// </summary>
        //public string CustomerCodeStr {
        //    get;
        //    set;
        //}
        /// <summary>
        /// MDM供应商编码
        /// </summary>
        public string SupplierCode {
            get;
            set;
        }
        ///// <summary>
        ///// MDM供应商编码_Str
        ///// </summary>
        //public string SupplierCodeStr {
        //    get;
        //    set;
        //}
        /// <summary>
        /// 成立日期
        /// </summary>
        public DateTime? FoundDate {
            get;
            set;
        }
        /// <summary>
        /// 成立日期_Str
        /// </summary>
        public string FoundDateStr {
            get;
            set;
        }

        /// <summary>
        /// 省市县Id
        /// </summary>
        public int? RegionId {
            get;
            set;
        }

        /// <summary>
        /// 省
        /// </summary>
        public string ProvinceName {
            get;
            set;
        }
        ///// <summary>
        ///// 省_Str
        ///// </summary>
        //public string ProvinceNameStr {
        //    get;
        //    set;
        //}
        /// <summary>
        /// 市
        /// </summary>
        public string CityName {
            get;
            set;
        }
        ///// <summary>
        ///// 市_Str
        ///// </summary>
        //public string CityNameStr {
        //    get;
        //    set;
        //}
        /// <summary>
        /// 县
        /// </summary>
        public string CountyName {
            get;
            set;
        }
        ///// <summary>
        ///// 县_Str
        ///// </summary>
        //public string CountyNameStr {
        //    get;
        //    set;
        //}
        /// <summary>
        /// 企业城市级别
        /// </summary>
        public int CityLevel {
            get;
            set;
        }
        /// <summary>
        /// 企业城市级别_Str
        /// </summary>
        public string CityLevelStr {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }

        /// <summary>
        /// 企业联系人
        /// </summary>
        public string ContactPerson {
            get;
            set;
        }

        /// <summary>
        /// 企业固定电话
        /// </summary>
        public string ContactPhone {
            get;
            set;
        }
        /// <summary>
        /// 企业E-MAIL
        /// </summary>
        public string ContactMail {
            get;
            set;
        }
        /// <summary>
        /// 企业联系人手机
        /// </summary>
        public string ContactMobile {
            get;
            set;
        }
        /// <summary>
        /// 企业传真
        /// </summary>
        public string Fax {
            get;
            set;
        }
        /// <summary>
        /// 企业邮政编码
        /// </summary>
        public string ContactPostCode {
            get;
            set;
        }
        /// <summary>
        /// 企业营业地址
        /// </summary>
        public string BusinessAddress {
            get;
            set;
        }
        /// <summary>
        /// 企业注册证号
        /// </summary>
        public string RegisterCode {
            get;
            set;
        }
        /// <summary>
        /// 企业注册名称
        /// </summary>
        public string RegisterName {
            get;
            set;
        }
        /// <summary>
        /// 企业注册日期
        /// </summary>
        public DateTime? RegisterDate {
            get;
            set;
        }
        /// <summary>
        /// 企业注册日期_Str
        /// </summary>
        public string RegisterDateStr {
            get;
            set;
        }
        /// <summary>
        /// 企业法定代表人
        /// </summary>
        public string LegalRepresentative {
            get;
            set;
        }
        /// <summary>
        /// 企业注册资金
        /// </summary>
        public string RegisterCapital {
            get;
            set;
        }
        /// <summary>
        /// 企业固定资产
        /// </summary>
        public string FixedAsset {
            get;
            set;
        }
       

        /// <summary>
        /// 企业法人代表电话
        /// </summary>
        public string LegalRepresentTel {
            get;
            set;
        }
        /// <summary>
        /// 企业性质
        /// </summary>
        public int? CorporateNature {
            get;
            set;
        }
        /// <summary>
        /// 企业性质_Str
        /// </summary>
        public string CorporateNatureStr {
            get;
            set;
        }
        /// <summary>
        /// 经营范围
        /// </summary>
        public string BusinessScope {
            get;
            set;
        }
        /// <summary>
        /// 营业地址(经度)
        /// </summary>
        public decimal BusinessAddressLongitude {
            get;
            set;
        }
        /// <summary>
        /// 营业地址(经度)Str
        /// </summary>
        public string BusinessAddressLongitudeStr {
            get;
            set;
        }
        /// <summary>
        /// 营业地址(纬度)
        /// </summary>
        public decimal BusinessAddressLatitude {
            get;
            set;
        }
        /// <summary>
        ///营业地址(纬度)Str
        /// </summary>
        public string BusinessAddressLatitudeStr {
            get;
            set;
        }
        /// <summary>
        /// 企业注册地址
        /// </summary>
        public string RegisteredAddress {
            get;
            set;
        }
        /// <summary>
        /// 企业身份证件类型
        /// </summary>
        public int IdDocumentType {
            get;
            set;
        }
        /// <summary>
        /// 企业身份证件类型_Str
        /// </summary>
        public string IdDocumentTypeStr {
            get;
            set;
        }
        /// <summary>
        /// 企业身份证件号码
        /// </summary>
        public string IdDocumentNumber {
            get;
            set;
        }

        public int RepairQualification {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息维修资质
        /// </summary>
        public string RepairQualificationStr {
            get;
            set;
        }
        /// <summary>
        /// 企业开票信息税务登记号
        /// </summary>
        public string TaxRegisteredNumber {
            get;
            set;
        }
        /// <summary>
        /// 企业开票信息银行名称
        /// </summary>
        public string BankName {
            get;
            set;
        }
        /// <summary>
        /// 企业开票信息银行账号
        /// </summary>
        public string BankAccount {
            get;
            set;
        }

        /// <summary>
        /// 企业开票信息财务联系电话
        /// </summary>
        public string ContactNumber {
            get;
            set;
        }
        /// <summary>
        /// 企业开票信息税务登记地址
        /// </summary>
        public string TaxRegisteredAddress {
            get;
            set;
        }

        /// <summary>
        /// 经销商服务扩展信息隶属单位
        /// </summary>
        public string OwnerCompany {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息建厂时间
        /// </summary>
        public DateTime? BuildTime {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息建厂时间_Str
        /// </summary>
        public string BuildTimeStr {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息交通限行描述
        /// </summary>
        public string TrafficRestrictionsdescribe {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息主营范围
        /// </summary>
        public string MainBusinessAreas {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息兼营范围
        /// </summary>
        public string AndBusinessAreas {
            get;
            set;
        }

        /// <summary>
        /// 经销商服务扩展信息服务品牌
        /// 红岩授权品牌					
        /// </summary>
        public string BrandScope {
            get;
            set;
        } 

        /// <summary>
        /// 经销商服务扩展信息服务竞争品牌
        /// 非红岩授权品牌					
        /// </summary>
        public string CompetitiveBrandScope {
            get;
            set;
        } 

        /// <summary>
        /// 经销商服务扩展信息危险品运输车辆维修许可证
        /// </summary>
        public int DangerousRepairQualification {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息危险品运输车辆维修许可证_Str
        /// </summary>
        public string DangerousRepairQualificationStr {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息有无二级服务站
        /// </summary>
        public int HasBranch {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息有无二级服务站_Str
        /// </summary>
        public string HasBranchStr {
            get;
            set;
        } 
        /// <summary>
        /// 经销商服务扩展信息车辆行驶路线
        /// </summary>
        public int VehicleTravelRoute {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息车辆行驶路线_Str
        /// </summary>
        public string VehicleTravelRouteStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商服务扩展信息车辆使用特性
        /// </summary>
        public int VehicleUseSpeciality {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息车辆使用特性_Str
        /// </summary>
        public string VehicleUseSpecialityStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商服务扩展信息车辆停靠点
        /// </summary>
        public int VehicleDockingStation {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息车辆停靠点_Str
        /// </summary>
        public string VehicleDockingStationStr {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息员工人数
        /// 服务站人数					
        /// </summary>
        public int EmployeeNumber {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息员工人数_Str
        /// 服务站人数					
        /// </summary>
        public string EmployeeNumberStr {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息维修车间面积
        /// </summary>
        public string RepairingArea {
            get;
            set;
        }
        /// <summary>
        /// 经销商服务扩展信息接待室面积
        /// </summary>
        public string ReceptionRoomArea {
            get;
            set;
        }

        /// <summary>
        /// 经销商服务扩展信息停车场面积
        /// </summary>
        public string ParkingArea {
            get;
            set;
        }

        /// <summary>
        /// 经销商服务扩展信息配件库面积
        /// </summary>
        public string PartWarehouseArea {
            get;
            set;
        }


        /// <summary>
        /// 企业开票信息纳税人性质
        /// </summary>
        public int TaxpayerQualification {
            get;
            set;
        }

        /// <summary>
        /// 企业开票信息纳税人性质_Str
        /// </summary>
        public string TaxpayerQualificationStr {
            get;
            set;
        }
        /// <summary>
        /// 企业开票信息发票限额
        /// </summary>
        public string InvoiceAmountQuota {
            get;
            set;
        } 
        /// <summary>
        /// 企业开票信息开票名称
        /// </summary>
        public string InvoiceTitle {
            get;
            set;
        }
        /// <summary>
        /// 企业开票信息开票类型
        /// </summary>
        public int InvoiceType {
            get;
            set;
        }

        /// <summary>
        /// 企业开票信息开票类型_Str
        /// </summary>
        public string InvoiceTypeStr {
            get;
            set;
        }
        /// <summary>
        /// 企业开票信息开票税率
        /// <summary>
        public string InvoiceTax {
            get;
            set;
        }
        /// <summary>
        /// 企业开票信息财务联系人
        /// </summary>
        public string Linkman {
            get;
            set;
        }
        /// <summary>
        /// 企业开票信息财务传真
        /// </summary>
        public string AccountFax {
            get;
            set;
        }
        /// <summary>
        /// 站间距离
        /// </summary>
        public int Distance {
            get;
            set;
        }
        public string DistanceStr {
            get;
            set;
        }
        public string MinServiceCode {
            get;
            set;
        }
        public string MinServiceName {
            get;
            set;
        }

    }
}
