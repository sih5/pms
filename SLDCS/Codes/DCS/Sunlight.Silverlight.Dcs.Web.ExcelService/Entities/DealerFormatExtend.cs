﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DealerFormatExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        public int DealerId {
            get;
            set;
        }
        public string DealerCodeStr {
            get;
            set;
        }
        public string DealerCode {
            get;
            set;
        }
        public string DealerNameStr {
            get;
            set;
        }
        public string DealerName {
            get;
            set;
        }
        public string Format {
            get;
            set;
        }
        public string FormatStr {
            get;
            set;
        }
        public string Quarter {
            get;
            set;
        }
        public string QuarterStr {
            get;
            set;
        }
    }
}
