﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DealerMobileNumberListExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 经销商Id
        /// </summary>
        public int DealerId {
            get;
            set;
        }

        /// <summary>
        /// 经销商编号
        /// </summary>
        public string DealerCode {
            get;
            set;
        }

        /// <summary>
        /// 经销商名称
        /// </summary>
        public string DealerName {
            get;
            set;
        }

        /// <summary>
        /// 视频手机号
        /// </summary>
        public string VideoMobileNumber {
            get;
            set;
        }
    }
}
