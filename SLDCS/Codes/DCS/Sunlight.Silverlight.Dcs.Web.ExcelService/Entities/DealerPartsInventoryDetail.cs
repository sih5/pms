﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DealerPartsInventoryDetail {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商盘点单Id
        /// </summary>
        public int DealerPartsInventoryId {
            get;
            set;
        }
        /// <summary>
        /// 经销商盘点单Id_Str
        /// </summary>
        public string DealerPartsInventoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 当前库存
        /// </summary>
        public int CurrentStorage {
            get;
            set;
        }
        /// <summary>
        /// 当前库存_Str
        /// </summary>
        public string CurrentStorageStr {
            get;
            set;
        }

        /// <summary>
        /// 盘点后库存
        /// </summary>
        public int StorageAfterInventory {
            get;
            set;
        }
        /// <summary>
        /// 盘点后库存_Str
        /// </summary>
        public string StorageAfterInventoryStr {
            get;
            set;
        }

        /// <summary>
        /// 库存差异
        /// </summary>
        public int StorageDifference {
            get;
            set;
        }
        /// <summary>
        /// 库存差异_Str
        /// </summary>
        public string StorageDifferenceStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Memo {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string MemoStr {
            get;
            set;
        }
        /// <summary>
        /// 服务站价
        /// </summary>
        public decimal? DealerPrice {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string DealerPriceStr {
            get;
            set;
        }
    }
}
