﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DealerPartsRetailOrderExtend {
        /// <summary>
        /// 配件Id 
        /// </summary>
        public int PartsId {
            get;
            set;
        }
        /// <summary>
        /// 配件编号 
        /// </summary>
        public string PartsCode {
            get;
            set;
        }
        /// <summary>
        /// 配件名称 
        /// </summary>
        public string PartsName {
            get;
            set;
        }
        /// <summary>
        /// 数量Str
        /// </summary>
        public string QuantityStr {
            get;
            set;
        }
        /// <summary>
        /// 单价Str 
        /// </summary>
        public string PriceStr {
            get;
            set;
        }
        /// <summary>
        /// 数量 
        /// </summary>
        public int Quantity {
            get;
            set;
        }
        /// <summary>
        /// 单价 
        /// </summary>
        public decimal Price {
            get;
            set;
        }
        /// <summary>
        /// 备注 
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// 标签码
        /// </summary>
        public string SIHLabelCode {
            get;
            set;
        }
        /// <summary>
        /// 追溯属性
        /// </summary>
        public int? TraceProperty {
            get;
            set;
        }
    }
}
