﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DealerPartsStockExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 经销商Id
        /// </summary>
        public int DealerId {
            get;
            set;
        }

        /// <summary>
        /// 经销商编号
        /// </summary>
        public string DealerCode {
            get;
            set;
        }
        /// <summary>
        /// 经销商编号_Str
        /// </summary>
        public string DealerCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商名称
        /// </summary>
        public string DealerName {
            get;
            set;
        }
        /// <summary>
        /// 经销商名称_Str
        /// </summary>
        public string DealerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 二级站Id
        /// </summary>
        public int? SubDealerId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 销售类型Id
        /// </summary>
        public int SalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 销售类型Id_Str
        /// </summary>
        public string SalesCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 销售类型名称
        /// </summary>
        public string SalesCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 销售类型名称_Str
        /// </summary>
        public string SalesCategoryNameStr {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }

    }
}
