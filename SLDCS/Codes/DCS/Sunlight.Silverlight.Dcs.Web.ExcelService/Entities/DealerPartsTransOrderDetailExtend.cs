﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DealerPartsTransOrderDetailExtend {
        // 配件Id	
        public int SparePartId {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }

        //配件编号	 
        public string SparePartCode {
            get;
            set;
        }

        //配件名称	 
        public string SparePartName {
            get;
            set;
        }

        //调拨数量	
        public int Amount {
            get;
            set;
        }
        public string AmountStr {
            get;
            set;
        }


        //保内销售价格 
        public decimal? WarrantyPrice {
            get;
            set;
        }

        //保外销售价格
        public decimal? NoWarrantyPrice {
            get;
            set;
        }

        /// <summary>
        /// 配件库存
        /// </summary>
        public int Quantity {
            get;
            set;
        }
    }
}
