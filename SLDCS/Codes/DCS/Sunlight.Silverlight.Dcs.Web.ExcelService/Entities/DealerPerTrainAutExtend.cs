﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DealerPerTrainAutExtend {
        public string PartsSalesCategoryName {
            get;
            set;
        }

        public int BranchId {
            get;
            set;
        }

        public int PartsSalesCategoryId {
            get;
            set;
        }

        public int MarketingDepartmentId {
            get;
            set;
        }
        public int DealerId {
            get;
            set;
        }

        public string DealerCode {
            get;
            set;
        }
        public string DealerName {
            get;
            set;
        }
        public string Name {
            get;
            set;
        }
        public int PPartsSalesCategoryId {
            get;
            set;
        }
        public int PDealerId {
            get;
            set;
        }

        public int PositionId {
            get;
            set;
        }
        public int? TrainingType {
            get;
            set;
        }
        public DateTime? TrainingTime {
            get;
            set;
        }

        public string TrainingTimeStr {
            get;
            set;
        }

        public int? AuthenticationType {
            get;
            set;
        }
        public DateTime? AuthenticationTime {
            get;
            set;
        }

        public string AuthenticationTimeStr {
            get;
            set;
        }
        public int Status {
            get;
            set;
        }
        public DateTime CreateTime {
            get;
            set;
        }
        public int? CreatorId {
            get;
            set;
        }
        public string CreatorName {
            get;
            set;
        }
        public int? ModifierId {
            get;
            set;
        }
        public DateTime ModifyTime {
            get;
            set;
        }
        public string ModifierName {
            get;
            set;
        }

        public string MarketingDepartmentName {
            get;
            set;
        }

        public int MPartsSalesCategoryId {
            get;
            set;
        }

        public string PositionName {
            get;
            set;
        }

        public string TrainingTypeName {
            get;
            set;
        }

        public string AuthenticationTypeName {
            get;
            set;
        }
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        public string CertificateId {
            get;
            set;
        }
        public string TrainingName {
            get;
            set;
        }
        public string IdCard {
            get;
            set;
        }

    }
}
