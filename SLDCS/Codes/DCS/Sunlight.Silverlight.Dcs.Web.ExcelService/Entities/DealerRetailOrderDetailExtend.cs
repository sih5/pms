﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DealerRetailOrderDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber {
            get;
            set;
        }

        /// <summary>
        /// 服务站配件零售订单Id
        /// </summary>
        public int DealerPartsRetailOrderId {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int PartsId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string PartsCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string PartsCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string PartsName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string PartsNameStr {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }
        /// <summary>
        /// 数量_Str
        /// </summary>
        public string QuantityStr {
            get;
            set;
        }

        /// <summary>
        /// 单价
        /// </summary>
        public Decimal Price {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }
    }
}
