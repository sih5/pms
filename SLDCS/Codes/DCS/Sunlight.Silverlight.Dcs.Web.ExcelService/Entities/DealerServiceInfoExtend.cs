﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DealerServiceInfoExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }


        public int ExternalState {
            get;
            set;
        }

        public string ExternalStateStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id_Str
        /// </summary>
        public string PartsSalesCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商Id
        /// </summary>
        public int DealerId {
            get;
            set;
        }
        /// <summary>
        /// 经销商Id_Str
        /// </summary>
        public string DealerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商名称
        /// </summary>
        public string DealerName {
            get;
            set;
        }

        /// <summary>
        /// 市场部Id
        /// </summary>
        public int MarketingDepartmentId {
            get;
            set;
        }
        /// <summary>
        /// 市场部Id_Str
        /// </summary>
        public string MarketingDepartmentIdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件仓库Id
        /// </summary>
        public int UsedPartsWarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 旧件仓库Id_Str
        /// </summary>
        public string UsedPartsWarehouseIdStr {
            get;
            set;
        }

        /// <summary>
        /// 网络业务能力Id
        /// </summary>
        public int ChannelCapabilityId {
            get;
            set;
        }
        /// <summary>
        /// 网络业务能力Id_Str
        /// </summary>
        public string ChannelCapabilityIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件管理费率等级Id
        /// </summary>
        public int? PartsManagingFeeGradeId {
            get;
            set;
        }
        /// <summary>
        /// 配件管理费率等级Id_Str
        /// </summary>
        public string PartsManagingFeeGradeIdStr {
            get;
            set;
        }

        /// <summary>
        /// 星级系数Id
        /// </summary>
        public int? GradeCoefficientId {
            get;
            set;
        }
        /// <summary>
        /// 星级系数Id_Str
        /// </summary>
        public string GradeCoefficientIdStr {
            get;
            set;
        }

        /// <summary>
        /// 外出服务半径
        /// </summary>
        public int OutServiceradii {
            get;
            set;
        }
        /// <summary>
        /// 外出服务半径_Str
        /// </summary>
        public string OutServiceradiiStr {
            get;
            set;
        }

        /// <summary>
        /// 事业本部
        /// </summary>
        public string BusinessDivision {
            get;
            set;
        }
        /// <summary>
        /// 事业本部_Str
        /// </summary>
        public string BusinessDivisionStr {
            get;
            set;
        }

        /// <summary>
        /// 授权时间
        /// </summary>
        public DateTime? AccreditTime {
            get;
            set;
        }
        /// <summary>
        /// 授权时间_Str
        /// </summary>
        public string AccreditTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 维修权限
        /// </summary>
        public string ServicePermission {
            get;
            set;
        }
        /// <summary>
        /// 维修权限_Str
        /// </summary>
        public string ServicePermissionStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站类别
        /// </summary>
        public int? ServiceStationType {
            get;
            set;
        }
        /// <summary>
        /// 服务站类别_Str
        /// </summary>
        public string ServiceStationTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 区域
        /// </summary>
        public int? Area {
            get;
            set;
        }
        /// <summary>
        /// 区域_Str
        /// </summary>
        public string AreaStr {
            get;
            set;
        }

        /// <summary>
        /// 地区类别
        /// </summary>
        public int? RegionType {
            get;
            set;
        }
        /// <summary>
        /// 地区类别_Str
        /// </summary>
        public string RegionTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 材料费开票类型
        /// </summary>
        public int? MaterialCostInvoiceType {
            get;
            set;
        }
        /// <summary>
        /// 材料费开票类型_Str
        /// </summary>
        public string MaterialCostInvoiceTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 工时费开票类型
        /// </summary>
        public int? LaborCostCostInvoiceType {
            get;
            set;
        }
        /// <summary>
        /// 工时费开票类型_Str
        /// </summary>
        public string LaborCostCostInvoiceTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 工时费开票系数
        /// </summary>
        public Decimal? LaborCostInvoiceRatio {
            get;
            set;
        }
        /// <summary>
        /// 工时费开票系数_Str
        /// </summary>
        public string LaborCostInvoiceRatioStr {
            get;
            set;
        }

        /// <summary>
        /// 材料费开票系数
        /// </summary>
        public Decimal? InvoiceTypeInvoiceRatio {
            get;
            set;
        }
        /// <summary>
        /// 材料费开票系数_Str
        /// </summary>
        public string InvoiceTypeInvoiceRatioStr {
            get;
            set;
        }

        /// <summary>
        /// 解约时间
        /// </summary>
        public DateTime? CancellingDate {
            get;
            set;
        }
        /// <summary>
        /// 解约时间_Str
        /// </summary>
        public string CancellingDateStr {
            get;
            set;
        }

        /// <summary>
        /// 是否职守
        /// </summary>
        public bool IsOnDuty {
            get;
            set;
        }
        /// <summary>
        /// 是否职守_Str
        /// </summary>
        public string IsOnDutyStr {
            get;
            set;
        }

        /// <summary>
        /// 工时上浮系数
        /// </summary>
        public Decimal? LaborHourFloatCoefficient {
            get;
            set;
        }
        /// <summary>
        /// 工时上浮系数_Str
        /// </summary>
        public string LaborHourFloatCoefficientStr {
            get;
            set;
        }

        /// <summary>
        /// 上浮生效时间
        /// </summary>
        public DateTime? FloatValidFrom {
            get;
            set;
        }
        /// <summary>
        /// 上浮生效时间_Str
        /// </summary>
        public string FloatValidFromStr {
            get;
            set;
        }

        /// <summary>
        /// 上浮失效时间
        /// </summary>
        public DateTime? FloatValidTo {
            get;
            set;
        }
        /// <summary>
        /// 上浮失效时间_Str
        /// </summary>
        public string FloatValidToStr {
            get;
            set;
        }

        /// <summary>
        /// 外出服务费等级Id
        /// </summary>
        public int? OutFeeGradeId {
            get;
            set;
        }
        /// <summary>
        /// 外出服务费等级Id_Str
        /// </summary>
        public string OutFeeGradeIdStr {
            get;
            set;
        }

        /// <summary>
        /// 维修资质等级
        /// </summary>
        public int RepairAuthorityGrade {
            get;
            set;
        }
        /// <summary>
        /// 维修资质等级_Str
        /// </summary>
        public string RepairAuthorityGradeStr {
            get;
            set;
        }

        /// <summary>
        /// 24小时热线
        /// </summary>
        public string HotLine {
            get;
            set;
        }
        /// <summary>
        /// 24小时热线_Str
        /// </summary>
        public string HotLineStr {
            get;
            set;
        }

        /// <summary>
        /// 固定电话
        /// </summary>
        public string Fix {
            get;
            set;
        }
        /// <summary>
        /// 固定电话_Str
        /// </summary>
        public string FixStr {
            get;
            set;
        }

        /// <summary>
        /// 传真
        /// </summary>
        public string Fax {
            get;
            set;
        }
        /// <summary>
        /// 传真_Str
        /// </summary>
        public string FaxStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }

        /// <summary>
        /// 业务编号
        /// </summary>
        public string BusinessCode {
            get;
            set;
        }
        /// <summary>
        /// 业务名称
        /// </summary>
        public string BusinessName {
            get;
            set;
        }
        /// <summary>
        /// 简称
        /// </summary>
        public string ShortName {
            get;
            set;
        }
        /// <summary>
        /// 仓库IdStr
        /// </summary>
        public string WarehouseIdStr {
            get;
            set;
        }
        /// <summary>
        /// 仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 销售与服务场地布局关系
        /// </summary>
        public int SaleandServicesiteLayout {
            get;
            set;
        }
        /// <summary>
        /// 销售与服务场地布局关系Str
        /// </summary>
        public string SaleandServicesiteLayoutStr {
            get;
            set;
        }
        /// <summary>
        /// 配件储备金额Str
        /// </summary>
        public int PartReserveAmount {
            get;
            set;
        }
        /// <summary>
        /// 配件储备金额
        /// </summary>
        public string PartReserveAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 24小时手机号Str
        /// </summary>
        public string HourPhone24Str {
            get;
            set;
        }

        /// <summary>
        /// 24小时手机号
        /// </summary>
        public string HourPhone24 {
            get;
            set;
        }
        /// <summary>
        /// 主干网络类型Str
        /// </summary>
        public string TrunkNetworkTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 主干网络类型
        /// </summary>
        public int? TrunkNetworkType {
            get;
            set;
        }

        /// <summary>
        /// 是否中心站Str
        /// </summary>
        public string CenterStackStr {
            get;
            set;
        }

        /// <summary>
        /// 是否中心站
        /// </summary>
        public bool CenterStack {
            get;
            set;
        }
        public string Grade {
            get;
            set;
        }
        public string GradeStr {
            get;
            set;
        }
        public string DealerGradeName
        {
            get;
            set;
        }
        public int? DealerGradeid
        {
            get;
            set;
        }
    }
}
