﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class DeferredDiscountTypeExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 品牌
        /// </summary>
        public string PartsSalesCategory {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 延保产品Id
        /// </summary>
        public int ExtendedWarrantyProductId {
            get;
            set;
        }

        /// <summary>
        /// 延保产品Id
        /// </summary>
        public string ExtendedWarrantyProductIdStr {
            get;
            set;
        }
        /// <summary>
        /// 延保产品
        /// </summary>
        public string ExtendedWarrantyProductName {
            get;
            set;
        }
        /// <summary>
        /// 延保折扣类型
        /// </summary>
        public string ExtendedWarrantyAgioType {
            get;
            set;
        }
        /// <summary>
        /// 延保折扣
        /// </summary>
        public string ExtendedWarrantyAgio {
            get;
            set;
        }
        /// <summary>
        /// 品牌_Str
        /// </summary>
        public string PartsSalesCategoryStr {
            get;
            set;
        }
        /// <summary>
        /// 延保产品_Str
        /// </summary>
        public string ExtendedWarrantyProductNameStr {
            get;
            set;
        }

        /// <summary>
        /// 延保产品编号
        /// </summary>
        public string ExtendedWarrantyProductCode {
            get;
            set;
        }
        /// <summary>
        /// 延保折扣类型_Str
        /// </summary>
        public string ExtendedWarrantyAgioTypeStr {
            get;
            set;
        }
        /// <summary>
        /// 延保折扣_Str
        /// </summary>
        public string ExtendedWarrantyAgioStr {
            get;
            set;
        }
    }
}
