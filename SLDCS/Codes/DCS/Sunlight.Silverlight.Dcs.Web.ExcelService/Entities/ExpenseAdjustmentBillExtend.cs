﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ExpenseAdjustmentBillExtend {
        /// <summary>
        /// 配件销售类型名称_Str
        /// </summary>
        public string PartsSalesCategoryNameStr {
            get;
            set;
        }


        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int GradeCoefficientStatus {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 扣补款单编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 扣补款单编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 结算状态
        /// </summary>
        public int SettlementStatus {
            get;
            set;
        }
        /// <summary>
        /// 结算状态_Str
        /// </summary>
        public string SettlementStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 扣补款方向
        /// </summary>
        public int DebitOrReplenish {
            get;
            set;
        }
        /// <summary>
        /// 扣补款方向_Str
        /// </summary>
        public string DebitOrReplenishStr {
            get;
            set;
        }

        /// <summary>
        /// 扣补款类型
        /// </summary>
        public int TransactionCategory {
            get;
            set;
        }
        /// <summary>
        /// 扣补款类型_Str
        /// </summary>
        public string TransactionCategoryStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id_Str
        /// </summary>
        public string PartsSalesCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站Id
        /// </summary>
        public int DealerId {
            get;
            set;
        }
        /// <summary>
        /// 服务站Id_Str
        /// </summary>
        public string DealerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号
        /// </summary>
        public string DealerCode {
            get;
            set;
        }
        /// <summary>
        /// 服务站编号_Str
        /// </summary>
        public string DealerCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称
        /// </summary>
        public string DealerName {
            get;
            set;
        }
        /// <summary>
        /// 服务站名称_Str
        /// </summary>
        public string DealerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线名称
        /// </summary>
        public string ServiceProductLineName {
            get;
            set;
        }
        /// <summary>
        /// 服务产品线名称_Str
        /// </summary>
        public string ServiceProductLineNameStr {
            get;
            set;
        }
        /// <summary>
        /// 服务产品线Id
        /// </summary>
        public int ServiceProductLineId {
            get;
            set;
        }
        /// <summary>
        /// 服务产品线类型
        /// </summary>
        public int ServiceProductLineType {
            get;
            set;
        }

        /// <summary>
        /// 源单据类型
        /// </summary>
        public int? SourceType {
            get;
            set;
        }
        /// <summary>
        /// 源单据类型_Str
        /// </summary>
        public string SourceTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 源单据Id
        /// </summary>
        public int? SourceId {
            get;
            set;
        }
        /// <summary>
        /// 源单据Id_Str
        /// </summary>
        public string SourceIdStr {
            get;
            set;
        }

        /// <summary>
        /// 星级系数id
        /// </summary>
        public int? GradeCoefficientId {
            get;
            set;
        }

        /// <summary>
        /// 源单据编号
        /// </summary>
        public string SourceCode {
            get;
            set;
        }
        /// <summary>
        /// 源单据编号_Str
        /// </summary>
        public string SourceCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站联系人
        /// </summary>
        public string DealerContactPerson {
            get;
            set;
        }
        /// <summary>
        /// 服务站联系人_Str
        /// </summary>
        public string DealerContactPersonStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站联系电话
        /// </summary>
        public string DealerPhoneNumber {
            get;
            set;
        }
        /// <summary>
        /// 服务站联系电话_Str
        /// </summary>
        public string DealerPhoneNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 扣补款金额
        /// </summary>
        public Decimal TransactionAmount {
            get;
            set;
        }
        /// <summary>
        /// 扣补款金额_Str
        /// </summary>
        public string TransactionAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 扣补款原因
        /// </summary>
        public string TransactionReason {
            get;
            set;
        }
        /// <summary>
        /// 扣补款原因_Str
        /// </summary>
        public string TransactionReasonStr {
            get;
            set;
        }

        /// <summary>
        /// 是否向责任单位索赔
        /// </summary>
        public bool IfClaimToResponsible {
            get;
            set;
        }
        /// <summary>
        /// 是否向责任单位索赔_Str
        /// </summary>
        public string IfClaimToResponsibleStr {
            get;
            set;
        }

        /// <summary>
        /// 责任单位Id
        /// </summary>
        public int? ResponsibleUnitId {
            get;
            set;
        }
        /// <summary>
        /// 责任单位Id_Str
        /// </summary>
        public string ResponsibleUnitIdStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id_Str
        /// </summary>
        public string AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废人_Str
        /// </summary>
        public string AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// 作废时间_Str
        /// </summary>
        public string AbandonTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 审批人Id
        /// </summary>
        public int? ApproverId {
            get;
            set;
        }
        /// <summary>
        /// 审批人Id_Str
        /// </summary>
        public string ApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 审批人
        /// </summary>
        public string ApproverName {
            get;
            set;
        }
        /// <summary>
        /// 审批人_Str
        /// </summary>
        public string ApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 审批时间
        /// </summary>
        public DateTime? ApproveTime {
            get;
            set;
        }
        /// <summary>
        /// 审批时间_Str
        /// </summary>
        public string ApproveTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }
    }
}
