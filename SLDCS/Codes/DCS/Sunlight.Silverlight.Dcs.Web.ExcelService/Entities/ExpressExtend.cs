﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class ExpressExtend {
        /// <summary> 
        /// Id 
        /// </summary> 
        public int Id {
            get;
            set;
        }

        public string ExpressCode {
            get;
            set;
        }

        public string ExpressName {
            get;
            set;
        }

        public string Contacts {
            get;
            set;
        }

        public string ContactsNumber {
            get;
            set;
        }

        public string FixedTelephone {
            get;
            set;
        }
        public string E_Mail {
            get;
            set;
        }
        public string Address {
            get;
            set;
        }
        public int Status {
            get;
            set;
        }
        public int CreatorId {
            get;
            set;
        }
        public string CreatorName {
            get;
            set;
        }

        public DateTime? CreateTime {
            get;
            set;
        }

        public int ModifierId {
            get;
            set;
        }
        public string ModifierName {
            get;
            set;
        }

        public DateTime? ModifyTime {
            get;
            set;
        }


        public string ExpressCodeStr {
            get;
            set;
        }

        public string ExpressNameStr {
            get;
            set;
        }
        public string AddressStr {
            get;
            set;
        }

        public string ContactsStr {
            get;
            set;
        }

        public string ContactsNumberStr {
            get;
            set;
        }

        public string FixedTelephoneStr {
            get;
            set;
        }
        public string E_MailStr {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }
    }
}
