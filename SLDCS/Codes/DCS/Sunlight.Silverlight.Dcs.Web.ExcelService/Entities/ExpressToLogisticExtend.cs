﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class ExpressToLogisticExtend {
        /// <summary> 
        /// Id 
        /// </summary> 
        public int Id {
            get;
            set;
        }

        public string ExpressCode {
            get;
            set;
        }
        public int ExpressId {
            get;
            set;
        }
        public string ExpressName {
            get;
            set;
        }
        public int FocufingCoreId {
            get;
            set;
        }
        public string FocufingCoreCode {
            get;
            set;
        }
        public string FocufingCoreName {
            get;
            set;
        }

        public string LogisticsCompanyCode {
            get;
            set;
        }
        public int LogisticsCompanyId {
            get;
            set;
        }
        public string LogisticsCompanyName {
            get;
            set;
        }
        public string ExpressCodeStr {
            get;
            set;
        }

        public string FocufingCoreNameStr {
            get;
            set;
        }

        public string LogisticsCompanyCodeStr {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }

    }
}
