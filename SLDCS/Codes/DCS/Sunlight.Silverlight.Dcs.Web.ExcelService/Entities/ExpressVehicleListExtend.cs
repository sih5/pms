﻿
using System;
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ExpressVehicleListExtend {
        public string ErrorMsg {
            get;
            set;
        }
        public string VIN {
            get;
            set;
        }
        public string VINStr {
            get;
            set;
        }
        public int VehicleId {
            get;
            set;
        }

        public string VehicleCode {
            get;
            set;
        }

        public string FaultyMileageStr {
            get;
            set;
        }

        public float? FaultyMileage {
            get;
            set;
        }
  
        public DateTime? FaultTime {
            get;
            set;
        }
        public string FaultTimeStr {
            get;
            set;
        }
        public string BatchNumber {
            get;
            set;
        }
        public int PartsSalesCategoryId {
            get;
            set;
        }

        public string PartsSalesCategoryName {
            get;
            set;
        }

        public DateTime? OutOfFactoryDate {
            get;
            set;
        }


        public DateTime? SalesDate {
            get;
            set;
        }
    }
}
