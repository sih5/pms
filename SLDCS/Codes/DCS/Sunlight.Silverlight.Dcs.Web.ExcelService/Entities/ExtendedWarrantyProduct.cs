﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ExtendedWarrantyProduct {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 延保产品编号
        /// </summary>
        public string ExtendedWarrantyProductCode {
            get;
            set;
        }
        /// <summary>
        /// 延保产品名称
        /// </summary>
        public string ExtendedWarrantyProductName {
            get;
            set;
        }
        /// <summary>
        /// 品牌Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 品牌
        /// </summary>
        public string PartsSalesCategory {
            get;
            set;
        }
        /// <summary>
        /// 品牌Id
        /// </summary>
        public int ServiceProductLineId {
            get;
            set;
        }
        /// <summary>
        /// 品牌
        /// </summary>
        public string ServiceProductLine {
            get;
            set;
        }
         
    }
}
