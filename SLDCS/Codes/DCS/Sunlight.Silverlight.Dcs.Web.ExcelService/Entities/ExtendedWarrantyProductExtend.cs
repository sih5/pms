﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ExtendedWarrantyProductExtend {

        public string PartsSalesCategory {
            get;
            set;
        }

        public int? PartsSalesCategoryId {
            get;
            set;
        }

        public string ServiceProductLine {
            get;
            set;
        }

        public int? ServiceProductLineId {
            get;
            set;
        }

        public string ExtendedWarrantyProductName {
            get;
            set;
        }

        public int? ExtendedWarrantyTerm {
            get;
            set;
        }

        public string ExtendedWarrantyTermStr {
            get;
            set;
        }

        public int? ExtendedWarrantyMileage {
            get;
            set;
        }

        public string ExtendedWarrantyMileageStr {
            get;
            set;
        }

        public int? ExtendedWarrantyTermRat {
            get;
            set;
        }

        public string ExtendedWarrantyTermRatStr {
            get;
            set;
        }

        public int? ExtendedWarrantyMileageRat {
            get;
            set;
        }

        public string ExtendedWarrantyMileageRatStr {
            get;
            set;
        }

        public decimal? CustomerRetailPrice {
            get;
            set;
        }

        public string CustomerRetailPriceStr {
            get;
            set;
        }

        public decimal? TradePrice {
            get;
            set;
        }

        public string TradePriceStr {
            get;
            set;
        }

        public int? PayStartTimeCondition {
            get;
            set;
        }

        public string PayStartTimeConditionStr {
            get;
            set;
        }

        public int? PayEndTimeCondition {
            get;
            set;
        }

        public string PayEndTimeConditionStr {
            get;
            set;
        }

        public int? PayStartTimeMileage {
            get;
            set;
        }

        public string PayStartTimeMileageStr {
            get;
            set;
        }

        public int? PayEndTimeMileage {
            get;
            set;
        }

        public string PayEndTimeMileageStr {
            get;
            set;
        }

        public int? AutomaticAuditPeriod {
            get;
            set;
        }

        public string AutomaticAuditPeriodStr {
            get;
            set;
        }

        public string ExtensionCoverage {
            get;
            set;
        }

        public string OutExtensionCoverage {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }
    }
}
