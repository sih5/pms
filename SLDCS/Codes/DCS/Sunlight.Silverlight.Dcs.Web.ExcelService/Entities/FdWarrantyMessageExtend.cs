﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class FdWarrantyMessageExtend {
        public DateTime? Gengxinshijian {
            get;
            set;
        }
        public string Fengongsibianhao {
            get;
            set;
        }
        public string Fengongsimingcheng {
            get;
            set;
        }
        public string Suopeidanbianhao {
            get;
            set;
        }
        public string Danjuzhuangtai {
            get;
            set;
        }
        public string Weixiuleixing {
            get;
            set;
        }
        public string Vin {
            get;
            set;
        }
        public string Chuchangbianhao {
            get;
            set;
        }
        public string Pinpai {
            get;
            set;
        }
        public string Zhengchechanpinxian {
            get;
            set;
        }
        public string Fuwuchanpinxian {
            get;
            set;
        }
        public string Chanpinxianleixing {
            get;
            set;
        }
        public DateTime? Chuchangriqi {
            get;
            set;
        }
        public DateTime? Goumairiqi {
            get;
            set;
        }
        public int? Xingshilicheng {
            get;
            set;
        }
        public int? Shouciguzhanglicheng {
            get;
            set;
        }
        public string Gongkuangleibie {
            get;
            set;
        }
        public string Qudongxingshi {
            get;
            set;
        }
        public string Chexing {
            get;
            set;
        }
        public string Gonggaohao {
            get;
            set;
        }
        public string Fadongjixinghao {
            get;
            set;
        }
        public string Fadongjibianhao {
            get;
            set;
        }
        public string Shichangbu {
            get;
            set;
        }
        public string Fuwuzhanbianhao {
            get;
            set;
        }
        public string Fuwuzhanmingcheng {
            get;
            set;
        }
        public string Yewubianhao {
            get;
            set;
        }
        public string Chelianglianxiren {
            get;
            set;
        }
        public string Chanpinbianhao {
            get;
            set;
        }
        public DateTime? Baoxiushijian {
            get;
            set;
        }
        public DateTime? Chuangjianriqi {
            get;
            set;
        }
        public string Guzhangdaima {
            get;
            set;
        }
        public string Guzhangxianxiangmiaoshu {
            get;
            set;
        }
        public string Guzhangmoshi {
            get;
            set;
        }
        public string Huoshoujiantuhao {
            get;
            set;
        }
        public string Huoshoujianmingcheng {
            get;
            set;
        }
        public string Huoshoujianshengchanchangjia {
            get;
            set;
        }
        public string Zerendanweibianma {
            get;
            set;
        }
        public string Zerendanweimingcheng {
            get;
            set;
        }
        public string Huoshoujiansuoshuzongcheng {
            get;
            set;
        }
        public string Peijiansuoshuzongcheng {
            get;
            set;
        }
        public string Gongyingshangquerenzhuangtai {
            get;
            set;
        }
        public DateTime? Fengongsishenheshijian {
            get;
            set;
        }
        public string Zhongshenyijian {
            get;
            set;
        }
        public DateTime? Zhongshenshijian {
            get;
            set;
        }
        public string Zhongshenren {
            get;
            set;
        }
        public string Gongyingshangquerenyijian {
            get;
            set;
        }
        public string Iszerengongyingshangsuopei {
            get;
            set;
        }
        public string Weixiusuopeishenqingdanhao {
            get;
            set;
        }
        public string Weixiusuopeishenqingleixing {
            get;
            set;
        }
        public string Waichusuopeidanbianhao {
            get;
            set;
        }
        public string Waichusuopeidanzhuangtai {
            get;
            set;
        }
        public string Waichusuopeishenqingdanhao {
            get;
            set;
        }
        public int? Waichurenshu {
            get;
            set;
        }
        public int? Waichutianshu {
            get;
            set;
        }
        public int? Waichulicheng {
            get;
            set;
        }
        public decimal? Waichuchefeidanjia {
            get;
            set;
        }
        public decimal? Waichulutubuzhudanjia {
            get;
            set;
        }
        public string Waichuleixing {
            get;
            set;
        }
        public string Waichuyuanyin {
            get;
            set;
        }
        public string Waichushenheyijian {
            get;
            set;
        }
        public string Suopeidanqitafeiyongshuoming {
            get;
            set;
        }
        public string Waichuqitafeiyongshuoming {
            get;
            set;
        }
        public string Guzhangbeizhu {
            get;
            set;
        }
        public string Jiesuanzhuangtai {
            get;
            set;
        }
        public DateTime? Jiesuanshijian {
            get;
            set;
        }
        public decimal? Cailiaofei {
            get;
            set;
        }
        public decimal? Gongshifei {
            get;
            set;
        }
        public decimal? Waichufuwufei {
            get;
            set;
        }
        public decimal? Peijianguanlifei {
            get;
            set;
        }
        public decimal? Guzhangjianqingtuiyunfei {
            get;
            set;
        }
        public decimal? Qitafeiyong {
            get;
            set;
        }
        public decimal? Feiyongheji {
            get;
            set;
        }
        public decimal? Gongshidinge {
            get;
            set;
        }
        public decimal? Gongshidanjia {
            get;
            set;
        }
        public string Fuwuhuodongbianhao {
            get;
            set;
        }
        public string Fuwuhuodongmingcheng {
            get;
            set;
        }
        public string Fuwuhuodongleixing {
            get;
            set;
        }
        public string Fuwuhuodongneirong {
            get;
            set;
        }
        public string Kehuleixing {
            get;
            set;
        }
        public string Feiyongfenlei {
            get;
            set;
        }
        public string Wenjianbianhao {
            get;
            set;
        }
        public string Baogaojianshu {
            get;
            set;
        }
        public string Shichangbuyijian {
            get;
            set;
        }
        public string Pizhunren {
            get;
            set;
        }
        public decimal? Shejijine {
            get;
            set;
        }
        public string Zhiliangbuyijian {
            get;
            set;
        }
        public string Kuaibaodaihao {
            get;
            set;
        }
        public string Shejicheliang {
            get;
            set;
        }
        public string Wentimiaoshu {
            get;
            set;
        }
        public string Zhenggaiweixiufangan {
            get;
            set;
        }
        public string Suopeidanwei {
            get;
            set;
        }
        public string Isshiyebusuopei {
            get;
            set;
        }
        public string Zongchengjianmingcheng {
            get;
            set;
        }
        public string ShichangABkuaibaobianhao {
            get;
            set;
        }
        public string Weixiusuopeidanbianhao {
            get;
            set;
        }
        public string Zhuangtai {
            get;
            set;
        }
        public string Lianxidianhua {
            get;
            set;
        }
        public decimal? Koukuanjine {
            get;
            set;
        }
        public decimal? Bukuanjine {
            get;
            set;
        }
        public string Suopeidanweishiyebubianma {
            get;
            set;
        }
        public string Suopeidanweishiyebumingcheng {
            get;
            set;
        }
        public int? Waichugonglishu {
            get;
            set;
        }
        public decimal? Gaosugonglutuochefei {
            get;
            set;
        }
        public string Waidijiuyuandi {
            get;
            set;
        }
        public string Chepaihao {
            get;
            set;
        }
        public string Shendanleixing {
            get;
            set;
        }
        public string Suopeigongyingshangbianhao {
            get;
            set;
        }
        public string Suopeigongyingshangmingcheng {
            get;
            set;
        }
        public DateTime? Chuangjianshijian {
            get;
            set;
        }
        public string Kehudizhi {
            get;
            set;
        }
        public string Weixiushuxing {
            get;
            set;
        }
        public string Waichusuopeibeizhuxinxi {
            get;
            set;
        }
        public string Fuwuzhanzhucezhenghao {
            get;
            set;
        }
        public string Fuwuzhanzhucemingcheng {
            get;
            set;
        }
        public string Fuwuzhanyouzhengbianma {
            get;
            set;
        }
        public string Fuwuzhangudingdianhua {
            get;
            set;
        }
        public string Fuwuzhanchuanzhen {
            get;
            set;
        }
        public string Fuwuzhanqiyeleixing {
            get;
            set;
        }
        public string Fuwuzhanshengbianma {
            get;
            set;
        }
        public string Fuwuzhanshengmingcheng {
            get;
            set;
        }
        public string Fuwuzhanshibianma {
            get;
            set;
        }
        public string Fuwuzhanshimingcheng {
            get;
            set;
        }
        public string Fuwuzhanqubianma {
            get;
            set;
        }
        public string Fuwuzhanqumingcheng {
            get;
            set;
        }
        public string Fuwuzhandizhi {
            get;
            set;
        }
        public string Fuwuzhanlianxiren {
            get;
            set;
        }
        public string Fuwuzhandianziyouxiang {
            get;
            set;
        }
        public string Fuwuzhanjingli {
            get;
            set;
        }
        public DateTime? Fuwuzhanchengliriqi {
            get;
            set;
        }
        public string Fuwuzhanzhuangtai {
            get;
            set;
        }
        public string Erjifuwuzhanbianhao {
            get;
            set;
        }
        public string Erjifuwuzhanmingcheng {
            get;
            set;
        }
        public string Erjifuwuzhangudingdianhua {
            get;
            set;
        }
        public string Erjifuwuzhanqiyeleixing {
            get;
            set;
        }
        public string Erjifuwuzhandizhi {
            get;
            set;
        }
        public string Erjifuwuzhandianziyouxiang {
            get;
            set;
        }
        public string Erjifuwuzhanjingli {
            get;
            set;
        }
        public string Erjifuwuzhanzhuangtai {
            get;
            set;
        }
        public int? Status {
            get;
            set;
        }
        public int? CreatorId {
            get;
            set;
        }
        public string CreatorName {
            get;
            set;
        }
        public DateTime? CreateTime {
            get;
            set;
        }
        public DateTime? ModifyTime {
            get;
            set;
        }
        public int? ModifierId {
            get;
            set;
        }
        public string ModifierName {
            get;
            set;
        }
        public string GengxinshijianErrorStr {
            get;
            set;
        }
        public string ChuchangriqiErrorStr {
            get;
            set;
        }
        public string GoumairiqiErrorStr {
            get;
            set;
        }
        public string BaoxiushijianErrorStr {
            get;
            set;
        }
        public string ChuangjianriqiErrorStr {
            get;
            set;
        }
        public string FengongsishenheshijianErrorStr {
            get;
            set;
        }
        public string ZhongshenshijianErrorStr {
            get;
            set;
        }
        public string JiesuanshijianErrorStr {
            get;
            set;
        }
        public string ChuangjianshijianErrorStr {
            get;
            set;
        }
        public string FuwuzhanchengliriqiErrorStr {
            get;
            set;
        }
        public string CreateTimeErrorStr {
            get;
            set;
        }
        public string ModifyTimeErrorStr {
            get;
            set;
        }
        public string StatusErrorStr {
            get;
            set;
        }
        public string CreatorIdErrorStr {
            get;
            set;
        }
        public string ModifierIdErrorStr {
            get;
            set;
        }
        public string XingshilichengErrorStr {
            get;
            set;
        }
        public string ShouciguzhanglichengErrorStr {
            get;
            set;
        }
        public string WaichurenshuErrorStr {
            get;
            set;
        }
        public string WaichutianshuErrorStr {
            get;
            set;
        }
        public string WaichulichengErrorStr {
            get;
            set;
        }
        public string WaichuchefeidanjiaErrorStr {
            get;
            set;
        }
        public string WaichulutubuzhudanjiaErrorStr {
            get;
            set;
        }
        public string CailiaofeiErrorStr {
            get;
            set;
        }
        public string GongshifeiErrorStr {
            get;
            set;
        }
        public string WaichufuwufeiErrorStr {
            get;
            set;
        }
        public string PeijianguanlifeiErrorStr {
            get;
            set;
        }
        public string GuzhangjianqingtuiyunfeiErrorStr {
            get;
            set;
        }
        public string QitafeiyongErrorStr {
            get;
            set;
        }
        public string FeiyonghejiErrorStr {
            get;
            set;
        }
        public string GongshidingeErrorStr {
            get;
            set;
        }
        public string GongshidanjiaErrorStr {
            get;
            set;
        }
        public string ShejijineErrorStr {
            get;
            set;
        }
        public string KoukuanjineErrorStr {
            get;
            set;
        }
        public string BukuanjineErrorStr {
            get;
            set;
        }
        public string WaichugonglishuErrorStr {
            get;
            set;
        }
        public string GaosugonglutuochefeiErrorStr {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }
    }
}