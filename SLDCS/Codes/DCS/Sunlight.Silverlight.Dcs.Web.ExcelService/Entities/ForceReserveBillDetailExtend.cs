﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ForceReserveBillDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }
        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }
        /// <summary>
        /// 企业编号
        /// </summary>
        public string CompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 企业编号_Str
        /// </summary>
        public string CompanyCodeStr {
            get;
            set;
        }
        /// <summary>
        /// 企业编号
        /// </summary>
        public string CompanyName {
            get;
            set;
        }
        /// <summary>
        /// 企业编号_Str
        /// </summary>
        public string CompanyNameStr {
            get;
            set;
        }
        /// <summary>
        /// 储备数量
        /// </summary>
        public int ForceReserveQty {
            get;
            set;
        }
        /// <summary>
        /// 储备数量
        /// </summary>
        public string ForceReserveQtyStr {
            get;
            set;
        }
        public int SparePartId {
            get;
            set;
        }
        public int CompanyId {
            get;
            set;
        }
        public Decimal? CenterPrice {
            get;
            set;
        }
        public int CenterPartProperty {
            get;
            set;
        }
    }
}
