﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class FundRedPostVehicleMsgExtend {
        public string RedPostCode {
            get;
            set;
        }

        public string VIN {
            get;
            set;
        }

        public string BrandName {
            get;
            set;
        }

        public string RedPostName {
            get;
            set;
        }

        public string RedPostDescribe {
            get;
            set;
        }

        public string CustomerName {
            get;
            set;
        }

        public string CellPhoneNumber {
            get;
            set;
        }

        public decimal RedPostAmount {
            get;
            set;
        }

        public decimal LimitPrice {
            get;
            set;
        }

        public DateTime ValidationDate {
            get;
            set;
        }

        public DateTime ExpireDate {
            get;
            set;
        }

        public string RedPostAmountStr {
            get;
            set;
        }

        public string LimitPriceStr {
            get;
            set;
        }

        public string ValidationDateStr {
            get;
            set;
        }

        public string ExpireDateStr {
            get;
            set;
        }

        public int LssuingUnit {
            get;
            set;
        }
        public string LssuingUnitStr {
            get;
            set;
        }
        public string Status {
            get;
            set;
        }

        public string RepairContractCode {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }
    }
}