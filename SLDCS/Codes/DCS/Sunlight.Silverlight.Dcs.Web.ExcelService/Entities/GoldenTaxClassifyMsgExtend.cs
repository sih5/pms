﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class GoldenTaxClassifyMsgExtend {
        /// <summary> 
        /// Id 
        /// </summary> 
        public int Id {
            get;
            set;
        }
        public string GoldenTaxClassifyCode {
            get;
            set;
        }

        public string GoldenTaxClassifyName {
            get;
            set;
        }

        public string GoldenTaxClassifyCodeStr {
            get;
            set;
        }

        public string GoldenTaxClassifyNameStr {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }
    }
}
