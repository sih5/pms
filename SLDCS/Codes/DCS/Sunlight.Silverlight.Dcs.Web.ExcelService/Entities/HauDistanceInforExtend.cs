﻿using System;


namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class HauDistanceInforExtend {
        public int Id {
            get;
            set;
        }

        public int StorageCompanyId {
            get;
            set;
        }
        public string StorageCompanyCode {
            get;
            set;
        }
        public string StorageCompanyName {
            get;
            set;
        }
        public int CompanyId {
            get;
            set;
        }
        public string CompanyCode {
            get;
            set;
        }
        public string CompanyName {
            get;
            set;
        }
        public int CompanyAddressId {
            get;
            set;
        }
        public string DetailAddress {
            get;
            set;
        }
        public int WarehouseId {
            get;
            set;
        }
        public string WarehouseName {
            get;
            set;
        }
        public int HauDistance {
            get;
            set;
        }
        public string HauDistanceStr {
            get;
            set;
        }
        public int Status {
            get;
            set;
        }
        public int CreatorId {
            get;
            set;
        }
        public string CreatorName {
            get;
            set;
        }
        public DateTime CreateTime {
            get;
            set;
        }
        public int ModifierId {
            get;
            set;
        }
        public string ModifierName {
            get;
            set;
        }
        public DateTime ModifyTime {
            get;
            set;
        }
        public int AbandonerId {
            get;
            set;
        }
        public string AbandonerName {
            get;
            set;
        }
        public DateTime AbandonTime {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }
    }
}
