﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ImpOutboundAndInboundBill {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 单据编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 单据类型
        /// </summary>
        public int Type {
            get;
            set;
        }
        
        /// <summary>
        /// 结算金额
        /// </summary>
        public decimal SettlementAmount {
            get;
            set;
        }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal CostAmount {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业Id
        /// </summary>
        public int StorageCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业Id
        /// </summary>
        public int CounterpartCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 入库类型
        /// </summary>
        public int InboundType {
            get;
            set;
        }
        /// <summary>
        /// 仓库id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string WarehouseName {
            get;
            set;
        }
        /// <summary>
        /// 退货原因
        /// </summary>
        public string ReturnReason {
            get;
            set;
        }
        /// <summary>
        /// 原始需求单据编号
        /// </summary>
        public string OriginalRequirementBillCode {
            get;
            set;
        }

        public DateTime CreateTime {
            get;
            set;
        } 
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }


    }
}
