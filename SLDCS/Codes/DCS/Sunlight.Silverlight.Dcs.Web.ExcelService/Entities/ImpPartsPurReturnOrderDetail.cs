﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ImpPartsPurReturnOrderDetail {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 配件采购退货单Id
        /// </summary>
        public int PartsPurReturnOrderId {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 供应商图号
        /// </summary>
        public string SupplierPartCode {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 批次号
        /// </summary>
        public string BatchNumber {
            get;
            set;
        }

        /// <summary>
        /// 单价
        /// </summary>
        public Decimal UnitPrice {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
    }
}
