﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ImpPartsPurchaseOrderDetail {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 采购订单Id
        /// </summary>
        public int PartsPurchaseOrderId {
            get;
            set;
        }

        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 供应商Id
        /// </summary>
        public int SupplierPartId {
            get;
            set;
        }

        /// <summary>
        /// 供应商图号
        /// </summary>
        public string SupplierPartCode {
            get;
            set;
        }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SupplierPartName {
            get;
            set;
        }

        /// <summary>
        /// 承诺到货时间
        /// </summary>
        public DateTime? PromisedDeliveryTime {
            get;
            set;
        }

        /// <summary>
        /// 单价
        /// </summary>
        public Decimal UnitPrice {
            get;
            set;
        }

        /// <summary>
        /// 订货量
        /// </summary>
        public int OrderAmount {
            get;
            set;
        }

        /// <summary>
        /// 订货量_Str
        /// </summary>
        public String OrderAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 确认量
        /// </summary>
        public int ConfirmedAmount {
            get;
            set;
        }

        /// <summary>
        /// 发运量
        /// </summary>
        public int? ShippingAmount {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 包装数量
        /// </summary>
        public int? PackingAmount {
            get;
            set;
        }

        /// <summary>
        /// 包装规格
        /// </summary>
        public string PackingSpecification {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 确认备注
        /// </summary>
        public string ConfirmationRemark {
            get;
            set;
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
    }
}
