﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ImpPartsPurchasePricingDetail {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 申请单Id
        /// </summary>
        public int ParentId {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int PartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string PartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string PartName {
            get;
            set;
        }

        /// <summary>
        /// 供应商Id
        /// </summary>
        public int SupplierId {
            get;
            set;
        }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SupplierName {
            get;
            set;
        }

        /// <summary>
        /// 价格类型
        /// </summary>
        public int PriceType {
            get;
            set;
        }

        /// <summary>
        /// 价格类型_Str
        /// </summary>
        public String PriceTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 价格
        /// </summary>
        public Decimal Price {
            get;
            set;
        }

        /// <summary>
        /// 价格_Str
        /// </summary>
        public String PriceStr {
            get;
            set;
        }

        /// <summary>
        /// 参考价格
        /// </summary>
        public Decimal ReferencePrice {
            get;
            set;
        }

        /// <summary>
        /// 生效时间
        /// </summary>
        public DateTime ValidFrom {
            get;
            set;
        }

        /// <summary>
        /// 生效时间_Str
        /// </summary>
        public string ValidFromStr {
            get;
            set;
        }


        /// <summary>
        /// 失效时间
        /// </summary>
        public DateTime ValidTo {
            get;
            set;
        }


        /// <summary>
        /// 失效时间_Str
        /// </summary>
        public string ValidToStr {
            get;
            set;
        }

        /// <summary>
        /// 闭口数量
        /// </summary>
        public int LimitQty
        {
            get;
            set;
        }
        /// <summary>
        /// 闭口数量
        /// </summary>
        public string LimitQtyStr
        {
            get;
            set;
        }
        /// <summary>
        /// 是否首选
        /// </summary>
        public bool IsPrimary
        {
            get;
            set;
        }
        /// <summary>
        /// 是否首选
        /// </summary>
        public string IsPrimaryStr
        {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            get;
            set;

        }
        /// <summary>
        /// 品牌名称
        /// </summary>
        public string PartsSalesCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 品牌Id
        /// </summary>
        public string PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// 海外配件图号
        /// </summary>
        public string OverseasPartsFigureQuery {
            get;
            set;
        }

        /// <summary>
        /// 最低价供应商id
        /// </summary>
        public int? MinPriceSupplierId
        {
            get;
            set;
        }
        /// <summary>
        /// 最低价供应商名称
        /// </summary>
        public string MinPriceSupplier
        {
            get;
            set;
        }
        /// <summary>
        /// 最低采购价
        /// </summary>
        public decimal? MinPurchasePrice
        {
            get;
            set;
        }
        /// <summary>
        /// 最低采购价是否首选
        /// </summary>
        public bool? IsPrimaryMinPrice
        {
            get;
            set;
        }
        /// <summary>
        /// 是否是新配件
        /// </summary>
        public bool? IsNewPart
        {
            get;
            set;
        }
         /// <summary>
        /// 供应商图号
        /// </summary>
        public string SupplierPartCode
        {
            get;
            set;
        }
        /// <summary>
        /// 是否可采购
        /// </summary>
        public string IsOrderableStr
        {
            get;
            set;
        }
        /// <summary>
        /// 是否可采购
        /// </summary>
        public bool? IsOrderable
        {
            get;
            set;
        }
        public int? OldPriSupplierId {
            get;
            set;
        }
        public string OldPriSupplierCode {
            get;
            set;
        }
        public string OldPriSupplierName {
            get;
            set;
        }
        public decimal? OldPriPrice {
            get;
            set;
        }
        public int? OldPriPriceType {
            get;
            set;
        }
    }
}
