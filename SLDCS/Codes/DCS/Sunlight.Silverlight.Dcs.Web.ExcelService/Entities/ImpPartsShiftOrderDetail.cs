﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ImpPartsShiftOrderDetail {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 原库位id
        /// </summary>
        public int OriginalWarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 原库位
        /// </summary>
        public string OriginalWarehouseAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 目标库位id
        /// </summary>
        public int DestWarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 目标库位
        /// </summary>
        public string DestWarehouseAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 移库数量
        /// </summary>
        public int ShiftQuantity
        {
            get;
            set;
        }
        /// <summary>
        /// 移库数量
        /// </summary>
        public string ShiftQuantityStr
        {
            get;
            set;
        }
        /// <summary>
        /// 源库存数量
        /// </summary>
        public int OriginalWarehouseAreaQuantity
        {
            get;
            set;
        }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }


    }
}
