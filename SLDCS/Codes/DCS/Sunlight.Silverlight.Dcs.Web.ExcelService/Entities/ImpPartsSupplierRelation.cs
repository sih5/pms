﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ImpPartsSupplierRelation {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }


        /// <summary>
        ///分公司编号
        /// </summary>
        public string BranchCode {
            get;
            set;
        }

        /// <summary>
        ///分公司名称
        /// </summary>
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        ///配件编号
        /// </summary>
        public string PartCode {
            get;
            set;
        }

        /// <summary>
        ///配件名称
        /// </summary>
        public string PartName {
            get;
            set;
        }

        /// <summary>
        ///供应商编号
        /// </summary>
        public string SupplierCode {
            get;
            set;
        }

        /// <summary>
        ///供应商名称
        /// </summary>
        public string SupplierName {
            get;
            set;
        }

        /// <summary>
        ///供应商使用图号
        /// </summary>
        public string SupplierUseCode {
            get;
            set;
        }

        /// <summary>
        /// 供应商使用名称
        /// </summary>
        public string SupplierUseName {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 销售类型名称
        /// </summary>
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int PartId {
            get;
            set;
        }

        /// <summary>
        /// 供应商Id
        /// </summary>
        public int SupplierId {
            get;
            set;
        }

        /// <summary>
        /// 供应商图号
        /// </summary>
        public string SupplierPartCode {
            get;
            set;
        }

        /// <summary>
        /// 供应商配件名称
        /// </summary>
        public string SupplierPartName {
            get;
            set;
        }

        /// <summary>
        /// 是否首选供应商
        /// </summary>
        public bool IsPrimary {
            get;
            set;
        }

        /// <summary>
        /// 是否首选供应商_Str
        /// </summary>
        public string IsPrimaryStr {
            get;
            set;
        }

        /// <summary>
        /// 采购占比
        /// </summary>
        public Decimal PurchasePercentage {
            get;
            set;
        }

        /// <summary>
        /// 采购占比
        /// </summary>
        public string PurchasePercentageStr {
            get;
            set;
        }


        /// <summary>
        /// 经济采购批量
        /// </summary>
        public int EconomicalBatch {
            get;
            set;
        }
        /// <summary>
        /// 经济采购批量_Str
        /// </summary>
        public string EconomicalBatchStr {
            get;
            set;
        }


        /// <summary>
        /// 最小采购批量
        /// </summary>
        public int MinBatch {
            get;
            set;
        }

        /// <summary>
        /// 最小采购批量_Str
        /// </summary>
        public string MinBatchStr {
            get;
            set;
        }


        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime RowVersion {
            get;
            set;
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }


        /// <summary>
        /// 月度采购到货周期
        /// </summary>
        public int MonthlyArrivalPeriod {
            get;
            set;
        }

        /// <summary>
        /// 补货采购到货周期
        /// </summary>
        public int ArrivalReplenishmentCycle {
            get;
            set;
        }
        /// <summary>
        /// 订货周期
        /// </summary>
        public int OrderCycle {
            get;
            set;
        }

        /// <summary>
        /// 订货周期_Str
        /// </summary>
        public string OrderCycleStr {
            get;
            set;
        }

        /// <summary>
        /// 紧急采购到货周期
        /// </summary>
        public int EmergencyArrivalPeriod {
            get;
            set;
        }

        /// <summary>
        /// 默认订货仓库Id
        /// </summary>
        public int DefaultOrderWarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 默认订货仓库
        /// </summary>
        public string DefaultOrderWarehouseName {
            get;
            set;
        }
        //订货周期
        public int? OrderGoodsCycle {
            get;
            set;
        }
        public string OrderGoodsCycleStr {
            get;
            set;
        }
    }
}
