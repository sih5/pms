﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ImpPlannedPriceAppDetail {
        /// <summary>
        /// Id
        /// </summary>
        public Decimal Id {
            get;
            set;
        }

        /// <summary>
        /// 计划价申请单Id
        /// </summary>
        public int PlannedPriceAppId {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 申请价格
        /// </summary>
        public Decimal RequestedPrice {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public int? Quantity {
            get;
            set;
        }

        /// <summary>
        /// 库存数量
        /// </summary>
        public int? StockQuantity {
            get;
            set;
        }

        /// <summary>
        /// 在途数量
        /// </summary>
        public int? OnRoadQuantity {
            get;
            set;
        }

        /// <summary>
        /// 变动前价格
        /// </summary>
        public Decimal? PriceBeforeChange {
            get;
            set;
        }

        /// <summary>
        /// 差异金额
        /// </summary>
        public Decimal? AmountDifference {
            get;
            set;
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        /// <summary>
        /// 计划价申请单编号
        /// </summary>
        public string PlannedPriceAppCode {
            get;
            set;
        }
    }
}
