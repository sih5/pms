﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ImpProductionPlan {
        /// <summary>
        /// 产品信息ID
        /// </summary>
        public int ProductId {
            get;
            set;
        }

        /// <summary>
        /// 产品编号
        /// </summary>
        public string ProductCode {
            get;
            set;
        }

        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName {
            get;
            set;
        }

        /// <summary>
        /// 车型编号
        /// </summary>
        public string ProductCategoryCode {
            get;
            set;
        }

        /// <summary>
        /// 车型名称
        /// </summary>
        public string ProductCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 订单年
        /// </summary>
        public int YearOfOrder {
            get;
            set;
        }

        /// <summary>
        /// 导入的订单年
        /// </summary>
        public string YearOfOrderStr {
            get;
            set;
        }

        /// <summary>
        /// 订单月
        /// </summary>
        public int MonthOfOrder {
            get;
            set;
        }

        /// <summary>
        /// 导入的订单月
        /// </summary>
        public string MonthOfOrderStr {
            get;
            set;
        }

        /// <summary>
        /// 生产SON
        /// </summary>
        public string ProductionPlanSON {
            get;
            set;
        }

        /// <summary>
        /// 下线日期
        /// </summary>
        public DateTime? RolloutDate {
            get;
            set;
        }

        /// <summary>
        /// 下线日期
        /// </summary>
        public string RolloutDateStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 导入的状态
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
    }
}