﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ImpVehicleDLRStartSecurityDeposit {
        /// <summary>
        /// 主键Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 经销商Id
        /// </summary>
        public int DealerId {
            get;
            set;
        }

        /// <summary>
        /// 经销商名称
        /// </summary>
        public string DealerName {
            get;
            set;
        }

        /// <summary>
        /// 经销商编号
        /// </summary>
        public string DealerCode {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public string Status {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }

        public string DebtorStr {
            get;
            set;
        }

        /// <summary>
        /// 借方
        /// </summary>
        public decimal Debtor {
            get;
            set;
        }

        public string LenderStr {
            get;
            set;
        }

        /// <summary>
        /// 贷方
        /// </summary>
        public decimal Lender {
            get;
            set;
        }

        public string CurrentBalanceStr {
            get;
            set;
        }

        /// <summary>
        /// 本期余额借/贷
        /// </summary>
        public decimal CurrentBalance {
            get;
            set;
        }

        public string TimeOfRecordStr {
            get;
            set;
        }

        /// <summary>
        /// 记账时间
        /// </summary>
        public DateTime TimeOfRecord {
            get;
            set;
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
    }
}