﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ImportVehicleOrderPlanSummary {
        /// <summary>
        /// 营销分公司ID
        /// </summary>
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司编号
        /// </summary>
        public string BranchCode {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司名称
        /// </summary>
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        /// 产品信息ID
        /// </summary>
        public int ProductId {
            get;
            set;
        }

        /// <summary>
        /// 产品编号
        /// </summary>
        public string ProductCode {
            get;
            set;
        }

        /// <summary>
        /// 产品名称
        /// </summary>
        public string ProductName {
            get;
            set;
        }

        /// <summary>
        /// 车型编号
        /// </summary>
        public string ProductCategoryCode {
            get;
            set;
        }

        /// <summary>
        /// 车型名称
        /// </summary>
        public string ProductCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 订单年
        /// </summary>
        public int YearOfOrder {
            get;
            set;
        }

        /// <summary>
        /// 订单月
        /// </summary>
        public int MonthOfOrder {
            get;
            set;
        }

        /// <summary>
        /// 订单类型
        /// </summary>
        public int OrderType {
            get;
            set;
        }

        /// <summary>
        /// 区域代码
        /// </summary>
        public string RegionCode {
            get;
            set;
        }

        /// <summary>
        /// 车系代码Id
        /// </summary>
        public int VehicleSeriesId {
            get;
            set;
        }

        /// <summary>
        /// 车系代码编号
        /// </summary>
        public string VehicleSeriesCode {
            get;
            set;
        }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime {
            get;
            set;
        }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime {
            get;
            set;
        }

        /// <summary>
        /// 上传类型
        /// </summary>
        public int UploadType {
            get;
            set;
        }

        /// <summary>
        /// 订单上传状态
        /// </summary>
        public int VehicleOrderUploadStatus {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public int Amount {
            get;
            set;
        }

        /// <summary>
        /// 错误的计划年
        /// </summary>
        public string YearOfOrderStr {
            get;
            set;
        }

        /// <summary>
        /// 错误的计划月
        /// </summary>
        public string MonthOfOrderStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        /// <summary>
        /// 生产SON
        /// </summary>
        public string ProductionPlanSON {
            get;
            set;
        }
    }
}
