﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class InternalAllocationBillExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 领入单编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 领入单编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 领入仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 领入仓库Id_Str
        /// </summary>
        public string WarehouseIdStr {
            get;
            set;
        }

        /// <summary>
        /// 领入仓库名称
        /// </summary>
        public string WarehouseName {
            get;
            set;
        }
        /// <summary>
        /// 领入仓库名称_Str
        /// </summary>
        public string WarehouseNameStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 分公司名称
        /// </summary>
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 分公司名称_Str
        /// </summary>
        public string BranchNameStr {
            get;
            set;
        }

        /// <summary>
        /// 部门Id
        /// </summary>
        public int DepartmentId {
            get;
            set;
        }
        /// <summary>
        /// 部门Id_Str
        /// </summary>
        public string DepartmentIdStr {
            get;
            set;
        }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string DepartmentName {
            get;
            set;
        }
        /// <summary>
        /// 部门名称_Str
        /// </summary>
        public string DepartmentNameStr {
            get;
            set;
        }

        /// <summary>
        /// 领入原因
        /// </summary>
        public string Reason {
            get;
            set;
        }
        /// <summary>
        /// 领入原因_Str
        /// </summary>
        public string ReasonStr {
            get;
            set;
        }

        /// <summary>
        /// 经办人
        /// </summary>
        public string Operator {
            get;
            set;
        }
        /// <summary>
        /// 经办人_Str
        /// </summary>
        public string OperatorStr {
            get;
            set;
        }

        /// <summary>
        /// 总金额
        /// </summary>
        public Decimal TotalAmount {
            get;
            set;
        }
        /// <summary>
        /// 总金额_Str
        /// </summary>
        public string TotalAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id_Str
        /// </summary>
        public string AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废人_Str
        /// </summary>
        public string AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// 作废时间_Str
        /// </summary>
        public string AbandonTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 审批人Id
        /// </summary>
        public int? ApproverId {
            get;
            set;
        }
        /// <summary>
        /// 审批人Id_Str
        /// </summary>
        public string ApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 审批人
        /// </summary>
        public string ApproverName {
            get;
            set;
        }
        /// <summary>
        /// 审批人_Str
        /// </summary>
        public string ApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 审批时间
        /// </summary>
        public DateTime? ApproveTime {
            get;
            set;
        }
        /// <summary>
        /// 审批时间_Str
        /// </summary>
        public string ApproveTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }
    }
}
