﻿
using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class InternalAllocationDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber {
            get;
            set;
        }
        /// <summary>
        /// 序号_Str
        /// </summary>
        public string SerialNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 领出单Id
        /// </summary>
        public int InternalAllocationBillId {
            get;
            set;
        }
        /// <summary>
        /// 领出单Id_Str
        /// </summary>
        public string InternalAllocationBillIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }
        /// <summary>
        /// 计量单位_Str
        /// </summary>
        public string MeasureUnitStr {
            get;
            set;
        }

        /// <summary>
        /// 单价
        /// </summary>
        public Decimal UnitPrice {
            get;
            set;
        }
        /// <summary>
        /// 单价_Str
        /// </summary>
        public string UnitPriceStr {
            get;
            set;
        }
        /// <summary>
        /// 批发价
        /// </summary>
        public Decimal SalesPrice {
            get;
            set;
        }
        /// <summary>
        /// 批发价_Str
        /// </summary>
        public string SalesPriceStr {
            get;
            set;
        }
        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }
        /// <summary>
        /// 数量_Str
        /// </summary>
        public string QuantityStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }
    }
}
