﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class IvecoPriceChangeAppDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 互换件最高价
        /// </summary>
        public decimal MaxExchangeSalePrice {
            get;
            set;
        }

        /// <summary>
        /// 申请单Id
        /// </summary>
        public int ParentId {
            get;
            set;
        }
        /// <summary>
        /// 申请单Id_Str
        /// </summary>
        public string ParentIdStr {
            get;
            set;
        }

        /// <summary>
        /// 品牌Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }

        public string PartsSalesCategoryCode {
            get;
            set;
        }
        public string PartsSalesCategoryName {
            get;
            set;
        }
        public string PartsSalesCategoryNameStr {
            get;
            set;
        }
        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 依维柯价格
        /// </summary>
        public Decimal IvecoPrice {
            get;
            set;
        }
        /// <summary>
        /// 依维柯价格_Str
        /// </summary>
        public string IvecoPriceStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }
        public Decimal? SalesPrice {
            get;
            set;
        }
        
    }
}
