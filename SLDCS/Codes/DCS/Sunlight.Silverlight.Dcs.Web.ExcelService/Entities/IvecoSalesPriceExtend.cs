﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class IvecoSalesPriceExtend {
        public int SparePartId {
            get;
            set;

        }

        public decimal SalesPrice {
            get;
            set;

        }

        public int PartsSalesCategoryId {
            get;
            set;
        }
    }
}
