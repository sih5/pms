﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class LoadingDetailExtend {
        public int BranchId {
            get;
            set;
        }

        public int VehicleId {
            get;
            set;
        }

        public string VIN {
            get;
            set;
        }

        public string SerialNumber {
            get;
            set;
        }
        public string FaultyPartsAssemblyName {
            get;
            set;
        }

        public string ResponsibleUnitCode {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }
    }
}
