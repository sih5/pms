﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class MalfunctionExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }	/// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }
        /// <summary>
        /// 故障现象编号
        /// </summary>
        public string Code {
            get;
            set;
        }	/// <summary>
        /// 故障现象编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }
        /// <summary>
        /// 故障现象描述
        /// </summary>
        public string Description {
            get;
            set;
        }	/// <summary>
        /// 故障现象描述_Str
        /// </summary>
        public string DescriptionStr {
            get;
            set;
        }
        /// <summary>
        /// 故障现象分类Id
        /// </summary>
        public int MalfunctionCategoryId {
            get;
            set;
        }	/// <summary>
        /// 故障现象分类Id_Str
        /// </summary>
        public string MalfunctionCategoryIdStr {
            get;
            set;
        }
        /// <summary>
        /// 速查码
        /// </summary>
        public string QuickCode {
            get;
            set;
        }	/// <summary>
        /// 速查码_Str
        /// </summary>
        public string QuickCodeStr {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }	/// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 是否ISG
        /// </summary>
        public int ISISG {
            get;
            set;
        }	/// <summary>
        /// 是否ISG_Str
        /// </summary>
        public string ISISG_Str {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }	/// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }	/// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }	/// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }	/// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }	/// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }
        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }	/// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }	/// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }
        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }	/// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }


    }
}
