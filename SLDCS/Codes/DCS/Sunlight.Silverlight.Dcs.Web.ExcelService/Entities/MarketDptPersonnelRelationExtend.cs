﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class MarketDptPersonnelRelationExtend {
        /// <summary>
        /// 分公司名称_Str 
        /// </summary>
        public string BranchNameStr {
            get;
            set;
        }
        /// <summary>
        /// 市场部编号_Str 
        /// </summary>
        public string MarketDepartmentCodeStr {
            get;
            set;
        }
        /// <summary>
        /// 市场部名称_Str 
        /// </summary>
        public string MarketDepartmentNameStr {
            get;
            set;
        }

        /// <summary>
        /// 品牌名称
        /// </summary>
        public string PartsSalesCategoryName
        {
            get;
            set;
        }

        /// <summary>
        /// 品牌id
        /// </summary>
        public int PartsSalesCategoryId
        {
            get;
            set;
        }


        /// <summary>
        /// 人员姓名_Str 
        /// </summary>
        public string PersonnelNameStr {
            get;
            set;
        }
        /// <summary>
        /// 类型 
        /// </summary>
        public int? Type {
            get;
            set;
        }
        /// <summary>
        /// 类型_Str 
        /// </summary>
        public string TypeStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 市场部Id
        /// </summary>
        public int MarketDepartmentId {
            get;
            set;
        }
        /// <summary>
        /// 市场部Id_Str
        /// </summary>
        public string MarketDepartmentIdStr {
            get;
            set;
        }

        /// <summary>
        /// 人员Id
        /// </summary>
        public int PersonnelId {
            get;
            set;
        }
        /// <summary>
        /// 人员Id_Str
        /// </summary>
        public string PersonnelIdStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id_Str
        /// </summary>
        public string AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废人_Str
        /// </summary>
        public string AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// 作废时间_Str
        /// </summary>
        public string AbandonTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }


    }
}
