﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class MarketingDepartmentExtend {

        ///<summary>
        /// 分公司名称_Str
        ///</summary>
        public string BranchNameStr {
            get;
            set;
        }

        ///<summary>
        /// 品牌_Str
        ///</summary>
        public string PartsSalesCategoryNameStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id_Str
        /// </summary>
        public string PartsSalesCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 业务类型
        /// </summary>
        public int BusinessType {
            get;
            set;
        }
        /// <summary>
        /// 业务类型_Str
        /// </summary>
        public string BusinessTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 市场部编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 市场部编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 市场部名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 市场部名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 区省市Id
        /// </summary>
        public int? RegionId {
            get;
            set;
        }
        /// <summary>
        /// 区省市Id_Str
        /// </summary>
        public string RegionIdStr {
            get;
            set;
        }

        /// <summary>
        /// 省
        /// </summary>
        public string ProvinceName {
            get;
            set;
        }
        /// <summary>
        /// 省_Str
        /// </summary>
        public string ProvinceNameStr {
            get;
            set;
        }

        /// <summary>
        /// 市
        /// </summary>
        public string CityName {
            get;
            set;
        }
        /// <summary>
        /// 市_Str
        /// </summary>
        public string CityNameStr {
            get;
            set;
        }

        /// <summary>
        /// 县
        /// </summary>
        public string CountyName {
            get;
            set;
        }
        /// <summary>
        /// 县_Str
        /// </summary>
        public string CountyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 所在地址
        /// </summary>
        public string Address {
            get;
            set;
        }
        /// <summary>
        /// 所在地址_Str
        /// </summary>
        public string AddressStr {
            get;
            set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string Phone {
            get;
            set;
        }
        /// <summary>
        /// 联系电话_Str
        /// </summary>
        public string PhoneStr {
            get;
            set;
        }

        /// <summary>
        /// 传真
        /// </summary>
        public string Fax {
            get;
            set;
        }
        /// <summary>
        /// 传真_Str
        /// </summary>
        public string FaxStr {
            get;
            set;
        }

        /// <summary>
        /// 邮政编码
        /// </summary>
        public string PostCode {
            get;
            set;
        }
        /// <summary>
        /// 邮政编码_Str
        /// </summary>
        public string PostCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }


    }
}
