﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class OverstockPartsAppDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber {
            get;
            set;
        }
        /// <summary>
        /// 序号_Str
        /// </summary>
        public string SerialNumberStr {
            get;
            set;
        }
        public int SparePartId {
            get;
            set;
        }
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件图号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件图号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }
        //处理价格
        public decimal PartsSalesPrice {
            get;
            set;
        }
        public string PartsSalesPriceStr {
            get;
            set;
        }


    }
}
