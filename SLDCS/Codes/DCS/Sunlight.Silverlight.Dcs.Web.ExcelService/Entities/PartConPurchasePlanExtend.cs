﻿
using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartConPurchasePlanExtend {
        ///<summary>
        ///  Id  
        ///</summary>
        public int Id {
            get;
            set;
        }

        ///<summary>
        ///  配件Id  
        ///</summary>
        public int PartId {
            get;
            set;
        }

        ///<summary>
        ///  配件编号  
        ///</summary>
        public string PartCode {
            get;
            set;
        }

        ///<summary>
        ///  配件销售类型名称  
        ///</summary>
        public string PartsSalesCategoryName {
            get;
            set;
        }

        ///<summary>
        ///  配件销售类型Id  
        ///</summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }

        ///<summary>
        ///  营销分公司Id  
        ///</summary>
        public int BranchId {
            get;
            set;
        }

        ///<summary>
        ///  实际订货仓库  
        ///</summary>
        public string ActualOrderWarehouse {
            get;
            set;
        }

        ///<summary>
        ///  配件销售类型Id  
        ///</summary>
        public int? ActualOrderWarehouseId {
            get;
            set;
        }

        ///<summary>
        ///  实际下达供应商编码  
        ///</summary>
        public string ActualSupplierCode {
            get;
            set;
        }

        ///<summary>
        ///  实际下达供应商Id  
        ///</summary>
        public int? ActualSupplierId {
            get;
            set;
        }

        ///<summary>
        ///  建议采购量 
        ///</summary>
        public int? AdvicePurchase {
            get;
            set;
        }

        public string AdvicePurchaseStr {
            get;
            set;
        }

        ///<summary>
        ///  实际采购量
        ///</summary>
        public int? ActualPurchase {
            get;
            set;
        }

        public string ActualPurchaseStr {
            get;
            set;
        }

        ///<summary>
        ///  时间 
        ///</summary>
        public DateTime? Date {
            get;
            set;
        }

        public string DateStr {
            get;
            set;
        }

        ///<summary>
        ///  错误信息 
        ///</summary>
        public string ErrorMsg {
            get;
            set;
        }

    }
}
