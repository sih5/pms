﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartDeleaveInformationExtend {
        /// <summary>
        /// 品牌_Str
        /// </summary>
        public string PartsSalesCategoryNameStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 原配件Id
        /// </summary>
        public int OldPartId {
            get;
            set;
        }
        /// <summary>
        /// 原配件Id_Str
        /// </summary>
        public string OldPartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 原件图号
        /// </summary>
        public string OldPartCode {
            get;
            set;
        }
        /// <summary>
        /// 原件图号_Str
        /// </summary>
        public string OldPartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 拆散件图号
        /// </summary>
        public string DeleavePartCode {
            get;
            set;
        }
        /// <summary>
        /// 拆散件图号_Str
        /// </summary>
        public string DeleavePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 拆散件名称
        /// </summary>
        public string DeleavePartName {
            get;
            set;
        }
        /// <summary>
        /// 拆散件名称_Str
        /// </summary>
        public string DeleavePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id_Str
        /// </summary>
        public string PartsSalesCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 拆散数量
        /// </summary>
        public int DeleaveAmount {
            get;
            set;
        }
        /// <summary>
        /// 拆散数量_Str
        /// </summary>
        public string DeleaveAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }


    }
}
