﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsBranch {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int PartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string PartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string PartName {
            get;
            set;
        }

        /// <summary>
        /// 配件参图号
        /// </summary>
        public string ReferenceCode {
            get;
            set;
        }

        /// <summary>
        /// 库存高限
        /// </summary>
        public int? StockMaximum {
            get;
            set;
        }

        /// <summary>
        /// 库存低限
        /// </summary>
        public int? StockMinimum {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司
        /// </summary>
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int? PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型名称
        /// </summary>
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 配件ABC分类策略Id
        /// </summary>
        public int? ABCStrategyId {
            get;
            set;
        }

        /// <summary>
        /// 产品生命周期
        /// </summary>
        public int? ProductLifeCycle {
            get;
            set;
        }

        /// <summary>
        /// 损耗类型
        /// </summary>
        public int? LossType {
            get;
            set;
        }

        /// <summary>
        /// 是否可采购
        /// </summary>
        public bool? IsOrderable {
            get;
            set;
        }

        /// <summary>
        /// 采购周期（天）
        /// </summary>
        public int? PurchaseCycle {
            get;
            set;
        }

        /// <summary>
        /// 是否售后服务件
        /// </summary>
        public bool? IsService {
            get;
            set;
        }

        /// <summary>
        /// 是否可销售
        /// </summary>
        public bool? IsSalable {
            get;
            set;
        }

        /// <summary>
        /// 旧件返回政策
        /// </summary>
        public int PartsReturnPolicy {
            get;
            set;
        }

        /// <summary>
        /// 是否可直供
        /// </summary>
        public bool? IsDirectSupply {
            get;
            set;
        }

        /// <summary>
        /// 保内保外供货属性
        /// </summary>
        public int? WarrantySupplyStatus {
            get;
            set;
        }

        /// <summary>
        /// 最小销售批量
        /// </summary>
        public int? MinSaleQuantity {
            get;
            set;
        }

        /// <summary>
        /// 配件ABC分类
        /// </summary>
        public int? PartABC {
            get;
            set;
        }

        /// <summary>
        /// 采购路线
        /// </summary>
        public int? PurchaseRoute {
            get;
            set;
        }

        /// <summary>
        /// 维修用料最小单位
        /// </summary>
        public int? RepairMatMinUnit {
            get;
            set;
        }

        /// <summary>
        /// 配件保修分类Id
        /// </summary>
        public int? PartsWarrantyCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件保修分类编号
        /// </summary>
        public string PartsWarrantyCategoryCode {
            get;
            set;
        }

        /// <summary>
        /// 配件保修分类名称
        /// </summary>
        public string PartsWarrantyCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 配件仓储管理粒度
        /// </summary>
        public int? PartsWarhouseManageGranularity {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
    }
}
