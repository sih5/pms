﻿

using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsBranchExtend {
        public string PartsSalesCategoryNameStr {
            get;
            set;
        }
        ///<summary>
        ///  是否可采购  
        ///</summary>
        public bool IsOrderable {
            get;
            set;
        }
        ///<summary>
        ///  配件仓储管理粒度  
        ///</summary>
        public int? PartsWarhouseManageGranularity {
            get;
            set;
        }
        ///<summary>
        ///  是否售后服务件  
        ///</summary>
        public bool IsService {
            get;
            set;
        }
        ///<summary>
        ///  是否可销售  
        ///</summary>
        public bool IsSalable {
            get;
            set;
        }
        ///<summary>
        ///  是否可直供  
        ///</summary>
        public bool IsDirectSupply {
            get;
            set;
        }
        ///<summary>
        ///  维修用料最小单位  
        ///</summary>
        public bool RepairMatMinUnit {
            get;
            set;
        }
        ///<summary>
        ///  创建时间  
        ///</summary>
        public DateTime CreateTime {
            get;
            set;
        }
        ///<summary>
        ///  修改时间  
        ///</summary>
        public DateTime ModifyTime {
            get;
            set;
        }
        ///<summary>
        ///  作废时间  
        ///</summary>
        public DateTime AbandonTime {
            get;
            set;
        }
        ///<summary>
        ///  配件Id  
        ///</summary>
        public int? PartId {
            get;
            set;
        }
        ///<summary>
        ///  库存高限  
        ///</summary>
        public int? StockMaximum {
            get;
            set;
        }
        ///<summary>
        ///  (替换)库存高限  
        ///</summary>
        public int? OldStockMaximum {
            get;
            set;
        }
        ///<summary>
        ///  库存低限  
        ///</summary>
        public int? StockMinimum {
            get;
            set;
        }
        ///<summary>
        ///  (替换)库存低限  
        ///</summary>
        public int? OldStockMinimum {
            get;
            set;
        }
        ///<summary>
        ///  营销分公司Id  
        ///</summary>
        public int? BranchId {
            get;
            set;
        }
        ///<summary>
        ///  配件销售类型Id  
        ///</summary>
        public int? PartsSalesCategoryId {
            get;
            set;
        }
        ///<summary>
        ///  配件ABC分类策略Id  
        ///</summary>
        public int? ABCStrategyId {
            get;
            set;
        }
        ///<summary>
        ///  产品生命周期  
        ///</summary>
        public int? ProductLifeCycle {
            get;
            set;
        }
        ///<summary>
        ///  损耗类型  
        ///</summary>
        public int? LossType {
            get;
            set;
        }
        ///<summary>
        ///  (替换)损耗类型  
        ///</summary>
        public int? OldLossType {
            get;
            set;
        }
        ///<summary>
        ///  采购周期（天）  
        ///</summary>
        public int? PurchaseCycle {
            get;
            set;
        }
        ///<summary>
        ///  旧件返回政策  
        ///</summary>
        public int PartsReturnPolicy {
            get;
            set;
        }
        ///<summary>
        ///  (替换)旧件返回政策  
        ///</summary>
        public int OldPartsReturnPolicy {
            get;
            set;
        }
        ///<summary>
        ///  保内保外供货属性  
        ///</summary>
        public int? WarrantySupplyStatus {
            get;
            set;
        }
        ///<summary>
        ///  最小销售批量  
        ///</summary>
        public int? MinSaleQuantity {
            get;
            set;
        }
        ///<summary>
        ///  配件ABC分类  
        ///</summary>
        public int? PartABC {
            get;
            set;
        }
        ///<summary>
        ///  采购路线  
        ///</summary>
        public int? PurchaseRoute {
            get;
            set;
        }
        ///<summary>
        ///  配件保修分类Id  
        ///</summary>
        public int? PartsWarrantyCategoryId {
            get;
            set;
        }
        ///<summary>
        ///  (替换)配件保修分类Id  
        ///</summary>
        public int? OldPartsWarrantyCategoryId {
            get;
            set;
        }
        ///<summary>
        ///  配件保修期  
        ///</summary>
        public int? PartsWarrantyLong {
            get;
            set;
        }
        ///<summary>
        ///  配件材料费  
        ///</summary>
        public decimal? PartsMaterialManageCost {
            get;
            set;
        }
        ///<summary>
        ///  状态  
        ///</summary>
        public int Status {
            get;
            set;
        }
        ///<summary>
        ///  创建人Id  
        ///</summary>
        public int CreatorId {
            get;
            set;
        }
        ///<summary>
        ///  修改人Id  
        ///</summary>
        public int ModifierId {
            get;
            set;
        }
        ///<summary>
        ///  作废人Id  
        ///</summary>
        public int AbandonerId {
            get;
            set;
        }
        ///<summary>
        ///  Id  
        ///</summary>
        public int Id {
            get;
            set;
        }
        ///<summary>
        ///  配件名称  
        ///</summary>
        public string PartName {
            get;
            set;
        }
        ///<summary>
        ///  营销分公司名称  
        ///</summary>
        public string BranchName {
            get;
            set;
        }
        ///<summary>
        ///  营销分公司编码 
        ///</summary>
        public string BranchCode {
            get;
            set;
        }
        ///<summary>
        ///  配件保修分类名称  
        ///</summary>
        public string PartsWarrantyCategoryName {
            get;
            set;
        }
        ///<summary>
        ///  (替换)配件保修分类名称  
        ///</summary>
        public string OldPartsWarrantyCategoryName {
            get;
            set;
        }
        ///<summary>
        ///  创建人  
        ///</summary>
        public string CreatorName {
            get;
            set;
        }
        ///<summary>
        ///  修改人  
        ///</summary>
        public string ModifierName {
            get;
            set;
        }
        ///<summary>
        ///  作废人  
        ///</summary>
        public string AbandonerName {
            get;
            set;
        }
        ///<summary>
        ///  备注  
        ///</summary>
        public string Remark {
            get;
            set;
        }
        ///<summary>
        ///  (替换)备注  
        ///</summary>
        public string OldRemark {
            get;
            set;
        }
        ///<summary>
        ///  配件编号  
        ///</summary>
        public string PartCode {
            get;
            set;
        }
        ///<summary>
        ///  配件参图号  
        ///</summary>
        public string ReferenceCode {
            get;
            set;
        }
        ///<summary>
        ///  配件销售类型名称  
        ///</summary>
        public string PartsSalesCategoryName {
            get;
            set;
        }
        ///<summary>
        ///  配件保修分类编号  
        ///</summary>
        public string PartsWarrantyCategoryCode {
            get;
            set;
        }
        ///<summary>
        ///  (替换)配件保修分类编号  
        ///</summary>
        public string OldPartsWarrantyCategoryCode {
            get;
            set;
        }
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        public string PartCodeStr {
            get;
            set;
        }

        public string PartNameStr {
            get;
            set;
        }

        public string BranchNameStr {
            get;
            set;
        }

        public string ReferenceCodeStr {
            get;
            set;
        }

        public string IsOrderableStr {
            get;
            set;
        }

        public string IsSalableStr {
            get;
            set;
        }

        public string IsDirectSupplyStr {
            get;
            set;
        }

        public string MinSaleQuantityStr {
            get;
            set;
        }

        public string ProductLifeCycleStr {
            get;
            set;
        }

        public string LossTypeStr {
            get;
            set;
        }

        public string PartsWarrantyCategoryNameStr {
            get;
            set;
        }

        public string PartsReturnPolicyStr {
            get;
            set;
        }

        public string PartABCStr {
            get;
            set;
        }

        public string StockMaximumStr {
            get;
            set;
        }

        public string StockMinimumStr {
            get;
            set;
        }

        public string RemarkStr {
            get;
            set;
        }

        ///<summary>
        ///  加价率分组编号  
        ///</summary>
        public string IncreaseRateGroupCode {
            get;
            set;
        }

        ///<summary>
        ///  加价率分组Id  
        ///</summary>
        public int? IncreaseRateGroupId {
            get;
            set;
        }
        ///<summary>
        ///  (替换)加价率分组Id  
        ///</summary>
        public int? OldIncreaseRateGroupId {
            get;
            set;
        }
        ///<summary>
        ///  加价率分组名称 
        ///</summary>
        public string IncreaseRateGroupName {
            get;
            set;
        }
        ///<summary>
        ///  配件所属分类
        ///</summary>
        public int? PartsAttribution {
            get;
            set;
        }
        ///<summary>
        ///  配件所属分类名称 
        ///</summary>
        public string PartsAttributionStr {
            get;
            set;
        }

        ///<summary>
        ///  序号
        ///</summary>
        public int SerialNumber {
            get;
            set;
        }


        public string PartsMaterialManageCostStr {
            get;
            set;
        }

        public string PartsWarrantyLongStr {
            get;
            set;
        }
        public string IsEngNameRequired {
            get;
            set;
        }

        public string EnglishName {
            get;
            set;
        }

        /// <summary>
        /// 批量审核上限
        /// </summary>
        public string AutoApproveUpLimitStr {
            get;
            set;
        }

        public int? AutoApproveUpLimit {
            get;
            set;
        }
        /// <summary>
        /// 海外配件图号
        /// </summary>
        public string OverseasPartsFigure {
            get;
            set;
        }
        public string OverseasPartsFigureStr {
            get;
            set;
        }
    }
}
