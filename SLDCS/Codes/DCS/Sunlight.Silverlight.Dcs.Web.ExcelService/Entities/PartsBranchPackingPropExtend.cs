﻿using System;
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsBranchPackingPropExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber {
            get;
            set;
        }

        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// PartsBranchId
        /// </summary>
        public int? PartsBranchId {
            get;
            set;
        }
        /// <summary>
        /// PartsBranchId_Str
        /// </summary>
        public string PartsBranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 包装类型
        /// </summary>
        public int? PackingType {
            get;
            set;
        }
        /// <summary>
        /// 包装类型_Str
        /// </summary>
        public string PackingTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }
        /// <summary>
        /// 计量单位_Str
        /// </summary>
        public string MeasureUnitStr {
            get;
            set;
        }

        /// <summary>
        /// 包装数量
        /// </summary>
        public int? PackingCoefficient {
            get;
            set;
        }
        /// <summary>
        /// 包装数量_Str
        /// </summary>
        public string PackingCoefficientStr {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int? SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 是否可采购
        /// </summary>
        public bool? IsOrderable {
            get;
            set;
        }
        /// <summary>
        /// 是否可采购_Str
        /// </summary>
        public string IsOrderableStr {
            get;
            set;
        }
    }
}
