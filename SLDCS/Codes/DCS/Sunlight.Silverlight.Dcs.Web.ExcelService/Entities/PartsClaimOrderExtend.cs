﻿using System;
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsClaimOrderExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        /// <summary>
        /// Id
        /// </summary>
        public Int32 Id {
            get;
            set;
        }

        /// <summary>
        /// IdStr
        /// </summary>
        public String IdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件索赔单编号
        /// </summary>
        public String Code {
            get;
            set;
        }

        /// <summary>
        /// 配件索赔单编号Str
        /// </summary>
        public String CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回次数
        /// </summary>
        public Int32 RejectQty {
            get;
            set;
        }

        /// <summary>
        /// 驳回次数Str
        /// </summary>
        public String RejectQtyStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回原因
        /// </summary>
        public String RejectReason {
            get;
            set;
        }

        /// <summary>
        /// 驳回原因Str
        /// </summary>
        public String RejectReasonStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public Int32 PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型IdStr
        /// </summary>
        public String PartsSalesCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 销售组织Id
        /// </summary>
        public Int32 SalesUnitId {
            get;
            set;
        }

        /// <summary>
        /// 销售组织IdStr
        /// </summary>
        public String SalesUnitIdStr {
            get;
            set;
        }

        /// <summary>
        /// 零售订单编号
        /// </summary>
        public String PartsRetailOrderCode {
            get;
            set;
        }

        /// <summary>
        /// 零售订单编号Str
        /// </summary>
        public String PartsRetailOrderCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 零售订单Id
        /// </summary>
        public Int32 PartsRetailOrderId {
            get;
            set;
        }

        /// <summary>
        /// 零售订单IdStr
        /// </summary>
        public String PartsRetailOrderIdStr {
            get;
            set;
        }

        /// <summary>
        /// 大区Id
        /// </summary>
        public Int32 SalesRegionId {
            get;
            set;
        }

        /// <summary>
        /// 大区IdStr
        /// </summary>
        public String SalesRegionIdStr {
            get;
            set;
        }

        /// <summary>
        /// 市场部Id
        /// </summary>
        public Int32 MarketingDepartmentId {
            get;
            set;
        }

        /// <summary>
        /// 市场部IdStr
        /// </summary>
        public String MarketingDepartmentIdStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public Int32 Status {
            get;
            set;
        }

        /// <summary>
        /// 状态Str
        /// </summary>
        public String StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 结算状态
        /// </summary>
        public Int32 SettlementStatus {
            get;
            set;
        }

        /// <summary>
        /// 结算状态Str
        /// </summary>
        public String SettlementStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站Id
        /// </summary>
        public Int32 DealerId {
            get;
            set;
        }

        /// <summary>
        /// 服务站IdStr
        /// </summary>
        public String DealerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号
        /// </summary>
        public String DealerCode {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号Str
        /// </summary>
        public String DealerCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称
        /// </summary>
        public String DealerName {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称Str
        /// </summary>
        public String DealerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站Id
        /// </summary>
        public Int32 FirstClassStationId {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站IdStr
        /// </summary>
        public String FirstClassStationIdStr {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站编号
        /// </summary>
        public String FirstClassStationCode {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站编号Str
        /// </summary>
        public String FirstClassStationCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站名称
        /// </summary>
        public String FirstClassStationName {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站名称Str
        /// </summary>
        public String FirstClassStationNameStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public Int32 BranchId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司IdStr
        /// </summary>
        public String BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线Id
        /// </summary>
        public Int32 ServiceProductLineId {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线IdStr
        /// </summary>
        public String ServiceProductLineIdStr {
            get;
            set;
        }

        /// <summary>
        /// 产品线类型
        /// </summary>
        public Int32 ProductLineType {
            get;
            set;
        }

        /// <summary>
        /// 产品线类型Str
        /// </summary>
        public String ProductLineTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔人
        /// </summary>
        public String ClaimReqPerson {
            get;
            set;
        }

        /// <summary>
        /// 索赔人Str
        /// </summary>
        public String ClaimReqPersonStr {
            get;
            set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public String ContactPhone {
            get;
            set;
        }

        /// <summary>
        /// 联系电话Str
        /// </summary>
        public String ContactPhoneStr {
            get;
            set;
        }

        /// <summary>
        /// 联系地址
        /// </summary>
        public String ContactAddress {
            get;
            set;
        }

        /// <summary>
        /// 联系地址Str
        /// </summary>
        public String ContactAddressStr {
            get;
            set;
        }

        /// <summary>
        /// 保修开始时间
        /// </summary>
        public DateTime WarrantyStartTime {
            get;
            set;
        }

        /// <summary>
        /// 保修开始时间Str
        /// </summary>
        public String WarrantyStartTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 申请索赔时间
        /// </summary>
        public DateTime ClaimReqTime {
            get;
            set;
        }

        /// <summary>
        /// 申请索赔时间Str
        /// </summary>
        public String ClaimReqTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 工时费
        /// </summary>
        public Decimal LaborCost {
            get;
            set;
        }

        /// <summary>
        /// 工时费Str
        /// </summary>
        public String LaborCostStr {
            get;
            set;
        }

        /// <summary>
        /// 材料费
        /// </summary>
        public Decimal MaterialCost {
            get;
            set;
        }

        /// <summary>
        /// 材料费Str
        /// </summary>
        public String MaterialCostStr {
            get;
            set;
        }

        /// <summary>
        /// 配件管理费
        /// </summary>
        public Decimal PartsManagementCost {
            get;
            set;
        }

        /// <summary>
        /// 配件管理费Str
        /// </summary>
        public String PartsManagementCostStr {
            get;
            set;
        }

        /// <summary>
        /// 其他费用
        /// </summary>
        public Decimal OtherCost {
            get;
            set;
        }

        /// <summary>
        /// 其他费用Str
        /// </summary>
        public String OtherCostStr {
            get;
            set;
        }

        /// <summary>
        /// 其他费用产生原因
        /// </summary>
        public String OtherCostReason {
            get;
            set;
        }

        /// <summary>
        /// 其他费用产生原因Str
        /// </summary>
        public String OtherCostReasonStr {
            get;
            set;
        }

        /// <summary>
        /// 费用合计
        /// </summary>
        public Decimal TotalAmount {
            get;
            set;
        }

        /// <summary>
        /// 费用合计Str
        /// </summary>
        public String TotalAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件处理状态
        /// </summary>
        public Int32 UsedPartsDisposalStatus {
            get;
            set;
        }

        /// <summary>
        /// 旧件处理状态Str
        /// </summary>
        public String UsedPartsDisposalStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 初审意见
        /// </summary>
        public String InitialApprovertComment {
            get;
            set;
        }

        /// <summary>
        /// 初审意见Str
        /// </summary>
        public String InitialApprovertCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 会签意见
        /// </summary>
        public String FinalApproverComment {
            get;
            set;
        }

        /// <summary>
        /// 会签意见Str
        /// </summary>
        public String FinalApproverCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 审批意见历史
        /// </summary>
        public String ApproveCommentHistory {
            get;
            set;
        }

        /// <summary>
        /// 审批意见历史Str
        /// </summary>
        public String ApproveCommentHistoryStr {
            get;
            set;
        }

        /// <summary>
        /// 终审意见
        /// </summary>
        public String ServiceDepartmentComment {
            get;
            set;
        }

        /// <summary>
        /// 终审意见Str
        /// </summary>
        public String ServiceDepartmentCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public String Remark {
            get;
            set;
        }

        /// <summary>
        /// 备注Str
        /// </summary>
        public String RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public Int32 CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人IdStr
        /// </summary>
        public String CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public String CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建人Str
        /// </summary>
        public String CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 创建时间Str
        /// </summary>
        public String CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public Int32 ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人IdStr
        /// </summary>
        public String ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public String ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改人Str
        /// </summary>
        public String ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 修改时间Str
        /// </summary>
        public String ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public Int32 AbandonerId {
            get;
            set;
        }

        /// <summary>
        /// 作废人IdStr
        /// </summary>
        public String AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public String AbandonerName {
            get;
            set;
        }

        /// <summary>
        /// 作废人Str
        /// </summary>
        public String AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime AbandonTime {
            get;
            set;
        }

        /// <summary>
        /// 作废时间Str
        /// </summary>
        public String AbandonTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 终审人Id
        /// </summary>
        public Int32 ApproverId {
            get;
            set;
        }

        /// <summary>
        /// 终审人IdStr
        /// </summary>
        public String ApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 终审人
        /// </summary>
        public String ApproverName {
            get;
            set;
        }

        /// <summary>
        /// 终审人Str
        /// </summary>
        public String ApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 终审时间
        /// </summary>
        public DateTime ApproveTime {
            get;
            set;
        }

        /// <summary>
        /// 终审时间Str
        /// </summary>
        public String ApproveTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 初审人Id
        /// </summary>
        public Int32 InitialApproverId {
            get;
            set;
        }

        /// <summary>
        /// 初审人IdStr
        /// </summary>
        public String InitialApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 初审人
        /// </summary>
        public String InitialApproverName {
            get;
            set;
        }

        /// <summary>
        /// 初审人Str
        /// </summary>
        public String InitialApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 初审时间
        /// </summary>
        public DateTime InitialApproverTime {
            get;
            set;
        }

        /// <summary>
        /// 初审时间Str
        /// </summary>
        public String InitialApproverTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 会签人Id
        /// </summary>
        public Int32 FinalApproverId {
            get;
            set;
        }

        /// <summary>
        /// 会签人IdStr
        /// </summary>
        public String FinalApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 会签人
        /// </summary>
        public String FinalApproverName {
            get;
            set;
        }

        /// <summary>
        /// 会签人Str
        /// </summary>
        public String FinalApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 会签时间
        /// </summary>
        public DateTime FinalApproverTime {
            get;
            set;
        }

        /// <summary>
        /// 会签时间Str
        /// </summary>
        public String FinalApproverTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回人Id
        /// </summary>
        public Int32 RejectId {
            get;
            set;
        }

        /// <summary>
        /// 驳回人IdStr
        /// </summary>
        public String RejectIdStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回人
        /// </summary>
        public String RejectName {
            get;
            set;
        }

        /// <summary>
        /// 驳回人Str
        /// </summary>
        public String RejectNameStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回时间
        /// </summary>
        public DateTime RejectTime {
            get;
            set;
        }

        /// <summary>
        /// 驳回时间Str
        /// </summary>
        public String RejectTimeStr {
            get;
            set;
        }
    }
}
