﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsExchangeGroupExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 互换号
        /// </summary>
        public string ExchangeCode {
            get;
            set;
        }
        /// <summary>
        /// 互换号_Str
        /// </summary>
        public string ExchangeCodeStr {
            get;
            set;
        }
        /// <summary>
        /// 互换分组号
        /// </summary>
        public string ExGroupCode {
            get;
            set;
        }
        /// <summary>
        /// 互换分组号_Str
        /// </summary>
        public string ExGroupCodeStr {
            get;
            set;
        }
        /// <summary>
        /// 标识
        /// </summary>
        public string ExGroupExchangeCode {
            get;
            set;
        }

        /// <summary>
        /// 配件
        /// </summary>
        public string PartCode {
            get;
            set;
        }

        public int PartId {
            get;
            set;
        }
    }
}
