﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsInventoryDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件盘点单Id
        /// </summary>
        public int PartsInventoryBillId {
            get;
            set;
        }
        /// <summary>
        /// 配件盘点单Id_Str
        /// </summary>
        public string PartsInventoryBillIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 库位Id
        /// </summary>
        public int WarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 库位Id_Str
        /// </summary>
        public string WarehouseAreaIdStr {
            get;
            set;
        }

        /// <summary>
        /// 库位编号
        /// </summary>
        public string WarehouseAreaCode {
            get;
            set;
        }
        /// <summary>
        /// 库位编号_Str
        /// </summary>
        public string WarehouseAreaCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 当前批次号
        /// </summary>
        public string CurrentBatchNumber {
            get;
            set;
        }
        /// <summary>
        /// 当前批次号_Str
        /// </summary>
        public string CurrentBatchNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 当前库存
        /// </summary>
        public int CurrentStorage {
            get;
            set;
        }
        /// <summary>
        /// 当前库存_Str
        /// </summary>
        public string CurrentStorageStr {
            get;
            set;
        }

        /// <summary>
        /// 盘点后库存
        /// </summary>
        public int StorageAfterInventory {
            get;
            set;
        }
        /// <summary>
        /// 盘点后库存_Str
        /// </summary>
        public string StorageAfterInventoryStr {
            get;
            set;
        }

        /// <summary>
        /// 是否确认覆盖
        /// </summary>
        public bool? Ifcover {
            get;
            set;
        }
        /// <summary>
        /// 是否确认覆盖_Str
        /// </summary>
        public string IfcoverStr {
            get;
            set;
        }

        /// <summary>
        /// 库存差异
        /// </summary>
        public int StorageDifference {
            get;
            set;
        }
        /// <summary>
        /// 库存差异_Str
        /// </summary>
        public string StorageDifferenceStr {
            get;
            set;
        }

        /// <summary>
        /// 成本价
        /// </summary>
        public Decimal? CostPrice {
            get;
            set;
        }
        /// <summary>
        /// 成本价_Str
        /// </summary>
        public string CostPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 差异金额
        /// </summary>
        public Decimal? AmountDifference {
            get;
            set;
        }
        /// <summary>
        /// 差异金额_Str
        /// </summary>
        public string AmountDifferenceStr {
            get;
            set;
        }

        /// <summary>
        /// 新批次号
        /// </summary>
        public string NewBatchNumber {
            get;
            set;
        }
        /// <summary>
        /// 新批次号_Str
        /// </summary>
        public string NewBatchNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }
    }
}
