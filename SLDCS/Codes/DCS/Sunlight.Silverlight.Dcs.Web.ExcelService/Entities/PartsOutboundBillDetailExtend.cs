﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsOutboundBillDetailExtend {

        public int Id {
            get;
            set;
        }
        public int PartsOutboundBillId {
            get;
            set;
        }
        public int SparePartId {
            get;
            set;
        }
        public string SparePartCode {
            get;
            set;
        }
        public int OutboundAmount {
            get;
            set;
        }
        public decimal SettlementPrice {
            get;
            set;
        }
        public decimal OriginalPrice {
            get;
            set;
        }
    }
}
