﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class PartsPriceIncreaseRateExtend {
        /// <summary> 
        /// Id 
        /// </summary> 
        public int Id {
            get;
            set;
        }
        public string CategoryCode {
            get;
            set;
        }

        public string CategoryName {
            get;
            set;
        }
        public string Brand {
            get;
            set;
        }
        public int BrandId {
            get;
            set;
        }

        public double? SaleIncreaseRate {
            get;
            set;
        }

        public double? retailIncreaseRate {
            get;
            set;
        }

        public double? MaxSalesPriceFloating {
            get;
            set;
        }

        public double? MinSalesPriceFloating {
            get;
            set;
        }

        public double? MaxRetailOrderPriceFloating {
            get;
            set;
        }

        public double? MinRetailOrderPriceFloating {
            get;
            set;
        }

        public int Status {
            get;
            set;
        }

        public string SaleIncreaseRateStr {
            get;
            set;
        }

        public string retailIncreaseRateStr {
            get;
            set;
        }

        public string MaxSalesPriceFloatingStr {
            get;
            set;
        }

        public string MinSalesPriceFloatingStr {
            get;
            set;
        }

        public string MaxRetailOrderPriceFloatingStr {
            get;
            set;
        }

        public string MinRetailOrderPriceFloatingStr {
            get;
            set;
        }

        public string StatusStr {
            get;
            set;
        }

        public string Remark {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }
    }
}
