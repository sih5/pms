﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsPurchaseOrderDetailExtend {

        /// <summary>
        /// 订单类型名称_Str
        /// </summary>
        public string PartsPurchaseOrderTypeNameStr {
            get;
            set;
        }


        /// <summary>
        /// 规格型号_Str
        /// </summary>
        public string SpecificationStr {
            get;
            set;
        }

        /// <summary>
        /// 规格型号
        /// </summary>
        public string Specification {
            get;
            set;
        }

        /// <summary>
        /// 供应商名称_Str
        /// </summary>
        public string SupplierPartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 采购订单Id
        /// </summary>
        public int PartsPurchaseOrderId {
            get;
            set;
        }
        /// <summary>
        /// 采购订单Id_Str
        /// </summary>
        public string PartsPurchaseOrderIdStr {
            get;
            set;
        }

        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber {
            get;
            set;
        }
        /// <summary>
        /// 序号_Str
        /// </summary>
        public string SerialNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商图号
        /// </summary>
        public string SupplierPartCode {
            get;
            set;
        }
        /// <summary>
        /// 供应商图号_Str
        /// </summary>
        public string SupplierPartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 承诺到货时间
        /// </summary>
        public DateTime? PromisedDeliveryTime {
            get;
            set;
        }
        /// <summary>
        /// 承诺到货时间_Str
        /// </summary>
        public string PromisedDeliveryTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 单价
        /// </summary>
        public Decimal UnitPrice {
            get;
            set;
        }
        /// <summary>
        /// 单价_Str
        /// </summary>
        public string UnitPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 订货量
        /// </summary>
        public int OrderAmount {
            get;
            set;
        }
        /// <summary>
        /// 订货量_Str
        /// </summary>
        public string OrderAmountStr {
            get;
            set;
        }
        /// <summary>
        /// 主单备注
        /// </summary>
        public string OrderRemark {
            get;
            set;
        }
        /// <summary>
        /// 主单备注_Str
        /// </summary>
        public string OrderRemarkStr {
            get;
            set;
        }
        /// <summary>
        /// 清单备注
        /// </summary>
        public string DetailRemark {
            get;
            set;
        }
        /// <summary>
        /// 清单备注_Str
        /// </summary>
        public string DetailRemarkStr {
            get;
            set;
        }
        /// <summary>
        /// 确认量
        /// </summary>
        public int ConfirmedAmount {
            get;
            set;
        }
        /// <summary>
        /// 确认量_Str
        /// </summary>
        public string ConfirmedAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 发运量
        /// </summary>
        public int? ShippingAmount {
            get;
            set;
        }
        /// <summary>
        /// 发运量_Str
        /// </summary>
        public string ShippingAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }
        /// <summary>
        /// 计量单位_Str
        /// </summary>
        public string MeasureUnitStr {
            get;
            set;
        }

        /// <summary>
        /// 包装数量
        /// </summary>
        public int? PackingAmount {
            get;
            set;
        }
        /// <summary>
        /// 包装数量_Str
        /// </summary>
        public string PackingAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 包装规格
        /// </summary>
        public string PackingSpecification {
            get;
            set;
        }
        /// <summary>
        /// 包装规格_Str
        /// </summary>
        public string PackingSpecificationStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 确认备注
        /// </summary>
        public string ConfirmationRemark {
            get;
            set;
        }
        /// <summary>
        /// 确认备注_Str
        /// </summary>
        public string ConfirmationRemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 采购订单编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 采购订单编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 分公司编号
        /// </summary>
        public string BranchCode {
            get;
            set;
        }
        /// <summary>
        /// 分公司编号_Str
        /// </summary>
        public string BranchCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 分公司名称
        /// </summary>
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 分公司名称_Str
        /// </summary>
        public string BranchNameStr {
            get;
            set;
        }

        /// <summary>
        /// 仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 仓库Id_Str
        /// </summary>
        public string WarehouseIdStr {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        public string WarehouseName {
            get;
            set;
        }
        /// <summary>
        /// 仓库名称_Str
        /// </summary>
        public string WarehouseNameStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id_Str
        /// </summary>
        public string PartsSalesCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 销售类型名称
        /// </summary>
        public string PartsSalesCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 销售类型名称_Str
        /// </summary>
        public string PartsSalesCategoryNameStr {
            get;
            set;
        }

        /// <summary>
        /// 收货单位Id
        /// </summary>
        public int? ReceivingCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 收货单位Id_Str
        /// </summary>
        public string ReceivingCompanyIdStr {
            get;
            set;
        }

        /// <summary>
        /// 收货单位名称
        /// </summary>
        public string ReceivingCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 收货单位名称_Str
        /// </summary>
        public string ReceivingCompanyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 收货地址
        /// </summary>
        public string ReceivingAddress {
            get;
            set;
        }
        /// <summary>
        /// 收货地址_Str
        /// </summary>
        public string ReceivingAddressStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商Id
        /// </summary>
        public int PartsSupplierId {
            get;
            set;
        }
        /// <summary>
        /// 供应商Id_Str
        /// </summary>
        public string PartsSupplierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string PartsSupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 供应商编号_Str
        /// </summary>
        public string PartsSupplierCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string PartsSupplierName {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称_Str
        /// </summary>
        public string PartsSupplierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 是否直供
        /// </summary>
        public bool IfDirectProvision {
            get;
            set;
        }
        /// <summary>
        /// 是否直供_Str
        /// </summary>
        public string IfDirectProvisionStr {
            get;
            set;
        }

        /// <summary>
        /// 原始需求单据Id
        /// </summary>
        public int? OriginalRequirementBillId {
            get;
            set;
        }
        /// <summary>
        /// 原始需求单据Id_Str
        /// </summary>
        public string OriginalRequirementBillIdStr {
            get;
            set;
        }

        /// <summary>
        /// 原始需求单据类型
        /// </summary>
        public int? OriginalRequirementBillType {
            get;
            set;
        }
        /// <summary>
        /// 原始需求单据类型_Str
        /// </summary>
        public string OriginalRequirementBillTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 原始需求单据编号
        /// </summary>
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        /// <summary>
        /// 原始需求单据编号_Str
        /// </summary>
        public string OriginalRequirementBillCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 总金额
        /// </summary>
        public Decimal TotalAmount {
            get;
            set;
        }
        /// <summary>
        /// 总金额_Str
        /// </summary>
        public string TotalAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 配件采购订单类型Id
        /// </summary>
        public int PartsPurchaseOrderTypeId {
            get;
            set;
        }
        /// <summary>
        /// 配件采购订单类型Id_Str
        /// </summary>
        public string PartsPurchaseOrderTypeIdStr {
            get;
            set;
        }

        /// <summary>
        /// 要求到货时间
        /// </summary>
        public DateTime RequestedDeliveryTime {
            get;
            set;
        }
        /// <summary>
        /// 要求到货时间_Str
        /// </summary>
        public string RequestedDeliveryTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 计划来源
        /// </summary>
        public string PlanSource {
            get;
            set;
        }
        /// <summary>
        /// 计划来源_Str
        /// </summary>
        public string PlanSourceStr {
            get;
            set;
        }
        /// <summary>
        /// GPMS采购计划单号
        /// </summary>
        public string GPMSPurOrderCode {
            get;
            set;
        }
        /// <summary>
        /// GPMS采购计划单号_Str
        /// </summary>
        public string GPMSPurOrderCodeStr {
            get;
            set;
        }
        /// <summary>
        /// SAP采购计划单号
        /// </summary>
        public string SAPPurchasePlanCode {
            get;
            set;
        }
        /// <summary>
        /// SAP采购计划单号_Str
        /// </summary>
        public string SAPPurchasePlanCodeStr {
            get;
            set;
        }
        /// <summary>
        /// 发运方式
        /// </summary>
        public int ShippingMethod {
            get;
            set;
        }
        /// <summary>
        /// 发运方式_Str
        /// </summary>
        public string ShippingMethodStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }


        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id_Str
        /// </summary>
        public string AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废人_Str
        /// </summary>
        public string AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// 作废时间_Str
        /// </summary>
        public string AbandonTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 提交人Id
        /// </summary>
        public int? SubmitterId {
            get;
            set;
        }
        /// <summary>
        /// 提交人Id_Str
        /// </summary>
        public string SubmitterIdStr {
            get;
            set;
        }

        /// <summary>
        /// 提交人
        /// </summary>
        public string SubmitterName {
            get;
            set;
        }
        /// <summary>
        /// 提交人_Str
        /// </summary>
        public string SubmitterNameStr {
            get;
            set;
        }

        /// <summary>
        /// 提交时间
        /// </summary>
        public DateTime? SubmitTime {
            get;
            set;
        }
        /// <summary>
        /// 提交时间_Str
        /// </summary>
        public string SubmitTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 审批人Id
        /// </summary>
        public int? ApproverId {
            get;
            set;
        }
        /// <summary>
        /// 审批人Id_Str
        /// </summary>
        public string ApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 审批人
        /// </summary>
        public string ApproverName {
            get;
            set;
        }
        /// <summary>
        /// 审批人_Str
        /// </summary>
        public string ApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 审批时间
        /// </summary>
        public DateTime? ApproveTime {
            get;
            set;
        }
        /// <summary>
        /// 审批时间_Str
        /// </summary>
        public string ApproveTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 终止人Id
        /// </summary>
        public int? CloserId {
            get;
            set;
        }
        /// <summary>
        /// 终止人Id_Str
        /// </summary>
        public string CloserIdStr {
            get;
            set;
        }

        /// <summary>
        /// 终止人
        /// </summary>
        public string CloserName {
            get;
            set;
        }
        /// <summary>
        /// 终止人_Str
        /// </summary>
        public string CloserNameStr {
            get;
            set;
        }

        /// <summary>
        /// 终止时间
        /// </summary>
        public DateTime? CloseTime {
            get;
            set;
        }
        /// <summary>
        /// 终止时间_Str
        /// </summary>
        public string CloseTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }

        /// <summary>
        /// 订单汇总标识
        /// </summary>
        public string OrderCollectMark {
            get;
            set;
        }

        /// <summary>
        /// PO单号
        /// </summary>
        public string POCode {
            get;
            set;
        }

    }
}
