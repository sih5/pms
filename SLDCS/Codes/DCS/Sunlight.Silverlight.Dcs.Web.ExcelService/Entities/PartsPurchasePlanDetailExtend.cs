﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsPurchasePlanDetailExtend {

        /// <summary>
        /// Id
        /// </summary>
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr
        {
            get;
            set;
        }

        /// <summary>
        /// 采购计划单Id
        /// </summary>
        public int PurchasePlanId
        {
            get;
            set;
        }
        /// <summary>
        /// 采购计划单Id_Str
        /// </summary>
        public string PurchasePlanIdStr
        {
            get;
            set;
        }

        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber
        {
            get;
            set;
        }
        /// <summary>
        /// 序号_Str
        /// </summary>
        public string SerialNumberStr
        {
            get;
            set;
        } 
        /// <summary>
          /// 供应商图号
          /// </summary>
        public string SupplierPartCode
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商图号_Str
        /// </summary>
        public string SupplierPartCodeStr
        {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId
        {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr
        {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode
        {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr
        {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName
        {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr
        {
            get;
            set;
        }

        /// <summary>
        /// 计划量
        /// </summary>
        public int PlanAmount
        {
            get;
            set;
        }
        /// <summary>
        /// 计划量_Str
        /// </summary>
        public string PlanAmountStr
        {
            get;
            set;
        }

        /// <summary>
        /// 单位
        /// </summary>
        public string MeasureUnit
        {
            get;
            set;
        }
        /// <summary>
        /// 单位_Str
        /// </summary>
        public string MeasureUnitStr
        {
            get;
            set;
        }

        /// <summary>
        /// 价格
        /// </summary>
        public decimal Price
        {
            get;
            set;
        }
        /// <summary>
        /// 价格
        /// </summary>
        public string PriceStr
        {
            get;
            set;
        }

        /// <summary>
        /// 要求到货时间
        /// </summary>
        public DateTime PromisedDeliveryTime
        {
            get;
            set;
        }
        /// <summary>
        /// 要求到货时间
        /// </summary>
        public string PromisedDeliveryTimeStr
        {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        /// <summary>
        /// 闭口数量
        /// </summary>
        public int? LimitQty {
            get;
            set;
        }
        /// <summary>
        /// 闭口数量_Str
        /// </summary>
        public string LimitQtyStr {
            get;
            set;
        }

        /// <summary>
        /// 已使用数量
        /// </summary>
        public int? UsedQty {
            get;
            set;
        }
        /// <summary>
        /// 已使用数量_Str
        /// </summary>
        public string UsedQtyStr {
            get;
            set;
        }
        public int? PartABC {
            get;
            set;
        }
    }
}
