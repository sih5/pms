﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsPurchasePriceExtend {
        public int SparePartId {
            get;
            set;
        }

        public decimal PartsPurchasePrice {
            get;
            set;
        }

    }
}
