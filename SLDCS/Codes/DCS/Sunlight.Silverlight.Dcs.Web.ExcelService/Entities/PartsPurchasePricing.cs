﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsPurchasePricing {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int PartId {
            get;
            set;
        }

        /// <summary>
        /// 配件供应商Id
        /// </summary>
        public int PartsSupplierId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型名称
        /// </summary>
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 生效时间
        /// </summary>
        public DateTime ValidFrom {
            get;
            set;
        }

        /// <summary>
        /// 失效时间
        /// </summary>
        public DateTime ValidTo {
            get;
            set;
        }

        /// <summary>
        /// 价格类型
        /// </summary>
        public int PriceType {
            get;
            set;
        }

        /// <summary>
        /// 采购价格
        /// </summary>
        public Decimal PurchasePrice {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int AbandonerId {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime AbandonTime {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
    }
}
