﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsRebateApplicationExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 申请单编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 申请单编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 客户企业Id
        /// </summary>
        public int CustomerCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 客户企业编号
        /// </summary>
        public string CustomerCompanyCode {
            get;
            set;
        }
        /// <summary>
        /// 客户企业编号_Str
        /// </summary>
        public string CustomerCompanyCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 客户企业名称
        /// </summary>
        public string CustomerCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 客户企业名称_Str
        /// </summary>
        public string CustomerCompanyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 销售类型
        /// </summary>
        public string PartsSalesCategoryStr {
            get;
            set;
        }

        /// <summary>
        /// 返利类型Id
        /// </summary>
        public int PartsRebateTypeId {
            get;
            set;
        }

        /// <summary>
        /// 返利类型
        /// </summary>
        public string PartsRebateTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 账户组Id
        /// </summary>
        public int AccountGroupId {
            get;
            set;
        }

        /// <summary>
        /// 账户组
        /// </summary>
        public string AccountGroupStr {
            get;
            set;
        }

        /// <summary>
        /// 返利方向
        /// </summary>
        public int RebateDirection {
            get;
            set;
        }

        /// <summary>
        /// 返利方向
        /// </summary>
        public string RebateDirectionStr {
            get;
            set;
        }

        /// <summary>
        /// 金额
        /// </summary>
        public Decimal Amount {
            get;
            set;
        }
        /// <summary>
        /// 金额_Str
        /// </summary>
        public string AmountStr {
            get;
            set;
        }

        /// <summary>
        /// 审批意见
        /// </summary>
        public string ApprovalComment {
            get;
            set;
        }
        /// <summary>
        /// 审批意见_Str
        /// </summary>
        public string ApprovalCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 申请原因
        /// </summary>
        public string Motive {
            get;
            set;
        }
        /// <summary>
        /// 申请原因_Str
        /// </summary>
        public string MotiveStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 审批人Id
        /// </summary>
        public int? ApproverId {
            get;
            set;
        }

        /// <summary>
        /// 审批人
        /// </summary>
        public string ApproverName {
            get;
            set;
        }

        /// <summary>
        /// 审批时间
        /// </summary>
        public DateTime? ApproveTime {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }
    }
}
