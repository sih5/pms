﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsRebateTypeExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件返利类型编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 配件返利类型编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件返利类型名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 配件返利类型名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
    }
}
