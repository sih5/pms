﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsRetailOrderExtend {
        /// <summary>
        /// 配件Id 
        /// </summary>
        public int PartsId {
            get;
            set;
        }
        /// <summary>
        /// 配件编号 
        /// </summary>
        public string PartsCode {
            get;
            set;
        }
        /// <summary>
        /// 配件名称 
        /// </summary>
        public string PartsName {
            get;
            set;
        }
        /// <summary>
        /// 数量Str
        /// </summary>
        public string QuantityStr {
            get;
            set;
        }
        /// <summary>
        /// 折扣后价格Str 
        /// </summary>
        public string DiscountPriceStr {
            get;
            set;
        }
        /// <summary>
        /// 数量 
        /// </summary>
        public int Quantity {
            get;
            set;
        }
        /// <summary>
        /// 折扣后价格 
        /// </summary>
        public decimal DiscountPrice {
            get;
            set;
        }
        /// <summary>
        /// 零售指导价 
        /// </summary>
        public decimal Price {
            get;
            set;
        }
        /// <summary>
        /// 备注 
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

    }
}
