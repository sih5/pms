﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsSalePriceIncreaseRateExtend {
        public int Id {
            get;
            set;
        }
        public string GroupCode {
            get;
            set;
        }
        public string GroupName {
            get;
            set;
        }

        public decimal IncreaseRate {
            get;
            set;
        }
        public int Status {
            get;
            set;
        }
    }
}
