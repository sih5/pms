﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsSalesCategory {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 销售类型编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 销售类型名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 分公司编号
        /// </summary>
        public string BranchCode {
            get;
            set;
        }
        /// <summary>
        /// 分公司名称
        /// </summary>
        public string BranchName {
            get;
            set;
        }
        /// <summary>
        /// 是否用于索赔
        /// </summary>
        public bool IsForClaim {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id
        /// </summary>
        public int CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id
        /// </summary>
        public int ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id
        /// </summary>
        public int AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime RowVersion {
            get;
            set;
        }
    }
}
