﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsSalesOrderDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售订单Id
        /// </summary>
        public int PartsSalesOrderId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售订单Id_Str
        /// </summary>
        public string PartsSalesOrderIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }
        /// <summary>
        /// 计量单位_Str
        /// </summary>
        public string MeasureUnitStr {
            get;
            set;
        }

        /// <summary>
        /// 订货数量
        /// </summary>
        public int OrderedQuantity {
            get;
            set;
        }
        /// <summary>
        /// 订货数量_Str
        /// </summary>
        public string OrderedQuantityStr {
            get;
            set;
        }

        /// <summary>
        /// 审批数量
        /// </summary>
        public int? ApproveQuantity {
            get;
            set;
        }
        /// <summary>
        /// 审批数量_Str
        /// </summary>
        public string ApproveQuantityStr {
            get;
            set;
        }

        /// <summary>
        /// 建议售价
        /// </summary>
        public Decimal OriginalPrice {
            get;
            set;
        }
        /// <summary>
        /// 建议售价_Str
        /// </summary>
        public string OriginalPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 价格等级系数
        /// </summary>
        public Decimal CustOrderPriceGradeCoefficient {
            get;
            set;
        }
        /// <summary>
        /// 价格等级系数_Str
        /// </summary>
        public string CustOrderPriceGradeCoefficientStr {
            get;
            set;
        }

        /// <summary>
        /// 是否允许替互换
        /// </summary>
        public bool IfCanNewPart {
            get;
            set;
        }
        /// <summary>
        /// 是否允许替互换_Str
        /// </summary>
        public string IfCanNewPartStr {
            get;
            set;
        }

        /// <summary>
        /// 价格折让
        /// </summary>
        public Decimal DiscountedPrice {
            get;
            set;
        }
        /// <summary>
        /// 价格折让_Str
        /// </summary>
        public string DiscountedPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 订货价格
        /// </summary>
        public Decimal OrderPrice {
            get;
            set;
        }
        /// <summary>
        /// 订货价格_Str
        /// </summary>
        public string OrderPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 订货金额
        /// </summary>
        public Decimal OrderSum {
            get;
            set;
        }
        /// <summary>
        /// 订货金额_Str
        /// </summary>
        public string OrderSumStr {
            get;
            set;
        }

        /// <summary>
        /// 预计满足时间
        /// </summary>
        public DateTime? EstimatedFulfillTime {
            get;
            set;
        }
        /// <summary>
        /// 预计满足时间_Str
        /// </summary>
        public string EstimatedFulfillTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }
        public int OutboundAmount {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string OrderNo {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string OrderNoStr {
            get;
            set;
        }
    }
}
