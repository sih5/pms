﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsSalesOrderExtend {
        public int Id {
            get;
            set;
        }
        public string Code {
            get;
            set;
        }
        public int BranchId {
            get;
            set;
        }
        public string BranchCode {
            get;
            set;
        }
        public string BranchName {
            get;
            set;
        }
        public int WarehouseId {
            get;
            set;
        }
        public string WarehouseCode {
            get;
            set;
        }
        public string WarehouseName {
            get;
            set;
        }

        public int PartsOutboundBillId {
            get;
            set;
        }
        public int SparePartId {
            get;
            set;
        }
        public string SparePartCode {
            get;
            set;
        }
        public string SparePartName {
            get;
            set;
        }
    }
}
