﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsSalesOrderType {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 分公司编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 分公司名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
    }
}
