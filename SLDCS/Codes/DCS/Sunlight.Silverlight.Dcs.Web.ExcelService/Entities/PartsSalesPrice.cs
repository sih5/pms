﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsSalesPrice{
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int? PartsSalesCategoryId {
            get;
            set;
        }

        public DateTime? ValidationTime { 
            get;
            set;
        }

        public DateTime? ExpireTime { 
            get;
            set;
        }
        /// <summary>
        /// 服务站价
        /// </summary>
        public decimal SalesPrice { 
        get;
            set;
        }
    }
}
