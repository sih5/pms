﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsSalesPriceChangeDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 互换件最高价
        /// </summary>
        public decimal MaxExchangeSalePrice {
            get;
            set;
        }

        /// <summary>
        /// 申请单Id
        /// </summary>
        public int ParentId {
            get;
            set;
        }
        /// <summary>
        /// 申请单Id_Str
        /// </summary>
        public string ParentIdStr {
            get;
            set;
        }

        /// <summary>
        /// 品牌Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }

        public string PartsSalesCategoryCode {
            get;
            set;
        }
        public string PartsSalesCategoryName {
            get;
            set;
        }
        public string PartsSalesCategoryNameStr {
            get;
            set;
        }
        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 销售价
        /// </summary>
        public Decimal SalesPrice {
            get;
            set;
        }
        /// <summary>
        /// 销售价_Str
        /// </summary>
        public string SalesPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 零售指导价
        /// </summary>
        public Decimal? RetailGuidePrice {
            get;
            set;
        }
        /// <summary>
        /// 零售指导价_Str
        /// </summary>
        public string RetailGuidePriceStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站批发价
        /// </summary>
        public Decimal? DealerSalesPrice {
            get;
            set;
        }
        /// <summary>
        /// 服务站批发价_Str
        /// </summary>
        public string DealerSalesPriceStr {
            get;
            set;
        }


        /// <summary>
        /// 价格类型
        /// </summary>
        public int PriceType {
            get;
            set;
        }
        /// <summary>
        /// 价格类型_Str
        /// </summary>
        public string PriceTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }

        /// <summary>
        /// 中心库价
        /// </summary>
        public Decimal? CenterPrice {
            get;
            set;
        }
        /// <summary>
        /// 中心库价_Str
        /// </summary>
        public string CenterPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 价格类型
        /// </summary>
        public string PriceTypeName {
            get;
            set;
        }
        /// <summary>
        /// 价格类型_Str
        /// </summary>
        public string PriceTypeNameStr {
            get;
            set;
        }

        public int PriceTypeId {
            get;
            set;
        }
        public string PriceTypeCode {
            get;
            set;
        }
    }
}
