﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsSalesReturnBillDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售退货Id
        /// </summary>
        public int PartsSalesReturnBillId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售退货Id_Str
        /// </summary>
        public string PartsSalesReturnBillIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }
        /// <summary>
        /// 计量单位_Str
        /// </summary>
        public string MeasureUnitStr {
            get;
            set;
        }

        /// <summary>
        /// 退货数量
        /// </summary>
        public int ReturnedQuantity {
            get;
            set;
        }
        /// <summary>
        /// 退货数量_Str
        /// </summary>
        public string ReturnedQuantityStr {
            get;
            set;
        }

        /// <summary>
        /// 审批数量
        /// </summary>
        public int? ApproveQuantity {
            get;
            set;
        }
        /// <summary>
        /// 审批数量_Str
        /// </summary>
        public string ApproveQuantityStr {
            get;
            set;
        }

        /// <summary>
        /// 原始订单价格
        /// </summary>
        public Decimal? OriginalOrderPrice {
            get;
            set;
        }
        /// <summary>
        /// 原始订单价格_Str
        /// </summary>
        public string OriginalOrderPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 退货价格
        /// </summary>
        public Decimal ReturnPrice {
            get;
            set;
        }
        /// <summary>
        /// 退货价格_Str
        /// </summary>
        public string ReturnPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 批次号
        /// </summary>
        public string BatchNumber {
            get;
            set;
        }
        /// <summary>
        /// 批次号_Str
        /// </summary>
        public string BatchNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售订单Id
        /// </summary>
        public int PartsSalesOrderId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售订单编号
        /// </summary>
        public string PartsSalesOrderCode {
            get;
            set;
        }

        /// <summary>
        /// 配件出库单Id
        /// </summary>
        public int PartsOutboundBillId {
            get;
            set;
        }

        /// <summary>
        /// 原始价格
        /// </summary>
        public Decimal OriginalPrice {
            get;
            set;
        }

        public string SIHLabelCode {
            get;
            set;
        }
        public string SIHLabelCodeStr {
            get;
            set;
        }
        /// <summary>
        /// 追溯属性
        /// </summary>
        public int? TraceProperty {
            get;
            set;
        }
    }
}
