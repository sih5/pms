﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class PartsSalesReturnBillExtend {
        // 分公司编号
        public string BranchCode {
            get;
            set;
        }
        public int BranchId {
            get;
            set;
        }
        public string BranchName {
            get;
            set;
        }
        //销售组织名称
        public string SalesUnitName {
            get;
            set;
        }
        //退货类型
        public string ReturnTypeStr {
            get;
            set;
        }
        public int ReturnType {
            get;
            set;
        }
        //销售订单编号
        public string PartsSalesOrderCode {
            get;
            set;
        }

        public int PartsSalesOrderId {
            get;
            set;
        }
        //开票要求
        public string InvoiceRequirementStr {
            get;
            set;
        }
        public int InvoiceRequirement {
            get;
            set;
        }
        //蓝字发票号
        public string BlueInvoiceNumber {
            get;
            set;
        }
        //发运方式
        public string ShippingMethodStr {
            get;
            set;
        }
        public int ShippingMethod {
            get;
            set;
        }
        // 退货原因
        public string ReturnReason {
            get;
            set;
        }
        // 联系人
        public string ContactPerson {
            get;
            set;
        }
        // 联系电话
        public string ContactPhone {
            get;
            set;
        }
        //备注
        public string Remark {
            get;
            set;
        }
        //配件Id
        public int SparePartId {
            get;
            set;
        }
        //配件编号
        public string SparePartCode {
            get;
            set;
        }
        //配件名称
        public string SparePartName {
            get;
            set;
        }
        //退货数量
        public int ReturnedQuantity {
            get;
            set;
        }
        //退货数量
        public string ReturnedQuantityStr {
            get;
            set;
        }
        //清单备注
        public string RemarkDetail {
            get;
            set;
        }
        //退货仓库编号
        public string ReturnWarehouseCode {
            get;
            set;
        }
        //退货仓库Id
        public int ReturnWarehouseId {
            get;
            set;
        }
        //退货仓库名称
        public string ReturnWarehouseName {
            get;
            set;
        }
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        public string MeasureUnit {
            get;
            set;
        }
        public decimal OriginalPrice {
            get;
            set;
        }
        public decimal OrderPrice {
            get;
            set;
        }
        //总结算金额
        public decimal TotalAmount {
            get;
            set;
        }
        //退货单位Id
        public int ReturnCompanyId {
            get;
            set;
        }
        //退货单位编号
        public string ReturnCompanyCode {
            get;
            set;
        }
        //退货单位名称
        public string ReturnCompanyName {
            get;
            set;
        }
        //收票单位Id
        public int InvoiceReceiveCompanyId {
            get;
            set;
        }
        //收票单位编号
        public string InvoiceReceiveCompanyCode {
            get;
            set;
        }
        //收票单位名称
        public string InvoiceReceiveCompanyName {
            get;
            set;
        }
        //提报单位Id
        public int SubmitCompanyId {
            get;
            set;
        }
        //提报单位编号
        public string SubmitCompanyCode {
            get;
            set;
        }
        //提报单位名称
        public string SubmitCompanyName {
            get;
            set;
        }
        //销售组织隶属企业Id
        public int SalesUnitOwnerCompanyId {
            get;
            set;
        }
        //销售组织隶属企业编号
        public string SalesUnitOwnerCompanyCode {
            get;
            set;
        }
        //销售组织隶属企业名称
        public string SalesUnitOwnerCompanyName {
            get;
            set;
        }
        //客户账户Id
        public int CustomerAccountId {
            get;
            set;
        }
        public int SalesUnitId {
            get;
            set;
        }
        public int WarehouseId {
            get;
            set;
        }
        public string WarehouseCode {
            get;
            set;
        }
        public string WarehouseName {
            get;
            set;
        }
        //出库数量
        public int OutboundAmount {
            get;
            set;
        }
        public int Id {
            get;
            set;
        }
        public decimal ReturnPrice {
            get;
            set;
        }
        public string ReturnPriceStr {
            get;
            set;
        }
        public decimal OriginalOrderPrice {
            get;
            set;
        }
    }
}
