﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class PartsShippingOrderExtend {
        /// <summary> 
        /// Id 
        /// </summary> 
        public int Id {
            get;
            set;
        }
        /// <summary> 
        /// 配件发运单编号 
        /// </summary> 
        public string Code {
            get;
            set;
        }
        /// <summary> 
        /// 配件发运单类型 
        /// </summary> 
        public int Type {
            get;
            set;
        }
        /// <summary> 
        /// 营销分公司Id 
        /// </summary> 
        public int BranchId {
            get;
            set;
        }
        /// <summary> 
        /// 配件销售类型Id 
        /// </summary> 
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary> 
        /// 发货单位Id 
        /// </summary> 
        public int ShippingCompanyId {
            get;
            set;
        }
        /// <summary> 
        /// 发货单位编号 
        /// </summary> 
        public string ShippingCompanyCode {
            get;
            set;
        }
        /// <summary> 
        /// 发货单位名称 
        /// </summary> 
        public string ShippingCompanyName {
            get;
            set;
        }
        /// <summary> 
        /// 结算单位Id 
        /// </summary> 
        public int SettlementCompanyId {
            get;
            set;
        }
        /// <summary> 
        /// 结算单位编号 
        /// </summary> 
        public string SettlementCompanyCode {
            get;
            set;
        }
        /// <summary> 
        /// 结算单位名称 
        /// </summary> 
        public string SettlementCompanyName {
            get;
            set;
        }
        /// <summary> 
        /// 收货单位Id 
        /// </summary> 
        public int ReceivingCompanyId {
            get;
            set;
        }
        /// <summary> 
        /// 收货单位编号 
        /// </summary> 
        public string ReceivingCompanyCode {
            get;
            set;
        }
        /// <summary> 
        /// 收货单位销售类型Id 
        /// </summary> 
        public int InvoiceReceiveSaleCateId {
            get;
            set;
        }
        /// <summary> 
        /// 收货单位销售类型名称 
        /// </summary> 
        public string InvoiceReceiveSaleCateName {
            get;
            set;
        }
        /// <summary> 
        /// 收货单位名称 
        /// </summary> 
        public string ReceivingCompanyName {
            get;
            set;
        }
        /// <summary> 
        /// 出库仓库Id 
        /// </summary> 
        public int WarehouseId {
            get;
            set;
        }
        /// <summary> 
        /// 出库仓库编号 
        /// </summary> 
        public string WarehouseCode {
            get;
            set;
        }
        /// <summary> 
        /// 出库仓库名称 
        /// </summary> 
        public string WarehouseName {
            get;
            set;
        }
        /// <summary> 
        /// 收货仓库Id 
        /// </summary> 
        public int ReceivingWarehouseId {
            get;
            set;
        }
        /// <summary> 
        /// 收货仓库编号 
        /// </summary> 
        public string ReceivingWarehouseCode {
            get;
            set;
        }
        /// <summary> 
        /// 收货仓库名称 
        /// </summary> 
        public string ReceivingWarehouseName {
            get;
            set;
        }
        /// <summary> 
        /// 收货地址 
        /// </summary> 
        public string ReceivingAddress {
            get;
            set;
        }
        /// <summary> 
        /// 物流公司Id 
        /// </summary> 
        public int LogisticCompanyId {
            get;
            set;
        }
        /// <summary> 
        /// 物流公司编号 
        /// </summary> 
        public string LogisticCompanyCode {
            get;
            set;
        }
        /// <summary> 
        /// 物流公司名称 
        /// </summary> 
        public string LogisticCompanyName {
            get;
            set;
        }
        /// <summary> 
        /// 原始需求单据Id 
        /// </summary> 
        public string OriginalRequirementBillId {
            get;
            set;
        }
        /// <summary> 
        /// 原始需求单据编号 
        /// </summary> 
        public string OriginalRequirementBillCode {
            get;
            set;
        }
        /// <summary> 
        /// 原始需求单据类型 
        /// </summary> 
        public int OriginalRequirementBillType {
            get;
            set;
        }
        /// <summary> 
        /// 是否路损 
        /// </summary> 
        public bool IsTransportLosses {
            get;
            set;
        }
        /// <summary> 
        /// 路损处理状态 
        /// </summary> 
        public int TransportLossesDisposeStatus {
            get;
            set;
        }
        /// <summary> 
        /// 发运方式 
        /// </summary> 
        public int ShippingMethod {
            get;
            set;
        }
        /// <summary> 
        /// 承运商送达时间 
        /// </summary> 
        public DateTime LogisticArrivalDate {
            get;
            set;
        }
        /// <summary> 
        /// 要求到货日期 
        /// </summary> 
        public DateTime RequestedArrivalDate {
            get;
            set;
        }
        /// <summary> 
        /// 发运日期 
        /// </summary> 
        public DateTime ShippingDate {
            get;
            set;
        }
        /// <summary> 
        /// 重量(公斤) 
        /// </summary> 
        public float Weight {
            get;
            set;
        }
        /// <summary> 
        /// 体积(立方米) 
        /// </summary> 
        public float Volume {
            get;
            set;
        }
        /// <summary> 
        /// 计费方式 
        /// </summary> 
        public int BillingMethod {
            get;
            set;
        }
        /// <summary> 
        /// 运费费率 
        /// </summary> 
        public int TransportCostRate {
            get;
            set;
        }
        /// <summary> 
        /// 运费 
        /// </summary> 
        public decimal TransportCost {
            get;
            set;
        }
        /// <summary> 
        /// 运费结算状态 
        /// </summary> 
        public bool TransportCostSettleStatus {
            get;
            set;
        }
        /// <summary> 
        /// 保险费率 
        /// </summary> 
        public int InsuranceRate {
            get;
            set;
        }
        /// <summary> 
        /// 保险费 
        /// </summary> 
        public decimal InsuranceFee {
            get;
            set;
        }
        /// <summary> 
        /// 运输里程 
        /// </summary> 
        public int TransportMileage {
            get;
            set;
        }
        /// <summary> 
        /// 运输车牌号 
        /// </summary> 
        public string TransportVehiclePlate {
            get;
            set;
        }
        /// <summary> 
        /// 承运司机 
        /// </summary> 
        public string TransportDriver {
            get;
            set;
        }
        /// <summary> 
        /// 承运司机电话 
        /// </summary> 
        public string TransportDriverPhone {
            get;
            set;
        }
        /// <summary> 
        /// 送货单号 
        /// </summary> 
        public string DeliveryBillNumber {
            get;
            set;
        }
        /// <summary> 
        /// 装载单编号 
        /// </summary> 
        public string InterfaceRecordId {
            get;
            set;
        }
        /// <summary> 
        /// 状态 
        /// </summary> 
        public int Status {
            get;
            set;
        }
        /// <summary> 
        /// 备注 
        /// </summary> 
        public string Remark {
            get;
            set;
        }
        /// <summary> 
        /// 订单审批意见 
        /// </summary> 
        public string OrderApproveComment {
            get;
            set;
        }
        /// <summary> 
        /// 创建人Id 
        /// </summary> 
        public int CreatorId {
            get;
            set;
        }
        /// <summary> 
        /// 创建人 
        /// </summary> 
        public string CreatorName {
            get;
            set;
        }
        /// <summary> 
        /// 创建时间 
        /// </summary> 
        public DateTime CreateTime {
            get;
            set;
        }
        //<summary> 
        //修改人Id 
        //</summary> 
        public int ModifierId {
            get;
            set;
        }
        //<summary> 
        //修改人 
        //</summary> 
        public string ModifierName {
            get;
            set;
        }
        //<summary> 
        //修改时间 
        //</summary> 
        public DateTime ModifyTime {
            get;
            set;
        }
        /// <summary> 
        /// 收货确认人Id 
        /// </summary> 
        public int ConsigneeId {
            get;
            set;
        }
        /// <summary> 
        /// 收货确认人 
        /// </summary> 
        public string ConsigneeName {
            get;
            set;
        }
        /// <summary> 
        /// 收货确认时间 
        /// </summary> 
        public DateTime ConfirmedReceptionTime {
            get;
            set;
        }
        /// <summary> 
        /// 回执确认人Id 
        /// </summary> 
        public int ReceiptConfirmorId {
            get;
            set;
        }
        /// <summary> 
        /// 回执确认人 
        /// </summary> 
        public string ReceiptConfirmorName {
            get;
            set;
        }
        /// <summary> 
        /// 回执确认时间 
        /// </summary> 
        public DateTime ReceiptConfirmTime {
            get;
            set;
        }

        /// <summary> 
        /// GPMS采购计划单号 
        /// </summary> 
        public string GPMSPurOrderCode {
            get;
            set;
        }
        /// <summary> 
        /// 到货模式 
        /// </summary> 
        public int ArrivalMode {
            get;
            set;
        }
        /// <summary> 
        /// 发货日期 
        /// </summary> 
        public DateTime ShippingTime {
            get;
            set;
        }
        /// <summary> 
        /// 发货信息 
        /// </summary> 
        public string ShippingMsg {
            get;
            set;
        }
        /// <summary> 
        /// 到货站日期 
        /// </summary> 
        public DateTime ArrivalTime {
            get;
            set;
        }
        /// <summary> 
        /// 货站信息 
        /// </summary> 
        public string CargoTerminalMsg {
            get;
            set;
        }
        /// <summary> 
        /// 超期或电商运单流水反馈 
        /// </summary> 
        public string FlowFeedback {
            get;
            set;
        }
        /// <summary> 
        /// 快递公司 
        /// </summary> 
        public string ExpressCompany {
            get;
            set;
        }
        /// <summary> 
        /// 查询网址 
        /// </summary> 
        public string QueryURL {
            get;
            set;
        }
        //承运商
        public string Logistic {
            get;
            set;
        }


        public string StrShippingTime {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }

        public string StrArrivalMode {
            get;
            set;
        }

        public string StrArrivalTime {
            get;
            set;
        }

        public DateTime ApproveTime {
            get;
            set;
        }
    }
}
