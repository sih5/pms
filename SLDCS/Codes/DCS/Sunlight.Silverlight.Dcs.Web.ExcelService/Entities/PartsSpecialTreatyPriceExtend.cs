﻿
using System;
using System.ComponentModel;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsSpecialTreatyPriceExtend {

        public int Id {
            get;
            set;
        }
        public int Status {
            get;
            set;
        }
        public int CompanyId {
            get;
            set;
        }
        public int BranchId {
            get;
            set;
        }

        public DateTime? ValidationTime {
            get;
            set;
        }

        public DateTime? ExpireTime {
            get;
            set;
        }



        public int PartsSalesCategoryId {
            get;
            set;
        }
        public int SparePartId {
            get;
            set;
        }
        public string SparePartIdStr {
            get;
            set;
        }
        public string SparePartCode {
            get;
            set;
        }
        public string SparePartCodeStr {
            get;
            set;
        }

        public string SparePartName {
            get;
            set;
        }

        public string SparePartNameStr {
            get;
            set;
        }
        public decimal TreatyPrice {
            get;
            set;
        }

        public string TreatyPriceStr {
            get;
            set;
        }

        public string Remark {
            get;
            set;
        }
        public string RemarkStr {
            get;
            set;
        }

        public string PartsSalesCategoryCode {
            get;
            set;
        }
        public string PartsSalesCategoryCodeStr {
            get;
            set;
        }
        public string PartsSalesCategoryName {
            get;
            set;
        }

        public string PartsSalesCategoryNameStr {
            get;
            set;
        }

        public string CompanyCode {
            get;
            set;
        }

        public string CompanyCodeStr {
            get;
            set;
        }

        public string CompanyName {
            get;
            set;
        }

        public string CompanyNameStr {
            get;
            set;
        }
        public string ErrorMsg {
            get;
            set;
        }
    }
}
