﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsStock {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业Id
        /// </summary>
        public int StorageCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业类型
        /// </summary>
        public int StorageCompanyType {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 库位Id
        /// </summary>
        public int WarehouseAreaId {
            get;
            set;
        }

        /// <summary>
        /// 库区用途Id
        /// </summary>
        public int WarehouseAreaCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int PartId {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 库位code
        /// </summary>
        public string WarehouseAreaCode {
            get;
            set;
        }

        /// <summary>
        /// 锁定量
        /// </summary>
        public int LockedQuantity {
            get;
            set;
        }
    }
}
