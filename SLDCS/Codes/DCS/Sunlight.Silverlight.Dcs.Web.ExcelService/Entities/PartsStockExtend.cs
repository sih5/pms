﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsStockExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 仓库Id_Str
        /// </summary>
        public string WarehouseIdStr {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业Id
        /// </summary>
        public int StorageCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业Id_Str
        /// </summary>
        public string StorageCompanyIdStr {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业类型
        /// </summary>
        public int StorageCompanyType {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业类型_Str
        /// </summary>
        public string StorageCompanyTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 库位Id
        /// </summary>
        public int WarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 库位Id_Str
        /// </summary>
        public string WarehouseAreaIdStr {
            get;
            set;
        }

        /// <summary>
        /// 库区用途Id
        /// </summary>
        public int? WarehouseAreaCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 库区用途Id_Str
        /// </summary>
        public string WarehouseAreaCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int PartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string PartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }
        /// <summary>
        /// 数量_Str
        /// </summary>
        public string QuantityStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }
        /// <summary>
        /// 库区编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string PartCode
        {
            get;
            set;
        }
    }
}
