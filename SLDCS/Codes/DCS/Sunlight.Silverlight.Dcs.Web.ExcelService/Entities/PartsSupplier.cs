﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsSupplier {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 供应商编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Name {
            get;
            set;
        }

        /// <summary>
        /// 简称
        /// </summary>
        public string ShortName {
            get;
            set;
        }

        /// <summary>
        /// 供应商状态
        /// </summary>
        public int? SupplierType {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int? Status {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
    }
}
