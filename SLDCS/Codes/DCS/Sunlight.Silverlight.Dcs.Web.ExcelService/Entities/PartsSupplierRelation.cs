﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsSupplierRelation {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 销售类型名称
        /// </summary>
        public string PartsSalesCategoryName {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int PartId {
            get;
            set;
        }

        /// <summary>
        /// 供应商Id
        /// </summary>
        public int SupplierId {
            get;
            set;
        }

        /// <summary>
        /// 供应商图号
        /// </summary>
        public string SupplierPartCode {
            get;
            set;
        }

        /// <summary>
        /// 供应商配件名称
        /// </summary>
        public string SupplierPartName {
            get;
            set;
        }

        /// <summary>
        /// 是否首选供应商
        /// </summary>
        public int IsPrimary {
            get;
            set;
        }

        /// <summary>
        /// 采购占比
        /// </summary>
        public Decimal PurchasePercentage {
            get;
            set;
        }

        /// <summary>
        /// 经济采购批量
        /// </summary>
        public int EconomicalBatch {
            get;
            set;
        }

        /// <summary>
        /// 最小采购批量
        /// </summary>
        public int MinBatch {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime RowVersion {
            get;
            set;
        }
    }
}
