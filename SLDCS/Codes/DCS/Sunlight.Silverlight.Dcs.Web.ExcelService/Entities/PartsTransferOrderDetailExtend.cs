﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsTransferOrderDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }
        /// <summary>
        ///  源仓库可用库存
        /// </summary>
        public int UsableQuantity {
            get;
            set;
        }

        /// <summary>
        /// 配件调拨单Id
        /// </summary>
        public int PartsTransferOrderId {
            get;
            set;
        }
        /// <summary>
        /// 配件调拨单Id_Str
        /// </summary>
        public string PartsTransferOrderIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 计划量
        /// </summary>
        public int PlannedAmount {
            get;
            set;
        }
        /// <summary>
        /// 计划量_Str
        /// </summary>
        public string PlannedAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 确认量
        /// </summary>
        public int? ConfirmedAmount {
            get;
            set;
        }
        /// <summary>
        /// 确认量_Str
        /// </summary>
        public string ConfirmedAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 计划价格
        /// </summary>
        public Decimal PlanedPrice {
            get;
            set;
        }
        /// <summary>
        /// 计划价格_Str
        /// </summary>
        public string PlanedPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 价格
        /// </summary>
        public Decimal Price {
            get;
            set;
        }
        /// <summary>
        /// 价格_Str
        /// </summary>
        public string PriceStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// POCode
        /// </summary>
        public string POCode{
            get;
            set;
        }

        /// <summary>
        /// 品牌ID缓存
        /// </summary>
        public int? PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 最小销售数量
        /// </summary>
        public int? MInPackingAmount {
            get;
            set;
        }
    }
}
