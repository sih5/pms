﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PartsWarrantyStandardExtend {
        public string Code {
            get;
            set;
        }
        public string Name {
            get;
            set;
        }
        public int PartsWarrantyType {
            get;
            set;
        }
        public string PartsWarrantyTypeStr {
            get;
            set;
        }
        public int WarrantyPeriod {
            get;
            set;
        }
        public string WarrantyPeriodStr {
            get;
            set;
        }
        public int WarrantyMileage {
            get;
            set;
        }
        public string WarrantyMileageStr {
            get;
            set;
        }
        public int Capacity {
            get;
            set;
        }
        public string CapacityStr {
            get;
            set;
        }
        public int WorkingHours {
            get;
            set;
        }
        public string WorkingHoursStr {
            get;
            set;
        }
        public string Remark {
            get;
            set;
        }
        public int Status {
            get;
            set;
        }
        public string StatusStr {
            get;
            set;
        }
        public int CreatorId {
            get;
            set;
        }
        public string CreatorName {
            get;
            set;
        }
        public DateTime CreateTime {
            get;
            set;
        }
        public string ErrorMsg {
            get;
            set;
        }
    }
}
