﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PersonnelExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// 人员Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 人员Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 登陆Id
        /// </summary>
        public string LoginId {
            get;
            set;
        }
        /// <summary>
        /// 登陆Id_Str
        /// </summary>
        public string LoginIdStr {
            get;
            set;
        }

        /// <summary>
        /// 人员姓名
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 人员姓名_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 人员状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 人员状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 手机号
        /// </summary>
        public string CellNumber {
            get;
            set;
        }
        /// <summary>
        /// 手机号_Str
        /// </summary>
        public string CellNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 企业Id
        /// </summary>
        public int? CorporationId {
            get;
            set;
        }
        /// <summary>
        /// 企业Id_Str
        /// </summary>
        public string CorporationIdStr {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string CorporationName {
            get;
            set;
        }
        /// <summary>
        /// 企业名称_Str
        /// </summary>
        public string CorporationNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }



    }
}
