﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class PersonnelSupplierRelationExtend
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌ID
        /// </summary>
        public int PartsSalesCategoryId
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌ID_Str
        /// </summary>
        public string PartsSalesCategoryIdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌
        /// </summary>
        public string PartsSalesCategoryName
        {
            get;
            set;
        }
        /// <summary>
        /// 品牌_Str
        /// </summary>
        public string PartsSalesCategoryNameStr
        {
            get;
            set;
        }
        /// <summary>
        /// 人员ID
        /// </summary>
        public int PersonId
        {
            get;
            set;
        }
        /// <summary>
        /// 人员ID_Str
        /// </summary>
        public string PersonIdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 人员编号
        /// </summary>
        public string PersonCode
        {
            get;
            set;
        }
        /// <summary>
        /// 人员编号_Str
        /// </summary>
        public string PersonCodeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 人员名称
        /// </summary>
        public string PersonName
        {
            get;
            set;
        }
        /// <summary>
        /// 人员名称_Str
        /// </summary>
        public string PersonNameStr
        {
            get;
            set;
        }

        /// <summary>
        /// 供应商ID
        /// </summary>
        public int SupplierId
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商ID_Str
        /// </summary>
        public string SupplierIdStr
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商编号
        /// </summary>
        public string SupplierCode
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商编号_Str
        /// </summary>
        public string SupplierCodeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SupplierName
        {
            get;
            set;
        }
        /// <summary>
        /// 供应商名称_Str
        /// </summary>
        public string SupplierNameStr
        {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr
        {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId
        {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr
        {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }
    }
}
