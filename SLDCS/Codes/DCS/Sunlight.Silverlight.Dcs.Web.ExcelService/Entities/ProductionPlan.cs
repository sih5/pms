﻿namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ProductionPlan {
        /// <summary>
        /// 生产编号
        /// </summary>
        public string ProductCode {
            get;
            set;
        }

        /// <summary>
        /// 生产SON
        /// </summary>
        public string ProductionPlanSON {
            get;
            set;
        }
    }
}
