﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class RegionExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 上级区省市Id
        /// </summary>
        public int ParentId {
            get;
            set;
        }
        /// <summary>
        /// 上级区省市Id_Str
        /// </summary>
        public string ParentIdStr {
            get;
            set;
        }

        /// <summary>
        /// 区域类型
        /// </summary>
        public int Type {
            get;
            set;
        }
        /// <summary>
        /// 区域类型_Str
        /// </summary>
        public string TypeStr {
            get;
            set;
        }

        /// <summary>
        /// 编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 国家行政区号
        /// </summary>
        public string SARNumber {
            get;
            set;
        }
        /// <summary>
        /// 国家行政区号_Str
        /// </summary>
        public string SARNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 电话区号
        /// </summary>
        public string DDD {
            get;
            set;
        }
        /// <summary>
        /// 电话区号_Str
        /// </summary>
        public string DDDStr {
            get;
            set;
        }

        /// <summary>
        /// 邮政编码
        /// </summary>
        public string PostCode {
            get;
            set;
        }
        /// <summary>
        /// 邮政编码_Str
        /// </summary>
        public string PostCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 企业Id
        /// </summary>
        public int? EnterpriseId {
            get;
            set;
        }
        /// <summary>
        /// 企业Id_Str
        /// </summary>
        public string EnterpriseIdStr {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string CorporationName {
            get;
            set;
        }
        /// <summary>
        /// 企业名称_Str
        /// </summary>
        public string CorporationNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }

        public string ProvinceName {
            get;
            set;
        }

        public string CityName {
            get;
            set;
        }
        public string CountyName {
            get;
            set;
        }
    }
}
