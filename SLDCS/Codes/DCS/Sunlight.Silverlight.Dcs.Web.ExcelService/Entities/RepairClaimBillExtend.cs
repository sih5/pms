﻿using System;
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class RepairClaimBillExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        /// <summary>
        /// Id
        /// </summary>
        public Int32 Id {
            get;
            set;
        }

        /// <summary>
        /// IdStr
        /// </summary>
        public String IdStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔单编号
        /// </summary>
        public String ClaimBillCode {
            get;
            set;
        }

        /// <summary>
        /// 索赔单编号Str
        /// </summary>
        public String ClaimBillCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 维修单编号
        /// </summary>
        public String RepairContractCode {
            get;
            set;
        }

        /// <summary>
        /// 维修单编号Str
        /// </summary>
        public String RepairContractCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 维修单Id
        /// </summary>
        public Int32 RepairOrderId {
            get;
            set;
        }

        /// <summary>
        /// 维修单IdStr
        /// </summary>
        public String RepairOrderIdStr {
            get;
            set;
        }

        /// <summary>
        /// 维修工单Id
        /// </summary>
        public Int32 RepairWorkOrderId {
            get;
            set;
        }

        /// <summary>
        /// 维修工单IdStr
        /// </summary>
        public String RepairWorkOrderIdStr {
            get;
            set;
        }

        /// <summary>
        /// 维修工单
        /// </summary>
        public String RepairWorkOrder {
            get;
            set;
        }

        /// <summary>
        /// 维修工单Str
        /// </summary>
        public String RepairWorkOrderStr {
            get;
            set;
        }

        /// <summary>
        /// 维修单故障原因清单Id
        /// </summary>
        public Int32 RepairOrderFaultReasonId {
            get;
            set;
        }

        /// <summary>
        /// 维修单故障原因清单IdStr
        /// </summary>
        public String RepairOrderFaultReasonIdStr {
            get;
            set;
        }

        /// <summary>
        /// 售出状态
        /// </summary>
        public Int32 SalesStatus {
            get;
            set;
        }

        /// <summary>
        /// 售出状态Str
        /// </summary>
        public String SalesStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔类型
        /// </summary>
        public Int32 ClaimType {
            get;
            set;
        }

        /// <summary>
        /// 供应商Id
        /// </summary>
        public int SupplierId {
            get;
            set;
        }

        /// <summary>
        /// 供应商Id_Str
        /// </summary>
        public string SupplierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔类型Str
        /// </summary>
        public String ClaimTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public Int32 Status {
            get;
            set;
        }

        /// <summary>
        /// 状态Str
        /// </summary>
        public String StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 维修类型
        /// </summary>
        public Int32 RepairType {
            get;
            set;
        }

        /// <summary>
        /// 维修类型Str
        /// </summary>
        public String RepairTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 结算状态
        /// </summary>
        public Int32 SettlementStatus {
            get;
            set;
        }

        /// <summary>
        /// 结算状态Str
        /// </summary>
        public String SettlementStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 修改意见
        /// </summary>
        public String Amendments {
            get;
            set;
        }

        /// <summary>
        /// 修改意见Str
        /// </summary>
        public String AmendmentsStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回状态
        /// </summary>
        public Int32 RejectStatus {
            get;
            set;
        }

        /// <summary>
        /// 驳回状态Str
        /// </summary>
        public String RejectStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回次数
        /// </summary>
        public Int32 RejectQty {
            get;
            set;
        }

        /// <summary>
        /// 驳回次数Str
        /// </summary>
        public String RejectQtyStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回原因
        /// </summary>
        public String RejectReason {
            get;
            set;
        }

        /// <summary>
        /// 驳回原因Str
        /// </summary>
        public String RejectReasonStr {
            get;
            set;
        }

        /// <summary>
        /// 维修对象Id
        /// </summary>
        public Int32 RepairObjectId {
            get;
            set;
        }

        /// <summary>
        /// 维修对象IdStr
        /// </summary>
        public String RepairObjectIdStr {
            get;
            set;
        }

        /// <summary>
        /// 大区Id
        /// </summary>
        public Int32 SalesRegionId {
            get;
            set;
        }

        /// <summary>
        /// 大区IdStr
        /// </summary>
        public String SalesRegionIdStr {
            get;
            set;
        }

        /// <summary>
        /// 市场部Id
        /// </summary>
        public Int32 MarketingDepartmentId {
            get;
            set;
        }

        /// <summary>
        /// 市场部IdStr
        /// </summary>
        public String MarketingDepartmentIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public Int32 PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型IdStr
        /// </summary>
        public String PartsSalesCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 外出索赔申请单Id
        /// </summary>
        public Int32 ServiceTripClaimAppId {
            get;
            set;
        }

        /// <summary>
        /// 外出索赔申请单IdStr
        /// </summary>
        public String ServiceTripClaimAppIdStr {
            get;
            set;
        }

        /// <summary>
        /// 外出索赔申请单编号
        /// </summary>
        public String ServiceTripClaimAppCode {
            get;
            set;
        }

        /// <summary>
        /// 外出索赔申请单编号Str
        /// </summary>
        public String ServiceTripClaimAppCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 维修保养索赔申请单Id
        /// </summary>
        public Int32 RepairClaimApplicationId {
            get;
            set;
        }

        /// <summary>
        /// 维修保养索赔申请单IdStr
        /// </summary>
        public String RepairClaimApplicationIdStr {
            get;
            set;
        }

        /// <summary>
        /// 维修保养索赔申请单编号
        /// </summary>
        public String RepairClaimApplicationCode {
            get;
            set;
        }

        /// <summary>
        /// 维修保养索赔申请单编号Str
        /// </summary>
        public String RepairClaimApplicationCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 申请单类型
        /// </summary>
        public Int32 ApplicationType {
            get;
            set;
        }

        /// <summary>
        /// 申请单类型Str
        /// </summary>
        public String ApplicationTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务活动类型
        /// </summary>
        public Int32 ServiceActivityType {
            get;
            set;
        }

        /// <summary>
        /// 服务活动类型Str
        /// </summary>
        public String ServiceActivityTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务活动Id
        /// </summary>
        public Int32 ServiceActivityId {
            get;
            set;
        }

        /// <summary>
        /// 服务活动IdStr
        /// </summary>
        public String ServiceActivityIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务活动编号
        /// </summary>
        public String ServiceActivityCode {
            get;
            set;
        }

        /// <summary>
        /// 服务活动编号Str
        /// </summary>
        public String ServiceActivityCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务活动内容
        /// </summary>
        public String ServiceActivityContent {
            get;
            set;
        }

        /// <summary>
        /// 服务活动内容Str
        /// </summary>
        public String ServiceActivityContentStr {
            get;
            set;
        }

        /// <summary>
        /// 参加活动时间
        /// </summary>
        public DateTime ServiceActivityTime {
            get;
            set;
        }

        /// <summary>
        /// 参加活动时间Str
        /// </summary>
        public String ServiceActivityTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务活动索赔申请单Id
        /// </summary>
        public Int32 ServiceActivityAppId {
            get;
            set;
        }

        /// <summary>
        /// 服务活动索赔申请单IdStr
        /// </summary>
        public String ServiceActivityAppIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务活动索赔申请单编号
        /// </summary>
        public String ServiceActivityAppCode {
            get;
            set;
        }

        /// <summary>
        /// 服务活动索赔申请单编号Str
        /// </summary>
        public String ServiceActivityAppCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 单车保养条款Id
        /// </summary>
        public Int32 VehicleMaintenancePoilcyId {
            get;
            set;
        }

        /// <summary>
        /// 单车保养条款IdStr
        /// </summary>
        public String VehicleMaintenancePoilcyIdStr {
            get;
            set;
        }

        /// <summary>
        /// 单车保养条款名称
        /// </summary>
        public String VehicleMaintenancePoilcyName {
            get;
            set;
        }

        /// <summary>
        /// 单车保养条款名称Str
        /// </summary>
        public String VehicleMaintenancePoilcyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站Id
        /// </summary>
        public Int32 DealerId {
            get;
            set;
        }

        /// <summary>
        /// 服务站IdStr
        /// </summary>
        public String DealerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号
        /// </summary>
        public String DealerCode {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号Str
        /// </summary>
        public String DealerCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称
        /// </summary>
        public String DealerName {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称Str
        /// </summary>
        public String DealerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站Id
        /// </summary>
        public Int32 FirstClassStationId {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站IdStr
        /// </summary>
        public String FirstClassStationIdStr {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站编号
        /// </summary>
        public String FirstClassStationCode {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站编号Str
        /// </summary>
        public String FirstClassStationCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站名称
        /// </summary>
        public String FirstClassStationName {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站名称Str
        /// </summary>
        public String FirstClassStationNameStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public Int32 BranchId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司IdStr
        /// </summary>
        public String BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆联系人
        /// </summary>
        public String VehicleContactPerson {
            get;
            set;
        }

        /// <summary>
        /// 车辆联系人Str
        /// </summary>
        public String VehicleContactPersonStr {
            get;
            set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public String ContactPhone {
            get;
            set;
        }

        /// <summary>
        /// 联系电话Str
        /// </summary>
        public String ContactPhoneStr {
            get;
            set;
        }

        /// <summary>
        /// 联系地址
        /// </summary>
        public String ContactAddress {
            get;
            set;
        }

        /// <summary>
        /// 联系地址Str
        /// </summary>
        public String ContactAddressStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆Id
        /// </summary>
        public Int32 VehicleId {
            get;
            set;
        }

        /// <summary>
        /// 车辆IdStr
        /// </summary>
        public String VehicleIdStr {
            get;
            set;
        }

        /// <summary>
        /// VIN码
        /// </summary>
        public String VIN {
            get;
            set;
        }

        /// <summary>
        /// VIN码Str
        /// </summary>
        public String VINStr {
            get;
            set;
        }

        /// <summary>
        /// 销售日期
        /// </summary>
        public DateTime SalesDate {
            get;
            set;
        }

        /// <summary>
        /// 销售日期Str
        /// </summary>
        public String SalesDateStr {
            get;
            set;
        }

        /// <summary>
        /// 出厂日期
        /// </summary>
        public DateTime OutOfFactoryDate {
            get;
            set;
        }

        /// <summary>
        /// 出厂日期Str
        /// </summary>
        public String OutOfFactoryDateStr {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线Id
        /// </summary>
        public Int32 ServiceProductLineId {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线IdStr
        /// </summary>
        public String ServiceProductLineIdStr {
            get;
            set;
        }

        /// <summary>
        /// 产品线类型
        /// </summary>
        public Int32 ProductLineType {
            get;
            set;
        }

        /// <summary>
        /// 产品线类型Str
        /// </summary>
        public String ProductLineTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 车牌号
        /// </summary>
        public String VehicleLicensePlate {
            get;
            set;
        }

        /// <summary>
        /// 车牌号Str
        /// </summary>
        public String VehicleLicensePlateStr {
            get;
            set;
        }

        /// <summary>
        /// 行驶里程
        /// </summary>
        public Int32 Mileage {
            get;
            set;
        }

        /// <summary>
        /// 行驶里程Str
        /// </summary>
        public String MileageStr {
            get;
            set;
        }

        /// <summary>
        /// 工作小时
        /// </summary>
        public Int32 WorkingHours {
            get;
            set;
        }

        /// <summary>
        /// 工作小时Str
        /// </summary>
        public String WorkingHoursStr {
            get;
            set;
        }

        /// <summary>
        /// 方量
        /// </summary>
        public Int32 Capacity {
            get;
            set;
        }

        /// <summary>
        /// 方量Str
        /// </summary>
        public String CapacityStr {
            get;
            set;
        }

        /// <summary>
        /// 报修时间
        /// </summary>
        public DateTime RepairRequestTime {
            get;
            set;
        }

        /// <summary>
        /// 报修时间Str
        /// </summary>
        public String RepairRequestTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 完工时间
        /// </summary>
        public DateTime FinishingTime {
            get;
            set;
        }

        /// <summary>
        /// 完工时间Str
        /// </summary>
        public String FinishingTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 工时费
        /// </summary>
        public Decimal LaborCost {
            get;
            set;
        }

        /// <summary>
        /// 工时费Str
        /// </summary>
        public String LaborCostStr {
            get;
            set;
        }

        /// <summary>
        /// 材料费
        /// </summary>
        public Decimal MaterialCost {
            get;
            set;
        }

        /// <summary>
        /// 材料费Str
        /// </summary>
        public String MaterialCostStr {
            get;
            set;
        }

        /// <summary>
        /// 配件管理费
        /// </summary>
        public Decimal PartsManagementCost {
            get;
            set;
        }

        /// <summary>
        /// 配件管理费Str
        /// </summary>
        public String PartsManagementCostStr {
            get;
            set;
        }

        /// <summary>
        /// 其他费用
        /// </summary>
        public Decimal OtherCost {
            get;
            set;
        }

        /// <summary>
        /// 其他费用Str
        /// </summary>
        public String OtherCostStr {
            get;
            set;
        }

        /// <summary>
        /// 其他费用产生原因
        /// </summary>
        public String OtherCostReason {
            get;
            set;
        }

        /// <summary>
        /// 其他费用产生原因Str
        /// </summary>
        public String OtherCostReasonStr {
            get;
            set;
        }

        /// <summary>
        /// 费用合计
        /// </summary>
        public Decimal TotalAmount {
            get;
            set;
        }

        /// <summary>
        /// 费用合计Str
        /// </summary>
        public String TotalAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 故障原因
        /// </summary>
        public String MalfunctionReason {
            get;
            set;
        }

        /// <summary>
        /// 故障原因Str
        /// </summary>
        public String MalfunctionReasonStr {
            get;
            set;
        }

        /// <summary>
        /// 发动机型号
        /// </summary>
        public String EngineModel {
            get;
            set;
        }

        /// <summary>
        /// 发动机型号Str
        /// </summary>
        public String EngineModelStr {
            get;
            set;
        }

        /// <summary>
        /// 发动机型号Id
        /// </summary>
        public Int32 EngineModelId {
            get;
            set;
        }

        /// <summary>
        /// 发动机型号IdStr
        /// </summary>
        public String EngineModelIdStr {
            get;
            set;
        }

        /// <summary>
        /// 发动机序列号
        /// </summary>
        public String EngineSerialNumber {
            get;
            set;
        }

        /// <summary>
        /// 发动机序列号Str
        /// </summary>
        public String EngineSerialNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 变速箱型号
        /// </summary>
        public String GearModel {
            get;
            set;
        }

        /// <summary>
        /// 变速箱型号Str
        /// </summary>
        public String GearModelStr {
            get;
            set;
        }

        /// <summary>
        /// 变速箱序列号
        /// </summary>
        public String GearSerialNumber {
            get;
            set;
        }

        /// <summary>
        /// 变速箱序列号Str
        /// </summary>
        public String GearSerialNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 故障现象Id
        /// </summary>
        public Int32 MalfunctionId {
            get;
            set;
        }

        /// <summary>
        /// 故障现象IdStr
        /// </summary>
        public String MalfunctionIdStr {
            get;
            set;
        }

        /// <summary>
        /// 故障现象编号
        /// </summary>
        public String MalfunctionCode {
            get;
            set;
        }

        /// <summary>
        /// 故障现象编号Str
        /// </summary>
        public String MalfunctionCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 故障现象描述
        /// </summary>
        public String MalfunctionDescription {
            get;
            set;
        }

        /// <summary>
        /// 故障现象描述Str
        /// </summary>
        public String MalfunctionDescriptionStr {
            get;
            set;
        }

        /// <summary>
        /// 维修模板Id
        /// </summary>
        public Int32 RepairTempletId {
            get;
            set;
        }

        /// <summary>
        /// 维修模板IdStr
        /// </summary>
        public String RepairTempletIdStr {
            get;
            set;
        }

        /// <summary>
        /// 维修模板编号
        /// </summary>
        public String RepairTempletCode {
            get;
            set;
        }

        /// <summary>
        /// 维修模板编号Str
        /// </summary>
        public String RepairTempletCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件Id
        /// </summary>
        public Int32 FaultyPartsId {
            get;
            set;
        }

        /// <summary>
        /// 祸首件IdStr
        /// </summary>
        public String FaultyPartsIdStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件编号
        /// </summary>
        public String FaultyPartsCode {
            get;
            set;
        }

        /// <summary>
        /// 祸首件编号Str
        /// </summary>
        public String FaultyPartsCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件名称
        /// </summary>
        public String FaultyPartsName {
            get;
            set;
        }

        /// <summary>
        /// 祸首件名称Str
        /// </summary>
        public String FaultyPartsNameStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商Id
        /// </summary>
        public Int32 FaultyPartsSupplierId {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商IdStr
        /// </summary>
        public String FaultyPartsSupplierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件保修分类Id
        /// </summary>
        public Int32 FaultyPartsWCId {
            get;
            set;
        }

        /// <summary>
        /// 祸首件保修分类IdStr
        /// </summary>
        public String FaultyPartsWCIdStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商编号
        /// </summary>
        public String FaultyPartsSupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商编号Str
        /// </summary>
        public String FaultyPartsSupplierCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商名称
        /// </summary>
        public String FaultyPartsSupplierName {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商名称Str
        /// </summary>
        public String FaultyPartsSupplierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商Id
        /// </summary>
        public Int32 ClaimSupplierId {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商IdStr
        /// </summary>
        public String ClaimSupplierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商编号
        /// </summary>
        public String ClaimSupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商编号Str
        /// </summary>
        public String ClaimSupplierCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商名称
        /// </summary>
        public String ClaimSupplierName {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商名称Str
        /// </summary>
        public String ClaimSupplierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 是否向责任供应商索赔
        /// </summary>
        public Boolean IfClaimToSupplier {
            get;
            set;
        }

        /// <summary>
        /// 是否向责任供应商索赔Str
        /// </summary>
        public String IfClaimToSupplierStr {
            get;
            set;
        }

        /// <summary>
        /// 责任单位Id
        /// </summary>
        public Int32 ResponsibleUnitId {
            get;
            set;
        }

        /// <summary>
        /// 责任单位IdStr
        /// </summary>
        public String ResponsibleUnitIdStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认人Id
        /// </summary>
        public Int32 SupplierConfirmorId {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认人IdStr
        /// </summary>
        public String SupplierConfirmorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认人
        /// </summary>
        public String SupplierConfirmorName {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认人Str
        /// </summary>
        public String SupplierConfirmorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认时间
        /// </summary>
        public DateTime SupplierConfirmTime {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认时间Str
        /// </summary>
        public String SupplierConfirmTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认状态
        /// </summary>
        public Int32 SupplierConfirmStatus {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认状态Str
        /// </summary>
        public String SupplierConfirmStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认意见
        /// </summary>
        public String SupplierConfirmComment {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认意见Str
        /// </summary>
        public String SupplierConfirmCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核人Id
        /// </summary>
        public Int32 SupplierCheckerId {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核人IdStr
        /// </summary>
        public String SupplierCheckerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核人
        /// </summary>
        public String SupplierCheckerName {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核人Str
        /// </summary>
        public String SupplierCheckerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核时间
        /// </summary>
        public DateTime SupplierCheckTime {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核时间Str
        /// </summary>
        public String SupplierCheckTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核状态
        /// </summary>
        public Int32 SupplierCheckStatus {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核状态Str
        /// </summary>
        public String SupplierCheckStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核意见
        /// </summary>
        public String SupplierCheckComment {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核意见Str
        /// </summary>
        public String SupplierCheckCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 终审意见
        /// </summary>
        public String ServiceDepartmentComment {
            get;
            set;
        }

        /// <summary>
        /// 终审意见Str
        /// </summary>
        public String ServiceDepartmentCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 初审意见
        /// </summary>
        public String InitialApprovertComment {
            get;
            set;
        }

        /// <summary>
        /// 初审意见Str
        /// </summary>
        public String InitialApprovertCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 会签意见
        /// </summary>
        public String FinalApproverComment {
            get;
            set;
        }

        /// <summary>
        /// 会签意见Str
        /// </summary>
        public String FinalApproverCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 自动审核意见
        /// </summary>
        public String AutoApproveComment {
            get;
            set;
        }

        /// <summary>
        /// 自动审核意见Str
        /// </summary>
        public String AutoApproveCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 自动审核状态
        /// </summary>
        public Int32 AutoApproveStatus {
            get;
            set;
        }

        /// <summary>
        /// 自动审核状态Str
        /// </summary>
        public String AutoApproveStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 审批意见历史
        /// </summary>
        public String ApproveCommentHistory {
            get;
            set;
        }

        /// <summary>
        /// 审批意见历史Str
        /// </summary>
        public String ApproveCommentHistoryStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件处理状态
        /// </summary>
        public Int32 UsedPartsDisposalStatus {
            get;
            set;
        }

        /// <summary>
        /// 旧件处理状态Str
        /// </summary>
        public String UsedPartsDisposalStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public String Remark {
            get;
            set;
        }

        /// <summary>
        /// 备注Str
        /// </summary>
        public String RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 临时供应商Id
        /// </summary>
        public Int32 TempSupplierId {
            get;
            set;
        }

        /// <summary>
        /// 临时供应商IdStr
        /// </summary>
        public String TempSupplierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 临时供应商编号
        /// </summary>
        public String TempSupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 临时供应商编号Str
        /// </summary>
        public String TempSupplierCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 临时供应商名称
        /// </summary>
        public String TempSupplierName {
            get;
            set;
        }

        /// <summary>
        /// 临时供应商名称Str
        /// </summary>
        public String TempSupplierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public Int32 CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人IdStr
        /// </summary>
        public String CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public String CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建人Str
        /// </summary>
        public String CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 创建时间Str
        /// </summary>
        public String CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public Int32 ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人IdStr
        /// </summary>
        public String ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public String ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改人Str
        /// </summary>
        public String ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 修改时间Str
        /// </summary>
        public String ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public Int32 AbandonerId {
            get;
            set;
        }

        /// <summary>
        /// 作废人IdStr
        /// </summary>
        public String AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public String AbandonerName {
            get;
            set;
        }

        /// <summary>
        /// 作废人Str
        /// </summary>
        public String AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime AbandonTime {
            get;
            set;
        }

        /// <summary>
        /// 作废时间Str
        /// </summary>
        public String AbandonTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 初审企业Id
        /// </summary>
        public Int32 InitialApproverCompanyId {
            get;
            set;
        }

        /// <summary>
        /// 初审企业IdStr
        /// </summary>
        public String InitialApproverCompanyIdStr {
            get;
            set;
        }

        /// <summary>
        /// 初审人Id
        /// </summary>
        public Int32 InitialApproverId {
            get;
            set;
        }

        /// <summary>
        /// 初审人IdStr
        /// </summary>
        public String InitialApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 初审人
        /// </summary>
        public String InitialApproverName {
            get;
            set;
        }

        /// <summary>
        /// 初审人Str
        /// </summary>
        public String InitialApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 初审时间
        /// </summary>
        public DateTime InitialApproverTime {
            get;
            set;
        }

        /// <summary>
        /// 初审时间Str
        /// </summary>
        public String InitialApproverTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 会签人Id
        /// </summary>
        public Int32 FinalApproverId {
            get;
            set;
        }

        /// <summary>
        /// 会签人IdStr
        /// </summary>
        public String FinalApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 会签人
        /// </summary>
        public String FinalApproverName {
            get;
            set;
        }

        /// <summary>
        /// 会签人Str
        /// </summary>
        public String FinalApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 会签时间
        /// </summary>
        public DateTime FinalApproverTime {
            get;
            set;
        }

        /// <summary>
        /// 会签时间Str
        /// </summary>
        public String FinalApproverTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 终审人Id
        /// </summary>
        public Int32 ApproverId {
            get;
            set;
        }

        /// <summary>
        /// 终审人IdStr
        /// </summary>
        public String ApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 终审人
        /// </summary>
        public String ApproverName {
            get;
            set;
        }

        /// <summary>
        /// 终审人Str
        /// </summary>
        public String ApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 终审时间
        /// </summary>
        public DateTime ApproveTime {
            get;
            set;
        }

        /// <summary>
        /// 终审时间Str
        /// </summary>
        public String ApproveTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回人Id
        /// </summary>
        public Int32 RejectId {
            get;
            set;
        }

        /// <summary>
        /// 驳回人IdStr
        /// </summary>
        public String RejectIdStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回人
        /// </summary>
        public String RejectName {
            get;
            set;
        }

        /// <summary>
        /// 驳回人Str
        /// </summary>
        public String RejectNameStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回时间
        /// </summary>
        public DateTime RejectTime {
            get;
            set;
        }

        /// <summary>
        /// 驳回时间Str
        /// </summary>
        public String RejectTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 引起其它零部件损坏
        /// </summary>
        public Boolean CauseOtherMalfunction {
            get;
            set;
        }

        /// <summary>
        /// 引起其它零部件损坏Str
        /// </summary>
        public String CauseOtherMalfunctionStr {
            get;
            set;
        }

        /// <summary>
        /// 辅料费用
        /// </summary>
        public Decimal TrimCost {
            get;
            set;
        }

        /// <summary>
        /// 辅料费用Str
        /// </summary>
        public String TrimCostStr {
            get;
            set;
        }

        /// <summary>
        /// 路径
        /// </summary>
        public String Path {
            get;
            set;
        }

        /// <summary>
        /// 路径Str
        /// </summary>
        public String PathStr {
            get;
            set;
        }
    }
}
