﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class RepairItemAffiServProdLineExtend {
        /// <summary>
        /// 分公司名称_Str
        /// </summary>
        public string BranchNameStr {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线编号_Str
        /// </summary>
        public string ServiceProductLineCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线名称_Str
        /// </summary>
        public string ServiceProductLineNameStr {
            get;
            set;
        }

        /// <summary>
        /// 维修项目编号_Str
        /// </summary>
        public string RepairItemCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 维修项目名称_Str
        /// </summary>
        public string RepairItemNameStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 维修项目Id
        /// </summary>
        public int RepairItemId {
            get;
            set;
        }
        /// <summary>
        /// 维修项目Id_Str
        /// </summary>
        public string RepairItemIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线Id
        /// </summary>
        public int ServiceProductLineId {
            get;
            set;
        }
        /// <summary>
        /// 服务产品线Id_Str
        /// </summary>
        public string ServiceProductLineIdStr {
            get;
            set;
        }

        /// <summary>
        /// 产品线类型
        /// </summary>
        public int ProductLineType {
            get;
            set;
        }
        /// <summary>
        /// 产品线类型_Str
        /// </summary>
        public string ProductLineTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 工时定额
        /// </summary>
        public Decimal DefaultLaborHour {
            get;
            set;
        }
        /// <summary>
        /// 工时定额_Str
        /// </summary>
        public string DefaultLaborHourStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id_Str
        /// </summary>
        public string AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废人_Str
        /// </summary>
        public string AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// 作废时间_Str
        /// </summary>
        public string AbandonTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }

        /// <summary>
        /// PartsSalesCategoryId
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }

        public int? EngineRepairPower {
            get;
            set;
        }

        public string EngineRepairPowerStr {
            get;
            set;
        }

    }
}
