﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class RepairItemExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 维修项目编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 维修项目编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 维修项目名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 维修项目名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 维修项目分类Id
        /// </summary>
        public int RepairItemCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 维修项目分类Id_Str
        /// </summary>
        public string RepairItemCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 速查码
        /// </summary>
        public string QuickCode {
            get;
            set;
        }
        /// <summary>
        /// 速查码_Str
        /// </summary>
        public string QuickCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 工种类别
        /// </summary>
        public int JobType {
            get;
            set;
        }
        /// <summary>
        /// 工种类别_Str
        /// </summary>
        public string JobTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 维修资质等级
        /// </summary>
        public int RepairQualificationGrade {
            get;
            set;
        }
        /// <summary>
        /// 维修资质等级_Str
        /// </summary>
        public string RepairQualificationGradeStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }
    }
}
