﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ResRelationshipExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        public string ResTypeStr {
            set;
            get;
        }
        public string ResType {
            set;
            get;
        }
        public string ResTemStr {
            set;
            get;
        }
        public int? ResTem {
            set;
            get;
        }
    }
}
