﻿

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class ReserveFactorOrderDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        public int? ReserveFactorMasterOrderId {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int? Id {
            get;
            set;
        }
        /// <summary>
        /// 价格上限
        /// </summary>
        public string PriceCapStr {
            get;
            set;
        }
        /// <summary>
        /// 价格上限
        /// </summary>
        public decimal? PriceCap {
            get;
            set;
        }
        /// <summary>
        /// 价格下限(服务站价)
        /// </summary>
        public string PriceFloorStr {
            get;
            set;
        }
        /// <summary>
        /// 价格下限(服务站价)
        /// </summary>
        public decimal? PriceFloor {
            get;
            set;
        }
        /// <summary>
        /// 上限系数
        /// </summary>
        public string UpperLimitCoefficientStr {
            get;
            set;
        }
        /// <summary>
        /// 上限系数
        /// </summary>
        public decimal? UpperLimitCoefficient {
            get;
            set;
        }
        /// <summary>
        /// 下限系数
        /// </summary>
        public string LowerLimitCoefficientStr {
            get;
            set;
        }
        /// <summary>
        /// 下限系数
        /// </summary>
        public decimal? LowerLimitCoefficient {
            get;
            set;
        }
    }
}
