﻿

using System;
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class RetainedCustomerExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 客户编号
        /// </summary>
        public string CustomerCode {
            get;
            set;
        }

        /// <summary>
        /// 客户姓名
        /// </summary>
        public string CustomerName {
            get;
            set;
        }
      
        /// <summary>
        /// 手机号码
        /// </summary>
        public string CellPhoneNumber {
            get;
            set;
        }

        /// <summary>
        /// 家庭电话
        /// </summary>
        public string HomePhoneNumber {
            get;
            set;
        }
        /// <summary>
        /// 单位电话
        /// </summary>
        public string OfficePhoneNumber {
            get;
            set;
        }
        /// <summary>
        /// 固定电话
        /// </summary>
        public string PhoneNumber {
            get;
            set;
        }
        /// <summary>
        /// 证件号码
        /// </summary>
        public string IdDocumentNumber {
            get;
            set;
        }
        /// <summary>
        /// 电子邮箱
        /// </summary>
        public string Email {
            get;
            set;
        }
        /// <summary>
        /// 邮政编码
        /// </summary>
        public string PostCode {
            get;
            set;
        }
        /// <summary>
        /// 区域
        /// </summary>
        public string RegionName {
            get;
            set;
        }
        /// <summary>
        /// 省
        /// </summary>
        public string ProvinceName {
            get;
            set;
        }
        /// <summary>
        /// 市
        /// </summary>
        public string CityName {
            get;
            set;
        }
        /// <summary>
        /// 区县
        /// </summary>
        public string CountyName {
            get;
            set;
        }
      
        /// <summary>
        /// 单位名称
        /// </summary>
        public string CompanyName {
            get;
            set;
        }/// <summary>
        /// 详细地址
        /// </summary>
        public string Address {
            get;
            set;
        }
        /// <summary>
        /// 职务
        /// </summary>
        public int? JobPosition {
            get;
            set;
        }
        /// <summary>
        /// 职务Str
        /// </summary>
        public string JobPositionStr {
            get;
            set;
        }
        /// <summary>
        /// 客户性别
        /// </summary>
        public int? Gender {
            get;
            set;
        }
        /// <summary>
        /// 客户性别Str
        /// </summary>
        public string GenderStr {
            get;
            set;
        }

        /// <summary>
        /// 证件类型
        /// </summary>
        public int? IdDocumentType {
            get;
            set;
        }
        /// <summary>
        /// 证件类型Str
        /// </summary>
        public string IdDocumentTypeStr{
            get;
            set;
        }
        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime? Birthdate {
            get;
            set;
        }

        /// <summary>
        /// 出生日期Str
        /// </summary>
        public string BirthdateStr {
            get;
            set;
        }
        /// <summary>
        /// 年龄
        /// </summary>
        public int? Age {
            get;
            set;
        }

        /// <summary>
        /// 年龄Str
        /// </summary>
        public string AgeStr {
            get;
            set;
        }
        /// <summary>
        /// 驾龄
        /// </summary>
        public int? DrivingSeniority {
            get;
            set;
        }

        /// <summary>
        /// 驾龄Str
        /// </summary>
        public string DrivingSeniorityStr {
            get;
            set;
        }
        /// <summary>
        /// 地址类型
        /// </summary>
        public int? AddressType {
            get;
            set;
        }

        /// <summary>
        /// 地址类型Str
        /// </summary>
        public string AddressTypeStr {
            get;
            set;
        }
        /// <summary>
        /// 行业类型
        /// </summary>
        public int? OccupationType {
            get;
            set;
        }

        /// <summary>
        /// 行业类型Str
        /// </summary>
        public string OccupationTypeStr {
            get;
            set;
        }
        /// <summary>
        /// 文化程度
        /// </summary>
        public int? EducationLevel {
            get;
            set;
        }

        /// <summary>
        /// 文化程度Str
        /// </summary>
        public string EducationLevelStr {
            get;
            set;
        }
        /// <summary>
        /// 月收入
        /// </summary>
        public int? Income {
            get;
            set;
        }

        /// <summary>
        /// 月收入Str
        /// </summary>
        public string IncomeStr {
            get;
            set;
        }
        /// <summary>
        /// 婚否
        /// </summary>
        public bool? IfMarried {
            get;
            set;
        }

        /// <summary>
        /// 婚否Str
        /// </summary>
        public string IfMarriedStr {
            get;
            set;
        }
        /// <summary>
        /// 结婚纪念日
        /// </summary>
        public DateTime? WeddingDay {
            get;
            set;
        }

        /// <summary>
        /// 结婚纪念日Str
        /// </summary>
        public string WeddingDayStr {
            get;
            set;
        }
        /// <summary>
        /// 家庭月收入
        /// </summary>
        public int? AnnualIncome {
            get;
            set;
        }

        /// <summary>
        /// 家庭月收入Str
        /// </summary>
        public string AnnualIncomeStr {
            get;
            set;
        }

        /// <summary>
        /// 地理位置
        /// </summary>
        public int? Location {
            get;
            set;
        }

        /// <summary>
        /// 地理位置Str
        /// </summary>
        public string LocationStr {
            get;
            set;
        }
        /// <summary>
        ///家庭成员构成
        /// </summary>
        public int? FamilyComposition {
            get;
            set;
        }
        /// <summary>
        ///家庭成员构成
        /// </summary>
        public string FamilyCompositionStr {
            get;
            set;
        }

        /// <summary>
        /// 是否需要回访
        /// </summary>
        public bool? IfVisitBackNeeded {
            get;
            set;
        }

        /// <summary>
        /// 是否需要回访Str
        /// </summary>
        public string IfVisitBackNeededStr {
            get;
            set;
        }
        /// <summary>
        /// 是否接受短信
        /// </summary>
        public bool? IfTextAccepted {
            get;
            set;
        }

        /// <summary>
        /// 是否接受短信Str
        /// </summary>
        public string IfTextAcceptedStr {
            get;
            set;
        }
        /// <summary>
        /// 是否接受资料
        /// </summary>
        public bool? IfAdAccepted {
            get;
            set;
        }

        /// <summary>
        /// 是否接受资料Str
        /// </summary>
        public string IfAdAcceptedStr {
            get;
            set;
        }

        /// <summary>
        /// 是否为会员
        /// </summary>
        public bool? IfVIP {
            get;
            set;
        }

        /// <summary>
        /// 是否为会员Str
        /// </summary>
        public string IfVIPStr {
            get;
            set;
        }

        /// <summary>
        /// VIP类型
        /// </summary>
        public int? MemberType {
            get;
            set;
        }

        /// <summary>
        /// VIP类型Str
        /// </summary>
        public string MemberTypeStr {
            get;
            set;
        }
        /// <summary>
        /// VIP卡号
        /// </summary>
        public string CardNumber {
            get;
            set;
        }

        /// <summary>
        ///使用者性别
        /// </summary>
        public int? UserGender {
            get;
            set;
        }

        /// <summary>
        /// 使用者性别Str
        /// </summary>
        public string UserGenderStr {
            get;
            set;
        }
        /// <summary>
        ///客户类型
        /// </summary>
        public int? CustomerType {
            get;
            set;
        }

        /// <summary>
        /// 客户类型Str
        /// </summary>
        public string CustomerTypeStr {
            get;
            set;
        }
        /// <summary>
        /// 特殊纪念日
        /// </summary>
        public DateTime? SpecialDay {
            get;
            set;
        }
        /// <summary>
        /// 特殊纪念日Str
        /// </summary>
        public string SpecialDayStr {
            get;
            set;
        }
        /// <summary>
        /// 特殊纪念内容
        /// </summary>
        public string SpecialDayDetails {
            get;
            set;
        }
        /// <summary>
        /// 爱好
        /// </summary>
        public string Hobby {
            get;
            set;
        }
        /// <summary>
        /// 推荐人
        /// </summary>
        public string Referral {
            get;
            set;
        }
        /// <summary>
        /// 上牌人
        /// </summary>
        public string PlateOwner {
            get;
            set;
        }
        /// <summary>
        /// 行业类型
        /// </summary>
        public int? BusinessType {
            get;
            set;
        }

        /// <summary>
        /// 行业类型Str
        /// </summary>
        public string BusinessTypeStr {
            get;
            set;
        }

    }
}
