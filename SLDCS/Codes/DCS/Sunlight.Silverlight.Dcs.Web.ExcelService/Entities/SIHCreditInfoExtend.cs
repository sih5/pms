﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class SIHCreditInfoExtend {
        public int Id {
            get;
            set;
        }

        public string DealerCode {
            get;
            set;
        }
        public string DealerName {
            get;
            set;
        }
        public string ThisSIHCreditStr {
            get;
            set;
        }

        public decimal? ThisSIHCredit {
            get;
            set;
        }
        public string ValidationTimeStr {
            get;
            set;
        }

        public string ExpireTimeStr {
            get;
            set;
        }

        public DateTime? ValidationTime {
            get;
            set;
        }

        public DateTime? ExpireTime {
            get;
            set;
        }

        public int MarketingDepartmentId {
            get;
            set;
        }

        public string MarketingDepartmentCode {
            get;
            set;
        }
        public string MarketingDepartmentName {
            get;
            set;
        }
        public int CompanyId {
            get;
            set;
        }

        public string CompanyCode {
            get;
            set;
        }
        public string CompanyName {
            get;
            set;
        }
        public int AccountGroupId {
            get;
            set;
        }

        public string AccountGroupCode {
            get;
            set;
        }
        public string AccountGroupName {
            get;
            set;
        }

        public decimal? BSIHCredit {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }

        public int DealerId {
            get;
            set;
        }
    }
}
