﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class SIHRecommendPlanExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }
        public int? WarehouseId {
            get;
            set;
        }
        public string WarehouseName {
            get;
            set;
        }
        public string WarehouseNameStr {
            get;
            set;
        }
        public string WarehouseCode {
            get;
            set;
        }
        public string WarehouseCodeStr {
            get;
            set;
        }
        public int? SparePartId {
            get;
            set;
        }
        public string SparePartCode {
            get;
            set;
        }
        public string SparePartCodeStr {
            get;
            set;
        }
        public string SparePartName {
            get;
            set;
        }
        public string SparePartNameStr {
            get;
            set;
        }
        public string CarryTimeStr {
            get;
            set;
        }
        public DateTime? CarryTime {
            get;
            set;
        }
        public string ActQtyStr {
            get;
            set;
        }
        public int? ActQty {
            get;
            set;
        }
        public decimal? SalesPrice {
            get;
            set;
        }
    }
}
