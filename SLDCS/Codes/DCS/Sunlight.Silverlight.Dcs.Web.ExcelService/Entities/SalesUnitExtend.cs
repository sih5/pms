﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
   public  class SalesUnitExtend {
       public int Id {
           get;
           set;
       }
       public string Code {
           get;
           set;
       }
       public string Name {
           get;
           set;
       }
       public int OwnerCompanyId {
           get;
           set;
       }
       public string SalesUnitOwnerCompanyCode {
           get;
           set;
       }
       public string SalesUnitOwnerCompanyName {
           get;
           set;
       }
    }
}
