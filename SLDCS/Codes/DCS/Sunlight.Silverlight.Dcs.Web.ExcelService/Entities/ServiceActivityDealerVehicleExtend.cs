﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ServiceActivityDealerVehicleExtend {

        /// <summary>
        /// 经销商名称_Str 
        /// </summary>
        public string DealerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商编号_Str 
        /// </summary>
        public string DealerCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务活动Id
        /// </summary>
        public int ServiceActivityId {
            get;
            set;
        }
        /// <summary>
        /// 服务活动Id_Str
        /// </summary>
        public string ServiceActivityIdStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆Id
        /// </summary>
        public int VehicleId {
            get;
            set;
        }
        /// <summary>
        /// 车辆Id_Str
        /// </summary>
        public string VehicleIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站Id
        /// </summary>
        public int DealerId {
            get;
            set;
        }
        /// <summary>
        /// 服务站Id_Str
        /// </summary>
        public string DealerIdStr {
            get;
            set;
        }

        /// <summary>
        /// VIN
        /// </summary>
        public string VIN {
            get;
            set;
        }
        /// <summary>
        /// VIN_Str
        /// </summary>
        public string VINStr {
            get;
            set;
        }

        /// <summary>
        /// 已经使用
        /// </summary>
        public bool AlreadyUsed {
            get;
            set;
        }
        /// <summary>
        /// 已经使用_Str
        /// </summary>
        public string AlreadyUsedStr {
            get;
            set;
        }
    }
}
