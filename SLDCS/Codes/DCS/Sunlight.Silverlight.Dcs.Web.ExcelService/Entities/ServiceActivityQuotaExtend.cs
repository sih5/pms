﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ServiceActivityQuotaExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务活动Id
        /// </summary>
        public int ServiceActivityId {
            get;
            set;
        }
        /// <summary>
        /// 服务活动Id_Str
        /// </summary>
        public string ServiceActivityIdStr {
            get;
            set;
        }

        /// <summary>
        /// 经销商Id
        /// </summary>
        public int DealerId {
            get;
            set;
        }
        /// <summary>
        /// 经销商Id_Str
        /// </summary>
        public string DealerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 服务站编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 服务站名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 配额
        /// </summary>
        public int Quota {
            get;
            set;
        }
        /// <summary>
        /// 配额_Str
        /// </summary>
        public string QuotaStr {
            get;
            set;
        }

        /// <summary>
        /// 已使用配额
        /// </summary>
        public int QuotaUsed {
            get;
            set;
        }
        /// <summary>
        /// 已使用配额_Str
        /// </summary>
        public string QuotaUsedStr {
            get;
            set;
        }


    }
}
