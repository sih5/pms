﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ServiceProductLineViewExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }


        public int? Id {
            get;
            set;
        }

        public string Name {
            get;
            set;
        }

        /// <summary>
        /// 产品线类型
        /// </summary>
        public int ProductLineType {
            get;
            set;
        }
        /// <summary>
        /// 产品线类型_Str
        /// </summary>
        public string ProductLineTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线Id
        /// </summary>
        public int ServiceProductLineId {
            get;
            set;
        }
        /// <summary>
        /// 服务产品线Id_Str
        /// </summary>
        public string ServiceProductLineIdStr {
            get;
            set;
        }

        /// <summary>
        /// 产品线编号
        /// </summary>
        public string ProductLineCode {
            get;
            set;
        }
        /// <summary>
        /// 产品线编号_Str
        /// </summary>
        public string ProductLineCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 产品线名称
        /// </summary>
        public string ProductLineName {
            get;
            set;
        }
        /// <summary>
        /// 产品线名称_Str
        /// </summary>
        public string ProductLineNameStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public int PartsSalesCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 配件销售类型Id_Str
        /// </summary>
        public string PartsSalesCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }



    }
}
