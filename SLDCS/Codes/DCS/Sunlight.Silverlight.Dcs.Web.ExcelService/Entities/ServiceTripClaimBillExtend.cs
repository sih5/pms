﻿using System;
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class ServiceTripClaimBillExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }

        /// <summary>
        /// Id
        /// </summary>
        public Int32 Id {
            get;
            set;
        }

        /// <summary>
        /// IdStr
        /// </summary>
        public String IdStr {
            get;
            set;
        }

        /// <summary>
        /// 外出索赔申请单Id
        /// </summary>
        public Int32 ServiceTripClaimAppId {
            get;
            set;
        }

        /// <summary>
        /// 外出索赔申请单IdStr
        /// </summary>
        public String ServiceTripClaimAppIdStr {
            get;
            set;
        }

        /// <summary>
        /// 外出索赔申请单编号
        /// </summary>
        public String ServiceTripClaimAppCode {
            get;
            set;
        }

        /// <summary>
        /// 外出索赔申请单编号Str
        /// </summary>
        public String ServiceTripClaimAppCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 外出索赔单编号
        /// </summary>
        public String Code {
            get;
            set;
        }

        /// <summary>
        /// 外出索赔单编号Str
        /// </summary>
        public String CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public Int32 Status {
            get;
            set;
        }

        /// <summary>
        /// 状态Str
        /// </summary>
        public String StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 结算状态
        /// </summary>
        public Int32 SettlementStatus {
            get;
            set;
        }

        /// <summary>
        /// 结算状态Str
        /// </summary>
        public String SettlementStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 售出状态
        /// </summary>
        public Int32 SalesStatus {
            get;
            set;
        }

        /// <summary>
        /// 售出状态Str
        /// </summary>
        public String SalesStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 外出类型
        /// </summary>
        public Int32 ServiceTripType {
            get;
            set;
        }

        /// <summary>
        /// 外出类型Str
        /// </summary>
        public String ServiceTripTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站Id
        /// </summary>
        public Int32 DealerId {
            get;
            set;
        }

        /// <summary>
        /// 服务站IdStr
        /// </summary>
        public String DealerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号
        /// </summary>
        public String DealerCode {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号Str
        /// </summary>
        public String DealerCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称
        /// </summary>
        public String DealerName {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称Str
        /// </summary>
        public String DealerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 市场部Id
        /// </summary>
        public Int32 MarketingDepartmentId {
            get;
            set;
        }

        /// <summary>
        /// 市场部IdStr
        /// </summary>
        public String MarketingDepartmentIdStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆Id
        /// </summary>
        public Int32 VehicleId {
            get;
            set;
        }

        /// <summary>
        /// 车辆IdStr
        /// </summary>
        public String VehicleIdStr {
            get;
            set;
        }

        /// <summary>
        /// VIN码
        /// </summary>
        public String VIN {
            get;
            set;
        }

        /// <summary>
        /// VIN码Str
        /// </summary>
        public String VINStr {
            get;
            set;
        }

        /// <summary>
        /// 车牌号
        /// </summary>
        public String VehicleLicensePlate {
            get;
            set;
        }

        /// <summary>
        /// 车牌号Str
        /// </summary>
        public String VehicleLicensePlateStr {
            get;
            set;
        }

        /// <summary>
        /// 报修时间
        /// </summary>
        public DateTime RepairRequestTime {
            get;
            set;
        }

        /// <summary>
        /// 报修时间Str
        /// </summary>
        public String RepairRequestTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 销售日期
        /// </summary>
        public DateTime SalesDate {
            get;
            set;
        }

        /// <summary>
        /// 销售日期Str
        /// </summary>
        public String SalesDateStr {
            get;
            set;
        }

        /// <summary>
        /// 行使里程
        /// </summary>
        public Int32 Mileage {
            get;
            set;
        }

        /// <summary>
        /// 行使里程Str
        /// </summary>
        public String MileageStr {
            get;
            set;
        }

        /// <summary>
        /// 出厂日期
        /// </summary>
        public DateTime OutOfFactoryDate {
            get;
            set;
        }

        /// <summary>
        /// 出厂日期Str
        /// </summary>
        public String OutOfFactoryDateStr {
            get;
            set;
        }

        /// <summary>
        /// 方量
        /// </summary>
        public Int32 Capacity {
            get;
            set;
        }

        /// <summary>
        /// 方量Str
        /// </summary>
        public String CapacityStr {
            get;
            set;
        }

        /// <summary>
        /// 工作小时
        /// </summary>
        public Int32 WorkingHours {
            get;
            set;
        }

        /// <summary>
        /// 工作小时Str
        /// </summary>
        public String WorkingHoursStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public Int32 BranchId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司IdStr
        /// </summary>
        public String BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线Id
        /// </summary>
        public Int32 ServiceProductLineId {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线IdStr
        /// </summary>
        public String ServiceProductLineIdStr {
            get;
            set;
        }

        /// <summary>
        /// 产品线类型
        /// </summary>
        public Int32 ProductLineType {
            get;
            set;
        }

        /// <summary>
        /// 产品线类型Str
        /// </summary>
        public String ProductLineTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆联系人
        /// </summary>
        public String VehicleContactPerson {
            get;
            set;
        }

        /// <summary>
        /// 车辆联系人Str
        /// </summary>
        public String VehicleContactPersonStr {
            get;
            set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public String ContactPhone {
            get;
            set;
        }

        /// <summary>
        /// 联系电话Str
        /// </summary>
        public String ContactPhoneStr {
            get;
            set;
        }

        /// <summary>
        /// 联系地址
        /// </summary>
        public String ContactAddress {
            get;
            set;
        }

        /// <summary>
        /// 联系地址Str
        /// </summary>
        public String ContactAddressStr {
            get;
            set;
        }

        /// <summary>
        /// 外出时间
        /// </summary>
        public DateTime ServiceTripTime {
            get;
            set;
        }

        /// <summary>
        /// 外出时间Str
        /// </summary>
        public String ServiceTripTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 完工时间
        /// </summary>
        public DateTime FinishingTime {
            get;
            set;
        }

        /// <summary>
        /// 完工时间Str
        /// </summary>
        public String FinishingTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 外出原因
        /// </summary>
        public String ServiceTripReason {
            get;
            set;
        }

        /// <summary>
        /// 外出原因Str
        /// </summary>
        public String ServiceTripReasonStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆补助单价
        /// </summary>
        public Decimal OutServiceCarUnitPrice {
            get;
            set;
        }

        /// <summary>
        /// 车辆补助单价Str
        /// </summary>
        public String OutServiceCarUnitPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 人员补助单价
        /// </summary>
        public Decimal OutSubsidyPrice {
            get;
            set;
        }

        /// <summary>
        /// 人员补助单价Str
        /// </summary>
        public String OutSubsidyPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回次数
        /// </summary>
        public Int32 RejectQty {
            get;
            set;
        }

        /// <summary>
        /// 驳回次数Str
        /// </summary>
        public String RejectQtyStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回原因
        /// </summary>
        public String RejectReason {
            get;
            set;
        }

        /// <summary>
        /// 驳回原因Str
        /// </summary>
        public String RejectReasonStr {
            get;
            set;
        }

        /// <summary>
        /// 省
        /// </summary>
        public String ProvinceName {
            get;
            set;
        }

        /// <summary>
        /// 省Str
        /// </summary>
        public String ProvinceNameStr {
            get;
            set;
        }

        /// <summary>
        /// 市
        /// </summary>
        public String CityName {
            get;
            set;
        }

        /// <summary>
        /// 市Str
        /// </summary>
        public String CityNameStr {
            get;
            set;
        }

        /// <summary>
        /// 县
        /// </summary>
        public String CountyName {
            get;
            set;
        }

        /// <summary>
        /// 县Str
        /// </summary>
        public String CountyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 详细地址
        /// </summary>
        public String DetailedAddress {
            get;
            set;
        }

        /// <summary>
        /// 详细地址Str
        /// </summary>
        public String DetailedAddressStr {
            get;
            set;
        }

        /// <summary>
        /// 交通工具
        /// </summary>
        public String TrafficWay {
            get;
            set;
        }

        /// <summary>
        /// 交通工具Str
        /// </summary>
        public String TrafficWayStr {
            get;
            set;
        }

        /// <summary>
        /// 大区Id
        /// </summary>
        public Int32 AreaId {
            get;
            set;
        }

        /// <summary>
        /// 大区IdStr
        /// </summary>
        public String AreaIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型Id
        /// </summary>
        public Int32 PartsSalesCategoryId {
            get;
            set;
        }

        /// <summary>
        /// 配件销售类型IdStr
        /// </summary>
        public String PartsSalesCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 维修对象Id
        /// </summary>
        public Int32 RepairObjectId {
            get;
            set;
        }

        /// <summary>
        /// 维修对象IdStr
        /// </summary>
        public String RepairObjectIdStr {
            get;
            set;
        }

        /// <summary>
        /// 外出车辆车牌号
        /// </summary>
        public String OutVehicleLicensePlate {
            get;
            set;
        }

        /// <summary>
        /// 外出车辆车牌号Str
        /// </summary>
        public String OutVehicleLicensePlateStr {
            get;
            set;
        }

        /// <summary>
        /// 维修索赔单Id
        /// </summary>
        public Int32 RepairClaimBillId {
            get;
            set;
        }

        /// <summary>
        /// 维修索赔单IdStr
        /// </summary>
        public String RepairClaimBillIdStr {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站Id
        /// </summary>
        public Int32 OneLevelDealerId {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站IdStr
        /// </summary>
        public String OneLevelDealerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站编号
        /// </summary>
        public String OneLevelDealerCode {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站编号Str
        /// </summary>
        public String OneLevelDealerCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站名称
        /// </summary>
        public String OneLevelDealerName {
            get;
            set;
        }

        /// <summary>
        /// 二级服务站名称Str
        /// </summary>
        public String OneLevelDealerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 维修单Id
        /// </summary>
        public Int32 RepairOrderId {
            get;
            set;
        }

        /// <summary>
        /// 维修单IdStr
        /// </summary>
        public String RepairOrderIdStr {
            get;
            set;
        }

        /// <summary>
        /// 外出区域
        /// </summary>
        public String ServiceTripRegion {
            get;
            set;
        }

        /// <summary>
        /// 外出区域Str
        /// </summary>
        public String ServiceTripRegionStr {
            get;
            set;
        }

        /// <summary>
        /// 提报里程
        /// </summary>
        public Int32 ServiceTripDistance {
            get;
            set;
        }

        /// <summary>
        /// 提报里程Str
        /// </summary>
        public String ServiceTripDistanceStr {
            get;
            set;
        }

        /// <summary>
        /// 结算里程
        /// </summary>
        public Int32 SettleDistance {
            get;
            set;
        }

        /// <summary>
        /// 结算里程Str
        /// </summary>
        public String SettleDistanceStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站外出人数
        /// </summary>
        public Int32 DealerTripPerson {
            get;
            set;
        }

        /// <summary>
        /// 服务站外出人数Str
        /// </summary>
        public String DealerTripPersonStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站外出天数
        /// </summary>
        public Int32 DealerTripDuration {
            get;
            set;
        }

        /// <summary>
        /// 服务站外出天数Str
        /// </summary>
        public String DealerTripDurationStr {
            get;
            set;
        }

        /// <summary>
        /// 外出人数
        /// </summary>
        public Int32 ServiceTripPerson {
            get;
            set;
        }

        /// <summary>
        /// 外出人数Str
        /// </summary>
        public String ServiceTripPersonStr {
            get;
            set;
        }

        /// <summary>
        /// 外出天数
        /// </summary>
        public Int32 ServiceTripDuration {
            get;
            set;
        }

        /// <summary>
        /// 外出天数Str
        /// </summary>
        public String ServiceTripDurationStr {
            get;
            set;
        }

        /// <summary>
        /// 是否使用自备车
        /// </summary>
        public Boolean IfUseOwnVehicle {
            get;
            set;
        }

        /// <summary>
        /// 是否使用自备车Str
        /// </summary>
        public String IfUseOwnVehicleStr {
            get;
            set;
        }

        /// <summary>
        /// 是否使用拖车
        /// </summary>
        public Boolean IfUseTowTruck {
            get;
            set;
        }

        /// <summary>
        /// 是否使用拖车Str
        /// </summary>
        public String IfUseTowTruckStr {
            get;
            set;
        }

        /// <summary>
        /// 拖车费用
        /// </summary>
        public Decimal TowCharge {
            get;
            set;
        }

        /// <summary>
        /// 拖车费用Str
        /// </summary>
        public String TowChargeStr {
            get;
            set;
        }

        /// <summary>
        /// 外出服务费用
        /// </summary>
        public Decimal FieldServiceCharge {
            get;
            set;
        }

        /// <summary>
        /// 外出服务费用Str
        /// </summary>
        public String FieldServiceChargeStr {
            get;
            set;
        }

        /// <summary>
        /// 其他费用
        /// </summary>
        public Decimal OtherCost {
            get;
            set;
        }

        /// <summary>
        /// 其他费用Str
        /// </summary>
        public String OtherCostStr {
            get;
            set;
        }

        /// <summary>
        /// 其他费用产生原因
        /// </summary>
        public String OtherCostReason {
            get;
            set;
        }

        /// <summary>
        /// 其他费用产生原因Str
        /// </summary>
        public String OtherCostReasonStr {
            get;
            set;
        }

        /// <summary>
        /// 费用合计
        /// </summary>
        public Decimal TotalAmount {
            get;
            set;
        }

        /// <summary>
        /// 费用合计Str
        /// </summary>
        public String TotalAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 是否向责任供应商索赔
        /// </summary>
        public Boolean IfClaimToSupplier {
            get;
            set;
        }

        /// <summary>
        /// 是否向责任供应商索赔Str
        /// </summary>
        public String IfClaimToSupplierStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商Id
        /// </summary>
        public Int32 ClaimSupplierId {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商IdStr
        /// </summary>
        public String ClaimSupplierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商编号
        /// </summary>
        public String ClaimSupplierCode {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商编号Str
        /// </summary>
        public String ClaimSupplierCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商名称
        /// </summary>
        public String ClaimSupplierName {
            get;
            set;
        }

        /// <summary>
        /// 索赔供应商名称Str
        /// </summary>
        public String ClaimSupplierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 责任单位Id
        /// </summary>
        public Int32 ResponsibleUnitId {
            get;
            set;
        }

        /// <summary>
        /// 责任单位IdStr
        /// </summary>
        public String ResponsibleUnitIdStr {
            get;
            set;
        }

        /// <summary>
        /// 审批意见
        /// </summary>
        public String ApprovalComment {
            get;
            set;
        }

        /// <summary>
        /// 审批意见Str
        /// </summary>
        public String ApprovalCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 会签意见
        /// </summary>
        public String FinalApproveComment {
            get;
            set;
        }

        /// <summary>
        /// 会签意见Str
        /// </summary>
        public String FinalApproveCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 审批意见历史
        /// </summary>
        public String ApproveCommentHistory {
            get;
            set;
        }

        /// <summary>
        /// 审批意见历史Str
        /// </summary>
        public String ApproveCommentHistoryStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public String Remark {
            get;
            set;
        }

        /// <summary>
        /// 备注Str
        /// </summary>
        public String RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核人Id
        /// </summary>
        public Int32 SupplierCheckerId {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核人IdStr
        /// </summary>
        public String SupplierCheckerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核人
        /// </summary>
        public String SupplierCheckerName {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核人Str
        /// </summary>
        public String SupplierCheckerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核时间
        /// </summary>
        public DateTime SupplierCheckTime {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核时间Str
        /// </summary>
        public String SupplierCheckTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核意见
        /// </summary>
        public String SupplierCheckComment {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核意见Str
        /// </summary>
        public String SupplierCheckCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核状态
        /// </summary>
        public Int32 SupplierCheckStatus {
            get;
            set;
        }

        /// <summary>
        /// 供应商复核状态Str
        /// </summary>
        public String SupplierCheckStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商审批人Id
        /// </summary>
        public Int32 SupplierApproverId {
            get;
            set;
        }

        /// <summary>
        /// 供应商审批人IdStr
        /// </summary>
        public String SupplierApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商审批人
        /// </summary>
        public String SupplierApproverName {
            get;
            set;
        }

        /// <summary>
        /// 供应商审批人Str
        /// </summary>
        public String SupplierApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商审批时间
        /// </summary>
        public DateTime SupplierApproveTime {
            get;
            set;
        }

        /// <summary>
        /// 供应商审批时间Str
        /// </summary>
        public String SupplierApproveTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商审批意见
        /// </summary>
        public String SupplierApproveComment {
            get;
            set;
        }

        /// <summary>
        /// 供应商审批意见Str
        /// </summary>
        public String SupplierApproveCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认状态
        /// </summary>
        public Int32 SupplierApproveStatus {
            get;
            set;
        }

        /// <summary>
        /// 供应商确认状态Str
        /// </summary>
        public String SupplierApproveStatusStr {
            get;
            set;
        }

        /// <summary>
        /// 初审人Id
        /// </summary>
        public Int32 InitialApproverId {
            get;
            set;
        }

        /// <summary>
        /// 初审人IdStr
        /// </summary>
        public String InitialApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 初审人
        /// </summary>
        public String InitialApproverName {
            get;
            set;
        }

        /// <summary>
        /// 初审人Str
        /// </summary>
        public String InitialApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 初审时间
        /// </summary>
        public DateTime InitialApproveTime {
            get;
            set;
        }

        /// <summary>
        /// 初审时间Str
        /// </summary>
        public String InitialApproveTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 初审意见
        /// </summary>
        public String InitialApproveComment {
            get;
            set;
        }

        /// <summary>
        /// 初审意见Str
        /// </summary>
        public String InitialApproveCommentStr {
            get;
            set;
        }

        /// <summary>
        /// 会签人Id
        /// </summary>
        public Int32 FinalApproverId {
            get;
            set;
        }

        /// <summary>
        /// 会签人IdStr
        /// </summary>
        public String FinalApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 会签人
        /// </summary>
        public String FinalApproverName {
            get;
            set;
        }

        /// <summary>
        /// 会签人Str
        /// </summary>
        public String FinalApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 会签时间
        /// </summary>
        public DateTime FinalApproverTime {
            get;
            set;
        }

        /// <summary>
        /// 会签时间Str
        /// </summary>
        public String FinalApproverTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public Int32 CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人IdStr
        /// </summary>
        public String CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public String CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建人Str
        /// </summary>
        public String CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 创建时间Str
        /// </summary>
        public String CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public Int32 ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人IdStr
        /// </summary>
        public String ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public String ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改人Str
        /// </summary>
        public String ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 修改时间Str
        /// </summary>
        public String ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public Int32 AbandonerId {
            get;
            set;
        }

        /// <summary>
        /// 作废人IdStr
        /// </summary>
        public String AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public String AbandonerName {
            get;
            set;
        }

        /// <summary>
        /// 作废人Str
        /// </summary>
        public String AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime AbandonTime {
            get;
            set;
        }

        /// <summary>
        /// 作废时间Str
        /// </summary>
        public String AbandonTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 审批人Id
        /// </summary>
        public Int32 ApproverId {
            get;
            set;
        }

        /// <summary>
        /// 审批人IdStr
        /// </summary>
        public String ApproverIdStr {
            get;
            set;
        }

        /// <summary>
        /// 审批人
        /// </summary>
        public String ApproverName {
            get;
            set;
        }

        /// <summary>
        /// 审批人Str
        /// </summary>
        public String ApproverNameStr {
            get;
            set;
        }

        /// <summary>
        /// 审批时间
        /// </summary>
        public DateTime ApproveTime {
            get;
            set;
        }

        /// <summary>
        /// 审批时间Str
        /// </summary>
        public String ApproveTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回人Id
        /// </summary>
        public Int32 RejectId {
            get;
            set;
        }

        /// <summary>
        /// 驳回人IdStr
        /// </summary>
        public String RejectIdStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回人
        /// </summary>
        public String RejectName {
            get;
            set;
        }

        /// <summary>
        /// 驳回人Str
        /// </summary>
        public String RejectNameStr {
            get;
            set;
        }

        /// <summary>
        /// 驳回时间
        /// </summary>
        public DateTime RejectTime {
            get;
            set;
        }

        /// <summary>
        /// 驳回时间Str
        /// </summary>
        public String RejectTimeStr {
            get;
            set;
        }
    }
}
