﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class SmartOrderCalendarExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }
        public int? WarehouseId {
            get;
            set;
        }
        public string WarehouseCodeStr {
            get;
            set;
        }
        public string WarehouseCode {
            get;
            set;
        }
        public string WarehouseNameStr {
            get;
            set;
        }
        public string WarehouseName {
            get;
            set;
        }
        public DateTime? Times {
            get;
            set;
        }
        public string TimesStr {
            get;
            set;
        }
    }
}
