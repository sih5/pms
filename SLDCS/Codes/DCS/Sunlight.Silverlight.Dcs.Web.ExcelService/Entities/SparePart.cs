﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class SparePart {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string Name {
            get;
            set;
        }

        /// <summary>
        /// 上一替代件
        /// </summary>
        public string LastSubstitute {
            get;
            set;
        }

        /// <summary>
        /// 下一替代件
        /// </summary>
        public string NextSubstitute {
            get;
            set;
        }

        /// <summary>
        /// 保质期
        /// </summary>
        public int ShelfLife {
            get;
            set;
        }

        /// <summary>
        /// 英文名称
        /// </summary>
        public string EnglishName {
            get;
            set;
        }

        /// <summary>
        /// 拼音代码
        /// </summary>
        public string PinyinCode {
            get;
            set;
        }

        /// <summary>
        /// 配件参考编号
        /// </summary>
        public string ReferenceCode {
            get;
            set;
        }

        /// <summary>
        /// 配件参考名称
        /// </summary>
        public string ReferenceName {
            get;
            set;
        }

        /// <summary>
        /// 设计图号
        /// </summary>
        public string CADCode {
            get;
            set;
        }

        /// <summary>
        /// 设计名称
        /// </summary>
        public string CADName {
            get;
            set;
        }

        /// <summary>
        /// 配件类型
        /// </summary>
        public int? PartType {
            get;
            set;
        }

        /// <summary>
        /// 规格型号
        /// </summary>
        public string Specification {
            get;
            set;
        }

        /// <summary>
        /// 配件特征说明
        /// </summary>
        public string Feature {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int? Status {
            get;
            set;
        }

        /// <summary>
        /// 长（cm）
        /// </summary>
        public Decimal Length {
            get;
            set;
        }

        /// <summary>
        /// 宽(cm)
        /// </summary>
        public Decimal Width {
            get;
            set;
        }

        /// <summary>
        /// 高(cm)
        /// </summary>
        public Decimal Height {
            get;
            set;
        }

        /// <summary>
        /// 体积(cm*3)
        /// </summary>
        public Decimal Volume {
            get;
            set;
        }

        /// <summary>
        /// 重量(kg)
        /// </summary>
        public Decimal Weight {
            get;
            set;
        }

        /// <summary>
        /// 材料
        /// </summary>
        public string Material {
            get;
            set;
        }

        /// <summary>
        /// 包装数量
        /// </summary>
        public int PackingAmount {
            get;
            set;
        }

        /// <summary>
        /// 包装规格
        /// </summary>
        public string PackingSpecification {
            get;
            set;
        }

        /// <summary>
        /// 商品外包装编号
        /// </summary>
        public string PartsOutPackingCode {
            get;
            set;
        }

        /// <summary>
        /// 商品内包装编号
        /// </summary>
        public string PartsInPackingCode {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int CreatorId {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int ModifierId {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime ModifyTime {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int AbandonerId {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime AbandonTime {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime RowVersion {
            get;
            set;
        }

        /// <summary>
        /// 总成型号
        /// </summary>
        public string TotalNumber {
            get;
            set;
        }

        /// <summary>
        /// 配件标准名称
        /// </summary>
        public string SubstandardName {
            get;
            set;
        }
        /// <summary>
        /// 厂商
        /// </summary>
        public string Factury {
            get;
            set;
        }
        /// <summary>
        /// 海外配件图号
        /// </summary>
        public string OverseasPartsFigure {
            get;
            set;
        }

        /// <summary>
        /// 申报要素
        /// </summary>
        public string DeclareElement
        {
            get;
            set;
        }

        /// <summary>
        /// 是否可采购
        /// </summary>
        public bool? IsOrderable
        {
            get;
            set;
        }
    }
}
