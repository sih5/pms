﻿

using System;
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class SparePartExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber {
            get;
            set;
        }

        public string IsNotWarrantyTransferStr {
            get;
            set;
        }
        /// <summary>
        /// 是否保外调拨
        /// </summary>
        public int IsNotWarrantyTransfer {
            get;
            set;
        }

        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string Code {
            get;
            set;
        }

        /// <summary>
        /// 零部件图号
        /// </summary>
        public string RreferenceCode {
            get;
            set;
        }

        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 产品商标
        /// </summary>
        public string ProductBrand {
            get;
            set;
        }

        /// <summary>
        /// 产品商标_Str
        /// </summary>
        public string ProductBrandStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 上一替代件
        /// </summary>
        public string LastSubstitute {
            get;
            set;
        }
        /// <summary>
        /// 上一替代件_Str
        /// </summary>
        public string LastSubstituteStr {
            get;
            set;
        }

        /// <summary>
        /// 下一替代件
        /// </summary>
        public string NextSubstitute {
            get;
            set;
        }
        /// <summary>
        /// 下一替代件_Str
        /// </summary>
        public string NextSubstituteStr {
            get;
            set;
        }

        /// <summary>
        /// 保质期
        /// </summary>
        public int? ShelfLife {
            get;
            set;
        }
        /// <summary>
        /// 保质期_Str
        /// </summary>
        public string ShelfLifeStr {
            get;
            set;
        }

        /// <summary>
        /// 集团ABC类别
        /// </summary>
        public int? GroupABCCategory {
            get;
            set;
        }
        /// <summary>
        /// 集团ABC类别_Str
        /// </summary>
        public string GroupABCCategoryStr {
            get;
            set;
        }

        /// <summary>
        /// 英文名称
        /// </summary>
        public string EnglishName {
            get;
            set;
        }
        /// <summary>
        /// 英文名称_Str
        /// </summary>
        public string EnglishNameStr {
            get;
            set;
        }

        /// <summary>
        /// 拼音代码
        /// </summary>
        public string PinyinCode {
            get;
            set;
        }
        /// <summary>
        /// 拼音代码_Str
        /// </summary>
        public string PinyinCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件参考编号
        /// </summary>
        public string ReferenceCode {
            get;
            set;
        }
        /// <summary>
        /// 配件参考编号_Str
        /// </summary>
        public string ReferenceCodeStr {
            get;
            set;
        }

        /// <summary>
        /// IMS压缩号
        /// </summary>
        public string IMSCompressionNumber {
            get;
            set;
        }
        /// <summary>
        /// IMS压缩号_Str
        /// </summary>
        public string IMSCompressionNumberStr {
            get;
            set;
        }

        /// <summary>
        /// IMS厂商号
        /// </summary>
        public string IMSManufacturerNumber {
            get;
            set;
        }
        /// <summary>
        /// IMS厂商号_Str
        /// </summary>
        public string IMSManufacturerNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 配件参考名称
        /// </summary>
        public string ReferenceName {
            get;
            set;
        }
        /// <summary>
        /// 配件参考名称_Str
        /// </summary>
        public string ReferenceNameStr {
            get;
            set;
        }

        /// <summary>
        /// 设计图号
        /// </summary>
        public string CADCode {
            get;
            set;
        }
        /// <summary>
        /// 设计图号_Str
        /// </summary>
        public string CADCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 设计名称
        /// </summary>
        public string CADName {
            get;
            set;
        }
        /// <summary>
        /// 设计名称_Str
        /// </summary>
        public string CADNameStr {
            get;
            set;
        }

        /// <summary>
        /// 配件类型
        /// </summary>
        public int PartType {
            get;
            set;
        }
        /// <summary>
        /// 配件类型_Str
        /// </summary>
        public string PartTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 规格型号
        /// </summary>
        public string Specification {
            get;
            set;
        }
        /// <summary>
        /// 规格型号_Str
        /// </summary>
        public string SpecificationStr {
            get;
            set;
        }

        /// <summary>
        /// 配件特征说明
        /// </summary>
        public string Feature {
            get;
            set;
        }
        /// <summary>
        /// 配件特征说明_Str
        /// </summary>
        public string FeatureStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 长（cm）
        /// </summary>
        public Decimal? Length {
            get;
            set;
        }
        /// <summary>
        /// 长（cm）_Str
        /// </summary>
        public string LengthStr {
            get;
            set;
        }

        /// <summary>
        /// 宽(cm)
        /// </summary>
        public Decimal? Width {
            get;
            set;
        }
        /// <summary>
        /// 宽(cm)_Str
        /// </summary>
        public string WidthStr {
            get;
            set;
        }

        /// <summary>
        /// 高(cm)
        /// </summary>
        public Decimal? Height {
            get;
            set;
        }
        /// <summary>
        /// 高(cm)_Str
        /// </summary>
        public string HeightStr {
            get;
            set;
        }

        /// <summary>
        /// 体积(cm*3)
        /// </summary>
        public Decimal? Volume {
            get;
            set;
        }
        /// <summary>
        /// 体积(cm*3)_Str
        /// </summary>
        public string VolumeStr {
            get;
            set;
        }

        /// <summary>
        /// 重量(kg)
        /// </summary>
        public Decimal? Weight {
            get;
            set;
        }
        /// <summary>
        /// 重量(kg)_Str
        /// </summary>
        public string WeightStr {
            get;
            set;
        }

        /// <summary>
        /// 材料
        /// </summary>
        public string Material {
            get;
            set;
        }
        /// <summary>
        /// 材料_Str
        /// </summary>
        public string MaterialStr {
            get;
            set;
        }

        /// <summary>
        /// 申报要素
        /// </summary>
        public string DeclareElement
        {
            get;
            set;
        }
        /// <summary>
        /// 申报要素_Str
        /// </summary>
        public string DeclareElementStr
        {
            get;
            set;
        }

        /// <summary>
        /// 包装数量
        /// </summary>
        public int? PackingAmount {
            get;
            set;
        }
        /// <summary>
        /// 包装数量_Str
        /// </summary>
        public string PackingAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 包装规格
        /// </summary>
        public string PackingSpecification {
            get;
            set;
        }
        /// <summary>
        /// 包装规格_Str
        /// </summary>
        public string PackingSpecificationStr {
            get;
            set;
        }

        /// <summary>
        /// 商品外包装编号
        /// </summary>
        public string PartsOutPackingCode {
            get;
            set;
        }
        /// <summary>
        /// 商品外包装编号_Str
        /// </summary>
        public string PartsOutPackingCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 商品内包装编号
        /// </summary>
        public string PartsInPackingCode {
            get;
            set;
        }
        /// <summary>
        /// 商品内包装编号_Str
        /// </summary>
        public string PartsInPackingCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 计量单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }
        /// <summary>
        /// 计量单位_Str
        /// </summary>
        public string MeasureUnitStr {
            get;
            set;
        }

        /// <summary>
        /// 最小包装数量
        /// </summary>
        public int MInPackingAmount {
            get;
            set;
        }
        /// <summary>
        /// 最小包装数量_Str
        /// </summary>
        public string MInPackingAmountStr {
            get;
            set;
        }


        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id_Str
        /// </summary>
        public string AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废人_Str
        /// </summary>
        public string AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// 作废时间_Str
        /// </summary>
        public string AbandonTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }

        /// <summary>
        /// 总成型号
        /// </summary>
        public string TotalNumber {
            get;
            set;
        }
        /// <summary>
        /// 总成型号_Str
        /// </summary>
        public string TotalNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 配件标准名称
        /// </summary>
        public string SubstandardName {
            get;
            set;
        }
        /// <summary>
        /// 配件标准名称_Str
        /// </summary>
        public string SubstandardNameStr {
            get;
            set;
        }

        /// <summary>
        /// 厂商
        /// </summary>
        public string Factury {
            get;
            set;
        }
        /// <summary>
        /// 厂商_Str
        /// </summary>
        public string FacturyStr {
            get;
            set;
        }
        /// <summary>
        /// 是否原厂件
        /// </summary>
        public bool IsOriginal {
            get;
            set;
        }
        /// <summary>
        /// 是否原厂件_Str
        /// </summary>
        public string IsOriginalStr {
            get;
            set;
        }
        /// <summary>
        /// 分类编码
        /// </summary>
        public string CategoryCode {
            get;
            set;
        }
        /// <summary>
        /// 分类编码_Str
        /// </summary>
        public string CategoryCodeStr {
            get;
            set;
        }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string CategoryName {
            get;
            set;
        }
        /// <summary>
        /// 分类名称_Str
        /// </summary>
        public string CategoryNameStr {
            get;
            set;
        }
        /// <summary>
        /// 海外配件图号
        /// </summary>
        public string OverseasPartsFigure {
            get;
            set;
        }
        /// <summary>
        /// 海外配件图号_Str
        /// </summary>
        public string OverseasPartsFigureStr {
            get;
            set;
        }

        /// <summary>
        /// 金税分类编码
        /// </summary>
        public string GoldenTaxClassifyCode {
            get;
            set;
        }
        /// <summary>
        /// 金税分类编码_Str
        /// </summary>
        public string GoldenTaxClassifyCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 金税分类名称
        /// </summary>
        public string GoldenTaxClassifyName {
            get;
            set;
        }
        /// <summary>
        /// 金税分类名称_Str
        /// </summary>
        public string GoldenTaxClassifyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 互换识别号
        /// </summary>
        public string ExchangeIdentification {
            get;
            set;
        }
        /// <summary>
        /// 互换识别号_Str
        /// </summary>
        public string ExchangeIdentificationStr {
            get;
            set;
        }

        /// <summary>
        /// 产品执行标准代码
        /// </summary>
        public string StandardCode {
            get;
            set;
        }
        /// <summary>
        /// 产品执行标准名称
        /// </summary>
        public string StandardName {
            get;
            set;
        }
        /// <summary>
        /// 供应商图号---采购计划导入时用到
        /// </summary>
        public string SupplierPartCode
        {
            get;
            set;
        }
        /// <summary>
        /// 采购价---采购计划导入时用到
        /// </summary>
        public decimal PurchasePrice
        {
            get;
            set;
        }

        /// <summary>
        /// 闭口数量---采购计划导入时用到
        /// </summary>
        public int? LimitQty {
            get;
            set;
        }

        /// <summary>
        /// 已使用数量---采购计划导入时用到
        /// </summary>
        public int? UsedQty {
            get;
            set;
        }

        public int? PartABC {
            get;
            set;
        }
        public int? IsSupplierPutIn {
            get;
            set;
        }
        public string IsSupplierPutInStr {
            get;
            set;
        }
        /// <summary>
        /// 追溯属性
        /// </summary>
        public int TraceProperty {
            get;
            set;
        }
        /// <summary>
        /// 追溯属性_Str
        /// </summary>
        public string TracePropertyStr {
            get;
            set;
        }
        /// <summary>
        /// 安全天数最大值
        /// </summary>
        public int SafeDays {
            get;
            set;
        }
        /// <summary>
        /// 安全天数最大值_Str
        /// </summary>
        public string SafeDaysStr {
            get;
            set;
        }
        /// <summary>
        /// 库房天数
        /// </summary>
        public int WarehousDays {
            get;
            set;
        }
        /// <summary>
        /// 库房天数_Str
        /// </summary>
        public string WarehousDaysStr {
            get;
            set;
        }
        /// <summary>
        /// 临时天数
        /// </summary>
        public int TemDays {
            get;
            set;
        }
        /// <summary>
        /// 临时天数_Str
        /// </summary>
        public string TemDaysStr {
            get;
            set;
        }
    }
}
