﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class SpecialPriceChangeListExtend {
        
        public int SparePartId {
            get;
            set;
        }
        public string SparePartCode {
            get;
            set;
        }

        public string SparePartName {
            get;
            set;
        }
        public decimal SpecialTreatyPrice {
            get;
            set;
        }

        public string SpecialTreatyPriceStr {
            get;
            set;
        }

        public decimal? PurchasePrice {
            get;
            set;
        }

        public string Remark {
            get;
            set;
        }
        public string ErrorMsg {
            get;
            set;
        }
    }
}
