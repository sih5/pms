﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class SpecialTreatyPriceChangeWithDetailExtend {
        public int SequeueNumber {
            get;
            set;
        }
        /// <summary>
        /// 企业Id
        /// </summary>
        public int CorporationId {
            get;
            set;
        }

        /// <summary>
        /// 企业编号
        /// </summary>
        public string CorporationCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string CorporationNameStr {
            get;
            set;
        }

        /// <summary>
        /// 品牌Id
        /// </summary>
        public int BrandId {
            get;set;
        }

        /// <summary>
        /// 品牌 
        /// </summary>
        public string BrandNameStr {
            get;
            set;
        }
        /// <summary>
        /// 生效时间 
        /// </summary>
        public DateTime? ValidationTimeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 生效时间 
        /// </summary>
        public DateTime? ValidationTime
        {
            get;
            set;
        }
        /// <summary>
        /// 失效时间 
        /// </summary>
        public DateTime? ExpireTimeStr
        {
            get;
            set;
        }
        /// <summary>
        /// 失效时间 
        /// </summary>
        public DateTime? ExpireTime
        {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 特殊协议价
        /// </summary>
        public decimal SpecialTreatyPrice {
            get;
            set;
        }

        /// <summary>
        /// 特殊协议价
        /// </summary>
        public string SpecialTreatyPriceStr {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
    }
}
