﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class SubDealerExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称
        /// </summary>
        public string DealerName {
            get;
            set;
        }

        /// <summary>
        /// 服务站名称_Str
        /// </summary>
        public string DealerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号
        /// </summary>
        public string DealerCode {
            get;
            set;
        }

        /// <summary>
        /// 服务站编号_Str
        /// </summary>
        public string DealerCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 二级站名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 二级站名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 二级站编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 二级站编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 二级站类型
        /// </summary>
        public int? SubDealerType {
            get;
            set;
        }
        /// <summary>
        /// 二级站类型_Str
        /// </summary>
        public string SubDealerTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 一级站Id
        /// </summary>
        public int DealerId {
            get;
            set;
        }
        /// <summary>
        /// 一级站Id_Str
        /// </summary>
        public string DealerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address {
            get;
            set;
        }
        /// <summary>
        /// 地址_Str
        /// </summary>
        public string AddressStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 服务站站长
        /// </summary>
        public string DealerManager {
            get;
            set;
        }
        /// <summary>
        /// 服务站站长_Str
        /// </summary>
        public string DealerManagerStr {
            get;
            set;
        }

        /// <summary>
        /// 服务经理
        /// </summary>
        public string Manager {
            get;
            set;
        }
        /// <summary>
        /// 服务经理_Str
        /// </summary>
        public string ManagerStr {
            get;
            set;
        }

        /// <summary>
        /// 固定电话
        /// </summary>
        public string ManagerPhoneNumber {
            get;
            set;
        }
        /// <summary>
        /// 固定电话_Str
        /// </summary>
        public string ManagerPhoneNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 移动电话
        /// </summary>
        public string ManagerMobile {
            get;
            set;
        }
        /// <summary>
        /// 移动电话_Str
        /// </summary>
        public string ManagerMobileStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 电子邮箱
        /// </summary>
        public string ManagerMail {
            get;
            set;
        }
        /// <summary>
        /// 电子邮箱_Str
        /// </summary>
        public string ManagerMailStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }
    }
}
