﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class TemPurchasePlanOrderDetailExtend {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// 配件Id
        /// </summary>
        public int? SparePartId {
            get;
            set;
        }
        /// <summary>
        /// 配件Id_Str
        /// </summary>
        public string SparePartIdStr {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string SparePartCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string SparePartNameStr {
            get;
            set;
        }

        /// <summary>
        /// 计划量
        /// </summary>
        public int PlanAmount {
            get;
            set;
        }
        /// <summary>
        /// 计划量_Str
        /// </summary>
        public string PlanAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 单位
        /// </summary>
        public string MeasureUnit {
            get;
            set;
        }
        /// <summary>
        /// 单位_Str
        /// </summary>
        public string MeasureUnitStr {
            get;
            set;
        }
        /// <summary>
        /// 红岩号
        /// </summary>
        public string ReferenceCode {
            get;
            set;
        }
        /// <summary>
        /// 红岩号_Str
        /// </summary>
        public string ReferenceCodeStr {
            get;
            set;
        }
        public string ErrorMsg {
            get;
            set;
        }
        public int? SuplierId {
            get;
            set;
        }
        public string SuplierCode {
            get;
            set;
        }
        public string SuplierName {
            get;
            set;
        }
        public string SuplierCodeStr {
            get;
            set;
        }
        public string SuplierNameStr {
            get;
            set;
        }
        public string SupplierPartCode{
            get;
            set;
        }
    }
}
