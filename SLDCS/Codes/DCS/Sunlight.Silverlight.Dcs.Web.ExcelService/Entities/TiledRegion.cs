﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class TiledRegion {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        public string RegionName {
            get;
            set;
        }

        public string ProvinceName {
            get;
            set;
        }
        public string CityName {
            get;
            set;
        }
        public string CountyName {
            get;
            set;
        }
    }
}