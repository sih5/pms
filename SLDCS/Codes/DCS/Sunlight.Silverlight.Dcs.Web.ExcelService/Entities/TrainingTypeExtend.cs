﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class TrainingTypeExtend {
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int TrainingTypeCode {
            get;
            set;
        }

        public int TrainingTypeName {
            get;
            set;
        }
        public string TrainingTypeCodeStr {
            get;
            set;
        }
        public string TrainingTypeNameStr {
            get;
            set;
        }
        public string TrainingCode {
            get;
            set;
        }
        public string TrainingName {
            get;
            set;
        }
    }
}