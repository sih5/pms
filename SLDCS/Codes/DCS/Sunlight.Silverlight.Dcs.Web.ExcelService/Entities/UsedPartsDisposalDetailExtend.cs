﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class UsedPartsDisposalDetailExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件处理单Id
        /// </summary>
        public int UsedPartsDisposalBillId {
            get;
            set;
        }
        /// <summary>
        /// 旧件处理单Id_Str
        /// </summary>
        public string UsedPartsDisposalBillIdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件Id
        /// </summary>
        public int UsedPartsId {
            get;
            set;
        }
        /// <summary>
        /// 旧件配件Id_Str
        /// </summary>
        public string UsedPartsIdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件编号
        /// </summary>
        public string UsedPartsCode {
            get;
            set;
        }
        /// <summary>
        /// 旧件配件编号_Str
        /// </summary>
        public string UsedPartsCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件名称
        /// </summary>
        public string UsedPartsName {
            get;
            set;
        }
        /// <summary>
        /// 旧件配件名称_Str
        /// </summary>
        public string UsedPartsNameStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件条码
        /// </summary>
        public string UsedPartsBarCode {
            get;
            set;
        }
        /// <summary>
        /// 旧件条码_Str
        /// </summary>
        public string UsedPartsBarCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件批次号
        /// </summary>
        public string UsedPartsBatchNumber {
            get;
            set;
        }
        /// <summary>
        /// 旧件批次号_Str
        /// </summary>
        public string UsedPartsBatchNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件序列号
        /// </summary>
        public string UsedPartsSerialNumber {
            get;
            set;
        }
        /// <summary>
        /// 旧件序列号_Str
        /// </summary>
        public string UsedPartsSerialNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 价格
        /// </summary>
        public Decimal? Price {
            get;
            set;
        }
        /// <summary>
        /// 价格_Str
        /// </summary>
        public string PriceStr {
            get;
            set;
        }

        /// <summary>
        /// 分公司Id
        /// </summary>
        public int? BranchId {
            get;
            set;
        }
        /// <summary>
        /// 分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 计划量
        /// </summary>
        public int PlannedAmount {
            get;
            set;
        }
        /// <summary>
        /// 计划量_Str
        /// </summary>
        public string PlannedAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 确认量
        /// </summary>
        public int? ConfirmedAmount {
            get;
            set;
        }
        /// <summary>
        /// 确认量_Str
        /// </summary>
        public string ConfirmedAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 出库量
        /// </summary>
        public int? OutboundAmount {
            get;
            set;
        }
        /// <summary>
        /// 出库量_Str
        /// </summary>
        public string OutboundAmountStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商Id
        /// </summary>
        public int UsedPartsSupplierId {
            get;
            set;
        }
        /// <summary>
        /// 旧件供应商Id_Str
        /// </summary>
        public string UsedPartsSupplierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商编号
        /// </summary>
        public string UsedPartsSupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 旧件供应商编号_Str
        /// </summary>
        public string UsedPartsSupplierCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商名称
        /// </summary>
        public string UsedPartsSupplierName {
            get;
            set;
        }
        /// <summary>
        /// 旧件供应商名称_Str
        /// </summary>
        public string UsedPartsSupplierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔单Id
        /// </summary>
        public int ClaimBillId {
            get;
            set;
        }
        /// <summary>
        /// 索赔单Id_Str
        /// </summary>
        public string ClaimBillIdStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔单类型
        /// </summary>
        public int ClaimBillType {
            get;
            set;
        }
        /// <summary>
        /// 索赔单类型_Str
        /// </summary>
        public string ClaimBillTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔单编号
        /// </summary>
        public string ClaimBillCode {
            get;
            set;
        }
        /// <summary>
        /// 索赔单编号_Str
        /// </summary>
        public string ClaimBillCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 是否祸首件
        /// </summary>
        public bool? IfFaultyParts {
            get;
            set;
        }
        /// <summary>
        /// 是否祸首件_Str
        /// </summary>
        public string IfFaultyPartsStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商Id
        /// </summary>
        public int? FaultyPartsSupplierId {
            get;
            set;
        }
        /// <summary>
        /// 祸首件供应商Id_Str
        /// </summary>
        public string FaultyPartsSupplierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商编号
        /// </summary>
        public string FaultyPartsSupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 祸首件供应商编号_Str
        /// </summary>
        public string FaultyPartsSupplierCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商名称
        /// </summary>
        public string FaultyPartsSupplierName {
            get;
            set;
        }
        /// <summary>
        /// 祸首件供应商名称_Str
        /// </summary>
        public string FaultyPartsSupplierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 责任单位Id
        /// </summary>
        public int? ResponsibleUnitId {
            get;
            set;
        }
        /// <summary>
        /// 责任单位Id_Str
        /// </summary>
        public string ResponsibleUnitIdStr {
            get;
            set;
        }

        /// <summary>
        /// 责任单位编号
        /// </summary>
        public string ResponsibleUnitCode {
            get;
            set;
        }
        /// <summary>
        /// 责任单位编号_Str
        /// </summary>
        public string ResponsibleUnitCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 责任单位名称
        /// </summary>
        public string ResponsibleUnitName {
            get;
            set;
        }
        /// <summary>
        /// 责任单位名称_Str
        /// </summary>
        public string ResponsibleUnitNameStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件Id
        /// </summary>
        public int? FaultyPartsId {
            get;
            set;
        }
        /// <summary>
        /// 祸首件Id_Str
        /// </summary>
        public string FaultyPartsIdStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件编号
        /// </summary>
        public string FaultyPartsCode {
            get;
            set;
        }
        /// <summary>
        /// 祸首件编号_Str
        /// </summary>
        public string FaultyPartsCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件名称
        /// </summary>
        public string FaultyPartsName {
            get;
            set;
        }
        /// <summary>
        /// 祸首件名称_Str
        /// </summary>
        public string FaultyPartsNameStr {
            get;
            set;
        }


    }
}
