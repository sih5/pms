﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class UsedPartsDistanceInforExtend {
        /// <summary>
        /// 旧件仓储企业名称_Str
        /// </summary>
        public string UsedPartsCompanyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件仓库名称_Str
        /// </summary>
        public string UsedPartsWarehouseNameStr {
            get;
            set;
        }

        /// <summary>
        /// 源服务站编号_Str
        /// </summary>
        public string OriginCompanyCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 源服务站名称_Str
        /// </summary>
        public string OriginCompanyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 源旧件库编号_Str
        /// </summary>
        public string OriginUsedPartsWarehouseCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 源旧件库名称_Str
        /// </summary>
        public string OriginUsedPartsWarehouseNameStr {
            get;
            set;
        }


        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件仓储企业Id
        /// </summary>
        public int UsedPartsCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 旧件仓储企业Id_Str
        /// </summary>
        public string UsedPartsCompanyIdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件仓库Id
        /// </summary>
        public int UsedPartsWarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 旧件仓库Id_Str
        /// </summary>
        public string UsedPartsWarehouseIdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件运距类型
        /// </summary>
        public int UsedPartsDistanceType {
            get;
            set;
        }
        /// <summary>
        /// 旧件运距类型_Str
        /// </summary>
        public string UsedPartsDistanceTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 源服务站Id
        /// </summary>
        public int? OriginCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 源服务站Id_Str
        /// </summary>
        public string OriginCompanyIdStr {
            get;
            set;
        }

        /// <summary>
        /// 源旧件库Id
        /// </summary>
        public int? OriginUsedPartsWarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 源旧件库Id_Str
        /// </summary>
        public string OriginUsedPartsWarehouseIdStr {
            get;
            set;
        }

        /// <summary>
        /// 运距
        /// </summary>
        public int ShippingDistance {
            get;
            set;
        }
        /// <summary>
        /// 运距_Str
        /// </summary>
        public string ShippingDistanceStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人Id
        /// </summary>
        public int? AbandonerId {
            get;
            set;
        }
        /// <summary>
        /// 作废人Id_Str
        /// </summary>
        public string AbandonerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 作废人
        /// </summary>
        public string AbandonerName {
            get;
            set;
        }
        /// <summary>
        /// 作废人_Str
        /// </summary>
        public string AbandonerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 作废时间
        /// </summary>
        public DateTime? AbandonTime {
            get;
            set;
        }
        /// <summary>
        /// 作废时间_Str
        /// </summary>
        public string AbandonTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }


    }
}
