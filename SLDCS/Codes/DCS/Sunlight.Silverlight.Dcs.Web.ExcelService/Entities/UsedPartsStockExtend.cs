﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class UsedPartsStockExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件仓库Id
        /// </summary>
        public int UsedPartsWarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 旧件仓库Id_Str
        /// </summary>
        public string UsedPartsWarehouseIdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件库位Id
        /// </summary>
        public int UsedPartsWarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 旧件库位Id_Str
        /// </summary>
        public string UsedPartsWarehouseAreaIdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件Id
        /// </summary>
        public int UsedPartsId {
            get;
            set;
        }
        /// <summary>
        /// 旧件配件Id_Str
        /// </summary>
        public string UsedPartsIdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件编号
        /// </summary>
        public string UsedPartsCode {
            get;
            set;
        }
        /// <summary>
        /// 旧件配件编号_Str
        /// </summary>
        public string UsedPartsCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件配件名称
        /// </summary>
        public string UsedPartsName {
            get;
            set;
        }
        /// <summary>
        /// 旧件配件名称_Str
        /// </summary>
        public string UsedPartsNameStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件条码
        /// </summary>
        public string UsedPartsBarCode {
            get;
            set;
        }
        /// <summary>
        /// 旧件条码_Str
        /// </summary>
        public string UsedPartsBarCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件批次号
        /// </summary>
        public string UsedPartsBatchNumber {
            get;
            set;
        }
        /// <summary>
        /// 旧件批次号_Str
        /// </summary>
        public string UsedPartsBatchNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件序列号
        /// </summary>
        public string UsedPartsSerialNumber {
            get;
            set;
        }
        /// <summary>
        /// 旧件序列号_Str
        /// </summary>
        public string UsedPartsSerialNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 库存数量
        /// </summary>
        public int StorageQuantity {
            get;
            set;
        }
        /// <summary>
        /// 库存数量_Str
        /// </summary>
        public string StorageQuantityStr {
            get;
            set;
        }

        /// <summary>
        /// 锁定数量
        /// </summary>
        public int LockedQuantity {
            get;
            set;
        }
        /// <summary>
        /// 锁定数量_Str
        /// </summary>
        public string LockedQuantityStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔单Id
        /// </summary>
        public int? ClaimBillId {
            get;
            set;
        }
        /// <summary>
        /// 索赔单Id_Str
        /// </summary>
        public string ClaimBillIdStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔单类型
        /// </summary>
        public int? ClaimBillType {
            get;
            set;
        }
        /// <summary>
        /// 索赔单类型_Str
        /// </summary>
        public string ClaimBillTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 索赔单编号
        /// </summary>
        public string ClaimBillCode {
            get;
            set;
        }
        /// <summary>
        /// 索赔单编号_Str
        /// </summary>
        public string ClaimBillCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件Id
        /// </summary>
        public int? FaultyPartsId {
            get;
            set;
        }
        /// <summary>
        /// 祸首件Id_Str
        /// </summary>
        public string FaultyPartsIdStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件编号
        /// </summary>
        public string FaultyPartsCode {
            get;
            set;
        }
        /// <summary>
        /// 祸首件编号_Str
        /// </summary>
        public string FaultyPartsCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件名称
        /// </summary>
        public string FaultyPartsName {
            get;
            set;
        }
        /// <summary>
        /// 祸首件名称_Str
        /// </summary>
        public string FaultyPartsNameStr {
            get;
            set;
        }

        /// <summary>
        /// 是否祸首件
        /// </summary>
        public bool? IfFaultyParts {
            get;
            set;
        }
        /// <summary>
        /// 是否祸首件_Str
        /// </summary>
        public string IfFaultyPartsStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商Id
        /// </summary>
        public int? UsedPartsSupplierId {
            get;
            set;
        }
        /// <summary>
        /// 旧件供应商Id_Str
        /// </summary>
        public string UsedPartsSupplierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商编号
        /// </summary>
        public string UsedPartsSupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 旧件供应商编号_Str
        /// </summary>
        public string UsedPartsSupplierCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 旧件供应商名称
        /// </summary>
        public string UsedPartsSupplierName {
            get;
            set;
        }
        /// <summary>
        /// 旧件供应商名称_Str
        /// </summary>
        public string UsedPartsSupplierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商Id
        /// </summary>
        public int? FaultyPartsSupplierId {
            get;
            set;
        }
        /// <summary>
        /// 祸首件供应商Id_Str
        /// </summary>
        public string FaultyPartsSupplierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商编号
        /// </summary>
        public string FaultyPartsSupplierCode {
            get;
            set;
        }
        /// <summary>
        /// 祸首件供应商编号_Str
        /// </summary>
        public string FaultyPartsSupplierCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 祸首件供应商名称
        /// </summary>
        public string FaultyPartsSupplierName {
            get;
            set;
        }
        /// <summary>
        /// 祸首件供应商名称_Str
        /// </summary>
        public string FaultyPartsSupplierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 责任单位Id
        /// </summary>
        public int? ResponsibleUnitId {
            get;
            set;
        }
        /// <summary>
        /// 责任单位Id_Str
        /// </summary>
        public string ResponsibleUnitIdStr {
            get;
            set;
        }

        /// <summary>
        /// 责任单位编号
        /// </summary>
        public string ResponsibleUnitCode {
            get;
            set;
        }
        /// <summary>
        /// 责任单位编号_Str
        /// </summary>
        public string ResponsibleUnitCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 责任单位名称
        /// </summary>
        public string ResponsibleUnitName {
            get;
            set;
        }
        /// <summary>
        /// 责任单位名称_Str
        /// </summary>
        public string ResponsibleUnitNameStr {
            get;
            set;
        }

        /// <summary>
        /// 结算价格
        /// </summary>
        public Decimal? SettlementPrice {
            get;
            set;
        }
        /// <summary>
        /// 结算价格_Str
        /// </summary>
        public string SettlementPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 成本价格
        /// </summary>
        public Decimal? CostPrice {
            get;
            set;
        }
        /// <summary>
        /// 成本价格_Str
        /// </summary>
        public string CostPriceStr {
            get;
            set;
        }

        /// <summary>
        /// 分公司Id
        /// </summary>
        public int? BranchId {
            get;
            set;
        }
        /// <summary>
        /// 分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }


    }
}
