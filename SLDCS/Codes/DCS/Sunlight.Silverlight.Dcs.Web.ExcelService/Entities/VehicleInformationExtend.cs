﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class VehicleInformationExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// VIN
        /// </summary>
        public string VIN {
            get;
            set;
        }
        /// <summary>
        /// VIN_Str
        /// </summary>
        public string VINStr {
            get;
            set;
        }

        /// <summary>
        /// 出厂编号
        /// </summary>
        public string SerialNumber {
            get;
            set;
        }
        /// <summary>
        /// 出厂编号_Str
        /// </summary>
        public string SerialNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 是否车队客户
        /// </summary>
        public int? IsTeamCustomers {
            get;
            set;
        }
        /// <summary>
        /// 原是否车队客户
        /// </summary>
        public int? BIsTeamCustomers {
            get;
            set;
        }
        /// <summary>
        /// 是否车队客户_Str
        /// </summary>
        public string IsTeamCustomersStr {
            get;
            set;
        }


        /// <summary>
        /// 原始VIN
        /// </summary>
        public string OldVIN {
            get;
            set;
        }
        /// <summary>
        /// 原始VIN_Str
        /// </summary>
        public string OldVINStr {
            get;
            set;
        }

        /// <summary>
        /// 责任单位Id
        /// </summary>
        public int? ResponsibleUnitId {
            get;
            set;
        }
        /// <summary>
        /// 责任单位Id_Str
        /// </summary>
        public string ResponsibleUnitIdStr {
            get;
            set;
        }

        /// <summary>
        /// 责任单位名称
        /// </summary>
        public string ResponsibleUnitName {
            get;
            set;
        }
        /// <summary>
        /// 责任单位名称_Str
        /// </summary>
        public string ResponsibleUnitNameStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆种类Id
        /// </summary>
        public int VehicleCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 车辆种类Id_Str
        /// </summary>
        public string VehicleCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆种类名称
        /// </summary>
        public string VehicleCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 车辆种类名称_Str
        /// </summary>
        public string VehicleCategoryNameStr {
            get;
            set;
        }

        /// <summary>
        /// 车牌号
        /// </summary>
        public string VehicleLicensePlate {
            get;
            set;
        }
        /// <summary>
        /// 车牌号_Str
        /// </summary>
        public string VehicleLicensePlateStr {
            get;
            set;
        }

        /// <summary>
        /// 大客户（VIP）车辆
        /// </summary>
        public bool VIPVehicle {
            get;
            set;
        }

        /// <summary>
        /// 大客户（VIP）车辆
        /// </summary>
        public bool BVIPVehicle {
            get;
            set;
        }
        /// <summary>
        /// 大客户（VIP）车辆_Str
        /// </summary>
        public string VIPVehicleStr {
            get;
            set;
        }

        /// <summary>
        /// 车系
        /// </summary>
        public string VehicleSeries {
            get;
            set;
        }
        /// <summary>
        /// 车系_Str
        /// </summary>
        public string VehicleSeriesStr {
            get;
            set;
        }

        /// <summary>
        /// 产品线名称
        /// </summary>
        public string ProductLineName {
            get;
            set;
        }
        /// <summary>
        /// 产品线名称_Str
        /// </summary>
        public string ProductLineNameStr {
            get;
            set;
        }

        /// <summary>
        /// 车型平台
        /// </summary>
        public string ProductCategoryTerrace {
            get;
            set;
        }
        /// <summary>
        /// 车型平台_Str
        /// </summary>
        public string ProductCategoryTerraceStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆功能
        /// </summary>
        public string VehicleFunction {
            get;
            set;
        }
        /// <summary>
        /// 车辆功能_Str
        /// </summary>
        public string VehicleFunctionStr {
            get;
            set;
        }

        /// <summary>
        /// 车身颜色
        /// </summary>
        public string VehicleColor {
            get;
            set;
        }
        /// <summary>
        /// 车身颜色_Str
        /// </summary>
        public string VehicleColorStr {
            get;
            set;
        }

        /// <summary>
        /// 上装编码
        /// </summary>
        public string TopsCode {
            get;
            set;
        }
        /// <summary>
        /// 上装编码_Str
        /// </summary>
        public string TopsCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 改装类型
        /// </summary>
        public int? AdaptType {
            get;
            set;
        }
        /// <summary>
        /// 改装类型_Str
        /// </summary>
        public string AdaptTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 改装单位名称
        /// </summary>
        public string AdaptCompanyName {
            get;
            set;
        }
        /// <summary>
        /// 改装单位名称_Str
        /// </summary>
        public string AdaptCompanyNameStr {
            get;
            set;
        }

        /// <summary>
        /// 驾驶室型号
        /// </summary>
        public string CageCategory {
            get;
            set;
        }
        /// <summary>
        /// 驾驶室型号_Str
        /// </summary>
        public string CageCategoryStr {
            get;
            set;
        }

        /// <summary>
        /// 车桥类型
        /// </summary>
        public int? BridgeType {
            get;
            set;
        }
        /// <summary>
        /// 车桥类型_Str
        /// </summary>
        public string BridgeTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 是否公路车
        /// </summary>
        public bool IsRoadVehicl {
            get;
            set;
        }
        /// <summary>
        /// 是否公路车_Str
        /// </summary>
        public string IsRoadVehiclStr {
            get;
            set;
        }

        /// <summary>
        /// 前桥号
        /// </summary>
        public string FrontAxleCode {
            get;
            set;
        }
        /// <summary>
        /// 前桥号_Str
        /// </summary>
        public string FrontAxleCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 前二桥号
        /// </summary>
        public string SecondFrontAxleCode {
            get;
            set;
        }
        /// <summary>
        /// 前二桥号_Str
        /// </summary>
        public string SecondFrontAxleCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 中桥号
        /// </summary>
        public string CentreAxleCode {
            get;
            set;
        }
        /// <summary>
        /// 中桥号_Str
        /// </summary>
        public string CentreAxleCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 后桥号
        /// </summary>
        public string BehindAxleCode {
            get;
            set;
        }
        /// <summary>
        /// 后桥号_Str
        /// </summary>
        public string BehindAxleCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 后桥型号及吨级
        /// </summary>
        public string BehindAxleTypTonneLevel {
            get;
            set;
        }
        /// <summary>
        /// 后桥型号及吨级_Str
        /// </summary>
        public string BehindAxleTypTonneLevelStr {
            get;
            set;
        }

        /// <summary>
        /// 平衡轴号
        /// </summary>
        public string BalanceShaftCode {
            get;
            set;
        }
        /// <summary>
        /// 平衡轴号_Str
        /// </summary>
        public string BalanceShaftCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 转向机
        /// </summary>
        public string SteeringEngine {
            get;
            set;
        }
        /// <summary>
        /// 转向机_Str
        /// </summary>
        public string SteeringEngineStr {
            get;
            set;
        }

        /// <summary>
        /// 底盘品牌
        /// </summary>
        public string ChassisBrandName {
            get;
            set;
        }
        /// <summary>
        /// 底盘品牌_Str
        /// </summary>
        public string ChassisBrandNameStr {
            get;
            set;
        }

        /// <summary>
        /// 底盘号
        /// </summary>
        public string ChassisCode {
            get;
            set;
        }
        /// <summary>
        /// 底盘号_Str
        /// </summary>
        public string ChassisCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 产品Id
        /// </summary>
        public int? ProductId {
            get;
            set;
        }
        /// <summary>
        /// 产品Id_Str
        /// </summary>
        public string ProductIdStr {
            get;
            set;
        }

        /// <summary>
        /// 产品编号
        /// </summary>
        public string ProductCode {
            get;
            set;
        }
        /// <summary>
        /// 产品编号_Str
        /// </summary>
        public string ProductCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 品牌
        /// </summary>
        public string BrandName {
            get;
            set;
        }
        /// <summary>
        /// 品牌_Str
        /// </summary>
        public string BrandNameStr {
            get;
            set;
        }

        /// <summary>
        /// 销售车型编号
        /// </summary>
        public string ProductCategoryCode {
            get;
            set;
        }
        /// <summary>
        /// 销售车型编号_Str
        /// </summary>
        public string ProductCategoryCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 销售车型名称
        /// </summary>
        public string ProductCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 销售车型名称_Str
        /// </summary>
        public string ProductCategoryNameStr {
            get;
            set;
        }

        /// <summary>
        /// 发动机厂家
        /// </summary>
        public string EngineManufacture {
            get;
            set;
        }
        /// <summary>
        /// 发动机厂家_Str
        /// </summary>
        public string EngineManufactureStr {
            get;
            set;
        }

        /// <summary>
        /// 公告号
        /// </summary>
        public string AnnoucementNumber {
            get;
            set;
        }
        /// <summary>
        /// 公告号_Str
        /// </summary>
        public string AnnoucementNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 发动机型号
        /// </summary>
        public string EngineModel {
            get;
            set;
        }
        /// <summary>
        /// 发动机型号_Str
        /// </summary>
        public string EngineModelStr {
            get;
            set;
        }

        /// <summary>
        /// 发动机型号Id
        /// </summary>
        public int? EngineModelId {
            get;
            set;
        }
        /// <summary>
        /// 发动机型号Id_Str
        /// </summary>
        public string EngineModelIdStr {
            get;
            set;
        }

        /// <summary>
        /// 变速箱型号
        /// </summary>
        public int? GearModel {
            get;
            set;
        }
        /// <summary>
        /// 变速箱型号_Str
        /// </summary>
        public string GearModelStr {
            get;
            set;
        }

        /// <summary>
        /// 发动机序列号
        /// </summary>
        public string EngineSerialNumber {
            get;
            set;
        }
        /// <summary>
        /// 发动机序列号_Str
        /// </summary>
        public string EngineSerialNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 变速箱序列号
        /// </summary>
        public string GearSerialNumber {
            get;
            set;
        }
        /// <summary>
        /// 变速箱序列号_Str
        /// </summary>
        public string GearSerialNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 首次故障里程
        /// </summary>
        public int? FirstStoppageMileage {
            get;
            set;
        }
        /// <summary>
        /// 首次故障里程_Str
        /// </summary>
        public string FirstStoppageMileageStr {
            get;
            set;
        }

        /// <summary>
        /// 出厂日期
        /// </summary>
        public DateTime? OutOfFactoryDate {
            get;
            set;
        }
        /// <summary>
        /// 出厂日期_Str
        /// </summary>
        public string OutOfFactoryDateStr {
            get;
            set;
        }

        /// <summary>
        /// 是否售出
        /// </summary>
        public bool? IsWorkOff {
            get;
            set;
        }
        /// <summary>
        /// 是否售出_Str
        /// </summary>
        public string IsWorkOffStr {
            get;
            set;
        }

        /// <summary>
        /// 销售日期
        /// </summary>
        public DateTime? SalesDate {
            get;
            set;
        }
        /// <summary>
        /// 销售日期_Str
        /// </summary>
        public string SalesDateStr {
            get;
            set;
        }

        /// <summary>
        /// 方量
        /// </summary>
        public int? Capacity {
            get;
            set;
        }
        /// <summary>
        /// 方量_Str
        /// </summary>
        public string CapacityStr {
            get;
            set;
        }

        /// <summary>
        /// 工作小时
        /// </summary>
        public int? WorkingHours {
            get;
            set;
        }
        /// <summary>
        /// 工作小时_Str
        /// </summary>
        public string WorkingHoursStr {
            get;
            set;
        }

        /// <summary>
        /// 行驶里程
        /// </summary>
        public int? Mileage {
            get;
            set;
        }
        /// <summary>
        /// 行驶里程_Str
        /// </summary>
        public string MileageStr {
            get;
            set;
        }

        /// <summary>
        /// 最近保养工作小时
        /// </summary>
        public int? LastMaintenanceWorkingHours {
            get;
            set;
        }
        /// <summary>
        /// 最近保养工作小时_Str
        /// </summary>
        public string LastMaintenanceWorkingHoursStr {
            get;
            set;
        }

        /// <summary>
        /// 最近保养方量
        /// </summary>
        public int? LastMaintenanceCapacity {
            get;
            set;
        }
        /// <summary>
        /// 最近保养方量_Str
        /// </summary>
        public string LastMaintenanceCapacityStr {
            get;
            set;
        }

        /// <summary>
        /// 最近保养行驶里程
        /// </summary>
        public int? LastMaintenanceMileage {
            get;
            set;
        }
        /// <summary>
        /// 最近保养行驶里程_Str
        /// </summary>
        public string LastMaintenanceMileageStr {
            get;
            set;
        }

        /// <summary>
        /// 最近保养日期
        /// </summary>
        public DateTime? LastMaintenanceTime {
            get;
            set;
        }
        /// <summary>
        /// 最近保养日期_Str
        /// </summary>
        public string LastMaintenanceTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 销售发票号
        /// </summary>
        public string SalesInvoiceNumber {
            get;
            set;
        }
        /// <summary>
        /// 销售发票号_Str
        /// </summary>
        public string SalesInvoiceNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 开发票日期
        /// </summary>
        public DateTime? InvoiceDate {
            get;
            set;
        }
        /// <summary>
        /// 开发票日期_Str
        /// </summary>
        public string InvoiceDateStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆类型
        /// </summary>
        public int? VehicleType {
            get;
            set;
        }
        /// <summary>
        /// 车辆类型_Str
        /// </summary>
        public string VehicleTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 售前检查最大次数
        /// </summary>
        public int? PreSaleMaxTime {
            get;
            set;
        }
        /// <summary>
        /// 售前检查最大次数_Str
        /// </summary>
        public string PreSaleMaxTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 售前检查次数
        /// </summary>
        public int? PreSaleTime {
            get;
            set;
        }
        /// <summary>
        /// 售前检查次数_Str
        /// </summary>
        public string PreSaleTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int? Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }
        /// <summary>
        /// 是否保修
        /// </summary>
        public bool IsGuarantee {
            get;
            set;
        }
        /// <summary>
        /// 是否保修Str
        /// </summary>
        public string IsGuaranteeStr {
            get;
            set;
        }


    }
}
