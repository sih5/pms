﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class VehicleMainteProductCategoryExtend {
        /// <summary>
        /// ID
        /// </summary>
        public int id {
            get;
            set;
        }
        /// <summary>
        /// 整车保养条款Id
        /// </summary>
        public int VehicleMainteTermId {
            get;
            set;
        }
        /// <summary>
        /// 车型名称
        /// </summary>
        public string ProductCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 车型编号
        /// </summary>
        public string ProductCategoryCode {
            get;
            set;
        }
        /// <summary>
        /// 材料费
        /// </summary>
        public decimal? MaterialCost {
            get;
            set;
        }
        /// <summary>
        /// 类别1
        /// </summary>
        public int? Type1 {
            get;
            set;
        }
        /// <summary>
        /// 用量
        /// </summary>
        public double? Quantity {
            get;
            set;
        }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal? Price {
            get;
            set;
        }
        /// <summary>
        /// 类别1金额
        /// </summary>
        public decimal? Type1Amount {
            get;
            set;
        }
        /// <summary>
        /// 类别2
        /// </summary>
        public int? Type2 {
            get;
            set;
        }
        /// <summary>
        /// 用量2
        /// </summary>
        public double? Quantity2 {
            get;
            set;
        }
        /// <summary>
        /// 单价2
        /// </summary>
        public decimal? Price2 {
            get;
            set;
        }
        /// <summary>
        /// 类别2金额
        /// </summary>
        public decimal? Type2Amount {
            get;
            set;
        }


        public string idStr {
            get;
            set;
        }

        public string VehicleMainteTermIdStr {
            get;
            set;
        }

        public string ProductCategoryNameStr {
            get;
            set;
        }

        public string ProductCategoryCodeStr {
            get;
            set;
        }

        public string MaterialCostStr {
            get;
            set;
        }

        public string Type1Str {
            get;
            set;
        }

        public string QuantityStr {
            get;
            set;
        }

        public string PriceStr {
            get;
            set;
        }

        public string Type1AmountStr {
            get;
            set;
        }

        public string Type2Str {
            get;
            set;
        }

        public string Quantity2Str {
            get;
            set;
        }

        public string Price2Str {
            get;
            set;
        }

        public string Type2AmountStr {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }
    }
}
