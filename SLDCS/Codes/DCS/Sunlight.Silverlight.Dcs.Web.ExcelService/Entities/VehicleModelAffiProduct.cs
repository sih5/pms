﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class VehicleModelAffiProduct {
        /// <summary>
        /// 产品ID
        /// </summary>
        public int ProductId {
            get;
            set;
        }

        /// <summary>
        /// 产品编号
        /// </summary>
        public string ProductCode {
            get;
            set;
        }

        /// <summary>
        /// 销售车型编号
        /// </summary>
        public string ProductCategoryCode {
            get;
            set;
        }

        /// <summary>
        /// 销售车型名称
        /// </summary>
        public string ProductCategoryName {
            get;
            set;
        }
    }
}
