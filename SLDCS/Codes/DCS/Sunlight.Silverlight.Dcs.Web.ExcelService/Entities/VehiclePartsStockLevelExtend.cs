﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService.Entities {
    public class VehiclePartsStockLevelExtend {
        /// <summary> 
        /// Id 
        /// </summary> 
        public int Id {
            get;
            set;
        }
        /// <summary> 
        /// 品牌Id 
        /// </summary> 
        public int BrandId {
            get;
            set;
        }
        /// <summary> 
        /// 品牌编码 
        /// </summary> 
        public string BrandCode {
            get;
            set;
        }
        /// <summary> 
        /// 品牌名称 
        /// </summary> 
        public string BrandName {
            get;
            set;
        }
        /// <summary> 
        /// 仓库Id 
        /// </summary> 
        public int WarehouseId {
            get;
            set;
        }
        /// <summary> 
        /// 仓库编码 
        /// </summary> 
        public string WarehouseCode {
            get;
            set;
        }
        /// <summary> 
        /// 仓库名称 
        /// </summary> 
        public string WarehouseName {
            get;
            set;
        }
        /// <summary> 
        /// 配件Id 
        /// </summary> 
        public int PartId {
            get;
            set;
        }
        /// <summary> 
        /// 配件编码 
        /// </summary> 
        public string PartCode {
            get;
            set;
        }
        /// <summary> 
        /// 配件名称 
        /// </summary> 
        public string PartName {
            get;
            set;
        }
        /// <summary> 
        /// 库存高限 
        /// </summary> 
        public int StockMaximum {
            get;
            set;
        }
        /// <summary> 
        /// 库存低限 
        /// </summary> 
        public int StockMinimum {
            get;
            set;
        }
        /// <summary> 
        /// 安全库存 
        /// </summary> 
        public int SafeStock {
            get;
            set;
        }
        /// <summary> 
        /// 状态 
        /// </summary> 
        public int Status {
            get;
            set;
        }
        /// <summary> 
        /// 创建人Id 
        /// </summary> 
        public int CreatorId {
            get;
            set;
        }
        /// <summary> 
        /// 创建人 
        /// </summary> 
        public string CreatorName {
            get;
            set;
        }
        /// <summary> 
        /// 创建时间 
        /// </summary> 
        public string CreateTime {
            get;
            set;
        }
        /// <summary> 
        /// 修改人Id 
        /// </summary> 
        public string ModifierId {
            get;
            set;
        }
        /// <summary> 
        /// 修改人 
        /// </summary> 
        public int ModifierName {
            get;
            set;
        }
        /// <summary> 
        /// 修改时间 
        /// </summary> 
        public string ModifyTime {
            get;
            set;
        }
        /// <summary> 
        /// 品牌IdStr 
        /// </summary> 
        public string BrandIdStr {
            get;
            set;
        }
        /// <summary> 
        /// 仓库IdStr 
        /// </summary> 
        public string WarehouseIdStr {
            get;
            set;
        }
        /// <summary> 
        /// 配件IdStr 
        /// </summary> 
        public string PartIdStr {
            get;
            set;
        }
        /// <summary> 
        /// 库存高限Str 
        /// </summary> 
        public string StockMaximumStr {
            get;
            set;
        }
        /// <summary> 
        /// 库存低限Str 
        /// </summary> 
        public string StockMinimumStr {
            get;
            set;
        }
        /// <summary> 
        /// 安全库存Str 
        /// </summary> 
        public string SafeStockStr {
            get;
            set;
        }
        /// <summary> 
        /// 状态Str 
        /// </summary> 
        public string StatusStr {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }

    }
}
