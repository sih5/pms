﻿namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class VehicleSery {
        /// <summary>
        /// 产品编号
        /// </summary>
        public string ProductCode {
            get;
            set;
        }

        /// <summary>
        /// 车系ID
        /// </summary>
        public int Id {
            get;
            set;
        }

        /// <summary>
        /// 车系代码编号
        /// </summary>
        public string VehicleSeriesCode {
            get;
            set;
        }

        /// <summary>
        /// 区域代码
        /// </summary>
        public string RegionCode {
            get;
            set;
        }
    }
}
