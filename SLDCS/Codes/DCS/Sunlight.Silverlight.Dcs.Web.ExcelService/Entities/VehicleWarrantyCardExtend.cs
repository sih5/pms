﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class VehicleWarrantyCardExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 保修卡编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 保修卡编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆信息Id
        /// </summary>
        public int VehicleId {
            get;
            set;
        }
        /// <summary>
        /// 车辆信息Id_Str
        /// </summary>
        public string VehicleIdStr {
            get;
            set;
        }

        /// <summary>
        /// WarrantyPolicyCode
        /// </summary>
        public string WarrantyPolicyCode {
            get;
            set;
        }
        /// <summary>
        /// WarrantyPolicyCode_Str
        /// </summary>
        public string WarrantyPolicyCodeStr {
            get;
            set;
        }

        /// <summary>
        /// WarrantyPolicyName
        /// </summary>
        public string WarrantyPolicyName {
            get;
            set;
        }
        /// <summary>
        /// WarrantyPolicyName_Str
        /// </summary>
        public string WarrantyPolicyNameStr {
            get;
            set;
        }

        /// <summary>
        /// VIN
        /// </summary>
        public string VIN {
            get;
            set;
        }
        /// <summary>
        /// VIN_Str
        /// </summary>
        public string VINStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆种类Id
        /// </summary>
        public int VehicleCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 车辆种类Id_Str
        /// </summary>
        public string VehicleCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 车辆种类名称
        /// </summary>
        public string VehicleCategoryName {
            get;
            set;
        }
        /// <summary>
        /// 车辆种类名称_Str
        /// </summary>
        public string VehicleCategoryNameStr {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线Id
        /// </summary>
        public int ServiceProductLineId {
            get;
            set;
        }
        /// <summary>
        /// 服务产品线Id_Str
        /// </summary>
        public string ServiceProductLineIdStr {
            get;
            set;
        }

        /// <summary>
        /// 服务产品线名称
        /// </summary>
        public string ServiceProductLineName {
            get;
            set;
        }
        /// <summary>
        /// 服务产品线名称_Str
        /// </summary>
        public string ServiceProductLineNameStr {
            get;
            set;
        }

        /// <summary>
        /// 产品Id
        /// </summary>
        public int ProductId {
            get;
            set;
        }
        /// <summary>
        /// 产品Id_Str
        /// </summary>
        public string ProductIdStr {
            get;
            set;
        }

        /// <summary>
        /// 产品编号
        /// </summary>
        public string ProductCode {
            get;
            set;
        }
        /// <summary>
        /// 产品编号_Str
        /// </summary>
        public string ProductCodeStr {
            get;
            set;
        }

        /// <summary>
        /// 销售日期
        /// </summary>
        public DateTime? SalesDate {
            get;
            set;
        }
        /// <summary>
        /// 销售日期_Str
        /// </summary>
        public string SalesDateStr {
            get;
            set;
        }

        /// <summary>
        /// 保修开始日期
        /// </summary>
        public DateTime WarrantyStartDate {
            get;
            set;
        }
        /// <summary>
        /// 保修开始日期_Str
        /// </summary>
        public string WarrantyStartDateStr {
            get;
            set;
        }

        /// <summary>
        /// 保有客户Id
        /// </summary>
        public int RetainedCustomerId {
            get;
            set;
        }
        /// <summary>
        /// 保有客户Id_Str
        /// </summary>
        public string RetainedCustomerIdStr {
            get;
            set;
        }

        /// <summary>
        /// 客户姓名
        /// </summary>
        public string CustomerName {
            get;
            set;
        }
        /// <summary>
        /// 客户姓名_Str
        /// </summary>
        public string CustomerNameStr {
            get;
            set;
        }

        /// <summary>
        /// 客户性别
        /// </summary>
        public int CustomerGender {
            get;
            set;
        }
        /// <summary>
        /// 客户性别_Str
        /// </summary>
        public string CustomerGenderStr {
            get;
            set;
        }

        /// <summary>
        /// 证件类型
        /// </summary>
        public int? IdDocumentType {
            get;
            set;
        }
        /// <summary>
        /// 证件类型_Str
        /// </summary>
        public string IdDocumentTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 证件号码
        /// </summary>
        public string IdDocumentNumber {
            get;
            set;
        }
        /// <summary>
        /// 证件号码_Str
        /// </summary>
        public string IdDocumentNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 保修政策类型
        /// </summary>
        public int WarrantyPolicyCategory {
            get;
            set;
        }
        /// <summary>
        /// 保修政策类型_Str
        /// </summary>
        public string WarrantyPolicyCategoryStr {
            get;
            set;
        }

        /// <summary>
        /// 保修政策Id
        /// </summary>
        public int WarrantyPolicyId {
            get;
            set;
        }
        /// <summary>
        /// 保修政策Id_Str
        /// </summary>
        public string WarrantyPolicyIdStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 分公司
        /// </summary>
        public string BrandName {
            get;
            set;
        }

        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }


    }
}
