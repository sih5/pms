﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    class WarehouseAreaCategoryExtend {
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }
        /// <summary>
        /// 库区用途
        /// </summary>
        public string Category {
            get;
            set;
        }
        /// <summary>
        /// 库区用途_Str
        /// </summary>
        public string CategoryStr {
            get;
            set;
        }
    }
}
