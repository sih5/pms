﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class WarehouseAreaExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }
        /// <summary>
        /// 仓库Id_Str
        /// </summary>
        public string WarehouseIdStr {
            get;
            set;
        }

        /// <summary>
        /// 父节点Id
        /// </summary>
        public int? ParentId {
            get;
            set;
        }
        /// <summary>
        /// 父节点Id_Str
        /// </summary>
        public string ParentIdStr {
            get;
            set;
        }

        /// <summary>
        /// 库区库位编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 库区库位编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 顶层库区Id
        /// </summary>
        public int? TopLevelWarehouseAreaId {
            get;
            set;
        }
        /// <summary>
        /// 顶层库区Id_Str
        /// </summary>
        public string TopLevelWarehouseAreaIdStr {
            get;
            set;
        }

        /// <summary>
        /// 库区用途Id
        /// </summary>
        public int? AreaCategoryId {
            get;
            set;
        }
        /// <summary>
        /// 库区用途Id_Str
        /// </summary>
        public string AreaCategoryIdStr {
            get;
            set;
        }

        /// <summary>
        /// 库区用途
        /// </summary>
        public string Category {
            get;
            set;
        }
        /// <summary>
        /// 库区用途_Str
        /// </summary>
        public string CategoryStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 库区库位类型
        /// </summary>
        public int AreaKind {
            get;
            set;
        }
        /// <summary>
        /// 库区库位类型_Str
        /// </summary>
        public string AreaKindStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }
    }
}
