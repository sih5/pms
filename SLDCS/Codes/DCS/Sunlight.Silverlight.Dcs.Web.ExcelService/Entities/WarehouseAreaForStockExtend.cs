﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public class WarehouseAreaForStockExtend
    {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg
        {
            get;
            set;
        }
        /// <summary>
        /// 配件ID
        /// </summary>
        public int PartId
        {
            get;
            set;
        }

        /// <summary>
        /// 配件编号
        /// </summary>
        public string PartCode
        {
            get;
            set;
        }
        /// <summary>
        /// 配件编号_Str
        /// </summary>
        public string PartCodeStr
        {
            get;
            set;
        }

        /// <summary>
        /// 配件名称
        /// </summary>
        public string PartName
        {
            get;
            set;
        }
        /// <summary>
        /// 配件名称_Str
        /// </summary>
        public string PartNameStr
        {
            get;
            set;
        }

        /// <summary>
        /// 库位Id
        /// </summary>
        public int WarehouseAreaId
        {
            get;
            set;
        }

        /// <summary>
        /// 库位编号
        /// </summary>
        public string WarehouseAreaCode
        {
            get;
            set;
        }
        /// <summary>
        /// 库位编号_Str
        /// </summary>
        public string WarehouseAreaCodeStr
        {
            get;
            set;
        }

        /// <summary>
        /// 隶属企业ID
        /// </summary>
        public int StorageCompanyId
        {
            get;
            set;
        }

        /// <summary>
        /// 隶属企业类型
        /// </summary>
        public int StorageCompanyType
        {
            get;
            set;
        }

        public int BranchId
        {
            get;
            set;
        }

        /// <summary>
        /// 库区用途ID
        /// </summary>
        public int AreaCategoryId
        {
            get;
            set;
        }

        /// <summary>
        /// 仓库ID
        /// </summary>
        public int WarehouseId
        {
            get;
            set;
        }
    }
}
