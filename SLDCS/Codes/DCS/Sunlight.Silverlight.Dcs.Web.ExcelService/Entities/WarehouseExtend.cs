﻿using System;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class WarehouseExtend {
        /// <summary>
        /// 错误信息 
        /// </summary>
        public string ErrorMsg {
            get;
            set;
        }
        /// <summary>
        /// Id
        /// </summary>
        public int Id {
            get;
            set;
        }
        /// <summary>
        /// Id_Str
        /// </summary>
        public string IdStr {
            get;
            set;
        }

        /// <summary>
        /// 仓库编号
        /// </summary>
        public string Code {
            get;
            set;
        }
        /// <summary>
        /// 仓库编号_Str
        /// </summary>
        public string CodeStr {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        public string Name {
            get;
            set;
        }
        /// <summary>
        /// 仓库名称_Str
        /// </summary>
        public string NameStr {
            get;
            set;
        }

        /// <summary>
        /// 仓库类型
        /// </summary>
        public int Type {
            get;
            set;
        }
        /// <summary>
        /// 仓库类型_Str
        /// </summary>
        public string TypeStr {
            get;
            set;
        }

        /// <summary>
        /// 状态
        /// </summary>
        public int Status {
            get;
            set;
        }
        /// <summary>
        /// 状态_Str
        /// </summary>
        public string StatusStr {
            get;
            set;
        }

        /// <summary>
        /// 地址
        /// </summary>
        public string Address {
            get;
            set;
        }
        /// <summary>
        /// 地址_Str
        /// </summary>
        public string AddressStr {
            get;
            set;
        }

        /// <summary>
        /// 区省市Id
        /// </summary>
        public int? RegionId {
            get;
            set;
        }
        /// <summary>
        /// 区省市Id_Str
        /// </summary>
        public string RegionIdStr {
            get;
            set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string PhoneNumber {
            get;
            set;
        }
        /// <summary>
        /// 联系电话_Str
        /// </summary>
        public string PhoneNumberStr {
            get;
            set;
        }

        /// <summary>
        /// 联系人
        /// </summary>
        public string Contact {
            get;
            set;
        }
        /// <summary>
        /// 联系人_Str
        /// </summary>
        public string ContactStr {
            get;
            set;
        }

        /// <summary>
        /// 传真
        /// </summary>
        public string Fax {
            get;
            set;
        }
        /// <summary>
        /// 传真_Str
        /// </summary>
        public string FaxStr {
            get;
            set;
        }

        /// <summary>
        /// 电子邮件
        /// </summary>
        public string Email {
            get;
            set;
        }
        /// <summary>
        /// 电子邮件_Str
        /// </summary>
        public string EmailStr {
            get;
            set;
        }

        /// <summary>
        /// 存储策略
        /// </summary>
        public int StorageStrategy {
            get;
            set;
        }
        /// <summary>
        /// 存储策略_Str
        /// </summary>
        public string StorageStrategyStr {
            get;
            set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {
            get;
            set;
        }
        /// <summary>
        /// 备注_Str
        /// </summary>
        public string RemarkStr {
            get;
            set;
        }

        /// <summary>
        /// 图片
        /// </summary>
        public string Picture {
            get;
            set;
        }
        /// <summary>
        /// 图片_Str
        /// </summary>
        public string PictureStr {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }
        /// <summary>
        /// 营销分公司Id_Str
        /// </summary>
        public string BranchIdStr {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业Id
        /// </summary>
        public int StorageCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业Id_Str
        /// </summary>
        public string StorageCompanyIdStr {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业类型
        /// </summary>
        public int StorageCompanyType {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业类型_Str
        /// </summary>
        public string StorageCompanyTypeStr {
            get;
            set;
        }

        /// <summary>
        /// 储运中心
        /// </summary>
        public int? StorageCenter {
            get;
            set;
        }
        /// <summary>
        /// 储运中心_Str
        /// </summary>
        public string StorageCenterStr {
            get;
            set;
        }

        public bool IsCentralizedPurchase {
            get;
            set;
        }

        /// <summary>
        /// 与WMS接口
        /// </summary>
        public bool WmsInterface {
            get;
            set;
        }
        /// <summary>
        /// 与WMS接口_Str
        /// </summary>
        public string WmsInterfaceStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人Id
        /// </summary>
        public int? CreatorId {
            get;
            set;
        }
        /// <summary>
        /// 创建人Id_Str
        /// </summary>
        public string CreatorIdStr {
            get;
            set;
        }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreatorName {
            get;
            set;
        }
        /// <summary>
        /// 创建人_Str
        /// </summary>
        public string CreatorNameStr {
            get;
            set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime {
            get;
            set;
        }
        /// <summary>
        /// 创建时间_Str
        /// </summary>
        public string CreateTimeStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人Id
        /// </summary>
        public int? ModifierId {
            get;
            set;
        }
        /// <summary>
        /// 修改人Id_Str
        /// </summary>
        public string ModifierIdStr {
            get;
            set;
        }

        /// <summary>
        /// 修改人
        /// </summary>
        public string ModifierName {
            get;
            set;
        }
        /// <summary>
        /// 修改人_Str
        /// </summary>
        public string ModifierNameStr {
            get;
            set;
        }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? ModifyTime {
            get;
            set;
        }
        /// <summary>
        /// 修改时间_Str
        /// </summary>
        public string ModifyTimeStr {
            get;
            set;
        }

        /// <summary>
        /// RowVersion
        /// </summary>
        public DateTime? RowVersion {
            get;
            set;
        }
        /// <summary>
        /// RowVersion_Str
        /// </summary>
        public string RowVersionStr {
            get;
            set;
        }

        public string PartssalescategoryName {
            get;
            set;
        }

        public int PartssalescategoryId {
            get;
            set;
        }

    }
}
