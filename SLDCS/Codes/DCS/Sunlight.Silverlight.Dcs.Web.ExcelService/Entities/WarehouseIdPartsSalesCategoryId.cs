﻿namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    class WarehouseIdPartsSalesCategoryId {
        public int PartsSalesCategoryId {
            get;
            set;
        }

        public int WarehouseId {
            get;
            set;
        }
    }
}
