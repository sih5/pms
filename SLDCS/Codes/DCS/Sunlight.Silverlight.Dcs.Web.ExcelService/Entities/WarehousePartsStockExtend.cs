﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class WarehousePartsStockExtend {
        /// <summary>
        /// 供应商Id
        /// </summary>
        public int SupplierId {
            get;
            set;
        }
        /// <summary>
        /// 仓库Id
        /// </summary>
        public int WarehouseId {
            get;
            set;
        }

        /// <summary>
        /// 配件Id
        /// </summary>
        public int SparePartId {
            get;
            set;
        }

        /// <summary>
        /// 仓库编号
        /// </summary>
        public string WarehouseCode {
            get;
            set;
        }

        /// <summary>
        /// 仓库名称
        /// </summary>
        public string WarehouseName {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业Id
        /// </summary>
        public int StorageCompanyId {
            get;
            set;
        }
        /// <summary>
        /// 仓储企业编号
        /// </summary>
        public string StorageCompanyCode {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业名称
        /// </summary>
        public string StorageCompanyName {
            get;
            set;
        }

        /// <summary>
        /// 仓储企业类型
        /// </summary>
        public int StorageCompanyType {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司Id
        /// </summary>
        public int BranchId {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司编号
        /// </summary>
        public string BranchCode {
            get;
            set;
        }

        /// <summary>
        /// 营销分公司名称
        /// </summary>
        public string BranchName {
            get;
            set;
        }

        /// <summary>
        ///  配件编号
        /// </summary>
        public string SparePartCode {
            get;
            set;
        }

        /// <summary>
        ///  配件名称
        /// </summary>
        public string SparePartName {
            get;
            set;
        }

        /// <summary>
        ///  库存数量
        /// </summary>
        public int Quantity {
            get;
            set;
        }

        /// <summary>
        ///  可用库存
        /// </summary>
        public int UsableQuantity {
            get;
            set;
        }

        /// <summary>
        /// 配件销售价格
        /// </summary>
        public decimal PartsSalesPrice {
            get;
            set;
        }
        /// <summary>
        /// 替换件Id
        /// </summary>
        public int NewPartId {
            get;
            set;
        }

        /// <summary>
        /// 替换类型
        /// </summary>
        public int ReplacementType {
            get;
            set;
        }

        /// <summary>
        /// 替换件编号
        /// </summary>
        public string NewPartCode {
            get;
            set;
        }

        /// <summary>
        /// 替换件名称
        /// </summary>
        public string NewPartName {
            get;
            set;
        }

        /// <summary>
        /// 配件采购价
        /// </summary>
        public decimal PartsPurchasePricing {
            get;
            set;
        }

        /// <summary>
        ///  配件计划价
        /// </summary>
        public decimal PartsPlannedPrice {
            get;
            set;
        }

        /// <summary>
        /// 零售指导价
        /// </summary>
        public decimal PartsRetailGuidePrice {
            get;
            set;
        }
        
        /// <summary>
        /// 品牌ID缓存
        /// </summary>
        public int? PartsSalesCategoryId {
            get;
            set;
        }

    }
}
