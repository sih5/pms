﻿
namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public class WarehouseSequenceExtend {

        //客户编号,客户名称,品牌,是否参与自动审核,是否参与自动审核,首选仓库,首选仓库出库仓库,第一仓库,第一仓库出库仓库,第二仓库,第二仓库出库仓库,第三仓库,第三仓库出库仓库				



        public int Id {
            get;
            set;
        }
        public int Status {
            get;
            set;
        }

        public bool IsAutoApprove {
            get;
            set;
        }

        public string IsAutoApproveStr {
            get;
            set;
        }

        public int PartsSalesCategoryId {
            get;
            set;
        }

        public string PartsSalesCategoryName {
            get;
            set;
        }

        public string PartsSalesCategoryNameStr {
            get;
            set;
        }

        public int CustomerCompanyId {
            get;
            set;
        }

        public string CustomerCompanyCode {
            get;
            set;
        }

        public string CustomerCompanyCodeStr {
            get;
            set;
        }

        public string CustomerCompanyName {
            get;
            set;
        }

        public string CustomerCompanyNameStr {
            get;
            set;
        }

        public int DefaultWarehouseId {
            get;
            set;
        }

        public string DefaultWarehouseName {
            get;
            set;
        }

        public string DefaultWarehouseNameStr {
            get;
            set;
        }

        public bool DefaultIsCentralizedPurchase {
            get;
            set;
        }

        public int DefaultStorageCenter {
            get;
            set;
        }

        public int DefaultOutStorageCenter {
            get;
            set;
        }

        public int DefaultOutWarehouseId {
            get;
            set;
        }

        public string DefaultOutWarehouseName {
            get;
            set;
        }

        public string DefaultOutWarehouseNameStr {
            get;
            set;
        }

        public int FirstWarehouseId {
            get;
            set;
        }

        public string FirstWarehouseName {
            get;
            set;
        }

        public string FirstWarehouseNameStr {
            get;
            set;
        }

        public bool FirstIsCentralizedPurchase {
            get;
            set;
        }

        public int FirstStorageCenter {
            get;
            set;
        }

        public int FirstOutStorageCenter {
            get;
            set;
        }

        public int FirstOutWarehouseId {
            get;
            set;
        }

        public string FirstOutWarehouseName {
            get;
            set;
        }

        public string FirstOutWarehouseNameStr {
            get;
            set;
        }

        public int SecoundWarehouseId {
            get;
            set;
        }

        public string SecoundWarehouseName {
            get;
            set;
        }

        public string SecoundWarehouseNameStr {
            get;
            set;
        }

        public bool SecoundIsCentralizedPurchase {
            get;
            set;
        }

        public int SecoundStorageCenter {
            get;
            set;
        }

        public int SecoundOutStorageCenter {
            get;
            set;
        }

        public int SecoundOutWarehouseId {
            get;
            set;
        }

        public string SecoundOutWarehouseName {
            get;
            set;
        }

        public string SecoundOutWarehouseNameStr {
            get;
            set;
        }

        public int ThirdWarehouseId {
            get;
            set;
        }

        public string ThirdWarehouseName {
            get;
            set;
        }

        public string ThirdWarehouseNameStr {
            get;
            set;
        }

        public bool ThirdIsCentralizedPurchase {
            get;
            set;
        }

        public int ThirdStorageCenter {
            get;
            set;
        }

        public int ThirdOutStorageCenter {
            get;
            set;
        }

        public int ThirdOutWarehouseId {
            get;
            set;
        }

        public string ThirdOutWarehouseName {
            get;
            set;
        }

        public string ThirdOutWarehouseNameStr {
            get;
            set;
        }

        public string ErrorMsg {
            get;
            set;
        }
    }
}
