﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public static class GetTableName {
        public static object[] GetRegexNames(this string strs) {
            var regexName = new Regex(@"/\*(?<name>.*?)\*/");
            var matchName = regexName.Matches(strs);
            var names = (from Match name in matchName
                         select (object)name.Groups["name"].Value).ToArray();
            return names;
        }
    }
}
