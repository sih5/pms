﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出服务产品线与产品关系
        /// </summary>
        /// <param name="vehicleBrandCode">整车品牌编号</param>
        /// <param name="vehicleBrandName">整车品牌名称</param>
        /// <param name="subBrandCode">子品牌编号</param>
        /// <param name="subBrandName">子品牌名称</param>
        /// <param name="terraceCode">平台代码</param>
        /// <param name="terraceName">平台名称</param>
        /// <param name="vehicleProductLine">整车产品线编号</param>
        /// <param name="vehicleProductName">整车产品线名称</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool ExportServiceProdLineProduct(string vehicleBrandCode, string vehicleBrandName, string subBrandCode, string subBrandName, string terraceCode, string terraceName, string vehicleProductLine, string vehicleProductName, out string fileName) {
            fileName = GetExportFilePath("服务产品线与产品关系_" + ".xlsx");
            try {
                var userinfo = Utils.GetCurrentUserInfo();
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.Export_Validation_Dealer_Validation52);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();

                    sql.Append(string.Format(@" select a.BrandId,
                                         a.VehicleBrandCode,
                                         a.VehicleBrandName, 
                                         a.SubBrandCode, 
                                         a.SubBrandName,
                                         a.TerraceCode,
                                         a.TerraceName,                                         
                                         a.VehicleProductLine, 
                                         a.VehicleProductName,
                                         a.ProductFunctionCode, 
                                         a.SalesCategoryCode,
                                         a.SalesCategoryName,
                                         a.ServiceProductLineCode, 
                                         a.ServiceProductLineName
                                 from BranchVehicleBrand x
                                 inner join ServiceProdLineProduct a 
                                 on x.BrandCode = a.VehicleBrandCode
                                 left join PersonSalesCenterLink b
                                 on b.PartsSalesCategoryId = a.PartsSalesCategoryId 
                                 and b.PersonId = {0}
                                 and b.Status = 1 
                                 where x.BranchId = {1}  
                                  ", userinfo.Id, userinfo.EnterpriseId));
                    var dbParameters = new List<DbParameter>();
                    if(!string.IsNullOrEmpty(vehicleBrandCode)) {
                        sql.Append(@" and a.vehicleBrandCode like {0}vehicleBrandCode ");
                        dbParameters.Add(db.CreateDbParameter("vehicleBrandCode", '%' + vehicleBrandCode + '%'));
                    }
                    if(!string.IsNullOrEmpty(vehicleBrandName)) {
                        sql.Append(@" and a.vehicleBrandName like {0}vehicleBrandName ");
                        dbParameters.Add(db.CreateDbParameter("vehicleBrandName", '%' + vehicleBrandName + '%'));
                    }
                    if(!string.IsNullOrEmpty(subBrandCode)) {
                        sql.Append(@" and a.subBrandCode like {0}subBrandCode ");
                        dbParameters.Add(db.CreateDbParameter("subBrandCode", '%' + subBrandCode + '%'));
                    }
                    if(!string.IsNullOrEmpty(subBrandName)) {
                        sql.Append(@" and a.subBrandName like {0}subBrandName ");
                        dbParameters.Add(db.CreateDbParameter("subBrandName", '%' + subBrandName + '%'));
                    }
                    if(!string.IsNullOrEmpty(terraceCode)) {
                        sql.Append(@" and a.terraceCode like {0}terraceCode ");
                        dbParameters.Add(db.CreateDbParameter("terraceCode", '%' + terraceCode + '%'));
                    }
                    if(!string.IsNullOrEmpty(terraceName)) {
                        sql.Append(@" and a.terraceName like {0}terraceName ");
                        dbParameters.Add(db.CreateDbParameter("terraceName", '%' + terraceName + '%'));
                    }
                    if(!string.IsNullOrEmpty(vehicleProductLine)) {
                        sql.Append(@" and a.vehicleProductLine like {0}vehicleProductLine ");
                        dbParameters.Add(db.CreateDbParameter("vehicleProductLine", '%' + vehicleProductLine + '%'));
                    }
                    if(!string.IsNullOrEmpty(vehicleProductName)) {
                        sql.Append(@" and a.vehicleProductName like {0}vehicleProductName ");
                        dbParameters.Add(db.CreateDbParameter("vehicleProductName", '%' + vehicleProductName + '%'));
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "品牌Id", "整车品牌编号", "整车品牌名称","子品牌编号", "子品牌名称","平台代码", "平台名称","整车产品线编号", "整车产品线名称","产品功能","服务品牌编号", "服务品牌名称","服务产品线编号", "服务产品线名称"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}