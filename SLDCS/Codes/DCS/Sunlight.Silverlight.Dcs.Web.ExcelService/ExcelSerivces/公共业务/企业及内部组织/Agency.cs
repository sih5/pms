﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Data.Common;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出代理库详细信息
        /// </summary>
        public bool ExportAgencyDetail(string name, string code, int? status, int? agencyId, out string fileName) {
            fileName = GetExportFilePath("代理商代理库详细信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception("身份认证失效，请登录系统后再次尝试");
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"SELECT a.Code,--代理库.代理库编号
                                        a.Name,--代理库.代理库名称
                                        a.ShortName,--代理库.简称
                                        c.name, --分销中心名称
                                         wh.name,--订货仓库
                                        b.ProvinceName,--企业信息.省
                                        b.CityName,--企业信息.市
                                        b.CountyName,--企业信息.县
                                        b.CustomerCode,--企业信息.客户编码
                                        b.SupplierCode,--企业信息.供应商编码
                                        b.FoundDate,--企业信息.成立日期
                                        b.ContactPerson,--企业信息.联系人
                                        b.ContactMobile,--企业信息.联系人手机
                                        b.ContactPhone,--企业信息.联系电话
                                        b.BusinessLinkName,--企业信息.联系人2
                                        b.BusinessContactMethod,--企业信息.联系人2手机
                                        b.Fax,--企业信息.传真
                                        b.ContactAddress,--企业信息.联系地址
                                        b.ContactMail,--企业信息.电子邮箱
                                        b.RegisterCode,--企业信息.注册证号
                                        b.RegisterName,--企业信息.注册名称
                                        (select value from keyvalueitem where NAME = 'Corporate_Nature'and key=b.CorporateNature) As CorporateNature,--企业信息.企业性质
                                        b.LegalRepresentative,--企业信息.法人代表
                                        (select value from keyvalueitem where NAME = 'Customer_IdDocumentType'and key=b.IdDocumentType) As IdDocumentType,--企业信息.身份证件类型
                                        b.IdDocumentNumber,--企业信息.身份证件号码
                                        b.RegisterCapital,--企业信息.注册资本
                                        b.RegisterDate,--企业信息.注册日期
                                        b.BusinessScope,--企业信息.经营范围
                                        b.BusinessAddress,--企业信息.营业地址
                                        b.RegisteredAddress,--企业信息.注册地址
                                        b.Remark--企业信息.备注
                                        FROM Agency a
                                        INNER JOIN Company b ON a.id = b.id left join Warehouse wh on a.WarehouseId =wh.id
                                        LEFT JOIN MarketingDepartment c on c.id = a.MarketingDepartmentId where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(!string.IsNullOrEmpty(name)) {
                        sql.Append(@" and a.name like {0}name ");
                        dbParameters.Add(db.CreateDbParameter("name", "%" + name + "%"));
                    }
                    if(!string.IsNullOrEmpty(code)) {
                        sql.Append(@" and a.code like {0}code ");
                        dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and a.status ={0}status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    if(agencyId.HasValue) {
                        sql.Append(@" and a.Id ={0}agencyId");
                        dbParameters.Add(db.CreateDbParameter("agencyId", agencyId.Value));
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Agency_Code,ErrorStrings.Export_Title_Agency_Name,ErrorStrings.Export_Title_Agency_Abbreviation,ErrorStrings.Export_Title_Agency_MarketingDepartmentName,"订货仓库",
                                    ErrorStrings.Export_Title_Agency_Province,ErrorStrings.Export_Title_Agency_City,ErrorStrings.Export_Title_Agency_Country,ErrorStrings.Export_Title_Agency_CustomerCode,
                                    ErrorStrings.Export_Title_Agency_SupplierCode,ErrorStrings.Export_Title_Agency_FoundDate,ErrorStrings.Export_Title_Agency_LinkMan,ErrorStrings.Export_Title_Agency_LinkManPhone,ErrorStrings.Export_Title_Agency_LinkManMobile,
                                    ErrorStrings.Export_Title_Agency_LinkMan2,ErrorStrings.Export_Title_Agency_LinkManPhone2,ErrorStrings.Export_Title_Agency_Fax,
                                    ErrorStrings.Export_Title_Agency_Address,ErrorStrings.Export_Title_Agency_Email,ErrorStrings.Export_Title_Agency_RegistrationNo,ErrorStrings.Export_Title_Agency_RegistrationName,
                                    ErrorStrings.Export_Title_Agency_Nature,ErrorStrings.Export_Title_Agency_LegalRepresentative,ErrorStrings.Export_Title_Agency_IdDocumentType,
                                    ErrorStrings.Export_Title_Agency_IdDocumentNumber,ErrorStrings.Export_Title_Agency_RegisterCapital,ErrorStrings.Export_Title_Agency_RegisterDate,ErrorStrings.Export_Title_Agency_BusinessScope,
                                    ErrorStrings.Export_Title_Agency_BusinessAddress,ErrorStrings.Export_Title_Agency_RegisteredAddress,ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        /// <summary>
        /// 导入保底库存
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool Import中心库订货仓库(string fileName, out int excelImportNum, out List<AgencyExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<AgencyExtend>();
            var allList = new List<AgencyExtend>();
            var userInfo = Utils.GetCurrentUserInfo();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("Agency", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                List<AgencyExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource("中心库编号", "Code");
                    excelOperator.AddColumnDataSource("中心库名称", "Name");
                    excelOperator.AddColumnDataSource("订货仓库名称", "WarehouseName");
                   
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseData_Status", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new AgencyExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.CodeStr = newRow["Code"];
                        tempImportObj.NameStr = newRow["Name"];
                        tempImportObj.WarehouseNameStr = newRow["WarehouseName"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //中心库编号检查
                        var fieldIndex = notNullableFields.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("中心库编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CodeStr) > fieldLenght["Code".ToUpper()])
                                tempErrorMessage.Add("中心库编号过长");
                        }
                        //订货仓库名称检查
                        fieldIndex = notNullableFields.IndexOf("WarehouseName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.WarehouseNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("订货仓库不允许为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.WarehouseNameStr) > fieldLenght["WarehouseName".ToUpper()])
                                tempErrorMessage.Add("订货仓库名称过长");
                        }
                       
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //4、文件中品牌名称、人员名称、供应商名称组合唯一，否则提示：数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.CodeStr,
                        r.WarehouseNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、品牌名称存在于配件销售类型表中，否则提示：品牌名称不存在
                    //2、企业编号存在于企业表中，否则提示：企业编号不存在
                    //3、仓库编号存在于仓库表中，否则提示：仓库编号不存在
                    //4、配件编号存在于配件表中，否则提示：配件编号不存在
                    var CompanyCodesCheck = tempRightList.Select(r => r.CodeStr).Distinct().ToArray();
                    var WarehouseCodesNeedCheck = tempRightList.Select(r => r.WarehouseNameStr).Distinct().ToArray();
                    var dbCompanys = new List<AgencyExtend>();
                    var dbWarehouses = new List<WarehouseExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new AgencyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };                  
                    Func<string[], bool> getdbWarehouses = value => {
                        var dbObj = new WarehouseExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbWarehouses.Add(dbObj);
                        return false;
                    };


                    db.QueryDataWithInOperator("select Id,Code,Name  from Agency where status=1 ", "Code", false, CompanyCodesCheck, getDbCompanys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbCompanys.FirstOrDefault(v => v.Code == tempRight.CodeStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = "中心库编号不存在";
                            continue;
                        }
                        tempRight.Id = repairItem.Id;
                        tempRight.Name = repairItem.Name;
                        tempRight.Code = repairItem.Code;
                    }
                    db.QueryDataWithInOperator("select Id,Code,Name from Warehouse where status=1 ", "Name", false, WarehouseCodesNeedCheck, getdbWarehouses);
                    foreach(var tempRight in tempRightList) {

                                var repairItem = dbWarehouses.FirstOrDefault(v => v.Name == tempRight.WarehouseNameStr);
                                if(repairItem == null) {
                                    tempRight.ErrorMsg = "订货仓库不存在";
                                    continue;
                                }
                                tempRight.WarehouseId = repairItem.Id;
                                tempRight.WarehouseName = repairItem.Name;

                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值

                                tempObj.CodeStr,tempObj.NameStr,
                                tempObj.WarehouseNameStr,
                                tempObj.ErrorMsg
                                
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {


                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetUpdateSql("Agency", new[] {
                                "WarehouseId", "WarehouseName",  "ModifierId", "ModifierName", "ModifyTime"
                            }, new[] {
                                "Id"
                            });
                            #endregion
                            //往数据库增加市场部信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("WarehouseId", item.WarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("WarehouseName", item.WarehouseName));                                                             
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}
