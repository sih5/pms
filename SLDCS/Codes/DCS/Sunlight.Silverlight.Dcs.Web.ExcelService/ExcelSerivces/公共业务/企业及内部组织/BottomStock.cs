﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Data.Common;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public partial class ExcelService
    {
        /// <summary>
        /// 导出保底库存
        /// </summary>
        public bool ExportBottomStock(int[] ids, int? partsSalesCategoryId, int? companyType, string companyCode, string companyName, string sparePartCode, string sparePartName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, int? status, out string fileName)
        {
            fileName = GetExportFilePath("保底库存.xlsx");
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using (var conn = db.CreateDbConnection())
                {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select
                                   ps.name as PartsSalesCategoryName,
                                   bs.CompanyCode,
                                   c.name as CompanyName,
                                   (select value
                                      from keyvalueitem
                                     where NAME = 'Company_Type'
                                       and key = bs.CompanyType) As CompanyType,
                                   bs.WarehouseCode,
                                   w.name as WarehouseName,
                                   bs.SparePartCode,
                                   bs.SparePartName,
                                   bs.StockQty,
                                   (select value
                                      from keyvalueitem
                                     where NAME = 'BaseData_Status'
                                       and key = bs.Status) As Status,
                                   bs.CreatorName,
                                   bs.CreateTime,
                                   bs.ModifierName,
                                   bs.ModifyTime,
                                   bs.AbandonerName,
                                   bs.AbandonTime,
                                   bs.Remark
                              from BottomStock bs
                             inner join PartsSalesCategory ps
                                on ps.id = bs.partssalescategoryid
                             inner join Company c
                                on c.id = bs.companyid
                              left join Warehouse w
                                on w.id = bs.warehouseid
                                where 1=1 
                             ");
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and bs.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        #region 条件过滤
                        if (partsSalesCategoryId.HasValue)
                        {
                            sql.Append(" and bs.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId));
                        }
                        if(status.HasValue){
                            sql.Append(" and bs.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status));
                        }
                        if (companyType.HasValue)
                        {
                            sql.Append(" and bs.companyType = {0}companyType ");
                            dbParameters.Add(db.CreateDbParameter("companyType", companyType));
                        }
                        if (!string.IsNullOrEmpty(companyCode))
                        {
                            sql.Append(" and bs.CompanyCode = {0}CompanyCode ");
                            dbParameters.Add(db.CreateDbParameter("CompanyCode", companyCode));
                        }
                        if (!string.IsNullOrEmpty(companyName))
                        {
                            sql.Append(" and bs.companyName like {0}companyName ");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        if (!string.IsNullOrEmpty(sparePartCode))
                        {
                            var spareparts = sparePartCode.Split(',');
                            if (spareparts.Length == 1) {
                                sql.Append(" and bs.sparePartCode like {0}sparePartCode ");
                                dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + spareparts[0] + "%"));
                            } else {
                                for (int i = 0;i<spareparts.Length;i++) {
                                    spareparts[i] = "'" + spareparts[i] + "'";
                                }
                                sql.Append(" and bs.sparePartCode in (" + string.Join(",",spareparts) + ")");
                            }
                        }
                        if (!string.IsNullOrEmpty(sparePartName))
                        {
                            sql.Append(" and bs.sparePartName like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartName + "%"));
                        }
                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(@" and bs.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(@" and bs.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (modifyTimeBegin.HasValue)
                        {
                            sql.Append(@" and bs.ModifyTime >=to_date({0}modifyTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeBegin", tempTime.ToString("G")));
                        }
                        if (modifyTimeEnd.HasValue)
                        {
                            sql.Append(@" and bs.ModifyTime <=to_date({0}modifyTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeEnd", tempTime.ToString("G")));
                        }
                        #endregion
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                            {
                                return new object[] {
                                   ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Company_Code,ErrorStrings.Export_Title_Company_Name,ErrorStrings.Export_Title_Company_Type,ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_Company_WarehouseName, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_BottomStock_StockQty,ErrorStrings.Export_Title_BottomStock_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 导入保底库存
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportBottomStock(string fileName, out int excelImportNum, out List<BottomStockExtend> errorData, out string errorDataFileName, out string errorMessage)
        {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<BottomStockExtend>();
            var allList = new List<BottomStockExtend>();
            var userInfo = Utils.GetCurrentUserInfo();
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("BottomStock", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                List<BottomStockExtend> rightList;
                List<BottomStockExtend> DeleteList;//用于删除
                using (var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString))
                {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BottomStock_PartsSalesCategoryName, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Code, "CompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_WarehouseCode, "WarehouseCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BottomStock_StockQty, "StockQty");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseData_Status", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row =>
                    {
                        var tempImportObj = new BottomStockExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.CompanyCodeStr = newRow["CompanyCode"];
                        tempImportObj.WarehouseCodeStr = newRow["WarehouseCode"];
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.StockQtyStr = newRow["StockQty"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //品牌名称检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if (string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr))
                        {
                            if (fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //企业编号检查
                        fieldIndex = notNullableFields.IndexOf("CompanyCode".ToUpper());
                        if (string.IsNullOrEmpty(tempImportObj.CompanyCodeStr))
                        {
                            if (fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsNull);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.CompanyCodeStr) > fieldLenght["CompanyCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsLong);
                        }
                        //配件编号检查
                        fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if (string.IsNullOrEmpty(tempImportObj.SparePartCodeStr))
                        {
                            if (fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }
                        //保底库存检查
                        fieldIndex = notNullableFields.IndexOf("StockQty".ToUpper());
                        if (string.IsNullOrEmpty(tempImportObj.StockQtyStr))
                        {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_BottomStock_StockQtyIsNull);
                        }
                        else
                        {
                            if (int.Parse(tempImportObj.StockQtyStr)<=0)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_BottomStock_StockQtyOverZero);
                        }
                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if (!string.IsNullOrEmpty(tempImportObj.RemarkStr))
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion
                        if (tempErrorMessage.Count > 0)
                        {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //4、文件中品牌名称、人员名称、供应商名称组合唯一，否则提示：数据重复
                    var groups = tempRightList.GroupBy(r => new
                    {
                        r.PartsSalesCategoryNameStr,
                        r.CompanyCodeStr,
                        r.WarehouseCodeStr,
                        r.SparePartCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach (var groupItem in groups.SelectMany(r => r))
                    {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、品牌名称存在于配件销售类型表中，否则提示：品牌名称不存在
                    //2、企业编号存在于企业表中，否则提示：企业编号不存在
                    //3、仓库编号存在于仓库表中，否则提示：仓库编号不存在
                    //4、配件编号存在于配件表中，否则提示：配件编号不存在
                    var PartsSalesCategoryNamesNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var CompanyCodesCheck = tempRightList.Select(r => r.CompanyCodeStr).Distinct().ToArray();
                    var WarehouseCodesNeedCheck = tempRightList.Select(r => r.WarehouseCodeStr).Distinct().ToArray();
                    var SparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var dbCompanys = new List<CompanyExtend>();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    var dbWarehouses = new List<WarehouseExtend>();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbCompanys = value =>
                    {
                        var dbObj = new CompanyExtend
                        {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            Type= Convert.ToInt32(value[3])
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };
                    Func<string[], bool> getdbPartsSalesCategorys = value =>
                    {
                        var dbObj = new PartsSalesCategoryExtend
                        {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    Func<string[], bool> getdbWarehouses = value =>
                    {
                        var dbObj = new WarehouseExtend
                        {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbWarehouses.Add(dbObj);
                        return false;
                    };
                    Func<string[], bool> getdbSpareParts = value =>
                    {
                        var dbObj = new SparePartExtend
                        {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from PartsSalesCategory where status=1 ", "Name", false, PartsSalesCategoryNamesNeedCheck, getdbPartsSalesCategorys);
                    foreach (var tempRight in tempRightList)
                    {
                        var repairItem = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr);
                        if (repairItem == null)
                        {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_BottomStock_PartsSalesCategoryNameIsNull;
                            continue;
                        }
                        tempRight.PartsSalesCategoryId = repairItem.Id;
                    }
                    db.QueryDataWithInOperator("select Id,Code,Name,Type from Company where status=1 ", "Code", false, CompanyCodesCheck, getDbCompanys);
                    foreach (var tempRight in tempRightList)
                    {
                        var repairItem = dbCompanys.FirstOrDefault(v => v.Code == tempRight.CompanyCodeStr);
                        if (repairItem == null)
                        {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_Company_CodeNotExist;
                            continue;
                        }
                        tempRight.CompanyID = repairItem.Id;
                        tempRight.CompanyName = repairItem.Name;
                        tempRight.CompanyType = repairItem.Type;
                    }
                    db.QueryDataWithInOperator("select Id,Code,Name from Warehouse where status=1 ", "Code", false, WarehouseCodesNeedCheck, getdbWarehouses);
                    foreach (var tempRight in tempRightList)
                    {
                        //不是服务站企业,仓库信息必填
                        if ((tempRight.CompanyType != 2))
                        {
                            if (string.IsNullOrEmpty(tempRight.WarehouseCodeStr))
                            {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation14;
                                continue;
                            }
                            else
                            {
                                var repairItem = dbWarehouses.FirstOrDefault(v => v.Code == tempRight.WarehouseCodeStr);
                                if (repairItem == null)
                                {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_Company_WarehouseCodeIsNull;
                                    continue;
                                }
                                tempRight.WarehouseID = repairItem.Id;
                                tempRight.WarehouseName = repairItem.Name;
                            }
                            
                        }
                        else if ((tempRight.CompanyType == 2) && (!string.IsNullOrEmpty(tempRight.WarehouseCodeStr)))
                        {
                            var repairItem = dbWarehouses.FirstOrDefault(v => v.Code == tempRight.WarehouseCodeStr);
                            if (repairItem == null)
                            {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_Company_WarehouseCodeIsNull;
                                continue;
                            }
                            tempRight.WarehouseID = repairItem.Id;
                            tempRight.WarehouseName = repairItem.Name;
                        }
                        
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();


                    db.QueryDataWithInOperator("select Id,Code,Name from SparePart where status=1 ", "Code", false, SparePartCodesNeedCheck, getdbSpareParts);
                    foreach (var tempRight in tempRightList)
                    {
                        var repairItem = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.SparePartCodeStr);
                        if (repairItem == null)
                        {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation15;
                            continue;
                        }
                        tempRight.SparePartId = repairItem.Id;
                        tempRight.SparePartName = repairItem.Name;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //3、系统中品牌、企业编号、仓库编号、配件编号组合唯一，否则提法：已存在保底库存
                    var PartsSalesCategoryIdsNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbBottomStocks = new List<BottomStockExtend>();
                    Func<string[], bool> getDbBottomStocks = value =>
                    {
                        if(string.IsNullOrEmpty(value[2])){
                            value[2] = null;
                        }
                        var dbObj = new BottomStockExtend
                        {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            CompanyID = Convert.ToInt32(value[1]),
                            WarehouseID = Convert.ToInt32(value[2]),
                            SparePartId = Convert.ToInt32(value[3])
                        };
                        if (dbObj.WarehouseID == 0)
                            dbObj.WarehouseID = null;
                        dbBottomStocks.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select PartsSalesCategoryId,CompanyID,WarehouseID,SparePartId from BottomStock where status =1 ", "PartsSalesCategoryId", false, PartsSalesCategoryIdsNeedCheck, getDbBottomStocks);

                    //获取所有合格数据
                    DeleteList = new List<BottomStockExtend>();
                    foreach (var tempRight in tempRightList)
                    {
                        if (tempRight.WarehouseID == 0) 
                            tempRight.WarehouseID = null;
                        //已存在的删除后重新插入
                        var usedPartsDistanceInfor = dbBottomStocks.FirstOrDefault(v => v.PartsSalesCategoryId == tempRight.PartsSalesCategoryId && v.CompanyID == tempRight.CompanyID && v.WarehouseID == tempRight.WarehouseID && v.SparePartId == tempRight.SparePartId);
                        if (usedPartsDistanceInfor != null)
                        {
                            tempRight.PartsSalesCategoryId = usedPartsDistanceInfor.PartsSalesCategoryId;
                            tempRight.CompanyID = usedPartsDistanceInfor.CompanyID;
                            tempRight.WarehouseID = usedPartsDistanceInfor.WarehouseID;
                            tempRight.SparePartId = usedPartsDistanceInfor.SparePartId;
                            DeleteList.Add(tempRight);
                        }
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach (var rightItem in rightList)
                    {
                        rightItem.PartsSalesCategoryName = rightItem.PartsSalesCategoryNameStr;
                        rightItem.CompanyCode = rightItem.CompanyCodeStr;
                        rightItem.WarehouseCode = rightItem.WarehouseCodeStr;
                        rightItem.SparePartCode = rightItem.SparePartCodeStr;
                        rightItem.StockQty =Convert.ToInt32(rightItem.StockQtyStr);
                        rightItem.Remark = rightItem.RemarkStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseData_Status", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if (errorList.Any())
                {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using (var excelExport = new ExcelExport(errorDataFileName))
                    {
                        var list = errorList;
                        excelExport.ExportByRow(index =>
                        {
                            if (index == list.Count + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值

                                tempObj.PartsSalesCategoryNameStr,tempObj.CompanyCodeStr,
                                tempObj.WarehouseCodeStr,tempObj.SparePartCodeStr,tempObj.StockQtyStr, tempObj.RemarkStr,
                                tempObj.ErrorMsg
                                
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if (!rightList.Any())
                    return true;
                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try
                    {

                        //删除已存在的数据
                        var deleteList1 = DeleteList.Where(r => r.WarehouseID != null).ToList();
                        if (deleteList1.Any())
                        {
                            string sqlDelete = "Delete from BottomStock where PartsSalesCategoryId =:PartsSalesCategoryId and CompanyID=:CompanyID and WarehouseID=:WarehouseID and SparePartId=:SparePartId";
                            foreach (var item in deleteList1)
                            {
                                var command = db.CreateDbCommand(sqlDelete, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("CompanyID", item.CompanyID));
                                command.Parameters.Add(db.CreateDbParameter("WarehouseID", item.WarehouseID));
                                command.Parameters.Add(db.CreateDbParameter("SparePartId", item.SparePartId));
                                command.ExecuteScalar();
                            }
                        }
                        //删除已存在的数据
                        var deleteList2 = DeleteList.Where(r => r.WarehouseID == null).ToList();
                        if (deleteList2.Any())
                        {
                            string sqlDelete = "Delete from BottomStock where PartsSalesCategoryId =:PartsSalesCategoryId and CompanyID=:CompanyID and WarehouseID is null and SparePartId=:SparePartId";
                            foreach (var item in deleteList2)
                            {
                                var command = db.CreateDbCommand(sqlDelete, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("CompanyID", item.CompanyID));
                                command.Parameters.Add(db.CreateDbParameter("SparePartId", item.SparePartId));
                                command.ExecuteScalar();
                            }
                        }

                        //新增配件
                        if (rightList.Any())
                        {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("BottomStock", "Id", new[] {
                                "PartsSalesCategoryId","PartsSalesCategoryName", "CompanyID", "CompanyCode","CompanyName","CompanyType", "WarehouseID",  "WarehouseCode","WarehouseName", "SparePartId",  "SparePartCode","SparePartName","StockQty", "CreatorId", "CreatorName", "CreateTime","ModifierId","ModifierName","ModifyTime","Status","Remark"
                            });
                            #endregion
                            //往数据库增加市场部信息
                            foreach (var item in rightList)
                            {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSalesCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("CompanyID", item.CompanyID));
                                command.Parameters.Add(db.CreateDbParameter("CompanyCode", item.CompanyCode));
                                command.Parameters.Add(db.CreateDbParameter("CompanyName", item.CompanyName));
                                command.Parameters.Add(db.CreateDbParameter("CompanyType", item.CompanyType));
                                command.Parameters.Add(db.CreateDbParameter("WarehouseID", item.WarehouseID));
                                command.Parameters.Add(db.CreateDbParameter("WarehouseCode", item.WarehouseCode));
                                command.Parameters.Add(db.CreateDbParameter("WarehouseName", item.WarehouseName));
                                command.Parameters.Add(db.CreateDbParameter("SparePartId", item.SparePartId));
                                command.Parameters.Add(db.CreateDbParameter("SparePartCode", item.SparePartCode));
                                command.Parameters.Add(db.CreateDbParameter("SparePartName", item.SparePartName));
                                command.Parameters.Add(db.CreateDbParameter("StockQty", item.StockQty));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    }
                    catch (Exception ex)
                    {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        if (conn.State == System.Data.ConnectionState.Open)
                        {
                            conn.Close();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            }
            finally
            {
                errorData = errorList;
            }
        }
        /// <summary>
        /// 导入保底库存作废
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportBottomStockAbandon(string fileName, out int excelImportNum, out List<BottomStockExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<BottomStockExtend>();
            var allList = new List<BottomStockExtend>();
            var userInfo = Utils.GetCurrentUserInfo();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("BottomStock", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                List<BottomStockExtend> rightList;
                List<BottomStockExtend> DeleteList;//用于删除
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BottomStock_PartsSalesCategoryName, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Code, "CompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_WarehouseCode, "WarehouseCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseData_Status", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new BottomStockExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.CompanyCodeStr = newRow["CompanyCode"];
                        tempImportObj.WarehouseCodeStr = newRow["WarehouseCode"];
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //品牌名称检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //企业编号检查
                        fieldIndex = notNullableFields.IndexOf("CompanyCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CompanyCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CompanyCodeStr) > fieldLenght["CompanyCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsLong);
                        }
                        //配件编号检查
                        fieldIndex = notNullableFields.IndexOf("SparePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }                      
                      
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //4、文件中品牌名称、人员名称、供应商名称组合唯一，否则提示：数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.PartsSalesCategoryNameStr,
                        r.CompanyCodeStr,
                        r.WarehouseCodeStr,
                        r.SparePartCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、品牌名称存在于配件销售类型表中，否则提示：品牌名称不存在
                    //2、企业编号存在于企业表中，否则提示：企业编号不存在
                    //3、仓库编号存在于仓库表中，否则提示：仓库编号不存在
                    //4、配件编号存在于配件表中，否则提示：配件编号不存在
                    var PartsSalesCategoryNamesNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var CompanyCodesCheck = tempRightList.Select(r => r.CompanyCodeStr).Distinct().ToArray();
                    var WarehouseCodesNeedCheck = tempRightList.Select(r => r.WarehouseCodeStr).Distinct().ToArray();
                    var SparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var dbCompanys = new List<CompanyExtend>();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    var dbWarehouses = new List<WarehouseExtend>();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            Type = Convert.ToInt32(value[3])
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };
                    Func<string[], bool> getdbPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    Func<string[], bool> getdbWarehouses = value => {
                        var dbObj = new WarehouseExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbWarehouses.Add(dbObj);
                        return false;
                    };
                    Func<string[], bool> getdbSpareParts = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from PartsSalesCategory where status=1 ", "Name", false, PartsSalesCategoryNamesNeedCheck, getdbPartsSalesCategorys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_BottomStock_PartsSalesCategoryNameIsNull;
                            continue;
                        }
                        tempRight.PartsSalesCategoryId = repairItem.Id;
                    }
                    db.QueryDataWithInOperator("select Id,Code,Name,Type from Company where status=1 ", "Code", false, CompanyCodesCheck, getDbCompanys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbCompanys.FirstOrDefault(v => v.Code == tempRight.CompanyCodeStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_Company_CodeNotExist;
                            continue;
                        }
                        tempRight.CompanyID = repairItem.Id;
                        tempRight.CompanyName = repairItem.Name;
                        tempRight.CompanyType = repairItem.Type;
                    }
                    db.QueryDataWithInOperator("select Id,Code,Name from Warehouse where status=1 ", "Code", false, WarehouseCodesNeedCheck, getdbWarehouses);
                    foreach(var tempRight in tempRightList) {
                        //不是服务站企业,仓库信息必填
                        if((tempRight.CompanyType != 2)) {
                            if(string.IsNullOrEmpty(tempRight.WarehouseCodeStr)) {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation14;
                                continue;
                            } else {
                                var repairItem = dbWarehouses.FirstOrDefault(v => v.Code == tempRight.WarehouseCodeStr);
                                if(repairItem == null) {
                                    tempRight.ErrorMsg = ErrorStrings.Export_Validation_Company_WarehouseCodeIsNull;
                                    continue;
                                }
                                tempRight.WarehouseID = repairItem.Id;
                                tempRight.WarehouseName = repairItem.Name;
                            }

                        } else if((tempRight.CompanyType == 2) && (!string.IsNullOrEmpty(tempRight.WarehouseCodeStr))) {
                            var repairItem = dbWarehouses.FirstOrDefault(v => v.Code == tempRight.WarehouseCodeStr);
                            if(repairItem == null) {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_Company_WarehouseCodeIsNull;
                                continue;
                            }
                            tempRight.WarehouseID = repairItem.Id;
                            tempRight.WarehouseName = repairItem.Name;
                        }

                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();


                    db.QueryDataWithInOperator("select Id,Code,Name from SparePart where status=1 ", "Code", false, SparePartCodesNeedCheck, getdbSpareParts);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.SparePartCodeStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PartsBranch_Validation15;
                            continue;
                        }
                        tempRight.SparePartId = repairItem.Id;
                        tempRight.SparePartName = repairItem.Name;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //3、系统中品牌、企业编号、仓库编号、配件编号组合唯一，否则提法：已存在保底库存
                    var PartsSalesCategoryIdsNeedCheck = tempRightList.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbBottomStocks = new List<BottomStockExtend>();
                    Func<string[], bool> getDbBottomStocks = value => {
                        if(string.IsNullOrEmpty(value[2])) {
                            value[2] = null;
                        }
                        var dbObj = new BottomStockExtend {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            CompanyID = Convert.ToInt32(value[1]),
                            WarehouseID = Convert.ToInt32(value[2]),
                            SparePartId = Convert.ToInt32(value[3]),
                            StockQty = Convert.ToInt32(value[4]),
                            Id = Convert.ToInt32(value[5]),
                        };
                        if(dbObj.WarehouseID == 0)
                            dbObj.WarehouseID = null;
                        dbBottomStocks.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select  PartsSalesCategoryId,CompanyID,WarehouseID,SparePartId,StockQty,Id from BottomStock where status =1 ", "SparePartId", false, PartsSalesCategoryIdsNeedCheck, getDbBottomStocks);

                    //获取所有合格数据
                    DeleteList = new List<BottomStockExtend>();
                    foreach(var tempRight in tempRightList) {
                        if(tempRight.WarehouseID == 0)
                            tempRight.WarehouseID = null;
                        //已存在的删除后重新插入
                        var usedPartsDistanceInfor = dbBottomStocks.FirstOrDefault(v => v.PartsSalesCategoryId == tempRight.PartsSalesCategoryId && v.CompanyID == tempRight.CompanyID && v.WarehouseID == tempRight.WarehouseID && v.SparePartId == tempRight.SparePartId);
                        if(usedPartsDistanceInfor != null) {
                            tempRight.PartsSalesCategoryId = usedPartsDistanceInfor.PartsSalesCategoryId;
                            tempRight.CompanyID = usedPartsDistanceInfor.CompanyID;
                            tempRight.WarehouseID = usedPartsDistanceInfor.WarehouseID;
                            tempRight.SparePartId = usedPartsDistanceInfor.SparePartId;
                            tempRight.StockQty = usedPartsDistanceInfor.StockQty;
                            tempRight.Id = usedPartsDistanceInfor.Id;
                            DeleteList.Add(tempRight);
                        } else tempRight.ErrorMsg = "数据无效";
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = DeleteList.Where(r=>r.Id!=0).ToList();
                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.PartsSalesCategoryName = rightItem.PartsSalesCategoryNameStr;
                        rightItem.CompanyCode = rightItem.CompanyCodeStr;
                        rightItem.WarehouseCode = rightItem.WarehouseCodeStr;
                        rightItem.SparePartCode = rightItem.SparePartCodeStr;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.PartsSalesCategoryNameStr,tempObj.CompanyCodeStr,
                                tempObj.WarehouseCodeStr,tempObj.SparePartCodeStr,
                                tempObj.ErrorMsg
                                
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {

                        //删除已存在的数据
                        var deleteList1 = DeleteList.Where(r => r.Id!=0).ToList();
                        if(deleteList1.Any()) {
                            string sqlDelete = "update  BottomStock set status=99,Remark=Remark||'导入作废',AbandonTime=sysdate  where Id=:Id";
                            foreach(var item in deleteList1) {
                                var command = db.CreateDbCommand(sqlDelete, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.ExecuteScalar();
                            }
                        }                      
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}
