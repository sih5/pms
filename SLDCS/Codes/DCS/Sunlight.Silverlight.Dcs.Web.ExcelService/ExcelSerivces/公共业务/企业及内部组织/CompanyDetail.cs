﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportCompanyDetail(string fileName, out int excelImportNum, out List<CompanyDetailExtend> rightData, out List<CompanyDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<CompanyDetailExtend>();
            var allList = new List<CompanyDetailExtend>();
            List<CompanyDetailExtend> rightList = null;
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("CompanyDetail", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Name, "Name");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new CompanyDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.CodeStr = newRow["Code"];
                        tempImportObj.NameStr = newRow["Name"];
                        #endregion
                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        //企业编号检查
                        var fieldIndex = notNullableFields.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CodeStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsNull);
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CodeStr) > fieldLenght["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsLong);
                        }
                        //if(!string.IsNullOrEmpty(tempImportObj.CodeStr)) {
                        //    if(Encoding.Default.GetByteCount(tempImportObj.NameStr) > fieldLenght["Name".ToUpper()])
                        //        tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsSpecialTreatyPrice_NameIsLong);
                        //}
                        #endregion
                        if (tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.CodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpTemporarySupplier_Validation18;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、企业有效性校验（企业.编号=导入数据.企业编号），
                    //如果校验不通过，记录校验错误信息：企业不存在。
                    var codesNeedCheck = tempRightList.Select(r => r.CodeStr).Distinct().ToArray();
                    var dbCompanys = new List<CompanyExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            Type = Convert.ToInt32(value[3])
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("Select Id,Code,Name,Type From Company Where Status = {0}", (int)DcsMasterDataStatus.有效), "Code", true, codesNeedCheck, getDbCompanys);
                    var errorCompanys = tempRightList.Where(r => dbCompanys.All(v => v.Code != r.CodeStr)).ToList();
                    foreach(var errorItem in errorCompanys) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_CompanyDetail_Validation1;
                    }
                    //if(tempRightList.All(r => !string.IsNullOrEmpty(r.NameStr))) {
                    //    var errorCompanyName = tempRightList.Where(r => dbCompanys.Any(v => r.CodeStr == v.Code && r.NameStr != v.Name)).ToList();
                    //    foreach(var errorItem in errorCompanyName) {
                    //        if(errorItem.ErrorMsg == null) {
                    //            errorItem.ErrorMsg = "企业名称与编号不匹配";
                    //        } else {
                    //            errorItem.ErrorMsg = errorItem.ErrorMsg + ";企业名称与编号不匹配";
                    //        }
                    //    }
                    //}
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var supplier = tempRightList.Where(r =>dbCompanys.All(v=>v.Type == (int)DcsCompanyType.配件供应商)).ToList();
                    foreach(var errorItem in supplier) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_CompanyDetail_Validation2;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempCompany = dbCompanys.Single(r => r.Code == rightItem.CodeStr);
                        rightItem.CompanyId = tempCompany.Id;
                        rightItem.Code = tempCompany.Code;
                        rightItem.Name = tempCompany.Name;
                        rightItem.Type = tempCompany.Type;
                    }
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    //#region 将合格数据的值填上
                    //foreach(var rightItem in rightList) {
                    //    rightItem.Remark = rightItem.RemarkStr;
                    //    rightItem.IfCanNewPart = true;
                    //}
                    //#endregion
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.CodeStr, tempObj.NameStr,tempObj.ErrorMsg
                                
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
