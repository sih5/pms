﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Data.Common;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出服务商业态
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="status"></param>
        /// <param name="dealerCode"></param>
        /// <param name="dealerName"></param>
        /// <param name="format"></param>
        /// <param name="quarter"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool ExportDealerFormat(int[] ids, int? status, string dealerCode, string dealerName, string format, string quarter,DateTime? bCreateTime,DateTime? eCreateTime, out string fileName) {
         fileName = GetExportFilePath("配件保底库存合集版本号导出.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userInfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select kv.value,
                                   fr.DealerCode,
                                   fr.DealerName,
                                   fr.Format,
                                   fr.Quarter,
                                   fr.CreatorName,
                                   fr.CreateTime,
                                   fr.AbandonerName,
                                   fr.AbandonTime
                              from DealerFormat fr
                              join keyvalueitem kv
                                on fr.status = kv.key
                               and kv.name = 'BaseData_Status'
                              
                               where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and fr.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(status.HasValue) {
                            sql.Append(@" and fr.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!string.IsNullOrEmpty(dealerCode)) {
                            sql.Append("and LOWER(fr.dealerCode) like {0}dealerCode ");
                            dbParameters.Add(db.CreateDbParameter("dealerCode", "%" + dealerCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(dealerName)) {
                            sql.Append("and fr.dealerName like {0}dealerName ");
                            dbParameters.Add(db.CreateDbParameter("dealerName", "%" + dealerName + "%"));
                        }

                        if(!string.IsNullOrEmpty(format)) {
                            sql.Append("and fr.format like {0}format ");
                            dbParameters.Add(db.CreateDbParameter("format", "%" + format + "%"));
                        }

                        if(!string.IsNullOrEmpty(quarter)) {
                            sql.Append(" and fr.quarter like {0}quarter");
                            dbParameters.Add(db.CreateDbParameter("quarter", "%" + quarter.ToLower() + "%"));
                        }
                       

                        if(bCreateTime.HasValue) {
                            sql.Append(@" and fr.CreateTime >=To_date({0}bStartTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bCreateTime", tempTime.ToString("G")));
                        }
                        if(eCreateTime.HasValue) {
                            sql.Append(@" and fr.CreateTime <=To_date({0}eStartTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("eCreateTime", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_DealerFormat_DealerCode, ErrorStrings.Export_Title_DealerFormat_DealerName,ErrorStrings.Export_Title_DealerFormat_Format,ErrorStrings.Export_Title_DealerFormat_Quarter,
                                    ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        
       /// <summary>
        /// 导入服务商业态
       /// </summary>
       /// <param name="fileName"></param>
       /// <param name="excelImportNum"></param>
       /// <param name="errorData"></param>
       /// <param name="errorDataFileName"></param>
       /// <param name="errorMessage"></param>
       /// <returns></returns>
        public bool ImportDealerFormat(string fileName, out int excelImportNum, out List<DealerFormatExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DealerFormatExtend>();
            var allList = new List<DealerFormatExtend>();
            var userInfo = Utils.GetCurrentUserInfo();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DealerFormat", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                List<DealerFormatExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_DealerFormat_DealerCode, "DealerCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_DealerFormat_DealerName, "DealerName");
                    excelOperator.AddColumnDataSource("服务商业态", "Format");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_DealerFormat_Quarter, "Quarter");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseData_Status", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new DealerFormatExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.DealerCodeStr = newRow["DealerCode"];
                        tempImportObj.DealerNameStr = newRow["DealerName"];
                        tempImportObj.FormatStr = newRow["Format"];
                        tempImportObj.QuarterStr = newRow["Quarter"];
                      
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //服务商编号检查
                        var fieldIndex = notNullableFields.IndexOf("DealerCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DealerCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务商编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DealerCodeStr) > fieldLenght["DealerCode".ToUpper()])
                                tempErrorMessage.Add("服务商编号过长");
                        }

                        //服务商业态检查
                        fieldIndex = notNullableFields.IndexOf("Format".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.FormatStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("服务商业态不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.FormatStr) > fieldLenght["Format".ToUpper()])
                                tempErrorMessage.Add("服务商业态过长");
                            tempImportObj.Format = tempImportObj.FormatStr;
                        }
                        //季度检查
                        fieldIndex = notNullableFields.IndexOf("Quarter".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.QuarterStr)) {
                            tempErrorMessage.Add("季度不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.QuarterStr) > fieldLenght["Quarter".ToUpper()])
                                tempErrorMessage.Add("季度过长");
                            tempImportObj.Quarter = tempImportObj.QuarterStr;
                        }
                       
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //4、文件中品牌名称、人员名称、供应商名称组合唯一，否则提示：数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.DealerCodeStr,
                        r.FormatStr,
                        r.QuarterStr,
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //2、服务商编号存在于企业表中，否则提示：企业编号不存在
                   
                    var CompanyCodesCheck = tempRightList.Select(r => r.DealerCodeStr).Distinct().ToArray();
                   
                    var dbCompanys = new List<CompanyExtend>();
                   
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new CompanyExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            Type = Convert.ToInt32(value[3])
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };                   
                   
                    db.QueryDataWithInOperator("select Id,Code,Name,Type from Company where status=1 ", "Code", false, CompanyCodesCheck, getDbCompanys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbCompanys.FirstOrDefault(v => v.Code == tempRight.DealerCodeStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = "服务商编号不存在";
                            continue;
                        }
                        tempRight.DealerId = repairItem.Id;
                        tempRight.DealerCode = repairItem.Code;
                        tempRight.DealerName = repairItem.Name;
                    }
                                     
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                   
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.DealerCodeStr,tempObj.DealerNameStr,
                                tempObj.FormatStr,tempObj.QuarterStr,
                                tempObj.ErrorMsg
                                
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {

                        //新增后，根据服务商编号，更新上次有效的 数据为作废状态                
                        var deleteList1 = rightList.Select(t => t.DealerId).Distinct().ToArray();
                        if(deleteList1.Any()) {
                            string sqlDelete = " update  DealerFormat set status=99 , AbandonTime=sysdate where DealerId =:DealerId and status=1";
                            foreach(var dealerId in deleteList1) {
                                var command = db.CreateDbCommand(sqlDelete, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("DealerId", dealerId));
                                command.ExecuteScalar();
                            }
                        }
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("DealerFormat", "Id", new[] {
                                "DealerId","DealerCode","DealerName", "Format", "Quarter","Status", "CreatorId",  "CreatorName","CreateTime"
                            });
                            #endregion
                            //往数据库增加市场部信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("DealerId", item.DealerId));
                                command.Parameters.Add(db.CreateDbParameter("DealerCode", item.DealerCode));
                                command.Parameters.Add(db.CreateDbParameter("DealerName", item.DealerName));
                                command.Parameters.Add(db.CreateDbParameter("Format", item.Format));
                                command.Parameters.Add(db.CreateDbParameter("Quarter", item.Quarter));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));                                       
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}
