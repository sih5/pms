﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportMarketDptPersonnelRelation(string departmentCode, string departmentName, string personnelName, int? type, int? status, int companyType, out string fileName) {
            fileName = GetExportFilePath("分销中心与人员关系_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(this.GetSqlByCompanyType(companyType));
                    var dbParameters = new List<DbParameter>();
                    if(!string.IsNullOrEmpty(departmentCode)) {
                        sql.Append(@" and b.code like {0}departmentCode ");
                        dbParameters.Add(db.CreateDbParameter("departmentCode", "%" + departmentCode + "%"));
                    }
                    if(!string.IsNullOrEmpty(departmentName)) {
                        sql.Append(@" and b.name like {0}departmentName ");
                        dbParameters.Add(db.CreateDbParameter("departmentName", "%" + departmentName + "%"));
                    }
                    if(!string.IsNullOrEmpty(personnelName)) {
                        sql.Append(@" and c.name like {0}personnelName ");
                        dbParameters.Add(db.CreateDbParameter("personnelName", "%" + personnelName + "%"));
                    }
                    if(type.HasValue) {
                        sql.Append(@" and a.type ={0}type");
                        dbParameters.Add(db.CreateDbParameter("type", type.Value));
                    }
                    if(status.HasValue) {
                        sql.Append(@" and a.status ={0}status");
                        dbParameters.Add(db.CreateDbParameter("status", status.Value));
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_MarketingDepartment_Code,ErrorStrings.Export_Title_MarketingDepartment_Name,ErrorStrings.Export_Title_MarketingDepartment_PersonnelName,ErrorStrings.Export_Title_MarketingDepartment_Type,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        private string GetSqlByCompanyType(int companyType) {
            if(companyType == (int)DcsCompanyType.分公司) {
                var userInfo = Utils.GetCurrentUserInfo();
                return string.Format(@"select d.name as BranchName,
                                       b.code as DepartmentCode,
                                       b.name as DepartmentName,
                                       c.name as PersonnelName,
                                       (select value from keyvalueitem where NAME = 'DepartmentPersonType'and key=a.Type) As Type,
                                       (select value from keyvalueitem where NAME = 'MasterData_Status'and key=a.Status) As Status,
                                       a.CreatorName,
                                       a.CreateTime,
                                       a.ModifierName,
                                       a.ModifyTime,
                                       a.AbandonerName,
                                       a.AbandonTime
                                  from MarketDptPersonnelRelation a
                                 inner join marketingdepartment b
                                    on a.MarketDepartmentId = b.id
                                 inner join PersonSalesCenterLink d
                                    on  b.PartsSalesCategoryId = d.PartsSalesCategoryId
                                 inner join personnel c
                                    on a.personnelid = c.id
                                 inner join branch d
                                    on a.branchid = d.id
                                 where d.PersonId ={0}", userInfo.Id);
            } else {
                return @"select d.name as BranchName,
                                       b.code as DepartmentCode,
                                       b.name as DepartmentName,
                                       c.name as PersonnelName,
                                       (select value from keyvalueitem where NAME = 'DepartmentPersonType'and key=a.Type) As Type,
                                       (select value from keyvalueitem where NAME = 'MasterData_Status'and key=a.Status) As Status,
                                       a.CreatorName,
                                       a.CreateTime,
                                       a.ModifierName,
                                       a.ModifyTime,
                                       a.AbandonerName,
                                       a.AbandonTime
                                  from MarketDptPersonnelRelation a
                                 inner join marketingdepartment b
                                    on a.MarketDepartmentId = b.id
                                 inner join personnel c
                                    on a.personnelid = c.id
                                 inner join branch d
                                    on a.branchid = d.id where 1=1 ";
            }
        }

        /// <summary>
        /// 导入市场部与人员关系
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportMarketDptPersonnelRelation(string fileName, out int excelImportNum, out List<MarketDptPersonnelRelationExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<MarketDptPersonnelRelationExtend>();
            var allList = new List<MarketDptPersonnelRelationExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取营销分公司表结构
                List<string> notNullableFieldsOfBranch;
                Dictionary<string, int> fieldLenghtOfBranch;
                db.GetTableSchema("Branch", out notNullableFieldsOfBranch, out fieldLenghtOfBranch);

                //获取营销分公司市场部表结构
                List<string> notNullableFieldsOfMarketingDepartment;
                Dictionary<string, int> fieldLenghtOfMarketingDepartment;
                db.GetTableSchema("MarketingDepartment", out notNullableFieldsOfMarketingDepartment, out fieldLenghtOfMarketingDepartment);

                //获取人员名称表结构
                List<string> notNullableFieldsOfPerSonnel;
                Dictionary<string, int> fieldLenghtOfPerSonnel;
                db.GetTableSchema("PerSonnel", out notNullableFieldsOfPerSonnel, out fieldLenghtOfPerSonnel);

                List<object> excelColumns;
                List<MarketDptPersonnelRelationExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 ：excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Branch_BranchName, "BranchName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_Code, "MarketDepartmentCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_Name, "MarketDepartmentName");
                    //excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_PersonnelName, "PersonnelName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_Type, "Type");


                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举 例如：
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new MarketDptPersonnelRelationExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.MarketDepartmentCodeStr = newRow["MarketDepartmentCode"];
                        tempImportObj.BranchNameStr = newRow["BranchName"];
                        tempImportObj.MarketDepartmentNameStr = newRow["MarketDepartmentName"];
                        //tempImportObj.PartsSalesCategoryName = newRow["PartsSalesCategoryName"];
                        tempImportObj.PersonnelNameStr = newRow["PersonnelName"];
                        tempImportObj.TypeStr = newRow["Type"];

                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //分公司名称校验
                        var fieldIndex = notNullableFieldsOfBranch.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BranchNameStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Branch_BranchNameIsNull);
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BranchNameStr) > fieldLenghtOfBranch["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Branch_BranchNameIsLong);
                        }

                        //分销中心编号校验
                        fieldIndex = notNullableFieldsOfMarketingDepartment.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.MarketDepartmentCodeStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_CodeIsNull);
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.MarketDepartmentCodeStr) > fieldLenghtOfMarketingDepartment["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_CodeIsLong);
                        }
                        //分销中心名称校验
                        fieldIndex = notNullableFieldsOfMarketingDepartment.IndexOf("MarketDepartmentName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.MarketDepartmentCodeStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_NameIsNull);
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.MarketDepartmentCodeStr) > fieldLenghtOfMarketingDepartment["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_NameIsLong);
                        }
                        //人员名称校验
                        fieldIndex = notNullableFieldsOfPerSonnel.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PersonnelNameStr)) {
                            if(fieldIndex > -1) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PersonnelNameIsNull);
                            }
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PersonnelNameStr) > fieldLenghtOfPerSonnel["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PersonnelNameIsLong);
                        }
                        //类型检查
                        fieldIndex = notNullableFieldsOfPerSonnel.IndexOf("Type".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.TypeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_TypeIsNull);
                        } else {
                            DcsDepartmentPersonType tempValue;
                            if(Enum.TryParse(tempImportObj.TypeStr, out tempValue)) {
                                tempImportObj.Type = (int)tempValue;
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_TypeValueError);
                            }
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //文件内，市场部编号、市场部名称、人员姓名组合唯一，否则提示：文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.MarketDepartmentCodeStr,
                        r.MarketDepartmentNameStr,
                        r.PersonnelNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //分销中心编号、分销中心名称组合存在营销分公司市场部，否则提示：分销中心不存在
                    var marketDepartmentCodesNeedCheck = tempRightList.Select(r => r.MarketDepartmentCodeStr).Distinct().ToArray();
                    var dbMarketDepartments = new List<MarketingDepartmentExtend>();
                    Func<string[], bool> getDbMarketDepartmentCodes = value => {
                        var dbObj = new MarketingDepartmentExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                            //,
                            //PartsSalesCategoryNameStr = value[3]
                        };
                        dbMarketDepartments.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name,(select name from partssalescategory where MarketingDepartment.Partssalescategoryid=partssalescategory.id) as PartsSalesCategoryNameStr from MarketingDepartment where status=1 ", "Code", false, marketDepartmentCodesNeedCheck, getDbMarketDepartmentCodes);
                    foreach(var tempRight in tempRightList) {
                        var marketDepartment = dbMarketDepartments.FirstOrDefault(v => v.Code == tempRight.MarketDepartmentCodeStr && v.Name == tempRight.MarketDepartmentNameStr/* && v.PartsSalesCategoryNameStr == tempRight.PartsSalesCategoryName*/);
                        if(marketDepartment == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_MarketingDepartment_Validation1, tempRight.MarketDepartmentCodeStr, tempRight.MarketDepartmentNameStr);
                            continue;
                        }
                        tempRight.MarketDepartmentId = marketDepartment.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //分公司名称存在于营销公公司，否则提示：分公司名称**不存在
                    var branchNamesNeedCheck = tempRightList.Select(r => r.BranchNameStr).Distinct().ToArray();
                    var dbBranchs = new List<BranchExtend>();
                    Func<string[], bool> getDbBranchNames = value => {
                        var dbObj = new BranchExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbBranchs.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from Branch where status=1 ", "Name", false, branchNamesNeedCheck, getDbBranchNames);
                    foreach(var tempRight in tempRightList) {
                        var branch = dbBranchs.FirstOrDefault(v => v.Name == tempRight.BranchNameStr);
                        if(branch == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_MarketingDepartment_Validation2, tempRight.BranchNameStr);
                            continue;
                        }
                        tempRight.BranchId = branch.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //人员姓名存在于人员，否则提示：人员姓名***不存在
                    var personnelNamesNeedCheck = tempRightList.Select(r => r.PersonnelNameStr).Distinct().ToArray();
                    var dbPersonnels = new List<PersonnelExtend>();
                    Func<string[], bool> getDbPersonnelNames = value => {
                        var dbObj = new PersonnelExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1],
                            CorporationId = Convert.ToInt32(value[2])
                        };
                        dbPersonnels.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name,CorporationId from Personnel where status=1 ", "Name", false, personnelNamesNeedCheck, getDbPersonnelNames);
                    foreach(var tempRight in tempRightList) {
                        var personnel = dbPersonnels.FirstOrDefault(v => v.Name == tempRight.PersonnelNameStr && v.CorporationId == tempRight.BranchId);
                        if(personnel == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_MarketingDepartment_Validation3, tempRight.PersonnelNameStr);
                            continue;
                        }
                        tempRight.PersonnelId = personnel.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();



                    //分公司名称、分销中心编号、分销中心名称、人员姓名组合唯一，否则提法：系统内数据已存在
                    var branchIdsNeedCheck = tempRightList.Select(r => r.BranchId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbMarketDptPersonnelRelations = new List<MarketDptPersonnelRelationExtend>();
                    Func<string[], bool> getDbMarketDptPersonnelRelations = value => {
                        var dbObj = new MarketDptPersonnelRelationExtend {
                            BranchId = Convert.ToInt32(value[0]),
                            MarketDepartmentId = Convert.ToInt32(value[1]),
                            PersonnelId = Convert.ToInt32(value[2])
                        };
                        dbMarketDptPersonnelRelations.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select BranchId,MarketDepartmentId,PersonnelId from MarketDptPersonnelRelation where status=1 ", "BranchId", false, branchIdsNeedCheck, getDbMarketDptPersonnelRelations);
                    foreach(var tempRight in tempRightList) {
                        var marketDptPersonnelRelation = dbMarketDptPersonnelRelations.FirstOrDefault(v => v.BranchId == tempRight.BranchId && v.MarketDepartmentId == tempRight.MarketDepartmentId && v.PersonnelId == tempRight.PersonnelId);
                        if(marketDptPersonnelRelation != null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_MarketingDepartment_Validation4;
                        }
                    }

                    #endregion
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    foreach(var rightItem in rightList) {
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                    }
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.BranchNameStr,tempObj.MarketDepartmentCodeStr,
                                tempObj.MarketDepartmentNameStr,tempObj.PersonnelNameStr,tempObj.TypeStr,
                                tempObj.ErrorMsg
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("MarketDptPersonnelRelation", "Id", new[] {
                                "BranchId","MarketDepartmentId","PersonnelId","Type","Status","CreatorId","CreatorName","CreateTime"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("MarketDepartmentId", item.MarketDepartmentId));
                                command.Parameters.Add(db.CreateDbParameter("PersonnelId", item.PersonnelId));
                                command.Parameters.Add(db.CreateDbParameter("Type", item.Type));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }

        }

    }
}
