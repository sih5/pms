﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportMarketingDepartment(string code, string name, int? branchId, int? partsSalesCategoryId, int? businessType, out string fileName) {
            fileName = GetExportFilePath("营销分公司分销中心_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select    a.Code,
                                           a.Name,
                                           b.name as branchName,
                                           c.name as PartsSalesCategoryName,
                                           (select value from keyvalueitem where NAME = 'MarketingDepartment_Type'and key=a.BusinessType) As BusinessType,
                                           (select value from keyvalueitem where NAME = 'MasterData_Status'and key=a.Status) As Status,
                                           a.Address,
                                           a.Phone,
                                           a.Fax,
                                           a.PostCode,
                                           a.ProvinceName,
                                           a.CityName,
                                           a.CountyName,
                                           a.Remark,
                                           a.CreateTime,
                                           a.CreatorName,
                                           a.ModifyTime
                                      from MarketingDepartment a
                                     left join branch b
                                        on a.Branchid = b.id
                                     left join partssalescategory c
                                        on a.partssalescategoryid = c.id where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(!string.IsNullOrEmpty(code)) {
                        sql.Append(@" and a.code like {0}code ");
                        dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                    }
                    if(!string.IsNullOrEmpty(name)) {
                        sql.Append(@" and a.name like {0}name ");
                        dbParameters.Add(db.CreateDbParameter("name", "%" + name + "%"));
                    }
                    if(branchId.HasValue) {
                        sql.Append(@" and b.Id ={0}branchId");
                        dbParameters.Add(db.CreateDbParameter("branchId", branchId.Value));
                    }
                    if(partsSalesCategoryId.HasValue) {
                        sql.Append(@" and a.partsSalesCategoryId ={0}partsSalesCategoryId");
                        dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                    }
                    if(businessType.HasValue) {
                        sql.Append(@" and a.businessType ={0}businessType");
                        dbParameters.Add(db.CreateDbParameter("businessType", businessType.Value));
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_MarketingDepartment_Code,ErrorStrings.Export_Title_MarketingDepartment_Name,ErrorStrings.Export_Title_Branch_BranchName,ErrorStrings.Export_Title_Partssalescategory_Name,
                                    ErrorStrings.Export_Title_MarketingDepartment_BussinessType,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_MarketingDepartment_Jurisdiction,ErrorStrings.Export_Title_Agency_LinkManMobile,ErrorStrings.Export_Title_Agency_Fax,
                                    ErrorStrings.Export_Title_MarketingDepartment_PostCode,ErrorStrings.Export_Title_Agency_Province,ErrorStrings.Export_Title_Agency_City,ErrorStrings.Export_Title_MarketingDepartment_CountyName,ErrorStrings.Export_Title_PartsBranch_Remark,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                 };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }


        }
        /// <summary>
        /// 导入市场部
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportMarketingDepartment(string fileName, out int excelImportNum, out List<MarketingDepartmentExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<MarketingDepartmentExtend>();
            var allList = new List<MarketingDepartmentExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("MarketingDepartment", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsPartsSalesCategory;
                Dictionary<string, int> fieldLenghtPartsSalesCategory;
                db.GetTableSchema("PartsSalesCategory", out notNullableFieldsPartsSalesCategory, out fieldLenghtPartsSalesCategory);

                List<string> notNullableFieldsBranch;
                Dictionary<string, int> fieldLenghtBranch;
                db.GetTableSchema("Branch", out notNullableFieldsBranch, out fieldLenghtBranch);

                List<object> excelColumns;
                List<MarketingDepartmentExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 ：excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");

                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_Code, "Code");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_Name, "Name");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Branch_BranchName, "BranchName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_BussinessType, "BusinessType");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_Jurisdiction, "Address");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_LinkManMobile, "Phone");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Fax, "Fax");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_PostCode, "PostCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_Province, "ProvinceName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Agency_City, "CityName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_MarketingDepartment_CountyName, "CountyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举 例如：
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("BusinessType", "MarketingDepartment_Type"),
                        new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new MarketingDepartmentExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.CodeStr = newRow["Code"];
                        tempImportObj.NameStr = newRow["Name"];
                        tempImportObj.BranchNameStr = newRow["BranchName"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.BusinessTypeStr = newRow["BusinessType"];
                        tempImportObj.AddressStr = newRow["Address"];
                        tempImportObj.PhoneStr = newRow["Phone"];
                        tempImportObj.FaxStr = newRow["Fax"];
                        tempImportObj.PostCodeStr = newRow["PostCode"];
                        tempImportObj.ProvinceNameStr = newRow["ProvinceName"];
                        tempImportObj.CityNameStr = newRow["CityName"];
                        tempImportObj.CountyNameStr = newRow["CountyName"];
                        tempImportObj.RemarkStr = newRow["Remark"];

                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //市场部编号检查
                        var fieldIndex = notNullableFields.IndexOf("Code".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_MarketCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CodeStr) > fieldLenght["Code".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_MarketCodeIsLong);
                        }

                        //市场部名称检查
                        fieldIndex = notNullableFields.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.NameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_MarketNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.NameStr) > fieldLenght["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_MarketNameIsLong);
                        }

                        //品牌检查
                        fieldIndex = notNullableFieldsPartsSalesCategory.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenghtPartsSalesCategory["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsLong);
                        }

                        //分公司名称检查
                        fieldIndex = notNullableFieldsBranch.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BranchNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Branch_BranchNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.BranchNameStr) > fieldLenghtBranch["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Branch_BranchNameIsLong);
                        }

                        //业务类型检查
                        fieldIndex = notNullableFields.IndexOf("BusinessType".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.BusinessTypeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_BussinessTypeIsNull);
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("BusinessType", tempImportObj.BusinessTypeStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_BussinessTypeValueError);
                            }
                        }

                        //管辖区域检查
                        fieldIndex = notNullableFields.IndexOf("Address".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.AddressStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_JurisdictionIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.AddressStr) > fieldLenght["Address".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_JurisdictionIsLong);
                        }

                        //联系电话检查
                        fieldIndex = notNullableFields.IndexOf("Phone".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PhoneStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_LinkPhoneIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PhoneStr) > fieldLenght["Phone".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_LinkPhoneIsLong);
                        }

                        //传真检查
                        fieldIndex = notNullableFields.IndexOf("Fax".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.FaxStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_FaxNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.FaxStr) > fieldLenght["Fax".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_FaxIsLong);
                        }

                        //邮政编码检查
                        fieldIndex = notNullableFields.IndexOf("PostCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PostCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PostCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PostCodeStr) > fieldLenght["PostCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PostCodeIsLong);
                        }

                        //省检查
                        fieldIndex = notNullableFields.IndexOf("ProvinceName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ProvinceNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_ProvinceIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ProvinceNameStr) > fieldLenght["ProvinceName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_ProvinceIsLong);
                        }

                        //市检查
                        fieldIndex = notNullableFields.IndexOf("CityName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CityNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_CityIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CityNameStr) > fieldLenght["CityName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_CityIsLong);
                        }

                        //县检查
                        fieldIndex = notNullableFields.IndexOf("CountyName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.CountyNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_CountyIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.CountyNameStr) > fieldLenght["CountyName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_CountyIsLong);
                        }

                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //文件内，市场部编号 + 品牌 +类型 进行唯一性校验，否则提示：文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.CodeStr,
                        r.BranchNameStr,
                        r.PartsSalesCategoryNameStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //分公司名称存在于营销分公司中，否则提示：分公司名称***不存在
                    var branchNamesNeedCheck = tempRightList.Select(r => r.BranchNameStr).Distinct().ToArray();
                    var dbBranchs = new List<BranchExtend>();
                    Func<string[], bool> getDbBranchNames = value => {
                        var dbObj = new BranchExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbBranchs.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from Branch where status=1 ", "Name", false, branchNamesNeedCheck, getDbBranchNames);
                    foreach(var tempRight in tempRightList) {
                        var branch = dbBranchs.FirstOrDefault(v => v.Name == tempRight.BranchNameStr);
                        if(branch == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.Export_Validation_MarketingDepartment_Validation2, tempRight.BranchNameStr);
                            continue;
                        }
                        tempRight.BranchId = branch.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //品牌存在于配件销售类型中,否则提示：品牌***不存在
                    var partsSalesCategoryNamesCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from PartsSalesCategory where status=1 ", "Name", false, partsSalesCategoryNamesCheck, getDbPartsSalesCategorys);
                    foreach(var tempRight in tempRightList) {
                        var partsSalesCategory = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr);
                        if(partsSalesCategory == null) {
                            tempRight.ErrorMsg = String.Format(ErrorStrings.ImpPartsSupplierRelation_Validation28, tempRight.PartsSalesCategoryNameStr);
                            continue;
                        }
                        tempRight.PartsSalesCategoryId = partsSalesCategory.Id;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //省、市、县组合存在于区省市信息中，否则提示省、市、县（区）数据不存在
                    var provinceNameStrsCheck = tempRightList.Select(r => r.ProvinceNameStr);
                    var cityNamesNeedCheck = tempRightList.Select(r => r.CityNameStr);
                    var countyNamesNeedCheck = tempRightList.Select(r => r.CountyNameStr);
                    var partNamesNeedCheck = provinceNameStrsCheck.Concat(cityNamesNeedCheck).Concat(countyNamesNeedCheck).Distinct().ToArray();
                    var dbRegions = new List<RegionExtend>();
                    Func<string[], bool> getDbRegions = value => {
                        var dbObj = new RegionExtend {
                            Id = Convert.ToInt32(value[0]),
                            ParentId = Convert.ToInt32(value[1]),
                            Name = value[2]
                        };
                        dbRegions.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,ParentId,Name from Region", "Name", false, partNamesNeedCheck, getDbRegions);
                    foreach(var tempRight in tempRightList) {
                        if(!string.IsNullOrEmpty(tempRight.ProvinceNameStr)) {
                            var regionProvince = dbRegions.FirstOrDefault(v => v.Name == tempRight.ProvinceNameStr);

                            if(regionProvince != null) {
                                tempRight.RegionId = regionProvince.Id;

                                if(!string.IsNullOrEmpty(tempRight.CityNameStr)) {
                                    var regionCity = dbRegions.FirstOrDefault(v => v.Name == tempRight.CityNameStr && v.ParentId == regionProvince.Id);
                                    if(regionCity != null) {
                                        tempRight.RegionId = regionCity.Id;

                                        if(!string.IsNullOrEmpty(tempRight.CountyNameStr)) {
                                            var regionCounty = dbRegions.FirstOrDefault(v => v.Name == tempRight.CountyNameStr && v.ParentId == regionCity.Id);
                                            if(regionCounty != null) {
                                                tempRight.RegionId = regionCounty.Id;

                                            } else {
                                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_MarketingDepartment_Validation5;
                                            }
                                        }
                                    } else {
                                        tempRight.ErrorMsg = ErrorStrings.Export_Validation_MarketingDepartment_Validation6;
                                    }
                                }
                            } else {
                                tempRight.ErrorMsg = ErrorStrings.Export_Validation_MarketingDepartment_Validation7;
                            }
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //分公司名称、品牌、市场部编号、市场部名称组合唯一，否则提示：系统内数据已存在
                    var branchIdsNeedCheck = tempRightList.Select(r => r.BranchId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbMarketingDepartments = new List<MarketingDepartmentExtend>();
                    Func<string[], bool> getDbMarketingDepartmentns = value => {
                        var dbObj = new MarketingDepartmentExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2],
                            BranchId = Convert.ToInt32(value[3]),
                            PartsSalesCategoryId = Convert.ToInt32(value[4])
                        };
                        dbMarketingDepartments.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name,BranchId,PartsSalesCategoryId from MarketingDepartment where status=1 ", "BranchId", false, branchIdsNeedCheck, getDbMarketingDepartmentns);
                    foreach(var tempRight in tempRightList) {
                        var marketingDepartment = dbMarketingDepartments.FirstOrDefault(v => v.Code == tempRight.CodeStr && v.Name == tempRight.NameStr && v.BranchId == tempRight.BranchId && v.PartsSalesCategoryId == tempRight.PartsSalesCategoryId);
                        if(marketingDepartment != null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_MarketingDepartment_Validation4;
                        }
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.Code = rightItem.CodeStr;
                        rightItem.Name = rightItem.NameStr;
                        rightItem.BusinessType = tempExcelOperator.ImportHelper.GetEnumValue("BusinessType", rightItem.BusinessTypeStr) ?? 0;
                        rightItem.Address = rightItem.AddressStr;
                        rightItem.Phone = rightItem.PhoneStr;
                        rightItem.Fax = rightItem.FaxStr;
                        rightItem.PostCode = rightItem.PostCodeStr;
                        rightItem.ProvinceName = rightItem.ProvinceNameStr;
                        rightItem.CityName = rightItem.CityNameStr;
                        rightItem.CountyName = rightItem.CountyNameStr;
                        rightItem.Remark = rightItem.RemarkStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseDataStatus", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值

                                tempObj.CodeStr,tempObj.NameStr,
                                tempObj.PartsSalesCategoryNameStr,tempObj.BranchNameStr ,
                                tempObj.BusinessTypeStr ,tempObj.AddressStr ,
                                tempObj.PhoneStr ,tempObj.FaxStr ,
                                tempObj.PostCodeStr ,tempObj.ProvinceNameStr,
                                tempObj.CityNameStr,tempObj.CountyNameStr,
                                tempObj.RemarkStr,tempObj.ErrorMsg
                                
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("MarketingDepartment", "Id", new[] {
                                "BranchId", "PartsSalesCategoryId", "Code", "Name", "BusinessType", "Address", "Phone", "Fax", "PostCode", "RegionId", "ProvinceName", "CityName", "CountyName", "Status", "CreatorId", "CreatorName", "CreateTime", "Remark"
                            });
                            #endregion
                            var userInfo = Utils.GetCurrentUserInfo();
                            //往数据库增加市场部信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("Code", item.Code));
                                command.Parameters.Add(db.CreateDbParameter("Name", item.Name));
                                command.Parameters.Add(db.CreateDbParameter("BusinessType", item.BusinessType));
                                command.Parameters.Add(db.CreateDbParameter("Address", item.Address));
                                command.Parameters.Add(db.CreateDbParameter("Phone", item.Phone));
                                command.Parameters.Add(db.CreateDbParameter("Fax", item.Fax));
                                command.Parameters.Add(db.CreateDbParameter("PostCode", item.PostCode));
                                command.Parameters.Add(db.CreateDbParameter("RegionId", item.RegionId));
                                command.Parameters.Add(db.CreateDbParameter("ProvinceName", item.ProvinceName));
                                command.Parameters.Add(db.CreateDbParameter("CityName", item.CityName));
                                command.Parameters.Add(db.CreateDbParameter("CountyName", item.CountyName));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }

        }
    }
}
