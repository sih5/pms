﻿using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出人员与品牌关系
        /// </summary>
        /// <param name="ids">记录id集合</param>
        /// <param name="userId">当前用户id</param>
        /// <param name="partsSalesCategoryId">品牌id</param>
        /// <param name="storageCompanyId">当前用户企业id</param>
        /// <param name="status">状态</param>
        /// <param name="personName">人员名称</param>
        /// <param name="createTimeBegin">创建时间起始</param>
        /// <param name="createTimeEnd">创建时间结束</param>
        /// <returns></returns>
        public bool ExportPersonSalesCenterLink(int[] ids, int? partsSalesCategoryId, int? status, string personName, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath("人员与品牌关系.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);

                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"SELECT Categ.Name AS Category,
                                               person.Name AS personName,
                                               link.persontype,
                                               person.cellnumber,
                                               Categ.Branchname,
                                               (select value from keyvalueitem where NAME = 'MasterData_Status'and key=link.status) As Status,
                                               link.creatorname,
                                               link.createtime,
                                               link.modifiername,
                                               link.modifytime,
                                               link.abandonername,
                                               link.abandontime
                                          FROM PersonSalesCenterLink link
                                          LEFT OUTER JOIN Personnel person ON link.PersonId = person.Id
                                          LEFT OUTER JOIN PartsSalesCategory Categ ON link.PartsSalesCategoryId = Categ.Id where 1=1 "));
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and link.id in(");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and link.PartsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }

                        if(status.HasValue) {
                            sql.Append(" and link.Status = {0}Status ");
                            dbParameters.Add(db.CreateDbParameter("Status", status.Value));
                        }

                        if(!string.IsNullOrEmpty(personName)) {
                            sql.Append(" and LOWER(person.Name) LIKE {0}personName");
                            dbParameters.Add(db.CreateDbParameter("personName", "%" + personName.ToLower() + "%"));
                        }

                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and link.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }


                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and link.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_Partssalescategory_Name, ErrorStrings.Export_Title_PersonnelSupplierRelation_PersonName, ErrorStrings.Export_Title_PersonSalesCenterLink_PersonType, ErrorStrings.Export_Title_PersonSalesCenterLink_CellNumber,ErrorStrings.Export_Title_Company_Name, ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime, ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
