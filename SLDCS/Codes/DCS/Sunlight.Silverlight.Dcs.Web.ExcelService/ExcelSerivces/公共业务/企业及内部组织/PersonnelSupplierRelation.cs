﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Data.Common;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public partial class ExcelService
    {
        /// <summary>
        /// 导入人员与供应商关系
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportPersonnelSupplierRelation(string fileName, out int excelImportNum, out List<PersonnelSupplierRelationExtend> errorData, out string errorDataFileName, out string errorMessage)
        {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PersonnelSupplierRelationExtend>();
            var allList = new List<PersonnelSupplierRelationExtend>();
            var userInfo = Utils.GetCurrentUserInfo();
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PersonnelSupplierRelation", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                List<PersonnelSupplierRelationExtend> rightList;
                List<PersonnelSupplierRelationExtend> DeleteList;//用于删除
                using (var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString))
                {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_BottomStock_PartsSalesCategoryName, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PersonnelSupplierRelation_PersonCode, "PersonCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode, "SupplierCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseData_Status", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row =>
                    {
                        var tempImportObj = new PersonnelSupplierRelationExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.PersonCodeStr = newRow["PersonCode"];
                        tempImportObj.SupplierCodeStr = newRow["SupplierCode"];
                        tempImportObj.RemarkStr = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //品牌名称检查
                        var fieldIndex = notNullableFields.IndexOf("PartsSalesCategoryName".ToUpper());
                        if (string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr))
                        {
                            if (fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsNull);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenght["PartsSalesCategoryName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsBranch_PartsSalescategoryNameIsLong);
                        }
                        //人员编号检查
                        fieldIndex = notNullableFields.IndexOf("PersonCode".ToUpper());
                        if (string.IsNullOrEmpty(tempImportObj.PersonCodeStr))
                        {
                            if (fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_PersonCodeIsNull);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.PersonCodeStr) > fieldLenght["PersonCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_PersonCodeIsLong);
                        }
                        //供应商编号检查
                        fieldIndex = notNullableFields.IndexOf("SupplierCode".ToUpper());
                        if (string.IsNullOrEmpty(tempImportObj.SupplierCodeStr))
                        {
                            if (fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCode);
                        }
                        else
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.SupplierCodeStr) > fieldLenght["SupplierCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCodeIsLong);
                        }
                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if (!string.IsNullOrEmpty(tempImportObj.RemarkStr))
                        {
                            if (Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion
                        if (tempErrorMessage.Count > 0)
                        {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //4、文件中品牌名称、人员名称、供应商名称组合唯一，否则提示：数据重复
                    var groups = tempRightList.GroupBy(r => new
                    {
                        r.PartsSalesCategoryNameStr,
                        r.PersonCodeStr,
                        r.SupplierCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach (var groupItem in groups.SelectMany(r => r))
                    {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //1、品牌名称存在于配件销售类型表中，否则提示：品牌名称不存在
                    //2、人员名称存在于人员表中，否则提示：人员名称不存在
                    //3、供应商名称存在于供应商表中，否则提示：供应商名称不存在
                    var PartsSalesCategoryNamesNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr).Distinct().ToArray();
                    var PersonCodesNeedCheck = tempRightList.Select(r => r.PersonCodeStr).Distinct().ToArray();
                    var SupplierCodesNeedCheck = tempRightList.Select(r => r.SupplierCodeStr).Distinct().ToArray();
                    var dbPersonnels = new List<PersonnelExtend>();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    var dbPartsSuppliers = new List<PartsSupplierExtend>();
                    Func<string[], bool> getDbPersonnels = value =>
                    {
                        var dbObj = new PersonnelExtend
                        {
                            Id = Convert.ToInt32(value[0]),
                            LoginId = value[1],
                            Name = value[2]
                        };
                        dbPersonnels.Add(dbObj);
                        return false;
                    };
                    Func<string[], bool> getdbPartsSalesCategorys = value =>
                    {
                        var dbObj = new PartsSalesCategoryExtend
                        {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    Func<string[], bool> getdbPartsSuppliers = value =>
                    {
                        var dbObj = new PartsSupplierExtend
                        {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbPartsSuppliers.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from PartsSalesCategory where status=1 ", "Name", false, PartsSalesCategoryNamesNeedCheck, getdbPartsSalesCategorys);
                    foreach (var tempRight in tempRightList)
                    {
                        var repairItem = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr);
                        if (repairItem == null)
                        {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_BottomStock_PartsSalesCategoryNameIsNull;
                            continue;
                        }
                        tempRight.PartsSalesCategoryId = repairItem.Id;
                    }
                    db.QueryDataWithInOperator("select Id,LoginId,Name from Personnel where status=1 and CorporationId="+userInfo.EnterpriseId+" ", "LoginId", false, PersonCodesNeedCheck, getDbPersonnels);
                    foreach (var tempRight in tempRightList)
                    {
                        var repairItem = dbPersonnels.FirstOrDefault(v => v.LoginId == tempRight.PersonCodeStr);
                        if (repairItem == null)
                        {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PersonnelSupplierRelation_PersonCodeNotExist;
                            continue;
                        }
                        tempRight.PersonId = repairItem.Id;
                        tempRight.PersonName = repairItem.Name;
                    }
                    db.QueryDataWithInOperator("select Id,Code,Name from PartsSupplier where status=1 ", "Code", false, SupplierCodesNeedCheck, getdbPartsSuppliers);
                    foreach (var tempRight in tempRightList)
                    {
                        var repairItem = dbPartsSuppliers.FirstOrDefault(v => v.Code == tempRight.SupplierCodeStr);
                        if (repairItem == null)
                        {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_PersonnelSupplierRelation_SupplierCodeNotExist;
                            continue;
                        }
                        tempRight.SupplierId = repairItem.Id;
                        tempRight.SupplierName = repairItem.Name;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //3、系统中品牌名称、人员名称、供应商名称组合唯一，否则提法：已存在人员与供应商关系
                    var PartsSalesCategoryIdsNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    var dbPersonnelSupplierRelations = new List<PersonnelSupplierRelationExtend>();
                    Func<string[], bool> getDbPersonnelSupplierRelations = value =>
                    {
                        var dbObj = new PersonnelSupplierRelationExtend
                        {
                            PartsSalesCategoryId = Convert.ToInt32(value[0]),
                            PersonId = Convert.ToInt32(value[1]),
                            SupplierId = Convert.ToInt32(value[2])
                        };
                        dbPersonnelSupplierRelations.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select PartsSalesCategoryId,PersonId,SupplierId from PersonnelSupplierRelation where 1=1 ", "PartsSalesCategoryId", false, PartsSalesCategoryIdsNeedCheck, getDbPersonnelSupplierRelations);

                    //获取所有合格数据
                    DeleteList = new List<PersonnelSupplierRelationExtend>();
                    foreach (var tempRight in tempRightList)
                    {
                        //已存在的删除后重新插入
                        var usedPartsDistanceInfor = dbPersonnelSupplierRelations.FirstOrDefault(v => v.PartsSalesCategoryId == tempRight.PartsSalesCategoryId && v.PersonId == tempRight.PersonId && v.SupplierId == tempRight.SupplierId);
                        if (usedPartsDistanceInfor != null)
                        {
                            tempRight.PartsSalesCategoryId = usedPartsDistanceInfor.PartsSalesCategoryId;
                            tempRight.PersonId = usedPartsDistanceInfor.PersonId;
                            tempRight.SupplierId = usedPartsDistanceInfor.SupplierId;
                            DeleteList.Add(tempRight);
                        }
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach (var rightItem in rightList)
                    {
                        rightItem.PartsSalesCategoryName = rightItem.PartsSalesCategoryNameStr;
                        rightItem.PersonCode = rightItem.PersonCodeStr;
                        rightItem.SupplierCode = rightItem.SupplierCodeStr;
                        rightItem.Remark = rightItem.RemarkStr;
                        rightItem.Status = tempExcelOperator.ImportHelper.GetEnumValue("BaseData_Status", ErrorStrings.Export_Validation_PartsBranch_Used) ?? 0;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if (errorList.Any())
                {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using (var excelExport = new ExcelExport(errorDataFileName))
                    {
                        var list = errorList;
                        excelExport.ExportByRow(index =>
                        {
                            if (index == list.Count + 1)
                                return null;
                            if (index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值

                                tempObj.PartsSalesCategoryNameStr,tempObj.PersonCodeStr,
                                tempObj.SupplierCodeStr,tempObj.RemarkStr,
                                tempObj.ErrorMsg
                                
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if (!rightList.Any())
                    return true;
                using (var conn = db.CreateDbConnection())
                {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try
                    {

                        //删除已存在的数据
                        if (DeleteList.Any())
                        {
                            string sqlDelete = "Delete from PersonnelSupplierRelation where PartsSalesCategoryId =:PartsSalesCategoryId and PersonId=:PersonId and SupplierId=:SupplierId";
                            foreach (var item in DeleteList)
                            {
                                var command = db.CreateDbCommand(sqlDelete, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PersonId", item.PersonId));
                                command.Parameters.Add(db.CreateDbParameter("SupplierId", item.SupplierId));
                                command.ExecuteScalar();
                            }
                        }

                        //新增配件
                        if (rightList.Any())
                        {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("PersonnelSupplierRelation", "Id", new[] {
                                "PartsSalesCategoryId","PartsSalesCategoryName", "PersonId", "PersonCode","PersonName", "SupplierId",  "SupplierCode","SupplierName","Status", "CreatorId", "CreatorName", "CreateTime","Remark"
                            });
                            #endregion
                            //往数据库增加市场部信息
                            foreach (var item in rightList)
                            {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryName", item.PartsSalesCategoryName));
                                command.Parameters.Add(db.CreateDbParameter("PersonId", item.PersonId));
                                command.Parameters.Add(db.CreateDbParameter("PersonCode", item.PersonCode));
                                command.Parameters.Add(db.CreateDbParameter("PersonName", item.PersonName));
                                command.Parameters.Add(db.CreateDbParameter("SupplierId", item.SupplierId));
                                command.Parameters.Add(db.CreateDbParameter("SupplierCode", item.SupplierCode));
                                command.Parameters.Add(db.CreateDbParameter("SupplierName", item.SupplierName));
                                command.Parameters.Add(db.CreateDbParameter("Status", item.Status));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    }
                    catch (Exception ex)
                    {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        if (conn.State == System.Data.ConnectionState.Open)
                        {
                            conn.Close();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            }
            finally
            {
                errorData = errorList;
            }
        }

        /// <summary>
        /// 导出人员与供应商关系
        /// </summary>
        public bool ExportPersonnelSupplierRelation(int[] ids, int? partsSalesCategoryId, string personName, string supplierName, int? status, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, out string fileName)
        {
            fileName = GetExportFilePath("人员与供应商关系.xlsx");
            try
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);

                using (var conn = db.CreateDbConnection())
                {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select a.PartsSalesCategoryName,
                                               a.SupplierCode,
                                               a.SupplierName,
                                               a.PersonCode,
                                               a.PersonName,
                                               (select value
                                                  from keyvalueitem
                                                 where NAME = 'BaseData_Status'
                                                   and key = a.Status) As Status,
                                               a.CreatorName,
                                               a.CreateTime,
                                               a.ModifierName,
                                               a.ModifyTime,
                                               a.AbandonerName,
                                               a.AbandonTime,
                                               a.Remark
                                          FROM PersonnelSupplierRelation a
                                          LEFT JOIN PartsSalesCategory b
                                            ON a.partssalescategoryid = b.Id
                                          LEFT JOIN PartsSupplier c
                                            ON a.supplierid = c.Id
                                          LEFT JOIN Personnel d
                                            ON a.personid = d.id
                                         WHERE 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        #region 条件过滤
                        if (partsSalesCategoryId.HasValue)
                        {
                            sql.Append(" and a.partsSalesCategoryId = {0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId));
                        }
                        if (!string.IsNullOrEmpty(personName))
                        {
                            sql.Append(" and a.PersonName like {0}PersonName ");
                            dbParameters.Add(db.CreateDbParameter("PersonName", "%" + personName + "%"));
                        }
                        if (!string.IsNullOrEmpty(supplierName))
                        {
                            sql.Append(" and a.SupplierName like {0}SupplierName ");
                            dbParameters.Add(db.CreateDbParameter("SupplierName", "%" + supplierName + "%"));
                        }
                        if (status.HasValue)
                        {
                            sql.Append(" and a.Status = {0}Status ");
                            dbParameters.Add(db.CreateDbParameter("Status", status));
                        }
                        if (createTimeBegin.HasValue)
                        {
                            sql.Append(@" and a.createTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue)
                        {
                            sql.Append(@" and a.createTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if (modifyTimeBegin.HasValue)
                        {
                            sql.Append(@" and a.ModifyTime >=to_date({0}modifyTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeBegin", tempTime.ToString("G")));
                        }
                        if (modifyTimeEnd.HasValue)
                        {
                            sql.Append(@" and a.ModifyTime <=to_date({0}modifyTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeEnd", tempTime.ToString("G")));
                        }
                        #endregion
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using (var excelExport = new ExcelExport(fileName))
                    {
                        excelExport.ExportByRow(index =>
                        {
                            if (index == 0)
                            {
                                return new object[] {
                                    ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,ErrorStrings.Export_Title_PersonnelSupplierRelation_UserName,ErrorStrings.Export_Title_PersonnelSupplierRelation_PersonName,ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,ErrorStrings.Export_Title_PartsBranch_Remark
                                };
                            }
                            if (reader.Read())
                            {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if (num != reader.FieldCount)
                                {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
