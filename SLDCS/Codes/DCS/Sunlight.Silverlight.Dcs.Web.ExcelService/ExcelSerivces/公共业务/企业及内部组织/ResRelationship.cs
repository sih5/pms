﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using System.Data.Common;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService
{
    public partial class ExcelService{
        public bool Export责任关系绑定(int[] ids, string resType, int? resTem, int? status, DateTime? bCreateTime, DateTime? eCreateTime, out string fileName) {
            fileName = GetExportFilePath("责任关系绑定导出.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userInfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                        sql.Append(@"select 
                                       fr.ResType,
                                       kv1.value,
                                       kv.value,
                                       fr.CreatorName,
                                       fr.CreateTime,fr.ModifierName,fr.ModifyTime,
                                       fr.AbandonerName,
                                       fr.AbandonTime
                                  from ResRelationship fr
                                  join keyvalueitem kv
                                    on fr.status = kv.key
                                   and kv.name = 'BaseData_Status'
                                  join keyvalueitem kv1
                                    on fr.ResTem = kv1.key
                                   and kv1.name = 'ResponsibleMembersResTem'
                                   where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and fr.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(status.HasValue) {
                            sql.Append(@" and fr.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
         
                        if(!string.IsNullOrEmpty(resType)) {
                            sql.Append("and fr.resType like {0}resType ");
                            dbParameters.Add(db.CreateDbParameter("resType", "%" + resType + "%"));
                        }

                        if(resTem.HasValue) {
                            sql.Append(@" and fr.resTem = {0}resTem ");
                            dbParameters.Add(db.CreateDbParameter("resTem", resTem.Value));
                        }

                       


                        if(bCreateTime.HasValue) {
                            sql.Append(@" and fr.CreateTime >=To_date({0}bStartTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bCreateTime", tempTime.ToString("G")));
                        }
                        if(eCreateTime.HasValue) {
                            sql.Append(@" and fr.CreateTime <=To_date({0}eStartTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("eCreateTime", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "责任类型","责任组","状态",
                                    ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,"修改人","修改时间",ErrorStrings.Export_Title_PartsBranch_AbandonerName, ErrorStrings.Export_Title_PartsBranch_AbandonerTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        /// <summary>
        /// 导入责任关系绑定
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool Import责任关系绑定(string fileName, out int excelImportNum, out List<ResRelationshipExtend> rightData, out List<ResRelationshipExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<ResRelationshipExtend>();
            var allList = new List<ResRelationshipExtend>();
            var rightList = new List<ResRelationshipExtend>();
            var userInfo = Utils.GetCurrentUserInfo();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("ResRelationship", out notNullableFields, out fieldLenght);


                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如  
                    excelOperator.AddColumnDataSource("责任类型", "ResType");
                    excelOperator.AddColumnDataSource("责任组", "ResTem");                    
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("ResTem", "ResponsibleMembersResTem") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);
                    #endregion
                    #region
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new ResRelationshipExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"].Trim();

                        tempImportObj.ResTypeStr = newRow["ResType"].Trim();
                        tempImportObj.ResTemStr = newRow["ResTem"].Trim();
                       

                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //价格上限检查
                        var fieldIndex = notNullableFields.IndexOf("ResType".ToUpper());
                        if(newRow["ResType"] == null) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("责任类型不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.ResTypeStr) > fieldLenght["ResType".ToUpper()])
                                tempErrorMessage.Add("责任类型过长");
                            tempImportObj.ResType = tempImportObj.ResTypeStr;
                        }
                        //责任组
                        if(string.IsNullOrEmpty(tempImportObj.ResTemStr)) {
                            tempErrorMessage.Add("责任组不能为空");
                        } else {
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("ResTem", tempImportObj.ResTemStr);
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add("责任组不正确");
                            }
                            tempImportObj.ResTem = tempEnumValue;
                        }
                       
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    ////文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.ResTypeStr,
                        r.ResTemStr
                    }).Where(r => r.Count() > 1);
                    if(groups.Any()) {
                        foreach(var groupItem in groups.SelectMany(r => r)) {
                            if(groupItem.ErrorMsg == null) {
                                groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                            } else {
                                groupItem.ErrorMsg = groupItem.ErrorMsg + "," + ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                            }
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //责任类型是否已存在

                    var resTypeCheck = tempRightList.Select(r => r.ResTypeStr).Distinct().ToArray();

                    var dbCompanys = new List<ResRelationshipExtend>();

                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new ResRelationshipExtend {
                            Id = Convert.ToInt32(value[0]),
                            ResType = value[1],
                            ResTem = Convert.ToInt32(value[2]) 
                        };
                        dbCompanys.Add(dbObj);
                        return false;
                    };

                    db.QueryDataWithInOperator("select Id,ResType,ResTem  from ResRelationship where status=1 ", "ResType", false, resTypeCheck, getDbCompanys);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbCompanys.FirstOrDefault(v => v.ResType == tempRight.ResTypeStr);
                        if(repairItem != null) {
                            tempRight.ErrorMsg = "责任类型已存在";
                            continue;
                        }
                       
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                    #endregion
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                        #region 设置错误信息导出的列的值
                        tempObj.ResTypeStr,
                        tempObj.ResTemStr,
                        tempObj.ErrorMsg
                        #endregion                     
                    };
                            return values;
                        });
                    }
                    errorList = null;
                }
                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("ResRelationship", "Id", new[] {
                                "ResType","ResTem","Status",  "CreatorId","CreatorName", "CreateTime" 
                            });
                            #endregion
                            //往数据库增加市场部信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("ResType", item.ResType));
                                command.Parameters.Add(db.CreateDbParameter("ResTem", item.ResTem));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                command.ExecuteNonQuery();
                                #endregion
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception ex) {
                        //报错回滚
                        ts.Rollback();
                        throw new Exception(ex.Message);
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
