﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        //合并导出责任组人员维护主清单
        public bool ExportResponsibleMembersWithDetails(int? id, int? resTem,  int? status, out string fileName) {
            fileName = GetExportFilePath("责任组人员维护主清单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userinfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {

                    conn.Open();
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"select (select value
                                              from keyvalueitem kv
                                             where kv.name = 'ResponsibleMembersResTem'
                                               and kv.key = cs.ResTem) as ResTem, /*责任组*/
                                           (select value
                                              from keyvalueitem kv
                                             where kv.name = 'BaseData_Status'
                                               and kv.key = cs.Status) as Status, /*状态*/
                                           cs.creatorname, /*创建人*/
                                           cs.createtime, /*创建时间*/
                                           cs.modifiername, /*修改人*/
                                           cs.modifytime, /*修改时间*/
                                           cs.abandonername, /*作废人*/
                                           cs.abandontime, /*作废时间*/
                                           csd.PersonelCode, /*人员编号*/
                                           csd.PersonelName /*人员名称*/
                                      from ResponsibleMembers cs
                                      join ResponsibleDetail csd
                                        on cs.id = csd.ResponMemberId
                                     where 1 = 1 ");
                    var tableNames = sql.ToString().GetRegexNames();
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(" and cs.id = {0}id");
                        dbParameters.Add(db.CreateDbParameter("id", id.Value));
                    } else {

                        if(resTem.HasValue) {
                            sql.Append(@" and cs.ResTem={0}ResTem");
                            dbParameters.Add(db.CreateDbParameter("ResTem", resTem.Value));
                        }
                       

                        if(status.HasValue) {
                            sql.Append(@" and cs.status={0}status");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }

                        
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0)
                                return tableNames;
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }

        }
    }
}
