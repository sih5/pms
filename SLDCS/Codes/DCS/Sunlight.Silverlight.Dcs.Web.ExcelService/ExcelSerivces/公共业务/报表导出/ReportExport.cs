﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Microsoft.VisualBasic;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public void ExportPartsOrderConfirm(List<string> connStrings, string branchCode, int? partsSalesCategoryId, int? warehouseId, string partsSalesOrderCode, string spareCode, string spareName, string dealerCode, string dealerName, string marketingDepartmentName, DateTime? submitTimeStart, DateTime? submitTimeEnd, out string fileName) {
            fileName = GetExportFilePath("订单审核缺口统计_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                //新增下载信息
                int id = this.AddReportDownloadMsg("订单审核缺口统计报表", branchCode, fileName);

                var sql = new StringBuilder();
                #region 查询sql
                sql.Append(@"select    ss.SaleOrderCode, ---------------------------销售订单编码
                                           ss.PartsSalesOrderTypeName,
                                           ss.ReferenceCode,     ----------------------零部件图号
                                           ss.businesscode,
                                           ss.businessname,
                                           ss.Submitcompanycode,------------------------服务站编号
                                           ss.submitcompanyname,------------------------服务站名称
                                           ss.salescategoryname,
                                           --ss.BranchCode,
                                           --ss.partid,        ---------------------------配件Id
                                           ss.SpareCode,     ---------------------------配件图号
                                           ss.SpareName,     ---------------------------配件名称
                                           ss.SubmitTime,
                                           ss.ApproveTime,
                                           ss.mdtname,
                                           --ss.warehouseid,
                                           ss.WarehouseCode,    ------------------------仓库编码
                                           ss.WarehouseName,    ------------------------仓库名称
                                           case when ss.quantity is null then 0 else ss.quantity end as quantity,         ------------------------库存
                                           case when ss.AvailableStock is null then 0 else ss.AvailableStock end as AvailableStock,   ------------------------可用库存
                                           case when ss.TransferStock is null then 0 else ss.TransferStock end as TransferStock,    ------------------------调拨在途数量
                                           case when ss.SupplyStock is null then 0 else ss.SupplyStock end as SupplyStock,      ------------------------采购在途数量
                                           case when ss.SumOrderAmount is null then 0 else ss.SumOrderAmount end as SumOrderAmount,   ------------------------订单未满足数量
                                           case when ss.ReplaceStock is null then 0 else ss.ReplaceStock end as ReplaceStock,     ------------------------替换件可用库存
                                           case when ss.ReplaceSupplyStock is null then 0 else ss.ReplaceSupplyStock end as ReplaceSupplyStock,-----------------------替换件采购在途数量
                                           case when sum(ExchangeStocks.quantity) is null then 0 else sum(ExchangeStocks.quantity) end as ExchangeStock,-----------------互换件可用库存
                                           case when sum(ExchangeSupply.amount) is null then 0 else sum(ExchangeSupply.amount) end as ExchangeSupplyStock--------------互换件采购在途数量
                                           --ss.salescategoryid,
                                      from (select psos.BranchId,   ---------------------------分公司Id
                                                   psos.BranchCode,
                                                   psos.sparepartid as partid, ---------------------------配件Id
                                                   psos.SaleOrderCode,   ---------------------------销售订单编码
                                                   psos.sparepartcode as SpareCode, ---------------------------配件图号
                                                   psos.sparepartname as SpareName, ---------------------------配件名称
                                                   psos.Submitcompanycode,      ------------------------服务站编号
                                                   psos.submitcompanyname,      ------------------------服务站名称
                                                   psos.warehouseid,
                                                   psos.WarehouseCode,          ------------------------仓库编码
                                                   psos.WarehouseName,          ------------------------仓库名称
                                                   psos.ReferenceCode,
                                                   psos.PartsSalesOrderTypeName,
                                                   psos.SubmitTime,
                                                   psos.ApproveTime,
                                                   psos.mdtname,
                                                   psos.salescategoryid,
                                                   psos.salescategoryname,
                                                   psos.businesscode,
                                                   psos.businessname,
                                                   sum(nvl(ps.quantity, 0)) as quantity,----------------库存
                                                   sum(nvl(ps.AvailableStock, 0)) as AvailableStock,------------------------可用库存
                                                   sum(psos.SumOrderAmount) as SumOrderAmount,  -----------------订单未满足数量
                                                   sum((select sum(nvl(nvl(pobd.outboundamount, 0) - nvl(picbd.Inspectedquantity, 0),0))
                                                         from PartsTransferOrder pto  --------------------配件调拨单
                                                        inner join PartsOutboundBill pob  ----------------配件出库单
                                                           on pob.Originalrequirementbillcode = pto.code and pob.outboundtype = 4
                                                        inner join PartsOutboundBillDetail pobd ----------配件出库单清单
                                                           on pobd.partsoutboundbillid = pob.id
                                                         left join PartsInboundCheckBill picb   ----------配件入库检验单
                                                           on picb.Originalrequirementbillcode = pto.code and picb.inboundtype = 3
                                                         left join PartsInboundCheckBillDetail picbd ----------配件入库检验清单
                                                           on picbd.PartsInboundCheckBillId = picb.id and picbd.sparepartid=pobd.sparepartid where pobd.sparepartid = psos.sparepartid
                                                        group by pobd.sparepartid)) TransferStock,------------------------调拨在途数量
                                                   sum((select sum(nvl(ppod.ConfirmedAmount, 0) - nvl(pbcb.InspectedQuantity, 0))
                                                          from PartsPurchaseOrder ppo       ------------------------配件采购订单
                                                         inner join PartsPurchaseOrderDetail ppod  -----------------配件采购清单
                                                            on ppo.id = ppod.partspurchaseorderid
                                                          left join (select pb.originalrequirementbillcode,pc.sparepartid,sum(pc.inspectedquantity) as InspectedQuantity
                                                                       from partsinboundcheckbill pb ---------------配件入库检验单
                                                                      inner join partsinboundcheckbilldetail pc ----------配件入库检验清单
                                                                         on pc.partsinboundcheckbillid = pb.id where pb.inboundtype = 1
                                                                      group by pb.originalrequirementbillcode, pc.sparepartid) pbcb
                                                            on pbcb.originalrequirementbillcode = ppo.code
                                                           and pbcb.sparepartid = ppod.sparepartid
                                                         where ppo.status in (3, 4, 5, 6)
                                                         and psos.sparepartid=ppod.sparepartid
                                                         group by ppod.sparepartid
                                                        )) as SupplyStock,------------------------采购在途数量
                                                   sum((select sum(nvl(psk.quantity, 0) - nvl(plsk.lockedquantity, 0))
                                                         from (select a.warehouseid,a.partid,sum(a.quantity)as quantity from partsstock a where  WarehouseAreaCategoryId<>2 and storagecompanyid in (select id from branch) group by a.warehouseid,a.partid
                                                          ) psk --------------配件库存
                                                        inner join PartsReplacement b ------------配件替换信息
                                                           on psk.partid = b.newpartid
                                                        inner join warehouse ws   --------------仓库
                                                           on ws.id = psk.warehouseid /*and ws.Iscentralizedpurchase = 1*/
                                                        inner join SalesUnitAffiWarehouse sw
                                                         on sw.warehouseid=ws.id
                                                         inner join salesunit st
                                                         on st.id=sw.salesunitid
                                                         left join PartsLockedStock plsk  ---------------配件锁定库存
                                                           on ws.id = plsk.warehouseid and psk.partid = plsk.partid
                                                        where b.OldPartId = psos.sparepartid
                                                        and st.partssalescategoryid=psos.salescategoryid
                                                ---        group by psk.partid
                                                )) ReplaceStock,------------------------替换件可用库存
                                                   sum((select sum(nvl(ppod.ConfirmedAmount, 0) - nvl(pbcb.InspectedQuantity, 0))
                                                         from PartsPurchaseOrder ppo ------------------配件采购订单
                                                        inner join PartsPurchaseOrderDetail ppod  ------------------配件采购清单
                                                           on ppo.id = ppod.partspurchaseorderid
                                                        inner join PartsReplacement b -------------------配件替换信息
                                                           on ppod.sparepartid = b.newpartid
                                                         left join(select pb.originalrequirementbillcode,pc.sparepartid,sum(pc.inspectedquantity) as InspectedQuantity
                                                     from partsinboundcheckbill pb   ---------------------配件入库检验单
                                                        inner join partsinboundcheckbilldetail pc
                                                           on pc.partsinboundcheckbillid = pb.id where pb.inboundtype = 1
                                                        group by pb.originalrequirementbillcode, pc.sparepartid) pbcb
                                                        on pbcb.originalrequirementbillcode = ppo.code and pbcb.sparepartid = ppod.sparepartid
                                                          where ppo.status in (3, 4, 5,6) and b.OldPartId = psos.sparepartid
                                                ---        group by ppod.sparepartid
                                                )) as ReplaceSupplyStock -----------替换件采购在途数量
                                              from (select pso.branchid,---------------------------分公司Id
                                                           pso.BranchCode,
                                                           pso.warehouseid,------------------------仓库ID
                                                           pso.code as SaleOrderCode,---------------------------销售订单编码
                                                           w.code as WarehouseCode, ------------------------仓库编码
                                                           w.name as WarehouseName,------------------------仓库名称
                                                           pso.Submitcompanycode,------------------------服务站编号
                                                           pso.submitcompanyname,------------------------服务站名称
                                                           psod.sparepartid,    -------------------------配件ID
                                                           psod.sparepartcode,  -------------------------配件图号
                                                           psod.sparepartname,  -------------------------配件名称
                                                           sum(OrderedQuantity - nvl(ApproveQuantity, 0)) as SumOrderAmount,   ------------------------订单未满足数量
                                                           mdt.name as mdtname, ----------------------市场部名称
                                                           sp.ReferenceCode,     ----------------------零部件图号
                                                           pso.PartsSalesOrderTypeName,
                                                           pso.SubmitTime,
                                                           pso.ApproveTime,
                                                           pso.salescategoryid,
                                                           pso.salescategoryname,
                                                           dsi.businesscode,
                                                           dsi.businessname
                                                      from PartsSalesOrder pso   ---------------配件销售订单
                                                       left join (select s.businesscode, s.businessname, a.id dealerid,null MarketingDepartmentId,s.partssalescategoryid
                                      from SalesUnit s
                                     inner join Agency a
                                        on a.id = s.OwnerCompanyId
                                    union all
                                    select businesscode, businessname, dealerid,MarketingDepartmentId,partssalescategoryid from dealerserviceinfo
                                     inner join company
                                                   on company.id=dealerserviceinfo.dealerid
                                                   where company.type=2 and dealerserviceinfo.status<>99) dsi
                                    on pso.SubmitCompanyId = dsi.DealerId and pso.SalesCategoryId = dsi.partssalescategoryid
                                                       left join marketingdepartment mdt on dsi.MarketingDepartmentId = mdt.id
                                                     inner join partssalesorderdetail psod  ----------------------配件销售订单清单
                                                        on pso.id = psod.partssalesorderid
                                                     inner join sparepart sp
                                                            on psod.sparepartid=sp.id
                                                    /* inner join PartsBranch pb
                                                        on pb.partid = psod.sparepartid and pb.Partssalescategoryid in
                                                           (select id from partssalescategory )*/
                                                     inner join warehouse w on w.id = pso.warehouseid where pso.status in (2,4) and pso.SalesUnitOwnerCompanycode in (select code from branch) and OrderedQuantity - nvl(ApproveQuantity, 0)<>0");
                //处理拼接过滤查询条件
                if(!string.IsNullOrEmpty(branchCode))
                    sql.Append(@" and pso.BranchCode='" + branchCode + "'");
                if(partsSalesCategoryId.HasValue)
                    sql.Append(@" and pso.Salescategoryid=" + partsSalesCategoryId.Value + "");
                if(warehouseId.HasValue)
                    sql.Append(@" and pso.Warehouseid=" + warehouseId.Value + "");
                if(!string.IsNullOrEmpty(partsSalesOrderCode))
                    sql.Append(@" and pso.Code='" + partsSalesOrderCode + "'");
                if(!string.IsNullOrEmpty(spareCode))
                    sql.Append(@" and psod.SparePartCode='" + spareCode + "'");
                if(!string.IsNullOrEmpty(spareName))
                    sql.Append(@" and psod.SparePartName='" + spareName + "'");
                if(!string.IsNullOrEmpty(dealerCode))
                    sql.Append(@" and pso.SubmitCompanyCode='" + dealerCode + "'");
                if(!string.IsNullOrEmpty(dealerName))
                    sql.Append(@" and pso.submitCompanyName='" + dealerName + "'");
                if(!string.IsNullOrEmpty(marketingDepartmentName))
                    sql.Append(@" and mdt.Name='" + marketingDepartmentName + "'");
                if(submitTimeStart.HasValue) {
                    sql.Append(@" and pso.submittime >=to_date('" + submitTimeStart.Value + "','yyyy-mm-dd hh24:mi:ss')");
                }
                if(submitTimeEnd.HasValue) {
                    sql.Append(@" and pso.submittime <=to_date('" + submitTimeEnd.Value + "','yyyy-mm-dd hh24:mi:ss')");
                }

                sql.Append(@" group by pso.branchid,
                                                              pso.BranchCode,
                                                              pso.warehouseid,
                                                              pso.code ,
                                                              w.code,
                                                              w.name,
                                                              pso.Submitcompanycode,
                                                              pso.submitcompanyname,
                                                              psod.sparepartid,
                                                              psod.sparepartcode,
                                                              psod.sparepartname,
                                                              pso.Partssalesordertypename,
                                                              pso.SubmitTime,
                                                              pso.ApproveTime,
                                                              pso.salescategoryid,
                                                              pso.salescategoryname,
                                                              mdt.name,
                                                              sp.ReferenceCode,
                                                              dsi.businesscode,
                                                              dsi.businessname) psos
                                              left join (select p.partid,
                                                                p.warehouseid,
                                                                st.partssalescategoryid,
                                                               sum(p.quantity) as quantity,
                                                               sum(p.quantity - nvl(plsk.lockedquantity, 0))as  AvailableStock
                                                          from (select a.warehouseid,a.partid,sum(a.quantity)as quantity from partsstock a where  WarehouseAreaCategoryId<>2 and storagecompanyid in (select id from branch) group by a.warehouseid,a.partid
                                                          ) p
                                                         inner join warehouse ws on ws.id = p.warehouseid --and ws.Iscentralizedpurchase = 1
                                                          left join PartsLockedStock plsk on ws.id = plsk.warehouseid and p.partid = plsk.partid
                                                         inner join SalesUnitAffiWarehouse sw
                                                         on sw.warehouseid=ws.id
                                                         inner join salesunit st
                                                         on st.id=sw.salesunitid
                                                         group by p.partid,p.warehouseid, st.partssalescategoryid) ps
                                                on psos.sparepartid = ps.partid
                                                and psos.warehouseid=ps.warehouseid
                                                and psos.salescategoryid=ps.partssalescategoryid/*
                                                  where ps.quantity-psos.SumOrderAmount<0*/
                                             group by psos.branchid,
                                                      psos.branchcode,
                                                      psos.sparepartid,
                                                      psos.SaleOrderCode,
                                                      psos.sparepartcode,
                                                      psos.sparepartname,
                                                      psos.Submitcompanycode,
                                                      psos.submitcompanyname,
                                                      psos.warehouseid,
                                                      psos.WarehouseCode,
                                                      psos.WarehouseName,
                                                      psos.ReferenceCode,
                                                      psos.PartsSalesOrderTypeName,
                                                      psos.SubmitTime,
                                                      psos.ApproveTime,
                                                      psos.mdtname,
                                                      psos.salescategoryid,
                                                      psos.salescategoryname,
                                                      psos.businesscode,
                                                      psos.businessname
                                            ) ss  --------ss---------------------------------------------------------------------
                                      left join   (select dd.quantity,pes.partid,dd.partssalescategoryid from (select sum(nvl(psks.quantity, 0)) - sum(nvl(plsk.lockedquantity, 0)) as quantity,pe.exchangecode,psks.partssalescategoryid
                                                   from (select sum(nvl(quantity, 0)) quantity,
                                                           partsstock.warehouseid,
                                                           partsstock.StorageCompanyType,
                                                           partsstock.partid,
                                                           st.partssalescategoryid
                                                      from partsstock
                                                       inner join SalesUnitAffiWarehouse sw
                                                         on sw.warehouseid=partsstock.warehouseid
                                                         inner join salesunit st
                                                         on st.id=sw.salesunitid
                                                      where  WarehouseAreaCategoryId<>2 and storagecompanyid in (select id from branch)
                                                     group by partsstock.warehouseid, partsstock.StorageCompanyType, partsstock.partid, st.partssalescategoryid)  psks ------------------------配件库存
                                                   inner join PartsExchange pe -----------------配件互换信息
                                                         on pe.partid = psks.partid
                                                   inner join warehouse ws on ws.id = psks.warehouseid and ws.Storagecompanytype = 1 /*and ws.iscentralizedpurchase=1*/
                                                   left join PartsLockedStock plsk on ws.id = plsk.warehouseid and psks.partid = plsk.partid
                                                   where psks.StorageCompanyType = 1
                                                   group by pe.exchangecode,psks.partssalescategoryid) dd inner join PartsExchange pes on pes.exchangecode = dd.exchangecode
                                                   and pes.status = 1)ExchangeStocks -----ExchangeStocks互换件可用
                                                  on ss.partid = ExchangeStocks.partid
                                                  and ExchangeStocks.partssalescategoryid=ss.salescategoryid
                                      left join (select sum(nvl(ppod.ConfirmedAmount, 0) - nvl(pbcb.InspectedQuantity, 0)) as amount,ppod.sparepartid
                                                  from PartsPurchaseOrder ppo  ------------------配件采购订单
                                                inner join PartsPurchaseOrderType ppot on ppo.PartsPurchaseOrderTypeId = ppot.id
                                                  inner join PartsPurchaseOrderDetail ppod   ---------------------配件采购订单清单
                                                        on ppo.id = ppod.partspurchaseorderid
                                                   left join(select pb.originalrequirementbillcode,pc.sparepartid,sum(pc.inspectedquantity) as InspectedQuantity
                                                        from partsinboundcheckbill pb ----------------------配件入库检验单
                                                        inner join partsinboundcheckbilldetail pc ---------------------配件入库检验单清单
                                                        on pc.partsinboundcheckbillid = pb.id
                                                        where pb.inboundtype = 1
                                                        group by pb.originalrequirementbillcode, pc.sparepartid) pbcb
                                                  on pbcb.originalrequirementbillcode = ppo.code and pbcb.sparepartid = ppod.sparepartid
                                                  where ppo.status in (3, 4, 5,6)
                                                  group by ppod.sparepartid) ExchangeSupply ----------------ExchangeSupply互换件采购在途数量-------------------------------
                                        on ExchangeStocks.partid = ExchangeSupply.sparepartid
                                     group by ss.BranchId,
                                              ss.BranchCode,
                                              ss.partid,
                                              ss.SaleOrderCode,
                                              ss.SpareCode,
                                              ss.SpareName,
                                              ss.Submitcompanycode,
                                              ss.submitcompanyname,
                                              ss.warehouseid,
                                              ss.WarehouseCode,
                                              ss.WarehouseName,
                                              ss.quantity,
                                              ss.AvailableStock,
                                              ss.TransferStock,
                                              ss.SupplyStock,
                                              ss.SumOrderAmount,
                                              ss.ReplaceStock,
                                              ss.ReplaceSupplyStock,
                                              ss.mdtname,
                                              ss.ReferenceCode,
                                              ss.PartsSalesOrderTypeName,
                                              ss.SubmitTime,
                                              ss.ApproveTime,
                                              ss.salescategoryid,
                                              ss.salescategoryname,
                                              ss.businesscode,
                                              ss.businessname");

                #endregion

                var sqlStr = sql.ToString();


                List<DbDataReader> readers = new List<DbDataReader>();
                List<DbConnection> conns = new List<DbConnection>();


                foreach(var connString in connStrings) {
                    var db = DbHelper.GetDbHelp(connString);
                    var conn = db.CreateDbConnection();
                    conn.Open();
                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    var reader = command.ExecuteReader();
                    readers.Add(reader);
                    conns.Add(conn);
                }


                using(var excelExport = new ExcelExport(fileName)) {
                    excelExport.ExportByRow(index => {
                        if(index == 0) {
                            return new object[] {
                            ErrorStrings.Export_Title_SupplierShippingOrder_SalesOrderCode,ErrorStrings.Export_Title_PartsPurchaseOrder_OrderType,ErrorStrings.Export_Title_PartsBranch_Referencecode,ErrorStrings.Export_Title_BranchSupplierRelation_BusinessCode,ErrorStrings.Export_Title_BranchSupplierRelation_BusinessName,ErrorStrings.Export_Title_Dealer_Code,ErrorStrings.Export_Title_Dealer_Name,ErrorStrings.Export_Title_Partssalescategory_Name,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_PartsBranch_Code,
                            "提报时间",ErrorStrings.Export_Title_Dealerpartsinventorybill_ApproverTime,"市场部",ErrorStrings.Export_Title_Company_WarehouseCode,ErrorStrings.Export_Title_Company_WarehouseName,ErrorStrings.Export_Title_DealerPartsStockQueryView_Quantity,"可用库存","调拨在途数量","采购在途数量","订单未满足数量","替换件可用库存","替换件采购在途数量","互换件可用库存","互换件采购在途数量"
                         };
                        }
                        foreach(var reader in readers) {
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                        }
                        return null;
                    });
                }

                //关闭连接和reader
                foreach(var conn in conns)
                    conn.Close();
                foreach(var reader in readers) {
                    if(!reader.IsClosed)
                        reader.Close();
                }
                //更新下载信息
                UpdateReportDownloadMsg(id);

            } catch(Exception ex) {
                throw ex;
            }
        }

        //新增下载记录
        private int AddReportDownloadMsg(string nodeName, string queryCriteria, string filePath) {
            var db = DbHelper.GetDbHelp(ConnectionString);
            int id = 0;
            int pageId = this.GetPageId(nodeName);
            using(var conn = db.CreateDbConnection()) {
                conn.Open();
                //开启事务，新增更新配件在一个事务内
                var ts = conn.BeginTransaction();
                try {
                    //新增下载记录
                    //获取新增数据的sql语句，Id为主键
                    var sqlInsertReportDownloadMsg = db.GetInsertSql("ReportDownloadMsg", "Id", new[] {
                                "CompanyId","CompanyCode","CompanyName","FilePath", "NodeName", "NodeId", "QueryCriteria", "Status", "CreatorId", "CreatorName", "CreateTime"
                            });

                    var userInfo = Utils.GetCurrentUserInfo();

                    var commandInsertSql = db.CreateDbCommand(sqlInsertReportDownloadMsg, conn, ts);
                    commandInsertSql.Parameters.Add(db.CreateDbParameter("CompanyId", userInfo.EnterpriseId));
                    commandInsertSql.Parameters.Add(db.CreateDbParameter("CompanyCode", userInfo.EnterpriseCode));
                    commandInsertSql.Parameters.Add(db.CreateDbParameter("CompanyName", userInfo.EnterpriseName));
                    commandInsertSql.Parameters.Add(db.CreateDbParameter("FilePath", filePath));
                    commandInsertSql.Parameters.Add(db.CreateDbParameter("NodeName", nodeName));
                    commandInsertSql.Parameters.Add(db.CreateDbParameter("NodeId", pageId));
                    commandInsertSql.Parameters.Add(db.CreateDbParameter("QueryCriteria", queryCriteria));
                    commandInsertSql.Parameters.Add(db.CreateDbParameter("Status", (int)DcsDownloadStatus.生成中));
                    commandInsertSql.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                    commandInsertSql.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                    commandInsertSql.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                    id = db.ExecuteInsert(commandInsertSql, "Id");
                    //无异常提交
                    ts.Commit();
                } catch(Exception ex) {
                    //报错回滚
                    ts.Rollback();
                    throw new Exception(ex.Message);
                } finally {
                    if(conn.State == System.Data.ConnectionState.Open) {
                        conn.Close();
                    }
                }

                return id;
            }
        }
        //更新下载记录
        private void UpdateReportDownloadMsg(int id) {
            var db = DbHelper.GetDbHelp(ConnectionString);
            using(var conn = db.CreateDbConnection()) {
                conn.Open();
                //开启事务，新增更新配件在一个事务内
                var ts = conn.BeginTransaction();
                try {
                    //根据Id更新状态
                    var sqlUpdateReportDownloadMsg = db.GetUpdateSql("ReportDownloadMsg", new string[] { "Status", "ModifierId", "ModifierName", "ModifyTime" }, new string[] { "Id" });

                    var userInfo = Utils.GetCurrentUserInfo();

                    var commandPartsBranch = db.CreateDbCommand(sqlUpdateReportDownloadMsg, conn, ts);
                    commandPartsBranch.Parameters.Add(db.CreateDbParameter("Status", (int)DcsDownloadStatus.未下载));
                    commandPartsBranch.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                    commandPartsBranch.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                    commandPartsBranch.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                    commandPartsBranch.Parameters.Add(db.CreateDbParameter("Id", id));
                    commandPartsBranch.ExecuteNonQuery();
                    //无异常提交
                    ts.Commit();
                } catch(Exception ex) {
                    //报错回滚
                    ts.Rollback();
                    throw new Exception(ex.Message);
                } finally {
                    if(conn.State == System.Data.ConnectionState.Open) {
                        conn.Close();
                    }
                }
            }
        }

        //获取PageId
        private int GetPageId(string pageName) {
            var db = DbHelper.GetDbHelp(SecurityConnectionString);
            var dbBranches = new List<BranchExtend>();
            Func<string[], bool> getDbBranches = value => {
                var dbObj = new BranchExtend {
                    Id = Convert.ToInt32(value[0])
                };
                dbBranches.Add(dbObj);
                return false;
            };
            db.QueryDataWithInOperator("select Id from Page where status=2 ", "Name", true, new string[] { pageName }, getDbBranches);

            return dbBranches.FirstOrDefault().Id;
        }

    }
}