﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportBottomStockSubVersion(int[] ids, int? status, string subVersionCode, string colVersionCode, string sparePartCode, string sparePartName, string companyCode, string companyName, DateTime? bStartTime, DateTime? eStartTime, int? companyType, out string fileName) {
            fileName = GetExportFilePath("配件保底库存明细子集版本号.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userInfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select kv.value,
                                   fr.companycode,
                                   fr.companyname,
                                   p.value,
                                   fr.sparepartcode,
                                   fr.sparepartname,
                                   fr.subversioncode,
                                   fr.colversioncode,
                                   fr.ReserveType,
                                   fr.saleprice,
                                   fr.reservefee,
                                   fr.reserveqty,
                                   fr.starttime,
                                   fr.endtime
                              from BottomStockSubVersion fr
                              join keyvalueitem kv
                                on fr.status = kv.key
                               and kv.name = 'BaseData_Status'
                              join keyvalueitem p
                                on fr.companytype = p.key
                               and p.name = 'Company_Type'
                               where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and fr.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(subVersionCode)) {
                            sql.Append("and LOWER(fr.subVersionCode) like {0}subVersionCode ");
                            dbParameters.Add(db.CreateDbParameter("subVersionCode", "%" + subVersionCode.ToLower() + "%"));
                        }

                        if(status.HasValue) {
                            sql.Append(@" and fr.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!string.IsNullOrEmpty(colVersionCode)) {
                            sql.Append("and LOWER(fr.colVersionCode) like {0}colVersionCode ");
                            dbParameters.Add(db.CreateDbParameter("colVersionCode", "%" + colVersionCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append("and LOWER(fr.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(sparePartName)) {
                            sql.Append("and fr.sparePartName like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartCode + "%"));
                        }

                        if(!string.IsNullOrEmpty(companyCode)) {
                            sql.Append(" and LOWER(fr.companyCode) like {0}companyCode");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(companyName)) {
                            sql.Append(" and fr.companyName like {0}companyName");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        if(companyType.HasValue) {
                            sql.Append(" and fr.companyType = {0}companyType");
                            dbParameters.Add(db.CreateDbParameter("companyType", companyType));
                        }

                        if(bStartTime.HasValue) {
                            sql.Append(@" and fr.StartTime >=To_date({0}bStartTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bStartTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bStartTime", tempTime.ToString("G")));
                        }
                        if(eStartTime.HasValue) {
                            sql.Append(@" and fr.createTime <=To_date({0}eStartTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eStartTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("eStartTime", tempTime.ToString("G")));
                        }
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Company_Code, ErrorStrings.Export_Title_Company_Name,ErrorStrings.Export_Title_Company_Type,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_Accessinfo_Tmp_SubVersionCode,ErrorStrings.Export_Title_Accessinfo_Tmp_ColVersionCode,ErrorStrings.Export_Title_Accessinfo_Tmp_ReserveType,ErrorStrings.Export_Title_ForceReserveBillDetail_Price, 
                                    ErrorStrings.Export_Title_ForceReserveBillDetail_Money,ErrorStrings.Export_Title_ForceReserveBillDetail_Amount,ErrorStrings.Export_Title_FactoryPurchacePrice_ValidFrom,ErrorStrings.Export_Title_FactoryPurchacePrice_ValidTo
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
        public bool ExportBottomStockColVersion(int[] ids, int? status, string colVersionCode, string sparePartCode, string sparePartName, string companyCode, string companyName, DateTime? bStartTime, DateTime? eStartTime, int? companyType, out string fileName) {
            fileName = GetExportFilePath("配件保底库存合集版本号导出.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userInfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select kv.value,
                                   fr.companycode,
                                   fr.companyname,
                                   p.value,
                                   fr.sparepartcode,
                                   fr.sparepartname,
                                   fr.colversioncode,
                                   fr.saleprice,
                                   fr.reservefee,
                                   fr.reserveqty,
                                   fr.starttime,
                                   fr.endtime
                              from BottomStockColVersion fr
                              join keyvalueitem kv
                                on fr.status = kv.key
                               and kv.name = 'BaseData_Status'
                              join keyvalueitem p
                                on fr.companytype = p.key
                               and p.name = 'Company_Type'
                               where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and fr.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(status.HasValue) {
                            sql.Append(@" and fr.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!string.IsNullOrEmpty(colVersionCode)) {
                            sql.Append("and LOWER(fr.colVersionCode) like {0}colVersionCode ");
                            dbParameters.Add(db.CreateDbParameter("colVersionCode", "%" + colVersionCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(sparePartCode)) {
                            sql.Append("and LOWER(fr.sparePartCode) like {0}sparePartCode ");
                            dbParameters.Add(db.CreateDbParameter("sparePartCode", "%" + sparePartCode.ToLower() + "%"));
                        }

                        if(!string.IsNullOrEmpty(sparePartName)) {
                            sql.Append("and fr.sparePartName like {0}sparePartName ");
                            dbParameters.Add(db.CreateDbParameter("sparePartName", "%" + sparePartCode + "%"));
                        }

                        if(!string.IsNullOrEmpty(companyCode)) {
                            sql.Append(" and LOWER(fr.companyCode) like {0}companyCode");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(companyName)) {
                            sql.Append(" and fr.companyName like {0}companyName");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        if(companyType.HasValue) {
                            sql.Append(" and fr.companyType = {0}companyType");
                            dbParameters.Add(db.CreateDbParameter("companyType", companyType));
                        }

                        if(bStartTime.HasValue) {
                            sql.Append(@" and fr.StartTime >=To_date({0}bStartTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bStartTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bStartTime", tempTime.ToString("G")));
                        }
                        if(eStartTime.HasValue) {
                            sql.Append(@" and fr.StartTime <=To_date({0}eStartTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eStartTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("eStartTime", tempTime.ToString("G")));
                        }
                       
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Company_Code, ErrorStrings.Export_Title_Company_Name,ErrorStrings.Export_Title_Company_Type,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_Accessinfo_Tmp_ColVersionCode,ErrorStrings.Export_Title_ForceReserveBillDetail_Price, ErrorStrings.Export_Title_ForceReserveBillDetail_Money,ErrorStrings.Export_Title_ForceReserveBillDetail_Amount,ErrorStrings.Export_Title_FactoryPurchacePrice_ValidFrom,ErrorStrings.Export_Title_FactoryPurchacePrice_ValidTo
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
