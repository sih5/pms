﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 合并导出Csbom配件信息
        /// </summary>
        public bool ExportCsbomParts(int[] ids,string code,string name, string spmName,string englishName,string spmEnglishName,DateTime? bCreateTime, DateTime? eCreateTime, out string fileName)
        {
            fileName = GetExportFilePath("CSBOM配件信息.xlsx");

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@"select a.Code,
                           a.Name,
                           b.ReferenceCode as SPMReferenceCode,
                           a.ReferenceCode,
                           b.name        AS SPMName,
                           a.EnglishName,
                           b.EnglishName AS SPMEnglishName,
                           a.CreateTime,
                           cast ('是' as varchar2(100)) as IsUpdate 
                            from accessinfo_tmp a
                            inner join sparepart b
                            on a.code = b.code
                            where (a.name <> b.name or a.referencecode <> b.referencecode)"));

                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0)
                    {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < ids.Length; i++)
                        {
                            if (ids.Length == i + 1)
                            {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                            else
                            {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(code))
                        {
                            sql.Append("and a.Code like {0}code ");
                            dbParameters.Add(db.CreateDbParameter("code", "%" + code + "%"));
                        }
                        if (!string.IsNullOrEmpty(name))
                        {
                            sql.Append("and a.name like {0}name ");
                            dbParameters.Add(db.CreateDbParameter("name", "%" + name + "%"));
                        }
                        if (!string.IsNullOrEmpty(spmName))
                        {
                            sql.Append("and b.name like {0}spmName ");
                            dbParameters.Add(db.CreateDbParameter("spmName", "%" + spmName + "%"));
                        }
                        if (!string.IsNullOrEmpty(englishName))
                        {
                            sql.Append("and a.englishName like {0}englishName ");
                            dbParameters.Add(db.CreateDbParameter("englishName", "%" + englishName + "%"));
                        }
                        if (!string.IsNullOrEmpty(spmEnglishName))
                        {
                            sql.Append("and b.englishName like {0}spmEnglishName ");
                            dbParameters.Add(db.CreateDbParameter("spmEnglishName", "%" + spmEnglishName + "%"));
                        }
                        if (bCreateTime.HasValue)
                        {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (eCreateTime.HasValue)
                        {
                            sql.Append(@" and a.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode,ErrorStrings.Export_Title_Accessinfo_Tmp_CSBOMReferenceCode, ErrorStrings.Export_Title_Accessinfo_Tmp_CSBOMReferenceName, ErrorStrings.Export_Title_Accessinfo_Tmp_SPMName,ErrorStrings.Export_Title_Accessinfo_Tmp_CSBOMEnName, ErrorStrings.Export_Title_Accessinfo_Tmp_SPMEnName,ErrorStrings.Export_Title_Accessinfo_Tmp_TransportTime,ErrorStrings.Export_Title_Accessinfo_Tmp_IsUpdateInterFace};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
