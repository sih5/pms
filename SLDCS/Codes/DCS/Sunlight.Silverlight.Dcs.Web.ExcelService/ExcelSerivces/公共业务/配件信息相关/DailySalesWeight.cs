﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导入日均销量计算参数配置
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportDailySalesWeight(string fileName, out int excelImportNum, out List<DailySalesWeightExtend> rightData, out List<DailySalesWeightExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<DailySalesWeightExtend>();
            var allList = new List<DailySalesWeightExtend>();
            var userInfo = Utils.GetCurrentUserInfo();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("DailySalesWeight", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                List<DailySalesWeightExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource("仓库编号", "WarehouseCode");
                    excelOperator.AddColumnDataSource("仓库名称", "WarehouseName");
                    excelOperator.AddColumnDataSource("时间", "Times");
                    excelOperator.AddColumnDataSource("权重", "Weight");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;                  
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new DailySalesWeightExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.WarehouseCodeStr = newRow["WarehouseCode"];
                        tempImportObj.WarehouseNameStr = newRow["WarehouseName"];
                        tempImportObj.TimesStr = newRow["Times"];
                        tempImportObj.WeightStr = newRow["Weight"];
                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查

                        //仓库编号检查
                        var fieldIndex = notNullableFields.IndexOf("WarehouseCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.WarehouseCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("仓库编号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.WarehouseCodeStr) > fieldLenght["WarehouseCode".ToUpper()])
                                tempErrorMessage.Add("仓库编号过长");
                        }
                        //时间检查
                        fieldIndex = notNullableFields.IndexOf("Times".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.TimesStr)) {
                            if(fieldIndex > -1)
                            tempErrorMessage.Add("时间不能为空");
                        } else {
                            try {
                                var times = Convert.ToInt32(tempImportObj.TimesStr);
                                if(times <= 0 || times>168) {
                                    throw new Exception();
                                }
                            } catch(Exception) {
                                tempErrorMessage.Add("时间格式错误，必须是大于0小于等于168的整数");
                            }                            
                        }
                        //权重检查
                        fieldIndex = notNullableFields.IndexOf("Weight".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.WeightStr)) {
                            if(fieldIndex > -1)
                            tempErrorMessage.Add("权重不能为空");
                        } else {
                            if(Decimal.Parse(tempImportObj.WeightStr) < 0)
                                tempErrorMessage.Add("权重必须大于等于0");
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //4、文件中品牌名称、人员名称、供应商名称组合唯一，否则提示：数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.WarehouseCodeStr,
                        r.TimesStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //仓库编号存在于仓库表中，否则提示：仓库编号不存在
                    var WarehouseCodesNeedCheck = tempRightList.Select(r => r.WarehouseCodeStr).Distinct().ToArray();
                    var dbWarehouses = new List<WarehouseExtend>();
                    Func<string[], bool> getdbWarehouses = value => {
                        var dbObj = new WarehouseExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        };
                        dbWarehouses.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code,Name from Warehouse where status=1 ", "Code", false, WarehouseCodesNeedCheck, getdbWarehouses);
                    foreach(var tempRight in tempRightList) {
                        var repairItem = dbWarehouses.FirstOrDefault(v => v.Code == tempRight.WarehouseCodeStr);
                        if(repairItem == null) {
                            tempRight.ErrorMsg = ErrorStrings.Export_Validation_Company_WarehouseCodeIsNull;
                            continue;
                        }
                        tempRight.WarehouseId = repairItem.Id;
                        tempRight.WarehouseName = repairItem.Name;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    foreach(var rightItem in tempRightList) {
                        var timeNeedCheck = tempRightList.Where(t => t.WarehouseId == rightItem.WarehouseId && t.TimesStr == rightItem.TimesStr).Select(r => r.TimesStr).Distinct().ToArray();
                        var dbSuplierCode = new List<DailySalesWeightExtend>();
                        Func<string[], bool> getDbSuplierCode = value => {
                            var dbObj = new DailySalesWeightExtend {
                                Id = Convert.ToInt32(value[0]),
                                WarehouseId = Convert.ToInt32(value[1]),
                                Times = Convert.ToInt32(value[2])
                            };
                            dbSuplierCode.Add(dbObj);
                            return false;
                        };
                        db.QueryDataWithInOperator(string.Format(@"select id,WarehouseId,Times from DailySalesWeight where WarehouseId={0}", rightItem.WarehouseId), "Times", true, timeNeedCheck, getDbSuplierCode);
                        if(dbSuplierCode.Count() == 1) {
                            rightItem.Id = dbSuplierCode[0].Id;
                        }
                    }
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.WarehouseCode = rightItem.WarehouseCodeStr;
                        rightItem.Times = Convert.ToInt32(rightItem.TimesStr);
                        rightItem.Weight = Convert.ToDecimal(rightItem.WeightStr);
                    }
                    #endregion
                }
                rightData = allList.Except(errorList).ToList();
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值

                                tempObj.WarehouseCodeStr,tempObj.WarehouseNameStr,
                                tempObj.TimesStr,tempObj.WeightStr,
                                tempObj.ErrorMsg
                                
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    // errorList = null;
                }


                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        var insertData = rightList.Where(t => t.Id == 0).ToArray();
                        //新增配件
                        if(insertData.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("DailySalesWeight", "Id", new[] {
                                "WarehouseId","WarehouseCode","WarehouseName","Times","Weight","CreatorId","CreatorName","CreateTime"
                            });
                            #endregion
                            //往数据库增加配件信息
                            foreach(var item in insertData) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("WarehouseId", item.WarehouseId));
                                command.Parameters.Add(db.CreateDbParameter("WarehouseCode", item.WarehouseCode));
                                command.Parameters.Add(db.CreateDbParameter("WarehouseName", item.WarehouseName));
                                command.Parameters.Add(db.CreateDbParameter("Times", item.Times));
                                command.Parameters.Add(db.CreateDbParameter("Weight", item.Weight));
                                command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                #endregion
                                command.ExecuteNonQuery();
                            }
                        }
                        var updateData = rightList.Where(t => t.Id != 0).ToArray();
                        if(updateData.Any()) {
                            var sqlUpdateSparePart = db.GetUpdateSql("DailySalesWeight", new string[] {
                            "Weight","ModifierId","ModifyTime","ModifierName"
                        }, new string[] {
                            "Id"
                        });
                            foreach(var item in updateData) {
                                var command = db.CreateDbCommand(sqlUpdateSparePart, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("Weight", item.Weight));
                                command.Parameters.Add(db.CreateDbParameter("Id", item.Id));
                                command.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = allList.Except(errorList).ToList();
            }
        }
        public bool ExportDailySalesWeight(int[] ids, int? warehouseId, int? times, DateTime? createTimeBegin, DateTime? createTimeEnd, out string fileName) {
            fileName = GetExportFilePath(ErrorStrings.File_Name_FactoryPurchacePrice);

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@" select d.warehousecode,
                                                       d.warehousename,
                                                       d.times,
                                                       d.weight,
                                                       d.creatorname,
                                                       d.createtime,
                                                       d.modifiername,
                                                       d.modifytime
                                                  from DailySalesWeight d where 1=1 "));

                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and d.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {

                        if(warehouseId.HasValue) {
                            sql.Append(" and d.warehouseId =" + warehouseId);
                        }
                        if(times.HasValue) {
                            sql.Append(" and d.times =" + times);
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and d.createtime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and d.createtime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   "仓库编号","仓库名称","时间","权重","创建人","创建时间","修改人","修改时间"};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
