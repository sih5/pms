﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        /// <summary>
        /// 导出SAP采购价信息
        /// </summary>
        public bool ExportFactoryPurchacePrice(int[] ids,string supplierCode,string supplierName,string sparePartCode, string sparePartName,string hyCode,bool? isGenerate,DateTime? createTimeBegin,DateTime? createTimeEnd,bool? isExactExport,out string fileName)
        {
            fileName = GetExportFilePath(ErrorStrings.File_Name_FactoryPurchacePrice);

            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);

                var db = DbHelper.GetDbHelp(ConnectionString);
                var userInfo = Utils.GetCurrentUserInfo();
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(string.Format(@" select case when  nvl(a.IsGenerate,0)=1 then cast ('是' as varchar2(100)) else  cast ('否' as varchar2(100)) end as IsGenerate,a.suppliercode,
                                                    b.name as suppliername,
                                                    a.sapsparecode as sparepartcode,
                                                    a.HYCode,
                                                    c.name as sparepartname,
                                                    a.contractprice,
                                                    case when a.istemp = 0 then cast('正式价格' as varchar2(100)) when a.istemp=1 then cast('临时价格' as varchar2(100)) end as temp,
                                                    concat(round((case when a.source ='OA' then 1 else a.Rate end) * 100,2),'%') as rate,
                                                    case when d.IsOrderable = 1 then cast('是' as varchar2(100)) else  cast('否' as varchar2(100)) end as IsOrderable,
                                                    a.validfrom,
                                                    a.validto,
                                                    a.usercode,a.LimitQty, ( case when a.sapsparecode like 'W%' or a.sapsparecode like 'w%' then c.ReferenceCode else a.sapsparecode end) as SupplierPartCode,a.Source,
                                                    a.createttime as createtime
                                                    from FactoryPurchacePrice_tmp a left join partssupplier b on a.suppliercode = b.code and b.status =1 left join sparepart c on c.code = a.sapsparecode  left join PartsBranch d on c.id = d.partid and d.status = 1 where 1=1 "));

                    var dbParameters = new List<DbParameter>();
                    if (ids != null && ids.Length > 0) {
                        sql.Append(" and a.id in (");
                        for (var i = 0; i < ids.Length; i++) {
                            if (ids.Length == i + 1) {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter(ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if (!string.IsNullOrEmpty(supplierCode)) {
                            sql.Append("  and a.suppliercode like '%" + supplierCode + "%'");
                        }
                        if (!string.IsNullOrEmpty(supplierName)) {
                            sql.Append("  and b.name like '%" + supplierCode + "%'");
                        }
                        if (!string.IsNullOrEmpty(sparePartCode)) {
                            var spareparts = sparePartCode.Split(',');
                            if (spareparts.Length == 1) {
                                var sparepartcode = spareparts[0];
                                if (isExactExport.HasValue && isExactExport.Value == true) {
                                    sql.Append(" and a.sapsparecode = '" + sparepartcode + "'");
                                } else {
                                    sql.Append(" and a.sapsparecode like '%" + sparepartcode + "%'");
                                }
                            } else {
                                for (int i = 0; i < spareparts.Length; i++) {
                                    spareparts[i] = "'" + spareparts[i] + "'";
                                }
                                string codes = string.Join(",", spareparts);
                                sql.Append(" and a.sapsparecode in (" + codes + ")");
                            }
                        }
                        if (!string.IsNullOrEmpty(sparePartName)) {
                            sql.Append(" and c.name like '%" + sparePartName + "%'");
                        }
                        if (!string.IsNullOrEmpty(hyCode)) {
                            sql.Append(" and a.hycode like '%" + hyCode + "%'");
                        }

                        if (isGenerate.HasValue) {
                            if ((bool)isGenerate)
                                sql.Append(" and a.IsGenerate = 1");
                            else
                                sql.Append(" and (a.IsGenerate = 0 or a.IsGenerate is null)");
                        }

                        if (createTimeBegin.HasValue) {
                            sql.Append(@" and a.createtTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if (createTimeEnd.HasValue) {
                            sql.Append(@" and a.createtTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                    }

                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                   ErrorStrings.Export_Title_FactoryPurchacePrice_IsCreate,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierCode,ErrorStrings.Export_Title_FactoryPurchacePrice_SupplierName,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, ErrorStrings.Export_Title_Accessinfo_Tmp_ReferenceCode, ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,ErrorStrings.Export_Title_FactoryPurchacePrice_PurchasingPrice, ErrorStrings.Export_Title_FactoryPurchacePrice_Istemp,ErrorStrings.Export_Title_FactoryPurchacePrice_Share,ErrorStrings.Export_Title_PartsBranch_IsOrderableNew,ErrorStrings.Export_Title_FactoryPurchacePrice_ValidFrom,ErrorStrings.Export_Title_FactoryPurchacePrice_ValidTo,ErrorStrings.Export_Title_FactoryPurchacePrice_User,ErrorStrings.Export_Title_PartsPurchasePricingDetail_LimitQty,ErrorStrings.Export_Title_SupplierShippingOrder_SupplierpartCode,ErrorStrings.Export_Title_PartsPurchasePricing_DataSource,ErrorStrings.Export_Title_Accessinfo_Tmp_TransportTime};
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();

                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

    }
}
