﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
       public bool ExportForceReserveBill(int[] ids, int? status, string subVersionCode, string colVersionCode, string reserveTypeSubItem, string companyCode, string companyName, DateTime? bCreateTime, DateTime? eCreateTime, DateTime? bApproveTime, DateTime? eApproveTime, int? orderTypeId,int companyType,string reserveType, out string fileName) {

            fileName = GetExportFilePath("强制储备单.xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userInfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select kv.value,
                               fr.companycode,
                               fr.companyname,
                               fr.subversioncode,
                               fr.colversioncode,
                               fr.ReserveType,
                               fr.ReserveTypeSubItem,fr.ValidateFrom,
                               fr.submittername,
                               fr.submittime,
                               fr.checkername,
                               fr.checktime,
                               fr.approvername,
                               fr.approvetime,
                               fr.rejectername,
                               fr.rejecttime,
                               fr.abandonername,
                               fr.abandontime,
                               fr.creatorname,
                               fr.createtime,fr.modifiername,fr.modifytime,
                               fb.sparepartcode,
                               fb.sparepartname,
                               fb.suggestforcereserveqty,
                               fb.forcereserveqty,
                               fb.centerprice,
                               fb.reservefee,
                               kp.value
                          from ForceReserveBill fr
                          join ForceReserveBillDetail fb
                            on fr.id = fb.forcereservebillid
                          join keyvalueitem kv
                            on fr.status = kv.key
                           and kv.name = 'ForceReserveBillStatus'
                          left join keyvalueitem kp
                            on fb.centerpartproperty = kp.key
                           and kp.name = 'ABCSetting_Type'
                         where fr.companyType=" + companyType );
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and fr.id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(subVersionCode)) {
                            sql.Append("and LOWER(fr.subVersionCode) like {0}subVersionCode ");
                            dbParameters.Add(db.CreateDbParameter("subVersionCode", "%" + subVersionCode.ToLower() + "%"));
                        }

                        if(status.HasValue) {
                            sql.Append(@" and fr.status = {0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                        if(!string.IsNullOrEmpty(colVersionCode)) {
                            sql.Append("and LOWER(fr.colVersionCode) like {0}colVersionCode ");
                            dbParameters.Add(db.CreateDbParameter("colVersionCode", "%" + colVersionCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(reserveType)) {
                            sql.Append("and LOWER(fr.reserveType) like {0}reserveType ");
                            dbParameters.Add(db.CreateDbParameter("reserveType", "%" + reserveType.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(reserveTypeSubItem)) {
                            sql.Append("and LOWER(fr.reserveTypeSubItem) like {0}reserveTypeSubItem ");
                            dbParameters.Add(db.CreateDbParameter("reserveTypeSubItem", "%" + reserveTypeSubItem.ToLower() + "%"));
                        }


                        if(!string.IsNullOrEmpty(companyCode)) {
                            sql.Append(" and LOWER(fr.companyCode) like {0}companyCode");
                            dbParameters.Add(db.CreateDbParameter("companyCode", "%" + companyCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(companyName)) {
                            sql.Append(" and fr.companyName like {0}companyName");
                            dbParameters.Add(db.CreateDbParameter("companyName", "%" + companyName + "%"));
                        }
                        
                        if(bCreateTime.HasValue) {
                            sql.Append(@" and fr.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(eCreateTime.HasValue) {
                            sql.Append(@" and fr.createTime <=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eCreateTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(bApproveTime.HasValue) {
                            sql.Append(@" and fr.ApproveTime >=To_date({0}bApproveTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = bApproveTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("bApproveTime", tempTime.ToString("G")));
                        }
                        if(eApproveTime.HasValue) {
                            sql.Append(@" and fr.ApproveTime <=To_date({0}eApproveTime,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = eApproveTime.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("eApproveTime", tempTime.ToString("G")));
                        } 
                    }
                    var command = db.CreateDbCommand(string.Format(sql.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_AccountPeriod_Status,ErrorStrings.Export_Title_Company_Code, ErrorStrings.Export_Title_Company_Name,ErrorStrings.Export_Title_Accessinfo_Tmp_SubVersionCode,ErrorStrings.Export_Title_Accessinfo_Tmp_ColVersionCode,"储备类别", "储备类别子项目","生效时间","提交人","提交时间","审核人","审核时间",ErrorStrings.Export_Title_PackingPropertyApp_ApproverName,ErrorStrings.Export_Title_PackingPropertyApp_ApproveTime,"驳回人","驳回时间",ErrorStrings.Export_Title_PartsBranch_AbandonerName,ErrorStrings.Export_Title_PartsBranch_AbandonerTime,ErrorStrings.Export_Title_AccountPeriod_CreatorName,ErrorStrings.Export_Title_AccountPeriod_CreateTime,ErrorStrings.Export_Title_AccountPeriod_ModifierName,ErrorStrings.Export_Title_AccountPeriod_ModifyTime,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode,ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName,
                                    "建议强制储备数","强制储备数",ErrorStrings.Export_Title_ForceReserveBillDetail_Price,ErrorStrings.Export_Title_ForceReserveBillDetail_Money,"中心库配件属性"
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }
    }
}
