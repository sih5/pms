﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Entities;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool 导出金税分类信息(int[] ids, string goldenTaxClassifyCode, string goldenTaxClassifyName, DateTime? createTimeBegin, DateTime? createTimeEnd, DateTime? modifyTimeBegin, DateTime? modifyTimeEnd, out string fileName) {
            fileName = GetExportFilePath(string.Format("金税分类信息_{0}.xlsx", DateTime.Now.ToString("ddHHmmssffff")));
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var userinfo = Utils.GetCurrentUserInfo();
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    var sql = new StringBuilder();
                    sql.AppendFormat(@"SELECT 
                                       GoldenTaxClassifyCode,GoldenTaxClassifyName,CreatorName,CreateTime,ModifierName,ModifyTime From GoldenTaxClassifyMsg where ");
                    var dbParameters = new List<DbParameter>();
                    if(ids != null && ids.Length > 0) {
                        sql.Append(" and Id in (");
                        for(var i = 0; i < ids.Length; i++) {
                            if(ids.Length == i + 1) {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture));
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            } else {
                                sql.Append("{0}id" + ids[i].ToString(CultureInfo.InvariantCulture) + ",");
                                dbParameters.Add(db.CreateDbParameter("id" + ids[i].ToString(CultureInfo.InvariantCulture), ids[i]));
                            }
                        }
                        sql.Append(")");
                    } else {
                        if(!string.IsNullOrEmpty(goldenTaxClassifyCode)) {
                            sql.Append(" and Lower(GoldenTaxClassifyCode) like {0}GoldenTaxClassifyCode ");
                            dbParameters.Add(db.CreateDbParameter("GoldenTaxClassifyCode", "%" + goldenTaxClassifyCode.ToLower() + "%"));
                        }
                        if(!string.IsNullOrEmpty(goldenTaxClassifyName)) {
                            sql.Append(" and Lower(GoldenTaxClassifyName) like {0}GoldenTaxClassifyName ");
                            dbParameters.Add(db.CreateDbParameter("GoldenTaxClassifyName", "%" + goldenTaxClassifyName.ToLower() + "%"));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and CreateTime >=to_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(@" and CreateTime <=to_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(modifyTimeBegin.HasValue) {
                            sql.Append(@" and ModifyTime >=to_date({0}modifyTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeBegin", tempTime.ToString("G")));
                        }
                        if(modifyTimeEnd.HasValue) {
                            sql.Append(@" and ModifyTime <=to_date({0}modifyTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = modifyTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("modifyTimeEnd", tempTime.ToString("G")));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where  and ") > 0)
                        sqlStr = sqlStr.Replace(" where  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where ", "");
                    var command = db.CreateDbCommand(string.Format(sqlStr.ToString(), db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    conn.Open();
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    ErrorStrings.Export_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode, ErrorStrings.Export_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }
                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        public bool 金税分类信息导入(string fileName, out int excelImportNum, out List<GoldenTaxClassifyMsgExtend> rightData, out List<GoldenTaxClassifyMsgExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var allList = new List<GoldenTaxClassifyMsgExtend>();
            var errorList = new List<GoldenTaxClassifyMsgExtend>();
            var rightList = new List<GoldenTaxClassifyMsgExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("GoldenTaxClassifyMsg", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyCode, "GoldenTaxClassifyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_GoldenTaxClassifyMsg_GoldenTaxClassifyName, "GoldenTaxClassifyName");
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new GoldenTaxClassifyMsgExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region
                        tempImportObj.GoldenTaxClassifyCode = newRow["GoldenTaxClassifyCode"];
                        tempImportObj.GoldenTaxClassifyName = newRow["GoldenTaxClassifyName"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的内容基本检查

                        if(string.IsNullOrEmpty(tempImportObj.GoldenTaxClassifyCode)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyCodeIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.GoldenTaxClassifyCode) > fieldLenght["GoldenTaxClassifyCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyCodeIsLong);
                        }

                        if(string.IsNullOrEmpty(tempImportObj.GoldenTaxClassifyName)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyNameIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.GoldenTaxClassifyName) > fieldLenght["GoldenTaxClassifyName".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_Sparepart_GoldenTaxClassifyNameIsLong);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    //校验导入的数据业务校验
                    var groupsNeedCheckUnique = allList.Where(r => r.ErrorMsg == null).GroupBy(r => new {
                        r.GoldenTaxClassifyCode,
                    }).Where(r => r.Count() > 1);
                    foreach(var group in groupsNeedCheckUnique) {
                        foreach(var groupItem in group) {
                            if(groupItem.ErrorMsg == null) {
                                groupItem.ErrorMsg = ErrorStrings.ImpPartsSupplierRelation_Validation31;
                            } else {
                                groupItem.ErrorMsg = groupItem.ErrorMsg + ";文件内数据重复";
                            }
                        }
                    }
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    var goldenTaxClassifyCode = tempRightList.Select(r => r.GoldenTaxClassifyCode).Distinct().ToArray();
                    var dbGoldenTaxClassifyMsgs = new List<GoldenTaxClassifyMsgExtend>();
                    Func<string[], bool> getDbGoldenTaxClassifyMsgs = value => {
                        var dbObj = new GoldenTaxClassifyMsgExtend {
                            GoldenTaxClassifyCode = value[0]
                        };
                        dbGoldenTaxClassifyMsgs.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperatorByLower("select GoldenTaxClassifyCode from GoldenTaxClassifyMsg ", "GoldenTaxClassifyCode", true, goldenTaxClassifyCode, getDbGoldenTaxClassifyMsgs);
                    var errorGoldenTaxClassifyMsgs = tempRightList.Where(r => dbGoldenTaxClassifyMsgs.Any(v => v.GoldenTaxClassifyCode == r.GoldenTaxClassifyCode)).ToList();
                    foreach(var errorItem in errorGoldenTaxClassifyMsgs) {
                        errorItem.ErrorMsg = "已存在金税分类编码";
                    }

                    tempRightList = tempRightList.Where(r => r.ErrorMsg == null).ToList();
                    var goldenTaxClassifyName = tempRightList.Select(r => r.GoldenTaxClassifyName).Distinct().ToArray();
                    dbGoldenTaxClassifyMsgs = new List<GoldenTaxClassifyMsgExtend>();
                    Func<string[], bool> getDbGoldenTaxClassifyMsgNames = value => {
                        var dbObj = new GoldenTaxClassifyMsgExtend {
                            GoldenTaxClassifyName = value[0]
                        };
                        dbGoldenTaxClassifyMsgs.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperatorByLower("select GoldenTaxClassifyName from GoldenTaxClassifyMsg ", "GoldenTaxClassifyName", true, goldenTaxClassifyName, getDbGoldenTaxClassifyMsgNames);
                    errorGoldenTaxClassifyMsgs = tempRightList.Where(r => dbGoldenTaxClassifyMsgs.Any(v => v.GoldenTaxClassifyName == r.GoldenTaxClassifyName)).ToList();
                    foreach(var errorItem in errorGoldenTaxClassifyMsgs) {
                        errorItem.ErrorMsg = "已存在金税分类名称";
                    }

                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    rightList = allList.Except(errorList).ToList();
                    //导出所有不合格数据
                    if(errorList.Any()) {
                        excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                        errorDataFileName = GetErrorFilePath(fileName);
                        using(var excelExport = new ExcelExport(errorDataFileName)) {
                            var list = errorList;
                            excelExport.ExportByRow(index => {
                                if(index == list.Count + 1)
                                    return null;
                                if(index == 0)
                                    return excelColumns.ToArray();
                                var tempObj = list[index - 1];
                                var values = new object[] {
                                tempObj.GoldenTaxClassifyCode,
                                tempObj.GoldenTaxClassifyName,
                                tempObj.ErrorMsg
                            };
                                return values;
                            });
                        }
                        errorList = null;
                    }
                    //更新导入所有合格数据
                    if(!rightList.Any())
                        return true;
                    using(var conn = db.CreateDbConnection()) {
                        conn.Open();
                        //开启事务，新增更新配件在一个事务内
                        var ts = conn.BeginTransaction();
                        try {
                            //新增配件
                            if(rightList.Any()) {
                                var sqlInsertGoldenTaxClassifyMsg = db.GetInsertSql("GoldenTaxClassifyMsg", "Id", new[] {
                                    "GoldenTaxClassifyCode","GoldenTaxClassifyName","CreatorId","CreatorName","CreateTime"
                                });
                                var userInfo = Utils.GetCurrentUserInfo();
                                //往数据库增加配件信息
                                foreach(var item in rightList) {
                                    var command = db.CreateDbCommand(sqlInsertGoldenTaxClassifyMsg, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyCode", item.GoldenTaxClassifyCode));
                                    command.Parameters.Add(db.CreateDbParameter("GoldenTaxClassifyName", item.GoldenTaxClassifyName));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    command.ExecuteNonQuery();
                                }
                            }
                            //无异常提交
                            ts.Commit();
                        } catch(Exception) {
                            //报错回滚
                            ts.Rollback();
                            throw;
                        } finally {
                            if(conn.State == System.Data.ConnectionState.Open) {
                                conn.Close();
                            }
                        }
                    }
                    return true;
                }
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}
