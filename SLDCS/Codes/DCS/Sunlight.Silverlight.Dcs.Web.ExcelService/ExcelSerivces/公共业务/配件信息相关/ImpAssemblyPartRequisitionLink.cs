﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportAssemblyPartRequisitionLink(string fileName, out int excelImportNum, out List<AssemblyPartRequisitionLinkExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<AssemblyPartRequisitionLinkExtend>();
            var allList = new List<AssemblyPartRequisitionLinkExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("AssemblyPartRequisitionLink", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                List<AssemblyPartRequisitionLinkExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "PartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.ImpAssemblyPartRequisitionLink_KeyCode, "KeyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_WarehouseArea_Quantity, "Qty");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status"),new KeyValuePair<string, string>("SparePartKeyCode", "SparePart_KeyCode")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new AssemblyPartRequisitionLinkExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.PartCode = newRow["PartCode"];
                        tempImportObj.PartName = newRow["PartName"];
                        tempImportObj.KeyCodeStr = newRow["KeyCode"];
                        tempImportObj.QtyStr = newRow["Qty"];
                        tempImportObj.Remark = newRow["Remark"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        //互换组号,互换号检查
                        if(string.IsNullOrEmpty(tempImportObj.PartCode)) {
                            tempErrorMessage.Add(ErrorStrings.ImpAssemblyPartRequisitionLink_Validation1);
                        }
                        if (string.IsNullOrEmpty(tempImportObj.KeyCodeStr)) {
                            tempErrorMessage.Add(ErrorStrings.ImpAssemblyPartRequisitionLink_Validation2);
                        } else { 
                            var tempEnumValue = tempExcelOperator.ImportHelper.GetEnumValue("SparePartKeyCode", tempImportObj.KeyCodeStr.ToLower());
                            if(!tempEnumValue.HasValue) {
                                tempErrorMessage.Add(ErrorStrings.ImpAssemblyPartRequisitionLink_Validation3);
                            } else {
                                tempImportObj.KeyCode = tempEnumValue.Value;
                            }
                        }
                        if (string.IsNullOrEmpty(tempImportObj.QtyStr)) {
                            tempErrorMessage.Add(ErrorStrings.ImpAssemblyPartRequisitionLink_Validation4);
                        } else {
                            int qty;
                            if (int.TryParse(tempImportObj.QtyStr, out qty)) {
                                if (qty > 0)
                                    tempImportObj.Qty = qty;
                                else
                                    tempErrorMessage.Add(ErrorStrings.ImpAssemblyPartRequisitionLink_Validation5);
                            } else {
                                tempErrorMessage.Add(ErrorStrings.ImpAssemblyPartRequisitionLink_Validation6);
                            }
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查

                    var groups = tempRightList.GroupBy(r => new {
                        r.PartCode,r.KeyCode
                    }).Where(r => r.Count() > 1);
                    var list = groups.SelectMany(r => r).Select(r => r.PartCode).ToList();
                    var tempWrongList = tempRightList.Where(r => list.Any(v => v == r.PartCode)).ToList();
                    foreach(var item in tempWrongList) {
                        item.ErrorMsg = ErrorStrings.ImpAssemblyPartRequisitionLink_Validation7;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //配件有效性校验
                    var partsCodeNeedCheck = tempRightList.Select(r => r.PartCode).Distinct().ToArray();
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        dbSpareParts.Add(new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1],
                            Name = value[2]
                        });
                        return false;
                    };
                    db.QueryDataWithInOperator("Select Id,Code,Name From SparePart Where status=1 and parttype<>6 ", "Code", true, partsCodeNeedCheck, getDbSpareParts);

                    var dbAssemblyPartRequisitionLinkExtends = new List<AssemblyPartRequisitionLinkExtend>();
                    Func<string[], bool> getDbPartsExchangeGroupCodes = value => {
                        var dbObj = new AssemblyPartRequisitionLinkExtend {
                            Id = Convert.ToInt32(value[0]),
                            PartId = Convert.ToInt32(value[1]),
                            KeyCode =Convert.ToInt32( value[2]),
                        };
                        dbAssemblyPartRequisitionLinkExtends.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select id,partid,keycode from AssemblyPartRequisitionLink where status=1  ", "PartCode", true, partsCodeNeedCheck, getDbPartsExchangeGroupCodes);
                    
                    foreach(var tempRight in tempRightList) {
                        var part = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.PartCode);
                        if (part == null) {
                            tempRight.ErrorMsg = ErrorStrings.ImpAssemblyPartRequisitionLink_Validation8;
                        } else {
                            tempRight.PartId = part.Id;
                            tempRight.PartName= part.Name;
                        }
                        var partsExchangeGroup = dbAssemblyPartRequisitionLinkExtends.FirstOrDefault(v => v.PartId == tempRight.PartId && v.KeyCode == tempRight.KeyCode);
                        if(partsExchangeGroup != null) {
                            tempRight.ErrorMsg = ErrorStrings.ImpAssemblyPartRequisitionLink_Validation9;
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.PartCode, tempObj.PartName,tempObj.KeyCodeStr, tempObj.QtyStr,tempObj.Remark, tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务
                    var ts = conn.BeginTransaction();
                    try {
                        if(rightList.Any()) {
                            var sqlInsert = db.GetInsertSql("AssemblyPartRequisitionLink", "Id", new[] {
                                    "KeyCode", "PartId", "PartCode","PartName","Qty","Status","Remark", "CreatorId", "CreatorName", "CreateTime"
                                });
                            var userInfo = Utils.GetCurrentUserInfo();
                            foreach(var right in rightList) {
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("KeyCode", right.KeyCode));
                                command.Parameters.Add(db.CreateDbParameter("PartId", right.PartId));
                                command.Parameters.Add(db.CreateDbParameter("PartCode", right.PartCode));
                                command.Parameters.Add(db.CreateDbParameter("PartName", right.PartName));
                                command.Parameters.Add(db.CreateDbParameter("Qty", right.Qty));
                                command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                command.Parameters.Add(db.CreateDbParameter("Remark", right.Remark));
                                command.Parameters.Add(db.CreateDbParameter("CREATORID", userInfo.Id));
                                command.Parameters.Add(db.CreateDbParameter("CREATORNAME", userInfo.Name));
                                command.Parameters.Add(db.CreateDbParameter("CREATETIME", DateTime.Now));
                                command.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}
