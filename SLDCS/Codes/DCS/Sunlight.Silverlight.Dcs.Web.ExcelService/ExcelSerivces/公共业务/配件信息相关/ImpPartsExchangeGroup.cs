﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImportPartsExchangeGroup(string fileName, out int excelImportNum, out List<PartsExchangeGroupExtend> rightData, out List<PartsExchangeGroupExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsExchangeGroupExtend>();
            var rightList = new List<PartsExchangeGroupExtend>();
            var allList = new List<PartsExchangeGroupExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);
                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("UsedPartsReturnDetail", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    //指定文件中的列对应的名称
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExchangeCode, "ExchangeCode");

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    //Excel单行导入
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        //接收Excel导入数据
                        var tempImportObj = new PartsExchangeGroupExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        tempImportObj.ExchangeCodeStr = newRow["ExchangeCode"];

                        var errorMsgs = new List<string>();
                        //配件图号检查
                        var fieldIndex = notNullableFields.IndexOf("ExchangeCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.ExchangeCodeStr)) {
                            if(fieldIndex > -1)
                                errorMsgs.Add(ErrorStrings.Export_Validation_PartsExchangeGroupExtend_ExchangeCode);
                        }
                        if(errorMsgs.Count > 0)
                            tempImportObj.ErrorMsg = string.Join("; ", errorMsgs);
                        allList.Add(tempImportObj);
                        return false;
                    });
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    //  var serialNumber = 1;
                    //将合格数据的值填上
                    foreach(var rightItem in rightList) {
                        rightItem.ExchangeCode = rightItem.ExchangeCodeStr;
                        //rightItem.PlannedAmount = 1;
                        //rightItem.ConfirmedAmount = 0;
                        //rightItem.OutboundAmount = 0;
                    }
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                 //设置错误信息导出的列的值
                                tempObj.ExchangeCodeStr, tempObj.ExchangeCodeStr, tempObj.ErrorMsg
                             };
                            return values;
                        });
                    }
                    errorList = null;
                }

                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;



            }
        }

        public bool ImportPartsExchangeGroupInfo(string fileName, out int excelImportNum, out List<PartsExchangeGroupExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartsExchangeGroupExtend>();
            var allList = new List<PartsExchangeGroupExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartsExchangeGroup", out notNullableFields, out fieldLenght);

                List<object> excelColumns;
                List<PartsExchangeGroupExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExGroupCode, "ExGroupCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsExchangeGroupExtend_ExchangeCode, "ExchangeCode");
                    #endregion

                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;

                    #region 获取对应枚举
                    var keyValuePairs = new[] {
                        new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status")
                    };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion

                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartsExchangeGroupExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);

                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.ExGroupCodeStr = newRow["ExGroupCode"];
                        tempImportObj.ExGroupExchangeCode = newRow["ExGroupCode"];
                        tempImportObj.ExchangeCodeStr = newRow["ExchangeCode"];
                        #endregion

                        var tempErrorMessage = new List<string>();

                        #region 导入的数据基本检查
                        //互换组号,互换号检查
                        if(string.IsNullOrEmpty(tempImportObj.ExGroupCodeStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsExchangeGroupExtend_ExGroupCodeIsNull);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ExchangeCodeStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_PartsExchangeGroupExtend_ExchangeCode);
                        }
                        #endregion

                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    #region 剩下的数据进行业务检查
                    //互换号唯一
                    var groups = tempRightList.GroupBy(r => new {
                        r.ExchangeCodeStr
                    }).Where(r => r.Count() > 1);
                    var list = groups.SelectMany(r => r).Select(r => r.ExchangeCodeStr).ToList();
                    var tempWrongList = tempRightList.Where(r => list.Any(v => v == r.ExchangeCodeStr)).ToList();
                    foreach(var item in tempWrongList) {
                        item.ErrorMsg = ErrorStrings.Export_Validation_PartsExchangeGroupExtend_RepeatData;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //配件互换组信息 用互换号查询数据
                    var exchangeCodesNeedCheck = tempRightList.Select(r => r.ExchangeCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartsExchangeGroupExtends = new List<PartsExchangeGroupExtend>();
                    Func<string[], bool> getDbPartsExchangeGroupCodes = value => {
                        var dbObj = new PartsExchangeGroupExtend {
                            Id = Convert.ToInt32(value[0]),
                            ExGroupCode = value[1],
                            ExchangeCode = value[2],
                        };
                        dbPartsExchangeGroupExtends.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,ExGroupCode,ExchangeCode from PartsExchangeGroup where status=1 ", "ExchangeCode", true, exchangeCodesNeedCheck, getDbPartsExchangeGroupCodes);
                    
                    foreach(var tempRight in tempRightList) {
                        var partsExchangeGroup = dbPartsExchangeGroupExtends.FirstOrDefault(v => v.ExchangeCode == tempRight.ExchangeCodeStr);
                        if(partsExchangeGroup != null) {
                            tempRight.ExGroupExchangeCode = partsExchangeGroup.ExGroupCode;
                            //tempRight.ErrorMsg = String.Format("配件互换组与互换号关系已经存在,互换号“{0}”", tempRight.ExchangeCodeStr);
                        }
                    }
                    var tempRightListGroup = tempRightList.Where(v => v.ExGroupCodeStr != v.ExGroupExchangeCode).GroupBy(v => v.ExGroupExchangeCode).ToArray();
                    foreach(var rightListGroup in tempRightListGroup) {
                        var count = rightListGroup.GroupBy(v => v.ExGroupCodeStr).Count();
                        if(count>1) {
                            foreach(var item in rightListGroup) {
                                foreach(var temp in tempRightList) {
                                    if(item.ExchangeCodeStr == temp.ExchangeCodeStr) {
                                        item.ErrorMsg = String.Format(ErrorStrings.Export_Validation_PartsExchangeGroupExtend_CofictData, item.ExchangeCodeStr);
                                    }
                                }
                            }
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.ExGroupCodeStr, tempObj.ExchangeCodeStr, tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            var rightListGroups = rightList;

                            var sqlInsert = db.GetInsertSql("PartsExchangeGroup", "Id", new[] {
                                    "ExGroupCode", "ExchangeCode", "Status", "CreatorId", "CreatorName", "CreateTime"
                                });
                            var sqlUpdate = db.GetUpdateSql("PartsExchangeGroup", new[] {
                                    "Status", "ModifierId", "ModifierName", "ModifyTime", "AbandonerId", "AbandonerName", "AbandonTime"
                                }, new[] { "Id" });

                            var userInfo = Utils.GetCurrentUserInfo();
                            //用互换号查询数据
                            var exchangeCodesNeedCheck = rightListGroups.Select(r => r.ExchangeCodeStr.ToUpper()).Distinct().ToArray();
                            var dbPartsExchangeGroupExtends = new List<PartsExchangeGroupExtend>();
                            Func<string[], bool> getDbPartsExchangeGroupCodes = value => {
                                var dbObj = new PartsExchangeGroupExtend {
                                    Id = Convert.ToInt32(value[0]),
                                    ExGroupCode = value[1],
                                    ExchangeCode = value[2],
                                };
                                dbPartsExchangeGroupExtends.Add(dbObj);
                                return false;
                            };
                            db.QueryDataWithInOperator("select Id,ExGroupCode,ExchangeCode from PartsExchangeGroup where status=1 ", "ExchangeCode", true, exchangeCodesNeedCheck, getDbPartsExchangeGroupCodes);
                            //使用互换组号 查询互换信息
                            var exGroupCodes = dbPartsExchangeGroupExtends.Select(r => r.ExGroupCode.ToUpper()).Distinct().ToArray();
                            Func<string[], bool> getDbPartsExGroupCodeCodes = value => {
                                var dbObj = new PartsExchangeGroupExtend {
                                    Id = Convert.ToInt32(value[0]),
                                    ExGroupCode = value[1],
                                    ExchangeCode = value[2],
                                };
                                dbPartsExchangeGroupExtends.Add(dbObj);
                                return false;
                            };
                            db.QueryDataWithInOperator("select Id,ExGroupCode,ExchangeCode from PartsExchangeGroup where status=1 ", "ExGroupCode", true, exGroupCodes, getDbPartsExGroupCodeCodes);

                            var partsExchangeGroupExtends = dbPartsExchangeGroupExtends.Distinct().ToList();

                            var rightGroups = rightList.GroupBy(v => v.ExGroupCodeStr).ToArray();
                            foreach(var rightGroup in rightGroups) {
                                var code = CodeGenerator.Generate(db, "PartsExchange");
                                foreach(var right in rightGroup) {
                                    #region 添加Sql的参数
                                    var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                    command.Parameters.Add(db.CreateDbParameter("ExGroupCode", code));
                                    command.Parameters.Add(db.CreateDbParameter("ExchangeCode", right.ExchangeCodeStr));
                                    command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                    command.ExecuteNonQuery();
                                    #endregion

                                    var partsExchangeGroupExtend = partsExchangeGroupExtends.FirstOrDefault(v => v.ExchangeCode == right.ExchangeCodeStr);
                                    if(partsExchangeGroupExtend!=null) {
                                        var updatepartsExchangeGroups = partsExchangeGroupExtends.Where(v => v.ExGroupCode == partsExchangeGroupExtend.ExGroupCode).ToArray();
                                        var insertpartsExchangeGroups = partsExchangeGroupExtends.Where(v => v.ExGroupCode == partsExchangeGroupExtend.ExGroupCode && v.ExchangeCode.ToString() != right.ExchangeCodeStr.ToString()).ToArray();
                                        foreach(var partsExchangeGroup in updatepartsExchangeGroups) {
                                            #region 添加Sql的参数
                                            command = db.CreateDbCommand(sqlUpdate, conn, ts);
                                            command.Parameters.Add(db.CreateDbParameter("Id", partsExchangeGroup.Id));
                                            command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.作废));
                                            command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                                            command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                                            command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                                            command.Parameters.Add(db.CreateDbParameter("AbandonerId", userInfo.Id));
                                            command.Parameters.Add(db.CreateDbParameter("AbandonerName", userInfo.Name));
                                            command.Parameters.Add(db.CreateDbParameter("AbandonTime", DateTime.Now));
                                            command.ExecuteNonQuery();
                                            #endregion
                                        }
                                        foreach(var partsExchangeGroup in insertpartsExchangeGroups) {
                                            #region 添加Sql的参数
                                            command = db.CreateDbCommand(sqlInsert, conn, ts);
                                            command.Parameters.Add(db.CreateDbParameter("ExGroupCode", code));
                                            command.Parameters.Add(db.CreateDbParameter("ExchangeCode", partsExchangeGroup.ExchangeCode));
                                            command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                                            command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                                            command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                                            command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                                            command.ExecuteNonQuery();
                                            #endregion
                                            partsExchangeGroupExtends.Remove(partsExchangeGroup);
                                        }
                                    }
                                }
                            }


                            //var oldrightList = dbPartsExchangeGroupExtends.Where(v => rightList.Select(o => o.ExchangeCodeStr).Contains(v.ExchangeCode));
                            //foreach(var oldright in oldrightList) {
                            //    #region 添加Sql的参数
                            //    var command = db.CreateDbCommand(sqlUpdate, conn, ts);
                            //    command.Parameters.Add(db.CreateDbParameter("Id", oldright.Id));
                            //    command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.作废));
                            //    command.Parameters.Add(db.CreateDbParameter("ModifierId", userInfo.Id));
                            //    command.Parameters.Add(db.CreateDbParameter("ModifierName", userInfo.Name));
                            //    command.Parameters.Add(db.CreateDbParameter("ModifyTime", DateTime.Now));
                            //    command.Parameters.Add(db.CreateDbParameter("AbandonerId", userInfo.Id));
                            //    command.Parameters.Add(db.CreateDbParameter("AbandonerName", userInfo.Name));
                            //    command.Parameters.Add(db.CreateDbParameter("AbandonTime", DateTime.Now));
                            //    command.ExecuteNonQuery();
                            //    #endregion
                            //}
                            ////获取所有新增数据
                            ////var newrightList = rightList.Except(oldrightList).ToList();
                            ////与导入的配件信息可以互换的配件信息,因为要全部重新生成,所以先全部作废
                            //foreach(var item in rightList) {
                            //    #region 添加Sql的参数
                            //    var command = db.CreateDbCommand(sqlInsert, conn, ts);
                            //    command.Parameters.Add(db.CreateDbParameter("ExGroupCode", item.ExGroupCodeStr));
                            //    command.Parameters.Add(db.CreateDbParameter("ExchangeCode", item.ExchangeCodeStr));
                            //    command.Parameters.Add(db.CreateDbParameter("Status", (int)DcsBaseDataStatus.有效));
                            //    command.Parameters.Add(db.CreateDbParameter("CreatorId", userInfo.Id));
                            //    command.Parameters.Add(db.CreateDbParameter("CreatorName", userInfo.Name));
                            //    command.Parameters.Add(db.CreateDbParameter("CreateTime", DateTime.Now));
                            //    command.ExecuteNonQuery();
                            //    #endregion
                            //}
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }
    }
}
