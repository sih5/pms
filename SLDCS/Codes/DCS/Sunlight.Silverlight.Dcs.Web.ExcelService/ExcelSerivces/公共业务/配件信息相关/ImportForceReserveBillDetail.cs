﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;


namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ImpForceReserveBillDetail(string fileName, out int excelImportNum, out List<ForceReserveBillDetailExtend> rightData, out List<ForceReserveBillDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<ForceReserveBillDetailExtend>();
            var allList = new List<ForceReserveBillDetailExtend>();
            List<ForceReserveBillDetailExtend> rightList = null;
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("ForceReserveBillDetail", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Code, "CompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Name, "CompanyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_ForceReserveBillDetail_Amount, "ForceReserveQty");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new ForceReserveBillDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.CompanyCodeStr = newRow["CompanyCode"];
                        tempImportObj.CompanyNameStr = newRow["CompanyName"];
                        tempImportObj.ForceReserveQtyStr = newRow["ForceReserveQty"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.CompanyCodeStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsNull);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ForceReserveQtyStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Title_ForceReserveBillDetail_Amount_Null);
                        } else {
                            //数量检查
                            int checkValue;
                            if(int.TryParse(tempImportObj.ForceReserveQtyStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Title_ForceReserveBillDetail_Amount_Zero);
                                } else {
                                    tempImportObj.ForceReserveQty = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Title_ForceReserveBillDetail_Amount_Number);
                            }
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartCodeStr,
                        r.CompanyCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //1、配件有效性校验（配件信息.配件编号=导入数据.配件图号），
                    //如果校验不通过，记录校验错误信息：配件不存在。
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var dbSpareParts = new List<ForceReserveBillDetailExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        var dbObj = new ForceReserveBillDetailExtend {
                            SparePartId = Convert.ToInt32(value[0]),
                            SparePartCode = value[1],
                            SparePartName = value[2],
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("Select Id,Code,Name  From SparePart Where Status = {0}", (int)DcsMasterDataStatus.有效), "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    var errorSpareParts = tempRightList.Where(r => dbSpareParts.All(v => v.SparePartCode != r.SparePartCodeStr)).ToList();
                    foreach(var errorItem in errorSpareParts) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempSparePart = dbSpareParts.Single(r => r.SparePartCode == rightItem.SparePartCodeStr);
                        rightItem.SparePartId = tempSparePart.SparePartId;
                        rightItem.SparePartCode = tempSparePart.SparePartCode;
                        rightItem.SparePartName = tempSparePart.SparePartName;
                    }
                    //1、企业有效性校验（配件信息.配件编号=导入数据.配件图号），
                    //如果校验不通过，记录校验错误信息：配件不存在。
                    var companyCodeCheck = tempRightList.Select(r => r.CompanyCodeStr).Distinct().ToArray();
                    var dbCompany = new List<ForceReserveBillDetailExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new ForceReserveBillDetailExtend {
                            CompanyId = Convert.ToInt32(value[0]),
                            CompanyCode = value[1],
                            CompanyName = value[2],
                        };
                        dbCompany.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("Select Id,Code,Name  From Company Where Status = {0} and type=3 ", (int)DcsMasterDataStatus.有效), "Code", true, companyCodeCheck, getDbCompanys);
                    var errorCompanys = tempRightList.Where(r => dbCompany.All(v => v.CompanyCode != r.CompanyCodeStr)).ToList();
                    foreach(var errorItem in errorCompanys) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_CompanyDetail_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempSparePart = dbCompany.Single(r => r.CompanyCode == rightItem.CompanyCodeStr);
                        rightItem.CompanyId = tempSparePart.CompanyId;
                        rightItem.CompanyCode = tempSparePart.CompanyCode;
                        rightItem.CompanyName = tempSparePart.CompanyName;
                    }

                    var sparePartIds = dbSpareParts.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    if(sparePartIds.Any()) {
                        var dbPartsStocks = new List<ForceReserveBillDetailExtend>();
                        Func<string[], bool> getDbPartsStocks = value => {
                            var dbObj = new ForceReserveBillDetailExtend {
                                CenterPrice = Decimal.Parse(value[0]),
                                SparePartCode = value[1]
                            };
                            dbPartsStocks.Add(dbObj);
                            return false;
                        };
                        var sql1 = string.Format(@"Select CenterPrice,SparePartCode  from PartsSalesPrice a
                                                    Where a.status=1  ");
                        db.QueryDataWithInOperator(sql1, "a.SparePartId", false, sparePartIds, getDbPartsStocks);
                        foreach(var tempRight in tempRightList) {
                            var partsStock = dbPartsStocks.FirstOrDefault(r => r.SparePartCode == tempRight.SparePartCodeStr);
                            if(partsStock != null) {
                                tempRight.CenterPrice = partsStock.CenterPrice;
                            }

                        }
                    }
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                 tempObj.CompanyCodeStr,
                                tempObj.CompanyNameStr,
                                tempObj.SparePartCodeStr,
                                tempObj.SparePartNameStr,                               
                                tempObj.ForceReserveQtyStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
        public bool ImpForceReserveBillDealer(string fileName, string reserveType, out int excelImportNum, out List<ForceReserveBillDetailExtend> rightData, out List<ForceReserveBillDetailExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<ForceReserveBillDetailExtend>();
            var allList = new List<ForceReserveBillDetailExtend>();
            List<ForceReserveBillDetailExtend> rightList = null;
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("ForceReserveBillDetail", out notNullableFields, out fieldLenght);
                List<object> excelColumns;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Code, "CompanyCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Company_Name, "CompanyName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "SparePartCode");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareName, "SparePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_ForceReserveBillDetail_Amount, "ForceReserveQty");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new ForceReserveBillDetailExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值
                        tempImportObj.SparePartCodeStr = newRow["SparePartCode"];
                        tempImportObj.SparePartNameStr = newRow["SparePartName"];
                        tempImportObj.CompanyCodeStr = newRow["CompanyCode"];
                        tempImportObj.CompanyNameStr = newRow["CompanyName"];
                        tempImportObj.ForceReserveQtyStr = newRow["ForceReserveQty"];
                        #endregion
                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        if(string.IsNullOrEmpty(tempImportObj.SparePartCodeStr)) {
                            tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation1);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.SparePartCodeStr) > fieldLenght["SparePartCode".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchaseOrderDetail_Validation2);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.CompanyCodeStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Validation_Company_CodeIsNull);
                        }
                        if(string.IsNullOrEmpty(tempImportObj.ForceReserveQtyStr)) {
                            tempErrorMessage.Add(ErrorStrings.Export_Title_ForceReserveBillDetail_Amount_Null);
                        } else {
                            //数量检查
                            int checkValue;
                            if(int.TryParse(tempImportObj.ForceReserveQtyStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add(ErrorStrings.Export_Title_ForceReserveBillDetail_Amount_Zero);
                                } else {
                                    tempImportObj.ForceReserveQty = checkValue;
                                }
                            } else {
                                tempErrorMessage.Add(ErrorStrings.Export_Title_ForceReserveBillDetail_Amount_Number);
                            }
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //文件内数据重复
                    var groups = tempRightList.GroupBy(r => new {
                        r.SparePartCodeStr,
                        r.CompanyCodeStr
                    }).Where(r => r.Count() > 1);
                    foreach(var groupItem in groups.SelectMany(r => r)) {
                        groupItem.ErrorMsg = ErrorStrings.Export_Validation_PartsPurchaseOrder_Validation2;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //1、配件有效性校验（配件信息.配件编号=导入数据.配件图号），
                    //如果校验不通过，记录校验错误信息：配件不存在。
                    var sparePartCodesNeedCheck = tempRightList.Select(r => r.SparePartCodeStr).Distinct().ToArray();
                    var dbSpareParts = new List<ForceReserveBillDetailExtend>();
                    Func<string[], bool> getDbSpareParts = value => {
                        var dbObj = new ForceReserveBillDetailExtend {
                            SparePartId = Convert.ToInt32(value[0]),
                            SparePartCode = value[1],
                            SparePartName = value[2],
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("Select Id,Code,Name  From SparePart Where Status = {0}", (int)DcsMasterDataStatus.有效), "Code", true, sparePartCodesNeedCheck, getDbSpareParts);
                    var errorSpareParts = tempRightList.Where(r => dbSpareParts.All(v => v.SparePartCode != r.SparePartCodeStr)).ToList();
                    foreach(var errorItem in errorSpareParts) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_InternalAcquisitionDetail_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempSparePart = dbSpareParts.Single(r => r.SparePartCode == rightItem.SparePartCodeStr);
                        rightItem.SparePartId = tempSparePart.SparePartId;
                        rightItem.SparePartCode = tempSparePart.SparePartCode;
                        rightItem.SparePartName = tempSparePart.SparePartName;
                    }
                    //1、企业有效性校验（配件信息.配件编号=导入数据.配件图号），
                    //如果校验不通过，记录校验错误信息：配件不存在。
                    var companyCodeCheck = tempRightList.Select(r => r.CompanyCodeStr).Distinct().ToArray();
                    var dbCompany = new List<ForceReserveBillDetailExtend>();
                    Func<string[], bool> getDbCompanys = value => {
                        var dbObj = new ForceReserveBillDetailExtend {
                            CompanyId = Convert.ToInt32(value[0]),
                            CompanyCode = value[1],
                            CompanyName = value[2],
                        };
                        dbCompany.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator(string.Format("select dl.Id,dl.code,dl.name from DealerServiceInfo ds join ChannelCapability cc  on ds.ChannelCapabilityId = cc.id and ds.status = 1 and (cc.name = '经销商' or cc.name = '服务站')  join Dealer dl  on ds.dealerid = dl.id and dl.status = 1 where ds.status ={0}", (int)DcsMasterDataStatus.有效), "dl.Code", true, companyCodeCheck, getDbCompanys);
                    var errorCompanys = tempRightList.Where(r => dbCompany.All(v => v.CompanyCode != r.CompanyCodeStr)).ToList();
                    foreach(var errorItem in errorCompanys) {
                        errorItem.ErrorMsg = ErrorStrings.Export_Validation_CompanyDetail_Validation1;
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    foreach(var rightItem in tempRightList) {
                        var tempSparePart = dbCompany.Single(r => r.CompanyCode == rightItem.CompanyCodeStr);
                        rightItem.CompanyId = tempSparePart.CompanyId;
                        rightItem.CompanyCode = tempSparePart.CompanyCode;
                        rightItem.CompanyName = tempSparePart.CompanyName;
                    }
                    var sparePartIds = dbSpareParts.Select(r => r.SparePartId.ToString(CultureInfo.InvariantCulture)).Distinct().ToArray();
                    if(sparePartIds.Any()) {
                        var dbPartsStocks = new List<ForceReserveBillDetailExtend>();
                        Func<string[], bool> getDbPartsStocks = value => {
                            var dbObj = new ForceReserveBillDetailExtend {
                                CenterPrice = Decimal.Parse(value[0]),
                                SparePartCode = value[1]
                            };
                            dbPartsStocks.Add(dbObj);
                            return false;
                        };
                        var sql1 = string.Format(@"Select SalesPrice,SparePartCode  from PartsSalesPrice a
                                                    Where a.status=1  ");
                        db.QueryDataWithInOperator(sql1, "a.SparePartId", false, sparePartIds, getDbPartsStocks);
                        foreach(var tempRight in tempRightList) {
                            var partsStock = dbPartsStocks.FirstOrDefault(r => r.SparePartCode == tempRight.SparePartCodeStr);
                            if(partsStock != null) {
                                tempRight.CenterPrice = partsStock.CenterPrice;
                            }
                        }
                    }                  
                    //当储备类别是L时，根据服务站隶属中心库的ABC分类中，校验是否属于AB类型
                    //if(reserveType == "L") {
                    //    foreach(var companyId in tempRightList.Select(t=>t.CompanyId)) {
                    //        var spId = tempRightList.Where(t => t.CompanyId == companyId).Select(r => r.SparePartCode).Distinct().ToArray();
                    //        var dbClass = new List<ForceReserveBillDetailExtend>();
                    //        Func<string[], bool> getDbClass = value => {
                    //            var dbObj = new ForceReserveBillDetailExtend {
                    //                SparePartId = Convert.ToInt32(value[0]),
                    //                CenterPartProperty = Convert.ToInt32(value[1]),
                    //                CompanyId = Convert.ToInt32(value[2]),
                    //            };
                    //            dbClass.Add(dbObj);
                    //            return false;
                    //        };
                    //        db.QueryDataWithInOperator(string.Format("select ca.SparePartId,ca.NewType,ar.DealerId from AgencyDealerRelation ar  join CentralABCClassification ca  on ar.agencyid = ca.centerid  join sparepart sp on ca.sparepartid =sp.id where ar.status = 1 and ca.newtype in (1, 2) and ar.dealerid = {0} ", companyId), "sp.code", true, spId, getDbClass);
                    //        var errorClass = tempRightList.Where(r => r.CompanyId == companyId && dbClass.All(v => v.SparePartId != r.SparePartId || v.CompanyId != r.CompanyId)).ToList();
                    //        foreach(var errorItem in errorClass) {
                    //            errorItem.ErrorMsg = "不属于AB类";
                    //        }
                    //        tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    //        foreach(var rightItem in tempRightList) {
                    //            var tempSparePart = dbClass.Single(r => r.SparePartId == rightItem.SparePartId && r.CompanyId == rightItem.CompanyId);
                    //            rightItem.CenterPartProperty = tempSparePart.CenterPartProperty;
                    //        }
                    //    }
                    //}
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    //当储备类别是L时，根据服务站隶属中心库的ABC分类中，校验是否属于AB类型
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.CompanyCodeStr,
                                tempObj.CompanyNameStr,
                                tempObj.SparePartCodeStr,
                                tempObj.SparePartNameStr,
                                tempObj.ForceReserveQtyStr,
                                tempObj.ErrorMsg
                                #endregion
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
                rightData = rightList;
            }
        }
    }
}