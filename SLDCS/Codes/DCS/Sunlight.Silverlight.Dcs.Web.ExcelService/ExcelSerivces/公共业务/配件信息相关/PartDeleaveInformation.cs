﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Helper;
using Sunlight.Silverlight.Dcs.Web.ExcelService.Resources;
using Sunlight.Silverlight.Web.Extensions.ExcelOperators;

namespace Sunlight.Silverlight.Dcs.Web.ExcelService {
    public partial class ExcelService {
        public bool ExportPartDeleaveInformation(int? id, string oldPartCode, string deleavePartCode, string deleavePartName, int? partsSalesCategoryId, DateTime? createTimeBegin, DateTime? createTimeEnd, int? status, out string fileName) {
            fileName = GetExportFilePath("配件拆散件信息_" + ".xlsx");
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated) {
                    throw new Exception(ErrorStrings.File_Validation_User);
                }
                var db = DbHelper.GetDbHelp(ConnectionString);
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    var sql = new StringBuilder();
                    sql.Append(@"select a.OldPartCode，
                                        a.DeleavePartCode， 
                                        a.DeleavePartName， 
                                        b.name as BranchName， 
                                        c.name as PartsSalesCategoryName， 
                                        a.DeleaveAmount，
                                        a.Remark， 
                                        a.CreatorName， 
                                        a.CreateTime， 
                                        a.ModifierName， 
                                        a.ModifyTime
                                  from PartDeleaveInformation a
                                 inner join branch b
                                    on a.branchid = b.id
                                   and b.status = 1
                                 inner join partssalescategory c
                                    on a.partssalescategoryid = c.id
                                   and c.status = 1 where 1=1 ");
                    var dbParameters = new List<DbParameter>();
                    if(id.HasValue) {
                        sql.Append(@" and a.id = {0}id ");
                        dbParameters.Add(db.CreateDbParameter("id", id));
                    } else {
                        if(!string.IsNullOrEmpty(oldPartCode)) {
                            sql.Append(@" and a.oldPartCode like {0}oldPartCode ");
                            dbParameters.Add(db.CreateDbParameter("oldPartCode", '%' + oldPartCode + '%'));
                        }
                        if(!string.IsNullOrEmpty(deleavePartCode)) {
                            sql.Append(@" and a.deleavePartCode like {0}deleavePartCode ");
                            dbParameters.Add(db.CreateDbParameter("deleavePartCode", '%' + deleavePartCode + '%'));
                        }
                        if(!string.IsNullOrEmpty(deleavePartName)) {
                            sql.Append(@" and a.deleavePartName like {0}deleavePartName ");
                            dbParameters.Add(db.CreateDbParameter("deleavePartName", '%' + deleavePartName + '%'));
                        }
                        if(partsSalesCategoryId.HasValue) {
                            sql.Append(@" and a.partsSalesCategoryId ={0}partsSalesCategoryId ");
                            dbParameters.Add(db.CreateDbParameter("partsSalesCategoryId", partsSalesCategoryId.Value));
                        }
                        if(createTimeBegin.HasValue) {
                            sql.Append(@" and a.createTime >=To_date({0}createTimeBegin,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeBegin.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 0, 0, 0);
                            dbParameters.Add(db.CreateDbParameter("createTimeBegin", tempTime.ToString("G")));
                        }
                        if(createTimeEnd.HasValue) {
                            sql.Append(" and a.createTime<=To_date({0}createTimeEnd,'yyyy-mm-dd hh24:mi:ss')");
                            var tempValue = createTimeEnd.Value;
                            var tempTime = new DateTime(tempValue.Year, tempValue.Month, tempValue.Day, 23, 59, 59);
                            dbParameters.Add(db.CreateDbParameter("createTimeEnd", tempTime.ToString("G")));
                        }
                        if(status.HasValue) {
                            sql.Append(@" and a.status ={0}status ");
                            dbParameters.Add(db.CreateDbParameter("status", status.Value));
                        }
                    }
                    var sqlStr = sql.ToString();
                    if(sqlStr.IndexOf(" where 1=1  and ") > 0)
                        sqlStr = sqlStr.Replace(" where 1=1  and ", " where ");
                    else
                        sqlStr = sqlStr.Replace(" where 1=1 ", "");

                    var command = db.CreateDbCommand(string.Format(sqlStr, db.ParamMark), conn, null);
                    command.Parameters.AddRange(dbParameters.ToArray());
                    var reader = command.ExecuteReader();
                    using(var excelExport = new ExcelExport(fileName)) {
                        excelExport.ExportByRow(index => {
                            if(index == 0) {
                                return new object[] {
                                    "原件图号", "拆散件图号", "拆散件名称", ErrorStrings.Export_Title_PartsBranch_BranchName, ErrorStrings.Export_Title_Partssalescategory_Name, "拆散数量", ErrorStrings.Export_Title_PartsBranch_Remark, ErrorStrings.Export_Title_AccountPeriod_CreatorName, ErrorStrings.Export_Title_AccountPeriod_CreateTime, ErrorStrings.Export_Title_AccountPeriod_ModifierName, ErrorStrings.Export_Title_AccountPeriod_ModifyTime
                                };
                            }
                            if(reader.Read()) {
                                var values = new object[reader.FieldCount];
                                var num = reader.GetValues(values);
                                if(num != reader.FieldCount) {
                                    throw new Exception(ErrorStrings.Export_Validation_DataError);
                                }
                                return values;
                            }

                            return null;
                        });
                    }
                    reader.Close();
                }
                return true;
            } catch(Exception) {
                return false;
            }
        }

        /// <summary>
        /// 导入配件拆散信息
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="excelImportNum"></param>
        /// <param name="branchId">登陆企业Id</param>
        /// <param name="errorData"></param>
        /// <param name="errorDataFileName"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool ImportPartDeleaveInformation(string fileName, out int excelImportNum, int branchId, out List<PartDeleaveInformationExtend> errorData, out string errorDataFileName, out string errorMessage) {
            excelImportNum = 0;
            errorMessage = "";
            errorDataFileName = "";
            var errorList = new List<PartDeleaveInformationExtend>();
            var allList = new List<PartDeleaveInformationExtend>();
            try {
                if(!HttpContext.Current.User.Identity.IsAuthenticated)
                    throw new Exception(ErrorStrings.File_Validation_User);
                var db = DbHelper.GetDbHelp(ConnectionString);

                //获取指定表结构
                List<string> notNullableFields;
                Dictionary<string, int> fieldLenght;
                db.GetTableSchema("PartDeleaveInformation", out notNullableFields, out fieldLenght);

                List<string> notNullableFieldsPartsSalesCategory;
                Dictionary<string, int> fieldLenghtPartsSalesCategory;
                db.GetTableSchema("PartsSalesCategory", out notNullableFieldsPartsSalesCategory, out fieldLenghtPartsSalesCategory);


                List<object> excelColumns;
                List<PartDeleaveInformationExtend> rightList;
                using(var excelOperator = new ExcelImport(GetImportFilePath(fileName), ConnectionString)) {
                    #region 指定文件中的列对应的名称 例如 excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Accessinfo_Tmp_SpareCode, "PartCode");
                    excelOperator.AddColumnDataSource("原件图号", "OldPartCode");
                    excelOperator.AddColumnDataSource("拆散件图号", "DeleavePartCode");
                    excelOperator.AddColumnDataSource("拆散件名称", "DeleavePartName");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_Partssalescategory_Name, "PartsSalesCategoryName");
                    excelOperator.AddColumnDataSource("拆散数量", "DeleaveAmount");
                    excelOperator.AddColumnDataSource(ErrorStrings.Export_Title_PartsBranch_Remark, "Remark");
                    #endregion
                    excelColumns = excelOperator.Columns.Select(v => (object)v.Name).ToList();
                    var tempExcelOperator = excelOperator;
                    #region 获取对应枚举
                    var keyValuePairs = new[] { new KeyValuePair<string, string>("BaseDataStatus", "BaseData_Status") };
                    tempExcelOperator.ImportHelper.LoadEnumFromDb(null, null, keyValuePairs);

                    #endregion
                    excelImportNum = excelOperator.LoadExcelRow(row => {
                        var tempImportObj = new PartDeleaveInformationExtend();
                        var newRow = row.Keys.ToDictionary(key => key, key => row[key] != null ? row[key].Trim() : row[key]);
                        #region 根据导入的字段给对应的实体属性赋值 tempImportObj.PartsSalesCategoryNameStr = row["PartsSalesCategoryName"];
                        tempImportObj.OldPartCodeStr = newRow["OldPartCode"];
                        tempImportObj.DeleavePartCodeStr = newRow["DeleavePartCode"];
                        tempImportObj.DeleavePartNameStr = newRow["DeleavePartName"];
                        tempImportObj.PartsSalesCategoryNameStr = newRow["PartsSalesCategoryName"];
                        tempImportObj.DeleaveAmountStr = newRow["DeleaveAmount"];
                        tempImportObj.RemarkStr = newRow["Remark"];


                        #endregion

                        var tempErrorMessage = new List<string>();
                        #region 导入的数据基本检查
                        //原件图号 检查
                        var fieldIndex = notNullableFields.IndexOf("OldPartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.OldPartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("原件图号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.OldPartCodeStr) > fieldLenght["OldPartCode".ToUpper()])
                                tempErrorMessage.Add("原件图号过长");
                        }

                        //拆散件图号 检查
                        fieldIndex = notNullableFields.IndexOf("DeleavePartCode".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DeleavePartCodeStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("拆散件图号不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DeleavePartCodeStr) > fieldLenght["DeleavePartCode".ToUpper()])
                                tempErrorMessage.Add("拆散件图号过长");
                        }

                        //拆散件名称 检查
                        fieldIndex = notNullableFields.IndexOf("DeleavePartName".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DeleavePartNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("拆散件名称不能为空");
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.DeleavePartNameStr) > fieldLenght["DeleavePartName".ToUpper()])
                                tempErrorMessage.Add("拆散件名称过长");
                        }

                        //品牌检查
                        fieldIndex = notNullableFieldsPartsSalesCategory.IndexOf("Name".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.PartsSalesCategoryNameStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsNull);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.PartsSalesCategoryNameStr) > fieldLenghtPartsSalesCategory["Name".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.Export_Validation_MarketingDepartment_PartsSalesCategoryIsLong);
                        }

                        //拆散数量检查
                        fieldIndex = notNullableFields.IndexOf("DeleaveAmount".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.DeleaveAmountStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add("拆散数量不能为空");
                        } else {
                            int checkValue;
                            if(int.TryParse(tempImportObj.DeleaveAmountStr, out checkValue)) {
                                if(checkValue < 0) {
                                    tempErrorMessage.Add("拆散数量必须大于0");
                                }
                            } else {

                                tempErrorMessage.Add("拆散数量必须是数字");
                            }
                        }

                        //备注检查
                        fieldIndex = notNullableFields.IndexOf("Remark".ToUpper());
                        if(string.IsNullOrEmpty(tempImportObj.RemarkStr)) {
                            if(fieldIndex > -1)
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation15);
                        } else {
                            if(Encoding.Default.GetByteCount(tempImportObj.RemarkStr) > fieldLenght["Remark".ToUpper()])
                                tempErrorMessage.Add(ErrorStrings.ImpPartsPurchasePricingDetail_Validation16);
                        }
                        #endregion
                        if(tempErrorMessage.Count > 0) {
                            tempImportObj.ErrorMsg = string.Join("; ", tempErrorMessage);
                        }
                        allList.Add(tempImportObj);
                        return false;
                    });
                    var tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();
                    #region 剩下的数据进行业务检查
                    //原件图号在配件拆散信息中是否存在，如果存在，提示“导入原件xxx拆散信息已存在”
                    var oldPartCodesNeedCheck = tempRightList.Select(r => r.OldPartCodeStr.ToUpper()).Distinct().ToArray();
                    var dbPartDeleaveInformations = new List<PartDeleaveInformationExtend>();
                    Func<string[], bool> getDbPartDeleaveInformations = value => {
                        var dbObj = new PartDeleaveInformationExtend {
                            OldPartId = Convert.ToInt32(value[0]),
                            OldPartCode = value[1]
                        };
                        dbPartDeleaveInformations.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select OldPartId,OldPartCode from PartDeleaveInformation where status=1 ", "OldPartCode", true, oldPartCodesNeedCheck, getDbPartDeleaveInformations);
                    foreach(var tempRight in tempRightList) {
                        var partDeleaveInformation = dbPartDeleaveInformations.FirstOrDefault(v => v.OldPartCode == tempRight.OldPartCodeStr);
                        if(partDeleaveInformation != null) {
                            tempRight.ErrorMsg = String.Format("导入原件“{0}”在拆散信息中已存在", tempRight.OldPartId);
                        }
                    }
                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //原件图号在配件信息中是否存在，如果不存在，提示“导入原件xxx配件信息不存在”
                    var dbSpareParts = new List<SparePartExtend>();
                    Func<string[], bool> getDbSparePartCodes = value => {
                        var dbObj = new SparePartExtend {
                            Id = Convert.ToInt32(value[0]),
                            Code = value[1]
                        };
                        dbSpareParts.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Code from SparePart where status=1 ", "Code", true, oldPartCodesNeedCheck, getDbSparePartCodes);
                    foreach(var tempRight in tempRightList) {
                        var oldSparePart = dbSpareParts.FirstOrDefault(v => v.Code == tempRight.OldPartCodeStr);
                        if(oldSparePart == null) {
                            tempRight.ErrorMsg = String.Format("导入原件“{0}”在配件信息中不存在", tempRight.OldPartCodeStr);
                            continue;
                        }
                        tempRight.OldPartId = oldSparePart.Id;
                    }

                    tempRightList = allList.Where(r => r.ErrorMsg == null).ToList();

                    //3.导入品牌在配件销售类型中是否存在，如果不存在，提示“导入原件xxx的品牌不存在”
                    //如果品牌存在，校验品牌所属配件销售类型.营销分公司Id是否=登陆企业Id,如果不同，提示“导入品牌不属于当前所属分公司”
                    var partsSalesCategoryNamesNeedCheck = tempRightList.Select(r => r.PartsSalesCategoryNameStr.ToUpper()).Distinct().ToArray();
                    var dbPartsSalesCategorys = new List<PartsSalesCategoryExtend>();
                    Func<string[], bool> getDbPartsSalesCategorys = value => {
                        var dbObj = new PartsSalesCategoryExtend {
                            Id = Convert.ToInt32(value[0]),
                            Name = value[1]
                        };
                        dbPartsSalesCategorys.Add(dbObj);
                        return false;
                    };
                    db.QueryDataWithInOperator("select Id,Name from PartsSalesCategory where status=1 ", "Name", false, partsSalesCategoryNamesNeedCheck, getDbPartsSalesCategorys);
                    foreach(var tempRight in tempRightList) {
                        var partsSalesCategory = dbPartsSalesCategorys.FirstOrDefault(v => v.Name == tempRight.PartsSalesCategoryNameStr);
                        if(partsSalesCategory == null) {
                            tempRight.ErrorMsg = String.Format("导入原件“{0}”在配件信息中不存在", tempRight.PartsSalesCategoryNameStr);
                            continue;
                        }
                        if(partsSalesCategory.BranchId != branchId) {
                            tempRight.ErrorMsg = "导入品牌不属于当前所属分公司";
                            continue;
                        }
                        tempRight.PartsSalesCategoryId = partsSalesCategory.Id;

                    }

                    #endregion
                    //获取所有不合格数据
                    errorList = allList.Where(r => r.ErrorMsg != null).ToList();
                    //获取所有合格数据
                    rightList = allList.Except(errorList).ToList();
                    #region 将合格数据的值填上

                    foreach(var rightItem in rightList) {
                        rightItem.BranchId = branchId;
                        rightItem.DeleavePartCode = rightItem.DeleavePartCodeStr;
                        rightItem.DeleavePartName = rightItem.DeleavePartNameStr;
                        rightItem.DeleaveAmount = Convert.ToInt32(rightItem.DeleaveAmountStr);
                        rightItem.OldPartCode = rightItem.OldPartCodeStr;
                        rightItem.Remark = rightItem.RemarkStr;
                    }
                    #endregion
                }
                //导出所有不合格数据
                if(errorList.Any()) {
                    excelColumns.Add(ErrorStrings.Export_validation_PartsExchangeGroupExtend_CheckInformation);
                    errorDataFileName = GetErrorFilePath(fileName);
                    using(var excelExport = new ExcelExport(errorDataFileName)) {
                        var list = errorList;
                        excelExport.ExportByRow(index => {
                            if(index == list.Count + 1)
                                return null;
                            if(index == 0)
                                return excelColumns.ToArray();
                            var tempObj = list[index - 1];
                            var values = new object[] {
                                #region 设置错误信息导出的列的值
                                tempObj.OldPartCodeStr,tempObj.DeleavePartCodeStr,
                                tempObj.DeleavePartNameStr,tempObj.PartsSalesCategoryNameStr,
                                tempObj.DeleaveAmount,tempObj.RemarkStr,
                                tempObj.ErrorMsg
                                #endregion                     
                            };
                            return values;
                        });
                    }
                    errorList = null;
                }

                //导入所有合格数据
                if(!rightList.Any())
                    return true;
                using(var conn = db.CreateDbConnection()) {
                    conn.Open();
                    //开启事务，新增更新配件在一个事务内
                    var ts = conn.BeginTransaction();
                    try {
                        //新增配件
                        if(rightList.Any()) {
                            #region 获取对应的插入的SQL 通过db.GetInsertSql方法
                            //获取新增数据的sql语句，Id为主键
                            var sqlInsert = db.GetInsertSql("PartDeleaveInformation", "Id", new[] {
                                "OldPartId","OldPartCode","BranchId","PartsSalesCategoryId","DeleavePartCode","DeleavePartName","DeleaveAmount","Remark"
                            });

                            var sqlHistoryInsert = db.GetInsertSql("PartDeleaveInformationHistory", "Id", new[] {
                                "PartDeleaveInformationId","OldPartId","OldPartCode","BranchId","PartsSalesCategoryId","DeleavePartCode","DeleavePartName","DeleaveAmount","Remark"
                            });
                            #endregion
                            //往数据库增加配件信息
                            foreach(var item in rightList) {
                                #region 添加Sql的参数
                                var command = db.CreateDbCommand(sqlInsert, conn, ts);
                                command.Parameters.Add(db.CreateDbParameter("OldPartId", item.OldPartId));
                                command.Parameters.Add(db.CreateDbParameter("OldPartCode", item.OldPartCode));
                                command.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                command.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                command.Parameters.Add(db.CreateDbParameter("DeleavePartCode", item.DeleavePartCode));
                                command.Parameters.Add(db.CreateDbParameter("DeleavePartName", item.DeleavePartName));
                                command.Parameters.Add(db.CreateDbParameter("DeleaveAmount", item.DeleaveAmount));
                                command.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                #endregion
                                var tempId = db.ExecuteInsert(command, "Id");
                                //加履历
                                var commandHistory = db.CreateDbCommand(sqlHistoryInsert, conn, ts);
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartDeleaveInformationId", tempId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("OldPartId", item.OldPartId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("OldPartCode", item.OldPartCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("BranchId", item.BranchId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("PartsSalesCategoryId", item.PartsSalesCategoryId));
                                commandHistory.Parameters.Add(db.CreateDbParameter("DeleavePartCode", item.DeleavePartCode));
                                commandHistory.Parameters.Add(db.CreateDbParameter("DeleavePartName", item.DeleavePartName));
                                commandHistory.Parameters.Add(db.CreateDbParameter("DeleaveAmount", item.DeleaveAmount));
                                commandHistory.Parameters.Add(db.CreateDbParameter("Remark", item.Remark));
                                commandHistory.ExecuteNonQuery();
                            }
                        }
                        //无异常提交
                        ts.Commit();
                    } catch(Exception) {
                        //报错回滚
                        ts.Rollback();
                        throw;
                    } finally {
                        if(conn.State == System.Data.ConnectionState.Open) {
                            conn.Close();
                        }
                    }
                }
                return true;
            } catch(Exception ex) {
                errorMessage = ex.Message;
                errorList = null;
                return false;
            } finally {
                errorData = errorList;
            }
        }

    }
}
